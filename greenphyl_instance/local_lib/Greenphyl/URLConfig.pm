=pod

=head1 NAME

Greenphyl::URLConfig - Config file for URLs

=head1 SYNOPSIS

    use Greenphyl::URLConfig;

=head1 REQUIRES

Perl5

=head1 EXPORTS

Nothing

=head1 DESCRIPTION

Contains URLs definitions that are imported by Greenphyl::Config.

=cut

package Greenphyl::URLConfig;


use strict;
use warnings;




# Package constants
####################

=pod

=head1 CONSTANTS

B<$URLS>:  (hash)

Hash of URL definitions.

=cut

our $BASE_URL            = 'http://www.greenphyl.org';

our $PATH_URL_TYPE       = 1;
our $FILE_URL_TYPE       = 2;
our $JAVASCRIPT_URL_TYPE = 3;
our $CGI_URL_TYPE        = 4;
our $ACTION_URL_TYPE     = 5;
our $APPLET_URL_TYPE     = 6;
our $EXTERNAL_URL_TYPE   = 7;

our $URLS = {
    # Base URLs
    'base' => {
            'url' => $BASE_URL,
            'anchor' => '',
            'type' => $PATH_URL_TYPE,
        },
    'current' => {
            'url' => CGI::url(),
            'anchor' => '',
            'type' => $PATH_URL_TYPE,
        },
    'current_query' => {
            'url' => CGI::url('-query'=>1),
            'anchor' => '',
            'type' => $PATH_URL_TYPE,
        },
    'cgi' => {
            'url' => $BASE_URL . '/cgi-bin',
            'anchor' => '',
            'type' => $PATH_URL_TYPE,
        },
    'images' => {
            'url' => $BASE_URL . '/img',
            'anchor' => '',
            'type' => $PATH_URL_TYPE,
        },
    'applets' => {
            'url' => $BASE_URL . '/applets',
            'anchor' => '',
            'type' => $PATH_URL_TYPE,
        },
    'javascripts' => {
            'url' => $BASE_URL . '/js',
            'anchor' => '',
            'type' => $PATH_URL_TYPE,
        },
    'css' => {
            'url' => $BASE_URL . '/css',
            'anchor' => '',
            'type' => $PATH_URL_TYPE,
        },
    'media' => {
            'url' => $BASE_URL . '/media',
            'anchor' => '',
            'type' => $PATH_URL_TYPE,
        },
    'videos' => {
            'url' => $BASE_URL . '/media/videos',
            'anchor' => '',
            'type' => $PATH_URL_TYPE,
        },
    'temp' => {
            'url' => $BASE_URL . '/tmp',
            'anchor' => '',
            'type' => $PATH_URL_TYPE,
        },
    'doc' => {
            'url' => $BASE_URL . '/doc',
            'anchor' => '',
            'type' => $PATH_URL_TYPE,
        },
    'multi_align' => {
            'url' => $BASE_URL . '/alm',
            'anchor' => '',
            'type' => $PATH_URL_TYPE,
        },
    'trees' => {
            'url' => $BASE_URL . '/trees',
            'anchor' => '',
            'type' => $PATH_URL_TYPE,
        },
    #'meme' => {
    #        'url' => $BASE_URL . '/meme',
    #        'anchor' => '',
    #        'type' => $PATH_URL_TYPE,
    #    },
    #'chromosomes' => {
    #        'url' => $BASE_URL . '/chromosomes',
    #        'anchor' => '',
    #        'type' => $PATH_URL_TYPE,
    #    },
    # Pages/scripts
    'home' => {
            'url' => $BASE_URL . '/cgi-bin/index.cgi',
            'anchor' => '',
            'type' => $CGI_URL_TYPE,
        },
    'quick_search' => {
            'url' => $BASE_URL . '/cgi-bin/quick_search.cgi',
            'anchor' => '',
            'type' => $CGI_URL_TYPE,
        },
    'quick_search_service' => {
            'url' => $BASE_URL . '/cgi-bin/quick_search.pl',
            'anchor' => '',
            'type' => $CGI_URL_TYPE,
        },
    'search_families' => {
            'url' => $BASE_URL . '/cgi-bin/search_families.cgi',
            'anchor' => '',
            'type' => $CGI_URL_TYPE,
        },
    'search_go' => {
            'url' => $BASE_URL . '/cgi-bin/search_go.cgi',
            'anchor' => '',
            'type' => $CGI_URL_TYPE,
        },
    'blast' => {
            'url' => $BASE_URL . '/cgi-bin/blast.cgi',
            'anchor' => '',
            'type' => $CGI_URL_TYPE,
        },
    'seq2families' => {
            'url' => $BASE_URL . '/cgi-bin/seq2families.cgi',
            'anchor' => '',
            'type' => $CGI_URL_TYPE,
        },
    'homologs' => {
            'url' => $BASE_URL . '/cgi-bin/get_homologs.cgi',
            'anchor' => '',
            'type' => $CGI_URL_TYPE,
        },
    'get_homologs_data' => {
            'url' => $BASE_URL . '/cgi-bin/sequences.pl?service=get_homologs_data',
            'anchor' => '',
            'type' => $ACTION_URL_TYPE,
        },
    'ipr2genomes' => {
            'url' => $BASE_URL . '/cgi-bin/ipr2genomes.cgi',
            'anchor' => '',
            'type' => $CGI_URL_TYPE,
        },
    #'meme_blast_search' => {
    #        'url' => $BASE_URL . '/cgi-bin/meme_blast_search.cgi',
    #        'anchor' => '',
    #        'type' => $CGI_URL_TYPE,
    #    },
    'export_sequences' => {
            'url' => $BASE_URL . '/cgi-bin/export_sequences.cgi',
            'anchor' => '',
            'type' => $CGI_URL_TYPE,
        },
    #'transeq' => {
    #        'url' => $BASE_URL . '/cgi-bin/transeq.cgi',
    #        'anchor' => '',
    #        'type' => $CGI_URL_TYPE,
    #    },
    'custom_phylo' => {
            'url' => $BASE_URL . '/cgi-bin/run_custom_phylo.cgi',
            'anchor' => '',
            'type' => $CGI_URL_TYPE,
        },
    'login' => {
            'url' => $BASE_URL . '/cgi-bin/login.cgi',
            'anchor' => '',
            'type' => $CGI_URL_TYPE,
        },
    'annotation_management' => {
            'url' => $BASE_URL . '/cgi-bin/admin/annotations.cgi',
            'anchor' => '',
            'type' => $CGI_URL_TYPE,
        },
    'annotate_family' => {
            'url' => $BASE_URL . '/cgi-bin/annotate_family.cgi',
            'anchor' => '',
            'type' => $CGI_URL_TYPE,
        },
    'get_tree' => {
            'url' => $BASE_URL . '/cgi-bin/tree.pl?service=get_tree',
            'anchor' => '',
            'type' => $ACTION_URL_TYPE,
        },
    'suggest_name' => {
            'url' => $BASE_URL . '/cgi-bin/suggestname.cgi',
            'anchor' => '',
            'type' => $CGI_URL_TYPE,
        },
    'fetch_sequence' => {
            'url' => $BASE_URL . '/cgi-bin/sequence.cgi?p=id',
            'anchor' => '',
            'type' => $ACTION_URL_TYPE,
        },
    'fetch_custom_sequence' => {
            'url' => $BASE_URL . '/cgi-bin/custom_sequence.cgi?p=id',
            'anchor' => '',
            'type' => $ACTION_URL_TYPE,
        },
    'fetch_family' => {
            'url' => $BASE_URL . '/cgi-bin/family.cgi?p=id',
            'anchor' => '',
            'type' => $ACTION_URL_TYPE,
        },
    'fetch_custom_family' => {
            'url' => $BASE_URL . '/cgi-bin/custom_family.cgi?p=id',
            'anchor' => '',
            'type' => $ACTION_URL_TYPE,
        },
    'fetch_families' => {
            'url' => $BASE_URL . '/cgi-bin/families.pl',
            'anchor' => '',
            'type' => $CGI_URL_TYPE,
        },
    'get_ipr_specificity' => {
            'url' => $BASE_URL . '/cgi-bin/families.pl?service=get_ipr_specificity',
            'anchor' => '',
            'type' => $ACTION_URL_TYPE,
        },
    'get_ipr_relationship' => {
            'url' => $BASE_URL . '/cgi-bin/families.pl?service=get_ipr_relationship',
            'anchor' => '',
            'type' => $ACTION_URL_TYPE,
        },
    'get_family_relationship' => {
            'url' => $BASE_URL . '/cgi-bin/families.pl?service=get_family_relationship',
            'anchor' => '',
            'type' => $ACTION_URL_TYPE,
        },
    'get_custom_family_relationship' => {
            'url' => $BASE_URL . '/cgi-bin/families.pl?service=get_custom_family_relationship',
            'anchor' => '',
            'type' => $ACTION_URL_TYPE,
        },
    'get_annotations' => {
            'url' => $BASE_URL . '/cgi-bin/annotations.pl?service=get_annotations',
            'anchor' => '',
            'type' => $ACTION_URL_TYPE,
        },
    'fetch_family_list' => {
            'url' => $BASE_URL . '/cgi-bin/families.cgi?p=list',
            'anchor' => '',
            'type' => $ACTION_URL_TYPE,
        },
    'fetch_taxonomy_families' => {
            'url' => $BASE_URL . '/cgi-bin/families.cgi?p=specific',
            'anchor' => '',
            'type' => $ACTION_URL_TYPE,
        },
    'fetch_transcription_factor_families' => {
            'url' => $BASE_URL . '/cgi-bin/families.cgi?p=list&type=transcription_factor',
            'anchor' => '',
            'type' => $ACTION_URL_TYPE,
        },
    'families_processed' => {
            'url' => $BASE_URL . '/cgi-bin/families.cgi?p=list&type=processed',
            'anchor' => '',
            'type' => $ACTION_URL_TYPE,
        },
    'fetch_custom_family_list' => {
            'url' => $BASE_URL . '/cgi-bin/custom_families.cgi?p=list',
            'anchor' => '',
            'type' => $ACTION_URL_TYPE,
        },
    'go_browser' => {
            'url' => $BASE_URL . '/cgi-bin/go_browser.cgi',
            'anchor' => '',
            'type' => $CGI_URL_TYPE,
        },
    'orphans' => {
            'url' => $BASE_URL . '/cgi-bin/orphans.cgi',
            'anchor' => '',
            'type' => $CGI_URL_TYPE,
        },
    'get_sequence_data' => {
            'url' => $BASE_URL . '/cgi-bin/sequences.pl?service=get_sequence_data',
            'anchor' => '',
            'type' => $ACTION_URL_TYPE,
        },
    'treepattern_page' => {
            'url' => $BASE_URL . '/cgi-bin/treepattern.cgi',
            'anchor' => '',
            'type' => $ACTION_URL_TYPE,
        },
    'documentation' => {
            'url' => $BASE_URL . '/cgi-bin/documentation.cgi',
            'anchor' => '',
            'type' => $CGI_URL_TYPE,
        },
    'statistics' => {
            'url' => $BASE_URL . '/cgi-bin/statistics.cgi',
            'anchor' => '',
            'type' => $CGI_URL_TYPE,
        },
    'mylist' => {
            'url' => $BASE_URL . '/cgi-bin/mylist.cgi',
            'anchor' => '',
            'type' => $CGI_URL_TYPE,
        },
    'mylist_dump' => {
            'url' => $BASE_URL . '/cgi-bin/mylist.cgi?p=dump',
            'anchor' => '',
            'type' => $CGI_URL_TYPE,
        },
    'mylist_list' => {
            'url' => $BASE_URL . '/cgi-bin/mylist.cgi?p=list',
            'anchor' => '',
            'type' => $CGI_URL_TYPE,
        },
    'mylist_clear' => {
            'url' => $BASE_URL . '/cgi-bin/mylist.cgi?p=clear',
            'anchor' => '',
            'type' => $CGI_URL_TYPE,
        },
    'mylist_save_form' => {
            'url' => $BASE_URL . '/cgi-bin/mylist.cgi?p=save_form',
            'anchor' => '',
            'type' => $CGI_URL_TYPE,
        },
    'mylist_save' => {
            'url' => $BASE_URL . '/cgi-bin/mylist.cgi?p=save',
            'anchor' => '',
            'type' => $CGI_URL_TYPE,
        },
    'mylist_delete' => {
            'url' => $BASE_URL . '/cgi-bin/mylist.cgi?p=delete',
            'anchor' => '',
            'type' => $CGI_URL_TYPE,
        },
    'mylist_load' => {
            'url' => $BASE_URL . '/cgi-bin/mylist.cgi?p=load',
            'anchor' => '',
            'type' => $CGI_URL_TYPE,
        },
    'mylist_import' => {
            'url' => $BASE_URL . '/cgi-bin/mylist.cgi?p=import',
            'anchor' => '',
            'type' => $CGI_URL_TYPE,
        },
    'mylist_merge' => {
            'url' => $BASE_URL . '/cgi-bin/mylist.cgi?p=merge',
            'anchor' => '',
            'type' => $CGI_URL_TYPE,
        },
    'mylist_intersect' => {
            'url' => $BASE_URL . '/cgi-bin/mylist.cgi?p=intersect',
            'anchor' => '',
            'type' => $CGI_URL_TYPE,
        },
    'mylist_substract' => {
            'url' => $BASE_URL . '/cgi-bin/mylist.cgi?p=substract',
            'anchor' => '',
            'type' => $CGI_URL_TYPE,
        },
    'mylist_to_sequences' => {
            'url' => $BASE_URL . '/cgi-bin/mylist.cgi?p=to_sequences',
            'anchor' => '',
            'type' => $CGI_URL_TYPE,
        },
    'mylist_add_entry' => {
            'url' => $BASE_URL . '/cgi-bin/mylist.cgi?p=add_entry',
            'anchor' => '',
            'type' => $CGI_URL_TYPE,
        },
    'mylist_remove_entry' => {
            'url' => $BASE_URL . '/cgi-bin/mylist.cgi?p=remove_entry',
            'anchor' => '',
            'type' => $CGI_URL_TYPE,
        },
    'mylist_add_entries' => {
            'url' => $BASE_URL . '/cgi-bin/mylist.cgi?p=add_entries',
            'anchor' => '',
            'type' => $CGI_URL_TYPE,
        },
    'mylist_intersect_entries' => {
            'url' => $BASE_URL . '/cgi-bin/mylist.cgi?p=intersect_entries',
            'anchor' => '',
            'type' => $CGI_URL_TYPE,
        },
    'mylist_substract_entries' => {
            'url' => $BASE_URL . '/cgi-bin/mylist.cgi?p=substract_entries',
            'anchor' => '',
            'type' => $CGI_URL_TYPE,
        },
    #'show_ipr_stats' => {
    #        'url' => $BASE_URL . '/cgi-bin/info_sup.cgi',
    #        'anchor' => '',
    #        'type' => $CGI_URL_TYPE,
    #    },
    # Files
    #'atve' => {
    #        'url' => $BASE_URL . '/applets/' . $ATVE_APPLET,
    #        'anchor' => '',
    #        'type' => $APPLET_URL_TYPE,
    #    },
    'fetch_go_families' => {
            'url' => $BASE_URL . '/cgi-bin/go.pl?service=get_go_families&format=html',
            'anchor' => '',
            'type' => $ACTION_URL_TYPE,
        },
    'get_go_family_distribution' => {
            'url' => $BASE_URL . '/cgi-bin/families.pl?service=get_go_distribution&format=html',
            'anchor' => '',
            'type' => $ACTION_URL_TYPE,
        },
    'get_go_subtree' => {
            'url' => $BASE_URL . '/cgi-bin/go.pl?service=get_go_subtree&format=json&max_depth=0',
            'anchor' => '',
            'type' => $ACTION_URL_TYPE,
        },
    'user_details' => {
            'url' => $BASE_URL . '/cgi-bin/user_details.cgi',
            'anchor' => '',
            'type' => $CGI_URL_TYPE,
        },
    'family_domains' => {
            'url' => $BASE_URL . '/cgi-bin/domains.pl?service=get_family_domains&format=html',
            'anchor' => '',
            'type' => $ACTION_URL_TYPE,
        },
    'administration' => {
            'url' => $BASE_URL . '/cgi-bin/admin/admin.cgi',
            'anchor' => '',
            'type' => $CGI_URL_TYPE,
        },
    'process_list' => {
            'url' => $BASE_URL . '/cgi-bin/admin/processes.cgi?p=list',
            'anchor' => '',
            'type' => $ACTION_URL_TYPE,
        },
    'process_details' => {
            'url' => $BASE_URL . '/cgi-bin/admin/processes.cgi?p=details',
            'anchor' => '',
            'type' => $ACTION_URL_TYPE,
        },
    'process_new' => {
            'url' => $BASE_URL . '/cgi-bin/admin/processes.cgi?p=new',
            'anchor' => '',
            'type' => $ACTION_URL_TYPE,
        },
    'process_save' => {
            'url' => $BASE_URL . '/cgi-bin/admin/processes.cgi?p=save',
            'anchor' => '',
            'type' => $ACTION_URL_TYPE,
        },
    'process_update' => {
            'url' => $BASE_URL . '/cgi-bin/admin/processes.cgi?p=update',
            'anchor' => '',
            'type' => $ACTION_URL_TYPE,
        },
    'check_style' => {
            'url' => $BASE_URL . '/cgi-bin/admin/check_style.cgi',
            'anchor' => '',
            'type' => $CGI_URL_TYPE,
        },
    'manage_logs' => {
            'url' => $BASE_URL . '/cgi-bin/admin/log.cgi',
            'anchor' => '',
            'type' => $CGI_URL_TYPE,
        },
    'report' => {
            'url' => $BASE_URL . '/cgi-bin/admin/report.cgi',
            'anchor' => '',
            'type' => $CGI_URL_TYPE,
        },
    'select_db' => {
            'url' => $BASE_URL . '/cgi-bin/admin/select_db.cgi',
            'anchor' => '',
            'type' => $CGI_URL_TYPE,
        },
    'manage_users' => {
            'url' => $BASE_URL . '/cgi-bin/admin/users.cgi',
            'anchor' => '',
            'type' => $CGI_URL_TYPE,
        },
    'user_add' => {
            'url' => $BASE_URL . '/cgi-bin/admin/users.cgi?p=add',
            'anchor' => '',
            'type' => $CGI_URL_TYPE,
        },
    'user_save' => {
            'url' => $BASE_URL . '/cgi-bin/admin/users.cgi?p=save',
            'anchor' => '',
            'type' => $CGI_URL_TYPE,
        },
    'user_delete' => {
            'url' => $BASE_URL . '/cgi-bin/admin/users.cgi?p=delete',
            'anchor' => '',
            'type' => $CGI_URL_TYPE,
        },
    'create_custom_family' => {
            'url' => $BASE_URL . '/cgi-bin/custom_family.cgi?p=create',
            'anchor' => '',
            'type' => $CGI_URL_TYPE,
        },
    'mylist_to_family' => {
            'url' => $BASE_URL . '/cgi-bin/custom_family.cgi?p=mylist_to_family',
            'anchor' => '',
            'type' => $CGI_URL_TYPE,
        },
    'add_mylist_to_family' => {
            'url' => $BASE_URL . '/cgi-bin/custom_family.cgi?p=add_mylist_to_family',
            'anchor' => '',
            'type' => $CGI_URL_TYPE,
        },
    'save_custom_family' => {
            'url' => $BASE_URL . '/cgi-bin/custom_family.cgi?p=save',
            'anchor' => '',
            'type' => $CGI_URL_TYPE,
        },
    'save_custom_family_relationships' => {
            'url' => $BASE_URL . '/cgi-bin/custom_family.cgi?p=save_relationships',
            'anchor' => '',
            'type' => $CGI_URL_TYPE,
        },
    'delete_custom_family' => {
            'url' => $BASE_URL . '/cgi-bin/custom_family.cgi?p=delete',
            'anchor' => '',
            'type' => $CGI_URL_TYPE,
        },
    # external URLS
    #'fetch_kegg_orthology' => {
    #        'url' => 'http://www.genome.jp/dbget-bin/www_bget?ko:',
    #        'anchor' => '',
    #        'type' => $EXTERNAL_URL_TYPE,
    #    },
    'fetch_ipr' => {
            'url' => 'http://www.ebi.ac.uk/interpro/entry/',
            'anchor' => '',
            'type' => $EXTERNAL_URL_TYPE,
        },
    #'pubmed' => {
    #        'url' => 'http://www.ncbi.nlm.nih.gov/pubmed/',
    #        'anchor' => '',
    #        'type' => $EXTERNAL_URL_TYPE,
    #    },
    #'fetch_pirsf' => {
    #        'url' => 'http://pir.georgetown.edu/cgi-bin/ipcSF?id=',
    #        'anchor' => '',
    #        'type' => $EXTERNAL_URL_TYPE,
    #    },
    #'fetch_go_amigo' => {
    #        'url' => 'http://amigo.geneontology.org/cgi-bin/amigo/term_details?term=',
    #        'anchor' => '',
    #        'type' => $EXTERNAL_URL_TYPE,
    #    },
    'fetch_go_ebi' => {
            'url' => 'http://www.ebi.ac.uk/QuickGO/GTerm?id=',
            'anchor' => '',
            'type' => $EXTERNAL_URL_TYPE,
        },
    'uniprot' => {
            'url' => 'http://www.uniprot.org/uniprot/',
            'anchor' => '',
            'type' => $EXTERNAL_URL_TYPE,
        },
    #'orygenesdb_gbrowse' => {
    #        'url' => 'http://orygenesdb.cirad.fr/cgi-bin/gbrowse/',
    #        'anchor' => '',
    #        'type' => $EXTERNAL_URL_TYPE,
    #    },
    #'orygenesdb_gbrowse_img' => {
    #        'url' => 'http://orygenesdb.cirad.fr/cgi-bin/gbrowse_img/',
    #        'anchor' => '',
    #        'type' => $EXTERNAL_URL_TYPE,
    #    },
    'ncbi_taxonomy' => {
            'url' => 'http://www.ncbi.nlm.nih.gov/Taxonomy/Browser/wwwtax.cgi',
            'anchor' => '',
            'type' => $EXTERNAL_URL_TYPE,
        },
    'ncbi_get_taxonomy_id' => {
            'url' => 'http://www.ncbi.nlm.nih.gov/taxonomy',
            'anchor' => '',
            'type' => $EXTERNAL_URL_TYPE,
        },
    'uniprot_taxonomy' => {
            'url' => 'http://www.uniprot.org/taxonomy/', # followed by a taxonomy ID
            'anchor' => '',
            'type' => $EXTERNAL_URL_TYPE,
        },
    'ipr_to_gene_ontology' => {
            'url' => 'http://www.geneontology.org/external2go/interpro2go',
            'anchor' => '',
            'type' => $EXTERNAL_URL_TYPE,
        },
    'uniprot_to_go' => {
            'url' => 'ftp://ftp.ebi.ac.uk/pub/databases/GO/goa/UNIPROT/gene_association.goa_uniprot.gz',
            'anchor' => '',
            'type' => $EXTERNAL_URL_TYPE,
        },
    'gene_ontology_obo' => {
            'url' => 'http://www.geneontology.org/ontology/obo_format_1_2/gene_ontology_ext.obo',
            'anchor' => '',
            'type' => $EXTERNAL_URL_TYPE,
        },
    'ec_to_go' => {
            'url' => 'http://www.geneontology.org/external2go/ec2go',
            'anchor' => '',
            'type' => $EXTERNAL_URL_TYPE,
        },
    #'fetch_ec' => {
    #        'url' => 'http://www.genome.jp/dbget-bin/www_bget?enzyme+',
    #        'anchor' => '',
    #        'type' => $EXTERNAL_URL_TYPE,
    #    },
    'fetch_lineage' => {
            'url' => 'http://eutils.ncbi.nlm.nih.gov/entrez/eutils/efetch.fcgi?db=taxonomy&retmode=xml',
            'anchor' => '',
            'type' => $EXTERNAL_URL_TYPE,
        },
    'galaxy' => {
            'url' => 'http://gohelle.cirad.fr/galaxy/tool_runner?tool_id=GP_fasta;URL=',
            'anchor' => '',
            'type' => $EXTERNAL_URL_TYPE,
        },
    'intreegreate' => {
            'url' => 'http://gohelle.cirad.fr/phylogeny/treedisplay/index.php',
            'anchor' => '',
            'type' => $EXTERNAL_URL_TYPE,
        },
    'treepattern' => {
            'url' => 'http://gohelle.cirad.fr/phylogeny/treepattern_beta/index.php?databank=GreenPhyl',
            'anchor' => '',
            'type' => $EXTERNAL_URL_TYPE,
        },
    'species_codes_list' => {
            'url' => 'http://www.uniprot.org/docs/speclist.txt',
            'anchor' => '',
            'type' => $EXTERNAL_URL_TYPE,
        },
};




=pod

=head1 AUTHORS

Mathieu ROUARD (Bioversity), m.rouard@cgiar.org

Valentin GUIGNON (Bioversity), v.guignon@cgiar.org

Matthieu CONTE (Syngenta)

Sebastien BRIOIS (Syngenta)

=head1 VERSION

Version 1.5.0

Date 23/10/2013

=head1 SEE ALSO

GreenPhyl documentation.

=cut

return 1; # package return
