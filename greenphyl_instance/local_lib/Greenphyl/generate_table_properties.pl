#!/usr/bin/perl

=pod

=head1 NAME

generate_table_properties.pl - Generates the property hash for DBObjects

=head1 SYNOPSIS

    generate_table_properties.pl family

=head1 REQUIRES

Perl5

=head1 DESCRIPTION

Generates the property hash structure for DBObject derivated classes. The
script output can be copied&pasted into a new DBObject derivated class
property structure and adpated to suite needs.

=cut

use strict;
use warnings;

use lib '..';
use Carp qw (cluck confess croak);

use Getopt::Long;
use Pod::Usage;

use Greenphyl;




# Script options
#################

=pod

=head1 OPTIONS

generate_table_properties.pl [-debug | -help | -man] | <table_name>

=head2 Parameters

=over 4

=item B<-help>:

Prints a brief help message and exits.

=item B<-man>:

Prints the manual page and exits.

=item B<-debug>:

Executes the script in debug mode.

=item B<table_name>:

Name of the database table that should be used to generate property hash.

=back

=cut


# CODE START
#############

# options processing
my ($man, $help, $debug) = (0, 0, 0);
# parse options and print usage if there is a syntax error.
GetOptions("help|?" => \$help,
           "man"    => \$man,
           "debug"  => \$debug,
) or pod2usage(2);
if ($help) {pod2usage(0);}
if ($man) {pod2usage(-verbose => 2);}

my $table_name = shift() or pod2usage(2);;

my $dbh = GetDatabaseHandler() or confess "ERROR: unable to connect to database!\n";

print "our \$OBJECT_PROPERTIES = {\n";
print "    'table' => '$table_name',\n";

my $sth;

$sth = $dbh->primary_key_info(undef, undef, $table_name);
my $primary_key = $sth->fetchall_arrayref({});
print "    'key' => '" . join(",", map { $_->{'COLUMN_NAME'} } @$primary_key) . "',\n";

print "    'alternate_keys' => [],\n";

$sth = $dbh->column_info(undef, undef, $table_name, undef);
my ($columns) = ($sth->fetchall_arrayref({}));
my @column_names = sort(map { $_->{'COLUMN_NAME'} } @$columns);
print "    'load'     => [\n";
foreach (@column_names)
{
    print "        '" . $_ . "',\n";
}
print "    ],\n";
print "    'alias' => {\n";
foreach (@column_names)
{
    my $column_name = $_;
    if ($column_name =~ m/^$table_name(?:_)(.+)$/i)
    {
        my $alias_name = $1;
        print "        '$alias_name' => '" . $_ . "',\n";
    }
}
print "    },\n";
print "    'base' => {\n";


foreach (@column_names)
{
    print "        '" . $_ . "' => {\n";
    print "            'property_column' => '" . $_ . "',\n";
    print "        },\n";
}

print <<"___248_PROPERTIES___";
    },
    'secondary' => {
#        '<member_name>' => {
#            #'object_key'     => '<key_id>', # not defined means use default 'key'
#            'property_table'  => '<>',
#            'property_key'    => '<>',
#            'multiple_values' => 0,
#            'property_column' => '<property_column>',
#        },
#        '<member2_name>' => {
#            #'object_key'     => '<key_id>',
#            'property_table'  => '<>',
#            'property_key'    => '<>',
#            'multiple_values' => 1,
#            'property_module' => 'Greenphyl::<Object>',
#        },
    },
    'tertiary' => {
#        '<member3_name>' => {
#            #'object_key'       => '<key_id>', # not defined means 'key'
#            'link_table'        => '<>',
#            'link_object_key'   => '<>',
#            'link_property_key' => '<>',
#            'property_table'    => '<>',
#            'property_key'      => '<>',
#            'multiple_values'   => 1,
#            'property_module'   => 'Greenphyl::<Object>',
#        },
    },

___248_PROPERTIES___

print "};\n";

exit(0);

# CODE END
###########


=pod

=head1 AUTHORS

Mathieu ROUARD (Bioversity), m.rouard@cgiar.org

Valentin GUIGNON (Bioversity), v.guignon@cgiar.org

Matthieu CONTE (Syngenta)

=head1 VERSION

Version 1.0.0

Date 18/04/2012

=head1 SEE ALSO

GreenPhyl documentation, DBObject documentation.

=cut
