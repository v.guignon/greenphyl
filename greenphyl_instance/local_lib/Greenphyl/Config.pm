###############################################################################
#
# Greenphyl::Config
# This module contains global GreenPhyl parameters fro all scripts and modules.
#
# Greenphyl::Config_template:
# This file is a exemple of template for the configuration of the website.
# Its real name is Config.pm
#
###############################################################################

package Greenphyl::Config;

use strict;
use warnings;

use CGI; # for $BASE_URL url call
use CGI::Session; # to set global session name

use Greenphyl::URLConfig;




# Config Site Name
###################

=pod

=head2 $SITE_NAME

Used to identify session cookie for differents sites on a same server.

=cut

our $SITE_NAME = 'Greenphyl-dev';
CGI::Session->name($SITE_NAME);




# Database Parameters
##################################################

=pod

=head2 $DATABASES

Array of available databases. The first element is always used as default.
The selected (active) database can be changed by using '-db_index' command
line parameter or 'db_index' session parameter before instanciating a
Greenphyl::ConnectMysql object.

Each element of the array is a hash with the following keys/values:
* 'name'     => database display name (used on user interfaces)
* 'host'     => name of the server hosting the database (nb. port can be added)
* 'login'    => user login to log on the database
* 'password' => clear password to log on the database
* 'database' => name of the database

=head2 $DB_PROFILING

Enables/disables database profiling.

=head2 $DB_PROFILING_CUTOFF

Maximum amount of time (in seconds) an SQL query can use before being logged.
Faster queries won't be logged.

=cut

our $DATABASES = [
    {   name     => 'Main DB',
        host     => 'localhost',
        login    => 'greenphyl',
        password => 'toto1234',
        database => 'greenphyl_prod',
    },
    {   name     => 'Testing DB',
        host     => 'localhost',
        login    => 'greenphyl',
        password => 'toto1234',
        database => 'greenphyl_dev',
    },
    {   name     => 'Private DB',
        host     => 'localhost:3306',
        login    => 'greenphyl_priv',
        password => 'tutu5678',
        database => 'greenphyl_priv',
    },
];

our $DB_PROFILING = 0;
our $DB_PROFILING_CUTOFF = '10'; # seconds




# URLs and Web Files
##################################################

=pod

=head2 $BASE_URL

Base website URL without the trailing slash.

=head2 $URLS

URLs configuration. See Greenphyl::URLConfig for details.

=head2 $JAVASCRIPT_LIST

Array of javascript file names (without the .js extension) to load (from
$JAVASCRIPTS_URL). Additional parameters/arguments can be specified using the
question mark. For instance, if you need to load "scriptaculous" with the
parameter "load=effects", enter the string "scriptaculous?load=effects"
(without ".js").

=head2 $CSS_LIST

Array of CSS file names (without the .css extension) to load (from
$CSS_URL).

=head2 $LESS_LIST

Array of LESS file names (without the .less extension) to load (from
$CSS_URL). Note: greenphyl.less automatically imports custom.less so "custom"
should NOT be specified in the $LESS_LIST list.

=head2 $ATVE_APPLET

Name of the ATV (ATVE, Forester, Archeopteryx,...) Java tree viewer applet file
available in $APPLETS_URL.

=head2 $JALVIEW_APPLET

Name of the Jalview Java alignment viewer applet file available in
$APPLETS_URL.

=head2 $TREEFILTER_APPLET

Name of the tree filter applet.

=head2 $GSVIEWER_APPLET

Name of the GSViewer Flash applet file available in $APPLETS_URL.

=head2 $CHART_APPLET

Name of the Chart display Flash applet file available in $APPLETS_URL.

=cut

our $BASE_URL =  $Greenphyl::URLConfig::BASE_URL;

our $URLS = $Greenphyl::URLConfig::URLS;

# Javascripts
our $JAVASCRIPT_LIST           = [qw(
    less
    jquery jquery-ui
    jquery.autocomplete jquery.treeview
    jquery.scrollTo jquery.localscroll
    jquery.tablesorter jquery.tablesorter.pager
    jquery.cookie jquery.editable
    greenphyl
)];

# CSS
our $CSS_LIST           = [qw(
  south-street/jquery-ui
  jquery.autocomplete jquery.treeview
)];

our $LESS_LIST          = [qw(
  greenphyl custom
)];

# Java applets
our $ATVE_APPLET               = 'archaeopteryx_applets.jar';
our $JALVIEW_APPLET            = 'jalviewApplet.jar';
our $TREEFILTER_APPLET         = 'TreeFilter.jar';

# Other applets
our $GSVIEWER_APPLET           = 'GViewer2.swf';
our $CHART_APPLET              = 'open-flash-chart.swf';


# Server Path
##################################################

=pod

=head2 $ROOT_PATH

Base server path (absolute) to the root of the GreenPhyl project without the
trailing slash.

=head2 $BASE_PATH

Base server path (absolute) to the root of the GreenPhyl website without the
trailing slash.

=head2 $CGI_PATH

Absolute server path to CGI scripts without the trailing slash.

=head2 $IMAGES_PATH

Absolute server path to the image directory without the trailing slash.

=head2 $MEDIA_PATH

Absolute server path to the media directory without the trailing slash.

=head2 $VIDEOS_PATH

Absolute server path to the video directory without the trailing slash.

=head2 $APPLETS_PATH

Absolute server path to the applet directory without the trailing slash.

=head2 $TEMPLATES_PATH

Absolute server path to the HTML template directory without the trailing slash.

=head2 $TEMP_OUTPUT_PATH

Absolute server path to temporary directory without the trailing slash.

=head2 $SESSIONS_PATH

Absolute server path to sessions directory without the trailing slash.

=head2 $ADMIN_SCRIPTS_PATH

Absolute server path to the directory where admin/update scripts are stored
without the trailing slash.

=head2 $TEST_PATH

Absolute server path to the directory where regression test files can be found.

=head2 $DATA_PATH

Absolute server path to the directory where static and generated data are
stored without the trailing slash.

=head2 $LOG_PATH

Absolute server path to the directory where log files are stored without the
trailing slash.

=head2 $BLAST_BANKS_PATH

Absolute server path to BLAST bank directory without the trailing slash.

=head2 $REFSEQ_PATH

Absolute server path to the RefSeq directory without the trailing slash.

=head2 $BBMH_PATH

Absolute server path to the BBMH directory without the trailing slash.

=head2 $HMM_PATH

Absolute server path to the directory of HMM files of gene families without the
trailing slash.

=head2 $MULTI_ALIGN_PATH

Absolute server path to the multi-alignments directory without the trailing
slash.

=head2 $TREES_PATH

Absolute server path to the directory of gene trees (in phyloxml format)
without the trailing slash.

=head2 $MEME_OUTPUT_PATH

Absolute server path to the directory containing MEME output files without the
trailing slash.

=head2 $IPR_STATS_PATH

Absolute server path to the IPR statistics directory without the trailing slash.

=head2 $DUMP_PATH

Absolute server path to the directory where database should be dumper without
the trailing slash.

=head2 $IPR_OBSOLETE_PATH

Absolute server path to directory containing obsolete IPR results without the
trailing slash.

=head2 $GO_PATH

Absolute server path to Gene Ontology directory without the trailing slash.

=head2 $GO_OBO_FILE_PATH

Absolute server path to the file 'gene_ontology_ext.obo' containing Gene
Ontology data used to retrieve namespaces (GO types) when updating GO.

=head2 $IPR_TO_GO_FILE_PATH

Absolute server path to the file 'interpro2go' containing the
mapping between InterPro domains and Gene Ontology.

=head2 $UNIPROT_TO_GO_FILE_PATH

Absolute server path to the file 'gene_association.goa_uniprot' containing the
mapping between UniProt data and Gene Ontology.

=head2 $GO_PLANT_SLIM_FILE_PATH

Absolute server path to the file used to generate the GO tree.

=head2 $ALL_BLAST_BANK

Absolute server path to the BLAST bank that contains all species.

=head2 $REFSEQ_NOPLANT_BANK

Absolute server path to the BLAST bank that contains RefSeq protein sequences.

=head2 $RAW_ALIGNMENT_FILE_SUFFIX

File suffix used for the name of a family filtered alignment (raw FASTA).

=head2 $MASKING_FILE_SUFFIX

File suffix used for the name of a family masked alignment (HTML).

=head2 $FILTERED_FILE_SUFFIX

File suffix used for the name of a family filtered alignment (HTML).

=head2 $PHYLO_XML_TREE_FILE_SUFFIX

File suffix used for the name of a family phylogenetic tree (XML).

=head2 $PHYLO_NEWICK_TREE_FILE_SUFFIX

File suffix used for the name of a family phylogenetic tree (nwk).

=head2 $IPR_STATS_PATH

Directory to store the lock file for IRP statisitics execution.

=head2 $SPECIES_CODE_PATH

Path where the local copy of speclist.txt is.
Source URL: http://www.uniprot.org/docs/speclist.txt
GetURL('species_codes_list');

=cut

# general path
our $ROOT_PATH                 = '/apps/www/greenphyl';
our $BASE_PATH                 = $ROOT_PATH . '/website';
our $CGI_PATH                  = $BASE_PATH . '/cgi-bin';
our $IMAGES_PATH               = $BASE_PATH . '/htdocs/img';
our $MEDIA_PATH                = $BASE_PATH . '/htdocs/media';
our $VIDEOS_PATH               = $BASE_PATH . '/htdocs/media/videos';
our $APPLETS_PATH              = $BASE_PATH . '/htdocs/applets';
our $TEMPLATES_PATH            = $BASE_PATH . '/templates';
our $TEMP_OUTPUT_PATH          = $BASE_PATH . '/htdocs/tmp';
our $SESSIONS_PATH             = $BASE_PATH . '/sessions';
our $ADMIN_SCRIPTS_PATH        = $BASE_PATH . '/admin';
our $TEST_PATH                 = $BASE_PATH . '/t';

# data path
our $DATA_PATH                 = '/data/greenphyl';
our $LOG_PATH                  = $DATA_PATH;
our $BLAST_BANKS_PATH          = $DATA_PATH . '/blast';
our $REFSEQ_PATH               = $BLAST_BANKS_PATH . '/refseq';
our $BBMH_PATH                 = $BLAST_BANKS_PATH . '/bbmh';
our $HMM_PATH                  = $DATA_PATH . '/meme/hmm';
our $MULTI_ALIGN_PATH          = $DATA_PATH . '/alm';
our $TREES_PATH                = $DATA_PATH . '/trees';
our $MEME_OUTPUT_PATH          = $DATA_PATH . '/meme';
our $IPR_STATS_PATH            = $DATA_PATH . '/ipr_stat';
our $DUMP_PATH                 = $DATA_PATH . '/dump';
our $IPR_OBSOLETE_PATH         = $DATA_PATH . '/ipr_obs';
our $GO_PATH                   = $DATA_PATH . '/go';

# admin/update path
our $ADMIN_PATH                = $BASE_PATH . '/admin';
our $UTILS_PATH                = $ADMIN_PATH . '/utils';
our $UPDATE_PATH               = $ADMIN_PATH . '/update';

# update files
our $GO_OBO_FILE_PATH          = $DATA_PATH . '/go/gene_ontology_ext.obo';
our $IPR_TO_GO_FILE_PATH       = $DATA_PATH . '/go/interpro2go';
our $UNIPROT_TO_GO_FILE_PATH   = $DATA_PATH . '/go/gene_association.goa_uniprot.gz';
our $GO_PLANT_SLIM_FILE_PATH   = $DATA_PATH . '/go/goslim_plant.obo';
our $ALL_BLAST_BANK            = $BLAST_BANKS_PATH . '/ALL';
our $REFSEQ_NOPLANT_BANK       = $REFSEQ_PATH . '/refseq-noplant';
our $RAW_ALIGNMENT_FILE_SUFFIX     = '.fltr.fasta.aln';
our $MASKING_FILE_SUFFIX           = '_masking.html';
our $FILTERED_FILE_SUFFIX          = '_filtered.html';
our $PHYLO_XML_TREE_FILE_SUFFIX    = '_rap_tree.xml';
our $PHYLO_NEWICK_TREE_FILE_SUFFIX = '_rap_gene_tree.nwk';
our $EC_TO_GO_FILE_PATH            = $DATA_PATH . '/go/ec2go.txt';
our $SPECIES_CODE_PATH             = $DATA_PATH . '/speclist.txt';



# Tools Parameters
##################################################

=pod

=head2 $BLAST_PLUS_PATH

Full path to the BLAST plus binaries (without the trailing slash).

=head2 $BLASTALL_COMMAND

Full path to the BLAST all command.

=head2 $CLUSTER_BLASTALL_COMMAND

Full path to the command used to launch BLAST all on a cluster. This can be a
wrapper script. Command line parameters given to the script are the same as
BLAST all plus the queue parameter "-queue <queue name>"

=head2 $BLASTALL_TABULAR_FORMAT_PARAM

BLAST parameter to use to get tabular output (depending the version of BLAST,
this parameter can be '-m 8' or '-outfmt 6').

=head2 $BLASTALL_ADDITIONAL_PARAM

Additionnal BLAST parameters if needed.

=head2 $HMM_COMMAND

Full path to HMM command.

=head2 $GRAPHIVZ_COMMAND

Full path to dot command.

=head2 $FORMATDB_COMMAND

Full path to BLAST FormatDB command.

=head2 $TRANSEQ_COMMAND

Full path to Transeq command.

=head2 $BACK_TRANSEQ_COMMAND

Full path to Back Transeq command.

=head2 $MYSQL_DUMPER

Full path to MySQL dump command.

=head2 $MEME_COMMAND

Full path to MEME command.

=head2 $MAST_COMMAND

Full path to MAST command.

=head2 $QSUB_COMMAND

Full path to QSUB (SGE) command.

=head2 $QDEL_COMMAND

Full path to QDEL (SGE) command.

=head2 $SENDMAIL_PATH

Full path to sendmail command.

=head2 $JAVA_COMMAND

Full path to java interpreter.

=cut

our $BLAST_PLUS_PATH               = '/usr/local/bioinfo/blast+/bin';
our $BLASTALL_COMMAND              = "/usr/local/ncbi-toolbox/bin/blastall";
our $CLUSTER_BLASTALL_COMMAND      = '~/greenphyl/website/admin/cluster_scripts/sge_blast.pl';
our $BLASTALL_TABULAR_FORMAT_PARAM = '-m 8';
our $BLASTALL_ADDITIONAL_PARAM     = '-a 2';
our $HMM_COMMAND                   = "/usr/local/bin/mhmms";
our $GRAPHIVZ_COMMAND              = "/usr/bin/dot";
our $FORMATDB_COMMAND              = "/usr/local/ncbi-toolbox/bin/formatdb";
our $TRANSEQ_COMMAND               = "/usr/local/bin/transeq";
our $BACK_TRANSEQ_COMMAND          = "/usr/local/bin/backtranseq";
our $MYSQL_DUMPER                  = '/usr/local/mysql/bin/mysqldump'; # Win32: C:/Programme/MySQL/MySQL Server 5.1/bin/mysqldump.exe
our $MEME_COMMAND                  = 'meme';
our $MAST_COMMAND                  = 'mast';
our $QSUB_COMMAND                  = 'qsub';
our $QDEL_COMMAND                  = 'qdel';
our $SENDMAIL_PATH                 = '/usr/sbin/sendmail';
our $JAVA_COMMAND                  = '/opt/java/bin/java';




# Miscelaneous
##################################################

=pod

=head2 $DEBUG_MODE

Set this parameter to enable debug mode.

=head2 $VERBOSE_MODE

Set this parameter to enable verbose mode. The difference with debug mode is
that some actions are canceled in debug mode while it is not the case in verbose
mode.

=head2 $PROXY

Set this parameter only if you work behind a proxy server.

=head2 $GO_COVERAGE_LIMIT

Gene Ontology coverage limit for searches.

=head2 $ODB_USER

User login for ODB Chromosome position tool.

=head2 $ODB_PASSWORD

User password for ODB Chromosome position tool.

=head2 $ODB_HOST

Host name of ODB Chromosome position tool.

=head2 $SGE_ROOT

Path to SGE Root directory. This should be set to environment variable
'SGE_ROOT'.

=head2 $SGE_ID_PATH

Path to the file that contains the list of SGE process currently running.
#+FIXME: should be replaced by an other method using the directory instead of
         a file to prevent race conditions

=head2 $CLUSTER_QUEUE

Name of the cluster queue GreenPhyl should be run on when using the cluster.

=head2 $SEARCH_MAX_RESULTS

Maximum number of rows a search query should return.

=head2 $GOOGLE_ANALYTICS

Set value with GA website key to activate google analytics. 
Check that your website has the proper snippet (tracking code) in global_footer.tt

=head2 $CONTACTS_EMAIL

Email adress that will be used to send email via 'suggest a name' feature. if several, use a comma to separate 
the e-mail addresses.

=head2 $VALIDATION_STATUS

Family validation status that are allowed in database. Keys are the numeric
values of the validation status and values are the corresponding text.

=head2 $ACCESS_KEY_ADMINISTRATOR

Database value for administrator user flag.

=head2 $ACCESS_KEY_ANNOTATOR

Database value for annotator user flag.

=head2 $ACCESS_KEY_REGISTERED_USER

Database value for registered user user flag.

=head2 $REFSEQ_BANKS

RefSeq bank names excluding plant, complete, mitochondrion, plasmid and
plastid.

=cut

our $DEBUG_MODE                = 0;
our $VERBOSE_MODE              = 0;
our $PROXY                     = undef; # 'http://username:password@proxy.com:80'
our $GO_COVERAGE_LIMIT         = 0; 
our $ODB_USER                  = '';
our $ODB_PASSWORD              = '';
our $ODB_HOST                  = 'localhost';
our $SGE_ROOT                  = exists( $ENV{SGE_ROOT} ) ? $ENV{SGE_ROOT} : '';
our $SGE_ID_PATH               = $BASE_PATH . '/sge_id.txt';
our $CLUSTER_QUEUE             = 'greenphyl.q';
our $SEARCH_MAX_RESULTS        = 1000;
our $GOOGLE_ANALYTICS          = '';
our $CONTACTS_EMAIL            = '';  
our $VALIDATION_STATUS         =
    {
        '1' => 'N/A',
        '2' => 'high',
        '3' => 'normal',
        '4' => 'unknown',
        '5' => 'suspicious',
        '6' => 'clustering error',
    };
our $ACCESS_KEY_ADMINISTRATOR   = 'administrator';
our $ACCESS_KEY_ANNOTATOR       = 'annotator';
our $ACCESS_KEY_REGISTERED_USER = 'registered user';
our $REFSEQ_BANKS = [
    'fungi',
    'invertebrate',
    'microbial',
    'protozoa',
    'vertebrate_mammalian',
    'vertebrate_other',
    'viral',
];



# Log files
##################################################
#+FIXME: still to be cleaned...
our $REPORT_LOG_FILE                      = 'update.log';                         # old $report
our $NEW_SEQUENCE_FILE                    = 'list_new_seq.txt';                   # old $new_sequence
our $REASSIGNED_SEQUENCE_FILE             = 'list_reassigned_seq.txt';
our $KEPT_SEQUENCE_FILE                   = 'list_kept_seq.txt';
our $MODIFIED_SEQUENCE_FILE               = 'list_modif_seq.txt';                 # old $modif_sequence
our $OBSOLETE_SEQUENCE_FILE               = 'list_obsolete_seq.txt';              # old $obsolete_sequence
our $FAMILY_LIST_FILE                     = 'list_family_with_new.txt';           # old $liste_family
our $FAMILIES_WITH_OBSOLETE_SEQUENCE_FILE = 'list_family_with_obsolete.txt';      # old $rem_family
our $OBSOLETE_FAMILY_LIST_FILE            = 'list_obsolete_family.txt';           # old $obs_family
our $ANNOTATED_OBSOLETE_FAMILY_LIST_FILE  = 'list_annotated_obsolete_family.txt';
our $NEW_FAMILY_LIST_FILE                 = 'list_new_family.txt';                # old $new_family
our $MODIFIED_FAMILY_LIST_FILE            = 'list_modified_family.txt';
our $PERCENTAGE_FILE                      = 'percentage.txt';                     # old $pourcentage
our $MEME_MAST_LOG_FILE                   = 'run_meme_mast.log';
our $DATA_REPORT                          = 'data_report.log';
our $INTERPROSCAN_UPDATE_FILE             = 'ipr_update.txt';
our $PROCESSUS_LOG                        = 'LOG.txt';                                                     # old $log
our $XML_PARSING_FILE                     = 'result_parse.txt';                   # file containing the results of XML parsing # old $result_parse
our $INPUT_FASTA_FILENAME                 = 'input.fa';
our $FASTA_TO_PROCESS                     = 'fasta_modif_new.fa';                 # sequences to process using interproscan and to classify # old $modif_new
our $INTERPROSCAN_ERROR_FILE              = 'fasta_interpro_error.fa';            # interpro errors # old $interpro
our $ORPHANS_FASTA_FILENAME               = 'orphans.fa';                         # FASTA of orphan sequences
our $IPR_OBSOLETE_FILE                    = $DATA_PATH . '/ipr_obsolete.txt';      # old $ipr_obs
our $IPR_CONSERVED_FILE                   = $DATA_PATH . '/ipr_conserve.txt';      # old $ipr_cons
our $IPR_TO_GO_FILENAME                   = 'interpro2go';
1;
