#!/usr/bin/perl
#------------------------------------------------------------------------------
#--- Perl CGI script Template                                               ---
#------------------------------------------------------------------------------
#--- Description:
#--- This is the template file for Perl scripts of the ID team of CIRAD (BIOS,
#--- DAP) that should be used as starting point for new CGI scripts.
#--- This template was inspired from the work of Steven McDougall:
#---  http://world.std.com/~swmcd/steven/perl/module_pod.html
#--- and the work done for Phylogeny.fr.
#---
#--- Lignes containing "#+++" have to be modified or removed.
#--- Lignes containing "#---" have to be removed.
#--- Any content inside square brackets has to be modified, the rest can be
#--- left as is. Then, square brackets has to be removed.
#--- Once modifications have been done, "#+++" comments should be removed.
#---
#--- Authors:
#---  Valentin Guignon
#---
#--- Template version:
#---  0.1.2
#---
#--- Date:
#---  03/03/2009
#------------------------------------------------------------------------------

=pod

=head1 NAME

[CGI Script name - short description] #+++
#--- example: Welcome.cgi - Welcome Page

=head1 SYNOPSIS

    #+++ example URL with optional GET parameters

=head1 REQUIRES

#--- Module requierments
#--- example:
[Perl5.004, POSIX, Win32] #+++

=head1 DESCRIPTION

#--- What this script can do, how to use it, ...
[Script description]. #+++

=cut

use strict;
use Carp qw (cluck confess croak);
use warnings;
use Readonly;
#--- when using try {...} catch XXX::YYY with {...} otherwise {...};
#+++ use Error qw(:try);
#--- when working with files
#+++ use Fatal qw/:void open close/;
use CGI;
#--- when handling user sessions
#+++ use CGI::Session;


# Script global constants
##########################

=pod

=head1 CONSTANTS

B<$CONSTANT_NAME>: ([constant nature]) #+++

[constant description and use]. #+++

B<CONSTANT_NAME2>: (sub returning a [constant return nature]) #+++

[constant description and use]. #+++

#--- Example:
#--- B<PI>: (sub returning a real)
#---
#--- used by trigonometrical functions;
#--- can also be used to define an angle value.
#---
#--- ...
#---
#--- sub PI() { return (4 * atan2(1, 1)); }
#---

=cut

#+++ my [$CONSTANT_NAME] = ["value"];
#+++ sub [CONSTANT_NAME2]() { return ["value2"]; }


# Script global variables
##########################

=pod

=head1 VARIABLES

B<variable_name>: ([variable nature]) #+++

[variable description, use and default value]. #+++

#--- Example:
#--- B<$output_method>: (integer)
#---
#--- used to store current output method;
#--- 0=raw text (default), 1=image, 2=html.
#---
#---     ...
#---
#--- my $output_method = 0;
#---

=cut

#+++ my [$variable_name] = ["value"];


# Script global functions
##########################

=pod

=head1 FUNCTIONS

=head2 [subName] #+++

B<Description>: [function description]. #+++

B<ArgsCount>: [count of arguments] #+++

=over 4

=item variable_name: ([variable nature]) ([requirements]) #+++ see below

#--- requirement can be:
#--- (R)=required,
#--- (O)=optional
#--- (U)=optional and must be undef if omitted
[variable description]. #+++

=item variable_name2: ([variable nature]) ([requirements]) #+++

[variable description]. #+++

=back

B<Return>: ([return type]) #+++

[return description]. #+++

B<Exception>:

=over 4

[=item * exception_type:

description, case when it occurs.] #+++ see below

=back

#--- Example:
#---
#---=over 4
#---
#---=item * Range error:
#---
#---thrown when "argument x" is outside the supported range, for instance "-1".
#---
#---=back

B<Example>:

    [example code] #+++

=cut

#+++sub [subName] #+++
#+++{
#+++    my ([...]) = @_; #+++ add missing arguments
#--- if needed, parameters check:
#+++    # parameters check
#+++    if (0 != @_)
#+++    {
#+++        confess "usage: subName();"; #+++
#+++    }
#+++}


# Script options
#################

=pod

=head1 OPTIONS

#--- describes GET/POST parameters given to the CGI script

=head2 Parameters

=over 4

=item B<[option_name]> ([option nature]): #+++

[option description]. #+++
Default: [option default value if one] #+++

=back
#--- Example:
#---
#--- No parameters requiered or parameters given using POST method.
#---
#--- =over 4
#---
#--- =item B<login> (string):
#---
#--- The user login.
#---
#--- =item B<tracker> (integer):
#---
#--- A tracker used to retrive the data element to display.
#---
#---=back

=cut


# CODE START
#############

my $cgi = new CGI;

print $cgi->header();

# create a hash with default HTML header data
my %header_hash = (
#+++                      -title  => "[Site title]",
#+++                      -head   => "<link type=\"image/x-icon\" rel=\"shortcut icon\" href=\"[http://server/site/images/site_icon.ico]\" />",
#+++                      -style  => {'src' => "[http://server/site/default.all.css]", 'type' => 'text/css'},
#+++                      -script => [{-language => 'javascript', -src => "[http://server/js/functions.js]"},
#+++                                  {-language => 'javascript', -src => "[http://server/js/functions2.js]"}],
#+++                      -onLoad => 'DoLoad();' # javascript function called when "onLoad" event occurs
                  );
print $cgi->start_html(%header_hash);

print $cgi->end_html();

#... #+++

# CODE END
###########


=pod

=head1 DIAGNOSTICS

#--- Give and explain here every error message the the script may generate.
#--- Use: (W)=warning (optional), (D)=deprecation (optional),
#---      (S)=severe warning (mandatory), (F)=fatal error (trappable),
#---      (X)=very fatal error (non-trappable).
#--- example:
#---
#--- =over 4
#---
#--- =item *
#---
#--- Negative radius;
#--- (F) Can not draw a circle with a negative radius.
#---
#---=back #+++

=head1 AUTHORS

#+++ Valentin GUIGNON (CIRAD), valentin.guignon@cirad.fr

[author1_name (company), email]#+++

[author2_name (company), email]#+++

=head1 VERSION

Version [version.subversion.build] #+++

Date [DD/MM/YYY] #+++

=head1 SEE ALSO

#--- other documentation or objects related to this package
[perl(1), L<Geometry::Square>] #+++

=cut
