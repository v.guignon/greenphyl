#------------------------------------------------------------------------------
#--- Perl Modules Template                                                  ---
#------------------------------------------------------------------------------
#--- Description:
#--- This is the template file for Perl Modules of the ID team of CIRAD (BIOS,
#--- DAP) that should be used as starting point for new modules.
#--- This template was inspired from the work of Steven McDougall:
#---  http://world.std.com/~swmcd/steven/perl/module_pod.html
#--- and the work done for Phylogeny.fr.
#---
#--- Lignes containing "#+++" have to be modified or removed.
#--- Lignes containing "#---" have to be removed.
#--- Any content inside square brackets has to be modified, the rest can be
#--- left as is. Then, square brackets has to be removed.
#--- Once modifications have been done, "#+++" comments should be removed.
#---
#--- Authors:
#---  Valentin Guignon
#---
#--- Contributor:
#---  Steven McDougall
#---
#--- Template version:
#---  0.1.2
#---
#--- Date:
#---  03/03/2009
#------------------------------------------------------------------------------

=pod

=head1 NAME

[PackageName - short description] #+++

#--- example: Geometry::Circle - manages a circle

=head1 SYNOPSIS

#--- Code of essential steps in using the module

#--- example:

[my $instance = Group::PackageName->new(); #+++

$instance->subName(); #+++

...] #+++

=head1 REQUIRES

#--- Module requierments

#--- example:

[Perl5.004, Exporter, Geometry::Point] #+++

=head1 EXPORTS

#--- Tells what this module will do to other namespaces when imported.

#--- example:

[Nothing] #+++

=head1 DESCRIPTION

#--- What this module can do, how to use it, supported objects, ...

[Package description.] #+++

=cut

package [Group::PackageName]; #+++


use strict;
use Carp qw (cluck confess croak);
use warnings;
use Readonly;
#--- in case of inheritance:
#+++ use base qw([BaseClass]);
#--- when using try {...} catch XXX::YYY with {...} otherwise {...};
#+++ use Error qw(:try);
#--- when working with files
#+++ use Fatal qw/:void open close/;



# Package constants
####################

=pod

=head1 CONSTANTS

B<$CONSTANT_NAME>:  ([constant nature]) ([constant access]) #+++

#--- Constant access can be: private (my), protected or public (our).
[constant description and use] #+++

B<CONSTANT_NAME2>: (sub returning a [constant return nature]) ([constant access]) #+++

[constant description and use] #+++

#--- Example:
#--- B<PI>: sub returning a real, (public)
#--- Used by trigonometrical functions;
#--- can also be used to define an angle value.
#--- ...
#--- sub PI() { return (4 * atan2(1, 1)); }
#---
=cut

#+++ my/our [$CONSTANT_NAME] = ["value"];
#+++ sub [CONSTANT_NAME2]() { return ["value2"]; }


# Package variables
####################

=pod

=head1 VARIABLES

B<[variable_name]>: ([variable nature]) ([variable access]) #+++

#--- Variable access can be: private (my), protected or public (our, not recommanded).
[variable description] #+++
Default: [variable default value if one] #+++

=cut

#+++ my/our [$variable_name] = ["value"];


# Package subs
###############

=pod

=head1 STATIC METHODS

=head2 CONSTRUCTOR

B<Description>: [Supports non-static calls.] #+++

[Creates a new instance.] #+++

B<ArgsCount>: [count] #+++

=over 4

=item variable_name: ([variable nature]) ([requirements]) #+++ see below

#--- requirement can be:
#--- (R)=required,
#--- (O)=optional
#--- (U)=optional and must be undef if omitted
[variable description] #+++

=item variable_name2: ([variable nature]) ([requirements]) #+++

[variable description] #+++

=back

B<Return>: ([Group::PackageName]) #+++

a new instance.

B<Caller>: [general] #+++ general, specific class name, ...

B<Exception>:

=over 4

[=item exception_type

description, case when it occurs] #+++ see below

=back

#--- Example:

=over 4

=item Range error

thrown when "argument x" is outside the supported range, for instance "-1".

=back

B<Example>:

    [example code] #+++

=cut

sub new
{
    my ($proto, [...]) = @_; #+++ add missing arguments
    my $class = ref($proto) || $proto;
#--- if needed, parameters check:
#    # parameters check
#    if (1 != @_)
#    {
#        confess "usage: my \$instance = Group::PackageName->new();"; #+++
#    }
#--- if needed, copy constructor test:
#    # copy constructor
#    if (ref $proto)
#    {
#        ...
#    }
    # instance creation
    my $self = {};
#--- in case of inheritance, use this instead of the above line:
#    my $self = $class->SUPER::new();
    bless($self, $class);

#---    $self->{FIELD} = "value";

    return $self;
}

#+++ Destructor if needed
#=pod
#
#=head2 DESTRUCTOR
#
#B<Description>: destroys this instance.
#
#B<Caller>: perl system
#
#=cut
#
#sub DESTROY
#{
#+++    my ($self) = @_;
#}


=pod

=head1 ACCESSORS

=cut

#--- put here setters and getters (copy-paste text below)
#
#=pod
#
#=head2 get[MemberName] #+++
#
#B<Description>: #+++
#
#B<ArgsCount>: #+++
#
#=over 4
#
#=item arg: #+++
#
#=back
#
#B<Return>: #+++
#
#B<Caller>: #+++
#
#B<Exception>: #+++
#
#B<Example>: #+++
#
#=cut
#
#sub get[MemberName] #+++
#{
#    my ($self) = @_;
#    # check parameters
#    if ((1 != @_) || (not ref($self)))
#    {
#        confess "usage: my \$value = \$instance->get[MemberName]();"; #+++
#    }
#    return $self->{[MEMBER_NAME]}; #+++
#}

#=pod
#
#=head2 setMemberName #+++
#
#B<Description>:  #+++
#
#B<ArgsCount>:  #+++
#
#=over 4
#
#=item arg: #+++
#
#=back
#
#B<Return>:  #+++
#
#B<Caller>:  #+++
#
#B<Exception>:  #+++
#
#B<Example>:  #+++
#
#=cut
#
#sub set[MemberName] #+++
#{
#    my ($self, $value) = @_;
#    # check parameters
#    if ((2 != @_) || (not ref($self)))
#    {
#        confess "usage: my \$value = \$instance->set[MemberName](\$value);"; #+++
#    }
#    $self->{[MEMBER_NAME]} = [$value]; #+++
#}


=pod

=head1 METHODS

=cut

#=pod

#=head2 [subName] #+++
#
#B<Description>: #+++
#
#B<ArgsCount>: #+++
#
#=over 4
#
#=item arg: #+++
#
#=back
#
#B<Return>: #+++
#
#B<Caller>: #+++
#
#B<Exception>: #+++
#
#B<Example>: #+++
#
#=cut
#
#sub [subName] #+++
#{
#    my ($self) = @_;
#    # check parameters
#    if ((1 != @_) || (not ref($self)))
#    {
#        confess "usage: my \$value = \$instance->[subName]();"; #+++
#    }
#}


#--- AUTOLOAD if needed.

#--- AUTOLOAD requires "use vars qw($AUTOLOAD);".

#=pod

#=head1 AUTOLOAD
#
#=cut
#
#=head2 AUTOLOAD
#
#B<Description>: #+++
#
#B<ArgsCount>: #+++
#
#=over 4
#
#=item arg: #+++
#
#=back
#
#B<Return>: #+++
#
#B<Caller>: #+++
#
#B<Exception>: #+++
#
#B<Example>: #+++
#
#=cut
#
#sub AUTOLOAD
#{
#    my ($self) = @_;
#    my $[function_name] = $AUTOLOAD; #+++
#    $[function_name] =~ s/.*://; # strip fully-qualified portion #+++
#    [...] #+++
#}

=pod

=head1 DIAGNOSTICS

#--- Give and explain here every error message the the module may generate.
#--- Use: (W)=warning (optional), (D)=deprecation (optional),
#---      (S)=severe warning (mandatory), (F)=fatal error (trappable),
#---      (X)=very fatal error (non-trappable).
#--- example:
#--- =over 4
#--- =item Negative radius
#---
#--- (F) A circle may not be created with a negative radius.
#---
#--- =back #+++

=head1 AUTHORS

#+++ Valentin GUIGNON (CIRAD), valentin.guignon@cirad.fr

[author1_name (company), email]#+++

[author2_name (company), email]#+++

=head1 VERSION

Version [version.subversion.build] #+++

Date [DD/MM/YYY] #+++

=head1 SEE ALSO

#--- other documentation or objects related to this package

[perl(1), L<Geometry::Square>] #+++

=cut

return 1; # package return
