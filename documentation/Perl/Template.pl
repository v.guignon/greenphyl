#!/usr/bin/perl
#------------------------------------------------------------------------------
#--- Perl script Template                                                      ---
#------------------------------------------------------------------------------
#--- Description:
#--- This is the template file for Perl scripts of the ID team of CIRAD (BIOS,
#--- DAP) that should be used as starting point for new scripts.
#--- This template was inspired from the work of Steven McDougall:
#---  http://world.std.com/~swmcd/steven/perl/module_pod.html
#--- and the work done for Phylogeny.fr.
#---
#--- Lignes containing "#+++" have to be modified or removed.
#--- Lignes containing "#---" have to be removed.
#--- Any content inside square brackets has to be modified, the rest can be
#--- left as is. Then, square brackets has to be removed.
#--- Once modifications have been done, "#+++" comments should be removed.
#---
#--- Authors:
#---  Valentin Guignon
#---
#--- Template version:
#---  0.1.3
#---
#--- Date:
#---  11/08/2011
#------------------------------------------------------------------------------

=pod

=head1 NAME

[Script name - short description] #+++
#--- example: Welcome.pl - Welcome Message Script

=head1 SYNOPSIS

    #+++ Code of how to call this script

=head1 REQUIRES

#--- Module requierments
#--- example:
[Perl5.004, POSIX, Win32] #+++

=head1 DESCRIPTION

#--- What this script can do, how to use it, ...
[Script description]. #+++

=cut

use strict;
use Carp qw (cluck confess croak);
use warnings;
use Readonly;
#--- in case of a command-line script
#+++ use Getopt::Long;
#--- in case of a command-line script
#+++ use Pod::Usage;
#--- when using try {...} catch XXX::YYY with {...} otherwise {...};
#+++ use Error qw(:try);
#--- when working with files
#+++ use Fatal qw/:void open close/;
#--- when using multi-thread processes
#+++ use threads;
#+++ use threads::shared;


# Script global constants
##########################

=pod

=head1 CONSTANTS

B<$CONSTANT_NAME>: ([constant nature]) #+++

[constant description and use]. #+++

B<CONSTANT_NAME2>: (sub returning a [constant return nature]) #+++

[constant description and use]. #+++

#--- Example:
#--- B<PI>: (sub returning a real)
#---
#--- used by trigonometrical functions;
#--- can also be used to define an angle value.
#---
#--- ...
#---
#--- sub PI() { return (4 * atan2(1, 1)); }
#---

=cut

#+++ Readonly my $DEBUG => 0;
#+++ my [$CONSTANT_NAME] = ["value"];
#+++ sub [CONSTANT_NAME2]() { return ["value2"]; }


# Script global variables
##########################

=pod

=head1 VARIABLES

B<g_variable_name>: ([variable nature]) #+++

[variable description, use and default value]. #+++

#--- Example:
#--- B<$g_output_method>: (integer)
#---
#--- used to store current output method;
#--- 0=text on screen (default), 1=graphic on screen, 2=text in a file, 3=graphic to the printer.
#---
#---     ...
#---
#--- my $g_output_method = 0;
#---

=cut

#+++ my [$g_variable_name] = ["value"];


# Script global functions
##########################

=pod

=head1 FUNCTIONS

=head2 [subName] #+++

B<Description>: [function description]. #+++

B<ArgsCount>: [count of arguments] #+++

=over 4

=item variable_name: ([variable nature]) ([requirements]) #+++ see below

#--- requirement can be:
#--- (R)=required,
#--- (O)=optional
#--- (U)=optional and must be undef if omitted
[variable description]. #+++

=item variable_name2: ([variable nature]) ([requirements]) #+++

[variable description]. #+++

=back

B<Return>: ([return type]) #+++

[return description]. #+++

B<Exception>:

=over 4

[=item * exception_type:

description, case when it occurs.] #+++ see below

=back

#--- Example:
#---
#---=over 4
#---
#---=item * Range error:
#---
#---thrown when "argument x" is outside the supported range, for instance "-1".
#---
#---=back

B<Example>:

    [example code] #+++

=cut

#+++sub [subName] #+++
#+++{
#+++    my ([...]) = @_; #+++ add missing arguments
#--- if needed, parameters check:
#+++    # parameters check
#+++    if (0 != @_)
#+++    {
#+++        confess "usage: subName();"; #+++
#+++    }
#+++}


# Script options
#################

=pod

=head1 OPTIONS

#--- describes parameters given to the script
#+++ command line syntax
#--- requirement of the option and its parameter can be:
#--- required: name or nature inside <>
#--- optional: name or nature inside []
#--- alternative between 2 elements: elements separated by a |

=head2 Parameters

=over 4

=item B<[option_name]> ([option nature]): #+++

[option description]. #+++
Default: [option default value if one] #+++

=back
#--- Example:
#---
#--- Template.pl [-help | -man]
#---
#--- Template.pl [-debug [debug_level]] [-size <width> [height]]
#---
#--- =over 4
#---
#--- =item B<-help>:
#---
#--- Prints a brief help message and exits.
#---
#--- =item B<-man>:
#---
#--- Prints the manual page and exits.
#---
#--- =item B<-debug> (integer):
#---
#--- Executes the script in debug mode. If an integer value is specified, it will
#--- be the debug level. If "-debug" option was used without specifying a debug
#--- level, level 1 is assumed.
#--- Default: 0 (not in debug mode).
#---
#---=item B<-size> (positive_real) (positive_real):
#---
#--- Set the dimensions of the object that will be drawn. The first value is
#--- the width; the height is the second value if specified, otherwise it will
#--- assume height and width are equal to the first value.
#--- Default: width and height are set to 1.
#---
#---=back

=cut


# CODE START
#############

#--- uncomment the following lines in case of a command-line-like script
#+++# options processing
#+++my ($man, $help, $debug) = (0, 0, 0);
#+++# parse options and print usage if there is a syntax error.
#--- see doc at http://perldoc.perl.org/Getopt/Long.html
#+++GetOptions("help|?"     => \$help,
#+++           "man"        => \$man,
#+++           "debug:i"    => \$debug,
#+++           "length|l=i" => \$length, # numeric specified either with "-length 42" or "-l 42"
#+++           "file=s"     => \$data, # a string
#+++           "files=s{,}" => \@files, # a set of strings specified either with "-files toto tutu" or "-files toto -files tutu"
#+++           "flag!"      => \$flag, # a flag wich can set or unset with "--flag" (set) or "--noflag" (unset) or "--no-flag" (unset)
#+++) or pod2usage(2);
#+++if ($help) {pod2usage(0);}
#+++if ($man) {pod2usage(-verbose => 2);}
#--- end of lines for a command-line-like script

#--- opening a file when not using 'Fatal' lib:
#+++ my $file_handle;
#+++ if (open($file_handle, '>file.txt'))
#+++ {
#+++     ...
#+++     print {$file_handle} 'some text';
#+++     close($file_handle);
#+++ }
#+++ else
#+++ {
#+++     confess "ERROR: failed to open file:\n$!\n";
#+++ }
#--- if not using 'Fatal':
#+++ open($file_handle, '>file.txt');
#+++ ...
#+++ print {$file_handle} 'some text';
#+++ close($file_handle);

#--- try-catch statement with lib 'Error':
#+++ try
#+++ {
#+++     ...
#+++ }
#+++ otherwise
#+++ {
#+++     print "ERROR: " . shift() . "\n";
#+++ };

#... #+++

exit(0);

# CODE END
###########


=pod

=head1 DIAGNOSTICS

#--- Give and explain here every error message the the script may generate.
#--- Use: (W)=warning (optional), (D)=deprecation (optional),
#---      (S)=severe warning (mandatory), (F)=fatal error (trappable),
#---      (X)=very fatal error (non-trappable).
#--- example:
#---
#--- =over 4
#---
#--- =item *
#---
#--- Negative radius;
#--- (F) Can not draw a circle with a negative radius.
#---
#---=back #+++

=head1 AUTHORS

#+++ Valentin GUIGNON (CIRAD), valentin.guignon@cirad.fr

[author1_name (company), email]#+++

[author2_name (company), email]#+++

=head1 VERSION

Version [version.subversion.build] #+++

Date [DD/MM/YYY] #+++

=head1 SEE ALSO

#--- other documentation or objects related to this package
[perl(1), L<Geometry::Square>] #+++

=cut
