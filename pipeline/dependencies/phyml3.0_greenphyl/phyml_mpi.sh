#!/bin/bash

# debug:
set -x

#$ -q normal.q
#$ -cwd
#$ -j y
#$ -N GreenphylPhyML
#$ -pe parallel_fill 10

NSLOTS=10

IFS= 

/opt/cluster/openmpi-1.3.4/gcc-4.1.2/bin/mpirun -np 5 --prefix /opt/cluster/openmpi-1.3.4/gcc-4.1.2 $*