#ifndef CORELIB___NCBIAPP__HPP
#define CORELIB___NCBIAPP__HPP

/*  $Id: ncbiapp.hpp 121710 2008-03-11 14:54:57Z grichenk $
 * ===========================================================================
 *
 *                            PUBLIC DOMAIN NOTICE
 *               National Center for Biotechnology Information
 *
 *  This software/database is a "United States Government Work" under the
 *  terms of the United States Copyright Act.  It was written as part of
 *  the author's official duties as a United States Government employee and
 *  thus cannot be copyrighted.  This software/database is freely available
 *  to the public for use. The National Library of Medicine and the U.S.
 *  Government have not placed any restriction on its use or reproduction.
 *
 *  Although all reasonable efforts have been taken to ensure the accuracy
 *  and reliability of the software and data, the NLM and the U.S.
 *  Government do not and cannot warrant the performance or results that
 *  may be obtained by using this software or data. The NLM and the U.S.
 *  Government disclaim all warranties, express or implied, including
 *  warranties of performance, merchantability or fitness for any particular
 *  purpose.
 *
 *  Please cite the author in any work or product based on this material.
 *
 * ===========================================================================
 *
 * Authors:  Denis Vakatov, Vsevolod Sandomirskiy
 *
 *
 */

/// @file ncbiapp.hpp
/// Defines the CNcbiApplication and CAppException classes for creating
/// NCBI applications.
///
/// The CNcbiApplication class defines the application framework and the high
/// high level behavior of an application, and the CAppException class is used
/// for the exceptions generated by CNcbiApplication.


#include <corelib/ncbistd.hpp>
#include <corelib/ncbienv.hpp>
#include <corelib/ncbiargs.hpp>
#include <corelib/metareg.hpp>
#include <corelib/version.hpp>
#include <memory>

 50 
 51 /// Avoid preprocessor name clash with the NCBI C Toolkit.
 52 #if !defined(NCBI_OS_UNIX)  ||  defined(HAVE_NCBI_C)
 53 #  if defined(GetArgs)
 54 #    undef GetArgs
 55 #  endif
 56 #  define GetArgs GetArgs
 57 #endif
 58 
 59 
 60 /** @addtogroup AppFramework
 61  *
 62  * @{
 63  */
 64 
 65 
 66 BEGIN_NCBI_SCOPE
 67 
 68 
 69 /////////////////////////////////////////////////////////////////////////////
 70 ///
 71 /// CAppException --
 72 ///
 73 /// Define exceptions generated by CNcbiApplication.
 74 ///
 75 /// CAppException inherits its basic functionality from CCoreException
 76 /// and defines additional error codes for applications.
 77 
 78 class NCBI_XNCBI_EXPORT CAppException : public CCoreException
 79 {
 80 public:
 81     /// Error types that an application can generate.
 82     ///
 83     /// These error conditions are checked for and caught inside AppMain().
 84     enum EErrCode {
 85         eUnsetArgs,     ///< Command-line argument description not found
 86         eSetupDiag,     ///< Application diagnostic stream setup failed
 87         eLoadConfig,    ///< Registry data failed to load from config file
 88         eSecond,        ///< Second instance of CNcbiApplication is prohibited
 89         eNoRegistry     ///< Registry file cannot be opened
 90     };
 91 
 92     /// Translate from the error code value to its string representation.
 93     virtual const char* GetErrCodeString(void) const;
 94 
 95     // Standard exception boilerplate code.
 96     NCBI_EXCEPTION_DEFAULT(CAppException, CCoreException);
 97 };
 98 
 99 
100 
101 ///////////////////////////////////////////////////////
102 // CNcbiApplication
103 //
104 
105 
106 /////////////////////////////////////////////////////////////////////////////
107 ///
108 /// CNcbiApplication --
109 ///
110 /// Basic (abstract) NCBI application class.
111 ///
112 /// Defines the high level behavior of an NCBI application.
113 /// A new application is written by deriving a class from the CNcbiApplication
114 /// and writing an implementation of the Run() and maybe some other (like
115 /// Init(), Exit(), etc.) methods.
116 
117 class NCBI_XNCBI_EXPORT CNcbiApplication
118 {
119 public:
120     /// Singleton method.
121     ///
122     /// Track the instance of CNcbiApplication, and throw an exception
123     /// if an attempt is made to create another instance of the application.
124     /// @return
125     ///   Current application instance.
126     static CNcbiApplication* Instance(void);
127 
128     /// Constructor.
129     ///
130     /// Register the application instance, and reset important
131     /// application-specific settings to empty values that will
132     /// be set later.
133     CNcbiApplication(void);
134 
135     /// Destructor.
136     ///
137     /// Clean up the application settings, flush the diagnostic stream.
138     virtual ~CNcbiApplication(void);
139 
140     /// Main function (entry point) for the NCBI application.
141     ///
142     /// You can specify where to write the diagnostics to (EAppDiagStream),
143     /// and where to get the configuration file (LoadConfig()) to load
144     /// to the application registry (accessible via GetConfig()).
145     ///
146     /// Throw exception if:
147     ///  - not-only instance
148     ///  - cannot load explicitly specified config.file
149     ///  - SetupDiag() throws an exception
150     ///
151     /// If application name is not specified a default of "ncbi" is used.
152     /// Certain flags such as -logfile, -conffile and -version are special so
153     /// AppMain() processes them separately.
154     /// @return
155     ///   Exit code from Run(). Can also return non-zero value if application
156     ///   threw an exception.
157     /// @sa
158     ///   LoadConfig(), Init(), Run(), Exit()
159     int AppMain
160     (int                argc,     ///< argc in a regular main(argc, argv, envp)
161      const char* const* argv,     ///< argv in a regular ma exception if "conf" is non-empty, and cannot open file.
395     /// Throw an exception if file exists, but contains invalid entries.
396     /// @param reg
397     ///   The loaded registry is returned via the reg parameter.
398     /// @param conf
399     ///   The configuration file to loaded the registry entries from.
400     /// @param reg_flags
401     ///   Flags for loading the registry
402     /// @return
403     ///   TRUE only if the file was non-NULL, found and successfully read.
404     /// @sa
405     ///   CMetaRegistry::GetDefaultSearchPath
406     virtual bool LoadConfig(CNcbiRegistry& reg, const string* conf,
407                             CNcbiRegistry::TFlags reg_flags);
408 
409     /// Load settings from the configuration file to the registry.
410     ///
411     /// CNcbiApplication::LoadConfig(reg, conf) just calls
412     /// LoadConfig(reg, conf, IRegistry::fWithNcbirc).
413     virtual bool LoadConfig(CNcbiRegistry& reg, const string* conf);
414 
415     /// Set program's display name.
416     ///
417     /// Set up application name suitable for display or as a basename for
418     /// other files. It can also be set by the user when calling AppMain().
419     void SetProgramDisplayName(const string& app_name);
420 
421     /// Find the application's executable file.
422     ///
423     /// Find the path and name of the executable file that this application is
424     /// running from. Will be accessible by GetArguments().GetProgramName().
425     /// @param argc
426     ///   Standard argument count "argc".
427     /// @param argv
428     ///   Standard argument vector "argv".
429     /// @param real_path
430     ///   If non-NULL, will get the fully resolved path to the executable.
431     /// @return
432     ///   Name of application's executable file (may involve symlinks).
433     string FindProgramExecutablePath(int argc, const char* const* argv,
434                                      string* real_path = 0);
435 
436     /// Method to be called before application start.
437     /// Can be used to set DiagContext properties to be printed
438     /// in the application start message (e.g. host|host_ip_addr,
439     /// client_ip and session_id for CGI applications).
440     virtual void AppStart(void);
441 
442     /// Method to be called before application exit.
443     /// Can be used to set DiagContext properties to be printed
444     /// in the application stop message (exit_status, exit_signal,
445     /// exit_code).
446     virtual void AppStop(int exit_code);
447 
448     /// When to return a user-set exit code
449     enum EExitMode {
450         eNoExits,          ///< never (stick to existing logic)
451         eExceptionalExits, ///< when an (uncaught) exception occurs
452         eAllExits          ///< always (ignoring Run's return value)
453     };
454     /// Force the program to return a specific exit code later, either
455     /// when it exits due to an exception or unconditionally.  In the
456     /// latter case, Run's return value will be ignored.
457     void SetExitCode(int exit_code, EExitMode when = eExceptionalExits);
458 
459 private:
460     /// Read standard NCBI application configuration settings.
461     ///
462     /// [NCBI]:   HeapSizeLimit, CpuTimeLimit
463     /// [DEBUG]:  ABORT_ON_THROW, DIAG_POST_LEVEL, MessageFile
464     /// @param reg
465     ///   Registry to read from. If NULL, use the current registry setting.
466     void x_HonorStandardSettings(IRegistry* reg = 0);
467 
468     /// Setup C++ standard I/O streams' behaviour.
469     ///
470     /// Called from AppMain() to do compiler-specific optimization
471     /// for C++ I/O streams. For example, since SUN WorkShop STL stream
472     /// library has significant performance loss when sync_with_stdio is
473     /// TRUE (default), so we turn it off. Another, for GCC version greater
474     /// than 3.00 we forcibly set cin stream buffer size to 4096 bytes -- which
475     /// boosts the performance dramatically.
476     void x_SetupStdio(void);
477 
478     static CNcbiApplication*   m_Instance;   ///< Current app. instance
479     auto_ptr<CVersionInfo>     m_Version;    ///< Program version
480     auto_ptr<CNcbiEnvironment> m_Environ;    ///< Cached application env.
481     CRef<CNcbiRegistry>        m_Config;     ///< Guaranteed to be non-NULL
482     auto_ptr<CNcbiOstream>     m_DiagStream; ///< Opt., aux., see eDS_ToMemory
483     auto_ptr<CNcbiArguments>   m_Arguments;  ///< Command-line arguments
484     auto_ptr<CArgDescriptions> m_ArgDesc;    ///< Cmd.-line arg descriptions
485     auto_ptr<CArgs>            m_Args;       ///< Parsed cmd.-line args
486     TDisableArgDesc            m_DisableArgDesc;  ///< Arg desc. disabled
487     THideStdArgs               m_HideArgs;   ///< Std cmd.-line flags to hide
488     TStdioSetupFlags           m_StdioFlags; ///< Std C++ I/O adjustments
489     char*                      m_CinBuffer;  ///< Cin buffer if changed
490     string                     m_ProgramDisplayName;  ///< Display name of app
491     string                     m_ExePath;    ///< Program executable path
492     string                     m_RealExePath; ///< Symlink-free executable path
493     mutable string             m_LogFileName; ///< Log file name
494     string                     m_ConfigPath;  ///< Path to .ini file used
495     int                        m_ExitCode;    ///< Exit code to force
496     EExitMode                  m_ExitCodeCond; ///< When to force it (if ever)
497     bool                       m_DryRun;       ///< Dry run
498 };
499 
500 
501 /// Interface for application idler.
502 class NCBI_XNCBI_EXPORT INcbiIdler {
503 public:
504     virtual ~INcbiIdler(void) {}
505 
506     // Perform any actions. Called by RunIdle().
507     virtual void Idle(void) = 0;
508 };
509 
510 
511 /// Default idler.
512 class NCBI_XNCBI_EXPORT CDefaultIdler : public INcbiIdler
513 {
514 public:
515     CDefaultIdler(void) {}
516     virtual ~CDefaultIdler(void) {}
517 
518     virtual void Idle(void);
519 };
520 
521 
522 /// Return currently installed idler or NULL. Update idler ownership
523 /// according to the flag.
524 NCBI_XNCBI_EXPORT INcbiIdler* GetIdler(EOwnership ownership = eNoOwnership);
525 
526 /// Set new idler and ownership.
527 NCBI_XNCBI_EXPORT void SetIdler(INcbiIdler* idler,
528                                 EOwnership ownership = eTakeOwnership);
529 
530 /// Execute currently installed idler if any.
531 NCBI_XNCBI_EXPORT void RunIdler(void);
532 
533 
534 /* @} */
535 
536 
537 
538 /////////////////////////////////////////////////////////////////////////////
539 //  IMPLEMENTATION of INLINE functions
540 /////////////////////////////////////////////////////////////////////////////
541 
542 
543 inline const CNcbiArguments& CNcbiApplication::GetArguments(void) const
544 {
545     return *m_Arguments;
546 }
547 
548 inline const CNcbiEnvironment& CNcbiApplication::GetEnvironment(void) const
549 {
550     return *m_Environ;
551 }
552 
553 inline CNcbiEnvironment& CNcbiApplication::SetEnvironment(void)
554 {
555     return *m_Environ;
556 }
557 
558 inline const CNcbiRegistry& CNcbiApplication::GetConfig(void) const
559 {
560     return *m_Config;
561 }
562 
563 inline CNcbiRegistry& CNcbiApplication::GetConfig(void)
564 {
565     return *m_Config;
566 }
567 
568 inline const string& CNcbiApplication::GetConfigPath(void) const
569 {
570     return m_ConfigPath;
571 }
572 
573 inline bool CNcbiApplication::HasLoadedConfig(void) const
574 {
575     return !m_ConfigPath.empty();
576 }
577 
578 inline bool CNcbiApplication::ReloadConfig(CMetaRegistry::TFlags flags,
579                                            IRegistry::TFlags reg_flags)
580 {
581     return CMetaRegistry::Reload(GetConfigPath(), GetConfig(), flags,
582                                  reg_flags);
583 }
584 
585 inline const string& CNcbiApplication::GetProgramDisplayName(void) const
586 {
587     return m_ProgramDisplayName;
588 }
589 
590 inline const string&
591 CNcbiApplication::GetProgramExecutablePath(EFollowLinks follow_links) const
592 {
593     return follow_links == eFollowLinks ? m_RealExePath : m_ExePath;
594 }
595 
596 
597 inline const CArgDescriptions* CNcbiApplication::GetArgDescriptions(void) const
598 {
599     return m_ArgDesc.get();
600 }
601 
602 
603 inline bool CNcbiApplication::IsDryRun(void) const
604 {
605     return m_DryRun;
606 }
607 
608 
609 END_NCBI_SCOPE
610 
611 #endif  /* CORELIB___NCBIAPP__HPP */
612
