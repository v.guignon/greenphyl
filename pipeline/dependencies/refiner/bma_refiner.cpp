/*  $Id: bma_refiner.cpp 113835 2007-11-09 19:18:24Z lanczyck $
  * =========================================================================== 
  *                            PUBLIC DOMAIN NOTICE
  *               National Center for Biotechnology Information
  *
  *  This software/database is a "United States Government Work" under the
  *  terms of the United States Copyright Act.  It was written as part of
  *  the author's official duties as a United States Government employee and
  *  thus cannot be copyrighted.  This software/database is freely available
  *  to the public for use. The National Library of Medicine and the U.S.
  *  Government have not placed any restriction on its use or reproduction.
  *
  *  Although all reasonable efforts have been taken to ensure the accuracy
  *  and reliability of the software and data, the NLM and the U.S.
  *  Government do not and cannot warrant the performance or results that
  *  may be obtained by using this software or data. The NLM and the U.S.
  *  Government disclaim all warranties, express or implied, including
  *  warranties of performance, merchantability or fitness for any particular
  *  purpose.
  *
  *  Please cite the author in any work or product based on this material.
  *
  * ===========================================================================
  *
  * Authors:  Chris Lanczycki
  *
  * File Description:
  *       Block multiple alignment refiner application.  
  *       (formerly named AlignRefineApp2.cpp)
  *
  */
 
#include <ncbi_pch.hpp>
#include <corelib/ncbistd.hpp>
#include <objects/cdd/Cdd.hpp>
#include <algo/structure/bma_refine/RefinerEngine.hpp>

#include "bma_refiner.hpp"

//  hack to define namespace in using declaration in include file
BEGIN_SCOPE(cd_utils)
END_SCOPE(cd_utils)
#include <algo/structure/cd_utils/cuCdReadWriteASN.hpp>

USING_NCBI_SCOPE;
USING_SCOPE(objects);

BEGIN_SCOPE(align_refine)
 
const unsigned int CAlignmentRefiner::N_MAX_TRIALS = 500;
const unsigned int CAlignmentRefiner::N_MAX_CYCLES = 200;
const unsigned int CAlignmentRefiner::N_MAX_ROWS   = 2000;
 
static bool ReadCD(const string& filename, CCdd *cdd)
{
    // try to decide if it's binary or ascii
    auto_ptr<CNcbiIstream> inStream(new CNcbiIfstream(filename.c_str(), IOS_BASE::in | IOS_BASE::binary));
    if (!(*inStream)) {
        ERROR_MESSAGE_CL("Cannot open file '" << filename << "' for reading");
        return false;
    }
    string firstWord;
    *inStream >> firstWord;
    bool isBinary = !(firstWord == "Cdd");
    inStream->seekg(0);     // rewind file
    firstWord.erase();

    string err;
    EDiagSev oldLevel = SetDiagPostLevel(eDiag_Fatal); // ignore all but Fatal errors while reading data
    bool readOK = ReadASNFromFile(filename.c_str(), cdd, isBinary, &err);
    SetDiagPostLevel(oldLevel);
    if (!readOK)
        ERROR_MESSAGE_CL("can't read input file: " << err);

    return readOK;
}
 
 79 string RefinerRowSelectorCodeToStr(const RefinerRowSelectorCode& code) {
 80     if (code == eRandomSelectionOrder) 
 81         return "random (shuffled between cycles)";
 82     else if (code == eWorstScoreFirst)
 83         return "worst to best self-hit score";
 84     else if (code == eBestScoreFirst)
 85         return "best to best self-hit score";
 86     else 
 87         return "UNKNOWN!!";
 88 
 89 }
 90 
 91 /////////////////////////////////////////////////////////////////////////////
 92 //  Init test for all different types of arguments
 93 
 94 
 95 void CAlignmentRefiner::Init(void)
 96 {
 97     // Create command-line argument descriptions class
 98     auto_ptr<CArgDescriptions> argDescr(new CArgDescriptions);
 99 
100     // Specify USAGE context
101     argDescr->SetUsageContext(GetArguments().GetProgramBasename(),
102                               "Alignment Refinement w/ 'Leave One Out'");
103 
104     HideStdArgs(fHideLogfile | fHideConffile | fHideDryRun);
105 
106      // input/output CD
107     argDescr->AddKey("i", "CdFilenameIn", "full filename of input CD w/ alignment to refine (ascii or binary)", argDescr->eString);
108     argDescr->AddDefaultKey("o", "CdBasenameOut", "basename of output CD(s) containing refined alignment; output saved to 'basename_<number>.cn3'; ascii text by default", argDescr->eString, "refiner");
109 
110     // output binary
111     argDescr->AddFlag("ob", "output binary data");
112 
113     // File for data from the refinement process
114     argDescr->AddOptionalKey
115         ("details", "filename",
116          "create a file to save refinenment process details",
117          CArgDescriptions::eOutputFile,
118          CArgDescriptions::fPreOpen);
119     // quiet reporting of details (only at end of temp steps and trials)
120 
121     // quiet reporting (only 'Error' level messages)
122     argDescr->AddFlag("q", "shortened report; only Error level messages");
123 
124 
125     //  Number of cycles per trial (a cycle consists of a LOO phase followed by a 
126     //  block-editing phase, either of which may be disabled for all cycles)
127     //  In each LOO phase, each row is left out exactly once unless overriden by the 'nr' option.
128     //  The alignment is NOT reset after a cycle; alignment IS reset after a trial.
129     argDescr->AddDefaultKey("nc", "integer", "number of cycles per trial; a cycle consists of one leave-one-out (LOO) phase, followed by a block editing phase.  Either, but not both, of the phases in a cycle may be turned off.\n(Note:  alignments do NOT reset between cycles)\n", argDescr->eInteger, "1");
130     argDescr->SetConstraint("nc", new CArgAllow_Integers(1, N_MAX_CYCLES));
131 
132     //  Row selection order for LOO: randomly or based on the self-hit to the initial alignment.
133     argDescr->AddDefaultKey("selection_order", "integer", "Method for row selection in LOO phase:\n0 == randomly (default)\n1 == increasing self-hit row score to input alignment's PSSM\n2 == decreasing self-hit row score to input alignment's PSSM\n", argDescr->eInteger, "1");
134     argDescr->SetConstraint("selection_order", new CArgAllow_Integers(0, 2));
135 
136     // Convergence criteria
137     argDescr->AddDefaultKey("convSameScore", "double", "when >= this % of LOO attempts fail to change the score, stop further cycles", argDescr->eDouble, "0.95");
138     argDescr->SetConstraint("convSameScore", new CArgAllow_Doubles(0, 1.0));
139 
140 
141 
142 
143     //
144     //  Leave-one-out parameters & options
145     //
146 
147     argDescr->SetCurrentGroup("  Leave-one-out phase parameters and options  ");
148 
149     //  disable LOO phase
150     argDescr->AddFlag("no_LOO", "do not perform the LOO phase of each cycle", true);
151 
152     //  switch from 'leave-one-out' to 'leave-N-out':  recompute PSSM only after
153     //  lno rows refined.
154     argDescr->AddDefaultKey("lno", "integer", "leave-N-out mode:  speed up program by recomputing PSSM after 'lno' rows have been left out\n(values exceeding the number of rows interpreted as Nrows - 1)\nFor best results, value should be < 20% of number of rows in input alignment.\n", argDescr->eInteger, "1");
155     argDescr->SetConstraint("lno", new CArgAllow_Integers(0, kMax_Int));
156 
157     //  Number of row LOO events per cycle (may be equal to, more than or less than 
158     //  number of rows in the CD, and it is not guaranteed that each row will be chosen)
159 //    argDescr->AddOptionalKey("nr", "integer", "absolute number of LOO attempts per LOO cycle (defaults to all eligible rows in alignment except the master)\n", argDescr->eInteger);
160 //    argDescr->SetConstraint("nr", new CArgAllow_Integers(1, N_MAX_ROWS));
161 
162     //  declare whether structures are to be among the rows left out
163     argDescr->AddFlag("fix_structs", "do not perform LOO refinement on structures (i.e., those sequences having a PDB identifier)", true);
164 
165 
166     //
167     //  Block aligner parameters / constraints on search space
168     //
169 
170     //  leave-one-out arguments used by block aligner
171     argDescr->AddDefaultKey("p", "double", "percentile parameter for loop-length cutoff in block aligner", argDescr->eDouble, "1.0");
172     argDescr->SetConstraint("p", new CArgAllow_Doubles(0, 10.0));
173     argDescr->AddDefaultKey("x", "integer", "extension parameter for loop-length cutoff in block aligner", argDescr->eInteger, "0");
174     argDescr->SetConstraint("x", new CArgAllow_Integers(0, kMax_Int));
175     argDescr->AddDefaultKey("c", "integer", "cutoff parameter for loop-length cutoff in block aligner", argDescr->eInteger, "0");
176     argDescr->SetConstraint("c", new CArgAllow_Integers(0, kMax_Int));
177 
178     //  Constraints on sequence length used in search for improved alignments
179     argDescr->AddFlag("fs", "allow refiner to use full sequence (by default, refinement is constrained to initial aligned footprint)", true);
180     argDescr->AddDefaultKey("ex","integer", "footprint extension size (symmetric for N- and C-termini); positive values extend the footprint; negative values shrink the footprint and can be used with -fs; relevant extension overriden by -nex and/or -cex", argDescr->eInteger, "0");
181     argDescr->AddOptionalKey("nex","integer", "N-terminal footprint extension size; positive values extend the footprint; negative values shrink the footprint and can be used with -fs; overrides any N-terminal extension from -ex", argDescr->eInteger);
182     argDescr->AddOptionalKey("cex","integer", "C-terminal footprint extension size; positive values extend the footprint; negative values shrink the footprint and can be used with -fs; overrides any C-terminal extension from -ex\n", argDescr->eInteger);
183 
184 
185     //  Block freezing/un-freezing
186     argDescr->AddFlag("ab", "realign all blocks; overrides -f and -l options", true);
187     argDescr->AddOptionalKey("f", "integer", "first block to realign (post-IBM, from 1); overridden if -ab set", argDescr->eInteger);
188     argDescr->SetConstraint("f", new CArgAllow_Integers(1, kMax_Int));
189     argDescr->AddOptionalKey("l", "integer", "last block to realign (post-IBM, from 1); overridden if -ab set", argDescr->eInteger);
190     argDescr->SetConstraint("l", new CArgAllow_Integers(1, kMax_Int));
191 //    argDescr->AddExtra(0, 25, "Block number (post-IBM, from 1) to freeze:  can override -ab or -f, -l for a specific block(s) in a range of unfrozen blocks\n", argDescr->eInteger);
192     argDescr->AddExtra(0, 25, "Row OR block numbers to exclude from LOO (from 1 to # rows/blocks); master == row 1 is always excluded\n(See 'extras_are_blocks' flag.)\n", argDescr->eInteger);
193 
194     //  Flag to tell whether the extra args are row numbers (if not present) or block numbers (if present).
195     argDescr->AddFlag("extras_are_blocks", "treat extra arguments as block numbers (default is to treat them as row numbers)", true);
196 
197     argDescr->SetCurrentGroup("");
198 
199 
200 
201     //
202     //  Block editing options
203     //
204 
205     argDescr->SetCurrentGroup("  Block-editing phase parameters and options  ");
206 
207     argDescr->AddFlag("be_fix", "do not modify block boundaries", true);
208     argDescr->AddFlag("be_noShrink", "do not attempt to shrink block boundaries", true);
209     argDescr->AddFlag("be_shrinkFirst", "if set, try to shrink before extending a block; requires be_alg = both\n\n", true);
210 
211     argDescr->AddDefaultKey("be_minSize", "integer", "smallest allowed block width (set to 0 to allow block deletion events);\nblocks that start smaller than min size are not truncated.", argDescr->eInteger, "1");
212     argDescr->SetConstraint("be_minSize", new CArgAllow_Integers(0, 1000));
213 
214     //  Scoring methods and algorithms for block edits.
215     argDescr->AddDefaultKey("be_alg", "string", "block editing method", argDescr->eString, "both");
216     argDescr->SetConstraint("be_alg", &(*new CArgAllow_Strings, "extend", "greedyExt", "shrink", "both"));
217     argDescr->AddDefaultKey("be_score", "string", "column scoring method", argDescr->eString, "3.3.3");
218     argDescr->SetConstraint("be_score", &(*new CArgAllow_Strings, "vote", "sumScores", "median", "scoreWeight", "compound", "3.3.3"));
219 
220 
221     /////////////////////////
222     //  The following three options define the parameters for the 'compound' scoring method.
223     //  '3.3.3' is a special shortcut compound method that uses 3, .3, .3 for these values,
224     //  respectively.  
225     //  For future:  Use 'compound' to use same three scorers w/ non-default values.
226     /////////////////////////
227     argDesreturn result;
495 }
496 
497 
498 unsigned int  CAlignmentRefiner::GetBlocksToAlign(unsigned int nBlocks, vector<unsigned int>& blocks, string& msg, bool useExtras) {
499 
500     bool skip  = false;
501     unsigned int first = 0, last = nBlocks - 1;
502     blocks.clear();
503     if (nBlocks == 0) return 0;
504 
505     CArgs args = GetArgs();
506     unsigned int nExtra = (useExtras) ? (unsigned int) args.GetNExtra() : 0;
507 
508     //  If specify realignment of all blocks, the default settings are OK.
509     //  Otherwise, use the range specified in the -f and -l flags.
510     //  Recall that the command line takes in a one-based integer; blocks is zero-based.
511     if (!args["ab"]) {
512 
513         if (args["f"]) {
514             first = (unsigned) args["f"].AsInteger() - 1;
515         }
516         if (args["l"]) {
517             last  = (unsigned) args["l"].AsInteger() - 1;
518         }
519         if (first < 0 || first >= nBlocks) {
520             first = 0;
521         }
522         if (last < first || last >= nBlocks) {
523             last = nBlocks - 1;
524         }
525     }
526 
527     //  If there are any extra arguments provided, they refer to block numbers to freeze.
528     msg = "\nAligning blocks: ";
529     for (unsigned int i = first; i <= last; ++i) {
530         if (nExtra > 0) {
531             skip = false;
532             for (size_t extra = 1; extra <= nExtra; ++extra) {
533                 if (args[extra].AsInteger() - 1 == (int) i) {
534                     skip = true;
535                     break;
536                 }
537             }
538             if (skip) continue;
539         }
540         blocks.push_back(i);
541         msg.append(NStr::UIntToString(i+1) + " ");
542         if ((last-first+1)%15 == 0 && first != 0) msg.append("\n");
543     }
544     //TERSE_INFO_MESSAGE_CL("message in GetBlocksToAlign:\n" << msg);
545     return blocks.size();
546 }
547 
548 
549 RefinerResultCode CAlignmentRefiner::ExtractBEArgs(unsigned int nAlignedBlocks, string& msg) {
550 
551     // Get arguments
552     CArgs args = GetArgs();
553     RefinerResultCode result = eRefinerResultOK;
554 
555     m_blockEdit.editBlocks = (!args["be_fix"]);
556     m_blockEdit.canShrink  = (!args["be_noShrink"]);
557     m_blockEdit.extendFirst = (!args["be_shrinkFirst"]);
558 
559     msg.erase();
560     if (m_blockEdit.editBlocks) {
561 
562         //  Get algorithm to use.
563         string algMethod = args["be_alg"].AsString();
564         if (algMethod == "both") {
565             m_blockEdit.algMethod = eSimpleExtendAndShrink;
566         } else if (algMethod == "extend") {
567             m_blockEdit.algMethod = eSimpleExtend;
568         } else if (algMethod == "greedyExt") { 
569             m_blockEdit.algMethod = eGreedyExtend;
570         } else if (algMethod == "shrink") {
571             m_blockEdit.algMethod = eSimpleShrink;
572         } else {
573             m_blockEdit.algMethod = eInvalidBBAMethod;
574             msg += "Unrecognized algorithm specified:  'be_alg = " + algMethod + "'.  Stopping.";
575             result = eRefinerResultInconsistentArgumentCombination;
576         }
577 
578 
579         //  Define the column scoring method to be used.
580         //  Greedy extend implies median scoring in initial implementation.
581         string columnMethod = (m_blockEdit.algMethod == eGreedyExtend) ? "median" : args["be_score"].AsString();
582 
583         m_blockEdit.columnScorerThreshold = (double) args["be_minScore"].AsInteger();
584         if (columnMethod == "vote") {
585             m_blockEdit.columnMethod = ePercentAtOrOverThreshold;
586         } else if (columnMethod == "sumScores") {
587             m_blockEdit.columnMethod = eSumOfScores;
588         } else if (columnMethod == "median") {
589             m_blockEdit.columnMethod = eMedianScore;
590         } else if (columnMethod == "scoreWeight") {
591             m_blockEdit.columnMethod = ePercentOfWeightOverThreshold;
592         } else {
593             //  anything else is some flavor of compound scorer
594             m_blockEdit.columnMethod = eCompoundScorer;
595             m_blockEdit.median = args["be_median"].AsInteger();
596             m_blockEdit.negScoreFraction = args["be_negScore"].AsDouble();
597             m_blockEdit.negRowsFraction = args["be_negRows"].AsDouble();
598 
599             //  for the special case, ensure will be looking at -ve vs. non-neg in scorers
600             if (columnMethod == "3.3.3") m_blockEdit.columnScorerThreshold = 0;
601         }
602 
603 
604         //  Validate options re: block shrinking.
605         if (!m_blockEdit.canShrink) {
606             if (algMethod == "both") {
607                 WARNING_MESSAGE_CL("With '-be_noShrink' on, method 'both' is changed to 'extend'.");
608                 m_blockEdit.algMethod = eSimpleExtend;
609                 m_blockEdit.extendFirst = true;
610                 algMethod = "extend";
611             } else if (algMethod == "shrink") {
612                 msg += "\nInconsistent parameters:  Do not specify 'shrink' method with '-be_noShrink'!  Aborting";
613                 result = eRefinerResultInconsistentShrinkageSettings;
614             }
615         }
616 
617         //  Deal with shrinking & extending parameters.
618         if (!args["be_eThresh"] && !args["be_sThresh"] && 
619             !((columnMethod == "scoreWeight" && args["be_negScore"]) || columnMethod == "3.3.3")) {
620             msg += "\nWhen block boundaries are edited, you must provide either or both\nof the 'be_eThresh' or 'be_sThresh' options when not using '3.3.3', or 'scoreWeight' w/ be_negScore, scoring.\nAborting";
621             result = eRefinerResultInvalidThresholdValue;
622         }
623 
624         if (args["be_eThresh"] || columnMethod == "3.3.3") {
625             m_blockEdit.extensionThreshold = (args["be_eThresh"]) ? args["be_eThresh"].AsDouble() : 0;
626             m_blockEdit.shrinkageThreshold = (args["be_sThresh"]) ?
627                 args["be_sThresh"].AsDouble() : m_blockEdit.extensionThreshold;
628         } elf (args["be_sThresh"]) {
629             m_blockEdit.shrinkageThreshold = args["be_sThresh"].AsDouble();
630             m_blockEdit.extensionThreshold = m_blockEdit.shrinkageThreshold;
631         }
632 
633         if (columnMethod == "scoreWeight" && args["be_negScore"]) {
634             m_blockEdit.negScoreFraction = args["be_negScore"].AsDouble();
635             m_blockEdit.shrinkageThreshold = 1.0 - m_blockEdit.negScoreFraction;
636             m_blockEdit.extensionThreshold = m_blockEdit.shrinkageThreshold;
637         }
638 
639         if (!m_blockEdit.canShrink) m_blockEdit.shrinkageThreshold = 0;
640 
641 
642         //  Validate range of extension thresholds in voting (in other column
643         //  scoring methods, they may be outside this interval).
644         if (columnMethod == "vote") {
645             double et = m_blockEdit.extensionThreshold, st = m_blockEdit.shrinkageThreshold;
646             if (et < 0 || et > 1) {
647                 msg += "\nInvalid extension threshold (" + NStr::DoubleToString(et) + "); for vote scoring must be in [0, 1].\n";
648                 result = eRefinerResultInvalidThresholdValue;
649             }
650             if (st < 0 || st > 1) {
651                 msg += "\nInvalid shrinkage threshold (" + NStr::DoubleToString(st) + "); for vote scoring must be in [0, 1].\n";
652                 result = eRefinerResultInvalidThresholdValue;
653             }
654         }
655 
656         m_blockEdit.minBlockSize = (unsigned) args["be_minSize"].AsInteger();
657         m_blockEdit.columnMethod2 = eInvalidColumnScorerMethod;
658 
659         //  Exclude blocks in the same way done for LOO...
660         vector<unsigned int> blocksToAlign;
661         unsigned int nBlocksMade   = GetBlocksToAlign(nAlignedBlocks, blocksToAlign, msg, true);
662         m_blockEdit.editableBlocks.clear();
663         m_blockEdit.editableBlocks.insert(blocksToAlign.begin(), blocksToAlign.end());
664         msg = "Freeze " + NStr::UIntToString(nAlignedBlocks - nBlocksMade) + " blocks in ExtractBEArgs.\n";
665     }
666 
667     return result;
668 }
669 
670 
671 void CAlignmentRefiner::EchoSettings(ostream& echoStream, bool echoLOO, bool echoBE) {
672 
673     static string yes = "Yes", no = "No";
674 
675     CArgs args = GetArgs();
676     unsigned int nExtra = (unsigned int) args.GetNExtra();
677 
678     if ((!echoLOO && !echoBE) || (echoLOO && echoBE)) {
679         echoStream << "Global Refinement Parameters:" << endl;
680         echoStream << "=================================" << endl;
681         echoStream << "Number of trials = " << m_nTrials << endl;
682         echoStream << "Number of cycles per trial = " << m_nCycles << endl;
683         echoStream << "Alignment score deviation threshold = " << m_scoreDeviationThreshold << endl;
684 
685         if (nExtra > 0) {
686             echoStream << "Extra argument(s) freeze " << ((m_loo.extrasAreRows) ? "Row:\n    " : "Block:\n    ");
687             for (size_t extra = 1; extra <= nExtra; ++extra) {
688                 echoStream << args[extra].AsInteger() << "  ";
689             }
690             echoStream << endl;
691         } else {
692             echoStream << "No extra arguments that exclude specific rows/blocks from refinement." << endl;
693         }
694 //    echoStream << "Quiet details mode? " << ((m_quietDetails) ? "ON" : "OFF") << endl;
695 //    echoStream << "Forced threshold (for MC only) = " << m_forcedThreshold << endl;
696         echoStream << "Quiet mode? " << ((m_quietMode) ? "ON" : "OFF") << endl;
697         echoStream << endl;
698     }
699 
700     if (echoLOO) {
701         echoStream << "Leave-One_Out parameters:" << endl;
702         echoStream << "=================================" << endl;
703         echoStream << "LOO on?  " << ((m_loo.doLOO) ? yes : no) << endl;
704         echoStream << "Row selection order:  " << RefinerRowSelectorCodeToStr(m_loo.selectorCode) << endl;
705         echoStream << "Number left out between PSSM recomputation = " << m_loo.lno << endl;
706 
707         echoStream << "Freeze alignment of rows with structure?  " << ((m_loo.fixStructures) ? yes : no) << endl;
708         echoStream << "Use full sequence or aligned footprint?  " << ((m_loo.fullSequence) ? "Full" : "Aligned") << endl;
709         echoStream << "N-terminal extension allowed = " << m_loo.nExt << endl;
710         echoStream << "C-terminal extension allowed = " << m_loo.cExt << endl;
711 
712         echoStream << "Converged after fraction of rows left out do not change score = " << m_loo.sameScoreThreshold << endl;
713         echoStream << "Random number generator seed = " << m_loo.seed << endl;
714 
715         echoStream << "LOO loop percentile:  longest loop allowed = max initial loop * " << m_loo.percentile << endl;
716         echoStream << "LOO extension to longest loop allowed = " << m_loo.extension << endl;
717         echoStream << "LOO absolute maximum longest loop (zero == no max) = " << m_loo.cutoff << endl;
718         echoStream << endl;
719 }
720 
721     if (echoBE) {
722         string algMethod = "Invalid Method";
723         string columnMethod = algMethod;
724 
725         switch (m_blockEdit.algMethod) {
726         case eSimpleExtendAndShrink:
727             algMethod = "Extend and Shrink";
728             break;
729         case eSimpleExtend:
730             algMethod = "Extend Only";
731             break;
732         case eSimpleShrink:
733             algMethod = "Shrink Only";
734             break;
735         case eGreedyExtend:
736             algMethod = "Greedy Extend Only";
737             break;
738         default:
739             break;
740         };
741 
742         switch (m_blockEdit.columnMethod) {
743         case ePercentAtOrOverThreshold:
744             columnMethod = "% Rows at or Over Threshold";
745             break;
746         case eSumOfScores:
747             columnMethod = "Sum of Scores";
748             break;
749         case eMedianScore:
750             columnMethod = "Median Score";
751             break;
752         case ePercentOfWeightOverThreshold:
753             columnMethod = "% Score Weight at or Over Threshold";
754             break;
755         case eCompoundScorer:
756             columnMethod = "3.3.3";
757             if (GetArgs()["be_score"].AsString() != "3.3.3") {
758                 columnMethod = "Compound Scoring";
759             }
760             break;
761         default:
762             break;
763         };
764 
765 
766         echoStream << "Block editing parameters:" << endl;
767         echoStream << "=================================" << endl;
768         echoStream << "block editing on?         " << ((m_blockEdit.editBlocks) ? yes : no) << endl;
769         if (m_blockEdit.editBlocks) {
770             echoStream << "block shrinking on?       " << ((m_blockEdit.canShrink) ? yes : no) << endl;
771             echoStream << "extend first?             " << ((m_blockEdit.extendFirst) ? yes : no) << endl;
772             echoStream << endl;
773             echoStream << "block editing method    = " << algMethod << endl;
774             echoStream << "column scoring method   = " << columnMethod << endl;
775             echoStream << endl;
776 //        echoStream << "not used:  column meth2 = " << m_blockEdit.columnMethod2 << endl << endl; 
777             if (GetArgs()["be_score"].AsString() == "3.3.3") {
778                 echoStream << "(used for 3.3.3 scoring only):" << endl;
779                 echoStream << "    median threshold        = " << m_blockEdit.median << endl;
780                 echoStream << "    negative score fraction = " << m_blockEdit.negScoreFraction << endl;
781                 echoStream << "    negative row   fraction = " << m_blockEdit.negRowsFraction << endl;
782             } else {
783                 echoStream << "minimum block size      = " << m_blockEdit.minBlockSize << endl;
784                 echoStream << "column-scorer threshold = " << m_blockEdit.columnScorerThreshold << endl;
785                 echoStream << "extension threshold     = " << m_blockEdit.extensionThreshold << endl;
786                 echoStream << "shrinkage threshold     = " << m_blockEdit.shrinkageThreshold << endl;
787             }
788 
789         }
790         echoStream << endl;
791     }
792 }
793 
794 /////////////////////////////////////////////////////////////////////////////
795 //  Cleanup
796 
797 
798 void CAlignmentRefiner::Exit(void)
799 {
800     SetDiagStream(0);
801 }
802 
803 END_SCOPE(align_refine)
804 
805 /////////////////////////////////////////////////////////////////////////////
806 //  MAIN
807 
808 USING_SCOPE(align_refine);
809 
810 int main(int argc, const char* argv[])
811 {
812     int result;
813 
814     SetDiagStream(&NcbiCerr); // send all diagnostic messages to cerr
815     SetDiagPostLevel(eDiag_Warning);   
816     //    SetupCToolkitErrPost(); // reroute C-toolkit err messages to C++ err streams
817 
818     SetDiagTrace(eDT_Default);      // trace messages only when DIAG_TRACE env. var. is set
819     //#ifdef _DEBUG
820     //    SetDiagPostFlag(eDPF_File);
821     //    SetDiagPostFlag(eDPF_Line);
822     //#else
823     UnsetDiagTraceFlag(eDPF_File);
824     UnsetDiagTraceFlag(eDPF_Line);
825     //#endif
826 
827     // C++ object verification
828     CSerialObject::SetVerifyDataGlobal(eSerialVerifyData_Always);
829     CObjectIStream::SetVerifyDataGlobal(eSerialVerifyData_Always);
830     CObjectOStream::SetVerifyDataGlobal(eSerialVerifyData_Always);
831 
832     // Execute main application function
833     const CTime start(CTime::eCurrent);
834     CTime stop;
835     CAlignmentRefiner refiner;
836     result = refiner.AppMain(argc, argv, 0, eDS_Default, 0);
837 
838     //  Timing info
839     stop.SetCurrent();
840     cout << "\n\n****  Elapsed Time = " << stop.DiffSecond(start) << " sec  ****" << endl << endl;
841 
842     return result;
843 }
