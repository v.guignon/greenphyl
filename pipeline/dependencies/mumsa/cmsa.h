/*
	cmsa.h
	
	Released under GPL - see the 'COPYING' file   
	
	Copyright (C) 2005 Timo Lassmann <timolassmann@gmail.com>
	
	This program is free software; you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation; either version 2 of the License, or
	any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program; if not, write to the Free Software
	Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

	Please send bug reports, comments etc. to:
	timolassmann@gmail.com
*/

#define SEEK_START 0
#define SEEK_END 2
#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
#include <getopt.h>
#include <unistd.h>
#include <sys/wait.h>

extern unsigned int numseq;
extern unsigned int num_alignments;

void* tmalloc(int size);

struct aln_space{
	double* sets;
	double** sim;
};

struct column_node{
	struct column_node* next;
	unsigned int* column;
};

struct node{
	struct node* next;
	unsigned int pos;
	unsigned int group;
};

struct alignment{
	int** alignment;
	int* seq_len;
	unsigned int len;
	unsigned int pairs;
	unsigned int numseq;
};

int getopt(int argc, char * const argv[],const char *optstring);
struct node * insert_pair(unsigned int pos,unsigned int aln,struct node* n);

struct node** feed_hash(struct node** hash,struct alignment* ap,unsigned int a, unsigned int b,unsigned int aln,int gap_flag);

//struct alignment* read_aln_matrix(struct alignment* alignment,char* infile);
struct alignment* read_aln_matrix(struct alignment* ap, char* string);

int** read_columns_from_alignment(int** columns,char* infile,int total_seq_len);

char* get_alignment_into_string(char* string,char* infile);

//double* fill_sets(struct node** hash,double* sets,unsigned int hashsize,double test);
struct aln_space* make_sets(struct aln_space*,struct alignment** alignments,int hashsize,int gap_flag);
double** calculate_overlap(double* sets,double** overlap);
int pop(unsigned int x);
double* find_best_alignment(struct alignment** alignments,double* sets,double *scores);
void echo_alignment(char* InFileName);
void r_statistics_output(char ** name,double* scores,double** overlap,int num);
void print_alignment_scores(double diff,char**aln_name,double* scores,int server_flag);
void sanity_check(char**aln_name,struct alignment** alignments);

double* find_overlap_to_ref(double* overlap_to_ref,double* sets);

void  insert_column(int* column,struct column_node* old_n,struct column_node* n);
void make_columns(int** columns,struct alignment** alignments,int hashsize,int gap_flag,int criterion);
void inclusion_sort(int** numbers, int array_size,char**seq_names);
int find_criterion(double* sets,int level);

char** get_seq_names(char** seq_names,char* infile);
