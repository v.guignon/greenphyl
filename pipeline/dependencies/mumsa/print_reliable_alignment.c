/*
 	print_reliable_alignment.c
	
	Released under GPL - see the 'COPYING' file   
	
	Copyright (C) 2005 Timo Lassmann <timolassmann@gmail.com>
	
	This program is free software; you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation; either version 2 of the License, or
	any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program; if not, write to the Free Software
	Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
    
	Please send bug reports, comments etc. to:
	timolassmann@gmail.com
*/

#include "cmsa.h"

int find_criterion(double* sets,int level)
{
	int criterion = 0;
	int i;
	double max = -1.0;
	
	level = num_alignments - (level-1);
	if (level <= 0){
		level = 1;
	}
	
	for (i = (1 << num_alignments);i--;){
		if (pop(i) == level){
			if (sets[i] > max){
				max = (int) sets[i];
				criterion = i;
			}
		}
	}
	return criterion;
}

void make_columns(int** columns,struct alignment** alignments,int hashsize,int gap_flag,int criterion)
{
	struct node** hash = 0;
	struct node* old_n = 0;
	struct node* n = 0;
	int i,j,c;
	int* c_len = 0;
	int** cp1 = 0;
	int** cp2 = 0;
	
	c_len = tmalloc(sizeof(int)*numseq);
	
	c_len[0] = 0;
	for (i = 1; i< numseq;i++){
		c_len[i] = alignments[0]->seq_len[i-1] + c_len[i-1];
	} 
 
	gap_flag = 0;//gaps cannot be considered
	
	hash = tmalloc(sizeof(struct node*) * hashsize);
	for (i = hashsize;i--;){
		hash[i] = 0;
	}
	
	for (i =0; i < numseq;i++){
		cp1 = columns + c_len[i];
		for (j = i +1; j < numseq;j++){
			cp2 = columns + c_len[j];
			for (c = 0; c < num_alignments;c++){
				hash = feed_hash(hash,alignments[c],i,j,c,gap_flag);
			}
			for (c = 0;c < hashsize;c++){
				n = hash[c];
				while (n){
					//printf("%d	aligned to %d	in %d cases.\n",c,n->pos,n->group);
					if((n->group & criterion) == criterion){
						if (cp1[c][0] >= 0){
							if(cp2[n->pos][0] >= 0){
								cp1[c][j] = cp2[n->pos][j];
								cp2[n->pos][0] = -1;
								cp2[n->pos][1] = c+c_len[i];
							//	printf("++\n");
							}else{
								cp1[c][j] = columns[cp2[n->pos][1]][j];
								columns[cp2[n->pos][1]][0] = -1;
								columns[cp2[n->pos][1]][1] = c+c_len[i];
							//	printf("+-\n");
							}
						}else{
							if(cp2[n->pos][0] >= 0){
							//	printf("-+\n");
								columns[cp1[c][1]][j] = cp2[n->pos][j];
								cp2[n->pos][0] = -1;
								cp2[n->pos][1] = cp1[c][1];
							}//else{ /// all residues are already aligned correctly......
							//	printf("--\n");
							//	columns[cp1[c][1]][j] = columns[cp2[n->pos][1]][j];
							//	columns[cp2[n->pos][1]][0] = -1;
							//	columns[cp2[n->pos][1]][1] = cp1[c][1];
							//}
						}

					}
					//print_columns(columns);
					old_n = n;
					n = n->next;
					free(old_n);
				}
				hash[c] = 0;
			}
			//printf("\n");
		}
	}	
	free(hash);
	free(c_len);
}

void inclusion_sort(int** numbers, int array_size,char** seq_names)
{

	struct column_node* start = 0;
	struct column_node* n = 0;
	struct column_node* tmp = 0;
	
	int i;
	int aln_len = 0;
	char** aln = 0;
	
	aln = tmalloc(sizeof(char*)*numseq);
	
	start = tmalloc(sizeof(struct column_node));
	start->column = tmalloc(sizeof(int)*numseq);
	for (i =0; i < numseq;i++){
		start->column[i] = 0;
	}
	start->next = 0;
	
	for (i = 0;i< array_size;i++){
		if (numbers[i][0] !=-1){
		aln_len++;
		insert_column(numbers[i],start,start->next);
		}
	}
	for (i =0; i < numseq;i++){
		aln[i] = tmalloc(sizeof(char)*aln_len+1);
	}
	//printf ("INCLUSION Sort \n");
	aln_len = 0;
	n = start->next;
	while(n){
		tmp = n;
		for (i = 0; i< numseq;i++){
			if ((char)(n->column[i]&0x000000ff)){
			aln[i][aln_len] = (char)(n->column[i]&0x000000ff);
			}else{
			aln[i][aln_len] = '-';
			}
			//printf ("%c%d	",(char)(n->column[i]&0x0000ffff),n->column[i]>>16);
		}
		//printf ("\n");
		aln_len++;
		n = n->next;
		free(tmp->column);
		free(tmp);
	}
	for (i =0; i < numseq;i++){
		aln[i][aln_len] = 0;
		printf("%s\n",seq_names[i]);
		printf("%s\n",aln[i]);
		free(aln[i]);
		
	}
	free(aln);
	free(start->column);
	free(start);
}

void  insert_column(int* column,struct column_node* old_n,struct column_node* n)
{
	int i;
	int decision;
	struct column_node* tmp;
	if (n == NULL){
		n = tmalloc(sizeof(struct column_node));
		n->column = tmalloc( sizeof(int)* numseq);
		for (i = 0;i < numseq;i++){
			n->column[i] = column[i];
		}
		n->next = 0;
		old_n->next = n;
	}else if(n->column){
		decision = 0;
		for (i = 0;i < numseq;i++){
			if (n->column[i] && column[i]){
			if ((n->column[i]>>8) > (column[i]>>8)){
				decision = 1; 
			}
			}
		}
		if (decision){
			tmp = tmalloc(sizeof(struct column_node));
			tmp->column = tmalloc( sizeof(int)* numseq);
			for (i = 0;i < numseq;i++){
				tmp->column[i] = column[i];
			}
			tmp->next = n;
			old_n->next = tmp;
			return;
		}else{
			insert_column(column,n,n->next);
		}
	}else{
		insert_column(column,n,n->next);
	}
	return;
}
