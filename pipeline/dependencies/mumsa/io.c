/*
 	io.c
	
	Released under GPL - see the 'COPYING' file   
	
	Copyright (C) 2005 Timo Lassmann <timolassmann@gmail.com>
	
	This program is free software; you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation; either version 2 of the License, or
	any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program; if not, write to the Free Software
	Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
    
	Please send bug reports, comments etc. to:
	timolassmann@gmail.com
*/

#include "cmsa.h"
#include <string.h>

char** get_seq_names(char** seq_names, char* infile)
{
	int c = 0;
	int i = 0;
	int j = 0;
	int n = 0;
	int stop = 0;
  	int nbytes = 0;
  	char *string = 0;

	FILE *file = 0;
	if (!(file = fopen( infile, "r" ))){
 		fprintf(stderr,"Cannot open file for names '%s'\n", infile);
		exit(1);
	}
	if (fseek(file,0,SEEK_END) != 0){
		(void)fprintf(stderr, "ERROR: fseek failed\n");
		(void)exit(EXIT_FAILURE);
	}
	nbytes = ftell (file);
	if (fseek(file,0,SEEK_START) != 0){
		(void)fprintf(stderr, "ERROR: fseek failed\n");
		(void)exit(EXIT_FAILURE);
	}
	string = tmalloc (nbytes* sizeof(char));
	fread(string,sizeof(char), nbytes, file);
	fclose (file);
	
	seq_names = tmalloc(sizeof(char*)*numseq);//NOTE asssumes we know how many sequences.....!!!!
	j = -1;
	for (i =0;i < nbytes;i++){
		if (string[i] == '>'){
			j++;
			stop = 1;
			c = 0;
		}
		if((stop == 1) && (string[i] == '\n')){
			seq_names[j] = tmalloc(sizeof(char)*(c+1));
			n = 0;
			while (c){
				seq_names[j][n] = string[i-c];
				c--;
				n++;
			}
			seq_names[j][n] = 0;
			stop = 0;
		}
		if (stop == 1){
			c++;
		}
	}
	free(string);
	return seq_names;
}

int** read_columns_from_alignment(int** columns, char* infile, int total_seq_len)
{
	int c = 0;
	int i = 0;
	int j = 0;
	int n = 0;
	int stop = 0;
  	int nbytes = 0;
  	char *string = 0;

	FILE *file = 0;
	if (!(file = fopen( infile, "r" ))){
 		fprintf(stderr,"Cannot open file '%s'\n", infile);
		exit(1);
	}
	if (fseek(file,0,SEEK_END) != 0){
		(void)fprintf(stderr, "ERROR: fseek failed\n");
		(void)exit(EXIT_FAILURE);
	}
	nbytes = ftell (file);
	if (fseek(file,0,SEEK_START) != 0){
		(void)fprintf(stderr, "ERROR: fseek failed\n");
		(void)exit(EXIT_FAILURE);
	}
	string = tmalloc (nbytes* sizeof(char));
	fread(string,sizeof(char), nbytes, file);
	fclose (file);
	
	columns = tmalloc(sizeof(int*)*total_seq_len);
	for (i = 0;i < total_seq_len;i++){
		columns[i] = tmalloc(sizeof(int)*numseq);
		for(j=0;j < numseq;j++){
			columns[i][j] = 0; 
		}
	}

	j = -1;
	c = 0;
	n = 0;
	for (i =0;i < nbytes;i++){
		if (string[i] == '>'){
			stop = 1;
			j++;
			c = 0;
		}
		if (string[i] == '\n'){
			stop = 0;
		}
		if (stop == 0 && string[i] != '\n' && string[i] != 0 ){
			if(isalpha((unsigned int)string[i])){
				columns[n][j] = ((int) string[i]) | (c << 8);
				n++;
				c++;
			}
			
		}
	}
	free(string);
	return columns;
}

char* get_alignment_into_string(char* string,char* infile)
{
	int i = 0;     
	int string_length = 2;
	char c = 0;    
	FILE *file = 0;
	if(infile){
		if (!(file = fopen( infile, "r" ))){
 			fprintf(stderr,"Cannot open file '%s'\n", infile);
			exit(1);
		}
		if (fseek(file,0,SEEK_END) != 0){
			(void)fprintf(stderr, "ERROR: fseek failed\n");
			(void)exit(EXIT_FAILURE);
		}			 		 	 
		i= ftell (file);
		if (fseek(file,0,SEEK_START) != 0){
			(void)fprintf(stderr, "ERROR: fseek failed\n");
			(void)exit(EXIT_FAILURE);
		}
		string = tmalloc ((i+1)* sizeof(char));
		fread(string,sizeof(char), i, file);
		string[i] = 0;
		fclose(file);
	}else{
		if (!isatty(0)){
			string = tmalloc(sizeof(char*)*string_length);
			while (!feof (stdin)){
			c = getc(stdin);
			if (i == string_length){
				string_length <<=1;
				string = realloc(string,sizeof(char)*string_length);
			}
			string[i] = c;
			i++;
			}
			string[i] = 0;
		}else{
			return 0;
		}
	}
	return string;
}


struct alignment* read_aln_matrix(struct alignment* ap, char* string)
{
	int c = 0;
	int n = 0;
	int i = 0;
	int j = 0;
	int stop = 0;
  	int nbytes = 0;
  	//char *string = 0;
	char *a = 0;
	
	nbytes = strlen(string);
	
	ap = tmalloc(sizeof(struct alignment));
	ap->numseq = 0;
	for (i =0;i < nbytes;i++){
		if(islower((int)string[i])){
		string[i] = toupper((int)string[i]);
		}
		if (string[i] == '>'){
			ap->numseq++;
		}
	}
	ap->alignment = tmalloc(sizeof(int*)* ap->numseq);
	ap->seq_len = tmalloc(sizeof(int)*ap->numseq);
	ap->pairs = 0;
	ap->len = 0;
	a = string;
	c = 0;
	while(a){
		if (*a == '>'){
			c = 1;
		}
		if (c == 1 && *a == '\n'){
			a++;
			break;
		}
		a++;
	}
	while(*a != '>'){
		if (*a != '\n' && *a != '	' && *a != ' '){
			ap->len++;
		}
		a++;
	}
	for (i = ap->numseq;i--;){
		ap->alignment[i] = tmalloc(sizeof(int)*(ap->len));
	}
	j = -1;
	c = 0;
	for (i =0;i < nbytes;i++){
		if (string[i] == '>'){
			if(j != -1){
			ap->seq_len[j] = n;
			}
			stop = 1;
			j++;
			c = 0;
			n = 0;
			
		}
		if (string[i] == '\n'){
			stop = 0;
		}
		if (stop == 0 && string[i] != '\n' && string[i] != 0 && string[i] != ' ' && string[i] != '	'){
			if(isalpha((unsigned int)string[i])){
				ap->alignment[j][c] = n;
				n++;
			}
			if (string[i] == '-' || string[i] == '.'){
				ap->alignment[j][c] = ap->len;
			}
			c++;
		}
	}
	ap->seq_len[j] = n;
	
	return ap;
}

void sanity_check(char**aln_name,struct alignment** alignments)
{
	int i,j,c;
	for (i = 0;i < num_alignments;i++){
		for (j = i+1;j < num_alignments;j++){
			if (alignments[i]->numseq != alignments[j]->numseq){
				(void)fprintf(stderr, "Error:\nAlignment %s has %d sequences,\nAlignment %s has %d sequences.\n",aln_name[i],alignments[i]->numseq,aln_name[j],alignments[j]->numseq );
				(void)exit(EXIT_FAILURE);
			}
		}
	}
	numseq = alignments[0]->numseq;
		
	for (i = 0;i < num_alignments;i++){
		for (j = i+1;j < num_alignments;j++){
			for (c = 0;c < numseq;c++){
				if (alignments[i]->seq_len[c] != alignments[j]->seq_len[c]){
					(void)fprintf(stderr, "Error:\nSequence %d in alignment %d has different length than in alignment %d.\n",c,i,j);
					(void)exit(EXIT_FAILURE);
				}
			}
		}
	}
}

void print_alignment_scores(double diff,char**aln_name,double* scores,int server_flag)
{
	int i,j,c;
	double max = -1.0;
	int max_len = 0;
	int* len = 0;
	char **p = 0;
	
	p = tmalloc(sizeof(char*)*num_alignments);
	len = tmalloc(sizeof(int)*num_alignments);

	for (i = 0;i < num_alignments;i++){
		p[i] = aln_name[i];
		j =0;
		c = 0;
		while(aln_name[i][j]){
			if(aln_name[i][j] == 47){//47 = '/'
				c = j;
			}
			j++;
		}
		p[i] += c+1;
		len[i] = j-c+1;
		if(len[i] > max_len){
			max_len = len[i];
		}
		//	printf("%s\n",name[i]);
	}

	if (!server_flag){
		if(diff != -1){
			fprintf(stdout,"Alignment case difficulty (AOS score):%0.2f \n\n",diff);
		}
		fprintf(stdout,"Input Alignment:");
		if(15 < max_len){
			for (j = 0; j < max_len - 15;j++){
				fprintf(stdout," ");
			}
		}
		if(diff != -1){
		fprintf(stdout,"	Alignment accuracy (MOS Score):\n");
		}else{
		fprintf(stdout,"	Overlap Score:\n");
		}
		for (i = 0; i < num_alignments;i++){
			max = -1.0;
			c = 0;
			for (j = 0;j < num_alignments;j++){
				if (scores[j] > max){
					max = scores[j];
					c = j;
				}
			}
			fprintf(stdout,"%s",p[c]);
			for (j = 0; j < max_len - len[c];j++){
				fprintf(stdout," ");
			}
			fprintf(stdout,"	%0.2f\n",scores[c]);
			scores[c] = -2;
		}
	}else{
		if(diff != -1){
			fprintf(stdout,"%f	",diff);
		}
	
		for (i = 0; i < num_alignments;i++){
			fprintf(stdout,"%f	",scores[i]);
		}
		fprintf(stdout,"\n");
	}
	free(len);
	free(p);
}

void r_statistics_output(char ** name,double* scores,double** overlap,int num)
{
	FILE *fp;
	int i,j,c;
	int pid;
	char filename[] = "/tmp/exampleXXXXXX";
	int fd;
	char **p = 0;
	p = tmalloc(sizeof(char*)*num);
	
	if((fd = mkstemp(filename)) < 0){
		perror("mkstemp failed");
		exit(1);
	}
	if ((fp = fdopen(fd, "w")) == NULL){
		fprintf(stderr,"can't open output	%s\n",filename);
	}else{
		if(num == num_alignments){
			fprintf(fp,"postscript(file=\"quality.eps\",width=6.0,height=12.0,horizontal=F,paper=\"special\",family=\"Helvetica\",onefile = FALSE,pointsize=18,bg =\"white\")\n");
		}else{
			fprintf(fp,"postscript(file=\"sequences.eps\",width=6.0,height=12.0,horizontal=F,paper=\"special\",family=\"Helvetica\",onefile = FALSE,pointsize=4,bg =\"white\")\n");
		}
		fprintf(fp,"x <- matrix(ncol = %d,nrow = %d)\n",num,num);
		for (i = 0;i < num;i++){
			p[i] = name[i];
			j =0;
			c = 0;
			while(name[i][j]){
				if(name[i][j] == 47){//47 = '/'
					c = j;
				}
				j++;
			}
			if(c){
			p[i] += c+1;
			}
		}
		fprintf(fp,"colnames(x) = c(");
		for (i = 0;i < num-1;i++){
			fprintf(fp,"\"%s:	%0.2f\",",p[i],scores[i]);
		}
		fprintf(fp,"\"%s:	%0.2f\")\n",p[num-1],scores[num-1]);
		
		fprintf(fp,"rownames(x) = c(");
		for (i = 0;i < num-1;i++){
			fprintf(fp,"\"%s:	%0.2f\",",p[i],scores[i]);
		}
		fprintf(fp,"\"%s:	%0.2f\")\n",p[num-1],scores[num-1]);
		
		for (i = 0;i < num;i++){
			for ( j = 0; j < num;j++){
				fprintf(fp,"x[%d,%d] = %f\n",i+1,j+1,overlap[i][j]);
			}
		}
		fprintf(fp,"dst.mat<-as.dist(1-x)\n");
		fprintf(fp,"hcl.average<-hclust(dst.mat,method=\"average\")\n");
		if(num == num_alignments){
			fprintf(fp,"plclust(hcl.average,frame.plot = TRUE,ann = TRUE,hang = -1,axes=TRUE,unit=FALSE,xlab = \"Alignment Programs\",sub = \"\",ylab = \"Average Distance\")\n");
		}else{
			fprintf(fp,"plclust(hcl.average,frame.plot = TRUE,ann = TRUE,hang = -1,axes=TRUE,unit=FALSE,xlab = \"\",sub = \"\",ylab = \"\")\n");
		}
		fprintf(fp,"dev.off()\n");
		fclose(fp);
	}
	
	
	free(p);
	
	//printf("Forking process	%d\n",getpid());
	pid = fork();
	//printf("The process id is %d and return value is %d\n",	getpid(), return_value);
	if ( pid == 0 ){ /* Child process */ 
		execl("/usr/bin/R","R","CMD","BATCH",filename,"logfile.out",NULL);
	}else{ /* Parent process pid is child's pid */
		wait(0);
		if ((unlink(filename)) == -1){
			perror("Removing temporary file failed");
			exit(1);
		}
	}
}

void echo_alignment(char* InFileName)
{
  	unsigned int nbytes = 0;
	char* alp = 0;
	FILE *file = NULL;
	if (!(file = fopen(InFileName, "r" ))){
 		fprintf(stderr,"Cannot open file '%s'\n",InFileName);
		exit(1);
	}
	
	
	if (fseek(file,0,SEEK_END) != 0){
		(void)fprintf(stderr, "ERROR: fseek failed\n");
		(void)exit(EXIT_FAILURE);
	}
	nbytes = ftell (file);
	if (fseek(file,0,SEEK_START) != 0){
		(void)fprintf(stderr, "ERROR: fseek failed\n");
		(void)exit(EXIT_FAILURE);
	}
	alp = tmalloc ((nbytes+1) * sizeof(char));
	fread(alp,sizeof(char), nbytes, file);
	alp[nbytes] = 0;
	printf("%s\n",alp);
	free(alp);
	fclose(file);
}
