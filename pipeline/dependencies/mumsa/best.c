/*
 	best.c
	
	Released under GPL - see the 'COPYING' file   
	
	Copyright (C) 2005 Timo Lassmann <timolassmann@gmail.com>
	
	This program is free software; you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation; either version 2 of the License, or
	any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program; if not, write to the Free Software
	Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
    
	Please send bug reports, comments etc. to:
	timolassmann@gmail.com
*/

#include "cmsa.h"

double** calculate_overlap(double* sets, double** overlap)
{
	int test = 0;
	int i,j,c;
	int temp;
	for (j = 0; j < num_alignments;j++){
		for (c =j+1;c < num_alignments;c++){
			test = (1 << j)  | (1 << c);
			temp = 0;
			for (i = (1 << num_alignments);i--;){
				if ((i & test) == test){
					temp += sets[i];
				}
			}
			overlap[j][c] = temp;
		}
	}
	return overlap;
} 

double* find_overlap_to_ref(double* overlap_to_ref,double* sets)
{
	int i = 0;
	for (i = (1 << num_alignments);--i;){
		//printf("%d	%d %f\n",i,pop(i),sets[i]);
		if(i & 1){
			overlap_to_ref[pop(i)-1] += sets[i];
		}
	}	
	return overlap_to_ref;
}


double* find_best_alignment(struct alignment** alignments,double* sets,double *scores)
{
	int i = 0;
	int j = 0;
	int c = 0;

	//score Alignments
	for (i = (1 << num_alignments);i--;){
		j = 1 << (num_alignments-1);
		c = num_alignments-1;
		while(j){
			if (i & j){
				//Shall we include aligned Pairs of residues without support? Possibly increases the score of global methods
				//if(pop(i) > 1){
					scores[c] += sets[i]*(pop(i)-1);
				//}
			}
			j = j >>1;
			c--;
		}
	}
	
	for (i = num_alignments;i--;){
		scores[i] /= ((num_alignments-1) *(double)alignments[i]->pairs);
	}
	return scores;
}

int pop(unsigned int x)
{
	static char table[256] = {0, 1, 1, 2, 1, 2, 2, 3, 1, 2, 2, 3, 2, 3, 3, 4, 1, 2, 2, 3, 2, 3, 3, 4, 2, 3, 3, 4, 3, 4, 4, 5, 1, 2, 2, 3, 2, 3, 3, 4, 2, 3, 3, 4, 3, 4, 4, 5, 2, 3, 3, 4, 3, 4, 4, 5, 3, 4, 4, 5, 4, 5, 5, 6, 1, 2, 2, 3, 2, 3, 3, 4, 2, 3, 3, 4, 3, 4, 4, 5, 2, 3, 3, 4, 3, 4, 4, 5, 3, 4, 4, 5, 4, 5, 5, 6, 2, 3, 3, 4, 3, 4, 4, 5, 3, 4, 4, 5, 4, 5, 5, 6, 3, 4, 4, 5, 4, 5, 5, 6, 4, 5, 5, 6, 5, 6, 6, 7, 1, 2, 2, 3, 2, 3, 3, 4, 2, 3, 3, 4, 3, 4, 4, 5, 2, 3, 3, 4, 3, 4, 4, 5, 3, 4, 4, 5, 4, 5, 5, 6, 2, 3, 3, 4, 3, 4, 4, 5, 3, 4, 4, 5, 4, 5, 5, 6, 3, 4, 4, 5, 4, 5, 5, 6, 4, 5, 5, 6, 5, 6, 6, 7, 2, 3, 3, 4, 3, 4, 4, 5, 3, 4, 4, 5, 4, 5, 5, 6, 3, 4, 4, 5, 4, 5, 5, 6, 4, 5, 5, 6, 5, 6, 6, 7, 3, 4, 4, 5, 4, 5, 5, 6, 4, 5, 5, 6, 5, 6, 6, 7, 4, 5, 5, 6, 5, 6, 6, 7, 5, 6, 6, 7, 6, 7, 7, 8};
	return table[x & 0xff]+
			table[(x >> 8) & 0xff]+
			table[(x >> 16) & 0xff]+
			table[(x >> 24)];
}
