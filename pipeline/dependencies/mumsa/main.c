/*
	main.c
	
	Released under GPL - see the 'COPYING' file   
	
	Copyright (C) 2005 Timo Lassmann <timolassmann@gmail.com>
	
	This program is free software; you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation; either version 2 of the License, or
	any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program; if not, write to the Free Software
	Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
    
	Please send bug reports, comments etc. to:
	timolassmann@gmail.com
*/

#include "cmsa.h"

unsigned int numseq = 0;
unsigned int num_alignments = 0;

int main(int argc, char** argv)
{
	struct alignment** alignments = 0;
	struct aln_space* aln_space = 0;
	double* sets = 0;
	double* scores = 0;
	double** overlap = 0;
	char ** aln_name = 0;
	double max = 0;
	double diff = 0;
	int i,j,c;
	int hashsize = 0;
	int gap_flag = 0;
	int ref_flag = 0;
	int quiet_flag = 0;
	int print_alignment_flag = 0;
	int server_flag = 0;
	
	char* string = 0;
	
	static char  usage[] ="\n---------------------------------------------------------------------\n\n\
MUMSA version 1.0, Copyright (C) 2005 Timo Lassmann <timolassmann@gmail.com>\n\
http://msa.cgb.ki.se/\n\n\
Usage: mumsa [options] [test_alignment.fasta] [test_alignment 1.fasta] [test alignment_2.fasta]...\n\n\
Option:\n\
	-a	prints the alignment with the highest MOS score\n\
		to stdout.\n\
	-g	considers residues aligned to gaps in addition to pairs\n\
		of aligned residues when calculating all scores.\n\
	-s	prints AOS score followed by the MOS scores of the input\n\
		alignments in one tab-separated line.\n\
	-r	calculates overlap scores in respect to the first (reference) alignment.\n\
	-q	quiet mode - removes this message.\n\n\
	If R is installed, MUMSA produces two plots when run in default\n\
	mode (or with the '-g' option):\n\
	quality.eps	shows a tree reflecting how similar the input \n\
			alignments are to each other.\n\
	sequences.eps	shows a tree reflecting how well individual sequences\n\
			fits into the alignment case. Non-homologous sequence\n\
			will show up here.\n\n\
Please cite:\n\n\
	Timo Lassmann and Erik L. L. Sonnhammer (2005)\n\
	Automatic assessment of alignment quality.\n\
	Nucleic Acids Research Vol.33(22) pp.7120-7128\n\n\
---------------------------------------------------------------------\n\n";

		
	while ((c = getopt(argc, argv, "hsargq")) != -1) {
		switch(c) {
			case 'a':
				print_alignment_flag = 1;
			break;
			case 's':
				server_flag = 1;
			break;
			case 'r':
				ref_flag = 1;
			break;
			case 'g':
				gap_flag = 1;
			break;
			case 'q':
				quiet_flag = 1;
			break;
			case '?':
			(void)fprintf(stderr, "unknown argument: %c\n",optopt);
			(void)exit(EXIT_FAILURE);
			break;
		}
	}	
	if(quiet_flag){
		fclose(stderr);
	}
	if (!server_flag)fprintf(stderr,"%s\n",usage);

	if (optind < argc){
		num_alignments = argc - optind;
		alignments =  tmalloc(sizeof(struct alignment*)*num_alignments);
		aln_name = tmalloc(sizeof(char*)*num_alignments);
		i = 0;
		while (optind < argc){
			aln_name[i] = argv[optind];
			
			string = get_alignment_into_string(string,argv[optind++]);
			alignments[i] = read_aln_matrix(alignments[i],string);
			free(string);
			
			//alignments[i] = read_aln_matrix(alignments[i],argv[optind++]);
			i++;
		}
	}else{
		(void)fprintf(stderr, "No Alignments...\n");
		(void)exit(EXIT_FAILURE);
	}
	
	//Sanity checks
	sanity_check(aln_name,alignments);
			
	for ( i = 0; i < num_alignments;i++){
		if (hashsize < alignments[i]->len){
			hashsize = alignments[i]->len;
		}
	}
	if(gap_flag){
		gap_flag = hashsize;
	}
	hashsize++;
	
	aln_space = tmalloc(sizeof(struct aln_space)*1);
	aln_space = make_sets(aln_space,alignments,hashsize,gap_flag);
	
	sets = aln_space->sets;
		
	scores = tmalloc(sizeof(double)*num_alignments);
	for (i = num_alignments;i--;){
		scores[i] = 0;
	}
		
	overlap = tmalloc(sizeof(double*)*num_alignments);
	for (i = 0;i < num_alignments;i++){
		overlap[i] = tmalloc(sizeof(double)*num_alignments);
		for ( j = 0; j < num_alignments;j++){
			overlap[i][j] = 0.0f;
		}
	}
	
	overlap = calculate_overlap(sets,overlap);
	for (i = 0;i < num_alignments;i++){
		for ( j = 0; j < num_alignments;j++){
			overlap[j][i] = overlap[i][j];
		}
	}
	
	if(ref_flag){
		overlap[0][0] = 1;
		for (i = 1;i < num_alignments;i++){
			overlap[0][i] = (double)overlap[0][i]/(double)alignments[0]->pairs;
		}
		print_alignment_scores(-1,aln_name,overlap[0],server_flag);
	}else if(print_alignment_flag){
		scores = find_best_alignment(alignments,sets,scores);
		max = -1;
		for (i = num_alignments;i--;){
			if(scores[i] > max){
				max = scores[i];
				c = i;
			}
		}
		echo_alignment(aln_name[c]);
	}else{
	
		for (i = 0;i < num_alignments;i++){
			for ( j = 0; j < num_alignments;j++){
				overlap[i][j] = overlap[i][j]/(((double)alignments[i]->pairs+(double)alignments[j]->pairs)/2);
			}
		}
		
		diff = 0.0;
		c = 0;
	
		for (i = 0;i < num_alignments;i++){
			for ( j = 0; j < num_alignments;j++){
				overlap[j][i] = overlap[i][j];
				diff += overlap[i][j];
				c++;
			}
		}
		diff = (double)diff/(double)c;
	
	
		scores = find_best_alignment(alignments,sets,scores);

		//Try to produce R output (Relation between alignments)
		if(!server_flag){
			r_statistics_output(aln_name,scores,overlap,num_alignments);
		}
		
		//Try to produce R output (Relation between sequences );
		char** seq_names = 0;
		double* seq_scores = tmalloc(sizeof(double)*numseq);
		for (i =0; i < numseq;i++){
			seq_scores[i] = aln_space->sim[i][i];
		}
		seq_names = get_seq_names(seq_names,aln_name[0]);
		
		
		if(!server_flag){
			r_statistics_output(seq_names,seq_scores,aln_space->sim,numseq);
		}
		free(seq_scores);
		for (i =0; i < numseq;i++){
			free(seq_names[i]);
		}
		free(seq_names);
	
		//Print out default output -> alignment scores....
		print_alignment_scores(diff,aln_name,scores,server_flag);
		
		
	
	}
	for (i = numseq;i--;){
		free(aln_space->sim[i]);
	}
	free(aln_space->sim);
	free(aln_space->sets);
	free(aln_space);
	
	free(scores);
	
	for (i = 0;i < num_alignments;i++){
		free(overlap[i]);
	}
	free(overlap);
	
	for ( i = 0;i < num_alignments;i++){
		for (j = numseq;j--;){
			free(alignments[i]->alignment[j]);
		}
		free(alignments[i]->seq_len);
		free(alignments[i]->alignment);
		free(alignments[i]);
	}
	free(alignments);
	free(aln_name);
	return 0;
}

void* tmalloc(int size){
	void* p;
	p = (void*)malloc(size);
	if (!p){
		fprintf(stderr,"Out of memory!\n");
		exit(0);
	}
	return p;
}
