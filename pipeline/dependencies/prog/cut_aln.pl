#!/usr/bin/perl

=pod

=head1 NAME

cut_aln.pl - Remove bad alignment columns scored by Al2Co

=head1 SYNOPSIS

    cut_aln.pl alignment.aln 2

=head1 REQUIRES

Perl5

=head1 DESCRIPTION

This script generates a new alignment file with bad columns removed. If some
sequences are also removed from the alignment, they are listed in a another
file.

=cut

use strict;
use warnings;
use Carp qw (cluck confess croak);

use Error qw(:try);

use Bio::LocatableSeq;
use Bio::SimpleAlign;
use Bio::AlignIO;

++$|;

my $g_in_file             = shift();
my $g_threshold           = shift() || 2; #conserve only value > or = to 2 (remove only column with 0 or 1 value
my %data;

my $g_bsp_filename        = $g_in_file;
# remove extension
$g_bsp_filename =~ s/\.[^.]+$//;
# copy name without extension for filtered sequences
my $g_filteredseq_filename = $g_bsp_filename;
# append .bsp
$g_bsp_filename .= '.bsp';
# append extension
$g_filteredseq_filename .= '_cutalign_filtered.txt';


# remove column with '0' value (ie. not '1')
sub apply_mask
{
    my ($string, $mask) = @_;
    if (!defined($string))
    {
        confess "ERROR: no string provided!\n";
    }
    elsif (!$string)
    {
        confess "ERROR: empty string!\n";
    }

    if (!defined($mask))
    {
        confess "ERROR: no mask provided!\n";
    }
    elsif (!$mask)
    {
        confess "ERROR: empty mask!\n";
    }

	my @string = split(//, $string);
	my @mask   = split(//, $mask);
	
	for my $i (0 .. $#string)
	{
        if (!$mask[$i])
        {
            $string[$i] = '';
        }
	}

	return join('', @string);
}


# get column position for value > or = to threshold
sub makeBSP
{
    my $mask = shift();
    my $cpt   = 0;
    my @value = split(//, $mask);
    my $line = '';
    for my $a (@value)
    {
        if ($a > 0)
        {
            $line .= ' ' . $cpt;
        }
        ++$cpt;
    }
    $line =~ s/^\s//;
    write_bsp($line);
}

sub write_bsp
{
	my $line = shift();
	open(F, ">$g_bsp_filename") or confess "ERROR: Cannot create bsp file: $!\n";
	print F $line;
	close(F);
}


my @removed_sequences = ();

# get sequence id
open(IN, $g_in_file) or confess "ERROR: unable to open file '$g_in_file'!\n$!\n";
while (<IN>)
{
	    chomp;
	    my @line = split(/\s+/);
    if (scalar(@line) > 1)
	    {
		    $line[0] =~ s/^(.+)\/\d+-\d+$/$1/;
		    #print "$1\n"; #= id sequence
		    $data{$line[0]} .= $line[1];
	    }
}
close(IN);

# get AL2CO score
my $score = delete($data{'CSV:'});

if (!defined($score) || (2 >= length($score)))
{
    confess "ERROR: failed to read score line!\n";
}

my $mask = join('', map {$_ < $g_threshold ? 0 : 1} split(//, $score));

my $aln = new Bio::SimpleAlign;
# try to cut if not too stringent
try
{
    foreach (keys(%data))
    {
        #print "debug: $data{$_} \n $mask \n\n";
        my $sequence = apply_mask($data{$_}, $mask) || "X";
        
        if (!$sequence || ($sequence !~ m/\w/))
        {
            confess "Warning: too stringent! Trying less stringent...\n";
        }
	    my $seq = new Bio::LocatableSeq(
		    -id  => $_,
            -seq => $sequence,
            -alphabet=>'protein',
	    );
	    $aln->add_seq($seq);
    }
}
otherwise
{
    cluck shift();
    # try less stringent
    $mask = join("", map {$_ < $g_threshold-1 ? 0 : 1} split(//, $score));
    $aln = new Bio::SimpleAlign;
    try
	{
        foreach (keys(%data))
        {
            my $sequence = apply_mask($data{$_}, $mask) || "X";
            if (!$sequence || ($sequence !~ m/\w/))
            {
                confess "Warning: still too stringent! Try to remove full-gapped sequences...\n";
	}
            my $seq = new Bio::LocatableSeq(
                -id  => $_,
                -seq => $sequence,
                -alphabet=>'protein',
            );
            $aln->add_seq($seq);
        }
    }
    otherwise
    {
        cluck shift();
        # use original alignment
        $mask = join('', map {$_ < $g_threshold ? 0 : 1} split(//, $score));
        $aln = new Bio::SimpleAlign;
        foreach (keys(%data))
        {
            # check if the sequence has at least 1 amino acid in order to be processed by BioPerl
            my $sequence = apply_mask($data{$_}, $mask) || "X";
            if ($sequence && ($sequence =~ m/\w/))
            {
                my $seq = new Bio::LocatableSeq(
                    -id  => $_,
                    -seq => $sequence,
                    -alphabet=>'protein',
#                    -end=>0,
                );
                $aln->add_seq($seq);
         }
            else
            {
                # gapped sequences are removed
                warn "Sequence $_ contains only gaps and has been filtered.\n";
                push(@removed_sequences, $_);
    }
        } 
    };
};

$aln->map_chars('\.', '-');
my $alnIO = Bio::AlignIO->newFh('-format' => 'fasta');
print {$alnIO} $aln;
&makeBSP($mask);

# save list of filtered sequences
open(OUT, ">$g_filteredseq_filename") or confess "ERROR: unable to open file '$g_filteredseq_filename' for writing!\n$!\n";
print OUT join("\n", @removed_sequences);
close(OUT);

exit(0);
