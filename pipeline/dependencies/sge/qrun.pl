#!/usr/bin/perl

=pod

=head1 NAME

qrun.pl - Runs a command on SGE using qsub

=head1 SYNOPSIS

    qrun.pl ls -a -l >example.txt

=head1 REQUIRES

SGE, qsub, qstat, qdel, File::Temp

=head1 DESCRIPTION

Sends a command to a Sun Grid Engine (SGE) queue.
Standard input, output and error of the command are redirected from
and to qrun.pl standard input, output and error.
qrunl.pl waits for the command to end before ending itself.
Note: qrunl.pl must be able to create and write files into current directory.

=cut

use strict;
use Carp qw (cluck confess croak);
use warnings;

use Error qw(:try);

use Pod::Usage;

use File::Temp qw/ tempfile :mktemp /;


# Script global constants
##########################

=pod

=head1 CONSTANTS

B<$TEMP_TEMPLATE_*>: (string)
template used to generate temporary file names for stdin, stdout and stderr
redirection.

B<$TEMP_SUFFIX>: (string)
file name suffix for temporary files.

B<$SLEEP_TIME>: (string)
time to sleep between two qstat tests (to check if the job is over).

=cut

my $TEMP_TEMPLATE_IN  = 'qsinXXXXXXXX';
my $TEMP_TEMPLATE_OUT = 'qsoutXXXXXXX';
my $TEMP_TEMPLATE_ERR = 'qserrXXXXXXX';
my $TEMP_SUFFIX       = '.tmp';
my $SLEEP_TIME        = '1'; # seconds
my $DEFAULT_QUEUE     = 'greenphyl.q';
my $MPI_EXEC          = 'mpiexec';

my $GREENPHYL_EMAIL   = 'v.guignon\@cgiar.org'; # note: the @ must be provided to the command line escaped with '\', be aware to also escape the '\' if you use double quotes
my $GREENPHYL_USER    = 'greenphyl';
my $GREENPHYL_PROJECT = 'greenphyl';

my $SGE_DEFAULT_ARGS  = "-terse -cwd -b y -j n -V -m a";
#my $SGE_DEFAULT_ARGS  = "-terse -cwd -b y -j n -V -m a -M $GREENPHYL_EMAIL -N $GREENPHYL_USER -P $GREENPHYL_PROJECT"; # replace above line the day we got a 'project' for SGE


# Script options
#################

=pod

=head1 OPTIONS

qrun.pl <program_name> [program_parameters...]

=head2 Parameters

=over 4

=item B<program_name>:

name of the program to run.

=item B<program_parameters>:

any number of parameters to give to the program.

=back

=cut


# CODE START
#############

if (not @ARGV)
{
    pod2usage(1);
}

#$ENV{'TERM'}             = 'xterm';

#source /sge/default/common/settings.sh
my $SGE_ROOT = $ENV{'SGE_ROOT'} = '/SGE/8.1.8';
my $command;
# $command     = `/bin/arch`;
# chomp($command);
# $ENV{'ARCH'}             = $command;
# $command = `$SGE_ROOT/util/arch -m`;
# chomp($command);
# $ENV{'DEFAULTMANPATH'}   = $command;
# $command = `$SGE_ROOT/util/arch -mt`;
# chomp($command);
# $ENV{'MANTYPE'}          = $command;
$ENV{'SGE_ARCH'}         = 'lx-amd64';
$ENV{'SGE_CELL'}         = 'CIRAD_CLC2';
$ENV{'SGE_CLUSTER_NAME'} = 'CIRAD';
# $ENV{'SGE_QMASTER_PORT'} = 33001;
# $ENV{'SGE_EXECD_PORT'}   = 33002;
#if ('' eq $ENV{'MANPATH'})
#{
#    $ENV{'MANPATH'}      = $ENV{'DEFAULTMANPATH'};
#}
#my $man_type = $ENV{'MANTYPE'};
#if (not $man_type)
#{
#	$man_type = '';
#}
#my $man_path = $ENV{'MANPATH'};
#if (not $man_path)
#{
#	$man_path = '';
#}
#$ENV{'MANPATH'}          = "$SGE_ROOT/" . $man_type . ":/SGE/n1ge62/man:" . $man_path;

#my $arch = $ENV{'ARCH'};
#if (not $arch)
#{
#	$arch = '';
#}
#my $path = $ENV{'PATH'};
#if (not $path)
#{
#	$path = '';
#}
#$ENV{'PATH'}             = "$SGE_ROOT/bin/" . $arch . ':' . $path;

#+debug
#foreach my $key (sort keys(%ENV))
#{
#    print STDERR "$key = $ENV{$key}\n";
#}

#my $shlib_path_name      = `$ENV{'SGE_ROOT'}/util/arch -lib`;
#my $old_value            = `eval echo '$'$shlib_path_name`;
#if ('' eq $old_value)
#{
#    eval $shlib_path_name=$SGE_ROOT/lib/$ARCH;
#}
#else
#{
#    eval $shlib_path_name=$SGE_ROOT/lib/$ARCH:$old_value;
#}
#unset ARCH DEFAULTMANPATH MANTYPE shlib_path_name old_value

# extract additionnal parameters (every parameters before the program to launch)
my ($param, $sge_args, $queue, $mpi_cmd) = ('', '', $DEFAULT_QUEUE, '');
while (($param = shift @ARGV) && ($param =~ m/^\-/))
{
    # check if mpi environment variables should be loaded
    if ($param =~ m/^-mpi/)
    {
        #+FIXME: find a better way to fill environment variable, ie. use a dynamic way rather than hard-coded values
        $ENV{MODULESHOME} = '/usr/share/Modules';
        $ENV{MODULEPATH} = '/usr/share/Modules/modulefiles:/etc/modulefiles:';
        $ENV{LOADEDMODULES} = 'compiler/gcc:mpi/openmpi-1.3.4';
        $ENV{COMPILER} = 'gcc';
        $ENV{COMPILER_VER} = '4.1.2';
        $ENV{MPIHOME} = '/opt/cluster/openmpi-1.3.4/gcc-4.1.2';
        $ENV{MPI} = 'openmpi';
        $ENV{MPI_VER} = '1.3.4';
        $mpi_cmd = $MPI_EXEC;
    }
    elsif ($param =~ m/^-sge_args/)
    {
        # see if additionnal SGE arguments where specified
        # -using the -sge_args="... ... ..." syntax
        ($sge_args) = ($param =~ m/^-sge_args="(.*)"$/);
        if (!$sge_args)
        {
            # -using the -sge_args=... syntax
            ($sge_args) = ($param =~ m/^-sge_args=(.*)$/);
        }
        else
        {
            # -using the -sge_args ... syntax
            $sge_args = shift @ARGV;
        }
        #+FIXME: if some args override default args, remove default (using a regexp?)
        #        for instance for user name, project name and so...
    }
}
unshift @ARGV, $param; # put back program name

my ($program, $prog_name, $parameters, $stdin_handle, $stdin_file, $stdout_handle, $stdout_file, $stderr_handle, $stderr_file);
# get command
$program = shift @ARGV;
# get program name for SGE queue
($prog_name) = ($program =~ m/^\s*(?:\S+\/)?([\w\-]{2,8})/);
if ($prog_name)
{
    $prog_name = 'GP' . $prog_name;
}
else
{
    $prog_name = $GREENPHYL_PROJECT;
}

# get and concatenate parameters
$parameters = '';
foreach my $arg (@ARGV)
{
    # escape parameters
    $arg =~ s/([\\"\|<>&])/\\$1/g;
    if ($arg =~ m/ /)
    {
        $parameters .= "\\\"$arg\\\" ";
    }
    else
    {
        $parameters .= $arg . ' ';
    }
}
if ('' ne $parameters)
{
    chop($parameters);
    $parameters = "\"$parameters\"";
}
# print STDERR "PARAMETERS: $parameters\n"; #+debug

# prepare temporary files
# -create files
($stdin_handle, $stdin_file) = tempfile($TEMP_TEMPLATE_IN, SUFFIX => $TEMP_SUFFIX);
($stdout_handle, $stdout_file) = tempfile($TEMP_TEMPLATE_OUT, SUFFIX => $TEMP_SUFFIX);
($stderr_handle, $stderr_file) = tempfile($TEMP_TEMPLATE_ERR, SUFFIX => $TEMP_SUFFIX);
# -close output files
close($stdout_handle);
close($stderr_handle);
# -give R/W access to everyone
chmod 0666, $stdin_file;
chmod 0666, $stdout_file;
chmod 0666, $stderr_file;

#print "FILES: $stdin_file, $stdout_file, $stderr_file\n"; #+debug

# get STDIN content
my $data;
# check if a file content was redirected to STDIN
#  this ensure the script won't wait for user inputs
if (not -t STDIN)
{
    while ($data = <STDIN>)
    {
        print $stdin_handle $data;
    }
}
close($stdin_handle);

# submit the job
my $qsub_cmd = "qsub -q $queue $SGE_DEFAULT_ARGS -N $prog_name -i $stdin_file -o $stdout_file -e $stderr_file $sge_args $mpi_cmd $program $parameters";
#warn "Submitting job to queue $queue...\n"; #+debug
#warn "DEBUG: COMMAND: $qsub_cmd\n"; #+debug
my $qsub_result = `$qsub_cmd`;
my ($job_id) = ($qsub_result =~ m/(\d+)/);

if ($job_id)
{
    warn "SGE JOB ID: $job_id\n";
    # check the queue and wait for the job to end
    #
    # Job status:
    #  d(eletion), E(rror), h(old), r(unning), R(estarted), s(uspended)
    #  S(uspended), t(ransfering), T(hreshold), w(aiting)
    #
    # qstat status codes:
    # 'qw' - Queued and waiting,
    # 'w' - Job waiting,
    # 's' - Job suspended,
    # 't' - Job transferring and about to start,
    # 'r' - Job running,
    # 'h' - Job hold,
    # 'R' - Job restarted,
    # 'd' - Job has been marked for deletion,
    # 'Eqw' - An error occurred with the job.
    #
    my $qstat_result;
    while (($qstat_result = `qstat 2>/dev/null`)
           && ($qstat_result =~ m/^ *$job_id +\S+ +\S+ +\S+ +(qw|r|t) +/om))
    {
        sleep($SLEEP_TIME);
    }

    # check if queue error
    if ($qstat_result =~ m/^ +$job_id +\S+ +\S+ + \S+ +Eqw +/om)
    {
        # remove command from queue
        `qdel $job_id 2>/dev/null`;
        # output an error message on STDERR
        print STDERR "Job execution failed (on SGE).";
    }

}

# outputs STDOUT
open($stdout_handle, "<$stdout_file");
while($data = <$stdout_handle>)
{
    print $data;
}
close($stdout_handle);
# outputs STDERR
open($stderr_handle, "<$stderr_file");
while($data = <$stderr_handle>)
{
    print STDERR $data;
}
close($stderr_handle);

# remove temporary files
unlink($stdout_file);
unlink($stderr_file);
unlink($stdin_file);

# CODE END
###########


=pod

=head1 AUTHORS

Valentin GUIGNON (CIRAD), valentin.guignon@cirad.fr

=head1 VERSION

Version 0.1.5 CIRAD

Date 26/03/2010

=head1 SEE ALSO

qsub, qstat, qdel

=cut
