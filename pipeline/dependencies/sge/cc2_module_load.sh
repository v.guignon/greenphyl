#!/bin/bash
#$ -S /bin/bash
#
# Execute in the current working directory
# #$ -cwd
#
# Job Name and email
# # $ -N GreenPhyl
# # $ -M m.rouard@cgiar.org
# # $ -q greenphyl.q
#
# #$ -pe parallel_smp 1


# Load the appropriate module(s)
if [ $# -lt 2 ]; then
  /bin/echo "Usage: $SCRIPTNAME program parameters..." >&2
  exit 1
fi

case "$1" in
    mafft)
        module load bioinfo/mafft/7.313
        mafft ${@:2}
        ;;
    fftns)
        module load bioinfo/mafft/7.313
        fftns ${@:2}
        ;;
    hmmalign)
        module load bioinfo/hmmer/3.1b2
        hmmalign ${@:2}
        ;;
    hmmbuild)
        module load bioinfo/hmmer/3.1b2
        hmmbuild ${@:2}
        ;;
    trimal)
        module load bioinfo/trimal/1.4
        trimal ${@:2}
        ;;
    phyml)
#        module load bioinfo/PhyML/3.2.20160530_MPI
        module load bioinfo/PhyML/3.2.20160530
        phyml ${@:2}
        ;;
    distmat)
        module load bioinfo/EMBOSS/6.6.0
        distmat ${@:2}
        ;;
    busco)
        module load bioinfo/BUSCO/3.0.2
        run_BUSCO.py ${@:2}
        ;;
    diamond)
        module load bioinfo/diamond/0.9.24
        diamond ${@:2}
        ;;
    fasttree)
        module load bioinfo/fasttree/2.1.11
        FastTree ${@:2}
        ;;
    java)
        module load system/java/jdk6
        java ${@:2}
        ;;
    cdhit)
        module load bioinfo/cd-hit/4.6.4
        cd-hit ${@:2}
        ;;
    *)
        echo "Invalid program '$1'" >&2
        exit 3
        ;;
esac

#
# #$ -V


