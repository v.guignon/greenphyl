#!/usr/bin/perl

=pod

=head1 NAME

cmp_seq_list.pl - Compare the list of sequence of 2 FAST files

=head1 SYNOPSIS

    cmp_seq_list.pl fasta1.fa fasta2.fa

=head1 REQUIRES

Perl5

=head1 DESCRIPTION

Compare the list of sequence of 2 FAST files.

=cut

use strict;
use warnings;
use Carp qw (cluck confess croak);
use Readonly;

use Getopt::Long;
use Pod::Usage;
use Error qw(:try);
use Bio::SeqIO;




# Script options
#################

=pod

=head1 OPTIONS

trans_to_web.pl [-help | -man] <fasta_1> <fasta_2>


=over 4

=item B<-help>:

Prints a brief help message and exits.

=item B<-man>:

Prints the manual page and exits.

=item B<fasta_1>: (string)

.

=item B<fasta_2>: (string)

.

=back

=cut


# CODE START
#############

# options processing
my ($man, $help, $debug, $fasta_1, $fasta_2) = (0, 0, 0, '', '');
# parse options and print usage if there is a syntax error.
GetOptions("help|?"       => \$help,
           "man"          => \$man,
           "debug"        => \$debug,
) or pod2usage(2);
if ($help) {pod2usage(1);}
if ($man) {pod2usage(-verbose => 2);}

$fasta_1 = shift();
$fasta_2 = shift();
if (!$fasta_1 || !$fasta_2)
{
    pod2usage(2);
}

if (!-r $fasta_1)
{
    confess "ERROR: unable to read '$fasta_1'!\n";
}

if (!-r $fasta_2)
{
    confess "ERROR: unable to read '$fasta_2'!\n";
}

my %sequences; # keys are sequence name, values are unused
my (@fasta_1_specific, @fasta_2_specific, @common_set);

my $fasta_1_fh = Bio::SeqIO->newFh(
    '-file'   => $fasta_1,
    '-format' => 'Fasta'
);
# list sequences in a hash
while (<$fasta_1_fh>)
{
    $sequences{$_->display_id} = 1;
}

my $fasta_2_fh = Bio::SeqIO->newFh(
    '-file'   => $fasta_2,
    '-format' => 'Fasta'
);
# list sequences in a hash
while (<$fasta_2_fh>)
{
    if (exists($sequences{$_->display_id}))
    {
        delete($sequences{$_->display_id});
        push(@common_set, $_->display_id);
    }
    else
    {
        push(@fasta_2_specific, $_->display_id);
    }
}
@fasta_1_specific = keys(%sequences);

@fasta_1_specific = sort(@fasta_1_specific);
@fasta_2_specific = sort(@fasta_2_specific);
@common_set       = sort(@common_set);

print "Result:\n";
print "Common set (" . scalar(@common_set) . "):\n" . join(', ', @common_set) . "\n\n";
print "$fasta_1 specific (" . scalar(@fasta_1_specific) . "):\n" . join(', ', @fasta_1_specific) . "\n\n";
print "$fasta_2 specific (" . scalar(@fasta_2_specific) . "):\n" . join(', ', @fasta_2_specific) . "\n\n";


# CODE END
###########


=pod

=head1 AUTHORS

Valentin GUIGNON (CIRAD, Bioversity-France), valentin.guignon@cirad.fr, V.Guignon@cgiar.org
Mathieu ROUARD (Bioversity-France), m.rouard@cgiar.org



=head1 VERSION

Version 1.0.0

Date 23/06/2011

=head1 SEE ALSO

GreenPhyl documentation.

=cut
