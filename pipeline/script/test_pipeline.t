#!/usr/bin/perl

#
# to run the test:
# % perl test_pipeline.t
#
# to create reference dataset:
# % perl test_pipeline.t gen
#

use strict;
use warnings;

use lib '../lib';

use Test::Most;
use Test::Regression;
use Carp qw (cluck confess croak);

BEGIN {
    use_ok( 'GreenPhylConfig' );
}

my $pipeline_cmd             = 'perl run_pipeline.pl';
my $pipeline_multithread_cmd = 'perl run_pipe_in_multithread.pl';

my $DATASET_PATH = "../tests/datasets";
my @DATASET_FILES = ('test.fa', 'test2.fa', 'test3.fa');
my $REGRESSION_PATH = '../tests/regression';

my %greenphyl_commands = (
    'MAFFT'       => $GreenPhylConfig::MAFFT_CMD,
    'MAFFT FFTNS' => $GreenPhylConfig::MAFFT_FFTNS_CMD,
    'HMM Build'   => $GreenPhylConfig::HMMBUILD_CMD,
#    'Al2Co'       => $GreenPhylConfig::AL2CO_CMD,
#    'NormD'       => $GreenPhylConfig::NORMD_CMD,
#    'Rascal'      => $GreenPhylConfig::RASCAL_CMD,
#    'Cut Align'   => $GreenPhylConfig::CUT_ALN_CMD,
    'TrimAl'      => $GreenPhylConfig::TRIMAL_CMD,
#    'GBlocks'     => $GreenPhylConfig::GBLOCKS_CMD,
    'PhyML'       => $GreenPhylConfig::PHYML_CMD,
    'ReTree'      => $GreenPhylConfig::RETREE_CMD,
    'HMM Align'   => $GreenPhylConfig::HMM_ALIGN_CMD,
    'Java'        => $GreenPhylConfig::JAVA_CMD,
);

my %greenphyl_jar_commands = (
    'ReadSeq'     => $GreenPhylConfig::READSEQ_CMD,
    'PhyloXML'    => $GreenPhylConfig::PHYLOXML_CONV_CMD,
    'SDI'         => $GreenPhylConfig::SDI_CMD,
    'SDI R'       => $GreenPhylConfig::SDI_R_CMD,
    'RIO'         => $GreenPhylConfig::DORIO_CMD,
    'RAP'         => $GreenPhylConfig::RAP_CMD,
);


sub check_executable
{
    my $executable_file_name = shift();
    
    # check if the command has been set
    if (!$executable_file_name)
    {
        return 0;
    }

    # remove qrun.pl part from command line
    $executable_file_name =~ s/\Q$GreenPhylConfig::QRUN_PATH\E +(?:\-sge_args=".*")?//;
    ($executable_file_name) = ($executable_file_name =~ m/\s*(\S+)\s*/);
    # check if a valid name was set
    if ($executable_file_name)
    {
        print STDERR " Testing '$executable_file_name'";
        if (-x $executable_file_name)
        {
            print STDERR "\n";
            return 1;
        }
        # file not found, check if in the path
        print STDERR " in path\n";
        $executable_file_name = `which $executable_file_name 2>/dev/null`;
        chomp($executable_file_name);
        return (-x $executable_file_name);
    }
    else
    {
        # executable not set
        return 0;
    }
}

sub check_jar
{
    my $jar_file_name = shift();

    if (!$jar_file_name)
    {
        # not set!
        return 0;
    }

    # extract JAR portion
    ($jar_file_name) = ($jar_file_name =~ m/(\S+\.jar)/);
    print STDERR " Testing '$jar_file_name' JAR\n";
    return ($jar_file_name && (-r $jar_file_name));
}

# if the parameter 'gen' is first on the command line, the regression files are
# not checked against output, but instead generated from output
if ($ARGV[0] && ($ARGV[0] eq 'gen'))
{
    $ENV{TEST_REGRESSION_GEN} = 1;
}



# check config
print "\n* Check config\n";
ok(defined($GreenPhylConfig::GREENPHYL_PATH), 'Greenphyl path defined');
ok(-d $GreenPhylConfig::GREENPHYL_PATH, 'Greenphyl exists and is a directory');

ok(defined($GreenPhylConfig::QRUN_PATH), 'QRun defined');
ok(-x $GreenPhylConfig::GREENPHYL_PATH, 'QRun is executable');

ok(defined($GreenPhylConfig::TOOLS_PATH), 'Tools path defined');
ok(-d $GreenPhylConfig::TOOLS_PATH, 'Tools path is a directory');

ok(defined($GreenPhylConfig::TREE_OF_LIFE), 'Tree of life defined');
ok(-r $GreenPhylConfig::TREE_OF_LIFE, 'Tree of life is readable');

foreach my $greenphyl_command (keys(%greenphyl_commands))
{
    print "\n";
    ok(defined($greenphyl_commands{$greenphyl_command}), $greenphyl_command . ' command defined');
    ok(check_executable($greenphyl_commands{$greenphyl_command}), $greenphyl_command . ' can be executed');
}

foreach my $greenphyl_jar_command (keys(%greenphyl_jar_commands))
{
    print "\n";
    ok(defined($greenphyl_jar_commands{$greenphyl_jar_command}), $greenphyl_jar_command . ' JAR command defined');
    ok(check_jar($greenphyl_jar_commands{$greenphyl_jar_command}), $greenphyl_jar_command . ' JAR can be run');
}


# check test dataset
print "\n* Check test dataset\n";
foreach my $data_test_file (@DATASET_FILES)
{
    ok(-r $DATASET_PATH . '/' . $data_test_file, $data_test_file . ' test file exists and is readable');
}
if (!-e $REGRESSION_PATH)
{
    mkdir($REGRESSION_PATH, 0755);
}
ok(-d $REGRESSION_PATH && -x $REGRESSION_PATH && -r $REGRESSION_PATH, 'regression data directory is available');


# check output directory
print "\n* Check output directory";
if ($GreenPhylConfig::OUTPUT_PATH)
{
    print " '$GreenPhylConfig::OUTPUT_PATH'";
}
print "\n";
ok(defined($GreenPhylConfig::OUTPUT_PATH), 'Output path defined');
ok(-d $GreenPhylConfig::OUTPUT_PATH, 'Output path exists and is a directory');
ok(-w $GreenPhylConfig::OUTPUT_PATH, 'Output path is writable');
if (!-w $GreenPhylConfig::OUTPUT_PATH)
{
    confess "ERROR: unable to run more tests: output directory did not pass tests!\n";
}

# check run_pipeline.pl
print "\n* Check single-threaded pipeline (run_pipeline.pl)\n";

# -help display
my $command_line = "$pipeline_cmd -help >/dev/null";
ok(0 == system($command_line), 'Pipeline can be launched');

# prepare output directory (remove previous tests)
if (system("rm -rf $GreenPhylConfig::OUTPUT_PATH/autotest1"))
{
    warn "WARNING: failed to clear previous test! Some test results may not be correct!\n";
}


# -basic execution
print " Basic execution (this may take several minutes)\nTo check status, run 'tail -f $GreenPhylConfig::OUTPUT_PATH/autotest1.log'\n";
$command_line = "$pipeline_cmd -f autotest1 -i $DATASET_PATH/" . $DATASET_FILES[0] . " >$GreenPhylConfig::OUTPUT_PATH/autotest1.log 2>&1";
ok(0 == system($command_line), 'Basic execution');

my $test_fh;

# Pipeline
open($test_fh, "$GreenPhylConfig::OUTPUT_PATH/autotest1.log") or confess "Failed to open log file!\n$!\n";
# does not take in account header
ok_regression(sub { my $test_data = join('', <$test_fh>); $test_data =~ s/.*Start working on .*?autotest1\///s; return $test_data;}, "$REGRESSION_PATH/autotest1.log", 'Pipeline autotest1 log output');
close($test_fh);

# MAFFT
open($test_fh, "$GreenPhylConfig::OUTPUT_PATH/autotest1/autotest1.mafft") or confess "Failed to open output file!\n$!\n";
ok_regression(sub { my $test_data = join('', <$test_fh>); return $test_data;}, "$REGRESSION_PATH/autotest1.mafft", 'MAFFT autotest1 output');
close($test_fh);

# HMM
open($test_fh, "$GreenPhylConfig::OUTPUT_PATH/autotest1/autotest1.hmm") or confess "Failed to open output file!\n$!\n";
# remove DATE line and path of output file
ok_regression(sub { my $test_data = join('', <$test_fh>); $test_data =~ s/DATE.*$//m; $test_data =~ s/hmmbuild .*?\/autotest1/hmmbuild autotest1/m; return $test_data;}, "$REGRESSION_PATH/autotest1.hmm", 'HMM autotest1 output');
close($test_fh);

# Trimal .phy
open($test_fh, "$GreenPhylConfig::OUTPUT_PATH/autotest1/autotest1.phy") or confess "Failed to open output file!\n$!\n";
ok_regression(sub { my $test_data = join('', <$test_fh>); return $test_data;}, "$REGRESSION_PATH/autotest1.phy", 'Trimal autotest1 output');
close($test_fh);
#  Trimal .2.mask
open($test_fh, "$GreenPhylConfig::OUTPUT_PATH/autotest1/autotest1.2.mask") or confess "Failed to open output file!\n$!\n";
ok_regression(sub { my $test_data = join('', <$test_fh>); return $test_data;}, "$REGRESSION_PATH/autotest1.2.mask", 'Trimal masked FASTA autotest1 output');
close($test_fh);
#  Trimal .2_masking.html
open($test_fh, "$GreenPhylConfig::OUTPUT_PATH/autotest1/autotest1.2_masking.html") or confess "Failed to open output file!\n$!\n";
ok_regression(sub { my $test_data = join('', <$test_fh>); return $test_data;}, "$REGRESSION_PATH/autotest1.2_masking.html", 'Trimal masked HTML autotest1 output');
close($test_fh);
#  Trimal _filtered.html
open($test_fh, "$GreenPhylConfig::OUTPUT_PATH/autotest1/autotest1_filtered.html") or confess "Failed to open output file!\n$!\n";
ok_regression(sub { my $test_data = join('', <$test_fh>); return $test_data;}, "$REGRESSION_PATH/autotest1_filtered.html", 'Trimal filtered HTML autotest1 output');
close($test_fh);

# PhyML
open($test_fh, "$GreenPhylConfig::OUTPUT_PATH/autotest1/autotest1_phylogeny_tree.nwk") or confess "Failed to open output file!\n$!\n";
# does not take in account branch length and support
ok_regression(sub { my $test_data = join('', <$test_fh>); $test_data =~ s/(?:\d+\.\d+)?:\d+\.\d+//g; return $test_data;}, "$REGRESSION_PATH/autotest1_phylogeny_tree.nwk", 'PhyML autotest1 output');
close($test_fh);

# RAP
open($test_fh, "$GreenPhylConfig::OUTPUT_PATH/autotest1/autotest1_rap_gene_tree.nwk") or confess "Failed to open output file!\n$!\n";
# does not take in account branch length and support
ok_regression(sub { my $test_data = join('', <$test_fh>); $test_data =~ s/(?:\d+\.\d+)?:\d+\.\d+//g; return $test_data;}, "$REGRESSION_PATH/autotest1_rap_gene_tree.nwk", 'RAP autotest1 output 1');
close($test_fh);
open($test_fh, "$GreenPhylConfig::OUTPUT_PATH/autotest1/autotest1_rap_reconcilied_gene_tree.nwk") or confess "Failed to open output file!\n$!\n";
ok_regression(sub { my $test_data = join('', <$test_fh>); return $test_data;}, "$REGRESSION_PATH/autotest1_rap_reconcilied_gene_tree.nwk", 'RAP autotest1 output 2');
close($test_fh);
open($test_fh, "$GreenPhylConfig::OUTPUT_PATH/autotest1/autotest1_rap_tree.xml") or confess "Failed to open output file!\n$!\n";
# remove <branch_length> and <confidence> blocks
ok_regression(sub { my $test_data = join('', <$test_fh>); $test_data =~ s/<branch_length.*?\/branch_length>//gs; $test_data =~ s/<confidence.*?\/confidence>//gs; return $test_data;}, "$REGRESSION_PATH/autotest1_rap_tree.xml", 'RAP autotest1 output 3');
close($test_fh);


# perl run_pipeline.pl -f autotest1 -i ../tests/datasets/test.fa
# 
# -�tape par �tape
# perl run_pipeline.pl -f autotest2 -i ../tests/datasets/test.fa -r alignment -e alignment
# perl run_pipeline.pl -f autotest2 -i ../tests/datasets/test.fa -r masking -e masking
# perl run_pipeline.pl -f autotest2 -i ../tests/datasets/test.fa -r phylogeny -e phylogeny
# perl run_pipeline.pl -f autotest2 -i ../tests/datasets/test.fa -r rooting -e rooting
# 
# -avec SDI+RIO
# perl run_pipeline.pl -f autotest2 -i ../tests/datasets/test.fa -r phylogeny -e phylogeny -norap
# perl run_pipeline.pl -f autotest2 -i ../tests/datasets/test.fa -r rooting -e rooting -norap
# perl run_pipeline.pl -f autotest2 -i ../tests/datasets/test.fa -r rio -e rio -norap
# 
# -Probl�me avec le masking
# perl run_pipeline.pl -f autotest3 -i ../tests/datasets/test_masking.fa -skip_bad_masking
# 
# -Choix du r�pertoire de sortie:
# perl run_pipeline.pl -f autotest4 -i ../tests/datasets/test.fa -output_dir ../temp/auto_test
# 
# -overwrite automatique
# perl run_pipeline.pl -f autotest5 -i ../tests/datasets/test.fa
# 
# -Auto-resume
# perl run_pipeline.pl -f autotest6 -i ../tests/datasets/test.fa -autoresume
# *supprimer l'alignement doit tout recalculer
# *supprimer le masking doit tout recalculer sauf l'alignement
# *supprimer la phylog�nie doit tout recalculer � partir de la phylo
# *supprimer les sorties RAP ne doivent que recalculer RAP
# 
# -Auto-resume depuis une �tape
# *alignement et masking supprim�s, phylog�nie laiss�e
# perl run_pipeline.pl -f autotest7 -i ../tests/datasets/test.fa -autoresume -r rooting
# 
# 
# run_pipe_multithread
#######################
# -affichage de l'aide
# perl run_pipe_in_multithread.pl -help
# 
# -ex�cution sur un r�pertoire avec 3 fichiers sur 2 slots
# perl run_pipe_in_multithread.pl -dir ../tests/datasets/ -s 2 -test
# 
# -ex�cution avec 2 fichiers sur 4 slots
# perl run_pipe_in_multithread.pl -fid ../tests/datasets/test.fa ../tests/datasets/test2.fa -s 4 -test
# 
# -ex�cution avec 3 fichiers dans l'ordre inverse de l'ordre par d�faut (d�faut = -reverse)
# perl run_pipe_in_multithread.pl -dir ../tests/datasets/ -s 3 -noreverse -test
# 
# -ex�cution avec 1 fichier qui a d�j� tourn�: no overwrite
# perl run_pipe_in_multithread.pl -fid ../tests/datasets/test.fa -s 1 -test
# 
# -ex�cution avec 1 fichier qui a d�j� tourn�: overwrite
# perl run_pipe_in_multithread.pl -fid ../tests/datasets/test.fa -s 1 -overwrite -test
# 
# -ex�cution avec 1 fichiers dont 1 a d�j� tourn�: autoresume
# perl run_pipe_in_multithread.pl  -fid ../tests/datasets/test.fa -s 1 -autoresume -test
# 
# -ex�cution sur un r�pertoire avec 3 fichiers sur 3 slots avec limitations
#  *1 seul fichier:
#     perl run_pipe_in_multithread.pl -dir ../tests/datasets/ -s 3 -test -limitations test1.fa
#  *les 2 premiers fichiers
#     perl run_pipe_in_multithread.pl -dir ../tests/datasets/ -s 3 -test -limitations test1.fa-test2.fa
#  *les 2 derniers fichiers
#     perl run_pipe_in_multithread.pl -dir ../tests/datasets/ -s 3 -test -limitations test2.fa-test3.fa
#  *le 2�me fichier seulement    
#     perl run_pipe_in_multithread.pl -dir ../tests/datasets/ -s 3 -test -limitations start=test2.fa end=test2.fa
#  *le premier et le dernier fichier
#     perl run_pipe_in_multithread.pl -dir ../tests/datasets/ -s 3 -test -limitations test1.fa -limitations test3.fa
#  *limitation dans le temps
#     perl run_pipe_in_multithread.pl -dir ../tests/datasets/ -s 1 -test -limitations time=0.1
#  *limitation en nombre de s�quences
#     perl run_pipe_in_multithread.pl -dir ../tests/datasets/ -s 3 -test -limitations exact=50
#     perl run_pipe_in_multithread.pl -dir ../tests/datasets/ -s 3 -test -limitations less=50
#     perl run_pipe_in_multithread.pl -dir ../tests/datasets/ -s 3 -test -limitations more=50

# remove test files
#+++
#unlink("$TEST_LOG_NAME$Greenphyl::Log::LOG_FILE_EXTENSION");

done_testing();
