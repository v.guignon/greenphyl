#!/usr/bin/env perl
use strict;
use warnings;

use FindBin;
use lib "$FindBin::Bin/../../website/lib";
use lib "$FindBin::Bin/../../website/local_lib";
use Carp qw (cluck confess croak);

use Getopt::Long;
use Pod::Usage;

use Bio::SeqIO;


my @species_codes = (
  'IPOTF', 'CAPAN', 'TRITU', 'ZEAMA', 'GLYMA', 'VITVI', 'MEDTR', 'MALDO',
  'CUCSA', 'BRARR', 'BRANA', 'SORBI', 'THECC', 'CICAR', 'COCNU', 'BRAOL',
  'MUSAC', 'ORYSA', 'BRADI',
);


my ($man, $help, $debug) = (0, 0, undef);
my (%new_names, $fh, $species_code, $old_species_path, $species_path, $mcl_path);
# options processing...
GetOptions('help|?'              => \$help,
           'man'                 => \$man,
           'debug:s'             => \$debug,
           'species-code=s'      => \$species_code,
           'old-species-path=s'  => \$old_species_path,
           'species-path=s'      => \$species_path,
           'mcl-path=s'          => \$mcl_path,
) or pod2usage(1);
if ($help) {pod2usage('-verbose' => 1, '-exitval' => 0);}
if ($man) {pod2usage('-verbose' => 2, '-exitval' => 0);}

if (!$species_code || !$old_species_path  || !$species_path || !$mcl_path)
{
    pod2usage('-verbose' => 1, '-exitval' => 1);
}


# Parse old '_seq_map.txt' file.
my $old_seq_map_path = $old_species_path . '/' . $species_code . '_seq_map.txt';
my %old_mapping;
open($fh, $old_seq_map_path)
    or confess "Can't open file '$old_seq_map_path': $!";
print "Working on old associations...\n";
while (my $line = <$fh>)
{
    chomp($line);
    my @seqs = split("\t", $line);
    print $seqs[0] . "\n";
    if ($seqs[$#seqs] eq 'matched')
    {
        $old_mapping{$seqs[0]} = $seqs[0];
    }
    elsif ($seqs[$#seqs] eq 'orphan')
    {
    }
    else
    {
        $old_mapping{$seqs[0]} = $seqs[$#seqs];
    }
}
close($fh);

print "\n\nWorking on new associations...\n";
my $new_seq_map_path = $species_path . '/' . $species_code . '_seq_map.txt';
my %old_to_new_mapping;
open($fh, $new_seq_map_path)
    or confess "Can't open file '$new_seq_map_path': $!";
while (my $line = <$fh>)
{
    chomp($line);
    my @seqs = split("\t", $line);
    print $seqs[0] . "\n";
    if ($seqs[$#seqs] ne 'orphan')
    {
        $old_to_new_mapping{$old_mapping{$seqs[0]}} = $seqs[$#seqs];
    }
}
close($fh);

=pod

Old version:
Mazeb_scaffold21386_t000010     Mazeb_scaffold22132_t000010     musac_consensus_p030220
Mazeb_scaffold22132_t000010     musac_consensus_p030220
Mazeb_scaffold55174_t000010     Mazeb_scaffold11328_t000010     musac_representative_p006956
Mazeb_scaffold11328_t000010     musac_representative_p006956
Mazeb_scaffold9942_t000010      matched
Mazeb_scaffold996_t000050       orphan

New version:
Mazeb_scaffold21386_t000010     Mazeb_scaffold22132_t000010     musac_pan_p030220
Mazeb_scaffold22132_t000010     musac_pan_p030220
Mazeb_scaffold55174_t000010     Mazeb_scaffold11328_t000010     musac_pan_p006956
Mazeb_scaffold11328_t000010     musac_pan_p006956
Mazeb_scaffold9942_t000010      matched                         musac_pan_p012967
Mazeb_scaffold996_t000050       orphan

=cut

foreach my $mcl_file ('I12', 'I20', 'I30', 'I50')
{
    my $mcl_file_path = $mcl_path . '/dump.out.renamed.ALL_vs_ALL.m8.' . $mcl_file;
    print "Read current $mcl_file_path for update\n";
    open($fh, $mcl_file_path)
        or confess "Can't open file '$mcl_file_path': $!";
    my $data = do { local $/; <$fh> };
    close($fh);

    my @sorted_keys = sort { $b cmp $a } (keys(%old_to_new_mapping));
    foreach my $old_name (@sorted_keys)
    {
        $data =~ s/$old_name/$old_to_new_mapping{$old_name}/s;
    }

    print "Saving $mcl_file_path changes\n";
    my $fho;
    open($fho, ">$mcl_file_path")
        or confess "Can't open file '$mcl_file_path': $!";
    print {$fho} $data;
    close($fho);
}
