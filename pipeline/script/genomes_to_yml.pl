#!/usr/bin/env perl

=pod

=head1 NAME

genomes_to_yml.pl - Generate species YML files.

=head1 SYNOPSIS

    genomes_to_yml.pl

=head1 REQUIRES

Perl5

=head1 DESCRIPTION

Generate species YML files.

=cut

use strict;
use warnings;

use FindBin;
use lib "$FindBin::Bin/../../website/lib";
use lib "$FindBin::Bin/../../website/local_lib";
use Carp qw (cluck confess croak);

use Greenphyl;
use GreenPhylConfig;
use Greenphyl::Log('nolog' => 1,);

use YAML::XS qw(Dump DumpFile);

use Getopt::Long;
use Pod::Usage;

++$|; #no buffering




# Script global constants
##########################

=pod

=head1 CONSTANTS

B<$DEBUG>: (boolean)

When set to true, it enables debug mode.

=cut

our $DEBUG = 0;




# Script options
#################

=pod

=head1 OPTIONS

#--- describes parameters given to the script
#+++ command line syntax
#--- requirement of the option and its parameter can be:
#--- required: name or nature inside <>
#--- optional: name or nature inside []
#--- alternative between 2 elements: elements separated by a |

=head2 Parameters

=over 4

#+++=item B<-help>:
#+++
#+++Prints a brief help message and exits.
#+++
#+++=item B<-man>:
#+++
#+++Prints the manual page and exits.
#+++
#+++=item B<-debug> (integer):
#+++
#+++Executes the script in debug mode. The integer value is optional and is used
#+++to set debug level (use 0 to force disable debug mode).
#+++Default: 0 (not in debug mode).
#+++

=item B<[option_name]> ([option nature]): #+++

[option description]. #+++
Default: [option default value if one] #+++

#--- remove if log not used
#+++=item B<-log>:
#+++
#+++Enable logging.
#+++
#+++=item B<-log-*> (any):
#+++
#+++Logging arguments.

=back
#--- Example:
#---
#--- Template.pl [-help | -man]
#---
#--- Template.pl [-debug [debug_level]] [-size <width> [height]]
#---
#--- =over 4
#---
#--- =item B<-help>:
#---
#--- Prints a brief help message and exits.
#---
#--- =item B<-man>:
#---
#--- Prints the manual page and exits.
#---
#--- =item B<-debug> (integer):
#---
#--- Executes the script in debug mode. If an integer value is specified, it will
#--- be the debug level. If "-debug" option was used without specifying a debug
#--- level, level 1 is assumed.
#--- Default: 0 (not in debug mode).
#---
#---=item B<-size> (positive_real) (positive_real):
#---
#--- Set the dimensions of the object that will be drawn. The first value is
#--- the width; the height is the second value if specified, otherwise it will
#--- assume height and width are equal to the first value.
#--- Default: width and height are set to 1.
#---
#---=back

=cut


# CODE START
#############

# for regression tests
if ($ENV{'TESTING'} && $ENV{'TESTING_NO_RUN'})
{return 1;}

# options processing
my ($man, $help, $debug, $no_prompt) = (0, 0, undef, undef);

# parse options and print usage if there is a syntax error.
GetOptions('help|?'            => \$help,
           'man'               => \$man,
           'debug:s'           => \$debug,
           'q|noprompt'        => \$no_prompt,
           @Greenphyl::Log::LOG_GETOPT, # skip log parameters
) or pod2usage(1);
if ($help) {pod2usage('-verbose' => 1, '-exitval' => 0);}
if ($man) {pod2usage('-verbose' => 2, '-exitval' => 0);}

# change debug mode if requested/forced
if (defined($debug))
{
    # just '-debug' ?
    if ($debug eq '')
    {
        $debug = 1;
    }
    $DEBUG = $debug;
}

my %species_data;
my $species_tsv_file = $GreenPhylConfig::SPECIES_PATH . 'species.tsv';
my $fh;
if (!open($fh, $species_tsv_file))
{
    confess LogError("Failed to open species TSV file '$species_tsv_file'! $!");
}
my ($code, $organism, $common_name, $taxonomy_id, $color, $genome, $ploidy,
  $chromosome_count, $genome_size, $busco, $version, $version_notes,
  $url_institute, $url_fasta);
my $current_code;
my $line_number = 0;
while (defined(my $line = <$fh>))
{
    ++$line_number;
    chomp($line);
    ($code, $organism, $common_name, $taxonomy_id, $color, $genome, $ploidy,
    $chromosome_count, $genome_size, $busco, $version, $version_notes,
    $url_institute, $url_fasta) = split(/\t/, $line);
    if ($code)
    {
        $current_code = $code;
        if (exists($species_data{$current_code}))
        {
            confess LogError("Species code already used at line $line_number!");
        }
        $color =~ s/^(\d)/#$1/;
        $species_data{$current_code} = {
            'species' =>
            {
                'code' => $current_code,
                'organism' => $organism,
                'common_name' => $common_name,
                'taxonomy_id' => int($taxonomy_id),
                'url_picture' => '/img/species/' . lc($current_code) . '.png',
                'display_order' => 1,
                'color' => $color,
            },
            'genomes' => [],
        };
    }
    elsif (!$current_code)
    {
        confess LogError("Invalid data (missing species code at line $line_number)!");
    }
    elsif (!$code && !exists($species_data{$current_code}->{'pangene'}))
    {
        $species_data{$current_code}->{'pangene'} =
        {
            'path' => "$GreenPhylConfig::PANGENOMES_PATH$current_code/"
        };
    }

    
    if (exists($species_data{$current_code}->{$genome}))
    {
        confess LogError("Invalid data (duplicate genome '$genome' at line $line_number)!");
    }
    
    push(@{$species_data{$current_code}->{'genomes'}}, $genome);
    my $genome_fasta = "$GreenPhylConfig::GENOMES_PATH$current_code/$genome.faa";
    if (!-e $genome_fasta)
    {
        LogWarning("Missing genome FASTA file '$genome_fasta'!");
    }

    my ($complete_busco, $fragment_busco, $missing_busco) = ($busco =~ m/C:([\d\.]+)%[S:[\d\.]+%,D:[\d\.]+%],F:([\d\.]+)%,M:([\d\.]+)%,n:/);

    $species_data{$current_code}->{$genome} = {
        'fasta' => $genome_fasta,
        'chromosome_count' => int($chromosome_count),
        'ploidy' => int($ploidy),
        'genome_size' => int($genome_size),
        'complete_busco' => 1. * $complete_busco,
        'fragment_busco' => 1. * $fragment_busco,
        'missing_busco' => 1. * $missing_busco,
        'url_institute' => $url_institute,
        'url_fasta' => $url_fasta,
        'version' => $version,
        'version_notes' => $version_notes,
    };

}
close($fh);

foreach $code (keys(%species_data))
{
    my $species_yml_file = $GreenPhylConfig::SPECIES_PATH . $code . '.yml';
    if (-e $species_yml_file)
    {
        confess LogError("Ouptut species YML file '$species_yml_file' already exists!");
    }

    if (!open($fh, ">$species_yml_file"))
    {
        confess LogError("Failed to open species YML file '$species_yml_file'! $!");
    }
    print {$fh} substr(Dump({'species' => $species_data{$code}->{'species'}}), 4) . "\n";
    my @genomes = sort @{$species_data{$code}->{'genomes'}};
    print {$fh} substr(Dump({'genomes' => \@genomes}), 4) . "\n";

    if (exists($species_data{$code}->{'pangene'}))
    {
        print {$fh} substr(Dump({'pangene' => $species_data{$code}->{'pangene'}}), 4) . "\n";
        # Add symlinks.
        print "ln -s  $species_data{$code}->{'pangene'}->{'path'}${code}_complete.fasta  $GreenPhylConfig::SPECIES_PATH$code.faa\n";
    }
    else
    {
        print "ln -s  "
         . $species_data{$code}->{$genomes[0]}->{'fasta'}
         . "  $GreenPhylConfig::SPECIES_PATH$code.faa\n";
    }
    
    foreach $genome (@genomes)
    {
        print {$fh} substr(Dump({$genome => $species_data{$code}->{$genome}}), 4) . "\n";
    }
    
    close($fh);
    
    
    # DumpFile($species_yml_file, $species_data{$code});
}


exit(0);

# CODE END
###########


=pod

=head1 AUTHORS

Mathieu ROUARD (Bioversity), m.rouard@cgiar.org

Valentin GUIGNON (Bioversity), v.guignon@cgiar.org

=head1 VERSION

Version 1.0.0

Date 11/12/2019

=head1 SEE ALSO

GreenPhyl documentation.

=cut
