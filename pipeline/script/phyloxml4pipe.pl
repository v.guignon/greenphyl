#!/usr/bin/perl

=pod

=head1 NAME

phyloxml4pipe.pl 
 
=head1 SYNOPSIS

    

=head1 REQUIRES

Perl5

=head1 DESCRIPTION

extract homologous score data from Rio, load into the db and create a phyloxml file 
with relevant information.


=cut


use strict;
use warnings;
use Getopt::Long;
use Carp qw (cluck confess croak);
use Bio::TreeIO;
use Data::Dumper;
use File::Basename;

use lib '../../lib';
use Greenphyl::Web::Greenphyl;
use Greenphyl::ConnectMysql;

my $gp          = Greenphyl::Web::Greenphyl->new;
my $mysql       = $gp->{mysql};
my $dbh         = $gp->{mysql}->{dbh};
  
my $FIRST_COLUMN_WIDTH  = 30;
my $INDENTATION         = 4; 
 
my ($input, $load);
our $debug_g = 0;

GetOptions(
    'debug'         => \$debug_g,
    'file|i=s'      => \$input,
    'load|l'        => \$load,
);

# options checking
if (not defined $input)
{
    printError("No input specified");
}
elsif (!-e $input)
{
    printError("file does not exists! [$input]");
}


my ($filename, $directories) = fileparse($input);

printDebug("Filename  : " . $filename );
printDebug("Directory : " . $directories); 

 
my $treeio = Bio::TreeIO->new(
		   -format => 'phyloxml',
		   -file   => "$input");
 

my @list = glob($directories . 'rio_*.txt'); 

#print Dumper @list;exit;

my %orthologs;
foreach my $rio_file (@list)
{
    printDebug('text file parsed:' . $rio_file);
    
    open (IN, $rio_file) or die $!;
    $rio_file =~ /rio_([\w\.-]+).txt$/;
    my $ref_seq = $1;
    
    my $sql0 = "SELECT seq_id FROM sequences WHERE seq_textid = '$ref_seq'";
    my ($ref_seqid) = $mysql->select($sql0);
    
#    ## search IPR domains
#    my $sql_domain = "
#            SELECT s.seq_textid, s.seq_length, sh.ipr_start, sh.ipr_stop, i.ipr_code, sh.ipr_score  
#            FROM sequences s LEFT JOIN seq_has_an sh ON s.seq_id = sh.seq_id LEFT JOIN ipr i ON sh.ipr_id = i.ipr_id 
#            WHERE s.seq_id = ?
#        ";
#        
#    my $ref2 = $dbh->selectall_arrayref( $sql_domain, undef, $ref_seqid );
# 
#   foreach (@{$ref2})
#   {
#            $orthologs{$_->[0]}{length} = $_->[1];
#            push @{$orthologs{$_->[0]}{domains}}, [$_->[2], $_->[3], $_->[4], $_->[5]];          
#   }
    
    # read text file for scores
    while(<IN>)
    {
        if ($_ =~ /^([\w\.-]+)\s+(\d{1,3})/) # Os06g45330.1		83	-	-	
        { 
            my $sequence        = $1;
            my $orthology_score = $2;
            if ($orthology_score > 30)
            {
                $orthologs{$ref_seq}{$sequence} = $orthology_score;
            }	  
      	 }
      	      
        if ($load) 
        {
          	# Load score into DB
          	if ($_ =~ /^([\w\.-]+)\s+(\d{1,3})\s+(\d{1,3})\s+(\d{1,3})\s+(\S+)/) # Os06g45330.1		83	20	83 5.234	
            { 
                my $sequence        = $1;
                my $orthology_score = $2;
                my $subtree         = $3;
                my $superortholog   = $4;
                my $distance        = $5;
                my $sql = "SELECT seq_id FROM sequences WHERE seq_textid = '$sequence'";
        	    my ($seq_id) = $mysql->select($sql);
        	            
                if ($orthology_score > 30)
                {
          	        my $sql1 = "INSERT INTO scores(query_id, subject_id, type_id, score) VALUES('$ref_seqid', '$seq_id', '1','$orthology_score')";
          	        printDebug('orthology_score:' . $sql1 );
                    $mysql->insert($sql1);	
                }
                if ($subtree)
                {
          	        my $sql2 = "INSERT INTO scores(query_id, subject_id, type_id, score) VALUES('$ref_seqid', '$seq_id', '2','$subtree')";
          	        printDebug('subtree:' . $sql2 );
                    $mysql->insert($sql2);	
                }
                if ($superortholog)
                {
          	        my $sql3 = "INSERT INTO scores(query_id, subject_id, type_id, score) VALUES('$ref_seqid', '$seq_id', '3','$superortholog')";
          	        printDebug('superortholog:' . $sql3);
                    $mysql->insert($sql3);	
                }
                 if ($distance)
                {
          	        my $sql4 = "INSERT INTO scores(query_id, subject_id, type_id, score) VALUES('$ref_seqid', '$seq_id', '4','$distance')";
          	        printDebug('Distance:' . $sql4);
                    $mysql->insert($sql4);	
                }
            }
        }	     	 
    }
    close IN;
}

#print Dumper %orthologs;exit;

my $tree   = $treeio->next_tree;
my @nodes  = $tree->get_nodes;

my %sequences_seen;
foreach my $n (@nodes) {
  #print $n->id;
  
  if ($n->sequence) 
  {
       # get sequence object for the node
       my ($seq) = @{$n->sequence};
     
       # or get annotation using path
       my ($name) = $treeio->read_annotation('-obj'=>$seq, '-path'=>'name');
       #print $name2, "\n"; 
   
   	#keep track sequence ids of the tree
   	$sequences_seen{$name}++;
   	
       # set id_source. this is mandatory to link with sequence relation
       $treeio->add_attribute(
               '-obj'=>$seq,
               '-attr'=>"id_source = \"$name\"",
             );
                 
      
      # if sequence id and domain info exist
      # add IPR domains info
#      if ((defined($orthologs{$name}) || ($name eq $sequence) )
#           and defined $orthologs{$name}->{domains}) 
#      {
#           my $length 		= $orthologs{$name}{length};
#         my ($start,  $end, $confidence, $ipr_name, $str);
#     
#         my $all_domains = $orthologs{$name}->{domains};
#         foreach my $elt (@{$all_domains})
#         {
#           $start		= $elt->[0];
#           $end		    = $elt->[1];
#           $ipr_name 	= $elt->[2];
#           $confidence	= $elt->[3];
#           $confidence	||= "1"; #phyloxml does not like empty strings
#     
#           $str .= qq|<domain from="$start" to="$end" confidence="$confidence">$ipr_name</domain>|;
#         }
#   
#         $treeio->add_phyloXML_annotation(
#           '-obj'=>$seq,
#           '-xml'=>qq{<domain_architecture length="$length">$str</domain_architecture>}
#           );
#      }
      ########                    
  }
}


# generate XML for sequence relation.
my %relation_seen;
foreach my $keys (keys %orthologs)
{
  
  if (defined $sequences_seen{$keys})
  {
    #print $keys ."\n";
    foreach my $keys2 ( keys %{$orthologs{$keys}})
    {
        #print "$keys \t $keys2 \n";
        if (! $relation_seen{$keys2}{$keys})
        {
            $relation_seen{$keys}{$keys2}++;
        
	        $treeio->add_phyloXML_annotation(
                '-obj'=>$tree,
		        '-xml'=>qq{<sequence_relation id_ref_0="$keys" id_ref_1="$keys2" type="orthology"><confidence type="rio">$orthologs{$keys}{$keys2}</confidence></sequence_relation>}
            );
        }
    }
  }
}

printStageStart("Phyloxml file writing");

$filename =~ s/sdi/rio/;
# write_tree
my $output = $directories . $filename;
my $treeout = Bio::TreeIO->new(
		-format => 'phyloxml',
		-file   => ">$output");
$treeout->write_tree($tree);

if (-e $output)
{
    printStageEnd("$output created");
}
else
{
    printStageEnd("Failed!");
}

#printStageStart("tranfert");
#system "cp $output /apps/GreenPhyl/v2/htdocs/img/trees/"; 

#$filename =~ /^(\d+)_/;
#system "cp $directories/$1.mafft /apps/GreenPhyl/v2/htdocs/img/aln/";
#printStageEnd("Done");





# Script global functions
##########################

       
=pod

=head1 FUNCTIONS

=head2 printWarning

B<Description>: display a warning message on STDERR.

B<ArgsCount>: 1

=over 4

=item $message: (string) (R)

warning message to display.

=back

B<Example>:

    printWarning("the file $filename has been replaced!\nYou have been warned!");

=cut

sub printWarning
{
    my ($message) = @_;
    # check arguments
    if (1 != @_)
    {
        confess "usage: printWarning(message);";
    }

    # remove trailing invisible characters
    $message =~ s/\s+$//s;
    # check for multi-lines warning
    if ($message =~ m/[\r\n]/)
    {
        # multi-lines
        my @message_lines = split(/(?:\n|\r\n|\r)/, $message);
        print STDERR "WARNING: " . shift(@message_lines) . "\n         ";
        print STDERR join("\n         ", @message_lines);
        print STDERR "\n";
    }
    else
    {
        # single line
        print STDERR "WARNING: " . $message . "\n";
    }
    return;
}




=pod

=head2 printError

B<Description>: display an error message on STDERR.

B<ArgsCount>: 1

=over 4

=item $message: (string) (R)

error message to display.

=back

B<Example>:

    printError("Computation failed!");

=cut

sub printError
{
    my ($message) = @_;
    # check arguments
    if (1 != @_)
    {
        confess "usage: printError(message);";
    }

    # remove trailing invisible characters
    $message =~ s/\s+$//s;
    # check for multi-lines warning
    if ($message =~ m/[\r\n]/)
    {
        # multi-lines
        my @message_lines = split(/(?:\n|\r\n|\r)/, $message);
        print STDERR "ERROR: " . shift(@message_lines) . "\n         ";
        print STDERR join("\n       ", @message_lines);
        print STDERR "\n\n";
    }
    else
    {
        # single line
        print STDERR "ERROR: " . $message . "\n\n";
    }
    return;
}




=pod

=head2 printDebug

B<Description>: display a debug message on STDERR if debug mode is ON.

B<ArgsCount>: 1

=over 4

=item $message: (string) (R)

debug message to display.

=back

B<Example>:

    printDebug("Starting the process using paramaters:!\na=25\nb=50\nc=3");

=cut

sub printDebug
{
    my ($message) = @_;
    # check arguments
    if (1 != @_)
    {
        confess "usage: printDebug(message);";
    }

    # check if debugging is disabled
    if (!$debug_g)
    {
        #debugging disabled, nothing to do!
        return;
    }

    # remove trailing invisible characters
    $message =~ s/\s+$//s;
    # check for multi-lines warning
    if ($message =~ m/[\r\n]/)
    {
        # multi-lines
        my @message_lines = split(/(?:\n|\r\n|\r)/, $message);
        print STDERR "DEBUG: " . shift(@message_lines) . "\n       ";
        print STDERR join("\n       ", @message_lines);
        print STDERR "\n";
    }
    else
    {
        # single line
        print STDERR "DEBUG: " . $message . "\n";
    }
    return;
}




=pod

=head2 printStageStart

B<Description>: display the name of a stage when started.

B<ArgsCount>: 1

=over 4

=item $stage_name: (string) (R)

Name of the stage without any EOL character.

=back

B<Example>:

    printStageStart('Alignment');

=cut

sub printStageStart
{
    my ($stage_name) = @_;
    # check arguments
    if (1 != @_)
    {
        confess "usage: printStageStart(stage_name);";
    }

    # compute spaces to add
    my $reminding_spaces_count = $FIRST_COLUMN_WIDTH - $INDENTATION - length($stage_name);
    if (0 > $reminding_spaces_count)
    {
        $reminding_spaces_count = 0;
    }
    print ' ' x $INDENTATION . $stage_name . '.' x $reminding_spaces_count;
    # in case of debug mode, add a line-feed
    if ($debug_g)
    {
        print STDERR "\n"; #
    }
}



=pod

=head2 printStageEnd

B<Description>: display the status of a stage when terminating.

B<ArgsCount>: 1

=over 4

=item $status: (string) (R)

Status of the stage execution.

=back

B<Example>:

    printStageEnd('OK');

=cut

sub printStageEnd
{
    my ($status) = @_;
    # check arguments
    if (1 != @_)
    {
        confess "usage: printStageEnd(status);";
    }

    # indent if debug mode
    if ($debug_g)
    {
        print ' ' x $FIRST_COLUMN_WIDTH;
    }

    # remove trailing invisible characters
    $status =~ s/\s+$//s;
    print $status . "\n";
}




=cut
<sequence id_source="y">
      <sequence_relation id_ref_0="x" id_ref_1="y" type="paralogy"/>
      <sequence_relation id_ref_0="x" id_ref_1="z" type="orthology"/>
      <sequence_relation id_ref_0="y" id_ref_1="z" type="orthology"/>
</phylogeny>
