#!/usr/bin/env perl

=pod

=head1 NAME

check_genomes.pl - Check genome FASTA contents.

=head1 SYNOPSIS

    check_genomes.pl -genomes 'SOMEDIR/file1.faa;SOMEDIR2/file2.faa'

=head1 REQUIRES

Perl5

=head1 DESCRIPTION

Check genomes: sequence content, stop codons.

=cut

use strict;
use warnings;

use FindBin;
use lib "$FindBin::Bin";
use lib "$FindBin::Bin/../../website/lib";
use lib "$FindBin::Bin/../../website/local_lib";
use Carp qw (cluck confess croak);

use Bio::SeqIO;
use Bio::AlignIO;
use Bio::LocatableSeq;
use Bio::SimpleAlign;

use Greenphyl;
use GreenPhylConfig;
use Greenphyl::Log('nolog' => 1,);

use Getopt::Long;
use Pod::Usage;

use threads;
use threads::shared;

++$|; #no buffering




# Script global constants
##########################

=pod

=head1 CONSTANTS

B<$DEBUG>: (boolean)

When set to true, it enables debug mode.

B<$VERBOSE_MODE>: (boolean)

When set to a true value, it enables verbose mode.
Numeric values higher than 1 will verbose more.

=cut

our $DEBUG = 0;
our $VERBOSE_MODE = 1;
our @GENOME_LIST = (
    'AMBTC',
    'ARATH',
    'BETVU',
    'BRADI/BdistachyonABR2_337_v1.ABR2.1.cds.faa.gz',
    'BRADI/BdistachyonABR3_343_v1.ABR3.1.cds.faa.gz',
    'BRADI/BdistachyonABR4_364_v1.ABR4.1.cds.faa.gz',
    'BRADI/BdistachyonABR5_379_v1.ABR5.1.cds.faa.gz',
    'BRADI/BdistachyonABR6_336_v1.ABR6_r.1.cds.faa.gz',
    'BRADI/BdistachyonABR7_369_v1.ABR7.1.cds.faa.gz',
    'BRADI/BdistachyonABR8_356_v1.ABR8.1.cds.faa.gz',
    'BRADI/BdistachyonABR9_333_v1.ABR9_r.1.cds.faa.gz',
    'BRADI/BdistachyonAdi_10_381_v1.Adi-10.1.cds.faa.gz',
    'BRADI/BdistachyonAdi_12_359_v1.Adi-12.1.cds.faa.gz',
    'BRADI/BdistachyonAdi_2_372_v1.Adi-2.1.cds.faa.gz',
    'BRADI/BdistachyonArn1_355_v1.Arn1.1.cds.faa.gz',
    'BRADI/BdistachyonBd1_1_349_v1.Bd1-1.1.cds.faa.gz',
    'BRADI/BdistachyonBd18_1_362_v1.Bd18-1.1.cds.faa.gz',
    'BRADI/BdistachyonBd21_3_378_v1.Bd21-3_r.1.cds.faa.gz',
    'BRADI/BdistachyonBd21v2_1_283_Bd21v2.1.cds.faa.gz',
    'BRADI/BdistachyonBd2_3_353_v1.Bd2-3.1.cds.faa.gz',
    'BRADI/BdistachyonBd29_1_346_v1.Bd29-1.1.cds.faa.gz',
    'BRADI/BdistachyonBd30_1_344_v1.Bd30-1.1.cds.faa.gz',
    'BRADI/BdistachyonBd3_1_328_v1.Bd3-1_r.1.cds.faa.gz',
    'BRADI/BdistachyonBdTR10c_374_v1.BdTR10C.1.cds.faa.gz',
    'BRADI/BdistachyonBdTR11a_380_v1.BdTR11A.1.cds.faa.gz',
    'BRADI/BdistachyonBdTR11g_357_v1.BdTR11G.1.cds.faa.gz',
    'BRADI/BdistachyonBdTR11i_363_v1.BdTR11I.1.cds.faa.gz',
    'BRADI/BdistachyonBdTR12c_352_v1.BdTR12c.1.cds.faa.gz',
    'BRADI/BdistachyonBdTR13a_334_v1.BdTR13a.1.cds.faa.gz',
    'BRADI/BdistachyonBdTR13c_365_v1.BdTR13C.1.cds.faa.gz',
    'BRADI/BdistachyonBdTR1i_345_v1.BdTR1i.1.cds.faa.gz',
    'BRADI/BdistachyonBdTR2b_376_v1.BdTR2B.1.cds.faa.gz',
    'BRADI/BdistachyonBdTR2g_367_v1.BdTR2G.1.cds.faa.gz',
    'BRADI/BdistachyonBdTR3c_354_v1.BdTR3C.1.cds.faa.gz',
    'BRADI/BdistachyonBdTR5i_370_v1.BdTR5I.1.cds.faa.gz',
    'BRADI/BdistachyonBdTR7a_329_v1.BdTR7a.1.cds.faa.gz',
    'BRADI/BdistachyonBdTR8i_348_v1.BdTR8i.1.cds.faa.gz',
    'BRADI/BdistachyonBdTR9k_358_v1.BdTR9K.1.cds.faa.gz',
    'BRADI/BdistachyonBis_1_338_v1.Bis-1.1.cds.faa.gz',
    'BRADI/BdistachyonFoz1_366_v1.Foz1.1.cds.faa.gz',
    'BRADI/BdistachyonGaz_8_339_v1.Gaz-8.1.cds.faa.gz',
    'BRADI/BdistachyonJer1_375_v1.Jer1.1.cds.faa.gz',
    'BRADI/BdistachyonKah_1_351_v1.Kah-1.1.cds.faa.gz',
    'BRADI/BdistachyonKah_5_342_v1.Kah-5.1.cds.faa.gz',
    'BRADI/BdistachyonKoz_1_330_v1.Koz-1.1.cds.faa.gz',
    'BRADI/BdistachyonKoz_3_368_v1.Koz-3.1.cds.faa.gz',
    'BRADI/BdistachyonLuc1_347_v1.Luc1.1.cds.faa.gz',
    'BRADI/BdistachyonMig3_377_v1.Mig3.1.cds.faa.gz',
    'BRADI/BdistachyonMon3_350_v1.Mon3.1.cds.faa.gz',
    'BRADI/BdistachyonMur1_373_v1.Mur1.1.cds.faa.gz',
    'BRADI/BdistachyonPer1_326_v1.Per1.1.cds.faa.gz',
    'BRADI/BdistachyonRon2_360_v1.RON2.1.cds.faa.gz',
    'BRADI/BdistachyonS8iiC_340_v1.S8iiC.1.cds.faa.gz',
    'BRADI/BdistachyonSig2_371_v1.Sig2.1.cds.faa.gz',
    'BRADI/BdistachyonTek_2_341_v1.Tek-2.1.cds.faa.gz',
    'BRADI/BdistachyonTek_4_332_v1.Tek-4.1.cds.faa.gz',
    'BRADI/BdistachyonUni2_327_v1.Uni2.1.cds.faa.gz',
    'BRANA/Brassica_napus.annotation_v5.gff3.faa',
    'BRANA/Darmor_v81_assembly.all.maker.augustus_masked.filtered.faa',
    'BRANA/Tapidor_v63_assembly.all.maker.augustus_masked.filtered.faa',
    'BRAOL/A2_v1.0.chromosomes.faa',
    'BRAOL/HDEM.faa',
    'BRAOL/TO1000_GCF_000695525.1_BOL.faa',
    'BRARR/Brapa_genome_v3.0.faa',
    'BRARR/BRAPA_Z1.faa',
    'CAJCA',
    'CAPAN/Capsicum.annuum.L_Zunla-1_v2.0.faa',
    'CAPAN/Capsicum.annuum.var.glabriusculum_Chiltepin_v2.0.faa',
    'CAPAN/ref_cm334_Annuum.v.2.0.faa',
    'CHEQI',
    'CICAR/cicar.desi.faa',
    'CICAR/cicar.kabuli.faa',
    'CITMA',
    'CITME',
    'CITSI',
    'COCNU/COCNU-C3B02.faa',
    'COCNU/cocnu_CATD.faa',
    'COFAR/COFAR_protein.faa',
    'COFCA',
    'CUCME',
    'CUCSA/Cucumis_sativus.ASM407v2.all.faa',
    'CUCSA/Gy14_v2.faa',
    'CUCSA/PI183967.faa',
    'DAUCA',
    'DIORT',
    'ELAGV',
    'FRAVE',
    'SOYBN/Gmax_189_primaryTranscriptOnly.faa',
    'SOYBN/ZH13.faa',
    'HELAN/HanXRQr1.0-20151230-EGN-r1.2.faa',
    'HORVU',
    'IPOTF/NSP306.faa',
    'IPOTF/Y22.fixed.faa',
    'IPOTR',
    'MALDO/GDDH13_1-1.faa',
    'MALDO/HFTH1.gene.faa',
    'MALDO/Malus_x_domestica.v3.0.a1_gene_set.faa',
    'MANES/MANES.proteins.fasta',
    'MEDTR/Mt4.0v2_GenesSeq_20140818_1100.faa',
    'MEDTR/MtrunA17r5.0-ANR-EGN-r1.6.faa',
    'MUSAC/MUSAC_banksii.faa',
    'MUSAC/MUSAC_burmannicoides.faa',
    'MUSAC/MUSAC_malaccensis.faa',
    'MUSAC/MUSAC_zebrina.faa',
    'MUSBA',
    'OLEEU',
    'ORYGL/Oryza_glaberrima.AGI1.1.pep.all.fa',
    'ORYSA/Nipponbare.faa',
    'ORYSA/Oryza_sativa_indica_IR8.faa',
    'ORYSA/Oryza_sativa_indica_MH63_2.faa',
    'ORYSA/Oryza_sativa_indica_ZS97_2.faa',
    'ORYSA/Oryza_sativa_N22.faa',
    'ORYSA/OsativaKitaake_499_v3.1.primaryTranscriptOnly.faa',
    'ORYSA/R498_IGDBv3_coreset.long.faa',
    'PHAVU',
    'PHODA',
    'SACSP/AP85-441_Sspon.v20190103.faa',
    'SOLLY',
    'SOLTU',
    'SORBI/BTx623.faa',
    'SORBI/Tx430.faa',
    'THECC/cacao11genes_pub3i.filtered.faa',
    'THECC/Crillo.faa',
    'TRITU/Svevo.faa',
    'TRITU/Zavitan.faa',
    'VITVI/vitviv2.faa',
    'VITVI/CabernetSauvignon.faa',
    'VITVI/Carmenere.faa',
    'ZEAMA/ZEAMA_B73.faa',
    'ZEAMA/ZEAMA_CML247.faa',
    'ZEAMA/ZEAMA_MO17.faa',
    'ZEAMA/ZEAMA_PH207.faa',
);




# Script options
#################

=pod

=head1 OPTIONS

  check_genomes.pl
    [-help | -man]
    [-debug [debug_level]]
    -genomes <AA_FASTA_FILES>
    [-nogh]
    [-log]

=head2 Parameters

=over 4

=item B<-help>:

Prints a brief help message and exits.

=item B<-man>:

Prints the manual page and exits.

=item B<-debug> (integer):

Executes the script in debug mode. The integer value is optional and is used
to set debug level (use 0 to force disable debug mode).
Default: 0 (not in debug mode).

=item B<AA_FASTA_FILES> (string):

A list of ".faa" files to check separated by comas (can be full paths).
Files can be ".faa.gz".
Default path to search for files: $GreenPhylConfig::GENOMES_PATH.

=item B<-keep>:

When uncompressing gz files, do not auto-remove uncompressed files after the
process.

=item B<-fast>:

Just run a fast check that verify files are there.

=item B<-log>:

Enable logging.

=item B<-log-*> (any):

Logging arguments.

=back

=cut


# CODE START
#############

umask 0022;

# options processing
my ($man, $help, $debug, $no_prompt) = (0, 0, undef, undef);
my ($cmd, $genome_list, $keep_uncompressed, $fast_check);
# parse options and print usage if there is a syntax error.
GetOptions('help|?'  => \$help,
           'man'     => \$man,
           'debug:s' => \$debug,
           'genomes=s' => \$genome_list,
           'k|keep' => \$keep_uncompressed,
           'f|fast' => \$fast_check,
           @Greenphyl::Log::LOG_GETOPT,
) or pod2usage(1);
if ($help) {pod2usage('-verbose' => 1, '-exitval' => 0);}
if ($man) {pod2usage('-verbose' => 2, '-exitval' => 0);}

# change debug mode if requested/forced
if (defined($debug))
{
    # just '-debug' ?
    if ($debug eq '')
    {
        $debug = 1;
    }
    $DEBUG = $debug;
}

LogInfo("Checking genomes...");
my $checked_sequences = 0;
my %checked_genomes = ();
my %invalid_genomes = ();
my %invalid_content = ();

# Get genome list.
my @genome_list = $genome_list ? split(/[ ,;]+/, $genome_list) : @GENOME_LIST;

foreach my $genome (grep(/\w/, @genome_list))
{
    my $genome_faa_file_path = '';
    if ($genome =~ m/^\//)
    {
        # Absolute path.
        $genome_faa_file_path = $genome;
    }
    else
    {
        # Relative path.
        $genome_faa_file_path = $GreenPhylConfig::GENOMES_PATH . $genome;
    }
    my $genome_fna_file_path = $genome_faa_file_path;
    $genome_fna_file_path =~ s/(\.faa|\.fasta|\.fa)/.fna/;

    # Extract genome name.
    my ($species_code) = ($genome =~ m/(?:^|\/)([A-Z]{5,5}|PEA)\/[^\/]+$/);
    $species_code ||= 'unknown species';
    $genome =~ s/^.*\///;
    $genome =~ s/(\.faa|\.fasta|\.fa)(?:\.gz)?$//;
    my $extension = $1;
    $checked_genomes{$genome} ||= 0;
    LogInfo("...'$genome' ($species_code) " . ((-e $genome_fna_file_path) ? 'with NA ' : '') . "(" . scalar(keys(%checked_genomes)) . "/" . scalar(@genome_list) . ")");

    if (!-e $genome_faa_file_path)
    {
        $invalid_genomes{$genome} = {'File not found' => $genome_faa_file_path};
        $checked_genomes{$genome} = 0;
        next;
    }

    if ($extension && ('.faa' ne $extension))
    {
        $invalid_genomes{$genome} = {'Bad extension (should be .faa or nothing for single genomes)' => $genome_faa_file_path};
    }

    if ($fast_check)
    {
        LogInfo("$genome: $genome_faa_file_path OK");
        if (-e $genome_fna_file_path)
        {
            LogInfo("    with associated $genome_fna_file_path");
        }
        next;
    }

    # Check for compression.
    my ($genome_faa_file_path_gunzip, $genome_fna_file_path_gunzip);
    if ($genome_faa_file_path =~ m/\.gz$/)
    {
        # Uncompress.
        $genome_faa_file_path_gunzip = $genome_faa_file_path;
        $genome_faa_file_path_gunzip =~ s/\.gz$//;
        LogDebug("Compressed file '$genome_faa_file_path' (uncompressed: '$genome_faa_file_path_gunzip')");
        if (!-s $genome_faa_file_path_gunzip)
        {
            $cmd = "gunzip -c $genome_faa_file_path > $genome_faa_file_path_gunzip";
            LogDebug("COMMAND: $cmd");
            if ((0 != system($cmd)) || (!-s $genome_faa_file_path_gunzip))
            {
                if (not $!)
                {
                    confess LogError("GUNZIP: Failed to uncompress file '$genome_faa_file_path'.");
                }
                confess LogError("GUNZIP: $!");
            }
            $genome_faa_file_path = $genome_faa_file_path_gunzip;
        }
        else
        {
          LogDebug("No need to gunzip '$genome_faa_file_path' as '$genome_faa_file_path_gunzip' already exists.");
          $genome_faa_file_path = $genome_faa_file_path_gunzip;
          $genome_faa_file_path_gunzip = undef;
        }

        # Same with .fna if available.
        if (-e $genome_fna_file_path)
        {
            $genome_fna_file_path_gunzip = $genome_fna_file_path;
            $genome_fna_file_path_gunzip =~ s/\.gz$//;
            LogDebug("Compressed file '$genome_fna_file_path' (uncompressed: '$genome_fna_file_path_gunzip')");
            if (!-s $genome_fna_file_path_gunzip)
            {
                $cmd = "gunzip -c $genome_fna_file_path > $genome_fna_file_path_gunzip";
                LogDebug("COMMAND: $cmd");
                if ((0 != system($cmd)) || (!-s $genome_fna_file_path_gunzip))
                {
                    if (not $!)
                    {
                        confess LogError("GUNZIP: Failed to uncompress file '$genome_fna_file_path'.");
                    }
                    confess LogError("GUNZIP: $!");
                }
                $genome_fna_file_path = $genome_fna_file_path_gunzip;
            }
            else
            {
              LogDebug("No need to gunzip '$genome_fna_file_path' as '$genome_fna_file_path_gunzip' already exists.");
              $genome_fna_file_path = $genome_fna_file_path_gunzip;
              $genome_fna_file_path_gunzip = undef;
            }
        }
    }

    # Check content.
    my %fna_seq;
    # Read .fna if one.
    if (-e $genome_fna_file_path)
    {
        my $seqi_obj = Bio::SeqIO->new(
            -file => $genome_fna_file_path,
            -format => "fasta"
        );
        while (my $seq_obj = $seqi_obj->next_seq)
        {
            # Store sequence names.
            $fna_seq{$seq_obj->display_id} = 1;
        }
    }

    # Read faa.
    my %faa_seq;
    my $seqi_obj = Bio::SeqIO->new(
        -file => $genome_faa_file_path,
        -format => "fasta"
    );
    while (my $seq_obj = $seqi_obj->next_seq)
    {
        # Check duplicates.
        if (exists($faa_seq{$seq_obj->display_id}))
        {
            push(@{$invalid_genomes{$genome}->{'Duplicate sequence'}}, $seq_obj->display_id);
        }
        else
        {
            $faa_seq{$seq_obj->display_id} = 1;
        }

        # Check corresponding DNA sequence.
        if (scalar(keys(%fna_seq)) && !exists($fna_seq{$seq_obj->display_id}))
        {
            push(@{$invalid_genomes{$genome}->{'Missing associated nucleic acid sequence'}}, $seq_obj->display_id);
        }

        # Check gene sequence content.
        if ($seq_obj->seq() =~ m/[^A-Za-z*]/)
        {
            $invalid_genomes{$genome} ||= {};
            $invalid_genomes{$genome}->{'Invalid content'} ||= [];
            push(@{$invalid_genomes{$genome}->{'Invalid content'}}, $seq_obj->display_id);
            $invalid_content{$genome} ||= {};
            my $invalid_content = $seq_obj->seq();
            $invalid_content =~ s/[A-Za-z*]//g;
            foreach my $invalid_char (split(//, $invalid_content))
            {
                $invalid_content{$genome}->{$invalid_char} ||= 0;
                $invalid_content{$genome}->{$invalid_char}++;
            }
        }
        elsif ($seq_obj->seq() =~ m/\*[^A-Za-z]/)
        {
            $invalid_genomes{$genome} ||= {};
            $invalid_genomes{$genome}->{'Codon stop in sequence'} ||= [];
            push(@{$invalid_genomes{$genome}->{'Codon stop in sequence'}}, $seq_obj->display_id);
        }
        elsif ($seq_obj->seq() =~ m/^[^Mm]/)
        {
            $invalid_genomes{$genome} ||= {};
            $invalid_genomes{$genome}->{'Not starting with Met'} ||= [];
            push(@{$invalid_genomes{$genome}->{'Not starting with Met'}}, $seq_obj->display_id);
        }

        if ($seq_obj->seq() =~ m/\*/)
        {
            push(@{$invalid_genomes{$genome}->{'Has stars'}}, $seq_obj->display_id);
        }

        ++$checked_sequences;
        $checked_genomes{$genome}++;
    }

    if (scalar(keys(%fna_seq))
        && (scalar(keys(%fna_seq)) != scalar(keys(%faa_seq))))
    {
        $invalid_genomes{$genome}->{'Nucleic acid file and amino acid file have different sequence counts'} = scalar(keys(%fna_seq)) . ' vs ' . scalar(keys(%faa_seq));
    }

    # Remove uncompressed file.
    if (!$keep_uncompressed)
    {
        if ($genome_faa_file_path_gunzip)
        {
            unlink $genome_faa_file_path_gunzip;
        }
        if ($genome_fna_file_path_gunzip)
        {
            unlink $genome_fna_file_path_gunzip;
        }
    }
}

LogInfo('Checked genomes: ' . scalar(keys(%checked_genomes)));
LogInfo('Checked sequences: ' . $checked_sequences);
LogInfo('Invalid genomes: ' . scalar(keys(%invalid_genomes)));
foreach my $genome (keys(%invalid_genomes))
{
    LogInfo("Invalid sequences for '$genome' (" . $checked_genomes{$genome} . " sequences):");
    foreach my $error (keys(%{$invalid_genomes{$genome}}))
    {
      if (($error eq 'Missing associated nucleic acid sequence')
          && (scalar(@{$invalid_genomes{$genome}->{$error}}) == $checked_genomes{$genome}))
      {
          $invalid_genomes{$genome}->{$error} = 'they follow different nomenclatures';
      }
      
    
        if ('ARRAY' eq ref($invalid_genomes{$genome}->{$error}))
        {
            if (($error eq 'Invalid content')
                && (scalar(@{$invalid_genomes{$genome}->{$error}}) < 0.1 * $checked_genomes{$genome}))
            {
                LogInfo("   $error: " . scalar(@{$invalid_genomes{$genome}->{$error}}) . " (" . join(', ', keys(%{$invalid_content{$genome}})) . ")");
            }
            else
            {
                LogInfo("   $error: " . scalar(@{$invalid_genomes{$genome}->{$error}}));
            }
        }
        else
        {
            LogInfo("   $error: " . $invalid_genomes{$genome}->{$error});
        }
    }
}


exit(0);

__END__
# CODE END
###########

=pod

=head1 AUTHORS

Mathieu ROUARD (Bioversity), m.rouard@cgiar.org

Valentin GUIGNON (Bioversity), v.guignon@cgiar.org

Matthieu CONTE (Syngenta)

Abdel Toure (Syngenta)

=head1 VERSION

Version 1.0.4

Date 20/11/2019

=head1 SEE ALSO

GreenPhyl documentation.

=cut
