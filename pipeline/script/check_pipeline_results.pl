#!/usr/bin/perl

=pod

=head1 NAME

check_pipeline_results.pl - Display a summary on pipeline results

=head1 SYNOPSIS

    check_pipeline_results.pl /data/pipeline/output

=head1 REQUIRES

Perl5

=head1 DESCRIPTION

Process a pipleine output directory and list what has been correctly analysed
and what may need to be relaunched.

=cut

use strict;
use Carp qw (cluck confess croak);
use warnings;
use Readonly;

use File::Basename;
use Getopt::Long;
use Pod::Usage;
use Error qw(:try);

use GreenPhylConfig;



# Script global constants
##########################

=pod

=head1 CONSTANTS

B<$DEBUG>: (boolean)

If set to non-zero, debug messages will be displayed.

=cut

Readonly my $DEBUG                                => 0;
Readonly my $INPUT_FILE_EXTENSION                 => '.src';
Readonly my $OUTPUT_FILE_EXTENSION                => '.out';
Readonly my $DICTIONARY_FILE_EXTENSION            => '.dic';
Readonly my $FASTA_FILE_EXTENSION                 => '.fasta';
Readonly my $MAFFT_FILE_EXTENSION                 => '.mafft';
Readonly my $HMM_FILE_EXTENSION                   => '.hmm';
Readonly my $CLUSTAL_FILE_EXTENSION               => '.clw';
Readonly my $MASKING_FILE_EXTENSION               => '.mask';
Readonly my $FILTERING_FILE_EXTENSION             => '.fltr';
Readonly my $BSP_MASKING_FILE_EXTENSION           => '.bsp';
Readonly my $MASKING_HTML_FILE_EXTENSION          => "_masking.html";
Readonly my $FILTERED_HTML_FILE_EXTENSION         => "_filtered.html";
Readonly my $FILTERED_SEQ_FILE_EXTENSION          => "_filtered.seq";
Readonly my $PHYLIP_FILE_EXTENSION                => '.phy';
Readonly my $NEWICK_FILE_EXTENSION                => '.nwk';
Readonly my $NEWICK_EXTENDED_FILE_EXTENSION       => '.nhx';
Readonly my $PHYLOXML_FILE_EXTENSION              => '.xml';
Readonly my $PHYLIP_SCRIPT_FILENAME               => 'phylip.script';
Readonly my $PHYLIP_OUTTREE_FILENAME              => 'outtree';
Readonly my $PHYML_TREE_FILE_SUFFIX               => '_phyml_tree.txt';
Readonly my $PHYML_BOOTSTRAP_FILE_SUFFIX          => '_phyml_boot_trees.txt';
Readonly my $PHYML_DISTANCE_MATRIX_FILE_SUFFIX    => '_phyml_dist.txt';
Readonly my $PHYLOGENY_FILE_SUFFIX                => '_phylogeny_tree' . $NEWICK_FILE_EXTENSION;
Readonly my $ROOTED_TREE_FILE_SUFFIX              => '_rap_gene_tree' . $NEWICK_FILE_EXTENSION;
Readonly my $RECONCILIED_TREE_FILE_SUFFIX         => '_rap_reconcilied_gene_tree' . $NEWICK_FILE_EXTENSION;
Readonly my $STATS_TREE_FILE_SUFFIX               => '_rap_stats_tree.txt';
Readonly my $XML_TREE_FILE_SUFFIX                 => '_rap_tree.xml';
Readonly my $SDI_OUTPUT_FILE                      => 'sdir_outfile.xml';
Readonly my $SDI_OUTPUT_FILE_EXT                  => '.sdi.xml';
Readonly my $BOOTSTRAP_TREE_FILENAME_SEED         => 'bootstrap_';
Readonly my $RIO_OUTPUT_FILE_SEED                 => 'rio_';
Readonly my $RIO_DATA_OUTPUT_FILE_EXTENSION       => '.txt';
Readonly my $RIO_TREE_OUTPUT_FILE_EXTENSION       => '.xml';
Readonly my $LOG_EXTENSION                        => '_log.txt';


# Script global variables
##########################

=pod

=head1 VARIABLES

B<$g_debug>: (boolean)

Debug flag.

=cut

my $g_debug;
my @g_families_ok_list    = ();
my @g_families_error_list = ();
my %g_family_errors       = ();




# Script global functions
##########################

=pod

=head2 dumpFile

B<Description>: Dump the content of a file.

B<ArgsCount>: 1

=over 4

=item arg: $file_path (string) (R)

Full path to the file to dump.

=back

B<Return>: (string)

File content.

B<Example>:

    print dumpFile('/data/pipeline/output/1234/mafft_err.log');

=cut

sub dumpFile
{
    my ($file_path) = @_;

    my $content = '';

    # check arguments
    if (!$file_path)
    {
        confess "ERROR: dumpFile: no file specified!\n";
    }

    # display log
    my $fh;
    if (open($fh, $file_path))
    {
        my $filename = $file_path;
        $filename =~ s/.*\///;
        $content .= "$filename content:\n/---------\n|  ";
        $content .= join('|  ', <$fh>);
        $content .= "|\n\\---------\n\n";
    }
    else
    {
        $content .= "Failed to open log file '$file_path'!\n$!\n";
    }

    return $content;
}


=pod

=head2 checkOutputDirectory

B<Description>: check the output directory.

B<ArgsCount>: 1

=over 4

=item arg: $output_directory (string) (R)

Full path to the output directory.

=back

B<Return>: (string)

Checked output directory.

=cut

sub checkOutputDirectory
{
    my ($output_directory) = @_;

    # check arguments
    if (!$output_directory)
    {
        confess "ERROR: no directory to process provided!\n";
    }

    # make sure we got only one trailing slash
    $output_directory =~ s/\/+$//;
    $output_directory .= '/';

    if (!-e $output_directory)
    {
        confess "ERROR: directory '$output_directory' not found!\n";
    }
    elsif (!-d $output_directory)
    {
        confess "ERROR: '$output_directory' is not a directory!\n";
    }
    elsif (!-r $output_directory)
    {
        confess "ERROR: directory '$output_directory' is not readable!\n";
    }

    return $output_directory;
}


=pod

=head2 checkMAFFTOutputs

B<Description>: check the outputs of MAFFT.

B<ArgsCount>: 2

=over 4

=item arg: $output_directory (string) (R)

Full path to the output directory.

=item arg: $family_name (string) (R)

Name of the family.

=back

B<Return>: nothing

=cut

sub checkMAFFTOutputs
{
    my ($output_directory, $family_name) = @_;

    # check MAFFT outputs
    if (!-e "$output_directory$family_name$MAFFT_FILE_EXTENSION")
    {
        confess("MAFFT output missing");
    }
    elsif (!-s "$output_directory$family_name$MAFFT_FILE_EXTENSION")
    {
        confess("MAFFT output empty");
    }
}


=pod

=head2 checkMAFFTOutputErrors

B<Description>: check the outputs of MAFFT.

B<ArgsCount>: 2

=over 4

=item arg: $output_directory (string) (R)

Full path to the output directory.

=item arg: $family_name (string) (R)

Name of the family.

=back

B<Return>: (string)

Check result message.

=cut

sub checkMAFFTOutputErrors
{
    my ($output_directory, $family_name) = @_;
    my $error_tips = '';

    # check MAFFT outputs
    try
    {
        checkMAFFTOutputs($output_directory, $family_name);
    }
    otherwise
    {
        # check if alignment is available
        if (!-s "$output_directory$family_name$INPUT_FILE_EXTENSION")
        {
            $error_tips .= "Input alignment not available!\n";
        }
        if (-e $output_directory . "mafft_err.log")
        {
            $error_tips .= "mafft_err.log:\n" . dumpFile($output_directory . "mafft_err.log");
        }
        else
        {
            $error_tips .= "mafft_err.log not available\n";
        }
        $error_tips .= "\nSequence(s) to align: " . `cat $output_directory$family_name$INPUT_FILE_EXTENSION |grep '>' |wc -l` . "\n\n";
        $error_tips .= "\nAvailable files:\n----------\n" . `ls -al $output_directory 2>&1` . "----------\n\n";
        confess shift();
    };

    return $error_tips;
}


=pod

=head2 checkTrimalOutputs

B<Description>: check the outputs of Trimal.

B<ArgsCount>: 2

=over 4

=item arg: $output_directory (string) (R)

Full path to the output directory.

=item arg: $family_name (string) (R)

Name of the family.

=back

B<Return>: nothing

=cut

sub checkTrimalOutputs
{
    my ($output_directory, $family_name) = @_;

    # check Trimal outputs
    if (!-e "$output_directory$family_name$FILTERING_FILE_EXTENSION$FASTA_FILE_EXTENSION")
    {
        confess("Trimal output missing");
    }
    elsif (!-s "$output_directory$family_name$FILTERING_FILE_EXTENSION$FASTA_FILE_EXTENSION")
    {
        confess("Trimal output empty");
    }
}


=pod

=head2 checkTrimalOutputErrors

B<Description>: check the outputs of Trimal.

B<ArgsCount>: 2

=over 4

=item arg: $output_directory (string) (R)

Full path to the output directory.

=item arg: $family_name (string) (R)

Name of the family.

=back

B<Return>: (string)

Check result message.

=cut

sub checkTrimalOutputErrors
{
    my ($output_directory, $family_name) = @_;
    my $error_tips = '';

    checkTrimalOutputs($output_directory, $family_name);

    return $error_tips;
}


=pod

=head2 checkPhyMLOutputs

B<Description>: check the outputs of PhyML.

B<ArgsCount>: 2

=over 4

=item arg: $output_directory (string) (R)

Full path to the output directory.

=item arg: $family_name (string) (R)

Name of the family.

=back

B<Return>: nothing

=cut

sub checkPhyMLOutputs
{
    my ($output_directory, $family_name) = @_;

    # check PhyML outputs
    if (!-e "$output_directory$family_name$PHYLOGENY_FILE_SUFFIX")
    {
        confess("PhyML output missing");
    }
    elsif (!-s "$output_directory$family_name$PHYLOGENY_FILE_SUFFIX")
    {
        confess("PhyML output empty");
    }
}


=pod

=head2 checkPhyMLOutputErrors

B<Description>: check the outputs of PhyML.

B<ArgsCount>: 2

=over 4

=item arg: $output_directory (string) (R)

Full path to the output directory.

=item arg: $family_name (string) (R)

Name of the family.

=back

B<Return>: (string)

Check result message.

=cut

sub checkPhyMLOutputErrors
{
    my ($output_directory, $family_name) = @_;
    my $error_tips = '';

    # check PhyML outputs
    try
    {
        checkPhyMLOutputs($output_directory, $family_name);
    }
    otherwise
    {
        # get SGE job ID
        if (-e $output_directory . "phyml_err.log")
        {
            $error_tips .= "PhyML SGE job ID:\n" . dumpFile($output_directory . "phyml_err.log");
        }
        else
        {
            $error_tips .= "PhyML SGE job ID (phyml_err.log) not available\n";
        }
        # check logs
        if (-e $output_directory . "phyml.log")
        {
            $error_tips .= "phyml.log:\n" . dumpFile($output_directory . "phyml.log");
        }
        else
        {
            $error_tips .= "phyml.log not available\n";
        }
        
        confess shift();
    };

    return $error_tips;
}


=pod

=head2 checkRAPOutputs

B<Description>: check the outputs of RAPGreen.

B<ArgsCount>: 2

=over 4

=item arg: $output_directory (string) (R)

Full path to the output directory.

=item arg: $family_name (string) (R)

Name of the family.

=back

B<Return>: nothing

=cut

sub checkRAPOutputs
{
    my ($output_directory, $family_name) = @_;

    # check RAP outputs
    if (!-e "$output_directory$family_name$ROOTED_TREE_FILE_SUFFIX")
    {
        confess("RAP output missing");
    }
    elsif (!-s "$output_directory$family_name$ROOTED_TREE_FILE_SUFFIX")
    {
        confess("RAP output empty");
    }

}


=pod

=head2 checkRAPOutputErrors

B<Description>: check the outputs of RAPGreen.

B<ArgsCount>: 2

=over 4

=item arg: $output_directory (string) (R)

Full path to the output directory.

=item arg: $family_name (string) (R)

Name of the family.

=back

B<Return>: (string)

Check result message.

=cut

sub checkRAPOutputErrors
{
    my ($output_directory, $family_name) = @_;
    my $error_tips = '';

    checkRAPOutputs($output_directory, $family_name);

    return $error_tips;
}


=pod

=head2 displayFamilyErrorDetails

B<Description>: Process the directory containing all the output files of a
family processed by the pipeline and give details about the error that occured.

B<ArgsCount>: 2

=over 4

=item arg: $output_directory (string) (R)

Full path to the output directory.

=item arg: $family_name (string) (R)

Name of the family.

=back

B<Return>: nothing

B<Example>:

    displayFamilyErrorDetails('/data/pipeline/output', '1234');

=cut

sub displayFamilyErrorDetails
{
    my ($output_directory, $family_name) = @_;

    $output_directory = checkOutputDirectory($output_directory);

    # get family output directory
    $output_directory = "$output_directory$family_name/";
    
    print "Error details for family $family_name:\n";

    my $error_tips = '';
    try
    {
        $error_tips .= checkMAFFTOutputErrors($output_directory, $family_name);
        $error_tips .= checkTrimalOutputErrors($output_directory, $family_name);
        $error_tips .= checkPhyMLOutputErrors($output_directory, $family_name);
        $error_tips .= checkRAPOutputErrors($output_directory, $family_name);

        #+FIXME: check HMM and SDI-RIO

        # add family to the list of OK families
        print "No error found!\n";
    }
    otherwise
    {
        my $error = shift();
        # remove debug info from error message
        $error =~ s/ at .* line \d+.*$//s;
        # display error found
        print "-$error\n\n";

        # remove trailing slash for family log name
        $output_directory =~ s/\/$//;
        # display log
        print dumpFile($output_directory . $LOG_EXTENSION);

        if ($error_tips)
        {
            print "Other info:\n$error_tips\n";
        }
    };

    return;
}



=pod

=head2 processFamilyOutput

B<Description>: Process the directory containing all the output files of a
family processed by the pipeline.

B<ArgsCount>: 1

=over 4

=item arg: $output_directory (string) (R)

Full path to the family output directory.

=back

B<Return>: nothing

B<Example>:

    processFamilyOutput('/data/pipeline/output/1234/');

=cut

sub processFamilyOutput
{
    my ($output_directory) = @_;

    try
    {
        $output_directory = checkOutputDirectory($output_directory);
    }
    otherwise
    {
        cluck "WARNING: processFamilyOutput: unable to process directory" . ($output_directory ? " '$output_directory'" : '') . "!\n" . shift() . "\n";
        return;
    };

    my ($family_name) = ($output_directory =~ m/((?:GP0*)?\d+)\/$/);
    if ($family_name)
    {
        print "\rProcessing family $family_name: ";

        try
        {
            checkMAFFTOutputs($output_directory, $family_name);
            checkTrimalOutputs($output_directory, $family_name);
            checkPhyMLOutputs($output_directory, $family_name);
            checkRAPOutputs($output_directory, $family_name);

            #+FIXME: check HMM and SDI-RIO

            # add family to the list of OK families
            push(@g_families_ok_list, $family_name);
            print "OK        ";
        }
        otherwise
        {
            my $error = shift();
            # remove debug info from error message
            $error =~ s/ at .* line \d+.*$//s;

            # add family to the list of families with the same error type
            if (exists($g_family_errors{$error}))
            {
                push(@{$g_family_errors{$error}}, $family_name);
            }
            else
            {
                $g_family_errors{$error} = [$family_name];
            }
            # add family to the list of family with and error
            push(@g_families_error_list, $family_name);
            print "Error     ";
        };
    }
    else
    {
        print "ERROR: processFamilyOutput: family ID not found in given path ('$output_directory')\n";
    }

    return;
}



=pod

=head2 processOutputDirectory

B<Description>: Process the directory containing all the family directories
generated by the pipeline.

B<ArgsCount>: 1

=over 4

=item arg: $output_directory (string) (R)

Full path to the pipeline output directory.

=back

B<Return>: integer

Number of processed families.

B<Example>:

    processOutputDirectory('/data/pipeline/output/');

=cut

sub processOutputDirectory
{
    my ($output_directory) = @_;
    my $processed_families = 0;

    # check arguments
    try
    {
        $output_directory = checkOutputDirectory($output_directory);
    }
    otherwise
    {
        cluck "WARNING: processOutputDirectory: unable to process directory" . ($output_directory ? " '$output_directory'" : '') . "!\n" . shift() . "\n";
        return;
    };

    my @family_outputs = glob($output_directory . '*');

    print "Pipeline output directory: $output_directory\n";
    while (@family_outputs)
    {
        my $family_output_dir = shift @family_outputs;
        # check if entry is a family output directory
        if (($family_output_dir =~ m/\/(?:GP0*)?\d+$/) && (-d $family_output_dir))
        {
            processFamilyOutput($family_output_dir);
            ++$processed_families;
        }
    }

    print "\nDONE!\n"; # :-)

    return $processed_families;
}




# Script options
#################

=pod

=head1 OPTIONS

    run_pipe_in_multithread.pl [-help] [-man] [-debug] <DIRECTORY>
        [-list]
        [-list_error <ERROR_TYPE> [-list_error <ERROR_TYPE>] ...]
        [-all_errors]
        [-move <ERROR_TYPE> [-move <ERROR_TYPE>] ... -to <TARGET_DIR>]
        [-remove <ERROR_TYPE> [-remove <ERROR_TYPE>] ...]
        [-details <ERROR_TYPE> [-details <ERROR_TYPE>] ...]

=head2 Parameters

=over 4

=item B<-help>:

display this help message.

=item B<-man>:

display user manual.

=item B<-debug>:

set debug mode on.

=item B<DIRECTORY>:

Pipeline output directory to process.

=item B<-list>:

shows the list of families with errors.

=item B<-list_error> (string):

shows the list of families with the given error type.

=item B<-all_errors>:

shows the list of families with errors grouped by error type.

=item B<-remove> (string):

selects the families with the specified error in order to remove their results.

=item B<-move> (string):

selects the families with the specified error in order to move them.

=item B<-to> (string):

Target directory where selected results have to be moved into.

=item B<-details> (string):

Display available details about the error. The key word 'all' can be used to
detail all types of error.

=back

=head2 Examples

    check_pipeline_results.pl /data/pipeline/output

=cut


# CODE START
#############

++$| if !$|;
umask 0022;

print <<"___227_TITLE___";
######################################
#                                    #
# GreenPhyl pipeline results check   #
#                                    #
######################################
                                 v2.0

___227_TITLE___

# options processing
my ($help, $man, $output_directory, @list_errors, $all_errors,
$list_error_families, @remove_errors, @move_errors, $new_dir, @details,
$detail_all);

# parse options and print usage if there is a syntax error.
GetOptions('help|?'             => \$help,
           'man'                => \$man,
           'debug'              => \$g_debug,
           'list'               => \$list_error_families,
           'list_error=s'       => \@list_errors,
           'remove=s'           => \@remove_errors,
           'move=s'             => \@move_errors,
           'to=s'               => \$new_dir,
           'all_errors'         => \$all_errors,
           'details=s'          => \@details,
) or pod2usage(0);
if ($help) {pod2usage(0);}
if ($man) {pod2usage(-verbose => 2);}

$g_debug ||= $DEBUG;

# parameters check...

# check move option
if (@move_errors)
{
    if (!$new_dir)
    {
        # no target directory specified
        pod2usage(1);
    }
    # remove trailing slash
    $new_dir =~ s/\/+$//;
    
    if ((!-e $new_dir) && (!mkdir($new_dir, 0775)))
    {
        # target directory missing and unable to create it
        confess "ERROR: Failed to create target directory '$new_dir'!\n$!\n";
    }
    # check dir
    if (!-d $new_dir)
    {
        confess "ERROR: Specified target directory '$new_dir' is not a directory!\n";
    }
    elsif (!-x $new_dir)
    {
        confess "ERROR: Specified target directory '$new_dir' can not be opened!\n";
    }
    elsif (!-w $new_dir)
    {
        confess "ERROR: Specified target directory '$new_dir' is write-protected!\n";
    }
}

# get pipeline output directory to check
$output_directory = shift();

# make sure the directory is valid
if ((not defined $output_directory) || (!$output_directory))
{pod2usage(2);}

# remove trailing slash
$output_directory =~ s/\/+$//;

if (!-d $output_directory)
{
    print "ERROR: given output directory '$output_directory' is not a directory!\n";
    pod2usage(2);
}

# check if error details of specified families has been requested
my @errors_to_detail = ();
foreach my $family_to_detail (@details)
{
    if ($family_to_detail =~ m/^(?:GP0*)?\d+$/)
    {
        # family
        displayFamilyErrorDetails($output_directory, $family_to_detail);
    }
    else
    {
        # error to detail
        push(@errors_to_detail, $family_to_detail);
        if ($family_to_detail =~ m/^all$/i)
        {
            # detail all errors of all families
            $detail_all = 1;
        }
    }
}

# check if only family details were requested
if (@details && !@errors_to_detail)
{
    # we're done; don't process the whole directory again
    exit(0);
}

# process directory
my $processed_family_count = processOutputDirectory($output_directory);
my @family_error_list = sort(@g_families_error_list);

# print summary
print "\nSummary:\nProcessed families: $processed_family_count\n";
print "Families OK: " . scalar(@g_families_ok_list) . "\n";
print "Families with error: " . scalar(@family_error_list) . "\n";
if ($processed_family_count)
{
    print "Processed families OK ratio: " . int(0.5 + 100.*scalar(@g_families_ok_list)/$processed_family_count) . "%\n";
}
if ($processed_family_count != (scalar(@family_error_list) + scalar(@g_families_ok_list)))
{
    cluck("WARNING: Check family counts! Some families were not checked correctly!\n");
}
print "\n";

print "List of errors:\n";
foreach my $error_type (keys(%g_family_errors))
{
    print "-'$error_type': " . scalar(@{$g_family_errors{$error_type}}) . " family/-ies\n";
}
print "\n";

# list families with error if requested
if ($list_error_families)
{
    print "List of families with error: " . join(', ', @family_error_list) . "\n";
    print "\n";
}

# display lists of families with error grouped by error type
if ($all_errors)
{
    @list_errors = keys(%g_family_errors);
}
foreach my $list_error (@list_errors)
{
    if ($list_error
        && $g_family_errors{$list_error}
        && @{$g_family_errors{$list_error}})
    {
        print "List of families with error '$list_error': " . join(', ', @{$g_family_errors{$list_error}}) . "\n";
    }
}

# check if all errors should be detailed
if ($detail_all)
{
    @errors_to_detail = keys(%g_family_errors);
}

# display error details for associated families
foreach my $error_detail (@errors_to_detail)
{
    if ($error_detail
        && $g_family_errors{$error_detail}
        && @{$g_family_errors{$error_detail}})
    {
        foreach my $family_to_detail (@{$g_family_errors{$error_detail}})
        {
            displayFamilyErrorDetails($output_directory, $family_to_detail);
        }
    }
}


# move families
foreach my $move_error (@move_errors)
{
    if ($move_error
        && $g_family_errors{$move_error}
        && @{$g_family_errors{$move_error}})
    {
        foreach my $family_name (@{$g_family_errors{$move_error}})
        {
            # move family directory
            if (system("mv $output_directory/$family_name $new_dir/$family_name"))
            {
                cluck("WARNING: Failed to move family $family_name to '$new_dir/$family_name'!\n");
            }
            # move log
            if (system("mv $output_directory/$family_name$LOG_EXTENSION $new_dir/"))
            {
                cluck("WARNING: Failed to move family $family_name log to '$new_dir'!\n");
            }
        }
    }
}

# remove selected family results
foreach my $remove_error (@remove_errors)
{
    if ($remove_error
        && $g_family_errors{$remove_error}
        && @{$g_family_errors{$remove_error}})
    {
        foreach my $family_name (@{$g_family_errors{$remove_error}})
        {
            # remove family directory
            if (system("rm -rf $output_directory/$family_name"))
            {
                cluck("WARNING: Failed to remove family $family_name results!\n");
            }
            # remove log
            if (system("rm -f $output_directory/$family_name$LOG_EXTENSION"))
            {
                cluck("WARNING: Failed to remove family $family_name log!\n");
            }
        }
    }
}

exit(0);


=pod

=head1 AUTHORS

Valentin GUIGNON (CIRAD), valentin.guignon@cirad.fr

Mathieu ROUARD (Bioversity-France), m.rouard@cgiar.org

=head1 VERSION

Version 1.0.0

Date 03/08/2011

=head1 SEE ALSO

GreenPhyl documentation

=cut
