#!/usr/bin/perl

=pod

=head1 NAME

run_pipe_in_multithread.pl - Run several parallel GreenPhyl pipelines

=head1 SYNOPSIS

    # will only process '1234.fa'
    run_pipe_in_multithread.pl -fid /data/pipeline/in/1234.fa -limitations

    # start from '1234.fa' and then will process '1235.fa', '1236.fa', ...
    run_pipe_in_multithread.pl -dir /data/pipeline/in -limitations start=1234.fa -noreverse

    # will process files between '1234.fa' and '5678.fa'
    run_pipe_in_multithread.pl -dir /data/pipeline/in -limitations start=5678.fa end=1234.fa

    # will process files down to '5678.fa' which have less than 75 sequences
    run_pipe_in_multithread.pl -dir /data/pipeline/in -limitations end=5678.fa less=75

    # will only process files '2345.fa', '3456.fa' and '4444.fa'
    run_pipe_in_multithread.pl -dir /data/pipeline/in -limitations 2345.fa 3456.fa 4444.fa

    # will process phylogeny analyses on files between '1234.fa' and '5678.fa'
    # will only process files containing between 51 and 79 sequences
    # and will not process unprocessed files after 5min
    run_pipe_in_multithread.pl -d /data/pipeline/in -l 5678.fa-1234.fa more=50 less=80 time=5

=head1 REQUIRES

Perl5.005 (threading)

=head1 DESCRIPTION

This script can run $DEFAULT_THREADS_COUNT GreenPhyl pipelines in parallel and
will process each fasta ($FASTA_FILE_EXT) file of the supplied directory.

For multithreading, see:
http://perldoc.perl.org/perlthrtut.html

=cut

use strict;
use Carp qw (cluck confess croak);
use warnings;
use Readonly;

use threads;
use threads::shared;
use Getopt::Long;
use Pod::Usage;
use Error qw(:try);
use LockFile::Simple qw(trylock  unlock);
use Time::HiRes qw( time sleep );
use File::Basename;

use GreenPhylConfig;



# Script global constants
##########################

=pod

=head1 CONSTANTS

B<$DEBUG>: (boolean)

If set to non-zero, debug messages will be displayed.

B<$DEFAULT_THREADS_COUNT>: (integer)

default number of pipelines that can be run in parallel.

B<$FASTA_FILE_EXT>: (string)

Extension used of fasta files.

B<$LOG_EXTENSION>: (string)

Extension used of log files.

B<$DEFAULT_ADMIN_EMAIL>: (string)

e-mail address used as sender for notification e-mails.

=cut

Readonly my $DEBUG                        => 0;
Readonly my $DEFAULT_THREADS_COUNT        => 32;
Readonly my $FASTA_FILE_EXT               => '.fna';
Readonly my $LOG_EXTENSION                => '_log.txt';
Readonly my $DEFAULT_ADMIN_EMAIL          => 'm.rouard@cgiar.org';




# Script global variables
##########################

=pod

=head1 VARIABLES

B<@g_fasta_files>: (array of string)

Array of remaining fasta filenames to process.

B<$g_debug>: (boolean)

Debug flag.

B<$g_test>: (boolean)

Testing mode flag.

=cut

my @g_fasta_files :shared;
my $g_debug :shared;
my $g_test :shared;
my $g_computation_start_date :shared;




# Script global functions
##########################

=pod

=head1 FUNCTIONS

=head2 count_sequences_in_fasta

Expects a file name as input. The file is expected to be in fasta format.
Will return the number of sequences found in file.

=cut

sub count_sequences_in_fasta
{
    my ($file) = @_;
    my $total_size = qx/ grep "^>" $file | wc -l /;
    $total_size ||= 0;
    chomp $total_size;
    return $total_size;
}


=pod

=head2 run_pipeline_thread

B<Description>:
Runs a thread that launch pipelines on FASTA files to process.

B<ArgsCount>: 2

=over 4

=item arg: $slot_number (integer) (R)

Number of the thread in the thread list.

=item arg: $parameters (hash ref) (O)

additional parameters to take in account. See run_pipeline for available
options.

=back

B<Return>:

nothing.

=cut

sub run_pipeline_thread
{
    # since this procedure is run from threads, modules should be reloaded.
    use strict;
    use Carp qw (cluck confess croak);
    use warnings;
    use Readonly;
    use Error qw(:try);

    my ($slot_number, $parameters) = @_;

FASTA_FILES_LOOP:
    while (@g_fasta_files)
    {
        # loop on slots to find a free one
        my $fasta_file_path = '';
        {
            lock(@g_fasta_files);
            $fasta_file_path = shift(@g_fasta_files);
        }
        # make sure we got a fasta
        if ($fasta_file_path)
        {
            print STDERR "\nUsing slot $slot_number to process file '$fasta_file_path'\n" if $g_debug;
            run_pipeline($slot_number, $fasta_file_path, $parameters);
        }

        # check time limitations
        if (defined($parameters->{'time'})
            && (((time() - $g_computation_start_date)/60) >= $parameters->{'time'}))
        {
            last FASTA_FILES_LOOP;
        }
    }

}


=pod

=head2 run_pipeline

B<Description>:
launch GreenPhyl pipeline on the given fasta file. Thisprocedure is used by
threads.

B<ArgsCount>: 2-3

=over 4

=item $slot_number: (integer) (R)

Execution slot number to clear when done.

=item $fasta_file_path: (string) (R)

Name of the fasta file to process.

=item arg: $parameters (hash ref) (O)

Additional limitations that can restrict file processing can be specified using
this hash.
Available restrictions are:
'output' => (string) path of the output directory to use (instead of the
    FASTA directory as default);
'exact' => (integer) exact number of sequence the FASTA should contain in order to
    be processed;
'less' => (integer) only FASTA with less than the specified number of sequence
    will be processed;
'more' => (integer) only FASTA with more than the specified number of sequence
    will be processed;
'autoresume' => restart (overwrite) an analysis only if the last file has not
    been created or is empty;
'skip' => skip family when output directory is present;
'pipeparam' => additional parameters to give to run_pipeline.pl.

=back

B<Return>:

nothing.

B<Example>:

    run_pipeline(4, 'toto.fa');

=cut

sub run_pipeline
{
    my ($slot_number, $fasta_file_path, $parameters) = @_;

    if (!defined($slot_number))
    {
        cluck "WARNING: run_pipeline: thread number not specified!\n";
        return;
    }
    
    try
    {
        my $output_dir = $parameters->{'output'} || $GreenPhylConfig::OUTPUT_PATH;
        # parameters check
        if ((not defined $fasta_file_path) || ('' eq $fasta_file_path))
        {
            cluck "WARNING: run_pipeline: Fasta file name not specified! Pipeline execution aborted!\n";
            return;
        }

        if (not defined $slot_number)
        {
            cluck "WARNING: run_pipeline: Slot number not specified! Pipeline execution aborted for fasta file '$fasta_file_path'!\n";
            return;
        }
        if ($parameters && ('HASH' ne ref($parameters)))
        {
            cluck "WARNING: run_pipeline: Invalid 'parameters' argument (not a hash ref)!\n";
            return;
        }
        if (!$parameters)
        {
            $parameters = {};
        }

        my ($filename, $directory) = fileparse($fasta_file_path);
        my $family_base_name = $filename;
        $family_base_name =~ s/$FASTA_FILE_EXT$//;

        print "#$slot_number: -$filename\n";

        # check if already processed
        if (-d "$output_dir/$family_base_name")
        {
            # already processed, check if should be skipped
            if ($parameters->{'skip'})
            {
                print "#$slot_number:     skipped! (no overwrite)\n";
                return;
            }
            else
            {
                if ($parameters->{'autoresume'})
                {
                    print "#$slot_number:     Auto-resume\n";
                }
                else
                {
                    print "#$slot_number:     Overwrite\n";
                }
            }
        }

        # check if file exists
        if (!-e $fasta_file_path)
        {
            print "#$slot_number:     Error: file '$fasta_file_path' not found!\n";
            return;
        }
        elsif (!-r $fasta_file_path)
        {
            print "#$slot_number:     Error: file '$fasta_file_path' can not be read!\n";
            return;
        }

        # check limitations
        if (defined($parameters->{'exact'})
            || defined($parameters->{'less'})
            || defined($parameters->{'more'}))
        {
            my $seq_count = count_sequences_in_fasta($fasta_file_path);
            if (defined($parameters->{'exact'})
                && ($seq_count != $parameters->{'exact'}))
            {
                print "#$slot_number:     skipped! ($seq_count not matching 'exact' limitation)\n";
                return;
            }
            elsif ((defined($parameters->{'less'}))
                && ($seq_count >= $parameters->{'less'}))
            {
                print "#$slot_number:     skipped! ($seq_count not matching 'less' limitation)\n";
                return;
            }
            elsif ((defined($parameters->{'more'}))
                && ($seq_count <= $parameters->{'more'}))
            {
                print "#$slot_number:     skipped! ($seq_count not matching 'more' limitation)\n";
                return;
            }
            print "#$slot_number:     $seq_count sequences\n";
        }

        # check/set lock file
        # init lock manager:
        # -stale => 1: detect stale locks and break them if necessary
        # -hold  => 0: max life time (sec) of a lock. 0 prevents any forced unlocking.
        my $lockmgr = LockFile::Simple->make('-stale' => 1, '-hold' => 0);
        my $lock_name = "$output_dir/$family_base_name";
        # try to lock and skip if already locked
        if (!$lockmgr->trylock($lock_name))
        {
            print "#$slot_number:     skipped! (locked by another process - $lock_name)\n";
            return;
        }
        print "#$slot_number:     Lock FASTA\n";

        my $add_param = '';
        # check for additional parameters
        if ($parameters->{'pipeparam'})
        {
            $add_param .= $parameters->{'pipeparam'};
        }

        # check RAP mode
        if (0 >= $GreenPhylConfig::BOOTSTRAP_COUNT)
        {
            # 0 or negative: use RAP
            $add_param .= ' -rap';
        }

        # check if analyses should be restarted or resumed
        if ($parameters->{'autoresume'})
        {
            $add_param .= ' -autoresume';
        }

        # start timer to compute execution duration
        my $start_time = time();

        print "#$slot_number:     Processing...\n";
        try
        {
            my $log_filename = "$output_dir/$family_base_name$LOG_EXTENSION";
            my $command_line = "perl run_pipeline.pl -i $fasta_file_path -f $family_base_name -o '$output_dir' $add_param >$log_filename 2>&1";
            print "COMMAND: $command_line\n" if ($g_debug || $g_test); #+debug
            my $exit_code;
            if ($g_test)
            {
                sleep(0.05); # sleep 50 mili-seconds to let the system finish its stuff
                $exit_code = 0;
            }
            else
            {
                $exit_code = system($command_line);
                # give write access to group
                chmod(0664, $log_filename);
            }
            if ($exit_code)
            {
                print "#$slot_number:     An error occured! See '$log_filename' for details.\n";
            }
        }
        otherwise
        {
            cluck "#$slot_number: WARNING: Problem with FASTA file '$fasta_file_path': $!\n" # handle errors
        };
        print "#$slot_number:     Done!\n";

        print "#$slot_number:     Duration: " . (time() - $start_time) . " seconds\n";

        # release lock
        $lockmgr->unlock($lock_name);
        print "#$slot_number:     Lock released\n";
    }
    otherwise
    {
        print "#$slot_number:     An unexpected error occured!\n";
        print shift;
    };
}


=pod

=head2 processFastaDirectory

B<Description>: Process FASTA files of the given directory according to the
provided limitations.

B<ArgsCount>: 2-4

=over 4

=item arg: $nb_slots (integer) (R)

Number of thread to use to process directory.

=item arg: $fasta_directory (string) (R)

Full path to the directory containing FASTA files to process.

=item arg: $parameters (hash ref) (O)

Additional limitations on the files to process can be specified using this hash.
Available restrictions are:
'output_dir' => (string) path of the output directory to use (instead of the
    FASTA directory as default);
'reverse' => (boolean) if set to non-zero, file will be sorted in reverse
    alphabetical order otherwise files will be sorted in alphabetical order.
    This order hash an impact on the 'start' and 'end' limitations since it can
    invert starting and ending file names!
'start' => (string) name of the first file to process;
'end' => (string) name of the last file to process;
'time' => (integer) allowed processing time in minutes. After that delay, no
    more remaining FASTA file will be processed;
'exact' => (integer) exact number of sequence a FASTA should contain in order to
    be processed;
'less' => (integer) only FASTA with less than the specified number of sequence
    will be processed;
'more' => (integer) only FASTA with more than the specified number of sequence
    will be processed.

=item arg: $fasta_list (array ref) (O)

list of FASTA file names (path relative to $fasta_directory parameter)

=back

B<Return>: nothing

B<Example>:

    # this will process FASTA files in '/data/fasta/'
    # using 32 threads
    # starting from FASTA file '456.fa' down to '123.fa'
    # and will only process files if they have between 51 and 99 sequences
    # and will stop processing remaining FASTA file if the computation time
    # exceed 5 minutes.
    processFastaDirectory(32, '/data/fasta/',
        {
            'reverse' => 1,
            'start'   => '456.fa',
            'end'     => '123.fa',
            'time'    => 5,
            'less'    => 100,
            'more'    => 50,
        }
    );

=cut

sub processFastaDirectory
{
    my ($nb_slots, $fasta_directory, $parameters, $fasta_list) = @_;

    # check arguments
    if (!$nb_slots || (0 >= int($nb_slots)))
    {
        cluck "WARNING: processFastaDirectory: no number of thread provided!\n";
        return;
    }
    if (!$fasta_directory)
    {
        cluck "WARNING: processFastaDirectory: no directory to process provided!\n";
        return;
    }

    if ($parameters && ('HASH' ne ref($parameters)))
    {
        cluck "WARNING: processFastaDirectory: invalid 'parameters' argument (not a hash ref)!\n";
        return;
    }
    if (!$parameters)
    {
        $parameters = {};
    }
    if ($fasta_list && ('ARRAY' ne ref($fasta_list)))
    {
        cluck "WARNING: processFastaDirectory: invalid 'fasta_list' argument (not an array ref)!\n";
        return;
    }

    # make sure we got only one trailing slash
    $fasta_directory =~ s/\/+$//g;
    $fasta_directory .= '/';

    if (!-e $fasta_directory)
    {
        confess "ERROR: directory '$fasta_directory' not found!\n";
    }
    elsif (!-d $fasta_directory)
    {
        confess "ERROR: '$fasta_directory' is not a directory!\n";
    }
    elsif (!-r $fasta_directory)
    {
        confess "ERROR: directory '$fasta_directory' is not readable!\n";
    }

    if ($fasta_list && @$fasta_list)
    {
        foreach my $fasta_filename (@$fasta_list)
        {
            # remove FASTA directory if specified by error in file path
            $fasta_filename =~ s/^\Q$fasta_directory\E//;
            # check if an absolute path has been specified
            if ($fasta_filename =~ m/^\//)
            {
                # absolute
                push(@g_fasta_files, $fasta_filename);
            }
            else
            {
                # relative
                push(@g_fasta_files, $fasta_directory . $fasta_filename);
            }
        }
    }
    else
    {
        @g_fasta_files = glob($fasta_directory . '*' . $FASTA_FILE_EXT);
    }

    # sort list
    if ($parameters->{'reverse'})
    {
        @g_fasta_files = sort
        {
            if (($a =~ m/^(?:GP0*)?\d+\$FASTA_FILE_EXT$/)
                && ($b =~ m/^(?:GP0*)?\d+\$FASTA_FILE_EXT$/))
            {
                int(substr($b, 0, length($b)-3)) <=> int(substr($a, 0, length($a)-3));
            }
            else
            {
                $b cmp $a;
            }
        }
        @g_fasta_files;
    }
    else
    {
        @g_fasta_files = sort
        {

            if (($a =~ m/^(?:GP0*)?\d+\.fa$/) && ($b =~ m/^(?:GP0*)?\d+\.fa$/))
            {
                int(substr($a, 0, length($a)-3)) <=> int(substr($b, 0, length($b)-3));
            }
            else
            {
                $a cmp $a;
            }

        }
        @g_fasta_files;
    }

    # process file name restrictions
    if (defined($parameters->{'start'}))
    {
        # find first file to process and remove previous files
        while (@g_fasta_files && ($g_fasta_files[0] !~ m/$parameters->{'start'}$/))
        {shift(@g_fasta_files);}
    }
    if (defined($parameters->{'end'}))
    {
        # find last file to process and remove following files
        while (@g_fasta_files && ($g_fasta_files[$#g_fasta_files] !~ m/$parameters->{'end'}$/))
        {pop(@g_fasta_files);}
    }

    $g_computation_start_date = time();
    my $processed_fasta_count = 0;
    print "FASTA directory: $fasta_directory\nProcessing " . scalar(@g_fasta_files) . " files...\n";

    if (@g_fasta_files)
    {
        my @threads_list = ();
        # launch threads
        for (my $slot_number = 0; $slot_number < $nb_slots; ++$slot_number)
        {
            push(@threads_list, threads->create(\&run_pipeline_thread, $slot_number, $parameters));
        }
        # wait for last threads to end
        print "\nAlmost done...\n";
        foreach my $thread (@threads_list)
        {
            $thread->join();
        }
    }

    print "\nDONE!\n"; # :-)

    return;
}


=pod

=head2 validate_mail

B<Description>: validate a given e-mail address.

B<ArgsCount>: 1

=over 4

=item $target_email: (string) (R)

the target e-mail address to check.

=back

B<Return>: (boolean)

returns true (1) if the mail address is valid, false (0) otherwise.

B<Caller>:

general

B<Example>:

    my $target_mail = 'user@server.com';
    validate_mail($target_mail) or confess 'Not a valid e-mail address!\n';

=cut

sub validate_mail
{
    my ($target_email) = @_;

    # check parameters
    if (1 != @_)
    {
        confess "usage: validate_mail(target_email) or confess 'invalid e-mail address!';";
    }

    # RFC used:
    # -local mailbox: http://tools.ietf.org/html/rfc2822
    # -domain: http://tools.ietf.org/html/rfc1034
    my ($local_mailbox, $domain) = ($target_email =~ m/^(.*)@([a-zA-Z][a-zA-Z0-9\-]*(?:\.[a-zA-Z0-9\-]{1,63})*[a-zA-Z0-9])$/);
    if ((not $local_mailbox)
        || (not $domain)
        || (64 < length($local_mailbox))
        || (255 < length($domain)))
    {
        # invalid local address or domain
        return 0;
    }
    if (($local_mailbox !~ m/^[a-zA-Z0-9!~&'=_#\-\^\$\|\*\+\?\{\}\%\/\`]+(?:\.[a-zA-Z0-9!~&'=_#\-\^\$\|\*\+\?\{\}\%\/\`]+)*$/)
        && ($local_mailbox !~ m/^"(?:[^\\]*(?:\\"[^"]*\\")*(?:\\[^"])*)*"$/))
    {
        # invalid local address
        return 0;
    }
    return 1;
}


=pod

=head2 send_mail

B<Description>: Sends an e-mail.

B<ArgsCount>: hash

=over 4

=item email: (string) (R)

Target e-mail address.

=item subject: (string) (O)

Mail subject.

=item body: (string) (O)

Mail body.

=back

B<Return>: (nothing)

B<Example>:

    send_mail('email' => 'v.guignon@cgiar.org', 'subject' => 'Done', 'body' => 'done!');

=cut

sub send_mail
{
    my (%parameters) = @_;
    
    my $target_email = $parameters{'email'};
    my $mail_subject = $parameters{'subject'};
    my $mail_body    = $parameters{'body'};
    
    if (!$target_email)
    {
        confess "ERROR: send_mail: no target e-mail address provided!\n";
    }
    elsif (!validate_mail($target_email))
    {
        confess "ERROR: Invalid e-mail address!\n";
    }

    if (!$mail_subject)
    {
        $mail_subject = "[GreenPhyl] Pipeline $$ (multithread)";
    }
    $mail_subject =~ s/[\n\r]+//g;

    if (!$mail_body)
    {
        $mail_body = "-GreenPhyl-";
    }
    $mail_body .= "\n";

    my $sendmail_h;
    if (open($sendmail_h, "|/usr/sbin/sendmail -t"))
    {
        print {$sendmail_h} "To: $target_email\n";
        print {$sendmail_h} "From: $DEFAULT_ADMIN_EMAIL\n";
        print {$sendmail_h} "Subject: $mail_subject\n\n";
        print {$sendmail_h} $mail_body;
        close($sendmail_h);
    }
    else
    {
        warn "WARNING: Failed to send notrification e-mail!\n$!\n";
    }
}


# Script options
#################

=pod

=head1 OPTIONS

    run_pipe_in_multithread.pl [-help] [-man] [-debug] [-test]
        <-dir DIRECTORY | -fid FILENAME>
        [-output <DIRECTORY>]
        [-s <NB_SLOTS>] [-noreverse|-reverse] [-overwrite] [-autoresume]
        [-email <email>]
        [-pipeparam <"SPACED STRINGS" | STRING [STRING] ...>]
        [-limitations STRING [STRING] ...]

=head2 Parameters

=over 4

=item B<-help>:

display this help message.

=item B<-man>:

display user manual.

=item B<-debug>:

set debug mode on.

=item B<-test>:

does not launch the pipeline but outputs the command line and sleeps 3 seconds.

=item B<-dir DIRECTORY>:

DIRECTORY is the full path to the directory containing the FASTA files to
process. (Only files with the $FASTA_FILE_EXT file extension will be processed)

=item B<-fid FILENAMES>:

FILENAMES is a list (space-separated) of full path to FASTA files to process.

=item B<-output DIRECTORY>:

DIRECTORY is the full path to the directory where results will be output.

=item B<-s NB_SLOTS>:

Used to specify the number of SGE slots to use. Default: 32.

=item B<-noreverse|-reverse>:

By default '-reverse' is on and files are sorted and processed in reverse
alphabetical order (or reverse numeric order if only the name contains only numbers). When '-noreverse' is used, files will be sorted and
processed in alphabetical order.

=item B<-overwrite>:

By default, if the output directory exists for a family, the family is skipped.
To force these step execution, use the "-overwrite" parameter.

=item B<-autoresume>:

When set, if previous results exist, only missing results are computed.
Usefull to restart families that failed on various steps for various reasons.

=item B<-email> (string):

e-mail address that will receive an e-mail once all launched pipelines have
finished their job.

=item B<-pipeparam "STRINGS">:

Additional parameters that will be given to run_pipeline.pl.

=item B<-limitations STRING>:

This parameter can be used several times and the limitations will be cumulated.
STRING is a limitation string of the form:
1) "file1.fa-file2.fa": 'file1.fa' will be the first file processed and 'file2.fa'
    will be the last one;
2) "start=file1.fa": 'file1.fa' will be the first file processed;
3) "end=file2.fa": 'file2.fa' will be the last file processed;
4) "time=12": remaining FASTA files wont be processed after that delay in
    minutes (here 12 minutes);
5) "exact=50": only FASTA files containing 50 sequences will be processed;
6) "less=50": only FASTA files with less than 50 sequences will be processed;
7) "more=50": only FASTA files with more than 50 sequences will be processed;
8) "fasta_file.fa": a name of a FASTA file to process.

=back

=head2 Examples

    # will only process '1234.fa'
    run_pipe_in_multithread.pl -fid /data/pipeline/in/1234.fa -limitations

    # start from '1234.fa' and then will process '1235.fa', '1236.fa', ...
    run_pipe_in_multithread.pl -dir /data/pipeline/in -limitations start=1234.fa -noreverse

    # will process files between '1234.fa' and '5678.fa'
    run_pipe_in_multithread.pl -dir /data/pipeline/in -limitations start=5678.fa end=1234.fa

    # will process files down to '5678.fa' which have less than 75 sequences
    run_pipe_in_multithread.pl -dir /data/pipeline/in -limitations end=5678.fa less=75

    # will only process files '2345.fa', '3456.fa' and '4444.fa'
    run_pipe_in_multithread.pl -dir /data/pipeline/in -limitations 2345.fa 3456.fa 4444.fa

    # will process phylogeny analyses on files between '1234.fa' and '5678.fa'
    # will only process files containing between 51 and 79 sequences
    # and will not process unprocessed files after 5min
    run_pipe_in_multithread.pl -d /data/pipeline/in -l 5678.fa-1234.fa more=50 less=80 time=5

=cut


# CODE START
#############

++$| if !$|; # no buffering

print <<"___227_TITLE___";
######################################
#                                    #
# GreenPhyl pipeline in multithread  #
#                                    #
######################################
                                 v2.0

___227_TITLE___

# options processing
my ($help, $man, $fasta_directory , @fasta_file_path, @limitations, $overwrite,
    $reverse_order, @fasta_files, $output_dir, $nb_slots, $auto_resume,
    @pipe_parameters, $email);

# parse options and print usage if there is a syntax error.
GetOptions('help|?'             => \$help,
           'man'                => \$man,
           'debug'              => \$g_debug,
           'test'               => \$g_test,
           'dir|d=s'            => \$fasta_directory,
           'fid|f=s{,}'         => \@fasta_file_path,
           'output|o=s'         => \$output_dir,
           'reverse!'           => \$reverse_order,
           'overwrite|w'        => \$overwrite,
           'autoresume|a'       => \$auto_resume,
           'limitations|l=s{,}' => \@limitations,
           'pipeparam|p=s{,}'   => \@pipe_parameters,
           's=i'                => \$nb_slots,
           'email=s'            => \$email,
) or pod2usage(1);
if ($help) {pod2usage(0);}
if ($man) {pod2usage(-verbose => 2);}

$g_debug ||= $DEBUG;
$nb_slots ||= $DEFAULT_THREADS_COUNT;

if (!@fasta_file_path && ((not defined $fasta_directory) || ('' eq $fasta_directory))) {pod2usage(2);}

# parameters check
if (!$fasta_directory && !@fasta_file_path)
{
    print "ERROR: no file or directory to process!\n\n";
    pod2usage(2);
}

if ($fasta_directory && @fasta_file_path)
{
    print "ERROR: both file and directory specified! Only a directory or a list of files should be specified\n\n";
    pod2usage(2);
}

# if a list of files has been specified, init like if it was a directory
if (@fasta_file_path)
{
    # use current directory as directory
    $fasta_directory = './';
    @fasta_files = @fasta_file_path;
}

$output_dir = $output_dir || $fasta_directory || '';
if ($output_dir)
{
    $output_dir =~ s/\/+$//;
    $output_dir .= '/';
}
if ($output_dir && (!-e $output_dir))
{
    mkdir($output_dir, 0775) or warn "WARNING: failed to create directory '$output_dir' with the appropriate access rights!\n";
}


# process options
my %process_param = (
    'reverse'    => ($reverse_order?1:0),
    'skip'       => (($overwrite || $auto_resume)?0:1),
    'autoresume' => $auto_resume,
    'pipeparam'  => join(' ', @pipe_parameters),
    'output'     => $output_dir,
);
print "Reverse order: " . $process_param{'reverse'} . "\n";
foreach my $limitation (@limitations)
{
    print "Limitation: $limitation\n";
    # find limitation type
    if ($limitation =~ m/^([^-]+)-([^-]+)$/)
    {
        # limitation "start_filename"-"end_filename"
        $process_param{'start'} = $1;
        $process_param{'end'}   = $2;
        print "Will process files between (and including) '$1' and '$2'\n";
    }
    elsif ($limitation =~ m/^start=(.*)$/i)
    {
        # limitation start="start_filename"
        $process_param{'start'} = $1;
        print "Will start from file '$1'\n";
    }
    elsif ($limitation =~ m/^end=(.*)$/i)
    {
        # limitation end="end_filename"
        $process_param{'end'} = $1;
        print "Will end at file '$1'\n";
    }
    elsif ($limitation =~ m/^time=(.*)$/i)
    {
        # limitation time="duration"
        $process_param{'time'} = $1;
        print "Will stop processing new FASTA after $1 minutes\n";
    }
    elsif ($limitation =~ m/^exact=(\d+)$/i)
    {
        # limitation exact="num_seq"
        $process_param{'exact'} = $1;
        print "Will only process FASTA files with $1 sequences\n";
    }
    elsif ($limitation =~ m/^less=(\d+)$/i)
    {
        # limitation less="max_num_seq+1"
        $process_param{'less'} = $1;
        print "Will only process FASTA files with less than $1 sequences\n";
    }
    elsif ($limitation =~ m/^more=(\d+)$/i)
    {
        # limitation more="min_num_seq-1"
        $process_param{'more'} = $1;
        print "Will only process FASTA files with more than $1 sequences\n";
    }
    elsif ($limitation =~ m/^(.*\Q$FASTA_FILE_EXT\E)$/i)
    {
        # limitation fasta_file.fa
        push(@fasta_files, $1);
        print "Will process FASTA file $1\n";
    }
}

# process files
processFastaDirectory($nb_slots, $fasta_directory, \%process_param, \@fasta_files);

if ($email)
{
    send_mail(
        'email'   => $email,
        'subject' => "[GreenPhyl] Multithread Pipeline $$: done",
        'body'    => "All pipeline thread ended!",
    );
}

exit(0);


=pod

=head1 AUTHORS

Valentin GUIGNON (CIRAD), valentin.guignon@cirad.fr

Mathieu ROUARD (Bioversity-France), m.rouard@cgiar.org

=head1 VERSION

Version 1.4.0

Date 18/07/2011

=head1 SEE ALSO

http://perldoc.perl.org/perlthrtut.html

=cut
