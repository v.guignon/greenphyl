#!/usr/bin/env perl

=pod

=head1 NAME

manage_panspecies_output.pl - Gather consensus sequence alignments.

=head1 SYNOPSIS

    manage_panspecies_output.pl -species MUSAC -pan PANGENOME_DIR -ghe GET_HOMOLOGUES_EST_DIR -o OUTPUT_DIR

=head1 REQUIRES

Perl5

=head1 DESCRIPTION

Find the distance matrices (_multi.aln.distmat) and alignments (_multi.aln and
if available, _mono.faa.aln) for each consensus pangenome sequence and copy them
in the output directory using the pangene name + ".distmat", "_multi.aln" and
"_mono.aln".

Sample output file from GreenPhyl pangenome generation script:

/work/guignon/pangenomes/MUSAC_est_homologues/MUSACMacba2_rediso_0taxa_algOMCL_e0_F1.5_/9288_Maban_Contig228_t000230_mono_representative.txt
/work/guignon/pangenomes/MUSAC_est_homologues/MUSACMacba2_rediso_0taxa_algOMCL_e0_F1.5_/9288_Maban_Contig228_t000230_multi.aln.distmat
/work/guignon/pangenomes/MUSAC_est_homologues/MUSACMacba2_rediso_0taxa_algOMCL_e0_F1.5_/9288_Maban_Contig228_t000230_mono.faa.mafft.log
/work/guignon/pangenomes/MUSAC_est_homologues/MUSACMacba2_rediso_0taxa_algOMCL_e0_F1.5_/9288_Maban_Contig228_t000230_mono.faa.aln
/work/guignon/pangenomes/MUSAC_est_homologues/MUSACMacba2_rediso_0taxa_algOMCL_e0_F1.5_/9288_Maban_Contig228_t000230.faa
/work/guignon/pangenomes/MUSAC_est_homologues/MUSACMacba2_rediso_0taxa_algOMCL_e0_F1.5_/9288_Maban_Contig228_t000230_mono.faa

=cut

use strict;
use warnings;

use FindBin;
use lib "$FindBin::Bin";
use lib "$FindBin::Bin/../../website/lib";
use lib "$FindBin::Bin/../../website/local_lib";
use Carp qw (cluck confess croak);

use Bio::AlignIO;
use Bio::LocatableSeq;

use Greenphyl;
use GreenPhylConfig;
use Greenphyl::Log('nolog' => 1,);
use Greenphyl::Species;
use Greenphyl::Sequence;

use Getopt::Long;
use Pod::Usage;
use File::Copy qw(copy);

++$|; #no buffering




# Script global constants
##########################

=pod

=head1 CONSTANTS

B<$DEBUG>: (boolean)

When set to true, it enables debug mode.

=cut

our $DEBUG = 0;
our $CLUSTER_ALIGNMENT_SUFFIX       = '.faa.aln';
our $CLUSTER_MULTI_ALIGNMENT_SUFFIX = '_multi.aln';
our $CLUSTER_DISTANCE_MATRIX_SUFFIX = '_multi.aln.distmat';
our $CLUSTER_MONO_ALIGNMENT_SUFFIX  = '_mono.faa.aln';




# Script global functions
##########################

=pod

=head1 FUNCTIONS

=head2 generate_consensus_alignment

B<Description>: Generates a consensus sequence from a monogenic FASTA file.

B<ArgsCount>: 2

=over 4

=item $fasta_input_filename: (string) (R)

The path to the FASTA file to process.

=item $parameters: (hash) (R)

The script parameter hash.

=back

B<Return>: (list)

The first element is an amino-acid sequence (string) of the consensus, the
second element is an array ref of sequence names behind that consensus.

=cut

sub generate_consensus_alignment
{
    my ($sequence_accession, $aligned_sequence, $source_mono_aln, $target_mono_aln) = @_;

    my $in_align_obj = Bio::SeqIO->new(
        -file => $source_mono_aln,
        -format => 'fasta',
    );
    my $out_align_obj = Bio::SeqIO->new(
        -file => ">$target_mono_aln",
        -format => 'fasta',
    );

    while (my $seq_obj = $in_align_obj->next_seq)
    {
        # Add sequence alignment.
        $out_align_obj->write_seq($seq_obj);
    }

    # Add consensus sequence.
    $out_align_obj->write_seq(
        Bio::Seq->new(
            -id  => $sequence_accession,
            -seq => $aligned_sequence,
        )
    );

    return 1;
}




# Script options
#################

=pod

=head1 OPTIONS

    manage_panspecies_output.pl [-help | -man]
    manage_panspecies_output.pl <-species=CODE> <-pan=PAN_DIR> <-ghe=GHE_DIR> <-o=OUTPUT_DIR>

=over 4

=item B<-help>:

Prints a brief help message and exits.

=item B<-species>: (string)

Species code.

=item B<-pan>: (string)

Pangenome directory containing GreenPhyl pangenome generated files.

=item B<-ghe>: (string)

Get_Homologues-EST output directory for the given species.

=item B<-out>: (string)

Output directory where distance matrices and alignments should be copied.

=back

=cut


# CODE START
#############

umask 0022;

# options processing
my ($man, $help, $debug, $no_prompt) = (0, 0, undef, undef);
my ($species_code, $input_pangenome_dir, $input_get_homolog_est_dir, $output_dir, $genes) = (undef, undef, undef, undef, undef);
my $complete = 0;
# parse options and print usage if there is a syntax error.
GetOptions('help|?'      => \$help,
           'man'         => \$man,
           'debug:s'     => \$debug,
           's|species=s' => \$species_code,
           'pan=s'       => \$input_pangenome_dir,
           'ghe=s'       => \$input_get_homolog_est_dir,
           'o|out=s'     => \$output_dir,
           'genes=s'     => \$genes,
           'complete'    => \$complete,
           @Greenphyl::Log::LOG_GETOPT,
) or pod2usage(1);
if ($help) {pod2usage('-verbose' => 1, '-exitval' => 0);}
if ($man) {pod2usage('-verbose' => 2, '-exitval' => 0);}
if (!$species_code || !$input_pangenome_dir || !$input_get_homolog_est_dir || !$output_dir)
{pod2usage(1);}

# Change debug mode if requested/forced.
if (defined($debug))
{
    # just '-debug' ?
    if ($debug eq '')
    {
        $debug = 1;
    }
    $DEBUG = $debug;
}

# Check species code.
if ($species_code !~ m/$Greenphyl::Species::SPECIES_CODE_REGEXP/)
{
    confess LogError("Invalid species code: '$species_code'");
}

# Remove trailing slahes.
$input_pangenome_dir =~ s/\/+$//;
$input_get_homolog_est_dir =~ s/\/+$//;
$output_dir =~ s/\/+$//;

# Check pangenome dir.
my $cluster_file = $input_pangenome_dir . '/' . $species_code . '_clusters.txt';
if (!-d $input_pangenome_dir)
{
    confess LogError("Invalid pangenome directory: '$input_pangenome_dir'");
}
elsif ((!-s $cluster_file) || (!-r $cluster_file))
{
    confess LogError("Missing cluster file: '$cluster_file'");
}

if (!-d $input_get_homolog_est_dir)
{
    confess LogError("Invalid get-homologues_est directory: '$input_get_homolog_est_dir'");
}

if (!-d $output_dir)
{
    confess LogError("Invalid output directory: '$output_dir'");
}
elsif (!-w $output_dir)
{
    confess LogError("Output directory is write-protected: '$output_dir'");
}

if ($genes)
{
    LogInfo("Limiting transfer to pangenes matching regex '$genes'.");
}

if ($complete)
{
    LogInfo("Running in complete mode.");
}

my $fh;
# Read pangene alignments.
LogInfo("Reading aligned pangenes (ie. with gaps).");
my %pangene_alignments;
my $pangene_alignment_file = $input_pangenome_dir . '/' . $species_code . '.aln';
my $in_align_obj = Bio::AlignIO->new(
    -file => $pangene_alignment_file,
    -format => 'fasta',
);
while (my $maln = $in_align_obj->next_aln())
{
    foreach my $seq_obj ($maln->each_seq())
    {
        $pangene_alignments{$seq_obj->id} = $seq_obj->seq;
    }
}

# Read and parse cluster file.
LogInfo("Processing list of gene clusters.");
if (!open($fh, $cluster_file))
{
    confess LogError("Failed to open GHE cluster file '$cluster_file'! $!");
}

# For each cluster, find and copy associated files.
my $line_index = 1;
my $transfered_distance_matrices = 0;
my $transfered_multi_alignments = 0;
my $transfered_mono_alignments = 0;
while (defined(my $line = <$fh>))
{
  # Example line: "vitvi_pan_p000001:      58833_VvCabSauv08_P0534F.ver1.0.g615970.m01"
  if ($line =~ m/(\w+):\t(.+)/)
  {
      my ($pangene, $pangene_file_prefix) = ($1, $2);

      # Check if there is a gene filter to follow.
      if (!$genes || ($pangene =~ m/$genes/))
      {
          my $source_aln  = $input_get_homolog_est_dir . '/' . $pangene_file_prefix . $CLUSTER_ALIGNMENT_SUFFIX;
          my $source_multi_aln = $input_get_homolog_est_dir . '/' . $pangene_file_prefix . $CLUSTER_MULTI_ALIGNMENT_SUFFIX;
          my $source_distmat   = $input_get_homolog_est_dir . '/' . $pangene_file_prefix . $CLUSTER_DISTANCE_MATRIX_SUFFIX;
          my $source_mono_aln  = $input_get_homolog_est_dir . '/' . $pangene_file_prefix . $CLUSTER_MONO_ALIGNMENT_SUFFIX;
          my $target_directory = $output_dir . '/' . substr($pangene, 0 , -3) . '/';
          my $target_distmat   = $target_directory . $pangene . $Greenphyl::Sequence::CONSENSUS_DISTANCE_MATRIX_SUFFIX;
          my $target_multi_aln = $target_directory . $pangene . $Greenphyl::Sequence::CONSENSUS_MULTI_ALIGNMENT_SUFFIX;
          my $target_mono_aln  = $target_directory . $pangene . $Greenphyl::Sequence::CONSENSUS_MONO_ALIGNMENT_SUFFIX;
          
          # Check if we complete data and the alignment file has already been
          # transfered. We could check other files but we didn't for simplicity.
          if (!$complete || !-s $target_mono_aln)
          {
              # Create target directory if missing
              if (!-e $target_directory)
              {
                  mkdir $target_directory
                      or confess LogError("Failed to create directory '$target_directory'.");
              }

              if ((-e $source_distmat) && (-e $source_multi_aln))
              {
                  # Multigenic cluster.
                  # Copy distance matrix.
                  copy($source_distmat, $target_distmat)
                    or confess LogError("Copy of $source_distmat failed: $!");
                  ++$transfered_distance_matrices;
                  # Copy multigenic alignment.
                  copy($source_multi_aln, $target_multi_aln)
                    or confess LogError("Copy of $source_multi_aln failed: $!");
                  ++$transfered_multi_alignments;
                  # Copy generated monogenic alignment.
                  if (-e $source_mono_aln)
                  {
                      generate_consensus_alignment($pangene, $pangene_alignments{$pangene}, $source_mono_aln, $target_mono_aln)
                        or confess LogError("Failed to generate consensus alignment from '$source_mono_aln'!");
                      ++$transfered_mono_alignments;
                  }
                  # This warning would appear even when there is no $source_aln
                  # because the pangene represents multiple genes from only one
                  # genome. In this case, the monogenic FASTA only contains 1
                  # sequence: the genome representative. No consensus has to be
                  # computed. So the following warning is commented.
                  # else
                  # {
                  #     LogWarning("Missing monogenic alignment ($source_aln) or distance matrix ($source_distmat) and multiple alignment ($source_multi_aln) for pangene '$pangene' ($pangene_file_prefix)");
                  # }
              }
              elsif (-e $source_aln)
              {
                  # Monogenic cluster.
                  copy($source_aln, $target_mono_aln)
                    or confess LogError("Copy of $source_aln failed: $!");
                  ++$transfered_mono_alignments;
              }
              else
              {
                  LogWarning("Failed to find cluster alignment ($source_aln) or distance matrix ($source_distmat) and multiple alignment ($source_multi_aln) for pangene '$pangene' ($pangene_file_prefix)");
              }
          }
      }
  }
  elsif ($line)
  {
      confess LogError("Failed to parse cluster line $line_index:\n$line");
  }
  ++$line_index;
}
close($fh);

--$line_index;
LogInfo("Done processing $line_index lines.\nTransfered distances matrices: $transfered_distance_matrices\nTransfered multiple alignments: $transfered_multi_alignments\nTransfered mono-genic alignments: $transfered_mono_alignments");

exit(0);

__END__

# CODE END
###########

=pod

=head1 AUTHORS

Mathieu ROUARD (Bioversity), m.rouard@cgiar.org

Valentin GUIGNON (Bioversity), v.guignon@cgiar.org

=head1 VERSION

Version 1.0.0

Date 20/07/2020

=head1 SEE ALSO

GreenPhyl documentation.

=cut
