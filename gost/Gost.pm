# Copyright (C) 2002 Washington University School of Medicine
# and Howard Hughes Medical Institute
# All rights reserved
#
# Author: Christian M. Zmasek
# modified by Mathieu Rouard

package Gost;

use strict;
require Exporter;

our @ISA    = qw( Exporter );
our @EXPORT = qw( executePuzzleDQO
                  executePuzzleDQObootstrapped               
                  executeNeighbor
                  setModelForPuzzle
                  executePuzzleBootstrapped
                  executePuzzle
                  addDistsToQueryToPWDfile
                  testForTextFilePresence
                  isRFline
                  $LENGTH_OF_NAME
                  $SPECIES_NAMES_FILE                 
                  $MULTIPLE_TREES_FILE_SUFFIX
                  $ALIGN_FILE_SUFFIX
                  $TREE_FILE_SUFFIX
                  $SUFFIX_PWD
		          $SUFFIX_MASK
                  $SUFFIX_BOOT_STRP_POS
                  $SUFFIX_PWD_NOT_BOOTS
                  $SUFFIX_HMM
		          $SUFFIX_DICO
                  $MATRIX_FOR_PWD 
                  $NEIGHBOR
                  $CONSENSE
                  $PUZZLE 
                  $HMMALIGN
                  $HMMSEARCH
                  $HMMBUILD
                  $HMMFETCH
                  $SFE
                  $HMMCALIBRATE
                  $P7EXTRACT 
                  $MULTIFETCH 
                  $BOOTSTRAP_CZ
                  $BOOTSTRAP_CZ_PL
                  $MAKETREE
                  $TRANSFERSBRANCHLENGHTS
                  $PUZZLE_DQO
                  $BOOTSTRAPS
                  $PATH
                  $JAVA
                  $TEMP_DIR_DEFAULT
                  $MATRIX_DEFAULT      
 );
 
# Directories must end with a "/".

# RIO itself:
# -----------
our $PATH  = "/apps/GreenPhyl/v2/cgi-bin/lib/";        
 
# Java virtual machine:
# ---------------------
our $JAVA  = "/usr/local/jdk1.5.0_07/bin/java";


# Where all the temporary files can be created:
# ---------------------------------------------
our $TEMP_DIR_DEFAULT          = "/apps/GreenPhyl/v1/cgi-bin/htdocs/tmp/gost/";  # must end with "/" 
 
 
# HMMER:
our $HMMALIGN                  = $PATH . "dependencies/hmmer/binaries/hmmalign";
our $HMMSEARCH                 = $PATH . "dependencies/hmmer/binaries/hmmsearch";
our $P7EXTRACT                 = $PATH . "dependencies/perl/p7extract.pl";
our $MULTIFETCH                = $PATH . "dependencies/perl/multifetch.pl";
our $SFE                       = $PATH . "dependencies/hmmer/binaries/sfetch"; #multifetch.pl

# TREE-PUZZLE:
our $PUZZLE                    = $PATH . "dependencies/puzzle_mod/src/puzzle";
our $PUZZLE_DQO                = $PATH . "dependencies/puzzle_dqo/src/puzzle";

# PHYLIP
our $NEIGHBOR                  = $PATH . "dependencies/phylip_mod/exe/neighbor"; 
our $SEQBOOT                   = $PATH . "dependencies/phylip_mod/exe/seqboot";
our $CONSENSE                  = $PATH . "dependencies/phylip_mod/exe/consense";

# RIO/FORESTER:
our $BOOTSTRAP_CZ              = $PATH . "dependencies/C/bootstrap_cz";
our $BOOTSTRAP_CZ_PL           = $PATH . "dependencies/perl/bootstrap_cz.pl";
our $TRANSFERSBRANCHLENGHTS    = "java forester.tools.transfersBranchLenghts"; # sill exists?

our $MAKETREE                   = $PATH  . "dependencies/perl/makeTree.pl";
our $SPECIES_TREE_FILE          = $PATH  . "/Tree/Tree_of_life.nhx";
OUR $CONVERT_ID                 = $PATH  . "/dependencies.1/perl/id_convert.pl";

our  $mode              = 0;
our $E_VALUE_THRESHOLD  = 0.01;

our $MATRIX_DEFAULT     = 2;
# Data for using precalculated distances:
# ---------------------------------------
our $MATRIX_FOR_PWD     = 2;  # The matrix which has been used for the pwd in $RIO_PWD_DIRECTORY.
                              # 0=JTT, 1=PAM, 2=BLOSUM 62, 3=mtREV24, 5=VT, 6=WAG.
                                     
our $BOOTSTRAPS         = 100;
our $MIN_NUMBER_OF_AA   = 100;  # After removal of gaps, if less, gaps are not removed.
our $LENGTH_OF_NAME     = 26;



our $SUFFIX_BOOT_STRP_POS        = ".fa.sb.bsp";
our $SUFFIX_MASK                 = ".fa.mask.bsp";
our $SUFFIX_PWD_NOT_BOOTS        = ".fa.phy.dist";
our $SUFFIX_HMM                  = ".hmm";



# Funtions
# ---------------------------------------


sub alignWithHmmalign 
{
    my ($alignment, $query, $hmm, $outfile, $use_mapali) = @_;        
    
    my $E                   = 2000;
    my $ali = "--withali";
    
    if ( $use_mapali == 1 ) {
        $ali = "--mapali";
    }

    &testForTextFilePresence( $alignment );
    &testForTextFilePresence( $query );
    &testForTextFilePresence( $hmm );
  
    system( "$HMMSEARCH $hmm $query > $temp_dir/HMMSEARCHOUT" )
    and die( "Could not execute \"$HMMSEARCH $hmm $query > $temp_dir/HMMSEARCHOUT\"" );

   
    $E = &getEvalue( "$temp_dir/HMMSEARCHOUT" );
    if ( $E == 2000 ) 
    {   
        die ( "No E-value found in \"$temp_dir/HMMSEARCHOUT\"" );
    }
    elsif ( $E > $E_VALUE_THRESHOLD ) 
    {
        unlink( "$temp_dir/HMMSEARCHOUT" );
        return ( -1 );
    }
    
    #Convert to GDF format
    system( "$P7EXTRACT -d $temp_dir/HMMSEARCHOUT > $temp_dir/GDF" )
    and die ( "Could not execute \"$P7EXTRACT -d $temp_dir/HMMSEARCHOUT > $temp_dir/GDF\"" );

    #fetch sequence list
    system( "$MULTIFETCH  -d -g $query $temp_dir/GDF > $temp_dir/MULTIFETCHOUT" )
    and die ( "Could not execute \"$MULTIFETCH  -d -g $query $temp_dir/GDF > $temp_dir/MULTIFETCHOUT\"" );

    # Checks if score was too low to have made a reasonable alignment. 
    unless ( -s "$temp_dir/MULTIFETCHOUT" ) 
    { 
        return ( -1 );
    }
  
    system( "$HMMALIGN -o $outfile $ali $alignment $hmm $temp_dir/MULTIFETCHOUT >/dev/null 2>&1" )
    and die( "Could not execute \"$HMMALIGN -o $outfile $ali $alignment $hmm $temp_dir/MULTIFETCHOUT\"" );

    return 1; 
}


# Moves sequences which start with query name (argument 1) 
# to the last positions in pfam alignment sepecified by argument 2.
# Removes seqs present in argument 4, unless for query name.
# Four arguments:
# 1. Query name
# 2. Infile (alignment)
# 3. Outfile (=infile with query seq moved to the bottom)
# 4. Array of seq names to remove, unless for query name
# Last modified: 06/25/01
sub moveToLast 
{

    my $query       = $_[ 0 ];
    my $infile      = $_[ 1 ];
    my $outfile     = $_[ 2 ];
    my @to_remove   = @{ $_[ 3 ] };  # @{} tells Perl that this is a list.
    my $return_line = "";
    my $query_line  = "";
    my $n           = "";    

    &testForTextFilePresence( $infile );

    open( MTL_IN, "$infile" ) || die( "Cannot open file \"$infile\"" );
    open( MTL_OUT, ">$outfile" ) || die( "Cannot create file \"$outfile\"" );

    my $text = "# STOCKHOLM 1.0";
    print MTL_OUT $text;

    while ( $return_line = <MTL_IN> ) 
    {
    
        if ( @to_remove > 1 ) {
            foreach $n ( @to_remove ) {
                if ( $n ne $query && $return_line =~ /^\s*$n\s+/ ) {
                    next;
                }
            }
        }
        if ( $return_line =~ /^\s*$query\s+/ ) {
  
            $query_line = $return_line;
        }
        elsif ( $query_line ne "" 
        && ( $return_line !~ /\S+/ || isRFline( $return_line ) ) ) {
            print MTL_OUT $query_line;
            print MTL_OUT $return_line;
            $query_line = "";
        }
        else {
            print MTL_OUT $return_line;
        }
    }
    if ( $query_line ne "" ) { 
    
        print MTL_OUT $query_line;
    }
    close( MTL_IN );
    close( MTL_OUT );
    return; 
} 



sub CONVERT_dist 
{
    my $matrices   = $_[ 0 ];
    my $dist_query = $_[ 1 ];
    my $OUT        = $_[ 2 ];

    system( "sort_dist.pl $matrices $dist_query > $OUT" ) 
    && die("Could not execute SORT_DIST");
    return;
}



# Prend sortie HMMALIGN (MOVE TOLAST OUT) et va remettre bon align en amont et en aval
sub HMMparser 
{
    my $movetolast  = $_[ 0 ];
    my $rascal      = $_[ 1 ];
    my $bsp1        = $_[ 2 ];
    my $OUT         = $_[ 3 ];
    my $filename    = $_[ 4 ];
  
    system( "hmm_parser_M.pl $movetolast $rascal $bsp1 $OUT $filename" ) 
    and die("Could not execute MASK code");
    return;
}


# Two arguments:
# 1. Name of inputfile
# 2. matrix option: 0 = JTT; 2 = BLOSUM 62; 3 = mtREV24;
#    5 = VT; 6 = WAG; 7 = auto; PAM otherwise 
sub executePuzzleDQO {
    my $in            = $_[ 0 ];
    my $matrix_option = $_[ 1 ];
    my $mat           = "";
    
    &testForTextFilePresence( $in );

    $mat = setModelForPuzzle( $matrix_option );

   
    system( "$PUZZLE_DQO $in >/dev/null 2>&1 << !$mat
y
!" )
    && die( "Could not execute \"$PUZZLE_DQO\"" );
    
    return;

} 



# Two arguments:
# 1. Name of inputfile
# 2. matrix option: 0 = JTT; 2 = BLOSUM 62; 3 = mtREV24;
#    5 = VT; 6 = WAG; 7 = auto; PAM otherwise
# Last modified: 01/28/02

sub executePuzzleDQObootstrapped {
    my $in            = $_[ 0 ];
    my $matrix_option = $_[ 1 ];

    my $l             = 0;
    my $slen          = 0;
    my $counter       = 0; 
    my $mat           = "";
    my $a             = "";
    my @a             = ();

    &testForTextFilePresence( $in );

    open( GRP, "<$in" ) || die( "Cannot open file \"$in\"" );
    while( <GRP> ) 
    { 
    #va recup juste le nombre de seq et la taille alignement
        if ( $_ =~ /^\s*\d+\s+\d+\s*$/ ) 
        {        
            $counter++; 
        } 
    }
    close( GRP ); 
    
    $l   = `cat $in | wc -l`;
   
    $slen   = $l / $counter;

    system( "split -$slen $in $in.splt." )
    && die( "Could not execute \"split -$slen $in $in.splt.\"" );

    @a = <$in.splt.*>;

    $mat = setModelForPuzzle( $matrix_option );
    
    my $cpt = 0;
    foreach $a ( @a ) 
    {
       
    $cpt ++;
       
    system( "$PUZZLE_DQO $a >/dev/null 2>&1 << !$mat
y 
!" )  && die( "Could not execute \"$PUZZLE_DQO $a\"" );

    system( "cat $a.dist >> $in.dist" )
    && die( "Could not execute \"cat outdist >> $in.dist\"" );
  
    unlink( $a, $a.".dist" );
    }
    
    return;
}

# Five arguments:
# 1. pairwise distance file
# 2. number of bootstraps
# 3. randomize_input_order: 0: do not randomize input order; >=1 jumble
# 4. seed for random number generator
# 5. lower-triangular data matrix? 1: yes; no, otherwise
sub executeNeighbor 
{
    my $inpwd  = $_[ 0 ];
    my $bs     = $_[ 1 ];
    my $rand   = $_[ 2 ];
    my $s      = $_[ 3 ];
    my $l      = $_[ 4 ];
    my $jumble = "";
    my $multi  = "";
    my $lower  =  "";
    
    
    &testForTextFilePresence( $inpwd );

    if ( $rand >= 1 ) {
        $jumble = "
J
$s"; 
    }
   
    if (  $bs >= 2 ) {
        $multi = "
M
$bs
$s";
    }
    if ( $l == 1 ) {
        $lower = "
L"; 
    }

#print "TEST : $NEIGHBOR $inpwd $bs $rand $s $l $jumble $multi $lower\n";

    system( "$NEIGHBOR >/dev/null 2>&1 << !
$inpwd$jumble$multi$lower
2
3
Y
!" )
    && die( "Could not execute \"$NEIGHBOR $inpwd$jumble$multi$lower\"" );
    # 3: Do NOT print out tree
    
      
    return;

} 



# "Model of substitution" order for DQO TREE-PUZZLE 5.0:
# Auto
# m -> Dayhoff (Dayhoff et al. 1978)
# m -> JTT (Jones et al. 1992)
# m -> mtREV24 (Adachi-Hasegawa 1996)
# m -> BLOSUM62 (Henikoff-Henikoff 92)
# m -> VT (Mueller-Vingron 2000) 
# m -> WAG (Whelan-Goldman 2000)
# m -> Auto
# One argument:
# matrix option: 0 = JTT; 2 = BLOSUM 62; 3 = mtREV24;
# 5 = VT; 6 = WAG; 7 = auto; PAM otherwise
# Last modified: 07/07/01
sub setModelForPuzzle 
{
    my $matrix_option = $_[ 0 ];
    my $matr          = "";

    if ( $matrix_option == 0 ) { # JTT
        $matr = "
m
m";
    }
    elsif ( $matrix_option == 2 ) { # BLOSUM 62
        $matr = "
m
m
m
m";   
    }
    elsif ( $matrix_option == 3 ) { # mtREV24
        $matr = "
m
m
m";
    }
    elsif ( $matrix_option == 5 ) { # VT 
        $matr = "
m
m
m
m
m";
    }
    elsif ( $matrix_option == 6 ) { # WAG
        $matr = "
m
m
m
m
m
m";
    }
    elsif ( $matrix_option == 7 ) { # auto
        $matr = "";
    }          
    else { # PAM
        $matr = "
m"       
    }   

    return $matr;

} 



# Two arguments:
# 1. Name of inputfile
# 2. matrix option: 0 = JTT; 2 = BLOSUM 62; 3 = mtREV24;
#    5 = VT; 6 = WAG; 7 = auto; PAM otherwise
# Last modified: 01/28/02
sub executePuzzleBootstrapped 
{
    my $in            = $_[ 0 ];
    my $matrix_option = $_[ 1 ];

    my $l             = 0;
    my $slen          = 0;
    my $counter       = 0; 
    my $mat           = "";
    my $a             = "";
    my @a             = ();
    
    &testForTextFilePresence( $in );
   
    open( GRP, "<$in" ) || die "\n\n$0: Unexpected error: Cannot open file <<$in>>: $!";
    while( <GRP> ) { 
        if ( $_ =~ /^\s*\d+\s+\d+\s*$/ ) { 
            $counter++; 
        } 
    }
    close( GRP ); 

	$l   = `cat $in | wc -l`;
	$slen   = $l / $counter;

	system( "split -$slen $in $in.splt." )
    && die "\n\n$0: executePuzzleDQObootstrapped: Could not execute \"split -$slen $in $in.splt.\": $!";
    
    @a = <$in.splt.*>;
   
    $mat = setModelForPuzzle( $matrix_option );

    foreach $a ( @a ) {
        print "-".$a."\n";        
        system( "$PUZZLE $a << !
k
k$mat
y 
!" )
        && die "$0: Could not execute \"$PUZZLE $a\"";

        system( "cat $a.dist >> $in.dist" )
        && die "$0: Could not execute \"cat outdist >> $in.dist\"";
  
        unlink( $a, $a.".dist", $a.".tree" );
    }
    
    return;
} 


# Two arguments:
# 1. Name of inputfile
# 2. matrix option: 0 = JTT; 2 = BLOSUM 62; 3 = mtREV24;
#    5 = VT; 6 = WAG; 7 = auto; PAM otherwise 
sub executePuzzle 
{
    my $in            = $_[ 0 ];
    my $matrix_option = $_[ 1 ];
    my $mat           = "";
    
    &testForTextFilePresence( $in );

    $mat = setModelForPuzzle( $matrix_option );

    system( "$PUZZLE $in << !
k
k$mat
y
!" )
    && die "$0: Could not execute \"$PUZZLE\"";
    
    return;

} 



# Preparation of the pwd file
sub addDistsToQueryToPWDfile 
{
    my $pwd_file          = $_[ 0 ];
    my $disttoquery_file  = $_[ 1 ];
    my $outfile           = $_[ 2 ];
    my $name_of_query     = $_[ 3 ];
    
    my $name_of_query_    = ""; 
    my $return_line_pwd   = "";
    my $return_line_dq    = "";
    my $num_of_sqs        = 0;
    my $block             = 0;
    my $name_from_pwd     = "X";
    my $name_from_dq      = "Y";
    my @dists_to_query    = ();
    my $i                 = 0;
 
 
    &testForTextFilePresence( $pwd_file );
    &testForTextFilePresence( $disttoquery_file );
    $name_of_query=~ s/(\S+_\S+)\/\d+\-\d+/$1/g;
    $name_of_query_ = $name_of_query;
    
    for ( my $j = 0; $j <= ( $LENGTH_OF_NAME - length( $name_of_query ) - 1 ); ++$j ) 
    {
	    $name_of_query_ .= " ";
    }	
    open( OUT_AD, ">$outfile" ) || die( "Cannot create file \"$outfile\"" );
    open( IN_PWD, "$pwd_file" ) || die( "Cannot open file \"$pwd_file\"" );
    open( IN_DQ, "$disttoquery_file" ) || die( "Cannot open file \"$disttoquery_file\"" );
  
   W: while ( $return_line_pwd = <IN_PWD> ) 
   {
        # valeurs horizontales
	    chomp($return_line_pwd);
	    ### Print value query 
        if ( $return_line_pwd =~ /^\s*(\d+)\s*$/ ) 
        {
	        last if $block == 100;
            $num_of_sqs = $1;
            $num_of_sqs++;
            if ( $block > 0 ) 
            {
        	    print OUT_AD "$name_of_query_";		  
                for ( my $j = 0; $j < $i; ++$j ) 
                {
                    print OUT_AD "$dists_to_query[ $j ]  ";
		        }
                print OUT_AD "0.00000\n";
            }
            print OUT_AD "  $num_of_sqs\n";
            $block++;
            @dists_to_query = ();
            $i = 0;
        }
	    ### Print matrice + Query
        elsif ( $block > 0 && $return_line_pwd =~ /^\s*(\S+)\s+\S+/ ) 
        {	
            $name_from_pwd = $1;
            if ( !defined( $return_line_dq = <IN_DQ> ) ) 
            {
                die( "\"$disttoquery_file\" seems too short" );
            }
            if ( $return_line_dq !~ /\S/ ) 
            {
                if ( !defined( $return_line_dq = <IN_DQ>) ) {
                    die( "\"$disttoquery_file\" seems too short" );
                }
            }
            $return_line_dq =~ /^\s*(\S+)\s+(\S+)/;
            $name_from_dq = $1;
            $dists_to_query[ $i++ ] = $2;
	        my $dist_to_query = $2;
	        chomp( $dist_to_query);
	        $return_line_pwd=~ s/^(SEQ\d{7})\s+(\S+\s+)/$1                $2/g;
            if ( $name_from_pwd ne $name_from_dq ) 
            {
                die( "Order of sequence names in \"$pwd_file\" and \"$disttoquery_file\" is not the same: $name_from_pwd - $name_from_dq" );
            }
            print OUT_AD $return_line_pwd. "  ". $dist_to_query."\n";
        }
    }  
    ### Juste la derni�re ligne !!!
    print OUT_AD "$name_of_query_";
    for ( my $j = 0; $j < $i; ++$j ) 
    {
        print OUT_AD "$dists_to_query[ $j ]  ";
    }
    print OUT_AD "0.00000\n";

    close( OUT_AD );
    close( IN_PWD );
    close( IN_DQ );
    return $block;  
} 


sub executeBootstrap_cz 
{
    my $boots       = $_[ 0 ];
    my $bsp_file    = $_[ 1 ];
    my $infile      = $_[ 2 ];
    my $outfile     = $_[ 3 ];
    my $processors  = $_[ 4 ];

    $BOOTSTRAP_CZ = "/apps/GreenPhyl/v1/cgi-bin/lib/RIO1.1/C/bootstrap_cz";
    
    system( "$BOOTSTRAP_CZ $boots $infile $bsp_file $outfile " )
        and die( "Could not execute \"$BOOTSTRAP_CZ $boots $infile $bsp_file $outfile\"" );       
} 


# Four arguments:
# 1. options ("-" is not necessary)
# 2. alignment or pwd file
# 3. outfile
# 4. temp dir

sub executeMakeTree 
{
    my $opts         = $_[ 0 ]; 
    my $B            = $_[ 1 ];
    my $C            = $_[ 2 ];
    my $D            = $_[ 3 ];
    
    &testForTextFilePresence( $B );

    $opts = "-".$opts;

    system( "$MAKETREE $opts $B $C $D" )
    && die( "Could not execute \"$MAKETREE $opts $B $C $D\"" ); 
    
} 

# One argument:
# options for DoRIO.main.

sub executeDoRIO {

    my $options = $_[ 0 ]; 
    system("$DORIO $options") && die( "Could not execute \"$DORIO $options\"" );

    return;

} 




#### sub functions


# Gets the E value for complete sequences (score includes all domains)
# from a HMMSEARCH output file.
# One argument: the HMMSEARCH output file name
# Returns the E value, 2000 if no E value found
sub getEvalue 
{

    my $infile      = $_[ 0 ];
    my $return_line = "";
    my $flag        = 0;
    my $E           = 2000;

    &testForTextFilePresence( $infile );

    open( E, "$infile" ) || die ( "Cannot open file \"$infile\"" );
    while ( $return_line = <E> ) {
        
        # "Sequence    Description                                 Score    E-value  N"  
        if ( $return_line =~ /Sequence.+Description.+Score.+E.+value.+N/ ) { 
            $flag = 1; 
        }
        # "QUERY_HUMAN                                             657.4   1.3e-198   1"
        elsif ( $flag == 1 && $return_line =~ /\s+(\S+)\s+\d+\s*$/ ) { 
            $E = $1;
            close( E );
            return $E;
        }  
    }
    close( E );
    return $E;

}

# This gets the complete name(s) of a sequence from a Pfam alignment.
# I.e. it adds "/xxx-xxx".
# 2 arguments:
# 1. Infile (alignment)
# 2. Name of query
# Returns a String-array of all the complete names found.
# Last modified: 03/04/01
sub getCompleteName 
{

    my $infile         = $_[ 0 ];
    my $query_name     = $_[ 1 ];
    my $return_line    = "";
    my @complete_names = ();
    my $complete_name  = "";
    my $i              = 0;
    
    &testForTextFilePresence( $infile );

    $query_name =~ s/\/.*//;

    open( INGCN, $infile ) || die( "Cannot open file \"$infile\"" );
    while ( $return_line = <INGCN> ) {
        if ( $return_line =~ /^\s*$query_name(\S+)\s+.+/ ) {
            $complete_name = $query_name.$1;
            if ( $i > 0 && $complete_names[ 0 ] eq $complete_name ) {
                # Now, we saw of all of them.
                last;
            }
            $complete_names[ $i++ ] = $complete_name;
        }
    }
    
    close( INGCN );
    return @complete_names; 

} 

# Checks wether a file is present, not empty and a plain textfile.
# One argument: name of file.
sub testForTextFilePresence 
{
    my $file = $_[ 0 ];
    unless ( ( -s $file ) && ( -f $file ) && ( -T $file ) ) {
        die ( "File \"$file\" does not exist, is empty, or is not a plain textfile" );
    }
} 


# Returns whether the argument starts with XXX XXXXX. 
# Last modified: 06/21/01
sub isRFline 
{
    return ( $_[ 0 ] =~ /^#.*RF/ );
}



1;
