#!/usr/bin/perl

use Bio::AlignIO;
use Data::Dumper;
use File::Temp qw/ tempfile tempdir /;

#perl hmm_parser_M.pl MOVETOLAST.txt OUT_rascal OUT_mask.bsp

# Ouvre l'alignement multiple avec la query: (MOVETOLAST)
my $stream  = Bio::AlignIO->new(
				-file => $ARGV[0],
				'-format' => 'stockholm'
);

# Ouvre l'alignement d'origine  : (family.fa.rascal )
my $stream2  = Bio::AlignIO->new(
				 -file => $ARGV[1],
				 '-format' => 'fasta'
);

my $aln2 = $stream2->next_aln();
my @seq2 = $aln2->each_seq();
my $length2 = $aln2->length();
my $outfile = $ARGV[3];
my $filename = $ARGV[4];

## Initialisation des variables;
my $cpt = 0;


my %start;
my %end;
my $no_sequences;
my $length;
my @gapToRemove;


## Nombre d'acide aminé �  extraire �  partir du 1er aa en majuscule ou avant le dernier aa en majuscule
my $pas            = 20;
my $pos_start      = 0;
my $pos_end        = 0;
my $LENGTH_OF_NAME = 26;
my $stop           = 0;
my $indice_start   = 0;
my $indice_end     = 0;

	
## Pour chaque alignement
while(my $aln = $stream->next_aln) {
    $no_sequences = $aln->no_sequences;
    $length = $aln->length();
    ## Pour chaque sequence de l'alignement
    ## Objectif : recherche une fenetre de 20AA dans alignement avec query et revenir sur alignement d'origine pour trouver position (n�colonne) de debut et de fin 
    my %lc_pos;
     
    foreach my $seq ($aln->each_seq) {
	### Sort si query
	#print $seq->id."\n";
	
	### 1 . Recherche les positions des minuscules entre position start et end !!!
        
	if (($cpt + 1) == $no_sequences){
        	for ( my $i=1; $i<=$length; $i++ ) {
		
        	 	next if ($i < $pos_start);
        	 	last if ($i > $pos_end);
			
          		 if ($seq->subseq($i,$i) =~ /[a-z]/) {
			    my $minus = $seq->subseq($i,$i);
            	 	    my $gapo = $i - $pos_start;
			    
			 #fonctionne car query est � la fin : repere les minuscule uniquement dans la query pour les supprimer car ils introduisent un gap.		    
	    		 push @gapToRemove, $gapo unless ($lc_pos{$gapo});
			 #print $lc_pos{$gapo}."$i\n";
		 		#print "Trouve minuscules-----".$minus."--en position--".$gapo." <br />\n";
				#print "-".$gapo."-";
           		} 
        	}
		last;
	}
	
	
	my $state;
	### 2. Recherche de la position en debut: $start
	for ( my $i=1; $i<=$length; $i++ ) {
	
	    my $aa_start = $seq->subseq($i, $i);
	    
	    ## Cherche le 1er aa en majuscule
	    if ( $aa_start =~ /[A-Z-]/ &&  $state != 1) {
		$pos_start = $i;
		$state = 1;
		my $seq = $seq->subseq($i, $length);
		## Recupere la sequence de $i a $pas
		$seq = uc((substr($seq,0,$pas)));
		$seq =~ s/\-/\./g;
		#print "START HMMALIGN".$pos_start." valeur ".$seq."<br />\n";
		## Recherche du motif dans la sequence Rascal
		if ($seq =~ /[A-Z]/){
	
			my $index = rindex($seq2[$cpt]->seq,$seq) + 1;
			## Passe si le motif n'est pas trouvé
			next if $index == 0;
			## Compte pour chaque position le nb de fois ou on trouve l'occurence
			$start{$index}++;
		
		#print "START rascal".$index."<br />\n";
		}
		
	    }
	    #Pour conserver la position des minuscules dans les seqeunces
	    elsif ($aa_start =~ /[a-z]/ && ($pos_start)) {
	        
	          my $minus = $seq->subseq($i,$i);
                  my $gapo = $i - $pos_start;
	          #print $gapo."\t".$minus."\n";
	          #repere les minuscule dans les sequences de l'alignement d'origine.
	          $lc_pos{$gapo} = $minus ;
              } 	   
	}
	
	
	### 3. Recherche la position en fin: $end
	for ( my $j=$length; $j<=$length; $j-- ) {
	    my $aa_end = $seq->subseq($j, $j);
	    ## Cherche le dernier aa en majuscule
	    if ( $aa_end =~ /[A-Z-]/ ) {
		$pos_end = $j;
		my $motif = $seq->subseq($j-$pas, $j);
		my $seq = uc($seq->subseq($j-$pas, $j));
		$seq =~ s/\-/\./g;
		## Recherche du motif dans la sequence Rascal
		my $index = rindex($seq2[$cpt]->seq,$seq) + $pas +1;
		## Passe si pas de motif
		next if $index == $pas;
		$end{$index}++;
		last;
	    }
	}
	$cpt++;
    }
}
#print Dumper @gapToRemove;
### Recupere l'indice de debut ayant le plus grand nb d'occurence
foreach my $start (sort {$start{$b}<=>$start{$a}}keys%start) {
    $indice_start = $start;
    last;
}
### Recupere l'indice de fin ayant le plus grand nb d'occurence
foreach my $end (sort {$end{$b}<=>$end{$a}}keys %end) {
    $indice_end = $end;
    last;
}

my $stop = $length2 - $pos_end;

#print "MOVETOLAST : $pos_start - $pos_end<br />RASCAL : $indice_start - $indice_end<br />";

## Ouvre le bsp avec les positions conserves du masking
open ( G, "<$ARGV[2]" ) or die "Cannot read file : $!";
my @positions = split ( / /, <G>);
close G;
# Recup le nombre de position a recuperer via le bsp
my $cpt2 = scalar @positions;


#open ( FHTEMP, ">$outfile"."$filename"."_tmp") or die ('Cannot create fasta alignment');
open ( FHTEMP, ">$outfile"."_tmp") or die ('Cannot create fasta alignment');
open ( OUTT, ">$outfile") or die ('Cannot create fasta alignment');
my $cpt = 0;

print FHTEMP "$no_sequences $cpt2\n";
print OUTT "$no_sequences $cpt2\n";
my $stream  = Bio::AlignIO->new(
				-file => $ARGV[0],
				'-format' => 'stockholm'
				);
				
### Reconstruction de l'alignement a partir du MOVETOLASTOUT
while(my $aln = $stream->next_aln ) {
    my $no_sequences = $aln->no_sequences;
    my $length = $aln->length();
   
    my $cpt1 =0;
    ## Pour chaque sequence de l'alignement
    foreach my $seqq ( $aln->each_seq ) {
	    my $chain;
	    my $masked_chain;
	    my $last = $cpt+1;
	    $cpt1 ++;
	    ## Ecris le nom des sequences dans fichier sortie
	    print FHTEMP $seqq->id; 
	    print OUTT $seqq->id;  
	    #print "<br />\n".$seqq->id."";
	
	    ## Contr�les pour voir si AA des positions start et end sont identiques
	    unless ( ($cpt + 1) == $no_sequences) 
	    {
    	    my $aa_rascal_begin =  $seqq->subseq($pos_start, $pos_start);
    	    my $aa_move_begin   =  $seq2[$cpt]->subseq($indice_start, $indice_start);
    	    my $aa_rascal_end   =  $seqq->subseq($pos_end, $pos_end);
    	    my $aa_move_end     =  $seq2[$cpt]->subseq($indice_end, $indice_end);
    	    $aa_move_begin      =  '-' if ( $aa_rascal_begin eq '-');
    	    $aa_move_end        =  '-' if ( $aa_rascal_end eq '-');
    	     
    		#print "TEST AA: ".$aa_rascal_begin."****".$aa_move_begin."****".$aa_rascal_end."****".$aa_move_end."<br />\n";
    	    #print "TEST POSITIONS: ".$indice_start."****".$pos_start."****".$pos_end."****".$indice_end."<br />\n";
    		
    	    unless ($seq2[$cpt]->id eq $seqq->id) {
    		die "error: sequence name is different (".$seq2[$cpt]->id.", ".$seqq->id."); please check the position of the sequences in the alignment\n";
    	    }
    	    if ($aa_rascal_begin ne $aa_move_begin) { 
    		print "Error: columns start are not identical (".$seq2[$cpt]->id." ---- '".$aa_rascal_begin."' ---- ".$pos_start." / ".$seqq->id." ---- '".$aa_move_begin."' ---- ".$indice_start.") please contact us at greenphyl AT cirad.fr<br />\n";
    		exit(0);
    	    }
    	    elsif ($aa_rascal_end ne $aa_move_end) { 
    	       print "Error: columns end are not identical (".$aa_rascal_end." ---- ".$pos_end." / ".$aa_move_end." ---- ".$indice_end.") please contact us at greenphyl AT cirad.fr<br />\n";
    		print " $aa_rascal_end != $aa_move_end <br />";
    		exit(0);
    	    }
    	    else {
    		#print $seqq->id , " : Rascal ($pos_start - $pos_end) Start : ",$aa_rascal_begin ." / Move ($indice_start - $indice_end) Start : ", $aa_move_begin ," \--\ Rascal End : ", $aa_rascal_end , " / Move End : ", $aa_move_end ,"<br />";
    	    }
	    }
	
    	## Ecris un espace entre id seq et seq
    	for ( my $i = 0; $i < ( $LENGTH_OF_NAME - length( $seqq->id ) ); $i++ ) {
    	    print FHTEMP " ";
    	    print OUTT " ";
    	}
    	my ($aln_start,$aln_middle,$aln_end);
    	
    	## 1: Recupere entre position start et end sur align d'origine (rascal) sans position gap query
    	
    	$aln_middle = $seqq->subseq($pos_start, ($pos_end));
    	#print "<br />\n".$aln_middle."<br />\n";
    	my $dl = 0;
    	foreach my $elt (@gapToRemove) {
    		$elt2 = ($elt - $dl);
    			 #print "-".$elt2."\t".$dl."\n";
    		substr($aln_middle, $elt2, 1) = "";
    		$dl++;	
    	}
    	#print "".$aln_middle."<br />\n";
    	
    	## 2: recupere avant le start et apres end sur HMM
    	unless ( ($cpt + 1) == $no_sequences) {
    	    unless($indice_start == 1) {
    		$aln_start  = $seq2[$cpt]->subseq(1, ($indice_start - 1));
    	    }
    	    $aln_end    = $seq2[$cpt]->subseq($indice_end, $length2);
    	}
    	
    	else {
    	    unless ($indice_start==1) {
    		$aln_start = ( "-" x ($indice_start - 1) );
    	    }
    	    $aln_end 	=  ( "-" x $stop );
    	}
    	## 3: recontruit la chaine entiere
    	$chain = $aln_start.$aln_middle.$aln_end;
    	print FHTEMP $chain;
    	print FHTEMP "\n";
    	
    
    	## 4: recup les colonnes en fonction du BSP du masking			 
    			
    	foreach my $pos (@positions) {
    	  $masked_chain .= substr ($chain, $pos, 1);
    	}
    	$masked_chain =~ s/\./-/g;
    	$masked_chain = uc($masked_chain);
    	print OUTT $masked_chain;
    	print OUTT "\n";
    	$cpt++;
    }  
}
