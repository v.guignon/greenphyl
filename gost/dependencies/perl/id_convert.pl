#!/usr/bin/perl

use strict;
use Getopt::Long;
use Bio::SeqIO;

my $dic_suffix = ".dic";
my $out_suffix = ".out";

my $in_file;
my $out_file;
my $dic_file;
my $decrypt;
my $verbose;

GetOptions(
	'infile|in|i=s'   => \$in_file,
	'outfile|out|o=s' => \$out_file,
	'dicfile|f=s'     => \$dic_file,
	'decrypt|d'       => \$decrypt,
	'verbose|v'       => \$verbose,
) || die("Bad usage!");

print "--1--$in_file--\n";
#$in_file = $_ if(defined($_ = shift()));
print "--2--$in_file--\n";
die("Need an input file!:$in_file") if !$in_file;
die("Input file does not exists!:$in_file") if !-e $in_file;
$dic_file = $in_file . $dic_suffix if !$dic_file;
$out_file = $in_file . $out_suffix if !$out_file;

if ($verbose) {
	print "in file : $in_file\n";
	print "out file: $out_file\n";
	print "dic file: $dic_file\n";
	print "action  : ", $decrypt ? "decrypt" : "crypt", "\n";
}

if (!$decrypt) {
	my $cpt = 0;
	my $seq_stream = Bio::SeqIO->new(-file => $in_file, '-format' => 'Fasta');
	my $seq_out = Bio::SeqIO->new(-file => ">$out_file", '-format' => 'Fasta');
	
	open(DIC, ">$dic_file");
	
	while(my $seq = $seq_stream->next_seq()) {
		$cpt++;
		my $new_id = "SEQ" . "0" x (7 - length($cpt)) . $cpt;
		print DIC join("\t", $new_id, $seq->id()), "\n";
		$seq->id($new_id);
		$seq_out->write_seq($seq);
	}
	close(DIC);
} 
else {
	die("Need a dic file!") if !-e $dic_file;
	my %dictionary;
	open(DIC, $dic_file);
	
	while(<DIC>) {
		chomp;
		my @line = split("\t");
		$dictionary{$line[0]} = $line[1];
	}
	
	close(DIC);
	open(IN, $in_file);
	open(OUT, ">$out_file");
	
	while(<IN>) {
		s/(SEQ\d{7})/$dictionary{$1}/g;
		print OUT $_;
	}
	
	close(OUT);
	close(IN);
}

exit 0;
