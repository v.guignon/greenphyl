#!/usr/bin/perl -W

# makeTree.pl
# -----------
# Copyright (C) 1999-2002 Washington University School of Medicine
# and Howard Hughes Medical Institute
# All rights reserved
#
# Author: Christian M. Zmasek 
#         zmasek@genetics.wustl.edu
#         http://www.genetics.wustl.edu/eddy/people/zmasek/
#
# Last modified 01/28/02


#  Requirements: - modified TREE-PUZZLE 5.0 (which allows longer seq names and more seqs)
#                - modified PHYLIP 3.6 (SEQBOOT, NEIGHBOR, CONSENSE)
#                - transfersBranchLenghts.java (http://www.genetics.wustl.edu/eddy/forester/).
#                - Java 1.2 (http://java.sun.com/products/index.html).
#                - Perl 5.

#  Usage: makeTree.pl [-options] <input alignment in SELEX (Pfam) or PHYLIP
#         sequential format> <outputfile> [path/name for temporary
#         directory to be created]
#
#  Usage:  makeTree.pl <-options, includes "F"> <pwdfile: boostrapped pairwise
#          distances> <outputfile> [path/name for temporary directory
#          to be created]
#
# 

#  -N  : Suggestion to remove columns in the alignment which contain gaps.
#        Gaps are not removed, if, after removal of gaps, the resulting alignment would
#        be shorter than $MIN_NUMBER_OF_AA. Default is not to remove gaps.
#  -Bx : Number of bootstrapps. B0: do not bootstrap. Default is 100 bootstrapps.
#        The number of bootstrapps should be divisible by 10.
#  -U  : Use TREE-PUZZLE to calculate ML branchlengths for consesus tree, in case of 
#        bootstrapped analysis.
#  -J  : Use JTT matrix (Jones et al. 1992) in TREE-PUZZLE, default: PAM.
#  -L  : Use BLOSUM 62 matrix (Henikoff-Henikoff 92) in TREE-PUZZLE, default: PAM.
#  -M  : Use mtREV24 matrix (Adachi-Hasegawa 1996) inTREE-PUZZLE, default: PAM.
#  -W  : Use WAG matrix (Whelan-Goldman 2000) in TREE-PUZZLE, default: PAM.
#  -T  : Use VT matrix (Mueller-Vingron 2000) in TREE-PUZZLE, default: PAM.
#  -P  : Let TREE-PUZZLE choose which matrix to use, default: PAM.
#  -R  : Randomize input order in PHYLIP NEIGHBOR.
#  -Sx : Seed for random number generator(s). Must be 4n+1. Default is 9.
#  -X  : To keep multiple tree file (=trees from bootstrap resampled alignments).
#  -D  : To keep (and create in case of bootstrap analysis) pairwise distance matrix file.
#        This is created form the not resampled (original) alignment.
#  -C  : Calculate pairwise distances only (no tree). Bootstrap is always 1.
#        No other files are generated.  
#  -F  : Pairwise distance (pwd) file as input (instead of alignment).
#        No -U, -D, -C, and -N options available in this case.
#  -V  : Verbose.
#

       

use strict;

use FindBin;
use lib $FindBin::Bin;
use rio_module;

my $VERSION                = "4.000";

my $TEMP_DIR_DEFAULT       = "/tmp/maketree"; # Where all the infiles, outfiles, etc will be created.
   
my $remove_gaps           = 0;   # 0: do not remove gaps;  1: remove gaps
my $bootstraps            = 100; # 0,1: do not bootstrap. Default: 100
my $puzzle_consensus_tree = 0;   # 0: no; 1: yes. No is default.
my $matrix                = 1;   # 0 = JTT (Puzzle only)
                                 # 1 = PAM - default 
                                 # 2 = BLOSUM 62 (Puzzle only) 
                                 # 3 = mtREV24 (Puzzle only)
                                 # 5 = VT (Puzzle only)
                                 # 6 = WAG (Puzzle only)
                                 # 7 = auto (Puzzle only)
my $randomize_input_order = 0;   # 0: do not randomize input order; 1 jumble
my $seed                  = 9;   # Seed for random number generators. Default: 9
my $keep_multiple_trees   = 0;   # 0: delete multiple tree file
                                 # 1: do not delete multiple tree file
my $keep_distance_matrix  = 0;   # 1: (create and) keep; 0: do not (create and) keep
my $verbose               = 0;   # 0: no; 1: yes
my $pairwise_dist_only    = 0;   # 0: no; 1: yes
my $start_with_pwd        = 0;   # 0: no; 1: yes


my %seqnames       = ();           # number   =>  seqname 
my %numbers        = ();           # seqname  =>  number
my $options        = "";
my $infile         = "";
my $pwdfile        = "";
my $nbdfile        = "";
my $outfile        = "";
my $outfilenhx     = "";
my $logfile        = "";
my $alignfile      = "";
my $multitreefile  = "";
my $distancefile   = "";
my $log            = "";
my $number_of_aa   = 0;
my $orig_length    = 0;
my $ii             = 0;
my $temp_dir       = "";
my $current_dir    = "";
my @out            = ();
my $number_of_seqs = 0;



unless ( @ARGV == 2 || @ARGV == 3 || @ARGV == 4 || @ARGV == 5 ) {
    &printUsage();
    exit ( -1 ); 
}



# Analyzes the options:
# ---------------------

if ( $ARGV[ 0 ] =~ /^-.+/ ) {
    
    unless ( @ARGV > 2 ) {
        &printUsage();
        exit ( -1 ); 
    }
    $options = $ARGV[ 0 ];
    
    if ( $options =~ /F/ ) {
        $start_with_pwd = 1;
        $infile         = "";
        $pwdfile        = $ARGV[ 1 ];
        
        $outfile = $ARGV[ 2 ];
        $nbdfile = "";
        if ( @ARGV == 4 ) {
            $temp_dir = $ARGV[ 3 ];
        }
        
    }
    else {
        $infile  = $ARGV[ 1 ];
        $outfile = $ARGV[ 2 ];
        if ( @ARGV == 4 ) {
            $temp_dir = $ARGV[ 3 ];
        }
    }
    
    if ( $options =~ /N/ ) {
        $remove_gaps    = 1; # do remove gaps 
    }
    if ( $options =~ /B(\d+)/ ) {
        $bootstraps = $1;
        if ( $bootstraps <= 1 ) {
            $bootstraps = 0;
        }
        elsif ( $bootstraps > 1 && $bootstraps < 10 ) {
            $bootstraps = 10;
        }
        elsif ( $bootstraps % 10 != 0 ) {
            $bootstraps = $bootstraps - $bootstraps % 10; # to ensure $bootstraps % 10 == 0
        }    
    }
    if ( $options =~ /J/ ) {
        $matrix = 0;      # JTT
    }
    if ( $options =~ /L/ ) {
        $matrix = 2;      # Blossum
    }
    if ( $options =~ /M/ ) {
        $matrix = 3;      # mtREV24
    }
    if ( $options =~ /T/ ) {
        $matrix = 5;      # VT
    }
    if ( $options =~ /W/ ) {
        $matrix = 6;      # WAG
    }
    if ( $options =~ /P/ ) {
        $matrix = 7;      # auto
    }
    if ( $options =~ /R/ ) {
        $randomize_input_order = 1;
    }
    if ( $options =~ /S(\d+)/ ) {
        $seed = $1; 
    }
    if ( $options =~ /U/ ) {
        $puzzle_consensus_tree = 1;
    }
    if ( $options =~ /X/ ) {
        $keep_multiple_trees = 1; 
    }
    if ( $options =~ /D/ ) {
        $keep_distance_matrix = 1; 
    }
    if ( $options =~ /E/ ) {
        $verbose = 1; 
    }
    if ( $options =~ /V/ ) {
        $verbose = 1; 
    }
    if ( $options =~ /C/ ) {
        $pairwise_dist_only = 1; 
    }
}

else {
    unless ( @ARGV == 2 || @ARGV == 3 ) {
        &printUsage();
        exit ( -1 );  
    }
    $infile  = $ARGV[ 0 ];
    $outfile = $ARGV[ 1 ];
    if ( @ARGV == 3 ) {
        $temp_dir = $ARGV[ 2 ];
    } 
}



$current_dir = `pwd`;
$current_dir =~ s/\s//;

if ( $outfile !~ /^\// ) {
    # outfile is not absolute path.
    $outfile = $current_dir."/".$outfile;
}


if ( $start_with_pwd == 1 ) {
    $pairwise_dist_only    = 0;
    $keep_distance_matrix  = 0;
    $remove_gaps           = 0;
    $puzzle_consensus_tree = 0;
}



if ( $pairwise_dist_only == 1 ) {
    $bootstraps            = 0;
    $keep_multiple_trees   = 0;
    $puzzle_consensus_tree = 0;
    $randomize_input_order = 0;
    $start_with_pwd        = 0;
    $keep_distance_matrix  = 1; 
}



$logfile       = $outfile.$LOG_FILE_SUFFIX;
$alignfile     = $outfile.$ALIGN_FILE_SUFFIX;
$multitreefile = $outfile.$MULTIPLE_TREES_FILE_SUFFIX;
$distancefile  = $outfile.$SUFFIX_PWD_NOT_BOOTS;

if ( $outfile =~ /\.nhx$/i ) {
    $outfilenhx    = $outfile;
    $logfile       =~ s/\.nhx//i;
    $alignfile     =~ s/\.nhx//i;
    $outfile       =~ s/\.nhx//i;
    $multitreefile =~ s/\.nhx//i;
    $distancefile  =~ s/\.nhx//i;
}  
else {
    $outfilenhx    = $outfile.".nhx";
}

if ( -e $outfilenhx ) {
    die "\n\nmakeTree: \"$outfilenhx\" already exists.\n\n";
}
if ( $start_with_pwd != 1 ) {
    unless ( ( -s $infile ) && ( -f $infile ) && ( -T $infile ) ) {
        die "\n\nmakeTree: Input alignment file \"$infile\" does not exist, is empty, or is not a plain textfile.\n\n";
    }
}
if ( $start_with_pwd == 1 ) {
    unless ( ( -s $pwdfile ) && ( -f $pwdfile ) && ( -T $pwdfile ) ) {
        die "\n\nmakeTree: Pairwise distance file \"$pwdfile\" does not exist, is empty, or is not a plain textfile.\n\n";
    }
    if ( $nbdfile ne "" ) {
        unless ( ( -s $nbdfile ) && ( -f $nbdfile ) && ( -T $nbdfile ) ) {
            die "\n\nmakeTree: Pairwise distance file \"$nbdfile\" does not exist, is empty, or is not a plain textfile.\n\n";
        }
    }
}



# Prints out the options:
# -----------------------


$log = "\n$0 logfile:\n";
$log = $log."Version: $VERSION\n\n";


if ( $start_with_pwd == 1 ) {
    $log = $log."Input pairwise distance file (bootstrapped): $pwdfile\n";
}
if ( $nbdfile ne "" ) {
    $log = $log."Pairwise distance file (non bootstrapped): $nbdfile\n";
}
if ( $infile ne ""  ) {
    $log = $log."Input alignment                     : $infile\n";
}

$log = $log."Output tree file                    : $outfilenhx\n";

if ( $keep_multiple_trees == 1 && $bootstraps >= 2 ) {
    $log = $log."Output  multiple trees file         : $multitreefile\n";
}
if ( $keep_distance_matrix ) {
    $log = $log."Output pairwise distance file       : $distancefile\n";
}

$log = $log."Bootstraps                          : $bootstraps\n";

if ( $start_with_pwd != 1 ) {
    $log = $log."Prgrm to calculate pairwise dist.   : TREE-PUZZLE\n";
}

$log = $log."Program to calculate tree           : PHYLIP NEIGHBOR (NJ)\n";

if ( $bootstraps >= 2  && $puzzle_consensus_tree == 1 ) {
    $log = $log."Prgrm to calculate ML branch lenghts: TREE-PUZZLE\n";
}
if ( $start_with_pwd != 1 || $puzzle_consensus_tree == 1 ) {
    $log = $log."Model                               : ";
    if ( $matrix == 0 ) { 
        $log = $log."JTT (Jones et al. 1992)\n";
    }
    elsif ( $matrix == 2 ) {
        $log = $log."BLOSUM 62 (Henikoff-Henikoff 92)\n";
    }
    elsif ( $matrix == 3 ) {
        $log = $log."mtREV24 (Adachi-Hasegawa 1996)\n";
    }
    elsif ( $matrix == 5 ) {
        $log = $log."VT (Mueller-Vingron 2000)\n";
    }
    elsif ( $matrix == 6 ) {
        $log = $log."WAG (Whelan-Goldman 2000)\n";
    }
    elsif ( $matrix == 7 ) {
        $log = $log."auto\n";
    }
    else {
        $log = $log."PAM (Dayhoff et al. 1978)\n";
    }
}

if ( $randomize_input_order >= 1 ) {
    $log = $log."Randomize input order in NEIGHBOR   : yes\n";
}

$log = $log."Seed for random number generators   : $seed\n";


$log = $log."Start time/date                     : ".`date`;




# That's where the mischief starts....
# ------------------------------------

$ii = 0;

my $time_st = time;

if ( $temp_dir eq "" ) {
    $temp_dir = $TEMP_DIR_DEFAULT;
}

$temp_dir = $temp_dir.$time_st.$ii;

while ( -e $temp_dir ) {
    $ii++;
    $temp_dir = $temp_dir.$time_st.$ii;
}

mkdir(  $temp_dir, 0700 )
|| die "\n\n$0: Unexpected error: Could not create <<$temp_dir>>: $!\n\n";

unless ( ( -e $temp_dir ) && ( -d $temp_dir ) ) {
    die "\n\n$0: Unexpected error: <<$temp_dir>> does not exist, or is not a directory.\n\n";
}


if ( $start_with_pwd != 1 ) {
    system( "cp", $infile, $temp_dir."/INFILE" );
    unless ( chmod ( 0600, $temp_dir."/INFILE" ) ) {
        warn "\n\n$0: Could not chmod. $!\n\n";
    }
    $infile = "INFILE";
}


chdir ( $temp_dir ) 
|| die "\n\n$0: Unexpected error: Could not chdir to <<$temp_dir>>: $!\n\n";


if ( $start_with_pwd != 1 ) {

    @out = &DoPfam2phylip( $infile, $alignfile, $remove_gaps );
    $number_of_aa   = $out[ 0 ];
    $orig_length    = $out[ 1 ];
    $number_of_seqs = $out[ 2 ];

    system( "cp", $alignfile, "infile" );
    system( "cp", $alignfile, "align" );

    # Calculating the pairwise distances (saved in file "infile"): "puzzle" or "protdist":
    if ( $bootstraps >= 2 ) {

        &executeSeqboot( $seed, $bootstraps );
       
        if ( $keep_distance_matrix ) {
            system( "mv", "outfile", "outfile___" );
            system( "cp", "align", "infile" );
            &executePuzzle( "infile", $matrix );
            system( "mv", "infile.dist", $distancefile );
            system( "mv", "outfile___", "outfile" );
        }
        unlink( "infile" ); # Necessary, since "infile" is puzzle's default input. 
        system( "mv", "outfile", "IN" );
       
        &executePuzzleBootstrapped( "IN", $matrix );
        
        $pwdfile = "IN.dist";

    } ## if ( $bootstraps >= 2 )
    else {

        &executePuzzle( "infile", $matrix );
        if ( $keep_distance_matrix ) {
            system( "cp outdist $distancefile" );
        } 
        $pwdfile = "outdist";
    }

    unlink( "infile.tree" );

    if ( $pairwise_dist_only == 1 ) {
        unlink( "infile", "align", "INFILE", "outdist", $alignfile );
        chdir( $current_dir ) 
        || die "\n\n$0: Unexpected error: Could not chdir to <<$current_dir>>: $!\n\n";

        rmdir( $temp_dir )
        || die "\n\n$0: Unexpected error: Could not remove <<$temp_dir>>: $!\n\n";

        print "\n\n$0 finished.\n\n";
        print "Output pairwise distance file written as: $distancefile\n\n";
        print "\n\nmakeTree successfully terminated.\n\n";
        exit( 0 );
    }

} ## if ( $start_with_pwd != 1 )

 
# Calculating the tree (saved in file "infile"):
unlink( "infile" );
&executeNeighbor( $pwdfile, $bootstraps, $randomize_input_order, $seed, 1 );
unlink( "outfile" );

if ( $keep_multiple_trees == 1 && $bootstraps >= 2 ) {
   
    system( "cp", "outtree", $multitreefile );
}


system( "mv", "outtree", "intree" );

if ( $bootstraps >= 2 ) {

    # Consense:
    &executeConsense( "intree" );

    if ( $puzzle_consensus_tree == 1 ) { 
        
        system( "cp", "outtree", "treefile_consense" );
        system( "mv", "outtree", "intree" );

        # Puzzle for ML branch lenghts:
        # The alignment is read from infile by default.
        # The tree is read from intree by default.
        
        system( "mv", "align", "infile" ); # align = original alignment in phylip interleaved.
       
        &executePuzzleToCalculateBranchLenghts( $matrix );
       
        unlink( "outfile", "outdist" );
        system( "mv", "outtree", "outree_puzzle" );

        # Transfer
        &executeTransfersBranchLenghts( "outree_puzzle", "treefile_consense", $outfilenhx );
        
    }
    else {
        unlink( "outfile", "align" );
        system( "mv", "outtree", $outfilenhx );
    }
}
else {
    unlink( "align" );
    if ( $start_with_pwd != 1 ) {
        system( "mv intree $outfilenhx" );
    }

}


unlink( "treefile_consense", "outtree", "outree_puzzle",
        "infile", "intree", "align", "INFILE", "IN", "IN.dist", "outdist"  );


$log = $log."Finish time/date                    : ".`date`;

if ( $start_with_pwd != 1 ) {
    $log = $log."Removed gaps                        : ";
    if ( $remove_gaps == 1 ) {
        $log = $log."yes\n";
    }
    else {
        $log = $log."no\n";
    }
    $log = $log."Columns in alignment used           : $number_of_aa\n";
    $log = $log."Columns in original alignment       : $orig_length\n";
    $log = $log."Number of sequences in alignment    : $number_of_seqs\n";
}


open( OUT, ">$logfile" ) || die "\n$0: Cannot create file <<$logfile>>: $!\n";
print OUT $log;
close( OUT );


chdir( $current_dir ) 
|| die "\n\n$0:Unexpected error: Could not chdir to <<$current_dir>>: $!\n\n";


rmdir( $temp_dir )
|| die "\n\n$0:Unexpected error: Could not remove <<$temp_dir>>: $!\n\n";

if ( $verbose == 1 ) {
    print "\n\n$0 finished.\n";
    print "Output tree written as    : $outfilenhx\n";
    print "Log written as            : $logfile\n";
    if ( $start_with_pwd != 1 ) {
        print "Alignment written as      : $alignfile\n";
    }
    if ( $keep_multiple_trees == 1 && $bootstraps >= 2 ) {
        print "Multiple trees written as : $multitreefile\n";
    }
    if ( $keep_distance_matrix ) {
        print "Distance matrix written as: $distancefile\n";
    }
}


exit( 0 ); 
    




# Methods:
# --------




# Executes pfam2phylip.
# If resulting alignment is too short due to the removal
# of gaps, is does not remove gaps.
# Three arguments:
# 1. infile
# 2. outfile
# 3. remove gaps: 1 to remove gaps; 0: do not remove gaps 
# Last modified: 06/04/01
sub DoPfam2phylip {
    my $in         = $_[ 0 ]; 
    my $out        = $_[ 1 ];
    my $option     = $_[ 2 ];
    my $aa         = 0;
    my @output     = ();

    if ( $option == 1 ) {
        @output = &pfam2phylip( $in, $out, 1 );
        $aa = $output[ 0 ];
        if ( $aa < 0 ) {
            die "\n\n$0: DoPfam2phylip: Unexpected error.\n\n";
        }
        if ( $aa < $MIN_NUMBER_OF_AA ) {
            unlink( $out );
            $option      = 0;
            $remove_gaps = 0;
        }
    }
    if ( $option == 0 ) {    # Must be another "if" (no elsif of else)!
        @output = &pfam2phylip( $in, $out, 2 );
        # 2 is to substitute non-letters with "-" in the sequence.
        $aa = $output[ 0 ];
        if ( $aa <= 0 ) {
             die "\n\n$0: DoPfam2phylip: Unexpected error.\n\n";
        }
    }
    return @output;
}



# Two arguments:
# 1. seed for random number generator
# 2. number of bootstraps
# Reads in "infile" by default.
sub executeSeqboot {

    my $s    = $_[ 0 ];
    my $bs   = $_[ 1 ];
    my $verb = "";
    
    &testForTextFilePresence( $infile );

    if ( $verbose != 1 ) {
        $verb = "
2";
    }

   
    system( "$SEQBOOT << !
r
$bs$verb
Y
$s
!" )
    && die "$0: Could not execute \"$SEQBOOT\"";
    
    return;
    
}




# One argument:
# matrix option: 0 = JTT; 2 = BLOSUM 62; 3 = mtREV24;
# 5 = VT; 6 = WAG; 7 = auto; PAM otherwise
# Reads in tree from "intree" by default. (Presence of "intree" automatically 
# switches into "User defined trees" mode.)
sub executePuzzleToCalculateBranchLenghts {
    my $matrix_option = $_[ 0 ];
    my $i             = 0;
    my $mat           = "";
    
    unless ( ( -s "infile" ) && ( -f "infile" ) && ( -T "infile" ) ) {
        die "\n$0: executePuzzleToCalculateBranchLenghts: <<infile>> does not exist, is empty, or is not a plain textfile.\n";
    }
    unless ( ( -s "intree" ) && ( -f "intree" ) && ( -T "intree" ) ) {
        die "\n$0: executePuzzleToCalculateBranchLenghts: <<intree>> does not exist, is empty, or is not a plain textfile.\n";
    }

    $mat = setModelForPuzzle( $matrix_option );
    
    system( "$PUZZLE << !
$mat
x
y
!" )
    && die "$0: Could not execute \"$PUZZLE\"";
    
    return;
    
}







# Three/four arguments:
# 1. Name of file containing tree with correct branch lengths
# 2. Name of file containing tree with correct bootstraps
# 3. Outputfilename
# 4. R to reroot both trees in the same manner (use for FITCH,
#    since this changes to rooting.
sub executeTransfersBranchLenghts {
    my $tree_with_bl = $_[ 0 ];
    my $tree_with_bs = $_[ 1 ];
    my $out          = $_[ 2 ];
    my $reroot       = $_[ 3 ];
    my $R            = "";

    if ( $reroot && $reroot eq "R" ) {
        $R = "R";
    }

    &testForTextFilePresence( $tree_with_bl );
    &testForTextFilePresence( $tree_with_bs );
    
    system( "$TRANSFERSBRANCHLENGHTS $tree_with_bl $tree_with_bs $out $R" )
    && die "$0: Could not execute \"$TRANSFERSBRANCHLENGHTS $tree_with_bl $tree_with_bs $out $R\"";
    
    
    return;
}



# Called by method DoPfam2phylip.
# This reads a multiple sequence alignment file in Pfam format or
# and Phylip's sequential format and saves them in Phylip's sequential
# or interleaved format. (Those two are the same in this case,
# since all the seqs will be one line in length (no returns)).
# It returns (1st) the number of aa (columns) in the resulting
# alignment and the (2nd) number of aa (columns) in the original
# alignment.
#
# Reads a file containing a sequence alignment in the following format
# (as used in Pfam):
#  #comments      <- empty lines and lines begining with # (not mandatory)
#  name1 kal
#  name2 kal
#                 <- at least one empty line between blocks
#  name1 kale
#  name2 k.le
#
# Saves it in the "sequential" format of phylip:
#  number of OTUs length of aa seqs
#  name1     kalkale
#  name2     kalk-le
#
# Three arguments:
# 1. infile name
# 2. outfile name
# 3. 1  : Removes colums with a gap (non-letter character)
#    2  : Substitutes non-letter characters in the sequence with "-".
#
# Last modified: 07/10/01
#
sub pfam2phylip { 

    my $infile              = $_[ 0 ];
    my $outfile             = $_[ 1 ];
    my $options             = $_[ 2 ]; # 1: remove gaps; 2: non-letters->"-"
    my $return_line         = "";
    my $x                   = 0;
    my $y                   = 0;
    my $x_offset            = 0;
    my $original_length     = 0;
    my @seq_name            = ();
    my @seq_array           = ();
    my $seq                 = "";
    my $max_x               = 0;
    my $max_y               = 0;
    my $m                   = 0;
    my $n                   = 0;
    my $i                   = 0;
    my $move                = 0;
    my $saw_a_sequence_line = 0;

    if ( -e $outfile ) {
        die "\n$0: pfam2phylip: <<$outfile>> already exists.\n";
    }

    &testForTextFilePresence( $infile );

    open( INPP, "$infile" ) || die "\n$0: pfam2phylip: Cannot open file <<$infile>>: $!\n";

    until ( $return_line !~ /^\s*\S+\s+\S+/ && $saw_a_sequence_line == 1 ) {
        if ( $return_line =~ /^\s*\S+\s+\S+/ 
        && $return_line !~ /^\s*#/ 
        && $return_line !~ /^\s*\d+\s+\d+/ ) {
            $saw_a_sequence_line = 1;
            $return_line =~ /^\s*(\S+)\s+(\S+)/;
            $seq_name[ $y ] = $1;
            $seq = $2;
            $seq_name[ $y ] = substr( $seq_name[ $y ], 0, $LENGTH_OF_NAME );
           
            for ( $x = 0; $x <= length( $seq ) - 1; $x++ ) {
                $seq_array[ $x ][ $y ] = substr( $seq, $x, 1 );
            }
            if ( $x_offset < length( $seq ) ) {
                $x_offset = length( $seq );
            }
            $y++;
        }
        $return_line = <INPP>;
        if ( !$return_line ) {
            last;
        }
    }

    $max_y = $y;
    $y     = 0;
    $max_x = 0;

    while ( $return_line = <INPP> ) {
        if ( $return_line =~ /^\s*(\S+)\s+(\S+)/
        && $return_line !~ /^\s*#/ 
        && $return_line !~ /^\s*\d+\s+\d+/ ) {
            $return_line =~ /^\s*\S+\s+(\S+)/;
            $seq = $1;
            for ( $x = 0; $x <= length( $seq ) - 1; $x++ ) {
                $seq_array[ $x + $x_offset ][ $y % $max_y ] = substr( $seq, $x, 1 );
            }
            if ( $max_x < length( $seq ) ) {
                $max_x = length( $seq );
            }
            $y++;
            if ( ( $y % $max_y ) == 0 ) {
                $y = 0;
                $x_offset = $x_offset + $max_x;
                $max_x = 0;
            }
        }
    }
    $original_length = $x_offset;
    close( INPP );


    # Removes "gap-columns" (gaps = everything except a-z characters):
    if ( $options == 1 ) {
        $move = 0;

        COLUMN: for ( $x = 0; $x <= $x_offset - 1; $x++ ) {  # goes through all aa positions (columns)

            for ( $y = 0; $y <= $max_y - 1; $y++ ) { # goes through all aas in a particular position

                unless ( $seq_array[ $x ][ $y ] && $seq_array[ $x ][ $y ] =~ /[a-z]/i ) {
                    $move++;
                    next COLUMN;
                }
            }

            # If this point is reached, column must be OK = no gaps.
            if ( $move >= 1 ) {
                for ( $m = 0; $m <= $max_y; $m++ ) {       
                    for ( $n = $x; $n <= $x_offset; $n++ ) {
                        $seq_array[ $n - $move ][ $m ] = $seq_array[ $n ][ $m ];
                    }
                }  
                $x_offset = $x_offset - $move;
                $x = $x - $move;
                $move = 0;
            }
        }
        if ( $move >= 1 ) {
            for ( $m = 0; $m <= $max_y; $m++ ) {       
                for ( $n = $x; $n <= $x_offset; $n++ ) {
                    $seq_array[ $n - $move ][ $m ] = $seq_array[ $n ][ $m ];
                }
            }   
            $x_offset = $x_offset - $move;
            $x = $x - $move;
            $move = 0;
        }
    }


    # Writes the file:

    open( OUTPP, ">$outfile" ) || die "\n$0: pfam2phylip: Cannot create file <<$outfile>>: $!\n";
    print OUTPP "$max_y $x_offset\n";
    for ( $y = 0; $y < $max_y; $y++ ) {
        print OUTPP "$seq_name[ $y ]";
        for ( $i = 0; $i <= ( $LENGTH_OF_NAME - length( $seq_name[ $y ] ) - 1 ); $i++ ) {
	        print OUTPP " ";
        }
        for ( $x = 0; $x <= $x_offset - 1; $x++ ) {
            if ( $options == 2 ) {
                if ( $seq_array[ $x ][ $y ] ) {
                    $seq_array[ $x ][ $y ] =~s /[^a-zA-Z]/-/;
                }
                else {
                    $seq_array[ $x ][ $y ] = "-";
                }
            }
            print OUTPP "$seq_array[ $x ][ $y ]";
        }
        print OUTPP "\n";
    }  
    close( OUTPP );

    return $x_offset, $original_length, $max_y;

} ## pfam2phylip



sub printUsage {
    print "\n";
    print " makeTree.pl  version $VERSION\n";
    print " -----------\n";
    print " Copyright (C) 2000-2002 Washington University School of Medicine\n";
    print " and Howard Hughes Medical Institute\n";
    print " All rights reserved\n";
    print "\n";
    print " Author: Christian M. Zmasek\n";
    print " zmasek\@genetics.wustl.edu\n";
    print " http://www.genetics.wustl.edu/eddy/forester/\n";
    print "\n";
    print "\n";
    print " Usage: makeTree.pl [-options] <input alignment in SELEX (Pfam) or PHYLIP\n";
    print "        sequential format> <outputfile> [path/name for temporary directory\n";
    print "        to be created]\n";
    print "\n";
    print " Usage: makeTree.pl <-options, includes \"F\"> <pwdfile: boostrapped pairwise\n";
    print "        distances> [NBD file: not-boostrapped pairwise distances, if \"G\"\n";
    print "        option is used] <outputfile> [path/name for temporary directory\n";
    print "        to be created]\n";
    print "\n";
    print " Options:\n";
    print "\n";
    print " -N  : Suggestion to remove columns in the alignment which contain gaps.\n"; 
    print "       Gaps are not removed, if, after removal of gaps, the resulting\n";
    print "       alignment would be shorter than $MIN_NUMBER_OF_AA aa (\$MIN_NUMBER_OF_AA).\n";
    print "       Default is not to remove gaps.\n";
    print " -Bx : Number of bootstrapps. B0: do not bootstrap. Default: 100 bootstrapps.\n";
    print "       The number of bootstrapps should be divisible by 10.\n";
    print " -U  : Use TREE-PUZZLE to calculate ML branchlengths for consesus tree, in case of\n";
    print "       bootstrapped analysis.\n";
    print " -J  : Use JTT matrix (Jones et al. 1992) in TREE-PUZZLE, default: PAM.\n";
    print " -L  : Use BLOSUM 62 matrix (Henikoff-Henikoff 92) in TREE-PUZZLE, default: PAM.\n";
    print " -M  : Use mtREV24 matrix (Adachi-Hasegawa 1996) in TREE-PUZZLE, default: PAM.\n";
    print " -W  : Use WAG matrix (Whelan-Goldman 2000) in TREE-PUZZLE, default: PAM.\n";
    print " -T  : Use VT matrix (Mueller-Vingron 2000) in TREE-PUZZLE, default: PAM.\n";
    print " -P  : Let TREE-PUZZLE choose which matrix to use, default: PAM.\n";
    print " -R  : Randomize input order in PHYLIP NEIGHBOR.\n";
    print " -Sx : Seed for random number generator(s). Must be 4n+1. Default is 9.\n";
    print " -X  : To keep multiple tree file (=trees from bootstrap resampled alignments).\n";
    print " -D  : To keep (and create, in case of bootstrap analysis) pairwise distance\n";
    print "       matrix file. This is created form the not resampled aligment.\n";
    print " -C  : Calculate pairwise distances only (no tree). Bootstrap is always 1.\n";
    print "       No other files are generated.\n";
    print " -F  : Pairwise distance (pwd) file as input (instead of alignment).\n";
    print "       No -U, -D, -C, and -N options available in this case.\n";
    print " -V  : Verbose\n";
    print "\n";

}


