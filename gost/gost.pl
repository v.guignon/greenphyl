#!/usr/bin/perl -W

use strict;
use File::Basename;

#system ("gost.pl 1 
#A=$OUT_PIPE/$family_id/$family_id 
#B=$dossier 
#Q=$filename 
#N=$filename 
#O=$TMP/$dossier/out2  
#I C E 2>/dev/null 1>/dev/null") and "execution of gost1 failed: $?"; 
              

my ($alignment, $dossier, $complete_description, 
        $long_output, $query_name, $outfile, $seqX_file, $output_ultraparalogs) = &analyzeCommandLine( @ARGV );


my $matrix_n   = $MATRIX_DEFAULT;


# This creates the temp directory:
# --------------------------------

my $temp_dir = $TEMP_DIR_DEFAULT . $dossier;
mkdir( $temp_dir, 0700 );
unless ( ( -e $temp_dir ) && ( -d $temp_dir ) ) 
{
    die( $temp_dir .  "does not exist or is not a directory" );
}

# The analysis starts here:
# -------------------------

my $output_dir      = dirname( $outfile );

unless ( ( -e $output_dir ) && ( -d $output_dir ) ) {
    die( "Outfile directory (" . $output_dir . ") does not exist,\n or is not a directory." );
}

$query_name =~ s/\/(\S{4}_\S*)$/$1/g;
$query_name = $1;
$query_name = substr( $query_name, 0, $LENGTH_OF_NAME - 10 );

# Prepares the query file:
# ------------------------
$query_name = &seqFile2FastaFile( $seqX_file, "$temp_dir/QUERY_SEQ", $query_name );


# initialize file names:
# ------------------------
my $hmm_file        = $alignment . $SUFFIX_HMM;
my $first_bsp_file  = $alignment . $SUFFIX_MASK;
my $bsp_file        = $alignment . $SUFFIX_BOOT_STRP_POS;
my $dico            = $alignment . $SUFFIX_DICO;
my $pwd_file        = $alignment . $SUFFIX_PWD;
my $nbd_file        = $alignment . $SUFFIX_PWD_NOT_BOOTS;
$alignment          = $alignment . $ALIGN_FILE_SUFFIX;

# Align the sequence of the family alignment
# -------------------------------------------

print "Step1 Align query to family alignment";

my $f = &alignWithHmmalign( $alignment, $temp_dir."/QUERY_SEQ", $hmm_file, $temp_dir."/HMMALIGNOUT", 1 ); 


if ( $f != 1 ) 
{ 
        die( "sequence query have a too low similarity with the HMM profile of gene family: 
            Are you sure your sequence is complete?" );
}

# In case query contains more than one of the same domain:
@complete_names = &getCompleteName( $temp_dir."/HMMALIGNOUT", $query_name );

if ( @complete_names < 1 ) {
    die( "Could not find \"$query_name in $temp_dir"."/HMMALIGNOUT\" " );
}

# This loop goes through the different domains of the query
# which aligned to the alignment 
# -----------------------------------------------------------
for ( $jj = 0; $jj < @complete_names; ++$jj ) 
{
   
    #Move query to the last position in alignment 
    &moveToLast( $complete_names[ $jj ],       
                     $temp_dir."/HMMALIGNOUT",
                     $temp_dir."/MOVETOLASTOUT",
                     \@complete_names );
  
       
    #masking using bsp file
    &HMMparser( $temp_dir."/MOVETOLASTOUT",
                    $alignment,
                    $first_bsp_file,
                    $temp_dir."/ALIGN2_PHYLIP",
                    $query_name,
                    1 );
   
     
    my $infile1  = $temp_dir."/ALIGN2_PHYLIP"."_tmp";
    my $outfile1 = $temp_dir."/ALIGN2_PHYLIP"."_full";
    
    ##  convert id for view alignment 
    my $cmd      = join(" ", $CONVERT_ID,"-i", $infile1,"-o", $outfile1,"-f", $dico,"-d","1>/dev/null");
    system($cmd);
    unlink( $temp_dir."/ALIGN2_PHYLIP"."_tmp");    
  

    unlink( $temp_dir."/ALIGN2_BOOTSTRAPPED" );      
    &executeBootstrap_cz( $BOOTSTRAPS,
                            $bsp_file,
                            $temp_dir."/ALIGN2_PHYLIP",
                            $temp_dir."/ALIGN2_BOOTSTRAPPED" );
   $current_dir = `pwd`;
   $current_dir =~ s/\s//;
  
   chdir ( $temp_dir ) || die( "Could not chdir to \"$temp_dir\"" );
   
   print "Step 3  Compute distance to query";  
   &executePuzzleDQObootstrapped( $temp_dir."/ALIGN2_BOOTSTRAPPED", $matrix_n );
  
   system( "mv", "ALIGN2_BOOTSTRAPPED.dist", "DISTs_TO_QUERY_tmp" )
   and die( "$!" );
      
   &CONVERT_dist($pwd_file,
          $temp_dir."/DISTs_TO_QUERY_tmp",
          $temp_dir."/DISTs_TO_QUERY",
              1 );
   unlink $temp_dir."/DISTs_TO_QUERY_tmp";  
  
   &executePuzzleDQO( $temp_dir."/ALIGN2_PHYLIP", $matrix_n ); 
  
  
   system( "mv", "ALIGN2_PHYLIP.dist", "DIST_TO_QUERY_tmp" )
    and die( "$!" );
  
   &CONVERT_dist($pwd_file,
          $temp_dir."/DIST_TO_QUERY_tmp",
          $temp_dir."/DIST_TO_QUERY",
              1 );
   unlink $temp_dir."/DIST_TO_QUERY_tmp";
  
  
   #print " 12 espaces dans 'addDistsToQueryToPWDfile' \n";
   #100 boostraped matrix
   &addDistsToQueryToPWDfile( $pwd_file,
                              $temp_dir."/DISTs_TO_QUERY",
                              $temp_dir."/PWD_INC_QUERY",
                              $complete_names[ $jj ] );
  
   
   #   1 matrice alignement r�el
   #original matrix
   &addDistsToQueryToPWDfile( $nbd_file,
                              $temp_dir."/DIST_TO_QUERY",
                              $temp_dir."/NBD_INC_QUERY",
                              $complete_names[ $jj ] );  
  
  
   unlink( $temp_dir."/MAKETREEOUT".$TREE_FILE_SUFFIX );
    
   print "Step 4 Compute phylogenomic tree"; 

    # This calculates the trees
    # -------------------------
    
    # for real tree matrix
    &executeNeighbor( $temp_dir."/NBD_INC_QUERY",
                              0,
                              0,
                              0,
                              1 );

    unlink( "outfile" );
    system( "mv", "outtree", "NBD_NJ_TREE" )
    && die( "$!" );  
     
    my $options_for_makeTree .= "F"; #pair wise distance
    # for 100 bootstraped matrices
    &executeMakeTree( $options_for_makeTree,
                          $temp_dir."/PWD_INC_QUERY",
                          $temp_dir."/MAKETREEOUT".$TREE_FILE_SUFFIX,
                          $temp_dir."/maketree_tempdir" );
    

    chdir( $current_dir )  || die( "Could not chdir to \"$current_dir\"" );   

    unlink( $temp_dir."/ALIGN2" );

    my $multiple_trees_file    = $temp_dir."/MAKETREEOUT".$MULTIPLE_TREES_FILE_SUFFIX;
    my $maketree_out_tree_file = $temp_dir."/MAKETREEOUT".$TREE_FILE_SUFFIX;
    my $distance_matrix_file   = $temp_dir."/MAKETREEOUT".$SUFFIX_PWD_NOT_BOOTS;
 
    if ( $mode == 1 || $mode == 3 ) {
        $query_name = $complete_names[ $jj ];
    }
 
   
    print "Step 5 Phylogenomic analyses of the generated tree";  
    
    # Convert id for final display
    
    #print "\t*4-1\t DIST_TO_QUERY \n"; 
    $infile1     = $temp_dir."/DIST_TO_QUERY";
    $outfile1    = $temp_dir."/DIST_TO_QUERY_2"; 
    my $cmd1     = join(" ", $CONVERT_ID,"-i", $infile1,"-o", $outfile1,"-f", $dico,"-d","1>/dev/null");
    system($cmd1);
    system( "mv $outfile1 $infile1" );
    unlink( $outfile1 );
  
    #print "\t*4-2\t NBD_NJ_TREE \n";
    my $infile2  = $temp_dir."/NBD_NJ_TREE";
    my $outfile2 = $temp_dir."/NBD_NJ_TREE_2"; 
    my $cmd2     = join(" ", $CONVERT_ID,"-i", $infile2,"-o", $outfile2,"-f", $dico,"-d","1>/dev/null");
    system($cmd2);
    system( "mv $outfile2 $infile2" );
    unlink( $outfile2 );
  
  
    my $infile3 = $temp_dir."/MAKETREEOUT.mlt";
    my $outfile3 = $temp_dir."/MAKETREEOUT.mlt_2"; 
    my $cmd3 = join(" ", $CONVERT_ID,"-i", $infile3,"-o", $outfile3,"-f", $dico,"-d","1>/dev/null");

    system($cmd3);
    system( "mv $outfile3 $infile3" );
    unlink( $outfile3 );
    
    my $infile4 = $temp_dir."/MAKETREEOUT.nhx";
    my $outfile4 = $temp_dir."/MAKETREEOUT.nhx_2"; 
    my $cmd4 = join(" ", $CONVERT_ID,"-i", $infile4,"-o", $outfile4,"-f", $dico,"-d","1>/dev/null");

    system($cmd4);
    system( "mv $outfile4 $infile4" );
    unlink( $outfile4 );
    
    $query_name=~ s/(\S+_\S+)\/\d+\-\d+/$1/g;
    #convert NHX to PhyloXML (tbd)
    
    
    # put relevant file names
    &executeRIO("dictionary.txt", "rooted_tree.nwk", "bootstrap_trees.nwk", "distance_matrix.txt");
     
} 
# End of for loop going through possible 




#### Function


sub analyzeCommandLine {

    my $args = "";
    my $arg  = "";
    my $char = "";
   
    $mode = shift( @_ );
   
    foreach $args ( @_ ) 
    {
        $args =~ s/\s//g;
        $char = substr( $args, 0, 1 );
         
        if ( length( $args ) > 1 ) {
            $arg = substr( $args, 2 );
        }  
      
        if ( $char =~ /A/ ) 
        {           
            my $alignment = $arg;
        }        
        elsif ( $char =~ /B/ ) 
        {
            my $dossier = $arg;
        }        
        elsif ( $char =~ /C/ ) 
        {       
            my $complete_description = 1;
        }             
        elsif ( $char =~ /E/ ) 
        {
            my $long_output = 1;
        }
                    
        elsif ( $char =~ /N/ ) {
            my $query_name = $arg;
        }
        elsif ( $char =~ /O/ ) {
            my $outfile = $arg;
        }
        elsif ( $char =~ /Q/ ) {
            die( "file $arg does not exist" ) if (! -e $arg );
            my $seqX_file = $arg;
        }
        else {
            die( "Unknown option: \"$args\"." );
        }
    }
    return ($alignment, $dossier, $complete_description, $long_output, $query_name, $outfile, $seqX_file, $output_ultraparalogs);
} 


sub seqFile2FastaFile 
{
    my $infile        = $_[ 0 ];
    my $outfile       = $_[ 1 ];
    my $new_seq_name  = $_[ 2 ];
    my $return_line   = "";
    my $mod_desc      = "";
    my $saw_desc_line = 0;

    &testForTextFilePresence( $infile );

    open( IN_CUFF, "$infile" ) || die( "Cannot open file \"$infile\"" );
    open( OUT_CUFF, ">$outfile" ) || die( "Cannot create file \"$outfile\"" );
   
    print OUT_CUFF ( ">".$new_seq_name."\n" );
    while ( $return_line = <IN_CUFF> ) 
    {     
          if ( $return_line =~ /\w/ ) 
          {
              print OUT_CUFF $return_line;
          }
    }
    $mod_desc = $new_seq_name;
    close( IN_CUFF );
    close( OUT_CUFF );

    return $mod_desc;
}