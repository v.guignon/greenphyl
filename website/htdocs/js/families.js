/**
********************************************************************************
**************************************************
* GreenPhyl Javascript Library - Families module *
**************************************************
Version:  3.1.0
Contacts: Mathieu Rouard <m.rouard@cgiar.org>
          Valentin Guignon <v.guignon@cgiar.org>
Requires: jQuery >=1.9, jQueryUI >=1.10, Raphael >= 2.0.1

Description:
This file contains the Javascript code used on GreenPhyl website to draw
families relationship.

Copyright 2011, Bioversity International - CIRAD

********************************************************************************
*/

/**
Notes:
------

Family boxes + label have name:
'family_' + family.id

Links between a parent family A and a child family B have name:
'link_' + A.id + '_' + B.id
and use tooltip:
'link_' + A.id + '_' + B.id + '_tooltip'

Species chart for a family has name:
'species_chart_' + family.id

Species chart for a links between a parent family A and a child family B have name:
'species_chart_' + A.id + '_' + B.id

*/

var FAMILY_BOX_HEIGHT = 64;
var FAMILY_IPR_BOX_WIDTH = 128;
var FAMILY_IPR_CODE_WIDTH = 60;
var FAMILY_IPR_STATS_WIDTH = 50;

var FAMILY_GO_BOX_WIDTH = 136;
var FAMILY_GO_STATS_WIDTH = 50;

// maximum number of sequence displayed in a tooltip box
var MAX_SEQUENCE_DISPLAY = 8;

// Info box
var FAMILY_INFO_BOX_WIDTH  = 256;
var FAMILY_INFO_BOX_HEIGHT = 256;
var LINK_INFO_BOX_WIDTH  = 256;
var LINK_INFO_BOX_HEIGHT = 320;

// size of pie chart
var PIE_CHART_RADIUS       = 32;
// constant for angle computation
var RAD = Math.PI / 180;

// colors assigned to species
var g_species_color = {
/*
Set in templates.
ex:
  'Arabidopsis thaliana': '#0020ff',
  'Oryza sativa'        : '#e00000',
  'Musa acuminata'      : '#c09000',
*/
};


/**
* ActivateFamily
*****************
Called when the mouse moves over the SVG element representing a family box.

*/
function ActivateFamily(event) {
  if (this.node && $(this.node).prop('name')) {
    //alert('TEST: ' + this.node.raphaelid + '/' + GetKeys($(this.node)).join(', '));
    // find family class name and hightlight family
    $('.' + 'family' + '-' + $(this.node).prop('name').substr(7)).addClass('highlight-family');
    // show tooltip
    showTooltip.call(this.node, event); // we use .call() to for 'this' to not change it
    // show species chart
    $('[name="species_chart_' + $(this.node).prop('name').substr(7) + '"]').show();
  }
}


/**
* DeactivateFamily
*******************
Called when the mouse leaves the SVG element representing a family box.

*/
function DeactivateFamily(event) {
  if (this.node && $(this.node).prop('name')) {
    // find family class name and set family label to normal
    $('.family-' + $(this.node).prop('name').substr(7)).removeClass('highlight-family');
    // hide tooltip
    hideTooltip.call(this.node, event); // we use .call() to for 'this' to not change it
    // hide species chart
    $('[name="species_chart_' + $(this.node).prop('name').substr(7) + '"]').hide();
  }
}


/**
* ShowSpeciesTooltip
*********************
Called when the mouse moves over the SVG element representing a box link.

*/
function ShowSpeciesTooltip(event) {
  // show tooltip
  showTooltip.call(this.node, event); // we use .call() to for 'this' to not change
}


/**
* HideSpeciesTooltip
*********************
Called when the mouse leaves the SVG element representing a box link.

*/
function HideSpeciesTooltip(event) {
  // hide tooltip
  hideTooltip.call(this.node, event); // we use .call() to for 'this' to not change
}


/**
* GetSpeciesColor
******************

*/
function GetSpeciesColor(species) {
  if (!g_species_color[species]) {
    g_species_color[species] = GetRandomColor();
  }
  return g_species_color[species];
}


/**
* DrawSector
*************
Draw a pie  chart sector.

*/
function DrawSector(paper, cx, cy, r, start_angle, end_angle, params) {
  var x1 = cx + r * Math.cos(-start_angle * RAD),
      x2 = cx + r * Math.cos(-end_angle * RAD),
      y1 = cy + r * Math.sin(-start_angle * RAD),
      y2 = cy + r * Math.sin(-end_angle * RAD);
  return paper.path(["M", cx, cy, "L", x1, y1, "A", r, r, 0, + (end_angle - start_angle > 180), 0, x2, y2, "z"]).attr(params);
}


/**
* DisplaySpeciesLegend
***********************
Display species color legend.

Parameters:
* canvas: diagram canvas.

*/
function DisplaySpeciesLegend(canvas) {
  var species_legend = '';
  var species_tooltips = '';
  $.each(g_species_color, function (species, species_color) {
    species_legend += '      <li class="legend-item"><div class="color-box" style="background-color: ' + species_color + ';">&nbsp;</div><div class="description">' + species + '</div></li>\n';
    species_tooltips += '<div id="species_chart_' + species.replace(/\W/, '_') + '_tooltip" class="hidden-tooltip">' + species + '</div>\n';
  });
  species_legend = '  <div class="species-legend">\n    Species Color:<br class="clear"/>\n    <ul>\n' + species_legend + '    </ul>\n  </div>\n  <br class="clear"/>\n';
  var parent = $('#legend ul').first();
  if (!parent.exists()) {
    $(species_legend).insertAfter(canvas.parent());
  }
  else {
    parent.append($('<li>\n' + species_legend + '</li>\n'));
  }
  $('body').append(species_tooltips);
}


/**
* GetSequenceLinkInfoBox
*************************
Display a popup window showing information about the sequence flow between 2
families. It also includes species distribution.

Parameters:
* family_data: a family data structure for parent element;
* child_data: a family data structure for child element;

*/
function GetSequenceLinkInfoBox(family_data, child_data) {
  var link_info_id = 'link_info_' + family_data.id + '_' + child_data.id;
  var link_info_box = $('#' + link_info_id);
  if (!link_info_box.exists()) {
    // create canvas element
    var canvas_width = (2*(PIE_CHART_RADIUS + BOX_HORIZONTAL_SPACE)) + 'px';
    var canvas_height = (2*(PIE_CHART_RADIUS + BOX_HORIZONTAL_SPACE)) + 'px';

    // compute statistics
    var parent_percent = Math.round(100 * child_data.common[family_data.id].count / family_data.count);
    if (1 > parent_percent) {
      parent_percent = 'less than 1'
    }
    var child_percent = Math.round(100 * child_data.common[family_data.id].count / child_data.count);
    if (1 > child_percent) {
      child_percent = 'less than 1'
    }

    var common_seq = '';
    if (child_data.common[family_data.id].count) {
      common_seq = 'Common sequences:<br/>';
      common_seq += child_data.common[family_data.id].first_10_seq.join(', ');
      if (MAX_SEQUENCE_DISPLAY < child_data.common[family_data.id].count) {
        common_seq = '';
      }
    }

    // create DOM element
    $('#' + link_info_id).remove(); // remove previous
    link_info_box =
      $('<div class="link-info" id="' + link_info_id + '"></div>')
        .width(canvas_width)
        .height(canvas_height)
        .appendTo('body')
        .append('<div class="legend">Species composition</div><br/>\n<div>' + child_data.common[family_data.id].count + ' sequences representing ' + parent_percent + '% of ' + family_data.label + ' (upper level) and ' + child_percent + '% of ' + child_data.label + ' (lower level).<br/>' + common_seq + '</div>\n')
      ;

    link_info_box.dialog({
      autoOpen: false,
      height: LINK_INFO_BOX_HEIGHT,
      width: LINK_INFO_BOX_WIDTH,
      modal: false,
      title: 'Cluster composition between ' + family_data.label + ' (lower stringency) and ' + child_data.label + ' (higher stringency)'
    });


    // create Raphael paper object for drawing  and link it to the given canvas
    var paper = Raphael(link_info_id, canvas_width, canvas_height);
    $(paper.node).prop('id', link_info_id + '_paper');

    // draw species chart
    var chart_name = 'species_chart_' + family_data.id + '_' + child_data.id;
    DrawSpeciesChart(paper, PIE_CHART_RADIUS + BOX_HORIZONTAL_SPACE, PIE_CHART_RADIUS + BOX_HORIZONTAL_SPACE, child_data.common[family_data.id].species, child_data.common[family_data.id].count, chart_name);

    // store paper reference
    link_info_box.data('paper', paper);
  }

  return link_info_box;
}


/**
* GetFamilySequenceInfoBox
***************************
Display an info popup about a family.

Parameters:
* family_data: a family data structure.

*/
function GetFamilySequenceInfoBox(family_data) {
  var family_info_box_id = 'family_info_' + family_data.id;
  var family_info_box = $('#' + family_info_box_id);
  if (!family_info_box.exists()) {
    // create canvas element
    var canvas_width = (2*(PIE_CHART_RADIUS + BOX_HORIZONTAL_SPACE)) + 'px';
    var canvas_height = (2*(PIE_CHART_RADIUS + BOX_HORIZONTAL_SPACE)) + 'px';


    // create DOM element
    family_info_box =
      $('<div class="family-info" id="' + family_info_box_id + '"></div>')
        .width(canvas_width)
        .height(canvas_height)
        .appendTo('body')
        .append('<div class="legend">Species composition</div><br/>\n\
          <div>\n\
            <a href="' + BASE_URL + '/' + family_data.class + '.cgi?p=id&accession=' + family_data.label + '#tab-famstruct" title="open family information page">' + family_data.label + '</a><br/>\n\
            <b>Sequences:</b> <span class="sequence-count">' + family_data.count + '</span><br/>\n\
          </div>\n\
          '
        );
      ;

    family_info_box.dialog({
      autoOpen: false,
      height: FAMILY_INFO_BOX_HEIGHT,
      width: FAMILY_INFO_BOX_WIDTH,
      modal: false,
      title: 'Family ' + family_data.label + ' Info'
    });


    // create Raphael paper object for drawing  and link it to the given canvas
    var paper = Raphael(family_info_box_id, canvas_width, canvas_height);
    $(paper.node).prop('id', family_info_box_id + '_paper');

    var chart_name = 'species_chart_' + family_data.id;
    DrawSpeciesChart(paper, PIE_CHART_RADIUS + BOX_HORIZONTAL_SPACE, PIE_CHART_RADIUS + BOX_HORIZONTAL_SPACE, family_data.species, family_data.count, chart_name);

    // store paper reference
    family_info_box.data('paper', paper);
  }

  return family_info_box;
}


/**
* DrawSpeciesChart
*******************
Draw species pie chart.

Parameters:
* paper: Raphael paper to draw on
* x, y: coordinates of the pie center
* species_data:
* count:
* chart_name:

*/
function DrawSpeciesChart(paper, x, y, species_data, count, chart_name) {

  var current_angle = 90,
      new_current_angle = 0,
      attributes = {
        'stroke'      : '#000000',
        'stroke-width': 1
      };
  // check if we got more than one species
  if (GetKeys(species_data).length > 1) {
    var sorted_species = [];
    $.each(species_data, function(species, count) {
        sorted_species.push({'species' : species, 'count': count});
    });
    // sort species
    sorted_species = sorted_species.sort(function (a, b) {return a.count - b.count;});

    // draw pie
    $.each(sorted_species, function(i, species_stats) {
      species = species_stats.species;
      species_seq_count = species_stats.count;
      new_current_angle = current_angle + 360*(species_seq_count / count);
      attributes.fill = GetSpeciesColor(species);
      var sector_name = chart_name + '_' + species.replace(/\W/, '_');
      var sector = DrawSector(paper, x, y, PIE_CHART_RADIUS, current_angle, new_current_angle, attributes);
      $(sector.node)
        .prop('name', sector_name)
      ;
      sector.hover(ShowSpeciesTooltip, HideSpeciesTooltip);
      sector.hover(ShowSpeciesTooltip, HideSpeciesTooltip);
      $('body').append('<div id="' + sector_name + '_tooltip" class="hidden-tooltip">' + species + ' (' + Math.round(100 * species_seq_count / count) + '%)</div>\n');
      current_angle = new_current_angle;
    });
  }
  else {
    var species = GetKeys(species_data)[0];
    var circle_name = chart_name + '_' + species.replace(/\W/, '_');
    var circle = paper
      .circle(x, y, PIE_CHART_RADIUS)
      .attr({
        'stroke'      : '#000000',
        'stroke-width': 1,
        'fill'        : GetSpeciesColor(species)
      })
    ;
    $(circle.node)
      .prop('name', circle_name)
    ;
    circle.hover(ShowSpeciesTooltip, HideSpeciesTooltip);
    circle.hover(ShowSpeciesTooltip, HideSpeciesTooltip);
    $('body').append('<div id="' + circle_name + '_tooltip" class="hidden-tooltip">' + species + ' (100%)</div>\n');
    current_angle = 450;
  }

  // if all sequences are not available
  if (Math.round(current_angle - 450) < 0) {
    attributes.fill = '#808080';
    var sector = DrawSector(paper, x, y, PIE_CHART_RADIUS, current_angle, 450, attributes);
  }

  var circle = paper
    .circle(x, y, PIE_CHART_RADIUS)
    .attr({
      'stroke'        : '#000000',
      'stroke-width'  : 1
    });
}


/**
* DrawFamilySequencesConnectors
********************************
Draw connectors between family boxes for sequence relationship.

Parameters:
* paper: Raphael paper to draw on
* family_data: family data including top-left coordinate of current family box
* children_data: children family data including top-left coordinate of boxes

*/
function DrawFamilySequencesConnectors(paper, family_data, children_data) {

  // process each child
  $.each(children_data, function(i, child_data) {

    var link = DrawDefaultConnector(paper, family_data, child_data);
    link.attr({'cursor': 'pointer'});

    // note: Raphael can not set ID attribute
    $(link.node).prop('name', 'link_' + family_data.id + '_' + child_data.id);

    // click event
    var ShowLinkInfoBox = function() {
      var link_info_box = GetSequenceLinkInfoBox(family_data, child_data);
      link_info_box.dialog('open');
    };
    link.click(ShowLinkInfoBox)

    //+FIXME: mouseover?
    //...
  });

}
// add handler
g_draw_connectors_handlers['family-sequences'] = DrawFamilySequencesConnectors;


/**
* DrawFamilySequencesBox
*************************
Draws a family box for sequence relationship.

Parameters:
* paper: Raphael paper to draw on
* left, top: top-left coordinate of the area to draw in
* family_data: object containing family data

*/
function DrawFamilySequencesBox(paper, left, top, family_data) {

  family_data.fixed_height = FAMILY_BOX_HEIGHT;
  if (family_data.label.length > 8) {
    family_data.fixed_height += 6 * (family_data.label.length - 8);
  }
  DrawDefaultBox(paper, left, top, family_data);

  var family_html  = $(family_data.selector).first();
  var family_box   = family_data.svg_box;
  var family_label = family_data.svg_label;

  family_box.attr({'cursor': 'pointer'});
  family_label.attr({'cursor': 'pointer'});

  // draw underline if annoation is available
  if (family_html.exists() && family_html.is('.annotated-family')) {

    var underline_length = family_data.svg_box.getBBox().height - TEXT_PADDING;
    if ((family_data.svg_label) && (family_data.svg_label.getBBox().height < underline_length)) {
      underline_length = family_data.svg_label.getBBox().height;
    }
    var family_underline = paper
      .path(
        'M' +
        (left + BOX_HORIZONTAL_SPACE/2. + TEXT_PADDING) +
        ',' +
        (top + BOX_VERTICAL_SPACE/2. + TEXT_PADDING) +
        'L' +
        (left + BOX_HORIZONTAL_SPACE/2. + TEXT_PADDING) +
        ',' +
        (top + BOX_VERTICAL_SPACE/2. + underline_length)
      )
      .attr({
        'stroke': family_data.svg_label.attr('fill'),
        'stroke-width': 1,
        'stroke-dasharray': '.',
        'opacity': family_data.svg_label.attr('opacity')
      })
    ;
  }
  family_data.svg_set.push(family_underline);

  // set name attribute of the element using jQuery so the tooltip functions can
  // retrieve the associated tooltip
  // note: Raphael can not set ID attribute
  $(family_box.node)
    .prop('name', family_data.class + '_' + family_data.id)
    .addClass('.family-box')
  ;
  $(family_label.node)
    .prop('name', family_data.class + '_' + family_data.id)
    .addClass('.family-box')
  ;
  // add tooltip (set event handler on SVG object)
  family_box.hover(ActivateFamily, DeactivateFamily);
  family_label.hover(ActivateFamily, DeactivateFamily);

  // click event
  var ShowFamilyInfoBox = function() {
    var family_info_box = GetFamilySequenceInfoBox(family_data);
    family_info_box.dialog('open');
  };
  family_box.click(ShowFamilyInfoBox);
  family_label.click(ShowFamilyInfoBox);

  // make sure associated species are set
  $.each(family_data.species, function(species, i) {
    GetSpeciesColor(species);
  });

  // save and return boundaries
  return family_data.boundaries;

}
// add handler
g_draw_box_handlers['family-sequences'] = DrawFamilySequencesBox;




/*
********************************************************************************
* IPR
********************************************************************************
*/

/**
* GetIPRLinkInfo
*****************
.

Parameters:
* family_data: a family data structure for parent element;
* child_data: a family data structure for child element;

*/
function GetIPRLinkInfo(family_data, child_data) {
  // compute statistics
  var parent_percent = Math.round(100 * child_data.common[family_data.id].count / family_data.count);
  if (1 > parent_percent) {
    parent_percent = 'less than 1'
  }
  var child_percent = Math.round(100 * child_data.common[family_data.id].count / child_data.count);
  if (1 > child_percent) {
    child_percent = 'less than 1'
  }

  var parent_total_percent = Math.round(100 * child_data.count / family_data.total_count);
  var child_total_percent = Math.round(100 * child_data.count / child_data.total_count);

  // check for multiple-IPR domains
  var link_info;
  if (1 < GetKeys(child_data.iprs).length) {
    link_info = child_data.common[family_data.id].count + ' common sequences bearing at least one of the selected IPR domains representing<br/>' + parent_percent + '% of the sequences of <span class="family-id family-' + family_data.label + '">' + family_data.label + '</span> (upper level) bearing at least one selected IPR domains<br/> &nbsp; (' + parent_total_percent + '% of the whole family sequences) and<br/>' + child_percent + '% of the sequences of <span class="family-id family-' + child_data.label + '">' + child_data.label + '</span> (lower level) bearing at least one of the selected IPR domains<br/> &nbsp; (' + child_total_percent + '% of the whole family sequences).<br/>';
  }
  else {
    var ipr_domain = GetKeys(child_data.iprs)[0];
    link_info = child_data.common[family_data.id].count + ' common sequences bearing the IPR domain <span class="ipr ipr-' + ipr_domain + '  ipr_code_' + ipr_domain + '">' + ipr_domain + '</span> representing<br/>' + parent_percent + '% of the sequences of <span class="family-id family-' + family_data.label + '">' + family_data.label + '</span> (upper level) bearing that IPR domain<br/> &nbsp; (' + parent_total_percent + '% of the whole family sequences) and<br/>' + child_percent + '% of the sequences of <span class="family-id family-' + child_data.label + '">' + child_data.label + '</span> (lower level) bearing that IPR domain<br/> &nbsp; (' + child_total_percent + '% of the whole family sequences).<br/>';
  }
  return link_info;
}


/**
* GetIPRLinkInfoBox
********************
Display a popup window showing information about the IPR flow between 2
families.

Parameters:
* family_data: a family data structure for parent element;
* child_data: a family data structure for child element;

*/
function GetIPRLinkInfoBox(family_data, child_data) {
  var link_info_id = 'ipr_link_info_' + family_data.id + '_' + child_data.id;
  var link_info_box = $('#' + link_info_id);
  if (!link_info_box.exists()) {

    // create DOM element
    link_info_box =
      $('<div class="link-info" id="' + link_info_id + '"></div>')
        .width(FAMILY_INFO_BOX_WIDTH)
        .height(FAMILY_INFO_BOX_HEIGHT)
        .appendTo('body')
        .append('<div>' + GetIPRLinkInfo(family_data, child_data) + '</div>\n')
      ;

    link_info_box.dialog({
      autoOpen: false,
      height: LINK_INFO_BOX_HEIGHT,
      width: LINK_INFO_BOX_WIDTH,
      modal: false,
      title: 'Cluster composition between ' + family_data.label + ' (lower stringency) and ' + child_data.label + ' (higher stringency)'
    });

  }

  return link_info_box;
}


/**
* GetFamilyIPRInfoBox
**********************
Display an info popup about a family.

Parameters:
* family_data: a family data structure.

*/
function GetFamilyIPRInfoBox(family_data) {
  var family_info_box_id = 'ipr_family_info_' + family_data.id;
  var family_info_box = $('#' + family_info_box_id);
  if (!family_info_box.exists()) {

    var total_percent = Math.round(100 * family_data.count / family_data.total_count);
    // create DOM element
    family_info_box =
      $('<div class="family-info" id="' + family_info_box_id + '"></div>')
        .width(FAMILY_INFO_BOX_WIDTH)
        .height(FAMILY_INFO_BOX_HEIGHT)
        .appendTo('body')
        .append('\n\
            <a href="' + BASE_URL + '/' + family_data.class + '.cgi?p=id&accession=' + family_data.label + '#tab-famstruct" title="open family information page">' + family_data.label + '</a><br/>\n\
            <b>Sequences bearing the IPR domain:</b> <span class="sequence-count">' + family_data.count + ' (' + total_percent + '% of family sequences)</span><br/>\n\
          \n\
          '
        );
      ;

    family_info_box.dialog({
      autoOpen: false,
      height: FAMILY_INFO_BOX_HEIGHT,
      width: FAMILY_INFO_BOX_WIDTH,
      modal: false,
      title: 'Family ' + family_data.label + ' Info'
    });

  }

  return family_info_box;
}


/**
* DrawFamilyIPRConnectors
**************************
Draw connectors between family boxes for IPR relationship.

Parameters:
* paper: Raphael paper to draw on
* family_data: family data including top-left coordinate of current family box
* children_data: children family data including top-left coordinate of boxes

*/
function DrawFamilyIPRConnectors(paper, family_data, children_data) {
  // process each child
  $.each(children_data, function(i, child_data) {

    var link = DrawDefaultConnector(paper, family_data, child_data);

    // DOM name
    var dom_name = 'link_' + family_data.id + '_' + child_data.id;
    // note: Raphael can not set name attribute
    $(link.node)
      .prop({
        'name': dom_name
//+val: tooltip disabled        'class': 'tooltiped'
      })
    ;

/*+val: tooltip disabled
    // add tooltip element
    $('#' + dom_name).remove(); // remove any previous element
    $('body').append('<div class="hidden-tooltip" id="' + dom_name + '_tooltip">' + GetIPRLinkInfo(family_data, child_data) + '</div>');
*/
    /*
    //+val: popup disabled

    // link.attr({'cursor': 'pointer'});
    // click event
    link.mousedown(
      function() {
        link.mousetracker = true;
      }
    );

    link.mousemove(
      function() {
        link.mousetracker = false;
      }
    );

    link.mouseup(
      function() {
        if (link.mousetracker) {
          var link_info_box = GetIPRLinkInfoBox(family_data, child_data);
          link_info_box.dialog('open');
          link.mousetracker = false;
        }
      }
    );
    */

  });

}
// add handler
g_draw_connectors_handlers['family-ipr'] = DrawFamilyIPRConnectors;


/**
* DrawFamilyIPRBox
*******************
Draws a family box for IPR relationship.

Parameters:
* paper: Raphael paper to draw on
* left, top: top-left coordinate of the area to draw in
* family_data: object containing family data

*/
function DrawFamilyIPRBox(paper, left, top, family_data) {

  // family_data.fixed_height = FAMILY_BOX_HEIGHT;
  family_data.fixed_width = FAMILY_IPR_BOX_WIDTH;
/*
  family_data.label += '\n' + family_data.total_count + ' sequence' + (1 < family_data.total_count ? 's' : '') + '\nIPR statistics:\n';
  $.each(GetKeys(family_data.iprs), function(i, ipr_code) {
    var ipr_count_ratio = Math.round(100 * family_data.iprs[ipr_code].count / family_data.total_count);
    if (1 > ipr_count_ratio) {
        ipr_count_ratio = '<1';
    }
    family_data.label += ipr_code + ': ' + family_data.iprs[ipr_code].count + ' (' + ipr_count_ratio + '%) ' + (0 < family_data.iprs[ipr_code].specificity ? 'Y' : 'N') + '\n';
  });
*/

  DrawDefaultBox(paper, left, top, family_data);

  // get new line position from current label position
  var current_text_x = family_data.svg_label.attr('x');
  var current_text_y = family_data.svg_label.attr('y') + family_data.svg_label.getBBox().height + TEXT_PADDING;

  // add sequence count
  var sequence_count_text = family_data.total_count + ' sequence' + (1 < family_data.total_count ? 's' : '');
  var sequence_count_box = paper
    .text(
      current_text_x,
      current_text_y,
      sequence_count_text
    )
    .attr({
      'fill': '#404040',
      'opacity': family_data.svg_label.attr('opacity'),
      'text-anchor': 'start',
      'font-size': FONT_SIZE,
      'transform': family_data.svg_label.attr('transform')
    })
  ;
  // add SVG element to SVG set
  family_data.svg_set.push(sequence_count_box);
  // compute new text position
  current_text_y += sequence_count_box.getBBox().height + TEXT_PADDING;
  // compute new box boundaries
  family_data.svg_box.attr('height', family_data.svg_box.attr('height') + sequence_count_box.getBBox().height + TEXT_PADDING);

  // IPR statistics
  $.each(GetKeys(family_data.iprs), function(i, ipr_code) {
    // default text attributes
    var text_attributes = {
      'fill': '#404040',
      'opacity': family_data.svg_label.attr('opacity'),
      'text-anchor': 'start',
      'font-size': FONT_SIZE,
      'transform': family_data.svg_label.attr('transform')
    };

    // try to get associated IPR HTML element
    var ipr_html  = $('.ipr-' + ipr_code).first();

    // set a new IPR color
    try {
      text_attributes.fill = GetAndSetIPRColor(ipr_code);
    }
    catch(error) {
      if (ipr_html.exists()) {
        if (ipr_html.css('color') && (ipr_html.css('color') != 'transparent')) {
          text_attributes.fill = ipr_html.css('color');
        }
      }
      else if (DEBUG) {
        throw error;
      }
    }

    // check for specificity
    if (0 < family_data.iprs[ipr_code].specificity) {
      text_attributes['font-weight'] = 'bold';
    }

    // IPR label
    var ipr_stats_code_box = paper
      .text(
        current_text_x,
        current_text_y,
        ipr_code
      )
      .attr(text_attributes)
    ;
    // add SVG element to SVG set
    family_data.svg_set.push(ipr_stats_code_box);

    // statistics
    var ipr_count_ratio = Math.round(100 * family_data.iprs[ipr_code].count / family_data.total_count);
    if (1 > ipr_count_ratio) {
        ipr_count_ratio = '<1';
    }
    var ipr_stats_data_text = family_data.iprs[ipr_code].count + ' (' + ipr_count_ratio + '%)';
    var ipr_stats_data_box = paper
      .text(
        current_text_x + FAMILY_IPR_CODE_WIDTH,
        current_text_y,
        ipr_stats_data_text
      )
      .attr(text_attributes)
    ;
    // add SVG element to SVG set
    family_data.svg_set.push(ipr_stats_data_box);

    // statistics
    var ipr_specificity_text = (0 < family_data.iprs[ipr_code].specificity ? 'Y' : 'N');
    var ipr_specificity_box = paper
      .text(
        current_text_x + FAMILY_IPR_CODE_WIDTH + FAMILY_IPR_STATS_WIDTH,
        current_text_y,
        ipr_specificity_text
      )
      .attr(text_attributes)
    ;
    // add SVG element to SVG set
    family_data.svg_set.push(ipr_specificity_box);

    // compute new text position
    current_text_y += ipr_stats_code_box.getBBox().height + TEXT_PADDING;
    // compute new box boundaries
    family_data.svg_box.attr('height', family_data.svg_box.attr('height') + ipr_stats_code_box.getBBox().height + TEXT_PADDING);
  });

  var family_html  = $(family_data.selector).first();
  var family_box   = family_data.svg_box;
  var family_label = family_data.svg_label;

  // draw underline if annoation is available
  if (family_html.exists() && family_html.is('.annotated-family')) {

    var underline_length = family_data.svg_box.getBBox().height - TEXT_PADDING;
    if ((family_data.svg_label) && (family_data.svg_label.getBBox().height < underline_length)) {
      underline_length = family_data.svg_label.getBBox().height;
    }
    var family_underline = paper
      .path(
        'M' +
        (left + BOX_HORIZONTAL_SPACE/2. + TEXT_PADDING) +
        ',' +
        (top + BOX_VERTICAL_SPACE/2. + TEXT_PADDING) +
        'L' +
        (left + BOX_HORIZONTAL_SPACE/2. + TEXT_PADDING) +
        ',' +
        (top + BOX_VERTICAL_SPACE/2. + underline_length)
      )
      .attr({
        'stroke': family_data.svg_label.attr('fill'),
        'stroke-width': 1,
        'stroke-dasharray': '.',
        'opacity': family_data.svg_label.attr('opacity')
      })
    ;
  }
  family_data.svg_set.push(family_underline);

  // set name attribute of the element using jQuery so the tooltip functions can
  // retrieve the associated tooltip
  // note: Raphael can not set ID attribute
  family_data.svg_set.forEach(
    function () {
      $(this)
        .prop('name', 'family_' + family_data.id)
        .addClass('.family-box')
      ;
    }
  );

  // add tooltip (set event handler on SVG object)
  $(family_data.svg_label)
    .prop({
//      'name': 'family_' + family_data.id,
      'class': 'tooltiped'
    })
  ;

  // family_data.svg_label.hover(
  //   function(event) {
  //     showTooltip.call(this.node, event); // we use .call() to for 'this' to not change it
  //   }, 
  //   function(event) {
  //     hideTooltip.call(this.node, event); // we use .call() to for 'this' to not change it
  //   }
  // );

/*
  //+val: popup disabled becauses statistics are already displayed
  // click event (cancel click when dragging)
  family_data.svg_set.mousedown(
    function() {
      family_data.mousetracker = true;
    }
  );

  family_data.svg_set.mousemove(
    function() {
      family_data.mousetracker = false;
    }
  );

  family_data.svg_set.mouseup(
    function() {
      if (family_data.mousetracker) {
        var family_info_box = GetFamilyIPRInfoBox(family_data);
        family_info_box.dialog('open');
        family_data.mousetracker = false;
      }
    }
  );

  family_data.svg_set.attr({'cursor': 'pointer'});
*/

  // update and return boundaries
  return family_data.boundaries = family_data.svg_box.getBBox();

}
// add handler
g_draw_box_handlers['family-ipr'] = DrawFamilyIPRBox;


/*+DEBUG test
function ScaleSubtree(object_data) {
  $.each(object_data.children, function(i, sub_object_data) {
    if (1 == sub_object_data.parents.length) {
      ScaleSubtree(sub_object_data);
      if (sub_object_data.svg_set) {
        sub_object_data.svg_set.transform('...S0.5,1');
      }
      if (sub_object_data.svg_parent_connectors) {
          $.each(sub_object_data.svg_parent_connectors, function(i, sub_object_link) {
            sub_object_link.transform('...S0.5,1');
          });
      }
    }
  });
}
*/


/*
********************************************************************************
* GO
********************************************************************************
*/

/**
* GetGOLinkInfo
****************
.

Parameters:
* family_data: a family data structure for parent element;
* child_data: a family data structure for child element;

*/
function GetGOLinkInfo(family_data, child_data) {
  // compute statistics
  var parent_percent = Math.round(100 * child_data.common[family_data.id].count / family_data.count);
  if (1 > parent_percent) {
    parent_percent = 'less than 1'
  }
  var child_percent = Math.round(100 * child_data.common[family_data.id].count / child_data.count);
  if (1 > child_percent) {
    child_percent = 'less than 1'
  }

  var parent_total_percent = Math.round(100 * child_data.count / family_data.total_count);
  var child_total_percent = Math.round(100 * child_data.count / child_data.total_count);

  // check for multiple-GO
  var link_info;
  if (1 < GetKeys(child_data.gos).length) {
    link_info = child_data.common[family_data.id].count + ' common sequences related to at least one of the selected GO representing<br/>' + parent_percent + '% of the sequences of <span class="family-id family-' + family_data.label + '">' + family_data.label + '</span> (upper level) related to at least one selected GO<br/> &nbsp; (' + parent_total_percent + '% of the whole family sequences) and<br/>' + child_percent + '% of the sequences of <span class="family-id family-' + child_data.label + '">' + child_data.label + '</span> (lower level) related to at least one of the selected GO<br/> &nbsp; (' + child_total_percent + '% of the whole family sequences).<br/>';
  }
  else {
    var go = GetKeys(child_data.gos)[0];
    link_info = child_data.common[family_data.id].count + ' common sequences related to the GO <span class="go go-' + go + '">' + go + '</span> representing<br/>' + parent_percent + '% of the sequences of <span class="family-id family-' + family_data.label + '">' + family_data.label + '</span> (upper level) related to that GO<br/> &nbsp; (' + parent_total_percent + '% of the whole family sequences) and<br/>' + child_percent + '% of the sequences of <span class="family-id family-' + child_data.label + '">' + child_data.label + '</span> (lower level) related to that GO<br/> &nbsp; (' + child_total_percent + '% of the whole family sequences).<br/>';
  }
  return link_info;
}


/**
* GetGOLinkInfoBox
*******************
Display a popup window showing information about the GO flow between 2
families.

Parameters:
* family_data: a family data structure for parent element;
* child_data: a family data structure for child element;

*/
function GetGOLinkInfoBox(family_data, child_data) {
  var link_info_id = 'go_link_info_' + family_data.id + '_' + child_data.id;
  var link_info_box = $('#' + link_info_id);
  if (!link_info_box.exists()) {

    // create DOM element
    link_info_box =
      $('<div class="link-info" id="' + link_info_id + '"></div>')
        .width(FAMILY_INFO_BOX_WIDTH)
        .height(FAMILY_INFO_BOX_HEIGHT)
        .appendTo('body')
        .append('<div>' + GetGOLinkInfo(family_data, child_data) + '</div>\n')
      ;

    link_info_box.dialog({
      autoOpen: false,
      height: LINK_INFO_BOX_HEIGHT,
      width: LINK_INFO_BOX_WIDTH,
      modal: false,
      title: 'Cluster composition between ' + family_data.label + ' (lower stringency) and ' + child_data.label + ' (higher stringency)'
    });

  }

  return link_info_box;
}


/**
* GetFamilyGOInfoBox
*********************
Display an info popup about a family.

Parameters:
* family_data: a family data structure.

*/
function GetFamilyGOInfoBox(family_data) {
  var family_info_box_id = 'go_family_info_' + family_data.id;
  var family_info_box = $('#' + family_info_box_id);
  if (!family_info_box.exists()) {

    var total_percent = Math.round(100 * family_data.count / family_data.total_count);
    // create DOM element
    family_info_box =
      $('<div class="family-info" id="' + family_info_box_id + '"></div>')
        .width(FAMILY_INFO_BOX_WIDTH)
        .height(FAMILY_INFO_BOX_HEIGHT)
        .appendTo('body')
        .append('\n\
            <a href="' + BASE_URL + '/' + family_data.class + '.cgi?p=id&accession=' + family_data.label + '#tab-famstruct" title="open family information page">' + family_data.label + '</a><br/>\n\
            <b>Sequences related to the GO:</b> <span class="sequence-count">' + family_data.count + ' (' + total_percent + '% of family sequences)</span><br/>\n\
          \n\
          '
        );
      ;

    family_info_box.dialog({
      autoOpen: false,
      height: FAMILY_INFO_BOX_HEIGHT,
      width: FAMILY_INFO_BOX_WIDTH,
      modal: false,
      title: 'Family ' + family_data.label + ' Info'
    });

  }

  return family_info_box;
}


/**
* DrawFamilyGOConnectors
*************************
Draw connectors between family boxes for GO relationship.

Parameters:
* paper: Raphael paper to draw on
* family_data: family data including top-left coordinate of current family box
* children_data: children family data including top-left coordinate of boxes

*/
function DrawFamilyGOConnectors(paper, family_data, children_data) {
  // process each child
  $.each(children_data, function(i, child_data) {

    var link = DrawDefaultConnector(paper, family_data, child_data);

    // DOM name
    var dom_name = 'link_' + family_data.id + '_' + child_data.id;
    // note: Raphael can not set name attribute
    $(link.node)
      .prop({
        'name': dom_name
//+val: tooltip disabled        'class': 'tooltiped'
      })
    ;

/*+val: tooltip disabled
    // add tooltip element
    $('#' + dom_name).remove(); // remove any previous element
    $('body').append('<div class="hidden-tooltip" id="' + dom_name + '_tooltip">' + GetIPRLinkInfo(family_data, child_data) + '</div>');
*/
    /*
    //+val: popup disabled

    // link.attr({'cursor': 'pointer'});
    // click event
    link.mousedown(
      function() {
        link.mousetracker = true;
      }
    );

    link.mousemove(
      function() {
        link.mousetracker = false;
      }
    );

    link.mouseup(
      function() {
        if (link.mousetracker) {
          var link_info_box = GetIPRLinkInfoBox(family_data, child_data);
          link_info_box.dialog('open');
          link.mousetracker = false;
        }
      }
    );
    */

  });

}
// add handler
g_draw_connectors_handlers['family-go'] = DrawFamilyGOConnectors;


/**
* DrawFamilyGOBox
******************
Draws a family box for GO relationship.

Parameters:
* paper: Raphael paper to draw on
* left, top: top-left coordinate of the area to draw in
* family_data: object containing family data

*/
function DrawFamilyGOBox(paper, left, top, family_data) {

  // family_data.fixed_height = FAMILY_BOX_HEIGHT;
  family_data.fixed_width = FAMILY_GO_BOX_WIDTH;
  DrawDefaultBox(paper, left, top, family_data);

  // get new line position from current label position
  var current_text_x = family_data.svg_label.attr('x');
  var current_text_y = family_data.svg_label.attr('y') + family_data.svg_label.getBBox().height + TEXT_PADDING;

  // add sequence count
  var sequence_count_text = family_data.total_count + ' sequence' + (1 < family_data.total_count ? 's' : '') + ' total';
  var sequence_count_box = paper
    .text(
      current_text_x,
      current_text_y,
      sequence_count_text
    )
    .attr({
      'fill': '#404040',
      'opacity': family_data.svg_label.attr('opacity'),
      'text-anchor': 'start',
      'font-size': FONT_SIZE,
      'transform': family_data.svg_label.attr('transform')
    })
  ;
  // add SVG element to SVG set
  family_data.svg_set.push(sequence_count_box);
  // compute new text position
  current_text_y += sequence_count_box.getBBox().height + TEXT_PADDING;
  // compute new box boundaries
  family_data.svg_box.attr('height', family_data.svg_box.attr('height') + sequence_count_box.getBBox().height + TEXT_PADDING);
  //family_data.svg_box.attr('width', FAMILY_GO_BOX_WIDTH);

  // GO statistics
  $.each(GetKeys(family_data.gos), function(i, go_code) {
    // default text attributes
    var text_attributes = {
      'fill': '#404040',
      'opacity': family_data.svg_label.attr('opacity'),
      'text-anchor': 'start',
      'font-size': FONT_SIZE,
      'transform': family_data.svg_label.attr('transform')
    };

    // try to get associated GO HTML element
    var go_html  = $('.go-' + go_code).first();

    // check for percent
    if (50 < family_data.gos[go_code].percent) {
      text_attributes['font-weight'] = 'bold';
    }

    /*
    // GO label
    var go_stats_code_box = paper
      .text(
        current_text_x,
        current_text_y,
        go_code
      )
      .attr(text_attributes)
    ;
    // add SVG element to SVG set
    family_data.svg_set.push(go_stats_code_box);

    // compute new text position
    current_text_y += go_stats_code_box.getBBox().height + TEXT_PADDING;
    */

    // statistics
    var go_count_ratio = Math.round(100 * family_data.gos[go_code].count / family_data.total_count);
    if (1 > go_count_ratio) {
        go_count_ratio = '<1';
    }
    var go_stats_data_text = family_data.gos[go_code].count + ' (' + go_count_ratio + '%) related to GO';
    var go_stats_data_box = paper
      .text(
        current_text_x,
        current_text_y,
        go_stats_data_text
      )
      .attr(text_attributes)
    ;
    // add SVG element to SVG set
    family_data.svg_set.push(go_stats_data_box);
    // compute new text position
    current_text_y += go_stats_data_box.getBBox().height + TEXT_PADDING;

    // statistics
    var go_source_text = 'IPR sources: ' + family_data.gos[go_code].ipr + '\nUniProt sources: ' + family_data.gos[go_code].uniprot + '\n\Evidence code(s):\n  ' + Object.keys(family_data.gos[go_code].evidence_codes);
    var go_source_box = paper
      .text(
        current_text_x,
        current_text_y + 2*FONT_SIZE,
        go_source_text
      )
      .attr(text_attributes)
    ;
    // add SVG element to SVG set
    family_data.svg_set.push(go_source_box);
    // compute new text position
    current_text_y += go_source_box.getBBox().height + TEXT_PADDING;

    // compute new box boundaries
    // family_data.svg_box.attr('height', family_data.svg_box.attr('height') + go_stats_code_box.getBBox().height + TEXT_PADDING);
    family_data.svg_box.attr('height', family_data.svg_box.attr('height') + go_stats_data_box.getBBox().height + TEXT_PADDING + go_source_box.getBBox().height + TEXT_PADDING);
  });

  var family_html  = $(family_data.selector).first();
  var family_box   = family_data.svg_box;
  var family_label = family_data.svg_label;

  // draw underline if annoation is available
  if (family_html.exists() && family_html.is('.annotated-family')) {

    var underline_length = family_data.svg_box.getBBox().height - TEXT_PADDING;
    if ((family_data.svg_label) && (family_data.svg_label.getBBox().height < underline_length)) {
      underline_length = family_data.svg_label.getBBox().height;
    }
    var family_underline = paper
      .path(
        'M' +
        (left + BOX_HORIZONTAL_SPACE/2. + TEXT_PADDING) +
        ',' +
        (top + BOX_VERTICAL_SPACE/2. + TEXT_PADDING) +
        'L' +
        (left + BOX_HORIZONTAL_SPACE/2. + TEXT_PADDING) +
        ',' +
        (top + BOX_VERTICAL_SPACE/2. + underline_length)
      )
      .attr({
        'stroke': family_data.svg_label.attr('fill'),
        'stroke-width': 1,
        'stroke-dasharray': '.',
        'opacity': family_data.svg_label.attr('opacity')
      })
    ;
  }
  family_data.svg_set.push(family_underline);

  // set name attribute of the element using jQuery so the tooltip functions can
  // retrieve the associated tooltip
  // note: Raphael can not set ID attribute
  family_data.svg_set.forEach(
    function () {
      $(this)
        .prop('name', 'family_' + family_data.id)
        .addClass('.family-box')
      ;
    }
  );

  // add tooltip (set event handler on SVG object)
  $(family_data.svg_label)
    .prop({
//      'name': 'family_' + family_data.id,
      'class': 'tooltiped'
    })
  ;

  // family_data.svg_label.hover(
  //   function(event) {
  //     showTooltip.call(this.node, event); // we use .call() to for 'this' to not change it
  //   }, 
  //   function(event) {
  //     hideTooltip.call(this.node, event); // we use .call() to for 'this' to not change it
  //   }
  // );

  //+val: popup disabled becauses statistics are already displayed
  /*
  // click event (cancel click when dragging)
  family_data.svg_set.mousedown(
    function() {
      family_data.mousetracker = true;
    }
  );

  family_data.svg_set.mousemove(
    function() {
      family_data.mousetracker = false;
    }
  );

  family_data.svg_set.mouseup(
    function() {
      if (family_data.mousetracker) {
        var family_info_box = GetFamilyGOInfoBox(family_data);
        family_info_box.dialog('open');
        family_data.mousetracker = false;
      }
    }
  );

  family_data.svg_set.attr({'cursor': 'pointer'});
  */

  // update and return boundaries
  return family_data.boundaries = family_data.svg_box.getBBox();

}
// add handler
g_draw_box_handlers['family-go'] = DrawFamilyGOBox;
