/**
********************************************************************************
***********************************************
* GreenPhyl Javascript Library - Chart module *
***********************************************
Version:  3.1.0
Contacts: Mathieu Rouard <m.rouard@cgiar.org>
          Valentin Guignon <v.guignon@cgiar.org>
Requires: jQuery >=1.9, jQueryUI >=1.10, Raphael >= 2.0.1

Description: Draws a chart diagram generated from HTML table data.

Copyright 2012, Bioversity International - CIRAD

********************************************************************************
*/

var CHART_HORIZONTAL_PADDING = 100;
var CHART_TOP_PADDING        = 30;
var CHART_BOTTOM_PADDING     = 172;
var TEXT_COLOR               = '#000000';
var BAR_LINE_WIDTH           = 2;
var TICKS_LENGTH             = 8;
var MAIN_AXIS_WIDTH          = 2;
var MAIN_AXIS_COLOR          = '#404040';
var SECONDARY_AXIS_WIDTH     = 1;
var SECONDARY_AXIS_COLOR     = '#808080';
var BOTTOM_LABEL_SIZE        = '14px'; // font size
var BOTTOM_LABEL_ANGLE       = 60; // degrees
var LEFT_LABEL_SIZE          = '12px'; // font size
var TITLE_TOP_PADDING        = 16;
var TITLE_LABEL_SIZE         = '16px'; // font size
var BAR_HIGHLIGHT_STYLE      = {
  'opacity': 1,
  'width'  : 8,
  'color'  : '#ffffff'
};

/**
* DrawBarChartAxis
*******************
Draws axis with ticks, background grid and units/labels.

Parameters:
-chart: chart object.

*/
function DrawBarChartAxis(chart) {

  var max_height = chart.y + chart.h;
  var chart_bottom = chart.bars[0][0].y + chart.bars[0][0].h;

  // draw bar labels (bottom labels)
  for (var i = 0; chart.bars[0].length > i; ++i) {
    chart.paper
      .text(chart.bars[0][i].x, chart.bars[0][i].y + chart.bars[0][i].h, chart.labels[i])
      .attr({
        'fill'       : TEXT_COLOR,
        'font-size'  : BOTTOM_LABEL_SIZE,
        'text-anchor': 'start'
      })
      .translate(0, 18)
      .rotate(BOTTOM_LABEL_ANGLE, chart.bars[0][i].x, chart.bars[0][i].y + chart.bars[0][i].h)
    ;

    // draw ticks
    chart.paper
      .path('m'
        + chart.bars[0][i].x
        + ','
        + (chart.bars[0][i].y + chart.bars[0][i].h)
        + 'l'
        + 0
        + ','
        + TICKS_LENGTH
        + 'z'
      )
      .attr({
        'stroke': '#404040',
        'stroke-width': 1
      })
      .toBack()
    ;

    // keep track of maximum height
    if (max_height > chart.bars[chart.bars.length - 1 ][i].y) {
      max_height = chart.bars[chart.bars.length - 1 ][i].y;
    }
  }

  // draw horizontal axis
  chart.paper
    .path('m'
      + chart.x
      + ','
      + (chart_bottom + MAIN_AXIS_WIDTH)
      + 'l'
      + chart.w 
      + ','
      + 0
      + 'z'
    )
    .attr({
      'stroke': MAIN_AXIS_COLOR,
      'stroke-width': MAIN_AXIS_WIDTH
    })
    .toBack()
  ;

  // draw vertical axis
  chart.paper
    .path('m'
      + chart.x
      + ','
      + CHART_TOP_PADDING
      + 'l'
      + 0
      + ','
      + (chart_bottom - CHART_TOP_PADDING + MAIN_AXIS_WIDTH)
      + 'z'
    )
    .attr({
      'stroke': MAIN_AXIS_COLOR,
      'stroke-width': MAIN_AXIS_WIDTH
    })
    .toBack()
  ;

  // draw background grid...
  // -compute units
  var vertical_unit = Math.round(chart.max_value / 10);
  if (!vertical_unit) {
    vertical_unit = chart.max_value;
  }
  var vertical_pixel_unit = (chart_bottom - max_height) / chart.max_value;

  // -draw lines
  var i = 1;
  while (i*vertical_unit < chart.max_value + vertical_unit) {
    //var current_unit = Math.round(10 * i * vertical_unit) / 10;
    var current_unit = i * vertical_unit;
    chart.paper
      .text((chart.x - 2*TICKS_LENGTH), (chart_bottom - current_unit * vertical_pixel_unit), current_unit)
      .attr({
        'fill'       : TEXT_COLOR,
        'font-size'  : LEFT_LABEL_SIZE,
        'text-anchor': 'end'
      })
    ;
    chart.paper
      .path('m'
        + (chart.x - TICKS_LENGTH)
        + ','
        + (parseInt(chart_bottom - current_unit * vertical_pixel_unit)+0.5)
        + 'l'
        + (chart.w + TICKS_LENGTH)
        + ','
        + 0
        + 'z'
      )
      .attr({
        'stroke': SECONDARY_AXIS_COLOR,
        'stroke-width': SECONDARY_AXIS_WIDTH
      })
      .toBack()
    ;
    ++i;
  }
}

/**
* DrawBarChartTitle
********************
Draws chart title.

Parameter:
-chart: the chart object;
-title: title to draw.

*/
function DrawBarChartTitle(chart, title) {
  chart.paper
    .text(chart.area.width()/2, TITLE_TOP_PADDING, title)
    .attr({
      'fill'       : TEXT_COLOR,
      'font-size'  : TITLE_LABEL_SIZE,
      'font-weight': 'bold',
      'text-anchor': 'middle'
    })
  ;
}


/**
* DrawChart
************
Draw chart diagram with bars, grid, axis and labels.

Parameters:
options: see DrawBarChartFromTable documentation.

Return:
a chart object. Use:
 alert('Available keys:' + GetKeys(chart).join(', '));
to get available keys or methods.

*/
function DrawChart(paper, chart_area, chart_width, chart_height, data, options) {

  var chart = paper.barchart(
      CHART_HORIZONTAL_PADDING,
      CHART_TOP_PADDING,
      chart_width,
      chart_height,
      data.normalized_values,
      {
        'stacked': true,
        'type'   : 'soft'
      }
    )
  ;

  // store data
  chart.area       = chart_area;
  chart.paper      = paper;
  chart.label_type = data.label_type;
  chart.labels     = data.labels;
  chart.columns    = data.columns;
  chart.values     = data.values;
  chart.max_value  = data.max_value;
  chart.x          = CHART_HORIZONTAL_PADDING;
  chart.y          = CHART_TOP_PADDING;
  chart.w          = chart_width + 10; // +10: it seems the chart is larger than specified
  chart.h          = chart_height;


  // draw grid
  DrawBarChartAxis(chart);

  var elements_to_front = [];

  // handle "hover" event on bars
  if (null != options.bar_hover) {
    // g.bar.js uses hidden rectangles in order to handle hover events over full
    // columns. These hidden elements are named "covers2" and are available
    // in chart[2] (chart[0]=="bars", chart[1]=="covers").
    // we will had a 3rd level cover over the 2nd level covers that only capture
    // event over the bars of the column.
    chart.covers3 = [];
    chart.push(chart.covers3);

    // associate "hover" event to columns
    for (var bar_series=chart.bars.length; bar_series--;) {
      var covers3 = chart.paper.set();
      $.each(chart.bars[bar_series], function (i, bar) {
        // we create a transparent <RECT> element above each bar to handle
        // hover events.
        var cover3 = bar.paper.rect(bar.x - bar.w/2, bar.y, bar.w, bar.h + 2); // 2==BAR_STORKE_WIDTH
        cover3.index = i;
        cover3.series = bar_series;
        cover3
          .attr({
            'opacity': 0, // transparent
            'fill': '#ffffff', // capture event in filled area
            'cursor': 'pointer'
          })
        ;
        cover3.series = bar_series;
        $(cover3.node).raphael = cover3;
        // create highlight glow
        cover3.highlight_glow = bar.glow(BAR_HIGHLIGHT_STYLE)
          .toFront()
          .hide()
        ;

        // check if user specified a true value instead of a hash
        if ('object' !== typeof(options.bar_hover)) {
          // turn variable into an object
          options.bar_hover = {'in': null, 'out': null};
        }

        if (options.bar_hover['init_popup'] == null) {
          // create popup flag
          var y = [];
          var column_values = [];
          // loop on series
          for (var i = chart.bars.length; i--;) {
            y.push(chart.bars[i][cover3.index].y);
            column_values.push(chart.columns[i] + ': ' + chart.values[i][cover3.index]);
          }
          cover3.popup_flag = chart.paper.popup(
              chart.bars[0][cover3.index].x,
              Math.min.apply(Math, y),
              column_values.join('\n')
            )
            .attr({'opacity': 0})
            //.insertBefore(cover3)
            .toFront()
          ;
        }
        else {
          cover3.popup_flag = options.bar_hover['init_popup'](cover3, chart);
        }
        if (cover3.popup_flag) {
          elements_to_front.push(cover3.popup_flag);
        }

        // check is functions have been specified
        // ['in'] instead of .in --> for IE compatibility
        if (options.bar_hover['in'] == null) {
          options.bar_hover['in'] = function() {
            this.popup_flag.attr({'opacity': 1});
            // show highlight glow
            this.highlight_glow.show();
          };
        }

        // ['out'] --> for IE compatibility
        if (options.bar_hover['out'] == null) {
          options.bar_hover['out'] = function() {
            if (this.popup_flag) {
              this.popup_flag.animate({'opacity': 0}, 100);
            }
            // hide highlight glow
            this.highlight_glow.hide();
          };
        }

        cover3
          .hover(options.bar_hover['in'], options.bar_hover['out'])
        ;
        
        if (options.bar_click) {
          cover3.click(options.bar_click);
        }

        covers3.push(cover3);
      });
      chart.covers3[bar_series] = covers3;
    }
  }
    // handle "hover" event on columns
  else if (options.column_tracker) {
    var ShowColumnValues = function () {
      var y = [], column_values = [];
      for (var i = this.bars.length; i--;) {
        y.push(this.bars[i].y);
        column_values.push(chart.columns[i] + ': ' + chart.values[i][this.bars[i].index]);
      }
      this.flag = paper.popup(
          this.bars[0].x,
          Math.min.apply(Math, y),
          column_values.join('\n')
        )
        .insertBefore(this)
      ;
    };
    
    var HideColumnValues = function () {
      this.flag.animate({opacity: 0}, 100, function () {this.remove();});
    };
    
    chart.hoverColumn(ShowColumnValues, HideColumnValues);
  }
  
  
  
  // customize bars
  if (options.bar_colors || options.plastic_bars) {
    for (var i = 0; chart.bars.length > i; ++i) {
      $.each(chart.bars[i], function(j, element) {
        var bar_color;
        if (options.bar_colors) {
          // get color from user function
          bar_color = options.bar_colors(j, i, chart);
        }
        else {
          // get default color
          bar_color = element.attr('fill');
        }
        
        bar_color = Raphael.rgb2hsl(Raphael.getRGB(bar_color));
    
        var normal_color = Raphael.hsl(bar_color.h, bar_color.s, bar_color.l);
        var border_color = Raphael.hsl(bar_color.h, bar_color.s, 0.4);
    
        // customize bars (add plastic effect)
        if (options.plastic_bars) {
          var lightness = bar_color.l*2;
          if (1 < lightness) {
            lightness = 1;
          }
          var light_color = Raphael.hsl(bar_color.h, bar_color.s, lightness);
          var less_light_color = Raphael.hsl(bar_color.h, bar_color.s, lightness*0.8);
          var darker_color = Raphael.hsl(bar_color.h, bar_color.s, bar_color.l*0.8);
  
          element.attr({
            'fill': '0-' + light_color + '-' + less_light_color + ':40-' + normal_color + ':50-' + normal_color + ':75-' + darker_color,
            'stroke': border_color,
            'stroke-width': BAR_LINE_WIDTH
          });
          // shadow disabled because it's too heavy (for FF)
          // element.shadow = element.glow({
          //   'width': 8,
          //   'fill': true,
          //   'color': '#404040',
          //   'opacity': 0.25,
          //   'offsetx': 2,
          //   'offsety': 4
          // });
        }
        else {
          element.attr({
            'fill': normal_color
          });
        }
        // store element index info
        element.index = j;
        // store element series info
        element.series = i;
      });
    }
  }

  // draw title
  if (options.title) {
    DrawBarChartTitle(chart, options.title);
  }

  var element_index = elements_to_front.length;
  while (element_index) {
    elements_to_front[--element_index].toFront();
  }

  chart.coversToFront = function() {
    // put all covers to front
    var cover_index = 0;
    if (chart.covers) {
      cover_index = chart.covers.length;
      while (cover_index) {
        chart.covers[--cover_index].toFront();
      }
    }
    if (chart.covers2) {
      cover_index = chart.covers2.length;
      while (cover_index) {
        chart.covers2[--cover_index].toFront();
      }
    }
    if (chart.covers3) {
      cover_index = chart.covers3.length;
      while (cover_index) {
        chart.covers3[--cover_index].toFront();
      }
    }
  };
  chart.coversToFront();

  return chart;
}


/**
* ExtractDataFromTable
***********************
Extract numeric data and labels from an HTML table.

Parameters:
-table_selector: a table jQuery selector that identify the <table> to process.

Return: an object:
 'columns'   : array of column lables; 
 'label_type': (string) type (nature) of rows labels;
 'labels'    : array of row labels;
 'max_value' : (float) maximum value found in data cells;
 'values'    : array of columns, each column being an array of row values.

*/
function ExtractDataFromTable(table_selector) {
  var data_table = $(table_selector);
  var label_type;
  var labels = [];
  var values = [];
  var columns = [];

  // iterate on rows
  data_table.find('tr').each(function(row_index, row) {
    // first row: column labels
    // iterate on columns
    $(row).find('th').each(function(column_index, cell) {
      var column_label = $(cell).text().replace(/^\s*/,'').replace(/\s*$/,'');
      // check column
      if (0 == column_index) {
        // first column: row type
        label_type = column_label;
      }
      else if (column_index) {
        // other columns: data series labels
        columns.push(column_label);
      }
    });

    // other rows: data rows
    // iterate on columns
    $(row).find('td').each(function(column_index, cell) {
      // check column (label or value?)
      if (0 == column_index) {
        // row label
        var cell_label = $(cell).text().replace(/^\s*/,'').replace(/\s*$/,'').replace(/\W/, ' ');
        labels.push(cell_label);
      }
      else {
        // column value
        var cell_value = parseFloat($(cell).text().replace(/^\s*/,'').replace(/\s*$/,''));
        if (Number.NaN == cell_value) {
          cell_value = 0;
        }
        if (!values[column_index - 1]) {
          values[column_index - 1] = [];
        }
        values[column_index - 1][row_index - 1] = cell_value;
      }
    });
  });

  // find max value
  var max_value = 0;
  for (var i = 0; values[0].length > i; ++i) {
    if (max_value < values[values.length - 1][i]) {
      max_value = values[values.length - 1][i];
    }
  }

  // return data
  return {
    'columns'   : columns,
    'label_type': label_type,
    'labels'    : labels,
    'max_value' : max_value,
    'values'    : values
  };
}

/**
* NormalizeData
****************
Translate given values into pixel values.

Parameters:
-values: array of columns containing array of values.
-max_value: data cell maximum value;
-chart_height: chart height in pixels.

*/
function NormalizeData(values, max_value, chart_height) {
  // normalize chart data (translate given units into pixel units)
  var normalized_values = [];
  for (var i = 0; values[0].length > i; ++i) {
    // keep track of cumulative value
    var stacked_value = 0;
    for (var j = 0; values.length > j; ++j) {
      if (!normalized_values[j]) {
        normalized_values[j] = [];
      }
      normalized_values[j][i] = chart_height * (values[j][i] - stacked_value) / max_value;
      stacked_value += values[j][i];
    }
  }
  return normalized_values;
}


/**
* DrawBarChartFromTable
************************
Replaces a given HTML table with an SVG chart diagram in the given area.

Parameters:
-chart_area_selector: jQuery selector that identify the element to draw in. The
  size of that element should be set.
-table_selector: jQuery selector that identify the HTML table element.
-options: an option object:
  title: (string) chart title.
  column_tracker: (boolean) display column values when mouse moves over (not compatible
    with bar_hover);
  bar_hover: (hash) if set, hover events over chart bar are triggered using the
    given functions: 'in' when the mouse enters and 'out' when the mouse leaves.
  bar_colors: (function(row_index, column_index, chart)) a function that returns
    a string color value that can be parsed by Raphael.
  bar_click: (function()) function to call when a click event on a bar occurs.
    The function caller (this) will be the bar object.

Return:
a chart object.

*/
function DrawBarChartFromTable(chart_area_selector, table_selector, options) {
  var chart_area = $(chart_area_selector);
  var chart;

  if (!options) {
    options = {};
  }

  // make sure we got a table
  if ($(table_selector).exists() && chart_area.exists()) {
    try
    {
      // get data...
      var data = ExtractDataFromTable(table_selector);
      // create Raphael paper object for drawing and link it to the given canvas
      var paper = Raphael(chart_area.attr('id'), chart_area.width(), chart_area.height());
      chart_area
        .css({
          'background-color': '#f0f0f0',
          'border': '1px solid #000000'
        })
      ;


      var chart_width  = chart_area.width()  - 2*CHART_HORIZONTAL_PADDING;
      var chart_height = chart_area.height() - CHART_TOP_PADDING - CHART_BOTTOM_PADDING;

      var normalized_values = NormalizeData(data.values, data.max_value, chart_height);

      data.normalized_values = normalized_values;
      chart = DrawChart(paper, chart_area, chart_width, chart_height, data, options);
    }
    catch(error) {
      // an error occurred
      alert('Warning: an error occurred while drawing chart diagram (' + error + ').\n');
      if (DEBUG) {
        throw error;
      }
    }
  }
  else {
    alert('Warning: table "' + selector + '" not found on current page! Unable to display bar chart!');
  }
  
  // return chart object if one
  return chart;
}
