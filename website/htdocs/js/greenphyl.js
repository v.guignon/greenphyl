/**
********************************************************************************
********************************
* GreenPhyl Javascript Library *
********************************
Version:  3.1.0
Contacts: Mathieu Rouard <m.rouard@cgiar.org>
          Valentin Guignon <v.guignon@cgiar.org>
Requires: jQuery >=1.9, jQueryUI >=1.10

Description:
This file contains the main Javascript code used on GreenPhyl website.

Note: the following global "constants" are defined in the HTML page header:
    var BASE_URL
    var IMG_URL
    var TREE_URL
    var APPLETS_URL
    var ATVE_APPLET

Copyright 2011, Bioversity International - CIRAD

********************************************************************************
*/


/**
********************************************************************************
* GLOBAL CONSTANTS
********************************************************************************
*/
var AUTOCOMPLETE_DEFAULT_WIDTH = '320px';
var PAGE_WIDTH                 = 1024; // note: dynamically updated when the page is loaded
var ATVE_APPLET_HEIGHT         = '700';
var MIN_PAGED_TABLE_ROWS       = 10;
var EDITABLE_FIELDS_TEXT       = 'Click to edit';
var DEFAULT_TITLE              = 'GreenPhyl';


/**
********************************************************************************
* GLOBAL VARIABLES
********************************************************************************
*/
// tooltips global variable
var g_tooltip_box;
// My List modal dialog box
var g_my_list_box;
// Help dialog box
var g_help_box;
//color picker selected color
var g_cp_color = '#ffffff'; // in CSS format
// Flash chart
var g_chart_json_array = new Array();
// stack of init functions to call from other scripts
var g_init_functions = new Array();


/**
********************************************************************************
* FUNCTIONS
********************************************************************************
*/


/**
*********************
* JQUERY EXTENSIONS *
*********************
*/
// tells if an object really exists
jQuery.fn.exists = function() {return this.length>0;};

// extract and return the value following a given class name
jQuery.fn.getClassValue = function(class_prefix) {
  var value = null;
  jQuery.each(this.get(0).className.split(/\s+/), function(i, class_name) {
    if (0 == class_name.indexOf(class_prefix)) {
      // extract the value from the class name
      value = class_name.substr(class_prefix.length);
      // stop loop
      return;
    }
  });
  // return extracted value or null
  return value;
};

// stolen from Anurag post on Stack Overflow
// binds an event before previous handlers
jQuery.fn.bindFirst = function(name, fn) {
    // bind as you normally would
    // don't want to miss out on any jQuery magic
    this.bind(name, fn);

    // Thanks to a comment by @Martin Drapeau, adding support for
    // namespaced events too.
    var handlers = this.data('events')[name.split('.')[0]];
    // take out the handler we just inserted from the end
    var handler = handlers.pop();
    // move it at the beginning
    handlers.splice(0, 0, handler);

    return this;
};

/** Other extensions */
// trim (older IE versions don't support .trim())
if (!String.prototype.trim) {
  String.prototype.trim = function() {return this.replace(/^\s\s*/, '').replace(/\s\s*$/, '');};
}

// get object keys
function GetKeys(object) {
  var keys = [];
  for (var key in object) {
    if (object.hasOwnProperty(key)) {
       keys.push(key);
    }
  }
  return keys;
}



/**
***************
* TABULATIONS *
***************
*/
/**
* initTabs
***********
Initializes tabulation system.

*/
function initTabs() {
  var active_tab = jQuery(".tabs-container").not('.tabs-processed').addClass('tabs-processed');

  if (active_tab.exists()) {
    active_tab
      .tabs({ 'show': function(event, ui) { initGreenPhylPage(); } }) // reinit for previously hidden elements
      .find(".ui-tabs-nav")
        .each(function(index) {
          jQuery(this).sortable();
        })
    ;

    /* set URL to match selected tab */
    active_tab.find('a.ui-tabs-anchor').click(function () {
      if (history.pushState) {
        history.pushState(null, null, jQuery(this).attr('href'));
      }
      else {
        location.hash = jQuery(this).attr('href');
      }
    });
  }
}


/**
************
* TOOLTIPS *
************

To create an element with tooltip:
1) Create the tooltip with an ID ending with "_tooltip" and the class "hidden-tooltip";
Example:
<div id="test_tooltip" class="hidden-tooltip">The tooltip <b>content</b> to display...</div>

2) create the element that will have the tooltip, give it the class "tooltiped"
and link it to the tooltip using either the ID of the tooltip without the
ending "_tooltip" or add the class "with_tooltip_" + the tooltip ID without the
ending "_tooltip".
Example 1:
<span class="tooltiped" id="test">HERE</span>

Example 2:
<span class="tooltiped with_tooltip_test" >THERE</span>

*/
/**
* showTooltip
**************
Displays tooltip box with the associated text when the pointer moves over an
element with a tooltip set.

*/
function showTooltip(event) {
  var target = event.delegateTarget || event.target;
  if ('object' == typeof(target)) {
    target = jQuery(target).get(0);
  }
  if (!target) {
    target = this;
  }
  
  // get associated tooltip using element ID
  var tooltip_html = jQuery('#' + target.id + '_tooltip').html();

  // if not found, try with name
  // note: we use jQuery(this).prop('name') for SVG elements
  var target_name = target.name || jQuery(this).prop('name');
  if ((!tooltip_html || (0 == tooltip_html.length)) && target_name) {
    tooltip_html = jQuery('#' + target_name + '_tooltip').html();
  }

  // if not found, try with class
  if ((!tooltip_html || (0 == tooltip_html.length)) && target.className) {
    tooltip_html = '';
    var class_list = '' + target.className; // force string because SVG elements return an object and not a string
    // loop on each class and see if it's a tooltip class
    jQuery.each(class_list.split(/\s+/), function(i, class_name) {
      // class name must start with "with_tooltip_"
      if (0 == class_name.indexOf('with_tooltip_')) {
        tooltip_html = jQuery('#' + class_name.substr(13) + '_tooltip').html(); // 13 = length('with_tooltip_')
        // stop loop
        return false;
      }
    });
  }

  if (!tooltip_html || (0 == tooltip_html.length)) {
    return;
  }

  bringToFrontTooltip();
  g_tooltip_box.html(tooltip_html).show();
}

/**
* hideTooltip
**************
Hides the tooltip box when the pointer leaves the element with a tooltip set.

*/
function hideTooltip(event) {
  g_tooltip_box.hide().text('');
}

/**
* moveTooltip
**************
Make the tooltip box follow the mouse pointer.

*/
function moveTooltip(event) {
  g_tooltip_box.offset({ top: event.pageY + 2, left: event.pageX + 16});
}

/**
* bringToFrontTooltip
**********************
Make the tooltip box above last created elements.

*/
function bringToFrontTooltip() {
  jQuery('#tooltip_box').remove(); // remove previous box
  g_tooltip_box = jQuery('<div id="tooltip_box" style="z-index: 100000;"></div>').appendTo(jQuery('body')); // create a new one
}

/**
* initTooltips
***************
Initializes tooltips system.

*/
function initTooltips() {
  // Init Bootstrap popups first.
  jQuery('[data-toggle="tooltip"]')
    .not('.tooltip_processed')
    .addClass('tooltip_processed')
    .tooltip();
  
  // Init old style popups.
  if (!jQuery('#tooltip_box').exists()) {
    // create tooltip box
    g_tooltip_box = jQuery('<div id="tooltip_box"></div>').appendTo(jQuery('body'));
    // set the handler to make the popup follow the mouse pointer
    jQuery(document).on('mousemove', moveTooltip);
  }
  // init event handlers for elemens with tooltips
  jQuery('.tooltiped')
    .not('.tooltip_processed')
    .addClass('tooltip_processed')
    .on('mouseover', showTooltip)
    .on('mouseout', hideTooltip)
  ;
  //+Val disabled because we always call bringToFrontTooltip when displaying a tooltip
  // // make sure we got tooltips on top of everything that will be generated
  // // once the document is loaded by adding the call to bringToFrontTooltip after
  // // the others.
  // jQuery(bringToFrontTooltip);
}



/**
*****************
* TOGGLE BLOCKS *
*****************
*/
/**
* toggleBlock
**************
Toggle (hide/show) a div block.

*/
function toggleBlock(id) {

  // toggle display status and change style
  if (jQuery('#' + id).toggle().is(':visible')) {
    jQuery('#toggler_icon_' + id)
      .prop('src', IMG_URL + '/minus.png')
      .prop('alt', 'Close')
      .prop('title', 'Close')
    ;
    jQuery('#toggler_label_hide_' + id)
      .show()
    ;
    jQuery('#toggler_label_show_' + id)
      .hide()
    ;
    jQuery('#toggle_container_' + id)
      .removeClass('toggle-container-closed')
      .addClass('toggle-container-opened')
    ;
  }
  else {
    jQuery('#toggler_icon_' + id)
      .prop('src', IMG_URL + '/plus.png')
      .prop('alt', 'Open')
      .prop('title', 'Open')
    ;
    jQuery('#toggler_label_hide_' + id)
      .hide()
    ;
    jQuery('#toggler_label_show_' + id)
      .show()
    ;
    jQuery('#toggle_container_' + id)
      .removeClass('toggle-container-opened')
      .addClass('toggle-container-closed')
    ;
  };  
}

/**
********************
* HIGHLIGHT GROUPS *
********************
*/
/**
* highlightGroup
*****************
Highlight a group of elements.

*/
function highlightGroup(event) {
  // find element highligh class
  jQuery.each(event.target.className.split(/\s+/), function(i, class_name) {
    // class name must start with "hl-"
    if (0 == class_name.indexOf('hl-')) {
      jQuery('.' + class_name).addClass('highlight-group-on');
      // stop loop
      return false;
    }
  });
}

/**
* clearHighlightGroup
**********************
Remove a group of elements highlight.

*/
function clearHighlightGroup(event) {
  // find element highligh class
  jQuery.each(event.target.className.split(/\s+/), function(i, class_name) {
    // class name must start with "hl-"
    if (0 == class_name.indexOf('hl-')) {
      jQuery('.' + class_name).removeClass('highlight-group-on');
      // stop loop
      return false;
    }
  });
}

/**
* initHighlightGroups
**********************
Initializes groups of elements that should be highlighted together.

*/
function initHighlightGroups() {
  jQuery('.highlight-group')
    .not('.highligh-processed')
    .addClass('highligh-processed')
    .on('mouseover', highlightGroup)
    .on('mouseout', clearHighlightGroup)
  ;
}




/**
***********
* MY LIST *
***********
*/
/**
* initMyList
*************
Initializes My List modal dialog box.

*/
function initMyList() {
  jQuery('.mylist-show-link, .mylist-add-link')
    .not('.popup-link')
    .addClass('popup-link')
    .addClass('popup-id-mylist')
    .addClass('popup-title-My_List')
  ;

  /* multi-entries */
  jQuery('.mylist-multi')
    .not('.mylist-multi-processed')
    .addClass('mylist-multi-processed')
    .not('.popup-link')
    .addClass('popup-link')
    .addClass('popup-id-mylist')
    .addClass('popup-title-My_List')
    .addClass('popup-use-form')
  ;
}




/**
**************
* HELP POPUP *
**************
*/

/**
* initHelp
***********
Initializes Help popup dialog box.

*/
function initHelp() {
  jQuery('a.help-link')
    .not('.popup-link')
    .addClass('popup-link')
    .addClass('popup-id-help_popup')
    .addClass('popup-title-Greenphyl_Help')
  ;
}


/**
****************
* QUICK SEARCH *
****************
*/
/**
* initQuickSearch
*******************
Initializes Quick Search  field in header to use AJAX.

*/
function initQuickSearch() {
  // find and initialize search box (in page header)
  jQuery('#qsearch form')
    .not('.qsearch-processed')
    .addClass('qsearch-processed')
    .on('submit', function() {
        var qsearch_input = jQuery('#qsearch_head_input').val();
        var qsearch_type  = jQuery('#qsearch_head_type').val();
        var deep_search = jQuery('#qdeep_head_search').val();
        var mmode_param = '';
        var master_mode = GetURLParameter('mmode');
        if (master_mode) {
          mmode_param = '&mmode=' + master_mode;
        }
        window.location.replace(BASE_URL + '/quick_search.cgi?using_ajax=1&qsearch_input=' + encodeURI(qsearch_input) + '&qsearch_type=' + encodeURI(qsearch_type) + '&qdeep_search=' + encodeURI(deep_search) + mmode_param);
        return false;
    })
  ;
}


/**
****************
* AUTOCOMPLETE *
****************
*/
/**
* initAutocomplete
*******************
Initializes autocompletion system.

*/
function initAutocomplete() {

  // find and initialize search box (in page header)
  jQuery('#qsearch_head_input')
    .not('.autocomplete-processed')
    .addClass('autocomplete-processed')
    .autocomplete(
      {
        source: BASE_URL + '/autocomplete.pl',
        minLength: 2,
        width: AUTOCOMPLETE_DEFAULT_WIDTH,
        selectFirst: false,
        dataType: 'json',
        parse: function(data) {
          if (!data || ('an error occured' == data[0])) { // nb.: check error message given by autocomplete.pl
              return;
          }
          return jQuery.map(
            data,
            function(row) {
              return {
                data: row,
                value: row.label,
                result: row.value
              };
            }
          );
        },
        formatItem: function(item) {
          return item.label;
        },
        extraParams: {
          search_type: 'any'
        }
      }
    )
  ;

  // IPR
  jQuery('.autocomplete-ipr')
    .not('.autocomplete-processed')
    .addClass('autocomplete-processed')
    .autocomplete(
      {
        source: BASE_URL + '/autocomplete.pl',
        minLength: 2,
        width: AUTOCOMPLETE_DEFAULT_WIDTH,
        selectFirst: false,
        dataType: 'json',
        parse: function(data) {
          return jQuery.map(
            data,
            function(row) {
              return {
                data: row,
                value: row.label,
                result: row.value
              };
            }
          );
        },
        formatItem: function(item) {
          return item.label;
        },
        extraParams: {
          search_type: 'interpro_seq'
        }
      }
    )
  ;

  // Species codes
  jQuery('.autocomplete-species-code')
    .not('.autocomplete-processed')
    .addClass('autocomplete-processed')
    .autocomplete(
      {
        source: BASE_URL + '/species.pl?service=complete',
        minLength: 2,
        width: AUTOCOMPLETE_DEFAULT_WIDTH,
        selectFirst: false,
        dataType: 'json',
        parse: function(data) {
          return jQuery.map(
            data,
            function(row) {
              return {
                data: row,
                value: row.label,
                result: row.value
              };
            }
          );
        },
        formatItem: function(item) {
          return item.label;
        },
      }
    )
  ;

  jQuery('.autocomplete-species-name')
    .not('.autocomplete-processed')
    .addClass('autocomplete-processed')
    .autocomplete(
      {
        source: BASE_URL + '/species.pl?service=complete_name&gp_species=1',
        minLength: 2,
        width: AUTOCOMPLETE_DEFAULT_WIDTH,
        selectFirst: false,
        dataType: 'json',
        parse: function(data) {
          return jQuery.map(
            data,
            function(row) {
              return {
                data: row,
                value: row.label,
                result: row.value
              };
            }
          );
        },
        formatItem: function(item) {
          return item.label;
        },
      }
    )
  ;

  jQuery('.autocomplete-species-name-with-taxonomy')
    .not('.autocomplete-processed')
    .addClass('autocomplete-processed')
    .autocomplete(
      {
        source: BASE_URL + '/species.pl?service=complete_name&gp_species=1&with_taxonomy=1',
        minLength: 2,
        width: AUTOCOMPLETE_DEFAULT_WIDTH,
        selectFirst: false,
        dataType: 'json',
        parse: function(data) {
          return jQuery.map(
            data,
            function(row) {
              return {
                data: row,
                value: row.label,
                result: row.value
              };
            }
          );
        },
        formatItem: function(item) {
          return item.label;
        },
      }
    )
  ;
}


/**
**********
* TABLES *
**********
*/
/**
 *
 */
function sortHyperlinkLabels(a, b) {
    var aa = $(a).text();
    var bb = $(b).text();
    return aa.localeCompare(bb);
}


/**
* stripeTables
***************
Stripe the rows of any table in the page using the class 'striped'. Also add
the row highlight effect.
CSS used: tr.even, tr.odd, tr.over

*/
function stripeTables() {
  // for tables using class 'striped', 'sortable'
  jQuery('.striped tbody tr:odd')
    .addClass('odd')
  ;
  jQuery('.striped tbody tr:even')
    .addClass('even')
  ;
/*
  jQuery('.striped tr').on('mouseover', function() {
    jQuery(this).addClass('over');
  });
  jQuery('.striped tr').on('mouseout', function() {
    jQuery(this).removeClass('over');
  });
*/
}

/**
* Sortable Pager
*****************
*/
function addPager(paged_table, pager_id) {
  jQuery(paged_table)
    .wrap('<div class="pager-wrapper"></div>')
    .after(jQuery('    <div id="' + pager_id + '" class="pager">\
        <form>\
          <img src="' + IMG_URL + '/first.png" alt="&lt;&lt;" class="first" alt="First" title="First"/>\
          <img src="' + IMG_URL + '/previous.png" alt="&lt;" class="prev" alt="Previous" title="Previous"/>\
          <span style="padding-left: 2ex;"></span>\
          Row(s) <span class="pagedisplay"></span>\
          <span style="padding-left: 2ex;"></span>\
          <img src="' + IMG_URL + '/next.png" alt="&gt;" class="next" alt="Next" title="Next"/>\
          <img src="' + IMG_URL + '/last.png"  alt="&gt;&gt;" class="last" alt="Last" title="Last"/>\
          <span style="padding-left: 6ex;"></span>\
          <label>Rows per page\
            <select class="pagesize">\
              <option value="' + MIN_PAGED_TABLE_ROWS + '" selected="selected">' + MIN_PAGED_TABLE_ROWS + '</option>\
              <option value="25">25</option>\
              <option value="50">50</option>\
              <option value="100">100</option>\
              <option value="150">150</option>\
              <option value="200">200</option>\
              <option value="1000000000">All</option>\
            </select>\
          </label>\
        </form>\
      </div>')
    )
  ;
  jQuery(paged_table).tablesorterPager({ container: jQuery('#' + pager_id), positionFixed: false });
  jQuery('#' + pager_id).css('width', jQuery(paged_table).css('width'));

  // check if tables contain input elements inside a form
  // and should be expanded on form submit
  var parent_form = jQuery(paged_table).parents('form').first();
  if (jQuery(paged_table).find('input').exists()
      && parent_form.exists()) {
    parent_form.on('submit', function() {
        // make the pagger display all the table pages to enable all form items
        jQuery('#' + pager_id + ' .pagesize').val(1000000000).change();
    });
  }
}

/**
* addColumnToggler
*******************

*/
function addColumnToggler() {
  // for tables using class 'column-toggle'
  jQuery('.column-toggle')
    .removeClass('column-toggle')
    .addClass('column-toggle-processed')
    .find('thead th') // process headers
    .each(function(col_index) {
      // skip headers that should not be toggleable
      if (jQuery(this).is('.no-toggler')) {
        return;
      }
      // create a column toggler element
      var toggler = jQuery('<div class="column-toggler">-</div>');
      // get column cells references
      var column_elements = [];
      jQuery(this).parents('table').first().find('tr').each(function(row_index) {
        jQuery(this).find('th, td').each(function(cell_index) {
          if (cell_index == col_index) {
            column_elements.push(this);
            // save elements original font size
            jQuery(this).find('*').each(function(child_index) {
              jQuery(this).data('original_font_size', jQuery(this).css('font-size'));
            });
          }
        });
      });

      // add toggler element to the column header cell
      jQuery(this)
        .css('vertical-align', 'top')
        .prepend(toggler)
      ;

      // store column cell references and add event handler
      toggler
        .data('column_elements', column_elements)
        .data('original_font_size', toggler.css('font-size'))
        .data('expanded', true)
        .on('click', function(event) {
          // check state
          if (!jQuery(this).data('expanded')) {
            // expand
            jQuery.each(jQuery(this).data('column_elements'), function(index, element) {
              // jQuery(element).css('font-size', jQuery(element).data('original_font_size'));
              jQuery(element)
                .animate({'font-size': jQuery(this).data('original_font_size')}, 250)
                .find('*')
                .each(function() {
                  jQuery(this).animate({'font-size': jQuery(this).data('original_font_size')}, 250);
                })
              ;
            });
            // update the toggler
            jQuery(this)
              .stop(true)
              .data('expanded', true)
              .text('-');
          }
          else {
            // collapse
            jQuery.each(jQuery(this).data('column_elements'), function(index, element) {
              // jQuery(element).css('font-size', 0);
              jQuery(element)
                .animate({'font-size': 0}, 250)
                .find('*')
                .animate({'font-size': 0}, 250)
              ;
            });
            // update the toggler
            jQuery(this)
              .stop(true)
              .data('expanded', false)
              .text('+')
              .css('font-size', jQuery(this).data('original_font_size'))
            ;
          }
      });
    })
  ;
}




/***
**********************
* CENTERING ELEMENTS *
**********************
*/
/**
* initCenteringElements
************************
This function create the appropriate structure in order to center blocks.
Blocks content can have a different alignment (ie. not centered).

Starting content:
<div class="center-block">Any kind of content</div>

Resulting content:
<div class="centered-outer">
  <div class="centered-inner">
    Any kind of content
  </div>
</div>
<br class="clear"/>

*/
function initCenteringElements() {

  // prevent scripts from being executed twice
  jQuery('.center-block').find('script').remove();
  
  jQuery('.center-block')
    .removeClass('center-block')
    .addClass('centered-inner')
//    .after('<br class="clear"/>') // removed for IE7 compatibility
    .wrap('<div class="new-centered-outer"></div>')
  ;

  // append a br-clear if needed
  jQuery('.new-centered-outer')
    .each(function(index, element) {
      if (!jQuery(element).next().is('br.clear')) {
        jQuery(element).after('<br class="clear"/>');
      }
    })
  ;

  // added for IE7 compatibility
  jQuery('.new-centered-outer')
    .removeClass('new-center-outer')
    .addClass('centered-outer')
    .parent()
      .css('overflow', 'hidden') // hide overflow to prevent horizontal scrollbar bug
  ;
}


/**
*******************
* EDITABLE FIELDS *
*******************
*/
/**
* InitEditableFields
*********************
Initializes editable fields.

Example of minimalist editable field:
<div>
  <input type="text" value="your text" id="[FIELD_ID]" />
</div>

Note: input element must have a parent and no (preceeding) sibblings.

*/
function InitEditableFields() {
  // prepare elements for the jQuery editable plugin
  jQuery('.editable-field')
    .not('.editable-processed')
    .addClass('editable-processed')
    .each(function(index, element) {
      var label_text;
      if (!jQuery(element).val()) {
        label_text = EDITABLE_FIELDS_TEXT;
      }
      else if (jQuery(element).val() instanceof Array) {
        label_text = jQuery(element).val().join(', ').trim() || EDITABLE_FIELDS_TEXT;
      }
      else {
        label_text = jQuery(element).val().trim() || EDITABLE_FIELDS_TEXT;
      }
      jQuery(element)
        .parent()
          .prepend(jQuery('<div id="' + element.id + '_label" class="editable" data-type="editable" data-for="#' + element.id + '">' + label_text + '</div>'))
      ;
    })
  ;
  // jQuery('body')
  //   .editables({
  //     beforeEdit: function(field) {
  //       if (this.text().trim() != EDITABLE_FIELDS_TEXT) {
  //         if (field.is('select')) {
  //           var selected_values = this.text().trim().split(/\s*,\s*/);
  //           field.val(selected_values);
  //         }
  //         else {
  //           field.val(this.text().trim());
  //         }
  //       }
  //       this.parents('form').find('input[type=submit]').prop('disabled', true);
  //       return true;
  //     },
  //     beforeFreeze: function(field) {
  //       if (!jQuery(this).val()) {
  //         new_text = EDITABLE_FIELDS_TEXT;
  //       }
  //       else if (jQuery(this).val() instanceof Array) {
  //         new_text = jQuery(this).val().join(', ').trim() || EDITABLE_FIELDS_TEXT;
  //       }
  //       else {
  //         new_text = jQuery(this).val().trim() || EDITABLE_FIELDS_TEXT;
  //       }
  //       if (field.text().trim() != new_text) {
  //         // if updated, highlight submit button
  //         HighlightSaveButton(this);
  //         field
  //           .addClass('modified')
  //           .text(new_text)
  //         ;
  //       }
  //       this.parents('form').find('input[type=submit]').removeAttr('disabled');
  //       return true;
  //     }
  // });
  // set as processed to not process the elements again
  jQuery('[data-type=editable]').each(function(index, element) {
    jQuery(element).prop('data-type', 'editable-processed');
  });
};

function HighlightSaveButton(element) {
  jQuery(element).parents('form').find('input[type=submit]')
    .not('.need-save')
    .addClass('need-save')
  ;
}

function UnhighlightSaveButton(element) {
  jQuery(element).parents('form').find('input[type=submit]')
    .removeClass('need-save')
  ;
}

function UpdateEditableFieldLabels(notify_changes) {
  jQuery('.editable-field')
    .each(function(index, element) {
      var label_text;
      if (!jQuery(element).val()) {
        label_text = EDITABLE_FIELDS_TEXT;
      }
      else if (jQuery(element).val() instanceof Array) {
        label_text = jQuery(element).val().join('; ').trim() || EDITABLE_FIELDS_TEXT;
      }
      else {
        label_text = jQuery(element).val().trim() || EDITABLE_FIELDS_TEXT;
      }

      var label = jQuery(element).parent().find(jQuery('#' + element.id + '_label'));
      if (label.exists()) {
        if (label.text().trim() != label_text) {
          // if updated, highlight submit button
          HighlightSaveButton(element);
          label.addClass('modified');
        }
        else {
          label.removeClass('modified');
        }
      }
      label.text(label_text);
    });
}


/**
**********************
* ANNOTATION HELPERS *
**********************
*/
/**
* TransferAnnotation
*********************
Transfer the content of parent of current element to a given annotation field.

*/
function TransferAnnotation(event) {
  // get clicked target element
  var target = event.delegateTarget || event.target;
  if ('object' == typeof(target)) {
    target = jQuery(target).get(0);
  }
  if (!target) {
    target = this;
  }

  // get link text to remove it later from its parent text
  var link_text = jQuery(target).text();
  // prepare regexp to remove link text
  var pattern = new RegExp('\s*' + link_text + '$'); // used to remove text from link
  var scroll_to_element;
  // make sure we got a valid parent
  if (jQuery(target).parent().is('.for-annotation')) {
    // get annotation target name
    var field_name = jQuery(target).parent().getClassValue('annotate-to-');
    if (field_name) {
      // replace annotation text
      jQuery('#' + field_name).val(jQuery(target).parent().text().replace(pattern, ''));
      // set modified element for later scrolling
      scroll_to_element = jQuery('#' + field_name).first();
    }
    field_name = jQuery(target).parent().getClassValue('annotate-add-to-');
    if (field_name) {
      // append annotation text
      var current_val = jQuery('#' + field_name).val();
      if (current_val) {
        current_val += '; ';
      }
      jQuery('#' + field_name).val(current_val + jQuery(target).parent().text().replace(pattern, ''));
      // set modified element for later scrolling
      scroll_to_element = jQuery('#' + field_name).first();
    }

    // in case of editable fields, update labels
    UpdateEditableFieldLabels(true);

    // move screen to the modified element to show the change to the user
    if (scroll_to_element) {
      // make sure we got a visible element (editable fields are hidden)
      while (scroll_to_element && !scroll_to_element.is(':visible')) {
        scroll_to_element = scroll_to_element.parent();
      }
      jQuery.scrollTo(scroll_to_element, {'duration': 500});
    }
  }
}

/**
* InitAnnotationHelpers
************************
Initializes annotation helpers.

Example of minimalist editable field:
<input type="text" id="family_name" name="family_name" value="" />
<span class="for-annotation annotation-label-Transfer_to_name annotate-to-family_name">
  This text will be send to family_name field.
</span>

Note: input element must have a parent and no (preceeding) sibblings.

*/
function InitAnnotationHelpers() {
  jQuery('.for-annotation').not('.for-annotation-processed').each(function(index, element) {
    var label = jQuery(element).getClassValue('annotation-label-');
    if (label) {
      label = label.replace(/_/g, ' ');
    }
    else {
      label = 'To annotation';
    }
    jQuery(element)
      .addClass('for-annotation-processed')
      .append(' <a class="to-annotation" href="#" onclick="TransferAnnotation(event); return false;">' + label + '</a>')
    ;
  });
}



/**
* InitBulkOperations
*********************
Initializes bulk operation links.

Bulk operations are link that will submit the form they are in using the link
URL instead of the form initial URL.

*/
function InitBulkOperations() {
  jQuery('a.bulk-operation')
    .not('.bulk-operation-processed')
    .addClass('bulk-operation-processed')
    .on('click', function (event) {
      jQuery(event.target).parents('form').first()
        .prop('action', jQuery(event.target).prop('href'))
        .submit()
      ;
      jQuery(event.target).prop('href', '#');
      event.preventDefault();
      return false;
    })
  ;
}



/***
******************
* GREENPHYL INIT *
******************
*/
/**
* initGreenPhylPage
********************
Function called when the page is loaded in order to initialize various parts of
the site. This function may be called several time in on same page (after ajax
loading for instance).

*/
function initGreenPhylPage() {
  PAGE_WIDTH = jQuery('#page').width();

  // Order of calls can be important as some elements should be initialized in
  // a certain order.

  // centered elements
  initCenteringElements();
  // highlight groups
  initHighlightGroups();
  // default input/textarea text
  initInputDefaultText();
  // init master checkboxes
  initCheckAll();
  // tooltips
  initTooltips();
  // Help popups
  initHelp(); // must come before initPopups()
  // My List
  initMyList(); // must come before initPopups()
  // Popups
  initPopups();
  // Editable fields
  // InitEditableFields();
  // Quick search
  initQuickSearch();
  // autocomplete
  initAutocomplete();
  // annotation
  InitAnnotationHelpers();
  // Bulk operations
  InitBulkOperations();
  // click confirmations
  InitConfirmations();
  
  // // sortable tables
  // jQuery('table.sortable')
  //   .not('.sortable-processed')
  //   .addClass('sortable-processed')
  //   .each(function(i, sortable_table) {
  // 
  //     //+FIXME: maybe disable sortable and/or pager for hudge tables? To try...
  // 
  //     // make sure table has more than 1 row!
  //     if (1 < jQuery(sortable_table).children('tbody').children('tr').length) {
  //       // list columns that shouldn't be sorted
  //       var unsortable_columns = {}; // hash that will contain index of columns that shouldn't be sortable
  //       // process each header cell
  //       // note: rowspan and colspan don't need to be taken in account as tablesorter just use the order the 'th' cells appear on
  //       jQuery(sortable_table)
  //         .children('thead')
  //         .children('tr')
  //         .children('th')
  //         .each(function(index, cell) {
  //           if (jQuery(cell).is('.not-sortable')) {
  //             unsortable_columns[index] = {sorter: false};
  //           }
  //         })
  //       ;
  // 
  //       // check if a text extraction function was set
  //       // nb: tablesorter built-in sort function can be used using the class
  //       // attribute on the column; for instance: class="sorter-text"
  //       // available: sorter-text, sorter-digit, sorter-currency,
  //       //     sorter-ipAddress, sorter-url, sorter-isoDate, sorter-percent,
  //       //     sorter-usLongDate, sorter-shortDate, sorter-time, sorter-metadata
  //       var extracter_function = function(node) { return jQuery(node).text().trim().replace(/[\s\n\r]+/g, ' '); };
  // 
  //       jQuery.each(sortable_table.className.split(/\s+/), function(i, class_name) {
  //         // class name must start with "extracter-"
  //         if (0 == class_name.indexOf('extracter-')) {
  //           // extract the name of the function from the class name
  //           // and get a pointer on the function from the window global object
  //           var new_extracter_function = window[class_name.substr(10)]; // 10 = length('extracter-')
  //           if (new_extracter_function) {
  //               extracter_function = new_extracter_function;
  //           }
  //           else {
  //               throw('Function "' + class_name.substr(10) + '" not found for tablesorter. Check the function name you specified using the class "extracter-..." in one of your table.');
  //           }
  //           // stop loop
  //           return false;
  //         }
  //       });
  // 
  //       // make table sortable
  //       jQuery(sortable_table)
  //         .tablesorter({
  //           widthFixed: true,
  //           // first column can be not sortable (for checkboxes for instance)
  //           headers: unsortable_columns,
  //           // striped? use zebra widget
  //           widgets: (jQuery(sortable_table).hasClass('striped')? ['zebra'] : []),
  //           // text extraction function
  //           textExtraction: extracter_function
  //           //, textSorter: function() {}
  //         })
  //         .removeClass('striped') // remove striped class if present as it's handled by zebra widget
  //       ;
  //     }
  //   })
  //   .not('.no-note')
  //   .after('<div class="centered note">Note: sort multiple columns simultaneously by holding down the shift key and clicking other column headers.</div>')
  // ;
  // // paged tables
  // jQuery.each(jQuery('table.paged').not('.paged-processed'),
  //   function(i, paged_table) {
  //     if (jQuery(paged_table).children('tbody').children('tr').length > MIN_PAGED_TABLE_ROWS) {
  //       addPager(paged_table, 'pager_' + paged_table.id);
  //     }
  //     jQuery(paged_table).addClass('paged-processed');
  //   })
  // ;
  // // scrollable tables
  // jQuery.each(jQuery('table.scrollable').not('.scrollable-processed'),
  //   function(i, scrollable_table) {
  //     // get scroll height
  //     var scroll_height = parseInt(jQuery(scrollable_table).getClassValue('scroll-height-'));
  //     if (scroll_height) { // && (jQuery(scrollable_table).height() > scroll_height)) {
  //       jQuery(scrollable_table)
  //         .wrap('<div class="table-scroller" style="height: ' + scroll_height + 'px;"></div>');
  //     }
  //     jQuery(scrollable_table)
  //       .removeClass('scrollable')
  //       .addClass('scrollable-processed');
  //   })
  // ;
  // // synchronize sortable and scrollable
  // jQuery('.sfhtHeader thead th')
  //   .each(function(index) {
  //     var $cloneTH = jQuery(this);
  //     var $trueTH = jQuery(jQuery('.sfhtData thead th')[index]);
  //     $cloneTH.prop('class', $trueTH.prop('class'));
  //     $cloneTH
  //       .click(function() {
  //         $trueTH.click();
  //       })
  //     ;
  //   })
  // ;
  // stripe table rows (must be called after sortable pages init)
  stripeTables();
  
  // add column toggler
  addColumnToggler();

  // hide elements to hide
  jQuery('.start-hidden').hide().removeClass('start-hidden');

  // Init table search.
  $(function () {
    $('.bootstrap-table').find('.fixed-table-toolbar .search-input').attr('placeholder', 'Filter on page');
  });

  // init tabs
  initTabs();

  // init other elements
  jQuery.each(g_init_functions, function(i, init_function) {
    init_function();
  });

  // init forms
  jQuery('form')
    .not('.form-processed')
    .addClass('form-processed')
    .each(function() {
      // add validation function to each form
      jQuery(this)
        .on('submit', validate)
        // however, due to a bug, "on submit" event won't be catched if a submit
        // button exists! The workarround is to bind the event to the button itself
        .find('input[type=submit]')
          .on('click', validate)
      ;

    })
  ;
  
  // Forces Firefox to scroll to anchor when one. (FF bug)
  // Source: https://support.mozilla.org/en-US/questions/1267968
  function pgshow(e) {
    var elId = window.location.hash;
    if (elId.length > 1) {
      el = document.getElementById(elId.substr(1));
      if (el) {
        el.scrollIntoView(true);
      }
    }
  }
  // pageshow fires after load and on Back/Forward
  window.addEventListener('pageshow', pgshow);
}


/**
*********
* FORMS *
*********
*/

/**
* setText
**********
Set the text of an input or textarea element and update its style.

Parameters:
text (string): text content.

*/
function setText(text) {
  this.val(text);
  this.resetInputTextStyle();
}
jQuery.fn.setText = setText;


/**
* setDefaultText
*****************
Set the default text of an input or textarea element, update its style and
display this text.

Parameters:
text (string): text content.

*/
function setDefaultText(text) {
  this.data('default_text', text);
  this.val(this.data('default_text'));
  this.resetInputTextStyle();
}
jQuery.fn.setDefaultText = setDefaultText;


/**
* resetInputTextStyle
**********************
Update the sytle of an input or textarea element with a default text. If the
default text is displayed and only in this case, the element will use
'.default-text' CSS class.

*/
function resetInputTextStyle() {
  // check if element has its default value set
  if (this.val() == this.data('default_text')) {
    this.addClass('default-text');
  }
  else {
    this.removeClass('default-text');
  }
}
jQuery.fn.resetInputTextStyle = resetInputTextStyle;

/**
* ClearDefaultText
*******************
Remove default text from input text elements.

*/
function ClearDefaultText() {
  // clear fields with default text
  jQuery('input.with-default-text, textarea.with-default-text')
    .each(function() {
      if (jQuery(this).val() == jQuery(this).data('default_text')) {
        jQuery(this).val('');
      }
    })
  ;
}

/**
* PutBackDefaultText
*******************
Put back default text on empty input text elements.

*/
function PutBackDefaultText() {
  // clear fields with default text
  jQuery('input.with-default-text, textarea.with-default-text')
    .each(function() {
      if (jQuery(this).val() == '') {
        jQuery(this).val(jQuery(this).data('default_text'));
      }
    })
  ;
}

/**
* initInputDefaultText
***********************
Initialize input and textarea having a default text. The default text will be
displayed using the 'default-text'. Other text will be displayed normaly.

To create an input or textarea element with a default text, just create the HTML
element this way (the important attributes are 'class' and 'value'):

<input type="text" id="blabla" name="blabla" class="with-default-text" value="This is the default text"/>

<textarea id="species-desc" name="description" cols="40" rows="10" class="with-default-text">Enter your text here.</textarea>

*/
function initInputDefaultText() {
  // init input element
  jQuery.each(
    jQuery('input.with-default-text, textarea.with-default-text').not('.default-text-processed'),
    function(i, element_with_default) {
    jQuery(element_with_default).addClass('default-text-processed');
    if (null != element_with_default.defaultValue) {
      jQuery(element_with_default)
        .data('default_text', element_with_default.defaultValue)
        .bind({
          focus: function() {
            // clear quick search box if it contains default text
            if (jQuery(this).val() == jQuery(this).data('default_text'))
            {
              jQuery(this).val('');
              jQuery(this).removeClass('default-text');
            }
          },
          blur: function() {
            // put default text if quick search box is empty
            if (jQuery(this).val() == '')
            {
              jQuery(this).val(jQuery(this).data('default_text'))
              jQuery(this).addClass('default-text');
            }
          }
        })
      ;
      jQuery(element_with_default).resetInputTextStyle();
    }
  });
  PutBackDefaultText();
}


/**
* initCheckAll
***************
*/
function initCheckAll() {
  // make each checkbox with class check-all become the "master" checkbox of
  // its fieldset.
  jQuery('input.check-all')
    .not('.check-all-processed')
    .addClass('check-all-processed')
    .click(function(event) {
      // manage paged tables
      var pager_size = jQuery(this).parents('fieldset:eq(0)').find('.pagesize').first();
      var old_pager_size = pager_size.val();
      pager_size.val(1000000000).change();
      // check/uncheck all checkboxes
      jQuery(this).parents('fieldset:eq(0)').find(':checkbox').not('.no-check-all').prop('checked', this.checked);
      // check/uncheck all associated master checkboxes
      jQuery('input[class~="check-all-block-' + jQuery(this).parents('fieldset:eq(0)').prop('id') + '"]').not('.no-check-all').prop('checked', this.checked);
      // manage paged tables: put back old size value
      pager_size.val(old_pager_size).change();
    })
    .each(function(index, check_all_input) {
      // set inital state of all checkboxes
      jQuery(check_all_input).parents('fieldset:eq(0)').find(':checkbox').not('.no-check-all').prop('checked', check_all_input.checked);
      // add class to associated label
      jQuery(check_all_input).parents('label:eq(0)').addClass('check-all');
      jQuery('label[for="' + check_all_input.id + '"]').addClass('check-all');
    })
  ;
  // make each checkbox with class check-all-block + check-all-block-<block_id>
  // become the "master" checkbox of a given block having the ID "block_id".
  jQuery('input.check-all-block')
    .not('.check-all-processed')
    .addClass('check-all-processed')
    .click(function(event) {
      var block_id = jQuery(this).getClassValue('check-all-block-');
      jQuery('#' + block_id).find(':checkbox').not('.no-check-all').prop('checked', this.checked);
    })
    .each(function(index, check_all_input) {
      var block_id = jQuery(check_all_input).getClassValue('check-all-block-');
      // set inital state of all checkboxes
      jQuery('#' + block_id).find(':checkbox').prop('checked', check_all_input.checked);
    })
  ;
}


/**
Applets
*/

function insertApplet(applet_id, code, applet_file, codebase, url_of_tree_to_load, config_file, sequence, rel_type) {
  // parameters default values
  sequence = ((typeof(sequence) != 'undefined') ? sequence : '');
  rel_type = ((typeof(rel_type) != 'undefined') ? rel_type : '');

  jQuery(applet_id)
    .html('      <applet archive="' + applet_file + '" code="' + code + '" codebase="' + codebase + '" width="0" height="0" alt="ArchaeopteryxE is not working on your system (requires at least Sun Java 1.5)!" >\n\
        <param name="url_of_tree_to_load" value="' + url_of_tree_to_load + '" />\n\
        <param name="config_file" value="' + config_file + '" />\n\
        <param name="homology_type_analysis_query_sequence" value="' + sequence + '" />\n\
        <param name="homology_type_analysis_initial_relation_type" value="' + rel_type + '" />\n\
      </applet>');
}

function insertATVEApplet(input, data_file, applet_id, sequence, rel_type) {
  var applet_file = ATVE_APPLET;
  var config_file = "_aptx_configuration_file";
  var config_url  = APPLETS_URL + "/" + config_file;
  var applet_code = 'org.forester.archaeopteryx.ArchaeopteryxA.class'; // ArchaeopteryxA --> popup; ArchaeopteryxE --> embeded in page
  var parent_form = jQuery(input).parents('form');
  if (parent_form.exists()) {
    data_file += GetRemoveSpeciesParameter(parent_form);
  }
  insertApplet(applet_id, applet_code, applet_file, APPLETS_URL, data_file, config_url, sequence, rel_type);
}


var displayExceptionAlert = function (req, exception) {
    var dsException = "";
    dsException += " AJAX - EXCEPTION";
    dsException += "\n--------\n";
    dsException += "\n";
    dsException += "Request Information:";
    dsException += "\n--------\n";
    dsException += "req.transport.responseText: \n";
    dsException += req.transport.responseText.substr(0,50);
    dsException += "\n\n";
    dsException += "Exception Information:";
    dsException += "\n--------\n";
    dsException += "exception.name: ";
    dsException += exception.name
    dsException += "\n";
    dsException += "exception.number: ";
    dsException += exception.number;
    dsException += "\n";
    dsException += "exception.description: ";
    dsException += exception.description;
    alert(dsException);
}

function ShowLoading(target) {
  return jQuery(target).html('<i class="fas fa-spinner fa-pulse greenphyl-loading" title="Loading! Please wait..."></i>');
};

function EndLoading(target) {
  return jQuery(target).find('.greenphyl-loading').remove();
};

function ShowFailure(target, message) {
  return jQuery(target).html(
    '<i class="fas fa-times text-danger" title="'
    + (message ? message.replace(/"/g, "'") : 'Failed!')
    + '"></i>'
  );
};

function ShowWarning(target, message) {
  return jQuery(target).html(
    '<i class="fas fa-exclamation-triangle" title="'
    + (message ? message.replace(/"/g, "'") : 'Warning!')
    + '"></i>'
  );
};


/**
* SubmitFamilyAnnotation
*************************
*/
function SubmitFamilyAnnotation() {

  var submit_button = jQuery('form#family_annotation_form input[type=submit]');
  // disable save button
  submit_button.prop('disabled', true);

  // clear default values
  ClearDefaultText();

  //+FIXME: maybe use jQuery(form).serialize() instead?

  // gather field values
  var family_id = '&family_id=' + encodeURIComponent(jQuery('form#family_annotation_form input[name=family_id]').val());
  var family_type = '&family_type=' + encodeURIComponent(jQuery('#family_type').val());
  var family_name = '&family_name=' + encodeURIComponent(jQuery('#family_name').val());
  var family_synonyms = '&family_synonyms=' + encodeURIComponent(jQuery('#family_synonyms').val());
  var family_inferences = '';
  if (jQuery('#family_inferences').val()) {
    family_inferences = '&family_inferences=';
    if (jQuery('#family_inferences').val() instanceof Array) {
      family_inferences += jQuery('#family_inferences').val().join(',');
    }
    else {
      family_inferences += jQuery('#family_inferences').val();
    }
  }
  var family_description = '&family_description=' + encodeURIComponent(jQuery('#family_description').val());
  var family_dbxref = '&family_dbxref=' + encodeURIComponent(jQuery('#family_dbxref').val());
  var family_validated = '&family_validated=' + encodeURIComponent(jQuery('#family_validated').val());
  var annotation_status = '&annotation_status=' + encodeURIComponent(jQuery('#annotation_status').val());
  var comments = '&comments=' + encodeURIComponent(jQuery('#comments').val());

  var mmode_param = '';
  var master_mode = GetURLParameter('mmode');
  if (master_mode) {
    mmode_param = '&mmode=' + master_mode;
  }

  // prepare query
  var url = BASE_URL + "/annotate_family.cgi?mode=inner&p=update"
    + mmode_param
    + family_id
    + family_type
    + family_name
    + family_synonyms
    + family_inferences
    + family_description
    + family_dbxref
    + family_validated
    + annotation_status
    + comments
  ;

  // alert('DEBUG: ' + url); //+debug

  // post data
  jQuery.ajax({
    'url':      url,
    'type':     'GET',
    'dataType': 'html',
    'success':  function(html_data) {
        var updated_data = jQuery(html_data);
        // check if result is ok
        if (updated_data.find('form#family_annotation_form input[name=family_id]').val()
            != jQuery('form#family_annotation_form input[name=family_id]').val()) {
            alert("An error occurred:\n" + updated_data.text().replace(/( \&nbsp;)+/g, ' ').replace(/(\s*[\n\r])+/g, '\n'));
            HighlightSaveButton(submit_button);
        }
        else {
          // update fields
          jQuery('#family_type').val(updated_data.find('#family_type').val());
          jQuery('#family_name').val(updated_data.find('#family_name').val());
          jQuery('#family_synonyms').val(updated_data.find('#family_synonyms').val());
          jQuery('#family_inferences').find('option').each(function(index, element) {
            var selected_inferences = updated_data.find('#family_inferences').val();
            if (selected_inferences instanceof Array) {
              selected_inferences = selected_inferences.join(',');
            }
            if (0 <= selected_inferences.indexOf(jQuery(element).prop('value'))) {
              jQuery(element).prop('selected', true);
            }
            else {
              jQuery(element).prop('selected', false);
            }
          });
          jQuery('#family_description').val(updated_data.find('#family_description').val());
          jQuery('#family_dbxref').val(updated_data.find('#family_dbxref').val());
          jQuery('#family_validated').val(updated_data.find('#family_validated').val());
          UpdateEditableFieldLabels();
          UnhighlightSaveButton(submit_button);
        }

        // re-enable save button
        submit_button.removeAttr('disabled');
      },
    'error':    function(jqXHR, textStatus, errorThrown) {
      alert("An error occurred: " + jqXHR.statusText);
      // re-enable save button
      HighlightSaveButton(submit_button);
      submit_button.removeAttr('disabled');
      return;
    }
  });

  return false;
}


/**
* AddSelectedFamiliesToProcess
*******************************
Add Selected families to process creation/update form.

*/
function AddSelectedFamiliesToProcess(process_form_selector) {
  // remove previous families
  jQuery(process_form_selector).find('input[name="family_id"]').remove();
  // check if list exists
  if (jQuery('#family_results').find('input[name="family_id"]:eq(0)').exists()) {
      // expand list
      jQuery('#family_results .pagesize').val(1000000000).change();
      // get selected families
      jQuery('#family_results')
        .find('input[name="family_id"]:checked')
          .each(function(index, element) {
            // add them to the form
            jQuery(process_form_selector).append('<input type="hidden" name="family_id" value="' + jQuery(element).val() + '"/>');
          })
      ;
  }
  // remove previous steps
  jQuery(process_form_selector).find('input[name="family_step"]').remove();
  // add steps
  jQuery('#steps_to_perform')
    .find('input[name="family_step"]:checked')
      .each(function(index, element) {
        // add them to the form
        jQuery(process_form_selector).append('<input type="hidden" name="family_step" value="' + jQuery(element).val() + '"/>');
      })
  ;
  return true;
}


/**
* SubmitFormWithAjaxToBlock()
*/
function SubmitFormWithAjaxToBlock(form_selector, target_block_selector, complete_func) {
  var local_form = jQuery(form_selector);
  var target_block = jQuery(target_block_selector);

  if (!local_form.exists()) {
    alert('An error occured using AJAX: requested form (' + form_selector + ') was not found! Please report this error to the site administrator.');
    return;
  }
  if (!target_block.exists()) {
    alert('An error occured using AJAX: target block for output results (' + target_block_selector + ') was not found! Please report this error to the site administrator.');
    return;
  }
  
  // make sure we got an initialisation function
  if (!complete_func) {
    complete_func = function(code) {
        initGreenPhylPage();
        // scroll to loaded content
        jQuery.scrollTo(target_block, {'duration': 500});
    };
  }

  // prepare URL
  var url = local_form.prop('action');
  // change output mode
  var master_mode = GetURLParameter('mmode');
  if (!master_mode) {
    master_mode = 'full';
  }
  url = InsertURLParameter(url, 'mmode', master_mode);
  url = RemoveURLParameter(url, 'mode', true);
  url = InsertURLParameter(url, 'mode', 'inner');
  
  // prepare data
  ClearDefaultText();
  var data = local_form.serialize();
  PutBackDefaultText();

  // alert('POST url: ' + url + '\nSerialized data: ' + data); //+debug

  // display loading animated gif
  ShowLoading(target_block_selector);

  // Disable submit buttons
  local_form.find('input[type="submit"]').prop('disabled', 'disabled');
  
  // post data
  jQuery.ajax({
    'url':      url,
    'data':     data,
    'type':     'POST',
    'dataType': 'html',
    'success':  function(html_data) {
        // re-enable submit button
        local_form.find('input[type="submit"]').removeAttr('disabled');
        // transfer output
        target_block.html(html_data);
        // run initialization
        complete_func(html_data);
      },
    'error':    function(jqXHR, textStatus, errorThrown) {
      if (DEBUG) {
        alert("An error occurred: " + jqXHR.statusText);
      }
      // re-enable submit button
      local_form.find('input[type="submit"]').removeAttr('disabled');
      // transfer error message
      if (target_block.exists()) {
        target_block.text("An error occurred: " + jqXHR.statusText);
        return;
      }
      return;
    }
  });
  return false;
}


/**
*********
* POPUP *
*********
*/

/**
* ShowDialogFor
****************
*/
function ShowDialogBox(popup_box, element, frame_url, post_data) {
    var popup_title = jQuery(element).getClassValue('popup-title-');
    if (popup_title) {
      popup_title = popup_title.replace(/_/g, ' ');
    }
    else {
      popup_title = DEFAULT_TITLE;
    }
    // clear previous content
    var frameDoc = popup_box.get(0).contentDocument || popup_box.get(0).contentWindow.document;
    if (frameDoc) {
      frameDoc.documentElement.innerHTML = '';
      // popup_box.get(0).contentWindow.location.reload();
      // popup_box.get(0).contentWindow.location.assign('');
    }

    // prepare URL: change output mode
    var master_mode = GetURLParameter('mmode');
    if (!master_mode) {
      master_mode = 'full';
    }
    frame_url = InsertURLParameter(frame_url, 'mmode', master_mode);
    frame_url = RemoveURLParameter(frame_url, 'mode', true);
    frame_url = InsertURLParameter(frame_url, 'mode', 'frame');

    // load box
    if (post_data) {
      popup_box
        .dialog('option', 'title', popup_title)
        .dialog('open')
        .width(popup_box.parent().innerWidth() - 16)
      ;

      // submit using POST method
      jQuery.ajax({
        url: frame_url,
        data: post_data,
        type: 'post',
        success: function(data) {
          frameDoc.open();
          frameDoc.write(data);
          frameDoc.close();
        }
      });
    }
    else {
      popup_box
        .prop('src', frame_url)
        .dialog('option', 'title', popup_title)
        .dialog('open')
        .width(popup_box.parent().innerWidth() - 16)
      ;
      // Force iframe to position to anchor if one.
      if (popup_box.get(0).contentWindow.location.hash) {
        popup_box.get(0).contentWindow.location.hash = popup_box.get(0).contentWindow.location.hash;
      }
    }
  return false;
}

/**
* initPopups
*************
Initializes popups dialog box.

To create a popup link, replace DIALOG_ID by a valid HTML identifier and
DIALOG_TITLE by the dialog box title using underscores instead of spaces:
<a href="..." class="popup-link popup-id-DIALOG_ID popup-title-DIALOG_TITLE" >
  Link label
</a>

*/
function initPopups() {
  jQuery('.popup-link').not('.popup-processed').each(function(index, element) {
    // get popup ID
    var popup_id = jQuery(element).getClassValue('popup-id-');
    if (!popup_id) {
      popup_id = 'popup';
    }

    // check if popup exists
    var popup_box = jQuery('#' + popup_id);
    if (!popup_box.exists()) {
      // create popup dialog box (as an iframe)
      popup_box = jQuery('<iframe id="' + popup_id + '"></iframe>').appendTo(jQuery('body'));
        popup_box.dialog({
        autoOpen: false,
        height: 640,
        width: 768,
        modal: false,
        title: DEFAULT_TITLE,
        dialogClass: 'popup-dialog'
      });
      jQuery('.popup-dialog.ui-dialog').css({position:"fixed"});
      jQuery(".ui-resizable").stop(function() {
        jQuery(".popup-dialog.ui-dialog").css({position:"fixed"});
      });
    }

    jQuery(element).addClass('popup-processed');
    if ('INPUT' == jQuery(element).get(0).nodeName) {
      // button
      jQuery(element)
        .prop('onclick', 'return false;')
        .click(function(event) {
            event.preventDefault();
            var parent_form = jQuery(this).parents('form');

            // make sure everything is ok
            if (!validate(parent_form)) {
                return false;
            }

            // prepare data
            var old_pager_size = parent_form.find('.pagesize').val();
            parent_form.find('.pagesize').val(1000000000).change();
            ClearDefaultText();
            var data = parent_form.serialize();
            PutBackDefaultText();
            parent_form.find('.pagesize').val(old_pager_size).change();

            // get URL
            var frame_url = parent_form.prop('action');

            // load box (submit using POST)
            return ShowDialogBox(popup_box, this, frame_url, data);
        })
      ;
    }
    else if ('A' == jQuery(element).get(0).nodeName) {
      // anchor
      if (jQuery(element).is('.popup-use-form')) {
        // submit form to popup using the anchor URL instead of the form one
        jQuery(element)
          .click(function(event) {
            event.preventDefault();
            var parent_form = jQuery(this).parents('form');

            // prepare data
            var old_pager_size = parent_form.find('.pagesize').val();
            parent_form.find('.pagesize').val(1000000000).change();
            ClearDefaultText();
            var data = parent_form.serialize();
            PutBackDefaultText();
            parent_form.find('.pagesize').val(old_pager_size).change();

            // get URL
            var frame_url = jQuery(this).prop('href');

            // load box (submit using POST)
            return ShowDialogBox(popup_box, this, frame_url, data);
          })
        ;
      }
      else {
        // no form, just use the anchor URL
        jQuery(element)
          .click(function(event) {
              // get URL
              var frame_url = jQuery(this).prop('href') || '';

              // load box
              return ShowDialogBox(popup_box, this, frame_url);
          })
        ;
      }
    }
    else {
        console.log('Warning: unsupported popup opener element "' + jQuery(element).get(0).nodeName + '"!');
    }
  });
}




/**
GetRandomColor()
*/
function GetRandomColor() {
  return '#' + (0x1000000 + Math.random() * 0xFFFFFF).toString(16).substr(1,6);
}


/**
Color Picker
*/
function getHexFromRGB(r, g, b) {
  return (0x1000000 + r * 0x010000 + g * 0x000100 + b * 0x01).toString(16).substr(1,6);
}

function refreshSwatch() {
  var red   = jQuery('#cp_red').slider('value'),
      green = jQuery('#cp_green').slider('value'),
      blue  = jQuery('#cp_blue').slider('value'),
      hex   = getHexFromRGB(red, green, blue);
  g_cp_color =  '#' + hex;
  jQuery('#cp_swatch')
    .css('background-color', g_cp_color)
  ;
  jQuery('#cp_value')
    .val(g_cp_color)
  ;
}

function showColorPicker(ok_callback) {

  ok_callback = ((typeof(ok_callback) != 'undefined') ? ok_callback : null);
  
  if (!jQuery('#color_picker').exists()) {
    // create color picker box
    jQuery('#greenphyl_website').append('<div id="color_picker" class="horizontal-container">\n\
  <div id="cp_color_sliders" class="block">\n\
    <div id="cp_red"></div>\n\
    <div id="cp_green"></div>\n\
    <div id="cp_blue"></div>\n\
  </div>\n\
  <div id="cp_swatch" class="block"></div>\n\
  <input type="text" id="cp_value" size="9" class="center-block"/>\n\
</div>'); // note: requiered closing br-clear (for horizontal-container) is added by initCenteringElements for the center-block class
    initCenteringElements();
  }

  jQuery('#color_picker')
    .dialog({
      title: 'Color Picker',
      width: '444px',
//      height: '400px',
      buttons: { 'Ok': function() { jQuery(this).dialog('close'); ok_callback(); } },
      draggable: true,
      modal: false
    })
  ;

  jQuery('#cp_red, #cp_green, #cp_blue')
    .not('.cp-slider-processed')
    .addClass('cp-slider-processed')
    .slider({
      orientation: 'horizontal',
      range: 'min',
      max: 255,
      value: 255,
      slide: refreshSwatch,
      change: refreshSwatch
    })
  ;
  
  jQuery('#cp_value')
    .not('.cp-value-processed')
    .addClass('cp-value-processed')
    .change(function(event) {
      if (jQuery('#cp_value').val().match(/^#[0-9a-fA-F]{6}$/)) {
        g_cp_color = jQuery('#cp_value').val();
        initColorPickerSliders();
      } 
    })
  ;
  initColorPickerSliders();
}

function setBackgroundColor(event, ui) {
  jQuery('html').css('background-color', g_cp_color);
//+debug  jQuery.cookie('background-color', g_cp_color);
}

function initColorPickerSliders() {
  var current_red_color   = parseInt('0x' + g_cp_color.substr(1,2), 16),
      current_green_color = parseInt('0x' + g_cp_color.substr(3,2), 16),
      current_blue_color  = parseInt('0x' + g_cp_color.substr(5,2), 16);
  jQuery('#cp_red').slider('value',   current_red_color);
  jQuery('#cp_green').slider('value', current_green_color);
  jQuery('#cp_blue').slider('value',  current_blue_color);
}


/**
URL parameters
*/
/**
* GetURLParameter
******************
*/
/* returns the value of a given parameter */
function GetURLParameter(name) {
  name = name.replace(/[\[]/, "\\\[").replace(/[\]]/, "\\\]");
  var regexS = "[\\?&]" + name + "=([^&#]*)";
  var regex = new RegExp(regexS);
  var results = regex.exec(window.location.search);
  if (null == results) {
    return '';
  }
  else {
    return decodeURIComponent(results[1].replace(/\+/g, ' '));
  }
}

/**
* GetURLParameters
*******************
*/
/* returns a hash with all parameters */
function GetURLParameters() {
    var vars = [], hash;
    var hashes = window.location.href.slice(window.location.href.indexOf('?') + 1).split('&');
    for (var i = 0; i < hashes.length; i++) {
        hash = hashes[i].split('=');
        vars.push(hash[0]);
        vars[hash[0]] = hash[1];
    }
    return vars;
}

/**
* InsertURLParameter
*********************
Add a parameter inside a URL which may inclue an anchor.

*/
function InsertURLParameter(url, parameter, value) {
  var hash = '';
  var query = '';
  var join_char = '?';
  var path = '';
  var new_url = url;
  // check for anchor
  if (0 <= new_url.indexOf('#')) {
    hash = new_url.substr(new_url.indexOf('#'));
    new_url = new_url.substr(0, new_url.indexOf('#'));
  }
  // check for query string
  if (0 <= new_url.indexOf('?')) {
    query = new_url.substr(new_url.indexOf('?'));
    new_url = new_url.substr(0, new_url.indexOf('?'));
    join_char = '&';
  }
  new_url += query + join_char + parameter + (null != value ? '=' + value : '') + hash;
  return new_url;
}

/**
* RemoveURLParameter
*********************
Remove the first occurrence of a parameter from query string inside a URL which
may inclue an anchor.

If 'all_occurences', all occurrences are removed.

*/
function RemoveURLParameter(url, parameter, all_occurences) {
  var hash = '';
  var new_url = url;
  // check for anchor
  if (0 <= new_url.indexOf('#')) {
    hash = new_url.substr(new_url.indexOf('#'));
    new_url = new_url.substr(0, new_url.indexOf('#'));
  }
  
  var modifier = '';
  if (all_occurences) {
    modifier = 'g';
  }
  
  // prepare parameter name to be used in a regexp
  parameter = parameter.replace('.', '\\.');
  // remove parameter from url
  var prefix = '?';
  var regexp = new RegExp('\\?' + parameter + '=[^&]*&?', modifier);
  if (!new_url.match(regexp)) {
    prefix = '&';
    regexp.compile('&' + parameter + '=[^&]*&?', modifier);
  }

  new_url = new_url.replace(regexp, prefix) + hash;
  return new_url;
}

/**
* ReplaceURLParameter
**********************
Replace the value of all occurrences of a parameter from query string inside a
URL which may inclue an anchor.

*/
function ReplaceURLParameter(url, parameter, new_value) {
  var new_url = RemoveURLParameter(url, parameter, true);
  new_url = InsertURLParameter(new_url, parameter, new_value);
  return new_url;
}


/*
Select the text of an element.
*/
function selectText(selector) {
  var text_element = jQuery(selector).get(0);

  if (!text_element) {
    return;
  }

  if (document.body.createTextRange) { // ms
      var range = document.body.createTextRange();
      range.moveToElementText(text_element);
      range.select();
  } else if (window.getSelection) { // moz, opera, webkit
      var selection = window.getSelection();            
      var range = document.createRange();
      range.selectNodeContents(text_element);
      selection.removeAllRanges();
      selection.addRange(range);
  }
}


/**
 Toggle details
*/
function toggleDetails(selector) {
  jQuery(selector).find('.details').toggle();

}




/**
* Form Validation
******************

*/
var STATUS_OK      = 'ok';
var STATUS_WARNING = 'warning';
var STATUS_ERROR   = 'error';

function InitConfirmations() {
  jQuery('input.with-confirmation, a.with-confirmation')
    .not('.with-confirmation-processed')
    .addClass('with-confirmation-processed')
    .on('click', function(event) {
      var confirmation_function = jQuery(this).getClassValue('confirmation-');
      if (confirmation_function) {
        if (window[confirmation_function]
            && (typeof window[confirmation_function] == 'function')) {
          if (!window[confirmation_function](this)) {
            event.preventDefault();
            return false;
          }
          else {
            return true;
          }
        }
        else {
          alert("ERROR: This action does not appear to have been properly configured! Please report this error to the site administrator.");
          event.preventDefault();
          return false;
        }
      }
      return true;
    })
  ;
  jQuery('form.with-confirmation')
    .not('.with-confirmation-processed')
    .addClass('with-confirmation-processed')
    .on('submit', function(event) {
      var confirmation_function = jQuery(this).getClassValue('confirmation-');
      if (confirmation_function) {
        if (window[confirmation_function]
            && (typeof window[confirmation_function] == 'function')) {
          if (!window[confirmation_function]()) {
            event.preventDefault();
            return false;
          }
          else {
            return true;
          }
        }
        else {
          alert("ERROR: This action does not appear to have been properly configured! Please report this error to the site administrator.");
          event.preventDefault();
          return false;
        }
      }
      return true;
    })
  ;
}
  


function getFieldName(field) {
  var label = jQuery(field).parent('label:eq(0)');
  if (label.exists()) {
    return label.text();
  }
  else if (field.id) {
    label = jQuery('label[for="' + field.id + '"]')
    if (label.exists()) {
      return label.text();
    }
  }
  return field.name;
}


function validate(form) {
  // get current object if none specified
  form = ((typeof(form) != 'undefined') ? form : this);
  
  // if it's an event, use target
  if (('target' in form) && form.target) {
    form = form.target;
  }

  // accept form submission by default
  var return_status = true;

  // remove default text
  ClearDefaultText();

  // init form validation data
  jQuery(form).data('status', STATUS_OK);
  jQuery(form).data('reasons', new Array());
  jQuery(form).data('can_ignore', true);

  // clear invalidated elements
  jQuery(form).find('.invalidated').removeClass('invalidated');
  
  // check form elements
  jQuery(form).find('.required').each(function(index){validateNonEmptyField(this, form);});
  jQuery(form).find('.required_number').each(function(index){validateNumberField(this, form);});
  jQuery(form).find('.required_positive_int').each(function(index){validatePositiveIntField(this, form);});
  jQuery(form).find('.required_valid_sequences').each(function(index){validateAvailableSequencesField(this, form);});
  jQuery(form).find('.required_selected_ipr').each(function(index){validateSelectedIPR(this, form);});

  // display an error message if one
  if (STATUS_OK != jQuery(form).data('status')) {
    // put back default text
    PutBackDefaultText();

    // if dialog box does not exist, create it
    var form_validation_dialog = jQuery('#form_validation_dialog');
    if (!form_validation_dialog.exists()) {
      form_validation_dialog = jQuery('<div id="form_validation_dialog" title="Form Validation Error Message"></div>');
      form_validation_dialog.appendTo('body');
    }

    // set dialog content
    form_validation_dialog.html('Some fields need correction:<br/>\n<ul>\n  <li>\n' + jQuery(form).data('reasons').join('</li>\n  <li>') + '</li>\n</ul>\n');

    // prepare buttons
    var buttons;
    //if (STATUS_WARNING == jQuery(form).data('status')) {
    //  buttons = {
    //    'Ignore': function() {
    //      jQuery(this).dialog('close');
    //      // // force re-submission
    //      // jQuery(form).submit();
    //    },
    //    'Cancel': function() {
    //      jQuery(this).dialog('close');
    //      // focus on and select first invalidated element
    //      jQuery(form).find('.invalidated').first().focus().select();
    //    }
    //  };
    //}
    //else {
      buttons = {
        'Ok': function() {
          jQuery(this).dialog('close');
          // focus on and select first invalidated element
          jQuery(form).find('.invalidated').first().focus().select();
        }
      };
    //}

    // display jQuery UI dialog
    form_validation_dialog.dialog({
        'modal': true,
        'width': '50%',
        'buttons': buttons
    });
    // invalidate form submission
    return_status = false;
  }

  // submission ok
  return return_status;
}

function validateNonEmptyField(field, form) {
  if (!jQuery(field).val().length) {
    // mark field as invalidated
    jQuery(field).addClass('invalidated');
    // invalidate form
    jQuery(form).data('status', STATUS_ERROR);
    jQuery(form).data('reasons').push('The required field "' + getFieldName(field) + '" has not been filled in!');
  }
}

function validateNumberField(field, form) {
  if (!jQuery(field).val().length || isNaN(jQuery(field).val())) {
    // mark field as invalidated
    jQuery(field).addClass('invalidated');
    // invalidate form
    jQuery(form).data('status', STATUS_ERROR);
    jQuery(form).data('reasons').push('The required field "' + getFieldName(field) + '" must be filled with a number!');
  }
}

function validatePositiveIntField(field, form) {
  if (!jQuery(field).val().length
      || !jQuery(field).val().match(/^\+?[0-9]+$/)) {
    // mark field as invalidated
    jQuery(field).addClass('invalidated');
    // invalidate form
    jQuery(form).data('status', STATUS_ERROR);
    jQuery(form).data('reasons').push('The required field "' + getFieldName(field) + '" must be filled with a positive integer!');
  }
}

function validateAvailableSequencesField(field, form) {
  // check if we got sequences to check
  if (jQuery(field).val().length) {
    // get array of sequences text ID
    var seq_textids = '&seq_textid=' + jQuery(field).val().split(/[,\s\t\n\r]+/).join('&seq_textid=');
    var query_url = BASE_URL + '/sequences.pl?service=check_seq_textids&format=json' + seq_textids;
    // get status from database...
    var sequence_data = {'unavailable': [], 'available': []};
    jQuery.ajax({
      url: query_url,
      type: "GET",
      cache: false,
      async: false,
      timeout: 5000,
      dataType: "json",
      error: function(jq_xhr, text_status, error_thrown) {
        // an error occured, we can't check
        // if (text_status == "abort", "timeout", "No Transport"...)
        // just ignore checking
      },
      success: function(data, textStatus, jqXHR) {
        sequence_data = data;
      }
    });

    // check if we got invalid sequence text ID
    if (sequence_data.unavailable.length) {
       // mark field as invalidated
       jQuery(field).addClass('invalidated');
       // set warning if ok
       if (STATUS_OK == jQuery(form).data('status')) {
         jQuery(form).data('status', STATUS_WARNING);
       }
       var invalid_sequence_list = sequence_data.unavailable.join(', ');
       invalid_sequence_list = invalid_sequence_list.replace(/[^0-9a-zA-Z\-_\.,\s]/g, '?');
       // invalidate form
       jQuery(form).data('reasons').push('The field "' + getFieldName(field) + '" contains sequences that are not in our database:<br/>\n<em>' + invalid_sequence_list + '</em>');
    }
  }

}

/* applies to a block or a fieldset */
function validateSelectedIPR(field, form) {
  if (!jQuery(field).find('input[name="ipr_id"]:checked').exists()) {
    // mark field as invalidated
    jQuery(field).addClass('invalidated');
    // invalidate form
    jQuery(form).data('status', STATUS_ERROR);
    jQuery(form).data('reasons').push('You must check at least one IPR domain!');
  }
}



var extractFamilySortingData = function(node)  
{
    if (jQuery(node).find('img.family-status').exists()) {
        return jQuery(node).find('img.family-status').first().getClassValue('family-status-');
    }
    if (jQuery(node).find('img.family-analyzes').exists()) {
        return jQuery(node).find('img.family-analyzes').first().getClassValue('family-analyzes-');
    }
    return jQuery(node).text();
} 

/**
* TREE DISPLAY
***************
*/
function GetUnselectedSpecies(form) {
  // check if we have unselected species and a least one species seleted
  var unselected_species_input = form.find('input[name="species_code"]:not(:checked)');
  var unselected_species = new Array();
  if (unselected_species_input.exists()
      && form.find('input[name="species_code"]:checked').exists()) {
    unselected_species_input.each(function (index, element) {
        unselected_species.push(jQuery(element).val());
    });
  }
  return unselected_species;
}

function GetRemoveSpeciesParameter(form) {
  // check if we have species to remove
  var remove_parameter = '';
  jQuery.each(GetUnselectedSpecies(form), function (i, species_code) {
    remove_parameter += '&remove=' + species_code;
  });
  return remove_parameter;
}

function showTree(submit_button, tree_url) {

  // get newick content
  var newick_content;
  // check if we have species to remove
  var remove_species = GetRemoveSpeciesParameter(jQuery(submit_button).parents('form'));
  tree_url += remove_species;
  tree_url = RemoveURLParameter(tree_url, 'format', true);
  tree_url = InsertURLParameter(tree_url, 'format', 'newick');
  // console.log('DEBUG: tree URL: ' + tree_url); //+debug
  
  // do AJAX query to get newick content
  jQuery.ajax({
    'url':      tree_url,
    'async':    false,
    'cache':    false,
    'type':     'GET',
    'dataType': 'text',
    'success':  function (text_tree) {
      // create a in-memory div, set it's inner text(which jQuery automatically encodes)
      // then grab the encoded contents back out.  The div never exists on the page.
      // newick_content = jQuery('<div/>').text(text_tree).html().replace(/"/g, '&quot;');
      newick_content = text_tree.replace(/^\s*/, '').replace(/\s*$/, '');
    },
    'error':    function(jqXHR, textStatus, errorThrown) {
      alert("An error occurred: " + jqXHR.statusText);
      return;
    }
  });

  // console.log('NEWICK: ' + newick_content); //+debug

  // create (form) target iframe used in the popup
  var iframe_name = 'tree_frame';
  jQuery('iframe[name="tree_frame"]').remove(); //+FIXME: check if it works with jQuery UI Dialog
  var tree_rendering_box = jQuery('<iframe name="' + iframe_name  + '"></iframe>')
    .appendTo(jQuery('body'))
    .css({'margin' : '0 0 16px 0'})
    .dialog({
      autoOpen: false,
      height: 650,
      width: '80%',
      modal: false,
      title: 'Greenphyl Tree Display',
      dialogClass: 'tree-dialog',
      close: function(event, ui) {
          jQuery(ui).remove(); //+FIXME: to check...
        }
    })
  ;
  tree_rendering_box
    .dialog('open')
	.width(tree_rendering_box.parent().innerWidth() - 32)
  ;

  if (!jQuery(submit_button).parents('form').exists()) {
    return;
  }

  var parent_form = jQuery(submit_button).parents('form').first();
  var intreegreate_url = parent_form.find('input[name="intreegreate_url"]').val();
  var treedisplay_url = parent_form.find('input[name="treedisplay_url"]').val();
  // Check if we can use TreeDisplay or if we have species to filter.
  if (remove_species)
  {
      // Change form URL.
      parent_form.prop('action', intreegreate_url);
  }
  else
  {
      // Use TreeDisplay.
      parent_form.prop('action', treedisplay_url);
  }
  parent_form
    .prop('target', iframe_name)
    .find('input[name="hiddenfield"]')
    .val(newick_content);
  // console.log(parent_form); //+debug

  // set InTreeGreate width
  var form_url = parent_form.prop('action');
  var iframe_width = jQuery('iframe[name="tree_frame"]').width();
  form_url = RemoveURLParameter(form_url, 'width', true);
  form_url = InsertURLParameter(form_url, 'width', iframe_width);
  parent_form.prop('action', form_url);
  // console.log(form_url); //+debug
  // console.log('debug done'); //+debug
  
  // var area_width = parent_form.find('input[name="widtharea"]').val();
  // var tree_display_url = '?=' + newick_tree) + '&amp;widtharea=1800&amp;collapsewidth=2&amp;linearea=3&amp;roundarea=20&amp;fontarea=14&amp;supportsizearea=11&amp;familyarea=Candara&amp;colorBackArea=white&amp;colorLineArea=%2305357E&amp;colorCollapseArea=%23EEEEEE&amp;colorTagArea=%23FF0000&amp;colorTextArea=black&amp;display=ultra';
  //    alert('URL: ' + tree_display_url);
  //    tree_rendering_box
  //      .prop('src', tree_display_url)
  //      .dialog('open')
  //      .width(tree_rendering_box.parent().innerWidth() - 16)
  //    ;

  // submit
  return true;
}



/**
* TREE DISPLAY
***************
*/
function showFamiliesRelatedToGO(go_code) {
  if (!go_code) {
    alert('No GO code provided! Unable to show go family relationship diagram.');
    return;
  }
  var mmode_param = '';
  var master_mode = GetURLParameter('mmode');
  if (master_mode) {
    mmode_param = '&mmode=' + master_mode;
  }
  // create (form) target iframe used in the popup
  var iframe_name = 'go_frame';
  jQuery('iframe[name="go_frame"]').remove(); //+FIXME: check if it works with jQuery UI Dialog
  var go_rendering_box = jQuery('<iframe name="' + iframe_name  + '" src="' + BASE_URL + '/go.pl?service=get_go_families&format=html&mode=frame' + mmode_param + '&go_code=' + go_code + '&level=1"></iframe>')
    .appendTo(jQuery('body'))
    .css({'margin' : '0 0 16px 0'})
    .dialog({
      autoOpen: false,
      height: 650,
      width: '80%',
      modal: false,
      title: 'Greenphyl GO Family Relationships',
      dialogClass: 'go-dialog',
      close: function(event, ui) {
          jQuery(ui).remove(); //+FIXME: to check...
        }
    })
  ;
  go_rendering_box
    .dialog('open')
	.width(go_rendering_box.parent().innerWidth() - 32)
  ;

  // no submit
  return false;
}

function showGORelationships(go_code) {

  if (!go_code) {
    alert('No GO code provided! Unable to show go family relationship diagram.');
    return;
  }

  var mmode_param = '';
  var master_mode = GetURLParameter('mmode');
  if (master_mode) {
    mmode_param = '&mmode=' + master_mode;
  }

  // create (form) target iframe used in the popup
  var iframe_name = 'go_frame';
  jQuery('iframe[name="go_frame"]').remove(); //+FIXME: check if it works with jQuery UI Dialog
  var go_rendering_box = jQuery('<iframe name="' + iframe_name  + '" src="' + BASE_URL + '/families.pl?service=get_go_relationship&format=html&mode=frame' + mmode_param + '&go_code=' + go_code + '"></iframe>')
    .appendTo(jQuery('body'))
    .css({'margin' : '0 0 16px 0'})
    .dialog({
      autoOpen: false,
      height: 650,
      width: '80%',
      modal: false,
      title: 'Greenphyl GO Family Relationships',
      dialogClass: 'go-dialog',
      close: function(event, ui) {
          jQuery(ui).remove(); //+FIXME: to check...
        }
    })
  ;
  go_rendering_box
    .dialog('open')
	.width(go_rendering_box.parent().innerWidth() - 32)
  ;

  // no submit
  return false;
}

/**
* V5 Stuff
***********
*/

/**
 * isDark
 *********
 * Tells if a color is dark. Useful to select a text color according to its
 * background.
 *
 * @param background_color (string): a color string like "rgb(124, 32, 63)" or
 *   "rgba(255, 48, 0, 16)" usually provided by
 * jQuery(element).css('background-color');
 */
function isDark(background_color) {
  var background_color
  var rgba = /rgba?\((\d+).*?(\d+).*?(\d+)(?:.*?(\d+))?\)/.exec(background_color);
  // http://www.w3.org/TR/AERT#color-contrast
  var brightness = Math.round(
    (
      (parseInt(rgba[1]) * 299)
      + (parseInt(rgba[2]) * 587)
      + (parseInt(rgba[3]) * 114)
    )
    / 1000
  );
  return (brightness < 125);
}

/**
 * guessSequenceType
 ********************
 * Returns the type of sequence: protein, dna, rna or an empty string if
 * the sequence is invalid.
 *
 * @param sequence (string): the sequence string.
 *
 * @return string: either 'protein', 'dna', 'rna' or an empty string if the
 *   sequence is not valid
 */
function guessSequenceType(sequence)
{
  var match = sequence.match(/^(?:>[\x20-\x7E\t]*[\r\n]+)?([A-Z\*\-\.\?\r\n]+)$/i);
  var sequence_type = '';
  if (match)
  {
    // Looks like a valid sequence content.
    // Check for protein.
    if (match[1].match(/[EFIJLOPQXZ]/i))
    {
      sequence_type = 'protein';
    }
    else
    {
      // Unsure. Check proportions:
      // over 70% ATUGCNWSKM, it should not be a protein.
      var clean_sequence = sequence
        .replace(/^>[^\r\n]*[\r\n]+/, '')
        .replace(/[\*\-\.\?\r\n]+/g, '')
      ;
      var na_count = (clean_sequence.match(/[ATUGCNWSKM]/gi) || []).length;
      if ((na_count / clean_sequence.length) > 0.7)
      {
        // Nucleic acids.
        if (clean_sequence.match(/U/i))
        {
          sequence_type = 'rna';
        }
        else
        {
          sequence_type = 'dna';
        }
      }
      else
      {
        sequence_type = 'protein';
      }
    }
  }
  return sequence_type;
}
// Calls the initialization function once the document is loaded
jQuery(initGreenPhylPage);
