/**
********************************************************************************
******************************************************
* GreenPhyl Javascript Library - Relationship module *
******************************************************
Version:  3.1.0
Contacts: Mathieu Rouard <m.rouard@cgiar.org>
          Valentin Guignon <v.guignon@cgiar.org>
Requires: jQuery >=1.9, jQueryUI >=1.10, Raphael >= 2.0.1

Description:
This file contains the Javascript code used on GreenPhyl website to draw
relationships in a SVG graph.

Copyright 2011, Bioversity International - CIRAD

********************************************************************************
*/

// delay between family relationship loadings
var RELATIONSHIP_INTER_LOADING_DELAY = 3; // milliseconds

var ZOOM_MIN = 0.25;
var ZOOM_MAX = 2;

var TREE_HORIZONTAL_PADDING = 40; // pixels; default: 40
var TREE_VERTICAL_PADDING   = 40; // pixels; default: 40

// Label padding
var TEXT_PADDING = 4; // pixels; default: 4

var FONT_SIZE    = 10; // pixels; default: 10
var STROKE_WIDTH = 1; // pixels; default: 1
var LINK_WIDTH   = 4; // pixels; default: 4


// Object box width
var BOX_DEFAULT_WIDTH = 18; // pixels; default: 18
// Object box height
var BOX_DEFAULT_HEIGHT = 64; // pixels; default 64
// space between boxes
var BOX_HORIZONTAL_SPACE = 10; // pixels; default 10
var BOX_VERTICAL_SPACE   = 80; // pixels; default: 80

// set link style
var STRAIGHT_LINKS = false;
var PROPORTIONAL_WIDTH_LINKS = true;
var PROPORTIONAL_OPACITY_LINKS = true;

var VERTICAL_LABELS = true;
var VERTICAL_TREE = true;

// colors
var BOX_LABEL_COLOR  = '#000000';
var BOX_BACKGROUND   = '#e0ffe0';
var BOX_BORDER       = '#000000';
var LINK_MAIN_COLOR  = '#008000';
var LINK_OTHER_COLOR = '#404040';
var LINK_MIN_OPACITY = 0.25;

// stack of relationship loading function (see "LoadRelationshipWithDelay"
// below)
var g_relationship_load_stack = new Array();
// stack for relationship graph that couldn't be drawn
var g_relationship_notloaded_stack = new Array();
// keep track of graph already rendered
var g_processed_objects = {};

// maximum number of elements for an object (used to set link opacity)
// This is used to setup the maximum opacity to have a global overview
// of the elements flow (while link width give a local view of the flow
// between a parent and its children). It's computed when the graph is drawn by
// looping on root elements to find the one the the max amount of elements.
var g_max_elements = 1;

// function handler for drawing boxes and connectors
var g_draw_box_handlers = {};
var g_draw_connectors_handlers = {};


/**
* DrawDefaultConnector
***********************
Draw a single default connector between 2 object boxes.

Parameters: 
* paper: Raphael paper to draw on
* object_data: object data including top-left coordinate of current object box
* children_data: children object data including top-left coordinate of boxes

*/
function DrawDefaultConnector(paper, object_data, child_data) {

  var link_path;

  // check if HTML elements exist
  var object_html = $(object_data.selector).first();
  var child_html = $(child_data.selector).first();

  // select link color
  var link_color = LINK_MAIN_COLOR;

  // select path opacity
  var link_opacity = 1;
  if (PROPORTIONAL_OPACITY_LINKS) {
    link_opacity = LINK_MIN_OPACITY + (1-LINK_MIN_OPACITY)*(child_data.common[object_data.id].count / g_max_elements);
    if (!object_html.exists() || !child_html.exists()) {
      // link is not for a main relationship
      // (ie. it does not contain any element of selected object)
      link_color = LINK_OTHER_COLOR;
      // divide opacity by 2
      link_opacity *= 0.5;
    }
  }

  // line/polygone stroke width
  var stroke_width = LINK_WIDTH;
  var fill_color = null;

  // compute horizontal and vertical positions (x1,x2-->horizontal tree, y1,y2-->vertical tree)
  var
      x1 = (object_data.boundaries.x + object_data.boundaries.width), // take in account box width
      x2 = (child_data.boundaries.x),
      y1 = (object_data.boundaries.y + object_data.boundaries.height), // take in account box height
      y2 = (child_data.boundaries.y);

  // compute paths
  if (PROPORTIONAL_WIDTH_LINKS) {
    // we will use a filled polygone, no line needed
    stroke_width = 0;
    fill_color = link_color;
    // compute parent-to-child width
    var real_parent_link_width;
    if (VERTICAL_TREE) {
      real_parent_link_width = object_data.boundaries.width * child_data.common[object_data.id].count / object_data.count;
    }
    else {
      real_parent_link_width = object_data.boundaries.height * child_data.common[object_data.id].count / object_data.count;
    }

    var parent_link_width = real_parent_link_width;

    if ((1 > parent_link_width) || (isNaN(parent_link_width))) {
      // minimal width
      parent_link_width = 1;
    }
    
    // compute child-to-parent width
    var real_child_link_width;
    if (VERTICAL_TREE) {
      real_child_link_width = child_data.boundaries.width * child_data.common[object_data.id].count / child_data.count;
    }
    else {
      real_child_link_width = child_data.boundaries.height * child_data.common[object_data.id].count / child_data.count;
    }
    var child_link_width = real_child_link_width;
    if ((1 > child_link_width) || (isNaN(child_link_width))) {
      // minimal width
      child_link_width = 1;
    }
    
    if (VERTICAL_TREE) {
      // Vertical tree
      // init parent link position
      if (!object_data.child_link_left) {
          object_data.child_link_left = object_data.boundaries.x + (BOX_HORIZONTAL_SPACE / 2);
      }
      
      // init child link position
      if (!child_data.parent_link_left) {
          child_data.parent_link_left = child_data.boundaries.x + (BOX_HORIZONTAL_SPACE / 2);
      }
      
      if (STRAIGHT_LINKS) {
        // draw straight lines
        link_path = 'M' // move to x, y
                + object_data.child_link_left
                + ','
                + y1
                + 'L' // line to x, y
                + (object_data.child_link_left + parent_link_width) // center link + half link width
                + ','
                + y1
                + 'L'
                + (child_data.parent_link_left + child_link_width)
                + ','
                + y2
                + 'L'
                + child_data.parent_link_left
                + ','
                + y2
                + 'Z' // close path
        ;
      }
      else {
        // draw curved lines using vectors of length 1/3 of line height
        link_path = 'M' // move to x, y
                + (object_data.child_link_left + parent_link_width)
                + ','
                + y1
                + ' C'
                + (object_data.child_link_left + parent_link_width) // center link + half link width
                + ','
                + (y1 + BOX_VERTICAL_SPACE/3)
                + ' '
                + (child_data.parent_link_left + child_link_width)
                + ','
                + (y2 - BOX_VERTICAL_SPACE/3)
                + ' '
                + (child_data.parent_link_left + child_link_width)
                + ','
                + y2
                + ' L'
                + child_data.parent_link_left
                + ','
                + y2
                + ' C'
                + child_data.parent_link_left
                + ','
                + (y2 - BOX_VERTICAL_SPACE/3)
                + ' '
                + object_data.child_link_left
                + ','
                + (y1 + BOX_VERTICAL_SPACE/3)
                + ' '
                + object_data.child_link_left
                + ','
                + y1
                + 'Z' // close path
        ;
      }
      
      // update link position for next links
      object_data.child_link_left += real_parent_link_width;
      child_data.parent_link_left += real_child_link_width;
    }
    else {
      // Horizontal tree
      // init parent link position
      if (!object_data.child_link_top) {
          object_data.child_link_top = object_data.boundaries.y + (BOX_VERTICAL_SPACE / 2.);
      }
      
      // init child link position
      if (!child_data.parent_link_top) {
          child_data.parent_link_top = child_data.boundaries.y + (BOX_VERTICAL_SPACE / 2.);
      }
      
      if (STRAIGHT_LINKS) {
        // draw straight lines
        link_path = 'M' // move to x, y
                + x1
                + ','
                + object_data.child_link_top
                + 'L' // line to x, y
                + x1
                + ','
                + (object_data.child_link_top + parent_link_width)
                + 'L'
                + x2
                + ','
                + (child_data.parent_link_top + child_link_width)
                + 'L'
                + x2
                + ','
                + child_data.parent_link_top
                + 'Z' // close path
        ;
      }
      else {
        // we won't draw a polygon, use lines
        stroke_width = LINK_WIDTH;
        // draw curved lines using vectors of length 1/3 of line height
        link_path = 'M' // move to x, y
                + x1
                + ','
                + (object_data.child_link_top + parent_link_width)
                + ' C'
                + (x1 + BOX_HORIZONTAL_SPACE/3.)
                + ','
                + (object_data.child_link_top + parent_link_width)
                + ' '
                + (x2 - BOX_HORIZONTAL_SPACE/3.)
                + ','
                + (child_data.parent_link_top + child_link_width)
                + ' '
                + x2
                + ','
                + (child_data.parent_link_top + child_link_width)
                + ' L'
                + x2
                + ','
                + child_data.parent_link_top
                + ' C'
                + (x2 - BOX_HORIZONTAL_SPACE/3.)
                + ','
                + child_data.parent_link_top
                + ' '
                + (x1 + BOX_HORIZONTAL_SPACE/3.)
                + ','
                + object_data.child_link_top
                + ' '
                + x1
                + ','
                + object_data.child_link_top
                + 'Z' // close path
        ;
      }
      
      // update link position for next links
      object_data.child_link_top += real_parent_link_width;
      child_data.parent_link_top += real_child_link_width;
    }
  }
  else {
    if (VERTICAL_TREE) {
      // Vertical tree
      if (STRAIGHT_LINKS) {
        // draw straight lines
        link_path = 'M' // move to x, y
                + (object_data.boundaries.x + (object_data.boundaries.width / 2.) + (BOX_HORIZONTAL_SPACE / 2.))
                + ','
                + y1
                + 'L'
                + (child_data.boundaries.x + (child_data.boundaries.width / 2.) + (BOX_HORIZONTAL_SPACE / 2.))
                + ','
                + y2
        ;
      }
      else {
        // draw curved lines using vectors of length 1/3 of line height
        link_path = 'M' // move to x, y
                + (object_data.boundaries.x + (object_data.boundaries.width / 2.) + (BOX_HORIZONTAL_SPACE / 2.))
                + ','
                + y1
                + ' C'
                + (object_data.boundaries.x + (object_data.boundaries.width / 2.) + (BOX_HORIZONTAL_SPACE / 2.))
                + ','
                + (y1 + BOX_VERTICAL_SPACE/3)
                + ' '
                + (child_data.boundaries.x + (child_data.boundaries.width / 2.) + (BOX_HORIZONTAL_SPACE / 2.))
                + ','
                + (y2 - BOX_VERTICAL_SPACE/3)
                + ' '
                + (child_data.boundaries.x + (child_data.boundaries.width / 2.) + (BOX_HORIZONTAL_SPACE / 2.))
                + ','
                + y2
        ;
      }
    }
    else {
      // Horizontal tree
      if (STRAIGHT_LINKS) {
        // draw straight lines
        link_path = 'M' // move to x, y
                + x1
                + ','
                + (object_data.boundaries.y + (object_data.boundaries.height / 2.) + (BOX_VERTICAL_SPACE / 2.))
                + 'L'
                + x2
                + ','
                + (child_data.boundaries.y + (child_data.boundaries.height / 2.) + (BOX_VERTICAL_SPACE / 2.))
        ;
      }
      else {
        // draw curved lines using vectors of length 1/3 of line height
        link_path = 'M' // move to x, y
                + x1
                + ','
                + (object_data.boundaries.y + (object_data.boundaries.height / 2.) + (BOX_VERTICAL_SPACE / 2.))
                + ' C'
                + (x1 + BOX_HORIZONTAL_SPACE / 3.)
                + ','
                + (object_data.boundaries.y + (object_data.boundaries.height / 2.) + (BOX_VERTICAL_SPACE / 2.))
                + ' '
                + (x2 - BOX_HORIZONTAL_SPACE / 3.)
                + ','
                + (child_data.boundaries.y + (child_data.boundaries.height / 2.) + (BOX_VERTICAL_SPACE / 2.))
                + ' '
                + x2
                + ','
                + (child_data.boundaries.y + (child_data.boundaries.height / 2.) + (BOX_VERTICAL_SPACE / 2.))
        ;
      }
    }
  }


  var link = paper
    .path(link_path)
    .attr({
      'fill'          : fill_color,
      'stroke'        : link_color,
      'stroke-width'  : stroke_width,
      'stroke-linecap': 'round',
      'opacity': link_opacity
    })
    .toBack()
  ;
  
  if (!object_data.svg_child_connectors) {
    object_data.svg_child_connectors = [];
  }
  object_data.svg_child_connectors.push(link);
  if (!child_data.svg_parent_connectors) {
    child_data.svg_parent_connectors = [];
  }
  child_data.svg_parent_connectors.push(link);
  
  return link;
}


/**
* DrawDefaultConnectors
************************
Draw default connectors between object boxes.

Parameters: 
* paper: Raphael paper to draw on
* object_data: object data including top-left coordinate of current object box
* children_data: children object data including top-left coordinate of boxes

*/
function DrawDefaultConnectors(paper, object_data, children_data) {

  // process each child
  $.each(children_data, function(i, child_data) {
    DrawDefaultConnector(paper, object_data, child_data);
  });
}


/**
* DrawConnectors
*****************
Draw connectors between object boxes.

Parameters: 
* paper: Raphael paper to draw on
* object_data: object data including top-left coordinate of current object box
* children_data: children object data including top-left coordinate of boxes

*/
function DrawConnectors(paper, object_data, children_data) {

  // check if we got a built-in function to draw object connectors
  if (object_data.type && g_draw_connectors_handlers[object_data.type]) {
    // a function has been set for this type
    return g_draw_connectors_handlers[object_data.type](paper, object_data, children_data);
  }
  else {
    DrawDefaultConnectors(paper, object_data, children_data);
  }

}


/**
* DrawDefaultBox
*****************
Draws a object box.

Parameters:
* paper: Raphael paper to draw on
* left, top: top-left coordinate of the area to draw in
* object_data: object containing object data

*/
function DrawDefaultBox(paper, left, top, object_data) {

  // get color
  var background_color = BOX_BACKGROUND;
  var label_color = BOX_LABEL_COLOR;
  var object_html = $(object_data.selector).first();
  var stroke_color = BOX_BORDER;
  var stroke_dasharray = '';
  var opacity = 1;
  if (object_html.exists()) {
    background_color = object_html.css('background-color') || BOX_BACKGROUND;
    if ('transparent' == background_color) {
      background_color = $('#page').css('background-color') || '#ffffff';
    }
    label_color = object_html.css('color') || BOX_LABEL_COLOR;
  }
  else {
    stroke_dasharray = '-';
    opacity = 0.5;
  }

  // prepare to store SVG object set
  object_data.svg_set = paper.set();

  // draw label if one
  var object_label;
  var box_width = BOX_DEFAULT_WIDTH;
  var box_height = BOX_DEFAULT_HEIGHT;
  if ((null != object_data.label)
      && (object_data.label.length)) {
    object_label = paper
      .text(
        left,
        top,
        object_data.label
      )
      .attr({
        'fill': label_color,
        'opacity': opacity,
        'text-anchor': 'start',
        'font-size': FONT_SIZE
      })
      /*
        Note: (object_label.getBBox().height / 2.)
        text is by default vertically centered and we want the coordinate of the
        bottom of the text. Therefore, we translate the text by its half height.
      */
      /*
        Note: lower case letters indicates relative transformation while capital
        letters indicate absolute transformation. As we rotate by 90°, we don't
        want our X coordinates to become the Y so we use absolute translation
        that does not take in account the rotation.
      */
    ;
    if (VERTICAL_LABELS) {
      object_label.transform('r90,' + left + ',' + top + 'T' + (BOX_HORIZONTAL_SPACE/2. + (object_label.getBBox().height / 2.) + TEXT_PADDING) + ',' + (BOX_VERTICAL_SPACE/2. + TEXT_PADDING));
    }
    else {
      object_label.transform('T' + (BOX_HORIZONTAL_SPACE/2. + TEXT_PADDING) + ',' + (BOX_VERTICAL_SPACE/2. + (object_label.getBBox().height / 2.) + TEXT_PADDING));
    }
    box_width = object_label.getBBox().width + 2*TEXT_PADDING;
    box_height = object_label.getBBox().height + 2*TEXT_PADDING;
    object_data.svg_set.push(object_label);
  }
  
  // check for fixed height or width
  if (object_data.fixed_height) {
    box_height = object_data.fixed_height;
  }
  if (object_data.fixed_width) {
    box_width = object_data.fixed_width;
  }

  // draw box background
  var object_box = paper
    .rect(
      left,
      top,
      box_width,
      box_height
    )
    .transform('t' + (BOX_HORIZONTAL_SPACE/2.) + ',' + (BOX_VERTICAL_SPACE/2.))
    .attr({
      'stroke': stroke_color,
      'stroke-dasharray': stroke_dasharray,
      'stroke-width': STROKE_WIDTH,
      'fill': background_color,
      'opacity': opacity
    })
  ;

  object_data.svg_set.push(object_box);

  object_data.svg_box = object_box;
  object_data.svg_label = object_label;
  
  // if there is a label, put it above the box
  if (object_label) {
    object_label.toFront();
  }

  // save and return boundaries
  return object_data.boundaries = object_box.getBBox();
}


/**
* DrawBox
**********
Draws an object box.

Parameters:
* paper: Raphael paper to draw on
* left, top: top-left coordinate of the area to draw in
* object_data: object containing object data

Returns: the box dimensions in hash with keys 'height' and 'width'.

*/
function DrawBox(paper, left, top, object_data) {

  // check if we got a built-in function to draw object box
  if (object_data.type && g_draw_box_handlers[object_data.type]) {
    // a function has been set for this type
    return g_draw_box_handlers[object_data.type](paper, left, top, object_data);
  }
  else {
    // otherwise, use default box
    return DrawDefaultBox(paper, left, top, object_data);
  }
}


/**
* DrawTree
***********
Draws an object tree and returns the height and width of the area used to draw
the relationship graph subtree (including spaces).

Parameters:
* paper: Raphael paper to draw on
* left, top: top-left coordinate of the area to draw in
* object_data: object containing object relationship data (hash)

*/
function DrawTree(paper, left, top, object_data) {
  // container object for drawing dimensions
  var dimensions;
  // container array for children data
  var children_data = [];

  // draw root object box
  DrawBox(paper, left, top, object_data);
  g_processed_objects[object_data.id] = object_data;

  var current_left, current_top, current_height = 0, current_width = 0;

  if (VERTICAL_TREE) {
    // left position of current next free area
    current_left = left;
    // children top will be below current object box
    current_top = top + object_data.boundaries.height + BOX_VERTICAL_SPACE;
  }
  else {
    // children left will be after current object box
    current_left = left + object_data.boundaries.width + BOX_HORIZONTAL_SPACE;
    // top position of current next free area
    current_top = top;
  }

  // draw object subtree
  $.each(object_data.children, function(i, sub_object_data) {
    // make sure we got data to draw
    if (sub_object_data) {
      // check if child object has already been drawn
      if (g_processed_objects[sub_object_data.id]) {
        // already processed
        // just merge common data with instance already met
        g_processed_objects[sub_object_data.id].common[object_data.id] = sub_object_data.common[object_data.id];
        // replace given object by the processed one (remove duplicate)
        object_data.children[i] = g_processed_objects[sub_object_data.id];
        // add parent
        g_processed_objects[sub_object_data.id].parents.push(object_data);
        // and keep track of link to draw
        children_data.push(g_processed_objects[sub_object_data.id]);
      }
      else {
        // not drawn, draw it!
        var child_dimensions = DrawTree(paper, current_left, current_top, sub_object_data);
        
        sub_object_data.parents = [object_data];

        // keep track of link to draw
        children_data.push(g_processed_objects[sub_object_data.id]);

        // take child dimensions in account to enlarge drawn area
        if (VERTICAL_TREE) {
          current_left += child_dimensions.width;
          if (current_height < child_dimensions.height) {
              current_height = child_dimensions.height;
          }
        }
        else {
          current_top += child_dimensions.height;
          if (current_width < child_dimensions.width) {
              current_width = child_dimensions.width;
          }
        }
      }
    }
  });

  if (VERTICAL_TREE) {
    // Vertical tree
    // compute current width
    if (current_left == left) {
      // no child, we used at least current object box width as minimal width
      current_width = object_data.boundaries.width + BOX_HORIZONTAL_SPACE;
    }
    else {
      // we used children width as width
      current_width = (current_left - left);
    }

    // move object box to new position (center amonst its children)
    if (object_data.children.length) {
      var box_width = object_data.fixed_width;
      if (!box_width) {
        box_width = BOX_DEFAULT_WIDTH;
      }
      object_data.boundaries.x = left + (current_width / 2.) - ((box_width + BOX_HORIZONTAL_SPACE) / 2.);
      object_data.svg_set.transform('...T' + ((current_width / 2.) - ((box_width + BOX_HORIZONTAL_SPACE) / 2.)) + ',0');
    }
    else {
      object_data.boundaries.x = left;
    }

    // re-order children list
    children_data = children_data.sort(function(a, b) {return a.boundaries.x - b.boundaries.x;});
  }
  else {
    // Horizontal tree
    // compute current height
    if (current_top == top) {
      // no child, we used at least current object box width as minimal width
      current_height = object_data.boundaries.height + BOX_VERTICAL_SPACE;
    }
    else {
      // we used children height as height
      current_height = (current_top - top);
    }

    // move object box to new position (center amonst its children)
    object_data.boundaries.y = top;

    // re-order children list
    children_data = children_data.sort(function(a, b) {return a.boundaries.y - b.boundaries.y;});
  }

  // draw connectors
  DrawConnectors(paper, object_data, children_data);

  if (VERTICAL_TREE) {
    current_height += object_data.boundaries.height + BOX_VERTICAL_SPACE;
  }
  else {
    current_width += object_data.boundaries.width + BOX_HORIZONTAL_SPACE;
  }
  return {
    'height': current_height,
    'width':  current_width
  };
}


/**
* DrawRelationship
*******************
Draws a relationship graph.

Parameters:
* canvas: the jQuery object that will hold the space of the canvas.
* relationship: a structure containing the relationship data.
  ex.: 
  var relationship = {
    "id"      : 1234, // object unique identifier
    "label"   : "GP001234", // object label
    "type"    : "family-sequences", // object type
    "count"   : "4", // number of elements in the object
    "common"  : {}, // number of common elements with parent objects
    "children": [ // children data
      {
        "common"  : {
          1234: {
            'count': "3" // common element with parent ID 1234
          }
        },
        "children": [],
        "count"   : "3",
        "label"   : "GP005678",
        "id"      : 5678
      },
    ],
    "selector": ".family-GP001234" // CSS selector for associated HTML elements
  };

*/
function DrawRelationship(canvas, relationship) {

  // check if visible and if not, postpone drawing
  if (!canvas.exists() || !canvas.is(':visible')) {
    // the "eval" part determines the function reference using the function name
    // computed in the string. 
    // We assume that the target canvas ID has the form:
    // "#canvas_<some_object_identifier>" and the function
    // "DrawRelationship<some_object_identifier>" exists.
    g_relationship_notloaded_stack.push(eval('DrawRelationship' + canvas.selector.substr(canvas.selector.indexOf('canvas_')+7)));
    return;
  }

  // by default use 100% width
  var canvas_width = canvas.width();
  // if the canvas has no specified height, it will be adjusted later
  // note: changing the content of the canvas object will affect its height
  // so we need to store the original value somewhere before changing its
  // content!
  var canvas_height = canvas.height();

  // create Raphael paper object for drawing  and link it to the given canvas
  var paper = Raphael(canvas.prop('id'), canvas_width, canvas_height);

  // for zooming and panning
  var zpd = new RaphaelZPD(paper, { zoom: true, pan: true, drag: false, zoomThreshold: [ZOOM_MIN, ZOOM_MAX] });

  // make sure we got a relationship structure
  if (relationship) {
    try {
      // reset processed object hash
      g_processed_objects = {};
      var dimensions = {'width': 0, 'height': 0};
      // vertical tree: current position represent current left position
      // horizontal tree: current position represent current top position
      var initial_position = (VERTICAL_TREE ?
        (TREE_HORIZONTAL_PADDING  - (BOX_HORIZONTAL_SPACE / 2.))
        : (TREE_VERTICAL_PADDING - (BOX_VERTICAL_SPACE / 2.)))
      ;
      var current_position = initial_position;
      g_max_elements = 1;
      
      // get max number of elements:
      // loop on root elements and find the one with the maximum amount of
      // elements. This will setup the maximum opacity to have a global overview
      // of the elements flow (while link width give a local view of the flow
      // between a parent and its children).
      $.each(relationship, function(i, single_relationship){
        if (single_relationship) {
          if (g_max_elements < single_relationship.count) {
            g_max_elements = single_relationship.count;
          }
        }
      });
      
      // draw trees
      $.each(relationship, function(i, single_relationship){
        if (single_relationship) {
          // draw the tree and get dimensions in order to resize the canvas area
          if (VERTICAL_TREE) {
            // Vertical tree
            var tree_dimensions = DrawTree(paper, current_position, (TREE_VERTICAL_PADDING - (BOX_VERTICAL_SPACE / 2.)), single_relationship);
            current_position += tree_dimensions.width;
            dimensions.width += tree_dimensions.width;
            if (dimensions.height < tree_dimensions.height) {
              dimensions.height = tree_dimensions.height;
            }
          }
          else {
            // Horizontal tree
            var tree_dimensions = DrawTree(paper, (TREE_HORIZONTAL_PADDING - (BOX_HORIZONTAL_SPACE / 2.)), current_position, single_relationship);
            current_position += tree_dimensions.height;
            dimensions.height += tree_dimensions.height;
            if (dimensions.width < tree_dimensions.width) {
              dimensions.width = tree_dimensions.width;
            }
          }
        }
      });
      
      dimensions.width += 2*TREE_HORIZONTAL_PADDING - BOX_HORIZONTAL_SPACE;
      dimensions.height += 2*TREE_VERTICAL_PADDING - BOX_VERTICAL_SPACE;
      // // draw borders
      // paper.rect(
      //   TREE_HORIZONTAL_PADDING,
      //   0,
      //   dimensions.width,
      //   dimensions.height
      //   )
      //   .attr({
      //     'stroke': '#000000',
      //     'stroke-width': 1
      //   })
      // ;


      // resize canvas as needed
      canvas.css('padding', '0');
      if (!canvas_height) {
        canvas_height = dimensions.height
        canvas.height(dimensions.height);
        paper.setSize(canvas.width(), dimensions.height);
      }
      // check if drawing area is smaller than canvas
      if ((dimensions.width < canvas_width)
          || (dimensions.height < canvas_height)) {
        if (zpd && zpd.opts) {
        // disable zooming and panning
          zpd.opts.zoom = false;
          zpd.opts.pan  = false;
        }
        canvas.width(dimensions.width);
        // also resize paper in order to be able to center it
        paper.setSize(dimensions.width, dimensions.height);
      }
      else {
        // compute default zoom for larger graph
        // zoomWithDeltaAndPosOnSvgDoc
        //*****************************
        // This method is not documented but it can be used to change the SVG
        // view scale.
        // scale_delta: the scale delta value is positive for zooming in
        //   and negative for zooming out. For instance, to divide the graph
        //   size by 3, you have to compute the new scale first and then
        //   substract the original scale from that value to have a relative 
        //   scale: scale_delta = 1/3 - 1
        //     "1" means "actual scale"
        //     "/3" means "divide actuel scale by 3"
        //     "-1" means "we want the delta between actual scale and the wanted
        //     scale so substract current scale which is 1"
        //   To double the size of a graph, we want 2 times actual size,
        //   which means a scale_delta of 1 (= 2 - 1).
        //   Leaving scale_delta to a value of 0 will not change the scale.
        // zoom_coordinates: coordinates of the point that should be the center
        //   of the zoom event (on the canvas object that owns the SVG).
        // document: current DOM document where the SVG object is embeded.
        var scale_delta = (canvas_width / dimensions.width) - 1;
        // we don't want to zoom out too much, so check the limit
        if (scale_delta < (ZOOM_MIN - 1)) {
          scale_delta = ZOOM_MIN - 1;
        }

        // centers the zoom out
        // -0.5*dimensions.width*(scale_delta+1):
        //   -: offset to the left
        //   0.5: divide by 2 (middle position)
        //   *dimensions.width: original graph size
        //   *(scale_delta+1): new scale used (that may have been cut off)
        var zoom_coordinates = {
          'x': 0, // -0.5*dimensions.width*(scale_delta+1),
          'y': canvas_height/2.
          };

          zpd.zoomWithDeltaAndPosOnSvgDoc(scale_delta, zoom_coordinates, document);
      }


      var svg_for_uri = escape(canvas.html());
      var svg_download_form = $('\
        <form id="relationship_svg_form" action="' + BASE_URL + '/svg.pl" enctype="multipart/form-data" method="post" style="display: none;">\n\
          <input type="hidden" name="content" value="' + svg_for_uri + '"/>\n\
          <input type="hidden" name="file_name" value="' + canvas.selector.substr(canvas.selector.indexOf('canvas_')+7) + '_relationship.svg"/>\n\
        </form>');
      $('body').append(svg_download_form);
      var download_button = $('<div class="dump-links"><span class="header">Download:</span>[<input type="button" value="SVG" class="dump-link" onclick="$(\'#relationship_svg_form\').submit();"/>]</div>');
      download_button.insertAfter(canvas.parent());
    }
    catch(error) {
      // an error occurred
      alert('Warning: an error occurred while drawing relationship graph (' + error + ').\n');
      if (DEBUG) {
        throw error;
      }
    }
  }
}


/**
* LoadRelationshipWithDelay
****************************
Loads relationship graph in a delayed way so the browser won't freeze because
of a two heavy number of function calls to proceed.

*/
function LoadRelationshipWithDelay() {
  // check if we have something to load
  if (g_relationship_load_stack.length) {
    // get next relationship graph to load (ie. the function to call)
    var l_LoadGraph = g_relationship_load_stack.shift();
    // launch the function that loads the relationship graph
    l_LoadGraph();
    // get to the next one to be loaded (if one)
    if (g_relationship_load_stack.length) {
      window.setTimeout(LoadRelationshipWithDelay, RELATIONSHIP_INTER_LOADING_DELAY);
    }
  }
}


/**
* ReprocessRelationshipDrawingStack
************************************
Put back into drawing queue the unprocessed relationship graph.

*/
function ReprocessRelationshipDrawingStack() {
    g_relationship_load_stack = g_relationship_load_stack.concat(g_relationship_notloaded_stack);
    g_relationship_notloaded_stack = new Array();
    LoadRelationshipWithDelay();
}

// call LoadRelationshipWithDelay when document is ready
//+Val: without greenphyl.js: $(LoadRelationshipWithDelay);
//+Val: using greenphyl.js
g_init_functions.push(LoadRelationshipWithDelay);

// if within tabs or paged tables, reload...
$(function() {
  // 1) when we use a tabs, we can't draw relationship graph on hidden tabs
  // therefore these relationship graph should be processed once the tab
  // changes. To do so, we add a listener on tab changes
  $('.tabs-container')
    .on(
      'tabsactivate',
      ReprocessRelationshipDrawingStack
    )
  ;

  // 2) when we use a paged sortable table, we can't draw relationship graph on
  // hidden pages therefore these relationship graph should be processed once
  // the table page changes. To do so, we add a listener on page changes
  $('table.sortable, table.paged')
    .on(
      'sortEnd applyWidgets',
      ReprocessRelationshipDrawingStack
    )
  ;
});
