/**
********************************************************************************
*******************************************************
* GreenPhyl Javascript Library - Homepage Tree module *
*******************************************************
Version:  3.1.0
Contacts: Mathieu Rouard <m.rouard@cgiar.org>
          Valentin Guignon <v.guignon@cgiar.org>
Requires: jQuery >=1.9, jQueryUI >=1.10

Description:
This file contains the Javascript code used on GreenPhyl website to draw homepage tree.

Copyright 2011, Bioversity International - CIRAD

********************************************************************************
*/

/**
********************************************************************************
* GLOBAL CONSTANTS
********************************************************************************
*/
var SPECIES_DATA_FETCH_URL      = BASE_URL + '/get_species_data.cgi?mode=inner&mmode=' + GetURLParameter('mmode') + '&species=';
var SPECIES_MISSING_IMG_URL     = IMG_URL + '/species_no_picture.png';
var g_species_popup_height      = 200;
var g_species_popup_width       = 256;
var g_species_popup_middle      = 0; // computed
var g_species_popup_top         = 0; // computed
var g_species_popup_img_radius  = 64;
var g_species_data_loading      = false;
var g_species_tree              = null;
var g_render_width              = 960;
var g_branch_label_margin       = 8; // horizontal only
var g_compatibility_mode        = false;
var g_species_to_load           = new Array();
var g_species_data_cache        = new Array();
var g_hide_species_data_timeout = 0; // leave it set to 0
var g_species_to_show           = ''; // leave it empty


function placeBranch(branch, div_cell, span_label, ul_next_element) {
    var branch_left;
    var branch_width;

    // set connector width and horizontal position
    if (div_cell.is('.leaf')) {
      // for leaves, end connector to the begining of the label
      branch_left = parseInt(div_cell.offset().left);
      branch.css('left', branch_left + 'px');
      branch_width = parseInt(span_label.offset().left) - branch_left;
      branch.css('width', branch_width + 'px');
    }
    else {
      // internal nodes, the connector passes through the label
      if (g_compatibility_mode) {
        // fixes label z-index not supported by IE: clone the element and position it after branches so it'll be on top
        var span_label_ie = span_label.clone().appendTo(g_species_tree);
        span_label_ie
          .css({
            'position': 'absolute',
            'top':      parseInt(span_label.position().top) + 'px',
            'left':     parseInt(span_label.position().left) + 'px'
          })
          .addClass('ie-node-label')
        ;
        // hide old label
        span_label.css('visibility', 'hidden');
      }
      else {
        span_label
          .css({
            'position': 'relative',
            'top':      ((parseInt(div_cell.height()) / 2) - (parseInt(span_label.height())/2)) + 'px'
          });
        // span_label.css('visibility', 'hidden'); //+debug
      }

      // get position of the element next to the label where the connector should end
      if (ul_next_element.length) {
        branch_left = parseInt(div_cell.offset().left);
        branch.css('left', branch_left + 'px');
        branch_width = parseInt(ul_next_element.offset().left) - branch_left;
        branch.css('width', branch_width + 'px');
      }
    }

    // compute vertical position
    if (div_cell.is('.first-element')) {
      // link to the top element of a sub-tree
      branch
        .addClass('first-element')
        .css({
          'top':           (parseInt(div_cell.offset().top) + parseInt(div_cell.height()) / 2) + 'px',
          'height':        (parseInt(div_cell.height()) / 2 + 1) + 'px',
          'border-bottom': '0',
          'border-right':  '0'
        })
      ;
    }
    else if (div_cell.is('.middle-element')) {
      // middle elements require an additionnal div element:
      // -one for the horizontal bar in the middle (provided before)
      // -one for the verical junction between siblings (above and below)
      branch
        .addClass('middle-element-horizontal')
        .css({
          'border-bottom': '0',
          'width':         (branch_width - parseInt(branch.css('border-left-width'))) + 'px',
          'top':           (parseInt(div_cell.offset().top) + parseInt(div_cell.height()) / 2) + 'px',
          'border-right':  '0'
        })
      ;
      var vertical_branch = $('<span class="branch middle-element-vertical"></span>').insertBefore(branch);
      var branch_top = parseInt(div_cell.offset().top);
      var branch_height = parseInt(div_cell.height());
      vertical_branch
        .css({
          'top':           branch_top + 'px',
          'left':          branch_left + 'px',
          'height':        branch_height + 'px',
          'border-top':    '1',
          'border-top-color': 'transparent',
          'border-bottom': '0',
          'border-right':  '0'
        })
      ;
    }
    else if (div_cell.is('.single-element')) {
      branch
        .addClass('single-element')
        .css({
          'border-bottom': '0',
          'width': (branch_width - parseInt(branch.css('border-left-width'))) + 'px',
          'top': parseInt(span_label.offset().top) + (parseInt(span_label.outerHeight()) / 2) + 'px'
        })
      ;
    }
    else if (div_cell.is('.last-element')) {
      var branch_top = parseInt(div_cell.offset().top);
      branch.css('top', branch_top + 'px');
      var branch_height = parseInt(div_cell.height()) / 2;
      branch
        .addClass('last-element')
        .css({
          'height': branch_height + 'px',
          'border-top': '0',
          'border-right': '0'
        })
      ;
    }
}


function fetchSpeciesData() {
  // check if there are species to load in the queue
  // g_species_data_loading tells us if we arleady loading something to prevent
  // concurrent loading
  while (!g_species_data_loading && g_species_to_load.length) {
    // get the next one
    var species_name = g_species_to_load.pop();
    var species_name_id = species_name.replace(/ /g, '%20');
    // check if species is not already loaded
    if (!g_species_data_cache[species_name]) {
      // species not loaded, start loading
      g_species_data_loading = true;
      // fetch data and fill cache
      $.get(SPECIES_DATA_FETCH_URL + species_name_id + '&picture=no',
        function(data) {
          g_species_data_cache[species_name] = data;
          g_species_data_loading = false; // done loading
          // get next one
          fetchSpeciesData();
          // show species
          if (g_species_to_show) {
            showSpeciesData(g_species_to_show);
          }
        })
      .error(
        function() {
          // failed to load species data
          g_species_data_cache[species_name] = '<div class="error">ERROR: Failed to load species data. You may try again by reloading the page.</div>';
          g_species_data_loading = false; // done loading
          // get next one
          fetchSpeciesData();
        })
      ;

      $.get(SPECIES_DATA_FETCH_URL + species_name_id + '&picture=only',
        function(data) {
          // add picture to hidden DOM object so the browser will load the image in its cache
          $('#tree_cache').append(data);
        })
      ;
    }
  }
}


function loadSpeciesData(species_name) {
  // check if species is already loaded
  if (species_name && !g_species_data_cache[species_name]) {
    // not loaded, add it to the queue on top
    g_species_to_load.push(species_name);
    // and fetch species
    fetchSpeciesData();
  }
}

function showSpeciesPopup(species_name, html) {

  var species_popup = $('#species_popup');

  // clear hide timeout
  if (g_hide_species_data_timeout) {
    clearTimeout(g_hide_species_data_timeout);
  }

  // if no species specified, do nothing else
  if (!species_name)
  {return;}

  // get popup content
  html = ((typeof(html) != 'undefined') ? html : 'Loading species data...');
  if (g_species_data_cache[species_name]) {
    html = g_species_data_cache[species_name];
  }
  // alert("DEBUG: " + html);
  species_popup.html(html);

  // display species image
  var cache_picture = $('#picture_' + species_name.toLowerCase().replace(/ /g, '_') + ' img');
  var species_picture = $('#species_picture');
  if (species_picture.exists()) {
    species_picture
      .css({
        'position': 'fixed',
        'top':      (g_species_popup_top - g_species_popup_img_radius) + 'px',
        'left':     (g_species_popup_middle - g_species_popup_img_radius + 24) + 'px'
      });
    if (cache_picture.exists()) {
      species_picture
        .error(function() {$('#species_picture').prop('src', SPECIES_MISSING_IMG_URL);})
        .prop('src', cache_picture.prop('src'))
        .show()
      ;
    }
    else {
      species_picture
        .prop('src', SPECIES_MISSING_IMG_URL)
        .show()
      ;
    }
  }

  // check if only the content should be refreshed or if the open animation should start
  if (!species_popup.is(':animated') && !species_popup.is(':visible')) {
    // display species popup
    species_popup
      .show()
      .css({
        'height':  '1px',
        'width':   '1px',
        'padding': '1px',
        'left':    g_species_popup_middle + 'px'
      })
      .animate({
          'opacity'    : '1',
          'width'      : g_species_popup_width + 'px',
          'left'       : '-=' + (g_species_popup_width/2) + 'px',
          'padding'    : '18px',
          'padding-top': (g_species_popup_img_radius + 9) + 'px'
        },
        100
      )
      .animate({
          'height': g_species_popup_height + 'px'
        },
        200
      )
    ;
  }
}


function showSpeciesData(species_name) {
  // check if species is already loaded
  if (!g_species_data_cache[species_name]) {
    // display empty species box
    showSpeciesPopup(species_name);
    // set "species loaded" handler
    g_species_to_show = species_name;
    // not loaded, call loadSpeciesData
    loadSpeciesData(species_name);
  }
  else {
    // species loaded, display content
    showSpeciesPopup(species_name);
    g_species_to_show = '';
  }
}

function delayHideSpeciesData(delay) {
  delay = ((typeof(delay) != 'undefined') ? delay : 1500);

  if (g_hide_species_data_timeout) {
    clearTimeout(g_hide_species_data_timeout);
  }
  g_hide_species_data_timeout = setTimeout("hideSpeciesData()", delay);
}


function hideSpeciesData() {
  $('#species_popup').fadeOut(300);
  $('#species_picture').fadeOut(300);
  // put species text color back to normal
  g_species_tree.find('.label').removeClass('species-name-highlight');
}


// buildAndInitTree()
// This function is called after page loading to compute and display the
// position of branches. It also initialize event handlers on labels to
// used to display species popup.
function buildAndInitTree() {

  // transform internal nodes into tooltips
  g_species_tree.find('.root-element .node').each(function() {
    var div_cell = $(this); // node (cell) container which contains node label and subtree
    var span_label = div_cell.children('.label').first(); // node label
    // check for list to table conversion (IE6-7)
    if (!span_label.length) {
        span_label = div_cell.find('table tr td .label').first(); // node label
    }
    if (!span_label.length) {
        span_label = div_cell.find('table tbody tr td .label').first(); // node label
    }
    if (!span_label.length) {
      // can't work without label
      return;
    }

    var tooltip_id = 'tree_node_' + span_label.text().replace(/\W/g, '_');

    g_species_tree.after('<span id="' + tooltip_id + '_tooltip" class="hidden-tooltip">' + span_label.text() + '</span>\n');

    span_label
      .prop('id', tooltip_id)
      .addClass('tooltiped')
      .text(span_label.text().substr(0,2) + '.')
    ;

  });
  // re-init tooltips
  initTooltips();

  // work on every node (or leaf) of the tree which can be considered as table cells
  g_species_tree.find('.root-element .node, .root-element .leaf').each(function() {
  
    // Build Tree
    //------------
    var div_cell = $(this); // node (cell) container which contains node label and subtree
    var span_label = div_cell.children('.label').first(); // node label
    // check for list to table conversion (IE6-7)
    if (!span_label.length) {
        span_label = div_cell.find('table tr td .label').first(); // node label
    }
    if (!span_label.length) {
        span_label = div_cell.find('table tbody tr td .label').first(); // node label
    }
    if (!span_label.length) {
      // can't work without label
      return;
    }
    
    // get sub-table container used to contain sub-tree
    // ul_next_element is used to compute branch ending position
    // a sub-table follows each branch label
    // for leaves, no next element will be found
    var ul_next_element = span_label.next();
    // no next element found, maybe we got the IE6-7 tree version?
    if (!ul_next_element.length) {
      ul_next_element = span_label.parent().next();
    }

    // create a branch connector
    var branch = $('<span class="branch"></span>').prependTo(g_species_tree);

    placeBranch(branch, div_cell, span_label, ul_next_element);

    
    // Init event handlers
    //---------------------
    
    // add species popup handlers and pre-load species pictures
    if (div_cell.is('.leaf')) {
      span_label
        .hover(
          function(event) {
            // put species text color back to normal
            g_species_tree.find('.label').removeClass('species-name-highlight');
            // highlight species text
            $(this).addClass('species-name-highlight');
            // show species popup
            showSpeciesData($(this).text());
          },
          function(event) {
            // species popup can be hidden soon
            delayHideSpeciesData();
          }
        )
        .css('cursor', 'pointer')
      ;

      // pre-load data
      loadSpeciesData(span_label.text());
    }
  });
  
  // Init Tree
  //-----------
  // make sure we got everything...
  if (!$('#tree_cache').exists()) {
    // add cache container
    g_species_tree.after('<div id="tree_cache">\n  <img src="' + SPECIES_MISSING_IMG_URL + '" alt="Picture not available"/>\n</div>');
  }
  if (!$('#species_picture').exists()) {
    // add cache container
    g_species_tree.after('<img id="species_picture src="' + SPECIES_MISSING_IMG_URL + '" alt="" class="species-picture" style="display: none;"/>');
  }
  $('#species_picture')
    .hover(
      function(event) {
        // show species popup
        showSpeciesData();
      },
      function(event) {
        // species popup can be hidden soon
        delayHideSpeciesData();
      }
    )
    .draggable({
      drag: function(event, ui) {
        g_species_popup_middle = ui.position.left + g_species_popup_img_radius - 24;
        g_species_popup_top = ui.position.top + g_species_popup_img_radius;
        $('#species_popup')
          .css({
            'top':      g_species_popup_top + 'px',
            'left':     (g_species_popup_middle - (g_species_popup_width/2)) + 'px'
          })
        ;
      }
    })
    .css('cursor', 'pointer')
  ;

  if (!$('#species_popup').exists()) {
    // add cache container
    g_species_tree.after('<div id="species_popup"></div>');
  }

}


// Resizing tree
function resizeTree() {
  $('.branch').each(function() {
    $(this).remove();
  });
  if (g_compatibility_mode) {
    // remove IE-specific elements
    $('.ie-node-label').each(function() {
      $(this).remove();
    });
  }
  // work on every node (or leaf) of the tree which can be considered as table cells
  g_species_tree.find('.root-element .node, .root-element .leaf').each(function() {
    var div_cell = $(this); // node container which contains node label and subtree
    var span_label = div_cell.find('.label').first(); // node label
    if (!span_label.length) {
      // can't work without label
      return;
    }
    var ul_next_element = span_label.next();
    // no next element found, maybe we got the IE6-7 tree version?
    if (!ul_next_element.length) {
      ul_next_element = span_label.parent().next();
    }

    // get branch connector
    // create a branch connector
    var branch = $('<span class="branch"></span>').prependTo(g_species_tree);

    placeBranch(branch, div_cell, span_label, ul_next_element);

  });

  // reposition species popup
  g_species_popup_top         = parseInt(g_species_tree.offset().top) + g_species_popup_img_radius;
  // g_species_popup_middle      = parseInt(g_species_tree.offset().left) + (parseInt(g_species_tree.outerWidth()) / 2) - 1;
  g_species_popup_middle      = (parseInt(g_species_tree.offset().left) + parseInt(g_species_tree.outerWidth()) / 2) - 1;
  $('#species_popup')
    .css({
      'top':  g_species_popup_top + 'px',
      'left': g_species_popup_middle + 'px'
    })
  ;
}

$(
  function() {

    // global object pointing to the tree main container
    g_species_tree = $('#species_tree');
    //alert('debug: ' + g_species_tree.text());
    
    // For disabled-jQuery compatibility: remove compatibility style
    g_species_tree.find('div, div.leaf .label').css('border', '0');

    // position of the species popup
    g_species_popup_top         = parseInt(g_species_tree.offset().top) + g_species_popup_img_radius;
    //g_species_popup_middle      = parseInt(g_species_tree.offset().left) + (parseInt(g_species_tree.outerWidth()) / 2) - 1;
    g_species_popup_middle      = (parseInt(g_species_tree.offset().left) + parseInt(g_species_tree.outerWidth()) / 2) - 1;


    // remove root label
    var root_label = g_species_tree.find('.root-element .label').first();
    // check for list to table conversion
    if (!root_label.length) {
      root_label = g_species_tree.find('.root-element table tr td .label').first(); // node label
    }
    if (!root_label.length) {
      root_label = g_species_tree.find('.root-element table tbody tr td .label').first(); // node label
    }
    if (root_label.length) {
      root_label.remove();
    }

    // center tree
    if (g_species_tree.find('ul, table').first().length) {
      g_species_tree.css('margin-left', ((parseInt(g_species_tree.parent().width()) - parseInt(g_species_tree.find('ul, table').first().width())) / 2) + 'px');
    }

    // add branches
    buildAndInitTree();

    $(window).resize(function(event){
      resizeTree();
    });

    // initialize species popup
    $('#species_popup')
      .hover(
        function(event) {
          showSpeciesPopup();
        },
        function(event) {
          delayHideSpeciesData();
        }
      )
      .css({
        'display':  'none',
        'position': 'fixed',
        'width':    g_species_popup_width + 'px',
        'height':   g_species_popup_height + 'px',
        'top':      g_species_popup_top + 'px',
        'left':     g_species_popup_middle + 'px'
      })
      .draggable({
        drag: function(event, ui) {
          g_species_popup_middle = ui.position.left + (g_species_popup_width/2);
          g_species_popup_top = ui.position.top;
          $('#species_picture')
            .css({
              'top':      (g_species_popup_top - g_species_popup_img_radius) + 'px',
              'left':     (g_species_popup_middle - g_species_popup_img_radius + 24) + 'px'
            })
          ;
        }
      })
    ;
  }
);
