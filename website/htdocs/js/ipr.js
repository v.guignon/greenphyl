/**
********************************************************************************
*********************************************
* GreenPhyl Javascript Library - IPR module *
*********************************************
Version:  3.1.0
Contacts: Mathieu Rouard <m.rouard@cgiar.org>
          Valentin Guignon <v.guignon@cgiar.org>
Requires: jQuery >=1.9, jQueryUI >=1.10, Raphael >= 2.0.1

Description:
This file contains the Javascript code used on GreenPhyl website to draw IPR
domains.

Copyright 2011, Bioversity International - CIRAD

********************************************************************************
*/

// delay between IPR loadings
var IPR_INTER_LOADING_DELAY = 10; // milliseconds

// stores assigned IPR colors
var g_ipr_colors = new Array();
// stack of IPR loading function (see "LoadIPRWithDelay" below)
var g_ipr_load_stack = new Array();
// stack for IPR that couldn't be drawn
var g_ipr_notloaded_stack = new Array();

/**
* GetAndSetIPRColor
********************
Select the next available Raphael color in order to have different colors for
each IPR.

Parameter:
* ipr_code: the code of the IPR that can be used to retrieve the CSS class used
  for HTML elements in order to share the same color between SVG and HTML
  elements for the same IPR.

*/
function GetAndSetIPRColor(ipr_code) {

  if (!g_ipr_colors[ipr_code]) {
    g_ipr_colors[ipr_code] = Raphael.getColor();
    // this could also be used for random colors:
    //g_ipr_colors[ipr_code] = '#'+(0x1000000+(Math.random())*0xffffff).toString(16).substr(1,6);
  }

  // apply the same IPR color to associated HTML elements (using CSS class)
  $('.ipr_code_' + ipr_code).css('color', g_ipr_colors[ipr_code]);

  // return used color
  return g_ipr_colors[ipr_code];
}


/**
* DrawIPRConnectors
********************
Draw connectors between sequence line and IPR blocks.

Parameters: 
* paper: the Raphael paper object to draw on;
* layer: the layer of the related IPR block;
* ipr_height: the height of an IPR block (in pixel units);
* left: the left position of the IPR block (in pixel units);
* right: the right position of the IPR block (in pixel units);

*/
function DrawIPRConnectors(paper, layer, ipr_height, left, right) {
  if (!layer) return 0;

  var step = ipr_height / 3;
  right = left + right;

  var height = (ipr_height * layer + ipr_height / 2);

  paper
    .path('M' + left + ' ' + height + 'L' + left + ' ' + (ipr_height / 2))
    .toBack()
  ;

  paper
    .path('M' + right + ' ' + height + 'L' + right + ' ' + (ipr_height / 2))
    .toBack()
  ;

  return 0;
}


/**
* ActivateIPR
**************
Called when the mouse moves over the SVG element representing an IPR domain.

*/
function ActivateIPR(event) {
  // find IPR class name and highlight IPR
  $('.ipr_code_' + $(this.node).prop('name').substr(4)).addClass('highlight-ipr');
  // show tooltip
  showTooltip.call(this.node, event); // we use .call() to for 'this' to not change
}


/**
* DeactivateIPR
****************
Called when the mouse leaves the SVG element representing an IPR domain.

*/
function DeactivateIPR(event) {
  // find IPR class name and set IPR label to normal
  $('.ipr_code_' + $(this.node).prop('name').substr(4)).removeClass('highlight-ipr');
  // hide tooltip
  hideTooltip.call(this.node, event); // we use .call() to for 'this' to not change
}


/**
* DrawIPRDomains
*****************
Draws the IPR domains of a sequence.

Parameters:
* ipr_canvas: the jQuery object that will hold the space of the canvas.

* current_sequence_length: the sequence length (in biological units).

* max_sequence_length: the length of the longuest sequence of the set (in
  biological units). This is used in order to have all the sequences share the
  same scale.

* ipr_height: the height of an IPR box on the canvas (in pixel units).

* layer_count: the number of layer in the IPR diagram.

* iprs: an array of IPR objects. An IPR object must provide the following
  members:
  * ipr_code: the name (code) of the IPR domain;
  * layer: the layer the IPR domain is on, on the diagram;
  * start: the start position of the domain (in biological units);
  * length: the length of the domain (in biological units).

*/
function DrawIPRDomains(ipr_canvas, current_sequence_length, max_sequence_length, ipr_height, layer_count, iprs) {

  // check if visible and if not, postpone drawing
  if (!ipr_canvas.exists() || !ipr_canvas.is(':visible')) {
    // the "eval" part determines the function reference using the function name
    // computed in the string. "ipr_domain.tt" template defines the function
    // 'DrawIPRCanvas' + the a sequence identifier which can be retrieved
    // from the ipr_canvas.selector (the 12 characters correspond to
    // '#ipr_canvas_' that we skip adding this offset)
    g_ipr_notloaded_stack.push(eval('DrawIPRCanvas' + ipr_canvas.selector.substr(12)));
    return;
  }
  
  var canvas_width = ipr_canvas.innerWidth();

  var canvas_height = 8 + (ipr_height * layer_count); // +8 for line borders that could be cut and also for space
  // make the div hold the same space as the SVG canvas
  ipr_canvas.height(canvas_height);

  // compute the canvas width (proportional to the longest sequence)
  // the longest sequence of the set will just fit 100% the table column (ipr_canvas.innerWidth())
  var sequence_width = (current_sequence_length / max_sequence_length * canvas_width);
  
  var canvas_top = ipr_canvas.offset().top;
  var canvas_left = ipr_canvas.offset().left;

  // create Raphael object for drawing  
  var paper = Raphael(ipr_canvas.prop('id'), canvas_width, canvas_height);

  // draw horizontal line
  paper.path('M0 ' + (ipr_height / 2) + 'L' + sequence_width + ' ' + (ipr_height / 2));

// draw starting and ending graduations
  paper.path('M0 ' + (ipr_height / 2 - 5) + 'L0 ' + (5 + ipr_height / 2));
  paper.path('M' + sequence_width + ' ' + (ipr_height / 2 - 5) + 'L' + sequence_width + ' ' + (5 + ipr_height / 2));
  // draw intermediate graduations
  var graduation_x = 1;
  while (graduation_x <= current_sequence_length) {
    var x = (graduation_x * sequence_width / current_sequence_length);
    paper.path('M' + x + ' ' + (ipr_height / 2) + 'L' + x + ' ' + (3 + ipr_height / 2));
    graduation_x += 50;
  }

  $.each(iprs, function(i, ipr_data) {
    // skip (last) null element
    if (!ipr_data) {return;}
    // first, draw all the vertical connectors from the horizontal line to each IPR domain
    DrawIPRConnectors(
      paper,
      ipr_data.layer,
      ipr_height,
      (ipr_data.start*sequence_width/current_sequence_length),
      (ipr_data.length*sequence_width/current_sequence_length)
    );
    // then draw each IPR domain above connectors and assign color to both graphic block and text
    var ipr_rect = paper
      .rect(
        (ipr_data.start*sequence_width/current_sequence_length),
        (ipr_height*ipr_data.layer + 1), /* + 1 to fit border lines inside the shape */
        (ipr_data.length*sequence_width/current_sequence_length),
        (ipr_height - 1), /* - 1 to fit border lines inside the shape */
        7 /* round corners */
      )
      // set gradient angle: 90�, from black to white through the IPR color
      .attr({ fill: '90-#000000-' + GetAndSetIPRColor(ipr_data.ipr_code) + "-#ffffff" })
      .toFront()
    ;
    // set name attribute of the element using jQuery so the tooltip functions
    // can retrieve the associated tooltip
    // note: Raphael can not set ID attribute
    $(ipr_rect.node).prop('name', 'ipr_' + ipr_data.ipr_code);
    // add tooltip (set event handler on SVG object)
    ipr_rect.hover(ActivateIPR, DeactivateIPR);
  });
}


/**
* LoadIPRWithDelay
*******************
Loads IPR in a delayed way so the browser won't freeze because of a two heavy
number of function calls to proceed.

*/
function LoadIPRWithDelay() {
  // check if we have something to load
  if (g_ipr_load_stack.length) {
    // get next IPR to load (ie. the function to call)
    var load_ipr = g_ipr_load_stack.shift();
    // launch the function that loads the IPR
    load_ipr();
    // get to the next one to be loaded (if one)
    if (g_ipr_load_stack.length) {
      window.setTimeout(LoadIPRWithDelay, IPR_INTER_LOADING_DELAY);
    }
  }
}


/**
* ReprocessIPRDrawingStack
***************************
Put back into drawing queue the unprocessed IPR.

*/
function ReprocessIPRDrawingStack(e, t) {
    g_ipr_load_stack = g_ipr_load_stack.concat(g_ipr_notloaded_stack);
    g_ipr_notloaded_stack = new Array();
    LoadIPRWithDelay();
}

// call LoadIPRWithDelay when document is ready
//+Val: without greenphyl.js: $(LoadIPRWithDelay);
//+Val: using greenphyl.js
g_init_functions.push(LoadIPRWithDelay);

// if within tabs or paged tables, reload...
$(function() {
  // 1) when we use a tabs, we can't draw IPR on hidden tabs
  // therefore these IPR should be processed once the tab changes.
  // To do so, we add a listener on tab changes
  $('.tabs-container')
    .on(
      'tabsactivate',
      ReprocessIPRDrawingStack
    )
  ;

  // 2) when we use a paged sortable table, we can't draw IPR on hidden pages
  // therefore these IPR should be processed once the table page changes.
  // To do so, we add a listener on page changes
  $('table.sortable, table.paged')
    .on(
      'pagerComplete sortEnd applyWidgets',
      ReprocessIPRDrawingStack
    )
  ;
});
