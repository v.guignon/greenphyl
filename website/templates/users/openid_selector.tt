<!-- BEGIN users/openid_selector.tt -->
[%#
Template users/openid_selector.tt

Description:
 This template renders the OpenID selection block.

Requires:
openid-jquery.js and openid.css.

Parameters:
-openids: list of Greenphyl::OpenID objects to use.
-openid_autoselect: if true, auto-selects the OpenID icon according to user
  cookies.
-openid_field_title

Example:

  [#% INCLUDE users/openid_selector.tt
    openid_autoselect = 1
    openid_field_title = 'Use an external account (OpenID)'
  %#]

~%]

[% IF GP('ENABLE_OPENID') %]
  <script type="text/javascript">
  [% WRAPPER javascript.tt %]
    var providers_large = {};
    var providers_small = {};

    [% FOREACH openid IN openids -%]
      [% IF openid.enabled %]
        [% IF 'large' == openid.icon_size -%]
      providers_large.[% openid.name.replace('\W', '') -%]
        [% ELSE -%]
      providers_small.[% openid.name.replace('\W', '') -%]
        [% END -%] = {
          name: '[% openid.name %]',
          index: '[% openid.icon_index %]',
          [% IF openid.label %]
          label: '[% openid.label %]',
          [% END %]

          [% IF openid.url %]
          url: '[% openid.url %]'
          [% ELSE %]
          url: null
          [% END %]
      };
      [% END %]
    [% END %]

      $(document).ready(function() {
        openid.locale = 'en';
        openid.sprite = 'en';
        openid.demo_text = 'In client demo mode. Normally would have submitted OpenID:';
        openid.signin_text = 'Sign-In';
        openid.image_title = 'Log in with {provider}';
        [% IF !openid_autoselect ~%]
        openid.setAutoSelect(false);
        [%~ END %]
        openid.init('openid_identity');
      });
  [% END %]
  </script>

  <input type="hidden" name="action" value="verify" />
  <fieldset>
      <legend>[% openid_field_title %]</legend>
      <div id="openid_choice">
        <p>Please select your account provider:</p>
        <div id="openid_btns"></div>
      </div>
      <div id="openid_input_area">
        <input id="openid_identity" name="openid_identity" type="text" value="http://" size="40"/>
      </div>
      <noscript>
        <p>OpenID is service that allows you to log-on to many different websites using a single indentity.
        Find out <a href="http://openid.net/what/">more about OpenID</a> and <a href="http://openid.net/get/">how to get an OpenID enabled account</a>.</p>
      </noscript>
  </fieldset>
  <br/>
[% END %]
<!-- END users/openid_selector.tt -->
