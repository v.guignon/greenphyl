<!-- BEGIN users/register_form.tt -->
[% start_visible = 0 %]
[% IF access_message %]
  [% INCLUDE error.tt
    title   = 'Registration failed'
    message = access_message
  %]
  [% start_visible = 1 %]
[% END %]
<div class="mt-5 mb-5">
[% WRAPPER div_toggle.tt
    id='about_account'
    label='About creating your account'
%]
  <p>
    You can register to GreenPhyl either by using a dedicated GreenPhyl account
    or by using your favorite OpenID account. In both cases, you need to choose
    a GreenPhyl user name that will appear if you share gene families.
    GreenPhyl user names can only contain alpha-numeric characters, spaces,
    underscores ("_") and dashes ("-"), they must contain at least 3 consecutive
    alphabetic characters, at most 32 characters and they can not contain the
    word "greenphyl".
  </p>
  <p>
    Please also read <a class="popup-link popup-title-Terms-of-Service" href="[% gpurl('tos') %]" title="View GreenPhyl terms of service (popup)">GreenPhyl terms of service</a>.
  </p>
[% END %]
</div>

<h3>Create your account</h3>
<div class="form-group row">
  <label for="user_login" class="col-3 col-form-label">GreenPhyl database user name (login): </label>
  <div class="col-9">
    <input class="form-control required" type="text" id="user_login" name="user_login" value="[% user_login %]" placeholder="Enter your user name here."/>
  </div>
</div>

<div class="form-group row">
  <label for="user_password" class="col-3 col-form-label">New password: </label>
  <div class="col-9">
    <input class="form-control required" type="password" id="user_password" name="user_password" value="" placeholder="Type your password."/>
  </div>
</div>

<div class="form-group row">
  <label for="user_password_confirm" class="col-3 col-form-label">Confirm password: </label>
  <div class="col-9">
    <input class="form-control required" type="password" id="user_password_confirm" name="user_password_confirm" value="" placeholder="Re-type your password for confirmation."/>
  </div>
</div>

<div class="form-group row">
  <label for="user_display_name" class="col-3 col-form-label">Display name (optional): </label>
  <div class="col-9">
    <input class="form-control" type="text" id="user_display_name" name="user_display_name" value="[% user_display_name %]" placeholder="Name to display to other users if different from login."/>
  </div>
</div>

<div class="form-group row mb-0 pb-0">
  <label for="user_email" class="col-3 col-form-label">Contact e-mail (optional): </label>
  <div class="col-9">
    <input class="form-control" type="text" id="user_email" name="user_email" value="[% user_email %]" placeholder="Remains strictly confidential and is never shared with anybody other than the 2 site administrators."/>
  </div>
</div>
<u>Note</u>: your e-mail address will remain hidden and will only be used to contact you in case of account data policy changes or if you need a password reset.<br/>

<div class="form-group">
  <div class="spamcheck">
    <label for="nofill">Do not fill: </label>
    <input name="nofill" id="nofill" type="text" />
  </div>
</div>

<div class="form-check mt-5 mb-5">
  <input name="agreement" id="agreement" type="checkbox" class="form-check-input" [% agreement ? ' checked="checked"' : '' %] />
  <label class="form-check-label" for="agreement">
    By checking this box, you acknowledge that you have read and agree to <a class="popup-link popup-title-Terms-of-Service" href="[% gpurl('tos') %]" title="View GreenPhyl terms of service (popup)">GreenPhyl terms of service</a>.
  </label>
</div>

<div class="form-group">
[% INCLUDE antispam.tt %]
</div>

<input type="hidden" name="registration" value="1"/>

<!-- END users/register_form.tt -->
