<!-- BEGIN sequences/sequence_id_card.tt -->
[%#
Template sequences/sequence_id_card.tt

Description:
 This template displays a sequence ID card.

Parameters:
-sequence: the sequence object

~%]
[%~ IF 0 < sequence.id ~%]
  <table class="table details">
    <tbody>
      <tr>
        <th class="compact-left">Sequence ID</th>
        <td>
          <b>[% sequence.accession ~%]</b>&nbsp;
          [%~ INCLUDE mylists/mylist_link.tt
            object = sequence
          %]
          <!-- <a href="[% gpurl('rdf', {'p' = 'sequences', 'accession' = sequence.accession}) | html %]" title="RDF Export" class="rdf-export">RDF</a> -->
        </td>
      </tr>
      
      <tr>
        <th class="compact-left">Species</th>
        <td>
          [% INCLUDE species/species_name.tt
            species = sequence.species
          %]
        </td>
      </tr>
      
      <tr>
        <th class="compact-left">Alias</th>
        <td>
          [% alias_list = [] %]
          [% FOREACH uniprot IN sequence.uniprot %]
              [% alias_list.push('<a href="' _ gpurl('uniprot', undef, { 'path' = uniprot.accession }) _ '" title="View UniProt details at uniprot.org" ' _ link_target _ '>' _ uniprot.accession _ '</a>') %]
          [% END %]
          [% FOREACH synonym IN sequence.synonyms %]
              [% alias_list.push(synonym) %]
          [% END %]

          [% IF alias_list.size %]
              [% alias_list.join(', ') %]
          [% ELSE %]
              No gene alias
          [% END %]
        </td>
      </tr>

      [% IF ('Sequence' == sequence.getObjectType().replace('^Cached', '')) %]
        [% IF !sequence.representative || Array(sequence.getRepresentedSequences()).size %]
        <tr>
          <th>
            Pangenome status
          </th>
          <td>
            [% INCLUDE sequences/pangenome_status.tt %]
          </td>
        </tr>
        [% END %]
      [% END %]

      <tr>
        <th class="compact-left">Length</th>
        <td>
          [% sequence.length %]aa
        </td>
      </tr>

      [% links = sequence.getExternalLinks %]
      [% IF 0 #+debug links.size %]
        [% cross_link_count = 0 %]
        [% cross_link_table = BLOCK %]
          <table class="table table-striped">
            <tbody>
              [% FOR link_category_key IN links.keys.sort %]
                [% link_category = links.$link_category_key %]
                <tr>
                  <th class="compact-left">
                    [% link_category.label %]
                  </th>
                  <td>
                    [% FOR link IN link_category.links %]
                      [% cross_link_count = cross_link_count + 1 %]
                      <a href="[% link.url %]" title="[% link.description %]" [% link_target %]>[% link.label %]</a><br/>
                    [% END %]
                  </td>
                </tr>
              [% END %]
            </tbody>
          </table>
        [% END %]

      <tr>
        <th class="compact-left">Cross-reference(s)</th>
        <td>
          [% INCLUDE div_toggle.tt
            id='cross_ref'
            label_show='Display link(s) (' _ cross_link_count _ ')'
            label_hide='Hide'
            start_visible=0
            content = cross_link_table
          %]
        </td>
      </tr>
      [% END %]

      [% IF sequence.pubmed.size %]
      <tr>
        <th class="compact-left">PubMed References</th>
        <td>
          [% WRAPPER div_toggle.tt
            id='pubmed_ref'
            label_show='Display PubMed Reference(s) (' _ sequence.pubmed.size _ ')'
            label_hide='Hide'
            start_visible=0
          %]
            [% INCLUDE dbxref/dbxref_list.tt
              dbxref_list = sequence.pubmed
              identifier = 'pubmed_list'
              columns = ['description']
            %]
          [% END %]
        </td>
      </tr>
      [% END %]

      [% IF sequence.annotation %]
      <tr>
        <th class="compact-left">Gene Annotation</th>
        <td>[% sequence.annotation %]</td>
      </tr>
      [% END %]
      
      [% IF sequence.go.size %]
      <tr>
        <th class="compact-left">Gene Ontology</th>
        <td>
          [% WRAPPER div_toggle.tt
            id='go_terms'
            label_show='Display term(s) (' _ sequence.go.size _ ')'
            label_hide='Hide'
            start_visible=0
          %]
            [% INCLUDE go/go_groups.tt %]
          [% END %]
        </td>
      </tr>
      [% END %]

      [% IF ('CustomSequence' == sequence.getObjectType().replace('^Cached', '')) && (0 < Array(sequence.sequences).size) && Array(sequence.sequences).0 %]
      <tr>
        <th class="compact-left">Associated GPv5 sequence[% 1 < Array(sequence.sequences).size && Array(sequence.sequences) ? 's' : '' %]</th>
        <td>
          [% separator = '' %]
          [% FOREACH associated_sequence IN sequence.sequences %]
            [% separator %]
            [% INCLUDE sequences/sequence_id.tt
              sequence = associated_sequence
              with_form_field = ''
            %]
            [% separator = ', ' %]
          [% END %]
        </td>
      </tr>
      [% END %]

    </tbody>
  </table>
  <br />

[%~ ELSE ~%]
  <p class="centered not-available">Sequence not found in database!</p>
[%~ END %]
<!-- END sequences/sequence_id_card.tt -->
