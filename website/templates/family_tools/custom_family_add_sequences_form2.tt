<!-- BEGIN family_tools/custom_family_add_sequences_form2.tt -->

[% IF family %]
  <input type="hidden" name="custom_family_id" value="[% family.id %]"/>
[% END %]

[% sequence_counter = 0 %]

[% MACRO sequence_data_block BLOCK %]
  <input type="hidden" name="sequence_accession_[% sequence_counter %]" value="[% accession | url %]"/>
  [% IF species_code %]
    <input type="hidden" name="sequence_polypeptide_[% sequence_counter %]" value="[% seq_data | url %]"/>
    <input type="hidden" name="species_code_[% sequence_counter %]" value="[% species_code | url %]"/>
  [% ELSE %]
    <input type="hidden" name="sequence_polypeptide_[% sequence_counter %]" value="[% seq_data.polypeptide | url %]"/>
    <input type="hidden" name="species_code_[% sequence_counter %]" value="[% seq_data.organism | url %]"/>
  [% END %]
[% END %]

[% show_node = TRUE  %]
[% MACRO missing_species_block BLOCK %]
  <div class="form-group">
    <label for="species_code_[% sequence_counter %]">
      Correct the species code corresponding to &quot;[% accession | html %]&quot; or leave this field as is:
    </label>
    [% IF seq_data.organism %]
      <input type="text" id="species_code_[% sequence_counter %]" name="species_code_[% sequence_counter %]" value="[% seq_data.organism | html %]" class="form-control autocomplete-species-code"/>
      [% IF seq_data.organism && seq_data.organism.match('^[A-Z]{5}$') %]
        <div class="note"><b>Note:</b> unrecognised species code &quot;[% seq_data.organism | html %]&quot;</div>
      [% END %]
    [% ELSE %]
      <input type="text" id="species_code_[% sequence_counter %]" name="species_code_[% sequence_counter %]" placeholder="Start to enter your species name here and use auto-completion." class="form-control autocomplete-species-code"/>
    [% END %]
  </div>
  [%- IF show_node  -%]
    <div class="note">(e.g. for Arabidopsis thaliana, enter &quot;ARATH&quot;)</div>
    [%- show_node = FALSE  -%]
  [%- END -%]
[% END %]

[% MACRO ok_species_block BLOCK %]
    <li>[% accession | html %]</li>
[% END %]


[% WRAPPER paragraph.tt
  title = 'Sequence details'
%]
Please review your sequences and associated species. Some sequences may already
be in database.
[% END %]

[%
  missing_species_html = ''
  ok_species_html = ''
%]

[% FOR species_code IN  species_sequences.keys %]
  [% IF species_code %]
    [% ok_species_html = ok_species_html _ '<b>' _ species_code _ ':</b><ul>' %]
  [% END %]
  [% FOR accession IN species_sequences.$species_code.keys %]
    [% sequence_data_block(seq_data = species_sequences.$species_code.$accession) %]
    [% sequence_counter = sequence_counter + 1 %]

    [% IF !species_code %]
      [% missing_species_html = missing_species_html _ missing_species_block(seq_data = species_sequences.$species_code.$accession) %]
    [% ELSE %]
      [% ok_species_html = ok_species_html _ ok_species_block() %]
    [% END %]
    
  [% END %]
  [% IF species_code %]
    [% ok_species_html = ok_species_html _ '</ul><br/>' %]
  [% END %]
[% END %]
<input type="hidden" name="sequence_count" value="[% sequence_counter %]"/>

[% IF missing_species_html %]
<fieldset>
  <legend>Incomplete sequences (missing or invalid species codes)</legend>
  [% missing_species_html %]
</fieldset>
[% END %]

<br/>
[% IF ok_species_html %]
<h3>Species validated:</h3>
<br/>
  [% ok_species_html %]
<br/>
[% END %]

<!-- END family_tools/custom_family_add_sequences_form2.tt -->
