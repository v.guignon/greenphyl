<!-- BEGIN family_tools/phylogenomic_analysis.tt -->

[% TREE_DISPLAY = 1 %]
[% IN_TREE_GREATE = 1 %]

[% can_submit_phylogeny_files = 0 %]

[% IF phylogenomic_analysis.alignment_url %]

  [% IF phylogenomic_analysis.alignment_file_size >= 2097152 %]
    Note: Alignment too large for online display.
  [% ELSE %]
    [% INCLUDE tools/MSAviewer.tt
      alignment_url = phylogenomic_analysis.alignment_url
    %]
  [% END %]

  <div style="font-size: small;">
    [%~ INCLUDE dump_links.tt
      dump_data =
      {
        header = 'Download alignment file:'
        links = [{label  = 'FASTA'}]
        script = phylogenomic_analysis.alignment_url
        mode   = 'link'
      }
    %]
    <br/>
  </div>
  <br/>
  [% IF 'CustomFamily' == family.getObjectType().replace('^Cached', '') && family.hasWriteAccess() %]
    <label for="alignment_fasta_input">Replace alignment file (FASTA format):</label>
    <input class="form-control-file" type="file" id="alignment_fasta_input" name="alignment_fasta" form="phylogeny_files_form"/>
    [% can_submit_phylogeny_files = 1 %]
  [% END %]

[% ELSIF phylogenomic_analysis.family_level_1 && phylogenomic_analysis.alignment_url_level_1 %]
  <div class="note">Alignment available at level 1 family ([% INCLUDE families/family_id.tt
            family = phylogenomic_analysis.family_level_1
          %] ).</div>

[% ELSE %]
  <div class="note">Alignment not available.</div>
  [% IF 'CustomFamily' == family.getObjectType().replace('^Cached', '') && family.hasWriteAccess() %]
    <label for="alignment_fasta_input">Upload your alignment file (FASTA format):</label>
    <input class="form-control-file" type="file" id="alignment_fasta_input" name="alignment_fasta" form="phylogeny_files_form"/>
    [% can_submit_phylogeny_files = 1 %]
  [% END %]
[% END %]

[%# IF phylogenomic_analysis.masking1_url % ]
  <span>[<a href="[ % phylogenomic_analysis.masking1_url % ]" [ % link_target % ]>Masking step 1 details</a>]<span/>
[ % END %]

[%# IF phylogenomic_analysis.masking2_url % ]
  <span>[<a href="[ % phylogenomic_analysis.masking2_url % ]" [ % link_target % ]>Masking step 2 details</a>]<span/>
[ % END %]

[%# IF phylogenomic_analysis.filtration_url % ]
  <span>[<a href="[ % phylogenomic_analysis.filtration_url % ]" [ % link_target % ]>Filtration details</a>]<span/>
[ % END %]

[% IF phylogenomic_analysis.hmm_url %]
  <span>[<a href="[% phylogenomic_analysis.hmm_url %]" [% link_target %]>Global HMM file</a>]<span/>
[% END %]
<br/>
<br/>

[% IF family.alignment_sequence_count %]
  <div class="note">Alignment and filtration statistics:</div>
  <br/>
  <table class="table table-hover table-bordered" id="[% GetFreeHTMLIdentifier('family_align_stats') %]">
    <thead>
      <tr>
        <th>Overall family sequences</th>
        <th>Sequences used for alignment (w/o splice)</th>
        <th>Sequences kept after filtration</th>
        <th>[min; median; max] ~average sequence length</th>
        <th>Alignment length</th>
        <th>Masked alignment length</th>
      </tr>
    </thead>
    <tbody>
      <tr>
        <td>[% family.sequence_count %]</td>
        <td>[% family.alignment_sequence_count %] ([% 100 * family.alignment_sequence_count / family.sequence_count | format('%.0f')%]%)</td>
        <td>[% family.masked_alignment_sequence_count %] ([% 100 * family.masked_alignment_sequence_count / family.sequence_count | format('%.0f')%]%)</td>
        <td>[[% family.min_sequence_length %]; [% family.median_sequence_length %]; [% family.max_sequence_length %]] ~[% family.average_sequence_length %]</td>
        <td>[% family.alignment_length %]</td>
        <td>[% family.masked_alignment_length %]</td>
      </tr>
    </tbody>
  </table>
  <br/>
[% END %]





[% IF TREE_DISPLAY && phylogenomic_analysis.alignment_tree_url %]
    <form target="tree_frame" action="[% gpurl('phyd3') | html %]&amp;id=[% family.getSubPath _ '/' _ family.accession _ '/' _ phylogenomic_analysis.tree_file %]" enctype="multipart/form-data" method="post" style="padding:0px">
      <input type="submit" value="View gene tree with PhyD3"  onclick="return showTree(this, '[% phylogenomic_analysis.alignment_tree_url | html %]');" />
    </form>
[% END %]
 <br /> 
 <br /> 
 <br /> 
[% IF IN_TREE_GREATE && (phylogenomic_analysis.tree_url || phylogenomic_analysis.alignment_tree_url) %]

  <div class="note">You can filter species displayed in the phylogenomic tree by checking the species you want:</div>
  <form target="tree_frame" action="[% gpurl('treedisplay') | url %]" enctype="multipart/form-data" method="post" style="padding:0px">
    [% INCLUDE species/species_list.tt
      identifier = 'phylogeny_list_species'
      format = 'fieldset'
      with_form_field = 'checkbox'
      use_species_code = 1
    %]
  <br/>

  [% IF IN_TREE_GREATE && TREE_DISPLAY && phylogenomic_analysis.alignment_tree_url %]

        <input type="hidden" name="treedisplay_url" value="[% gpurl('treedisplay') | url %]"/>
        <input type="hidden" name="intreegreate_url" value="[% gpurl('intreegreate') | url %]"/>
        <input type="hidden" name="hiddenfield" value="((a,b),c);"/>
        <input type="hidden" name="id" value="[% family.accession %]"/>
        <input type="hidden" name="widtharea" value="1500"/>
        <input type="hidden" name="display" value="ultra"/>
        <input type="hidden" name="collapsewidth" value="2"/>
        <input type="hidden" name="linearea" value="3"/>
        <input type="hidden" name="roundarea" value="20"/>
        <input type="hidden" name="fontarea" value="14"/>
        <input type="hidden" name="supportsizearea" value="11"/>
        <input type="hidden" name="familyarea" value="Candara"/>
        <input type="hidden" name="colorBackArea" value="white"/>
        <input type="hidden" name="colorLineArea" value="#05357E"/>
        <input type="hidden" name="colorCollapseArea" value="#EEEEEE"/>
        <input type="hidden" name="colorTagArea" value="#FF0000"/>
        <input type="hidden" name="colorTextArea" value="black"/>
        <input type="submit" value="View gene tree with InTreeGreat"  onclick="return showTree(this, '[% phylogenomic_analysis.alignment_tree_url | html %]');" />

  [% END %]
  
  </form>
  <div style="font-size: small;">
    [%~ 
      tree_links = []
    %]
    
    [%~ IF phylogenomic_analysis.tree_url ~%]
      [% tree_links.push(
              {
                  label  = 'XML'
                  script = phylogenomic_analysis.tree_url
              }
          )
      %]
    [%~ END ~%]

    [%~ IF phylogenomic_analysis.alignment_tree_url ~%]
      [% tree_links.push(
              {
                label  = 'Newick'
                script = phylogenomic_analysis.alignment_tree_url
              }
          )
      %]
    [%~ END ~%]

    [%~ INCLUDE dump_links.tt
      dump_data =
      {
        header = 'Download tree file:'
        links = tree_links
        mode   = 'link'
      }
    %]
  </div>

[% END %]

[% IF !phylogenomic_analysis.tree_url && !phylogenomic_analysis.alignment_tree_url %]
  [% IF phylogenomic_analysis.family_level_1 && phylogenomic_analysis.tree_url_level_1 %]
    <div class="note">Phylogeny tree available at level 1 family ([% INCLUDE families/family_id.tt
      family = phylogenomic_analysis.family_level_1
    %] ).</div>
  [% ELSE %]

    <div class="note">Phylogeny tree not available.</div>
    
    [% IF 'CustomFamily' == family.getObjectType().replace('^Cached', '') && family.hasWriteAccess() %]
      <label for="tree_newick_input">Upload your tree file (Newick format):</label>
      <input class="form-control-file" type="file" id="tree_newick_input" name="tree_newick" form="phylogeny_files_form"/>
      <br/>
      <label for="tree_xml_input">Upload your tree file (PhyloXML format):</label>
      <input class="form-control-file" type="file" id="tree_xml_input" name="tree_xml" form="phylogeny_files_form"/>

      [% can_submit_phylogeny_files = 1 %]
    [% END %]

  [% END %]

[% ELSIF phylogenomic_analysis.tree_url && phylogenomic_analysis.alignment_tree_url %]

  [% IF 'CustomFamily' == family.getObjectType().replace('^Cached', '') && family.hasWriteAccess() %]
    <label for="tree_newick_input">Replace tree file (Newick format):</label>
    <input type="file" id="tree_newick_input" name="tree_newick" form="phylogeny_files_form"/>

    <label for="tree_xml_input">Replace tree file (PhyloXML format):</label>
    <input type="file" id="tree_xml_input" name="tree_xml" form="phylogeny_files_form"/>
    [% can_submit_phylogeny_files = 1 %]
  [% END %]

[% ELSIF phylogenomic_analysis.tree_url %]

  [% IF 'CustomFamily' == family.getObjectType().replace('^Cached', '') && family.hasWriteAccess() %]
    <label for="tree_newick_input">Upload your tree file (Newick format):</label>
    <input type="file" id="tree_newick_input" name="tree_newick" form="phylogeny_files_form"/>
    
    <label for="tree_xml_input">Replace tree file (PhyloXML format):</label>
    <input type="file" id="tree_xml_input" name="tree_xml" form="phylogeny_files_form"/>
    [% can_submit_phylogeny_files = 1 %]
  [% END %]

[% ELSIF phylogenomic_analysis.alignment_tree_url %]

  [% IF 'CustomFamily' == family.getObjectType().replace('^Cached', '') && family.hasWriteAccess() %]
    <label for="tree_newick_input">Replace tree file (Newick format):</label>
    <input type="file" id="tree_newick_input" name="tree_newick" form="phylogeny_files_form"/>
    
    <label for="tree_xml_input">Upload your tree file (PhyloXML format):</label>
    <input type="file" id="tree_xml_input" name="tree_xml" form="phylogeny_files_form"/>
    [% can_submit_phylogeny_files = 1 %]
  [% END %]

[% END %]

<br/>
<br/>

[% IF can_submit_phylogeny_files %]
  [% WRAPPER form.tt
    form_data =
    {
      'identifier' = 'phylogeny_files_form'
      'action'     = gpurl('update_custom_family_phylogeny_files', {'custom_family_id' = family.id}, {'anchor' => 'tab-phyloana'})
      'noclear'    = 1
      'submit'     = 'Update files'
    }
    # nb: input files above are associated to this form and don't need to appear inside the form
  %]
  [% END %]
[% END %]

[% IF family.alignment_sequence_count %]
  <div class="note">Alignment and filtration statistics:</div>
  <br/>
  <table class="table table-hover table-bordered" id="[% GetFreeHTMLIdentifier('family_align_stats') %]">
    <thead>
      <tr>
        <th>Overall family sequences</th>
        <th>Sequences used for alignment (w/o splice)</th>
        <th>Sequences kept after filtration</th>
        <th>[min; median; max] ~average sequence length</th>
        <th>Alignment length</th>
        <th>Masked alignment length</th>
      </tr>
    </thead>
    <tbody>
      <tr>
        <td>[% family.sequence_count %]</td>
        <td>[% family.alignment_sequence_count %] ([% 100 * family.alignment_sequence_count / family.sequence_count | format('%.0f')%]%)</td>
        <td>[% family.masked_alignment_sequence_count %] ([% 100 * family.masked_alignment_sequence_count / family.sequence_count | format('%.0f')%]%)</td>
        <td>[[% family.min_sequence_length %]; [% family.median_sequence_length %]; [% family.max_sequence_length %]] ~[% family.average_sequence_length %]</td>
        <td>[% family.alignment_length %]</td>
        <td>[% family.masked_alignment_length %]</td>
      </tr>
    </tbody>
  </table>
  <br/>
[% END %]

<br/>
[% IF family.branch_average || family.branch_median || family.branch_standard_deviation  %]
  <div class="note">Phylogenomic tree statistics:</div>
  <br/>
  <table class="table table-hover table-bordered" id="[% GetFreeHTMLIdentifier('family_tree_stats') %]">
    <thead>
      <tr>
        <th>Overall family sequences</th>
        <th>Average branch length</th>
        <th>Median branch length</th>
        <th>Standard deviation</th>
      </tr>
    </thead>
    <tbody>
      <tr>
        <td>[% family.sequence_count %]</td>
        <td>[% family.branch_average %]</td>
        <td>[% family.branch_median %]</td>
        <td>[% family.branch_standard_deviation %]</td>
      </tr>
    </tbody>
  </table>
  <br/>
[% END %]

<!-- END family_tools/phylogenomic_analysis.tt -->
