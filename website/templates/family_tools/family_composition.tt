<!-- BEGIN family_tools/family_composition.tt -->
[% family_type = family.getObjectType(1).replace('^cached_', '') %]
[% family_id_field = family_type _ '_id' %]
[% family_composition_block = BLOCK %]

  <input type="hidden" name="service" value="get_sequence_data" />
  <input type="hidden" name="format" value="html" />
  <input type="hidden" name="[% family_type %]_id" value="[% family.id %]" />

  <div class="centered header" id="family_composition_title">Sequence total: [% family.sequence_count %]</div>

  <div class="centered">
    <div id="family_composition_chart_area" class="centered"></div>

    <div id="family_composition_table_container">
      <table class="table"
        id="family_composition_table"
        data-toggle="table"
        data-sortable="true"
        data-pagination="false"
        data-search="false"
      >
        <thead>
          <tr>
            <th data-sortable="true">Species</th>
            <th data-sortable="true">Sequence Count</th>
          </tr>
        </thead>
        <tbody>
          [% FOR species IN family_composition.species_order %]
          <tr>
            <td>
              [% INCLUDE species/species_name.tt
                with_form_field = 'checkbox'
              %]
            </td>
            [% species_key = species.id %]
            [% IF GP('NONUNIPROT_SPECIES_CODE') == species.code %]
              [% species_key = species.id  _ ' ' _ species.organism %]
            [% END %]
            <td>[% family_composition.sequence_counts.${species_key}.sequence_count %]</td>
          </tr>
          [% END %]
        </tbody>
      </table>
    </div>

    <script type="text/javascript">
    [% WRAPPER javascript.tt %]
      jQuery(function () {
        var species = {
            [% FOR species IN family_composition.species_order %]
              '[% species.organism %]': [% to_json(species.getHashValue()) %][% loop.last ? '' : ',' %]
            [% END %]
        };
        // Parse data.
        var data = [];
        jQuery("#family_composition_table tbody tr").each(function(i) {
          var sequence_count = parseInt(jQuery(this).children('td:eq(1)').text(), 10);
          if (sequence_count)
          {
            data.push({
              'species': jQuery(this).children('td:eq(0)').text().replace(/^\s*(\S.*\S)\s*$/, "$1"),
              'sequence_count' : sequence_count
            });
          }
        });
        // Remove table.
        jQuery("#family_composition_table_container").html('');

        var height = 400, width = 768;
        var margin = {
          top: 10,
          right: 10,
          bottom: 150,
          left: 30
        };
        var internal_height = height - margin.top - margin.bottom,
          internal_width = width - margin.left - margin.right;

        var svg_container = d3.select("#family_composition_table_container");
        var svg = svg_container.append("svg:svg")
          .attr("viewBox", "0 0 "
            + width
            + " "
            + height
          );
        // Move to include the margins.
        var g = svg.append("g").attr("transform", "translate("
          + margin.left
          + ","
          + margin.top
          + ")"
        );

        // Prepare scale axis.
        var x_axis = d3.scaleBand()
          .rangeRound([0, internal_width])
          .padding(0.3)
          .domain(
            data.map(function (d) {
              return d.species;
            })
          );
        var sequence_count_for_scale = data.map(function (d) {
          return d.sequence_count;
        });
        sequence_count_for_scale.push(5);
        var y_axis = d3.scaleLinear()
          .rangeRound([internal_height, 0])
          .domain([
            0,
            d3.max(sequence_count_for_scale)
          ]);
        // Horizontal x axis.
        var x_axis_g = g.append("g")
          .attr("transform", "translate(0," + internal_height + ")")
          .call(d3.axisBottom(x_axis))
            .selectAll("text")	
              .style("text-anchor", "end")
              .attr("dx", "-0.7em")
              .attr("dy", "-0.3em")
              .attr("transform", function(d) {
                return "rotate(-60)";
              })
        ;

        // Vertical y axis.
        var y_axis_g = g.append("g")
          .call(d3.axisLeft(y_axis))
          .append("text")
            .attr("fill", "#000")
            .attr("transform", "rotate(-90)")
            .attr("y", 6)
            .attr("dx", "-0.3em")
            .attr("dy", "0.3em")
            .attr("text-anchor", "end")
            .text("Sequence count")
        ;

        // Colors.
        var color_scale = d3.scaleSequential()
          .domain([0,data.length])
          .interpolator(d3.interpolateWarm)
        ;

        var loading_counter = 0;
        // Bar diagram.
        var bar = g.selectAll(".bar")
          .data(data)
          .enter()
          .append("g")
            .attr("class", "bar")
            .attr("transform", function (d) {
              return "translate(" + x_axis(d.species) + "," + internal_height + ")"
            })
          bar.append("rect")
            .attr("y", function (d) {
              return y_axis(Number(d.sequence_count)) - internal_height;
            })
            .attr("width", x_axis.bandwidth())
            .attr("height", function (d) {
              return internal_height - y_axis(Number(d.sequence_count));
            })
            .attr("fill", function (d, i) {
              return d.selected ? color_scale(i) : d3.color(color_scale(i)).brighter(2.5);
            })
            .attr("stroke", function (d, i) {
              return d.selected ? "darkgreen" : "black";
            })
            .attr("vector-effect", "non-scaling-stroke")
            .on("mouseenter", function (d, i) {
              d3.select(this)
                .attr("fill", d3.color(color_scale(i)).darker(1))
                .attr("stroke", "green")
              ;
            })
            .on("mouseleave", function (d, i) {
              d3.select(this)
                .attr("fill", d.selected ? color_scale(i) : d3.color(color_scale(i)).brighter(2.5))
                .attr("stroke", d.selected ? "red" : "black")
                .attr("stroke-width", d.selected ? '3px' : '1px')
              ;
            })
            .on("click", function (d, i) {
              // console.log(species[d.species]);
              d.selected = !d.selected;
              if (d.selected) {
                jQuery("body").css("cursor", "progress");
                ++loading_counter;
                // Add species sequences.
                jQuery
                  .ajax({
                    url: '[% gpurl('get_sequence_data', {'class' = ('custom_family' == family_type ? 'custom_sequence' : 'sequence'), 'mode' = 'inner', 'format' = 'html', ${family_id_field} = family.id }) %]' + '&seq_info=pangene,genome,uniprot,ipr,annotation,go,pubmed&species_id=' + species[d.species].id,
                    dataType: 'html'
                  })
                  .done(function(data, status, xhr) {
                    // Clear loading image.
                    // $('#[% search_type.type %]_status').html('');
                    // Check if an error occurred.
                    if (status == "error") {
                      // $('#qsearch_results_status')
                      //   .append('<div class="error">ERROR: an error occured: '  + xhr.status + ' ' + xhr.statusText + '</div>')
                      // ;
                      return;
                    }
                    // Make sure another search is not running.
                    // if (search_index == g_search_index) {
                      // No error, load data.
                      if (jQuery('#composition_sequence_table table').exists()) {
                        // Append elements
                        jQuery('#composition_sequence_table table').append(
                          jQuery(data).find('table.sequence-list >tbody >tr').addClass('species-' + species[d.species].code)
                        );
                      }
                      else {
                        jQuery('#composition_sequence_table').append(
                          jQuery(data)
                        );
                        jQuery('#composition_sequence_table table.sequence-list >tbody >tr').addClass('species-' + species[d.species].code);
                      }
                      initGreenPhylPage();
                      jQuery("body").scrollTo(jQuery('#composition_sequence_table'));
                    // }
                  })
                  .fail(function(jqXHR, status, error) {
                    // Set failure image.
                    ShowFailure('#[% search_type.type %]_status');
                    console.log('Failed to search [% search_type.type %]: ' + status + ' ' + error);
                  })
                  .always(function(data, status, xhr) {
                    if (0 >= --loading_counter) {
                      jQuery("body").css("cursor", "default");
                    }
                  })
                ;

              }
              else {
                  // Remove species sequences.
                  jQuery('#composition_sequence_table table.sequence-list >tbody >tr.species-' + species[d.species].code).remove();
                  if (!jQuery('#composition_sequence_table table.sequence-list >tbody >tr').exists()) {
                    // Last sequence removed, clear area.
                    jQuery('#composition_sequence_table').html('');
                  }
              }
            })
          ;
          bar.append("text")
            // .attr("fill", function (d, i) {
            //   return isDark(color_scale(i)) ? 'white' : 'black';
            // })
            .attr("y", function (d) {
              // return y_axis(Number(d.sequence_count)) - internal_height + 8;
              return y_axis(Number(d.sequence_count)) - internal_height - 2;
            })
            .attr("x", x_axis.bandwidth() / 2)
            .attr("font-size", "0.3em")
            .attr("text-anchor", "middle")
            .text(function(d) { return d.sequence_count; })
        ;
      });
     [% END %]
    </script>
  </div>

[% END %]

  <div id="fam_comp">
  <center><span class="note">(click on individual chart bar to display sequence composition below. Several bars can be selected and will remain highlighted)</span></center>
    [% family_composition_block %]
  </div>
  <div id="composition_sequence_table" class="dynamic-block centered">
    
  </div>

<!-- END family_tools/family_composition.tt -->
