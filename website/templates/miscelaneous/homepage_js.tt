[%#
  Template miscelaneous/homepage_js.tt

  Contains the javascript used to generate the homepage tree.
  Requires D3JS v5.

~%]
<script>
[% WRAPPER javascript.tt %]
jQuery(function () {

  [% PROCESS miscelaneous/homepage_data.tt %]
  var node_size = 4, transition_duration = 750, ids = 1;

  var svg_container = d3.select("#homepage_tree");

  var height = 720, width = 720, padding = 20;

  var svg = svg_container.append("svg:svg")
        .attr("viewBox", "0 0 " + width + " " + height),
      g = svg.append("g")
        .attr("transform", "translate(" + (width / 2) + "," + (height / 2) + ")");

  // Define the div for the species popup.
  var species_popup = d3.select("body").append("div")
    .attr("class", "species-popup")
    .style("opacity", 0);

  var zoom = d3.zoom()
    .scaleExtent([0.5, 40])
    .translateExtent([[-100, -100], [width + 100, height + 100]])
    .on("zoom", doZoom);

  svg.call(zoom);

  var tree_layout = d3.cluster()
    .size([360, (height / 3)])
    .separation(function(a, b) { return (a.parent == b.parent ? 1 : 1.5) / a.depth; });

  var continue_random_toggle = true;

  function doZoom() {
    g.attr("transform", "translate(" + (d3.event.transform.k * width / 2) + "," + (d3.event.transform.k * height / 2) + ") " + d3.event.transform);
  }

  function resetZoom() {
    svg.transition()
      .duration(750)
      .call(zoom.transform, d3.zoomIdentity);
  }

  // Sort nodes.
  function sort_greenphyl_tree(json_tree_node) {
    if (json_tree_node.children) {
      json_tree_node.children.sort(function (a, b) {
        return a.id.localeCompare(b.id);
      });
      json_tree_node.children.forEach(function (node) {
        sort_greenphyl_tree(node);
      });
    }
  }
  sort_greenphyl_tree(greenphyl_tree);

  var tree_data = d3.hierarchy(greenphyl_tree);

  // Compute radial coordinates of a point.
  function project_radial(x, y) {
    var angle = (x - 90) / 180 * Math.PI, radius = y;
    return [radius * Math.cos(angle), radius * Math.sin(angle)];
  }

  // Returns the path of a link between a node and its parent.
  function draw_link(d) {
    if (!d.parent) {
      console.log("Warning: invalid data for draw_link().");
      console.log(d);
      return "";
    }
    else if (!d.parent.parent) {
      // Root node, draw straight lines.
      return "M" + project_radial(d.parent.x, d.parent.y) + " " + project_radial(d.x, d.y);
    }
    else {
      // Regular node.
      return "M" + project_radial(d.x, d.y)
          + " " + project_radial(d.parent.x, d.parent.y);
//      return "M" + project_radial(d.x, d.y)
//          + "C" + project_radial(d.x, (d.y + d.parent.y) / 2)
//          + " " + project_radial(d.parent.x, (d.y + d.parent.y) / 2)
//          + " " + project_radial(d.parent.x, d.parent.y);
    }
  }

  // Hide genomes.
  function collapse_species(d) {
    function _collapse_species(d) {
      if (d.children) {
        d.children.forEach(function(element) {
          if (element.data.rank && ("species" == element.data.rank)) {
            if (element.children) {
              element._children = element.children;
              element.children = null;
            }
          }
          else {
            _collapse_species(element);
          }
        });
      }
    }
    _collapse_species(d);
    update_tree(tree_data);
  }

  // Toggle children on click.
  function toggle_node(d) {
    stop_random_node_switch();
    if ("root" == d.data.rank) {
      return;
    }
    if (d.children) {
        d._children = d.children;
        d.children = null;
    }
    else {
      d.children = d._children;
      d._children = null;
    }
    // Redraw tree.
    update_tree(tree_data);
  }

  function get_visible_parent(d) {
    var lineage = [];
    var e = d;
    var x = 0;
    while (e.parent && (++x < 10000)) {
      lineage.push(e);
      e = e.parent;
    }
    var parent = d;
    while (lineage.length) {
      e = lineage.pop();
      if (!e.children) {
        parent = e;
        lineage = [];
      }
    }
    return parent;
  }

  function find_node_class(d) {
    return "node"
      + (d.children ? " node-internal" : " node-leaf")
      + (d.data.rank ? " node-" + d.data.rank : "")
      + ((("species" == d.data.rank) && d.data.pangenome) ? " node-pangenome" : "")
    ;
  }


  function update_tree(data) {

    var root = tree_layout(data);
    var nodes = root.descendants(),
        links = root.descendants().slice(1);

    // Store the old positions for transition.
    nodes.forEach(function(d) {
      d.x0 = d.x0 || d.x;
      d.y0 = d.y0 || d.y;
      // Keep initial distance to center.
      d.y = d.y0;
    });

    var node = g.selectAll(".node").data(nodes, function(d) {
      // return d.id || (d.id = ++ids);
      return d.id || (d.id = d.data.id);
    });
    var link = g.selectAll(".link").data(links, function(d) { return d.id; });

    var link_enter = link.enter()
      .append("path")
        .attr("class", "link")
        .attr("d", draw_link);

    var node_enter = node.enter()
      .append("g")
        .attr("class", find_node_class)
        .attr("transform", function(d) {
          return "translate(" + project_radial(d.x, d.y) + ")";
        })
        .on('click', toggle_node)
    ;

    node_enter.append('circle')
      .attr('r', node_size)
      .on("mouseover", function(d) {
        if ("species" == d.data.rank) {
          species_popup
            .transition()
            .duration(200)
            .style("opacity", .9)
          ;
          species_popup
            .html(
              (d.data.url_picture ? '<img src="' + d.data.url_picture + '" alt="" class="mx-auto d-block" />' : '')
              + '<div><span class="header">' + d.data.id + '</span> (species)</div>'
              + (d.data.common_name ?  'Common name: ' + d.data.common_name  + '<br/>' : '')
              + 'has a pangenome: ' + (d.data.pangenome ? 'yes' : 'no') + '<br/>'
            )
            .style("left", (d3.event.pageX + 10) + "px")
            .style("top", (d3.event.pageY - 10) + "px")
          ;
        }
        else if ("genome" == d.data.rank) {
          species_popup
            .transition()
            .duration(200)
            .style("opacity", .9)
          ;
          species_popup
            .html(
              '<div><span class="header">' + d.data.id + '</span></div>'
              + 'Ploidy: ' + d.data.ploidy + 'x<br/>'
              + 'Chromosome Count: ' + d.data.chromosome_count + '<br/>'
              + 'Genome size: ' + d.data.genome_size + 'Mbp<br/>'
              + 'Version: ' + d.data.version + '<br/>'
            )
            .style("left", (d3.event.pageX + 10) + "px")
            .style("top", (d3.event.pageY - 10) + "px")
          ;
        }
        else {
          species_popup
            .transition()
            .duration(200)
            .style("opacity", .9)
          ;
          species_popup
            .html(
              '<div>' + d.data.id + '</div>'
            )
            .style("left", (d3.event.pageX + 10) + "px")
            .style("top", (d3.event.pageY - 10) + "px")
          ;
        }
      })
      .on("mouseout", function(d) {
        species_popup
          .transition()
          .duration(500)
          .style("opacity", 0)
        ;
      })
    ;

    node_enter.append("text")
      .attr("dy", ".35em")
      .attr("x", function(d) {
        return d.x < 180 === !d.children ? 6 : -6;
      })
      .style("text-anchor", function(d) {
        return d.x < 180 === !d.children ? "start" : "end";
      })
      .attr("transform", function(d) {
        return "rotate(" + (d.x < 180 ? d.x - 90 : d.x + 90) + ")";
      })
      .text(function(d) {
        return (
          d.data.rank.match(/^(species|genome)$/) ?
            d.data.id
            : (d.children ? "" : d.data.id)
        );
      });

    // Update on changes.
    var link_update = link_enter.merge(link);

    // Transition back to the parent element position
    link_update.transition()
        .duration(transition_duration)
        .attr('d', function(d) { return draw_link(d) });

    // Remove any exiting links.
    var link_exit = link.exit().transition()
        .duration(transition_duration)
        .attr('d', function(d) {
          var parent = get_visible_parent(d);
          var center = {
            "x": parent.x0,
            "y": parent.y0,
            "parent": {
              "x": parent.x0,
              "y": parent.y0
            }
          };
          return draw_link(center)
        })
        .remove();

    var node_update = node_enter.merge(node);

    // Transition to the proper position for the node.
    node_update.transition()
      .duration(transition_duration)
      .attr("class", find_node_class)
      .attr("transform", function(d) {
          return "translate(" + project_radial(d.x, d.y) + ")";
       });

    // Update the node attributes and style.
    node_update.select('.node circle')
      .attr('r', node_size)
      .attr('cursor', 'pointer');

    node_update.select('.node text')
      .transition()
      .duration(transition_duration)
      .style("text-anchor", function(d) {
        return d.x < 180 === !d.children ? "start" : "end";
      })
      .attr("x", function(d) {
        return d.x < 180 === !d.children ? 6 : -6;
      })
      .attr("transform", function(d) {
        return "rotate(" + (d.x < 180 ? d.x - 90 : d.x + 90) + ")";
      })
      .text(function(d) {
        return (
          d.data.rank.match(/^(species|genome)$/) ?
            d.data.id
            : (d.children ? "" : d.data.id)
        );
      });


    // Remove any exiting nodes
    var node_exit = node.exit().transition()
        .duration(transition_duration)
        .attr("transform", function(d) {
            return "translate(" + project_radial(d.parent.x, d.parent.y) + ")";
        })
        .remove();

    // On exit reduce the node circles size to 0
    node_exit.select('circle')
      .attr('r', 1e-6);

    // On exit reduce the opacity of text labels
    node_exit.select('text')
      .style('fill-opacity', 1e-6);

    // Store the old positions for transitions.
    nodes.forEach(function(d) {
      d.x0 = d.x;
      d.y0 = d.y;
    });

  }

  // Lists available pangenomes.
  var pangenome_nodes = [];
  function collect_pangenomes(d) {
    if (d.children) {
      d.children.forEach(function(element) {
        // Exclude BRADI as it has too many children for a nice display.
        if (element.data.pangenome && ('BRADI' != element.data.code)) {
          pangenome_nodes.push(element);
        }
        else {
          collect_pangenomes(element);
        }
      });
    }
  }

  var random_node_switch_callback;
  function stop_random_node_switch() {
    continue_random_toggle = false;
    clearTimeout(random_node_switch_callback);
  }
  // Randomly closes and opens nodes.
  function random_node_switch() {
    var expand_node = pangenome_nodes[Math.floor(Math.random() * pangenome_nodes.length)];
    if (expand_node._children) {
      expand_node.children = expand_node._children;
      expand_node._children = null;
    }
    expand_node = pangenome_nodes[Math.floor(Math.random() * pangenome_nodes.length)];
    if (expand_node._children) {
      expand_node.children = expand_node._children;
      expand_node._children = null;
    }
    update_tree(tree_data);
    if (continue_random_toggle) {
      random_node_switch_callback = setTimeout(function() {
          collapse_species(tree_data);
          random_node_switch();
        },
        2000
      );
    }
  }

  // Init display.
  update_tree(tree_data);
  collapse_species(tree_data);
  // Expand 2 random nodes after 2 seconds.
  collect_pangenomes(tree_data);
  random_node_switch_callback = setTimeout(random_node_switch, 2000);

});
[% END %]
</script>
