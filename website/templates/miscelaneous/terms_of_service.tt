<!-- BEGIN miscelaneous/terms_of_service.tt -->

[%#
  Terms of Service should include:
  --------------------------------
  Disambiguation/definition of key words and phrases
  User rights and responsibilities
    Proper or expected usage; potential misuse
    Accountability for online actions, behavior, and conduct
    Privacy policy outlining the use of personal data
    Payment details such as membership or subscription fees, etc.
    Opt-out policy describing procedure for account termination, if available
    Disclaimer/Limitation of Liability clarifying the site's legal liability
      for damages incurred by users
  User notification upon modification of terms, if offered
%]

[% toc_link = BLOCK %]
<div class="right"><a class="jump-link" href="#tos_toc" onclick="$(document).scrollTo('#tos_toc', 500); return false;">^ Back to TOC ^</a></div>
[% END %]

<hr/>
<h3 id="tos_toc">Table of Content</h3>
<ol>
  <li>. <a href="#tos_account">Account Terms</a></li>
  <li>. <a href="#tos_termination">Cancellation and Termination</a></li>
  <li>. <a href="#tos_modifications">Modifications to the Service</a></li>
  <li>. <a href="#tos_copyright">Copyright and Content Ownership</a></li>
  <li>. <a href="#tos_general">General Conditions</a></li>
</ol>
<br/>
<hr/>

<p>
By using the GreenPhyl web site ("Service"), you are agreeing to be bound by
the following terms and conditions ("Terms of Service"). IF YOU ARE ENTERING
INTO THIS AGREEMENT ON BEHALF OF A COMPANY OR OTHER LEGAL ENTITY, YOU REPRESENT
THAT YOU HAVE THE AUTHORITY TO BIND SUCH ENTITY, ITS AFFILIATES AND ALL USERS
WHO ACCESS OUR SERVICES THROUGH YOUR ACCOUNT TO THESE TERMS AND CONDITIONS, IN
WHICH CASE THE TERMS "YOU" OR "YOUR" SHALL REFER TO SUCH ENTITY, ITS AFFILIATES
AND USERS ASSOCIATED WITH IT. IF YOU DO NOT HAVE SUCH AUTHORITY, OR IF YOU DO
NOT AGREE WITH THESE TERMS AND CONDITIONS, YOU MUST NOT ACCEPT THIS AGREEMENT
AND MAY NOT USE THE SERVICES.
</p>

<p>
Please note that if you are accessing any GreenPhyl service in your capacity as
a government entity, there are special terms that may apply to you.
</p>

<p>
If GreenPhyl makes material changes to these Terms, we will notify you by email
if you provided one or by posting a notice on our site at least 2 days before
the changes are effective. Any new features that augment or enhance the current
Service, including the release of new tools and resources, shall be subject to
the Terms of Service. Continued use of the Service after any such changes shall
constitute your consent to such changes. You can review the most current
version of the Terms of Service at any time at:
<a href="[% gpurl('tos') %]" title="GreenPhyl terms of service">[% gpurl('tos') %]</a>.
</p>

<p>
Violation of any of the terms below will result in the termination of your
Account. While GreenPhyl prohibits such conduct and Content on the Service,
you understand and agree that GreenPhyl cannot be responsible for the Content
posted on the Service and you nonetheless may be exposed to such materials.
You agree to use the Service at your own risk.
</p>


<h3 id="tos_account">1. Account Terms</h3>
[% toc_link %]

<p>
You must be 18 years or older to use this Service.
</p>

<p>
You must be a human. Accounts registered by "bots" or other automated methods
are not permitted.
</p>

<p>
You must not create an account using a login that does not correspond to your
legal full name or the name of your company or other legal entity you belong to
as that name may be displayed as the owner of the gene families you publish on
GreenPhyl. If you want to use the name of your company or other legal entity you
belong to as login, you must ensure your are allowed (by your company or
entity) to do so before creating your account.
</p>

<p>
You are responsible for maintaining the security of your account and password.
GreenPhyl cannot and will not be liable for any loss or damage from your failure
to comply with this security obligation.
</p>

<p>
You are responsible for all Content posted and activity that occurs under the
accounts you created.
</p>

<p>
One person or legal entity may maintain up to 5 accounts. If you need more
accounts,
<a href="[% gpurl('contact') | html %]" title="Contact Greenphyl team">contact us</a>
to find an agreement.
</p>

<p>
You may not use the Service for any illegal or unauthorized purpose. You must
not, in the use of the Service, violate any laws in your jurisdiction
(including but not limited to copyright or trademark laws).
</p>

[% toc_link %]

<h3 id="tos_termination">2. Cancellation and Termination</h3>
[% toc_link %]

<p>
You are solely responsible for properly canceling your account. An email or
phone request to cancel your account is not considered cancellation. You can
cancel your account at any time by clicking on the
"<a href="[% gpurl('user_details') %]">My account</a>" item in the
"<a href="[% gpurl('my_space_page') %]">My Space</a>" menu in the global
navigation bar at the top of the screen. The User details screen (Account page)
provides a simple checkbox to check before using the "Save" button to
permanently remove your account.
</p>

<p>
All of your Content (except submitted family annotations) will be immediately
deleted from the Service upon removal. This information can not be recovered
once your account is removed.
</p>

<p>
GreenPhyl, in its sole discretion, has the right to suspend your account and
refuse any and all current or future use of the Service using your account, at
any time if GreenPhyl feels you are not using the Service in the purpose it has
been designed for. Once your account has been suspended, you have 30 days to
<a href="[% gpurl('contact') | html %]" title="Contact Greenphyl team">contact</a>
GreenPhyl and try to negociate a reactivation of your account. After
these 30 days, GreenPhyl may or may not, in its sole discretion, proceed to
your account permanent removal.
</p>

[% toc_link %]

<h3 id="tos_modifications">3. Modifications to the Service</h3>
[% toc_link %]

<p>
GreenPhyl reserves the right at any time and from time to time to modify or
discontinue, temporarily or permanently, the Service (or any part thereof)
with or without notice.
</p>

<p>
GreenPhyl shall not be liable to you or to any third party for any
modification, suspension or discontinuance of the Service.
</p>

[% toc_link %]

<h3 id="tos_copyright">4. Copyright and Content Ownership</h3>
[% toc_link %]

<p>
We claim no intellectual property rights over the material you provide to the
Service. Your profile and materials uploaded remain yours. However, by setting
parts or all of your custom gene sequences and families to be viewed publicly,
you agree to allow others to view and use your Content for their research.
</p>

<p>
GreenPhyl does not pre-screen Content, but GreenPhyl and its designee have the
right (but not the obligation) in their sole discretion to refuse or
remove any Content that is available via the Service and that does not suite
GreenPhyl policy.
</p>

<p>
The look and feel of the Service is copyright &copy; 2014 Bioversity
International. All rights reserved. You may not duplicate, copy, or reuse any
portion of the HTML/CSS, Javascript, or visual design elements or concepts
without express written permission from GreenPhyl.
</p>

[% toc_link %]

<h3 id="tos_general">5. General Conditions</h3>
[% toc_link %]

<p>
Your use of the Service is at your sole risk. The service is provided on an
"as is" and "as available" basis.
</p>

<p>
Support for GreenPhyl services is free, only available in English, via email.
GreenPhyl has no obligation to provide any support for its Service but will
intend to answer any relevant query made to its support team.
</p>

<p>
You understand that GreenPhyl uses hosting partners to provide the necessary
hardware, software, networking, storage, and related technology required to run
the Service.
</p>

<p>
You must not modify, adapt or hack the Service or modify another website so as
to falsely imply that it is associated with the Service, GreenPhyl, or any
other GreenPhyl service.
</p>

<p>
You agree not to sell, resell or exploit any portion of the Service, use of the
Service, or access to the Service without the express written permission by
GreenPhyl.
</p>

<p>
We may, but have no obligation to, remove Content and Accounts containing
Content that we determine in our sole discretion are unlawful, offensive,
threatening, libelous, defamatory, pornographic, obscene or otherwise
objectionable or violates any party's intellectual property or these Terms of
Service.
</p>

<p>
You understand that the technical processing and transmission of the Service,
including your Content, may be transfered unencrypted and involve (a)
transmissions over various networks; and (b) changes to conform and adapt to
technical requirements of connecting networks or devices.
</p>

<p>
You must not upload, post, host, or transmit unsolicited email, SMSs, or "spam"
messages.
</p>

<p>
You expressly understand and agree that GreenPhyl shall not be liable for any
direct, indirect, incidental, special, consequential or exemplary damages,
including but not limited to, damages for loss of profits, goodwill, use, data
or other intangible losses (even if GreenPhyl has been advised of the
possibility of such damages), resulting from: (i) the use or the inability to
use the Service; (ii) the cost of procurement of substitute goods and services
resulting from any goods, data, information or services purchased or obtained
or messages received or transactions entered into through or from the Service;
(iii) unauthorized access to or alteration of your transmissions or data; (iv)
statements or conduct of any third party on the Service; (v) or any other
matter relating to the Service.
</p>

<p>
The failure of GreenPhyl to exercise or enforce any right or provision of the
Terms of Service shall not constitute a waiver of such right or provision. The
Terms of Service constitutes the entire agreement between you and GreenPhyl and
govern your use of the Service, superseding any prior agreements between you
and GreenPhyl (including, but not limited to, any prior versions of the Terms
of Service). You agree that these Terms of Service and Your use of the Service
are governed under French law.
</p>

[% toc_link %]

<hr/>

<!-- END miscelaneous/terms_of_service.tt -->
