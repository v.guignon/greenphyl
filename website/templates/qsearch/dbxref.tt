<!-- BEGIN qsearch/dbxref.tt -->
[%#

Parameters:
-search_title:
-db_name:
-dbxref_label:

%]
<h3>[% search_title %] '[% search_text | html %]'</h3>


[% IF ((!results) || (0 == results.sequences.size)) %]
   <br/>
   <div>No match found.</div>
[% ELSE %]
  [% WRAPPER dump_links.tt
    dump_data =
    {
      links = [
        {
          label       = 'Excel'
          format      = 'excel'
          file_name   = 'sequences.xls'
          namespace   = 'excel'
        }
        {
          label       = 'CSV'
          format      = 'csv'
          file_name   = 'sequences.csv'
          namespace   = 'csv'
        }
        {
          label       => 'XML'
          format      => 'xml'
          file_name   => 'sequences.xml'
          namespace   => 'xml'
        }
      ]
      parameters  =
        {
          columns = "{'01.Sequence'=>'accession','02.DBXRef'=>'getCGIMember.dbxref','03.Accession'=>'getCGIMember.accession','04.Description'=>'getCGIMember.description','05.Annotation'=>'annotation','06.lev_1_fam_id'=>'families.0.accession','07.lev_1_fam_name'=>'families.0.name','08.lev_1_fam_validated'=>'families.0.validated','09.lev_2_fam_id'=>'families.1.accession','10.lev_2_fam_name'=>'families.1.name','11.lev_2_fam_validated'=>'families.1.validated','12.lev_3_fam_id'=>'families.2.accession','13.lev_3_fam_name'=>'families.2.name','14.lev_3_fam_validated'=>'families.2.validated','15.lev_4_fam_id'=>'families.3.accession','16.lev_4_fam_name'=>'families.3.name','17.lev_4_fam_validated'=>'families.3.validated'}"
        }
      object_type = 'sequences'
      fields      = 'seq_id'
      mapping     = {'seq_id' = 'id'}
      mode        = 'form'
    }
  %]
    <br class="clear"/>
    [% INCLUDE mylists/mylist_link_multi.tt
      mapping = {
        'Sequence' = 'seq_id'
      }
    %]
    <br/>
    <span class="header">Matching sequences:</span><br/>
    <br/>
    [%
      additional = {
        columns = [
          {
            label  = 'Description'
            member = 'dbxref.description'
            style  = ''
            hidden_field = 'description'
            highlight = search_text
          }
          {
            label  = 'Annotation'
            member = 'sequence.annotation'
            style  = ''
            hidden_field = 'annotation'
          }
        ]
        values = results.dbxrefs
      }
    %]
    [% IF with_synonyms %]
      [%
        additional.columns.unshift(
        {
            label  = 'Synonyms'
            member = 'dbxref.synonyms.join(",")'
            style  = ''
            hidden_field = 'synonyms'
            highlight = search_text
        })
      %]
    [% END %]
    [% IF dbxref_label && dbxref_member %]
      [% BLOCK render_dbxref_list %]
        [% FOR dbxref IN dbxref_list %]
          [% INCLUDE dbxref/dbxref_accession.tt
            with_form_field = 0
          %]:
          [%= INCLUDE highlight.tt
            content = dbxref.description
            highlight_text = search_text
          ~%]<br/>
        [% END %]
      [% END %]
      [%
        additional.columns.unshift(
        {
            label  = dbxref_label
            member = dbxref_member
            style  = ''
            hidden_field = 'dbxref'
            template = 'render_dbxref_list'
            template_parameters =
            {
              'dynamic' =
              {
                'dbxref_list' = 'member_value'
              }
            }
        })
      %]
    [% ELSE %]
      [%
        additional.columns.unshift(
        {
          label  = 'Accession'
          member = 'dbxref'
          style  = ''
          hidden_field = 'accession'
          template = 'dbxref/dbxref_accession.tt'
          template_parameters = {
            dynamic = {
              dbxref = 'member_value'
            }
            static = {
              with_form_field = 1
              highlight = search_text
            }
          }
        })
      %]
    [% END %]
    [% INCLUDE sequences/sequence_list.tt
      sequences = results.sequences
      identifier = db_name
      paged = 1
      columns = []
      with_form_field = 'checkbox'
    %]
    <br class="clear"/>
    [% INCLUDE mylists/mylist_link_multi.tt
      mapping = {
        'Sequence' = 'seq_id'
      }
    %]
    <br/>
    [%# INCLUDE legend.tt  # enable it if 'with_classification' is set to true
      legends = ['families/family_legend.tt']
    %]
  [% END %]
[% END %]

<div id="result_count_[% db_name %]" class="result-count">
[%- IF ((!results) || (0 == results.sequences.size)) -%]
  no match
[%- ELSIF too_many_results -%]
  <span class="tooltiped with_tooltip_too_many_results">too many results</span>
[%- ELSE -%]
  [% results.sequences.size %] sequence[% (results.sequences.size > 1) ? 's' : ''%]
[%- END -%]
</div>

<!-- END qsearch/dbxref.tt -->
