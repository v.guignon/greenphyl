<!-- BEGIN qsearch/intepro_fam.tt -->
<h3>Search result for families containing Interpro entries related to '[% search_text | html %]'</h3>

[% result_count = 0 %]
[% IF results.size == 0 %]
   <br/>
   <div>No match found.</div>
[% ELSE %]
  [% WRAPPER dump_links.tt
    dump_data =
    {
      links = [
        {
          label       = 'Excel'
          format      = 'excel'
          file_name   = 'ipr_families.xls'
          namespace   = 'excel'
        }
        {
          label       = 'CSV'
          format      = 'csv'
          file_name   = 'ipr_families.csv'
          namespace   = 'csv'
        }
        {
          label       => 'XML'
          format      => 'xml'
          file_name   => 'ipr_families.xml'
          namespace   => 'xml'
        }
      ]
      parameters  =
        {
          columns = "{'01.IPR'=>'getCGIMembers.ipr_code.*','02.Family ID'=>'accession','03.Family  Name'=>'name','04.Sequences count'=>'sequence_count','05.Status'=>'validated','06.Sequences with IPR'=>'getCGIMembers.seq_percent.*'}"
        }
      object_type = 'families'
      fields      = 'family_id'
      mapping     = {'family_id' = 'id'}
      mode        = 'form'
    }
  %]
    <br class="clear"/>
    [% INCLUDE mylists/mylist_link_multi.tt
      mapping = {
        'Family' = 'family_id'
      }
    %]
    <br/>
    [% items = [] %]
    [% FOR result IN results %]
      [% item = {
           block_id = 'ipr_fam_result_' _ result.ipr.code
         }
      %]
      [% item.title = BLOCK %]
        [% INCLUDE ipr/ipr_code.tt
          ipr = result.ipr
        ~%]:
        [%= INCLUDE highlight.tt
          content = result.ipr.description
          highlight_text = search_text
        %]
      [% END %]
      [% item.link_label = BLOCK %]
        [%~ IF (0 == result.families.size) ~%]
          no match
        [%~ ELSIF (1 == result.families.size) ~%]
          1 family
        [%~ ELSIF (max_results == result.families.size) ~%]
          too many families
        [%~ ELSE ~%]
          [% result.families.size %] families
        [%~ END ~%]
      [% END %]
      [% items.push(item); %]
    [% END %]
    [% INCLUDE jump_list.tt 
      list_id = 'matching_fam_ipr_list'
      list_header = 'Matching IPR:'
      with_master_checkboxes = 1
    %]
    <br/>
    <span class="header">Detailed results:</span><br/>
    <br/>
    [% FOR result IN results %]
      <br/>
      [%~ FOR family IN result.families -%]
      <input type="hidden" name="[% family %].ipr_code" value="[% result.ipr.code %]"/>
      [%~ END -%]
      <fieldset id="ipr_fam_result_[% result.ipr.code %]">
        <legend>
          Families found for
          [% INCLUDE ipr/ipr_code.tt
            ipr = result.ipr
          ~%]:
          [% INCLUDE highlight.tt
            content = result.ipr.description
            highlight_text = search_text
          %]
        </legend>
        [% IF result.families.size %]
            <label><input type="checkbox" checked="checked" class="check-all"/>Select all ([% (1 == result.families.size) ? '1 family' : result.families.size _ ' families' %])</label><br/>
            [% INCLUDE families/family_list.tt
              families = result.families
              identifier = 'ipr_family_list'
              paged = 1
              additional = {
                columns = [
                  {
                    label  = '% of sequences bearing ' _ result.ipr.code
                    member = ''
                    style  = 'right'
                    hidden_field = 'seq_percent'
                  }
                ]
                values = result.ipr_stat
              }
              with_form_field = 'checkbox'
            %]
            [% result_count = result_count + result.families.size %]
        [% ELSE %]
          <p>No matching family found!</p>
        [% END %]
      </fieldset>
      [% INCLUDE jump_back.tt
        list_id = 'matching_fam_ipr_list'
      %]
      <br/>
    [% END %]
  [% END %]
[% END %]

[%- IF results.size -%]
  [% INCLUDE legend.tt
    legends = ['families/family_legend.tt']
  %]
[%- END -%]

<div id="result_count_interpro_fam" class="result-count">
[%- IF 0 == results.size -%]
  no match
[%- ELSIF too_many_results -%]
  <span class="tooltiped with_tooltip_too_many_results">too many results</span>
[%- ELSE -%]
  [% result_count %] famil[% (result_count > 1) ? 'ies' : 'y'%]
[%- END -%]
</div>

<!-- END qsearch/intepro_fam.tt -->
