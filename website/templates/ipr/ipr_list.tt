<!-- BEGIN ipr/ipr_list.tt -->
[%#
Template ipr/ipr_list.tt

Description:
 This template displays a list of ipr details.

Parameters:
-ipr_list: the list of ipr object
-identifier: HTML identifier for the table. Default: 'ipr_list'.
-columns: array of built-in columns to add. Possible values are:
  description, type, domain.
  Default: none.
-additional.columns: an array of hash having a column label in a 'label' key and
 a member chain in a 'member' key. 'member' can be empty if the value can be
 accessed directly from the seq_id (see below).
 Additional keys can be used:
 'style' to specify column CSS style
 'header_style' to specify column header CSS style
 'hidden_field' to add a hidden field with name seq_id.hidden_field
 'highlight' to highlight a given string
-additional.values: a hash which keys are seq_id and values are
  composite objects which have members corresponding to additional.columns
  values.
-paged: a boolean telling if the list should be paged.
  Default: not paged.
-not_sortable: a boolean telling if the list can be sorted using Javascript.
  Default: sortable.
-before_list: header to display before the list.
-with_dump_links: if set to true, dump links are rendered.

~%]
[%~ IF !identifier ~%]
  [% USE Math %]
  [%~
    identifier = GetFreeHTMLIdentifier('ipr_list_' _ Math.int(1000 * Math.rand()))
  ~%]
[%~ END ~%]

[% IF before_list.match('^[\w/-]+\.tt$') && before_list != 'ipr/ipr_list.tt' -%]
  [%- INCLUDE $before_list -%]
[%- ELSE -%]
  [%- before_list -%]
[%- END %]

[%~ IF ipr_list.size ~%]
  [%~ ipr_table = BLOCK ~%]
    [%~ IF with_dump_links && !with_form_field ~%]
      [%~ with_form_field = 'hidden' ~%]
    [%~ END ~%]
    <table class="table table-hover table-bordered"
      id="[% GetFreeHTMLIdentifier(identifier) %]"
      data-toggle="table"
      data-sortable="[% not_sortable ? 'false':'true' %]"
      data-pagination="[% paged ? 'true':'false' %]"
      data-search="false"
    >
      <thead>
        <tr>
          <th>IPR</th>
          [% FOREACH column IN columns.list %]
            [%~ IF 'description' == column %]
              <th>Description</th>
            [%~ ELSIF 'type' == column %]
              <th>Type</th>
            [%~ ELSIF 'domain' == column %]
              <th>Domain</th>
            [%~ END %]
          [%~ END %]
          [% INCLUDE add_columns.tt
            header = 1
          %]
        </tr>
      </thead>
      <tbody>
      [%~ FOREACH ipr IN ipr_list ~%]
        <tr>
          <td class="left">[% INCLUDE ipr/ipr_code.tt %]</td>
          [% FOREACH column IN columns.list %]
            [%~ IF 'description' == column %]
              <td>
                [% ipr.description | html %]
              </td>
            [%~ ELSIF 'type' == column %]
              <td>
                [% ipr.type %]
              </td>
            [%~ ELSIF 'domain' == column %]
              <td>
                [% INCLUDE ipr/ipr_domain.tt %]
              </td>
            [%~ END %]
          [%~ END %]
    
          [% INCLUDE add_columns.tt
            current_object = ipr
            identifier = ipr
          %]
        </tr>
      [%~ END %]
      </tbody>
    </table>
  [%~ END %]

  [%~ IF with_dump_links ~%]
    [% INCLUDE dump_links.tt
      content = ipr_table
    %]
  [%~ ELSE ~%]
    [% ipr_table %]
  [%~ END ~%]

[%~ ELSE ~%]
    No IPR.
[%~ END %]
<!-- END ipr/ipr_list.tt -->
