<!-- BEGIN add_columns.tt -->
[%#
Template add_columns.tt

Description:
 This template adds columns to a table.

Parameters:
-header: if set to true, generates headers
-identifier: current element identifier (useless if header is set)
-additional.columns: an array of hash having a column label in a 'label' key and
 a member chain in a 'member' key. 'member' can be empty if the value can be
 accessed directly from the identifier (see below).
 If 'member' starts with 'current_object.', then the parameter 'current_object'
 is used to fetch the value from the remining part of the member chain instead
 of 'additional.values'.
 Additional keys can be used:
 'style' to specify column CSS style
 'header_style' to specify column header CSS style
 'hidden_field' to add a hidden field with name identifier.hidden_field
 'template' a template to use to render element
 'template_parameters' a hash of hash of parameter names as key and associated
   values passed to the template if one specified. The first main hash contains
   2 keys: static and dynamic. Values in the static hash ar used as is while
   values in the dynamic hash are interpolated using ${}.
   See local_template.tt for details.
 'highlight' to highlight a given string. Note: if template parameter is used,
   the used template will have to do the highlighting thing
-additional.values: a hash which keys are seq_id and values are
  composite objects which have members corresponding to additional.columns
  values.

Example:
  See sequences/sequence_list.tt

~%]

[%~ IF header %]
  [%~ FOREACH additional_column IN additional.columns %]
    <th class="[%~ additional_column.header_style ~%]"[% sortable ? ' data-sortable="true"':'' %]>[% additional_column.label ~%]</th>
  [%~ END %]
[%~ ELSE %]
  [%- FOREACH additional_column IN additional.columns ~%]
    <td class="wrapword [% additional_column.style ~%]">
      [%~ member_value = '' ~%]
      [%~ IF additional_column.member ~%]
        [%~
          # getting the member value here is quite tricky:
          # values are stored in additional.values hash or in 'current_object'
          # and the key to used is provided by "identifier" which may contain
          # dots which shouldn't be interpreted as member separators! But the
          # part "additional_column.member" may contain dots that must be
          # interpreted. That's why we use a 2 step processing.
          # first, we get a direct pointer on the member part
        ~%]
        [%~ IF additional_column.member.match('^current_object\.') ~%]
          [%~
            member_value = current_object
            # we also need to remove 'current_object.' from member path when using 'current_object'
            member_path = additional_column.member.substr(15) # 15 = length('current_object.')
          ~%]
        [%~ ELSE ~%]
          [%~
            member_value = additional.values.${identifier}
            member_path = additional_column.member
          ~%]
        [%~ END ~%]
        [%~
          # then we compute the member "path" to the value
          member_path  = 'member_value.' _ member_path

          # and finaly we get the value
          member_value = ${member_path}
        ~%]
      [%~ ELSE ~%]
        [%~ member_value = additional.values.${identifier} ~%]
      [%~ END ~%]
      [% IF additional_column.template ~%]
        [%~ INCLUDE local_template.tt
          template = additional_column.template
          parameters = additional_column.template_parameters
        ~%]
      [% ELSIF additional_column.block ~%]
        [%~ INCLUDE $additional_column.block ~%]
      [% ELSIF additional_column.highlight ~%]
        [%= INCLUDE highlight.tt
          content = member_value
          highlight_text = additional_column.highlight
        %]
      [%~ ELSE ~%]
        [%~ member_value ~%]
      [%~ END ~%]
      [%~ IF additional_column.hidden_field ~%]
        <input type="hidden" name="[%~ identifier _ '.' _ additional_column.hidden_field ~%]" value="[%~ member_value | html ~%]" />
      [%~ END ~%]
    </td>
  [%- END ~%]
[%~ END %]

<!-- END add_columns.tt -->
