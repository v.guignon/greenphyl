<!-- BEGIN tools/get_homologs_results.tt -->

[%#

Display the homology and similarity results.

Parameters:
* species_list: an array of selected species objects
* homology_data: an array of hash. Each hash has key 'sequence' containing the
  sequence object corresponding to user input and a key homologs containing an
  array of homolog/similar sequence objects.
* output_homologs: a boolean telling if homolog column should be displayed
* output_bbmh: a boolean telling if bbmh column should be displayed

~%]
<br/>
<span class="header">Selected species:</span>
<span style="font-style: italic;">
[%~ first_species = 1 ~%]
[%~ FOR species IN species_list ~%]
  [% first_species ? '' : ',' %]
  [%~ INCLUDE species/species_name.tt ~%]
  [%~ first_species = 0 ~%]
[%~ END %]
</span>
<br/>
<br/>
[% WRAPPER dump_links.tt %]
    [% items = [] %]
    [% FOR sequence_homology IN homology_data %]
      [% item = {
           block_id = 'seq_' _ sequence_homology.sequence.id
         }
      %]
      [% item.title = BLOCK %]
        Homology for 
        [%- INCLUDE sequences/sequence_id.tt 
          sequence = sequence_homology.sequence
        %]
      [% END %]
      [% item.link_label = BLOCK %]
        [%~ IF (0 == sequence_homology.homologs.size) ~%]
          no match found
        [%~ ELSIF (1 == sequence_homology.homologs.size) ~%]
          1 sequence
        [%~ ELSE ~%]
          [% sequence_homology.homologs.size %] sequences
        [%~ END ~%]
      [% END %]
      [% items.push(item); %]
    [% END %]
    [% INCLUDE jump_list.tt 
      list_id = 'sequences_list'
      list_header = 'Sequence summary:'
      with_master_checkboxes = 1
    %]
  <br/>
  [% FOR sequence_homology IN homology_data %]
    <br/>
    <fieldset id="seq_[% sequence_homology.sequence.id %]">
      <legend>
        Homology for
        [%- INCLUDE sequences/sequence_id.tt 
          sequence = sequence_homology.sequence
        ~%]:
      </legend>
      [% IF sequence_homology.homologs.size %]
        <br/>
        <label><input type="checkbox" checked="checked" class="check-all"/>Select all ([% (1 == sequence_homology.homologs.size) ? '1 sequence' : sequence_homology.homologs.size _ ' sequences' %])</label><br/>
        <table class="table table-hover table-bordered table-striped"
          data-toggle="table"
          data-sortable="true"
          data-pagination="false"
          data-search="false"
        >
          <thead>
            <tr>
              <th></th>
              <th></th>
              [%- IF output_homologs ~%]
              <th>Homology</th>
              <th></th>
              <th></th>
              [%- END ~%]
              [%- IF output_bbmh ~%]
              <th>RBH</th>
              <th></th>
              [%- END ~%]
            </tr>
            <tr>
              <th data-sortable="true">Sequence ID</th>
              <th data-sortable="true">Species</th>
              [%- IF output_homologs ~%]
              <th data-sortable="true">Type</th>
              <th data-sortable="true">Evolutionary distance</th>
              <th data-sortable="true">Node distance</th>
              [%- END ~%]
              [%- IF output_bbmh ~%]
              <th data-sortable="true">score</th>
              <th data-sortable="true">e-value</th>
              [%- END ~%]
            </tr>
          </thead>
          <tbody>
          [% FOR homolog IN sequence_homology.homologs %]
          [%~ homosim_id = sequence_homology.sequence.id _ '_' _ homolog.id ~%]
            <tr>
              <td>
                <input type="hidden" name="[%~ homosim_id ~%].query" value="[%~ sequence_homology.sequence ~%]"/>
                <input type="hidden" name="[%~ homosim_id ~%].homolog" value="[%~ homolog ~%]"/>
                <input type="checkbox" name="composite_id" value="[%~ homosim_id ~%]"/>
                [%~ INCLUDE sequences/sequence_id.tt 
                  sequence = homolog
                  with_form_field = 'hidden'
                ~%]
              </td>
              <td>
                <input type="hidden" name="[%~ homosim_id ~%].query_species" value="[%~ sequence_homology.sequence.species.organism ~%]"/>
                <input type="hidden" name="[%~ homosim_id ~%].homolog_species" value="[%~ homolog.species.organism ~%]"/>
                [%~ INCLUDE species/species_name.tt 
                  species = homolog.species ~%]
              </td>
              [%- IF output_homologs ~%]
                [%~ homology = sequence_homology.sequence.getHomologyWith(homolog) ~%]
                [%~ IF homology ~%]
                <td class="right">[%~ homology.type ~%]<input type="hidden" name="[%~ homosim_id ~%].homology_type" value="[%~ homology.type ~%]"/></td>
                <td class="right">[%~ homology.distance | format('%.3g') ~%]<input type="hidden" name="[%~ homosim_id ~%].homology_distance" value="[%~ homology.distance | format('%.3g') ~%]"/></td>
                <td class="right">[%~ homology.speciation ~%]<input type="hidden" name="[%~ homosim_id ~%].homology_speciation" value="[%~ homology.speciation ~%]"/></td>
                [%~ ELSE ~%]
                <td class="right">-</td>
                <td class="right">-</td>
                <td class="right">-</td>
                [%- END ~%]
              [%- END ~%]
              [%- IF output_bbmh ~%]
                [%~ bbmh = sequence_homology.sequence.getBBMHWith(homolog) ~%]
                [%~ IF bbmh ~%]
                <td class="right">[%- bbmh.score ~%]<input type="hidden" name="[%~ homosim_id ~%].bbmh_score" value="[%~ bbmh.score ~%]"/></td>
                <td class="right">[%- bbmh.evalue | format('%.3g') ~%]<input type="hidden" name="[%~ homosim_id ~%].bbmh_evalue" value="[%~ bbmh.evalue | format('%.3g') ~%]"/></td>
                [%- ELSE ~%]
                <td class="right">-</td>
                <td class="right">-</td>
                [%- END ~%]
              [%- END ~%]
            </tr>
          [% END %]
          </tbody>
        </table>
        <script>
          $(function() {$('#seq_[% sequence_homology.sequence.id %] table').bootstrapTable();});
        </script>

      [% ELSE %]
        [%- IF output_homologs ~%]
          <div>No homolog found!</div>
        [%- ELSIF output_bbmh ~%]
          <div>No RBH found!</div>
        [%- ELSE ~%]
          <div>No homolog or RBH found!</div>
        [% END %]
      [% END %]
    </fieldset>
    [% INCLUDE jump_back.tt
      list_id = 'sequences_list'
    %]
    <br/>
  [% END %]
[% END %]

<!-- END tools/get_homologs_results.tt -->
