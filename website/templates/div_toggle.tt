<!-- BEGIN div_toggle.tt -->
[%#
Template div_toggle.tt

Description:
 This template displays a [+/-] button before a block title and enables block
 display/hide.

Parameters:
 start_visible: default status for visibility
 id: HTML ID of the block to hide/show;
 label: the block title;
 label_show: the block title when hidden;
 label_hide: the block title when displayed;
 header_content: content always displayed between title and block (visible or
   not);
 content: the block content;

Notes:
* this template is used by Greenphyl::Web::RenderToggler(...) function.
  
Ex.:
[#% toggle_content = BLOCK %#]
  bla bla bla...
[#% END %#]

[#% INCLUDE div_toggle.tt
    id='my_data'
    label='My data'
    content=toggle_content
    start_visible=1
%#]

Ex.2:
[#% WRAPPER div_toggle.tt
    id='my_data'
    label='My data'
    start_visible=1
%#]
  bla bla bla...
[#% END %#]

Ex.3:
[#% WRAPPER div_toggle.tt
    id='my_data'
    label_show='View'
    label_hide='Collapse'
    start_visible=0
%#]
  bla bla bla...
[#% END %#]

~%]
[%~ IF !id ~%]
  [%~ id = 'block_toggler' ~%]
[%~ END ~%]
[%~ id = GetFreeHTMLIdentifier(id) || id  # "|| id" --> for retro-compatibility when function is not available ~%]
[%~ IF !label_show ~%]
  [%~ label_show = label ~%]
[%~ END ~%]
[%~ IF !label_hide ~%]
  [%~ label_hide = label ~%]
[%~ END ~%]

  <div id="toggle_container_[% id %]" class="toggle-container [% start_visible ? 'toggle-container-opened' : 'toggle-container-closed'%]">
    <div class="header clickable" id="toggler_[% id %]" onclick="toggleBlock('[% id %]'); return false;">
      <img class="toggler" id="toggler_icon_[% id %]" src="[% gpurl('images', undef, {'path' = '/' _ (start_visible ? 'minus.png' : 'plus.png')}) | html %]" alt="[% start_visible ? 'Close' : 'Open' %]" title="[% start_visible ? 'Close' : 'Open' %]" /> <span id="toggler_label_show_[% id %]" style="[% start_visible ? 'display: none;' : '' %]">[% label_show %]</span> <span id="toggler_label_hide_[% id %]" style="[% start_visible ? '' : 'display: none;' %]">[% label_hide %]</span>
    </div>

    [% IF header_content %]<div class="header-content">[% header_content %]</div>[% END %]
    
    <div id="[% id %]">
      [% content %]
    </div>
    [%#
      we use Javascript to hide the block if it should be hidden at the
      begining instead of CSS because if the user disables Javascript
      he/she wouldn't be able to see the block.
    %]
    [% IF 0 == start_visible %]
    <script type="text/javascript">
      [% WRAPPER javascript.tt %]
      $(function() {
        $('#[% id %]').hide();
      });
      [% END %]
    </script>
    [% END %]
  </div>
<!-- END div_toggle.tt -->