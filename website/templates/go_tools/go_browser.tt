<!-- BEGIN go_tools/go_browser.tt -->
[%#
Template go_browser.tt

Description:
 This template render the GO Browser.

Parameters:
  max_depth

%]

[% IF !max_depth.defined %]
  [% max_depth = 2 %]
[% END %]

<a href="[% gpurl('documentation', {'page' = 'methodology'}, { 'anchor' = 'goslim'}) | html %]" class="help-link" title="Click for help (popup)" [% link_target %]>Info ?</a>
<br/>
<br/>
<div id="tabs" class="tabs-container">
  <ul>
    [% FOREACH root_term IN go_roots %]
      <li><a href="#tab-[% root_term.name %]">[% root_term.name.replace('_', ' ') | ucfirst %]</a></li>
    [% END %]
  </ul>

  [% FOREACH root_term IN go_roots %]

    <div id="tab-[% root_term.name %]">

      <script type="text/javascript">
      [% WRAPPER javascript.tt %]
        var go_family_counts = new Array();
        $(function () {
          // GO Trees
          $("#go_tree_[% root_term.name %]").treeview({
            persist: 'location',
            url: '[% gpurl('get_go_subtree', {'mode' = 'frame'}) %]',
            // collapsed: true,
            animated: 'fast',
            control: '#treecontrol-[% root_term.name %]'
          });
          // developpe first level
          $("#go_tree_[% root_term.name %]").find('>li>.hitarea').click();
        });

      [% END %]
      </script>

      <br/>
      <div id="treecontrol-[% root_term.name %]">
        <a title="Expand the entire tree below" href="#" class="treeview_expand"><img src="[% gpurl('images', undef, { 'path' = '/plus.png' }) %]"  alt="expand"/> Expand All</a>
        <a title="Collapse the entire tree below" href="#" class="treeview_collapse"><img src="[% gpurl('images', undef, { 'path' = '/minus.png' }) %]" alt="collapse"/> Collapse All</a>
        <a title="Toggle the tree below, opening closed branches, closing open branches" href="#" class="treeview_toggle"><img src="[% gpurl('images', undef, { 'path' = '/toggle.png' }) %]" alt="toggle"/> Toggle All</a>
      </div>

      <ul class="go-tree" id="[% 'go_tree_' _ root_term.name %]">
        [%- INCLUDE go_tools/go_subtree.tt
          go = root_term
        %]
      </ul>

    </div>
  
  [% END %]
</div>

<fieldset style="background-color: #f5f5f5">
  <legend>Legend</legend>
  <br/>
  GO:&lt;CODE&gt;: &lt;term name&gt; (number of directly linked gene families /
  total number of directly linked gene families + underlying gene families)<br/>
  <br/>
  <div class="horizontal-container">
    <div class="block" style="width: 49%;">
      <img src="[% gpurl('images', undef, { 'path' = '/is_a.gif'}) | html %]" alt="is-a" /> is a
    </div>
    <div class="block" style="width: 49%;">
      <img src="[% gpurl('images', undef, { 'path' = '/part_of.gif'}) | html %]" alt="part-of" /> part of
    </div>
    <div class="block" style="width: 49%;">
      <img src="[% gpurl('images', undef, { 'path' = '/regulates.gif'}) | html %]" alt="regulates" /> regulates
    </div>
    <br class="clear"/>
  </div>
</fieldset>
<div class="note">
  <span class="header">Note:</span> you can see directly linked families
  (if some) by clicking on the number between parentheses.<br/>
</div>

[%# <div name="div_chart" id="div_chart"></div> %]

<br />

<!-- END go_tools/go_browser.tt -->
