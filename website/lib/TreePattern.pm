=pod

=head1 NAME

TreePattern - TreePattern tools

=head1 SYNOPSIS


=head1 REQUIRES

Perl5

=head1 EXPORTS

SubmitTreePatternJob CheckTreePatternJob GetTreePatternJobResults AnnotateTreeWithPattern RemoveOldTreePatternJob

=head1 DESCRIPTION

Provides several function to use TreePattern.

TreePattern protcol (through telnet):
Note:
* leading '>' and '<' should be removed and are here to indicate respectivly
output stream (sent to TreePattern daemon) and input stream (received from
TreePattern daemon)
* String delimited by square brackets ('[]') are place holders described below
the stream exchange.

1) Search database for a pattern:
----------
>patternSearch
>[DATABASE]
>[PATTERN]
<[JOB_ID]
<[MATCH_COUNT]
----------

Place holders:
[DATABASE]: a database name.
ex.: GreenPhyl

[PATTERN]: a tree pattern to search.
ex.: Viridiplantae:-1[<R>Viridiplantae</R><S>1</S>];

[JOB_ID]: unique job identifier corresponding to current query.
ex.: b40d4641-12a3-4921-8e42-f91ba7914d52

[MATCH_COUNT]: number of match found.
ex.: 9894


2) Retrieve result list:
----------
>resultSplit
>[JOB_ID]
<[OFFSET]
<[LENGTH]
----------
Place holders:

[JOB_ID]: a unique job identifier obtained from patternSearch.
ex.: b40d4641-12a3-4921-8e42-f91ba7914d52

[OFFSET]: the result offset of the result list from which results should be
returned.
ex.: 1

[LENGTH]: number of results that should be returned (starting from the given
offset).
ex.: 40

3) Retrieve a newick pattern-annotated tree:
----------
>patternNewick
>[DATABASE]
>[TREE_FILE]
>[PATTERN]
<[NEWICK]
----------

Place holders:
[DATABASE]: a database name.
ex.: GreenPhyl

[TREE_FILE]: a tree file name given in the search results.
ex.: 4990_rap_tree.xml

[PATTERN]: a tree pattern to use for annotation.
ex.: Viridiplantae:-1[<R>Viridiplantae</R><S>1</S>];

[NEWICK]: a newick pattern-annotated tree.
ex.: (((COLORED_Cre17.g711350.t1.1_CHLRE:5.2E-8,(COLORED_Cre01.g061400.t1.1_CHLRE:1.0E-10,(COLORED_Cre17.g698300.t1.1_CHLRE:1.0E-10,COLORED_Cre17.g738650.t1.1_CHLRE:1.323E-7)D_0.0:9.9E-8)D_0.0:5.2E-8)D_1.0:0.9443198643,((COLORED_Cre10.g453200.t1.1_CHLRE:1.0E-10,COLORED_Cre13.g599950.t1.1_CHLRE:1.0E-10)D_0.999:0.1034750272,(COLORED_Cre08.g379150.t1.1_CHLRE:0.0107210338,(COLORED_Cre16.g672700.t1.1_CHLRE:4.13E-8,(COLORED_Cre12.g490950.t1.1_CHLRE:1.0E-10,(COLORED_Cre17.g730900.t1.1_CHLRE:1.0E-10,(COLORED_Cre06.g255850.t1.1_CHLRE:1.0E-10,((COLORED_Cre09.g400900.t1.1_CHLRE:1.0E-10,COLORED_Cre11.g469550.t1.1_CHLRE:1.0E-10)D_0.0:6.69E-8,(COLORED_Cre17.g730850.t1.1_CHLRE:1.0E-10,(COLORED_Cre17.g697850.t1.1_CHLRE:1.0E-10,COLORED_Cre50.g789950.t1.1_CHLRE:0.0101523981)D_0.0:1.16E-7)D_0.0:9.16E-8)D_0.0:6.46E-8)D_0.0:7.58E-8)D_0.0:8.01E-8)D_0.0:4.13E-8)D_0.747:0.0023731993)D_0.0:5.009E-7)D_0.994:0.6852984076)D_1.0:0.08783334434999995,(COLORED_Selmo_415133_SELMO:9.278E-7,(COLORED_Selmo_415128_SELMO:0.008258749,COLORED_Selmo_427615_SELMO:0.1071852729)D_0.397:0.0073176677)D_1.0:0.9176505513500002)S_1.0:0.0;

=cut

package TreePattern;


use strict;
use warnings;

use Carp qw(cluck confess croak);

use base qw(Exporter);

our @EXPORT = qw(
    SubmitTreePatternJob CheckTreePatternJob GetTreePatternJobResults AnnotateTreeWithPattern RemoveOldTreePatternJob
); 

use File::Temp qw(tempfile);
use Net::Telnet;

use Greenphyl;




# Package constants
####################

=pod

=head1 CONSTANTS

B<$DEBUG>: (boolean)

Enables debug mode.

B<$TREEPATTERN_HOST>: (string)

Name of the host where TreePattern daemon is running.

B<$TREEPATTERN_PORT>: (integer)

Port used by the TreePattern daemon.

B<$TEMPORARY_DIRECTORY>: (string)

Path to the temporary directory used to store temporary job files.

B<$TEMPORARY_FILE_PREFIX>: (string)

Job file name prefix. It must not include any capital 'X'.

B<$TEMPORARY_FILE_SUFFIX>: (string)

Job file name suffix. It must not include any capital 'X'.

B<$JOB_ID_LENGTH>: (integer)

length of job identifier. Must be > 1.

B<$DATABASE_GREENPHYL_V4>: (string)

Database name for GreenPhyl v4.

=cut

our $DEBUG = 0;
our $TREEPATTERN_HOST = 'valdaine.cirad.fr';
our $TREEPATTERN_PORT = 56789;
our $TEMPORARY_DIRECTORY = GP('TEMP_OUTPUT_PATH');
our $TEMPORARY_FILE_PREFIX = 'tpat';
our $TEMPORARY_FILE_SUFFIX = '.tmp';
our $JOB_ID_LENGTH         = 8;
our $AUTOCLEAN_DELAY       = 0; # days
our $DATABASE_GREENPHYL_V5 = 'GreenPhylV5';



# Package subs
###############

=pod

=head1 FUNCTIONS

=head2 RemoveOldTreePatternJob

B<Description>: Removes old job ID files.

B<ArgsCount>: 0

B<Return>: nothing

=cut

sub RemoveOldTreePatternJob
{
    my $temp_directory_dh;
    if (opendir($temp_directory_dh, $TEMPORARY_DIRECTORY))
    {
        while (my $file_name = readdir($temp_directory_dh))
        {
            if (($file_name =~ m/^\Q$TEMPORARY_FILE_PREFIX\E\w{$JOB_ID_LENGTH}\Q$TEMPORARY_FILE_SUFFIX\E$/)
                && ($AUTOCLEAN_DELAY < -M "$TEMPORARY_DIRECTORY/$file_name"))
            {
                if ($DEBUG)
                {
                    warn "DEBUG: AUTOCLEAN: removing old job file '$file_name'\n";
                }
                unlink("$TEMPORARY_DIRECTORY/$file_name");
                if ($!)                
                {
                    warn "ERROR: Failed to remove '$file_name'!\n$!\n";
                }
            }
            # elsif ($DEBUG)
            # {
            #     warn "DEBUG: AUTOCLEAN: skipping '$file_name'\n";
            # }
        }

        close($temp_directory_dh);
    }
}


=pod

=head2 RunTreePatternCommand

B<Description>: Sends a command to the daemon and return the result.

B<ArgsCount>: 1

=over 4

=item $command: (string) (R)

The command string.

=back

B<Return>: (string)

The multi-lines output.

=cut

sub RunTreePatternCommand
{
    my ($command) = @_;

    if ($DEBUG)
    {
        warn "DEBUG: COMMAND:\n----------\n$command----------\n";
    }

    my $telnet = new Net::Telnet();
    $telnet->open(
        'Host' => $TREEPATTERN_HOST,
        'Port' => $TREEPATTERN_PORT,
        'Timeout' => 10, # seconds
    );

    # connect to daemon
    $telnet->put($command);
    my @lines = $telnet->getlines();

    if ($telnet->errmsg)
    {
        warn $telnet->errmsg;
    }

    $telnet->close();

    if ($DEBUG)
    {
        warn "DEBUG: Got answer (" . scalar(@lines) . " lines):\n----------\n" . join('', @lines) . "----------\n";
    }

    return join('', @lines);
}


=pod

=head2 SubmitTreePatternJob

B<Description>: Submit a TreePattern job and return its ID.

B<ArgsCount>: 1

=over 4

=item $parameters: (hash) (R)

Parameter hash. Supported keys are:
-'database' (string) (R): search database.
-'pattern' (string) (R): tree pattern to search.

=back

B<Return>: (integer)

A job identifier.

=cut

sub SubmitTreePatternJob
{
    my ($parameters) = @_;

    # remove old analyses
    RemoveOldTreePatternJob();

    $parameters ||= {};
    if ((ref($parameters) ne 'HASH')
        || (!exists($parameters->{'database'}))
        || (!exists($parameters->{'pattern'}))
        )
    {
        confess "ERROR: SubmitTreePatternJob: invalid parameter!\n";
    }
    
    # create a temporary file
    my ($temp_file_fh, $temp_file_name) =
        tempfile(
            $TEMPORARY_FILE_PREFIX . ('X' x $JOB_ID_LENGTH),
            'DIR' => $TEMPORARY_DIRECTORY,
            'SUFFIX' => $TEMPORARY_FILE_SUFFIX,
        );

    if ($DEBUG)
    {
        warn "DEBUG: using job file '$temp_file_name' ($TEMPORARY_DIRECTORY)\n";
    }

    # get its name as a job identifier
    my ($job_id) = ($temp_file_name =~ m/^.*\Q$TEMPORARY_FILE_PREFIX\E(\w{$JOB_ID_LENGTH})$TEMPORARY_FILE_SUFFIX$/);

    # fork and launch the process in background
    my $pid = fork();
    if (!defined $pid)
    {
        confess "ERROR: Fork failed: $!\n";
    }

    if ($pid == 0)
    {
        # child process
        # save corresponding Unix process ID
        print {$temp_file_fh} "$$\n";

        # save used database
        $parameters->{'database'} =~ s/[\r\n]/ /g;
        print {$temp_file_fh} "$parameters->{'database'}\n";

        # save pattern searched
        $parameters->{'pattern'} =~ s/[\r\n]/ /g;
        print {$temp_file_fh} "$parameters->{'pattern'}\n";

        # closes streams to prevent parent process from being hold
        open(STDIN, "</dev/null");
        open(STDOUT, ">/dev/null");
        open(STDERR, ">/dev/null");
        
        # save treepattern job ID and result count
        my $lines = RunTreePatternCommand("patternSearch\n" . $parameters->{'database'} . "\n" . $parameters->{'pattern'} . "\n");
        
        # write the real job identifier in the file and close
        print {$temp_file_fh} $lines;
        close($temp_file_fh);

        exit(0);
    }

    close($temp_file_fh);

    if ($DEBUG)
    {
        warn "DEBUG: got job ID '$job_id'\n";
    }

    # return the job identifier    
    return $job_id;
}


=pod

=head2 CheckTreePatternJob

B<Description>: returns the status of a job.

B<ArgsCount>: 1

=over 4

=item $job_id: (string) (R)

Job identifier.

=back

B<Return>: (list of 4 elements or just first element in scalar context)

First element is the job status (integer or undef):
undef: job is still running.
-4: unable to open result file
-3: job lost or an error occurred
-2: invalid identifier
-1: results have been removed, analysis not available
0: no match found.
positive integer: number of match found.

Second element is the real job identifier (string).

Third element is the queried database.

Fourth element is the pattern.

=cut

sub CheckTreePatternJob
{
    my ($job_id) = @_;

    if ($job_id !~ m/^\w{$JOB_ID_LENGTH}$/)
    {
        # invalid job ID
        return -2;
    }

    my $job_file_path = "$TEMPORARY_DIRECTORY/$TEMPORARY_FILE_PREFIX$job_id$TEMPORARY_FILE_SUFFIX";
    if (!-e $job_file_path)
    {
        # 
        return -1;
    }

    my ($unix_job_id, $treepattern_job_id, $tp_database, $pattern, $match_count);
    my $temp_file_fh;
    if (open($temp_file_fh, $job_file_path))
    {
        $unix_job_id = <$temp_file_fh>;
        if ($unix_job_id)
        {
            chomp($unix_job_id);
            # check if process is till running
            my $process_running = kill 0, $unix_job_id;

            $tp_database = <$temp_file_fh> || '';
            chomp($tp_database);

            $pattern = <$temp_file_fh> || '';
            chomp($pattern);

            $treepattern_job_id = <$temp_file_fh> ||'';
            chomp($treepattern_job_id);

            if ($treepattern_job_id)
            {
                $match_count = <$temp_file_fh>;
                chomp($match_count);
                $match_count ||= 0;
            }
            elsif (!$process_running)
            {
                # process is not running anymore but TreePattern did not return something
                return -3;
            }

            close($temp_file_fh);
        }
    }
    else
    {
        return -4;
    }
    
    if (wantarray)
    {
        return ($match_count, $treepattern_job_id, $tp_database, $pattern);
    }
    else
    {
        return $match_count;
    }
}


=pod

=head2 GetTreePatternJobResults

B<Description>: returns a list of results from the given TreePattern job.

B<ArgsCount>: 1

=over 4

=item $parameters: (hash) (R)

Parameter hash. Supported keys are:
-'job' (string) (R): a TreePattern job identifier.
-'offset' (integer) (O): an offset from which results should be returned.
    Default: 0
-'length' (integer) (O): the number of result to return.
    Default: all results

=back

B<Return>: (array ref)

An array of matching file names (string).

=cut

sub GetTreePatternJobResults
{
    my ($parameters) = @_;
    
    $parameters ||= {};
    if ((ref($parameters) ne 'HASH')
        || !exists($parameters->{'job'}))
    {
        confess "ERROR: GetTreePatternJobResults: invalid parameter!\n";
    }

    my ($result_count, $real_job_id) = CheckTreePatternJob($parameters->{'job'});
    if (!$real_job_id || !$result_count || (0 > $result_count))
    {
        # no result found
        return [];
    }

    $parameters->{'offset'} ||= 0;
    $parameters->{'length'} ||= $result_count - $parameters->{'offset'};

    my $command = "resultSplit\n" . $real_job_id . "\n" . $parameters->{'offset'} . "\n" . $parameters->{'length'} . "\n";
    my $match_list = RunTreePatternCommand($command);
    my @matches = grep {$_} split(/[\n\r+]/, $match_list);

    return \@matches;
}


=pod

=head2 AnnotateTreeWithPattern

B<Description>: returns a pattern-annotated newick tree string.

B<ArgsCount>: 1

=over 4

=item $parameters: (hash) (R)

Parameter hash. Supported keys are:
-'database' (R): search database.
-'pattern' (R): tree pattern to search.
-'file' (R): the tree file name of the tree to annotate.

=back

B<Return>: (integer)

A job identifier.

=cut

sub AnnotateTreeWithPattern
{
    my ($parameters) = @_;

    $parameters ||= {};
    if ((ref($parameters) ne 'HASH')
        || (!exists($parameters->{'database'}))
        || (!exists($parameters->{'pattern'}))
        || (!exists($parameters->{'file'}))
        )
    {
        confess "ERROR: AnnotateTreeWithPattern: invalid parameter!\n";
    }

    my $command = "patternNewick\n" . $parameters->{'database'} . "\n" . $parameters->{'file'} . "\n" . $parameters->{'pattern'} . "\n";
    my $newick = RunTreePatternCommand($command);
    chomp($newick);

    return $newick;
}


=pod

=head1 AUTHORS

Mathieu ROUARD (Bioversity), m.rouard@cgiar.org

Valentin GUIGNON (Bioversity), v.guignon@cgiar.org

Matthieu CONTE (Syngenta)

Sebastien BRIOIS (Syngenta)

=head1 VERSION

Version 1.0.0

Date 23/10/2012

=head1 SEE ALSO

GreenPhyl documentation.

=cut

return 1; # package return
