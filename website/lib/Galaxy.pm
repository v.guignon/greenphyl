#!/usr/bin/env perl
# 
# creation : Sebastien Briois 01/09/2013
package Galaxy;
use Moose;

use Galaxy::History;
use Galaxy::Workflow;

use Data::Dumper;
use HTTP::Request::Common;
use LWP::UserAgent;
use URI;
use JSON;

our $HISTORIES_URL = '/api/histories';
our $WORKFLOWS_URL = '/api/workflows';
our $TOOLS_URL     = '/api/tools';
our $DATASETS_URL  = '/api/datasets';

has api_key => (
    is      => 'ro',
    isa     => 'Str',
    default => $ENV{'GALAXY_API_KEY'},
);

has base_url => (
    is      => 'ro',
    isa     => 'Str',
    default => $ENV{'GALAXY_BASE_URL'},
);

has verbose => (
    is      => 'ro',
    isa     => 'Int',
    default => 0
);

has histories => (
    is      => 'ro',
    isa     => 'ArrayRef[Galaxy::History]',
    auto_deref => 1,
    lazy_build => 1,
    init_arg => undef,
);

has workflows => (
    is      => 'ro',
    isa     => 'ArrayRef[Galaxy::Workflow]',
    auto_deref => 1,
    lazy_build => 1,
    init_arg => undef,
);


#
#   LAZY BUILDS
#

sub _build_histories
{
    my ( $self ) = @_;
        
    my $histories = $self->_request( 'GET', $HISTORIES_URL );
    
    my @history_objects = map {
        Galaxy::History->new(
            id       => $_->{'id'},
            name     => $_->{'name'},
            verbose  => $self->verbose,
            api_key  => $self->api_key,
            base_url => $self->base_url,
        )
    } @$histories;
    
    return \@history_objects;
}

sub _build_workflows
{
    my ($self) = @_;
    
    my $workflows = $self->_request( 'GET', $WORKFLOWS_URL );
    
    my @workflow_objects = map {
        Galaxy::Workflow->new(
            id       => $_->{'id'},
            name     => $_->{'name'},
            verbose  => $self->verbose,
            api_key  => $self->api_key,
            base_url => $self->base_url,
        )
    } @$workflows;
    
    return \@workflow_objects;
}


#
#   PUBLIC METHODS
#

sub create_history
{
    my ( $self, $history_name ) = @_;
    
    my $new_history = $self->_request( 'POST', $HISTORIES_URL, { 'name' => $history_name } );
    
    if ( $self->verbose ) {
        print STDERR "Created history! " . $new_history->{'name'} . " (" . $new_history->{'id'} . ")\n";
    }
    
    $self->clear_histories;
    
    return $self->get_history( $new_history->{'id'} );
}

sub get_workflow
{
    my ( $self, %filters ) = @_;
    
    my @selected_worflows = $self->workflows;
    while ( my ($key, $value) = each %filters ) {
        @selected_worflows = grep{ $_->{$key} eq $value } @selected_worflows;
    }
    my $selected_worflow = shift @selected_worflows;
    
    if ( $self->verbose ) {
        if ( $selected_worflow ) {
            print STDERR "Found workflow! " . $selected_worflow->{'name'} . " (id: " . $selected_worflow->{'id'} . ")\n";
        }
        else {
            print STDERR "Workflow not found!\n";
        }
    }
    
    return $selected_worflow;
}

sub get_history
{
    my ( $self, $history_id ) = @_;
    
    my ($history_obj) = grep { $_->id eq $history_id } $self->histories;
    
    if ( not defined $history_obj ) {
        my $history = $self->_request( 'GET', $HISTORIES_URL . '/' . $history_id );
        
        $history_obj = Galaxy::History->new(
            id       => $_->{'id'},
            name     => $_->{'name'},
            verbose  => $self->verbose,
            api_key  => $self->api_key,
            base_url => $self->base_url,
        );
        
        push( @{ $self->histories }, $history_obj );
    }
    
    return $history_obj;
}


#
#   PRIVATE METHODS
#

sub _request
{
    my ( $self, $method, $url, $data, $as_is ) = @_;
    $as_is ||= 0; # Return response as it is, do not assume JSON response
    
    $url = $self->_make_url( $self->base_url . $url ); # insert api key into url
    my $request;
    
    if ( uc $method eq 'GET' ) {
        $request = GET( $url );
    }
    elsif( uc $method eq 'POST' ) {
        $request = POST( $url, 
            'Content_Type' => 'form-data', 
            'Content' => [ %$data ]
        );
    }
    elsif( uc $method eq 'PUT' ) {
        $request = PUT( $url,
            'Content_Type' => 'form-data', 
            'Content' => [ %$data ]
        );
    }
    elsif( uc $method eq 'DELETE' ) {
        $request = DELETE( $url );
    }
    
    my $browser  = LWP::UserAgent->new;
    my $response = $browser->request( $request );
    
    if ( !$response->is_success ) {
        die "[$url] " . $response->message . ' : ' . $response->decoded_content;
    }
    
    return ($as_is ? $response->decoded_content : from_json( $response->decoded_content ));
}

#
# Adds the API Key to the URL if it's not already there.
# Note: API Key should always be in the URL, even when not using GET method
sub _make_url
{
    my ( $self, $url, $args ) = @_;
    $args ||= [];
    
    my $argsep = '&';
    
    if ( not $url =~ m/\?/ ) {
        $argsep = '?';
    }
    
    if ( not $url =~ m/\?key/ && not  $url =~ m/&key=/ ) {
        unshift( @$args, [ 'key', $self->api_key ] );
    }
    
    return $url . $argsep . join( '&', join( '=', map{ @{$_} } @$args ) );
}

no Moose;

1;
