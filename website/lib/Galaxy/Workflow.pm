#!/usr/bin/env perl
package Galaxy::Workflow;
use Moose;
use JSON;

use Data::Dumper;

extends 'Galaxy';

has ['id', 'name'] => (
    is  => 'ro', 
    isa => 'Str'
);

has 'steps' => (
    is          => 'ro',
    isa         => 'ArrayRef',
    auto_deref  => 1,
    lazy_build  => 1,
);


#
#   LAZY BUILDS
#
sub _build_steps
{
    my ($self) = @_;
    
    if ( $self->verbose ) {
        print STDERR "Loading steps of workflow " . $self->id . "\n";
    }
    
    my $workflow_details = $self->_request( 'GET', $Galaxy::WORKFLOWS_URL . '/' . $self->id );
    
    my @steps = values %{ $workflow_details->{'steps'} };
    
    # Append 'label' to input steps
    foreach my $step ( grep { not %{$_->{'input_steps'}} } @steps ) {
        $step->{'label'} = $workflow_details->{'inputs'}{$step->{'id'}}{'label'};
    }
    
    return \@steps;
}

#
#   PUBLIC FUNCTIONS
#

sub get_step
{
    my ( $self, %filters ) = @_;
    
    my @selected_steps = $self->steps;
    while ( my ($key, $value) = each %filters ) {
        @selected_steps = grep{ exists $_->{$key} && $_->{$key} eq $value } @selected_steps;
    }
    my $selected_step = shift @selected_steps;
    
    if ( $self->verbose ) {
        if ( $selected_step ) {
            print STDERR "Found step! " . $selected_step->{'label'} . " (id: " . $selected_step->{'id'} . ")\n";
        }
        else {
            print STDERR "Step not found!\n";
        }
    }
    
    return $selected_step;
}

=pod
Run this workflow under the context of the given history.
hda_id_for_step_id is a hashref:
- key   : step_id
- value : hda_id
HDA : History Dataset Association, an event in the history like an uploaded file.
=cut
sub run
{
    my ( $self, $history, $hda_id_for_step_id ) = @_;
    
    if ( $self->verbose ) {
        print STDERR "Starting '" . $self->name . "' workflow\n";
    }
    
    # 3 required keys for running a workflow
    my %data = (
        'workflow_id'   => $self->id,
        'history'       => 'hist_id=' . $history->id,
        'ds_map'        => {}
    );
    
    # ds_map key defines inputs of each source step
    my %input_for_step;
    
    while ( my ($step_id, $hda_id) = each %$hda_id_for_step_id ) {
        $input_for_step{ $step_id } = {
            'src' => 'hda',
            'id'  => $hda_id,
        }
    }
    
    $data{'ds_map'} = to_json( \%input_for_step );
    
    if ( $self->verbose ) {
        print STDERR Dumper( \%data );
    }
    
    return $self->_request( 'POST', $Galaxy::WORKFLOWS_URL, \%data );
}

no Moose;

1;
