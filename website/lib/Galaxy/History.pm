#!/usr/bin/env perl
package Galaxy::History;
use Moose;

use File::Basename;
use JSON;

extends 'Galaxy';

has ['id', 'name'] => (
    is  => 'ro', 
    isa => 'Str'
);

=pod
Return all HDAs (History Dataset Association) related to this history_id
=cut
sub get_hdas
{
    my ($self) = @_;
    
    return $self->_request( 'GET', $Galaxy::HISTORIES_URL . '/' . $self->id . '/contents' );
}

=pod
Return a HASH of detailed information for a specific HDA.
=cut
sub get_hda
{
    my ( $self, $hda_id ) = @_;
    
    return $self->_request( 'GET', $Galaxy::HISTORIES_URL . '/' . $self->id . '/contents/' . $hda_id );
}

sub download_output
{
    my ( $self, $hda_id ) = @_;
    
    my $hda          = $self->get_hda( $hda_id );
    my $download_url = $hda->{'download_url'};
    $download_url    =~ s{/galaxy}{};
    
    return $self->_request( 'GET', $download_url, undef, 1 );
}


sub upload_file
{
    my ( $self, $filepath, $file_type ) = @_;
    $file_type ||= 'auto';
    
    if ( $self->verbose ) {
        print "Uploading $filepath ... \n";
    }
    
    if ( not -e $filepath ) {
        die "File does not exist : $filepath";
    }
    
    my %data = (
        'tool_id'           => 'upload1',
        'history_id'        => $self->{'id'},
        'files_0|file_data' => [$filepath],
    );
    
    my %inputs = (
        'files_0|NAME'      => basename( $filepath ),
        'files_0|type'      => 'upload_dataset',
        'dbkey'             => '?',
        'file_type'         => $file_type,
        'ajax_upload'       => 'true',
    );
    
    $data{'inputs'} = to_json(\%inputs);
    
    my $response = $self->_request( 'POST', $Galaxy::TOOLS_URL, \%data );
    my $upload_details = $response->{'outputs'}->[0];
    
    # Return related HDA
    my ($hda) = grep{ $_->{'name'} eq $upload_details->{'name'} } @{$self->get_hdas()};
    
    return $hda;
}

no Moose;

1;
