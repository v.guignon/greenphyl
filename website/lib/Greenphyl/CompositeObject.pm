=pod

=head1 NAME

Greenphyl::CompositeObject - class for composite object

=head1 SYNOPSIS

    my $composite = Greenphyl::CompositeObject->new(
        {
            'title'     => 'something',
            'species'   => $species,
            'count'     => 42,
            'sequences' => [$seq1, $seq2],
        },
        ['a', 't', 'g', 'c'],
        $family,
    );
    
    print "The answer to life, the universe and everything is " . $composite->count . "\n";
    my $first_sequence = $composite->sequences->[0];
    print "Current species: " . $composite->species->name . "\n";
    print "Family name: " . $composite->name() . "\n";
    print "Nucleic acids: " . join(', ', @{$composite}) . "\n";
    print "Adenine: " . $composite->[0] . "\n";

=head1 REQUIRES

Perl5

=head1 EXPORTS

Nothing

=head1 DESCRIPTION

This module is for composite objects. A composite object is composed by
zero, one or several arrays, zero, one or several hashes and zero or one
object which are called components of the composite object.

Each component of the composite object can be accessed through the composite
object just like it if was the same object with some minor differences.

As the composite object can be composed by several components that may (or may
not) share the same accessors (either indexes, or keys or member functions),
there are some priority rules:

* if several arrays are provided to the constructor, all arrays are merged into
  one bigger array in the same order they are given to the constructor.
ex.: 
    my $composite = Greenphyl::CompositeObject->new(
        ['a', 'b', 'c'],
        ['g', 'h'],
        ['d', 'e', 'f'],
    );
    print "List: " . join(', ', @$composite) . "\n";
    # will display: "List: a, b, c, g, h, d, e, f"

* if several hashes are provided to the constructor, they are merged into one
  single hash. If they share some identical keys, the corresponding value of
  the last hash (having the key) given to the constructor is kept for each key.
    my $composite = Greenphyl::CompositeObject->new(
        {'a' => 33},
        {'a' => 806},
        {'a' => 42},
    );
    print "Value: " . $composite->{'a'} . "\n";
    # will display: "Value: 42"

* as hash members can be accessed through method names, they can conflict with
  object members. As it can be convenient to be able to override some object
  method using a composite object, hash members are fetched first if the
  key does not exist, then the object member is used.
ex.:
    my $composite = Greenphyl::CompositeObject->new(
        {
            'answer'     => 42,
        },
        $object, # let's assume $object has an "answer()" method
    );
    print $composite->answer(0);
    # will display 42

    my $composite2 = Greenphyl::CompositeObject->new(
        $object,
    );
    print $composite2->answer(0);
    # will display whatever $object->answer(0) is supposed to output

=cut

package Greenphyl::CompositeObject;


use strict;
use warnings;
use vars qw($AUTOLOAD);

# bool operator is used to check if the composite object contains soemthing or
# is empty
# see http://perldoc.perl.org/overload.html for details
use overload
    (
        '""'   => \&getStringValue,
        'bool' => \&getBooleanValue,
        '%{}'  => \&_getHashValue,
        '@{}'  => \&_getArrayValue,
    );

use Carp qw(cluck confess croak);

use base qw(Greenphyl::DumpableObject Greenphyl::DumpableTabularData);

use Greenphyl;
use JSON;



# Package constants
####################

=pod

=head1 CONSTANTS

B<$SUPPORTED_DUMP_FORMAT>:  (hash ref)

contains supported dump formats.

=cut

our $DEBUG = 0;
our $SUPPORTED_DUMP_FORMAT =
    {
        'static'  => {
            'excel' => \&DumpExcel,
            'csv'   => \&DumpCSV,
            'xml'   => \&DumpXML,
            'json'  => \&DumpJSON,
            'html'  => \&DumpHTML,
        },
        'dynamic' => {
            'excel' => \&DumpExcel,
            'csv'   => \&DumpCSV,
            'xml'   => \&DumpXML,
            'json'  => \&dumpJSON,
            'html'  => \&dumpHTML,
        },
    };

our $_instance_index = 1;
our $g_instances = {};


    


# Package subs
###############

=pod

=head1 STATIC METHODS

=head2 CONSTRUCTOR

B<Description>: Creates a new instance of composite object with the given
members.

B<ArgsCount>: 1-n

=over 4

=item $proto: (string) (R)

The class name.

=item @other_parameters: (list) (R)

A list of elements to store in the composite object. These elements can be
either hash refs, array refs or object refs. The priority for accessing
member values is the following:
* array values > hash values > object values;
* arrays are merged: the index of the first element of an array is the sum of
  the size of all previous arrays given to the constructor;
* for hashes with same keys: the last one given to the constructor overrides
  previous values;
* only the first valid object given to the constructor is kept.

=back

B<Return>: (Greenphyl::CompositeObject)

a new instance.

B<Caller>: general

B<Exception>:

B<Example>:

    my $composite = Greenphyl::CompositeObject->new(
        {
            'title'     => 'something',
            'species'   => $species,
            'count'     => 42,
            'sequences' => [$seq1, $seq2],
        }
    );

=cut

sub new
{
    my ($proto, @other_parameters) = @_;
    my $class = ref($proto) || $proto;

    my ($composite_object, $composite_hash, $composite_array);
    
    foreach my $parameter (@other_parameters)
    {
        # check parameter type
        if ('HASH' eq ref($parameter))
        {
            # init hash if not done yet
            $composite_hash ||= {};
            # copy/override key-values
            map { $composite_hash->{$_} = $parameter->{$_} } keys(%$parameter);
        }
        elsif ('ARRAY' eq ref($parameter))
        {
            $composite_array ||= [];
            # merge arrays
            push(@{$composite_array}, @$parameter);
        }
          # only one object is allowed and it's the first valid object
        elsif (ref($parameter) && !$composite_object)
        {
            $composite_object = $parameter;
        }
    }
    
    # instance creation...
    # here, we want self to be a scalar ref and not a hash ref
    # because we want to override the hash @{} operator.
    # the scalar ref will be used as a key to identify the hash that will store
    # our data in the global $g_instances hash.
    # All could have been fine if we stopped here but...
    # ...but we also want to be able to print the underlying object name with
    # a simple "print $composite;". If the underlying object is a
    # Greenphyl::Sequence for instance, then the "print" above would display
    # the sequence seq_textid (and not something like
    # "Greenphyl::Sequence=HASH(0x1477c310)").
    # So we override the "" operator but this raise a problem since we want to
    # use "$self" as a hash key, which would call the "" operator and lead to
    # several objects sharing the same key... which is not what we want!
    # So we use a second ref on the scalar ref.
    #
    # The base scalar is a simple string that is set to 'Composite' or, by
    # priority order, the string version of the given object if one, the hash
    # key 'composite_id' if one, or the first array value if it contains only
    # one element.
    # The first scalar ref is here in order to have a pointer to the name that
    # won't be used as key.
    # The second scalar ref will be our key in the %g_instances hash.
    #

    # -create a new scalar reference
    my $new_scalar = 'Composite';
    # -try to get a better scalar content if possible: object string version
    if ($composite_object)
    {$new_scalar = ''.$composite_object;}
    elsif ($composite_hash && exists($composite_hash->{'composite_id'}))
    {
        # if the hash has a composit_id key, use it
        $new_scalar = $composite_hash->{'composite_id'};
    }
    elsif ($composite_array && (1 == @$composite_array))
    {
        # if the array has only one value, use it
        $new_scalar = $composite_array->[0];
    }
    # -get its reference
    my $key = \$new_scalar;
    # -get its reference
    my $self = \$key;
    bless($self, $class);

    # -create a hash in the global instances hash using the scalar ref as key
    $g_instances->{$self->getKeyValue()} = {};

    if ($composite_object)
    {
        $g_instances->{$self->getKeyValue()}->{'object'} = $composite_object;
    }
    if ($composite_hash)
    {
        $g_instances->{$self->getKeyValue()}->{'hash'}   = $composite_hash;
    }
    if ($composite_array)
    {
        $g_instances->{$self->getKeyValue()}->{'array'}  = $composite_array;
    }


    return $self;
}


#+FIXME: handle destructor to free $g_instances hash values


=pod

=head2 CreateTable

B<Description>: Returns a table containing the requested columns of given
composite object.

This method is requiered and called by
Greenphyl::DumpableTabularData::DumpTable method.

B<ArgsCount>: 3

=over 4

=item $class: (string) (R)

Should be Greenphyl::CompositeObject or any derived object class.

=item $object_ref: (array of Greenphyl::CompositeObject) (R)

The list of composite objects to process.

=item $parameters: (hash ref) (R)

a hash containing the key 'columns' that contains the columns to dump.
Each key is a column name (label) and each corresponding value is a string
containing the chain (dot-separated) of members to call to fetch the value from
each composite object.

=back

B<Return>: (array ref)

An array containing a list of hash, one hash per composite object. Each hash
key is a column label and each value is the corresponding string value.

=cut

sub CreateTable
{
    my ($class, $object_ref, $parameters) = @_;
    
    my $table = [];
    if ($parameters->{'columns'})
    {
        foreach my $object (@$object_ref)
        {
            my $object_row = {};
            foreach my $column (keys(%{$parameters->{'columns'}}))
            {
                $object_row->{$column} = $object->getColumnValue($parameters->{'columns'}->{$column});
            }
            push(@$table, $object_row);
        }
    }
    return $table;
}


=pod

=head2 CreateTable

B<Description>: Returns a table containing the requested columns of given
composite objects.

This method is requiered and called by
Greenphyl::DumpableTabularData::dumpTable method.

B<ArgsCount>: 2

=over 4

=item $self: (Greenphyl::CompositeObject) (R)

The composite object to process.

=item $parameters: (hash ref) (R)

a hash containing the key 'columns' that contains the columns to dump.
Each key is a column name (label) and each corresponding value is a string
containing the chain (dot-separated) of members to call to fetch the value from
current composite object.

=back

B<Return>: (hash ref)

Each hash key is column label and each value is the corresponding string value.

=cut

sub createTable
{
    my ($self, $parameters) = @_;
    my $object_table = {};
    if ($parameters->{'columns'})
    {
        foreach my $column (keys(%{$parameters->{'columns'}}))
        {
            $object_table->{$column} = $self->getColumnValue($parameters->{'columns'}->{$column});
        }
    }
    return $object_table;
}


=pod

=head2 DumpExcel, DumpCSV, DumpXML

B<Description>: Returns a string in the given format containing a table with
the requested columns.

B<ArgsCount>: 2

=over 4

=item $object_ref: (array of Greenphyl::CompositeObject) (R)

The list of composite objects to process.

=item $parameters: (hash ref) (R)

a hash containing the key 'columns' that contains the columns to dump.
Each key is a column name (label) and each corresponding value is a string
containing the chain (dot-separated) of members to call to fetch the value from
each composite object.

=back

B<Return>: (string)

a string in the given format containing a table with the requested columns of
given composite objects.

B<Example>:

    my $excel_fh;
    if (open($excel_fh, '>sequences.xls'))
    {
        binmode($excel_fh);
        print {$excel_fh} Greenphyl::CompositeObject::DumpExcel([$composite1, $composite2], {'columns' => {'01.ID'=>'sequence.id','02.Name'=>'sequence.name','03.species'=>'species'}});
        close($excel_fh);
    }
    ...
    print {$csv_fh} Greenphyl::CompositeObject::DumpCSV([$sequence1, $sequence2], {'columns' => {'01.ID'=>'sequence.id','02.Name'=>'sequence.name','03.species'=>'species'}});
    ...
    print {$xml_fh} Greenphyl::CompositeObject::DumpXML([$sequence1, $sequence2], {'columns' => {'01.ID'=>'sequence.id','02.Name'=>'sequence.name','03.species'=>'species'}});

=cut

sub DumpExcel
{
    my ($object_ref, $parameters) = @_;

    return Greenphyl::CompositeObject->DumpTable('excel', $object_ref, $parameters);
}


sub DumpCSV
{
    my ($object_ref, $parameters) = @_;

    return Greenphyl::CompositeObject->DumpTable('csv', $object_ref, $parameters);
}


sub DumpXML
{
    my ($object_ref, $parameters) = @_;

    #convert column names for XML
    my @column_names = keys(%{$parameters->{'columns'}});
    foreach my $column_name (@column_names)
    {
        my $new_column_name = $column_name;
        # remove indexes
        $new_column_name =~ s/^[0-9a-z]{1,2}\.\s*//;
        # remove unwanted characters
        $new_column_name =~ s/\W+/_/g;
        # make sure name does not begin with a number
        $new_column_name =~ s/^([0-9])/_$1/;
        $new_column_name = lc($new_column_name);
        # rename column
        $parameters->{'columns'}->{$new_column_name} = delete($parameters->{'columns'}->{$column_name});
    }
    
    return Greenphyl::CompositeObject->DumpTable('xml', $object_ref, $parameters);
}


=pod

=head2 dumpJSON

B<Description>: Returns a string in JSON format containing either the hash part
or the array part or the stringyfied object part of the composite object if
only one of these is available.
If more than one is available, all elements are returned in an array in the
order: object, hash, array.

B<ArgsCount>: 1

=over 4

=item $object_ref: (Greenphyl::CompositeObject) (R)

The composite object to process.

=back

B<Return>: (string)

a string in JSON format containing the content of the composite object.

=cut

sub dumpJSON
{
    my ($object_ref) = @_;

    my @composite_content;

    if ($g_instances->{$object_ref->getKeyValue()}->{'object'})
    {
        # push stringyfied version of the object
        push(@composite_content, '' . $object_ref);
    }

    if ($g_instances->{$object_ref->getKeyValue()}->{'hash'})
    {
        push(@composite_content, $g_instances->{$object_ref->getKeyValue()}->{'hash'});
    }

    if ($g_instances->{$object_ref->getKeyValue()}->{'array'})
    {
        push(@composite_content, $g_instances->{$object_ref->getKeyValue()}->{'array'});
    }

    if ((1 == @composite_content)
        && (ref($composite_content[0]) =~ m/^(?:ARRAY|HASH)$/))
    {
        return to_json($composite_content[0]);
    }
    elsif (1 < @composite_content)
    {
        return to_json(\@composite_content);
    }
    else
    {
        return '';
    }
}


=pod

=head2 DumpJSON

B<Description>: Returns a string in JSON format containing an array of composite
object dumped just like dumpJSON would do it.

B<ArgsCount>: 1

=over 4

=item $object_ref: (array of Greenphyl::CompositeObject) (R)

The list of composite objects to process.

=back

B<Return>: (string)

a string in JSON format containing the array of the dumped composite objects.

=cut

sub DumpJSON
{
    my ($object_array) = @_;

    my @composite_contents;
    foreach my $object_ref (@$object_array)
    {
        my @composite_content;

        if ($g_instances->{$object_ref->getKeyValue()}->{'object'})
        {
            # push stringyfied version of the object
            push(@composite_content, '' . $g_instances->{$object_ref->getKeyValue()}->{'object'});
        }

        if ($g_instances->{$object_ref->getKeyValue()}->{'hash'})
        {
            push(@composite_content, $g_instances->{$object_ref->getKeyValue()}->{'hash'});
        }

        if ($g_instances->{$object_ref->getKeyValue()}->{'array'})
        {
            push(@composite_content, $g_instances->{$object_ref->getKeyValue()}->{'array'});
        }

        if (1 < @composite_content)
        {
            push(@composite_contents, \@composite_content);
        }
        elsif (1 == @composite_content)
        {
            push(@composite_contents, @composite_content);
        }
    }
    return to_json(\@composite_contents);
}


=pod

=head2 dumpHTML

B<Description>: Returns an HTML string generated from the given template
with the given object.

B<ArgsCount>: 2

=over 4

=item $object_ref: (Greenphyl::CompositeObject) (R)

The composite object to process.

=item $parameters: (hash ref) (R)

a hash containing at leaste the key 'content' with a template name.

=back

B<Return>: (string)

.

=cut

sub dumpHTML
{
    my ($object_ref, $parameters) = @_;

    if (!$parameters->{'content'})
    {
        confess "ERROR: No template specified for composite dump!\n";
    }
    elsif ($parameters->{'content'} !~ m/\.tt$/)
    {
        confess "ERROR: Not a valid template file name ('" . $parameters->{'content'} . "') for composite dump!\n";
    }

    $parameters ||= {};
    # transfer composite content to template parameters as a 'composite' parameter
    $parameters->{'composite'} = $g_instances->{$object_ref->getKeyValue()};

    # set mode to inner as we don't want any header
    $parameters->{'mode'} = 'inner';
    $parameters->{'mime'} = 'html';
    $parameters->{'header_args'} = [];

    return Greenphyl::Web::RenderHTMLFullPage($parameters);
}


=pod

=head2 DumpHTML

B<Description>: .

B<ArgsCount>: 2

=over 4

=item $object_ref: (array of Greenphyl::CompositeObject) (R)

The list of composite objects to process.

=item $parameters: (hash ref) (R)

a hash containing at leaste the key 'template' with a template name.

=back

B<Return>: (string)

.

=cut

sub DumpHTML
{
    my ($object_array, $parameters) = @_;
    
    if (!$parameters->{'content'})
    {
        confess "ERROR: No template specified for composite dump!\n";
    }
    elsif ($parameters->{'content'} !~ m/\.tt$/)
    {
        confess "ERROR: Not a valid template file name ('" . $parameters->{'content'} . "') for composite dump!\n";
    }

    $parameters ||= {};

    # transfer composite contents to template parameters as a 'composites' array
    $parameters->{'composites'} = $object_array;

    # set mode to inner as we don't want any header
    $parameters->{'mode'} = 'inner';

    return Greenphyl::Web::RenderHTMLFullPage($parameters);
}


=pod

=head1 AUTOLOAD

B<Description>: Autogeneration of getters.

B<Example>:

    my $composite_data = Greenphyl::CompositeObject->new({'sequences'=>[$seq1, $seq2, $seq3],'family'=>$family,});
    print "Sequences: " . join(', ', @{$composite_data->sequences});
    print "Family IPR: " . $composite_data->family->getIPR();

=cut

sub AUTOLOAD
{
    my ($self, @parameters) = @_;
    (my $member_name = $AUTOLOAD) =~ s/.*://; # strip fully-qualified portion
    return unless $member_name =~ m/[a-z]/;  # skip DESTROY and all-cap methods

    # static calls
    if (!ref($self))
    {
        confess "ERROR: unknown/unhandled static method $member_name!\n";
    }

    # check if the member exists
    if ($g_instances->{$self->getKeyValue()}->{'hash'} && exists($g_instances->{$self->getKeyValue()}->{'hash'}->{$member_name}))
    {
        return $g_instances->{$self->getKeyValue()}->{'hash'}->{$member_name};
    }
    elsif ($g_instances->{$self->getKeyValue()}->{'object'})
    {
        my $value;
        my $object = $g_instances->{$self->getKeyValue()}->{'object'};
        eval "\$value = \$object->$member_name(\@parameters);";
        if ($@)
        {
            confess $@;
        }
        return $value;
    }
    confess "ERROR: member field not defined or unknown method '$member_name' for object type " . ref($self) . " ($self)!\n";
    # return undef;
}


=pod

=head2 getKeyValue

B<Description>: Returns the key value in g_instances hash of current object.

B<ArgsCount>: 1

=over 4

=item $self: (Greenphyl::CompositeObject) (R)

The composite object.

=back

B<Return>: (string)

the key hash.

=cut

sub getKeyValue
{
    my ($self) = @_;
    return $$self;
}


=pod

=head2 getStringValue

B<Description>: Returns a string version of the composite object. If the
composite object wraps another object, then, the stringyfied version of that
object is returned.

B<ArgsCount>: 1

=over 4

=item $self: (Greenphyl::CompositeObject) (R)

The composite object.

=back

B<Return>: (string)

the stringyfied version of the composite object.

=cut

sub getStringValue
{
    my ($self) = @_;
    # Warning: do not add any code that uses $self as a string here
    #          as we are overring the "to string" operator! It would lead to
    #          a deep recursion!
    # Just get original scalar
    return $$$self;
}


=pod

=head2 getBooleanValue

B<Description>: Tells if the composit object has been instanciated with
something.

B<ArgsCount>: 1

=over 4

=item $self: (Greenphyl::CompositeObject) (R)

The composite object.

=back

B<Return>: (boolean)

true if the composit object contains a non-empty hash or a non-empty array or
a non-empty object.

=cut

sub getBooleanValue
{
    my ($self) = @_;
    my $return_value = 0;

    if (exists($g_instances->{$self->getKeyValue()}->{'hash'})
        && defined($g_instances->{$self->getKeyValue()}->{'hash'}))
    {
        if (%{$g_instances->{$self->getKeyValue()}->{'hash'}})
        {
            $return_value = 1;
        }
    }

    if ($g_instances->{$self->getKeyValue()}->{'array'} && @{$g_instances->{$self->getKeyValue()}->{'array'}})
    {
        $return_value = 1;
    }

    if ($g_instances->{$self->getKeyValue()}->{'object'})
    {
        $return_value = 1;
    }

    return $return_value;
}


=pod

=head2 _getHashValue

B<Description>: Returns the underlying hash of the composite object.

B<ArgsCount>: 1

=over 4

=item $self: (Greenphyl::CompositeObject) (R)

The composite object.

=back

B<Return>: (array ref)

A reference to the composite object hash or undef.

=cut

sub _getHashValue
{
    my ($self) = @_;
    return $g_instances->{$self->getKeyValue()}->{'hash'};
}


=pod

=head2 _getArrayValue

B<Description>: Returns the underlying array of the composite object.

B<ArgsCount>: 1

=over 4

=item $self: (Greenphyl::CompositeObject) (R)

The composite object.

=back

B<Return>: (array ref)

A reference to the composite object array or undef.

=cut

sub _getArrayValue
{
    my ($self) = @_;
    return $g_instances->{$self->getKeyValue()}->{'array'};
}


=pod

=head1 DIAGNOSTICS

=head1 AUTHORS

Mathieu ROUARD (Bioversity), m.rouard@cgiar.org

Valentin GUIGNON (Bioversity), v.guignon@cgiar.org

Matthieu CONTE (Syngenta)

=head1 VERSION

Version 1.0.0

Date 24/05/2012

=head1 SEE ALSO

GreenPhyl documentation. DumpableObject doc.

=cut

return 1; # package return
