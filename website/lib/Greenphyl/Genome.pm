=pod

=head1 NAME

Greenphyl::Genome - GreenPhyl Genome Object

=head1 SYNOPSIS

    use Greenphyl::Genome;
    my $genome = Greenphyl::Genome->new($dbh, {'selectors' => {'accession' => 'malacensis_v2'}});
    print $genome->sequence_count();

=head1 REQUIRES

Perl5

=head1 EXPORTS

Nothing

=head1 DESCRIPTION

This module implements a GreenPhyl Genome database object.

=cut

package Greenphyl::Genome;

use strict;
use warnings;

use base qw(
    Greenphyl::CachedDBObject
    Greenphyl::DumpableObject
    Greenphyl::DumpableTabularData
);

use vars qw($AUTOLOAD);

use Carp qw(cluck confess croak);

use Greenphyl;
use Greenphyl::Taxonomy;
use Greenphyl::Sequence;
use Greenphyl::SourceDatabase;




# Package constants
####################

=pod

=head1 CONSTANTS

B<$DEBUG>: (boolean)

When set to true, it enables debug mode.

B<$OBJECT_PROPERTIES>: (hash ref)

object properties description. See DBObject constructor documentation for
details.

=cut

our $DEBUG = 0;

our $OBJECT_PROPERTIES = {
    'table' => 'genomes',
    'key' => 'id',
    'alternate_keys' => ['accession'],
    'load'     => [
        'accession',
        'complete_busco',
        'fragment_busco',
        'genome_size',
        'chromosome_count',
        'id',
        'missing_busco',
        'ploidy',
        'representative',
        'sequence_count',
        'species_id',
        'url_fasta',
        'url_institute',
        'version',
        'version_notes',
    ],
    'alias' => {
        'genome_id' => 'id',
        'name' => 'accession',
    },
    'base' => {
        'accession' => {
            'property_column' => 'accession',
        },
        'complete_busco' => {
            'property_column' => 'complete_busco',
        },
        'fragment_busco' => {
            'property_column' => 'fragment_busco',
        },
        'genome_size' => {
            'property_column' => 'genome_size',
        },
        'chromosome_count' => {
            'property_column' => 'chromosome_count',
        },
        'id' => {
            'property_column' => 'id',
        },
        'missing_busco' => {
            'property_column' => 'missing_busco',
        },
        'ploidy' => {
            'property_column' => 'ploidy',
        },
        'representative' => {
            'property_column' => 'representative',
        },
        'sequence_count' => {
            'property_column' => 'sequence_count',
        },
        'species_id' => {
            'property_column' => 'species_id',
        },
        'url_fasta' => {
            'property_column' => 'url_fasta',
        },
        'url_institute' => {
            'property_column' => 'url_institute',
        },
        'version' => {
            'property_column' => 'version',
        },
        'version_notes' => {
            'property_column' => 'version_notes',
        },
    },
    'secondary' => {
        'taxonomy' => {
            'object_key'      => 'taxonomy_id',
            'property_table'  => 'taxonomy',
            'property_key'    => 'id',
            'multiple_values' => 0,
            'property_module' => 'Greenphyl::Taxonomy',
        },
        'species' => {
            'object_key'      => 'species_id',
            'property_table'  => 'species',
            'property_key'    => 'id',
            'multiple_values' => 0,
            'property_module' => 'Greenphyl::Species',
        },
        'sequences' => {
            'property_table'  => 'sequences',
            'property_key'    => 'genome_id',
            'multiple_values' => 1,
            'property_module' => 'Greenphyl::Sequence',
        },
    },
    'tertiary' => {
        # 'families'   => {
        #     'link_table'        => 'sequence_count_by_families_species_cache',
        #     'link_object_key'   => 'species_id',
        #     'link_property_key' => 'family_id',
        #     'property_table'    => 'families',
        #     'property_key'      => 'id',
        #     'multiple_values'   => 1,
        #     'property_module'   => 'Greenphyl::Family',
        # },
    },

};

our $SUPPORTED_DUMP_FORMAT =
    {
        'static'  => {
            'excel'   => \&DumpExcel,
            'csv'     => \&DumpCSV,
            'xml'     => \&DumpXML,
        },
        'dynamic' => {
            'excel'   => \&DumpExcel,
            'csv'     => \&DumpCSV,
            'xml'     => \&DumpXML,
        },
    };



# Package subs
###############

=pod

=head1 STATIC METHODS

=head2 CONSTRUCTOR

B<Description>: Creates a new instance of a GreenPhyl Genome object.

B<ArgsCount>: 2

=over 4

=item $dbh: (DBI::db) (U)

Database handler of the database to load the object from.

=item $parameters: (hash ref) (R)

See Greenphyl::DBObject constructor documentation for details.

=back

B<Return>: (Greenphyl::Genome)

a new instance.

B<Caller>: General

B<Example>:

    my $genome = new Greenphyl::Genome($dbh, {'selectors' => {'accession' => 'malacensis_v2'}});

=cut

sub new
{
    my $proto = shift();
    my $class = ref($proto) || $proto;
    
    my $object_properties = $class->GetDefaultClassProperties();

    return $class->SUPER::new($object_properties, @_);
}




=pod

=head1 METHODS

=pod

=head2 getStringValue

B<Description>: returns Species code.

B<ArgsCount>: 1

=over 4

=item $self: (Greenphyl::Species)

the species object.

=back

B<Return>: (string)

the species code.

=cut

sub getStringValue
{
    my ($self) = @_;
    return $self->{'accession'};
}


=pod

=head2 DumpExcel, DumpCSV, DumpXML

B<Description>: Returns a string in the given format containing a table with
the requested columns of given species.

B<ArgsCount>: 2

=over 4

=item $object_ref: (array of Greenphyl::species) (R)

The list of species to process.

=item $parameters: (hash ref) (R)

a hash containing the key 'columns' that contains the columns to dump.
Each key is a column name (label) and each corresponding value is a string
containing the chain (dot-separated) of members to call to fetch the value from
each species.

=back

B<Return>: (string)

a string in the given format containing a table with the requested columns of
given species.

B<Example>:

    my $excel_fh;
    if (open($excel_fh, '>species.xls'))
    {
        binmode($excel_fh);
        print {$excel_fh} Greenphyl::Species::DumpExcel([$species1, $species2], {'columns' => {'01.Name'=>'name','02.Code'=>'code','03.Common name'=>'common_name'}});
        close($excel_fh);
    }
    ...

=cut

sub DumpExcel
{
    my ($object_ref, $parameters) = @_;

    return Greenphyl::Species->DumpTable('excel', $object_ref, $parameters);
}


sub DumpCSV
{
    my ($object_ref, $parameters) = @_;

    return Greenphyl::Species->DumpTable('csv', $object_ref, $parameters);
}


sub DumpXML
{
    my ($object_ref, $parameters) = @_;
    
    return Greenphyl::Species->DumpTable('xml', $object_ref, $parameters);
}


=pod

=head1 AUTHORS

Mathieu ROUARD (Bioversity), m.rouard@cgiar.org

Valentin GUIGNON (Bioversity), v.guignon@cgiar.org

Matthieu CONTE (Syngenta)

=head1 VERSION

Version 2.0.0

Date 11/10/2019

=head1 SEE ALSO

GreenPhyl documentation, DBObject documentation.

=cut

return 1; # package return
