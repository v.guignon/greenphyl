=pod

=head1 NAME

Greenphyl::Web::Access - Manage user access

=head1 SYNOPSIS

    use Greenphyl::Web::Access;

    InitAccess();
    ...

    my $requirements = {
        'require_access' => [GP('ACCESS_KEY_ADMINISTRATOR'), GP('ACCESS_KEY_ANNOTATOR'),],
        'access_needs'   => 'all',
    };

    my $current_user = GetCurrentUser();

    if (CheckAccess($requirements, $current_user))
    {
        # access granted: iser is both admin and annotator
        ...
    }
    else
    {
        # access denied
        ...
    }

    if (IsAdmin())
    {
        # current user is and admin
        ...
    }

=head1 REQUIRES

Perl5

=head1 EXPORTS

    InitAccess CheckAccess IsAdmin GetCurrentUser
    GetOpenIDPageParameters OpenIDAuthenticatePhase1 OpenIDAuthenticatePhase2

=head1 DESCRIPTION

This package provides functions to handle user access.
Note: calling InitAccess() is only required if you didn't use Greenphyl::Web.

=cut

package Greenphyl::Web::Access;


use strict;
use warnings;

use Carp qw(cluck confess croak);

use base qw(Exporter);

our @EXPORT = qw(InitAccess CheckAccess IsAdmin GetCurrentUser
    GetOpenIDPageParameters OpenIDAuthenticatePhase1 OpenIDAuthenticatePhase2);

use Greenphyl;
use Greenphyl::User;
use Greenphyl::OpenID;




# Package constants
####################

=pod

=head1 CONSTANTS

B<$DEBUG>: (boolean)
When set to true, it enables debug mode.

=cut

our $DEBUG = 0;




# Package variables
####################

=pod

=head1 VARIABLES

B<$g_session>: (CGI::Session)

Global current session object reference.

B<$g_current_user>: (Greenphyl::User)

Current user object.

=cut

our $g_session;
our $g_current_user;




# Package subs
###############

=pod

=head1 STATIC METHODS

=head2 InitAccess

B<Description>: Initializes current user data. It handles login and logout
procedures and should be called before the web page is loaded.
Note: you don't need to call this function if you use Greenphyl::Web as it will
be automatically called by that package.

B<ArgsCount>: 0

B<Return>: nothing

B<Caller>: general

=cut

sub InitAccess
{
    my $dbh;
    eval "\$g_session = Greenphyl::Web::GetSession();";

    if (!$g_session)
    {
        Greenphyl::PrintDebug('Failed to initialize session!');
        return;
    }

    # check if user tries to logout
    if (Greenphyl::GetParameterValues('logout'))
    {
        Greenphyl::PrintDebug('Logging out');
        $g_session->clear('user');
    }

    # check if user tries to log in using local account
    if (Greenphyl::GetParameterValues('registration'))
    {
        # skip registration
        PrintDebug("Registration process");
    }
    elsif (my $login = Greenphyl::GetParameterValues('user_login'))
    {
        Greenphyl::PrintDebug("Got a login request for '$login'");
        # check login content
        eval "
            use Greenphyl::Tools::Users;
            Greenphyl::Tools::Users::CheckLoginString(\$login);
        ";
        if ($@)
        {
            confess $@;
        }

        my $password = Greenphyl::GetParameterValues('user_password');
        if (length($password) > 64)
        {
            main::ThrowGreenphylError('error' => "Password too long!");
        }
        $dbh = Greenphyl::GetDatabaseHandler();
        # authenticate user using MySQL function
        my $is_authenticated = $dbh->selectrow_arrayref("SELECT authenticateGreenPhylUser(?, ?) AS is_authenticated;", undef, $login, $password)
          or main::ThrowGreenphylError('error' => "Failed to run database authentication function! " . $dbh->errstr);
        if ($is_authenticated && ($is_authenticated->[0]))
        {
            # user is authenticated, get user info
            if (my $user = Greenphyl::User->new(
                $dbh,
                {
                    'selectors' => {'login' => $login},
                },
            ))
            {
                $g_session->param('user', $user->getHashValue());
                Greenphyl::PrintDebug('User "' . $user . '" successfuly logged in!');
            }
        }
        else
        {
            main::ThrowGreenphylError('error' => "Failed to login! Invalid user name or password!");
        }
    }
      # or try to log in using OpenID
    elsif (GP('ENABLE_OPENID')
        && (my $openid_phase = Greenphyl::GetParameterValues('openid_phase')))
    {
        Greenphyl::PrintDebug("Login using OpenID");
        if ('1' eq $openid_phase)
        {
            Greenphyl::PrintDebug("Phase 1");
            my $openid_url = Greenphyl::GetParameterValues('openid_identity');
            OpenIDAuthenticatePhase1($openid_url, Greenphyl::GetURL('current', {'openid_phase' => '2',}));
        }
        elsif ('2' eq $openid_phase)
        {
            Greenphyl::PrintDebug("Phase 2");
            my ($status, $openid_data) = OpenIDAuthenticatePhase2();
            if ('verified' eq $status)
            {
                $dbh ||= Greenphyl::GetDatabaseHandler();
                my @users = Greenphyl::User->new(
                    $dbh,
                    {
                        'selectors' => {'id' => ['IN', '(SELECT user_id FROM user_openids WHERE openid_identity = ' . $dbh->quote($openid_data) . ')']},
                    },
                );

                if (!@users)
                {
                    main::ThrowGreenphylError('error' => "Failed to login! OpenID identifier not found in database! Did you register on GreenPhyl using that openID?");
                }
                elsif ((1 != @users) || !$users[0])
                {
                    Greenphyl::PrintDebug("Not just 1 user returned: " . @users . "\n" . join(', ', @users));
                    main::ThrowGreenphylError('error' => "Failed to login! Duplicate OpenID identifiers found in database!");
                }
                $g_session->param('user', $users[0]->getHashValue());
                Greenphyl::PrintDebug('User "' . $users[0] . '" successfuly logged in!');

            }
            else
            {
                main::ThrowGreenphylError('error' => "Failed to login! $openid_data");
            }
        }
        elsif ('3' eq $openid_phase)
        {
            Greenphyl::PrintDebug("OAuth2 Phase 1");
            my $oauth2_url = Greenphyl::GetParameterValues('openid_identity');
            OAuth2AuthenticatePhase1($oauth2_url, Greenphyl::GetURL('current', {'openid_phase' => '4',}));
        }
        elsif ('4' eq $openid_phase)
        {
            Greenphyl::PrintDebug("OAuth2 Phase 2");
            my ($status, $oauth2_data) = OAuth2AuthenticatePhase2();
            if ('verified' eq $status)
            {
                $dbh ||= Greenphyl::GetDatabaseHandler();
                my @users = Greenphyl::User->new(
                    $dbh,
                    {
                        'selectors' => {'id' => ['IN', '(SELECT user_id FROM user_openids WHERE openid_identity = ' . $dbh->quote($oauth2_data) . ')']},
                    },
                );

                if (!@users)
                {
                    main::ThrowGreenphylError('error' => "Failed to login! OpenID identifier not found in database! Did you register on GreenPhyl using that openID?");
                }
                elsif ((1 != @users) || !$users[0])
                {
                    Greenphyl::PrintDebug("Not just 1 user returned: " . @users . "\n" . join(', ', @users));
                    main::ThrowGreenphylError('error' => "Failed to login! Duplicate OpenID identifiers found in database!");
                }
                $g_session->param('user', $users[0]->getHashValue());
                Greenphyl::PrintDebug('User "' . $users[0] . '" successfuly logged in!');

            }
            else
            {
                main::ThrowGreenphylError('error' => "Failed to login! $oauth2_data");
            }
        }
        else
        {
            Greenphyl::PrintDebug("Unknown OpenID authentication phase: '$openid_phase'");
        }
    }


    # if user is logged in, set current user in DB session
    if (GetCurrentUser())
    {
        $dbh ||= Greenphyl::GetDatabaseHandler();
        $dbh->do("SET \@authenticated_user := '" . $g_current_user->login . "';")
            or confess "ERROR: Failed to set current (acting) user!\n" . $dbh->errstr;
    }
}


=pod

=head2 CheckAccess

B<Description>: Tells if the given user meets the given requirements.
If the user doesn't have the required access level, then the option hash is
changed to prevent access to page content:
The following keys are removed: 'before_content', 'after_content', 'pager_data'.
The following keys are modified:
'title', 'content' (set to 'users/login.tt'), 'form_data' (replaced by login
form parameters).
This function is called by GreenPhyl API system when rendering a page. You only
not to set the appropriate values in the option hash (as described below) given
to the RenderHTMLFullPage() procedure.

B<ArgsCount>: 2

=over 4

=item $option_hash: (hash ref) (R)

A hash with the following key/values:

-'require_access': (optional) an array of access level required to access to
    current page. By default, access is granted.

-access_needs: (optional) can be one of 'none', 'any' or 'all'.
    'none': current user must not have any of the required access level in order
        to access to the page.
    'any': current user must have at least one of the required access
        level in order to access to the page.
    'all': current user must have all of the required access level in order to
        access to the page.
    Default: 'any'.

=item $user: (Greenphyl::User) (O)

User to check. Default: current user.

=back

B<Return>: (boolean)

True if the user is allowed to access the page, false otherwise.

B<Example>:

    if (GetCurrentUser()->hasAccess(GP('ACCESS_KEY_REGISTERED_USER')))
    {
        # do some computation stuff or other processing stuff
        ...
    }

    my $html_content = RenderHTMLFullPage(
        {
            'title'                => 'Page title',
            'content'              => 'page_template.tt',
            'access_message'       => 'You need to log in first!',
            'require_access'       => [GP('ACCESS_KEY_REGISTERED_USER')],
        },
    );

=cut

sub CheckAccess
{
    my ($option_hash, $user) = @_;

    my $access_granted = 0;

    # check if a specific access is required
    if ($option_hash->{'require_access'})
    {
        $user ||= GetCurrentUser();

        # count how many required access level has the user
        my $owned_access = 0;
        foreach my $required_access (@{$option_hash->{'require_access'}})
        {
            if ($user->hasAccess($required_access))
            {
                ++$owned_access;
            }
        }

        $option_hash->{'access_needs'} ||= 'any'; # default to 'any'
        if (   (('none' eq $option_hash->{'access_needs'})
                && (!$owned_access))
            || (('any' eq $option_hash->{'access_needs'})
                && $owned_access)
            || (('all' eq $option_hash->{'access_needs'})
                && ($owned_access == @{$option_hash->{'require_access'}}))
            )
        {
            Greenphyl::PrintDebug('Access granted!');
            $access_granted = 1;
        }

        if (!$access_granted)
        {
            Greenphyl::PrintDebug('Access denied! Access needs: ' . $option_hash->{'access_needs'} . '; Required: ' . join(', ', @{$option_hash->{'require_access'}}));
            # access denied, clear page data and display login form
            delete($option_hash->{'before_content'});
            delete($option_hash->{'after_content'});
            delete($option_hash->{'pager_data'});
            $option_hash->{'title'}     = 'Authentication';
            $option_hash->{'content'}   = 'users/login.tt';
            $option_hash = GetOpenIDPageParameters($option_hash);
            my $current_url = CGI::url('-query'=>1);
            $current_url =~ s/([?;&])logout=[^?;&]*/$1/g;
            $option_hash->{'form_data'} =
                {
                    'action'      => $current_url,
                    'identifier'  => 'login_form',
                    'submit'      => 'Login',
                    'customstyle' => 'center-block',
                }
            ;
        }
    }
    else
    {
        Greenphyl::PrintDebug('No specific access required');
        $access_granted = 1;
    }

    return $access_granted;
}


=pod

=head2 GetCurrentUser

B<Description>: Returns current user. If current user is not logged in, it
returns the anonymous user object which is evaluated to false in boolean
comparisons.

B<ArgsCount>: 0

B<Return>: (boolean)

current user object (true value) or the anonymous user object if no user is
logged in (false value).

B<Caller>: general

B<Example>:

    if (GetCurrentUser())
    {
        # someone is logged in
        ...
    }
    else
    {
        # anonymous access
        ...
    }

=cut

sub GetCurrentUser
{
    if (!defined($g_current_user))
    {
        if ($g_session && $g_session->param('user'))
        {
            $g_current_user = Greenphyl::User->new(
                GetDatabaseHandler(),
                {
#                    'load'    => 'none',
#                    'members' => $g_session->param('user'),
                    'selectors' => {
                        'id' => $g_session->param('user')->{'id'},
                        'flags' => ['NOT LIKE', '%disabled%'],
                    },
                },
            );
        }
        $g_current_user ||= $Greenphyl::User::ANONYMOUS;
    }

    return $g_current_user;
}


=pod

=head2 IsAdmin

B<Description>: Tells if current user is admin or not.
Note: this function can be used in HTML templates.

B<ArgsCount>: 0

B<Return>: (boolean)

a true value if current user is admin.

B<Caller>: general

B<Example>:

    if (IsAdmin())
    {
        # current user is admin
        ...
    }

=cut

sub IsAdmin
{
    return (GetCurrentUser()->hasAccess(Greenphyl::GP('ACCESS_KEY_ADMINISTRATOR')))
        #+FIXME: remove old version compatibility
        || ($g_session && $g_session->param('admin'));
}


=pod

=head2 GetOpenIDPageParameters

B<Description>: Returns a page parameters hash containing the values required
by the OpenID system.

B<ArgsCount>: 0-1

=over 4

=item $page_parameters: (hash ref) (O)

Other page parameters that will be merged with OpenID parameters.

=back

B<Return>: (hash ref)

The page parameters containing the OpenID parameter values.

=cut

sub GetOpenIDPageParameters
{
    my ($page_parameters) = @_;

    $page_parameters ||= {};
    
    if (!ref($page_parameters) || (ref($page_parameters) ne 'HASH'))
    {
        confess "Invalid page parameters given to GetOpenIDPageParameters!\n";
    }

    if (Greenphyl::GP('ENABLE_OPENID'))
    {
        my @openids = Greenphyl::OpenID->new(GetDatabaseHandler(), {'selectors' => {'enabled' => 1,}, 'sql' => {'ORDER BY' => 'icon_index'}, });

        $page_parameters->{'css'} ||= [];
        if (!ref($page_parameters->{'css'}))
        {
            $page_parameters->{'css'} = [$page_parameters->{'css'}];
        }
        push(@{$page_parameters->{'css'}}, 'openid');

        $page_parameters->{'javascript'} ||= [];
        if (!ref($page_parameters->{'javascript'}))
        {
            $page_parameters->{'javascript'} = [$page_parameters->{'javascript'}];
        }
        push(@{$page_parameters->{'javascript'}}, 'openid-jquery');

        $page_parameters->{'openids'} = \@openids;
    }

    return $page_parameters;
}



=pod

=head2 OpenIDAuthenticatePhase1

B<Description>: Handles the first part of OpenID identification process.
Throws a redirection where the user should be redirected to continue the
authentication process if needed.

B<ArgsCount>: 2

=over 4

=item $openid_url: (string) (R)

URL of the OpenID identity provider.

=item $return_url: (string) (R)

GreenPhyl URL that will handle the OpenID identity provider response.

=back

B<Return>: (nothing)

=cut

sub OpenIDAuthenticatePhase1
{
    my ($openid_url, $return_url) = @_;

    if (Greenphyl::GP('ENABLE_OPENID') && $openid_url && IsRunningCGI())
    {
        # dynamically loads OpenID libraries
        eval('
            use Net::OpenID::Consumer;
            use LWP::UserAgent;
        ');

        if ($@)
        {
            confess $@;
        }

        # try OpenID identification...
        # Workaround for a known problem with myopenid. Change "http" to "https".
        if ($openid_url =~ /myopenid/)
        {
            $openid_url =~ s@(^http://|^(?!https))@https://@;
        }
        
        my $session = Greenphyl::Web::GetSession();
        my $consumer_secret = '';
        for (1..16)
        {
            $consumer_secret .= chr(32+int(rand(95)));
        }

        $session->param('consumer_secret', $consumer_secret);
        
        my $csr = Net::OpenID::Consumer->new(
             # The user agent which sends the openid off to the server.
             'ua'              => LWP::UserAgent->new,
             'required_root'   => Greenphyl::GP('BASE_URL') . '/',
             'consumer_secret' => $consumer_secret,
        );

        my $claimed_id = $csr->claimed_identity($openid_url);

        if ($claimed_id)
        {
            # Automatically redirect the user to the endpoint.
            my $redirect_url = $claimed_id->check_url(
                # The place we go back to.
                'return_to'  => $return_url,
                # Having this simplifies the login process.
                'trust_root' => Greenphyl::GP('BASE_URL') . '/',
                'delayed_return' => 1,
            );
            Greenphyl::PrintDebug("Should redirect user to '$redirect_url'");
            main::ThrowRedirection($redirect_url);
        }
        else
        {
            main::ThrowGreenphylError('error' => "claimed_identity for '$openid_url' failed with error code: " . $csr->errcode());
        }
    }
}


=pod

=head2 OpenIDAuthenticatePhase2

B<Description>: Handles second part of OpenID identification process.

B<ArgsCount>: 0

B<Return>: (list of 2 scalar)

The first string is the status code:
'verified', 'not_openid', 'setup_needed', 'cancelled' or 'error'.

The second one it the OpenID data associated to that status code:
-in case of 'verified', it will be the OpenID identity;
-otherwise it will be the error message.

=cut

sub OpenIDAuthenticatePhase2
{
    if (!IsRunningCGI())
    {
        return ('error', 'Not running CGI!');
    }

    my ($session, $consumer_secret);
    eval "\$session = Greenphyl::Web::GetSession();";
    if ($session)
    {
        $consumer_secret = $session->param('consumer_secret');
        $session->clear('consumer_secret');

        if (!$consumer_secret)
        {
            main::ThrowGreenphylError('error' => "Your current user session have not been found! You need either to enable cookies or just retry.");
        }
    }

    my ($status, $openid_data) = ('error', 'OpenID disabled!');
    if (Greenphyl::GP('ENABLE_OPENID'))
    {
        # dynamically loads OpenID libraries
        eval('
            use Net::OpenID::Consumer;
            use LWP::UserAgent;
        ');
        if ($@)
        {
            confess $@;
        }

        my $csr = Net::OpenID::Consumer->new(
            'required_root'   => Greenphyl::GP('BASE_URL') . '/',
            'consumer_secret' => $consumer_secret,
            # Where to get the information from.
            'args'            => Greenphyl::Web::GetCGI(),
        );

        $csr->handle_server_response(
            'not_openid' => sub {
                $status = 'not_openid';
            },
            'setup_needed' => sub {
                if ($csr->message->protocol_version >= 2)
                {
                    # (OpenID 2) retry request in checkid_setup mode (above)
                    # delayed_return => 1
                    $session->param('consumer_secret', $consumer_secret);
                }
                else
                {
                    # (OpenID 1) redirect user to $csr->user_setup_url
                    print Greenphyl::Web::Redirect($csr->user_setup_url);
                    exit(0);
                }
                $openid_data = shift();
                $status = 'setup_needed';
            },
            'cancelled' => sub {
                $status = 'cancelled';
            },
            'verified' => sub {
                my $vident = shift();
                $status = 'verified';
                $openid_data = $vident->url;
            },
            'error' => sub {
                $status = 'error';
                my ($errcode, $errtext) = @_;
                $openid_data = "ERROR $errcode: $errtext";
            },
        );
    }
    
    return ($status, $openid_data);
}


=pod

=head2 OAuth2AuthenticatePhase1

B<Description>: Handles the first part of OpenID identification process.
Throws a redirection where the user should be redirected to continue the
authentication process if needed.

B<ArgsCount>: 2

=over 4

=item $openid_url: (string) (R)

URL of the OpenID identity provider.

=item $return_url: (string) (R)

GreenPhyl URL that will handle the OpenID identity provider response.

=back

B<Return>: (nothing)

=cut

sub OAuth2AuthenticatePhase1
{
    my ($oauth2_url, $return_url) = @_;

    if (Greenphyl::GP('ENABLE_OPENID') && $oauth2_url && IsRunningCGI())
    {
        # dynamically loads OpenID libraries
        eval('
            use Net::OAuth2::AccessToken;
            use Net::OAuth2::Profile::WebServer;
            use LWP::UserAgent;
        ');

        if ($@)
        {
            confess $@;
        }

        my $session = Greenphyl::Web::GetSession();
        my $client_secret = '';
        for (1..16)
        {
            $client_secret .= chr(32+int(rand(95)));
        }

        $session->param('client_secret', $client_secret);

        # try OAuth2 authentication...
        my $auth = Net::OAuth2::Profile::WebServer->new(
            'name'                   => 'Google Contacts',
            'client_id'              => 'guignonv@gmail.com', #+FIXME: get user login
            'client_secret'          => $client_secret,
            'site'                   => 'https://accounts.google.com',
            'scope'                  => 'email', # 'https://www.google.com/m8/feeds/',
            'authorize_path'         => '/o/oauth2/auth',
            'access_token_path'      => '/o/oauth2/token',
            'redirect_uri'           => $return_url,
            # 'protected_resource_url' =>  'https://www.google.com/m8/feeds/contacts/default/full',
        );


        # Let user ask for a grant from the resource owner
        # Greenphyl::PrintDebug($auth->authorize_response->as_string);
        # Greenphyl::PrintDebug('URL: ' . $auth->authorize);
        main::ThrowRedirection($auth->authorize);
    }
}


=pod

=head2 OAuth2AuthenticatePhase2

B<Description>: Handles the first part of OpenID identification process.
Throws a redirection where the user should be redirected to continue the
authentication process if needed.

B<ArgsCount>: 2

=over 4

=item $openid_url: (string) (R)

URL of the OpenID identity provider.

=item $return_url: (string) (R)

GreenPhyl URL that will handle the OpenID identity provider response.

=back

B<Return>: (nothing)

=cut

sub OAuth2AuthenticatePhase2
{
    if (!IsRunningCGI())
    {
        return ('error', 'Not running CGI!');
    }

    my ($session, $client_secret);
    eval "\$session = Greenphyl::Web::GetSession();";
    if ($session)
    {
        $client_secret = $session->param('client_secret');
        $session->clear('client_secret');

        if (!$client_secret)
        {
            main::ThrowGreenphylError('error' => "Your current user session have not been found! You need either to enable cookies or just retry.");
        }
    }

    my ($status, $oauth2_data) = ('error', 'OAuth2 disabled!');
    if (Greenphyl::GP('ENABLE_OPENID'))
    {
        # dynamically loads OpenID libraries
        eval('
            use Net::OAuth2::AccessToken;
            use Net::OAuth2::Profile::WebServer;
            use LWP::UserAgent;
        ');
        if ($@)
        {
            confess $@;
        }

        Greenphyl::PrintDebug('DEBUG 0');
#        # The $info are the parameters from the callback to your service, it
#        # will contain a 'code' value.
#        my $access_token  = $auth->get_access_token($info->{'code'});
#
#        # communicate with the resource serve
#        my $response      = $access_token->get('/me');
#        $response->is_success
#            or die "error: " . $response->status_line;
#
#        print "Yay, it worked: " . $response->decoded_content;
#
#        
#        my $csr = Net::OpenID::Consumer->new(
#            'required_root'   => Greenphyl::GP('BASE_URL') . '/',
#            'client_secret' => $client_secret,
#            # Where to get the information from.
#            'args'            => Greenphyl::Web::GetCGI(),
#        );
#
#        $csr->handle_server_response(
#            'not_openid' => sub {
#                $status = 'not_openid';
#            },
#            'setup_needed' => sub {
#                if ($csr->message->protocol_version >= 2)
#                {
#                    # (OpenID 2) retry request in checkid_setup mode (above)
#                    # delayed_return => 1
#                    $session->param('client_secret', $client_secret);
#                }
#                else
#                {
#                    # (OpenID 1) redirect user to $csr->user_setup_url
#                    print Greenphyl::Web::Redirect($csr->user_setup_url);
#                    exit(0);
#                }
#                $oauth2_data = shift();
#                $status = 'setup_needed';
#            },
#            'cancelled' => sub {
#                $status = 'cancelled';
#            },
#            'verified' => sub {
#                my $vident = shift();
#                $status = 'verified';
#                $oauth2_data = $vident->url;
#            },
#            'error' => sub {
#                $status = 'error';
#                my ($errcode, $errtext) = @_;
#                $oauth2_data = "ERROR $errcode: $errtext";
#            },
#        );
    }
    
    return ($status, $oauth2_data);
}




=pod

=head1 AUTHORS

Mathieu ROUARD (Bioversity), m.rouard@cgiar.org

Valentin GUIGNON (Bioversity), v.guignon@cgiar.org

Matthieu CONTE (Syngenta)

Sebastien BRIOIS (Syngenta)

=head1 VERSION

Version 2.0.0

Date 06/08/2015

=head1 SEE ALSO

GreenPhyl documentation.

=cut

return 1; # package return
