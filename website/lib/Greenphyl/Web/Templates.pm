=pod

=head1 NAME

Greenphyl::Web::Template - Web template API

=head1 SYNOPSIS

    my $html_output .= ProcessPage(
           {
               'content' => 'my_template.tt',
               'param1'  => 'val1',
               'param2'  => 'val2',
           }
    );


=head1 REQUIRES

Perl5

=head1 EXPORTS

ProcessTemplate ProcessPage

=head1 DESCRIPTION

This module handles GreenPhyl template processing functions.

=cut

package Greenphyl::Web::Templates;

use strict;
use warnings;

use Carp qw(cluck confess croak);
require CGI;
use Template;

use Greenphyl;
use Greenphyl::Web;
use Greenphyl::Web::Access;

use JSON;

use base qw(Exporter);
our @EXPORT = qw(ProcessTemplate ProcessPage);




# Package variables
####################

=pod

=head1 VARIABLES

B<$g_template_manager>: (Template)

The global template object. We set 'RECURSION' => 1 for security against
relative path hacks: only subdirectories can be processed.

=cut

my $g_template_manager = Template->new(
    {
        'INCLUDE_PATH' => Greenphyl::GP('TEMPLATES_PATH'),
        'RECURSION' => 1,
    },
);




# Package subs
###############

=pod

=head1 STATIC METHODS

=head2 ProcessTemplate

B<Description>: process given template file and returns its HTML output.

Note: the 'debug' template parameters is always passed to any
template without being requiered in the $option_hash argument.

Note: the following functions are also exported by default:
GP, gpurl (corresponding to GetURL), CheckHTMLIdentifierOk,
IsHTMLIdentifierAvailable, UseHTMLIdentifier, GetFreeHTMLIdentifier, IsAdmin,
GetCurrentUser and Array.

B<ArgsCount>: 1-2

=over 4

=item $template_file: (string) (R)

File name of the template file including the '.tt' extensions. Template files
are searched into the path specified by the Greenphyl::GP('TEMPLATES_PATH') constant.
Files can be located in subdirectories of that directory but in that case, the
relative path must be included in the file name.

=item $option_hash: (hash ref) (O)

Hash containing template processing parameters that will be given to the
template.

=back

B<Return>: (string)

the HTML output.

B<Caller>: general

B<Example>:

    # for a regular page
    my $html_output .= ProcessTemplate(
           'some_template.tt',
           {
               'param1'           => 'val1',
               'param2'           => 'val2',
           }
    );
    
=cut

sub _GetAsArray
{
    my $array = shift();
    if ('ARRAY' eq ref($array))
    {
        return $array;
    }
    elsif ((ref($array) && $array) || (!ref($array) && defined($array)))
    {
        return [$array];
    }
    else
    {
        return [];
    }
}

sub ProcessTemplate
{
    my ($template_file, $option_hash) = @_;

    if (!$template_file || ((2 == @_) && (ref($option_hash) ne 'HASH')))
    {
        confess "ERROR: ProcessTemplate: invalid argument! A template file name and a hash ref with template options must be specified.\n";
    }
    
    # add debug mode parameter
    $option_hash->{'debug'} ||= Greenphyl::GP('DEBUG_MODE');

    # add session ID
    $option_hash->{'session_id'} ||= Greenphyl::Web::GetSession()->id();

    # add global helper functions
    $option_hash->{'GP'}                        = \&Greenphyl::GP;
    $option_hash->{'gpurl'}                     = \&Greenphyl::Web::GetURL;
    $option_hash->{'mode'}                      = Greenphyl::Web::GetPageMode();
    $option_hash->{'link_target'}               = Greenphyl::Web::GetLinkTarget();
    $option_hash->{'CheckHTMLIdentifierOk'}     = \&Greenphyl::Web::CheckHTMLIdentifierOk;
    $option_hash->{'IsHTMLIdentifierAvailable'} = \&Greenphyl::Web::IsHTMLIdentifierAvailable;
    $option_hash->{'UseHTMLIdentifier'}         = \&Greenphyl::Web::UseHTMLIdentifier;
    $option_hash->{'GetFreeHTMLIdentifier'}     = \&Greenphyl::Web::GetFreeHTMLIdentifier;
    $option_hash->{'IsAdmin'}                   = \&Greenphyl::Web::Access::IsAdmin;
    $option_hash->{'GetCurrentUser'}            = \&Greenphyl::Web::Access::GetCurrentUser;
    $option_hash->{'Array'}                     = \&_GetAsArray;
    $option_hash->{'to_json'}                   = \&to_json;

    my $output;
    $g_template_manager->process($template_file, $option_hash, \$output)
        or croak($g_template_manager->error . "\n");

    return $output;

    # following code removed because templates called inside orther templates
    # aren't tagged. Therefore, it's better to manualy add the BEGIN-END tags
    # to each template, which also solves the problem for header and footer
    # with the <html> tag.
    #my $html;
    #$html .= qq|\n<!-- BEGIN $template_file -->\n| unless $output =~ m/<html/i;
    #$html .= $output;
    #$html .= qq|\n<!-- END $template_file -->\n|   unless $output =~ m/<\/html>/i;

    #return $html;
}


=pod

=head2 ProcessPage

B<Description>: generates HTML output from provided options.

Please see ProcessTemplate() documentation for variables and functions always
passed to templates.

B<ArgsCount>: 1

=over 4

=item $option_hash: (hash ref) (R)

Hash containing template processing parameters that will be given to the
template. You can either specify a key parameter 'content' containing
the file name of the template file to process (including the '.tt' extensions)
or raw HTML code.
Template files are searched into the path specified by the Greenphyl::GP('TEMPLATES_PATH')
constant. Files can be located in subdirectories of that directory but in that
case, the relative path must be included in the file name.

Available options (not including template file specific options):
 'title', 'dump_links', 'before_content', 'content', 'after_content',
 'form_data' and 'pager_data'.

Options auto-set if missing: 'debug'

Please refer to 'content.tt' documentation for details.

=back

B<Return>: (string)

the HTML output.

B<Caller>: general

B<Example>:

    # for a regular page
    my $html_output .= ProcessPage(
        {
            'title'   => 'My Page',
            'content' => 'my_template.tt',
            'param1'  => 'val1',
            'param2'  => 'val2',
        }
    );

    # for a page without template file
    my $html_content = '<p>Beautiful day today. Mary is a girl and John is a boy.</p>';
    my $html_output .= ProcessPage(
        {
            'title'   => 'Hello World',
            'content' => $html_content,
        }
    );

    # for a page with a form
    my $html_output .= ProcessPage(
        {
            'content'   => 'my_form_template.tt',
            'form_data' =>
                {
                    'identifier' => 'my_form',
                    'submit'     => 'Go go go!',
                    'noclear'    => 1,
                },
            'param1'    => 'val1',
            'param2'    => 'val2',
        }
    );

    # (almost) all the parameters
    my $html_output .= ProcessPage(
        {
            'before_content' => '<img class="centered" src="http://www.greenphyl.fr/images/header_line.png"/>',
            'content'        => 'full_template.tt',
            'after_content'  => 'pre_footer.tt',
            'dump_links'     =>
                [
                    'xls' => 'dumper.cgi?page=full&amp;format=xls',
                    'csv' => 'dumper.cgi?page=full&amp;format=csv',
                ],
            'pager_data'     =>
                {
                    pager       => $pager,
                    page_link   => $page_link,
                    entry_link  => $entry_link,
                    entry_range => $entry_range,
                },
            'form_data'      =>
                {
                    'identifier' => 'my_form',
                    'action'     => 'submitter.cgi',
                    'method'     => 'POST',
                    'enctype'    => 'application/x-www-form-urlencoded',
                    'submit'     => 'Go go go!',
                    'nosubmit'   => 0,
                    'noclear'    => 1,
                },
            'access_message'       => 'You need to log in first!',
            'require_access'       => [GP('ACCESS_KEY_REGISTERED_USER')],
            'access_needs'         => 'any',
            'param1'         => 'val1',
            'param2'         => 'val2',
            'param3'         => 'val3',
        }
    );

=cut

sub ProcessPage
{
    my ($option_hash) = @_;

    if ((1 != @_) || (ref($option_hash) ne 'HASH'))
    {
        confess "ERROR: ProcessPage: invalid argument! A hash ref containing template name and options must be specified.\n";
    }
    
    if (!exists($option_hash->{'content'}))
    {
        confess "ERROR: ProcessPage called without specifying a 'content' in the option hash!\n";
    }

    if ($option_hash->{'require_access'}
        && @{$option_hash->{'require_access'}})
    {
        # make sure current user is allowed to view the page
        if (!Greenphyl::Web::Access::CheckAccess($option_hash))
        {
            main::ThrowAccessDenied('error' => 'Access denied!', 'access' => $option_hash);
        }
    }

    return ProcessTemplate('content.tt', $option_hash);
}


=pod

=head1 AUTHORS

Mathieu ROUARD (Bioversity), m.rouard@cgiar.org

Valentin GUIGNON (Bioversity), v.guignon@cgiar.org

Matthieu CONTE (Syngenta)

=head1 VERSION

Version 3.0.1

Date 14/02/2013

=head1 SEE ALSO

GreenPhyl documentation. Template Toolkit documentation.

=cut

return 1; # package return
