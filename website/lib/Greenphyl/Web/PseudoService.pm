=pod

=head1 NAME

Greenphyl::Web::PseudoService - Handles service-like features

=head1 SYNOPSIS


    use Greenphyl::Web::PseudoService;

    our $SEQUENCE_SERVICES =
        {
            'check_seq_textids' => \&CheckSeqTextID,
            'get_sequence_data' => \&GetSequenceList,
            'get_homologs_data' => \&GetHomologList,
        };
    ...

    # CODE START
    #############

    print HandleService($SEQUENCE_SERVICES);

    exit(0);
    
=head1 REQUIRES

Perl5

=head1 EXPORTS

HandleService

=head1 DESCRIPTION

This module handles generic pseudo-web services. Typical use is in Perl script
that are called through AJAX calls. Return formats are various (HTML, JSON,...).

=cut

package Greenphyl::Web::PseudoService;


use strict;
use warnings;

use Carp qw(cluck confess croak);

use base qw(Exporter);

our @EXPORT = qw(HandleService); 

use Greenphyl;
use Greenphyl::Web;
use Greenphyl::Dumper;




# Package constants
####################

=pod

=head1 CONSTANTS

B<$DEBUG>: (boolean)

When set to true, it enables debug mode.

=cut

our $DEBUG = 0;




# Package subs
###############

=pod

=head1 FUNCTIONAL INTERFACE

=head2 HandleService

B<Description>: Handle the service execution and error handling. This function
uses the following HTTP parameters (either POST or GET):
-'service': must contain a valid service name;
-'format': must contain the output format, typically 'json', 'html' or 'xml';
-'mode': only requiered if format set to 'html' or if the service only outputs
a hash ref (as parameter for RenderHTMLFullPage function). It can be one of
'inner', 'frame' or 'full' (default) . See RenderHTMLFullPage for details.
  'inner': inner HTML code for AJAX calls;
  'frame': no graphic header or footer for jQuery UI dialog boxes/iframes;
  'full': stand-alone web page.

B<ArgsCount>: 1

=over 4

=item $service_hash (hash ref):

Hash containing the association between a service name and a function.
Each key of the hash is a service name. Each value is a function reference.
Functions are expected to return either:
-a single or an array (ref) of dumpable Greenphyl objects;
-a string ready to be output as is;
-a hash ref having the same structure as the hash provided to RenderHTMLFullPage
function (in Greenphyl::Web module).

=back

B<Return>: nothing

B<Caller>: general

B<Example>:

    use Greenphyl::Web::PseudoService;

    our $SEQUENCE_SERVICES =
        {
            'check_seq_textids' => \&CheckSeqTextID,
            'get_sequence_data' => \&GetSequenceList,
            'get_homologs_data' => \&GetHomologList,
        };
    ...

    # CODE START
    #############

    print HandleService($SEQUENCE_SERVICES);

    exit(0);

=cut


sub HandleService
{
    my ($service_hash) = @_;

    my ($output, $service_output, $format_parameters) = ('', '', undef);
    # get service name
    my $service_name = GetParameterValues('service') || '';
    PrintDebug("Got service '$service_name'");
    
    # get requested output format
    my $output_format = GetDumpFormat() || '';
    PrintDebug("Got format '$output_format'");
    
    # note: output mode is handled if output format is set to 'html'

    eval
    {
        # process requested service
        if (exists($service_hash->{$service_name}))
        {
            ($service_output, $format_parameters) = $service_hash->{$service_name}->($output_format);
            PrintDebug("Service object:\n$service_output");
        }
        else
        {
            PrintDebug("An invalid service has been requested: '$service_name'");
            confess "Invalid service!";
        }

        # check if the service specified parameters
        if (!$format_parameters)
        {
            # none from service, try from form data
            $format_parameters = GetDumpParameters();
        }

        # format output if needed
        if ((($output_format eq 'html') && ref($service_output))
            || ('HASH' eq ref($service_output)))
        {
            PrintDebug('Renders HTML service');
            my $parameters_hash = {};
            if ('HASH' eq ref($service_output))
            {
                # hash of HTML parameters
                $parameters_hash = $service_output;
            }
            elsif ('HASH' eq ref($format_parameters))
            {
                $parameters_hash = $format_parameters;
            }
            $output_format = $parameters_hash->{'format'} = 'html';
            $parameters_hash->{'parameters'} = $format_parameters;

            $output = RenderHTMLFullPage($parameters_hash);
        }
        elsif ('ARRAY' eq ref($service_output))
        {
            PrintDebug('Service on an array of objects');
            # array of objects
            $output = DumpObjects($service_output, $output_format, $format_parameters);
        }
        elsif (ref($service_output))
        {
            PrintDebug('Service on a single object');
            $output = DumpObjects([$service_output], $output_format, $format_parameters);
        }
        else
        {
            PrintDebug('Render a raw scalar service');
            # if we already got a scalar, just output the scalar data
            $output = $service_output;
        }

        # display HTTP response header if not already handled by html formater
        if (IsRunningCGI() && ($output_format ne 'html'))
        {
            my $mime = uc($output_format);
            if (!exists($Greenphyl::Web::MIME{$mime}))
            {
                $mime = 'TEXT';
            }
            print RenderHTTPResponse({
                'mime' => $mime,
                'header_args' => ['-expires' => '-1d'],
            });
        }

        # show output if one
        if ($output)
        {
            print $output;
        }
        elsif (IsRunningCGI())
        {
            # no result: display an empty page
            # we need at least a line break for the header in order to display an
            # empty page
            print "\n\n";
        }
    };

    my $error;
    if ($error = Exception::Class->caught())
    {
        # display HTTP response header
        if (IsRunningCGI())
        {
            print RenderHTTPResponse({'header_args' => ['-expires' => '-1d'],});
        }
        print "An unexpected error occured! If the problem persists, you may contact the site administrator giving him this code: $$\n";
        cluck "An error occured in pseudo-service $0 (PS $$):\n$error";
    }
}


=pod

=head1 AUTHORS

Mathieu ROUARD (Bioversity), m.rouard@cgiar.org

Valentin GUIGNON (Bioversity), v.guignon@cgiar.org

Matthieu CONTE (Syngenta)

Sebastien BRIOIS (Syngenta)

=head1 VERSION

Version 1.0.0

Date 23/10/2012

=head1 SEE ALSO

#--- other documentation or objects related to this package
GreenPhyl documentation.

=cut

return 1; # package return
