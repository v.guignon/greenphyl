package Greenphyl::SGEUtils;

use strict;
use warnings;
no warnings 'once';

use Carp qw (cluck confess croak);

use Greenphyl::Config;

use base 'Exporter';
our @EXPORT = qw (
    launch_job   waitSGEResult
);

my $SLEEP_TIME = 10; # time to wait between each SGE job status check
my $MPI_EXEC   = 'mpiexec';

=head2 launch_job

Launches an SGE job

=cut

sub launch_job
{
    my ( $code, $species_name, $program, $parameters, $name, $process_dir ) = @_;

    my %files;
    for (qw( sge_stdout sge_stderr ))
    {
        my $file = $files{$_} = "$process_dir/" . join( "_", $_, $code, $species_name ) . ".txt";
        unlink $file if -e $file;
        system("touch $file") == 0 or die "could not create '$file': $?"
    }

    $ENV{SGE_ROOT} = $Greenphyl::Config::SGE_ROOT;
    my $dir_qsub = $Greenphyl::Config::QSUB_COMMAND;

    if ( $^O eq 'MSWin32' or $ENV{TESTING} )
    {
        my $call = "$program $parameters 1>$files{sge_stdout} 2>$files{sge_stderr}";
        my $pid = system 1, $call;
        $pid = 9999;    # to enable job_process.cgi tests
        sleep 2;    # this enables testing to run through without collisions
        return $pid, "($pid) : '$call'";
    }

    my $qsub_result = `$dir_qsub -cwd -b y -o $files{sge_stdout} -e $files{sge_stderr} -N $name -j n -V -m a $program $parameters`
        or die $!;

    #retrieve identifier
    my ($task_id) = ( $qsub_result =~ m/(\d+)/ );

    return $task_id, $qsub_result;
}



sub waitSGEResult
{

    my ($job_id) = @_;
    
    confess if (!$job_id);
    
    #+debug warn "SGE JOB ID: $job_id\n";

    # check the queue and wait for the job to end
    #
    # Job status:
    #  d(eletion), E(rror), h(old), r(unning), R(estarted), s(uspended)
    #  S(uspended), t(ransfering), T(hreshold), w(aiting)
    #
    # qstat status codes:
    #  qw  (queue waiting)
    #  t   (transfering to a node)
    #  r   (running)
    #  Eqw (done with an error)
    #
    my $qstat_result;
    
    while (($qstat_result = `qstat 2>/dev/null`)
           && ($qstat_result =~ m/^ *$job_id +\S+ +\S+ +\S+ +(qw|r|t) +/m))
    {
        
        sleep($SLEEP_TIME);
    }
    
    
    # check if queue error
    if ($qstat_result =~ m/^ *$job_id +\S+ +\S+ +\S+ +Eqw +/m)
    {
        # remove command from queue
        `$Greenphyl::Config::QDEL_COMMAND $job_id 2>/dev/null`;
        # output an error message on STDERR
        print STDERR "Job execution failed (on SGE).";
        return -1;
    }
    
    return 1;
}

1;
