=pod

=head1 NAME

Greenphyl::Run::Diamond - Execute Diamond program

=head1 SYNOPSIS

    use Greenphyl::Run::Diamond;
    ...
    sub DisplayResults
    {
        my ($results) = @_;
        ...
    }
    ...
    
    my $diamond_program = new Greenphyl::Run::Diamond('diamond');
    $diamond_program->validateInputFiles('temp/test.fasta');
    $diamond_program->validateParameters(
        '-e=1e-10',
        '-k=5',
        '-d=/gs7k1/projects/greenphyl/data/v5/species/banks/ARATH',
        '-f=6',
    );
    $diamond_program->setOutputFiles('temp/output.diamond');
    if (0 == $diamond_program->run())
    {
        # execution ok
        DisplayResults($diamond_program->getResults());
        ...
    }
    else
    {
        print "ERROR: Diamond execution failed!\n";
    }

=head1 REQUIRES

Perl5, Diamond program

=head1 EXPORTS

Nothing

=head1 DESCRIPTION

Execute Diamond program using given input FASTA file and parameters.
This module also provides static methods to handle some Diamond tasks.

=cut

package Greenphyl::Run::Diamond;


use strict;
use warnings;

use Carp qw(cluck confess croak);

use base qw(Greenphyl::Run::Program);

use Greenphyl;

use Getopt::Long;
use Bio::SeqIO;
use Bio::SearchIO;





# Package constants
####################

=pod

=head1 CONSTANTS

B<$DEBUG>: (boolean)

Controls debug mode. If set to a true value, displays debug messages.

B<$Diamond_PIN_EXT>: (string)

File extension for PIN files (for Diamond bank).

B<$Diamond_PHR_EXT>: (string)

File extension for PHR files (for Diamond bank).

B<$Diamond_PSQ_EXT>: (string)

File extension for PSQ files (for Diamond bank).

B<$Diamond_ALIAS_EXT>: (string)

File extension for alias files (for Diamond bank).

B<$Diamond_FILE_EXT>: (string)

Default file extension for Diamond output files.

B<$FASTA_FILE_EXT>: (string)

Default file extension for FASTA files.

=cut

our $DEBUG = 0;
our $DIAMOND_BANK_EXT  = '.dmnd';
our $DIAMOND_FILE_EXT = '.blast';
our $FASTA_FILE_EXT = '.fasta';




# Package subs
###############

=pod

=head1 STATIC METHODS

=head2 CONSTRUCTOR

B<Description>: Creates a new instance of a Diamond program.

B<ArgsCount>: 1

=over 4

=item $diamond_program_name: (string) (R)

Name of the Diamond program to use. Can be one of:
diamondn, diamond, diamondx and diamondall

=item @other_args: (any) (O)

eventually other arguments.

=back

B<Return>: ([Greenphyl::PackageName]) #+++

a new instance.

B<Caller>: [general] #+++ general, specific class name, ...

B<Exception>:

=over 4

[=item exception_type

description, case when it occurs] #+++ see below

=back

#--- Example:

=over 4

=item Range error

thrown when "argument x" is outside the supported range, for instance "-1".

=back

B<Example>:

    [example code] #+++

=cut

sub new
{
    my ($proto, $diamond_program_name, @other_args) = @_;
    my $class = ref($proto) || $proto;

    # instance creation
    my $self = $class->SUPER::new($diamond_program_name, @other_args);
    bless($self, $class);

    return $self;
}



=pod

=head1 ACCESSORS

=cut

#--- put here setters and getters (copy-paste text below)
#
#=pod
#
#=head2 get[MemberName] #+++
#
#B<Description>: #+++
#
#B<ArgsCount>: #+++
#
#=over 4
#
#=item $arg: #+++
#
#=back
#
#B<Return>: #+++
#
#B<Caller>: #+++
#
#B<Exception>: #+++
#
#B<Example>: #+++
#
#=cut
#
#sub get[MemberName] #+++
#{
#    my ($self) = @_;
#    # check parameters
#    if ((1 != @_) || (not ref($self)))
#    {
#        confess "usage: my \$value = \$instance->get[MemberName]();"; #+++
#    }
#    return $self->{[MEMBER_NAME]}; #+++
#}

#=pod
#
#=head2 setMemberName #+++
#
#B<Description>:  #+++
#
#B<ArgsCount>:  #+++
#
#=over 4
#
#=item $arg: #+++
#
#=back
#
#B<Return>:  #+++
#
#B<Caller>:  #+++
#
#B<Exception>:  #+++
#
#B<Example>:  #+++
#
#=cut
#
#sub set[MemberName] #+++
#{
#    my ($self, $value) = @_;
#    # check parameters
#    if ((2 != @_) || (not ref($self)))
#    {
#        confess "usage: my \$value = \$instance->set[MemberName](\$value);"; #+++
#    }
#    $self->{[MEMBER_NAME]} = [$value]; #+++
#}


=pod

=head1 METHODS

=head2 validateInputFiles

B<Description>: #+++

B<ArgsCount>: #+++

=over 4

=item $arg: #+++

=back

B<Return>: #+++

B<Caller>: #+++

B<Exception>: #+++

B<Example>: #+++

=cut

sub validateInputFiles
{
    my ($self, @file_names) = @_;

    if (1 != @file_names)
    {
        confess "ERROR: No or too many input file name(s) provided!\n";
    }

    foreach my $file_name (@file_names)
    {
        # check if file exists and is readable
        if (!-e $file_name)
        {
            confess "ERROR: Input FASTA file '$file_name' not found!\n";
        }
        elsif (!-r $file_name)
        {
            confess "ERROR: Unable to read input FASTA file '$file_name'! Please check file permissions.\n";
        }

        # check FASTA content
        my $input_fasta_io = Bio::SeqIO->new(
                '-file'   => $file_name,
                '-format' => 'Fasta',
        );
        if (!$input_fasta_io)
        {
            confess "ERROR: invalid input FASTA file!\n";
        }
        # get all sequences seq_textid in FASTA file
        $self->{'sequences'} = {};
        while (my $sequence = $input_fasta_io->next_seq())
        {
            # save sequences ID to keep track of unmatched sequences
            $self->{'sequences'}->{$sequence->id} = 0;
        }
        $self->{'input_filename'} = $file_name;
    }
}


=pod

=head2 validateParameters

B<Description>: #+++

B<ArgsCount>: #+++

=over 4

=item $arg: #+++

=back

B<Return>: #+++

B<Caller>: #+++

B<Exception>: #+++

B<Example>: #+++

=cut

sub validateParameters
{
    my ($self, @parameters) = @_;

    my ($e_value, $max_best_hit, $diamond_bank, $output_format, $temp_dir);
    my @ARGV_BACK = @ARGV;
    @ARGV = @parameters;
    GetOptions(
        "d=s"        => \$diamond_bank,
        "evalue|e=s" => \$e_value,
        "k=s"        => \$max_best_hit,
        "outfmt|f=s" => \$output_format,
        "t=s"        => \$temp_dir,
    );
    @ARGV = @ARGV_BACK;

    #+FIXME: check and validate parameters...
    
    # check e-value
    $self->{'d'} = $diamond_bank;
    $self->{'e'} = $e_value;
    $self->{'k'} = $max_best_hit;
    $self->{'f'} = $output_format;
    $self->{'t'} = $temp_dir;

    return 0;
}


=pod

=head2 setOutputFiles

B<Description>: #+++

B<ArgsCount>: #+++

=over 4

=item $arg: #+++

=back

B<Return>: #+++

B<Caller>: #+++

B<Exception>: #+++

B<Example>: #+++

=cut

sub setOutputFiles
{
    my ($self, @file_names) = @_;

    if (1 != @file_names)
    {
        confess "ERROR: No or too many output file name(s) provided!\n";
    }

    return $self->{'output_filename'} = $file_names[0];
}


=pod

=head2 run

B<Description>: #+++

B<ArgsCount>: #+++

=over 4

=item $arg: #+++

=back

B<Return>: #+++

B<Caller>: #+++

B<Exception>: #+++

B<Example>: #+++

=cut

sub run
{
    my ($self) = @_;
    my %diamond_parameters;

    if (defined($self->{'input_filename'}))
    {
        $diamond_parameters{'-i'} = $self->{'input_filename'};
    }

    if (defined($self->{'output_filename'}))
    {
        $diamond_parameters{'-o'}      = $self->{'output_filename'};
    }

    if (defined($self->{'d'}))
    {
        $diamond_parameters{'-d'}      = $self->{'d'};
    }

    if (defined($self->{'e'}))
    {
        $diamond_parameters{'-e'}      = $self->{'e'};
    }

    if (defined($self->{'k'}))
    {
        $diamond_parameters{'-k'}      = $self->{'k'};
    }

    if (defined($self->{'f'}))
    {
        $diamond_parameters{'-f'} = $self->{'f'};
    }

    if (defined($self->{'t'}))
    {
        $diamond_parameters{'-t'} = $self->{'t'};
    }

    my $diamond_command = 'blastp';
    if (defined($self->{'command'}))
    {
        $diamond_command = $self->{'command'};
    }


    runDiamond($diamond_command, %diamond_parameters);
    
    return 0;
}


=pod

=head2 getResults

B<Description>: #+++

B<ArgsCount>: #+++

=over 4

=item $arg: #+++

=back

B<Return>: #+++

B<Caller>: #+++

B<Exception>: #+++

B<Example>: #+++

=cut

sub getResults
{
    my ($self) = @_;

    my $results = {
        'output_files' => [$self->{'output_filename'}],
        'sequences'    => $self->parseDiamondOutput($self->{'output_filename'}),
    };

    return $results;
}



=pod

=head1 FUNCTIONS

=head2 runDiamond

B<Description>:

B<ArgsCount>: 6

=over 4

=item $input_fasta_filename: (string) (R)

file name of a multifasta of protein sequence.

=item $evalue: (real) (U)

e-value for Diamond.

=item $database: (string) (R)

Diamond bank file to use as database.

=item $max_best_hit: (integer) (U)

Maximum number of best hits Diamond should report.

=item $output_directory: (string) (R)

path where the output Diamond files will be created.

=item $output_filename: (string) (R)

name of the Diamond output file.

=back

=cut

sub runDiamond
{
    my (%parameters) = @_;
    my @diamond_parameters;

    # special handling of input file:
    my $input_fasta_filename = $parameters{'-i'} or confess "ERROR: input file not specified!\n";
    delete($parameters{'-i'});
    push(@diamond_parameters, '-i', $input_fasta_filename);
    
    # special handling of ouput file:
    my $ouput_fasta_filename = $parameters{'-o'} or confess "ERROR: output file not specified!\n";
    delete($parameters{'-o'});
    push(@diamond_parameters, '-o', $ouput_fasta_filename);

    if (!exists($parameters{'-d'}))
    {
        confess "ERROR: no Diamond bank specified!\n";
    }

    #+FIXME: constants should not be hard coded here.

    # Diamond default parameters
    $parameters{'-p'} ||= 'blastp';
    $parameters{'-t'} ||= GP('TEMP_OUTPUT_PATH');
    
    # handle max_best_hit
    if (!exists($parameters{'-k'}))
    {
        $parameters{'-k'} = $parameters{'max_best_hit'};
        $parameters{'-k'} ||= 5;
    }
    delete($parameters{'max_best_hit'});
    
    my $diamond_cmd;
    $diamond_cmd = GP('DIAMOND_COMMAND');

    # push(@diamond_parameters, map {($_, $parameters{$_})} keys(%parameters));
    push(@diamond_parameters, %parameters);

    print "DEBUG: COMMAND: $diamond_cmd " . join(' ', @diamond_parameters) . "\n" if 1; #+debug $DEBUG;
    my $program_sh;
    open($program_sh, "-|")
        or exec({$diamond_cmd}
            $diamond_cmd, @diamond_parameters
        )
            or confess "ERROR: unable to launch Diamond! $?, $!\n";
    print join('', <$program_sh>);
    close($program_sh);            

}


=pod

=head2 FormatDB

B<Description>: format the given multi-FASTA files of protein sequence for
Diamond tool.

B<ArgsCount>: 1-3

=over 4

=item $fasta_files: (array ref/string) (R)

An array of multi-FASTA files to process into a single Diamond bank file.
A single file name can be specified as a string.

=item $output_db_name: (string) (R/U)

The output Diamond bank file name. In case of a single FASTA file, it can be
omitted (but set to undef if $options is used) and the FASTA file name will be
used as output name.

=item $options: (hash ref/string) (O)

A hash of additional formatdb parameters. The following keys are recognized:
-'gi': FASTA sequences have GI identifiers;
-'nucleotide': if set to true, FASTA file contain nucleotide sequences;
-'name': can be used to specify a human-readable explicit bank name;
-'extra': can contain raw command-line parameters appended to the formatdb
 command-line.
If a string is given, it will be treated as an 'extra' command-line parameter.

=back

=head3 SEE ALSO

http://gensoft.pasteur.fr/docs/diamond/0.8.29/diamond_manual.pdf

=cut

sub FormatDB
{
    my ($fasta_files, $output_db_name) = @_;

    # parameter check
    if (!ref($fasta_files))
    {
        # turn string into array
        $fasta_files = [$fasta_files];
    }
    elsif ('ARRAY' ne ref($fasta_files))
    {
        confess "ERROR: First parameter is not an array of FASTA file names!\n";
    }
    
    if (!@$fasta_files)
    {
        confess "ERROR: No FASTA file given!\n";
    }
    
    # if no output name given and only one fasta, use the same name
    if (!$output_db_name && (1 == @$fasta_files))
    {
        $output_db_name = $fasta_files->[0];
        $output_db_name =~ s/\.\w+$//;
    }
    
    if (!$output_db_name)
    {
        confess "ERROR: no output database name provided!\n";
    }
    
    # make sure files exist
    foreach my $fasta_file (@$fasta_files)
    {
        if (!-r $fasta_file)
        {
            confess "ERROR: Unable to read the FASTA file ('$fasta_file')!\n";
        }
    }
    
    # remove old bank file
    unlink($output_db_name . $DIAMOND_BANK_EXT);

    PrintDebug("Generating Diamond bank...");
    # check for single file
    if (1 == @$fasta_files)
    {
        my $command = GP('DIAMOND_COMMAND') . " makedb -i '$fasta_files->[0]' -d '$output_db_name'";
        PrintDebug("COMMAND: $command");
        if (system($command))
        {
            confess "ERROR: makedb failed for '" . $fasta_files->[0] . "'!\n$!\n";
        }
    }
    else
    {
        # multiple FASTA
        my $command = 'cat ' . join(' ', @$fasta_files) . ' | ' . GP('DIAMOND_COMMAND') . " -d '$output_db_name'";
        PrintDebug("COMMAND: $command");
        if (system($command))
        {
            confess "ERROR: makedb failed!\n$!\n";
        }
    }
    
    # check if Diamond database files were generated correctly
    unless (-e $output_db_name . $DIAMOND_BANK_EXT)
    {
        confess "ERROR: makedb did not produce the expected files ($DIAMOND_BANK_EXT) for '$output_db_name'!\n";
    }

    return $output_db_name;
}




=pod

=head2 loadBestHit

B<Description>: R

B<ArgsCount>: 1

=over 4

=item arg: $d (integer) (R)

D.

=back

B<Return>: (string)

A.

B<Example>:

    my

=cut

sub parseDiamondOutput
{
    my ($self, $diamond_result_filename) = @_;
    my $diamond_results = {};

    # check Diamond output format
    my $output_format = 'blast';
    if ($self->{'f'} && ($self->{'f'} eq '6'))
    {
        $output_format = 'blasttable';
    }

    # parse Diamond file
    my $diamond_io = new Bio::SearchIO(
        '-format' => $output_format,
        '-file' => $diamond_result_filename
        ) or confess("ERROR: $!\n");

    # init each sequence match records
    foreach my $query_seq_textid (keys(%{$self->{'sequences'}}))
    {
        $diamond_results->{$query_seq_textid} = {};
    }
    # process each query sequence
    while (my $result = $diamond_io->next_result())
    {
        # process each match
        while (my $hit = $result->next_hit())
        {
            if (my $hsp = $hit->next_hsp())
            {
                $diamond_results->{$result->query_name}->{$hit->name} = {'score' => $hsp->bits, 'e-value' => $hsp->evalue};
                if (!defined($hsp->bits) || !defined($hsp->evalue))
                {
                    confess "ERROR: failed to parse Diamond results (missing a score or an e-value for match between '" . $result->query_name . "' and '" . $hit->name . "')!\n";
                }
            }
            else
            {
                print "WARNING: no hsp found for '" . $result->query_name . "'!\n";
            }
        }
    }
    $diamond_io->close();

    return $diamond_results;
}


=pod

=head1 DIAGNOSTICS

#--- Give and explain here every error message the the module may generate.
#--- Use: (W)=warning (optional), (D)=deprecation (optional),
#---      (S)=severe warning (mandatory), (F)=fatal error (trappable),
#---      (X)=very fatal error (non-trappable).
#--- example:
#--- =over 4
#--- =item Negative radius
#---
#--- (F) A circle may not be created with a negative radius.
#---
#--- =back #+++

=head1 AUTHORS

Mathieu ROUARD (Bioversity), m.rouard@cgiar.org

Valentin GUIGNON (Bioversity), v.guignon@cgiar.org

Matthieu CONTE (Syngenta)

=head1 VERSION

Version [version.subversion.build] #+++

Date [DD/MM/YYY] #+++

=head1 SEE ALSO

#--- other documentation or objects related to this package
GreenPhyl documentation.

=cut

return 1; # package return
