=pod

=head1 NAME

Greenphyl::Run::Program - Program execution interface

=head1 SYNOPSIS

    package Greenphyl::Run::Blast;
    ....
    use base qw(Greenphyl::Run::Program);
    ...

    return 1;

    package main;
    ...
    use Greenphyl::Run::Blast;
    ...
    sub DisplayResults
    {
        my ($results) = @_;
        ...
    }
    ...
    
    my $blast_program = new Greenphyl::Run::Blast('blastp');
    $blast_program->validateInputFiles('temp/test.fasta');
    $blast_program->validateParameters(
        '-evalue=1e-10',
        '-cluster_queue=greenphyl',
        '-K=5',
        '-d=/bank/greenphyl/CHLRE',
        '-outfmt=6',
    );
    $blast_program->setOutputFiles('temp/output.blast');
    if (0 == $blast_program->run())
    {
        # execution ok
        DisplayResults($blast_program->getResults());
        ...
    }
    else
    {
        print "ERROR: BLAST execution failed!\n";
    }

=head1 REQUIRES

Perl5

=head1 EXPORTS

Nothing

=head1 DESCRIPTION

This is an interface for programs used by GreenPhyl. The interface provides
several methods that must be overridden by child class.

=cut

package Greenphyl::Run::Program;


use strict;
use warnings;

use Carp qw(cluck confess croak);




# Package subs
###############

=pod

=head1 STATIC METHODS

=head2 CONSTRUCTOR

B<Description>: Creates a new instance and sets class member 'program_name' to
the given program name.

B<ArgsCount>: 1

=over 4

=item $program_name: (string) (R)

The name of the program that as been instanciated. This can be usefull in the
case of programs having various ways of being launched. For instance BLAST
can be run using blastp, blastn, blastall, blastx,...

=back

B<Return>: (Greenphyl::Run::Program child class)

a new instance.

B<Caller>: general

B<Example>:

    package Greenphyl::Run::Blast;
    ...
    sub new
    {
        my ($proto, @parameters) = @_;
        my $class = ref($proto) || $proto;

        # instance creation
        my $self = $class->SUPER::new(@parameters);
        bless($self, $class);

        return $self;
    }

=cut

sub new
{
    my ($proto, $program_name) = @_;
    my $class = ref($proto) || $proto;

    # parameters check
    if ((2 != @_) || (!$program_name))
    {
        confess "usage: my \$instance = Greenphyl::Run::Program->new(program_name);";
    }

    # instance creation
    my $self = {'program_name' => $program_name};
    bless($self, $class);

    return $self;
}


=pod

=head1 ACCESSORS

=pod

=head2 getProgramName

B<Description>: returns the program name used when the object has been
instanciated.

B<ArgsCount>: 0

B<Return>: (string)

The program name.

B<Caller>: general

B<Example>:

    print 'Program used: ' . $program->getProgramName() . "\n";

=cut

sub getProgramName
{
    my ($self) = @_;

    # check parameters
    if ((1 != @_) || (not ref($self)))
    {
        confess "usage: my \$value = \$instance->getProgramName();";
    }
    return $self->{'program_name'};
}

=pod

=head2 setProgramName

B<Description>: replaces the program name given when the program object has
been instanciated by a new given program name.

B<ArgsCount>:  1

=over 4

=item $program_name: (string) (R)

The new program name to use.

=back

B<Return>: (string)

The previous program name.

B<Caller>: general

B<Example>:

    my $previous_program_name = $program->setProgramName($new_program_name);
    print "$previous_program_name has been replaced ny $new_program_name.\n";

=cut

sub setProgramName
{
    my ($self, $program_name) = @_;
    # check parameters
    if ((2 != @_) || (not ref($self)))
    {
        confess "usage: my \$value = \$instance->setProgramName(program_name);";
    }
    my $old_program_name = $self->{'program_name'};
    $self->{'program_name'} = $program_name;
    return $old_program_name;
}


=pod

=head1 METHODS

=head2 validateInputFiles

B<Description>: This method is supposed to raise an exception if one of the
given input file is not valid for program processing.

B<ArgsCount>: 1-n

=over 4

=item $self: (Greenphyl::run::Program child class)

current program object.

=item @file_names: (array of strings)

List of file names with path.

=back

B<Return>: nothing

B<Caller>: general

B<Example>:

    $program->validateInputFiles('temp_path/file.ext');

=cut

sub validateInputFiles
{
    confess "ERROR: not implemented!\n";
}


=pod

=head2 validateParameters

B<Description>: This method is supposed to raise an exception if one of the
given parameter is not valid.

B<ArgsCount>: 1-n

=over 4

=item $self: (Greenphyl::run::Program child class)

current program object.

=item @parameters: (array of strings)

List of parameters.

=back

B<Return>: nothing

B<Caller>: general

B<Example>:

    $program->validateParameters('-x', 'some_param', '-blah=2');

=cut

sub validateParameters
{
    confess "ERROR: not implemented!\n";
}


=pod

=head2 setOutputFiles

B<Description>: Tells the program object what should be the name and path of
the output files.

B<ArgsCount>: 1-n

=over 4

=item $self: (Greenphyl::run::Program child class)

current program object.

=item @file_names: (array of strings)

List of output file names. The name order may be important depending on the
program object.

=back

B<Return>: (list of string)

The list of output file path+names.

B<Caller>: general

B<Example>:

    $program->setOutputFiles('temp/output.file');

=cut

sub setOutputFiles
{
    confess "ERROR: not implemented!\n";
}


=pod

=head2 run

B<Description>: Execute the program and perform computation.

B<ArgsCount>: 1

=over 4

=item $self: (Greenphyl::run::Program child class)

current program object.

=back

B<Return>: (integer)

The error code if one or 0.

B<Caller>: general

B<Example>:

    $program->validateInputFiles('temp_path/file.ext');
    $program->validateParameters('-x', 'some_param', '-blah=2');
    $program->setOutputFiles('temp/output.file');
    if (0 == $program->run())
    {
        # execution ok
        ...
    }
    else
    {
        print "ERROR: execution failed!\n";
    }

=cut

sub run
{
    confess "ERROR: not implemented!\n";
}


=pod

=head2 getResults

B<Description>: Returns program execution results in a hash.

B<ArgsCount>: 1

=over 4

=item $self: (Greenphyl::run::Program child class)

current program object.

=back

B<Return>: (hash ref)

Expected output format:
{
    'output_files' => [<list of output file path+names>],
    'sequences'    =>
    {
        <n-times the following structure:>
        '<a query sequence seq_textid>' =>
        {
            '<a matching sequence seq_textid>' =>
            {
                'score' => <matching score>,
                <any other relevant results:>
                ...     => ...,
            },
        },
    },
}

B<Caller>: general

B<Example>:

    $program->setOutputFiles('temp/output.file');

=cut

sub getResults
{
    confess "ERROR: not implemented!\n";
}



=pod

=head1 AUTHORS

Mathieu ROUARD (Bioversity), m.rouard@cgiar.org

Valentin GUIGNON (Bioversity), v.guignon@cgiar.org

Matthieu CONTE (Syngenta)

=head1 VERSION

Version 1.0.0

Date 22/05/2012

=head1 SEE ALSO

GreenPhyl documentation.

=cut

return 1; # package return
