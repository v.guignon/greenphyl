=pod

=head1 NAME

Greenphyl::Run::Blast - Execute BLAST program

=head1 SYNOPSIS

    use Greenphyl::Run::Blast;
    ...
    sub DisplayResults
    {
        my ($results) = @_;
        ...
    }
    ...
    
    my $blast_program = new Greenphyl::Run::Blast('blastp');
    $blast_program->validateInputFiles('temp/test.fasta');
    $blast_program->validateParameters(
        '-evalue=1e-10',
        '-cluster_queue=greenphyl',
        '-K=5',
        '-d=/bank/greenphyl/CHLRE',
        '-outfmt=6',
    );
    $blast_program->setOutputFiles('temp/output.blast');
    if (0 == $blast_program->run())
    {
        # execution ok
        DisplayResults($blast_program->getResults());
        ...
    }
    else
    {
        print "ERROR: BLAST execution failed!\n";
    }

=head1 REQUIRES

Perl5, BLAST program

=head1 EXPORTS

Nothing

=head1 DESCRIPTION

Execute BLAST program using given input FASTA file and parameters.
This module also provides static methods to handle some BLAST tasks.

=cut

package Greenphyl::Run::Blast;


use strict;
use warnings;

use Carp qw(cluck confess croak);

use base qw(Greenphyl::Run::Program);

use Greenphyl;

use Getopt::Long;
use Bio::SeqIO;
use Bio::SearchIO;





# Package constants
####################

=pod

=head1 CONSTANTS

B<$DEBUG>: (boolean)

Controls debug mode. If set to a true value, displays debug messages.

B<$BLAST_PIN_EXT>: (string)

File extension for PIN files (for BLAST bank).

B<$BLAST_PHR_EXT>: (string)

File extension for PHR files (for BLAST bank).

B<$BLAST_PSQ_EXT>: (string)

File extension for PSQ files (for BLAST bank).

B<$BLAST_ALIAS_EXT>: (string)

File extension for alias files (for BLAST bank).

B<$BLAST_FILE_EXT>: (string)

Default file extension for BLAST output files.

B<$FASTA_FILE_EXT>: (string)

Default file extension for FASTA files.

=cut

our $DEBUG = 0;
our $BLAST_PIN_EXT  = '.pin';
our $BLAST_PHR_EXT  = '.phr';
our $BLAST_PSQ_EXT  = '.psq';
our $BLAST_ALIAS_EXT  = '.pal';
our $BLAST_FILE_EXT = '.blast';
our $FASTA_FILE_EXT = '.fasta';




# Package subs
###############

=pod

=head1 STATIC METHODS

=head2 CONSTRUCTOR

B<Description>: Creates a new instance of a BLAST program.

B<ArgsCount>: 1

=over 4

=item $blast_program_name: (string) (R)

Name of the BLAST program to use. Can be one of:
blastn, blastp, blastx and blastall

=item @other_args: (any) (O)

eventually other arguments.

=back

B<Return>: ([Greenphyl::PackageName]) #+++

a new instance.

B<Caller>: [general] #+++ general, specific class name, ...

B<Exception>:

=over 4

[=item exception_type

description, case when it occurs] #+++ see below

=back

#--- Example:

=over 4

=item Range error

thrown when "argument x" is outside the supported range, for instance "-1".

=back

B<Example>:

    [example code] #+++

=cut

sub new
{
    my ($proto, $blast_program_name, @other_args) = @_;
    my $class = ref($proto) || $proto;

    # instance creation
    my $self = $class->SUPER::new($blast_program_name, @other_args);
    bless($self, $class);

    return $self;
}



=pod

=head1 ACCESSORS

=cut

#--- put here setters and getters (copy-paste text below)
#
#=pod
#
#=head2 get[MemberName] #+++
#
#B<Description>: #+++
#
#B<ArgsCount>: #+++
#
#=over 4
#
#=item $arg: #+++
#
#=back
#
#B<Return>: #+++
#
#B<Caller>: #+++
#
#B<Exception>: #+++
#
#B<Example>: #+++
#
#=cut
#
#sub get[MemberName] #+++
#{
#    my ($self) = @_;
#    # check parameters
#    if ((1 != @_) || (not ref($self)))
#    {
#        confess "usage: my \$value = \$instance->get[MemberName]();"; #+++
#    }
#    return $self->{[MEMBER_NAME]}; #+++
#}

#=pod
#
#=head2 setMemberName #+++
#
#B<Description>:  #+++
#
#B<ArgsCount>:  #+++
#
#=over 4
#
#=item $arg: #+++
#
#=back
#
#B<Return>:  #+++
#
#B<Caller>:  #+++
#
#B<Exception>:  #+++
#
#B<Example>:  #+++
#
#=cut
#
#sub set[MemberName] #+++
#{
#    my ($self, $value) = @_;
#    # check parameters
#    if ((2 != @_) || (not ref($self)))
#    {
#        confess "usage: my \$value = \$instance->set[MemberName](\$value);"; #+++
#    }
#    $self->{[MEMBER_NAME]} = [$value]; #+++
#}


=pod

=head1 METHODS

=head2 validateInputFiles

B<Description>: #+++

B<ArgsCount>: #+++

=over 4

=item $arg: #+++

=back

B<Return>: #+++

B<Caller>: #+++

B<Exception>: #+++

B<Example>: #+++

=cut

sub validateInputFiles
{
    my ($self, @file_names) = @_;

    if (1 != @file_names)
    {
        confess "ERROR: No or too many input file name(s) provided!\n";
    }

    foreach my $file_name (@file_names)
    {
        # check if file exists and is readable
        if (!-e $file_name)
        {
            confess "ERROR: Input FASTA file '$file_name' not found!\n";
        }
        elsif (!-r $file_name)
        {
            confess "ERROR: Unable to read input FASTA file '$file_name'! Please check file permissions.\n";
        }

        # check FASTA content
        my $input_fasta_io = Bio::SeqIO->new(
                '-file'   => $file_name,
                '-format' => 'Fasta',
        );
        if (!$input_fasta_io)
        {
            confess "ERROR: invalid input FASTA file!\n";
        }
        # get all sequences seq_textid in FASTA file
        $self->{'sequences'} = {};
        while (my $sequence = $input_fasta_io->next_seq())
        {
            # save sequences ID to keep track of unmatched sequences
            $self->{'sequences'}->{$sequence->id} = 0;
        }
        $self->{'input_filename'} = $file_name;
    }
}


=pod

=head2 validateParameters

B<Description>: #+++

B<ArgsCount>: #+++

=over 4

=item $arg: #+++

=back

B<Return>: #+++

B<Caller>: #+++

B<Exception>: #+++

B<Example>: #+++

=cut

sub validateParameters
{
    my ($self, @parameters) = @_;

    my ($e_value, $cluster_queue, $filter, $max_best_hit, $max_result_display,
        $max_alignments, $blast_bank, $output_format, $output_format_m, $wordsize, $comp_based_stats, $matrix);
    my @ARGV_BACK = @ARGV;
    @ARGV = @parameters;
    GetOptions(
        "evalue|e=s"      => \$e_value,
        "cluster_queue=s" => \$cluster_queue,
        "F=s"             => \$filter,
        "K=s"             => \$max_best_hit,
        "v=s"             => \$max_result_display,
        "b=s"             => \$max_alignments,
        "d=s"             => \$blast_bank,
        "outfmt=s"        => \$output_format,
        "m=s"             => \$output_format_m,
        'wordsize=i'      => \$wordsize,
        'comp_based_stats=s' => \$comp_based_stats,
        'matrix=s'           => \$matrix,
    );
    @ARGV = @ARGV_BACK;

    #+FIXME: check and validate parameters...
    
    # check e-value
    $self->{'e_value'} = $e_value;
    $self->{'cluster_queue'} = $cluster_queue;
    $self->{'F'}      = $filter;
    $self->{'K'}      = $max_best_hit;
    $self->{'v'}      = $max_result_display;
    $self->{'b'}      = $max_alignments;
    $self->{'d'}      = $blast_bank;
    $self->{'m'}      = $output_format_m;
    $self->{'outfmt'} = $output_format;
    $self->{'wordsize'} = $wordsize;
    $self->{'comp_based_stats'} = $comp_based_stats;
    $self->{'matrix'} = $matrix;

    return 0;
}


=pod

=head2 setOutputFiles

B<Description>: #+++

B<ArgsCount>: #+++

=over 4

=item $arg: #+++

=back

B<Return>: #+++

B<Caller>: #+++

B<Exception>: #+++

B<Example>: #+++

=cut

sub setOutputFiles
{
    my ($self, @file_names) = @_;

    if (1 != @file_names)
    {
        confess "ERROR: No or too many output file name(s) provided!\n";
    }

    return $self->{'output_filename'} = $file_names[0];
}


=pod

=head2 run

B<Description>: #+++

B<ArgsCount>: #+++

=over 4

=item $arg: #+++

=back

B<Return>: #+++

B<Caller>: #+++

B<Exception>: #+++

B<Example>: #+++

=cut

sub run
{
    my ($self) = @_;
    my %blast_parameters;

    if (defined($self->{'input_filename'}))
    {
        $blast_parameters{'-i'} = $self->{'input_filename'};
    }

    if (defined($self->{'output_filename'}))
    {
        $blast_parameters{'-o'}      = $self->{'output_filename'};
    }

    if (defined($self->{'d'}))
    {
        $blast_parameters{'-d'}      = $self->{'d'};
    }

    if (defined($self->{'cluster_queue'}))
    {
        $blast_parameters{'cluster_queue'} = $self->{'cluster_queue'};
    }

    if (defined($self->{'e_value'}))
    {
        $blast_parameters{'-e'}      = $self->{'e_value'};
    }

    if (defined($self->{'program_name'}))
    {
        $blast_parameters{'-p'}      = $self->{'program_name'};
    }

    if (defined($self->{'F'}))
    {
        $blast_parameters{'-F'}      = $self->{'F'};
    }

    if (defined($self->{'K'}))
    {
        $blast_parameters{'-K'}      = $self->{'K'};
    }

    if (defined($self->{'v'}))
    {
        $blast_parameters{'-v'}      = $self->{'v'};
    }

    if (defined($self->{'b'}))
    {
        $blast_parameters{'-b'}      = $self->{'b'};
    }

    if (defined($self->{'m'}))
    {
        $blast_parameters{'-m'}      = $self->{'m'};
    }

    if (defined($self->{'outfmt'}))
    {
        $blast_parameters{'-outfmt'} = $self->{'outfmt'};
    }
    
    if (defined($self->{'wordsize'}))
    {
        $blast_parameters{'-wordsize'} = $self->{'wordsize'};
    }
    if (defined($self->{'comp_based_stats'}))
    {
        $blast_parameters{'-comp_based_stats'} = $self->{'comp_based_stats'};
    }
    if (defined($self->{'matrix'}))
    {
        $blast_parameters{'-matrix'} = $self->{'matrix'};
    }
    

    runBlast(%blast_parameters);
    
    return 0;
}


=pod

=head2 getResults

B<Description>: #+++

B<ArgsCount>: #+++

=over 4

=item $arg: #+++

=back

B<Return>: #+++

B<Caller>: #+++

B<Exception>: #+++

B<Example>: #+++

=cut

sub getResults
{
    my ($self) = @_;

    my $results = {
        'output_files' => [$self->{'output_filename'}],
        'sequences'    => $self->parseBLASTOutput($self->{'output_filename'}),
    };

    return $results;
}



=pod

=head1 FUNCTIONS

=head2 runBlast

B<Description>:

B<ArgsCount>: 6

=over 4

=item $input_fasta_filename: (string) (R)

file name of a multifasta of protein sequence.

=item $evalue: (real) (U)

e-value for BLAST.

=item $database: (string) (R)

BLAST bank file to use as database.

=item $max_best_hit: (integer) (U)

Maximum number of best hits BLAST should report.

=item $output_directory: (string) (R)

path where the output BLAST files will be created.

=item $output_filename: (string) (R)

name of the BLAST output file.

=back

=cut

sub runBlast
{
    my (%parameters) = @_;
    my @blast_parameters;

    # special handling of input file:
    my $input_fasta_filename = $parameters{'-i'} or confess "ERROR: input file not specified!\n";
    delete($parameters{'-i'});
    push(@blast_parameters, '-i', $input_fasta_filename);
    
    # special handling of ouput file:
    my $ouput_fasta_filename = $parameters{'-o'} or confess "ERROR: output file not specified!\n";
    delete($parameters{'-o'});
    push(@blast_parameters, '-o', $ouput_fasta_filename);

    if (!exists($parameters{'-d'}))
    {
        confess "ERROR: no BLAST bank specified!\n";
    }

    # special handling for cluster
    my $cluster_queue = $parameters{'cluster_queue'};
    delete($parameters{'cluster_queue'});

    #+FIXME: constants should not be hard coded here.

    # BLAST default parameters
    $parameters{'-e'} ||= 1e-10;
    $parameters{'-p'} ||= 'blastp';
    $parameters{'-F'} ||= 'none';
    
    # handle max_best_hit
    if (!exists($parameters{'-K'}))
    {
        $parameters{'-K'} = $parameters{'max_best_hit'};
        $parameters{'-K'} ||= 5;
    }
    if (!exists($parameters{'-v'}))
    {
        $parameters{'-v'} = $parameters{'max_best_hit'};
        $parameters{'-v'} ||= 5;
    }
    if (!exists($parameters{'-b'}))
    {
        $parameters{'-b'} = $parameters{'max_best_hit'};
        $parameters{'-b'} ||= 5;
    }
    delete($parameters{'max_best_hit'});
    

    my $blast_cmd;
    if ($cluster_queue)
    {
        $blast_cmd = GP('CLUSTER_BLASTALL_COMMAND');
        push(@blast_parameters, '-queue', $cluster_queue);
    }
    else
    {
        $blast_cmd = GP('BLASTALL_COMMAND');
    }
    push(@blast_parameters, GP('BLASTALL_ADDITIONAL_PARAM'));

    push(@blast_parameters, map {($_, $parameters{$_})} keys(%parameters));

    print "DEBUG: COMMAND: $blast_cmd " . join(' ', @blast_parameters) . "\n" if 1; #+debug $DEBUG;
    my $program_sh;
    open($program_sh, "-|")
        or exec({$blast_cmd}
            $blast_cmd, @blast_parameters
        )
            or confess "ERROR: unable to launch BLAST! $?, $!\n";
    print join('', <$program_sh>);
    close($program_sh);            

}


=pod

=head2 FormatDB

B<Description>: format the given multi-FASTA files of protein sequence for
BLAST tool.

B<ArgsCount>: 1-3

=over 4

=item $fasta_files: (array ref/string) (R)

An array of multi-FASTA files to process into a single BLAST bank file.
A single file name can be specified as a string.

=item $output_db_name: (string) (R/U)

The output BLAST bank file name. In case of a single FASTA file, it can be
omitted (but set to undef if $options is used) and the FASTA file name will be
used as output name.

=item $options: (hash ref/string) (O)

A hash of additional formatdb parameters. The following keys are recognized:
-'gi': FASTA sequences have GI identifiers;
-'nucleotide': if set to true, FASTA file contain nucleotide sequences;
-'name': can be used to specify a human-readable explicit bank name;
-'extra': can contain raw command-line parameters appended to the formatdb
 command-line.
If a string is given, it will be treated as an 'extra' command-line parameter.

=back

=head3 SEE ALSO

http://etutorials.org/Misc/blast/Part+V+BLAST+Reference/Chapter+13.+NCBI-BLAST+Reference/13.4+formatdb+Parameters/

=cut

sub FormatDB
{
    my ($fasta_files, $output_db_name, $options) = @_;

    # parameter check
    if (!ref($fasta_files))
    {
        # turn string into array
        $fasta_files = [$fasta_files];
    }
    elsif ('ARRAY' ne ref($fasta_files))
    {
        confess "ERROR: First parameter is not an array of FASTA file names!\n";
    }
    
    if (!@$fasta_files)
    {
        confess "ERROR: No FASTA file given!\n";
    }
    
    # if no output name given and only one fasta, use the same name
    if (!$output_db_name && (1 == @$fasta_files))
    {
        $output_db_name = $fasta_files->[0];
        $output_db_name =~ s/\.\w+$//;
    }
    
    if (!$output_db_name)
    {
        confess "ERROR: no output database name provided!\n";
    }
    
    $options ||= {};
    if (!ref($options))
    {
        $options = {'extra' => $options};
    }
    elsif ('HASH' ne ref($options))
    {
        confess "ERROR: Invalid option parameter!\n";
    }

    # make sure files exist
    foreach my $fasta_file (@$fasta_files)
    {
        if (!-r $fasta_file)
        {
            confess "ERROR: Unable to read the FASTA file ('$fasta_file')!\n";
        }
    }
    
    # remove old files
    #+FIXME: unlink .00, .01,... files
    unlink($output_db_name . $BLAST_PIN_EXT,
           $output_db_name . $BLAST_PHR_EXT,
           $output_db_name . $BLAST_PSQ_EXT);

    # prepare additional parameters
    my $extra_parameters = '';
    if ($options->{'gi'} || $options->{'GI'})
    {
        $extra_parameters .= ' -o T -s T ';
    }

    if ($options->{'nucleotide'})
    {
        $extra_parameters .= ' -p F ';
    }
    
    if ($options->{'name'})
    {
        $extra_parameters .= " -t '$options->{'name'}' ";
    }

    if ($options->{'extra'})
    {
        $extra_parameters .= $options->{'extra'};
    }
    
    
    PrintDebug("Formatting BLAST bank...");
    # check for single file
    if (1 == @$fasta_files)
    {
        my $command = GP('FORMATDB_COMMAND') . " -i '$fasta_files->[0]' -n '$output_db_name' $extra_parameters";
        PrintDebug("COMMAND: $command");
        if (system($command))
        {
            confess "ERROR: formatdb failed for '" . $fasta_files->[0] . "'!\n$!\n";
        }
    }
    else
    {
        # multiple FASTA
        my $command = 'cat ' . join(' ', @$fasta_files) . ' | ' . GP('FORMATDB_COMMAND') . " -i stdin -n '$output_db_name' $extra_parameters";
        PrintDebug("COMMAND: $command");
        if (system($command))
        {
            confess "ERROR: formatdb failed!\n$!\n";
        }
    }
    
    # check if BLAST database files were generated correctly
    unless ((-e $output_db_name . $BLAST_PIN_EXT)
            && (-e $output_db_name . $BLAST_PHR_EXT)
            && (-e $output_db_name . $BLAST_PSQ_EXT)
            || (-e $output_db_name . '.00' . $BLAST_PIN_EXT)
            && (-e $output_db_name . '.00' . $BLAST_PHR_EXT)
            && (-e $output_db_name . '.00' . $BLAST_PSQ_EXT))
    {
        confess "ERROR: formatdb did not produce the expected files ($BLAST_PIN_EXT, $BLAST_PHR_EXT and $BLAST_PSQ_EXT) for '$output_db_name'!\n";
    }

    return $output_db_name;
}


=pod

=head2 concatBlastBank

B<Description>: concatenate a list of species and eventually an input BLAST
bank into one new BLAST bank file. Formatdb is also applyed to the new bank.

#+FIXME: needs refactoring!
http://etutorials.org/Misc/blast/Part+IV+Industrial-Strength+BLAST/Chapter+11.+BLAST+Databases/11.2+BLAST+Databases/


B<ArgsCount>: 2-3 (hash)

=over 4

=item arg: 'output_filename' (string) (R)

name (including path) to the new BLAST bank file to produce. If the file
already exists, it won't be modified and an exception is raised.

=item arg: 'species_list' (array ref) (R)

An array of string. Each element is a species name and is supposed to have
a file with the same name in $Greenphyl::Config::BLAST_BANKS_PATH directory.

=item arg: 'input_filename' (O)

If specified, it must be the path to an existing BLAST bank that will be
used as basis for the new bank.

=back

B<Return>: nothing

B<Example>: 

Greenphyl::Run::Blast::concatBlastBank('output_filename' => 'NEW_BANK',
                                      'species_list'    => [MEDTR, SORBI, BRADI],
                                      'input_filename'  => 'ARATH_ORYZA',);

=cut

sub concatBlastBank
{
    my (%parameters) = @_;

    my $target_filename  = $parameters{'output_filename'};
    my $species_list     = $parameters{'species_list'};
    my $input_blast_bank = $parameters{'input_filename'};

    # arguments check...
    # -species list
    if (ref($species_list) ne 'ARRAY')
    {
        confess "ERROR: concatBlastBank: Invalid species list argument! Species list must be a reference to an array of species name!\n";
    }
    elsif (!@$species_list)
    {
        confess "ERROR: concatBlastBank: Empty species list!\n";
    }
    # -target BLAST bank
    if (-e $target_filename)
    {
        # file already exists!
        confess "ERROR: Output BLAST bank file already exists and will not be overwritten!\n";
    }
    # -input BLAST bank
    if ($input_blast_bank && (!-e $input_blast_bank))
    {
        # input BLAST bank is missing!
        confess "ERROR: Input BLAST bank ('$input_blast_bank') file not found!\n";
    }

    # concate to an existing BLAST bank?
    if ($input_blast_bank && system('cp $input_blast_bank $target_filename'))
    {
        confess "ERROR: Failed to copy input BLAST bank ('$input_blast_bank') to '$target_filename'!\n$!\n";
    }

    print "DEBUG: Creating new BLAST bank '$target_filename'...\n" if $DEBUG;
    open(NEW_BLAST_BANK, ">$target_filename") or confess "ERROR: Failed to open output BLAST file '$target_filename' for BLAST concatenation!\n$!\n";
    # loop on species
    foreach my $species_name (@$species_list)
    {
        my $species_bank = $Greenphyl::Config::BLAST_BANKS_PATH . '/' . $species_name;
        print "DEBUG: -adding $species_name ('$species_bank')\n" if $DEBUG;
        # check if bank file exists for current species
        if (!-e $species_bank)
        {
            # does not exist! Warn user
            warn "WARNING: BLAST bank file ('$species_bank') for species '$species_name' not found!\n";
        }
        my $content = '';
        if ($^O =~ m/Win32/i)
        {
            $content = `type $species_bank`;
        }
        else
        {
            $content = `cat $species_bank`;
        }
        
        if (!$content)
        {
            warn "WARNING: BLAST bank file ('$species_bank') for species '$species_name' appears to be empty!\n";
        }
        print NEW_BLAST_BANK $content;
    }
    close(NEW_BLAST_BANK);
    print "DEBUG: Done!\n" if $DEBUG;

    # run formatdb to finalize BLAST bank
    formatdb($target_filename);

}



=pod

=head2 loadBestHit

B<Description>: R

B<ArgsCount>: 1

=over 4

=item arg: $d (integer) (R)

D.

=back

B<Return>: (string)

A.

B<Example>:

    my

=cut

sub parseBLASTOutput
{
    my ($self, $blast_result_filename) = @_;
    my $blast_results = {};

    # check BLAST output format
    my $output_format = 'blast';
    if (($self->{'outfmt'} && ($self->{'outfmt'} eq '6'))
        || ($self->{'m'} && ($self->{'m'} eq '9')))
    {
        $output_format = 'blasttable';
    }

    # parse BLAST file
    my $blast_io = new Bio::SearchIO(
        '-format' => $output_format,
        '-file' => $blast_result_filename
        ) or confess("ERROR: $!\n");

    # init each sequence match records
    foreach my $query_seq_textid (keys(%{$self->{'sequences'}}))
    {
        $blast_results->{$query_seq_textid} = {};
    }
    # process each query sequence
    while (my $result = $blast_io->next_result())
    {
        # process each match
        while (my $hit = $result->next_hit())
        {
            if (my $hsp = $hit->next_hsp())
            {
                $blast_results->{$result->query_name}->{$hit->name} = {'score' => $hsp->bits, 'e-value' => $hsp->evalue};
                if (!defined($hsp->bits) || !defined($hsp->evalue))
                {
                    confess "ERROR: failed to parse BLAST results (missing a score or an e-value for match between '" . $result->query_name . "' and '" . $hit->name . "')!\n";
                }
            }
            else
            {
                print "WARNING: no hsp found for '" . $result->query_name . "'!\n";
            }
        }
    }
    $blast_io->close();

    return $blast_results;
}


=pod

=head1 DIAGNOSTICS

#--- Give and explain here every error message the the module may generate.
#--- Use: (W)=warning (optional), (D)=deprecation (optional),
#---      (S)=severe warning (mandatory), (F)=fatal error (trappable),
#---      (X)=very fatal error (non-trappable).
#--- example:
#--- =over 4
#--- =item Negative radius
#---
#--- (F) A circle may not be created with a negative radius.
#---
#--- =back #+++

=head1 AUTHORS

Mathieu ROUARD (Bioversity), m.rouard@cgiar.org

Valentin GUIGNON (Bioversity), v.guignon@cgiar.org

Matthieu CONTE (Syngenta)

=head1 VERSION

Version [version.subversion.build] #+++

Date [DD/MM/YYY] #+++

=head1 SEE ALSO

#--- other documentation or objects related to this package
GreenPhyl documentation.

=cut

return 1; # package return
