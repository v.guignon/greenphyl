package Greenphyl::Run::Statistics;

use strict;
use warnings;
use lib '../../lib';
use lib '../../local_lib';

use Greenphyl::Log('nolog' => 1,);

my $DEBUG = 0;

my ( $mysql, $dbh );

sub new
{
    my ( $class, %p ) = @_;
    my $self = {};

    $mysql = $p{'mysql'};
    $dbh   = $p{'dbh'};

    bless( $self, $class );
    return $self;
}


# this function make statistics on IPR and load ipr_stat table
sub iprSpecific
{
    my ($self) = @_;

    # get all family_id from family table
    my $sql_query = "SELECT f.family_id AS family_id, COALESCE(fi.class_id, -1) AS class_id FROM family f LEFT JOIN found_in fi USING(family_id);";
    my @fam = @{$dbh->selectall_arrayref($sql_query, {'Slice' => {}})};

    my $count = 0;

    foreach my $fam_data (@fam)
    {
        my $fam = $fam_data->{'family_id'};
        LogDebug("-1: $fam: " . time());
        ++$count;
        LogInfo("$count / " . scalar(@fam));

        # get total number of genes from all species / family
        my $sql_query = "SELECT sum(number) FROM count_family_seq WHERE family_id = $fam";
        my $genes_count_for_family = 0;
        if (my @total = @{$dbh->selectrow_arrayref($sql_query)})
        {
            $genes_count_for_family = $total[0];
        }
        
        # get composition + count of IPR by family
        $sql_query = "SELECT sa.ipr_id, COUNT(DISTINCT(sa.seq_id)) as count
                          FROM seq_is_in sii
                               JOIN seq_has_an sa ON sa.seq_id = sii.seq_id
                          WHERE sii.family_id = ?
                          GROUP BY sa.ipr_id";
        LogDebug("0: QUERY: $sql_query");
        my $iprs_data = $dbh->selectall_arrayref($sql_query, { Slice => {} }, $fam );

        my $class = $fam_data->{'class_id'};

        if (!$genes_count_for_family)
        {
            warn "WARNING: Invalid number of sequence for family $fam! Please make sure count_family_seq table is up-to-date! (under MySQL: \"CALL countFamilySeqBySpecies();\")\n";
            $genes_count_for_family = 1;
        }

        foreach my $ipr_element (@{$iprs_data})
        {
            # make stat on ipr
            my $percentage = int((100.0 * $ipr_element->{count} / $genes_count_for_family ) + 0.5); # int round

            $sql_query = "SELECT COUNT(DISTINCT sii.family_id)
                              FROM seq_has_an sha
                                JOIN seq_is_in sii ON sii.seq_id = sha.seq_id
                                JOIN found_in fi ON fi.family_id = sii.family_id
                              WHERE sha.ipr_id = $ipr_element->{ipr_id}
                                    AND fi.class_id = $class";
            
            LogDebug("2: QUERY: $sql_query");
            my ($spec_fam) = ($dbh->selectrow_array($sql_query));

            my $spec = 0;
            $spec = 1 if ($spec_fam == 1);

            $sql_query = "INSERT INTO ipr_stat VALUES ($fam, $ipr_element->{ipr_id}, $ipr_element->{count}, $percentage, $spec) ON DUPLICATE KEY UPDATE perc = $percentage, spec = $spec";
            $dbh->do($sql_query);
            LogInfo("$sql_query");            
        }
    }
    $dbh->do("TRUNCATE go_family_cache;");
    return;
}



1;
