package Greenphyl::Run::InterproWS;

use strict;
use warnings;

use Carp qw (cluck confess croak);
use Bio::Seq;
use Bio::SeqIO;
use lib '../../../lib';
use lib '../../../local_lib';

use lib 'Modules';
use File::Basename;
use Data::Dumper;
use XML::Simple qw(:strict);
use Greenphyl::PathManager;
use File::Basename;
use Greenphyl::SequenceFilter 'filter_text_id';
use Greenphyl::Utils 'count_sequences_in_fasta';


my $nb_lauch = 25;    # number of simultaneous queries

sub new
{
    my ( $class, $email ) = @_;
    my $self = { "email" => $email };
    bless( $self, $class );
    return $self;
}

sub Interproscan
{
    my ( $self, $file, $res, $temp_file, $species_name, $restart ) = @_;
    my $file_interpro;
    my @jobid;    # contient les identifiant de chaque requete faites a interproscan tournant sur les serveurs EBI.
    my $cpt_process = 0;
    my $total       = 0;
    my $cpt_done    = 0;
    my $cpt_death   = 0;
    my $cpt_jobid   = 0;
    my $nb_turn     = 0;
    my $nb_sequence = count_sequences_in_fasta($file);

    # plus besoin de gestion d'erreur faites au niveau du formulaire

    my $input_fasta_io = Bio::SeqIO->new(
        -file   => $file,
        -format => 'fasta'
    );

    my $cpt_queries = 0;

    while (my $seq = $input_fasta_io->next_seq())    # conversion d'un fichier multifasta en un fichier fasta (car interproscan ne gere par le multifasta)
    {
        $cpt_queries++;

        my $id = $seq->display_id;
        $id                             = filter_text_id($id);
        $seq->{primary_id}              = $id;
        $seq->{primary_seq}{primary_id} = $id;
        $seq->{primary_seq}{display_id} = $id;

        if ( $cpt_queries > $restart) # redemarrer a ce niveau la recuperation des xml (si arret du script en cours) $restart = valeur pour repartir
        {

            my $output = Bio::SeqIO->new(
                -file   => "> $temp_file",
                -format => 'fasta'
            );
            $output->write_seq($seq);

            print "Requete JOB : " . $cpt_process . " / " . $nb_sequence . " => $id || turn : " . $nb_turn . "\n";
            if ( $cpt_process == $nb_lauch )
            {
                ( $total, $cpt_done, $cpt_death, $cpt_jobid ) =
                    recupJob( $res, $total, $cpt_done, $cpt_death, $cpt_jobid, @jobid );
                my @job;
                @jobid       = @job;
                $cpt_process = 0;
                $nb_turn++;
            }
            else
            {

                # lance interproscan.pl, sur le fichier fasta passe en parametre
                print "load : sequence number $cpt_queries\n";
                my $out = $res . $id;
                my $cmd =
                    "perl $Greenphyl::Config::ADMIN_SCRIPTS_PATH/interproscan.pl --outformat null --outfile $out --async --email $self->{email} --goterms --seqtype p --outfile $id $temp_file";
                $cmd .= " 2>/dev/null" if $^O ne 'MSWin32';
                print $cmd. "\n";
                my $job_id = `$cmd`;
                push( @jobid, [ $job_id, $id ] );    # recupere les identifiants des requetes
                print "Jobid => " . $job_id . "\n";
                $cpt_process++;

            }
        }
    }
    if ( scalar( @jobid != 0 ) )                     # reste des xml non recupere
    {
        print "reste des sequences a recuperer : " . scalar(@jobid) . "\n";
        ( $total, $cpt_done, $cpt_death, $cpt_jobid ) =
            recupJob( $res, $total, $cpt_done, $cpt_death, $cpt_jobid, @jobid );

    }

    print "Please wait, program loading\n";
    print "	Here is the output path : $res\n";

    # bouger le fichier historique a ce niveau
    return $total, $cpt_done, $cpt_death, $cpt_jobid, $cpt_queries;    # @jobid_error;
}

# on lance interproscan en async et on doit r�cup�rer ce qui a ete produit et qui est stock� sur le serveur.
# @jobid contient les identifiants des jobs que on lance sur les serveurs de l'EBI.
# on teste le status du job et si le job est termine, on recupere son travail, s'il est en cours, on attends qu'il soit fini.
sub recupJob
{
    my ( $dir, $total, $cpt_done, $cpt_death, $cpt_jobid, @jobid ) = @_;

    my $nb_recup    = 0;
    my $cpt_running = 0;

    print "voici la taille de la liste " . scalar(@jobid) . "\n";

    for my $i ( 0 .. $#jobid )
    {
        my $jobid = $jobid[$i][0];
        my $id    = $jobid[$i][1];
        $nb_recup++;
        print "RECUP JOB : " . $nb_recup . " / " . scalar(@jobid) . " => " . $id . "\n";
        my $out = $dir . $id;
        chomp($out);
        chomp($jobid);
        print " Loading .... JOBID : $jobid\n";
        my $cmd = "perl $Greenphyl::Config::ADMIN_SCRIPTS_PATH/interproscan.pl --status --jobid $jobid";
        $cmd .= " 2>/dev/null" if $^O ne 'MSWin32';
        print $cmd. "\n";
        my $resultat = `$cmd`;

        print "RESULTS : $resultat \n";
        chomp($resultat);
        $cpt_jobid++;

        while ( $resultat eq "RUNNING" and $cpt_running < 40 )
        {
            print "the job is currently being processed\n";
            $cpt_running++;
            if ( $cpt_running == 1 )    # premiere fois qu'on a running
            {
                sleep(40);
            }
            elsif ( $cpt_running == 2 )
            {
                sleep(20);
            }
            elsif ( $cpt_running == 3 )
            {
                sleep(10);
            }
            else                        # 4 fois ou plus
            {
                sleep(5);
            }
            print "voici mon compte runninng $cpt_running \n";
            print $cmd. "\n";
            $resultat = `$cmd`;
            chomp($resultat);
        }

        $cpt_running = 0;               # reinitialisation a zero

        if ( $resultat eq "DONE" )
        {
            print "job has finished, and the results can then be retrieved.\n";

            print "voici mon fichier de sortie : $out \n";
            my $cmd =
                "perl $Greenphyl::Config::ADMIN_SCRIPTS_PATH/interproscan.pl  --polljob --jobid $jobid --outfile $out --outformat xml";
            print $cmd. "\n";
            system($cmd ) and croak "Cannot get interproscan result : $! ($cmd)";

            $cpt_done++;
        }
        elsif ( $resultat eq "PENDING" )
        {
            print "the job is in a queue waiting processing\n";

            $cpt_death++;
        }
        elsif ( $resultat eq "NOT_FOUND" )
        {
            print "the job id is no longer available (job results are deleted after 24 h)\n";

            $cpt_death++;
        }
        else
        {
            print "the job failed or no results where found\n";
            $cpt_death++;
        }
    }

    # close IN;
    my $res_total = $cpt_done + $cpt_death;

    # print "Nombre de fichier non traite, par interproscan (pas de xml genere)  ".scalar(@jobid_error)." \n";
    return ( $res_total, $cpt_done, $cpt_death, $cpt_jobid );    # ,@jobid_error;
}



# a new multi-fasta file will be generated from job ID in error state
# $file: species multi-fasta file
#

sub errorJobid
{
    my ( $self, $file, $species_name, @jobid_error ) = @_;
    my $path_manager = new Greenphyl::PathManager($species_name);

    my $error_interpro = Bio::SeqIO->new(
        -file   => '>' . $path_manager->getOutputDirectory()  . '/' . $Greenphyl::Config::INTERPROSCAN_ERROR_FILE,
        -format => 'fasta'
    );
    print "voici mon fichier $file\n";
    if ( -e $file )
    {
        print "ce fichier existe\n";
    }
    for my $i ( 0 .. $#jobid_error )
    {
        my $id = $jobid_error[$i];    # rescupere l'id de la sequence qui a rate
        print " voici l'id de la sequence a traiter $id\n";
        my $input_fasta_io = Bio::SeqIO->newFh( -file => $file );    # dans le for pour reprendre le fichier du debut a chaque fois

        while (<$input_fasta_io>)
        {
            my $text_id = $_->display_id;

            if ( $text_id eq $id )
            {
                $error_interpro->write_seq($_);

            }
        }
    }
}

# parse tous les fichiers xml generes par interproscan.pl
sub parse_xml
{
    my ( $self, $species_name, $file ) = @_;
    my $ref = XMLin(
        $file,
        forcearray   => 1,
        keeproot     => 1,
        forcecontent => 1,
        keyattr      => {}
    );
    my $prot_ref = $ref->{EBIInterProScanResults}->[0]->{interpro_matches}->[0]->{protein};
    my $count;

    for my $protein_ref (@$prot_ref)
    {
        my $id = $protein_ref->{id};
        &process_interpro( $protein_ref->{interpro}, $id, $species_name );
    }
}

sub content
{
    my ($arrayref) = @_;
    return ( $arrayref->[0]->{content} );
}

# on recupere le contenu du xml dans un fichier texte dont l'accesseur est PARSE.
sub process_interpro
{
    my ( $protein_ref, $id, $species_name ) = @_;
    my $path_manager = new Greenphyl::PathManager($species_name);
    my $parse_file_path = $path_manager->getOutputDirectory() . '/' . $Greenphyl::Config::XML_PARSING_FILE;
    open(PARSE, '>>' . $parse_file_path) or confess("ERROR: Could not create file '$parse_file_path'!\n");
    print PARSE "Sequence $id\n";

    for my $interpro (@$protein_ref)
    {
        my $id        = $interpro->{id};
        my $name      = $interpro->{name};
        my $type      = $interpro->{type};
        my $match_ref = $interpro->{match};

        next if $id eq "noIPR";
        print PARSE "Interpro\t$id\t" . $name . "\t$type\t ";

        for my $match (@$match_ref)    # on parcourt les noeuds du xml
        {
            my $db_id    = $match->{id};
            my $dbname   = $match->{dbname};
            my $location = $match->{location};
            my $start    = $location->[0]->{start};
            my $end      = $location->[0]->{end};
            my $score    = $location->[0]->{score};
            my $evidence = $location->[0]->{evidence};
            print PARSE "$score\t$start\t$end\n";
        }

        my $classification_ref = $interpro->{classification};
        for my $classification (@$classification_ref)
        {
            my $category          = $classification->{category}->[0]->{content};
            my $class_type        = $classification->{class_type};
            my $classification_id = $classification->{id};
            my $description       = $classification->{description}->[0]->{content};

            if ( $category =~ /^Molecular Function$/ )
            {
                print PARSE "$category\t$classification_id\t$description\n";
            }
        }
    }
    close PARSE;
}

sub find_error
{
    my ( $self, $file, $species_name ) = @_;
    my @list;
    my @list_error;
    my $path_manager = new Greenphyl::PathManager($species_name);
    my $directory = $path_manager->getOutputDirectory();
    my $compte    = 0;

    my $error_interpro = Bio::SeqIO->new(
        -file   => '>' . $directory . '/' . $Greenphyl::Config::INTERPROSCAN_ERROR_FILE,
        -format => 'fasta'
    );

    my $input_fasta_io = Bio::SeqIO->newFh(
        -file   => $file,
        -format => 'Fasta'
    );

    # list XML files
    opendir(DIR, $directory) or confess "ERROR: failed to list directory '$directory'!\n";
    while (my $listed_filename = readdir(DIR))
    {
        if ($listed_filename =~ /\.xml$/) # filter XML file to exclude other files like text files
        {
            push(@list, $listed_filename); # add file to yhe list # voir pour enlever extention (basename)
        }
    }
    closedir(DIR);

    while (<$input_fasta_io>)
    {
        my $text_id = $_->display_id;
        print "SEARCH FOR $text_id \n";

        my $result = &error( $text_id, @list );
        print "here is my result: $result \n";
        if ( $result == 0 )    # bug ici rentre dans la boucle meme si = 1
        {
            $compte++;
            print "sequence not found number: $compte \n";
            print "file not generated for: $text_id \n";
            push(@list_error, $text_id);
            $error_interpro->write_seq($_);
        }
        # else = 1 donc xml genere
    }
    return @list_error;
}

sub error
{
    my ( $text_id, @liste ) = @_;

    for my $i ( 0 .. $#liste )
    {

        my $name_file =
            fileparse( $liste[$i], qr/\.[^.]*/ );    # on recupere le nom fichier ss extension : id de la sequence
        chomp($name_file);
        chomp($text_id);

        if ( $name_file eq $text_id )
        {
            print "$name_file with $text_id \n";
            print "XML \n";
            return 1;                                # on a trouve !!!!
                                                     # si eq = le fichier xml pour cette sequence a bien ete genere

        }
    }
    return 0;                                        # pas trouve car on est arrive a la fin de la liste

}

1;
