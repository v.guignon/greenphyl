=pod

=head1 NAME

Greenphyl::FamilyAnnotation - GreenPhyl Family Annotation Object

=head1 SYNOPSIS

    use Greenphyl::FamilyAnnotation;
    my $annotation = Greenphyl::FamilyAnnotation->new($dbh, {'selectors' => {'accession' => 'GP000042'}});
    print $annotation->status();

=head1 REQUIRES

Perl5

=head1 EXPORTS

Nothing

=head1 DESCRIPTION

This module implements a GreenPhyl FamilyAnnotation database object.

=cut

package Greenphyl::FamilyAnnotation;

use strict;
use warnings;

use base qw(Greenphyl::CachedDBObject Greenphyl::DumpableObject Greenphyl::DumpableTabularData);

use vars qw($AUTOLOAD);

use Carp qw(cluck confess croak);

use Greenphyl;

use Greenphyl::SourceDatabase;
use Greenphyl::DBXRef;
use Greenphyl::HashInsensitive;
use Greenphyl::Tools::DBXRefs;




# Package constants
####################

=pod

=head1 CONSTANTS

B<$OBJECT_PROPERTIES>: (hash ref)

object properties description. See DBObject constructor documentation for
details.

=cut

our $OBJECT_PROPERTIES = {
    'table' => 'family_annotations',
    'key' => 'id',
    'alternate_keys' => [],
    'load'     => [
        'accession',
        'confidence_level',
        'date',
        'dbxref',
        'description',
        'id',
        'inferences',
        'name',
        'status',
        'synonyms',
        'type',
        'user_id',
        'user_name',
    ],
    'alias' => {
    },
    'base' => {
        'id' => {
            'property_column' => 'id',
        },
        'user_id' => {
            'property_column' => 'user_id',
        },
        'user_name' => {
            'property_column' => 'user_name',
        },
        'accession' => {
            'property_column' => 'accession',
        },
        'description' => {
            'property_column' => 'description',
        },
        'name' => {
            'property_column' => 'name',
        },
        'type' => {
            'property_column' => 'type',
        },
        'synonyms' => {
            'property_column' => 'synonyms',
        },
        'date' => {
            'property_column' => 'date',
        },
        'status' => {
            'property_column' => 'status',
        },
        'inferences' => {
            'property_column' => 'inferences',
        },
        'confidence_level' => {
            'property_column' => 'confidence_level',
        },
        'confidence_level_value' => {
            'property_column' => '0+confidence_level',
        },
        'dbxref' => {
            'property_column' => 'dbxref',
        },
        'comments' => {
            'property_column' => 'comments',
        },
    },
    'secondary' => {
        'user' => {
            'object_key'      => 'user_id',
            'property_table'  => 'users',
            'property_key'    => 'id',
            'multiple_values' => 0,
            'property_module' => 'Greenphyl::User',
        },
        'family' => {
            'object_key'     => 'accession',
            'property_table'  => 'family',
            'property_key'    => 'accession',
            'multiple_values' => 0,
            'property_module' => 'Greenphyl::Family',
        },
    },
    'tertiary' => {
    },

};




# Package subs
###############

=pod

=head1 STATIC METHODS

=head2 CONSTRUCTOR

B<Description>: Creates a new instance of a GreenPhyl FamilyAnnotation object.

B<ArgsCount>: 2

=over 4

=item $dbh: (DBI::db) (U)

Database handler of the database to load the object from.

=item $parameters: (hash ref) (R)

See Greenphyl::DBObject constructor documentation for details.

=back

B<Return>: (Greenphyl::FamilyAnnotation)

a new instance.

B<Caller>: General

B<Example>:

    my $annotation = Greenphyl::FamilyAnnotation->new($dbh, {'selectors' => {'accession' => 'GP000042'}});

=cut

sub new
{
    my $proto = shift();
    my $class = ref($proto) || $proto;
    
    my $object_properties = $class->GetDefaultClassProperties();

    return $class->SUPER::new($object_properties, @_);
}




=pod

=head2 transferAnnotation

B<Description>: transfer annotation to the related family.

B<ArgsCount>: 1

=over 4

=item $self: (Greenphyl::FamilyAnnotation) (R)

Current family annotation.

=back

B<Return>: (boolean)

True if the annotation has been successfully transfered. Returned value is the
count of annotated fields that have been transfered or -1 if all fields remain
unchanged.

B<Example>:

    if ($annotation->transferAnnotation())
    {
        # annotation transfer OK
        ...
    }
    else
    {
        # annotation has not been transfered
    }

=cut

sub transferAnnotation
{
    my ($self) = @_;

    my $family = $self->family
        or confess('No corresponding family found!');
    my $updated_fields = 0;
    my $sql_query;

    # Cluster type
    if ($self->type && (!$family->type || ($self->type ne $family->type)))
    {
        $family->type($self->type);
        ++$updated_fields;
    }

    # Family name
    if ($self->name && (!$family->name || ($self->name ne $family->name)))
    {
        $family->name($self->name);
        ++$updated_fields;
    }
    # Synonym(s)
    # store current family synonyms in an case-insensitive hash
    tie my %family_synonyms, 'Greenphyl::HashInsensitive';
    map { $family_synonyms{$_} = $_ } @{$family->synonyms};
    # hash values are synonym values or unef when the synonym has already been
    # processed

    foreach my $synonym (split(/\s*[\n\r]+\s*/, $self->synonyms))
    {
        # check if already present (case insensitive)
        if (exists($family_synonyms{$synonym}))
        {
            # case change?
            if (defined($family_synonyms{$synonym})
                && ($synonym ne $family_synonyms{$synonym}))
            {
                # case has been changed
                $sql_query = "UPDATE family_synonyms SET synonym = ? WHERE family_id = ? AND synonym = ?;";
                $self->{'_dbh'}->do($sql_query, undef, $synonym, $family->id, $family_synonyms{$synonym})
                    or confess $self->{'_dbh'}->errstr;
                # set as processed
                $family_synonyms{$synonym} = undef;
                ++$updated_fields;
            }
        }
        else
        {
            # not present, add it
            $sql_query = "INSERT INTO family_synonyms (family_id, synonym) VALUES (?, ?);";
            $self->{'_dbh'}->do($sql_query, undef, $family->id, $synonym)
                or confess $self->{'_dbh'}->errstr;
            # set as processed (in case the user inserted twice the same line)
            $family_synonyms{$synonym} = undef;
            ++$updated_fields;
        }
    }
    # here, already processed synonyms (either updated or added) have undef values
    # remove unwanted synonyms
    foreach my $synonym (keys(%family_synonyms))
    {
        # remove only unprocessed
        if (defined($family_synonyms{$synonym}))
        {
            $sql_query = "DELETE FROM family_synonyms WHERE family_id = ? AND synonym = ?;";
            $self->{'_dbh'}->do($sql_query, undef, $family->id, $synonym)
                or confess $self->{'_dbh'}->errstr;
            ++$updated_fields;
        }
    }

    # Evidence
    if ($self->inferences && (!$family->inferences || ($self->inferences ne $family->inferences)))
    {
        $family->inferences($self->inferences);
        ++$updated_fields;
    }

    # Description
    if ($self->description && (!$family->description || ($self->description ne $family->description)))
    {
        $family->description($self->description);
        ++$updated_fields;
    }

    # DBXRef
    my %family_pmid = map {$_->accession => $_} (@{$family->getPubMed()});
    # load PubMed DB object
    my $pubmed_db = Greenphyl::SourceDatabase->new($self->{'_dbh'}, {'selectors' => {'name' => 'PubMed'}})
        or Throw('error' => 'PubMed source database reference not found in database!');

    foreach my $pmid (split(/\D+/, $self->dbxref))
    {
        next if !$pmid;
        # cross-ref already in database?
        if (exists($family_pmid{$pmid}))
        {
            # set as processed
            $family_pmid{$pmid} = undef;
        }
        else
        {
            # no, add cross-ref
            # first get dbxref associated object
            my $dbxref = Greenphyl::DBXRef->new($self->{'_dbh'}, {'selectors' => {'accession' => $pmid, 'db_id' => $pubmed_db->id,}});
            # insert missing DBXRef
            $dbxref ||= AddPubMedDBXRef({'accession' => $pmid,});
            if (!$dbxref)
            {
                confess "ERROR: Failed to add PubMed DBXRef (PMID:$pmid)!\n";
            }
            # and insert cross-ref
            $sql_query = "INSERT INTO dbxref_families (family_id, dbxref_id) VALUES (?, ?);";
            $self->{'_dbh'}->do($sql_query, undef, $family->id, $dbxref->id)
                or confess $self->{'_dbh'}->errstr;
            ++$updated_fields;
        }
    }
    # remove unwanted DBXRef
    foreach my $dbxref (values(%family_pmid))
    {
        if (defined($dbxref))
        {
            $sql_query = "DELETE FROM dbxref_families WHERE family_id = ? AND dbxref_id = ?;";
            $self->{'_dbh'}->do($sql_query, undef, $family->id, $dbxref->id)
                or confess $self->{'_dbh'}->errstr;
            ++$updated_fields;
        }
    }

    # Confidence
    if ($self->confidence_level_value && ($self->confidence_level_value != $family->validated_value))
    {
        $family->validated($self->confidence_level_value);
        ++$updated_fields;
    }

    # save annotation in DB
    $self->family->save() or return 0; # saving failed
    return $updated_fields || -1;
}


=pod

=head1 AUTHORS

Mathieu ROUARD (Bioversity), m.rouard@cgiar.org

Valentin GUIGNON (Bioversity), v.guignon@cgiar.org

Matthieu CONTE (Syngenta)

=head1 VERSION

Version 1.0.0

Date 17/04/2012

=head1 SEE ALSO

GreenPhyl documentation, DBObject documentation.

=cut

return 1; # package return
