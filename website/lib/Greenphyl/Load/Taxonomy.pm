=pod

=head1 NAME

Greenphyl::Load::Taxonomy - Taxonomy loading library

=head1 SYNOPSIS

    use Greenphyl::Load::Taxonomy;

    my $taxonomy_id = GetNCBITaxonomyID('arabidopsis thaliana');
    StoreTaxonomyForSpecies($taxonomy_id, 'ARATH');

=head1 REQUIRES

Perl5

=head1 EXPORTS

    StoreTaxonomyData GetNCBITaxonomyID GetTaxonomyData StoreTaxonomyForSpecies

=head1 DESCRIPTION

This library contains functions used to load taxonomy data.

=cut

package Greenphyl::Load::Taxonomy;


use strict;
use warnings;

use Carp qw(cluck confess croak);

use DBIx::Simple;
use LWP::UserAgent;
use XML::Simple qw(:strict);

use base qw(Exporter);

our @EXPORT = qw(
    StoreTaxonomyData GetNCBITaxonomyID GetTaxonomyData StoreTaxonomyForSpecies
);

use Greenphyl;



# Package constants
####################

=pod

=head1 CONSTANTS

B<$DEBUG>: (boolean)
When set to true, it enables debug mode.

=cut

our $DEBUG = 0;




# Package subs
###############

=pod

=head1 FUNCTIONS

=head2 StoreTaxonomyData

B<Description>: Parses taxonomy structure to build taxonomy lineage and store it
into database.
Updated tables:
-taxonomy
-lineage
-taxonomy_synonyms

B<ArgsCount>: 1

=over 4

=item $taxonomy_info: (hash ref) (R)

The taxonomy info structure.

=back

B<Return>: (hash ref)

The lineage data for each species.

=cut

sub StoreTaxonomyData
{
    my ($taxonomy_info) = @_;

    # species to lineage hash: keys are species ID, values are list of lineage entries (taxonomy_id)
    my %species_to_lineage = ();
    my $dbh = GetDatabaseHandler();

    # loop on Taxon of TaxaSet (species)
    foreach my $taxon_data (@{$taxonomy_info->{'Taxon'}})
    {
        # get TaxId
        # get ScientificName for logs
        PrintDebug("Working on " . $taxon_data->{'ScientificName'} . " (" . $taxon_data->{'TaxId'} . ")");
        # make sure species is inserted in taxonomy. We'll fill details later
        my $sql_query = "INSERT IGNORE INTO taxonomy (id, scientific_name, rank, level) VALUES (?, ?, ?, ?);";
        if (!$dbh->do($sql_query, undef, $taxon_data->{'TaxId'}, $taxon_data->{'ScientificName'}, $taxon_data->{'Rank'}, 0))
        {
            confess "ERROR: Failed to insert taxonomy TaxId " . $taxon_data->{'TaxId'} . "(" . $taxon_data->{'ScientificName'} . ")";
        }

        # loop on Taxon of LineageEx
        my $parent_taxon = undef;
        my $lineage_level = 0;
        # init lineage list for current species to an empty array
        $species_to_lineage{$taxon_data->{'TaxId'}} = [];
        foreach my $lineage_taxon (@{$taxon_data->{'LineageEx'}->{'Taxon'}})
        {
            # get ScientificName
            PrintDebug("    -" . $lineage_taxon->{'ScientificName'} . " (" . $lineage_taxon->{'TaxId'} . ")");
            $sql_query = "
                INSERT INTO
                    taxonomy (
                        id,
                        scientific_name,
                        rank,
                        parent_taxonomy_id,
                        level
                    )
                VALUES (?, ?, ?, ?, ?)
                ON DUPLICATE KEY
                    UPDATE
                        scientific_name = VALUES(scientific_name),
                        rank = VALUES(rank),
                        parent_taxonomy_id = VALUES(parent_taxonomy_id),
                        level = VALUES(level)
                ;";
            if (!$dbh->do($sql_query, undef, $lineage_taxon->{'TaxId'}, $lineage_taxon->{'ScientificName'}, $lineage_taxon->{'Rank'}, $parent_taxon, $lineage_level))
            {
                PrintDebug("SQL QUERY: $sql_query\nBindings: " . ($lineage_taxon->{'TaxId'} || '') . ", " . ($lineage_taxon->{'ScientificName'} || '') . ", " . ($lineage_taxon->{'Rank'} || '') . ", " . ($parent_taxon || '') . ", " . ($lineage_level || ''));
                confess "ERROR: Failed to insert taxonomy lineage TaxId " . $lineage_taxon->{'TaxId'} . "(" . $lineage_taxon->{'ScientificName'} . ")";
            }

            # save lineage data for species into database
            $sql_query = "INSERT IGNORE INTO lineage (taxonomy_id, lineage_taxonomy_id) VALUES (?, ?);";
            if (!$dbh->do($sql_query, undef, $taxon_data->{'TaxId'}, $lineage_taxon->{'TaxId'}))
            {
                confess "ERROR: Failed to insert linage between " . $taxon_data->{'TaxId'} . " and " . $lineage_taxon->{'TaxId'} . ".";
            }

            # save lineage data for current taxon into database
            foreach my $lineage_of_current_taxon (@{$species_to_lineage{$taxon_data->{'TaxId'}}})
            {
                $sql_query = "INSERT IGNORE INTO lineage (taxonomy_id, lineage_taxonomy_id) VALUES (?, ?);";
                if (!$dbh->do($sql_query, undef, $lineage_taxon->{'TaxId'}, $lineage_of_current_taxon))
                {
                    confess "ERROR: Failed to insert linage between " . $lineage_taxon->{'TaxId'} . " and " . $lineage_of_current_taxon . ".";
                }
            }

            # add taxon to lineage of current species
            push(@{$species_to_lineage{$taxon_data->{'TaxId'}}}, $lineage_taxon->{'TaxId'});

            # save parent ID for next loop
            $parent_taxon = $lineage_taxon->{'TaxId'};
            ++$lineage_level;
        }
        push(@{$species_to_lineage{$taxon_data->{'TaxId'}}}, $taxon_data->{'TaxId'});
        
        # fixes species division: NCBI division field is obsolete and not reliable
        if (($taxon_data->{'Lineage'} =~ m/Viridiplantae/) && ($taxon_data->{'Division'} !~ m/Plants/))
        {
            $taxon_data->{'Division'} = 'Plants';
        }
        
        $sql_query = "INSERT INTO taxonomy (id, scientific_name, rank, division, parent_taxonomy_id, level) VALUES (?, ?, ?, ?, ?, ?) ON DUPLICATE KEY UPDATE scientific_name = VALUES(scientific_name), rank = VALUES(rank), division = VALUES(division), parent_taxonomy_id = VALUES(parent_taxonomy_id), level = VALUES(level);";
        if (!$dbh->do($sql_query, undef, $taxon_data->{'TaxId'}, $taxon_data->{'ScientificName'}, $taxon_data->{'Rank'}, $taxon_data->{'Division'}, $taxon_data->{'ParentTaxId'}, $lineage_level))
        {
            confess "ERROR: Failed to insert taxonomy (2) TaxId " . $taxon_data->{'TaxId'} . "(" . $taxon_data->{'ScientificName'} . ")";
        }

        # add species name
        $sql_query = "INSERT IGNORE INTO taxonomy_synonyms (taxonomy_id, name, synonym_order) VALUES (?, ?, 1) ON DUPLICATE KEY UPDATE taxonomy_id = VALUES(taxonomy_id), name = VALUES(name), synonym_order = 1;";
        if (!$dbh->do($sql_query, undef, $taxon_data->{'TaxId'}, $taxon_data->{'ScientificName'}))
        {
            confess "ERROR: Failed to insert taxonomy synonym for TaxId " . $taxon_data->{'TaxId'} . "(" . $taxon_data->{'ScientificName'} . ")";
        }
        # add synonyms (OtherNames)
        if ($taxon_data->{'OtherNames'}->{'GenbankCommonName'})
        {
            $sql_query = "INSERT IGNORE INTO taxonomy_synonyms (taxonomy_id, name, synonym_order) VALUES (?, ?, 2);";
            if (!$dbh->do($sql_query, undef, $taxon_data->{'TaxId'}, $taxon_data->{'OtherNames'}->{'GenbankCommonName'}))
            {
                confess "ERROR: Failed to insert taxonomy synonym (2) for TaxId " . $taxon_data->{'TaxId'} . "(" . $taxon_data->{'ScientificName'} . ")";
            }
        }
        my $synonym_order = 3; # we reserve order 0 to custom name, 1 to species name and 2 to genbank common name
        if ($taxon_data->{'OtherNames'}->{'Synonym'})
        {
            foreach my $synonym (@{$taxon_data->{'OtherNames'}->{'Synonym'}})
            {
                ++$synonym_order;
                # make sure order fits MySQL unisgned tinyint limits
                if (255 < $synonym_order)
                {$synonym_order = 255;}
                $sql_query = "INSERT IGNORE INTO taxonomy_synonyms (taxonomy_id, name, synonym_order) VALUES (?, ?, $synonym_order);";
               # $sql_query = "INSERT IGNORE INTO taxonomy_synonyms (taxonomy_id, name, synonym_order) VALUES ('" . $taxon_data->{'TaxId'} . "', '" . $synonym . "', $synonym_order);";
                if (!$dbh->do($sql_query, undef, $taxon_data->{'TaxId'}, $synonym))
                {
                    confess "ERROR: Failed to insert taxonomy synonym (3) for TaxId " . $taxon_data->{'TaxId'} . "(" . $taxon_data->{'ScientificName'} . ")";
                }
            }
        }
    }
    
    return \%species_to_lineage;
}


=pod

=head2 GetNCBITaxonomyID

B<Description>: Return NBCI taxonomy ID for given organism/species/etc.

B<ArgsCount>: 1

=over 4

=item $organism: (string) (R)

an organism name. eg: 'arabidopsis thaliana'

=back

B<Return>: (integer)

The NCBI taxonomy ID from the submitted organism.

=cut

sub GetNCBITaxonomyID
{
    my ($organism) = @_;
    my $taxonomy_id;

    my $taxonomy_url = GetURL(
        'ncbi_get_taxonomy_id',
        {
            'report' => 'taxid',
            'format' => 'text',
            'term'   => lc($organism),
        }
    );
    my $ua = LWP::UserAgent->new();
    $ua->timeout(120); # seconds
    $ua->env_proxy();
    my $response = $ua->get($taxonomy_url);

    if ($response->is_success())
    {
        ($taxonomy_id) = ($response->content =~ m/<pre>[\n\r\s]*(\d+)[\n\r\s]*<\/pre>/g);
    }
    else
    {
        my $error_msg = "webservice response was unsuccessful using URL '$taxonomy_url'\n"
            . $response->status_line . "\n"
            . $response->as_string;
        warn $error_msg;
    }

    return $taxonomy_id;
}


=pod

=head2 GetTaxonomyData

B<Description>: fetch NBCI taxonomy for the given taxonomy IDs or load taxonomy
from the given file.

B<ArgsCount>: 1-2

=over 4

=item $taxonomy_id: (integer or array ref of integer) (R)

The NCBI taxonomy identifiers to fetch.

=item $ncbi_taxonomy_filename: (string) (O)

Path to the taxonomy file downloaded from NCBI. If omitted, it will be
downloaded from the web using the taxonomy identifier provided above.

=back

B<Return>: (hash ref)

Hash representing the XML content of a NCBI taxonomy record or undef.

Ex.:

    {
      'CreateDate' => '2005/12/05 10:31:44',
      'Division' => 'Plants', # deprecated!
      'TaxId' => '360583',
      'MitoGeneticCode' => {
                             'MGCId' => '1',
                             'MGCName' => 'Standard'
                           },
      'OtherNames' => {
                        'CommonName' => 'annual wallrocket',
                        'Synonym' => 'Diplotaxis muralis (L.) DC.'
                      },
      'LineageEx' => {
                       'Taxon' => [
                                    {
                                      'TaxId' => '131567',
                                      'Rank' => 'no rank',
                                      'ScientificName' => 'cellular organisms'
                                    },
                                    {
                                      'TaxId' => '2759',
                                      'Rank' => 'superkingdom',
                                      'ScientificName' => 'Eukaryota'
                                    },
                                    {
                                      'TaxId' => '33090',
                                      'Rank' => 'kingdom',
                                      'ScientificName' => 'Viridiplantae'
                                    },
                                    {
                                      'TaxId' => '35493',
                                      'Rank' => 'phylum',
                                      'ScientificName' => 'Streptophyta'
                                    },
                                    {
                                      'TaxId' => '131221',
                                      'Rank' => 'no rank',
                                      'ScientificName' => 'Streptophytina'
                                    },
                                    {
                                      'TaxId' => '3193',
                                      'Rank' => 'no rank',
                                      'ScientificName' => 'Embryophyta'
                                    },
                                    {
                                      'TaxId' => '58023',
                                      'Rank' => 'no rank',
                                      'ScientificName' => 'Tracheophyta'
                                    },
                                    {
                                      'TaxId' => '78536',
                                      'Rank' => 'no rank',
                                      'ScientificName' => 'Euphyllophyta'
                                    },
                                    {
                                      'TaxId' => '58024',
                                      'Rank' => 'no rank',
                                      'ScientificName' => 'Spermatophyta'
                                    },
                                    {
                                      'TaxId' => '3398',
                                      'Rank' => 'no rank',
                                      'ScientificName' => 'Magnoliophyta'
                                    },
                                    {
                                      'TaxId' => '71240',
                                      'Rank' => 'no rank',
                                      'ScientificName' => 'eudicotyledons'
                                    },
                                    {
                                      'TaxId' => '91827',
                                      'Rank' => 'no rank',
                                      'ScientificName' => 'core eudicotyledons'
                                    },
                                    {
                                      'TaxId' => '71275',
                                      'Rank' => 'subclass',
                                      'ScientificName' => 'rosids'
                                    },
                                    {
                                      'TaxId' => '91836',
                                      'Rank' => 'no rank',
                                      'ScientificName' => 'malvids'
                                    },
                                    {
                                      'TaxId' => '3699',
                                      'Rank' => 'order',
                                      'ScientificName' => 'Brassicales'
                                    },
                                    {
                                      'TaxId' => '3700',
                                      'Rank' => 'family',
                                      'ScientificName' => 'Brassicaceae'
                                    },
                                    {
                                      'TaxId' => '981071',
                                      'Rank' => 'tribe',
                                      'ScientificName' => 'Brassiceae'
                                    },
                                    {
                                      'TaxId' => '3731',
                                      'Rank' => 'genus',
                                      'ScientificName' => 'Diplotaxis'
                                    }
                                  ]
                     },
      'Lineage' => 'cellular organisms; Eukaryota; Viridiplantae; Streptophyta; Streptophytina; Embryophyta; Tracheophyta; Euphyllophyta; Spermatophyta; Magnoliophyta; eudicotyledons; core eudicotyledons; rosids; malvids; Brassicales; Brassicaceae; Brassiceae; Diplotaxis',
      'Rank' => 'species',
      'GeneticCode' => {
                         'GCName' => 'Standard',
                         'GCId' => '1'
                       },
      'PubDate' => '2006/05/26 18:00:20',
      'ScientificName' => 'Diplotaxis muralis',
      'UpdateDate' => '2005/12/05 12:52:51',
      'ParentTaxId' => '3731'
    }

=cut

sub GetTaxonomyData
{
    my ($taxonomy_id, $ncbi_taxonomy_filename) = @_;

    if (!$taxonomy_id && !$ncbi_taxonomy_filename)
    {
        confess "Missing taxonomy identifier!\n";
    }

    # check if it's not an array and turn it into an array
    if (!ref($taxonomy_id))
    {
        $taxonomy_id = [$taxonomy_id];
    }

    my $ncbi_taxonomy_xml_data = '';
    # check how to get taxonomy
    if ($ncbi_taxonomy_filename
        && (-s $ncbi_taxonomy_filename))
    {
        # user provided a file
        my $taxonomy_fh;
        if (open($taxonomy_fh, $ncbi_taxonomy_filename))
        {
          $ncbi_taxonomy_xml_data = join('', <$taxonomy_fh>);
        }
        else
        {
            confess "Unable to open file '$ncbi_taxonomy_filename'!\n$!\n";
        }
    }
    else
    {
        # fetch lineage from NCBI for all species
        # http://eutils.ncbi.nlm.nih.gov/entrez/eutils/efetch.fcgi?db=taxonomy&report=xml&id=...&id=...
        my $taxonomy_id_list = '&id=' . join('&id=', @$taxonomy_id);
        my $taxonomy_url = GetURL('fetch_lineage', undef, {'append' => $taxonomy_id_list});
        PrintDebug("URL: $taxonomy_url");
        my $ua = LWP::UserAgent->new();
        $ua->timeout(120); # seconds
        $ua->env_proxy();
        my $response = $ua->get($taxonomy_url);

        my $max_tries = 3;
        # retry in 5 seconds if NCBI did not answer correctly
        while (!$response->is_success() && $max_tries--)
        {
            PrintDebug('Fetch failed, retrying...');
            sleep(5);
            $response = $ua->get($taxonomy_url);
        }

        if (!$response->is_success())
        {
            PrintDebug('Fetch failed!');
            my $error_msg = "Failed to fetch taxonomy data from NCBI! "
                . $response->status_line . "\n"
                . $response->as_string;
            warn $error_msg;
            return undef;
        }

        $ncbi_taxonomy_xml_data = $response->content;
        
        # if a file name was given, store data
        my $taxonomy_fh;
        if ($ncbi_taxonomy_filename)
        {
            PrintDebug("Saving taxonomy data into '$ncbi_taxonomy_filename'");
            if (open($taxonomy_fh, ">$ncbi_taxonomy_filename"))
            {
                print {$taxonomy_fh} $ncbi_taxonomy_xml_data;
                close($taxonomy_fh);
            }
            else
            {
                cluck "Unable to open file '$ncbi_taxonomy_filename' for writting!\n$!\n";
            }
        }
    }

    # Parse XML
    my $ncbi_taxonomy = XMLin(
        $ncbi_taxonomy_xml_data,
        'ForceArray' => ['Taxon','Synonym'],
        'KeyAttr'    => {},
    );

    if (!$ncbi_taxonomy || !ref($ncbi_taxonomy))
    {
        confess "Failed to parse XML data!\n";
    }

    return $ncbi_taxonomy;
}


=pod

=head2 StoreTaxonomyForSpecies

B<Description>: Load NBCI taxonomy for given taxonomy ID and insert taxonomy
relationship tree into taxonomy table.

B<ArgsCount>: 1

=over 4

=item $tax_id: (integer) (R)

The NCBI taxonomy identifier.

=back

B<Return>: nothing

=cut

sub StoreTaxonomyForSpecies
{
    my ($taxonomy_id, $species_code) = @_;

    my $ncbi_taxonomy = GetTaxonomyData($taxonomy_id, GP('SPECIES_PATH') . '/' . $species_code . '_taxonomy.xml');
    StoreTaxonomyData($ncbi_taxonomy);

}


=pod

=head1 AUTHORS

Mathieu ROUARD (Bioversity), m.rouard@cgiar.org

Valentin GUIGNON (Bioversity), v.guignon@cgiar.org

Matthieu CONTE (Syngenta)

Sebastien BRIOIS (Syngenta)

=head1 VERSION

Version 1.1.0

Date 10/12/2013

=head1 SEE ALSO

GreenPhyl documentation.

=cut

return 1; # package return
