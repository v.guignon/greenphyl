=pod

=head1 NAME

Greenphyl::Load::GO - Manages GO loading

=head1 SYNOPSIS

    [my $instance = Greenphyl::PackageName->new(); #+++

    $instance->subName(); #+++

    ...] #+++

=head1 REQUIRES

#--- Module requierments

#--- example:

[Perl5.004, Exporter, Geometry::Point] #+++

=head1 EXPORTS

#--- Tells what this module will do to other namespaces when imported.

#--- example:

[Nothing] #+++

=head1 DESCRIPTION

#--- What this module can do, how to use it, supported objects, ...

[Package description.] #+++

=cut

package Greenphyl::Load::GO;


use strict;
use warnings;

use Carp qw(cluck confess croak);

use base qw(Exporter);

our @EXPORT = qw(ParseGOOBO); 
#+++ our @EXPORT_OK = qw(... list of functions exported on demand...); 

use Greenphyl;
use Greenphyl::GO;
use Greenphyl::IPR;
use Greenphyl::DBXRef;




# Package constants
####################

=pod

=head1 CONSTANTS

B<$CONSTANT_NAME>:  ([constant nature]) #+++

[constant description and use] #+++

#--- Example:
#--- B<$DEBUG>: (boolean)
#--- When set to true, it enables debug mode.
#--- ...
#--- our $DEBUG = 0;
#---
=cut

#+++ our [$CONSTANT_NAME] = ["value"];




# Package variables
####################

=pod

=head1 VARIABLES

B<[$m_variable_name]>: ([variable nature]) #+++

[member variable description] #+++
Default: [variable default value if one] #+++

#--- Example:
#--- B<$m_output_method>: (integer)
#---
#--- used to store current output method;
#--- 0=raw text (default), 1=image, 2=html.
#---
#---     ...
#---
#--- my $m_output_method = 0;
#---

=cut

#+++ my/our [$m_variable_name] = ["value"];




# Package subs
###############

=pod

=head1 STATIC METHODS

=head2 StoreGO

B<Description>: store a new GO object in database.

B<ArgsCount>: 1

=over 4

=item $go_data: (hash ref) (R)

a GO hash containg the data:
* id => (string) GO code
* namespace => (string) GO namespace
* name => (string) GO name
* alt_id => array of alternate GO codes
* synonym => array of synonym strings
* is_a => array of is_a GO codes
* relationship => array of arrays containing pairs "relationship type", "GO
  code"


=back

B<Return>: (Greenphyl::GO)

The corresponding Greenphyl::GO object.

B<Example>:

    StoreGO(...);

=cut

sub StoreGO
{
    my ($go_data) = @_;
    my $dbh = GetDatabaseHandler();

    if (!$go_data->{'go_code'})
    {
        my $more_info = '';
        if (keys(%$go_data))
        {
            $more_info = ' ' . join(', ', map {"'$_' -> '" . $go_data->{$_} . "'"} keys(%$go_data));
        }
        confess("ERROR: unable to store GO term: no GO code given!$more_info");
    }
    LogInfo("Storing GO term '" . $go_data->{'go_code'} . "'!");
    # check GO type
    if (!$go_data->{'name'})
    {
        LogWarning("GO term name not provided for GO '" . $go_data->{'go_code'} . "'!");
        $go_data->{'name'} = '';
    }
    if (!$go_data->{'namespace'})
    {
        LogWarning("GO term type not found for GO '" . $go_data->{'go_code'} . "'!");
        $go_data->{'namespace'} = '';
    }

    my $go = Greenphyl::GO->new(
        $dbh,
        {
            'load'    => 'none',
            'members' => $go_data,
        },
    );

    # store data
    $go->save();

    if ($go)
    {
        # save other GO fields (synonyms, relationships,...)
        # synonyms
        if ($go_data->{'synonym'} && @{$go_data->{'synonym'}})
        {
            foreach my $synonym (@{$go_data->{'synonym'}})
            {
                $dbh->do("INSERT INTO go_synonyms (go_id, synonym) VALUE (" . $go->id . ", ?);", undef, $synonym);
            }
        }
        # alternate GO codes
        if ($go_data->{'alt_id'} && @{$go_data->{'alt_id'}})
        {
            foreach my $alternate_code (@{$go_data->{'alt_id'}})
            {
                $dbh->do("INSERT INTO go_alternates (go_id, alternate_code) VALUE (" . $go->id . ", ?);", undef, $alternate_code);
            }
        }
        # relationships
        if ($go_data->{'relationship'} && @{$go_data->{'relationship'}})
        {
            foreach my $relationship (@{$go_data->{'relationship'}})
            {
                # make sure the GO is in database
                StoreGO({'go_code' => $relationship->[1]});
                $dbh->do("INSERT INTO go_relationships (subject_go_id, object_go_id, relationship) SELECT " . $go->id . ", g.id, ? FROM go g WHERE g.code = ?;", undef, @$relationship);
            }
        }
        if ($go_data->{'is_a'} && @{$go_data->{'is_a'}})
        {
            foreach my $is_a_go_code (@{$go_data->{'is_a'}})
            {
                StoreGO({'go_code' => $is_a_go_code});
                $dbh->do("INSERT INTO go_relationships (subject_go_id, object_go_id, relationship) SELECT " . $go->id . ", g.id, 'is_a' FROM go g WHERE g.code = ?;", undef, $is_a_go_code);
            }
        }
    }
    else
    {
        LogError("Failed to insert a new GO ('" . $go_data->{'go_code'} . "')!");
    }
    
    return $go;
}

    
=cut

=head2 ParseGOOBO

B<Description>: Given an OBO file, it parses GO data into a hash structure. If
the OBO file is missing, the user will be prompted to load it from the web. If
the file exists, the user will be prompted for a web update of the file.

B<ArgsCount>: 0-1

=over 4

=item $obo_file_path: (string) (O)

Path to the OBO file. If not specified, GP('GO_OBO_FILE_PATH') will be used.

=back

B<Return>: (hashref)

A hash which keys are GO codes and values are hash:
id => (string) GO code
namespace => (string) GO namespace
name => (string) GO name
alt_id => array of alternate GO codes
synonym => array of synonym strings
is_a => array of is_a GO codes
relationship => array of arrays containing pairs "relationship type", "GO code"

B<Example>:

    ParseGOOBO('./mygofile.obo');

=cut

sub ParseGOOBO
{
    my ($obo_file_path) = @_;

    # load GO data...
    # -make sure we got a file path
    if (!$obo_file_path)
    {
        $obo_file_path = GP('GO_OBO_FILE_PATH');
    }

    # -check if the file exists
    if (!-e $obo_file_path)
    {
        if (Prompt("GO OBO file is missing ('$obo_file_path'). Fetch it from the web? (y/n)", { 'default' => 'y', 'constraint' => '[yYnN]' }) =~ m/y/i)
        {
            if (system("wget -O '$obo_file_path' '" . GP('GENE_ONTOLOGY_OBO_URL') . "'"))
            {
                confess "ERROR: Failed to fetch file using wget!\n$?\n";
            }
        }
        else
        {
            confess "ERROR: no GO OBO file available!\n";
        }
    }
    else
    {
        # replace/update existing file?
        if (Prompt("Update current GO OBO file ('$obo_file_path') using the web? (y/n)", { 'default' => 'n', 'constraint' => '[yYnN]' }) =~ m/y/i)
        {
            unlink($obo_file_path);
            if (system("wget -O '$obo_file_path' '" . GP('GENE_ONTOLOGY_OBO_URL') . "'"))
            {
                confess "ERROR: Failed to fetch file using wget!\n$?\n";
            }
        }
        else
        {
            LogInfo("Existing OBO file '$obo_file_path' not updated.");
        }
    }
            
    LogInfo("GO OBO file: '$obo_file_path'");
    # open GO file
    my $go_fh;
    if (!open($go_fh, $obo_file_path))
    {
        confess "ERROR: Failed to open GO OBO file '$obo_file_path' (specified in GreenPhyl config file)!\n$!\n";
    }
    my $obo_file_size = -s $obo_file_path;
    my $progress = 0;
    my $obo_data = {}; # keys: GO ID, values: {namespace => ..., name => ...}
    my $current_go_data = {};

    # parse file
    LogInfo("Loading Gene Ontology data");
    while (my $line = <$go_fh>)
    {
        $progress += length($line);
        print GetProgress(
            {
                'label'   => ($current_go_data->{'id'}? '(' . $current_go_data->{'id'} . ')':''),
                'current' => $progress,
                'total'   => $obo_file_size,
            },
            80,
        );

        # Parsing: see http://www.geneontology.org/GO.format.obo-1_2.shtml
        # check if we got a term block
        if ($line =~ m/^\s*\[Term\]/i)
        {
            # save current data
            if ($current_go_data->{'id'})
            {
                $obo_data->{$current_go_data->{'id'}} = $current_go_data;
            }
            # reset GO data
            $current_go_data = {};
        }
        elsif ($line =~ m/^\s*id:\s*(GO:\d{7})/i)
        {
            # new GO ID
            $current_go_data->{'id'} = uc($1);
        }
        elsif ($line =~ m/^\s*namespace:\s*(.*?)\s*$/i)
        {
            # GO type
            $current_go_data->{'namespace'} = $1;
        }
        elsif ($line =~ m/^\s*name:\s*(.*?)\s*$/i)
        {
            # GO name
            $current_go_data->{'name'} = $1;
        }
        elsif ($line =~ m/^\s*alt_id:\s*(GO:\d{7})\s*$/i)
        {
            # GO alternates
            $current_go_data->{'alt_id'} ||= [];
            push(@{$current_go_data->{'alt_id'}}, $1);
        }
        elsif ($line =~ m/^\s*(?:exact_|narrow_|broad_)?synonym:\s*"(.*?)"/i)
        {
            # GO synonym
            $current_go_data->{'synonym'} ||= [];
            push(@{$current_go_data->{'synonym'}}, $1);
        }
        elsif ($line =~ m/^\s*is_a:\s*(GO:\d{7})/i)
        {
            # GO alternates
            $current_go_data->{'is_a'} ||= [];
            push(@{$current_go_data->{'is_a'}}, $1);
        }
        elsif ($line =~ m/^\s*relationship:\s*(\S+)\s+(GO:\d{7})/i)
        {
            # GO relationships
            $current_go_data->{'relationship'} ||= [];
            push(@{$current_go_data->{'relationship'}}, [$1, $2]);
        }
        elsif ($line =~ m/^\s*(?:is_obsolete|replaced_by|consider|use_term)/i)
        {
            # obsolete GO, clear it
            $current_go_data = {};
        }
    }
    # save last parsed GO
    if ($current_go_data->{'id'})
    {
        $obo_data->{$current_go_data->{'id'}} = $current_go_data;
    }
    close($go_fh);
    LogInfo("Gene Ontology data loaded.");
    

    # make sure we loaded something
    my $loaded_go_data = scalar(keys(%$obo_data));
    if ($loaded_go_data < 10)
    {
        confess "ERROR: it seems the GO OBO file ('$obo_file_path') was not parsed correctly or did not contain valid data (less than 10 entries)!\n";
    }
    LogInfo("GO entries loaded: $loaded_go_data");
            
    return $obo_data;
}


=pod

=head2 ParseAndUpdateIPR2GO

B<Description>: .

Mapping:
http://www.geneontology.org/GO.format.ext2go.shtml

Mapping format:

    !comments
    external database:term identifier (id/name) > GO:GO term name ; GO:id

Example:

    !version date: 2012/09/22 08:36:26
    !description: Mapping of GO terms to InterPro entries.
    !external resource: http://www.ebi.ac.uk/interpro
    !citation : Hunter et al. (2009) Nucleic Acids Res. 37 :D211-D215
    !contact:interhelp@ebi.ac.uk
    !
    InterPro:IPR000003 Retinoid X receptor/HNF4 > GO:DNA binding ; GO:0003677


B<ArgsCount>: 0-1

=over 4

=item $iprto_go_file_path: (string) (O)

Path to the IPR to GO file. If not specified, GP('IPR_TO_GO_FILE_PATH') will be used.

=back

B<Return>: (hashref)


B<Example>:

    ParseIPR2GO('./interpro2go');

=cut

sub ParseAndUpdateIPR2GO
{
    my ($ipr2go_file_path, $obo_data) = @_;

    my ($inserted_go, $inserted_go_ipr) = (0, 0);
    LogInfo("Updating IPR to GO");

    # process IPR to GO update...
    if (!$ipr2go_file_path)
    {
        $ipr2go_file_path = GP('IPR_TO_GO_FILE_PATH');
    }
    
    # -check if the file exists
    if (!-e $ipr2go_file_path)
    {
        if (Prompt("Interpro to GO mapping file is missing ('$ipr2go_file_path')! Fetch it from the web? (y/n)", { 'default' => 'y', 'constraint' => '[yYnN]' }) =~ m/y/i)
        {
            if (system("wget -O '$ipr2go_file_path' '$Greenphyl::Config::IPR_TO_GENE_ONTOLOGY_URL'"))
            {
                confess "ERROR: Failed to fetch file using wget!\n$?\n";
            }
        }
        else
        {
            confess "ERROR: no Interpro to GO mapping file available!\n";
        }
    }
    else
    {
        # replace/update existing file?
        if (Prompt("Update current Interpro to GO mapping file '$ipr2go_file_path' using the web? (y/n)", { 'default' => 'n', 'constraint' => '[yYnN]' }) =~ m/y/i)
        {
            unlink($ipr2go_file_path);
            if (system("wget -O '$ipr2go_file_path' '$Greenphyl::Config::IPR_TO_GENE_ONTOLOGY_URL'"))
            {
                confess "ERROR: Failed to fetch file using wget!\n$?\n";
            }
        }
        else
        {
            LogInfo("Existing Interpro to GO mapping file '$ipr2go_file_path' not updated.");
        }
    }

    LogInfo("IPR2GO mapping file: '$ipr2go_file_path'");

    # get current database handler
    my $dbh = GetDatabaseHandler();

    # open IPR to GO file
    my $ipr_to_go_fh;
    if (!open($ipr_to_go_fh, $ipr2go_file_path))
    {
        confess "ERROR: Failed to open file '$ipr2go_file_path'!\n$!\n";
    }
    my $ipr_file_size = -s $ipr2go_file_path;
    my $progress = 0;

    # get release date
    # format: "!version date: YYYY/MM/DD hh:mm:ss"
    my ($ipr_to_go_version) = (<$ipr_to_go_fh> =~ m/!version date: (\S+ \S+)/i);
    LogInfo("IPR2GO version: $ipr_to_go_version");
    if (!$dbh->do("INSERT INTO variables (name, value) VALUES ('ipr2go_version', ?) ON DUPLICATE KEY UPDATE value = ?;", undef, $ipr_to_go_version, $ipr_to_go_version))
    {
        LogWarning("Failed to store IPR2GO version ($ipr_to_go_version)!");
    }

    # parse each line
    LogInfo("Parsing IPR to GO data");
IPR_TO_GO_LOOP:
    while (my $line = <$ipr_to_go_fh>)
    {
        $progress += length($line);
        print GetProgress(
            {
                'label'   => "                          ", # length correspond to '(IPR012345 --> GO:0123456)'
                'current' => $progress,
                'total'   => $ipr_file_size,
            },
            80,
        );

        # skip comments and empty lines
        if (($line =~ m/^!/) || ($line =~ m/^\s*$/))
        {
            next IPR_TO_GO_LOOP;
        }

        # get IPR code
        if ($line =~ m/^InterPro:(IPR\d{6}) (?:.*?>\s+GO:)(.*?)\s*;\s*(GO:\d{7})\s*$/i)
        {
            my ($ipr_code, $go_name, $go_code) = ($1, $2, $3);
            # keep original name if available
            if (($obo_data->{$go_code}->{'name'}) && ($go_name ne $obo_data->{$go_code}->{'name'}))
            {
                LogWarning("WARNING: GO name in Interpro to GO mapping file and GO name in OBO file differ:\n'" . $go_name . "'\n'" . $obo_data->{$go_code}->{'name'} . "'");
                $go_name = $obo_data->{$go_code}->{'name'};
            }
            print GetProgress(
                {
                    'label'   => "($ipr_code --> $go_code)",
                    'current' => $progress,
                    'total'   => $ipr_file_size,
                },
                80,
            );

            # check if IPR is in database
            my $ipr = Greenphyl::IPR->new(
                $dbh,
                {
                    'selectors' => {'code' => ['LIKE', $ipr_code],},
                },
            );

            if ($ipr)
            {
                my $go = Greenphyl::GO->new(
                    $dbh,
                    {
                        'selectors' => {'code' => ['LIKE', $go_code],},
                    },
                );
                
                # check if GO is in database
                if (!$go)
                {
                    # not there, insert...
                    $go = StoreGO($obo_data->{$go_code});
                    ++$inserted_go;
                }

                if ($go)
                {
                    # link IPR to GO if needed
                    if (!$dbh->do("INSERT IGNORE INTO go_ipr (ipr_id, go_id, evidence_code) VALUES (?, ?, 'IC');", undef, $ipr->id, $go->id))
                    {
                        LogError("Failed to insert a GO to IPR relationship! IPR: '$ipr_code' (id=" . $ipr->id . ", GO: '$go_code' (id=" . $go->id . ")\n");
                    }
                    else
                    {
                        ++$inserted_go_ipr;
                    }
                }
            }
            else
            {
                LogDebug("skipping IPR not found in database: $ipr_code");
            }
        }
        else
        {
            LogWarning("Failed to parse IPR line:\n$line\n");
        }
    }
    LogInfo("Done parsing IPR to GO data");

    # close file
    close($ipr_to_go_fh);
}




=pod

=head1 AUTHORS

Mathieu ROUARD (Bioversity), m.rouard@cgiar.org

Valentin GUIGNON (Bioversity), v.guignon@cgiar.org

Matthieu CONTE (Syngenta)

Sebastien BRIOIS (Syngenta)

=head1 VERSION

Version 1.0.0

Date 26/09/2012

=head1 SEE ALSO

GreenPhyl documentation.

=cut

return 1; # package return
