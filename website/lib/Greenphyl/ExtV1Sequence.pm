=pod

=head1 NAME

Greenphyl::ExtV1Sequence - GreenPhyl v1 Sequence Object

=head1 SYNOPSIS

    use Greenphyl::ExtV1Sequence;
    my $v1_sequence = Greenphyl::ExtV1Sequence->new($dbh, {'selectors' => {'seq_textid' => ['LIKE', 'Os01g01050.1']}});
    print $v1_sequence->name();

=head1 REQUIRES

Perl 5.8.0, Greenphyl v1,
BioPerl

=head1 EXPORTS

nothing

=head1 DESCRIPTION

This module implements a GreenPhyl v1 sequence database object to use with an
external v1 database.

=cut

package Greenphyl::ExtV1Sequence;

use strict;
use warnings;

use base qw(Greenphyl::AbstractSequence);

use vars qw($AUTOLOAD);

use Carp qw(cluck confess croak);

use Greenphyl;




# Package constants
####################

=pod

=head1 CONSTANTS

B<$OBJECT_PROPERTIES>: (hash ref)

sequence object properties description. See DBObject constructor documentation
for details.

=cut

our $DEBUG = 0;

our $OBJECT_PROPERTIES = {
    'table' => 'SEQUENCES',
    'key' => 'seq_id',
    'alternate_keys' => [],
    'load'     => [
        'MEME_score',
        'annotation',
        'bbmh',
        'hidden',
        'scanned',
        'seq_id',
        'seq_length',
        'seq_textid',
        'sequence',
        'species_id',
    ],
    'alias' => {
        'filtered'    => 'hidden',
        'id'          => 'seq_id',
        'sequence_id' => 'seq_id',
        'length'      => 'seq_length',
        'textid'      => 'seq_textid',
        'accession'   => 'seq_textid',
        'polypeptide' => 'sequence',
    },
    'base' => {
        'MEME_score' => {
            'property_column' => 'MEME_score',
        },
        'annotation' => {
            'property_column' => 'annotation',
        },
        'bbmh' => {
            'property_column' => 'bbmh',
        },
        'hidden' => {
            'property_column' => 'hidden',
        },
        'scanned' => {
            'property_column' => 'scanned',
        },
        'seq_id' => {
            'property_column' => 'seq_id',
        },
        'seq_length' => {
            'property_column' => 'seq_length',
        },
        'seq_textid' => {
            'property_column' => 'seq_textid',
        },
        'sequence' => {
            'property_column' => 'sequence',
        },
        'species_id' => {
            'property_column' => 'species_id',
        },
        'locus' => {
            'property_column' => "substring_index(seq_textid, '.', 1)",
        },
    },
    'secondary' => {
        'alias' => {
            'object_key'     => "substring_index(seq_textid, '.', 1)",
            'property_table'  => 'LOCUS',
            'property_key'    => 'locus',
            'multiple_values' => 0,
            'property_column' => 'alias',
        },
        'swiss' => {
            'object_key'     => "substring_index(seq_textid, '.', 1)",
            'property_table'  => 'LOCUS',
            'property_key'    => 'locus',
            'multiple_values' => 0,
            'property_column' => 'swiss',
        },
        'kegg' => {
            'object_key'     => "substring_index(seq_textid, '.', 1)",
            'property_table'  => 'LOCUS',
            'property_key'    => 'locus',
            'multiple_values' => 0,
            'property_column' => 'kegg',
        },
        'ec' => {
            'object_key'     => "substring_index(seq_textid, '.', 1)",
            'property_table'  => 'LOCUS',
            'property_key'    => 'locus',
            'multiple_values' => 0,
            'property_column' => 'EC',
        },
        'species_code' => {
            'object_key'     => 'species_id',
            'property_table'  => 'SPECIES',
            'property_key'    => 'species_id',
            'multiple_values' => 0,
            'property_column' => 'species_name',
        },
    },
    'tertiary' => {
        'families' => {
            'link_table'        => 'SEQ_IS_IN',
            'link_object_key'   => 'seq_id',
            'link_property_key' => 'family_id',
            'multiple_values'   => 1,
            'property_module'   => 'Greenphyl::ExtV1Family',
            'property_table'    => 'FAMILY',
            'property_key'      => 'family_id',
        },
    },
};

our $SUPPORTED_DUMP_FORMAT = $Greenphyl::AbstractSequence::SUPPORTED_DUMP_FORMAT;




# Package subs
###############

=pod

=head1 STATIC METHODS

=head2 CONSTRUCTOR

B<Description>: Creates a new instance of a GreenPhyl v1 sequence object.

B<ArgsCount>: 1-2

=over 4

=item $dbh: (DBI) (U)

Database handler of the database to load the object from.

=item $parameters: (hash ref) (R)

See Greenphyl::DBObject constructor documentation for details.

=back

B<Return>: (Greenphyl::ExtV1Sequence)

a new instance.

B<Caller>: General

B<Example>:

    my $v1_sequence = new Greenphyl::Sequence($dbh, {'selectors' => {'seq_textid' => ['LIKE', 'Os01g01050%']}});

=cut

sub new
{
    my ($proto) = shift();
    my $class = ref($proto) || $proto;

    return $class->SUPER::new(@_);
}


=pod

=head1 METHODS

=head2 getStringValue

B<Description>: returns sequence name.

B<ArgsCount>: 1

=over 4

=item $self: (Greenphyl::AbstractSequence)

the sequence.

=back

B<Return>: (string)

the sequence name (seq_textid).

B<Caller>: internal

=cut

sub getStringValue
{
    my ($self) = @_;
    return $self->{'seq_textid'};
}


=pod

=head2 getKEGG

B<Description>: returns the sequence KEGG DBXRef.

B<Aliases>: kegg, getKegg

B<ArgsCount>: 1

=over 4

=item $self: (Greenphyl::AbstractSequence) (R)

Current sequence.

=back

B<Return>: (array ref)

Returns an array of sequence EC or an empty array.

=cut

sub getKEGG
{
    my ($self) = shift();

    return $self->SUPER::SUPER::kegg(@_);
}
# Aliases
sub getKegg; *getKegg = \&getKEGG;
sub kegg; *kegg = \&getKEGG;


=pod

=head2 getEC

B<Description>: returns the sequence EC DBXRef.

B<Aliases>: ec

B<ArgsCount>: 1

=over 4

=item $self: (Greenphyl::AbstractSequence) (R)

Current sequence.

=back

B<Return>: (array ref)

Returns an array of sequence EC or an empty array.

=cut

sub getEC
{
    my ($self) = shift();

    return $self->SUPER::SUPER::ec(@_);
}
# Aliases
sub ec; *ec = \&getEC;


=pod

=head1 AUTHORS

Mathieu ROUARD (Bioversity), m.rouard@cgiar.org

Valentin GUIGNON (Bioversity), v.guignon@cgiar.org

Matthieu CONTE (Syngenta)

Sebastien BRIOIS (Syngenta)

=head1 VERSION

Version 1.0.0

Date 01/03/2014

=head1 SEE ALSO

GreenPhyl documentation.

=cut

return 1; # package return
