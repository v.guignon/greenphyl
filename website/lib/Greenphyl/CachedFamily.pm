=pod

=head1 NAME

Greenphyl::CachedFamily - GreenPhyl Cached Family Object

=head1 SYNOPSIS

    use Greenphyl::CachedFamily;
    my $family = Greenphyl::CachedFamily->new($dbh, {'selectors' => {'id' => 205}});
    print $family->name();

=head1 REQUIRES

Perl5

=head1 EXPORTS

Nothing

=head1 DESCRIPTION

This module implements a GreenPhyl family database object.

=cut

package Greenphyl::CachedFamily;

use strict;
use warnings;

use base qw(Greenphyl::Family);

use vars qw($AUTOLOAD);

use Carp qw(cluck confess croak);

use Clone qw(clone);

use Greenphyl::Config;
use Greenphyl;




# Package constants
####################

=pod

=head1 CONSTANTS

B<$OBJECT_PROPERTIES>: (hash ref)

sequence object properties description. See DBObject constructor documentation
for details.

=cut

our $DEBUG = 0;
our $OBJECT_PROPERTIES =
    {
        'table' => 'families_cache',
        'relationship' => {
            'table' => 'family_relationships_cache',
            'subject' => 'subject_family_id',
            'object' => 'object_family_id',
        },
        'key' => 'id',
        'alternate_keys' => ['accession',],
        'load'     => [
            'accession',
            # 'alignment_length',
            # 'alignment_sequence_count',
            # 'average_sequence_length',
            'black_listed',
            # 'branch_average',
            # 'branch_median',
            # 'branch_standard_deviation',
            'description',
            'id',
            'name',
            # 'inferences',
            'level',
            # 'masked_alignment_length',
            # 'masked_alignment_sequence_count',
            # 'max_sequence_length',
            # 'median_sequence_length',
            # 'min_sequence_length',
            'plant_specific',
            'sequence_count',
            # 'taxonomy_id',
            # 'type',
            'validated',
        ],
        'alias' => {
            'alignment_seq_count' => 'alignment_sequence_count',
            'average_seq_length' => 'average_sequence_length',
            'black_list' => 'black_listed',
            'classification' => 'level',
            'family_has_an' => 'ipr_species',
            'family_id' => 'id',
            'max_seq_length' => 'max_sequence_length',
            'median_seq_length' => 'median_sequence_length',
            'min_seq_length' => 'min_sequence_length',
            'family_name' => 'name',
            'family_accession' => 'accession',
            'plant_spe' => 'plant_specific',
            'pubmed' => 'pmid',
            'seq_count'  => 'sequence_count',
            'synonym' => 'synonyms',
            'phylonode' => 'taxonomy',
            'count_family_seq' => 'sequence_count_by_families_and_species',
            'sequence_count_by_families_species' => 'sequence_count_by_families_and_species',
            'sequence_count_without_splice' => 'sequence_count_without_splice_by_families_and_species_sum',
        },
        'base' => {
            'alignment_length' => {
                'property_column' => 'alignment_length',
            },
            'alignment_sequence_count' => {
                'property_column' => 'alignment_sequence_count',
            },
            'average_sequence_length' => {
                'property_column' => 'average_sequence_length',
            },
            'black_listed' => {
                'property_column' => 'black_listed',
            },
            'branch_average' => {
                'property_column' => 'branch_average',
            },
            'branch_median' => {
                'property_column' => 'branch_median',
            },
            'branch_standard_deviation' => {
                'property_column' => 'branch_standard_deviation',
            },
            'description' => {
                'property_column' => 'description',
            },
            'id' => {
                'property_column' => 'id',
            },
            'level' => {
                'property_column' => 'level',
            },
            'name' => {
                'property_column' => 'name',
            },
            'accession' => {
                'property_column' => 'accession',
            },
            'inferences' => {
                'property_column' => 'inferences',
            },
            'masked_alignment_length' => {
                'property_column' => 'masked_alignment_length',
            },
            'masked_alignment_sequence_count' => {
                'property_column' => 'masked_alignment_sequence_count',
            },
            'max_sequence_length' => {
                'property_column' => 'max_sequence_length',
            },
            'median_sequence_length' => {
                'property_column' => 'median_sequence_length',
            },
            'min_sequence_length' => {
                'property_column' => 'min_sequence_length',
            },
            'plant_specific' => {
                'property_column' => 'plant_specific',
            },
            'sequence_count' => {
                'property_column' => 'sequence_count',
            },
            'taxonomy_id' => {
                'property_column' => 'taxonomy_id',
            },
            'type' => {
                'property_column' => 'type',
            },
            'validated' => {
                'property_column' => 'validated',
            },
            'validated_value' => {
                'property_column' => '0+validated',
            },
            # replaces fields ipr, ipr_species
            # caches tables families_ipr_cache, families_ipr_species_cache, ipr
            'ipr_cache' => {
                'property_column' => 'ipr_cache',
                'serialized'      => 1,
            },
            # replaces genomes, species, sequence_count_by_families_and_species, sequence_count_by_families_and_species_sum, sequence_count_without_splice_by_families_and_species_sum
            'sequence_counts_cache' => {
                'property_column' => 'sequence_count_cache',
                'serialized'      => 1,
            },
            # replaces homologies
            'homologies_cache' => {
                'property_column' => 'homologies_cache',
                'serialized'      => 1,
            },
            'sequences_cache' => {
                'property_column' => 'sequences_cache',
                'serialized'      => 1,
            },
        },
        'secondary' => {
            'annotations' => {
                'object_key'      => 'accession',
                'property_table'  => 'family_annotations',
                'property_key'    => 'accession',
                'multiple_values' => 1,
                'property_module' => 'Greenphyl::FamilyAnnotation',
            },
            # 'sequence_count_by_families_and_species' => {
            #     'property_table'  => 'sequence_count_by_families_species_cache',
            #     'property_key'    => 'family_id',
            #     'multiple_values' => 1,
            #     'property_module' => 'Greenphyl::SequenceCount',
            # },
            # 'sequence_count_by_families_and_species_sum' => {
            #     'property_table'  => 'sequence_count_by_families_species_cache',
            #     'property_key'    => 'family_id',
            #     'multiple_values' => 0,
            #     'property_column' => 'SUM(sequence_count)',
            # },
            # 'sequence_count_without_splice_by_families_and_species_sum' => {
            #     'property_table'  => 'sequence_count_by_families_species_cache',
            #     'property_key'    => 'family_id',
            #     'multiple_values' => 0,
            #     'property_column' => 'SUM(sequence_count_without_splice)',
            # },
            'taxonomy' => {
                'object_key'      => 'taxonomy_id',
                'property_table'  => 'taxonomy',
                'property_key'    => 'id',
                'multiple_values' => 0,
                'property_module' => 'Greenphyl::Taxonomy',
            },
            # 'homologies' => {
            #     'property_table'    => 'homologies',
            #     'property_key'      => 'family_id',
            #     'multiple_values'   => 1,
            #     'property_module'   => 'Greenphyl::Homology',
            # },
            # 'ipr_species' => {
            #     'property_table'  => 'families_ipr_species_cache',
            #     'property_key'    => 'family_id',
            #     'multiple_values' => 1,
            #     'property_module' => 'Greenphyl::FamilyIPR',
            # },
            'ipr_stats' => {
                'property_table'  => 'families_ipr_cache',
                'property_key'    => 'family_id',
                'multiple_values' => 1,
                'property_module' => 'Greenphyl::FamilyIPRStats',
            },
            'synonyms' => {
                'property_table'  => 'family_synonyms',
                'property_key'    => 'family_id',
                'multiple_values' => 1,
                'property_column' => 'synonym',
            },
            'processing' => {
                'property_table'  => 'family_processing',
                'property_key'    => 'family_id',
                'multiple_values' => 1,
                'property_module' => 'Greenphyl::FamilyProcessing',
            },
        },
        'tertiary' => {
            'dbxref' => {
                'link_table'        => 'dbxref_families',
                'link_object_key'   => 'family_id',
                'link_property_key' => 'dbxref_id',
                'multiple_values'   => 1,
                'property_module'   => 'Greenphyl::DBXRef',
                'property_table'    => 'dbxref',
                'property_key'      => 'id',
            },
            'go' => {
                'link_table'        => 'families_go_cache',
                'link_object_key'   => 'family_id',
                'link_property_key' => 'go_id',
                'multiple_values'   => 1,
                'property_module'   => 'Greenphyl::GO',
                'property_table'    => 'go',
                'property_key'      => 'id',
            },
            # 'ipr' => {
            #     'link_table'        => 'families_ipr_species_cache',
            #     'link_object_key'   => 'family_id',
            #     'link_property_key' => 'ipr_id',
            #     'multiple_values'   => 1,
            #     'property_module'   => 'Greenphyl::IPR',
            #     'property_table'    => 'ipr',
            #     'property_key'      => 'id',
            # },
            # 'sequences' => {
            #     'link_table'        => 'families_sequences',
            #     'link_object_key'   => 'family_id',
            #     'link_property_key' => 'sequence_id',
            #     'multiple_values'   => 1,
            #     'property_module'   => 'Greenphyl::Sequence',
            #     'property_table'    => 'sequences',
            #     'property_key'      => 'id',
            # },
            # 'species' => {
            #     'link_table'        => 'sequence_count_by_families_species_cache',
            #     'link_object_key'   => 'family_id',
            #     'link_property_key' => 'species_id',
            #     'multiple_values'   => 1,
            #     'property_module'   => 'Greenphyl::Species',
            #     'property_table'    => 'species',
            #     'property_key'      => 'id',
            # },
            # 'genomes' => {
            #     'link_table'        => 'sequence_count_by_families_genomes_cache',
            #     'link_object_key'   => 'family_id',
            #     'link_property_key' => 'genome_id',
            #     'multiple_values'   => 1,
            #     'property_module'   => 'Greenphyl::Genome',
            #     'property_table'    => 'genomes',
            #     'property_key'      => 'id',
            # },
            'processes' => {
                'link_table'        => 'family_processing',
                'link_object_key'   => 'family_id',
                'link_property_key' => 'process_id',
                'multiple_values'   => 1,
                'property_module'   => 'Greenphyl::AnalysisProcess',
                'property_table'    => 'processes',
                'property_key'      => 'id',
            },
        },
    };

our $SUPPORTED_DUMP_FORMAT = $Greenphyl::Family::SUPPORTED_DUMP_FORMAT;



# Package subs
###############

=pod

=head1 STATIC METHODS

=head2 CONSTRUCTOR

B<Description>: Creates a new instance of a GreenPhyl family object.

B<ArgsCount>: 2

=over 4

=item $dbh: (DBI::db) (U)

Database handler of the database to load the object from.

=item $parameters: (hash ref) (R)

See Greenphyl::DBObject constructor documentation for details.

=back

B<Return>: (Greenphyl::CachedFamily)

a new instance.

B<Caller>: General

B<Example>:

    my $family = new Greenphyl::CachedFamily($dbh, {'selectors' => {'id' => 205, 'validated' => ['<>', 0]}});

=cut

sub new
{
    my ($proto) = shift();
    my $class = ref($proto) || $proto;

    return $class->SUPER::new(@_);
}




=pod

=head1 DIAGNOSTICS

=head1 AUTHORS

Mathieu ROUARD (Bioversity), m.rouard@cgiar.org

Valentin GUIGNON (Bioversity), v.guignon@cgiar.org

=head1 VERSION

Version 1.0.0

Date 13/05/2020

=head1 SEE ALSO

GreenPhyl documentation, DBObject documentation.

=cut

return 1; # package return
