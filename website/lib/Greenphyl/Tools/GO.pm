=pod

=head1 NAME

Greenphyl::Tools::GO - GO tools

=head1 SYNOPSIS


=head1 REQUIRES

Perl5

=head1 EXPORTS

LoadGO
GroupGOByType ParseOBOToGraph GetSubtree
GenerateGOBrowserTemplate DoGOBrowserTemplateNeedsUpdate
GetGOFamilies GenerateGOFamilyRelationshipJSONData

=head1 DESCRIPTION

Provides several helper function to process GO and generate associated 
data structures.

=cut

package Greenphyl::Tools::GO;


use strict;
use warnings;

use Carp qw(cluck confess croak);

use base qw(Exporter);

our @EXPORT = qw(
    LoadGO
    GroupGOByType ParseOBOToGraph GetSubtree
    GenerateGOBrowserTemplate DoGOBrowserTemplateNeedsUpdate
    GetGOFamilies GenerateGOFamilyRelationshipJSONData
); 

use JSON;
use GO::Parser;

use Greenphyl;
use Greenphyl::GO;
use Greenphyl::Tools::Families;


# prevent annoying recurrent warnings about missing methods
# from "GO::Handlers::obj" (imported by GO::Parser).
our %seen_warnings;
sub SmartWarning
{
    my ($message) = @_;
    if ($message =~ m/^add method for (\w+)/i)
    {
        if (!exists($seen_warnings{$1}))
        {
            $seen_warnings{$1} = 0;
            warn $message;
        }
        $seen_warnings{$1}++;
    }
}
*GO::Handlers::obj::warn = \&SmartWarning;




# Package constants
####################

=pod

=head1 CONSTANTS

B<$DEBUG>: (boolean)

Enables debug mode.

=cut

our $DEBUG = 0;
our $GO_BROWSER_TEMPLATE = 'cache/go_browser.tt';




# Package subs
###############

=pod

=head1 FUNCTIONS

=head2 GroupGOByType

B<Description>: returns a hash of GO sorted by types.

B<ArgsCount>: 1

=over 4

=item $go_objects: (array ref of Greenphyl::GO)

the GO terms to group.

=back

B<Return>: (hash)

A hash wich keys are GO types and values are the corresponding GO objects.

=cut

sub GroupGOByType
{
    my ($go_objects) = @_;

    my %sorted_go;
    foreach (@$go_objects)
    {
        push @{ $sorted_go{$_->type} }, $_;
    }
    return \%sorted_go;
}


=pod

=head2 LoadGO

B<Description>: Loads and returns a GO object. Throws an exception if the
GO couldn't be loaded. The GO is selected using either (by priority) the
HTTP parameters 'go_code', or 'root' (as GO code for GO tree) or 'go_id'.

B<ArgsCount>: 0

B<Return>: (Greenphyl::GO)

Loaded GO.

=cut

sub LoadGO
{
    my $go_code = GetParameterValues('go_code');
    my $go_id = GetParameterValues('go_id') || GetParameterValues('id');
    my $selector = {};

    # try for code from GO tree
    if (!$go_code)
    {
        $go_code = GetParameterValues('root');
    }
    # check what we got
    if (!$go_id || ($go_id !~ m/^\d{1,10}$/))
    {
        $go_id = undef;
    }
    if (!$go_code || ($go_code !~ m/^$Greenphyl::GO::GO_REGEXP$/o))
    {
        $go_code = undef;
    }
    
    if ($go_code)
    {
        $selector = {'code' => $go_code};
    }
    elsif ($go_id)
    {
        $selector = {'id' => $go_id};
    }
    else
    {
        Throw('error' => 'No valid GO identifier provided!');
    }

    my $go = Greenphyl::GO->new(
        GetDatabaseHandler(),
        {'selectors' => $selector }
    );

    if (!$go)
    {
        if ($go_code)
        {
            Throw('error' => "GO not found!", 'log' => "Code: $go_code");
        }
        else
        {
            Throw('error' => "GO ID not found!", 'log' => "ID: $go_id");
        }
    }

    return $go;
}


=pod

=head2 ParseOBOToGraph

B<Description>: returns a GO::Model::Graph corresponding to the given OBO file.

B<ArgsCount>: 1-2

=over 4

=item $format: (string) (U)

File format to use for parsing.
Can be one of:

=over 4

=item obo_text:
    Files with suffix ".obo"
    This is a new file format replacement for the existing GO flat file formats.
    It handles ontologies, definitions and xrefs (but not associations)

=item go_ont:
    Files with suffix ".ontology"
    These store the ontology DAGs

=item go_def:
    Files with suffix ".defs"

=item go_xref:
    External database references for GO terms
    Files with suffix "2go" (eg ec2go, metacyc2go)

=item go_assoc:
    Annotations of genes or gene products using GO
    Files with prefix "gene-association."

=item obo_xml:
    Files with suffix ".obo.xml" or ".obo-xml"
    This is the XML version of the OBO flat file format above
    See http://www.godatabase.org/dev/xml/doc/xml-doc.html

=item obj_yaml:
    A YAML dump of the perl GO::Model::Graph object. You need YAML from CPAN for
    this to work

=item obj_storable:
    A dump of the perl GO::Model::Graph object. You need Storable from CPAN for
    this to work. This is intended to cache objects on the filesystem, for fast
    access. The obj_storable representation may not be portable

=back

See http://search.cpan.org/~cmungall/go-perl-0.14/GO/Parser.pm for details
Default: not specified (GO::Parser default).

=item @go_obo_files: (array of string) (R)

list of OBO file path.

=back

B<Return>: (hash)

A hash wich keys are GO types and values are the corresponding GO objects.

=cut

sub ParseOBOToGraph
{
    my ($format, @go_obo_files) = @_;
    # format=>'obo_xml',
    my $parser_parameters = { 'handler' => 'obj' };
    if ($format)
    {
        $parser_parameters->{'format'} = $format;
    }
    # create parser object
    my $parser = new GO::Parser($parser_parameters);
    # parse file into objects
    foreach my $obo_file_path (@go_obo_files)
    {
        PrintDebug("Parsing OBO file '$obo_file_path'...");
        $parser->parse($obo_file_path);
        PrintDebug("...parsing done.");
    }
    # get L<GO::Model::Graph> object
    return $parser->handler->graph;
}




=pod

=head2 GenerateGOBrowserTemplate

B<Description>: .

B<ArgsCount>: 1

=over 4

=item $node: ()

the GO terms to group.

=back

B<Return>: (list)

.

=cut

sub GenerateGOBrowserTemplate
{
    my $go_browser_template_file = GP('TEMPLATES_PATH') . '/' . $GO_BROWSER_TEMPLATE;
    PrintDebug("Generate GO Browser template file ($go_browser_template_file)");

    # cache all database GO objects
    my @go_terms = Greenphyl::GO->new(GetDatabaseHandler());
    # get root terms
    my $root_terms = [
        Greenphyl::GO->new(GetDatabaseHandler(), {'selectors' => {'code' => 'GO:0008150'}}), # biological_process
        Greenphyl::GO->new(GetDatabaseHandler(), {'selectors' => {'code' => 'GO:0005575'}}), # cellular_component
        Greenphyl::GO->new(GetDatabaseHandler(), {'selectors' => {'code' => 'GO:0003674'}}), # molecular_function
    ];

    my $max_depth = GetParameterValues('maxdepth');

    my $go_browser_content = Greenphyl::Web::Templates::ProcessPage(
        {
            'title'     => 'Gene Family Ontology Browser',
            'content'   => 'go_tools/go_browser.tt',
            'go_roots'  => $root_terms,
            'max_depth' => $max_depth,
        },
    );

    PrintDebug("Storing GO Browser template file ($go_browser_template_file)");
    my $go_template_fh;
    if (open($go_template_fh, ">$go_browser_template_file" ))
    {
        # chmod(0666, $go_browser_template_file) or confess $!;
        print {$go_template_fh} $go_browser_content;
        close($go_template_fh);
    }
    else
    {
        Throw('error' => 'Unable to generate GO Browser template! An unexpected error occurred!', 'log' => "Error generating GO Browser template file: $!");
    }
}


=pod

=head2 DoGOBrowserTemplateNeedsUpdate

B<Description>: .

B<ArgsCount>: 1

=over 4

=item $node: ()

the GO terms to group.

=back

B<Return>: (list)

.

=cut

sub DoGOBrowserTemplateNeedsUpdate
{
    if (GetParameterValues('nocache'))
    {
        return 1;
    }

    my $go_browser_template_file = GP('TEMPLATES_PATH') . '/' . $GO_BROWSER_TEMPLATE;
    if (!-e $go_browser_template_file)
    {
        return 1;
    }
    return 0;
}




=pod

=head2 GetGOFamilies

B<Description>: .

B<ArgsCount>: 1

=over 4

=item $node: ()

the GO terms to group.

=back

B<Return>: (list)

.

=cut

sub GetGOFamilies
{
    my $go = LoadGO();

    my $parameters = Greenphyl::Tools::Families::GetFamilyListParameters();

    my $families;
    my @levels = GetParameterValues('level');
    if (@levels)
    {
        $families = $go->fetchFamilies({'selectors' => {'level' => ['IN', @levels],} });
    }
    else
    {
        $families = $go->families();
        # add level if missing
        if ($parameters->{'columns'})
        {
            if (!grep('level', @{$parameters->{'columns'}}))
            {
                push(@{$parameters->{'columns'}}, 'level');
            }
        }
        else
        {
            $parameters->{'columns'} = ['level',];
        }
    }

    my $go_percent_threshold = 1;
    
    $parameters->{'families'} = $families;
    $parameters->{'go'} = $go;
    $parameters->{'additional'} = {
        'columns' => [
            {
                'label' => 'Gene Ontology (>=' . $go_percent_threshold . '%)',
                'member' => 'current_object.go',
                'template' => 'go/go_list.tt',
                'template_parameters' => {
                    'static' => {
                        'with_family_info' => 1,
                        'highlight' => $go->code,
                        'go_percent_threshold' => $go_percent_threshold,
                    },
                    'dynamic' => {
                        'go_list' => 'current_object.go',
                    },
                },
                'style' => 'item-list',
            },
            {
                'label' => 'GO Distribution',
                'member' => 'current_object',
                'template' => 'go_tools/family_go_distribution_link.tt',
                'template_parameters' => {
                    'static' =>{
                        'with_family_info' => 1,
                    },
                    'dynamic' =>{
                        'family' => 'current_object',
                    },
                },
            },
        ],
    };

    return ($families, $parameters);
}


=pod

=head2 GenerateGOFamilyRelationshipJSONData

B<Description>: Generate GO family relationship JSON data.

B<ArgsCount>: 1-2

=over 4

=item $family: (Greenphyl::Family) (R)

Current family object.

=item $parent_family: (Greenphyl::Family) (O)

The parent family object if one.

=back

B<Return>: (string)

A JSON string containing an array of hash containing the following keys:
-'id': a family database ID
-'accession': current family accession
-'children': an array ref containing hash refs similar to the current one
-'count': number of sequences related to the GO in current family
-'common': a hash which keys are family_id and values are hash:
  -'count': total number of common sequences related to the GO;
-'selector': a CSS selector that can identify a corresponding DOM object;

=cut

sub GenerateGOFamilyRelationshipJSONData
{
    my ($go_list, $family_list) = @_;

    $family_list ||= [];
    PrintDebug('Generating JSON relationship for GOs [' . join(', ', @$go_list) . '] and families [' . join(', ', @$family_list) . ']');
    
    # get families related to given families (self, ancestors and descendents)
    my %families;
    foreach my $family (@$family_list)
    {
        $families{$family->id} = $family;
        foreach my $related (@{$family->getRelatedFamilies()})
        {
            $families{$related->id} = $related;
        }
    }
    PrintDebug('Only take in account families: ' . join(', ', values(%families)));


    my %go_id_to_go;
    my @go_ids = map { $go_id_to_go{$_->id} = $_; $_->id; } @$go_list;

    my $dbh = GetDatabaseHandler();
    my $family_selection_statement = '';
    if (%families)
    {
        $family_selection_statement = 'AND family_id IN (?' . (', ?' x (scalar(keys(%families))-1)) . ')';
    }
    my $sql_query = "
        SELECT family_id, go_id, percent
        FROM families_go_cache
        WHERE
            go_id IN (?" . (', ?' x $#go_ids) . ")
            $family_selection_statement
        ;";
    PrintDebug("SQL QUERY: $sql_query\nBindings: " . join(', ', @go_ids, keys(%families)));
    my $family_go_percents_raw = $dbh->selectall_arrayref($sql_query, undef, @go_ids, keys(%families))
        or confess $dbh->errstr;
    my $family_go_percents = {};
    foreach my $family_go_percent (@$family_go_percents_raw)
    {
        # 0 => family_id
        # 1 => go_id
        # 2 => percent
        $family_go_percents->{$family_go_percent->[0]} ||= {};
        $family_go_percents->{$family_go_percent->[0]}->{$family_go_percent->[1]} = $family_go_percent->[2];
    }
    
    

    # get families and sequences related to the GO
    $sql_query = '
        SELECT
            fs.sequence_id AS "sequence_id",
            gsc.go_id AS "go_id",
            fs.family_id AS "family_id",
            gsc.ipr_id AS "ipr_id",
            gi.evidence_code AS "ipr_evidence_code",
            gsc.uniprot_id AS "uniprot_id",
            gu.evidence_code AS "uniprot_evidence_code"
        FROM families_sequences fs
            JOIN go_sequences_cache gsc ON (fs.sequence_id = gsc.sequence_id)
            LEFT JOIN go_ipr gi ON (gi.ipr_id = gsc.ipr_id AND gi.go_id = gsc.go_id)
            LEFT JOIN go_uniprot gu ON (gu.dbxref_id = gsc.uniprot_id AND gu.go_id = gsc.go_id)
        WHERE
            gsc.go_id IN (?' . (', ?' x $#go_ids) . ')
    ;';
    PrintDebug("SQL QUERY: $sql_query\nBindings: " . join(', ', @go_ids));
    my $sequence_family_for_gos = $dbh->selectall_arrayref($sql_query, undef, @go_ids)
        or confess $dbh->errstr;

    # store family to go and to sequences association in a hash of hash of hash
    # structure:
    # {
    #     <family_id> =>
    #         {
    #             'go' =>
    #                 {
    #                     <go_id> =>
    #                         {
    #                             'sequences' =>
    #                                 {
    #                                     <sequence_id> => 1,
    #                                     <sequence_id> => 1,
    #                                     ...
    #                                 },
    #                             'sources' =>
    #                                 {
    #                                     'ipr' => <ipr_source_count>,
    #                                     'uniprot' => <uniprot_source_count>,
    #                                     'evidence_codes' =>
    #                                         {
    #                                             <evidence_code> => <sequence_count>,
    #                                             ...
    #                                         },
    #                                 },
    #                         },
    #                     <go_id> =>
    #                         ...
    #                 },
    #             'sequences' =>
    #                 {
    #                     <sequence_id> => <go_count>,
    #                     <sequence_id> => <go_count>,
    #                     ...
    #                 },
    #         },
    #     <family_id> =>
    #         ...
    #     ...
    # }
    my $family_to_go_and_sequences = {};
    # sequence to families associations (keys=sequence_id, values=array of family_id)
    my $sequence_to_families = {};
    foreach my $sequence_family (@$sequence_family_for_gos)
    {
        # 0 => sequence_id
        # 1 => go_id
        # 2 => family_id
        # 3 => ipr_id
        # 4 => ipr_evidence_code
        # 5 => uniprot_id
        # 6 => uniprot_evidence_code
        # only keep families of interest
        if (!%families || exists($families{$sequence_family->[2]}))
        {
            PrintDebug("Family " . $sequence_family->[2] . " has expected GO");
            # dedicace Mathieu: several orchish manoeuvres ;)
            $family_to_go_and_sequences->{$sequence_family->[2]} ||= {
                'go' => {}, 
                'sequences' => {},
            };
            # GO data
            $family_to_go_and_sequences->{$sequence_family->[2]}->{'go'}->{$sequence_family->[1]} ||= {
                'sequences' => {},
                'sources' => {
                    'ipr' => 0,
                    'uniprot' => 0,
                    'evidence_codes' => {},
                },
            };
            $family_to_go_and_sequences->{$sequence_family->[2]}->{'go'}->{$sequence_family->[1]}->{'sequences'}->{$sequence_family->[0]} = 1;
            # GO sources
            # -IPR
            if ($sequence_family->[3])
            {
                $family_to_go_and_sequences->{$sequence_family->[2]}->{'go'}->{$sequence_family->[1]}->{'sources'}->{'ipr'}++;
                $family_to_go_and_sequences->{$sequence_family->[2]}->{'go'}->{$sequence_family->[1]}->{'sources'}->{'evidence_codes'}->{$sequence_family->[4]} ||= 0;
                $family_to_go_and_sequences->{$sequence_family->[2]}->{'go'}->{$sequence_family->[1]}->{'sources'}->{'evidence_codes'}->{$sequence_family->[4]}++;
            }
            # -UniProt
            if ($sequence_family->[5])
            {
                $family_to_go_and_sequences->{$sequence_family->[2]}->{'go'}->{$sequence_family->[1]}->{'sources'}->{'uniprot'}++;
                $family_to_go_and_sequences->{$sequence_family->[2]}->{'go'}->{$sequence_family->[1]}->{'sources'}->{'evidence_codes'}->{$sequence_family->[6]} ||= 0;
                $family_to_go_and_sequences->{$sequence_family->[2]}->{'go'}->{$sequence_family->[1]}->{'sources'}->{'evidence_codes'}->{$sequence_family->[6]}++;
            }
            # count sequence GO
            $family_to_go_and_sequences->{$sequence_family->[2]}->{'sequences'}->{$sequence_family->[0]} ||= 0;
            $family_to_go_and_sequences->{$sequence_family->[2]}->{'sequences'}->{$sequence_family->[0]}++;
            # store sequence to families association
            $sequence_to_families->{$sequence_family->[0]} ||= [];
            push(@{$sequence_to_families->{$sequence_family->[0]}}, $sequence_family->[2]);
        }
    }

    # get all related families associated to a GO
    my @families;
    my @family_ids = keys(%$family_to_go_and_sequences);
    if (%families)
    {
        foreach my $family_id (@family_ids)
        {
            push(@families, $families{$family_id});
        }
        @families = sort {$a->level <=> $b->level || $a->id <=> $b->id} @families;
    }
    elsif (@family_ids)
    {
        # get all related families (by ascending level as we will init lower levels first)
        push(
            @families,
            Greenphyl::Family->new(
                $dbh,
                {
                    'selectors' => {'id' => ['IN', @family_ids]},
                    'sql' => {'ORDER BY' => 'level ASC'},
                }
            )
        );
    }
    PrintDebug('Generating stats on ' . scalar(@families) . ' families.');

    # tree structure that contains root families (with their children)
    my $tree_structure = [];
    my $family_to_family_go_data = {};
    foreach my $family (@families)
    {
        my $family_go_data = $family_to_family_go_data->{$family->id} =
            {
                'id'        => $family->id,
                'label'     => $family->accession,
                'type'      => 'family-go',
                # count sequences with at least one of the selected GO
                'count'     => scalar(keys(%{$family_to_go_and_sequences->{$family->id}->{'sequences'}})),
                'gos'       => { }, # counts and percent by GO (filled below)
                'total_count' => $family->seq_count, # total number of sequences in family
                'common'    => {}, # common sequence counts by family (filled below)
                'children'  => [], # array of child family GO data structures (filled below by children)
                'selector'  => '.family-' . $family->accession,
            }
        ;
        # root family?
        if (1 == $family->level)
        {
            push(@$tree_structure, $family_go_data);
        }

        # percent by GO
        my $go_id;
        foreach $go_id (keys(%{$family_go_percents->{$family->id}}))
        {
            $family_go_data->{'gos'}->{$go_id_to_go{$go_id}}->{'percent'} = $family_go_percents->{$family->id}->{$go_id};
        }
        # counts and sources by GO
        foreach $go_id (keys(%{$family_to_go_and_sequences->{$family->id}->{'go'}}))
        {
            $family_go_data->{'gos'}->{$go_id_to_go{$go_id}}->{'count'} = scalar(keys(%{$family_to_go_and_sequences->{$family->id}->{'go'}->{$go_id}->{'sequences'}}));
            $family_go_data->{'gos'}->{$go_id_to_go{$go_id}}->{'ipr'} = $family_to_go_and_sequences->{$family->id}->{'go'}->{$go_id}->{'sources'}->{'ipr'};
            $family_go_data->{'gos'}->{$go_id_to_go{$go_id}}->{'uniprot'} = $family_to_go_and_sequences->{$family->id}->{'go'}->{$go_id}->{'sources'}->{'uniprot'};
            # evidence codes
            $family_go_data->{'gos'}->{$go_id_to_go{$go_id}}->{'evidence_codes'} = $family_to_go_and_sequences->{$family->id}->{'go'}->{$go_id}->{'sources'}->{'evidence_codes'};
        }

        # extra temporary hash to keep track of children already added
        my $parent_to_child = {}; # keys are parent ID, values are has which keys are child ID
        # extra temporary hash to keep track of sequences alread counted
        my $processed_sequences = {}; # keys are sequence ID
        # loop on sequences to get stats
        foreach my $sequence_id (keys(%{$family_to_go_and_sequences->{$family->id}->{'sequences'}}))
        {
            # loop on each common family
            foreach my $common_family_id (@{$sequence_to_families->{$sequence_id}})
            {
                # load family object from cache
                my $common_family = Greenphyl::Family->new($dbh, { 'selectors' => {'id' => $common_family_id} });
                # check level
                if ($common_family->level == $family->level - 1)
                {
                    # common family is a parent
                    # make sure it has not already been added
                    $parent_to_child->{$common_family_id} ||= {};
                    if (!exists($parent_to_child->{$common_family_id}->{$family->id}))
                    {
                        # add current family to parent's children list
                        push(@{$family_to_family_go_data->{$common_family_id}->{'children'}}, $family_go_data);
                        $parent_to_child->{$common_family_id}->{$family->id} = 1;
                    }
                    # make sure we did not already count the common sequence
                    $processed_sequences->{$common_family_id} ||= {};
                    if (!exists($processed_sequences->{$common_family_id}->{$sequence_id}))
                    {
                        # fill stats about sequences
                        $family_go_data->{'common'}->{$common_family_id} ||= {'count' => 0,};
                        $family_go_data->{'common'}->{$common_family_id}->{'count'}++;
                        $processed_sequences->{$common_family_id}->{$sequence_id} = 1;
                    }
                }
                elsif ($common_family->level == $family->level + 1)
                {
                    # common family is a children
                    # make sure we did not already count the common sequence
                    $processed_sequences->{$common_family_id} ||= {};
                    if (!exists($processed_sequences->{$common_family_id}->{$sequence_id}))
                    {
                        # fill stats about sequences
                        $family_go_data->{'common'}->{$common_family_id} ||= {'count' => 0,};
                        $family_go_data->{'common'}->{$common_family_id}->{'count'}++;
                        $processed_sequences->{$common_family_id}->{$sequence_id} = 1;
                    }
                }
                # otherwise, nothing to do
            }
        }
    }

    my $json = to_json($tree_structure);

    return {'JSON' => $json,};
}


=pod

=head2 GetGOSubtree

B<Description>: .

B<ArgsCount>: 1

=over 4

=item $node: ()

the GO terms to group.

=back

B<Return>: (list)

.

Example of expected JSON structure:

[
	{
		"text": "1. Pre Lunch (120 min)",
		"expanded": true,
		"classes": "important",
		"children":
		[
			{
				"text": "1.1 The State of the Powerdome (30 min)"
			},
		 	{
				"text": "1.2 The Future of jQuery (30 min)"
			},
		 	{
				"text": "1.2 jQuery UI - A step to richnessy (60 min)"
			}
		]
	},
	{
		"text": "2. Lunch  (60 min)"
	},
	{
		"text": "3. After Lunch  (120+ min)",
		"children":
		[
			{
				"text": "3.1 jQuery Calendar Success Story (20 min)"
			},
		 	{
				"text": "3.2 jQuery and Ruby Web Frameworks (20 min)"
			},
		 	{
				"text": "3.3 Hey, I Can Do That! (20 min)"
			},
		 	{
				"text": "3.4 Taconite and Form (20 min)"
			},
		 	{
				"text": "3.5 Server-side JavaScript with jQuery and AOLserver (20 min)"
			},
		 	{
				"text": "3.6 The Onion: How to add features without adding features (20 min)",
				"id": "36",
				"hasChildren": true
			},
		 	{
				"text": "3.7 Visualizations with JavaScript and Canvas (20 min)"
			},
		 	{
				"text": "3.8 ActiveDOM (20 min)"
			},
		 	{
				"text": "3.8 Growing jQuery (20 min)"
			}
		]
	}
]

=cut

sub GetGOSubtree
{
    my ($go, $max_depth) = @_;

    # check tree max depth
    if (!defined($max_depth))
    {
        $max_depth = GetParameterValues('max_depth');
    }
    if ($max_depth !~ m/^\d+$/)
    {
        $max_depth = 0;
    }

    my $go_data = [];

    foreach my $go_child (@{$go->children()})
    {
        # generate HTML for child
        my $go_child_label = Greenphyl::Web::Templates::ProcessTemplate(
            'go_tools/go_tree_node_label.tt',
            {
                'go'        => $go_child,
                'go_parent' => $go,
            },
        );

        my $child_data = {};
        $child_data->{'text'} = $go_child_label;

        # check for sub-children
        my $underlying_family_count = $go_child->getUnderlyingFamilyCount({'selectors' => {'families.level' => 1}});
        if ($underlying_family_count)
        {
            if (0 < $max_depth)
            {
                # load children
                $child_data->{'children'} = GetGOSubtree($go_child, $max_depth-1);
            }
            else
            {
              $child_data->{'hasChildren'} = 'true';
            }
        }

        if ($go_child->countFamilies({'selectors' => {'families.level' => 1}})
            || $underlying_family_count)
        {
            push(@$go_data, $child_data);
        }
    }

    return $go_data;
}


=pod

=head2 GetGOJSONSubtree

B<Description>: .

B<ArgsCount>: 1

=over 4

=item $node: ()

the GO terms to group.

=back

B<Return>: (list)

.

=cut

sub GetGOJSONSubtree
{
    my $go = LoadGO();

    return to_json(GetGOSubtree($go));
}




=pod

=head1 AUTHORS

Mathieu ROUARD (Bioversity), m.rouard@cgiar.org

Valentin GUIGNON (Bioversity), v.guignon@cgiar.org

Matthieu CONTE (Syngenta)

Sebastien BRIOIS (Syngenta)

=head1 VERSION

Version 1.0.0

Date 29/10/2012

=head1 SEE ALSO

GreenPhyl documentation.

=cut

return 1; # package return
