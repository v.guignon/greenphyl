=pod

=head1 NAME

Greenphyl::Tools::AnalysisProcesses - Utilities to manage processes.

=head1 SYNOPSIS

    use Greenphyl::Tools::AnalysisProcesses;
    ...
    my $status = GetFamilyStatusForProcess($family);
    SetFamilyStatusForProcess($family, 'done');

    SetFamiliesStatusForProcess('done');

    my @steps = GetFamilyStepsForProcess($family);
    RemoveFamilyStepsForProcess($family, 'phylogeny');
    AddFamilyStepsForProcess($family, 'phylogeny');

    RemoveFamiliesStepsForProcess('phylogeny');
    AddFamiliesStepsForProcess('phylogeny');

=head1 REQUIRES

Perl5

=head1 EXPORTS

GetProcessParameter GetCurrentProcess
GetFamilyStatusForProcess SetFamilyStatusForProcess SetFamiliesStatusForProcess
GetFamilyStepsForProcess GetFamilyPerformedSteps
RemoveFamilyStepsForProcess SetFamilyStepsDoneForProcess AddFamilyStepsForProcess
RemoveFamiliesStepsForProcess SetFamiliesStepsDoneForProcess AddFamiliesStepsForProcess
AddProcessDuration

=head1 DESCRIPTION

Provides tools to manage family processing.

=cut

package Greenphyl::Tools::AnalysisProcesses;


use strict;
use warnings;

use Carp qw(cluck confess croak);

use Greenphyl;
use Greenphyl::AnalysisProcess;




# Package constants
####################

=pod

=head1 CONSTANTS

B<$DEBUG>: (boolean)

Enables/disables local debug mode.

B<$FAMILY_STATUS_PENDING>: (string)

Status value used in database for families that should be processed.

B<$FAMILY_STATUS_IN_PROGRESS>: (string)

Status value used in database for families that are currently being processed.

B<$FAMILY_STATUS_DONE>: (string)

Status value used in database for families that have been processed.

B<$FAMILY_STATUS_CANCELED>: (string)

Status value used in database for families that do not need to be processed
anymore.

B<$FAMILY_STATUS_ERROR>: (string)

Status value used in database for families that couldn't be processed because
an error occurred.

B<$FAMILY_STATUS_WARNING>: (string)

Status value used in database for families that have been processed but with
warning(s).

B<%FAMILY_STATUS>: (hash)

Keys are available status codes for family processing.

=cut

our $DEBUG = 0;
our $FAMILY_STATUS_PENDING     = 'pending';
our $FAMILY_STATUS_IN_PROGRESS = 'in progress';
our $FAMILY_STATUS_DONE        = 'done';
our $FAMILY_STATUS_CANCELED    = 'canceled';
our $FAMILY_STATUS_ERROR       = 'error';
our $FAMILY_STATUS_WARNING     = 'warning';
our %FAMILY_STATUS = (
    $FAMILY_STATUS_PENDING     =>
        {
            'description' => 'Family has to be processed',
            'is_end'      => 0,
        },
    $FAMILY_STATUS_IN_PROGRESS =>
        {
            'description' => 'Family is currently being processed',
            'is_end'      => 0,
        },
    $FAMILY_STATUS_DONE =>
        {
            'description' => 'Family has been processed with success',
            'is_end'      => 1,
        },
    $FAMILY_STATUS_CANCELED =>
        {
            'description' => 'Family analysis has been canceled',
            'is_end'      => 1,
        },
    $FAMILY_STATUS_ERROR =>
        {
            'description' => 'An error occurred while family was processed',
            'is_end'      => 1,
        },
    $FAMILY_STATUS_WARNING =>
        {
            'description' => 'Family has been processed but some warning occurred',
            'is_end'      => 1,
        },
);

#+FIXME: get them from DB directly?
our %FAMILY_STEPS = (
    'other' =>
        {
            'description' => 'other type of step',
            'field_value' => 'step_other',
        },
    'fasta' =>
        {
            'description' => 'FASTA file generation',
            'field_value' => 'step_fasta',
        },
    'hmm' =>
        {
            'description' => 'HMM Build',
            'field_value' => 'step_hmm',
        },
    'mafft' =>
        {
            'description' => 'MAFFT alignment',
            'field_value' => 'step_mafft',
        },
    'trimal' =>
        {
            'description' => 'Trimal alignment curation',
            'field_value' => 'step_trimal',
        },
    'phyml' =>
        {
            'description' => 'PhyML phylogeny analysis',
            'field_value' => 'step_phyml',
        },
    'rap' =>
        {
            'description' => 'RAP Green tree rooting and ortholog predictions',
            'field_value' => 'step_rap',
        },
    'web transfer' =>
        {
            'description' => 'Analysis files to transfer to the web site',
            'field_value' => 'step_to_web',
        },
    'statistics' =>
        {
            'description' => 'Statistics computation',
            'field_value' => 'step_stats',
        },
    'orthology' =>
        {
            'description' => 'Orthology prediction with OrthoMCL',
            'field_value' => 'step_',
        },
    'meme/mast' =>
        {
            'description' => 'MEME/MAST analysis',
            'field_value' => 'step_meme',
        },
    'plant specificity' =>
        {
            'description' => 'Plant-specificity prediction',
            'field_value' => 'step_plant_spe',
        },
    'taxonomy specificity' =>
        {
            'description' => 'Taxonomy specificity analysis',
            'field_value' => 'step_tax',
        },
);
our %FAMILY_STEP_FIELD_VALUE_TO_DB_VALUE = (
    'step_other'     => 'other',
    'step_fasta'     => 'fasta',
    'step_hmm'       => 'hmm',
    'step_mafft'     => 'mafft',
    'step_trimal'    => 'trimal',
    'step_phyml'     => 'phyml',
    'step_rap'       => 'rap',
    'step_to_web'    => 'web transfer',
    'step_stats'     => 'statistics',
    'step_'          => 'orthology',
    'step_meme'      => 'meme/mast',
    'step_plant_spe' => 'plant specificity',
    'step_tax'       => 'taxonomy specificity',
);

our $PROCESS_STATUS_TO_PERFORM  = 'to perform';
our $PROCESS_STATUS_IN_PROGRESS = 'in progress';
our $PROCESS_STATUS_PAUSED      = 'paused';
our $PROCESS_STATUS_STOPPED     = 'stopped';
our $PROCESS_STATUS_DONE        = 'done';
our $PROCESS_STATUS_CANCELED    = 'canceled';
our %PROCESS_STATUS = (
    $PROCESS_STATUS_TO_PERFORM  =>
        {
            'description' => 'Process still to be performed',
            'field_value' => 'process_to_perform',
            'is_end'      => 0,
        },
    $PROCESS_STATUS_IN_PROGRESS =>
        {
            'description' => 'Process currently started',
            'field_value' => 'process_in_progress',
            'is_end'      => 0,
        },
    $PROCESS_STATUS_PAUSED =>
        {
            'description' => 'Process has been manually halted',
            'field_value' => 'process_paused',
            'is_end'      => 0,
        },
    $PROCESS_STATUS_STOPPED =>
        {
            'description' => 'Process was stopped due to an error',
            'field_value' => 'process_stopped',
            'is_end'      => 1,
        },
    $PROCESS_STATUS_DONE =>
        {
            'description' => 'Process has terminated successfully',
            'field_value' => 'process_done',
            'is_end'      => 1,
        },
    $PROCESS_STATUS_CANCELED =>
        {
            'description' => 'Process has been canceled',
            'field_value' => 'process_canceled',
            'is_end'      => 1,
        },
);
our %PROCESS_STATUS_FIELD_VALUE_TO_DB_VALUE = (
    'process_to_perform'  => $PROCESS_STATUS_TO_PERFORM,
    'process_in_progress' => $PROCESS_STATUS_IN_PROGRESS,
    'process_paused'      => $PROCESS_STATUS_PAUSED,
    'process_stopped'     => $PROCESS_STATUS_STOPPED,
    'process_done'        => $PROCESS_STATUS_DONE,
    'process_canceled'    => $PROCESS_STATUS_CANCELED,
);


our $FAMILY_STEP_OTHER                = 'other';
our $FAMILY_STEP_FASTA                = 'fasta';
our $FAMILY_STEP_HMM                  = 'hmm';
our $FAMILY_STEP_MAFFT                = 'mafft';
our $FAMILY_STEP_TRIMAL               = 'trimal';
our $FAMILY_STEP_PHYML                = 'phyml';
our $FAMILY_STEP_RAP                  = 'rap';
our $FAMILY_STEP_WEB_TRANSFER         = 'web transfer';
our $FAMILY_STEP_STATISTICS           = 'statistics';
our $FAMILY_STEP_ORTHOLOGY            = 'orthology';
our $FAMILY_STEP_MEME_MAST            = 'meme/mast';
our $FAMILY_STEP_PLANT_SPECIFICITY    = 'plant specificity';
our $FAMILY_STEP_TAXONOMY_SPECIFICITY = 'taxonomy specificity';

our %FAMILY_PROCESSING_STEPS = (
    ''                                => 'undefined or unknown step to perform',
    $FAMILY_STEP_OTHER                => 'other custom step to perform',
    $FAMILY_STEP_FASTA                => 'FASTA file generation',
    $FAMILY_STEP_HMM                  => 'hmm build to perform',
    $FAMILY_STEP_MAFFT                => 'MAFFT alignment to generate',
    $FAMILY_STEP_TRIMAL               => 'Refine alignment',
    $FAMILY_STEP_PHYML                => 'Phyml phylogeny analysis',
    $FAMILY_STEP_RAP                  => 'RapGreen tree rooting/ortholog prediction',
    $FAMILY_STEP_WEB_TRANSFER         => 'Web file(s) to transfer',
    $FAMILY_STEP_STATISTICS           => 'Update family statistics in database',
    $FAMILY_STEP_ORTHOLOGY            => 'Orthology prediction with OrthoMCL',
    $FAMILY_STEP_MEME_MAST            => 'Meme/Mast analysis',
    $FAMILY_STEP_PLANT_SPECIFICITY    => 'Compute Plant specificity',
    $FAMILY_STEP_TAXONOMY_SPECIFICITY => 'Update taxonomy specificity',
);



# Package variables
####################

=pod

=head1 VARIABLES

B<$g_current_process>: (Greenphyl::AnalysisProcess)

Current process object.

=cut

my $g_current_process;
my $g_process_mode;




# Package subs
###############

=pod

=head1 FUNCTIONS

=head2 import

B<Description>: this function initialize the process management.

B<ArgsCount>: 0-1

=over 4

=item option: (string) (O)

This parameter defines how to handle current analysis process:

-nothing or 'w' or 'warn': user must provide a valid (existing) analysis process
   ID. If families not listed in the process list are processed, just a warning
  will be issued and thoses families will be added to the processed list.
-'r' or 'required': user must provide a valid (existing) analysis process ID. If
  families not listed in the process list are processed, an error will be
  raised (and if not catched, SQL transaction will be aborted).
-'a' or 'auto': user may provide a valid (existing) analysis process ID. If no
  process ID is provided, a new process is created and used. If families not
  listed in the process list are processed, they will be added to the processed
  list silently.
-'o' or 'optional': user may provide a valid (existing) analysis process ID. If
  no process ID is provided, no process will be created and processed families
  won't be tracked. If families not listed in the process list are processed,
  they will be added to the processed list silently.

Note: the module parameter can be overridden by the command line argument:
  "process_mode=<option>"

=back

B<Caller>: perl system (use, import)

B<Example>:

    use Greenphyl::Tools::AnalysisProcesses;
    use Greenphyl::Tools::AnalysisProcesses('required');
    use Greenphyl::Tools::AnalysisProcesses('auto');
    use Greenphyl::Tools::AnalysisProcesses('optional');

=cut

sub import
{
    # process module parameters
    # get from command line first
    my $process_mode = GetParameterValues('process_mode');

    # if not specified, use module parameters
    if (!$process_mode)
    {
        # skip package name
        if (@_ && $_[0] eq 'Greenphyl::Tools::AnalysisProcesses')
        {shift();}

        ($process_mode) = @_;
    }

    if (!$process_mode)
    {
        # default mode
        $g_process_mode = 'w';
    }
    elsif ($process_mode =~ m/^(|w|warn|r|required|a|auto|o|optional)$/i)
    {
        $process_mode = $1 || 'w'; # empty string case
        $g_process_mode = lc(substr($process_mode, 0, 1));
    }
    else
    {
        confess "ERROR: Invalid analysis process mode specified: '$process_mode'\n";
    }

    _InitCurrentProcess();
}


=pod

=head2 _InitCurrentProcess

B<Description>: Initialize current process and make sure everything is ok.

B<ArgsCount>: 0

B<Return>: nothing

B<Caller>: Internal

=cut

sub _InitCurrentProcess
{
    my $process_id = GetParameterValues('process_id');
    
    # check for regression tests
    if (!$process_id && $ENV{'TESTING'})
    {
        PrintDebug('Analysis Process ID retrieved from testing environment');
        $process_id = $ENV{'TEST_ANALYSIS_PROCESS_ID'};
    }

    # check if no process ID has been specified
    if (!$process_id)
    {
        if ($g_process_mode eq 'a')
        {
            # auto, create a new one
            $g_current_process = CreateAnalysisProcess({
                'name'       => $0,
                'comments'   => "Auto-created for $0 (PID=$$)",
                'status'     => $PROCESS_STATUS_IN_PROGRESS,
                'start_date' => time(),
            });
        }
        elsif ($g_process_mode eq 'o')
        {
            # ignore process support
            $process_id = -1;
        }
        else
        {
            $process_id = 0;
        }
    }

    $g_current_process ||= Greenphyl::AnalysisProcess->new(
        GetDatabaseHandler(),
        {'selectors' => {'id' => $process_id,} }
    );
    
    my $process_error_message = <<"___160_PROCESS_ERROR_MESSAGE___";
You must specify a valid process identifier using the syntax 'process_id=...'!
Available process identifiers can be found using the administrative web
interface or by using the SQL query "SELECT * FROM processes;".

If you do not want to use GreenPhyl analysis process tracking, then specify '-1'
as the process identifier: 'process_id=-1' or use the 'optional' processing mode
'process_mode=o' (mode currently set: '$g_process_mode').

___160_PROCESS_ERROR_MESSAGE___

    if (!$process_id
        || ((0 < $process_id) && !$g_current_process))
    {
        Throw('error' => $process_error_message);
    }
    
    if ($g_current_process)
    {
        warn "Note: Working on process '" . $g_current_process->name . "' (" . $g_current_process->id . ")\n";
        if ($PROCESS_STATUS_DONE eq $g_current_process->status)
        {
            warn "WARNING: Process was supposed to be done.\n";
        }
        # check if start date has been set
        if (!$g_current_process->start_date)
        {
            $g_current_process->start_date('CURRENT_TIMESTAMP');
        }
        $g_current_process->save();
    }
}


=pod

=head2 [subName] #+++

B<Description>: #+++

B<ArgsCount>: #+++

=over 4

=item $arg: #+++

=back

B<Return>: #+++

B<Caller>: #+++

B<Exception>: #+++

B<Example>: #+++

=cut

sub GetProcessParameter
{
    my $command_line_arguments = "process_id=-1";
    if ($g_current_process)
    {
        $command_line_arguments = "process_id=" . $g_current_process->id;
    }
    
    $command_line_arguments .= " process_mode=$g_process_mode";
    
    return $command_line_arguments;
}
# export
*main::GetProcessParameter = \&GetProcessParameter;


=pod

=head2 [subName] #+++

B<Description>: #+++

B<ArgsCount>: #+++

=over 4

=item $arg: #+++

=back

B<Return>: #+++

B<Caller>: #+++

B<Exception>: #+++

B<Example>: #+++

=cut

sub GetCurrentProcess
{
    return $g_current_process;
}
# export
*main::GetCurrentProcess = \&GetCurrentProcess;


=pod

=head2 [subName] #+++

B<Description>: #+++

B<ArgsCount>: #+++

=over 4

=item $arg: #+++

=back

B<Return>: #+++

B<Caller>: #+++

B<Exception>: #+++

B<Example>: #+++

=cut

sub GetFamilyStatusForProcess
{
    my ($family, $process) = @_;
    $process ||= $g_current_process;
    my $status;
    if ($process)
    {
        if (!$family)
        {
            confess "No family provided!\n";
        }
        my $sql_query = "
            SELECT status
            FROM family_processing
            WHERE family_id = " . $family->id . "
                AND process_id = " . $process->id . "
        ";
        PrintDebug("SQL QUERY: $sql_query");
        ($status) = GetDatabaseHandler()->selectrow_array($sql_query);
    }

    return $status;
}
# export
*main::GetFamilyStatusForProcess = \&GetFamilyStatusForProcess;



=pod

=head2 [subName] #+++

B<Description>: #+++

B<ArgsCount>: #+++

=over 4

=item $arg: #+++

=back

B<Return>: #+++

B<Caller>: #+++

B<Exception>: #+++

B<Example>: #+++

=cut

sub SetFamilyStatusForProcess
{
    my ($family, $status, $process) = @_;
    $process ||= $g_current_process;
    if ($process)
    {
        if (!$family)
        {
            confess "No family provided!\n";
        }
        my $sql_query = "
            UPDATE family_processing
            SET status = ?
            WHERE family_id = " . $family->id . "
                AND process_id = " . $process->id . "
        ";
        PrintDebug("SQL QUERY: $sql_query\nBindings: $status");
        my $update_count = GetDatabaseHandler()->do($sql_query, undef, $status); 
        if (!$update_count)
        {
            confess "ERROR: Failed to change family $family status (process $process): " . GetDatabaseHandler()->errstr;
        }
        # check if family was in the process list
        if (0 == $update_count)
        {
            if ($g_process_mode eq 'r')
            {
                confess "ERROR: Family $family has been processed but was not supposed to be processed (process $process and process_mode=r)!\n";
            }
            elsif ($g_process_mode eq 'w')
            {
                cluck "WARNING: Family $family has been processed but was not supposed to be processed (process $process)!";
            }
            # add family to the list
            AddFamiliesToProcess([$family], $process, '', $status);
        }
    }
}
# export
*main::SetFamilyStatusForProcess = \&SetFamilyStatusForProcess;


=pod

=head2 [subName] #+++

B<Description>: #+++

B<ArgsCount>: #+++

=over 4

=item $arg: #+++

=back

B<Return>: #+++

B<Caller>: #+++

B<Exception>: #+++

B<Example>: #+++

=cut

sub SetFamiliesStatusForProcess
{
    my ($status, $process) = @_;
    $process ||= $g_current_process;
    if ($process)
    {
        if (ref($status))
        {
            # in case a family has been provided by error
            confess "Invalid argument!\n";
        }
        my $sql_query = "
            UPDATE family_processing
            SET status = ?
            WHERE process_id = " . $process->id . "
        ";
        PrintDebug("SQL QUERY: $sql_query\nBindings: $status");
        if (!GetDatabaseHandler()->do($sql_query, undef, $status))
        {
            confess "ERROR: Failed to change families status (process $process): " . GetDatabaseHandler()->errstr;
        }
    }
}
# export
*main::SetFamiliesStatusForProcess = \&SetFamiliesStatusForProcess;


=pod

=head2 [subName] #+++

B<Description>: #+++

B<ArgsCount>: #+++

=over 4

=item $arg: #+++

=back

B<Return>: #+++

B<Caller>: #+++

B<Exception>: #+++

B<Example>: #+++

=cut

sub GetFamilyStepsForProcess
{
    my ($process) = @_;
    $process ||= $g_current_process;

    my @steps;
    if ($process)
    {
        my ($family) = @_;
        if (!$family)
        {
            confess "No family provided!\n";
        }
        my $sql_query = "
            SELECT steps_to_perform
            FROM family_processing
            WHERE family_id = " . $family->id . "
                AND process_id = " . $process->id . "
        ";
        PrintDebug("SQL QUERY: $sql_query");
        @steps = split(',', GetDatabaseHandler()->selectrow_array($sql_query));
    }

    return @steps;
}
# export
*main::GetFamilyStepsForProcess = \&GetFamilyStepsForProcess;


=pod

=head2 [subName] #+++

B<Description>: #+++

B<ArgsCount>: #+++

=over 4

=item $arg: #+++

=back

B<Return>: #+++

B<Caller>: #+++

B<Exception>: #+++

B<Example>: #+++

=cut

sub GetFamilyPerformedSteps
{
    my ($process) = @_;
    $process ||= $g_current_process;

    my @steps;
    if ($process)
    {
        my ($family) = @_;
        if (!$family)
        {
            confess "No family provided!\n";
        }
        my $sql_query = "
            SELECT steps_performed
            FROM family_processing
            WHERE family_id = " . $family->id . "
                AND process_id = " . $process->id . "
        ";
        PrintDebug("SQL QUERY: $sql_query");
        @steps = split(',', GetDatabaseHandler()->selectrow_array($sql_query));
    }

    return @steps;
}
# export
*main::GetFamilyPerformedSteps = \&GetFamilyPerformedSteps;


=pod

=head2 [subName] #+++

B<Description>: #+++

B<ArgsCount>: #+++

=over 4

=item $arg: #+++

=back

B<Return>: #+++

B<Caller>: #+++

B<Exception>: #+++

B<Example>: #+++

=cut

sub RemoveFamilyStepsForProcess
{
    my ($family, $steps, $process) = @_;
    $process ||= $g_current_process;

    if ($process)
    {
        if (!$family)
        {
            confess "No family provided!\n";
        }
        
        if (!ref($steps))
        {
            $steps = [$steps];
        }
        
        foreach my $step (@$steps)
        {
            my $sql_query = "
                UPDATE family_processing
                SET steps_to_perform = REPLACE(steps_to_perform, ?, '')
                WHERE family_id = " . $family->id . "
                    AND process_id = " . $process->id . "
            ";
            PrintDebug("SQL QUERY: $sql_query\nBindings: $step");
            my $update_count = GetDatabaseHandler()->do($sql_query, undef, $step);
            if (!$update_count)
            {
                confess "ERROR: Failed to remove family $family steps (process $process): " . GetDatabaseHandler()->errstr;
            }
            # check if family was in the process list
            if (0 == $update_count)
            {
                if ($g_process_mode eq 'r')
                {
                    confess "ERROR: Family $family has been processed but was not supposed to be processed (process $process and process_mode=r)!\n";
                }
                elsif ($g_process_mode eq 'w')
                {
                    cluck "WARNING: Family $family has been processed but was not supposed to be processed (process $process)!";
                }
                # add family to the list
                AddFamiliesToProcess([$family], $process, '', $FAMILY_STATUS_IN_PROGRESS);
            }
        }
    }
}
# export
*main::RemoveFamilyStepsForProcess = \&RemoveFamilyStepsForProcess;


=pod

=head2 [subName] #+++

B<Description>: #+++

B<ArgsCount>: #+++

=over 4

=item $arg: #+++

=back

B<Return>: #+++

B<Caller>: #+++

B<Exception>: #+++

B<Example>: #+++

=cut

sub SetFamilyStepsDoneForProcess
{
    my ($family, $steps, $process) = @_;
    $process ||= $g_current_process;

    if ($process)
    {
        if (!$family)
        {
            confess "No family provided!\n";
        }
        
        if (!ref($steps))
        {
            $steps = [$steps];
        }

        foreach my $step (@$steps)
        {
            my $sql_query = "
                UPDATE family_processing
                SET steps_to_perform = REPLACE(steps_to_perform, ?, ''),
                    steps_performed  = CONCAT_WS(',',steps_performed,?)
                WHERE family_id = " . $family->id . "
                    AND process_id = " . $process->id . "
            ";
            PrintDebug("SQL QUERY: $sql_query\nBindings: $step");
            my $update_count = GetDatabaseHandler()->do($sql_query, undef, $step, $step);
            if (!$update_count)
            {
                confess "ERROR: Failed to set family $family steps done (process $process): " . GetDatabaseHandler()->errstr;
            }
            # check if family was in the process list
            if (0 == $update_count)
            {
                if ($g_process_mode eq 'r')
                {
                    confess "ERROR: Family $family has been processed but was not supposed to be processed (process $process and process_mode=r)!\n";
                }
                elsif ($g_process_mode eq 'w')
                {
                    cluck "WARNING: Family $family has been processed but was not supposed to be processed (process $process)!";
                }
                # add family to the list
                AddFamiliesToProcess([$family], $process, '', $FAMILY_STATUS_IN_PROGRESS);
            }
        }
    }
}
# export
*main::SetFamilyStepsDoneForProcess = \&SetFamilyStepsDoneForProcess;


=pod

=head2 [subName] #+++

B<Description>: #+++

B<ArgsCount>: #+++

=over 4

=item $arg: #+++

=back

B<Return>: #+++

B<Caller>: #+++

B<Exception>: #+++

B<Example>: #+++

=cut

sub AddFamilyStepsForProcess
{
    my ($family, $steps, $process) = @_;
    $process ||= $g_current_process;

    if ($process)
    {
        if (!$family)
        {
            confess "No family provided!\n";
        }
        
        if ('ARRAY' eq ref($steps))
        {
            $steps = join(',', @$steps);
        }
        
        my $sql_query = "
            UPDATE family_processing
            SET steps_to_perform = CONCAT_WS(',',steps_to_perform,?)
            WHERE family_id = " . $family->id . "
                AND process_id = " . $process->id . "
        ";
        PrintDebug("SQL QUERY: $sql_query\nBindings: $steps");
        my $update_count = GetDatabaseHandler()->do($sql_query, undef, $steps);
        if (!$update_count)
        {
            confess "ERROR: Failed to add step '$steps' family $family (process $process): " . GetDatabaseHandler()->errstr;
        }
        # check if family was in the process list
        if (0 == $update_count)
        {
            if ($g_process_mode eq 'r')
            {
                confess "ERROR: Family $family has been processed but was not supposed to be processed (process $process and process_mode=r)!\n";
            }
            elsif ($g_process_mode eq 'w')
            {
                cluck "WARNING: Family $family has been processed but was not supposed to be processed (process $process)!";
            }
            # add family to the list
            AddFamiliesToProcess([$family], $process, $steps, $FAMILY_STATUS_IN_PROGRESS);
        }
    }
}
# export
*main::AddFamilyStepsForProcess = \&AddFamilyStepsForProcess;


=pod

=head2 [subName] #+++

B<Description>: #+++

B<ArgsCount>: #+++

=over 4

=item $arg: #+++

=back

B<Return>: #+++

B<Caller>: #+++

B<Exception>: #+++

B<Example>: #+++

=cut

sub RemoveFamiliesStepsForProcess
{
    my ($steps, $process) = @_;
    $process ||= $g_current_process;

    if ($process)
    {
        if (!ref($steps))
        {
            $steps = [$steps];
        }

        if ('ARRAY' ne ref($steps))
        {
            # in case a family has been provided by error
            confess "Invalid argument!\n";
        }

        foreach my $step (@$steps)
        {
            my $sql_query = "
                UPDATE family_processing
                SET steps_to_perform = REPLACE(steps_to_perform, ?, '')
                WHERE process_id = " . $process->id . "
            ";
            PrintDebug("SQL QUERY: $sql_query\nBindings: $step");
            if (!GetDatabaseHandler()->do($sql_query, undef, $step))
            {
                confess "ERROR: Failed to remove family steps (process $process): " . GetDatabaseHandler()->errstr;
            }
        }
    }
}
# export
*main::RemoveFamiliesStepsForProcess = \&RemoveFamiliesStepsForProcess;


=pod

=head2 [subName] #+++

B<Description>: #+++

B<ArgsCount>: #+++

=over 4

=item $arg: #+++

=back

B<Return>: #+++

B<Caller>: #+++

B<Exception>: #+++

B<Example>: #+++

=cut

sub SetFamiliesStepsDoneForProcess
{
    my ($steps, $process) = @_;
    $process ||= $g_current_process;

    if ($process)
    {
        if (!ref($steps))
        {
            $steps = [$steps];
        }

        if ('ARRAY' ne ref($steps))
        {
            # in case a family has been provided by error
            confess "Invalid argument!\n";
        }

        foreach my $step (@$steps)
        {
            my $sql_query = "
                UPDATE family_processing
                SET steps_to_perform = REPLACE(steps_to_perform, ?, ''),
                    steps_performed  = CONCAT_WS(',',steps_performed,?)
                WHERE process_id = " . $process->id . "
            ";
            PrintDebug("SQL QUERY: $sql_query\nBindings: $step");
            if (!GetDatabaseHandler()->do($sql_query, undef, $step, $step))
            {
                confess "ERROR: Failed to set family steps done (process $process): " . GetDatabaseHandler()->errstr;
            }
        }
    }
}
# export
*main::SetFamiliesStepsDoneForProcess = \&SetFamiliesStepsDoneForProcess;


=pod

=head2 [subName] #+++

B<Description>: #+++

B<ArgsCount>: #+++

=over 4

=item $arg: #+++

=back

B<Return>: #+++

B<Caller>: #+++

B<Exception>: #+++

B<Example>: #+++

=cut

sub AddFamiliesStepsForProcess
{
    my ($steps, $process) = @_;
    $process ||= $g_current_process;

    if ($process)
    {
        if ('ARRAY' eq ref($steps))
        {
            $steps = join(',', @$steps);
        }

        if (ref($steps))
        {
            # in case a family has been provided by error
            confess "Invalid argument!\n";
        }
        my $sql_query = "
            UPDATE family_processing
            SET steps_to_perform = CONCAT_WS(',',steps_to_perform,?)
            WHERE process_id = " . $process->id . "
        ";
        PrintDebug("SQL QUERY: $sql_query\nBindings: $steps");
        if (!GetDatabaseHandler()->do($sql_query, undef, $steps))
        {
            confess "ERROR: Failed to remove family steps (process $process): " . GetDatabaseHandler()->errstr;
        }
    }
}
# export
*main::AddFamiliesStepsForProcess = \&AddFamiliesStepsForProcess;


=pod

=head2 [subName] #+++

B<Description>: #+++

B<ArgsCount>: #+++

=over 4

=item $arg: #+++

=back

B<Return>: #+++

B<Caller>: #+++

B<Exception>: #+++

B<Example>: #+++

=cut

sub AddProcessDuration
{
    my ($duration, $process) = @_;
    $process ||= $g_current_process;

    if ($process)
    {
        if (!defined($duration) || ($duration !~ m/^\d+$/))
        {
            confess "Invalid duration argument!\n";
        }
        $process->duration($process->duration() + $duration);
        $process->save();
    }
}
# export
*main::AddProcessDuration = \&AddProcessDuration;


=pod

=head2 [subName] #+++

B<Description>: #+++

B<ArgsCount>: #+++

=over 4

=item $arg: #+++

=back

B<Return>: #+++

B<Caller>: #+++

B<Exception>: #+++

B<Example>: #+++

=cut

sub AddFamiliesToProcess
{
    my ($families, $process, $steps_to_perform, $status) = @_;

    $process ||= $g_current_process;
    
    if (!$process)
    {
        confess "ERROR: No process given!";
    }
    
    if (!$families || (ref($families) ne 'ARRAY'))
    {
        confess "ERROR: Invalid families argument!\n";
    }

    $steps_to_perform ||= [''];
    if (ref($steps_to_perform) ne 'ARRAY')
    {
        $steps_to_perform = [$steps_to_perform];
    }
    
    foreach my $step_to_perform (@$steps_to_perform)
    {
        if (!exists($FAMILY_PROCESSING_STEPS{$step_to_perform}))
        {
            confess "ERROR: Invalid step-to-perform: '$step_to_perform'\n";
        }
    }

    $status ||= $FAMILY_STATUS_PENDING;
    if (!exists($FAMILY_STATUS{$status}))
    {
        confess "ERROR: Invalid family status: '$status'\n";
    }

    if (@$families)
    {
        my @family_processing_values;
        foreach my $family (@$families)
        {
            push(@family_processing_values,
                '('
                . $family->id . ", '"
                . join(',', @$steps_to_perform) . "', '"
                . $status . "', "
                . $process->id
                . ')'
            );
            $family->reload(['processes']);
        }

        my $family_set_counter = 0;
        # loop on sets of 200 families (to avoid too long SQL queries)
        while ($family_set_counter < @family_processing_values)
        {
            my $sql_query = "INSERT INTO family_processing (family_id, steps_to_perform, status, process_id) VALUES\n"
                . join(",\n", grep { defined } @family_processing_values[$family_set_counter .. 199])
            ;
            my @update_fields;
            if (@$steps_to_perform 
                && ((1 < @$steps_to_perform) || $steps_to_perform->[0]))
            {
                push(@update_fields, "steps_to_perform = '" . join(',', @$steps_to_perform) . "'");
            }

            if ($status)
            {
                push(@update_fields, "status = '$status'");
            }

            if (@update_fields)
            {
                $sql_query .= " ON DUPLICATE KEY UPDATE " . join(',', @update_fields);
            }
            $sql_query .= ';';

            PrintDebug("SQL QUERY: $sql_query");
            GetDatabaseHandler()->do($sql_query)
                or confess "ERROR: Failed to insert family processing values: " . GetDatabaseHandler()->errstr;

            $family_set_counter += 200;
        }
        # reload families
        $process->reload(['families']);
    }
}
# export
*main::AddFamiliesToProcess = \&AddFamiliesToProcess;


=pod

=head2 [subName] #+++

B<Description>: #+++

B<ArgsCount>: #+++

=over 4

=item $arg: #+++

=back

B<Return>: #+++

B<Caller>: #+++

B<Exception>: #+++

B<Example>: #+++

=cut

sub CreateAnalysisProcess
{
    my ($members, $families, $steps_to_perform) = @_;

    if (!$members || (ref($members) ne 'HASH'))
    {
        confess "ERROR: invalid argument!";
    }

    # prepare data
    my (@columns, @place_holders, @values);
    
    if ($members->{'name'})
    {
        push(@columns, 'name');
        push(@place_holders, '?');
        push(@values, $members->{'name'});
    }

    if ($members->{'comments'})
    {
        push(@columns, 'comments');
        push(@place_holders, '?');
        push(@values, $members->{'comments'});
    }

    if ($members->{'status'})
    {
        if (exists($PROCESS_STATUS{$members->{'status'}}))
        {
            push(@columns, 'status');
            push(@place_holders, '?');
            push(@values, $members->{'status'});
        }
        else
        {
            confess "ERROR: Invalid process status for process creation: '" . $members->{'status'} . "'\n";
        }
    }

    if ($members->{'start_date'})
    {
        push(@columns, 'start_date');
        # type of date
        if ($members->{'start_date'} =~ m/^\d+$/)
        {
            # unix timestamp
            push(@place_holders, 'FROM_UNIXTIME(' . $members->{'start_date'} . ')');
        }
        elsif ($members->{'start_date'} =~ m/^(?:CURRENT_TIMESTAMP|NOW(?:\(\s*\))?|DEFAULT)$/i)
        {
            # use default
            push(@place_holders, 'CURRENT_TIMESTAMP');
        }
        else
        {
            push(@place_holders, '?');
            push(@values, $members->{'start_date'});
        }
    }

    if ($members->{'duration'})
    {
        push(@columns, 'duration');
        push(@place_holders, '?');
        push(@values, $members->{'duration'});
    }

    my $sql_query = "
        INSERT INTO processes (" . join(', ', @columns) . ")
        VALUES (" . join(', ', @place_holders) . ")
    ";
    PrintDebug("SQL QUERY: $sql_query\nBindings: " . join(', ', @values));
    GetDatabaseHandler()->do($sql_query, undef, @values)
        or confess "ERROR: Failed to create new process " . GetDatabaseHandler()->errstr;

    # get new process ID
    my $new_process_id = GetDatabaseHandler()->last_insert_id(undef, undef, 'processes', 'id');

    #instanciate object
    my $new_process = Greenphyl::AnalysisProcess->new(
        GetDatabaseHandler(),
        {'selectors' => {'id' => $new_process_id,} }
    );

    if (!$new_process)
    {
        confess "ERROR: Failed to load newly created process (id=$new_process_id)!\n";
    }

    # add selected families
    if ($families)
    {
        AddFamiliesToProcess($families, $new_process, $steps_to_perform, $FAMILY_STATUS_PENDING);
    }

    return $new_process;
}
# export
*main::CreateAnalysisProcess = \&CreateAnalysisProcess;




#+FIXME: on unload, check if current process status can be changed (ie. all families processed -> process done).


=pod

=head1 AUTHORS

Mathieu ROUARD (Bioversity), m.rouard@cgiar.org

Valentin GUIGNON (Bioversity), v.guignon@cgiar.org

Matthieu CONTE (Syngenta)

Sebastien BRIOIS (Syngenta)

=head1 VERSION

Version 1.0.0

Date 13/05/2013

=head1 SEE ALSO

GreenPhyl documentation.

=cut

return 1; # package return
