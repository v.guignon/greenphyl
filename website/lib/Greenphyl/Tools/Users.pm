=pod

=head1 NAME

Greenphyl::Tools::Users - Library of user management functions

=head1 SYNOPSIS

    use Greenphyl::Tools::Users;

=head1 REQUIRES

Perl5

=head1 EXPORTS

HashPassword CheckLoginString CreateUser UpdateUser DeleteUser
UpdateUserOpenID DeleteUserOpenID
GetUserDataFromForm CheckNewUserData

=head1 DESCRIPTION

Library of user management functions.

=cut

package Greenphyl::Tools::Users;


use strict;
use warnings;

use Carp qw(cluck confess croak);
use Digest::MD5 qw(md5_hex);

use base qw(Exporter);

our @EXPORT = qw(
    HashPassword CheckLoginString CreateUser UpdateUser DeleteUser
    UpdateUserOpenID DeleteUserOpenID
    GetUserDataFromForm CheckNewUserData
);

use Greenphyl;
use Greenphyl::User;
use Greenphyl::Web::Access;




# Package constants
####################

=pod

=head1 CONSTANTS

B<$DEBUG>: (boolean)

When set to true, it enables debug mode.

B<$LOGIN_MAX_LENGTH>: (integer)

Maximum login name length.

=cut

our $DEBUG = 0;
our $LOGIN_MAX_LENGTH = 32;
our $LOGIN_ALLOWED_CHARACTERS = '\w \@\-\.';
our $NAME_MAX_LENGTH = 32;
our $NAME_ALLOWED_CHARACTERS = '\wåáâàäæçéêèëíîìïñóôòöúûùüÿ \-\.';



# Package subs
###############

=pod

=head1 FUNCTIONS

=head2 HashPassword

B<Description>: Hash a given clear password and returns its hashed value and the
corresponding salt.

B<ArgsCount>: 1-2

=over 4

=item $password: (string) (R)

A clear password to hash.

=item $salt: (string) (O)

A 32-character string containg the hexadecimal representation of a 16bytes
array. Ex. '58acb81c3e42d6197fe511c01743c09d'
default (recommanded): automatically generated and returned.

=back

B<Return>: (list)

a list of 2 string values: the first one is the hashed password, the second one
is the salt that has been used.

=cut

sub HashPassword
{
    my ($password, $salt) = @_;
    $salt ||= md5_hex(rand());
    my $dbh = GetDatabaseHandler();
    my $sql_query = "SELECT hashGreenPhylPassword(" . $dbh->quote($password) . ", '$salt') AS 'hash';";
    PrintDebug("SQL Query: $sql_query");
    my ($password_hash) = $dbh->selectrow_array($sql_query);
    if (!$password_hash)
    {
        Throw('error' => "Unable to hash password!", 'log' => "Salt: '$salt'\nPassword: '$password'");
    }
    return ($password_hash, $salt);
}


=pod

=head2 CheckLoginString

B<Description>: Check if the given login meets the requirements. Throws an
exception if not.

B<ArgsCount>: 1

=over 4

=item $login: (string) (R)

=back

B<Return>: (boolean)

A true-value (1) if success. Otherwise, an exception is thrown.

=cut

sub CheckLoginString
{
    my ($login, $with_name_restrictions) = @_;

    # login
    if (!defined($login) || !$login)
    {
        Throw('error' => "No user login provided!");
    }

    # login length limited from 3 to 16 alpha-numeric characters
    if (($login !~ m/^[a-zA-Z][$LOGIN_ALLOWED_CHARACTERS]{2,$LOGIN_MAX_LENGTH}$/)
        || ($login !~ m/[a-zA-Z]{3}/)) # make sure the login contains at least 3 consecutive alphabetic characters
    {
        Throw('error' => "Invalid user login! Please enter a valid user login.", 'log' => "Login: '$login'");
    }

    if ($with_name_restrictions
        && (($login =~ m/greenphyl/i)
            || ($login =~ m/^admin(?:istrator)?s?|roots?|super_?users?$/i)))
    {
        Throw('error' => "Invalid user login! Please enter a user login that does not contain 'greenphyl' (and which is not also booked).", 'log' => "Login: '$login'");
    }

    return 1;
}


=pod

=head2 CheckDisplayNameString

B<Description>: Check if the given name meets the requirements. Throws an
exception if not.

B<ArgsCount>: 1

=over 4

=item $display_name: (string) (R)

=back

B<Return>: (boolean)

A true-value (1) if success. Otherwise, an exception is thrown.

=cut

sub CheckDisplayNameString
{
    my ($display_name, $with_name_restrictions) = @_;

    # login
    if (!defined($display_name) || !$display_name)
    {
        Throw('error' => "No user name provided!");
    }

    # login length limited from 3 to 16 alpha-numeric characters
    if (($display_name !~ m/^[$NAME_ALLOWED_CHARACTERS]{3,$NAME_MAX_LENGTH}$/)
        || ($display_name !~ m/[a-zA-Zåáâàäæçéêèëíîìïñóôòöúûùüÿ]{3}/)) # make sure name contains at least 3 consecutive characters
    {
        Throw('error' => "Invalid user name! Please enter a valid user name.", 'log' => "Name: '$display_name'");
    }
    if ($with_name_restrictions
        && (($display_name =~ m/greenphyl/i)
            || ($display_name =~ m/^admin(?:istrator)?s?|roots?|super_?users?$/i)))
    {
        Throw('error' => "Invalid user name! Please enter a user name that does not contain 'greenphyl' (and which is not also booked).", 'log' => "Name: '$display_name'");
    }

    return 1;
}


=pod

=head2 CreateUser

B<Description>: Creates a new user.

B<ArgsCount>: 1

=over 4

=item $user_data: (hash ref) (R)

A hash containing the new values. Supported values are:

'login': new login. Must have between 3 and 16 alpha-numeric characters.

'display_name': display name. Must have between 3 and 16 alpha-numeric characters.

'email': new e-mail address.

'password': new clear password (can be empty).

'password_hash': password hash. Useless and ignored if clear password is
provided.

'salt': salt tu use to hash password. If empty and a new password has been
provided, it will be automatically generated and added to the $user_data hash.

'flags': string containing coma-separated flags in litteral format. If an array
is provided, it will be turned into a coma-separated string.

'description': comments about current account (for administrative purposes).

=back

B<Return>: nothing

=cut

sub CreateUser
{
    my ($user_data) = @_;
    # login
    CheckLoginString($user_data->{'login'});

    # display_name
    if (!$user_data->{'display_name'})
    {
        $user_data->{'display_name'} = $user_data->{'login'};
        # replace login-allowed characters like '@' by underscores
        $user_data->{'display_name'} =~ s/[^$NAME_ALLOWED_CHARACTERS]+/_/g;
    }
    CheckDisplayNameString($user_data->{'display_name'});

    # email
    $user_data->{'email'} ||= undef;

    # description
    $user_data->{'description'} ||= '';

    # flags
    $user_data->{'flags'} ||= '';
    if (ref($user_data->{'flags'}) eq 'ARRAY')
    {
        $user_data->{'flags'} = join(',', @{$user_data->{'flags'}});
    }

    my $dbh = GetDatabaseHandler();

    # try to create a new user
    my $sql_query = "CALL addGreenPhylUser(
        " . $dbh->quote($user_data->{'login'}) . ",
        " . $dbh->quote($user_data->{'display_name'}) . ",
        " . $dbh->quote($user_data->{'email'}) . ",
        " . $dbh->quote($user_data->{'password'}) . ",
        " . $dbh->quote($user_data->{'flags'}) . ",
        " . $dbh->quote($user_data->{'description'}) . "
    );";
    PrintDebug("SQL Query: $sql_query");
    return $dbh->do($sql_query);
}


=pod

=head2 UpdateUser

B<Description>: Updates an existing user. This function is better than calling
$user->save() directly as it manages password update.

B<ArgsCount>: 2

=over 4

=item $user: (Greenphyl::User) (R)

User to update. It must have an identifier.

=item $user_data: (hash ref) (R)

A hash containing the new values. Supported values are:

'login': new login.

'display_name': user display name.

'email': new e-mail address.

'password': new clear password (can be empty).

'password_hash': password hash. Useless and ignored if clear password is
provided.

'salt': salt tu use to hash password. If empty and a new password has been
provided, it will be automatically generated and added to the $user_data hash.

'flags': string containing coma-separated flags in litteral format.

'description': comments about current account (for administrative purposes).

=back

B<Return>: nothing

=cut

sub UpdateUser
{
    my ($user, $user_data) = @_;

    if (!$user)
    {
        Throw('error' => "No user provided! Can't update!");
    }

    # check login
    if (exists($user_data->{'login'}) && defined($user_data->{'login'}))
    {
        CheckLoginString($user_data->{'login'});
    }

    # check display name
    if (exists($user_data->{'display_name'}) && defined($user_data->{'display_name'}))
    {
        CheckDisplayNameString($user_data->{'display_name'});
    }

    # check password
    if ((exists($user_data->{'password'}))
        && (defined($user_data->{'password'})))
    {
        ($user_data->{'password_hash'}, $user_data->{'salt'}) = HashPassword($user_data->{'password'}, $user_data->{'salt'});
    }

    # protect admin (user 1) against being disabled
    if ($user
        && (1 == $user->id)
        && exists($user_data->{'flags'})
        && defined($user_data->{'flags'}))
    {
        $user_data->{'flags'} =~ s/,?'disabled'//gi;
        $user_data->{'flags'} =~ s/^,//;
    }

    # update user
    foreach my $field ('login', 'display_name', 'email', 'password_hash', 'salt', 'flags', 'description')
    {
        if ((exists($user_data->{$field}))
            && (defined($user_data->{$field})))
        {
            $user->$field($user_data->{$field});
        }
    }

    return $user->save();
}


=pod

=head2 DeleteUser

B<Description>: Updates an existing user. This function is better than calling
$user->save() directly as it manages password update.

B<ArgsCount>: 2

=over 4

=item $user: (Greenphyl::User) (R)

User to update. It must have an identifier.

=item $user_data: (hash ref) (R)

A hash containing the new values. Supported values are:

'login': new login.

'display_name': user display name.

'email': new e-mail address.

'password': new clear password (can be empty).

'password_hash': password hash. Useless and ignored if clear password is
provided.

'salt': salt tu use to hash password. If empty and a new password has been
provided, it will be automatically generated and added to the $user_data hash.

'flags': string containing coma-separated flags in litteral format.

'description': comments about current account (for administrative purposes).

=back

B<Return>: nothing

=cut

sub DeleteUser
{
    my ($user) = @_;

    if (!$user)
    {
        Throw('error' => "No user provided! Can't remove!");
    }

    if ('1' eq $user->id)
    {
        Throw('error' => "Can't remove super user!");
    }

    PrintDebug("Removing user '$user'...");

    my $sql_query = "DELETE FROM users WHERE id = ?;";
    PrintDebug("SQL Query: $sql_query\nBindings: " . $user->id);
    my $removed = GetDatabaseHandler()->do($sql_query, undef, $user->id);
    if (!$removed)
    {
        Throw('error' => 'Failed to remove user!', 'log' => GetDatabaseHandler()->errstr);
    }
}


=pod

=head2 GetUserDataFromForm

B<Description>: Gather user profile information from GET or POST data and
returns it into a hash.

B<ArgsCount>: 0

B<Return>: (hash ref)

A user data hash with the following keys:
-'login'
-'display_name'
-'email'
-'description'
-'flags'
-'password'
-'confirm_password'
-'remove_user'

=cut

sub GetUserDataFromForm
{
    # get form data
    my $user_data = {};

    # login
    my $user_login = GetParameterValues('user_login');
    if (defined($user_login))
    {
        $user_data->{'login'} = $user_login;
    }

    # display name
    my $user_name = GetParameterValues('user_display_name');
    if (defined($user_name))
    {
        $user_data->{'display_name'} = $user_name;
    }

    # email
    my $user_email = GetParameterValues('user_email');
    if (defined($user_email) && ('' ne $user_email))
    {
        $user_data->{'email'} = $user_email;
    }

    # only admins are allowed to change these fields
    if (Greenphyl::Web::Access::IsAdmin())
    {
        # description
        my $user_description = GetParameterValues('user_description');
        if (defined($user_description))
        {
            $user_data->{'description'} = $user_description;
        }
        
        # flags
        my @flags = GetParameterValues('user_flags');
        if (@flags || GetParameterValues('set_flags'))
        {
            $user_data->{'flags'} = join(',', @flags);
        }
    }

    # password
    my $user_password = GetParameterValues('user_password');
    if ((defined($user_password) && ('' ne $user_password)))
    {
        $user_data->{'password'} = $user_password;
    }

    $user_data->{'confirm_password'} = GetParameterValues('user_confirm_password');
    if (!defined($user_data->{'confirm_password'}) && $user_data->{'password'})
    {
        $user_data->{'confirm_password'} = $user_data->{'password'};
    }

    if (GetParameterValues('clear_password'))
    {
        $user_data->{'password'} = $user_data->{'confirm_password'} = '';
    }

    # user remove
    $user_data->{'remove_user'} = GetParameterValues('remove_user');

    return $user_data;
}


=pod

=head2 CheckNewUserData

B<Description>: .

B<ArgsCount>: 0-1

=over 4

=item $parameters: (hash ref) (O)

Hash containing default values for some fields:
-'login': (string) default user login;
-'display_name': (string) default user display name;
-'email': (string) default e-mail address;
-'agreement': (boolean) default setting for the agreement checkbox.

=back

B<Return>: (string)

Returns an HTML string containing the GreenPhyl form for registration.

=cut

sub CheckNewUserData
{
    my ($user_data) = @_;

    # check login
    if ($user_data->{'login'})
    {
        utf8::decode($user_data->{'login'});
        $user_data->{'login'} =~ s/^\s+//;
        $user_data->{'login'} =~ s/\s+$//;
    }

    CheckLoginString($user_data->{'login'});
    utf8::encode($user_data->{'login'});

    my $existing_user = Greenphyl::User->new(
        GetDatabaseHandler(),
        {
            'selectors' => {'login' => ['LIKE', $user_data->{'login'}]},
        },
    );

    # check if already exists
    if ($existing_user)
    {
        Throw('error' => "The user login you want ('" . $user_data->{'login'} . "') is already used! Please use another login.");
    }

    if (defined($user_data->{'password'})
        && ($user_data->{'password'} ne '')
        && (length($user_data->{'password'}) < 4))
    {
        Throw('error' => "The password you entered is too short! Please try a longer one.");
    }

    # password check
    if ((defined($user_data->{'confirm_password'}) && !defined($user_data->{'password'}))
        || (defined($user_data->{'password'}) && defined($user_data->{'confirm_password'}) && ($user_data->{'password'} ne $user_data->{'confirm_password'})))
    {
        Throw('error' => "The password you entered did not match its confirmation!");
    }
    
    if (GP('ENABLE_OPENID'))
    {
        # check if the given Open ID is not in use already
        if ($user_data->{'openid_url'} && $user_data->{'openid_identity'})
        {
            my $dbh = GetDatabaseHandler();
            my $sql_query = "SELECT TRUE FROM user_openids WHERE openid_url = ? AND openid_identity = ?;";
            my ($result) = $dbh->selectrow_array($sql_query, undef, $user_data->{'openid_url'}, $user_data->{'openid_identity'});
            if ($result)
            {
                Throw('error' => "The Open ID your provided is alreay in use by another user account!");
            }
        }
    }
}


=pod

=head2 UpdateUserOpenID

B<Description>: Updates an existing user OpenIDs.

B<ArgsCount>: 2

=over 4

=item $user: (Greenphyl::User) (R)

User to update. It must have an identifier.

=item $add_using_target_url: (bool/string) (R)

URL of the page that will process the OpenID authentication success in case of
adding an OpenID identity. If set to a FALSE value, the OpenID identity will be
removed from the given user OpenID list.

=back

B<Return>: nothing

=cut

sub UpdateUserOpenID
{
    my ($user, $add_using_target_url) = @_;

    if (!$user)
    {
        Throw('error' => "No user provided! Can't update!");
    }

    # manage OpenID identities
    my $openid = GetParameterValues('openid_identity');
    if (GP('ENABLE_OPENID') && $openid)
    {
        PrintDebug("Updating OpenID: adding '$openid'");
        my $sql_query = "SELECT TRUE FROM user_openids WHERE user_id = ? AND openid_url = ?;";
        my ($already_in_use) = GetDatabaseHandler()->selectrow_array($sql_query, undef, $user->id, $openid);

        if ($already_in_use)
        {
            if (!$add_using_target_url)
            {
                DeleteUserOpenID($user, $openid);
            }
            else
            {
                Throw('error' => "This OpenID is already set for user '$user'", 'log' => "OpenID: '$openid'");
            }
        }
        else
        {
            # try OpenID identification... (throws a redirection and does not return)
            return Greenphyl::Web::Access::OpenIDAuthenticatePhase1($openid, $add_using_target_url);
        }
    }
}


=pod

=head2 DeleteUserOpenID

B<Description>: Removes an existing user OpenIDs.

B<ArgsCount>: 2

=over 4

=item $user: (Greenphyl::User) (R)

User to update. It must have an identifier.

=item $openid: (string) (R)

OpenID to remove.

=back

B<Return>: nothing

=cut

sub DeleteUserOpenID
{
    my ($user, $openid) = @_;

    if (!$user)
    {
        Throw('error' => "No user provided! Can't update!");
    }

    if (GP('ENABLE_OPENID') && $openid)
    {
        PrintDebug("Updating OpenID: removing '$openid'");
        my $sql_query = "SELECT TRUE FROM user_openids WHERE user_id = ? AND openid_identity = ?;";
        if (GetDatabaseHandler()->selectrow_array($sql_query, undef, $user->id, $openid))
        {
            # make sure user still have another OpenID or a password
            if (!$user->password
                && (@{$user->openids} <= 1)
                && !Greenphyl::Web::Access::IsAdmin())
            {
                Throw('error' => "You can't remove this OpenID as there would be no other way to authenticate! Please set a GreenPhyl password to this user account first.");
            }

            # remove OpenID
            $sql_query = "DELETE FROM user_openids WHERE user_id = ? AND openid_identity = ?;";
            PrintDebug("SQL Query: $sql_query\nBindings: " . $user->id . ', ' . $openid);
            GetDatabaseHandler()->do($sql_query, undef, $user->id, $openid);
        }
    }
}


=pod

=head1 AUTHORS

Mathieu ROUARD (Bioversity), m.rouard@cgiar.org

Valentin GUIGNON (Bioversity), v.guignon@cgiar.org

Matthieu CONTE (Syngenta)

Sebastien BRIOIS (Syngenta)

=head1 VERSION

Version 1.2.0

Date 10/02/2014

=head1 SEE ALSO

GreenPhyl documentation.

=cut

return 1; # package return
