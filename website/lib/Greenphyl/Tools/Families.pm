=pod

=head1 NAME

Greenphyl::Tools::Families - Family tools

=head1 SYNOPSIS

    my $go_data                  = GenerateGOData($family);
    my $sorted_go                = [sort {$a->code cmp $b->code} @$go_data];
    my $go_groups                = Greenphyl::GO::GroupGOByType($sorted_go);
    my $family_relationship_data = GenerateFamilyRelationshipJSONData($family);
    my $family_composition_data  = GenerateFamilyCompositionData($family);
    my $family_protein_domains   = GenerateFamilyProteinDomainData($family);
    my $family_protein_list      = GenerateFamilyProteinListData($family);
    my $phylogenomic_analysis    = GeneratePhylogenomicAnalysisData($family);

=head1 REQUIRES

Perl5

=head1 EXPORTS

LoadFamily LoadFamilies PrepareFamilyLoadingQuery GetFamilyLoadingParameters
HaveFamiliesBeenFiltered LoadCustomFamily
GenerateFamilyRelationshipData GenerateFamilyRelationshipJSONData
GenerateFamilyCompositionData GenerateFamilyProteinDomainData
GenerateFamilyProteinListData GeneratePhylogenomicAnalysisData
GenerateCustomPhylogenomicAnalysisData
GenerateGOData GenerateCustomFamilyCompositionData
LoadOrphans GetOrphanQueryParameters
LoopOnFamilyLists LoopOnFamilies
FindFamilyOutputDirectory FindFamilyFilePath
FilterCustomFamilies AddSequencesToCustomFamily

=head1 DESCRIPTION

Provides several helper function to process families and generate associated
data structures.

=cut

package Greenphyl::Tools::Families;


use strict;
use warnings;

use Carp qw(cluck confess croak);

use base qw(Exporter);

our @EXPORT = qw(
    LoadFamily LoadFamilies PrepareFamilyLoadingQuery GetFamilyLoadingParameters
    HaveFamiliesBeenFiltered LoadCustomFamily
    GenerateFamilyRelationshipData GenerateFamilyRelationshipJSONData
    GenerateFamilyCompositionData GenerateFamilyProteinDomainData
    GenerateFamilyProteinListData GeneratePhylogenomicAnalysisData
    GenerateGOData GenerateCustomFamilyCompositionData
    GenerateCustomPhylogenomicAnalysisData
    LoadOrphans GetOrphanQueryParameters
    LoopOnFamilyLists LoopOnFamilies
    FindFamilyOutputDirectory FindFamilyFilePath
    FilterCustomFamilies AddSequencesToCustomFamily
);

use JSON;

use Greenphyl;
# use Greenphyl::Web;
use Greenphyl::Tools::IPR;
use Greenphyl::Tools::GO;
use Greenphyl::Dumper;

use Greenphyl::CompositeObject;
use Greenphyl::AbstractFamily;
use Greenphyl::Family;
use Greenphyl::CustomFamily;
use Greenphyl::FamilyIPRStats;
use Greenphyl::GO;
use Greenphyl::IPR;
use Greenphyl::SequenceCount;
use Greenphyl::SourceDatabase;
use Greenphyl::Species;
use Greenphyl::Sequence;
use Greenphyl::FamilyGO;
use Greenphyl::CustomSequence;




# Package constants
####################

=pod

=head1 CONSTANTS

B<$DEBUG>: (boolean)

Enables debug mode.

B<$FILTERS>: (array ref)

list of filter that can be applyed on sequence lists.

B<%FAMILY_IPR_THRESHOLDS>: (hash)

Thresholds for IPR families below which an IPR is not shown. The IPR value is
computed from the number of sequence having the IPR in a cluster family against
the total number of sequence in that cluster family.
Each cluster family level (class) has a specific IPR families threshold.
Keys of the hash are cluster family level and values are associated threshold
values in percent (0-100).

B<$OTHER_IPR_THRESHOLD>: (integer)

Threshold applied on non-family IPR the same way %FAMILY_IPR_THRESHOLDS
thresholds are applied except that the cluster family level is not taken in
account.

B<@SQL_OPTION_ORDER>: (array of string)

List of SQL options in the correct order for a SELECT query.

=cut

our $DEBUG = 0;
our $FAMILY_LIMIT = 5000;
our $FILTERS = [
    {
        'label' => 'Gene name',
        'value' => 'locus',
    },
    {
        'label' => 'UniProt',
        'value' => 'swiss',
    },
#    {
#        'label' => 'KEGG',
#        'value' => 'kegg',
#    },
    {
        'label' => 'InterPro',
        'value' => 'ipr',
    },
    {
        'label' => 'Annotation',
        'value' => 'annotation',
    },
    {
        'label' => 'Gene Ontology',
        'value' => 'GO',
    },
#    {
#        'label' => 'EC',
#        'value' => 'EC',
#    },
    {
        'label' => 'PubMed',
        'value' => 'pubmed',
    },
    {
        'label' => 'Filter Status',
        'value' => 'filtered',
    },
];
our %FAMILY_IPR_THRESHOLDS = (
    1 => 51,
    2 => 70,
    3 => 80,
    4 => 90,
);
our $OTHER_IPR_THRESHOLD = 25;
our $FAMILY_GO_THRESHOLD = 1;

our $LEVEL_VALIDATION_REGEXP = '[1-4]';

our @SQL_OPTION_ORDER = ('GROUP BY', 'HAVING', 'ORDER BY', 'LIMIT', 'OFFSET');




# Package subs
###############

=pod

=head1 FUNCTIONS

=head2 LoadFamily

B<Description>: Loads and returns a family object. Throws an exception if the
family couldn't be loaded. The family is selected using either (by priority) the
HTTP parameters 'family_accession' or 'family_id'.

B<ArgsCount>: 0

B<Return>: (Greenphyl::Family)

Loaded family.

=cut

sub LoadFamily
{
    my $family_accession = GetParameterValues('family_accession');
    my $family_id = GetParameterValues('family_id') || GetParameterValues('id');
    my $selector = {};

    if (!$family_id || ($family_id !~ m/^\d{1,10}$/))
    {
        $family_id = undef;
    }
    if (!$family_accession || ($family_accession !~ m/^$Greenphyl::AbstractFamily::FAMILY_REGEXP$/))
    {
        $family_accession = undef;
    }

    if ($family_accession)
    {
        $selector = {'accession' => $family_accession};
    }
    elsif ($family_id)
    {
        $selector = {'id' => $family_id};
    }
    else
    {
        Throw('error' => 'No valid family identifier provided!');
    }

    my $family = Greenphyl::Family->new(
        GetDatabaseHandler(),
        {'selectors' => $selector }
    );

    if (!$family)
    {
        if ($family_accession)
        {
            Throw('error' => "Family not found!", 'log' => "Accession: $family_accession");
        }
        else
        {
            Throw('error' => "Family ID not found!", 'log' => "ID: $family_id");
        }
    }

    return $family;
}



=pod

=head2 LoadCustomFamily

B<Description>: Loads and returns a custom family object. Throws an exception if
the family couldn't be loaded. The family is selected using either (by priority)
the HTTP parameters 'accession' or 'custom_family_id'.

B<ArgsCount>: 0

B<Return>: (Greenphyl::CustomFamily)

Loaded custom family.

=cut

sub LoadCustomFamily
{
    my $family_accession = GetParameterValues('accession');
    my $family_id = GetParameterValues('custom_family_id') || GetParameterValues('id');
    my $selector = {};

    if (!$family_id || ($family_id !~ m/^\d{1,10}$/))
    {
        $family_id = undef;
    }

    if (!$family_accession
        || (($family_accession !~ m/^$Greenphyl::AbstractFamily::FAMILY_REGEXP$/)
            && ($family_accession !~ m/^GPV1_\d{1,5}_[1-4]$/)
            && ($family_accession !~ m/^GPV2_\d{1,5}_[1-4]$/)
            && ($family_accession !~ m/^ $Greenphyl::AbstractFamily::FAMILY_REGEXP _V3 $/x)
            && ($family_accession !~ m/^ $Greenphyl::AbstractFamily::FAMILY_REGEXP _V4 $/x)
            && ($family_accession !~ m/^ $Greenphyl::AbstractFamily::FAMILY_REGEXP _V5 $/x)
        )
    )
    {
        $family_accession = undef;
    }

    if ($family_accession)
    {
        $selector = {'accession' => $family_accession};
    }
    elsif ($family_id)
    {
        $selector = {'id' => $family_id};
    }
    else
    {
        Throw('error' => 'No valid custom family identifier provided!');
    }

    my $family = Greenphyl::CustomFamily->new(
        GetDatabaseHandler(),
        {'selectors' => $selector }
    );

    if (!$family)
    {
        if ($family_accession)
        {
            Throw('error' => "Family not found!", 'log' => "Accession: $family_accession");
        }
        else
        {
            Throw('error' => "Family ID not found!", 'log' => "ID: $family_id");
        }
    }

    return $family;
}


=pod

=head2 GetFamilyLoadingParameters

B<Description>: Returns a hash of parameters that are taken in account to load
families.

B<ArgsCount>: 0

B<Return>: (hash ref)

Returns a hash: keys are parameter name, and values are parameter values which
can be either a scalar or an array of scalar. If no parameters were set, returns
a hash with 3 entries:
{ 'ORDER BY' => 'f.accession ASC', 'black_listed' => 0, 'LIMIT' => 1000 }

=cut

sub GetFamilyLoadingParameters
{
    my $parameter_hash = {};
    my $sql_query;
    my $dbh = GetDatabaseHandler();

    # get family parameters...
    # -check for a list of family ID
    my @family_ids = GetParameterValues('family_id', undef, '[,;\s]+', 1);
    if (@family_ids)
    {$parameter_hash->{'family_id'} = \@family_ids;}


    # -check for name keywords
    my @family_name_words = GetParameterValues('name');
    if (@family_name_words)
    {$parameter_hash->{'name'} = \@family_name_words;}
    my @family_not_in_name_words = GetParameterValues('not_name', undef, '[,;]+');
    if (@family_not_in_name_words)
    {$parameter_hash->{'not_name'} = \@family_not_in_name_words;}
    PrintDebug("not_name: " . scalar(@family_not_in_name_words) . '=' . join(', ', @family_not_in_name_words));


    # -check for a list of family accessions
    my @family_accessions = GetParameterValues('accession', undef, '[,;\s]+', 1);
    if (@family_accessions)
    {$parameter_hash->{'accession'} = \@family_accessions;}


    # -check for level
    my @levels = GetParameterValues('level', undef, '[,;\s]+');
    if (@levels)
    {$parameter_hash->{'level'} = \@levels;}


    # -check for type
    my @types = GetParameterValues('type', undef, '[,;]+');
    if (@types)
    {$parameter_hash->{'type'} = \@types;}


    # -check for description key-words
    my @family_description_words = GetParameterValues('description');
    if (@family_description_words)
    {$parameter_hash->{'description'} = \@family_description_words;}


    # -check for validation status
    my @validated = GetParameterValues('validated', undef, '[,;]+');
    if (@validated)
    {$parameter_hash->{'validated'} = \@validated;}


    # -check for black_listed filter
    my $black_listed = GetParameterValues('black_listed');
    if (defined($black_listed))
    {$parameter_hash->{'black_listed'} = $black_listed;}
    else
    {
        # by default, exclude black-listed families
        $parameter_hash->{'black_listed'} = 0;
    }


    # -check for inference
    my @inferences = GetParameterValues('inference', undef, '[,;]+');
    if (@inferences)
    {$parameter_hash->{'inference'} = \@inferences;}


    # -check for a list of species or phylum
    my @taxonomy_ids = GetParameterValues('taxonomy_id', undef, '[,;\s]+');
    if (@taxonomy_ids)
    {$parameter_hash->{'taxonomy_id'} = \@taxonomy_ids;}


    # -check for plant_specific filter
    my @plant_specific = GetParameterValues('plant_specific', undef, '[,;\s]+');
    if (@plant_specific)
    {$parameter_hash->{'plant_specific'} = \@plant_specific;}
    my $max_plant_specific = GetParameterValues('max_plant_specific');
    if (defined($max_plant_specific))
    {$parameter_hash->{'max_plant_specific'} = $max_plant_specific;}
    my $min_plant_specific = GetParameterValues('min_plant_specific');
    if (defined($min_plant_specific))
    {$parameter_hash->{'min_plant_specific'} = $min_plant_specific;}


    # -check for a max number of sequences
    my $max_sequences = GetParameterValues('max_sequences');
    if (defined($max_sequences))
    {$parameter_hash->{'max_sequences'} = $max_sequences;}


    # -check for a min number of sequences
    my $min_sequences = GetParameterValues('min_sequences');
    if (defined($min_sequences))
    {$parameter_hash->{'min_sequences'} = $min_sequences;}

    # -check for phylogeny filter
    my $phylogeny_analysis = GetParameterValues('phylogeny_analysis');
    my $with_phylogeny = GetParameterValues('with_phylogeny');
    my $without_phylogeny = GetParameterValues('without_phylogeny');
    if ($phylogeny_analysis)
    {
        if ($phylogeny_analysis =~ m/with_phylogeny/i)
        {
            $with_phylogeny ||= 1;
        }
        elsif ($phylogeny_analysis =~ m/without_phylogeny/i)
        {
            $without_phylogeny ||= 1;
        }
        else
        {
            cluck "WARNING: unrecognized parameter value for 'phylogeny_analysis' ($phylogeny_analysis)! Parameters ignored.\n";
        }
    }
    if ($with_phylogeny && $without_phylogeny)
    {
        cluck "WARNING: both 'with_phylogeny' and 'without_phylogeny' parameters used! Parameters ignored.\n";
    }
    elsif ($with_phylogeny)
    {
        $parameter_hash->{'with_phylogeny'} = 1;
    }
    elsif ($without_phylogeny)
    {
        $parameter_hash->{'without_phylogeny'} = 1;
    }

    # -check for a list of parents
    my @parent_ids = GetParameterValues('parent_id', undef, '[,;\s]+', 1);
    # convert parent accessions into DB IDs
    if (my @parent_accessions = GetParameterValues('parent_accession', undef, '[,;\s]+', 1))
    {
        $sql_query = "SELECT id FROM families WHERE accession IN (" . ('?, ' x $#parent_accessions) . "?);";
        PrintDebug("Parents selection query: $sql_query\nBindings: " . join(', ', @parent_accessions));
        my $named_parent_ids = $dbh->selectcol_arrayref($sql_query, undef, @parent_accessions)
            or confess $dbh->errstr;
        if (@$named_parent_ids)
        {
            push(@parent_ids, @$named_parent_ids);
        }
    }

    if (@parent_ids)
    {$parameter_hash->{'parent_ids'} = \@parent_ids;}

    # -check for a list of children
    my @child_ids = GetParameterValues('child_id', undef, '[,;\s]+', 1);
    # convert child accessions into DB IDs
    if (my @child_accessions = GetParameterValues('child_accession', undef, '[,;\s]+', 1))
    {
        $sql_query = "SELECT family_id FROM family WHERE accession IN (" . ('?, ' x $#child_accessions) . "?);";
        PrintDebug("Children selection query: $sql_query\nBindings: " . join(', ', @child_accessions));
        my $named_child_ids = $dbh->selectcol_arrayref($sql_query, undef, @child_accessions)
            or confess $dbh->errstr;
        if (@$named_child_ids)
        {
            push(@child_ids, @$named_child_ids);
        }
    }

    if (@child_ids)
    {$parameter_hash->{'child_ids'} = \@child_ids;}

    # -check for a list of species
    my @species_ids = GetParameterValues('species_id', undef, '[,;\s]+', 1);
    # convert species codes into DB IDs
    if (my @species_codes = GetParameterValues('species_code', undef, '[,;\s]+', 1))
    {
        $sql_query = "SELECT id FROM species WHERE code IN (" . ('?, ' x $#species_codes) . "?);";
        PrintDebug("Species selection query: $sql_query\nBindings: " . join(', ', @species_codes));
        my $named_species_ids = $dbh->selectcol_arrayref($sql_query, undef, @species_codes)
            or confess $dbh->errstr;
        if (@$named_species_ids)
        {
            push(@species_ids, @$named_species_ids);
        }
    }

    if (@species_ids)
    {$parameter_hash->{'species_id'} = \@species_ids;}


    # -check for a list of sequences
    my @sequence_ids = GetParameterValues('sequence_id', undef, '[,;\s]+', 1);
    # convert sequence accessions into DB IDs
    if (my @sequence_accessions = GetParameterValues('sequence_accession', undef, '[,;\s]+', 1))
    {
        $sql_query = "SELECT id FROM sequences WHERE accession IN (" . ('?, ' x $#sequence_accessions) . "?);";
        PrintDebug("Sequence selection query: $sql_query\nBindings: " . join(', ', @sequence_accessions));
        my $accession_sequences_ids = $dbh->selectcol_arrayref($sql_query, undef, @sequence_accessions)
            or confess $dbh->errstr;
        PrintDebug("Got " . scalar(@$accession_sequences_ids) . " matching sequences.");
        if (@$accession_sequences_ids)
        {
            push(@sequence_ids, @$accession_sequences_ids);
        }
        else
        {
            # no matching sequence found, add a fake sequence identifier to
            # prevent any family match
            push(@sequence_ids, 0);
        }
    }
    if (my @sequence_accession_like = GetParameterValues('sequence_accession_like', undef, '[,;\s]+', 1))
    {
        @sequence_accession_like = map {$_ . '%'} @sequence_accession_like;
        $sql_query = "SELECT id FROM sequences WHERE accession LIKE ?" . (' OR accession LIKE ?' x $#sequence_accession_like) . ";";
        PrintDebug("Sequence selection query: $sql_query\nBindings: " . join(', ', @sequence_accession_like));
        my $accession_sequences_ids = $dbh->selectcol_arrayref($sql_query, undef, @sequence_accession_like)
            or confess $dbh->errstr;
        PrintDebug("Got " . scalar(@$accession_sequences_ids) . " matching sequences.");
        if (@$accession_sequences_ids)
        {
            push(@sequence_ids, @$accession_sequences_ids);
        }
        else
        {
            # no matching sequence found, add a fake sequence identifier to
            # prevent any family match
            push(@sequence_ids, 0);
        }
    }
    if (@sequence_ids)
    {
        my %unique_sequence_ids = map {$_ => 1} @sequence_ids;
        $parameter_hash->{'sequence_ids'} = [keys(%unique_sequence_ids)];
    }


    # -check for a list of IPR
    my @ipr_ids = GetParameterValues('ipr_id', undef, '[,;\s]+', 1);
    # convert IPR codes into DB IDs
    if (my @ipr_codes = GetParameterValues('ipr_code', undef, '[,;\s]+', 1))
    {
        my @valid_ipr_codes;
        foreach my $ipr_code (@ipr_codes)
        {
            if ($ipr_code =~ m/^$Greenphyl::IPR::IPR_REGEXP$/i)
            {
                push(@valid_ipr_codes, uc($ipr_code));
            }
        }
        if (@valid_ipr_codes)
        {
            $sql_query = "SELECT id FROM ipr WHERE code IN (" . ('?, ' x $#valid_ipr_codes) . "?);";
            PrintDebug("IPR selection query: $sql_query\nBindings: " . join(', ', @valid_ipr_codes));
            my $ipr_code_ids = $dbh->selectcol_arrayref($sql_query, undef, @valid_ipr_codes)
                or confess $dbh->errstr;
            if (@$ipr_code_ids)
            {
                push(@ipr_ids, @$ipr_code_ids);
            }
        }
    }

    if (@ipr_ids)
    {$parameter_hash->{'ipr_id'} = \@ipr_ids;}


    # -check for uniprot
    my $with_uniprot = GetParameterValues('uniprot');
    if ($with_uniprot)
    {$parameter_hash->{'uniprot'} = $with_uniprot;}


    # -check for a list of UniProt/DBXRef
    my @sequence_dbxref_ids = GetParameterValues('sequence_dbxref_id', undef, '[,;\s]+', 1);
    # convert DBXRef accession into DB IDs
    if (my @sequence_dbxref_accession = GetParameterValues('sequence_dbxref_accession', undef, '[\s,;]+', 1))
    {
        $sql_query = "SELECT id FROM dbxref WHERE UPPER(accession) IN (" . ('?, ' x $#sequence_dbxref_accession) . "?);";
        PrintDebug("UniProt selection query: $sql_query\nBindings: " . join(', ', map {uc($_)} @sequence_dbxref_accession));
        my $dbxref_accession_ids = $dbh->selectcol_arrayref($sql_query, undef, map {uc($_)} @sequence_dbxref_accession)
            or confess $dbh->errstr;
        if (@$dbxref_accession_ids)
        {
            push(@sequence_dbxref_ids, @$dbxref_accession_ids);
        }
    }

    if (@sequence_dbxref_ids)
    {$parameter_hash->{'sequence_dbxref_id'} = \@sequence_dbxref_ids;}


    # -check for a list of family DBXRef
    my @dbxref_ids = GetParameterValues('dbxref_id', undef, '[,;\s]+', 1);
    # convert DBXRef accession into DB IDs
    if (my @dbxref_accession = GetParameterValues('dbxref_accession', undef, '[\s,;]+', 1))
    {
        $sql_query = "SELECT id FROM dbxref WHERE UPPER(accession) IN (" . ('?, ' x $#dbxref_accession) . "?);";
        PrintDebug("Family cross-ref selection query: $sql_query\nBindings: " . join(', ', map {uc($_)} @dbxref_accession));
        my $dbxref_accession_ids = $dbh->selectcol_arrayref($sql_query, undef, map {uc($_)} @dbxref_accession)
            or confess $dbh->errstr;
        if (@$dbxref_accession_ids)
        {
            push(@dbxref_ids, @$dbxref_accession_ids);
        }
    }

    if (@dbxref_ids)
    {$parameter_hash->{'dbxref_id'} = \@dbxref_ids;}


    # -check for a list of GO
    my @go_ids = GetParameterValues('go_id', undef, '[,;\s]+', 1);
    # convert GO codes into GO IDs
    if (my @go_codes = GetParameterValues('go_code', undef, '[\s,;]+', 1))
    {
        $sql_query = "SELECT id FROM go WHERE UPPER(code) IN (" . ('?, ' x $#go_codes) . "?);";
        PrintDebug("Family GO code selection query: $sql_query\nBindings: " . join(', ', map {uc($_)} @go_codes));
        my $go_code_ids = $dbh->selectcol_arrayref($sql_query, undef, map {uc($_)} @go_codes)
            or confess $dbh->errstr;
        if (@$go_code_ids)
        {
            push(@go_ids, @$go_code_ids);
        }
    }
    # convert GO names into GO IDs
    for my $go_name (GetParameterValues('go_name', undef, '[\s,;]+', 1))
    {
        $sql_query = "SELECT id FROM go WHERE name LIKE ?;";
        PrintDebug("Family GO name selection query: $sql_query\nBindings: '%" . $go_name . "%'");
        my $go_name_ids = $dbh->selectcol_arrayref($sql_query, undef, '%' . $go_name . '%')
            or confess $dbh->errstr;
        if (@$go_name_ids)
        {
            push(@go_ids, @$go_name_ids);
        }
    }
    if (@go_ids)
    {
        # make unique IDs
        my %go_ids = map {$_ => 1} (@go_ids);
        $parameter_hash->{'go_id'} = [keys(%go_ids)];
    }


    # get SQL options
    # -limit
    my $limit = GetParameterValues('family_set_size');
    if ($limit && ($limit =~ m/^\d+$/))
    {
        $parameter_hash->{'LIMIT'} = $limit;
    }
    elsif ($limit && ($limit eq '-1'))
    {
        delete($parameter_hash->{'LIMIT'});
    }
    else
    {
        # use default limit
        $parameter_hash->{'LIMIT'} = $FAMILY_LIMIT;
    }

    # -offset
    my $family_page = GetParameterValues('family_page');
    if (defined($family_page) && ($family_page =~ m/^\d{1,4}$/))
    {
        if ($parameter_hash->{'LIMIT'})
        {
            $parameter_hash->{'OFFSET'} = $family_page * $parameter_hash->{'LIMIT'};
        }
        else
        {
            cluck "WARNING: family_page value set while no family_set_size set! Ignoring offset.\n";
        }
    }

    # -order
    if (GetParameterValues('unsorted'))
    {
        delete($parameter_hash->{'ORDER BY'});
    }
    elsif (GetParameterValues('reverse'))
    {
        $parameter_hash->{'ORDER BY'} = 'f.accession DESC';
    }
    elsif (GetParameterValues('sort_by_size'))
    {
        $parameter_hash->{'ORDER BY'} = 'f.sequence_count ASC';
    }
    elsif (GetParameterValues('reverse_size'))
    {
        $parameter_hash->{'ORDER BY'} = 'f.sequence_count DESC';
    }
    else
    {
        # default: sort by accession
        $parameter_hash->{'ORDER BY'} = 'f.accession ASC';
    }

    return $parameter_hash;
}




=pod

=head2 HaveFamiliesBeenFiltered

B<Description>: Returns true if a filter has been specified when loading
families.

B<ArgsCount>: 0-1

=over 4

=item $parameters: (hash ref) (O)

A hash like the one returned by GetFamilyLoadingParameters().

=back

B<Return>: (boolean)

A true value if families have been filtered or false otherwise.

=cut

sub HaveFamiliesBeenFiltered
{
    my ($parameters) = @_;
    $parameters ||= GetFamilyLoadingParameters();
    if (!ref($parameters))
    {
        confess "ERROR: Invalid family loading parameter!\n";
    }

    if ((3 == scalar(keys(%$parameters)))
        && (exists($parameters->{'ORDER BY'}) && ($parameters->{'ORDER BY'} eq 'f.accession ASC'))
        && (exists($parameters->{'black_listed'}) && ($parameters->{'black_listed'} == 0))
        && (exists($parameters->{'LIMIT'}) && ($parameters->{'LIMIT'} == $FAMILY_LIMIT))
       )
    {
        return 0;
    }
    else
    {
        return 1;
    }
}


=pod

=head2 PrepareFamilyLoadingQuery

B<Description>: Prepare elements of the family loading SQL query.

Family selection parameters handled:
-'family_id': one or several numeric family identifier (DB) separated by coma or
  semicolon or spaces.
-'name': a (partial) family name. Several names can be specified by duplicating
  this parameter.
-'not_name': a (partial) family name to exclude. Several names can be specified
  by duplicating this parameter or using coma or semicolon.
-'accession': one or several family accession separated by coma or semicolon or
  spaces.
-'level': one or more family clustering levels separated by coma or semicolon or
  spaces.
-'type': one or more cluster types separated by coma or semicolon or spaces.
  Possible valid values are: 'N/A','superfamily','family','subfamily','group'.
-'description': one or more words that should be found in family descriptions.
  This parameter is case-insensitive. Several expressions parts can be specified
  by duplicating this parameter.
  ex.: "description=transcription factor" "description=PLATZ"
-'validated': one or more validation status separated by coma or semicolon or
  spaces. Possible valid values are: 'N/A','high','normal','unknown',
  'suspicious','clustering error'.
-'black_listed': if set to 1, only select black-listed families, if set to 0,
  only select non-black-listed families.
  Default: 0 (ignore black-listed families).
-'inference': one or more validation status separated by coma or semicolon or
  spaces.
-'taxonomy_id': one or more taxonomy identifier (DB) separated by coma or
  semicolon or spaces used to select families that are specific to a given
  taxonomy element.
-'plant_specific': value of the plant_specific attribute.
-'max_plant_specific': maximum value of the plant_specific attribute.
-'min_plant_specific': minimum value of the plant_specific attribute.
-'max_sequences': maximum number of sequences in family (inclusive).
-'min_sequences': minimum number of sequences in family (inclusive).
-'phylogeny_analysis': can be either 'with_phylogeny' or 'without_phylogeny'.
-'with_phylogeny': set to true to only select families with homologies.
-'without_phylogeny': set to true to only select families without homologies.
-'species_id': one or more species identifier (DB) separated by coma or
 semicolon or spaces. Selection is relative to species_selection parameter.
-'species_code': one or more species codes (3-5 characters) separated by coma or
 semicolon or spaces. Selection is relative to species_selection parameter.
-'species_selection': can be one of:
  -'unfilter_species' or not defined: no species filtration;
  -'all_species': family must contain sequences from all the given species;
  -'species_specific': family must be specific to one of the given species;
-'sequence_id': one or more sequence identifier (DB) the family must contain;
-'sequence_accession': one or more sequence accession the family must contain;
-'sequence_accession_like': one or more sequence accession pattern the family
  must contain;
-'parent_id': one or more family identifier (DB) of families that must be the
  parents of selected families;
-'parent_accession': one or more family accession of families that must be the
  parents of selected families;
-'child_id': one or more family identifier (DB) of families that must be the
  children of selected families;
-'child_accession': one or more family accession of families that must be the
  children of selected families;
-'ipr_id': one or more IPR identifier (DB) separated by coma or
  semicolon or spaces used to select families that contain sequences bearing the
  given IPR domains.
-'ipr_code': one or more IPR code separated by coma or semicolon or spaces used
  to select families that contain sequences bearing the given IPR domains.
-'uniprot': if set to 1, the families must have sequences with a UniProt
  accession.
-'dbxref_id': one or more cross-reference identifier (DB) separated by coma or
  semicolon or spaces used to select families that have those cross-references
  (from dbxref_families table).
-'dbxref_accession': one or more cross-reference accessions separated by coma or
  semicolon or spaces used to select families that have those cross-references
  (from dbxref_families table).
-'go_id': one or more GO identifiers (DB) separated by coma or semicolon or
  spaces used to select families that are tagged with those GO
  (from families_go_cache table).
-'go_code': one or more GO coded separated by coma or semicolon or spaces used
  to select families that are tagged with those GO (from families_go_cache
  table).
-'go_name': one or more GO names or parts of GO names separated by coma or
  semicolon or spaces used to select families that are tagged with those GO
  (from families_go_cache table).
-'go_selection': GO selection method. Can be either 'and' or 'or' (default). If
  'and' is used, matching families must have all the requested GO.
  Default: 'or', families are matched if they have at least one of the given GO.
-'family_set_size': maximum number of families to load (default: $FAMILY_LIMIT)
-'family_page': index of the set of families to load (see 'family_set_size').
  0 means "first set of 'family_set_size' families",
  1 means "give the next 'family_set_size' families" after the first set.
-'unsorted': no not sort families (default: sorted by accession).
-'reverse': sort families by accession in reverse order.
-'sort_by_size': sort families by number of sequence (otherwise, sorted by
  accession).
-'reverse_size': sort families by number of sequencen largest families first.

Note: when using command line (and not CGI), spaces separated values should be
  grouped together inside quote otherwise some values will be treated as
  other parameters. You can also use an alternative way to specify multiple
  values: just duplicate the option (no quote needed).
  Ex.:
  bad: family_id=1 2 42 500 min_sequences=10
  good: "family_id=1 2 42 500" "min_sequences=10"
  good: family_id=1 family_id=2 family_id=42 family_id=500 min_sequences=10

B<ArgsCount>: 0-1

=over 4

=item $parameter_hash: (hash ref) (O)

A hash of parameters just like the one returned by GetFamilyLoadingParameters().

=back

B<Return>: (list)

Returns a list:
-array of names of qualified fields to load (array ref);
-table clause (string);
-array of "where" conditions (array ref);
-array of binding values (array ref).
-hash of SQL options for the query (like LIMIT, OFFSET, ORDER,...)

=cut

sub PrepareFamilyLoadingQuery
{
    # get parameters
    my ($parameter_hash) = @_;
    $parameter_hash ||= GetFamilyLoadingParameters();
    my $dbh = GetDatabaseHandler();

    # -list of species or phylum
    my $taxonomy_ids = $parameter_hash->{'taxonomy_id'};
    # -list of species
    my $species_ids = $parameter_hash->{'species_id'};


    my $table_clause = 'families f';
    my @where_clause = ();
    my @bind_values = ();
    # init selectors
    my $selectors = {};

    my $AddEqualField = sub
    {
        my ($field_name, $item_list) = @_;
        $item_list ||= [];
        if (1 == @$item_list)
        {
            push(@where_clause, $field_name . ' = ?');
            push(@bind_values, $item_list->[0]);
        }
        elsif (@$item_list)
        {
            push(@where_clause, $field_name . ' IN (' . ('?, ' x $#$item_list) . '?)');
            push(@bind_values, @$item_list);
        }
    };

    my $AddCaseInsensitiveField = sub
    {
        my ($field_name, $item_list) = @_;
        $item_list ||= [];
        if (1 == @$item_list)
        {
            push(@where_clause, 'UPPER(f.accession) = ?');
            push(@bind_values, uc($item_list->[0]));
        }
        elsif (@$item_list)
        {
            push(@where_clause, 'UPPER(f.accession) IN (' . ('?, ' x $#$item_list) . '?)');
            push(@bind_values, map {uc($_)} @$item_list);
        }
    };

    my $AddLikeField = sub
    {
        my ($field_name, $item_list) = @_;
        $item_list ||= [];
        if (1 == @$item_list)
        {
            push(@where_clause, $field_name . ' LIKE ?');
            #+FIXME: escape '_' and '%' in keyword?
            push(@bind_values, '%' . $item_list->[0] . '%');
        }
        elsif (@$item_list)
        {
            my @value_like_clauses;
            foreach my $keyword (@$item_list)
            {
                push(@value_like_clauses, $field_name . ' LIKE ?');
                #+FIXME: escape '_' and '%' in keywords?
                push(@bind_values, '%' . $keyword . '%');
            }
            push(@where_clause, '(' . join(' OR ', @value_like_clauses) . ')');
        }
    };

    my $AddNotLikeField = sub
    {
        my ($field_name, $item_list) = @_;
        $item_list ||= [];
        if (1 == @$item_list)
        {
            push(@where_clause, $field_name . ' NOT LIKE ?');
            #+FIXME: escape '_' and '%' in keyword?
            push(@bind_values, '%' . $item_list->[0] . '%');
        }
        elsif (@$item_list)
        {
            my @value_like_clauses;
            foreach my $keyword (@$item_list)
            {
                push(@value_like_clauses, $field_name . ' NOT LIKE ?');
                #+FIXME: escape '_' and '%' in keywords?
                push(@bind_values, '%' . $keyword . '%');
            }
            push(@where_clause, '(' . join(' AND ', @value_like_clauses) . ')');
        }
    };

    my $AddJoinField = sub
    {
        my ($table_link_column, $other_table, $other_table_link_column, $other_table_selection_field, $item_list) = @_;
        $item_list ||= [];
        if (1 == @$item_list)
        {
            $table_clause .= ' JOIN ' . $other_table . ' ON (' . $table_link_column . ' = ' . $other_table_link_column . ')';
            push(@where_clause, $other_table_selection_field . ' = ?');
            push(@bind_values, $item_list->[0]);
        }
        elsif (@$item_list)
        {
            $table_clause .= ' JOIN ' . $other_table . ' ON (' . $table_link_column . ' = ' . $other_table_link_column . ')';
            push(@where_clause, $other_table_selection_field . ' IN (' . ('?, ' x $#$item_list) . '?)');
            push(@bind_values, @$item_list);
        }
    };

    # family_id
    $AddEqualField->('f.id', $parameter_hash->{'family_id'});

    # family name keywords (case insensitive)
    $AddLikeField->('f.name', $parameter_hash->{'name'});
    $AddNotLikeField->('f.name', $parameter_hash->{'not_name'});

    # accession
    $AddCaseInsensitiveField->('f.accession', $parameter_hash->{'accession'});

    # level
    $AddEqualField->('f.level', $parameter_hash->{'level'});

    # type
    $AddEqualField->('f.type', $parameter_hash->{'type'});

    # family name keywords (case insensitive)
    $AddLikeField->('f.description', $parameter_hash->{'description'});

    # validated
    $AddEqualField->('f.validated', $parameter_hash->{'validated'});

    # black_listed
    if (defined($parameter_hash->{'black_listed'}) && ($parameter_hash->{'black_listed'} =~ m/^[01]$/))
    {
        $AddEqualField->('f.black_listed', [$parameter_hash->{'black_listed'}]);
    }

    # inferences
    $AddLikeField->('f.inference', $parameter_hash->{'inference'});

    # sequence count
    if ($parameter_hash->{'max_sequences'})
    {
        push(@where_clause, 'f.sequence_count <= ?');
        push(@bind_values, $parameter_hash->{'max_sequences'});
    }
    if ($parameter_hash->{'min_sequences'})
    {
        push(@where_clause, 'f.sequence_count >= ?');
        push(@bind_values, $parameter_hash->{'min_sequences'});
    }

    # plant_specific
    if (defined($parameter_hash->{'plant_specific'})
        && ('' ne $parameter_hash->{'plant_specific'}))
    {
        $AddEqualField->('f.plant_specific', $parameter_hash->{'plant_specific'});
    }
    if (defined($parameter_hash->{'max_plant_specific'})
        && ('' ne $parameter_hash->{'max_plant_specific'}))
    {
        push(@where_clause, 'f.plant_specific <= ?');
        push(@bind_values, $parameter_hash->{'max_plant_specific'});
    }
    if (defined($parameter_hash->{'min_plant_specific'})
        && ('' ne $parameter_hash->{'min_plant_specific'}))
    {
        push(@where_clause, 'f.plant_specific >= ?');
        push(@bind_values, $parameter_hash->{'min_plant_specific'});
    }

    # with_phylogeny
    if ($parameter_hash->{'with_phylogeny'})
    {
        push(@where_clause, 'EXISTS (SELECT TRUE FROM homologies h WHERE h.family_id = f.id LIMIT 1)');
    }
    elsif ($parameter_hash->{'without_phylogeny'})
    {
        push(@where_clause, 'NOT EXISTS (SELECT TRUE FROM homologies h WHERE h.family_id = f.id LIMIT 1)');
    }

    # parent_id
    if ($parameter_hash->{'parent_ids'} && @{$parameter_hash->{'parent_ids'}})
    {
        $AddJoinField->('f.id', 'family_relationships_cache frcp', 'frcp.subject_family_id', 'frcp.object_family_id', $parameter_hash->{'parent_ids'});
        push(@where_clause, 'frcp.level_delta = -1');
    }

    # child_id
    if ($parameter_hash->{'child_ids'} && @{$parameter_hash->{'child_ids'}})
    {
        $AddJoinField->('f.family_id', 'family_relationships_cache frcc', 'frcc.subject_family_id', 'frcc.object_family_id', $parameter_hash->{'child_ids'});
        push(@where_clause, 'frcc.level_delta = 1');
    }


    my %SPECIES_SELECTION_METHOD = (
        'unfilter_species' => sub {
        },
        'any_species'      => sub {
            # species_ids
            $AddJoinField->('f.id', 'sequence_count_by_families_species_cache scbfs', 'scbfs.family_id', 'scbfs.sequence_count > 0 AND scbfs.species_id', $species_ids);
        },
        'all_species'      => sub {
            if (@$species_ids)
            {
                push(@where_clause, '(SELECT ' . scalar(@$species_ids) . ' = COUNT(DISTINCT scbfs.species_id) FROM sequence_count_by_families_species_cache scbfs WHERE scbfs.family_id = f.id AND scbfs.species_id IN (?' . (',?' x $#$species_ids) . '))');
                push(@bind_values, @$species_ids);
            }
        },
        'species_specific' => sub {
            # convert species_id into taxonomy_id
            my %SPECIES_TO_TAXON;
            my $species_list = [Greenphyl::Species->new(GetDatabaseHandler(), {'selectors' => { 'display_order' => ['>', 0], },})];
            foreach my $species (@$species_list)
            {
                $SPECIES_TO_TAXON{$species->id} = $species->taxonomy_id;
            }
            push(@$taxonomy_ids, map { $SPECIES_TO_TAXON{$_} } @$species_ids);
            # taxonomy_id
            $AddEqualField->('f.taxonomy_id', $taxonomy_ids);
        },
    );
    my $species_selection = GetParameterValues('species_selection');
    if ($species_selection)
    {
        if (exists($SPECIES_SELECTION_METHOD{$species_selection}))
        {
            $SPECIES_SELECTION_METHOD{$species_selection}->();
        }
        else
        {
            PrintDebug("WARNING: Invalid species selection method (ignored): '$species_selection'");
        }
    }
    else
    {
        # species_ids
        $AddJoinField->('f.id', 'sequence_count_by_families_species_cache scbfs', 'scbfs.family_id', 'scbfs.species_id', $species_ids);
        # taxonomy_id
        $AddEqualField->('f.taxonomy_id', $taxonomy_ids);
    }


    # sequence_ids
    $AddJoinField->('f.id', 'families_sequences fs', 'fs.family_id', 'fs.sequence_id', $parameter_hash->{'sequence_ids'});

    # ipr_ids
    $AddJoinField->('f.id', 'families_ipr_species_cache fis', 'fis.family_id', 'fis.ipr_id', $parameter_hash->{'ipr_id'});

    # with uniprot
    if ($parameter_hash->{'uniprot'})
    {
        my $uniprot_db = Greenphyl::SourceDatabase->new($dbh, {'selectors' => {'name' => $Greenphyl::SourceDatabase::SOURCE_DATABASE_UNIPROT}});
        push(@where_clause, 'EXISTS(SELECT TRUE FROM dbxref_families fd JOIN dbxref dx ON dx.id = fd.dbxref_id WHERE fd.family_id = f.id AND dx.db_id = ' . $uniprot_db->id . ' LIMIT 1)');
    }

    # UniProt DBXRef (with hack for double-join)
    $AddJoinField->('f.id', 'families_sequences fs', 'fs.family_id) JOIN dbxref_sequences sdx ON (fs.sequence_id = sdx.sequence_id', 'sdx.dbxref_id', $parameter_hash->{'sequence_dbxref_id'});

    # dbxref_ids
    $AddJoinField->('f.id', 'dbxref_families fdx', 'fdx.family_id', 'fdx.dbxref_id', $parameter_hash->{'dbxref_id'});

    # go_ids
    my $go_selection = GetParameterValues('go_selection');
    if ($parameter_hash->{'go_id'} && @{$parameter_hash->{'go_id'}})
    {
        if ($go_selection && ($go_selection =~ m/^and$/i))
        {
            # AND
            foreach my $go_id (@{$parameter_hash->{'go_id'}})
            {
                if ($go_id =~ m/^\d+$/)
                {
                    push(@where_clause, "EXISTS(SELECT TRUE FROM families_go_cache fgc WHERE fgc.family_id = f.id AND fgc.go_id = $go_id LIMIT 1)");
                }
            }
        }
        else
        {
            # OR
            $AddJoinField->('f.id', 'families_go_cache fgc', 'fgc.family_id', 'fgc.go_id', @{$parameter_hash->{'go_id'}});
        }
    }

    # ipr_selection
    my $ipr_selection = GetParameterValues('ipr_selection');
    my %IPR_SELECTION_METHOD = (
        'without_ipr'            => sub {
            push(@where_clause, 'NOT EXISTS(SELECT TRUE FROM families_ipr_cache ips WHERE ips.family_id = f.id LIMIT 1)');
        },
        'without_specific_ipr'   => sub {
            push(@where_clause, 'NOT EXISTS(SELECT TRUE FROM families_ipr_cache ips WHERE ips.family_id = f.id AND ips.family_specific = 1 LIMIT 1)');
        },
        'with_specific_ipr' => sub {
            my $ipr_threshold = GetParameterValues('ipr_threshold');
            if ($ipr_threshold && ($ipr_threshold =~ m/^\d+(?:\.\d*)?$/))
            {
                my %IPR_OPERATOR = (
                    'greater'          => '>',
                    'greater_or_equal' => '>=',
                    'less_or_equal'    => '<=',
                    'less'             => '<',
                );
                my $ipr_threshold_operator = lc(GetParameterValues('ipr_threshold_operator') || '');
                if (!exists($IPR_OPERATOR{$ipr_threshold_operator}))
                {
                    $ipr_threshold_operator = 'greater_or_equal';
                }
                $ipr_threshold_operator = $IPR_OPERATOR{$ipr_threshold_operator};

                push(@where_clause, 'EXISTS(SELECT TRUE FROM families_ipr_cache ips WHERE ips.family_id = f.id AND ips.family_specific = 1 AND ips.percent ' . $ipr_threshold_operator . ' ? LIMIT 1)');
                push(@bind_values, $ipr_threshold);
            }
            else
            {
                push(@where_clause, 'EXISTS(SELECT TRUE FROM families_ipr_cache ips WHERE ips.family_id = f.id AND ips.family_specific = 1 LIMIT 1)');
            }
        },
    );
    if ($ipr_selection && exists($IPR_SELECTION_METHOD{$ipr_selection}))
    {
        $IPR_SELECTION_METHOD{$ipr_selection}->();
    }

    # get options
    my %options;
    foreach my $option_name (@SQL_OPTION_ORDER)
    {
        if (exists($parameter_hash->{$option_name})
            && defined($parameter_hash->{$option_name}))
        {
            $options{$option_name} = $parameter_hash->{$option_name};
        }
    }

    return (
        [Greenphyl::Family->GetDefaultMembers('f')],
        $table_clause,
        \@where_clause,
        \@bind_values,
        \%options,
    );
}

=pod

=head2 LoadFamilies

B<Description>: Returns an array of composite objects containing families.
The maximum number of families that function can return is $FAMILY_LIMIT.
Note that the parameter (command-line, post, get) 'family_page' can be used to
get the next range of families when the maximum number of families returned
is reached.
See PrepareFamilyLoadingQuery for supported family selection parameters.

B<ArgsCount>: 0-1

=over 4

=item $parameter_hash: (hash ref) (U)

A hash of parameters just like the one returned by GetFamilyLoadingParameters().

=back


B<Return>: (array ref)

An array of Greenphyl::CompositeObject objects containing families.

=cut

sub LoadFamilies
{
    # get parameters
    my ($parameter_hash) = @_;
    $parameter_hash ||= GetFamilyLoadingParameters();

    my $dbh = GetDatabaseHandler();
    my $sql_query;

    my ($family_members, $table_clause, $where_clause, $bind_values, $options) = PrepareFamilyLoadingQuery($parameter_hash);

    # stringify options
    my $sql_options = '';
    foreach my $option_name (@SQL_OPTION_ORDER)
    {
        if (exists($options->{$option_name})
            && defined($options->{$option_name}))
        {
            $sql_options .= ' ' . $option_name . ' ' . $options->{$option_name};
        }
    }


    # get requested families
    $sql_query = "SELECT\n" . join(",\n  ", @$family_members)
        . " FROM\n  $table_clause\n"
        . (@$where_clause ? " WHERE\n  " . join("\n  AND ", @$where_clause) : '')
        . " $sql_options;";
    PrintDebug("Family loading query: $sql_query\nBindings: " . join(', ', @$bind_values));
    my $sql_families = $dbh->selectall_arrayref($sql_query, { 'Slice' => {} }, @$bind_values)
        or confess $dbh->errstr;
    PrintDebug("Got " . scalar(@$sql_families) . " records");
    if ($FAMILY_LIMIT == scalar(@$sql_families))
    {
        PrintDebug("WARNING: maximum number of families ($FAMILY_LIMIT) reached!");
    }
    my $families = {};
    foreach my $sql_family (@$sql_families)
    {
        $families->{$sql_family->{'id'}} ||= Greenphyl::Family->new($dbh, {'load' => 'none', 'members' => $sql_family});
        $families->{$sql_family->{'id'}}->discardChanges();
    }

    my $sorted_families = [sort {$a->accession cmp $b->accession;} values(%$families)];

    return $sorted_families;

    #+Val: family can be dumped in HTML/JSON now
    #my @composite_families;
    #foreach my $family (sort {$a->id <=> $b->id} values(%$families))
    #{
    #    # wrap families in composite object in order to be able to dump them in HTML or JSON
    #    push(@composite_families, Greenphyl::CompositeObject->new($family));
    #}
    #
    #return \@composite_families;
}


=head2 AddOrphanSpeciesFilter

B<Description>: Check if a species filter has been set and activate the filter
if so.

B<ArgsCount>: 1

=over 4

=item $selectors: (hash) (R)

the selectors that will be initially used to fetch sequences.

=back

B<Return>: (hash ref)

The updated selectors.

=cut

sub AddOrphanSpeciesFilter
{
    my ($selectors) = @_;
    my $dbh = GetDatabaseHandler();

    my @species_ids = GetParameterValues('species_id', undef, '[,;\s]+');

    # convert species codes into DB IDs
    if (my @species_codes = GetParameterValues('species_code', undef, '[,;\s]+'))
    {
        my $sql_query = "SELECT id FROM species WHERE code IN (" . ('?, ' x $#species_codes) . "?);";
        PrintDebug("Species selection query: $sql_query\nBindings: " . join(', ', @species_codes));
        my $named_species_ids = $dbh->selectcol_arrayref($sql_query, undef, @species_codes)
            or confess $dbh->errstr;
        if (@$named_species_ids)
        {
            push(@species_ids, @$named_species_ids);
        }
    }

    if (@species_ids)
    {
        if (1 == @species_ids)
        {
            $selectors->{'species_id'} = $species_ids[0],
        }
        elsif (@species_ids)
        {
            $selectors->{'species_id'} = ['IN', @species_ids],
        }
    }

    return $selectors;
}


=pod

=head2 AddOrphanLevelFilter

B<Description>: Check if a classification filter has been set and activate the
filter if so.

B<ArgsCount>: 1

=over 4

=item $selectors: (hash) (R)

the selectors that will be initially  used to fetch sequences.

=back

B<Return>: (hash ref)

The updated selectors.

=cut

sub AddOrphanLevelFilter
{
    my ($selectors) = @_;

    my @level_filter = GetParameterValues('level', undef, '[,;\s]+');

    my @levels;
    foreach my $level (@level_filter)
    {
        # only keep valid levels
        if ($level =~ m/^$LEVEL_VALIDATION_REGEXP$/ )
        {
            push(@levels, $level);
        }
    }

    if (1 == @levels)
    {
        $selectors->{'NOT EXISTS'} = 'SELECT TRUE
                                      FROM families fi
                                           JOIN families_sequences fs ON (fs.family_id = fi.family_id)
                                      WHERE fs.sequence_id = sequences.id
                                            AND fi.level = ' . $levels[0] . '
                                      LIMIT 1',
    }
    elsif (@levels)
    {
        $selectors->{'NOT EXISTS'} = 'SELECT TRUE
                                      FROM families fi
                                           JOIN families_sequences fs ON (fs.family_id = fi.family_id)
                                      WHERE fs.sequence_id = sequences.id
                                            AND fi.level IN ('  . join(', ', @levels) . ')
                                      LIMIT 1',
    }

    # check if the sequence must not be orphan at specified level(s)
    my @has_level_filter = GetParameterValues('has_level', undef, '[,;\s]+');

    @levels = ();
    foreach my $level (@has_level_filter)
    {
        # only keep valid levels
        if ($level =~ m/^$LEVEL_VALIDATION_REGEXP$/ )
        {
            push(@levels, $level);
        }
    }

    if (1 == @levels)
    {
        $selectors->{'EXISTS'} = 'SELECT TRUE
                                      FROM families fi
                                           JOIN families_sequences fs ON (fs.family_id = fi.family_id)
                                      WHERE fs.sequence_id = sequences.id
                                            AND fi.level = ' . $levels[0] . '
                                      LIMIT 1',
    }
    elsif (@levels)
    {
        $selectors->{'EXISTS'} = 'SELECT TRUE
                                      FROM families fi
                                           JOIN families_sequences fs ON (fs.family_id = fi.family_id)
                                      WHERE fs.sequence_id = sequences.id
                                            AND fi.level IN ('  . join(', ', @levels) . ')
                                      LIMIT 1',
    }

    return $selectors;
}


=pod

=head2 GetOrphanQueryParameters

B<Description>: Prepare selectors to load orphan sequences.

Orphan selection parameters handled:
-'species_id': one or more species identifier (DB) separated by coma or
 semicolon or spaces.
-'species_code': one or more species codes (3-5 characters) separated by coma or
 semicolon or spaces.
-'level': one or more family level separated by coma or semicolon or spaces.
  Matching orphan sequences will not belong to any family at these levels.
-'has_level': one or more family level separated by coma or semicolon or spaces.
  Matching orphan sequences will belong to families at these levels.

B<ArgsCount>: 0

B<Return>: (hash ref)

A selectors hash.

=cut

sub GetOrphanQueryParameters
{
    my $selectors = {};

    # default: all orphans
    $selectors->{'NOT EXISTS'} = 'SELECT TRUE
                                  FROM families_sequences fs
                                  WHERE fs.sequence_id = sequences.id
                                  LIMIT 1';
    # add filters...
    # -species filter
    $selectors = AddOrphanSpeciesFilter($selectors);

    # -class filter
    $selectors = AddOrphanLevelFilter($selectors);

    my $query_parameters =
        {
            'selectors' => $selectors,
            'sql' =>
                {
                    'ORDER BY' =>
                        [
                            'sequences.accession ASC',
                            'sequences.id ASC',
                        ],
                },
        };

    return $query_parameters;
}


=pod

=head2 LoadOrphans

B<Description>: Returns an array of orphan sequence objects.

B<ArgsCount>: 0

B<Return>: (array ref)

An array of Greenphyl::CompositeObject objects containing families.

=cut

sub LoadOrphans
{
    my $query_parameters = GetOrphanQueryParameters();
    my $orphans = Greenphyl::Sequence->new(GetDatabaseHandler(), $query_parameters);

    return $orphans;
}


=pod

=head1 FUNCTIONS

=head2 GetFamilyListParameters

B<Description>: Returns a parameter hash.

B<ArgsCount>: 0

B<Return>: (hash ref)

Template parameters.

=cut

sub GetFamilyListParameters
{

    # get requested columns
    my $column_index = 'a';
    my $columns = [];
    my $dump_columns = {};
    $dump_columns->{$column_index++ . '. Family'} = 'accession';

    # %FAMILY_INFO_COLUMNS keys are values of family_info inputs from
    # family_tools/information_selection.tt
    # label    => label to display for the column in dumps
    # field    => "path" to the sequence object field for dumps
    # selector => select family_list.tt columns
    my %FAMILY_INFO_COLUMNS = (
        'name' =>
            {
                'label' => 'Family name',
                'field' => 'name',
                'selector' => 'name',
            },
        # 'uniprot' =>
        #     {
        #         'label' => 'UniProt',
        #         'field' => 'uniprot.*',
        #         'selector' => 'uniprot',
        #     },
        # 'kegg' =>
        #     {
        #         'label' => 'KEGG',
        #         'field' => 'kegg.*',
        #         'selector' => 'kegg',
        #     },
        # 'ipr' =>
        #     {
        #         'label' => 'InterPro',
        #         'field' => 'ipr.*',
        #         'selector' => 'ipr',
        #     },
        # 'annotation' =>
        #     {
        #         'label' => 'Annotation',
        #         'field' => 'annotation',
        #         'selector' => 'annotation',
        #     },
        # 'go' =>
        #     {
        #         'label' => 'Gene Ontology',
        #         'field' => 'go.*',
        #         'selector' => 'go',
        #     },
        # 'ec' =>
        #     {
        #         'label' => 'EC',
        #         'field' => 'ec.*',
        #         'selector' => 'ec',
        #     },
        # 'pubmed' =>
        #     {
        #         'label' => 'PubMed',
        #         'field' => 'pubmed.*',
        #         'selector' => 'pubmed',
        #     },
        # 'filter_status' =>
        #     {
        #         'label' => 'Filter Status',
        #         'field' => 'filtered',
        #         'selector' => 'filter_status',
        #     },
    );

    my @family_infos = GetParameterValues('family_info');
    foreach my $family_infos (@family_infos)
    {
        foreach my $family_info (split(/\s*,\s*/, $family_infos))
        {
            if (exists($FAMILY_INFO_COLUMNS{$family_info}))
            {
                # add column info for dump
                my $column_name = $column_index++ . '. ' . $FAMILY_INFO_COLUMNS{$family_info}->{'label'};
                $dump_columns->{$column_name} = $FAMILY_INFO_COLUMNS{$family_info}->{'field'};
                # add column for family list template
                push(@$columns, $FAMILY_INFO_COLUMNS{$family_info}->{'selector'});
            }
            else
            {
                PrintDebug("WARNING: unknown family info column: '$family_info'");
            }
        }
    }

    # prepare parameters
    my $parameters = {
        'content'   => 'families/family_list.tt',
        'content_id' => 'content_' . $$,
        'columns'   => $columns,
        'dump_data' =>
        {
            'links' => [
                {
                    'label'       => 'Excel',
                    'format'      => 'excel',
                    'file_name'   => 'families.xls',
                    'namespace'   => 'excel',
                },
                {
                    'label'       => 'CSV',
                    'format'      => 'csv',
                    'file_name'   => 'families.csv',
                    'namespace'   => 'csv',
                },
                {
                    'label'       => 'XML',
                    'format'      => 'xml',
                    'file_name'   => 'families.xml',
                    'namespace'   => 'xml',
                },
            ],
            'parameters'  => {'columns' => EncodeDumpParameterValue($dump_columns),},
            'object_type' => 'family',
            'fields'      => 'family_id',
            'mapping'     => {'family_id' => 'id',},
            'mode'        => 'form',
        },
        'paged'           => 1,
        'with_form_field' => 'checkbox',
        'with_dump_links' => 1,
    };

    return $parameters;
}


=pod

=head2 GetFamilyParameters

B<Description>: Returns a parameter hash.

B<ArgsCount>: 0

B<Return>: (hash ref)

Template parameters.

=cut

sub GetFamilyParameters
{
    # prepare parameters
    my $parameters = {
        'content'   => 'families/family_id_card.tt',
        'content_id' => 'content_' . $$,
    };

    return $parameters;
}


=pod

=head2 GetFamilyList

B<Description>: Returns an array of Greenphyl::Family objects.

B<ArgsCount>: 1

=over 4

=item $output_format: (string) (R)

the requested output format.

B<Return>: (array ref, hash ref)

Returns an array of Greenphyl::CompositeObject objects. Each composit object
contains a Greenphyl::Family objects;

The hash ref is just a dump parameter hash.

=cut

sub GetFamilyList
{
    my $families = LoadFamilies();
    my $parameters = GetFamilyListParameters();
    $parameters->{'families'} = $families;

    return ($families, $parameters);
}




=pod

=head2 GetFamilyRelationship

B<Description>: Returns JSON.

B<ArgsCount>: 1

=over 4

=item $output_format: (string) (R)

the requested output format.

B<Return>: (array ref, hash ref)

Returns an array of Greenphyl::CompositeObject objects. Each composit object
contains a Greenphyl::Family objects;

The hash ref is just a dump parameter hash.

=cut

sub GetFamilyRelationship
{
    my $family = LoadFamily();
    my $parameters = GetFamilyParameters();

    my $family_relationship_data = GenerateFamilySankeyJSONData($family);

    $parameters->{'content'}             = 'families/family_relationship_diagram.tt';
    $parameters->{'family'}              = $family;
    $parameters->{'family_relationship'} = $family_relationship_data;
    # $parameters->{'javascript'} = ['raphael', 'raphael-zpd', 'g.raphael', 'g.bar', 'chart', 'relationship_graph', 'families', ],
    $parameters->{'javascript'} = ['d3-sankey.min', ],
    my $output = Greenphyl::Web::RenderHTMLFullPage($parameters);

    return ($output, $parameters);
}




=pod

=head2 GetCustomFamilyRelationship

B<Description>: Returns JSON.

B<ArgsCount>: 1

=over 4

=item $output_format: (string) (R)

the requested output format.

B<Return>: (array ref, hash ref)

Returns an array of Greenphyl::CompositeObject objects. Each composit object
contains a Greenphyl::Family objects;

The hash ref is just a dump parameter hash.

=cut

sub GetCustomFamilyRelationship
{
    my $family = LoadCustomFamily();
    my $parameters = GetFamilyParameters();

    # my $family_relationship_data = GenerateFamilyRelationshipJSONData($family);
    my $family_relationship_data = GenerateFamilySankeyJSONData($family);

    $parameters->{'content'}             = 'families/family_relationship_diagram.tt';
    $parameters->{'family'}              = $family;
    $parameters->{'family_relationship'} = $family_relationship_data;
    # $parameters->{'javascript'} = ['raphael', 'raphael-zpd', 'g.raphael', 'g.bar', 'chart', 'relationship_graph', 'families', ],
    $parameters->{'javascript'} = ['d3-sankey.min', ],
    my $output = Greenphyl::Web::RenderHTMLFullPage($parameters);

    return ($output, $parameters);
}




=pod

=head2 GetIPRFamilyRelationship

B<Description>: Returns JSON.

B<ArgsCount>: 1

=over 4

=item $output_format: (string) (R)

the requested output format.

B<Return>: (array ref, hash ref)

Returns an array of Greenphyl::CompositeObject objects. Each composit object
contains a Greenphyl::Family objects;

The hash ref is just a dump parameter hash.

=cut

sub GetIPRFamilyRelationship
{
    my $ipr_list = LoadIPR();
    my $parameters = GetFamilyParameters();

    my $family_relationship_data = GenerateIPRFamilyRelationshipJSONData($ipr_list);

    $parameters->{'body_id'}                 = 'ipr_distribution';
    $parameters->{'content'}                 = 'family_tools/ipr_family_distribution_diagram.tt';
    $parameters->{'ipr_list'}                = $ipr_list;
    $parameters->{'ipr_family_distribution'} = $family_relationship_data;
    $parameters->{'javascript'} = ['raphael', 'raphael-zpd', 'ipr', 'relationship_graph', 'families', ],
    my $output = Greenphyl::Web::RenderHTMLFullPage($parameters);

    return ($output, $parameters);
}




=pod

=head2 GetIPRFamilySpecificity

B<Description>: Returns JSON.

B<ArgsCount>: 1

=over 4

=item $output_format: (string) (R)

the requested output format.

B<Return>: (array ref, hash ref)

Returns an array of Greenphyl::CompositeObject objects. Each composit object
contains a Greenphyl::Family objects;

The hash ref is just a dump parameter hash.

=cut

sub GetIPRFamilySpecificity
{
    my $ipr_list   = LoadIPR();
    my $family     = LoadFamily();
    my $parameters = GetFamilyParameters();

    my @ipr_ids = map { $_->id } @$ipr_list;

    if (!@ipr_ids)
    {
        confess "ERROR: no valid IPR ID provided!\n";
    }

    # get IPR statistics (exclude current family)
    my @ipr_stats = Greenphyl::FamilyIPRStats->new(
        GetDatabaseHandler(),
        {
            'selectors' => {'ipr_id' => ['IN', @ipr_ids], 'family_id' => ['!=', $family->id]},
        }
    );

    # pick up only first level
    my @first_level_ipr_stats;
    foreach my $ipr_stats (@ipr_stats)
    {
        if ((1 == $ipr_stats->family->level) && ($ipr_stats->occurence))
        {
            push(@first_level_ipr_stats, $ipr_stats);
        }
    }

    # sort by occurences descending
    @first_level_ipr_stats = sort {$b->occurence <=> $a->occurence} @first_level_ipr_stats;

    $parameters->{'content'} = 'family_tools/ipr_specificity.tt';
    $parameters->{'family'}   = $family;
    $parameters->{'ipr_list'} = $ipr_list;
    $parameters->{'ipr_stats'} = \@first_level_ipr_stats;
    $parameters->{'columns'} = {'a. Family'=>'family','b. Occurence'=>'occurence','c. Percent'=>'percent'};

    return (\@first_level_ipr_stats, $parameters);
}


=pod

=head2 GenerateFamilyRelationshipData

B<Description>: Generate family relationship JSON data.

B<ArgsCount>: 1-2

=over 4

=item $family: (Greenphyl::Family) (R)

Current family object.

=item $parent_family: (Greenphyl::Family) (O)

The parent family object if one.

=back

B<Return>: (hash ref)

a hash containing the following keys:
-'id': current family database ID
-'accession': current family accession
-'children': an array ref containing hash refs similar to the current one
-'count': number of sequences in current family
-'common': a hash wich keys are family_id and values are hash:
  -'species': a hash which keys are species names and values are number of
    common sequences;
  -'count': total number of common sequences;
  -'first_10_seq': accession of the first 10 common sequences.
-'species': a hash which keys are species names and values are number of
    sequences in the family corresponding to that species;
-'selector': a CSS selector that can identify a corresponding DOM object;

=cut

sub GenerateFamilyRelationshipData
{
    my ($family, $parent_family) = @_;

    # common sequences with a given parent family if one
    my $family_species = {};
    foreach my $sequence (@{$family->sequences})
    {
        $family_species->{$sequence->species->name} ||= 0;
        $family_species->{$sequence->species->name}++;
    }
    my $child_families = [map { GenerateFamilyRelationshipData($_, $family) }    @{$family->getChildFamilies()}];

    my $family_type = $family->getObjectType(1);
    $family_type =~ s/^cached_//;
    my $tree_structure =
        {
            'id'        => $family->id,
            'label'     => $family->accession,
            'type'      => 'family-sequences',
            'count'     => $family->sequence_count,
            'children'  => $child_families,
            'species'   => $family_species,
            'selector'  => '.' . $family_type . '-' . $family->accession,
            'class'     => $family_type,
        };

    if ($parent_family)
    {
        my $common_sequences = $family->getCommonSequences($parent_family);
        my $common_10_accession = [];
        if (10 > scalar(@$common_sequences))
        {
            $common_10_accession = [map {$_->accession} @$common_sequences];
        }
        else
        {
            for (1..10)
            {
                if ($common_sequences && $common_sequences->[$_])
                {
                    push(@$common_10_accession, $common_sequences->[$_]->accession);
                }
            }
        }
        my $family_common_species = {};
        foreach my $sequence (@$common_sequences)
        {
            $family_common_species->{$sequence->species->name} ||= 0;
            $family_common_species->{$sequence->species->name}++;
        }
        $tree_structure->{'common'} =
            {
                $parent_family->id =>
                    {
                        'species'      => $family_common_species,
                        'count'        => scalar(@$common_sequences),
                        'first_10_seq' => $common_10_accession,
                    },
            };
    }
    else
    {
        $tree_structure->{'common'} = {};
    }

    return $tree_structure;
}


=head2 GenerateFamilyRelationshipJSONData

B<Description>: Generate family relationship JSON data.

B<ArgsCount>: 1

=over 4

=item $family: (Greenphyl::Family) (R)

Subject family object.

=back

B<Return>: (hash ref)

a hash containing the key 'JSON' containing a JSON string of the relationship
structure.

=cut

sub GenerateFamilyRelationshipJSONData
{
    my ($family) = @_;

    my $root_families = $family->getRelatedFamilies({'level' => 1,});
    if (!@$root_families)
    {
        $root_families = [$family];
    }

    my $json = to_json([map { GenerateFamilyRelationshipData($_) } @$root_families]);

    return {'JSON' => $json,};
}


=pod

=head2 GenerateFamilySankeyData

B<Description>: Generate family relationship JSON data.

B<ArgsCount>: 1

=over 4

=item $family: (Greenphyl::Family) (R)

Current family object.

=back

B<Return>: (hash ref)

a hash containing the following keys:

=cut

sub GenerateFamilySankeyData
{
    my ($family, $lineage) = @_;

    # Common sequences with a given parent family if one.
    my $family_species = {};
    foreach my $sequence (@{$family->sequences})
    {
        $family_species->{$sequence->species->name} ||= 0;
        $family_species->{$sequence->species->name}++;
    }

    my $sankey_data = {
        'nodes' => {},
        'links' => {},
    };
    
    my $family_type = $family->getObjectType(1);
    $family_type =~ s/^cached_//;
    $sankey_data->{'nodes'}->{$family->id} = {
        'id'        => $family->id,
        'lineage'   => ($lineage ? "$lineage." : '') . $family->id,
        'accession' => $family->accession,
        'name'      => $family->name,
        'level'     => $family->level,
        'type'      => 'family-sequences',
        'count'     => $family->sequence_count,
        'species'   => $family_species,
        'selector'  => '.' . $family_type . '-' . $family->accession,
        'class'     => $family_type,
    };
    
    foreach my $child_family (@{$family->getChildFamilies()})
    {
        my $common_sequences = $family->getCommonSequences($child_family);
        $sankey_data->{'links'}->{$family->id . '-' . $child_family->id} = {
            'source' => $family->id,
            'target' => $child_family->id,
            'value' => scalar(@$common_sequences),
            #+FIXME: add 10 common sequences and common species
        };
        my $child_sankey_data = GenerateFamilySankeyData($child_family, $sankey_data->{'nodes'}->{$family->id}->{'lineage'});
        $sankey_data = MergeSankeyData($sankey_data, $child_sankey_data);
    }

    return $sankey_data;
}


=pod

=head2 MergeSankeyData

B<Description>: .

B<ArgsCount>: 1-2

=over 4

=item $family: (Greenphyl::Family) (R)

Current family object.

=item $parent_family: (Greenphyl::Family) (O)

The parent family object if one.

=back

B<Return>: (hash ref)

a hash containing the following keys:

=cut

sub MergeSankeyData
{
    my ($sankey_data_1, $sankey_data_2) = @_;

    my $sankey_data = {%$sankey_data_1};

    foreach my $node (keys(%{$sankey_data_2->{'nodes'}}))
    {
        if (!exists($sankey_data->{'nodes'}->{$node}))
        {
            $sankey_data->{'nodes'}->{$node} = $sankey_data_2->{'nodes'}->{$node};
        }
    }

    foreach my $link (keys(%{$sankey_data_2->{'links'}}))
    {
        if (!exists($sankey_data->{'links'}->{$link}))
        {
            $sankey_data->{'links'}->{$link} = $sankey_data_2->{'links'}->{$link};
        }
    }

    return $sankey_data;
}


=head2 GenerateFamilySankeyJSONData

B<Description>: Generate family relationship JSON data.

B<ArgsCount>: 1

=over 4

=item $family: (Greenphyl::Family) (R)

Subject family object.

=back

B<Return>: (hash ref)

a hash containing the key 'JSON' containing a JSON string of the relationship
structure.

=cut

sub GenerateFamilySankeyJSONData
{
    my ($family) = @_;

    # Get a list of level-1 families first.
    my $root_families = $family->getRelatedFamilies({'level' => 1,});
    if (!@$root_families)
    {
        # No family above, use this one as level 1.
        $root_families = [$family];
    }

    # Get sankey data.
    my $sankey_data = {
        'nodes' => {},
        'links' => {},
    };

    foreach my $root_family (@$root_families)
    {
      my $sankey_root_family_data = GenerateFamilySankeyData($root_family);
      $sankey_data = MergeSankeyData($sankey_data, $sankey_root_family_data);
    }
    
    # Compute family count by level.
    my $level_counts = [0, 0, 0, 0];
    foreach my $node (values(%{$sankey_data->{'nodes'}}))
    {
        $level_counts->[$node->{'level'}]++;
    }

    # Generate sankey structure used by D3JS (slicely different from the one
    # returned by GenerateFamilySankeyData).
    my $json = to_json({
        'nodes' => [values(%{$sankey_data->{'nodes'}})],
        'links' => [values(%{$sankey_data->{'links'}})],
        'level_counts' => $level_counts,
    });

    return {'JSON' => $json,};
}


=pod

=head2 GenerateFamilyCompositionData

B<Description>: Generate family composition data.

B<ArgsCount>: 1

=over 4

=item $family: (Greenphyl::Family) (R)

Family object.

=back

B<Return>: (hash ref)

hash of parameters for the template.

=cut

sub GenerateFamilyCompositionData
{
    my ($family) = @_;
    my $dbh = GetDatabaseHandler();

    my %sequence_counts = map
        {$_->species_id => $_}
        (
            Greenphyl::SequenceCount->new(
                $dbh,
                {
                    'selectors' => { 'family_id' => $family->id, }
                },
            )
        )
    ;

    # compute counts without splice forms
    # get species order
    my @species_order = map {$_->species} (values(%sequence_counts));
    @species_order =
        sort
        {
            return (($a->display_order <=> $b->display_order) || ($a->organism cmp $b->organism));
        }
        @species_order
    ;

    return
    {
        'sequence_counts'           => \%sequence_counts,
        'species_order'             => \@species_order,
        'filters'                   => $FILTERS,
    };
}


=pod

=head2 GenerateCustomFamilyCompositionData

B<Description>: Generate custom family composition data.

B<ArgsCount>: 1

=over 4

=item $family: (Greenphyl::CustomFamily) (R)

Custom family object.

=back

B<Return>: (hash ref)

hash of parameters for the template.

=cut

sub GenerateCustomFamilyCompositionData
{
    my ($family) = @_;
    my $dbh = GetDatabaseHandler();

    my %sequence_counts;
    my %locus_tracker;
    foreach my $sequence (@{$family->sequences})
    {
        my $species_key = $sequence->species_id;
        if ($sequence->species->code eq GP('NONUNIPROT_SPECIES_CODE'))
        {
            $species_key = $sequence->species_id . ' ' . $sequence->species->organism;
        }
        # check if species count has been initialized
        if (!exists($sequence_counts{$species_key}))
        {
            # create a fake species count
            $sequence_counts{$species_key} =
                Greenphyl::SequenceCount->new(
                    undef,
                    {
                        'load' => 'none',
                        'members' => {
                            'family_id'      => $family->id,
                            'sequence_count' => 0,
                            'sequence_count_without_splice' => 0,
                            'species'        => $sequence->species,
                            'species_id'     => $sequence->species->id,
                            'species_code'   => $sequence->species->code,
                            'species_name'   => $sequence->species->organism,
                            'taxonomy_id'    => $sequence->species->taxonomy_id,
                            'family'         => $family,
                        },
                    },
                )
            ;
        }
        # manage splice count
        if (!exists($locus_tracker{$sequence->locus}))
        {
            my $nosplice_count = $sequence_counts{$species_key}->sequence_count_without_splice();
            $sequence_counts{$species_key}->sequence_count_without_splice(++$nosplice_count);
            $locus_tracker{$sequence->locus} = 1;
        }
        my $species_sequence_count = $sequence_counts{$species_key}->sequence_count();
        $sequence_counts{$species_key}->sequence_count(++$species_sequence_count);
    }

    # compute counts without splice forms
    # get species order
    my @species_order = map {$_->species} (values(%sequence_counts));
    @species_order =
        sort
        {
            if ((0 < $a->display_order) && (0 < $b->display_order))
            {
                return $a->display_order <=> $b->display_order;
            }
            
            if ((0 < $a->display_order) && (0 >= $b->display_order))
            {
                return -1;
            }
            elsif ((0 >= $a->display_order) && (0 < $b->display_order))
            {
                return 1;
            }
            
            return (
                ($b->display_order <=> $a->display_order)
                || ($a->organism cmp $b->organism));
        }
        @species_order
    ;

    return
    {
        'sequence_counts'           => \%sequence_counts,
        'species_order'             => \@species_order,
        'filters'                   => $FILTERS,
    };
}


=pod

=head2 GenerateFamilyProteinDomainData

B<Description>: Generate family protein domain data.

B<ArgsCount>: 1

=over 4

=item $family: (Greenphyl::Family) (R)

Family object.

=back

B<Return>: (hash ref)

hash of parameters for the template.

=cut

sub GenerateFamilyProteinDomainData
{
    my ($family) = @_;

    # get IPR threshold
    my $ipr_threshold = $FAMILY_IPR_THRESHOLDS{$family->level};

    my (@ipr_family, @ipr_other);
    foreach my $ipr_stat (@{$family->getIPRStats()})
    {
        if ($ipr_stat->ipr->type eq 'Family')
        {
            # family IPR
            if (($ipr_stat->percent > $ipr_threshold)
                || $ipr_stat->family_specific)
            {
                push(@ipr_family, $ipr_stat);
            }
        }
        elsif ($ipr_stat->percent > $OTHER_IPR_THRESHOLD)
        {
            # other IPR
            if (($ipr_stat->percent > $ipr_threshold)
                && !$ipr_stat->family_specific)
            {
                push(@ipr_other, $ipr_stat);
            }
        }

    }

    # sort IPR
    @ipr_family = sort {$a->ipr->code cmp $b->ipr->code} @ipr_family;
    @ipr_other  = sort {$a->ipr->code cmp $b->ipr->code} @ipr_other;

    return {'ipr_family' => \@ipr_family, 'ipr_other' => \@ipr_other,};
}


=pod

=head2 GenerateFamilyProteinListData

B<Description>: Generate family protein list data.

B<ArgsCount>: 1

=over 4

=item $family: (Greenphyl::Family) (R)

Family object.

=back

B<Return>: (hash ref)

hash of parameters for the template.

=cut

sub GenerateFamilyProteinListData
{
    my ($family) = @_;
    return {'filters' => $FILTERS,};
}


=pod

=head2 GeneratePhylogenomicAnalysisData

B<Description>: Generate family phylogenomic analysis data. If a field is not
available, it will use an empty string.

B<ArgsCount>: 1

=over 4

=item $family: (Greenphyl::Family) (R)

Family object.

=back

B<Return>: (hash ref)

hash of parameters for the template.

=cut

sub GeneratePhylogenomicAnalysisData
{
    my ($family) = @_;

    my ($alignment_url, $masking1_url, $masking2_url, $filtration_url, $hmm_url, $alignment_tree_url, $tree_url, $tree_file);
    $alignment_url      = Greenphyl::Web::GetAlignmentURL($family, 'raw');
    $masking1_url       = Greenphyl::Web::GetAlignmentURL($family, 'masking1');
    $masking2_url       = Greenphyl::Web::GetAlignmentURL($family, 'masking2');
    $filtration_url     = Greenphyl::Web::GetAlignmentURL($family, 'filtered');
    $hmm_url            = Greenphyl::Web::GetFamilyFileURL($family, 'hmm');
    $alignment_tree_url = Greenphyl::Web::GetPhylogenyAnalyzesURL($family, 'nwk');
    $tree_url           = Greenphyl::Web::GetPhylogenyAnalyzesURL($family, 'xml');
    $tree_file          = $family->accession . GP('PHYLO_NEWICK_TREE_FILE_SUFFIX');
    
    # We only run analysis at level 1 so point to level 1 family for lower
    # families.
    my ($family_1, $level_1_alignment_url, $level_1_tree_url);
    if ($family->level && (1 < $family->level))
    {
        # Get level 1 family.
        $family_1 = $family->getRelatedFamilies({'level' => 1,});
        if ($family_1 && @$family_1 && $family_1->[0])
        {
            $family_1 = $family_1->[0];
            $level_1_alignment_url = Greenphyl::Web::GetAlignmentURL($family_1, 'raw');
            $level_1_tree_url      = Greenphyl::Web::GetPhylogenyAnalyzesURL($family_1, 'xml');
        }
    }

    # Check file sizes.
    my $base_url = GP('BASE_URL');
    my $base_path = GP('BASE_PATH') . '/htdocs';
    my $alignment_path = $alignment_url;
    $alignment_path =~ s/$base_url/$base_path/;
    my $tree_path = $tree_url;
    $tree_path =~ s/$base_url/$base_path/;
    # PrintDebug($tree_file);    
    return {
        'family_level_1'        => $family_1,
        'alignment_url'         => $alignment_url,
        'alignment_url_level_1' => $level_1_alignment_url,
        'alignment_file_size'   => -s $alignment_path,
        'masking1_url'          => $masking1_url,
        'masking2_url'          => $masking2_url,
        'filtration_url'        => $filtration_url,
        'hmm_url'               => $hmm_url,
        'alignment_tree_url'    => $alignment_tree_url,
        'tree_url'              => $tree_url,
        'tree_url_level_1'      => $level_1_tree_url,
        'tree_file_size'        => -s $tree_path,
        'tree_file'             => $tree_file,
    };
}


=pod

=head2 GenerateCustomPhylogenomicAnalysisData

B<Description>: Generate custom family phylogenomic analysis data. If a field is
not available, it will use an empty string.

B<ArgsCount>: 1

=over 4

=item $family: (Greenphyl::CustomFamily) (R)

Custom family object.

=back

B<Return>: (hash ref)

hash of parameters for the template.

=cut

sub GenerateCustomPhylogenomicAnalysisData
{
    my ($custom_family) = @_;

    my ($alignment_url, $tree_url, $alignment_tree_url) = ('', '', '');
    my ($masking1_url, $masking2_url, $filtration_url) = ('', '', '');
    my $meme_url = '';
    
    if ($custom_family->data)
    {
        my $alignment  = $custom_family->data->{'alignment'};
        my $masking1   = $custom_family->data->{'masking1'};
        my $masking2   = $custom_family->data->{'masking2'};
        my $filtration = $custom_family->data->{'filtration'};
        my $meme       = $custom_family->data->{'meme'};
        my $alignment_tree = $custom_family->data-> {'alignment_tree'};
        my $tree       = $custom_family->data-> {'tree'};

        $alignment_url      = ($alignment ? $alignment->url : '');
        $masking1_url       = ($masking1 ? $masking1->url : '');
        $masking2_url       = ($masking2 ? $masking2->url : '');
        $filtration_url     = ($filtration ? $filtration->url : '');
        $meme_url           = ($meme ? $meme->url : '');
        $alignment_tree_url = ($alignment_tree ? $alignment_tree->url : '');
        $tree_url           = ($tree ? $tree->url : '');
    }

    return {
        'alignment_url'      => $alignment_url,
        'masking1_url'       => $masking1_url,
        'masking2_url'       => $masking2_url,
        'filtration_url'     => $filtration_url,
        'meme_url'           => $meme_url,
        'alignment_tree_url' => $alignment_tree_url,
        'tree_url'           => $tree_url,
    };
}


=pod

=head2 GenerateGOData

B<Description>: Generate family GO stats.

B<ArgsCount>: 1

=over 4

=item $family: (Greenphyl::Family) (R)

Family object.

=back

B<Return>: (array ref)

array of composite objects based on GO objects + a 'percent' (family coverage)
field.

=cut

sub GenerateGOData
{
    my ($family) = @_;

    my @go_data;
    foreach my $family_go (Greenphyl::FamilyGO->new(GetDatabaseHandler(), { 'selectors' => {'family_id' => $family->id, 'percent' => ['>=', $FAMILY_GO_THRESHOLD]} }))
    {
        push(@go_data, Greenphyl::CompositeObject->new($family_go->go, $family_go->getHashValue()));
    }

    return \@go_data;
}


=pod

=head2 GetGOFamilyDistribution

B<Description>: Returns JSON.

B<ArgsCount>: 1

=over 4

=item $output_format: (string) (R)

the requested output format.

B<Return>: (array ref, hash ref)

Returns an array of Greenphyl::CompositeObject objects. Each composit object
contains a Greenphyl::Family objects;

The hash ref is just a dump parameter hash.

=cut

sub GetGOFamilyDistribution
{
    my $go_list = [LoadGO()];
    my $parameters = GetFamilyParameters();
    my $families;
    eval
    {
        $families = [LoadFamily()];
    };

    my $family_relationship_data = GenerateGOFamilyRelationshipJSONData($go_list, $families);

    $parameters->{'body_id'}                 = 'go_distribution';
    $parameters->{'content'}                 = 'family_tools/go_family_distribution_diagram.tt';
    $parameters->{'go_list'}                = $go_list;
    $parameters->{'go_family_distribution'} = $family_relationship_data;
    $parameters->{'javascript'} = ['raphael', 'raphael-zpd', 'relationship_graph', 'families', ],
    my $output = Greenphyl::Web::RenderHTMLFullPage($parameters);

    return ($output, $parameters);
}



=pod

=head2 FindFamilyOutputDirectory

B<Description>: returns the output directory of a family or nothing if the
directory was not found.

B<ArgsCount>: 2

=over 1

=item $family: (Greenphyl::Family) (R)

GreenPhyl family object.

=item $output_path: (string) (R)

Path where family output directories should be located.

=back

B<Return>: (string)

Path to the existing family output directory or undef if no directory found.

=cut

sub FindFamilyOutputDirectory
{
    my ($family, $output_path) = @_;

    # try family sub-path first
    my $family_directory = $output_path . '/' . $family->getSubPath();
    PrintDebug("Looking for family output directory...");
    if (-d $family_directory && -d "$family_directory/" . $family->accession)
    {
        $family_directory = "$family_directory/" . $family->accession;
    }
    if (!-d $family_directory)
    {
        PrintDebug("    -not found in '$family_directory'");
        # try with family ID
        $family_directory = $output_path . '/' . $family->id;
    }
    if (!-d $family_directory)
    {
        PrintDebug("    -not found in '$family_directory'");
        # try with family accession
        $family_directory = $output_path . '/' . $family->accession;
    }

    # check if directory has been found
    if (-d $family_directory)
    {
        PrintDebug("...family directory found in '$family_directory'");
        return $family_directory;
    }
    else
    {
        PrintDebug("    -not found in '$family_directory'\n...no family directory found!");
        return undef;
    }
}


=pod

=head2 FindFamilyFilePath

B<Description>: process initial, masked and filtered family alignments and
update statistics in database.

B<ArgsCount>: 3

=over 1

=item $family: (Greenphyl::Family) (R)

Family object.

=back

B<Return>: (boolean)

A true value if the family was updated, false otherwise.

=cut

sub FindFamilyFilePath
{
    my ($family, $family_directory, $file_suffix) = @_;
    my $family_file_path = $family_directory . '/' . $family . $file_suffix;
    PrintDebug("Trying to find family file '$family_file_path'");
    if (!-r $family_file_path)
    {
        # try with family ID
        $family_file_path = $family_directory . '/' . $family->id . $file_suffix;
        PrintDebug("    -not found. Trying '$family_file_path'");
    }
    if (!-r $family_file_path)
    {
        PrintDebug("...not found!");
        return undef;
    }

    PrintDebug("...family file found!");
    return $family_file_path;
}


=pod

=head2 LoopOnFamilyLists

B<Description>: Execute the given subroutine on each set of families that
matche the command line parameters for family selection.

B<ArgsCount>: 1

=over 1

=item $procedure: (code ref) (R)

Code to execute. The first parameter given to the code (through @_) is an array
ref of an array of family objects (usually contains only $FAMILY_LIMIT families)
and the second parameter is current family selection hash (hash ref that
contains family selection parameters).

=back

B<Return>: (integer)

number of processed families.

=cut

sub LoopOnFamilyLists
{
    my ($procedure) = @_;

    if (!$procedure || ('CODE' ne ref($procedure)))
    {
        confess "ERROR: LoopOnFamilyLists: invalid parameter: no reference to a procedure to execute!\n";
    }

    my $processed_families = 0;

    # Check for family selection
    my $family_selection_hash = GetFamilyLoadingParameters();
    my $selected_families = LoadFamilies($family_selection_hash);

    if (%$family_selection_hash && (0 == @$selected_families))
    {
        confess "ERROR: No family matches your criteria!\n";
    }

    # loop on all matching families
    my $family_page_counter = 0;
    while (@$selected_families)
    {
        # count processed families
        $processed_families += scalar(@$selected_families);

        # run custom procedure
        $procedure->($selected_families, $family_selection_hash);

        # get next range
        ++$family_page_counter;
        if ($family_selection_hash->{'LIMIT'})
        {
            $family_selection_hash->{'OFFSET'} = $family_page_counter * $family_selection_hash->{'LIMIT'};
        }
        $selected_families = LoadFamilies($family_selection_hash);
    }

    return $processed_families;
}


=pod

=head2 LoopOnFamilies

B<Description>: Execute a subroutine on each family object that matches the
family selection creteria provided by the command line.

B<ArgsCount>: 1

=over 1

=item $procedure: (code ref) (R)

Code to execute on each family. The first and only parameter given to the code
(through @_) is a family object reference.

=back

B<Return>: (integer)

number of processed families.

=cut

sub LoopOnFamilies
{
    my ($procedure) = @_;

    if (!$procedure || ('CODE' ne ref($procedure)))
    {
        confess "ERROR: LoopOnFamilies: invalid parameter: no reference to a procedure to execute!\n";
    }

    return LoopOnFamilyLists(sub {
        my $selected_families = shift();
        foreach my $family (@$selected_families)
        {
            # run custom procedure
            $procedure->($family);
        }
    });
}


=pod

=head2 FilterCustomFamilies

B<Description>: Filters custom families according to the given parameters or by
default to what current user is allowed to view.

B<ArgsCount>: 1-2

=over 1

=item $families: (array ref) (R)

Array of Greenphyl::CustomFamily to filter.

=item $filter: (hash ref) (O)

hash of filter conditions. Keys/values:
-read: true = return only custom families with read access by current user
-write: true = return only custom families with write access by current user
-owner: array of user_id that can be owners of the families
-access: array of $Greenphyl::CustomFamily::CUSTOM_FAMILY_ACCESS_* values

Default:
    {'read' => 1,}

=back

B<Return>: (array ref)

Array of filtered Greenphyl::CustomFamily.

=cut

sub FilterCustomFamilies
{
    my ($families, $filter) = @_;

    $filter ||= {
        'access' => [
            $Greenphyl::CustomFamily::CUSTOM_FAMILY_ACCESS_PUBLIC_READ,
            $Greenphyl::CustomFamily::CUSTOM_FAMILY_ACCESS_PUBLIC_WRITE,
        ],
    };

    if ('HASH' ne ref($filter))
    {
        confess "ERROR: Invalid filter parameter ('$filter')!\n";
    }

    $filter->{'access'} ||= [];
    $filter->{'owner'} ||= [];

    my @filtered_families;
    foreach my $custom_family (@$families)
    {
        # check access
        if (@{$filter->{'access'}}
            && (!grep {$_ eq $custom_family->access} (@{$filter->{'access'}})))
        {
            next;
        }

        # check owners
        if (@{$filter->{'owner'}}
            && (!grep {$_ == $custom_family->user_id} (@{$filter->{'owner'}})))
        {
            next;
        }

        # check read access
        if ($filter->{'read'} && !$custom_family->hasReadAccess())
        {
            next;
        }

        # check write access
        if ($filter->{'write'} && !$custom_family->hasWriteAccess())
        {
            next;
        }

        push(@filtered_families, $custom_family);
    }

    return \@filtered_families;
}


=pod

=head2 AddSequencesToCustomFamily

B<Description>: Adds the given list of sequences to a custom family.

B<ArgsCount>: 2-3

=over 1

=item $custom_family: (Greenphyl::CustomFamily) (R)

The custom family to work on.

=item $sequences: (array ref) (R)

an array of Greenphyl::AbstractSequence objects.

=item $sequence_relationships: (hash ref) (R)

A hash which keys are $sequences accessions and values are sequence.id (of
database).

=back

B<Return>: nothing

=cut

sub AddSequencesToCustomFamily
{
    my ($custom_family, $sequences, $sequence_relationships) = @_;
    
    $sequence_relationships ||= {};

    my $dbh = GetDatabaseHandler();
    
    foreach my $sequence_to_insert (@$sequences)
    {
        # duplicate sequences into custom_sequences if missing
        my @annotation_list;

        if ($sequence_to_insert->annotation)
        {
            push(@annotation_list, $sequence_to_insert->annotation);
        }

        if (!@annotation_list)
        {
            push(@annotation_list, '');
        }

        my $sql_query = "
            INSERT IGNORE INTO custom_sequences (
                id,
                accession,
                locus,
                splice,
                splice_priority,
                length,
                species_id,
                organism,
                annotation,
                polypeptide,
                user_id
            )
            VALUES (
                DEFAULT,
                ?,
                ?,
                ?,
                ?,
                ?,
                ?,
                ?,
                ?,
                ?,
                ?
            );
        ";
        my $sequence_type = $sequence_to_insert->getObjectType();
        $sequence_type =~ s/^Cached//;
        my @field_values = (
            $sequence_to_insert->accession,
            $sequence_to_insert->locus,
            $sequence_to_insert->splice,
            $sequence_to_insert->splice_priority,
            $sequence_to_insert->length,
            $sequence_to_insert->species_id,
            ('CustomSequence' eq $sequence_type ? $sequence_to_insert->organism : undef),
            join('; ', @annotation_list),
            $sequence_to_insert->polypeptide,
            $custom_family->user_id,
        );
        PrintDebug("SQL Query: $sql_query\nValues: " . join(', ', @field_values));
        my $insert_count = $dbh->do($sql_query, undef, @field_values)
            or confess "Failed to add sequence '$sequence_to_insert' to custom sequences:\n" . $dbh->errstr;
        # my $new_custom_sequence_id = $dbh->last_insert_id(undef, undef, 'custom_sequences', 'id')
        #     or confess "ERROR: Failed to retrieve custom sequence ID!\n";
        my $custom_sequence = Greenphyl::CustomSequence->new(
            $dbh,
            {
                'selectors' => {
                    'accession' => $sequence_to_insert->accession,
                    'user_id' => $custom_family->user_id,
                },
            },
        );
        if (!$custom_sequence)
        {
            Throw('error' => "Failed to retrieve custom sequence ID for sequence " . $sequence_to_insert->accession . "!");
        }
        
        if (0 != $insert_count)
        {
            # add sequence relationship
            if ($sequence_type eq 'Sequence')
            {
                # got a new custom sequence inserted from a regular sequence,
                # add sequence relationship
                $sql_query = "
                    INSERT INTO custom_sequences_sequences_relationships (
                        custom_sequence_id,
                        sequence_id,
                        type
                    )
                    VALUES (
                        " . $custom_sequence->id . ",
                        " . $sequence_to_insert->id . ",
                        'synonym'
                    );
                ";
            }
            elsif ($sequence_relationships->{$sequence_to_insert->accession}
                && ($sequence_relationships->{$sequence_to_insert->accession} =~ m/^\d{1,10}$/))
            {
                # a relationship has been specified explicitly
                $sql_query = "
                    INSERT INTO custom_sequences_sequences_relationships (
                        custom_sequence_id,
                        sequence_id,
                        type
                    )
                    VALUES
                    (
                        " . $custom_sequence->id . ",
                        " . $sequence_relationships->{$sequence_to_insert->accession} . ",
                        'associated'
                    );
                ";
                PrintDebug("SQL Query: $sql_query");
                $dbh->do($sql_query)
                    or Throw('error' => 'Failed to add sequences relationship between custom sequence ' . $custom_sequence->id . ' and sequence ' . $sequence_relationships->{$sequence_to_insert->accession} . ' (' . $sequence_to_insert->accession . ') !');
            }
        }


        $sql_query = "
            INSERT IGNORE INTO custom_families_sequences (
                custom_family_id,
                custom_sequence_id
            )
            VALUES (
                " . $custom_family->id . ",
                " . $custom_sequence->id . "
            );
        ";
        PrintDebug("SQL Query: $sql_query");
        $dbh->do($sql_query)
            or Throw('error' => 'Failed to add sequence ' . $custom_sequence->accession . 'to new custom family ' . $custom_family->accession . '!');
    }

}




=pod

=head1 AUTHORS

Mathieu ROUARD (Bioversity), m.rouard@cgiar.org

Valentin GUIGNON (Bioversity), v.guignon@cgiar.org

Matthieu CONTE (Syngenta)

Sebastien BRIOIS (Syngenta)

=head1 VERSION

Version 1.0.0

Date 08/10/2012

=head1 SEE ALSO

GreenPhyl documentation.

=cut

return 1; # package return
