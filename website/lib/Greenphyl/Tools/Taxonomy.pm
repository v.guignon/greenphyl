=pod

=head1 NAME

Greenphyl::Tools::Taxonomy - Taxonomy tools

=head1 SYNOPSIS

    use Greenphyl::Tools::Taxonomy;

=head1 REQUIRES

Perl5

=head1 EXPORTS

    GetSpeciesList
    GetPhylumListForFamilies
    GetHTMLTaxonomySelectionTree
    CompleteSpeciesCode CompleteSpeciesName
    GetUnderlyingSpecies
    ComputeSpeciesColor

=head1 DESCRIPTION

Provides several helper function to process taxonomy and generate associated 
data structures.

=cut

package Greenphyl::Tools::Taxonomy;


use strict;
use warnings;

use Carp qw(cluck confess croak);

use base qw(Exporter);

our @EXPORT = qw(
    GetSpeciesList
    GetPhylumListForFamilies
    GetHTMLTaxonomySelectionTree
    CompleteSpeciesCode CompleteSpeciesName
    GetUnderlyingSpecies
    ComputeSpeciesColor
); 

use JSON;
use HTML::Entities;
use Bio::TreeIO;

use Greenphyl;
use Greenphyl::Web;
use Greenphyl::CompositeObject;
use Greenphyl::Species;
use Greenphyl::Taxonomy;
use Greenphyl::TreeNode;




# Package constants
####################

=pod

=head1 CONSTANTS

B<$DEBUG>: (boolean)

Enables debug mode.

=cut

our $DEBUG = 0;

our $MAX_SUGGESTIONS = 10;



# Package variables
####################

=pod

=head1 VARIABLES

B<$g_species_code_cache>: (hash ref)

Species code cache.

=cut

our $g_species_code_cache = {};




# Package subs
###############

=pod

=head1 FUNCTIONS

=head2 GetSpeciesList

B<Description>: Returns an array of species objects.

B<ArgsCount>: 0

B<Return>: (array ref)

Array of Greenphyl::Species objects.

=cut

sub GetSpeciesList
{
    my $species_list = [sort {$a->name cmp $b->name} Greenphyl::Species->new(GetDatabaseHandler(), {'selectors' => { 'display_order' => ['>', 0], },})];
    return $species_list;
}


=pod

=head2 GetPhylumListForFamilies

B<Description>: Returns an array of taxonomy objects that have specific
families.

B<ArgsCount>: 0

B<Return>: (array ref)

Array of Greenphyl::Taxonomy objects.

=cut

sub GetPhylumListForFamilies
{
    my $phylum_exclusive_list =
        [Greenphyl::Taxonomy->new(
            GetDatabaseHandler(),
            {
                'selectors' =>
                    {
                        'rank' => ['!=', 'species'],
                        'EXISTS' => '(SELECT TRUE FROM families f WHERE f.taxonomy_id = taxonomy.id LIMIT 1)',
                    },
                'sql' =>
                    {
                        'ORDER BY' =>
                            [
                                'scientific_name ASC',
                            ],
                    },
            },
        )];
    return $phylum_exclusive_list;
}


=pod

=head2 GetHTMLTaxonomySelectionTree

B<Description>: Returns .

B<ArgsCount>: 0

B<Return>: (string)

.

=cut

sub GetHTMLTaxonomySelectionTree
{
    my ($species_list, $taxonomy, $text_lookup) = @_;

    my ($taxonomy_newick_tree) = GetDatabaseHandler()->selectrow_array("SELECT value FROM variables WHERE name = 'taxonomy_newick_tree';");
    if (!$taxonomy_newick_tree)
    {
        Throw('error' => 'Species tree is not available in database!', 'log' => "table: variables\nrow: name = 'taxonomy_newick_tree'");
    }
    
    $text_lookup ||= {};

    my $tree = Greenphyl::TreeNode->loadNewickTree($taxonomy_newick_tree);
    PrintDebug('Got ' . $tree->getTreeNodesCount() . ' nodes');

    #PrintDebug("Got Newick: $taxonomy_newick_tree");
    #foreach my $node ($tree->get_nodes)
    #{
    #    PrintDebug("Node '" . $node->id . "'");
    #}
    
    # if no species or taxonomy has been specified, use all
    if (!$species_list)
    {
        $species_list = [Greenphyl::Species->new(GetDatabaseHandler(), {'selectors' => { 'display_order' => ['>', 0], },})];
        $taxonomy     = [Greenphyl::Taxonomy->new(GetDatabaseHandler())];
        PrintDebug("Got no specified species, using default " . @$species_list . " species.");
    }
    elsif (!$taxonomy)
    {
        # generate taxonomy from selected species
        my %taxonomy_nodes;
        foreach my $species (@$species_list)
        {
            foreach my $taxon (@{$species->lineage})
            {
                $taxonomy_nodes{$taxon->id} = $taxon;
            }
        }
        $taxonomy = [values(%taxonomy_nodes)];
        PrintDebug("Got no specified taxonomy, using default " . @$taxonomy . " taxonomy nodes.");
    }
    else
    {
        PrintDebug("Got a specified species set of " . @$species_list . " species and a taxonomy set of " . @$taxonomy . " taxonomy nodes.");
    }

    # create translation table between species name as identifier into species IDs
    my %SPECIES_NAME_TO_SPECIES_ID;
    foreach my $species (@$species_list)
    {
        # name conversion from TreeNode.pm
        if (exists($SPECIES_NAME_TO_SPECIES_ID{$species->name}))
        {
            PrintDebug("Warning: identifier for species '" . $species->name . "' replaced! " . $SPECIES_NAME_TO_SPECIES_ID{$species->name} . " -> " . $species->id);
        }
        $SPECIES_NAME_TO_SPECIES_ID{$species->name} = $species->id;
    }

    # create translation table between scientific name as identifier into taxonomy IDs
    my %SCIENTIFIC_NAME_TO_TAXONOMY_ID;
    foreach my $taxon (@$taxonomy)
    {
        # name conversion from TreeNode.pm
        if (exists($SCIENTIFIC_NAME_TO_TAXONOMY_ID{$taxon->scientific_name}))
        {
            PrintDebug("Warning: identifier for taxon '" . $taxon->scientific_name . "' replaced! " . $SCIENTIFIC_NAME_TO_TAXONOMY_ID{$taxon->scientific_name} . " -> " . $taxon->id);
        }
        $SCIENTIFIC_NAME_TO_TAXONOMY_ID{$taxon->scientific_name} = $taxon->id;
    }

    # process tree
    my $taxonomy_html_tree = '';
    
    sub ProcessTreeNode
    {
        my ($node, $processed_nodes, $species_name_to_species_id, $scientific_name_to_taxonomy_id, $text_lookup, $indent) = @_;
        $processed_nodes->{$node} = 1;
        PrintDebug("Processing node '" . $node->getName() . "'");
        my $html_tree = '';
        $indent ||= '';
        # species or taxonomy?
        if ($node->isLeaf)
        {
            # species
            if (exists($species_name_to_species_id->{$node->getName()}))
            {
                $html_tree .= qq|$indent<span class="sp-leaf">\n|;
                $html_tree .= qq|$indent  <label>\n|;
                $html_tree .= qq|$indent    <input name="species_id" value="| . $species_name_to_species_id->{$node->getName()} . qq|" onclick="UpdateTaxonomySubtree(this);" type="checkbox"/>\n|;
                $html_tree .= qq|$indent    <span class="label">| . $node->getName() . (exists($text_lookup->{$node->getName()}) ? $text_lookup->{$node->getName()} : '') . qq|</span>\n|;
                $html_tree .= qq|$indent  </label>\n|;
                $html_tree .= qq|$indent</span>\n|;
            }
            else
            {
                PrintDebug("HTML taxonomy tree generation: Species '" . $node->getName() . "' skipped");
            }
        }
        else
        {
            # taxonomy
            if (exists($scientific_name_to_taxonomy_id->{$node->getName()}))
            {
                my @descendent_html;
                my @descendent_nodes = grep {!exists($processed_nodes->{$_})} $node->getNeighborNodes();
                PrintDebug("Got " . @descendent_nodes . " descendent nodes for " . $node->getName());
                foreach my $subnode (@descendent_nodes)
                {
                    my $html_subtree = ProcessTreeNode($subnode, $processed_nodes, $species_name_to_species_id, $scientific_name_to_taxonomy_id, $text_lookup, $indent . '      ');
                    if ($html_subtree)
                    {
                        # push(@descendent_html, qq|$indent    <li>\n$html_subtree$indent    </li>\n|);
                        push(@descendent_html, $html_subtree);
                    }
                }
                # skip intermediate nodes
                if (1 < @descendent_html)
                {
                    $html_tree .= qq|$indent<span class="sp-node| . (!$indent ? 'root-element' : '') . qq|">\n|;
                    $html_tree .= qq|$indent  <label>\n|;
                    $html_tree .= qq|$indent    <input name="taxonomy_id" value="| . $scientific_name_to_taxonomy_id->{$node->getName()} . qq|" onclick="UpdateTaxonomySubtree(this);" type="checkbox"/>\n|;
                    $html_tree .= qq|$indent    <span class="label">| . $node->getName() . (exists($text_lookup->{$node->getName()}) ? $text_lookup->{$node->getName()} : '') . qq|</span>\n|;
                    $html_tree .= qq|$indent  </label>\n|;
                    $html_tree .= qq|$indent  <ul>\n|;
                    $html_tree .= qq|$indent    <li>\n| . join(qq|$indent    </li>\n$indent    <li>\n|, @descendent_html) . qq|$indent    </li>\n|;
                    $html_tree .= qq|$indent</ul>\n|;
                    $html_tree .= qq|$indent</span>\n|;
                }
                else
                {
                    $html_tree .= join('', @descendent_html);
                }
            }
            else
            {
                PrintDebug("HTML taxonomy tree generation: Taxonomy '" . $node->getName() . "' skipped");
            }
        }
        return $html_tree;
    };
    
    my $processed_nodes = {};
    $taxonomy_html_tree =
        qq|<div id="species_tree"><ul><li>\n|
        . ProcessTreeNode($tree, $processed_nodes, \%SPECIES_NAME_TO_SPECIES_ID, \%SCIENTIFIC_NAME_TO_TAXONOMY_ID, $text_lookup)
        . qq|</li></ul></div>\n|
    ;
    return $taxonomy_html_tree;
}


=pod

=head2 CompleteSpecies

B<Description>: Returns a JSON structure for jQueryUI auto-completion feature
giving a list of suggested species codes or names corresponding to user input
(either as code or organism name).

B<ArgsCount>: 0

B<Return>: (string)

A JSON string for jQueryUI auto-complete.

=cut

sub CompleteSpecies
{
    my ($parameters) = @_;

    $parameters ||= {};

    my $return_type = $parameters->{'return'} || 'code';
    my $species_restriction = '';
    if ($parameters->{'gp_species_only'})
    {
        $species_restriction .= ' AND display_order > 0';
    }

    my $user_input = $parameters->{'query'};
    my $max_suggestions = $parameters->{'max_suggestions'};

    if (!$max_suggestions || ($max_suggestions !~ m/^\d+$/))
    {
        $max_suggestions = $MAX_SUGGESTIONS;
    }

    # not enough for suggestion, return
    # minimum auto-complete: 2 characters
    if (!$user_input || ($user_input !~ m/[a-zA-Z]{2,}|^\*$/))
    {
        return to_json([]);
    }

    # remove unwanted characters
    $user_input =~ s/[^a-zA-Z0-9\s]+//g;
    my $sql_query = "SELECT DISTINCT code, organism, 'species' AS \"type\" FROM species WHERE (code LIKE '%$user_input%' OR organism LIKE '%$user_input%') $species_restriction;";
    my $suggestions = GetDatabaseHandler()->selectall_arrayref($sql_query, { 'Slice' => {} });

    if ($parameters->{'with_taxonomy'})
    {
        if ($parameters->{'gp_species_only'})
        {
            $sql_query = qq|
                SELECT
                    DISTINCT t.scientific_name AS "code",
                    t.scientific_name AS "organism",
                    'taxonomy' AS \"type\"
                FROM taxonomy t
                        JOIN lineage l ON (l.lineage_taxonomy_id = t.id),
                     species s
                WHERE
                    (t.scientific_name LIKE '%$user_input%')
                    AND rank != 'species'
                    AND s.display_order > 0
                    AND l.taxonomy_id = s.taxonomy_id
                GROUP BY t.scientific_name
            |;
        }
        else
        {
            $sql_query = qq|
                SELECT
                    DISTINCT scientific_name AS "code",
                    scientific_name AS "organism",
                    'taxonomy' AS \"type\"
                FROM taxonomy
                WHERE
                    scientific_name LIKE '%$user_input%'
                    AND rank != 'species'
            |;
        }
        push(@$suggestions, @{GetDatabaseHandler()->selectall_arrayref($sql_query, { 'Slice' => {} })});
    }
    
    return to_json([ map { {'value' => $_->{$return_type}, 'label' => encode_entities($_->{'organism'} . ' (' . $_->{'code'} . ')'), 'code' => $_->{'code'}, 'organism' => $_->{'organism'}}; } @$suggestions ]);
}


=pod

=head2 PrepareCompleteParameters

B<Description>: Get autocompletion parameters.

B<ArgsCount>: 0

B<Return>: (hash ref)

Parameters hash.

=cut

sub PrepareCompleteParameters
{
    my $user_input = GetParameterValues('term');
    my $max_suggestions = GetParameterValues('limit');
    my $gp_species_only = GetParameterValues('gp_species');
    my $with_taxonomy = GetParameterValues('with_taxonomy');

    return {
        'query' => $user_input,
        'max_suggestions' => $max_suggestions,
        'gp_species_only' => $gp_species_only,
        'with_taxonomy'   => $with_taxonomy,
        'return' => 'code',
    };
}


=pod

=head2 CompleteSpeciesCode

B<Description>: Returns a JSON structure for jQueryUI auto-completion feature
giving a list of suggested species codes corresponding to user input (either
as code or organism name).

B<ArgsCount>: 0

B<Return>: (string)

A JSON string for jQueryUI auto-complete.

=cut

sub CompleteSpeciesCode
{
    my $parameters = PrepareCompleteParameters();
    $parameters->{'return'} = 'code';
    
    return CompleteSpecies($parameters);
}


=pod

=head2 CompleteSpeciesName

B<Description>: Returns a JSON structure for jQueryUI auto-completion feature
giving a list of suggested species names corresponding to user input (either
as code or organism name).

B<ArgsCount>: 0

B<Return>: (string)

A JSON string for jQueryUI auto-complete.

=cut

sub CompleteSpeciesName
{
    my $parameters = PrepareCompleteParameters();
    $parameters->{'return'} = 'organism';
    
    return CompleteSpecies($parameters);
}


=pod

=head2 GetUnderlyingSpecies

B<Description>: Returns the list a GreenPhyl species under a given taxonomy
node identifier.

B<ArgsCount>: 1

=over 4

=item $taxonomy_id: (integer) (R)

Taxonomy node identifier.

=back

B<Return>: (array ref)

An array of Greenphyl::Species objects under the given node.

=cut

sub GetUnderlyingSpecies
{
    my ($taxonomy_id) = @_;
    
    if (!$taxonomy_id)
    {
        confess "ERROR: missing taxonomy identifier!\n";
    }
    elsif ($taxonomy_id !~ m/^\d+$/)
    {
        confess "ERROR: invalid taxonomy identifier ($taxonomy_id)!\n";
    }

    my $selection_subquery = 'SELECT l.taxonomy_id FROM lineage l WHERE l.lineage_taxonomy_id = ' . $taxonomy_id;

    my @species_list = Greenphyl::Species->new(GetDatabaseHandler(), {'selectors' => { 'display_order' => ['>', 0], 'taxonomy_id' => ["IN ($selection_subquery)"]}, });

    return \@species_list;
}


=pod

=head2 ComputeOrganismColor

B<Description>: Returns the color code corresponding to the given species code.
Each generated color componant is between 0xD0 and 0xFF. The color code is
computed using species code and is not random.

B<ArgsCount>: 1

=over 4

=item $species_code: (string) (R)

5 capital letter species code.

=back

B<Return>: (string)

a 6 hex character string corresponding to the hexadecimal-coded color code for
the given species code.

=cut

sub ComputeSpeciesColor
{
    my ($species_code) = @_;
    
    # compute species color
    # the goal is to obtain a color between #D0 and #FF for each color componant
    my $species_color = '808080';
    my $ascii_range = 36; # A-Z + 0-9
    my $color_range = 0x20;
    my $color_componant_offset = 0xD0;
    my $color_value = 0;

    # rework species code: all upper case and up to 5 characters
    my $species_code_for_colors = substr(uc($species_code), 0, 5);
    # replace non A-Z or 0-9 characters by 0
    $species_code_for_colors =~ s/[^A-Z0-9]/0/g;
    # translate 0-9 ASCII to codes following Z; ex. 0 becomes [
    $species_code_for_colors =~ s/([0-9])/chr(ord($1) - ord('0') + ord('Z') + 1)/ge;
    
    my @color_values = unpack("C*", $species_code_for_colors);
    $color_value = 0;

    # sum ASCII values into one integer
    while (@color_values)
    {
        $color_value *= $ascii_range;
        $color_value += (pop(@color_values) - ord('A'));
    }

    # get a numeric value between 0 and 1 from the sum
    $color_value /= ((1.*$ascii_range) ** 5);
    
    # get a value between 0 and $color_range*$color_range*$color_range
    $color_value = int($color_value * ($color_range ** 3));
    
    # extract each componant
    $species_color = sprintf("%02x%02x%02x",
        ($color_value % $color_range) + $color_componant_offset,
        ($color_value/$color_range % $color_range) + $color_componant_offset,
        ($color_value/($color_range*$color_range)) + $color_componant_offset
    );
    
    return $species_color;
}


=pod

=head1 AUTHORS

Mathieu ROUARD (Bioversity), m.rouard@cgiar.org

Valentin GUIGNON (Bioversity), v.guignon@cgiar.org

Matthieu CONTE (Syngenta)

Sebastien BRIOIS (Syngenta)

=head1 VERSION

Version 1.0.0

Date 30/10/2012

=head1 SEE ALSO

GreenPhyl documentation.

=cut

return 1; # package return
