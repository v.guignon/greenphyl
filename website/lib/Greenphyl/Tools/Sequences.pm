=pod

=head1 NAME

Greenphyl::Tools::Sequences - Sequences tools

=head1 SYNOPSIS


=head1 REQUIRES

Perl5

=head1 EXPORTS

CheckSeqTextID GetAccessionsFromSynonyms
LoadSequence LoadCustomSequence LoadSequences
GetSequenceList GetHomologList
ExtractLocusAndSpliceForm FilterSpliceForm LoadFASTA CreateFASTASubset

=head1 DESCRIPTION

Provides several helper function to process sequences and generate associated 
data structures.

=cut

package Greenphyl::Tools::Sequences;


use strict;
use warnings;

use Carp qw(cluck confess croak);

use base qw(Exporter);

our @EXPORT = qw(
    CheckSeqTextID GetAccessionsFromSynonyms
    LoadSequence LoadCustomSequence LoadSequences
    GetSequenceList GetHomologList
    ExtractLocusAndSpliceForm FilterSpliceForm LoadFASTA CreateFASTASubset
); 

use CGI qw( header );
use JSON;
use HTML::Entities;
use Bio::SeqIO;

use Greenphyl;
use Greenphyl::HashInsensitive;
use Greenphyl::Dumper;
use Greenphyl::Sequence;
use Greenphyl::CustomSequence;
use Greenphyl::CachedSequence;
use Greenphyl::CompositeObject;
use Greenphyl::Species;




# Package constants
####################

=pod

=head1 CONSTANTS

B<$DEBUG>: (boolean)

Enables debug mode.

=cut

our $DEBUG = 0;
our $SEQUENCE_LIMIT = 100000;
our %SPECIES_SPLICE_FORM_EXTRACTION_FUNCTIONS = (
    'DEFAULT' => \&_ExtractGenericLocusAndSpliceForm,

    # referenced species without splice form information
    'AMBTC' => undef,
    'CAJCA' => undef,
    'CICAR' => undef,
    'CITSI' => undef,
    'COFCA' => undef,
    'CYAME' => undef,
    'ELAGV' => undef,
    'HORVU' => undef,
    'LOTJA' => undef,
    'MALDO' => undef,
    'MANES' => undef,
    'MUSAC' => undef,
    'MUSBA' => undef,
    'OSTTA' => undef,
    'PHAVU' => undef,
    'PHODA' => undef,
    'PICAB' => undef,
    'RICCO' => undef,
    'SELMO' => undef,
    'SETIT' => undef,
    'SOLLY' => undef,
    'SOLTU' => undef,
    'THECC' => undef,
    'VITVI' => undef,

    # referenced species with splice form information
    'ARATH' => \&_ExtractGenericLocusAndSpliceForm,
    'BRADI' => \&_ExtractGenericLocusAndSpliceForm,
    'CARPA' => \&_ExtractCARPALocusAndSpliceForm,
    'CHLRE' => \&_ExtractCHLRELocusAndSpliceForm,
    'CUCSA' => \&_ExtractGenericLocusAndSpliceForm,
    'GLYMA' => \&_ExtractGenericLocusAndSpliceForm,
    'GOSRA' => \&_ExtractGenericLocusAndSpliceForm,
    'MEDTR' => \&_ExtractGenericLocusAndSpliceForm,
    'ORYSA' => \&_ExtractGenericLocusAndSpliceForm,
    'PHAVU' => \&_ExtractGenericLocusAndSpliceForm,
    'PHYPA' => \&_ExtractGenericLocusAndSpliceForm,
    'POPTR' => \&_ExtractGenericLocusAndSpliceForm,
    'SORBI' => \&_ExtractGenericLocusAndSpliceForm,
    'ZEAMA' => \&_ExtractZEAMALocusAndSpliceForm,
);




# Package subs
###############

=pod


=pod

=head1 FUNCTIONS

=head2 GetAccessionsFromSynonyms

B<Description>: Returns an array of given accessions and accession found from
the given synonyms.

B<ArgsCount>: 1-3

=over 4

=item $synonyms: (array ref) (R)

An array of synonym (strings).

=item $accessions: (array ref) (U)

An array of accessions to keep (strings).

=item $contains: (boolean) (O)

If true, given synonym strings will be used as part of complete synonym names
for the search.

B<Return>: (array ref)

Returns an array containing both the given accessions from $accessions if
provided and the accession found from the given synonym list. Each accession
is returned only once (no duplicates).

B<Example>:

  @sequence_textids = @{GetAccessionsFromSynonyms(\@sequence_textids, \@sequence_textids)};

=cut

sub GetAccessionsFromSynonyms
{
    my ($synonyms, $accessions, $contains) = @_;

    my %accessions;

    my $dbh = GetDatabaseHandler();
    my $sql_query = "SELECT DISTINCT s.accession FROM sequences s JOIN sequence_synonyms ss ON (ss.sequence_id = s.id) WHERE ss.synonym REGEXP ?;";
    my $escaped_synonyms;
    if ($contains)
    {
        $escaped_synonyms = [map {".*$_.*"} @{EscapeSQLRegExp($synonyms)}];
    }
    else
    {
        $escaped_synonyms = EscapeSQLRegExp($synonyms);
    }

    PrintDebug("SQL Query: $sql_query\nBindings: '" . join('|', @$escaped_synonyms) . "'");
    my $accession_found = $dbh->selectcol_arrayref($sql_query, undef, join('|', @$escaped_synonyms))
        or confess $dbh->errstr;

    $accession_found ||= [];

    if ($accessions)
    {
        if (!ref($accessions))
        {
            $accessions = [$accessions];
        }
        
        push(@$accession_found, @$accessions);
    }

    foreach (@$accession_found)
    {
        $accessions{$_} = 1;
    }

    return [keys(%accessions)];
}



=pod

=head2 CheckSeqTextID

B<Description>: Tells if a given set of sequence text ID are in database.

B<ArgsCount>: 1

=over 4

=item $output_format: (string) (R)

the requested output format.

B<Return>: (Greenphyl::CompositeObject, hash ref)

The Greenphyl::CompositeObject contains a hash with 2 keys:
'available': the array of sequence text ID that are in database;
'unavailable': the array of sequence text ID that are NOT in database;

The hash ref is just a dump parameter hash.

=cut

sub CheckSeqTextID
{
    my ($output_format) = @_;
    my @accessions = GetParameterValues('accession');
    my $results =
    {
        'available' => [],
        'unavailable' => [],
    };

    my $dbh = GetDatabaseHandler();
    @accessions = @{GetAccessionsFromSynonyms(\@accessions, \@accessions)};
    foreach my $check_accession (@accessions)
    {
        if ($check_accession)
        {
            if ((ValidateSequenceName($check_accession))
                && (my $sequence = Greenphyl::Sequence->new(
                        $dbh,
                        {'selectors' => {'accession' => $check_accession}},)
                   )
                )
            {
                push(@{$results->{'available'}}, $sequence->accession);
            }
            else
            {
                push(@{$results->{'unavailable'}}, $check_accession);
            }
        }
    }

    return (Greenphyl::CompositeObject->new($results), {'columns' => { '1.' => 'available', '2.' => 'unavailable', }});
}


=pod

=head2 LoadSequence

B<Description>: Loads and returns a sequence object. Throws an exception if the
sequence couldn't be loaded. The sequence is selected using either (by priority) the
HTTP parameters 'sequence_accession' or 'sequence_id'.

B<ArgsCount>: 0

B<Return>: (Greenphyl::Sequence)

Loaded sequence.

=cut

sub LoadSequence
{
    my $sequence_accession = GetParameterValues('sequence_accession');
    my $sequence_id = GetParameterValues('sequence_id') || GetParameterValues('seq_id');
    my $class = GetParameterValues('class') || '';
    my $module = 'Greenphyl::Sequence';
    if ($class =~ m/cached_sequence/i)
    {
        $module = 'Greenphyl::CachedSequence';
    }

    my $selector = {};

    if (!$sequence_id || ($sequence_id !~ m/^\d{1,10}$/))
    {
        $sequence_id = undef;
    }
    if (!$sequence_accession)
    {
        $sequence_accession = undef;
    }

    if ($sequence_accession)
    {
        $selector = {'accession' => $sequence_accession};
    }
    elsif ($sequence_id)
    {
        $selector = {'id' => $sequence_id};
    }
    else
    {
        Throw('error' => 'No valid sequence identifier provided!');
    }


    my $sequence = $module->new(
        GetDatabaseHandler(),
        {'selectors' => $selector }
    );

    if (!$sequence && $sequence_accession)
    {
        PrintDebug("Try to load sequence using synonyms");
        # not found, try using synonyms
        $selector =
            {
                'load' => { 'synonyms' => 1, },
                'selectors' => {'sequence_synonyms.synonym' => $sequence_accession},
            };
        $sequence = $module->new(GetDatabaseHandler(), $selector);
    }

    # make sure the sequence is in database
    if (!$sequence)
    {
        if ($sequence_accession)
        {
            Throw('error' => "Sequence not found!", 'log' => "Accession: $sequence_accession");
        }
        else
        {
            Throw('error' => "Sequence ID not found!", 'log' => "ID: $sequence_id");
        }
    }

    PrintDebug("Loaded sequence: $sequence");

    return $sequence;
}


=pod

=head2 LoadCustomSequence

B<Description>: Loads and returns a custom sequence object. Throws an exception
if the custom sequence couldn't be loaded. The custom sequence is selected using
either (by priority) the HTTP parameters 'sequence_accession' or 'csequence_id'.

B<ArgsCount>: 0

B<Return>: (Greenphyl::CustomSequence)

Loaded sequence.

=cut

sub LoadCustomSequence
{
    my $sequence_accession = GetParameterValues('csequence_accession');
    my $sequence_id = GetParameterValues('csequence_id') || GetParameterValues('cseq_id');
    my $selector = {};

    if (!$sequence_id || ($sequence_id !~ m/^\d{1,10}$/))
    {
        $sequence_id = undef;
    }
    if (!$sequence_accession)
    {
        $sequence_accession = undef;
    }

    if ($sequence_accession)
    {
        $selector = {'accession' => $sequence_accession};
    }
    elsif ($sequence_id)
    {
        $selector = {'id' => $sequence_id};
    }
    else
    {
        Throw('error' => 'No valid sequence identifier provided!');
    }

    my $sequence = Greenphyl::CustomSequence->new(
        GetDatabaseHandler(),
        {'selectors' => $selector }
    );

    # make sure the sequence is in database
    if (!$sequence)
    {
        if ($sequence_accession)
        {
            Throw('error' => "Custom sequence not found!", 'log' => "Accession: $sequence_accession");
        }
        else
        {
            Throw('error' => "Custom sequence ID not found!", 'log' => "ID: $sequence_id");
        }
    }

    PrintDebug("Loaded custom sequence: $sequence");

    return $sequence;
}


=pod

=head2 GetSequenceLoadingParameters

B<Description>: prepare fields for the loading query.

B<ArgsCount>: 0

B<Return>: (list of a string and 2 array ref)

List: (string)=table clause; (array ref)=where clause;
(array ref=binding values corresponding to where clause.

=cut

sub GetSequenceLoadingParameters
{
    # get sequence parameters...
    # -check for a list of sequence ID
    my @seq_ids = GetParameterValues('seq_id');
    # -check for a list of sequence names
    my @accessions = GetParameterValues('accession');
    # -check for a list of species
    my @species_ids = GetParameterValues('species_id');
    # -check for a list of species or phylum
    my @tax_ids = GetParameterValues('tax_id');
    # -check for a list of families
    my @family_ids = GetParameterValues('family_id');
    # -check for a list of IPR
    my @ipr_ids = GetParameterValues('ipr_id');
    # -check for a list of UniProt/DBXRef
    my @dbxref_ids = GetParameterValues('dbxref_id');
    # -check for filter status
    my @filtered = GetParameterValues('filtered');

    my $table_clause = 'sequences s';
    my @where_clause = ();
    my @bind_values = ();
    #init selectors
    my $selectors = {};

    # seq_id
    if (1 == @seq_ids)
    {
        push(@where_clause, 's.id = ?');
        push(@bind_values, $seq_ids[0]);
    }
    elsif (@seq_ids)
    {
        push(@where_clause, 's.id IN (' . ('?, ' x $#seq_ids) . '?)');
        push(@bind_values, @seq_ids);
    }

    # accession (case insensitive)
    if (1 == @accessions)
    {
        push(@where_clause, 's.accession LIKE ?');
        push(@bind_values, $accessions[0]);
    }
    elsif (@accessions)
    {
        push(@where_clause, 'UPPER(s.accession) IN (' . ('?, ' x $#accessions) . '?)');
        push(@bind_values, map {uc($_)} @accessions);
    }

    # tax_ids and species_id
    #+FIXME: convert tax_id to species_id (in case of non-species tax_id)
    my @split_species_ids;
    foreach my $species_ids (@species_ids)
    {
        push(@split_species_ids, split(/\s*,+\s*/, $species_ids));
    }
    if (1 == @split_species_ids)
    {
        push(@where_clause, 's.species_id = ? ');
        push(@bind_values, $split_species_ids[0]);
    }
    elsif (@split_species_ids)
    {
        push(@where_clause, 's.species_id IN (' . ('?, ' x $#split_species_ids) . '?)');
        push(@bind_values, @split_species_ids);
    }

    # family_ids
    if (1 == @family_ids)
    {
        $table_clause .= ' JOIN families_sequences sii ON (s.id = sii.sequence_id)';
        push(@where_clause, 'sii.family_id = ?');
        push(@bind_values, $family_ids[0]);
    }
    elsif (@family_ids)
    {
        $table_clause .= ' JOIN families_sequences sii ON (s.id = sii.sequence_id)';
        push(@where_clause, 'sii.family_id IN (' . ('?, ' x $#family_ids) . '?)');
        push(@bind_values, @family_ids);
    }
    
    # ipr_ids
    if (1 == @ipr_ids)
    {
        $table_clause .= ' JOIN ipr_sequences sha ON (s.id = sha.sequence_id)';
        push(@where_clause, 'sha.ipr_id = ?');
        push(@bind_values, $ipr_ids[0]);
    }
    elsif (@ipr_ids)
    {
        $table_clause .= ' JOIN ipr_sequences sha ON (s.id = sha.sequence_id)';
        push(@where_clause, 'sha.ipr_id IN (' . ('?, ' x $#ipr_ids) . '?)');
        push(@bind_values, @ipr_ids);
    }

    # dbxref_ids
    if (1 == @dbxref_ids)
    {
        $table_clause .= ' JOIN sequence_dbxref sdx ON (s.id = sdx.sequence_id)';
        push(@where_clause, 'sdx.dbxref_id = ?');
        push(@bind_values, $dbxref_ids[0]);
    }
    elsif (@dbxref_ids)
    {
        $table_clause .= ' JOIN sequence_dbxref sdx ON (s.id = sdx.sequence_id)';
        push(@where_clause, 'sdx.dbxref_id IN (' . ('?, ' x $#dbxref_ids) . '?)');
        push(@bind_values, @dbxref_ids);
    }

    # filter status
    if ((1 == @filtered) && ($filtered[0] =~ m/^\d+$/))
    {
        push(@where_clause, 's.filtered = ?');
        push(@bind_values, $filtered[0]);
    }
    elsif (@filtered)
    {
        my @filter_values = ();
        foreach my $filter_state (@filtered)
        {
            if ($filter_state =~ m/^\d+$/)
            {
                push(@filter_values, $filter_state);
            }
        }
        if (@filter_values)
        {
            push(@where_clause, 's.filtered IN (' . ('?, ' x $#filter_values) . '?)');
            push(@bind_values, @filter_values);
        }
    }

    return ($table_clause, \@where_clause, \@bind_values);
}


=pod

=head2 GetCustomSequenceLoadingParameters

B<Description>: prepare fields for the loading query.

B<ArgsCount>: 0

B<Return>: (list of a string and 2 array ref)

List: (string)=table clause; (array ref)=where clause;
(array ref=binding values corresponding to where clause.

=cut

sub GetCustomSequenceLoadingParameters
{
    # get sequence parameters...
    # -check for a list of sequence ID
    my @seq_ids = GetParameterValues('cseq_id');
    # -check for a list of sequence names
    my @accessions = GetParameterValues('caccession');
    # -check for a list of species
    my @species_ids = GetParameterValues('species_id');
    # -check for a list of species or phylum
    my @tax_ids = GetParameterValues('tax_id');
    # -check for a list of families
    my @custom_family_ids = GetParameterValues('custom_family_id');
    # -check for a user_id
    my @user_ids = GetParameterValues('user_id');

    my $table_clause = 'custom_sequences s';
    my @where_clause = ();
    my @bind_values = ();
    #init selectors
    my $selectors = {};

    # seq_id
    if (1 == @seq_ids)
    {
        push(@where_clause, 's.id = ?');
        push(@bind_values, $seq_ids[0]);
    }
    elsif (@seq_ids)
    {
        push(@where_clause, 's.id IN (' . ('?, ' x $#seq_ids) . '?)');
        push(@bind_values, @seq_ids);
    }

    # accession (case insensitive)
    if (1 == @accessions)
    {
        push(@where_clause, 's.accession LIKE ?');
        push(@bind_values, $accessions[0]);
    }
    elsif (@accessions)
    {
        push(@where_clause, 'UPPER(s.accession) IN (' . ('?, ' x $#accessions) . '?)');
        push(@bind_values, map {uc($_)} @accessions);
    }

    # tax_ids and species_id
    #+FIXME: convert tax_id to species_id (in case of non-species tax_id)
    my @split_species_ids;
    foreach my $species_ids (@species_ids)
    {
        push(@split_species_ids, split(/\s*,+\s*/, $species_ids));
    }
    if (1 == @split_species_ids)
    {
        push(@where_clause, 's.species_id = ? ');
        push(@bind_values, $split_species_ids[0]);
    }
    elsif (@split_species_ids)
    {
        push(@where_clause, 's.species_id IN (' . ('?, ' x $#split_species_ids) . '?)');
        push(@bind_values, @split_species_ids);
    }

    # custom_family_ids
    if (1 == @custom_family_ids)
    {
        $table_clause .= ' JOIN custom_families_sequences sii ON (s.id = sii.custom_sequence_id)';
        push(@where_clause, 'sii.custom_family_id = ?');
        push(@bind_values, $custom_family_ids[0]);
    }
    elsif (@custom_family_ids)
    {
        $table_clause .= ' JOIN custom_families_sequences sii ON (s.id = sii.custom_sequence_id)';
        push(@where_clause, 'sii.custom_family_id IN (' . ('?, ' x $#custom_family_ids) . '?)');
        push(@bind_values, @custom_family_ids);
    }

    # user_id
    # check if user is admin
#+FIXME: protect sequences of private families
#    if (IsAdmin())
#    {
        if (1 == @user_ids)
        {
            push(@where_clause, 's.user_id = ?');
            push(@bind_values, $user_ids[0]);
        }
        elsif (@user_ids)
        {
            push(@where_clause, 's.user_id IN (' . ('?, ' x $#user_ids) . '?)');
            push(@bind_values, @user_ids);
        }
#    }
#    else
#    {
#        # restrict to current user
#        push(@where_clause, 's.user_id = ?');
#        push(@bind_values, GetCurrentUser()->id);
#    }

    return ($table_clause, \@where_clause, \@bind_values);
}


=pod

=head2 LoadSequences

B<Description>: Loads and returns an array of sequence objects. Throws an
exception if no sequence could be loaded. Sequences can be selected and filtered
using either database ID, accessions, species, common taxonomy node ID,
family ID, databse cross-references, filter status and splice forms.

B<ArgsCount>: 0

B<Return>: (array ref)

An array of Greenphyl::Sequence objects.

=cut

sub LoadSequences
{
    my ($limit, $offset) = @_;

    if ((!defined($limit)) || ($limit !~ m/^\d+$/))
    {
        $limit = $SEQUENCE_LIMIT;
    }
    if ((!defined($offset)) || ($offset !~ m/^\d+$/))
    {
        $offset = 0;
    }

    my $dbh = GetDatabaseHandler();

    my ($table_clause, $where_clause, $bind_values);
    
    my $class = GetParameterValues('class') || '';
    my $module = 'Greenphyl::Sequence';
    if ($class =~ m/custom_sequence/i)
    {
        ($table_clause, $where_clause, $bind_values) = GetCustomSequenceLoadingParameters();
        $module = 'Greenphyl::CustomSequence';
    }
    elsif ($class =~ m/cached_sequence/i)
    {
        ($table_clause, $where_clause, $bind_values) = GetSequenceLoadingParameters();
        # Change table
        $table_clause =~ s/^$Greenphyl::Sequence::OBJECT_PROPERTIES->{'table'} /$Greenphyl::CachedSequence::OBJECT_PROPERTIES->{'table'} /;
        $module = 'Greenphyl::CachedSequence';
    }
    else
    {
        ($table_clause, $where_clause, $bind_values) = GetSequenceLoadingParameters();
        $module = 'Greenphyl::Sequence';
    }

    # get requested sequences
    my $sql_query = "SELECT\n" . join(",\n  ", $module->GetDefaultMembers('s')) . "\nFROM\n  $table_clause\n"
        . (@$where_clause ? "WHERE\n  " . join("\n  AND ", @$where_clause) : '')
        . " ORDER BY s.accession" . ((0 < $limit) ? " LIMIT $offset, $limit" : '') . ";";
    PrintDebug("Sequence loading query: $sql_query\nBindings: " . join(', ', @$bind_values));
    my $sql_sequences = $dbh->selectall_arrayref($sql_query, { 'Slice' => {} }, @$bind_values)
        or confess $dbh->errstr;
    PrintDebug("Got " . scalar(@$sql_sequences) . " records");
    if ($limit == scalar(@$sql_sequences))
    {
        PrintDebug("WARNING: maximum number of sequences ($limit) reached!");
    }
    my $sequences = [];
    foreach my $sql_sequence (@$sql_sequences)
    {
        push(@$sequences, $module->new($dbh, {'load' => 'none', 'members' => $sql_sequence}));
    }

    # -check for splice form filtering
    my $filter_splice = GetParameterValues('splice');
    if ($filter_splice)
    {
      $sequences = FilterSpliceForm($sequences);
    }

    return $sequences;
}


=pod

=head2 GetSequenceListParameters

B<Description>: Returns a parameter hash.

B<ArgsCount>: 0

B<Return>: (hash ref)

Template parameters.

=cut

sub GetSequenceListParameters
{
    my $class = GetParameterValues('class') || '';

    # get requested columns
    my $column_index = 'a';
    my $columns = [];
    my $dump_columns = {};
    my $json_dump_columns = {};
    $dump_columns->{$column_index++ . '. Sequence ID'} = 'accession';
    if ($class !~ m/custom_sequence/i)
    {
        $dump_columns->{$column_index++ . '. Pangene name'} = 'pangene_accession';
        $json_dump_columns->{'pangene'} = 'pangene_accession';
    }

    $dump_columns->{$column_index++ . '. Species'}     = 'species.code';
    $dump_columns->{$column_index++ . '. Genome'}      = 'genome.accession';

    $json_dump_columns->{'accession'} = 'accession';
    $json_dump_columns->{'species'}   = 'species.code';
    $json_dump_columns->{'genome'}    = 'genome.accession';
    $json_dump_columns->{'sequence'}  = 'polypeptide';

    # %SEQUENCE_INFO_COLUMNS keys are values of seq_info inputs from
    # sequence_tools/information_selection.tt
    # label    => label to display for the column in dumps
    # field    => "path" to the sequence object field for dumps
    # selector => select sequence_list.tt columns
    my %SEQUENCE_INFO_COLUMNS = (
        'alias' =>
            {
                'label' => 'Gene name',
                'json_label' => 'locus',
                'field' => 'locus',
                'selector' => 'alias',
            },
        'uniprot' =>
            {
                'label' => 'UniProt',
                'json_label' => 'uniprot',
                'field' => 'uniprot.*',
                'selector' => 'uniprot',
            },
        'kegg' =>
            {
                'label' => 'KEGG',
                'json_label' => 'kegg',
                'field' => 'kegg.*',
                'selector' => 'kegg',
            },
        'ipr' =>
            {
                'label' => 'InterPro',
                'json_label' => 'interpro',
                'field' => 'ipr.*',
                'selector' => 'ipr',
            },
        'annotation' =>
            {
                'label' => 'Annotation',
                'json_label' => 'annotation',
                'field' => 'annotation',
                'selector' => 'annotation',
            },
        'go' =>
            {
                'label' => 'Gene Ontology',
                'json_label' => 'go',
                'field' => 'go.*',
                'selector' => 'go',
            },
        'ec' =>
            {
                'label' => 'EC',
                'json_label' => 'ec',
                'field' => 'ec.*',
                'selector' => 'ec',
            },
        'pubmed' =>
            {
                'label' => 'PubMed',
                'json_label' => 'pubmed',
                'field' => 'pubmed.*',
                'selector' => 'pubmed',
            },
        'filter_status' =>
            {
                'label' => 'Filter Status',
                'json_label' => 'filtered',
                'field' => 'filtered',
                'selector' => 'filter_status',
            },
    );

    my $polypeptide_type = GetParameterValues('sequence_type') || 'protein';

    my $object_type = 'sequence';
    my $object_field_name = 'seq_id';
    if ($class =~ m/custom_sequence/i)
    {
        $object_type = 'custom_sequence';
        $object_field_name = 'cseq_id';
        # add associated sequences to custom sequences
        $SEQUENCE_INFO_COLUMNS{'associated'} = {
            'label' => 'Associated sequences',
            'json_label' => 'associated_sequences',
            'field' => 'sequences.*.accession',
            'selector' => 'associated',
        };
    }

    my @seq_infos = GetParameterValues('seq_info');
    foreach my $seq_infos (@seq_infos)
    {
        foreach my $seq_info (split(/\s*,\s*/, $seq_infos))
        {
            if (exists($SEQUENCE_INFO_COLUMNS{$seq_info}))
            {
                # add column info for dump
                my $column_name = $column_index++ . '. ' . $SEQUENCE_INFO_COLUMNS{$seq_info}->{'label'};
                $dump_columns->{$column_name} = $SEQUENCE_INFO_COLUMNS{$seq_info}->{'field'};
                $json_dump_columns->{$SEQUENCE_INFO_COLUMNS{$seq_info}->{'json_label'}} = $SEQUENCE_INFO_COLUMNS{$seq_info}->{'field'};
                # add column for sequence list template
                push(@$columns, $SEQUENCE_INFO_COLUMNS{$seq_info}->{'selector'});
            }
            else
            {
                PrintDebug("WARNING: unknown sequence info column: '$seq_info'");
            }
        }
    }
    
    my $unchecked = GetParameterValues('unchecked');
    
    
    # prepare parameters
    my $parameters = {
        'content'   => 'sequences/sequence_list.tt',
        'content_id' => 'content_' . $$,
        'columns'   => $columns,
        'dump_data' =>
        {
            'links' => [
                {
                    'label'       => 'Excel',
                    'format'      => 'excel',
                    'file_name'   => 'sequences.xls',
                    'namespace'   => 'excel',
                },
                {
                    'label'       => 'CSV',
                    'format'      => 'csv',
                    'file_name'   => 'sequences.csv',
                    'namespace'   => 'csv',
                },
                {
                    'label'       => 'XML',
                    'format'      => 'xml',
                    'file_name'   => 'sequences.xml',
                    'namespace'   => 'xml',
                },
                {
                    'label'       => 'FASTA',
                    'format'      => 'fasta',
                    'file_name'   => 'sequences.fasta',
                    'namespace'   => 'fasta',
                },
                {
                    'label'       => 'JSON',
                    'format'      => 'json',
                    'file_name'   => 'sequences.json',
                    'namespace'   => 'json',
                    'parameters'  => {'columns' => EncodeDumpParameterValue($json_dump_columns),},
                },
            ],
            'parameters'  => {'columns' => EncodeDumpParameterValue($dump_columns),},
            'object_type' => $object_type,
            'fields'      => "$object_field_name,sequence_type",
            'mapping'     => {$object_field_name => 'id', 'sequence_type' => 'type'},
            'mode'        => 'form',
        },
        'paged'           => 1,
        'with_form_field' => 'checkbox',
        'with_dump_links' => 1,
        'unchecked'       => $unchecked,
    };
    
    # manage external links
    if (my @external_links = GetParameterValues('external_links'))
    {
        foreach my $external_link_data (@external_links)
        {
            my ($label, $url) = ($external_link_data =~ m/^(.*?)=?(http:\/\/.*)$/);
            PrintDebug("Adding '$label' = '$url' to external links");
            $parameters->{'external_links'} ||= [];
            if ($label && $url)
            {
                push(
                    @{$parameters->{'external_links'}},
                    {
                        'label' => $label,
                        'url' => $url,
                    }
                );
            }
        }
    }
    else
    {
        PrintDebug('No external links');
    }

    # manage bulk operations
    if (my @bulk_operations = GetParameterValues('bulk_operations'))
    {
        foreach my $bulk_operation_data (@bulk_operations)
        {
            my ($label, $url) = ($bulk_operation_data =~ m/^(.*?)=?((?:https?:)?\/\/.*)$/);
            PrintDebug("Adding '$label' = '$url' to bulk operations");
            $parameters->{'bulk_operations'} ||= [];
            push(
                @{$parameters->{'bulk_operations'}},
                {
                    'label' => $label,
                    'url' => $url,
                }
            );
        }
    }
    else
    {
        PrintDebug('No external links');
    }

    return $parameters;
}


=pod

=head2 GetSequenceList

B<Description>: Returns an array of Greenphyl::Sequence objects.

B<ArgsCount>: 1

=over 4

=item $output_format: (string) (R)

the requested output format.

B<Return>: (array ref, hash ref)

Returns an array of Greenphyl::CompositeObject objects. Each composit object
contains a Greenphyl::Sequence objects;

The hash ref is just a dump parameter hash.

=cut

sub GetSequenceList
{
    my $sequences = LoadSequences(@_);
    my $parameters = GetSequenceListParameters(@_);
    $parameters->{'sequences'} = $sequences;

    return ($sequences, $parameters);
}


=pod

=head2 GetHomologListParameters

B<Description>: Returns a parameter hash.

B<ArgsCount>: 0

B<Return>: (hash ref)

Template parameters.

=cut

sub GetHomologListParameters
{
    # get requested species
    my $species_selector = {'display_order' => ['>', 0]};
    if (my @species_id_parameters = GetParameterValues('species_id'))
    {
        my @species_ids;
        foreach my $species_id (@species_id_parameters)
        {
            push(@species_ids, split(/\s*,+\s*/, $species_id));
        }
        $species_selector = {'id' => ['IN', @species_ids]};
    }
    my $species_list = [Greenphyl::Species->new(GetDatabaseHandler(), {'selectors' => $species_selector,})];

    # get homology type filter
    my $homology_type = GetParameterValues('homology_type');
    if (!$homology_type)
    {
        $homology_type = ''; # needed for the parameters hash
    }
    elsif ($homology_type && ($homology_type !~ m/^(?:orthology|ultra-paralogy)$/))
    {
        PrintDebug('Invalid homology type selection: ' . ($homology_type ? $homology_type : 'none selected'));
        $homology_type = 'orthology';
    }
    
    # get requested columns
    my $column_index = 'a';
    my $columns = [];
    my $dump_columns = {};
    $dump_columns->{$column_index++ . '. Sequence ID'}              = 'getCGIMember.query_accession';
    $dump_columns->{$column_index++ . '. Species'}                  = 'getCGIMember.query_species_name';
    $dump_columns->{$column_index++ . '. Similar Sequence ID'}      = 'getCGIMember.hit_accession';
    $dump_columns->{$column_index++ . '. Similar Sequence Species'} = 'getCGIMember.hit_species_name';
    $dump_columns->{$column_index++ . '. Homology Type'}            = 'getCGIMember.homology_type';
    $dump_columns->{$column_index++ . '. Evolutionary Distance'}    = 'getCGIMember.homology_distance';
    $dump_columns->{$column_index++ . '. Node Distance'}            = 'getCGIMember.homology_speciation';
    $dump_columns->{$column_index++ . '. RBH Score'}               = 'getCGIMember.bbmh_score';
    $dump_columns->{$column_index++ . '. RBH e-value'}             = 'getCGIMember.bbmh_evalue';

    my $object_type = 'sequence';
    my $class = GetParameterValues('class') || '';
    if ($class =~ m/custom_sequence/i)
    {
        $object_type = 'custom_sequence';
    }

    # prepare parameters
    my $parameters = {
        'content'      => 'sequence_tools/similarity.tt',
        'content_id'   => 'content_' . $$,
        'species_list' => $species_list,
        'dump_data'    =>
        {
            'links' => [
                {
                    'label'       => 'Excel',
                    'format'      => 'excel',
                    'file_name'   => 'sequences.xls',
                    'namespace'   => 'excel',
                },
                {
                    'label'       => 'CSV',
                    'format'      => 'csv',
                    'file_name'   => 'sequences.csv',
                    'namespace'   => 'csv',
                },
                {
                    'label'       => 'XML',
                    'format'      => 'xml',
                    'file_name'   => 'sequences.xml',
                    'namespace'   => 'xml',
                },
                {
                    'label'       => 'FASTA',
                    'format'      => 'fasta',
                    'file_name'   => 'sequences.fasta',
                    'namespace'   => 'fasta',
                    'object_type' => $object_type,
                    'fields'      => 'seq_id',
                    'mapping'     => {'seq_id' => 'id'},
                },
            ],
            'parameters'  => {'columns' => EncodeDumpParameterValue($dump_columns),},
            'object_type' => 'composite',
            'fields'      => 'similarity_id',
            'mapping'     => {'similarity_id' => 'composite_id'},
            'mode'        => 'form',
        },
        'paged'           => 1,
        'with_form_field' => 'checkbox',
        'with_dump_links' => 1,
        'homology_type'   => $homology_type,
    };

    return $parameters;
}


=pod

=head2 GetHomologList

B<Description>: Returns a composite object containing similarity data.

B<ArgsCount>: 1

=over 4

=item $output_format: (string) (R)

the requested output format.

B<Return>: (array ref, hash ref)

Returns a Greenphyl::CompositeObject object.

The hash ref is just a dump parameter hash.

=cut

sub GetHomologList
{
    my ($sequences) = @_;
    if (!$sequences
        || (ref($sequences) ne 'ARRAY')
        || (!@{$sequences})
        || (!ref($sequences->[0]))
        || (ref($sequences->[0]) !~ m/^(?:Greenphyl::Sequence|Greenphyl::CachedSequence)$/))
    {
        $sequences = LoadSequences();
    }

    my $parameters = GetHomologListParameters();

    # check for species filter
    my %selected_species;
    foreach my $species (@{$parameters->{'species_list'}})
    {
        $selected_species{$species->id} = $species;
    }

    # get similar sequences
    my $similarity_data = [];
    foreach my $sequence (@$sequences)
    {
        my $similar_sequences = [];
        # check for species filter
        if (%selected_species)
        {
            if (1 == scalar(keys(%selected_species)))
            {
                # single species only keep selected species matches in all other species
                if (exists($selected_species{$sequence->species_id}))
                {
                    foreach my $similar_sequence (@{$sequence->homologs})
                    {
                        push(@$similar_sequences, $similar_sequence);
                    }
                }                # only keep selected species
            }
            else
            {
                # multiple-species: keep only matches in the species set
                #if (exists($selected_species{$sequence->species_id}))
                #{
                    foreach my $similar_sequence (@{$sequence->homologs})
                    {
                        if (exists($selected_species{$similar_sequence->species_id}))
                        {
                            push(@$similar_sequences, $similar_sequence);
                        }
                    }
                #}
            }
        }
        else
        {
            # no species filter, get all
            $similar_sequences = $sequence->homologs;
        }
        
        # filter homology types
        my $filtered_similar_sequences = [];
        if ($parameters->{'homology_type'})
        {
            foreach my $similar_sequence (@$similar_sequences)
            {
                my $homolog_with = $sequence->getHomologyWith($similar_sequence);
                if (!$homolog_with)
                {
                    confess "Missing homology record for sequence $sequence with sequence $similar_sequence.";
                }
                if ($homolog_with->type() eq $parameters->{'homology_type'})
                {
                    push(@$filtered_similar_sequences, $similar_sequence);
                }
            }
        }
        else
        {
            $filtered_similar_sequences = $similar_sequences;
        }

        # sort
        $filtered_similar_sequences = [sort {$a->accession cmp $b->accession} @$filtered_similar_sequences];

        # store matches
        if (@$filtered_similar_sequences)
        {
            push(
                @$similarity_data,
                {
                    'sequence'     => $sequence,
                    'similarities' => $filtered_similar_sequences,
                },
            );
        }
    }

    $parameters->{'similarity_data'} = [sort {$a->{'sequence'}->accession cmp $b->{'sequence'}->accession} @$similarity_data];
    
    my $composite = Greenphyl::CompositeObject->new($similarity_data);

    return ($composite, $parameters);
}


=pod

=head2 _ExtractGenericLocusAndSpliceForm

B<Description>: returns locus and splice form from a full accession name.

B<ArgsCount>: 1

=over 4

=item $accession: (string) (R)

The sequence accession.

=back

B<Return>: (list)

A list of 2 elements: first is the locus name, the second is the splice form.

=cut

sub _ExtractGenericLocusAndSpliceForm
{
    my ($accession) = @_;

    my ($locus, $splice_form) = (
            $accession =~ m/
                ^
                (.+)        # get locus
                (\.\d{1,2}) # try to get splice form if one
                $
            /x
        );

    if (defined($splice_form))
    {
        $splice_form =~ s/^\.//g; # remove dot from splice form index
        $splice_form = int($splice_form);
    }

    return ($locus, $splice_form);
}


=pod

=head2 _ExtractCARPALocusAndSpliceForm

B<Description>: returns locus and splice form from a full accession name for a
CARPA sequence.

format: <locus>.<splice>
exceptions: no splice when the accession contains "supercontig"

B<ArgsCount>: 1

=over 4

=item $accession: (string) (R)

The sequence accession.

=back

B<Return>: (list)

A list of 2 elements: first is the locus name, the second is the splice form.

=cut

sub _ExtractCARPALocusAndSpliceForm
{
    my ($accession) = @_;

    my ($locus, $splice_form);

    if ($accession && ($accession =~ m/supercontig/i))
    {
        $locus       = $accession;
        $splice_form = undef;
    }
    else
    {
        ($locus, $splice_form) = (
                $accession =~ m/
                    ^
                    (.+)        # get locus
                    (\.\d{1,2}) # try to get splice form if one
                    $
                /x
            );

        if (defined($splice_form))
        {
            $splice_form =~ s/^\.//g; # remove dot from splice form index
            $splice_form = int($splice_form);
        }
    }

    return ($locus, $splice_form);
}


=pod

=head2 _ExtractCHLRELocusAndSpliceForm

B<Description>: returns locus and splice form from a full accession name for a
CHLRE sequence.

format: <locus>.t<splice> where splice can be 1, 2 or 1.1, 1.2, or 2.1 and so on.

B<ArgsCount>: 1

=over 4

=item $accession: (string) (R)

The sequence accession.

=back

B<Return>: (list)

A list of 2 elements: first is the locus name, the second is the splice form.

=cut

sub _ExtractCHLRELocusAndSpliceForm
{
    my ($accession) = @_;

    my ($locus, $splice_form) = (
            $accession =~ m/
                ^
                (.+)                       # get locus
                (\.t\d{1,2}(?:\.\d{1,2})?) # try to get splice form if one
                $
            /x
        );

    if (defined($splice_form))
    {
        $splice_form =~ s/\D+//g; # remove dots and 't' from splice form index
        $splice_form = int($splice_form);
    }

    return ($locus, $splice_form);
}


=pod

=head2 _ExtractZEAMALocusAndSpliceForm

B<Description>: returns locus and splice form from a full accession name for a
ZEAMA sequence.

format: <locus>_P<splice> where splice is on 2 digits

B<ArgsCount>: 1

=over 4

=item $accession: (string) (R)

The sequence accession.

=back

B<Return>: (list)

A list of 2 elements: first is the locus name, the second is the splice form.

=cut

sub _ExtractZEAMALocusAndSpliceForm
{
    my ($accession) = @_;

    my ($locus, $splice_form) = (
            $accession =~ m/
                ^
                (.+)     # get locus
                (_P\d\d) # try to get splice form if one
                $
            /x
        );

    if (defined($splice_form))
    {
        $splice_form =~ s/^_P//g; # remove splice prefix
        $splice_form = int($splice_form);
    }

    return ($locus, $splice_form);
}


=pod

=head2 ExtractLocusAndSpliceForm

B<Description>: returns locus and splice form from a full accession name.

B<ArgsCount>: 1-2

=over 4

=item $accession: (string) (R)

The sequence accession.

=item $species_code: (string) (O)

The species code related to this sequence.

=back

B<Return>: (list)

A list of 2 elements: first is the locus name, the second is the splice form.
If no splice form has been found, returns the given accession with any species
code and undef.

=cut

sub ExtractLocusAndSpliceForm
{
    my ($accession, $species_code) = @_;

    if (!$accession)
    {
        cluck "WARNING: can not extract locus and splice form from an empty accession!\n";
    }

    # remove species code from accession
    if ($species_code)
    {
        $accession =~ s/_\Q$species_code\E$//;
    }
    else
    {
        $accession =~ s/_($Greenphyl::Species::SPECIES_CODE_REGEXP)$//;
        $species_code = $1;
    }
    
    my ($locus, $splice_form);
    if ($species_code && exists($SPECIES_SPLICE_FORM_EXTRACTION_FUNCTIONS{$species_code}))
    {
        # a species code has been specified
        if (defined($SPECIES_SPLICE_FORM_EXTRACTION_FUNCTIONS{$species_code}))
        {
            # we can handle it
            ($locus, $splice_form) = $SPECIES_SPLICE_FORM_EXTRACTION_FUNCTIONS{$species_code}->($accession);
        }
        else
        {
            # there's no splice form
            ($locus, $splice_form) = ($accession, undef);
        }
    }
    else
    {
        # use default
        ($locus, $splice_form) = $SPECIES_SPLICE_FORM_EXTRACTION_FUNCTIONS{'DEFAULT'}->($accession);
    }

    if (!defined($locus) || ($locus eq '') || !defined($splice_form))
    {
        $locus = $accession;
        $splice_form = undef;
    }

    return ($locus, $splice_form);
}


=pod

=head2 FilterSpliceForm

B<Description>: Filters splice form of an array of sequences.

B<ArgsCount>: 1

=over 4

=item $sequences: (array of Greenphyl::Sequence) (R)

The list of sequences to process.

=back

B<Return>: (array ref)

An array with at most one instance of each splice form of sequences.

B<Example>:

    my $sequences = ...
    my $filtered_sequences = FilterSpliceForm($sequences);
    foreach my $sequence (@$filtered_sequences)
    {
        print $sequence->name . "\n";
    }

=cut

sub FilterSpliceForm
{
    my ($sequences) = @_;

    my %kept_sequences;
    foreach my $sequence (@$sequences)
    {
        my ($locus) = $sequence->getLocusAndSpliceForm();
        $locus = lc($locus); # consistency: remove capital letter for some genes like Arath...

        # skip current sequence if we already got better
        next if (
                 exists($kept_sequences{$locus})
                 && defined($sequence->splice_priority)
                 && defined($kept_sequences{$locus}->splice_priority)
                 && ($kept_sequences{$locus}->splice_priority < $sequence->splice_priority)
        );

        # keep sequence
        $kept_sequences{$locus} = $sequence;
    }

    return [values(%kept_sequences)];
}


=pod

=head2 ExcludeSpecies

B<Description>: Remove sequences belonging to a list a species from the given
array and return a new array.

B<ArgsCount>: 2

=over 4

=item $sequences: (array of Greenphyl::Sequence) (R)

The list of sequences to process.

=item $species: (array of Greenphyl::species) (R)

The list of species to exclude.

=back

B<Return>: (array ref)

An array with at most one instance of each splice form of sequences.

B<Example>:

    my $sequences = ...
    my $filtered_sequences = ExcludeSpecies($sequences);
    foreach my $sequence (@$filtered_sequences)
    {
        print $sequence->name . "\n";
    }

=cut

sub ExcludeSpecies
{
    my ($sequences, $species_list) = @_;

    my %remove_species = map {$_->code => $_} @$species_list;
    my @kept_sequences;
    foreach my $sequence (@$sequences)
    {
        if (!exists($remove_species{$sequence->species->code}))
        {
            push(@kept_sequences, $sequence);
        }
    }

    return \@kept_sequences;
}


=pod

=head2 IncludeSpecies

B<Description>: Remove sequences belonging to a list a species from the given
array and return a new array.

B<ArgsCount>: 2

=over 4

=item $sequences: (array of Greenphyl::Sequence) (R)

The list of sequences to process.

=item $species: (array of Greenphyl::species) (R)

The list of species to include.

=back

B<Return>: (array ref)

An array with at most one instance of each splice form of sequences.

B<Example>:

    my $sequences = ...
    my $filtered_sequences = IncludeSpecies($sequences);
    foreach my $sequence (@$filtered_sequences)
    {
        print $sequence->name . "\n";
    }

=cut

sub IncludeSpecies
{
    my ($sequences, $species_list) = @_;
    my %keep_species = map {$_->code => $_} @$species_list;
    my @kept_sequences;
    foreach my $sequence (@$sequences)
    {
        if (exists($keep_species{$sequence->species->code}))
        {
            push(@kept_sequences, $sequence);
        }
    }

    return \@kept_sequences;
}


=pod

=head2 LoadFASTA

B<Description>: Loads sequence objects from a FASTA file and returns them into
a hash.

B<ArgsCount>: 1

=over 4

=item $fasta_file_path: (string) (R)

the path to the FASTA file.

B<Return>: (hash ref, hash ref)

The first hash contains sequence name as ID and corresponding
Greenphyl::Sequence as values, the second hash contains BioPerl Seq objects as
values.

=cut

sub LoadFASTA
{
    my ($fasta_file_path) = @_;

    my $input_fasta_fh = Bio::SeqIO->new(
        '-file'   => $fasta_file_path,
        '-format' => 'Fasta'
    );

    # use insensitive hash
    tie my %sequences, 'Greenphyl::HashInsensitive';
    tie my %bio_sequences, 'Greenphyl::HashInsensitive';

    while (my $bio_seq = $input_fasta_fh->next_seq())
    {
        my $polypeptide = $bio_seq->seq;
        # remove optional trailing '*'
        $polypeptide =~ s/\*+$//;
        # store sequence
        $sequences{$bio_seq->display_id} = Greenphyl::Sequence->new(
            undef,
            {
                'load' => 'none',
                'members' =>
                {
                    'id'          => -1,
                    'annotation'  => $bio_seq->desc,
                    'length'      => $bio_seq->length,
                    'accession'   => $bio_seq->display_id,
                    'polypeptide' => $polypeptide,
                },
            }
        );
        # store Bio::Seq objects
        $bio_sequences{$bio_seq->display_id} = $bio_seq;
    }

    return (\%sequences, \%bio_sequences);
}


=pod

=head2 CreateFASTASubset

B<Description>: Creates a FASTA file that contains the given subset of sequences
from an input FASTA file. If no subset is given, the whole file is copied.
If the output FASTA file exists, it will be overwritten.

B<ArgsCount>: 2-3

=over 4

=item $input_fasta_file_path: (string) (R)

the path to the input FASTA file.

=item $output_fasta_file_path: (string) (R)

the path to the output FASTA file.

=item $keep_sequence_names: (hash ref) (O)

a hash of sequence names (hash keys) to keep.

B<Return>: nothing

=cut

sub CreateFASTASubset
{
    my ($input_fasta_file_path, $output_fasta_file_path, $keep_sequence_names) = @_;

    my $input_fasta_fh = Bio::SeqIO->new(
        '-file'   => $input_fasta_file_path,
        '-format' => 'Fasta'
    );
    my $output_fasta_fh = Bio::SeqIO->new(
        '-file' => ">$output_fasta_file_path",
        '-format' => 'Fasta'
    );

    # check if a sequence selection hash has been provided
    if ($keep_sequence_names)
    {
        # yes, keep only what's requested
        while (my $bio_seq = $input_fasta_fh->next_seq())
        {
            if (exists($keep_sequence_names->{$bio_seq->display_id}))
            {
                $output_fasta_fh->write_seq($bio_seq);
            }
        }
    }
    else
    {
        # no hash, copy the whole file
        while (my $bio_seq = $input_fasta_fh->next_seq())
        {$output_fasta_fh->write_seq($bio_seq);}
    }

}




=pod

=head1 AUTHORS

Mathieu ROUARD (Bioversity), m.rouard@cgiar.org

Valentin GUIGNON (Bioversity), v.guignon@cgiar.org

Matthieu CONTE (Syngenta)

Sebastien BRIOIS (Syngenta)

=head1 VERSION

Version 1.0.0

Date 23/10/2012

=head1 SEE ALSO

GreenPhyl documentation.

=cut

return 1; # package return
