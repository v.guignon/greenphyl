=pod

=head1 NAME

Greenphyl::Tools::Annotations - Annotation tools

=head1 SYNOPSIS


=head1 REQUIRES

Perl5

=head1 EXPORTS

LoadFilteredAnnotations GetAnnotationList

=head1 DESCRIPTION

Provides several helper functions to process annotations and generate associated 
data structures.

=cut

package Greenphyl::Tools::Annotations;


use strict;
use warnings;

use Carp qw(cluck confess croak);

use base qw(Exporter);

our @EXPORT = qw(
   LoadFilteredAnnotations GetAnnotationList
); 

use Greenphyl;
use Greenphyl::Web;
use Greenphyl::Web::Access;

use Greenphyl::FamilyAnnotation;
use Greenphyl::Tools::Families;
use Greenphyl::Tools::Taxonomy;
use Greenphyl::Tools::Annotations;




# Package constants
####################

=pod

=head1 CONSTANTS

B<$DEBUG>: (boolean)

Enables debug mode.

=cut

our $DEBUG = 0;




# Package subs
###############

=pod

=head1 FUNCTIONS

=head2 LoadFilteredAnnotations

B<Description>: Loads and returns a family object. Throws an exception if the
family couldn't be loaded. The family is selected using either (by priority) the
HTTP parameters 'family_accession' or 'family_id'.

B<ArgsCount>: 0

B<Return>: (array ref)

.

=cut

sub LoadFilteredAnnotations
{
    my ($selectors) = @_;
    my $annotations = [];
    my ($family_members, $table_clause, $where_clause, $bind_values) = PrepareFamilyLoadingQuery();

    # add family_annotations table as "main table" instead of families table
    $table_clause =~
        s/
            ^\s*families\s*(\w+)
        /
            family_annotations family_annotations JOIN families $1 ON ($1.accession = family_annotations.accession)
        /x;

    # check if we got selectors and if not, try to get selectors from query string
    if (!$selectors)
    {
        my @status = GetParameterValues('annotation_status', undef, '[,;\s]');
        my @valid_status;

        if (@status)
        {
            foreach my $status (@status)
            {
                if ($status =~ m/^[\w ]+$/)
                {
                    push(@valid_status, $status);
                }
            }
        }

        if (1 == @valid_status)
        {
            $selectors = {'status' => $valid_status[0]};
        }
        elsif (@valid_status)
        {
            $selectors = {'status' => ['IN', @valid_status]};
        }
    }

    # add selectors
    if ($selectors && %$selectors)
    {
        $where_clause ||= [];
        foreach my $member_name (keys(%$selectors))
        {
            push(@$where_clause, "family_annotations.$member_name = '$selectors->{$member_name}'");
        }
    }

    # apply filter to get requested family annotations
    my $dbh = GetDatabaseHandler();
    my $sql_query = "
          SELECT
            " . join(",\n            ", Greenphyl::FamilyAnnotation->GetDefaultMembers('family_annotations')) . "
          FROM
            $table_clause\n"
        . (@$where_clause ? "            WHERE\n            " . join("\n            AND ", @$where_clause) : '') . "
          ORDER BY family_annotations.status ASC,
            family_annotations.date DESC,
            family_annotations.user_id ASC,
            family_annotations.accession ASC;"
        ;
    PrintDebug("Family annotations loading query: $sql_query\nBindings: " . join(', ', @$bind_values));
    my $sql_family_annotations = $dbh->selectall_arrayref($sql_query, { 'Slice' => {} }, @$bind_values)
        or confess $dbh->errstr;
    
    foreach my $sql_family_annotation (@$sql_family_annotations)
    {
        push(@$annotations,
            Greenphyl::FamilyAnnotation->new(
                $dbh,
                {
                    'load' => 'none',
                    'members' => $sql_family_annotation,
                }
            )
        );
    }
    return $annotations;
}


=pod

=head2 GetAnnotationList

B<Description>: .

B<ArgsCount>: 1-2

B<Return>: (string)

.

=cut

sub GetAnnotationList
{
    my ($output_format, $selectors) = @_;
    my ($annotations, $pager_data) = ([], undef);
    my $filter_use = 0;
    # make sure we only work if current user is admin
    if (IsAdmin())
    {
        # get filters
        # make sure we got something to filter first
        if (GetParameterValues('filter_use'))
        {
            PrintDebug('Loading filtered annotations');
            $filter_use = 1;
            $annotations = LoadFilteredAnnotations($selectors);
        }
        else
        {
            PrintDebug('Loading all annotations');
            # nothing to filter, get all annotations
            my $query_parameters =
                {
                    'selectors' => $selectors,
                    'sql' =>
                        {
                            'ORDER BY' =>
                                [
                                    #+Val: to use if we want a specific order
                                    # "CASE
                                    #     WHEN family_annotations.status = 'submitted'
                                    #       THEN 0
                                    #     WHEN family_annotations.status = 'in progress'
                                    #       THEN 1
                                    #     WHEN family_annotations.status = 'validated'
                                    #       THEN 2
                                    #     WHEN family_annotations.status = 'denied'
                                    #       THEN 3
                                    #     ELSE 4
                                    #     END",
                                    'family_annotations.status ASC',
                                    'family_annotations.date DESC',
                                    'family_annotations.user_id ASC',
                                    'family_annotations.accession ASC',
                                ],
                        },
                };
            # ($annotations, $pager_data) = GetPagedObjects('Greenphyl::FamilyAnnotation', $query_parameters);
            $annotations = [Greenphyl::FamilyAnnotation->new(GetDatabaseHandler(), $query_parameters)];
        }
    }
    PrintDebug('Found ' . scalar(@$annotations) . ' annotations');

    my $parameters =
        {
            'access_message' => 'You need administrator priviledges to access to this page!',
            'require_access' => [GP('ACCESS_KEY_ADMINISTRATOR'),],
            'access_needs'   => 'any',
            'content'        => 'admin_tools/annotation_list.tt',
            'annotations'    => $annotations,
            # 'pager_data'     => $pager_data,
            'filter_use'     => $filter_use,
        };

    return ($annotations, $parameters);
}


=pod

=head1 AUTHORS

Mathieu ROUARD (Bioversity), m.rouard@cgiar.org

Valentin GUIGNON (Bioversity), v.guignon@cgiar.org

Matthieu CONTE (Syngenta)

Sebastien BRIOIS (Syngenta)

=head1 VERSION

Version 1.0.0

Date 31/10/2012

=head1 SEE ALSO

GreenPhyl documentation.

=cut

return 1; # package return
