=pod

=head1 NAME

Greenphyl::Tools::Domains - Protein domain tools

=head1 SYNOPSIS

    my $domains                  = ($family);

=head1 REQUIRES

Perl5

=head1 EXPORTS

GetFamilyDomains

=head1 DESCRIPTION

Provides several helper function to work with protein domains.

=cut

package Greenphyl::Tools::Domains;


use strict;
use warnings;

use Carp qw(cluck confess croak);

use base qw(Exporter);

our @EXPORT = qw(
    GetFamilyDomains
); 

use Greenphyl;
use Greenphyl::Web;
use Greenphyl::Tools::Families;
use Greenphyl::Dumper;





# Package constants
####################

=pod

=head1 CONSTANTS

B<$DEBUG>: (boolean)

Enables debug mode.

=cut

our $DEBUG = 0;




# Package subs
###############

=pod

=head1 FUNCTIONS

=head2 GetFamilyDomainParameters

B<Description>: Returns a parameter hash.

B<ArgsCount>: 0

B<Return>: (hash ref)

Template parameters.

=cut

sub GetFamilyDomainParameters
{
    # prepare parameters
    my $parameters = {
        'title'      => 'Domain List',
        'content'    => 'family_tools/protein_domain_table.tt',
        'content_id' => 'content_' . $$,
        'javascript' => ['raphael', 'ipr', ],
    };

    return $parameters;
}



=pod

=head2 GetFamilyDomains

B<Description>: .

B<ArgsCount>: 0

B<Return>: ()

.

=cut

sub GetFamilyDomains
{
    my $family = LoadFamily();
    my $parameters = GetFamilyDomainParameters();
    
    my $family_domains_data;
    
    my $max_sequence_length = 0;

    $parameters->{'family'}       = $family;
    $parameters->{'meme_domains'} = GetParameterValues('meme_domains');
    $parameters->{'ipr_domains'}  = GetParameterValues('ipr_domains');

    if ($parameters->{'ipr_domains'})
    {
        foreach my $sequence (@{$family->sequences})
        {
            if ($sequence->length() > $max_sequence_length)
            {
                $max_sequence_length = $sequence->length();
            }
        }
    }
    $parameters->{'max_length'}   = $max_sequence_length;


    $parameters->{'additional'}   = {
            'columns' => [],
            'values' => {},
        };
    if (GetParameterValues('level1'))
    {
        push(
            @{$parameters->{'additional'}->{'columns'}},
            {
                'label' => 'Level 1',
                'member' => 'current_object.families.0',
                'template' => 'families/family_id.tt',
                'template_parameters' =>
                    {
                        'dynamic' => {'family' => 'member_value'},
                    },
            },
        );
    }
    if (GetParameterValues('level2'))
    {
        push(
            @{$parameters->{'additional'}->{'columns'}},
            {
                'label' => 'Level 2',
                'member' => 'current_object.families.1',
                'template' => 'families/family_id.tt',
                'template_parameters' =>
                    {
                        'dynamic' => {'family' => 'member_value'},
                    },
            },
        );
    }
    if (GetParameterValues('level3'))
    {
        push(
            @{$parameters->{'additional'}->{'columns'}},
            {
                'label' => 'Level 3',
                'member' => 'current_object.families.2',
                'template' => 'families/family_id.tt',
                'template_parameters' =>
                    {
                        'dynamic' => {'family' => 'member_value'},
                    },
            },
        );
    }
    if (GetParameterValues('level4'))
    {
        push(
            @{$parameters->{'additional'}->{'columns'}},
            {
                'label' => 'Level 4',
                'member' => 'current_object.families.3',
                'template' => 'families/family_id.tt',
                'template_parameters' =>
                    {
                        'dynamic' => {'family' => 'member_value'},
                    },
            },
        );
    }

    my $output = RenderHTMLFullPage($parameters);

    return ($output, $parameters);
}


=pod

=head1 AUTHORS

Mathieu ROUARD (Bioversity), m.rouard@cgiar.org

Valentin GUIGNON (Bioversity), v.guignon@cgiar.org

Matthieu CONTE (Syngenta)

Sebastien BRIOIS (Syngenta)

=head1 VERSION

Version 1.0.0

Date 15/12/2013

=head1 SEE ALSO

GreenPhyl documentation.

=cut

return 1; # package return
