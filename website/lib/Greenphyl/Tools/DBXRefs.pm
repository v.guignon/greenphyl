=pod

=head1 NAME

Greenphyl::Tools::DBXRefs - Database cross-refenrences tools

=head1 SYNOPSIS


=head1 REQUIRES

Perl5

=head1 EXPORTS

AddDBXRef AddPubMedDBXRef

=head1 DESCRIPTION

Provides several helper function to process database cross-references and
generate associated data structures.

=cut

package Greenphyl::Tools::DBXRefs;


use strict;
use warnings;

use Carp qw(cluck confess croak);

use base qw(Exporter);

our @EXPORT = qw(
    AddDBXRef AddPubMedDBXRef
); 

use Greenphyl;
use Greenphyl::DBXRef;
use Greenphyl::SourceDatabase;

use Bio::Biblio;



# Package constants
####################

=pod

=head1 CONSTANTS

B<$DEBUG>: (boolean)

Enables debug mode.

=cut

our $DEBUG = 0;




# Script global variables
##########################

=pod

=head1 VARIABLES

B<$g_biblio>: (Bio::Biblio)

Bio::Biblio object if initialized. Use GetBioBiblio() instead of trying to use
$g_biblio directly.

=cut

my $g_biblio;




# Package subs
###############

=pod

=head1 FUNCTIONS

=head2 AddDBXRef

B<Description>: Adds a new DBXRef to the database.

B<ArgsCount>: 1

=over 4

=item $members: (hash ref) (R)

DBXRef members hash. Keys are member field names and values are associated
values. 2 members are requiered:
-'db' or 'db_id': a Greenphyl::SourceDatabase object or its ID;
-'accession': the new DBXRef accession;
Optional members:
-'description': DBXRef text description;
-'version': version of the DBXRef.

B<Return>: (Greenphyl::DBXRef)

The new DBXRef created.

=cut

sub GetBioBiblio
{
    if (!$g_biblio)
    {
        $g_biblio = Bio::Biblio->new(-access => 'biofetch')
            or confess "ERROR: Unable to instanciate Bio::Biblio object!\n";
    }

    return $g_biblio;
}


=pod

=head2 AddDBXRef

B<Description>: Adds a new DBXRef to the database.

B<ArgsCount>: 1

=over 4

=item $members: (hash ref) (R)

DBXRef members hash. Keys are member field names and values are associated
values. 2 members are requiered:
-'db' or 'db_id': a Greenphyl::SourceDatabase object or its ID;
-'accession': the new DBXRef accession;
Optional members:
-'description': DBXRef text description;
-'version': version of the DBXRef.

B<Return>: (Greenphyl::DBXRef)

The new DBXRef created.

=cut

sub AddDBXRef
{
    my ($members) = @_;

    if (!$members)
    {
        confess "ERROR: Unable to create a new DBXRef: no member provided!\n";
    }

    if (ref($members) ne 'HASH')
    {
        confess "ERROR: Invalid members provided (not a hash of field-values)!\n";
    }
    
    my $dbh = GetDatabaseHandler();

    if ((!$members->{'db'}) && $members->{'db_id'})
    {
        # load SourceDatabase object
        my $db = Greenphyl::SourceDatabase->new(
            $dbh,
            {
                'selectors' =>
                    {
                        'id' => $members->{'db_id'},
                    },
            },
        );

        if ($db)
        {
            $members->{'db'} = $db;
        }
    }

    if (!$members->{'db'} || (ref($members->{'db'}) ne 'Greenphyl::SourceDatabase'))
    {
        confess "ERROR: Missing or invalid database member for new DBXRef!\n";
    }

    if (!$members->{'accession'})
    {
        confess "ERROR: Missing accession member for new DBXRef!\n";
    }

    my @columns = ('db_id', 'accession');
    my @values  = ($members->{'db'}->id, $members->{'accession'});
    
    if ($members->{'description'})
    {
        push(@columns, 'description');
        push(@values, $members->{'description'});
    }

    if ($members->{'version'})
    {
        push(@columns, 'version');
        push(@values, $members->{'version'});
    }
    
    my $sql_query = "INSERT INTO dbxref(" . join(', ', @columns) . ") VALUE (?" . (', ?' x $#columns) . ");";
    $dbh->do($sql_query, undef, @values)
        or confess "ERROR: Failed to insert new DBXRef: " . $dbh->errstr;

    # try to get DBXRef again
    my $dbxref = Greenphyl::DBXRef->new($dbh, {'selectors' => {'accession' => $members->{'accession'}, 'db_id' => $members->{'db'}->id,}});

    if (!$dbxref)
    {
        confess "ERROR: Failed to retrieve newly created DBXRef!\n";
    }

    return $dbxref;
}


=pod

=head2 AddPubMedDBXRef

B<Description>: Adds a new PubMed DBXRef to the database.

B<ArgsCount>: 1

=over 4

=item $members: (hash ref) (R)

DBXRef members hash. Keys are member field names and values are associated
values. 1 member is requiered:
-'accession' or 'pmid': the numeric part of the PMID (text part auto-removed);
Optional member:
-'description': PubMed text description. If omitted, it will be auto-fetched
from the web;

B<Return>: (Greenphyl::DBXRef)

The new PubMed DBXRef created.

=cut

sub AddPubMedDBXRef
{
    my ($members) = @_;

    if (!$members)
    {
        confess "ERROR: Unable to create a new PubMed DBXRef: no member provided!\n";
    }

    if (ref($members) ne 'HASH')
    {
        confess "ERROR: Invalid members provided (not a hash of field-values)!\n";
    }
    
    $members->{'accession'} ||= $members->{'pmid'};
    if (!$members->{'accession'})
    {
        confess "ERROR: Missing PMID accession!\n";
    }
    # only keep numbers
    $members->{'accession'} =~ s/^\D+//; # only leading non-numbers
    if ($members->{'accession'} !~ m/^\d+$/)
    {
        confess "ERROR: Invalid PMID accession '" . $members->{'accession'} . "'!\n";
    }

    my $dbh = GetDatabaseHandler();

    # load PubMed DB object
    my $pubmed_db = Greenphyl::SourceDatabase->new($dbh, {'selectors' => {'name' => 'PubMed'}})
        or confess "ERROR: PubMed source database reference not found in database!\n";

    $members->{'db'} = $pubmed_db;
    
    # fecth description from the web if needed
    if (!$members->{'description'})
    {
        my $biblio = GetBioBiblio();
        my $pubmed_xml = $biblio->get_by_id($members->{'accession'});
        $members->{'description'} = $pubmed_xml->title;
    }

    return AddDBXRef($members);
}




=pod

=head1 AUTHORS

Mathieu ROUARD (Bioversity), m.rouard@cgiar.org

Valentin GUIGNON (Bioversity), v.guignon@cgiar.org

Matthieu CONTE (Syngenta)

Sebastien BRIOIS (Syngenta)

=head1 VERSION

Version 1.0.0

Date 16/05/2013

=head1 SEE ALSO

GreenPhyl documentation.

=cut

return 1; # package return
