=pod

=head1 NAME

Greenphyl::Tools::MyList - manages "My List"

=head1 SYNOPSIS

    use Greenphyl::Tools::MyList;


=head1 REQUIRES

Perl5

=head1 EXPORTS

FetchCurrentMyListHash GetCurrentMyList ClearCurrentMyList GetEntryTypeAndID
GetMultipleEntryTypeAndID GetSortedMyListObjects GetMyListAllSequences

=head1 DESCRIPTION

Manages My List.

=cut

package Greenphyl::Tools::MyList;


use strict;
use warnings;

use Carp qw(cluck confess croak);

use base qw(Exporter);

our @EXPORT = qw(
    FetchCurrentMyListHash GetCurrentMyList SetCurrentMyList ClearCurrentMyList GetEntryTypeAndID
    GetMultipleEntryTypeAndID GetSortedMyListObjects GetMyListAllSequences
);

use Greenphyl;
use Greenphyl::MyList;
use Greenphyl::MyListItem;




# Package constants
####################

=pod

=head1 CONSTANTS

B<$DEBUG>: (boolean)

When set to true, it enables debug mode.

=cut

our $DEBUG = 0;




# Package variables
####################

=pod

=head1 VARIABLES

B<$g_mylist>: (hash of objects)

My List hash of hash. First level keys are object types, second level keys are
object IDs.

=cut

my $g_mylist;




# Package subs
###############

=pod

=head1 FUNCTIONS

=head2 FetchCurrentMyListHash

B<Description>: Retrieves My List content from session object. It is a hash ref
where keys are entry types (family or sequence) and values are hashes which
keys are entry identifiers.

B<ArgsCount>: 0

B<Return>: (hash ref)

My List hash of hash. First level keys are object types, second level keys are
object IDs.

=cut

sub FetchCurrentMyListHash
{
    # get mylist from session
    my $session = Greenphyl::Web::GetSession();
    $g_mylist ||= $session->param('mylist');

    if (!ref($g_mylist))
    {
        # not in sessions, init mylist in session
        $session->param('mylist', {} );
        $g_mylist = $session->param('mylist');
    }

    return $g_mylist;
}


=pod

=head2 GetCurrentMyList

B<Description>: Retrieves My List content from session object and turns it into
a Greenphyl::MyList object.

B<ArgsCount>: 0

B<Return>: (Greenphyl::MyList)

Current My List as a Greenphyl::MyList object.

=cut

sub GetCurrentMyList
{
    my $mylist_hash = FetchCurrentMyListHash();

    my $current_user = Greenphyl::Web::Access::GetCurrentUser();

    my $mylist_data = {
        'description'    => '',
        'id'             => -1,
        'name'           => 'Current My List',
        'user_id'        => $current_user->id,
        'user'           => $current_user,
        'items'          => [],
    };
    
    foreach my $object_type (keys(%$mylist_hash))
    {
        foreach my $object_id (keys(%{$mylist_hash->{$object_type}}))
        {
            my $mylist_item = Greenphyl::MyListItem->new(
                    GetDatabaseHandler(),
                    {
                        'load' => 'none',
                        'members' => {
                            'my_list_id' => -1,
                            'foreign_id' => $object_id,
                            'type'       => $object_type,
                        },
                    },
                )
            ;
            push(@{$mylist_data->{'items'}}, $mylist_item);
        }
    }

    my $mylist = Greenphyl::MyList->new(
            undef,
            {
                'load' => 'none',
                'members' => $mylist_data,
            },
        )
    ;

    return $mylist;
}


=pod

=head2 SetCurrentMyList

B<Description>: Sets My List content. The parameter can be either a hash ref
with the same structure as the one described in FetchCurrentMyListHash or a
Greenphyl::MyList object.

B<ArgsCount>: 0-1

=over 4

=item $mylist: (Greenphyl::MyList or hash ref) (R)

A MyList object or hash of a MyList object.

=back

B<Return>: nothing

=cut

sub SetCurrentMyList
{
    my ($mylist) = @_;
    
    # get mylist from session
    my $session = Greenphyl::Web::GetSession();

    if ($mylist && (ref($mylist) eq 'Greenphyl::MyList'))
    {
        PrintDebug('Loading list from a MyList object');
        $mylist = $mylist->getAsMyListHash();
    }

    if (!$mylist || (ref($mylist) ne 'HASH'))
    {
        confess "ERROR: Invalid mylist parameter for SetCurrentMyList!\n";
    }

    $session->param('mylist', $mylist);
    $g_mylist = $session->param('mylist');
    $g_mylist = $mylist;
    return $g_mylist;
}


=pod

=head2 ClearCurrentMyList

B<Description>: Remove all entries from My List.

B<ArgsCount>: 0

B<Return>: (hash ref)

My List hash.

=cut

sub ClearCurrentMyList
{
    PrintDebug('Clear current my list');
    # get mylist from session
    my $session = Greenphyl::Web::GetSession();
    $session->param('mylist', {} );
    $g_mylist = $session->param('mylist');

    return $g_mylist;
}


=pod

=head2 GetEntryTypeAndID

B<Description>: return entry type and ID.

B<ArgsCount>: 0

B<Return>: (string, string)

The entry type and ID.

=cut

sub GetEntryTypeAndID
{
    my $type = GetParameterValues('type')
        or confess "ERROR: MyList: no item type given!\n";
    my $id   = GetParameterValues('id')
        or confess "ERROR: MyList: no item ID given!\n"; # note: ID '0' not supported

    return ($type, $id);
}


=pod

=head2 GetMultipleEntryTypeAndID

B<Description>: return entry type and ID.
ID CGI field names can be specified using CGI field "mylist.mapping" with a value
like "type_name~id_field_name".
ex.: "mylist.mapping=Sequence~seq_id;CustomSequence~cseq_id,cseq_id2;Family~family_id CustomFamily~custom_family_id".

B<ArgsCount>: 0

B<Return>: (hash ref)

A hash that maps object types to array of IDs.
ex.:
{
  'CustomSequence' => [3, 12, 14],
  'Sequence' => [42],
}

=cut

sub GetMultipleEntryTypeAndID
{
    my $mapping = {};

    my @mappings = GetParameterValues('mapping', 'mylist', '[;\s]+')
        or confess "ERROR: MyList: no mapping given!\n";

    if (@mappings)
    {
        foreach my $mapping_string (@mappings)
        {
            my ($type, $form_fields) = ($mapping_string =~ m/(\w+)~(.*)/);
            if ($type
                && exists($Greenphyl::MyListItem::ITEM_TYPES_TO_CLASS->{$type})
                && $form_fields
                && $form_fields =~ m/\w/)
            {
                $mapping->{$type} ||= [];
                foreach my $id_field (grep { $_ } (split(/,+/, $form_fields)))
                {
                    my @ids = GetParameterValues($id_field, undef, '[,;\s]+');
                    foreach my $id (@ids)
                    {
                        # only keep valid (numeric) IDs
                        if ($id && ($id =~ m/^\d+$/))
                        {
                            push(@{$mapping->{$type}}, $id);
                        }
                    }
                }
            }
            else
            {
                confess "ERROR: MyList: invalid ID mapping ($mapping_string)!\n";
            }
        }
    }
    else
    {
        PrintDebug('WARNING: MyList: no mapping found!');
    }

    return $mapping;
}


=pod

=head2 GetSortedMyListObjects

B<Description>: Returns a hash associating entry types to array of objects.

B<ArgsCount>: 0-1

=over 4

=item $mylist: (Greenphyl::MyList) (O)

A MyList object.
Default: current My List.

=back

B<Return>: (hash ref)

A hash ref of a hash of objects arrays.

=cut

sub GetSortedMyListObjects
{
    my ($mylist) = @_;

    $mylist ||= GetCurrentMyList();

    my %objects_hash;
    my $objects = $mylist->getObjects();

    foreach my $object (@$objects)
    {
        my $object_type = $object->getObjectType();
        $object_type =~ s/^Cached//;
        $objects_hash{$object_type} ||= [];
        push(@{$objects_hash{$object_type}}, $object);
    }

    return \%objects_hash;
}


=pod

=head2 GetMyListAllSequences

B<Description>: Returns an array ref of sequence/custom sequence objects
including sequences comming from families.

B<ArgsCount>: 0-1

=over 4

=item $mylist: (Greenphyl::MyList) (O)

My List object from wich sequences should be extracted.
Default: current My List.

=back

B<Return>: (array ref)

An array of sequence (Greenphyl::Sequence, Greenphyl::CustomSequence) objects.

=cut

sub GetMyListAllSequences
{

    my ($mylist) = @_;

    my $objects = GetSortedMyListObjects();

    # use a hash to have unique sequences
    my $sequences = {};

    # get selected sequences first
    if (exists($objects->{'Sequence'}))
    {
        foreach my $sequence (@{$objects->{'Sequence'}})
        {
            $sequences->{$sequence->id} = $sequence;
        }
    }

    # get selected custom sequences
    if (exists($objects->{'CustomSequence'}))
    {
        foreach my $custom_sequence (@{$objects->{'CustomSequence'}})
        {
            $sequences->{'C' . $custom_sequence->id} = $custom_sequence;
        }
    }

    # add sequences from families
    if (exists($objects->{'Family'}))
    {
        foreach my $family (@{$objects->{'Family'}})
        {
            foreach my $sequence (@{$family->sequences})
            {
                $sequences->{$sequence->id} ||= $sequence;
            }
        }
    }

    # add sequences from custom families
    if (exists($objects->{'CustomFamily'}))
    {
        foreach my $family (@{$objects->{'CustomFamily'}})
        {
            foreach my $custom_sequence (@{$family->sequences})
            {
                $sequences->{'C' . $custom_sequence->id} ||= $custom_sequence;
            }
        }
    }

    # get all sequences from hash values
    return [values(%$sequences)];
}


=pod

=head1 AUTHORS

Mathieu ROUARD (Bioversity), m.rouard@cgiar.org

Valentin GUIGNON (Bioversity), v.guignon@cgiar.org

Matthieu CONTE (Syngenta)

Sebastien BRIOIS (Syngenta)

=head1 VERSION

Version 1.1.0

Date 10/09/2013

=head1 SEE ALSO

GreenPhyl documentation.

=cut

return 1; # package return
