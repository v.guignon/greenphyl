=pod

=head1 NAME

Greenphyl::Tools::News - Library of news management functions

=head1 SYNOPSIS

    use Greenphyl::Tools::News;

=head1 REQUIRES

Perl5

=head1 EXPORTS

ListNews GetNewsFilters

=head1 DESCRIPTION

Library of news management functions.

=cut

package Greenphyl::Tools::News;


use strict;
use warnings;

use Carp qw(cluck confess croak);

use base qw(Exporter);

our @EXPORT = qw(
    ListNews GetNewsFilters
);

use Greenphyl;




# Package constants
####################

=pod

=head1 CONSTANTS

B<$DEBUG>: (boolean)

When set to true, it enables debug mode.

=cut

our $DEBUG = 0;



# Package subs
###############

=pod

=head1 FUNCTIONS

=head2 ListNews

B<Description>: Returns an array of news template.

B<ArgsCount>: 0-1

=over 4

=item $parameters: (hash ref) (O)

News filter parameters:
-'name': only return news corresponding to that name.
-'offset': offset in the list.
-'limit': maximum number of items to return.
-'order': can be 'asc' or 'desc' for news (file name) ordering.

=back

B<Return>: (array ref)

An array of news template as strings. Each news template is the name of the
template file including the ".tt" extension but without the path.

=cut

sub ListNews
{
    my ($parameters) = @_;
    $parameters ||= {};
    my @news_list = map { $_ =~ s/^.*\///g; $_; } glob(GP('NEWS_PATH') . '/*.tt');
    PrintDebug("Got " . scalar(@news_list) . " initial news files ('" . join("' ,'", @news_list) . "').");
    
    # name
    if ($parameters->{'name'})
    {
        if ('ARRAY' eq ref($parameters->{'name'}))
        {
            # name list
            my $name_array = join('|', map {$_ =~ s/[^\w.\-]//g; $_;} @{$parameters->{'name'}});
            @news_list = grep {m/^(?:$name_array)\.tt$/} @news_list;
        }
        elsif ('HASH' eq ref($parameters->{'name'}))
        {
            # name range or set
            #+FIXME: not supported yet
        }
        else
        {
            # single name
            my $news_name = $parameters->{'name'};
            $news_name =~ s/[^\w.\-]//g;
            @news_list = grep {m/^$news_name\.tt$/} @news_list;
        }
    }

    # order
    if (!exists($parameters->{'order'}))
    {
        # default order sort descending
        @news_list = sort {$b cmp $a} @news_list;
    }
    elsif ($parameters->{'order'} eq 'asc')
    {
        @news_list = sort @news_list;
    }
    
    #offset and limit
    my $offset = $parameters->{'offset'} || 0;
    my $limit = $parameters->{'limit'} || 0;
    if ($limit)
    {
        @news_list = splice(@news_list, $offset, $limit);
    }
    elsif ($offset)
    {
        @news_list = splice(@news_list, $offset);
    }

    return \@news_list;
}


=pod

=head2 GetNewsFilters

B<Description>: Returns a news filtration hash.

B<ArgsCount>: 0

B<Return>: (hash ref)

A hash containing news filtration parameters as used by ListNews.

=cut

sub GetNewsFilters
{
    my $filter_parameters = {};
    if (my $limit = GetParameterValues('limit'))
    {
        $filter_parameters->{'limit'} = $limit;
    }
    if (my $offset = GetParameterValues('offset'))
    {
        $filter_parameters->{'offset'} = $offset;
    }
    if (my $order = GetParameterValues('order'))
    {
        $filter_parameters->{'order'} = $order;
    }

    my @news_names = GetParameterValues('news');
    if (@news_names)
    {
        if (1 == @news_names)
        {
            $filter_parameters->{'name'} = $news_names[0];
        }
        else
        {
            $filter_parameters->{'name'} = \@news_names;
        }
    }
    
    return $filter_parameters;
}


=pod

=head1 AUTHORS

Mathieu ROUARD (Bioversity), m.rouard@cgiar.org

Valentin GUIGNON (Bioversity), v.guignon@cgiar.org

Matthieu CONTE (Syngenta)

Sebastien BRIOIS (Syngenta)

=head1 VERSION

Version 1.0.0

Date 20/02/2014

=head1 SEE ALSO

GreenPhyl documentation.

=cut

return 1; # package return
