=pod

=head1 NAME

Greenphyl::Tools::IPR - IPR tools

=head1 SYNOPSIS


=head1 REQUIRES

Perl5

=head1 EXPORTS

LoadIPR GenerateIPRFamilyRelationshipJSONData

=head1 DESCRIPTION

Provides several helper function to process IPR and generate associated 
data structures.

=cut

package Greenphyl::Tools::IPR;


use strict;
use warnings;

use Carp qw(cluck confess croak);

use base qw(Exporter);

our @EXPORT = qw(
    LoadIPR GenerateIPRFamilyRelationshipJSONData
); 

use JSON;

use Greenphyl;
use Greenphyl::Family;
use Greenphyl::FamilyIPRStats;




# Package constants
####################

=pod

=head1 CONSTANTS

B<$DEBUG>: (boolean)

Enables debug mode.

=cut

our $DEBUG = 0;




# Package subs
###############

=pod

=head1 FUNCTIONS

=head2 LoadIPR

B<Description>: Loads and returns an array of IPR objects. Throws an exception
if no IPR could be loaded. The IPR is selected using either (by priority) the
HTTP parameters 'ipr_code' or 'ipr_id'.

B<ArgsCount>: 0

B<Return>: (array ref)

An array of loaded IPR as Greenphyl::IPR objects.

=cut

sub LoadIPR
{
    my @given_ipr_code = GetParameterValues('ipr_code');
    my @given_ipr_id = GetParameterValues('ipr_id');
    my (@ipr_code, @ipr_id);

    foreach my $given_ipr_id (@given_ipr_id)
    {
        if ($given_ipr_id =~ m/^\d{1,10}$/)
        {
            push(@ipr_id, $given_ipr_id);
        }
    }
    foreach my $given_ipr_code (@given_ipr_code)
    {
        if ($given_ipr_code =~ m/^$Greenphyl::IPR::IPR_REGEXP$/o)
        {
            push(@ipr_code, $given_ipr_code);
        }
    }
    
    my $selectors = {};
    if (@ipr_code)
    {
        $selectors = {'code' => ['IN', @ipr_code]};
    }
    elsif (@ipr_id)
    {
        $selectors = {'id' => ['IN', @ipr_id]};
    }
    else
    {
        Throw('error' => 'No valid IPR identifier provided!');
    }

    my @ipr_list = Greenphyl::IPR->new(
        GetDatabaseHandler(),
        {'selectors' => $selectors }
    );

    if (!@ipr_list)
    {
        if (@ipr_code)
        {
            Throw('error' => "No corresponding IPR found!", 'log' => "Given codes: " . join(', ', @given_ipr_code) . "\nValid codes: " . join(', ', @ipr_code));
        }
        else
        {
            Throw('error' => "No corresponding IPR ID found!", 'log' => "Given IDs: " . join(', ', @given_ipr_id) . "\nValid IDs: " . join(', ', @ipr_id));
        }
    }

    return \@ipr_list;
}




=pod

=head2 GenerateIPRFamilyRelationshipJSONData

B<Description>: Generate IPR family relationship JSON data.

B<ArgsCount>: 1-2

=over 4

=item $family: (Greenphyl::Family) (R)

Current family object.

=item $parent_family: (Greenphyl::Family) (O)

The parent family object if one.

=back

B<Return>: (string)

A JSON string containing an array of hash containing the following keys:
-'id': a family database ID
-'accession': current family accession
-'children': an array ref containing hash refs similar to the current one
-'count': number of sequences bearing the IPR in current family
-'common': a hash wich keys are family_id and values are hash:
  -'count': total number of common sequences bearing the IPR;
-'selector': a CSS selector that can identify a corresponding DOM object;

=cut

sub GenerateIPRFamilyRelationshipJSONData
{
    my ($ipr_list) = @_;

    my %ipr_id_to_ipr;
    my @ipr_ids = map { $ipr_id_to_ipr{$_->id} = $_; $_->id; } @$ipr_list;

    # get IPR specificity
    # my $ipr_stats = Greenphyl::FamilyIPRStats->new(
    #     GetDatabaseHandler(),
    #     {
    #         'selectors' => {'ipr_id' => ['IN', @ipr_ids]},
    #     }
    # );
    my $sql_query = 'SELECT family_id, ipr_id, family_specific
        FROM families_ipr_cache
        WHERE
            ipr_id IN (?' . (', ?' x $#ipr_ids) . ');';
    my $family_ipr_specificities_raw = GetDatabaseHandler()->selectall_arrayref($sql_query, undef, @ipr_ids)
        or confess GetDatabaseHandler()->errstr;
    my $family_ipr_specificities = {};
    foreach my $family_ipr_specificity (@$family_ipr_specificities_raw)
    {
        # 0 => family_id
        # 1 => ipr_id
        # 2 => specificity
        $family_ipr_specificities->{$family_ipr_specificity->[0]} ||= {};
        $family_ipr_specificities->{$family_ipr_specificity->[0]}->{$family_ipr_specificity->[1]} = $family_ipr_specificity->[2];
    }
    
    

    # get families and sequences bearing the IPR domains
    $sql_query = 'SELECT sii.sequence_id, sha.ipr_id, sii.family_id
        FROM families_sequences sii
            JOIN ipr_sequences sha ON sii.sequence_id = sha.sequence_id
        WHERE
            sha.ipr_id IN (?' . (', ?' x $#ipr_ids) . ');';
    my $sequence_family_for_iprs = GetDatabaseHandler()->selectall_arrayref($sql_query, undef, @ipr_ids)
        or confess GetDatabaseHandler()->errstr;

    # store family to ipr and to sequences association in a hash of hash of hash
    # structure:
    # {
    #     <family_id> =>
    #         {
    #             'ipr' =>
    #                 {
    #                     <ipr_id> =>
    #                         {
    #                             <sequence_id> => 1,
    #                             <sequence_id> => 1,
    #                             ...
    #                         },
    #                     <ipr_id> =>
    #                         ...
    #                 },
    #             'sequences' =>
    #                 {
    #                     <sequence_id> => <ipr_domain_count>,
    #                     <sequence_id> => <ipr_domain_count>,
    #                     ...
    #                 },
    #         },
    #     <family_id> =>
    #         ...
    #     ...
    # }
    my $family_to_ipr_and_sequences = {};
    # sequence to families associations (keys=sequence_id, values=array of family_id)
    my $sequence_to_families = {};
    foreach my $sequence_family (@$sequence_family_for_iprs)
    {
        # 0 => sequence_id
        # 1 => ipr_id
        # 2 => family_id
        # dedicace Mathieu: several orchish manoeuvres ;)
        $family_to_ipr_and_sequences->{$sequence_family->[2]} ||= {'ipr' => {}, 'sequences' => {}};
        $family_to_ipr_and_sequences->{$sequence_family->[2]}->{'ipr'}->{$sequence_family->[1]} ||= {};
        $family_to_ipr_and_sequences->{$sequence_family->[2]}->{'ipr'}->{$sequence_family->[1]}->{$sequence_family->[0]} = 1;
        # count sequence IPR domains
        $family_to_ipr_and_sequences->{$sequence_family->[2]}->{'sequences'}->{$sequence_family->[0]} ||= 0;
        $family_to_ipr_and_sequences->{$sequence_family->[2]}->{'sequences'}->{$sequence_family->[0]}++;
        # store sequence to families association
        $sequence_to_families->{$sequence_family->[0]} ||= [];
        push(@{$sequence_to_families->{$sequence_family->[0]}}, $sequence_family->[2]);
    }

    # get all related families (by ascending level as we will init lower levels first)
    my @family_ids = keys(%$family_to_ipr_and_sequences);
    my @families = Greenphyl::Family->new(
        GetDatabaseHandler(),
        {
            'selectors' => {'id' => ['IN', @family_ids]},
            'sql' => {'ORDER BY' => 'level ASC'},
        }
    );

    # tree structure that contains root families (with their children)
    my $tree_structure = [];
    my $family_to_family_ipr_data = {};
    foreach my $family (@families)
    {
        my $family_ipr_data = $family_to_family_ipr_data->{$family->id} =
            {
                'id'        => $family->id,
                'label'     => $family->accession,
                'type'      => 'family-ipr',
                # count sequences with at least one of the selected IPR
                'count'     => scalar(keys(%{$family_to_ipr_and_sequences->{$family->id}->{'sequences'}})),
                'iprs'      => { }, # counts and specificities by IPR (filled below)
                'total_count' => $family->seq_count, # total number of sequences in family
                'common'    => {}, # common sequence counts by family (filled below)
                'children'  => [], # array of child family IPR data structures (filled below by children)
                'selector'  => '.family-' . $family->accession,
            }
        ;
        # root family?
        if (1 == $family->level)
        {
            push(@$tree_structure, $family_ipr_data);
        }

        # specificity by IPR
        my $ipr_id;
        foreach $ipr_id (keys(%{$family_ipr_specificities->{$family->id}}))
        {
            $family_ipr_data->{'iprs'}->{$ipr_id_to_ipr{$ipr_id}}->{'specificity'} = $family_ipr_specificities->{$family->id}->{$ipr_id};
        }
        # counts by IPR
        foreach $ipr_id (keys(%{$family_to_ipr_and_sequences->{$family->id}->{'ipr'}}))
        {
            $family_ipr_data->{'iprs'}->{$ipr_id_to_ipr{$ipr_id}}->{'count'} = scalar(keys(%{$family_to_ipr_and_sequences->{$family->id}->{'ipr'}->{$ipr_id}}));
        }
        
        # extra temporary hash to keep track of children already added
        my $parent_to_child = {}; # keys are parent ID, values are has which keys are child ID
        # extra temporary hash to keep track of sequences alread counted
        my $processed_sequences = {}; # keys are sequence ID
        # loop on sequences to get stats
        foreach my $sequence_id (keys(%{$family_to_ipr_and_sequences->{$family->id}->{'sequences'}}))
        {
            # loop on each common family
            foreach my $common_family_id (@{$sequence_to_families->{$sequence_id}})
            {
                # load family object from cache
                my $common_family = Greenphyl::Family->new(GetDatabaseHandler(), { 'selectors' => {'id' => $common_family_id} });
                # check level
                if ($common_family->level == $family->level - 1)
                {
                    # common family is a parent
                    # make sure it has not already been added
                    $parent_to_child->{$common_family_id} ||= {};
                    if (!exists($parent_to_child->{$common_family_id}->{$family->id}))
                    {
                        # add current family to parent's children list
                        push(@{$family_to_family_ipr_data->{$common_family_id}->{'children'}}, $family_ipr_data);
                        $parent_to_child->{$common_family_id}->{$family->id} = 1;
                    }
                    # make sure we did not already count the common sequence
                    $processed_sequences->{$common_family_id} ||= {};
                    if (!exists($processed_sequences->{$common_family_id}->{$sequence_id}))
                    {
                        # fill stats about sequences
                        $family_ipr_data->{'common'}->{$common_family_id} ||= {'count' => 0,};
                        $family_ipr_data->{'common'}->{$common_family_id}->{'count'}++;
                        $processed_sequences->{$common_family_id}->{$sequence_id} = 1;
                    }
                }
                elsif ($common_family->level == $family->level + 1)
                {
                    # common family is a children
                    # make sure we did not already count the common sequence
                    $processed_sequences->{$common_family_id} ||= {};
                    if (!exists($processed_sequences->{$common_family_id}->{$sequence_id}))
                    {
                        # fill stats about sequences
                        $family_ipr_data->{'common'}->{$common_family_id} ||= {'count' => 0,};
                        $family_ipr_data->{'common'}->{$common_family_id}->{'count'}++;
                        $processed_sequences->{$common_family_id}->{$sequence_id} = 1;
                    }
                }
                # otherwise, nothing to do
            }
        }
    }

    my $json = to_json($tree_structure);

    return {'JSON' => $json,};
}


=pod

=head1 AUTHORS

Mathieu ROUARD (Bioversity), m.rouard@cgiar.org

Valentin GUIGNON (Bioversity), v.guignon@cgiar.org

Matthieu CONTE (Syngenta)

Sebastien BRIOIS (Syngenta)

=head1 VERSION

Version 1.0.0

Date 08/10/2012

=head1 SEE ALSO

GreenPhyl documentation.

=cut

return 1; # package return
