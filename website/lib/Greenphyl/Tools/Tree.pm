=pod

=head1 NAME

Greenphyl::Tools::Tree - Tree tools

=head1 SYNOPSIS

    my $tree    = GetTree({'family' => $family, 'format' => 'xml'});

=head1 REQUIRES

Perl5

=head1 EXPORTS

GetTree

=head1 DESCRIPTION

Provides several helper function to process trees.

=cut

package Greenphyl::Tools::Tree;


use strict;
use warnings;

use Carp qw(cluck confess croak);

use base qw(Exporter);

our @EXPORT = qw(
    GetTree
); 

use JSON;

use Greenphyl;
use Greenphyl::Web;
use Greenphyl::Species;
use Greenphyl::Tools::Families;




# Package constants
####################

=pod

=head1 CONSTANTS

B<$DEBUG>: (boolean)

Enables debug mode.

=cut

our $DEBUG = 0;




# Package subs
###############

=pod

=head1 FUNCTIONS

=head2 GetTree

B<Description>: Return the phylogenetic tree corresponding to the given family
in the requested format.
Whene parameters are not provided to the function, it will try to fetch them
using GetParameterValues() function.

B<ArgsCount>: 0-2

=over 4

=item $output_format: (string) (U)

requested tree file format.

=item $parameters: (hash ref) (O)

Hash of parameters:
-'family' (Greenphyl::Family): family of interest
-'remove' (array ref): array of species codes of species to remove

=back


B<Return>: (string)

.

=cut

sub GetTree
{
    my ($output_format, $parameters) = @_;
    
    $parameters ||= {};
    
    # get family (note: LoadFamily() throws an exception if no family is loaded)
    my $family = $parameters->{'family'} || LoadFamily();

    # get expected tree format
    my $format = $output_format || GetParameterValues('format');
    if (!$format || ($format !~ m/^(?:xml|nwk|newick)$/i))
    {
        $format = 'newick';
    }
    $format = lc($format);
    
    # check if species should be removed
    my $discarded_species_code = $parameters->{'remove'} || [GetParameterValues('remove', undef, '[,;\s]+')];
    my $species_to_remove = [];
    # check if there are species to remove
    if ($discarded_species_code && @$discarded_species_code)
    {
        # Removed because new version supports newick.
        # if ($format ne 'xml')
        # {
        #     PrintDebug('WARNING: tree format changed to XML because of species removal');
        #     $format = 'xml';
        # }
        # check species
        $species_to_remove = [Greenphyl::Species->new(GetDatabaseHandler(), {'selectors' => {'code' => ['IN', @$discarded_species_code]}})];
        if (scalar(@$species_to_remove) != scalar(@$discarded_species_code))
        {
            PrintDebug('WARNING: some of the specified species code of species to remove were not found in database. Code provided: ' . join(', ', @$discarded_species_code));
        }
        PrintDebug("Got " . scalar(@$species_to_remove) . " to remove.");
    }


    my $tree_file = GetPhylogenyAnalyzesPath($family, $format);

    my $tree_content = '';
    if ($tree_file)
    {
        my $tree_fh;
        if (@$species_to_remove)
        {
            # remove requested species
            # -prepare command
            # ex.:  java -jar TreeFilter.jar -input GP015132_rap_tree.xml -remove CYAME -remove POPTR -remove ARATH
            my $command = GP('JAVA_COMMAND') . ' -jar ' . GP('APPLETS_PATH') . '/' . GP('TREEFILTER_APPLET') . ' -input ' . $tree_file . '  -output /dev/stdout';
            if ($output_format =~ m/^(?:nwk|newick)$/i)
            {
                $command .= ' -newick';
            }
            foreach my $species (@$species_to_remove)
            {
                $command .= ' -remove ' . $species->code;
            }
            PrintDebug("COMMAND: $command");
            $tree_content = `$command`;
        }
        elsif (open($tree_fh, $tree_file))
        {
            # return original tree
            $tree_content = join('', <$tree_fh>);
            close($tree_fh);
        }
        else
        {
            # failed to open file
            Throw('error' => "Unable to read tree for family $family!", 'log' => "File: '$tree_file'");
        }
    }
    else
    {
        # tree not available
        Throw('error' => "Tree not available for family $family!");
    }

    # return tree content
    return $tree_content;
}




=pod

=head1 AUTHORS

Mathieu ROUARD (Bioversity), m.rouard@cgiar.org

Valentin GUIGNON (Bioversity), v.guignon@cgiar.org

Matthieu CONTE (Syngenta)

Sebastien BRIOIS (Syngenta)

=head1 VERSION

Version 1.0.0

Date 21/03/2013

=head1 SEE ALSO

GreenPhyl documentation.

=cut

return 1; # package return
