=pod

=head1 NAME

Greenphyl::Dumper - Dumps GreenPhyl objects.

=head1 SYNOPSIS

   use Greenphyl::Dumper;


=head1 REQUIRES

Perl5

=head1 EXPORTS

DumpPageData DumpObjects LoadObjectsToDump GetDumpFormat GetDumpFileName
EncodeDumpParameterValue DecodeDumpParameters GetDumpParameters

=head1 DESCRIPTION

API to dump GreenPhyl objects into various format.
Forms for Dumper should provide the following CGI fields either in global
namespace or in a common namespace:
'format': a string containing the requested output format type
    ex.: 'format=fasta'
'file_name': the name of the output file (to provide to client)
    ex.: 'file_name=sequences.fasta'
'object_type': the type of object to dump. Must be one of the keys of
    %OBJECTS_TYPES.
'fields': contains the name of the CGI fields that will contain objects
    identifier/selectors. Several fields can be specified using coma as
    separator.
    ex.: 'fields=seq_id,seq_id2,seq_name'
'mapping': tells what CGI field correspond to what database field. A field
    mapping consists of a CGI field name followed by the tild sign '~'
    followed by the database field name. Several fields mapping can be joined
    using comas.
    If omitted, the default mapping assumes a CGI field name correspond to the
    same field name in database.
    ex.: 'mapping=seq_id~id,seq_id2~id,seq_name~accession'
'parameters': a frozen PERL structure containing additional dump parameters for
    object dump methods.
...and a list of identifiers/selectors using the value of 'fields' as field
    name. If line feed/carriage return characters are found for a single field,
    then its content is split in several values using LF/CR and treated as
    several separated values (using the same field mapping).
    

If a parameter is not found inside a namespace, then the functions try to load
it from the global namespace.
The fetching order of parameters is given by 'GetParameterValues' function.
First, it fetches the parameters from the namespace from the GET method,
then from the namespace from the POST method, then without namespace from
the GET method and finaly without namespace from the POST method.
Please refer to Greenphyl.pm documentaion for details.

=cut

package Greenphyl::Dumper;


use strict;
use warnings;

use Carp qw(cluck confess croak);

use base qw(Exporter);
our @EXPORT =
    qw(
        DumpPageData DumpObjects LoadObjectsToDump GetDumpFormat GetDumpFileName
        EncodeDumpParameterValue DecodeDumpParameters GetDumpParameters
    );

use Data::Dumper;
use URI::Escape;

use Greenphyl;




# Package constants
####################

=pod

=head1 CONSTANTS

B<$DEBUG>: (boolean)

When set to true, it enables debug mode.

B<$OBJECTS_TYPES>: (hash)

Hash containing the supported dumpable object types as keys and their
associated module as values.

=cut

our $DEBUG = 0;
our %OBJECTS_TYPES =
    (
        'sequence'   => 'Greenphyl::Sequence',
        'sequences'  => 'Greenphyl::Sequence',
        'custom_sequence'  => 'Greenphyl::CustomSequence',
        'custom_sequences' => 'Greenphyl::CustomSequence',
        'family'     => 'Greenphyl::Family',
        'families'   => 'Greenphyl::Family',
        'custom_family'    => 'Greenphyl::CustomFamily',
        'custom_families'  => 'Greenphyl::CustomFamily',
        'ipr'        => 'Greenphyl::IPR',
        'iprs'       => 'Greenphyl::IPR',
        'species'    => 'Greenphyl::Species',
        'composite'  => 'Greenphyl::CompositeObject',
        'composites' => 'Greenphyl::CompositeObject',
    );
our $MAPPING_SEPARATOR  = ',';
our $MAPPING_ASSIGNATOR = '~';




# Package subs
###############

=pod

=head1 STATIC METHODS

=head2 GetDumpFormat

B<Description>: gets the dump format either from command line or CGI GET or
POST parameters.

B<ArgsCount>: 1

=over 4

=item $namespace: (string) (O)

Namespace the format parameter may belong to.
Defaut: no namespace.

=back

B<Return>: a string containing a valid dump format name or undef.

=cut

sub GetDumpFormat
{
    my ($namespace) = @_;
    my $format = GetParameterValues('format', $namespace);
    if ($format && ($format !~ m/^\w+$/))
    {$format = undef;}

    return $format;
}


=pod

=head2 GetDumpFileName

B<Description>: gets the dump file name either from command line or CGI GET or
POST parameters.

B<ArgsCount>: 1

=over 4

=item $namespace: (string) (O)

Namespace the file name parameter may belong to.
Defaut: no namespace.

=back

B<Return>: a string containing a file name or undef.

=cut

sub GetDumpFileName
{
    my ($namespace) = @_;
    my $file_name = GetParameterValues('file_name', $namespace);
    # if ($file_name =~ m/[^\w\/\.\-\~]/)
    if ($file_name && ($file_name !~ m/^[\w\.\-]+$/))
    {$file_name = undef;}

    return $file_name;
}


=pod

=head2 LoadObjectsToDump

B<Description>: load objects to dump using either from command line or CGI GET
or POST parameters to find which objects to load.

B<ArgsCount>: 1

=over 4

=item $namespace: (string) (O)

Namespace the object parameters may belong to.
Defaut: no namespace.

=back

B<Return>: (array ref)
an array containing the objects.

=cut

sub LoadObjectsToDump
{
    my ($namespace) = @_;
    # get object type
    my $object_type  = GetParameterValues('object_type', $namespace);
    # make sure it's a valid type
    if (!$object_type
        || ($object_type !~ m/^\w+$/)
        || !exists($OBJECTS_TYPES{$object_type}))
    {
        confess "ERROR: invalid or unsupported object type to dump " . ($object_type?"($object_type) ":'') . "or no type (object_type) specified!\n";
    }
    eval "use $OBJECTS_TYPES{$object_type};";
    if ($@)
    {confess $@;}

    # get fields to load
    my $fields       = GetParameterValues('fields', $namespace);
    # make sure we got fields
    if (!$fields)
    {
        confess "ERROR: no fields available to fetch objects to dump!\n";
    }

    # get fields mapping
    my $fields_mapping = {};
    my $fields_mapping_param = GetParameterValues('mapping', $namespace);
    if ($fields_mapping_param)
    {
        # mapping specified, parse
        my @mapping = split(/\s*$MAPPING_SEPARATOR\s*/o, $fields_mapping_param);
        PrintDebug("Got mapping parameter: $fields_mapping_param (" . scalar(@mapping). ")");
        foreach my $mapping (@mapping)
        {
            # make sure we only got valid characters
            $mapping =~ s/^[\s\r\n]*//gs;
            $mapping =~ s/[\s\r\n]*$//gs;
            PrintDebug("parsing '$mapping'");
            if ($mapping =~ m/^\w+$MAPPING_ASSIGNATOR\w+$/o)
            {
                # get mapping data
                my ($cgi_field_name, $db_field_name) = split(/$MAPPING_ASSIGNATOR/o, $mapping);
                $fields_mapping->{$cgi_field_name} = $db_field_name;
                PrintDebug("Got mapping parameter: '$cgi_field_name' into '$db_field_name'");
            }
        }
    }

    PrintDebug("Starting to load $OBJECTS_TYPES{$object_type} to dump from fields '$fields'...");

    # get the field values
    my $object_selectors = {};
    my @fields = split(/,/, $fields);
    foreach my $field_name (@fields)
    {
        PrintDebug("processing field '$field_name'...");

        # check field name
        if ($field_name !~ m/^\w+$/)
        {
            $field_name =~ s/\&/&amp;/g;
            $field_name =~ s/</&lt;/g;
            $field_name =~ s/>/&gt;/g;
            confess "ERROR: Invalid field name '$field_name' for object dump!\n";
        }

        # in case some mapping info are missing, use CGI field name as default
        my $db_field_name = $fields_mapping->{$field_name} || $field_name;
        PrintDebug("mapping '$field_name' to '$db_field_name'");

        # check for single or multiple values
        my @form_values = GetParameterValues($field_name, $namespace);
        my @multi_values_separator = GetParameterValues('separator', $namespace);
        my %SEPARATORS_REGEXP =
            (
                'eol'   => '\s*[\n\r]+\s*',
                'blank' => '\s+',
                ' '     => ' +',
                ','     => '\s*,+\s*',
                '.'     => '\s*\.+\s*',
                ':'     => '\s*:+\s*',
                ';'     => '\s*;+\s*',
                '|'     => '\s*\|+\s*',
                '+'     => '\s*\++\s*',
                '-'     => '\s*-+\s*',
                '~'     => '\s*~+\s*',
                '#'     => '\s*#+\s*',
                '&'     => '\s*&+\s*',
            );
        my @values = @form_values;
        foreach my $multi_values_separator (@multi_values_separator)
        {
            PrintDebug("using separator '$multi_values_separator' to split inputs");
            my @splitted_values = ();
            if ($multi_values_separator && exists($SEPARATORS_REGEXP{$multi_values_separator}))
            {
                $multi_values_separator = $SEPARATORS_REGEXP{$multi_values_separator};
                foreach my $value (@values)
                {
                    # check if several values are stored inside a same string using
                    # CR/LF to separate them
                    foreach (split(/$multi_values_separator/, $value))
                    {
                        if (defined($_) && ('' ne $_))
                        {
                            push(@splitted_values, $_);
                        }
                    }
                }
                @values = @splitted_values;
            }
        }

        if (1 < @values)
        {
            PrintDebug("selector: $db_field_name IN (" . join(', ', @values) . ")");
            $object_selectors->{$db_field_name} = ['IN', @values];
        }
        elsif (1 == @values)
        {
            PrintDebug("selector: $db_field_name = $values[0]");
            $object_selectors->{$db_field_name} = $values[0];
        }
        else
        {
            PrintDebug("Warning: selector '$db_field_name' ($field_name) with no values!");
        }
    }

    # load objects
    my @objects;
    if ($object_type eq 'composite')
    {
        # composite objects
        foreach my $selector (keys(%$object_selectors))
        {
            # check for a single value or an array
            if (ref($object_selectors->{$selector}))
            {
                foreach my $identifier (splice(@{$object_selectors->{$selector}}, 1))
                {
                    push(@objects, "$OBJECTS_TYPES{$object_type}"->new({$selector => $identifier,}));
                }
            }
            else
            {
                push(@objects, "$OBJECTS_TYPES{$object_type}"->new({$selector => $object_selectors->{$selector},}));
            }
        }
    }
    else
    {
        # database objects
        push(@objects, "$OBJECTS_TYPES{$object_type}"->new(GetDatabaseHandler(), {'selectors' => $object_selectors}));
    }
    PrintDebug("loaded " . scalar(@objects) . " objects");

    # sort objects if needed
    my $sort_order = GetParameterValues('sort_order', $namespace);
    my $sort_field = GetParameterValues('sort_field', $namespace);
    # $sort_field =~ 
    if ($sort_field && ($sort_field =~ m/^[\w\.\*]+$/))
    {
        my $field_path = [split('\.', $sort_field)];
        # check sort order
        # allowed orders are: 'cmp', 'rcmp', 'icmp', 'ricmp'
        # default: 'cmp'
        $sort_order ||= 'cmp';
        PrintDebug("objects will be sorted using '$sort_order' through field '$sort_field'");
        if ($sort_order =~ /^icmp$/)
        {
            @objects = sort {$a->StringifyObjectMember($field_path) <=> $b->StringifyObjectMember($field_path)} @objects;
        }
        elsif ($sort_order =~ /^ricmp$/)
        {
            @objects = sort {$b->StringifyObjectMember($field_path) <=> $a->StringifyObjectMember($field_path)} @objects;
        }
        elsif ($sort_order =~ /^rcmp$/)
        {
            @objects = sort {$b->StringifyObjectMember($field_path) cmp $a->StringifyObjectMember($field_path)} @objects;
        }
        else # cmp
        {
            @objects = sort {$a->StringifyObjectMember($field_path) cmp $b->StringifyObjectMember($field_path)} @objects;
        }
    }
        
    return \@objects;
}


=pod

=head2 _CheckWrongDumpParameters

B<Description>: make sure half-encoded dump parameters do not contain invalid
characters. Throws an error if something wrong has been found.

Valid characters inside strings are:
alpha-numeric characters, spaces, '.', '-', '+', '_', '[', ']', '{', and '}'.
Exception: a star '*' is allowed if preceded by a dot '.'.

Why these restrictions? Because we use "Data::Dumper" to procude a stringified
version of the given parameter and we use "eval" to expand that stringified
version. Therefore, instead of doing heavy parsing and checking of user-provided
data in order to make sure there's no hack, we just check if thoses
special characters are at the right place. It's more restrictive but more secure
as well.

B<ArgsCount>: 1

=over 4

=item $parameters: (string) (R)

Half-encoded (url-decoded) dump parameters.

=back

B<Return>: nothing

=cut

sub _CheckWrongDumpParameters
{
    my ($half_encoded_parameters) = @_;
    if (($half_encoded_parameters =~ m/(?:=>|,)\s*[^\s\d'\{\[]/x) # matches any '=>' or ',' followed by something else than a space, a digit, a quote, an opening curly bracket or an opening bracket
        || ($half_encoded_parameters =~ m/=[^>]/) # matches any equal sign followed by something else than a '>'
        || ($half_encoded_parameters =~ m/[^\.]\*/) # matches any star preceded by something else than a '.'
        || ($half_encoded_parameters =~ m/[^\w\s=>,'\{\[\]\}\.\*\-\+]/)) # matches any unwanted character at any place
    {
        Throw('error' => "Invalid dump parameters!", 'log' => "parameters: $half_encoded_parameters");
    }
}


=pod

=head2 EncodeDumpParameterValue

B<Description>: encodes dump parameter value into a string.

B<ArgsCount>: 1

=over 4

=item $parameter_value: (string, array ref or hash ref) (R)

supported parameters are only digits, string, array ref or hash ref that may
contain any other set or subsets of digits, string, array or hash with the same
restrictions. No objects or function or handles are allowed at any level.
Important: string values must not contain certain characters. See 
_CheckWrongDumpParameters for details

=back

B<Return>: (string)

a string containing the encoded parameters.

=cut

sub EncodeDumpParameterValue
{
    my ($parameter_value) = @_;

    my $dumper = Data::Dumper->new([$parameter_value]);
    # we don't wan't new lines nor indentation
    $dumper->Indent(0);
    #$dumper->Maxdepth(3); # depth limitation
    # dump parameter value into a string
    my $stringified_parameters = $dumper->Dump();
    # remove Dump $VAR1 part
    $stringified_parameters =~ s/^\s*\$VAR1\s*=\s*(.*)\s*;\s*$/$1/;
    # security check: make sure we don't have other things than expected
    _CheckWrongDumpParameters($stringified_parameters);

    # URL-encode
    #$stringified_parameters = uri_escape($stringified_parameters);
    $stringified_parameters = $stringified_parameters;
    
    return $stringified_parameters;
}


=pod

=head2 DecodeDumpParameters

B<Description>: decode dump parameters from a URL string into a Perl structure.

B<ArgsCount>: 1

=over 4

=item $parameters: (string) (R)

String containing parameters generated using EncodeDumpParameterValue function.
Important: string values must not contain certain characters. See 
_CheckWrongDumpParameters for details

=back

B<Return>: (digit, string, array ref or hash ref)

the parameters structure.

=cut

sub DecodeDumpParameters
{
    my ($encoded_parameters) = @_;

    # may not be necessary but decode URL-encoded params
    my $decoded_parameters = uri_unescape($encoded_parameters);

    # security check: make sure we don't have other things than expected
    _CheckWrongDumpParameters($decoded_parameters);

    # if we got a string, quote it
    if ($decoded_parameters =~ m/^\s*[^\d\[\{]/)
    {
        $decoded_parameters = "'$decoded_parameters'";
    }

    my $parameters;
    eval "\$parameters = $decoded_parameters;\n";

    if ($@)
    {
        Throw('error' => "Invalid dump parameters!", 'log' => "parameters to decode: $decoded_parameters");
    }

    return $parameters;
}


=pod

=head2 GetDumpParameters

B<Description>: gets dump parameters that will be passed to object loading
function.

B<ArgsCount>: 1

=over 4

=item $namespace: (string) (O)

Namespace the dump parameters may belong to.
Defaut: no namespace.

=back

B<Return>: (ref of a hash of (digit, string, array ref or hash ref))

the parameters structure. Each key of the hash is a parameter name and its
associated value can be a digit, a string, an array ref or a hash ref.

=cut

sub GetDumpParameters
{
    my ($namespace) = @_;
    $namespace ||= '';
    my $parameters = {};
    if (my $parameters_list = GetParameterValues('parameters', $namespace))
    {
        foreach my $parameter_name (split(/\s*,\s*/, $parameters_list))
        {
            if ($parameter_name && ($parameter_name =~ m/^\w/))
            {
                my @parameter_values = GetParameterValues($parameter_name);
                # remove namespace from name if one (before using parameter as hash key)
                $parameter_name =~ s/^$namespace\.//;
                $parameters->{$parameter_name} = [];
                foreach my $parameter_value (@parameter_values)
                {
                    $parameters->{$parameter_name} = DecodeDumpParameters($parameter_value);
                }
            }
        }
    }

    return $parameters;
}


=pod

=head2 DumpPageData

B<Description>: Dump objects using current page settings.

B<ArgsCount>: 0

B<Return>: (string)

Dumped content.

=cut

sub DumpPageData
{
    # check if we got namespaces to checks
    my @namespaces   = GetParameterValues('namespace');
    my $namespace    = GetAction(\@namespaces);
    
    # get dump format
    my $dump_format     = GetDumpFormat($namespace);
    
    # get file name to display
    my $file_name       = GetDumpFileName($namespace);

    # get objects
    my $objects         = LoadObjectsToDump($namespace);

    # get dump parameters
    my $parameters      = GetDumpParameters($namespace);
    
    # dump data
    my $dump_content = DumpObjects($objects, $dump_format, $parameters);

    # get header after content in case we got an error during content generation
    # so we can still generate a clean header

    my $header = '';
    if (Greenphyl::IsRunningCGI())
    {
        eval 'use Greenphyl::Web;';
        if ($@)
        {confess $@;}

        if ($file_name)
        {
            # to file
            $header = Greenphyl::Web::RenderHTTPFileHeader({'file_name' => $file_name, 'format' => $dump_format});
        }
        else
        {
            if ($dump_format =~ /^json$/i)
            {
                # to screen
                $header = Greenphyl::Web::RenderHTTPFileHeader({'mime' => 'JSON', 'format' => $dump_format});
            }
            else
            {
                # to screen
                $header = Greenphyl::Web::RenderHTTPFileHeader({'mime' => 'TEXT', 'format' => $dump_format});
            }
        }
    }
    else
    {
        #+FIXME: non-CGI mode: if a file name has been specified, dump into the file
    }

    return $header . $dump_content;
}


=pod

=head2 DumpObjects

B<Description>: Dump given objects into the given format.

B<ArgsCount>: 2-3

=over 4

=item $objects: (object ref or array of objects of the same type) (R)

Object(s) to dump.

=item $dump_format: (string) (R)

name of the format.

=item $parameters: (any type) (O)

Additional parameters for the dumper.

=back

B<Return>: (string)

Dumped content.

=cut

sub DumpObjects
{
    my ($objects, $dump_format, $parameters) = @_;
    my $dump_content = '';
    if (!ref($objects))
    {
        confess "ERROR: Trying to dump a scalar (" . $objects . ") while a dumpable object was expected!\n";
    }

    if (ref($objects) eq 'ARRAY')
    {
        if (@$objects)
        {
            my $class = ref($objects->[0]);
            # multiple objects
            if (!$class->isa('Greenphyl::DumpableObject'))
            {
                confess "ERROR: Trying to dump a non-dumpable object (" . $class . ")!\n";
            }

            if ($class->can('Dump'))
            {
                $dump_content = $class->Dump($dump_format, $objects, $parameters);
            }
            elsif ($class->can('dump'))
            {
                foreach my $object (@$objects)
                {
                    $dump_content .= $object->dump($dump_format, $parameters);
                }
            }
            else
            {
                confess "ERROR: no dump method available for object " . $class . "\n";
            }
        }
        elsif ('json' eq $dump_format)
        {
            # No result.
            $dump_content = '[]';
        }
    }
    else
    {
        # single object
        if (!$objects->isa('Greenphyl::DumpableObject'))
        {
            confess "ERROR: Trying to dump a non-dumpable object (" . ref($objects) . ")!\n";
        }

        if ($objects->can('dump'))
        {
            $dump_content = $objects->dump($dump_format, $parameters);
        }
        elsif ($objects->can('Dump'))
        {
            $dump_content = $objects->Dump($dump_format, $objects, $parameters);
        }
        else
        {
            confess "ERROR: no dump method available for object " . ref($objects) . "\n";
        }
    }
    return $dump_content;
}




=pod

=head1 AUTHORS

Mathieu ROUARD (Bioversity), m.rouard@cgiar.org

Valentin GUIGNON (Bioversity), v.guignon@cgiar.org

Matthieu CONTE (Syngenta)

=head1 VERSION

Version 1.0.0

Date 02/05/2012

=head1 SEE ALSO

GreenPhyl documentation. DB Objects documentation.

=cut

return 1; # package return
