=pod

=head1 NAME

Greenphyl::UserOpenID - GreenPhyl user OpenID Object

=head1 SYNOPSIS

    use Greenphyl::UserOpenID;
    my $openid = Greenphyl::UserOpenID->new($dbh, {'selectors' => {'user_id' => 42, 'priority' => 0,}});
    print $openid->identity();

=head1 REQUIRES

Perl5

=head1 EXPORTS

Nothing

=head1 DESCRIPTION

This module implements a GreenPhyl user OpenID database object.

=cut

package Greenphyl::UserOpenID;

use strict;
use warnings;

use base qw(Greenphyl::CachedDBObject);

use vars qw($AUTOLOAD);

use Carp qw(cluck confess croak);

use Greenphyl::OpenID;



# Package constants
####################

=pod

=head1 CONSTANTS

B<$OBJECT_PROPERTIES>: (hash ref)

object properties description. See DBObject constructor documentation for
details.

=cut

our $OBJECT_PROPERTIES = {
    'table' => 'user_openids',
    'key' => 'user_id,priority',
    'alternate_keys' => ['openid_url','openid_identity'],
    'load'     => [
        'openid_identity',
        'openid_url',
        'priority',
        'user_id',
    ],
    'alias' => {
        'identity' => 'openid_identity',
        'url'      => 'openid_url',
    },
    'base' => {
        'openid_identity' => {
            'property_column' => 'openid_identity',
        },
        'openid_url' => {
            'property_column' => 'openid_url',
        },
        'priority' => {
            'property_column' => 'priority',
        },
        'user_id' => {
            'property_column' => 'user_id',
        },
    },
    'secondary' => {
        'user' => {
            'object_key'     => 'user_id',
            'property_table'  => 'users',
            'property_key'    => 'id',
            'multiple_values' => 0,
            'property_module' => 'Greenphyl::User',
        },
    },
    'tertiary' => {
    },

};




# Package subs
###############

=pod

=head1 STATIC METHODS

=head2 CONSTRUCTOR

B<Description>: Creates a new instance of a GreenPhyl user OpenID object.

B<ArgsCount>: 2

=over 4

=item $dbh: (DBI::db) (U)

Database handler of the database to load the object from.

=item $parameters: (hash ref) (R)

See Greenphyl::DBObject constructor documentation for details.

=back

B<Return>: (Greenphyl::UserOpenID)

a new instance.

B<Caller>: General

B<Example>:

    my $openid = Greenphyl::UserOpenID->new($dbh, {'selectors' => {'user_id' => 42, 'priority' => 0,}});

=cut

sub new
{
    my $proto = shift();
    my $class = ref($proto) || $proto;
    
    my $object_properties = $class->GetDefaultClassProperties();

    return $class->SUPER::new($object_properties, @_);
}




=pod

=head1 METHODS

=head2 openid

B<Description>: returns the OpenID object associated to this user Open ID if one
or undef otherwise.

B<Alias>: getOpenID

B<ArgsCount>: 1

=over 4

=item $self: (Greenphyl::UserOpenID) (R)

Current user Open ID.

=back

B<Return>: (Greenphyl::OpenID)

The OpenID object associated to this user Open ID if one or undef otherwise.

=cut

sub openid
{
    my ($self) = @_;
    my $openid = Greenphyl::OpenID->new($self->{'_dbh'}, {'selectors' => {'url' => $self->openid_url, 'enabled' => 1}});
    if ($openid)
    {
        return $openid;
    }
    else
    {
        return undef;
    }
}
sub getOpenID; *getOpenID = \&openid;




=pod

=head1 AUTHORS

Mathieu ROUARD (Bioversity), m.rouard@cgiar.org

Valentin GUIGNON (Bioversity), v.guignon@cgiar.org

Matthieu CONTE (Syngenta)

=head1 VERSION

Version 1.0.0

Date 23/01/2014

=head1 SEE ALSO

GreenPhyl documentation, DBObject documentation.

=cut

return 1; # package return
