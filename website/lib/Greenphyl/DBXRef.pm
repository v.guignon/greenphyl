=pod

=head1 NAME

Greenphyl::DBXRef - GreenPhyl DBXRef Object

=head1 SYNOPSIS

    use Greenphyl::DBXRef;
    my $dbxref = Greenphyl::DBXRef->new($dbh, {'selectors' => {'id' => 42}});
    print $dbxref->name();

=head1 REQUIRES

Perl5

=head1 EXPORTS

Nothing

=head1 DESCRIPTION

This module implements a GreenPhyl DBXRef database object.

=cut

package Greenphyl::DBXRef;

use strict;
use warnings;

use base qw(Greenphyl::CachedDBObject);

use vars qw($AUTOLOAD);

use Carp qw(cluck confess croak);




# Package constants
####################

=pod

=head1 CONSTANTS

B<$OBJECT_PROPERTIES>: (hash ref)

object properties description. See DBObject constructor documentation for
details.

=cut

our $OBJECT_PROPERTIES = {
    'table' => 'dbxref',
    'key' => 'id',
    'alternate_keys' => [],
    'load'     => [
        'accession',
        'db_id',
        'description',
        'id',
        'version',
    ],
    'alias' => {
        'dbxref_id' => 'id',
    },
    'base' => {
        'accession' => {
            'property_column' => 'accession',
        },
        'db_id' => {
            'property_column' => 'db_id',
        },
        'id' => {
            'property_column' => 'id',
        },
        'description' => {
            'property_column' => 'description',
        },
        'version' => {
            'property_column' => 'version',
        },
    },
    'secondary' => {
        'db' => {
            'object_key'      => 'db_id',
            'property_table'  => 'db',
            'property_key'    => 'id',
            'multiple_values' => 0,
            'property_module' => 'Greenphyl::SourceDatabase',
        },
        'synonyms' => {
            'property_table'  => 'dbxref_synonyms',
            'property_key'    => 'dbxref_id',
            'multiple_values' => 1,
            'property_column' => 'synonym',
        },
    },
    'tertiary' => {
        'sequences' => {
            'link_table'        => 'dbxref_sequences',
            'link_object_key'   => 'dbxref_id',
            'link_property_key' => 'sequence_id',
            'property_module'   => 'Greenphyl::Sequence',
            'property_table'    => 'sequences',
            'property_key'      => 'id',
            'multiple_values'   => 1,
        },
        'families' => {
            'object_key'        => 'dbxref_id',
            'link_table'        => 'dbxref_families',
            'link_object_key'   => 'dbxref_id',
            'link_property_key' => 'family_id',
            'property_module'   => 'Greenphyl::Family',
            'property_table'    => 'families',
            'property_key'      => 'family_id',
            'multiple_values'   => 1,
        },
    },

};




# Package subs
###############

=pod

=head1 STATIC METHODS

=head2 CONSTRUCTOR

B<Description>: Creates a new instance of a GreenPhyl DBXRef object.

B<ArgsCount>: 2

=over 4

=item $dbh: (DBI::db) (U)

Database handler of the database to load the object from.

=item $parameters: (hash ref) (R)

See Greenphyl::DBObject constructor documentation for details.

=back

B<Return>: (Greenphyl::DBXRef)

a new instance.

B<Caller>: General

B<Example>:

    my $dbxref = new Greenphyl::DBXRef($dbh, {'selectors' => {'id' => 42}});

=cut

sub new
{
    my $proto = shift();
    my $class = ref($proto) || $proto;

    my $object_properties = $class->GetDefaultClassProperties();

    return $class->SUPER::new($object_properties, @_);
}




=pod

=head1 METHODS

=head2 getStringValue

B<Description>: returns database cross-reference accession.

B<ArgsCount>: 1

=over 4

=item $self: (Greenphyl::DBXRef)

the DBXRef.

=back

B<Return>: (string)

the DBXRef accession.

=cut

sub getStringValue
{
    my ($self) = @_;
    # return $self->{'accession'} . ($self->{'version'} ? '-' . $self->{'version'} : '');
    return $self->{'accession'};
}




=pod

=head1 AUTHORS

Mathieu ROUARD (Bioversity), m.rouard@cgiar.org

Valentin GUIGNON (Bioversity), v.guignon@cgiar.org

Matthieu CONTE (Syngenta)

=head1 VERSION

Version 1.0.0

Date 17/04/2012

=head1 SEE ALSO

GreenPhyl documentation, DBObject documentation.

=cut

return 1; # package return
