=pod

=head1 NAME

Greenphyl::SequenceCount - GreenPhyl SequenceCount Object

=head1 SYNOPSIS

    use Greenphyl::SequenceCount;
    my @seq_counts = Greenphyl::SequenceCount->new($dbh, {'family_id' => 205});
    print "Sequences in family 205:\n";
    foreach (@seq_counts)
    {
        print "* " . $_->species_name . ": " . $_->sequence_count . "\n";
    }

=head1 REQUIRES

Perl5

=head1 EXPORTS

Nothing

=head1 DESCRIPTION

This module implements a GreenPhyl count_family_seq database object.

=cut

package Greenphyl::SequenceCount;

use strict;
use warnings;

use base qw(Greenphyl::DBObject);

use vars qw($AUTOLOAD);

use Carp qw(cluck confess croak);




# Package constants
####################

=pod

=head1 CONSTANTS

B<$OBJECT_PROPERTIES>: (hash ref)

object properties description. See DBObject constructor documentation for
details.

=cut

our $OBJECT_PROPERTIES = {
    'table' => 'sequence_count_by_families_species_cache',
    'key' => 'family_id,species_id',
    'alternate_keys' => [],
    'load'     => [
        'family_id',
        'sequence_count',
        'sequence_count_without_splice',
        'species_id',
        'species_name',
    ],
    'alias' => {
        'number' => 'sequence_count',
    },
    'base' => {
        'family_id' => {
            'property_column' => 'family_id',
        },
        'sequence_count' => {
            'property_column' => 'sequence_count',
        },
        'sequence_count_without_splice' => {
            'property_column' => 'sequence_count_without_splice',
        },
        'species_id' => {
            'property_column' => 'species_id',
        },
    },
    'secondary' => {
        'species' => {
            'object_key'      => 'species_id',
            'property_table'  => 'species',
            'property_key'    => 'id',
            'multiple_values' => 0,
            'property_module' => 'Greenphyl::Species',
        },
        'species_code' => {
            'object_key'      => 'species_id',
            'property_table'  => 'species',
            'property_key'    => 'id',
            'multiple_values' => 0,
            'property_column' => 'code',
        },
        'species_name' => {
            'object_key'      => 'species_id',
            'property_table'  => 'species',
            'property_key'    => 'id',
            'multiple_values' => 0,
            'property_column' => 'organism',
        },
        'taxonomy_id' => {
            'object_key'      => 'species_id',
            'property_table'  => 'species',
            'property_key'    => 'id',
            'multiple_values' => 0,
            'property_column' => 'taxonomy_id',
        },
        'family' => {
            'object_key'      => 'family_id',
            'property_table'  => 'families',
            'property_key'    => 'id',
            'multiple_values' => 0,
            'property_module' => 'Greenphyl::Family',
        },
    },
    'tertiary' => {
    },
};




# Package subs
###############

=pod

=head1 STATIC METHODS

=head2 CONSTRUCTOR

B<Description>: Creates a new instance of a GreenPhyl SequenceCount object.

B<ArgsCount>: 2

=over 4

=item $dbh: (DBI::db) (U)

Database handler of the database to load the object from.

=item $parameters: (hash ref) (R)

See Greenphyl::DBObject constructor documentation for details.

=back

B<Return>: (Greenphyl::SequenceCount)

a new instance.

B<Caller>: General

B<Example>:

    my $sequence_count_s2_f205 = new Greenphyl::SequenceCount($dbh, {'selectors' => {'species' => 2, 'family_id' => 205}});

=cut

sub new
{
    my $proto = shift();
    my $class = ref($proto) || $proto;
    
    my $object_properties = $class->GetDefaultClassProperties();

    return $class->SUPER::new($object_properties, @_);
}


=pod

=head1 METHODS

=pod

=head2 getStringValue

B<Description>: returns family species sequence count.

B<ArgsCount>: 1

=over 4

=item $self: (Greenphyl::SequenceCount)

the sequence count object.

=back

B<Return>: (string)

a string with the family ID, the species name and the sequence count.

=cut

sub getStringValue
{
    my ($self) = @_;
    return $self->{'family_id'} . '[' . $self->{'species_name'} . ']=' . $self->{'sequence_count'};
}




=pod

=head1 AUTHORS

Mathieu ROUARD (Bioversity), m.rouard@cgiar.org

Valentin GUIGNON (Bioversity), v.guignon@cgiar.org

Matthieu CONTE (Syngenta)

=head1 VERSION

Version 1.0.0

Date 05/04/2012

=head1 SEE ALSO

GreenPhyl documentation.

=cut

return 1; # package return
