=pod

=head1 NAME

Greenphyl::FamilyProcessing - GreenPhyl Family Processing Object

=head1 SYNOPSIS

    use Greenphyl::FamilyProcessing;
    my $family_processing = Greenphyl::FamilyProcessing->new($dbh, {'selectors' => {'process_id' => 42, 'family_id' = 806,}});
    print $family_processing->name();

=head1 REQUIRES

Perl5

=head1 EXPORTS

Nothing

=head1 DESCRIPTION

This module implements a GreenPhyl FamilyProcessing database object.

=cut

package Greenphyl::FamilyProcessing;

use strict;
use warnings;

use base qw(Greenphyl::CachedDBObject);

use vars qw($AUTOLOAD);

use Carp qw(cluck confess croak);




# Package constants
####################

=pod

=head1 CONSTANTS

B<$OBJECT_PROPERTIES>: (hash ref)

object properties description. See DBObject constructor documentation for
details.

=cut

our $DEBUG = 0;
our $OBJECT_PROPERTIES = {
    'table' => 'family_processing',
    'key' => 'family_id,process_id',
    'alternate_keys' => [],
    'load'     => [
        'family_id',
        'process_id',
        'status',
        'steps_performed',
        'steps_to_perform',
    ],
    'alias' => {
    },
    'base' => {
        'family_id' => {
            'property_column' => 'family_id',
        },
        'process_id' => {
            'property_column' => 'process_id',
        },
        'status' => {
            'property_column' => 'status',
        },
        'steps_performed' => {
            'property_column' => 'steps_performed',
        },
        'steps_to_perform' => {
            'property_column' => 'steps_to_perform',
        },
    },
    'secondary' => {
        'process' => {
            'object_key'     => 'process_id',
            'property_table'  => 'processes',
            'property_key'    => 'id',
            'multiple_values' => 0,
            'property_module' => 'Greenphyl::AnalysisProcess',
        },
        'family' => {
            'object_key'     => 'family_id',
            'property_table'  => 'families',
            'property_key'    => 'id',
            'multiple_values' => 0,
            'property_module' => 'Greenphyl::Family',
        },
    },
    'tertiary' => {
    },
};




# Package subs
###############

=pod

=head1 STATIC METHODS

=head2 CONSTRUCTOR

B<Description>: Creates a new instance of a GreenPhyl FamilyProcessing object.

B<ArgsCount>: 2

=over 4

=item $dbh: (DBI::db) (U)

Database handler of the database to load the object from.

=item $parameters: (hash ref) (R)

See Greenphyl::DBObject constructor documentation for details.

=back

B<Return>: (Greenphyl::FamilyProcessing)

a new instance.

B<Caller>: General

B<Example>:

    my $family_processing = new Greenphyl::FamilyProcessing($dbh, {'selectors' => {'process_id' => 42, 'family_id' = 806,}});

=cut

sub new
{
    my $proto = shift();
    my $class = ref($proto) || $proto;
    
    my $object_properties = $class->GetDefaultClassProperties();

    return $class->SUPER::new($object_properties, @_);
}




=pod

=head1 AUTHORS

Mathieu ROUARD (Bioversity), m.rouard@cgiar.org

Valentin GUIGNON (Bioversity), v.guignon@cgiar.org

Matthieu CONTE (Syngenta)

=head1 VERSION

Version 1.0.0

Date 17/04/2012

=head1 SEE ALSO

GreenPhyl documentation, DBObject documentation.

=cut

return 1; # package return
