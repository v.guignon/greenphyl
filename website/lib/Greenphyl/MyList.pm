=pod

=head1 NAME

Greenphyl::MyList - GreenPhyl MyList Object

=head1 SYNOPSIS

    use Greenphyl::MyList;
    my $my_list = Greenphyl::MyList->new($dbh, {'selectors' => {'user_id' => 1}});
    print $my_list->name();

=head1 REQUIRES

Perl5

=head1 EXPORTS

Nothing

=head1 DESCRIPTION

This module implements a GreenPhyl MyList database object.

=cut

package Greenphyl::MyList;

use strict;
use warnings;

use base qw(Greenphyl::CachedDBObject);

use vars qw($AUTOLOAD);

use Carp qw(cluck confess croak);

use Greenphyl;




# Package constants
####################

=pod

=head1 CONSTANTS

B<$OBJECT_PROPERTIES>: (hash ref)

object properties description. See DBObject constructor documentation for
details.

=cut

our $DEBUG = 0;

our $OBJECT_PROPERTIES = {
    'table' => 'my_lists',
    'key' => 'id',
    'alternate_keys' => [],
    'load'     => [
        'description',
        'id',
        'name',
        'user_id',
    ],
    'alias' => {
    },
    'base' => {
        'description' => {
            'property_column' => 'description',
        },
        'id' => {
            'property_column' => 'id',
        },
        'name' => {
            'property_column' => 'name',
        },
        'user_id' => {
            'property_column' => 'user_id',
        },
    },
    'secondary' => {
        'items' => {
            'object_key'      => 'id',
            'property_table'  => 'my_list_items',
            'property_key'    => 'my_list_id',
            'multiple_values' => 1,
            'property_module' => 'Greenphyl::MyListItem',
        },
        'user' => {
            'object_key'      => 'user_id',
            'property_table'  => 'users',
            'property_key'    => 'id',
            'multiple_values' => 0,
            'property_module' => 'Greenphyl::User',
        },
    },
    'tertiary' => {
    },

};

our $ITEM_TYPES_ALIASES = {
    'Sequence'        => 'Sequence',
    'Sequences'       => 'Sequence',
    'Family'          => 'Family',
    'Families'        => 'Family',
    'CustomSequence'  => 'CustomSequence',
    'CustomSequences' => 'CustomSequence',
    'CustomFamily'    => 'CustomFamily',
    'CustomFamilies'  => 'CustomFamily',
};




# Package subs
###############

=pod

=head1 STATIC METHODS

=head2 CONSTRUCTOR

B<Description>: Creates a new instance of a GreenPhyl MyList object.

B<ArgsCount>: 2

=over 4

=item $dbh: (DBI::db) (U)

Database handler of the database to load the object from.

=item $parameters: (hash ref) (R)

See Greenphyl::DBObject constructor documentation for details.

=back

B<Return>: (Greenphyl::MyList)

a new instance.

B<Caller>: General

B<Example>:

    my $my_list = new Greenphyl::MyList($dbh, {'selectors' => {'user_id' => 1}});

=cut

sub new
{
    my $proto = shift();
    my $class = ref($proto) || $proto;
    
    my $object_properties = $class->GetDefaultClassProperties();

    return $class->SUPER::new($object_properties, @_);
}




=pod

=head1 METHODS

=head2 getObjects

B<Description>: returns an array of GreenPhyl objects.

B<Alias>: objects

B<ArgsCount>: 1-2

=over 4

=item $self: (Greenphyl::MyList) (R)

Current MyList object.

=item $requested_types: (array) (O)

Array of item types like the field 'type' of the table 'my_list_items'.

=back

B<Return>: (array ref)

array of Greenphyl::* objects belonging to the list.

=cut

sub getObjects
{
    my ($self, $requested_types) = @_;

    # check if objects have already been fetched
    if (!$self->{'objects'})
    {
        $self->{'objects'} = [];
        # init by types    
        foreach my $object_type (keys(%$ITEM_TYPES_ALIASES))
        {
            $self->{$object_type} = [];
        }

        # get all items and sort them by type
        foreach my $item ($self->items)
        {
            if ($item)
            {
                my $object = $item->getObject();
                if ($object)
                {
                    push(@{$self->{'objects'}},   $object);
                    push(@{$self->{$item->type}}, $object);
                }
            }
        }
    }

    # check if a string (scalar) was provided as object type
    if ($requested_types && !ref($requested_types))
    {
        # turn it into an array
        $requested_types = [$requested_types];
    }

    # check for default value: all types
    if (!$requested_types || !@$requested_types)
    {
        return [@{$self->{'objects'}}];
    }

    # get unique valid type
    my %valid_types;
    foreach my $requested_type (@$requested_types)
    {
        if (exists($ITEM_TYPES_ALIASES->{$requested_type}))
        {
            $valid_types{$ITEM_TYPES_ALIASES->{$requested_type}} = 1;
        }
        else
        {
            PrintDebug("MyList::getObjects: Invalid type requested: '$requested_type'!");
        }
    }

    # get only related objects
    my @objects;
    foreach my $valid_type (keys(%valid_types))
    {
        if ($self->{$valid_type} && @{$self->{$valid_type}})
        {
            push(@objects, @{$self->{$valid_type}});
        }
    }

    # return all common sequences
    return \@objects;
}
sub objects; *objects = \&getObjects;


=pod

=head2 getAsMyListHash

B<Description>: returns an array of GreenPhyl objects.

B<Alias>: 

B<ArgsCount>: 1-2

=over 4

=item $self: (Greenphyl::MyList) (R)

Current MyList object.

=item $item_types: (array) (O)

Array of item types like the field 'type' of the table 'my_list_items'.

=back

B<Return>: (array ref)

array of Greenphyl::* objects belonging to the list.

=cut

sub getAsMyListHash
{
    my ($self) = @_;
    
    my $my_list_hash = {};
    
    foreach my $item ($self->items())
    {
        $my_list_hash->{$item->type} ||= {};
        $my_list_hash->{$item->type}->{$item->foreign_id} = 1;
    }

    return $my_list_hash;
}


=pod

=head1 AUTOLOAD

B<Description>: Autogeneration of setters and getters.

This autoload handles object types. For instance:

    $my_list->families()
    $my_list->sequences()
    $my_list->custom_sequences()
    $my_list->GetCustomSequences()

Other members are supported through parent (DBObject) autoloader.

=cut

sub AUTOLOAD
{
    my ($self, $value) = @_;
    (my $method_name = $AUTOLOAD) =~ s/.*://; # strip fully-qualified portion
    return unless $method_name =~ m/[a-z]/;  # skip DESTROY and all-cap methods

    # static calls
    if (!ref($self))
    {
        confess "ERROR: unknown/unhandled static method $method_name!\n";
    }

    # not an object
    if (ref($self) =~ m/^(ARRAY|HASH|SCALAR|GLOB)$/)
    {
        confess "ERROR: not an object: $self\n";
    }
    
    my ($accessor, $member) = ($method_name =~ m/^(get|)(.+)/);

    # convert member name from method name convention to class name convention
    # custom_sequence => CustomSequence
    $member =~ s/^([a-z])/uc($1)/eg;
    $member =~ s/_+([a-z])/uc($1)/eg;

    # check if member is supported
    if (exists($ITEM_TYPES_ALIASES->{$member}))
    {
        # if it's a supported object type, returns its list
        return $self->getObjects($member);
    }
    else
    {
        # call default DBObject handler
        my $super_method = 'SUPER::' . $method_name;
        shift();
        return $self->$super_method(@_);
    }
}


=pod

=head1 AUTHORS

Mathieu ROUARD (Bioversity), m.rouard@cgiar.org

Valentin GUIGNON (Bioversity), v.guignon@cgiar.org

Matthieu CONTE (Syngenta)

=head1 VERSION

Version 1.0.0

Date 25/10/2013

=head1 SEE ALSO

GreenPhyl documentation, DBObject documentation.

=cut

return 1; # package return
