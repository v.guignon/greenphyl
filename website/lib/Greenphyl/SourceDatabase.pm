=pod

=head1 NAME

Greenphyl::SourceDatabase - GreenPhyl DB Object

=head1 SYNOPSIS

    use Greenphyl::SourceDatabase;
    my $source_db = Greenphyl::SourceDatabase->new($dbh, {'selectors' => {'db_name' => 'PubMed'}});
    print $source_db->id();

=head1 REQUIRES

Perl5

=head1 EXPORTS

Nothing

=head1 DESCRIPTION

This module implements a GreenPhyl SourceDatabase database object.

=cut

package Greenphyl::SourceDatabase;

use strict;
use warnings;

use base qw(Greenphyl::CachedDBObject);

use vars qw($AUTOLOAD);

use Carp qw(cluck confess croak);




# Package constants
####################

=pod

=head1 CONSTANTS

B<$OBJECT_PROPERTIES>: (hash ref)

object properties description. See DBObject constructor documentation for
details.

B<$SOURCE_DATABASE_*>: (string)

Common source database names.

=cut

our $OBJECT_PROPERTIES = {
    'table' => 'db',
    'key' => 'id',
    'alternate_keys' => ['name'],
    'load'     => [
        'id',
        'description',
        'name',
        'site_url',
        'query_url_prefix',
    ],
    'alias' => {
    },
    'base' => {
        'id' => {
            'property_column' => 'id',
        },
        'description' => {
            'property_column' => 'description',
        },
        'name' => {
            'property_column' => 'name',
        },
        'site_url' => {
            'property_column' => 'site_url',
        },
        'query_url_prefix' => {
            'property_column' => 'query_url_prefix',
        },
    },
    'secondary' => {
        'dbxref' => {
            'property_table'  => 'dbxref',
            'property_key'    => 'db_id',
            'multiple_values' => 1,
            'property_module' => 'Greenphyl::DBXRef',
        },
    },
    'tertiary' => {
    },
};

our $SOURCE_DATABASE_UNIPROT = 'UNIPROT';
our $SOURCE_DATABASE_PUBMED  = 'PubMed';
our $SOURCE_DATABASE_KEGG    = 'Kegg';
our $SOURCE_DATABASE_EC      = 'EC';
our $SOURCE_DATABASE_GO      = 'GO';
our $SOURCE_DATABASE_IPR     = 'IPR';




# Package subs
###############

=pod

=head1 STATIC METHODS

=head2 CONSTRUCTOR

B<Description>: Creates a new instance of a GreenPhyl Source Database object.

B<ArgsCount>: 2

=over 4

=item $dbh: (DBI::db) (U)

Database handler of the database to load the object from.

=item $parameters: (hash ref) (R)

See Greenphyl::DBObject constructor documentation for details.

=back

B<Return>: (Greenphyl::SourceDatabase)

a new instance.

B<Caller>: General

B<Example>:

    my $source_db = new Greenphyl::SourceDatabase($dbh, {'selectors' => {'name' => 'PubMed'}});

=cut

sub new
{
    my $proto = shift();
    my $class = ref($proto) || $proto;

    my $object_properties = $class->GetDefaultClassProperties();

    return $class->SUPER::new($object_properties, @_);
}




=pod

=head1 AUTHORS

Mathieu ROUARD (Bioversity), m.rouard@cgiar.org

Valentin GUIGNON (Bioversity), v.guignon@cgiar.org

Matthieu CONTE (Syngenta)

=head1 VERSION

Version 1.0.0

Date 17/04/2012

=head1 SEE ALSO

GreenPhyl documentation, DBObject documentation.

=cut

return 1; # package return
