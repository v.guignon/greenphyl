=pod

=head1 NAME

PathManager - Module used to manage path

=head1 SYNOPSIS

my $path_manager = Greenphyl::PathManager->new($species_name);
$path_manager->initUpdateDirectories();

=head1 REQUIRES

Greenphyl::Config

=head1 DESCRIPTION

This module can be used to manage path for update process. It can ensure the
requiered directories are accessible and can warn if they are not empty.

=cut

package Greenphyl::PathManager;

use strict;
use warnings;
use vars qw{$AUTOLOAD $Debug};

use Carp qw (cluck confess croak);

use Greenphyl::Config;



# Package subs
###############

=pod

=head1 STATIC METHODS

=head2 CONSTRUCTOR

B<Description>: Creates a new PathManager instance for the specified species.

B<ArgsCount>: 1

=over 4

=item $species: (string) (R)

Name of the species to work for (required).

=back

B<Return>: (Greenphyl::PathManager)

a new instance.

B<Example>:

    my $path_manager = Greenphyl::PathManager->new('MUSA');

=cut

sub new
{
    my ($proto, $species_name) = @_;
    my $class = ref($proto) || $proto;

    if (!$species_name)
    {
        confess "ERROR: no species name provided!\n";
    }
    
    my $species_data_path = $Greenphyl::Config::DATA_PATH . '/' . $species_name;
    my $self              = {
        'data_directory'         => $Greenphyl::Config::DATA_PATH, # old = data_path
        'output_directory'       => $species_data_path,  # every other files here # old = file_result / racine
        'uniprot_directory'      => $species_data_path . '/uniprot', # old = dir_uniprot
        'interproscan_directory' => $species_data_path . '/iprscan', # old = dir_result
        'blast_directory'        => $species_data_path . '/blast', # old = blast_directory
        'old_blast_directory'    => $species_data_path . '/blast_bak', # old = blast_old
        'species_fasta_directory' => $species_data_path . '/species_fasta',
        'family_fasta_directory' => $species_data_path . '/family_fasta',
        'meme_mast_directory'    => $species_data_path . '/meme',
        'temp_directory'         => $species_data_path . '/tmp', # old = blast_tmp
        'species_name'           => $species_name, # old = species
    };

    bless($self, $class);
    return $self;
}




=pod

=head1 METHODS

=head2 initUpdateDirectories

B<Description>: initialize update directories and make sure they are
accessible and clean.

B<ArgsCount>: 0-1

=over 4

=item $cleaning: (boolean) (O)

if not specified and some directories to use are not empty, the user will be
prompted for cleaning those directories. If specified, the method will not
prompt for cleaning and will clean if set to a true value or will leave
each directory as is if set to a flase value. (optional)

=back

B<Return>: a true value in case of success and false if some
directories were not clean.

B<Exception>:

Raise an exception if a directory can not be created or is read-only.

=cut

sub initUpdateDirectories
{
    my ($self, $cleaning) = @_;
    # check parameters
    if ((2 < @_) || (not ref($self)))
    {
        confess "usage: \$instance->initUpdateDirectories();";
    }

    # create only missing sub-directories
    sub createSubdirs
    {
        my $self = shift;
        # 0776 -> others won't have write access
        if (!-d $self->{'uniprot_directory'})
        {
            mkdir($self->{'uniprot_directory'}, 0776) or confess "ERROR: Failed to create output directory '$self->{'uniprot_directory'}' for '$self->{'species_name'}'!\n$!\n";
        }
        if (!-d $self->{'interproscan_directory'})
        {
            mkdir($self->{'interproscan_directory'}, 0776) or confess "ERROR: Failed to create output directory '$self->{'interproscan_directory'}' for '$self->{'species_name'}'!\n$!\n";
        }
        if (!-d $self->{'blast_directory'})
        {
            mkdir($self->{'blast_directory'}, 0776) or confess "ERROR: Failed to create output directory '$self->{'blast_directory'}' for '$self->{'species_name'}'!\n$!\n";
        }
        if (!-d $self->{'species_fasta_directory'})
        {
            mkdir($self->{'species_fasta_directory'}, 0776) or confess "ERROR: Failed to create output directory '$self->{'species_fasta_directory'}' for '$self->{'species_name'}'!\n$!\n";
        }
        if (!-d $self->{'family_fasta_directory'})
        {
            mkdir($self->{'family_fasta_directory'}, 0776) or confess "ERROR: Failed to create output directory '$self->{'family_fasta_directory'}' for '$self->{'species_name'}'!\n$!\n";
        }
        if (!-d $self->{'meme_mast_directory'})
        {
            mkdir($self->{'meme_mast_directory'}, 0776) or confess "ERROR: Failed to create output directory '$self->{'meme_mast_directory'}' for '$self->{'species_name'}'!\n$!\n";
        }
        if (!-d $self->{'temp_directory'})
        {
            mkdir($self->{'temp_directory'}, 0776) or confess "ERROR: Failed to create output directory '$self->{'temp_directory'}' for '$self->{'species_name'}'!\n$!\n";
        }
    }

    # check if update root path already exists
    if (-e $self->{'output_directory'})
    {
        # it already exists
        # is it a directory?
        if (-d $self->{'output_directory'})
        {
            # check if the directories are writeable
            if (!-w $self->{'output_directory'})
            {
                confess("ERROR: Failed to initialize output directory for '$self->{'species_name'}': '$self->{'output_directory'}' is not writtable!\n");
            }
            # check if directories are empty
            my $dh;
            opendir($dh, $self->{'output_directory'}) or confess "ERROR: failed to initialize directory '$self->{'output_directory'}': unable to check if the directory is not empty!\n";
            my $file_count = 0;
            my $filename = '';
            while ((!$file_count) && ($filename = readdir($dh)))
            {
                # skip "." and ".."
                if ($filename !~ m/^\.\.?$/)
                {++$file_count;}
            }
            closedir($dh);
            if ($file_count)
            {
                # not empty, check which policy to apply
                if (!defined($cleaning))
                {
                    # ask user for cleanning
                    my $user_value = 'x';
                    while ($user_value !~ m/^\s*[ynYN]?\s*$/)
                    {
                        print "The output directory '$self->{'output_directory'}' is not empty!\nRemove all existing files and sub-directories? [y/n] (default: n) ";
                        chomp($user_value = <STDIN>);
                    }
                    if ($user_value =~ m/^\s*[yY]/)
                    {
                        $cleaning = 1;
                    }
                }
                if ($cleaning)
                {
                    # clean directory:
                    # 1) remove directory
                    if (system('rm -rf "' . $self->{'output_directory'} . '"'))
                    {
                        confess "ERROR: Failed to clear directory '$self->{'output_directory'}'!\n$!\n";
                    }
                    # 2) re-create it, 0776 -> others won't have write access
                    mkdir($self->{'output_directory'}, 0776) or confess "ERROR: Failed to re-create output directory '$self->{'output_directory'}' for '$self->{'species_name'}'!\n$!\n";
                }
                createSubdirs($self);
            }
        }
        else
        {
            # it's not a directory
            confess("ERROR: Failed to initialize output directory for '$self->{'species_name'}': '$self->{'output_directory'}' already exists and is not a directory!\n");
        }
    }
    else
    {
        # it does not exist, create the directories, 0776 -> others won't have write access
        mkdir($self->{'output_directory'}, 0776) or confess "ERROR: Failed to create output directory '$self->{'output_directory'}' for '$self->{'species_name'}'!\n$!\n";
        createSubdirs($self);
    }
    return 1;
}




=pod

=head1 AUTOLOAD

=cut

=head2 AUTOLOAD

B<Description>: it is used to handle any setters and getters to access
members. Each path member can be accessed using a get or set method
named using the member name where the first letter and each first letter
after an underscore is "upcased" and all underscores are removed.
For instance, member variable "blast_bank_directory" will be accessed
through getBlastBankDirectory() and setBlastBankDirectory(...).
For setters method, a new value argument must be provided.

B<Example>:

* to get BLAST bank directory:

$path_manager->getBlastBankDirectory();

* to set BLAST bank directory:

$path_manager->setBlastBankDirectory('/new/path/to/baslt/banks');

=cut

sub AUTOLOAD
{
    my $self = shift or return undef;

    # Get the called method name and strip off the fully-qualified part
    (my $method = $AUTOLOAD) =~ s/.*:://;
    return unless $method =~ m/[^A-Z]/;  # skip DESTROY and all-cap methods

    my ($accessor, $member) = ($method =~ m/^(set|get)(.*)/);
    my $member_name = $member; # save member name provided by the caller to report errors
    $member = lcfirst($member);
    $member =~ s/([A-Z])/'_' . lc($1)/eg;

    # check method
    if ($accessor)
    {
        # check if the member exists and can be accessed
        if (exists($self->{$member}))
        {
            if ('set' eq $accessor)
            {
                # set
                $self->{$member} = shift or cluck "WARNING: PathManager: $method called without a new value as argument!\n";
            }
            return $self->{$member};
        }
        else
        {
            confess "ERROR: Member $member_name ($member) not found!\n";
        }
    }
    else
    {
        confess "ERROR: Invalid method called: '$method'!\n";
    }
    return undef;
}


1;
