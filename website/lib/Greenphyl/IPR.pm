=pod

=head1 NAME

Greenphyl::IPR - GreenPhyl IPR Object

=head1 SYNOPSIS

    use Greenphyl::IPR;
    my @iprs = Greenphyl::IPR->new($dbh, {'selectors' => {'code' => ['LIKE', 'IPR0008%']}});
    foreach (@iprs)
    {
        print $_->code . ':' . $_->description . "\n";
    }

=head1 REQUIRES

Perl5

=head1 EXPORTS

Nothing

=head1 DESCRIPTION

This module implements a GreenPhyl IPR database object.

=cut

package Greenphyl::IPR;

use strict;
use warnings;

use base qw(Greenphyl::CachedDBObject);

use vars qw($AUTOLOAD);

use Carp qw(cluck confess croak);




# Package constants
####################

=pod

=head1 CONSTANTS

B<$OBJECT_PROPERTIES>: (hash ref)

object properties description. See DBObject constructor documentation for
details.

=cut

our $OBJECT_PROPERTIES = {
    'table' => 'ipr',
    'key' => 'id',
    'alternate_keys' => ['code'],
    'load'     => [
        'code',
        'id',
        'description',
        'pirsf_code',
        #'pirsf_description',
        'type',
        'version',
    ],
    'alias' => {
        'ipr_code' => 'code',
        'ipr_id'   => 'id',
        'longdesc' => 'description',
        'ipr_longdesc' => 'description',
    },
    'base' => {
        'code' => {
            'property_column' => 'code',
        },
        'id' => {
            'property_column' => 'id',
        },
        'description' => {
            'property_column' => 'description',
        },
        'pirsf_code' => {
            'property_column' => 'pirsf_code',
        },
        'pirsf_description' => {
            'property_column' => 'pirsf_description',
        },
        'type' => {
            'property_column' => 'type',
        },
        'version' => {
            'property_column' => 'version',
        },
    },
    'secondary' => {
        'family_stats' => {
            'property_table'  => 'families_ipr_cache',
            'property_key'    => 'ipr_id',
            'multiple_values' => 1,
            'property_module' => 'Greenphyl::FamilyIPRStats',
        },
    },
    'tertiary' => {
        'sequences' => {
            'link_table'        => 'ipr_sequences',
            'link_object_key'   => 'ipr_id',
            'link_property_key' => 'sequence_id',
            'property_module'   => 'Greenphyl::Sequence',
            'property_table'    => 'sequences',
            'property_key'      => 'id',
            'multiple_values'   => 1,
        },
        'families' => {
            'link_table'        => 'families_ipr_cache',
            'link_object_key'   => 'ipr_id',
            'link_property_key' => 'family_id',
            'property_module'   => 'Greenphyl::Family',
            'property_table'    => 'families',
            'property_key'      => 'id',
            'multiple_values'   => 1,
        },
        'species' => {
            'link_table'        => 'families_ipr_species_cache',
            'link_object_key'   => 'ipr_id',
            'link_property_key' => 'species_id',
            'property_module'   => 'Greenphyl::Species',
            'property_table'    => 'species',
            'property_key'      => 'id',
            'multiple_values'   => 1,
        },
    },

};

our $IPR_REGEXP = 'IPR\d{6}';



# Package subs
###############

=pod

=head1 STATIC METHODS

=head2 CONSTRUCTOR

B<Description>: Creates a new instance of a GreenPhyl IPR object.

B<ArgsCount>: 2

=over 4

=item $dbh: (DBI::db) (R)

Database handler of the database to load the object from.

=item $parameters: (hash ref) (R)

See Greenphyl::DBObject constructor documentation ($parameters item).

=back

B<Return>: (Greenphyl::IPR)

a new instance.

B<Caller>: General

B<Example>:

    my $ipr = new Greenphyl::IPR($dbh, {'selectors' => {'id' => 42}});

=cut

sub new
{
    my $proto = shift();
    my $class = ref($proto) || $proto;
    
    my $object_properties = $class->GetDefaultClassProperties();

    return $class->SUPER::new($object_properties, @_);
}




=pod

=head1 METHODS

=pod

=head2 getStringValue

B<Description>: returns IPR code.

B<ArgsCount>: 1

=over 4

=item $self: (Greenphyl::IPR)

the IPR.

=back

B<Return>: (string)

the IPR code.

=cut

sub getStringValue
{
    my ($self) = @_;
    return $self->{'code'};
}




=pod

=head1 ACCESSORS

=head2 species

B<Description>: getter/setter for species. Wraps default handler to keep only
one species of each kind.

B<ArgsCount>: 1

=over 4

=item $self: (Greenphyl::IPR)

the IPR.

=back

B<Return>: (array ref)

the array of species objects having the IPR.

=cut

sub species
{
    my ($self) = shift();
    # have the family list already been sorted?
    if (!$self->{'species'})
    {
        # no, call parent and make unique and sorted results
        $self->SUPER::species(@_);
        my %species_with_ipr = ();
        foreach (@{$self->{'species'}})
        {
            $species_with_ipr{$_->id} = $_;
        }
        $self->{'species'} = [values(%species_with_ipr)];
    }
    return $self->{'species'};
}




=pod

=head1 AUTHORS

Mathieu ROUARD (Bioversity), m.rouard@cgiar.org

Valentin GUIGNON (Bioversity), v.guignon@cgiar.org

Matthieu CONTE (Syngenta)

=head1 VERSION

Version 1.0.0

Date 17/04/2012

=head1 SEE ALSO

GreenPhyl documentation.

=cut

return 1; # package return
