=pod

=head1 NAME

Greenphyl::GOSequence - GreenPhyl GOSequence Object

=head1 SYNOPSIS

    use Greenphyl::GOSequence;
    my $go_sequence = Greenphyl::GOSequence->new($dbh, {'selectors' => {'go_id' => 42, 'sequence_id' => 806,}});
    if ($go_sequence->ipr)
    {
        print "GO relationship comes from IPR " . $go_sequence->ipr . "\n";
    }

=head1 REQUIRES

Perl5

=head1 EXPORTS

Nothing

=head1 DESCRIPTION

This module implements a GreenPhyl GOSequence database object.

=cut

package Greenphyl::GOSequence;

use strict;
use warnings;

use base qw(Greenphyl::CachedDBObject);

use vars qw($AUTOLOAD);

use Carp qw(cluck confess croak);




# Package constants
####################

=pod

=head1 CONSTANTS

B<$OBJECT_PROPERTIES>: (hash ref)

object properties description. See DBObject constructor documentation for
details.

=cut

our $OBJECT_PROPERTIES = {
    'table' => 'go_sequences_cache',
    'key' => 'go_id,sequence_id',
    'alternate_keys' => [],
    'load'     => [
        'ec_id',
        'go_id',
        'ipr_id',
        'sequence_id',
        'uniprot_id',
    ],
    'alias' => {
    },
    'base' => {
        'ec_id' => {
            'property_column' => 'ec_id',
        },
        'go_id' => {
            'property_column' => 'go_id',
        },
        'ipr_id' => {
            'property_column' => 'ipr_id',
        },
        'sequence_id' => {
            'property_column' => 'sequence_id',
        },
        'uniprot_id' => {
            'property_column' => 'uniprot_id',
        },
    },
    'secondary' => {
        'go' => {
            'object_key'      => 'go_id',
            'property_table'  => 'go',
            'property_key'    => 'id',
            'multiple_values' => 0,
            'property_module' => 'Greenphyl::GO',
        },
        'sequence' => {
            'object_key'      => 'sequence_id',
            'property_table'  => 'sequences',
            'property_key'    => 'id',
            'multiple_values' => 0,
            'property_module' => 'Greenphyl::Sequences',
        },
        'ipr' => {
            'object_key'      => 'ipr_id',
            'property_table'  => 'ipr',
            'property_key'    => 'id',
            'multiple_values' => 0,
            'property_module' => 'Greenphyl::IPR',
        },
        'uniprot' => {
            'object_key'      => 'uniprot_id',
            'property_table'  => 'dbxref',
            'property_key'    => 'id',
            'multiple_values' => 0,
            'property_module' => 'Greenphyl::DBXRef',
        },
        'ec' => {
            'object_key'      => 'ec_id',
            'property_table'  => 'dbxref',
            'property_key'    => 'id',
            'multiple_values' => 0,
            'property_module' => 'Greenphyl::DBXRef',
        },
    },
    'tertiary' => {
    },

};




# Package subs
###############

=pod

=head1 STATIC METHODS

=head2 CONSTRUCTOR

B<Description>: Creates a new instance of a GreenPhyl GOSequence object.

B<ArgsCount>: 2

=over 4

=item $dbh: (DBI::db) (U)

Database handler of the database to load the object from.

=item $parameters: (hash ref) (R)

See Greenphyl::DBObject constructor documentation for details.

=back

B<Return>: (Greenphyl::GOSequence)

a new instance.

B<Caller>: General

B<Example>:

    my $go_sequence = Greenphyl::GOSequence->new($dbh, {'selectors' => {'go_id' => 42, 'sequence_id' => 806,}});

=cut

sub new
{
    my $proto = shift();
    my $class = ref($proto) || $proto;
    
    my $object_properties = $class->GetDefaultClassProperties();

    return $class->SUPER::new($object_properties, @_);
}




=pod

=head1 AUTHORS

Mathieu ROUARD (Bioversity), m.rouard@cgiar.org

Valentin GUIGNON (Bioversity), v.guignon@cgiar.org

Matthieu CONTE (Syngenta)

=head1 VERSION

Version 1.0.0

Date 21/02/2013

=head1 SEE ALSO

GreenPhyl documentation, DBObject documentation.

=cut

return 1; # package return
