=pod

=head1 NAME

Greenphyl::FamilyIPR - GreenPhyl FamilyIPR Object

=head1 SYNOPSIS

    use Greenphyl::FamilyIPR;
    my @family_ipr = Greenphyl::FamilyIPR->new($dbh, {'selectors' => {'family_id' => 205}});
    foreach (@family_ipr)
    {
        print $_->ipr->code . " comes from species " . $_->species->name . "\n";
    }

=head1 REQUIRES

Perl5

=head1 EXPORTS

Nothing

=head1 DESCRIPTION

This module implements a GreenPhyl FamilyIPR database object.

=cut

package Greenphyl::FamilyIPR;

use strict;
use warnings;

use base qw(Greenphyl::CachedDBObject);

use vars qw($AUTOLOAD);

use Carp qw(cluck confess croak);




# Package constants
####################

=pod

=head1 CONSTANTS

B<$OBJECT_PROPERTIES>: (hash ref)

object properties description. See DBObject constructor documentation for
details.

=cut

our $OBJECT_PROPERTIES = {
    'table' => 'families_ipr_species_cache',
    'key' => 'family_id,ipr_id,species_id',
    'alternate_keys' => [],
    'load'     => [
        'family_id',
        'ipr_id',
        'species_id',
    ],
    'alias' => {
    },
    'base' => {
        'family_id' => {
            'property_column' => 'family_id',
        },
        'ipr_id' => {
            'property_column' => 'ipr_id',
        },
        'species_id' => {
            'property_column' => 'species_id',
        },
    },
    'secondary' => {
#        '<member_name>' => {
#            #'object_key'     => '<key_id>', # not defined means use default 'key'
#            'property_table'  => '<>',
#            'property_key'    => '<>',
#            'multiple_values' => 0,
#            'property_column' => '<property_column>',
#        },
#        '<member2_name>' => {
#            #'object_key'     => '<key_id>',
#            'property_table'  => '<>',
#            'property_key'    => '<>',
#            'multiple_values' => 1,
#            'property_module' => 'Greenphyl::<Object>',
#        },
    },
    'tertiary' => {
#        '<member3_name>' => {
#            #'object_key'       => '<key_id>', # not defined means 'key'
#            'link_table'        => '<>',
#            'link_object_key'   => '<>',
#            'link_property_key' => '<>',
#            'property_table'    => '<>',
#            'property_key'      => '<>',
#            'multiple_values'   => 1,
#            'property_module'   => 'Greenphyl::<Object>',
#        },
    },

};




# Package subs
###############

=pod

=head1 STATIC METHODS

=head2 CONSTRUCTOR

B<Description>: Creates a new instance of a GreenPhyl FamilyIPR object.

B<ArgsCount>: 2

=over 4

=item $dbh: (DBI::db) (U)

Database handler of the database to load the object from.

=item $parameters: (hash ref) (R)

See Greenphyl::DBObject constructor documentation for details.

=back

B<Return>: (Greenphyl::FamilyIPR)

a new instance.

B<Caller>: General

B<Example>:

    my @family_ipr = Greenphyl::FamilyIPR->new($dbh, {'selectors' => {'family_id' => 205}});

=cut

sub new
{
    my $proto = shift();
    my $class = ref($proto) || $proto;
    
    my $object_properties = $class->GetDefaultClassProperties();

    return $class->SUPER::new($object_properties, @_);
}




=pod

=head1 AUTHORS

Mathieu ROUARD (Bioversity), m.rouard@cgiar.org

Valentin GUIGNON (Bioversity), v.guignon@cgiar.org

Matthieu CONTE (Syngenta)

=head1 VERSION

Version 1.0.0

Date 17/04/2012

=head1 SEE ALSO

GreenPhyl documentation, DBObject documentation.

=cut

return 1; # package return
