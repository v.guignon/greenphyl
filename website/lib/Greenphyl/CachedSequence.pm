=pod

=head1 NAME

Greenphyl::CachedSequence - GreenPhyl Cached Sequence Object

=head1 SYNOPSIS

    use Greenphyl::CachedSequence;
    my $sequence = Greenphyl::CachedSequence->new($dbh, {'selectors' => {'accession' => ['LIKE', 'Os01g01050.1']}});
    print $sequence->name();

=head1 REQUIRES

Perl 5.8.0, Greenphyl::Sequence

=head1 EXPORTS

nothing

=head1 DESCRIPTION

This module implements a GreenPhyl cached sequence database object.

=cut

package Greenphyl::CachedSequence;

use strict;
use warnings;

use base qw(Greenphyl::Sequence);

use vars qw($AUTOLOAD);

use Carp qw(cluck confess croak);

use Clone qw(clone);

use Greenphyl;
use Greenphyl::Sequence;




# Package constants
####################

=pod

=head1 CONSTANTS

B<$OBJECT_PROPERTIES>: (hash ref)

sequence object properties description. See DBObject constructor documentation
for details.

=cut

our $DEBUG = 0;

our $OBJECT_PROPERTIES;

INIT {

    $OBJECT_PROPERTIES = clone($Greenphyl::Sequence::OBJECT_PROPERTIES);

    # Init differences.
    $OBJECT_PROPERTIES->{'table'} = 'sequences_cache';
    $OBJECT_PROPERTIES->{'load'} = [
        'annotation',
        'accession',
        'filtered',
        'genome_id',
        'id',
        'length',
        'locus',
        'representative',
        'splice',
        'splice_priority',
        # 'polypeptide',
        'species_id',
        'type',
        # Cached fields
        'bbmh_cache',
        'homologs_cache',
        'ipr_cache',
        'level_1_family_id',
        'level_2_family_id',
        'level_3_family_id',
        'level_4_family_id',
        'pangene_family_id',
    ];
    # Cached data.
    $OBJECT_PROPERTIES->{'base'}->{'bbmh_cache'} = {
        'property_column' => 'bbmh_cache',
        'serialized'      => 1,
    };
    $OBJECT_PROPERTIES->{'base'}->{'homologs_cache'} = {
        'property_column' => 'homologs_cache',
        'serialized'      => 1,
    };
    $OBJECT_PROPERTIES->{'base'}->{'ipr_cache'} = {
        'property_column' => 'ipr_cache',
        'serialized'      => 1,
    };
    # Shortcuts.
    $OBJECT_PROPERTIES->{'base'}->{'level_1_family_id'} = {
        'property_column' => 'level_1_family_id',
    };
    $OBJECT_PROPERTIES->{'base'}->{'level_2_family_id'} = {
        'property_column' => 'level_2_family_id',
    };
    $OBJECT_PROPERTIES->{'base'}->{'level_3_family_id'} = {
        'property_column' => 'level_3_family_id',
    };
    $OBJECT_PROPERTIES->{'base'}->{'level_4_family_id'} = {
        'property_column' => 'level_4_family_id',
    };
    $OBJECT_PROPERTIES->{'base'}->{'pangene_family_id'} = {
        'property_column' => 'pangene_family_id',
    };
    # Secondary properties.
    $OBJECT_PROPERTIES->{'secondary'}->{'level_1_family'} = {
        'object_key'      => 'level_1_family_id',
        'property_table'  => 'families',
        'property_key'    => 'id',
        'multiple_values' => 0,
        'property_module' => 'Greenphyl::Family',
    };
    $OBJECT_PROPERTIES->{'secondary'}->{'level_2_family'} = {
        'object_key'      => 'level_2_family_id',
        'property_table'  => 'families',
        'property_key'    => 'id',
        'multiple_values' => 0,
        'property_module' => 'Greenphyl::Family',
    };
    $OBJECT_PROPERTIES->{'secondary'}->{'level_3_family'} = {
        'object_key'      => 'level_3_family_id',
        'property_table'  => 'families',
        'property_key'    => 'id',
        'multiple_values' => 0,
        'property_module' => 'Greenphyl::Family',
    };
    $OBJECT_PROPERTIES->{'secondary'}->{'level_4_family'} = {
        'object_key'      => 'level_4_family_id',
        'property_table'  => 'families',
        'property_key'    => 'id',
        'multiple_values' => 0,
        'property_module' => 'Greenphyl::Family',
    };
    $OBJECT_PROPERTIES->{'secondary'}->{'pangene_family'} = {
        'object_key'      => 'pangene_family_id',
        'property_table'  => 'families',
        'property_key'    => 'id',
        'multiple_values' => 0,
        'property_module' => 'Greenphyl::PangeneFamily',
    };

    $OBJECT_PROPERTIES->{'secondary'}->{'bbmh'}->{'cached'} = 'bbmh_cache:bbmh';
    $OBJECT_PROPERTIES->{'secondary'}->{'ipr_details'}->{'cached'} = 'ipr_cache:ipr_details';
    $OBJECT_PROPERTIES->{'tertiary'}->{'ipr'}->{'cached'} = 'ipr_cache:ipr';
    $OBJECT_PROPERTIES->{'tertiary'}->{'homologies'}->{'cached'} = 'homologs_cache:homologies';
    $OBJECT_PROPERTIES->{'tertiary'}->{'homologs'}->{'cached'} = 'homologs_cache:homologs';
    $OBJECT_PROPERTIES->{'tertiary'}->{'bbmh_sequences'}->{'cached'} = 'bbmh_cache:bbmh_sequences';
}

our $SUPPORTED_DUMP_FORMAT = $Greenphyl::Sequence::SUPPORTED_DUMP_FORMAT;




# Package subs
###############

=pod

=head1 STATIC METHODS

=head2 CONSTRUCTOR

B<Description>: Creates a new instance of a GreenPhyl sequence object.

B<ArgsCount>: 1-2

=over 4

=item $fasta_content: (DBI::db) (O)

A FASTA content to load the sequences from.

=item $dbh: (DBI) (U)

Database handler of the database to load the object from.

=item $parameters: (hash ref) (R)

See Greenphyl::DBObject constructor documentation for details.

=back

B<Return>: (Greenphyl::CachedSequence)

a new instance.

B<Caller>: General

B<Example>:

    my $sequence = new Greenphyl::CachedSequence($dbh, {'selectors' => {'accession' => ['LIKE', 'Os01g01050%']}});
    my $fasta_sequence = new Greenphyl::CachedSequence(">Os01g01050.1_ORYSA\nMINFAVQSRCAPTNSLCSCVALCERNFKLEQVEELASLIKDNLYSKHLVLSTEETLVGIL");

    # load from synonym
    $sequence = Greenphyl::CachedSequence->new(
        GetDatabaseHandler(),
        {
            'load'      => { 'synonyms' => 1,},
            'selectors' => {'sequence_synonyms.synonym' => 'GSMUA_Achr10T23190_001',},
        },
    );

    # load from both synonyms and accessions
    use Greenphyl::Tools::Sequences;
    
    ...
    @sequence_accessions = @{GetAccessionsFromSynonyms(\@sequence_accessions, \@sequence_accessions)};

    $sequence = Greenphyl::CachedSequence->new(
        GetDatabaseHandler(),
        {
            'selectors' => {'accession' => ['IN', @sequence_accessions],},
            'sql' => {'DISTINCT' => 1,}
        },
    );

=cut

sub new
{
    my ($proto) = shift();
    my $class = ref($proto) || $proto;

    return $class->SUPER::new(@_);
}






=pod

=head1 METHODS

=head2 families

B<Description>: returns an array of families corresponding to GreenPhyl
clustering which have a level <> 0. Other family types are excluded.
Works just like "->families()".

B<Aliases>: gpfamilies

B<ArgsCount>: 1-...

=over 4

=item $self: (Greenphyl::Sequence) (R)

Current sequence.

=back

B<Return>: (array ref)

array of Greenphyl::Family or an empty array if no GreenPhyl family.

=cut

sub families
{
    my ($self, $selectors, @other_param) = @_;
    my $families;
    
    if (defined($selectors))
    {
        if (ref($selectors) eq 'HASH')
        {
            if (1 == scalar(keys(%{$selectors->{'selectors'}}))
                && exists($selectors->{'selectors'}->{'level'})
                && !ref($selectors->{'selectors'}->{'level'}))
            {
                if (1 == $selectors->{'selectors'}->{'level'})
                {
                    $families = [$self->level_1_family];
                }
                elsif (2 == $selectors->{'selectors'}->{'level'})
                {
                    $families = [$self->level_2_family];
                }
                elsif (3 == $selectors->{'selectors'}->{'level'})
                {
                    $families = [$self->level_3_family];
                }
                elsif (4 == $selectors->{'selectors'}->{'level'})
                {
                    $families = [$self->level_4_family];
                }
                elsif (!defined($selectors->{'selectors'}->{'level'}))
                {
                    $families = [$self->pangene_family];
                }
                else
                {
                    $families = [];
                }
            }
            elsif (!exists($selectors->{'selectors'}->{'id'}))
            {
                $selectors->{'selectors'}->{'id'} = [
                    'IN',
                    $self->level_1_family_id,
                    $self->level_2_family_id,
                    $self->level_3_family_id,
                    $self->level_4_family_id,
                    $self->pangene_family_id,
                ];
            }
        }
    }
    else
    {
        $selectors = {
            'selectors' => {
                'id' => [
                    'IN',
                    $self->level_1_family_id,
                    $self->level_2_family_id,
                    $self->level_3_family_id,
                    $self->level_4_family_id,
                    $self->pangene_family_id,
                ]
            }
        };
    }

    if (!$families)
    {
        $families = $self->SUPER::families($selectors, @other_param);
    }
    return $families;
}
# aliases
sub getFamilies; *getFamilies = \&families;
sub fetchFamilies; *fetchFamilies = \&families;
sub Families;    *Families    = \&families;


=pod

=head2 getIPRGO

B<Description>: returns GO terms (objects) linked to sequence through IPR.

B<Aliases>: ipr_go

B<ArgsCount>: 1

=over 4

=item $self: (Greenphyl::Family) (R)

Current family.

=back

B<Return>: (array ref)

Returns an array of Greenphyl::GO objects.

B<Example>:

    foreach my $ipr_go (@{$family->getIPRGO()})
    {
        ...
    }

=cut

sub getIPRGO
{
    my ($self) = @_;

    if (!exists($self->{'ipr_go'}))
    {
        my $ipr_ids = [map {$_->id} @{$self->ipr}];
        if (@$ipr_ids && $ipr_ids->[0])
        {
            my $ipr_go_sql_query =  '(
                SELECT DISTINCT gi.go_id
                FROM go_ipr gi
                WHERE
                    gi.ipr_id IN (?' .(',?' x (@$ipr_ids - 1)) . ')
            )';
            my $go_ids = $self->{'_dbh'}->selectcol_arrayref($ipr_go_sql_query, undef, @$ipr_ids)
                or confess "ERROR: in query:\n$ipr_go_sql_query\nWith bindings: " . join(', ', @$ipr_ids) . "\n" . ($self->{'_dbh'}->errstr || '');

            $self->{'ipr_go'} = [
                # returns a list of GO objects
                Greenphyl::GO->new(
                    GetDatabaseHandler(),
                    {
                        'selectors' => {'id' => ['IN', @$go_ids]},
                    },
                ),
            ];
        }
        else
        {
          $self->{'ipr_go'} = [];
        }
    }

    return $self->{'ipr_go'};
}
# Aliases
sub ipr_go; *ipr_go = \&getIPRGO;


=pod

=head2 initHomology

B<Description>: initialize current sequence homology hash.

B<ArgsCount>: 1

=over 4

=item $self: (Greenphyl::CachedSequence) (R)

Current sequence.

=back

B<Return>: (integer)

The number of homologs.

=cut

sub initHomology
{
    my ($self) = @_;

    if (!$self->getDBKeyValue())
    {return 0;}
    
    # init homology hash
    if (!defined($self->{'_homologies'}))
    {
        $self->{'_homologies'} = {};
        foreach my $homology (@{$self->homologies()})
        {
            my $homolog_id = $self->homologs_cache->{'homolgy_relationships'}->{'homologies_to_homologs'}->{$homology->id};
            if (!$homolog_id)
            {
                PrintDebug("Incomplete cached sequence: " . $self->id . " (" . $self->accession . ")\n");
            }
            else
            {
                $self->{'_homologies'}->{$homolog_id} = $homology;
            }
        }
    }
    return scalar(keys(%{$self->{'_homologies'}}));
}


=pod

=head2 getPangeneFamily

B<Description>: returns the pangene family of current sequence.

B<ArgsCount>: 1

=over 4

=item $self: (Greenphyl::Sequence) (R)

Current sequence.

=back

B<Return>: (Greenphyl::PangeneFamily)

A pangene family object.

=cut

sub getPangeneFamily
{
    my ($self) = @_;

    return $self->pangene_family;
}




=pod

=head1 AUTHORS

Mathieu ROUARD (Bioversity), m.rouard@cgiar.org

Valentin GUIGNON (Bioversity), v.guignon@cgiar.org

=head1 VERSION

Version 1.0.0

Date 13/05/2020

=head1 SEE ALSO

GreenPhyl documentation.

=cut

return 1; # package return
