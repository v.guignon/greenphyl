=pod

=head1 NAME

Greenphyl::User - GreenPhyl User Object

=head1 SYNOPSIS

    use Greenphyl::User;
    my $user = Greenphyl::User->new($dbh, {'selectors' => {'login' => 'admin'}});
    print $user->display_name();

=head1 REQUIRES

Perl5

=head1 EXPORTS

Nothing

=head1 DESCRIPTION

This module implements a GreenPhyl User database object.

=cut

package Greenphyl::User;

use strict;
use warnings;

use base qw(Greenphyl::CachedDBObject);

use vars qw($AUTOLOAD);

use Carp qw(cluck confess croak);

use Greenphyl;


# Package constants
####################

=pod

=head1 CONSTANTS

B<$OBJECT_PROPERTIES>: (hash ref)

object properties description. See DBObject constructor documentation for
details.

B<$USER_FLAG_ADMINISTRATOR>: (string)
Administrator flag.

B<$USER_FLAG_ANNOTATOR>: (string)
Annotator flag.

B<$USER_FLAG_REGISTERED>: (string)
Registered user flag.

B<$USER_FLAG_DISABLED>: (string)
Disabled account flag.

=cut

our $DEBUG = 0;

our $OBJECT_PROPERTIES = {
    'table' => 'users',
    'key' => 'id',
    'alternate_keys' => ['login'],
    'load'     => [
        # 'description',
        'display_name',
        'email',
        'flags',
        'id',
        'login',
        # 'password_hash',
        # 'salt',
    ],
    'alias' => {
    },
    'base' => {
        'description' => {
            'property_column' => 'description',
        },
        'email' => {
            'property_column' => 'email',
        },
        'flags' => {
            'property_column' => 'flags',
        },
        'id' => {
            'property_column' => 'id',
        },
        'login' => {
            'property_column' => 'login',
        },
        'display_name' => {
            'property_column' => 'display_name',
        },
        'password_hash' => {
            'property_column' => 'password_hash',
        },
        'salt' => {
            'property_column' => 'salt',
        },
    },
    'secondary' => {
        'annotations' => {
            'property_table'  => 'family_annotations',
            'property_key'    => 'user_id',
            'multiple_values' => 1,
            'property_module' => 'Greenphyl::FamilyAnnotation',
        },
        'custom_families' => {
            'property_table'  => 'custom_families',
            'property_key'    => 'user_id',
            'multiple_values' => 1,
            'property_module' => 'Greenphyl::CustomFamily',
        },
        'openids' => {
            'property_table'  => 'user_openids',
            'property_key'    => 'user_id',
            'multiple_values' => 1,
            'property_module' => 'Greenphyl::UserOpenID',
        },
        'files' => {
            'property_table'  => 'files',
            'property_key'    => 'user_id',
            'multiple_values' => 1,
            'property_module' => 'Greenphyl::File',
        },
        'quota' => {
            'property_table'  => 'files',
            'property_key'    => 'user_id',
            'multiple_values' => 0,
            'property_column' => 'SUM(file.size)',
        },
    },
    'tertiary' => {
        'groups' => {
            'link_table'        => 'groups_users',
            'link_object_key'   => 'user_id',
            'link_property_key' => 'group_id',
            'property_table'    => 'groups',
            'property_key'      => 'id',
            'multiple_values'   => 1,
            'property_module'   => 'Greenphyl::Group',
        },
    },

};

our $USER_FLAG_ADMINISTRATOR = 'administrator';
our $USER_FLAG_ANNOTATOR     = 'annotator';
our $USER_FLAG_REGISTERED    = 'registered user';
our $USER_FLAG_DISABLED      = 'disabled';




# Package subs
###############

=pod

=head1 STATIC METHODS

=head2 CONSTRUCTOR

B<Description>: Creates a new instance of a GreenPhyl User object.

B<ArgsCount>: 2

=over 4

=item $dbh: (DBI::db) (U)

Database handler of the database to load the object from.

=item $parameters: (hash ref) (R)

See Greenphyl::DBObject constructor documentation for details.

=back

B<Return>: (Greenphyl::User)

a new instance.

B<Caller>: General

B<Example>:

    my $user = new Greenphyl::User($dbh, {'selectors' => {'login' => 'admin'}});

=cut

sub new
{
    my $proto = shift();
    my $class = ref($proto) || $proto;
    
    my $object_properties = $class->GetDefaultClassProperties();

    return $class->SUPER::new($object_properties, @_);
}


=pod

=head1 METHODS

=head2 getStringValue

B<Description>: returns user login.

B<ArgsCount>: 1

=over 4

=item $self: (Greenphyl::User)

the user.

=back

B<Return>: (string)

the user login.

B<Caller>: internal

=cut

sub getStringValue
{
    my ($self) = @_;
    return $self->{'login'};
}


=pod

=head2 password

B<Description>: Set user password.

B<ArgsCount>: 1-2

=over 4

=item $self: (Greenphyl::User) (R)

Current user.

=item $password: (string) (O)

The new password.

=back

B<Return>: (string)

User password hash.

=cut

sub password
{
    my ($self, $password) = @_;

    if (defined($password) && ($password ne ''))
    {
        my ($password_hash, $salt) = HashPassword($password);
        $self->password_hash($password_hash);
        $self->salt($salt);
    }

    return $self->password_hash;
}


=pod

=head2 hasAccess

B<Description>: Tells if the user has a given access level.

B<ArgsCount>: 2

=over 4

=item $self: (Greenphyl::User) (R)

the given user object.

=item $access_level: (string) (R)

The access level to check. See $Greenphyl::Config::ACCESS_KEY_* constants for
allowed values.

=back

B<Return>: (boolean)

True if the user has the given access level.

B<Example>:

    if ($user->hasAccess(GP('ACCESS_KEY_ANNOTATOR')))
    {
        # user is an annotator
        ...
    }

=cut

sub hasAccess
{
    my ($self, $access_level) = @_;
    if (!defined($self->{'access_flag'}))
    {
        $self->{'access_flag'} = { map {$_ => 1} split(/\s*,\s*/, $self->flags) };
    }
    return $self->{'access_flag'}->{$access_level};
}




# CODE START
#############

# create the default global anonymous user
our $ANONYMOUS = new Greenphyl::User(
    undef,
    {
        'load' => 'none',
        'members' =>
        {
            'id'            => 0,
            'login'         => 'anonymous',
            'display_name'  => 'Anonymous',
            'flags'         => '',
            'email'         => '',
            'password_hash' => '',
            'salt'          => '',
            'description'   => '',
        },
    }
);

# CODE END
###########


=pod

=head1 AUTHORS

Mathieu ROUARD (Bioversity), m.rouard@cgiar.org

Valentin GUIGNON (Bioversity), v.guignon@cgiar.org

Matthieu CONTE (Syngenta)

=head1 VERSION

Version 1.0.0

Date 17/04/2012

=head1 SEE ALSO

GreenPhyl documentation, DBObject documentation.

=cut

return 1; # package return
