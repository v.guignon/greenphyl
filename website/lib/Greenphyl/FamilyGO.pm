=pod

=head1 NAME

Greenphyl::FamilyGO - GreenPhyl FamilyGO Object

=head1 SYNOPSIS

    use Greenphyl::FamilyGO;
    my $family_go = Greenphyl::FamilyGO->new($dbh, {'selectors' => {'go_id' => 42, 'family_id' => 806,}});
    print "Percent of sequences related to the GO in family: " . $family_go->percent;

=head1 REQUIRES

Perl5

=head1 EXPORTS

Nothing

=head1 DESCRIPTION

This module implements a GreenPhyl FamilyGO database object.

=cut

package Greenphyl::FamilyGO;

use strict;
use warnings;

use base qw(Greenphyl::CachedDBObject);

use vars qw($AUTOLOAD);

use Carp qw(cluck confess croak);




# Package constants
####################

=pod

=head1 CONSTANTS

B<$OBJECT_PROPERTIES>: (hash ref)

object properties description. See DBObject constructor documentation for
details.

=cut

our $OBJECT_PROPERTIES = {
    'table' => 'families_go_cache',
    'key' => 'go_id,family_id',
    'alternate_keys' => [],
    'load'     => [
        'family_id',
        'go_id',
        'percent',
    ],
    'alias' => {
    },
    'base' => {
        'family_id' => {
            'property_column' => 'family_id',
        },
        'go_id' => {
            'property_column' => 'go_id',
        },
        'percent' => {
            'property_column' => 'percent',
        },
    },
    'secondary' => {
        'family' => {
            'object_key'      => 'family_id',
            'property_table'  => 'families',
            'property_key'    => 'id',
            'multiple_values' => 1,
            'property_module' => 'Greenphyl::Family',
        },
        'go' => {
            'object_key'      => 'go_id',
            'property_table'  => 'go',
            'property_key'    => 'id',
            'multiple_values' => 1,
            'property_module' => 'Greenphyl::GO',
        },
    },
    'tertiary' => {
    },

};




# Package subs
###############

=pod

=head1 STATIC METHODS

=head2 CONSTRUCTOR

B<Description>: Creates a new instance of a GreenPhyl FamilyGO object.

B<ArgsCount>: 2

=over 4

=item $dbh: (DBI::db) (U)

Database handler of the database to load the object from.

=item $parameters: (hash ref) (R)

See Greenphyl::DBObject constructor documentation for details.

=back

B<Return>: (Greenphyl::FamilyGO)

a new instance.

B<Caller>: General

B<Example>:

    my $family_go = Greenphyl::FamilyGO->new($dbh, {'selectors' => {'go_id' => 42, 'family_id' => 806,}});

=cut

sub new
{
    my $proto = shift();
    my $class = ref($proto) || $proto;
    
    my $object_properties = $class->GetDefaultClassProperties();

    return $class->SUPER::new($object_properties, @_);
}




=pod

=head1 AUTHORS

Mathieu ROUARD (Bioversity), m.rouard@cgiar.org

Valentin GUIGNON (Bioversity), v.guignon@cgiar.org

Matthieu CONTE (Syngenta)

=head1 VERSION

Version 1.0.0

Date 21/02/2013

=head1 SEE ALSO

GreenPhyl documentation, DBObject documentation.

=cut

return 1; # package return
