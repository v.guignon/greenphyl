=pod

=head1 NAME

Greenphyl::Family - GreenPhyl Family Object

=head1 SYNOPSIS

    use Greenphyl::Family;
    my $family = Greenphyl::Family->new($dbh, {'selectors' => {'id' => 205}});
    print $family->name();

=head1 REQUIRES

Perl5

=head1 EXPORTS

Nothing

=head1 DESCRIPTION

This module implements a GreenPhyl family database object.

=cut

package Greenphyl::Family;

use strict;
use warnings;

use base qw(Greenphyl::AbstractFamily);

use vars qw($AUTOLOAD);

use Carp qw(cluck confess croak);

use Greenphyl::Config;
use Greenphyl;
use Greenphyl::SourceDatabase;
use Greenphyl::Sequence;
use Greenphyl::GO;
use Greenphyl::FamilyIPRStats;
use Greenphyl::SequenceIPR;

use Greenphyl::Tools::Families;
use Greenphyl::Tools::Sequences;
use Greenphyl::Tools::GO;



# Package constants
####################

=pod

=head1 CONSTANTS

B<$OBJECT_PROPERTIES>: (hash ref)

sequence object properties description. See DBObject constructor documentation
for details.

=cut

our $DEBUG = 0;
our $OBJECT_PROPERTIES =
    {
        'table' => 'families',
        'relationship' => {
            'table' => 'family_relationships_cache',
            'subject' => 'subject_family_id',
            'object' => 'object_family_id',
        },
        'key' => 'id',
        'alternate_keys' => ['accession',],
        'load'     => [
            'accession',
            # 'alignment_length',
            # 'alignment_sequence_count',
            # 'average_sequence_length',
            'black_listed',
            # 'branch_average',
            # 'branch_median',
            # 'branch_standard_deviation',
            'description',
            'id',
            'name',
            # 'inferences',
            'level',
            # 'masked_alignment_length',
            # 'masked_alignment_sequence_count',
            # 'max_sequence_length',
            # 'median_sequence_length',
            # 'min_sequence_length',
            'plant_specific',
            'sequence_count',
            # 'taxonomy_id',
            # 'type',
            'validated',
        ],
        'alias' => {
            'alignment_seq_count' => 'alignment_sequence_count',
            'average_seq_length' => 'average_sequence_length',
            'black_list' => 'black_listed',
            'classification' => 'level',
            'family_has_an' => 'ipr_species',
            'family_id' => 'id',
            'max_seq_length' => 'max_sequence_length',
            'median_seq_length' => 'median_sequence_length',
            'min_seq_length' => 'min_sequence_length',
            'family_name' => 'name',
            'family_accession' => 'accession',
            'plant_spe' => 'plant_specific',
            'pubmed' => 'pmid',
            'seq_count'  => 'sequence_count',
            'synonym' => 'synonyms',
            'phylonode' => 'taxonomy',
            'count_family_seq' => 'sequence_count_by_families_and_species',
            'sequence_count_by_families_species' => 'sequence_count_by_families_and_species',
            'sequence_count_without_splice' => 'sequence_count_without_splice_by_families_and_species_sum',
        },
        'base' => {
            'alignment_length' => {
                'property_column' => 'alignment_length',
            },
            'alignment_sequence_count' => {
                'property_column' => 'alignment_sequence_count',
            },
            'average_sequence_length' => {
                'property_column' => 'average_sequence_length',
            },
            'black_listed' => {
                'property_column' => 'black_listed',
            },
            'branch_average' => {
                'property_column' => 'branch_average',
            },
            'branch_median' => {
                'property_column' => 'branch_median',
            },
            'branch_standard_deviation' => {
                'property_column' => 'branch_standard_deviation',
            },
            'description' => {
                'property_column' => 'description',
            },
            'id' => {
                'property_column' => 'id',
            },
            'level' => {
                'property_column' => 'level',
            },
            'name' => {
                'property_column' => 'name',
            },
            'accession' => {
                'property_column' => 'accession',
            },
            'inferences' => {
                'property_column' => 'inferences',
            },
            'masked_alignment_length' => {
                'property_column' => 'masked_alignment_length',
            },
            'masked_alignment_sequence_count' => {
                'property_column' => 'masked_alignment_sequence_count',
            },
            'max_sequence_length' => {
                'property_column' => 'max_sequence_length',
            },
            'median_sequence_length' => {
                'property_column' => 'median_sequence_length',
            },
            'min_sequence_length' => {
                'property_column' => 'min_sequence_length',
            },
            'plant_specific' => {
                'property_column' => 'plant_specific',
            },
            'sequence_count' => {
                'property_column' => 'sequence_count',
            },
            'taxonomy_id' => {
                'property_column' => 'taxonomy_id',
            },
            'type' => {
                'property_column' => 'type',
            },
            'validated' => {
                'property_column' => 'validated',
            },
            'validated_value' => {
                'property_column' => '0+validated',
            },
        },
        'secondary' => {
            'annotations' => {
                'object_key'      => 'accession',
                'property_table'  => 'family_annotations',
                'property_key'    => 'accession',
                'multiple_values' => 1,
                'property_module' => 'Greenphyl::FamilyAnnotation',
            },
            'sequence_count_by_families_and_species' => {
                'property_table'  => 'sequence_count_by_families_species_cache',
                'property_key'    => 'family_id',
                'multiple_values' => 1,
                'property_module' => 'Greenphyl::SequenceCount',
            },
            'sequence_count_by_families_and_species_sum' => {
                'property_table'  => 'sequence_count_by_families_species_cache',
                'property_key'    => 'family_id',
                'multiple_values' => 0,
                'property_column' => 'SUM(sequence_count)',
            },
            'sequence_count_without_splice_by_families_and_species_sum' => {
                'property_table'  => 'sequence_count_by_families_species_cache',
                'property_key'    => 'family_id',
                'multiple_values' => 0,
                'property_column' => 'SUM(sequence_count_without_splice)',
            },
            'taxonomy' => {
                'object_key'      => 'taxonomy_id',
                'property_table'  => 'taxonomy',
                'property_key'    => 'id',
                'multiple_values' => 0,
                'property_module' => 'Greenphyl::Taxonomy',
            },
            'homologies' => {
                'property_table'    => 'homologies',
                'property_key'      => 'family_id',
                'multiple_values'   => 1,
                'property_module'   => 'Greenphyl::Homology',
            },
            'ipr_species' => {
                'property_table'  => 'families_ipr_species_cache',
                'property_key'    => 'family_id',
                'multiple_values' => 1,
                'property_module' => 'Greenphyl::FamilyIPR',
            },
            'ipr_stats' => {
                'property_table'  => 'families_ipr_cache',
                'property_key'    => 'family_id',
                'multiple_values' => 1,
                'property_module' => 'Greenphyl::FamilyIPRStats',
            },
            'synonyms' => {
                'property_table'  => 'family_synonyms',
                'property_key'    => 'family_id',
                'multiple_values' => 1,
                'property_column' => 'synonym',
            },
            'processing' => {
                'property_table'  => 'family_processing',
                'property_key'    => 'family_id',
                'multiple_values' => 1,
                'property_module' => 'Greenphyl::FamilyProcessing',
            },
        },
        'tertiary' => {
            'dbxref' => {
                'link_table'        => 'dbxref_families',
                'link_object_key'   => 'family_id',
                'link_property_key' => 'dbxref_id',
                'multiple_values'   => 1,
                'property_module'   => 'Greenphyl::DBXRef',
                'property_table'    => 'dbxref',
                'property_key'      => 'id',
            },
            'go' => {
                'link_table'        => 'families_go_cache',
                'link_object_key'   => 'family_id',
                'link_property_key' => 'go_id',
                'multiple_values'   => 1,
                'property_module'   => 'Greenphyl::GO',
                'property_table'    => 'go',
                'property_key'      => 'id',
            },
            'ipr' => {
                'link_table'        => 'families_ipr_species_cache',
                'link_object_key'   => 'family_id',
                'link_property_key' => 'ipr_id',
                'multiple_values'   => 1,
                'property_module'   => 'Greenphyl::IPR',
                'property_table'    => 'ipr',
                'property_key'      => 'id',
            },
            'sequences' => {
                'link_table'        => 'families_sequences',
                'link_object_key'   => 'family_id',
                'link_property_key' => 'sequence_id',
                'multiple_values'   => 1,
                'property_module'   => 'Greenphyl::Sequence',
                'property_table'    => 'sequences',
                'property_key'      => 'id',
            },
            'species' => {
                'link_table'        => 'sequence_count_by_families_species_cache',
                'link_object_key'   => 'family_id',
                'link_property_key' => 'species_id',
                'multiple_values'   => 1,
                'property_module'   => 'Greenphyl::Species',
                'property_table'    => 'species',
                'property_key'      => 'id',
            },
            'genomes' => {
                'link_table'        => 'sequence_count_by_families_genomes_cache',
                'link_object_key'   => 'family_id',
                'link_property_key' => 'genome_id',
                'multiple_values'   => 1,
                'property_module'   => 'Greenphyl::Genome',
                'property_table'    => 'genomes',
                'property_key'      => 'id',
            },
            'processes' => {
                'link_table'        => 'family_processing',
                'link_object_key'   => 'family_id',
                'link_property_key' => 'process_id',
                'multiple_values'   => 1,
                'property_module'   => 'Greenphyl::AnalysisProcess',
                'property_table'    => 'processes',
                'property_key'      => 'id',
            },
        },
    };

our $SUPPORTED_DUMP_FORMAT = $Greenphyl::AbstractFamily::SUPPORTED_DUMP_FORMAT;



# Package subs
###############

=pod

=head1 STATIC METHODS

=head2 CONSTRUCTOR

B<Description>: Creates a new instance of a GreenPhyl family object.

B<ArgsCount>: 2

=over 4

=item $dbh: (DBI::db) (U)

Database handler of the database to load the object from.

=item $parameters: (hash ref) (R)

See Greenphyl::DBObject constructor documentation for details.

=back

B<Return>: (Greenphyl::Family)

a new instance.

B<Caller>: General

B<Example>:

    my $family = new Greenphyl::Family($dbh, {'selectors' => {'id' => 205, 'validated' => ['<>', 0]}});

=cut

sub new
{
    my ($proto) = shift();
    my $class = ref($proto) || $proto;

    return $class->SUPER::new(@_);
}




=pod

=head1 METHODS

=head2 hasAnalyzes

B<Description>: returns true if the family has been processed by the pipeline.
The string value returned correspond to the last analysis level run:
* 'alignment':  only up to the alignment;
* 'filtration': only up to the masking/filtration;
* 'phylogeny':  only up to the phylogeny;
* 'rooting':    only up to the tree rooting;
* 'orthology':  only up to the orthology inference.

B<Alias>: hasAnalyses

B<ArgsCount>: 1

=over 4

=item $self: (Greenphyl::Family) (R)

Current family.

=back

B<Return>: (string)

the last analysis level run (see string values in method description).
An empty string if no analysis is available.

B<Example>:

    if ($family->hasAnalyzes() eq 'orthology')
    {
        # orthology prediction available
        ...
    }

=cut

sub hasAnalyzes
{
    my ($self) = @_;

    my $ALIGNMENT_LEVEL  = 'alignment';
    my $FILTRATION_LEVEL = 'filtration';
    my $PHYLOGENY_LEVEL  = 'phylogeny';
    my $ROOTING_LEVEL    = 'rooting';
    my $ORTHOLOGY_LEVEL  = 'orthology';


    # check if we're running on web server or from the command line (pipeline)
    if (IsRunningCGI())
    {
        eval "use Greenphyl::Web;";
        if ($@)
        {
            confess $@;
        }
        # web
        if (GetPhylogenyAnalyzesURL($self))
        {
            return $ORTHOLOGY_LEVEL;
        }
        elsif (GetAlignmentURL($self, 'filtered'))
        {
            return $FILTRATION_LEVEL;
        }
        elsif (GetAlignmentURL($self))
        {
            return $ALIGNMENT_LEVEL;
        }
        else
        {
            return '';
        }
    }
    else
    {
        # pipeline
        # these constants come from run_pipeline.pl
        our $NEWICK_FILE_EXTENSION                = '.nwk';
        our $MAFFT_FILE_EXTENSION                 = '.mafft';
        our $FILTERED_HTML_FILE_EXTENSION         = "_filtered.html";
        our $PHYLOGENY_FILE_SUFFIX                = '_phylogeny_tree' . $NEWICK_FILE_EXTENSION;
        our $XML_TREE_FILE_SUFFIX                 = '_rap_tree.xml';

        my $family_output_dir_prefix = GP('TEMP_OUTPUT_PATH') . '/' . $self->id . '/' . $self->id;
        my $family_output_dir_prefix2 = GP('TEMP_OUTPUT_PATH') . '/' . $self->accession . '/' . $self->accession;
        if ((-e $family_output_dir_prefix . $XML_TREE_FILE_SUFFIX)
            || (-e $family_output_dir_prefix2 . $XML_TREE_FILE_SUFFIX))
        {
            return $ORTHOLOGY_LEVEL;
        }
        elsif ((-e $family_output_dir_prefix . $PHYLOGENY_FILE_SUFFIX)
              || (-e $family_output_dir_prefix2 . $PHYLOGENY_FILE_SUFFIX))
        {
            return $PHYLOGENY_LEVEL;
        }
        elsif ((-e $family_output_dir_prefix . $FILTERED_HTML_FILE_EXTENSION)
               || (-e $family_output_dir_prefix2 . $FILTERED_HTML_FILE_EXTENSION))
        {
            return $FILTRATION_LEVEL;
        }
        elsif ((-e $family_output_dir_prefix . $MAFFT_FILE_EXTENSION)
               || (-e $family_output_dir_prefix2 . $MAFFT_FILE_EXTENSION))
        {
            return $ALIGNMENT_LEVEL;
        }
        else
        {
            return '';
        }
    }
    
    return '';
}
sub hasAnalyses; *hasAnalyses = \&hasAnalyzes;


=pod

=head2 hasAnnotationSubmitted

B<Description>: tells if a family has pending annotations.

B<ArgsCount>: 1

=over 4

=item $self: (Greenphyl::Family) (R)

Current family.

=back

B<Return>: (boolean)

Returns true if family has pending annotations.

B<Example>:

    if ($family->hasAnnotationSubmitted())
    {
        # family has annotations submitted
        ...
    }

=cut

sub hasAnnotationSubmitted
{
    my ($self) = @_;

    foreach (@{$self->annotations})
    {
        if ('submitted' eq $_->status)
        {return 1;}
    }

    return 0;
}


=pod

=head1 DIAGNOSTICS

=head1 AUTHORS

Mathieu ROUARD (Bioversity), m.rouard@cgiar.org

Valentin GUIGNON (Bioversity), v.guignon@cgiar.org

Matthieu CONTE (Syngenta)

=head1 VERSION

Version 2.0.0

Date 11/10/2013

=head1 SEE ALSO

GreenPhyl documentation, DBObject documentation.

=cut

return 1; # package return
