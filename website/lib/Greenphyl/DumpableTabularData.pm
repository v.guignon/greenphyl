=pod

=head1 NAME

Greenphyl::DumpableTabularData - parent class for objects "dumpable" in tables

=head1 SYNOPSIS

    package Greenphyl::CompositeObject;
    ...
    use base qw(Greenphyl::DumpableObject Greenphyl::DumpableTabularData);
    ...
    return 1;
    ...
    my $composite = Greenphyl::CompositeObject->new(
        {
            'a_sequence' => $seq_a,
            'b_sequence' => $seq_b,
        }
    );
    my $dump_data = $composite->dump(
        'excel',
        {
            'columns' =>
            {
                '1. ID DB1'       => 'a_sequence.id',
                '2. Sequence DB1' => 'a_sequence.name',
                '3. ID DB2'       => 'b_sequence.id',
                '4. Sequence DB2' => 'b_sequence.name',
            },
        },
    );
    ...

=head1 REQUIRES

Perl5

=head1 EXPORTS

Nothing

=head1 DESCRIPTION

This class is the parent class for objects that can be dumped in a tabular
format like Excel, CSV or XML.

=cut

package Greenphyl::DumpableTabularData;


use strict;
use warnings;

use Carp qw(cluck confess croak);

use Data::Tabular::Dumper;
use Data::Tabular::Dumper::XML;
use Data::Tabular::Dumper::CSV;
use Data::Tabular::Dumper::Excel;
use JSON;

use Greenphyl;
use Greenphyl::DumpableObject;



# Package subs
###############

=pod

=head1 STATIC METHODS

=head2 GetDumperOptions

B<Description>: This function takes a string detailing the dump type and a file
handle. It returns a hash detailing the options needed by Data::Tabular::Dumper.

B<ArgsCount>: 3

=over 4

=item $class: (string) (R)

the derived class name.

=item $dump_format: (string) (R)

the name of the dump format. ('excel', 'csv', 'xml', 'json')

=item $handle: (GLOB) (R)

a file handle.

=back

B<Return>: (list)

A list containing what Data::Tabular::Dumper->open() expects as arguments for
the given file format.

B<Example>:

    my $dumper = Data::Tabular::Dumper->open(Greenphyl::DumpableTabularData->GetDumperOptions($format_name, $file_handle));

=cut

sub GetDumperOptions
{
    my ($class, $dump_format, $handle) = @_;
    if (!$class)
    {confess "ERROR: No class! Maybe you didn't call the class method using the '->' operator.\n";}

    # to make sure it doesn't mangle the XLS
    binmode($handle);
    my %data_tabular_dumper_options =
        (
            'csv'   => ['CSV'   => [$handle, { 'eol' => "\n" }]],
            'xml'   => ['XML'   => [$handle, 'data']],
            'excel' => ['Excel' => [$handle]],
            'json'  => 1,
        );

    if (!exists($data_tabular_dumper_options{$dump_format}))
    {
        Throw('error' => "Unknown dump format! Please use one of the allowed dump format: xml, csv or excel.", 'log' => "format: $dump_format");
    }

    return @{$data_tabular_dumper_options{$dump_format}};
}


=pod

=head2 DumpTable

B<Description>: produce a table of the given format name using given objects
and given parameters.

B<ArgsCount>: 4

=over 4

=item $class: (string)

The class name calling this function.

=item $format_name: (string)

Name of the output format.

=item $object_ref: (ref on array of objects)

Array of objects to dump into table.

=item $parameters: (hash ref)

Hash of parameters given to the class' CreateTable method.

=back

B<Return>: (string)

the table in a string of the specified format.

B<Example>:

    Greenphyl::Family->DumpTable('excel', [$family1, $family2], {'columns' => {'01.ID'=>'id','02.Name'=>'name','03.Sequences'=>'sequences.*.name'}});

=cut

sub DumpTable
{
    my ($class, $format_name, $object_ref, $parameters) = @_;
    if (!$class)
    {confess "ERROR: No class! Maybe you didn't call the class method using the '->' operator.\n";}
    return _DumpTable($format_name, $class->CreateTable($object_ref, $parameters));
}


=pod

=head2 _DumpTable

B<Description>: produce a table of the given format name from a Perl structure.

B<ArgsCount>: 2

=over 4

=item $format_name: (string)

Name of the output format.

=item $table: (array ref, hash ref,...)

Any structure that can be processed by Data::Tabular::Dumper module.
see Data::Tabular::Dumper module documentation for details.

=back

B<Return>: (string)

the table in a string of the specified format.

=cut

sub _DumpTable
{
    my ($format_name, $table) = @_;

    if (!$table)
    {confess "ERROR: No table to dump.\n";}
    elsif ((ref($table) ne 'ARRAY') && (ref($table) ne 'HASH'))
    {confess "ERROR: Invalid table parameter: not an array ref or a hash ref!\n";}

    # create a filehandle to a buffer
    my ($output_buffer, $handle);
    
    # check if we can use Data::Tabular::Dumper or another dumper
    if ('json' eq $format_name)
    {
        # use JSON
        $output_buffer = to_json($table);
    }
    else
    {
        open($handle, '>', \$output_buffer);
        Data::Tabular::Dumper->open( Greenphyl::DumpableTabularData->GetDumperOptions($format_name, $handle) )->dump( $table );
    }

    return $output_buffer;
}


=pod

=head1 METHODS

=head2 dumpTable

B<Description>: produce a table of the given format name using current object
and given parameters.

B<ArgsCount>: 3

=over 4

=item $self: (object)

The object to dump into a table.

=item $format_name: (string)

Name of the output format.

=item $parameters: (hash ref)

Hash of parameters given to the createTable method.

=back

B<Return>: (string)

the table in a string of the specified format.

B<Example>:

    $family->dumpTable('excel', {'columns' => {'01.ID'=>'id','02.Name'=>'name','03.Sequences'=>'sequences.*.name'}});

=cut

sub dumpTable
{
    my ($self, $format_name, $parameters) = @_;
    if (!$self)
    {confess "ERROR: No object! Can't dump table.\n";}

    return _DumpTable($format_name, $self->createTable($parameters));
}


=pod

=head2 getColumnValue

B<Description>: produce a string from current object and a list of members to
proceed.

B<ArgsCount>: 2

=over 4

=item $self: (Greenphyl::DumpableObject derived object)

Current object.

=item $column: (string)

A chain of member methods or keys or indexes to use to fetch the value to
return from current object. These are separated by dots '.'.

=back

B<Return>: (string)

a string corresponding to the chain of members to call on current object.

B<Example>:

    my $sequence_list = $family->getColumnValue('sequences.*.seq_textid');
    print "Family $family contains sequences:\n$sequence_list\n";

=cut

sub getColumnValue
{
    my ($self, $column) = @_;
    return Greenphyl::DumpableObject::StringifyObjectMember($self, [split(/\./, $column)]);
}


=pod

=head2 CreateTable

B<Description>: Returns a table containing the requested columns of given
objects.

This method is requiered and called by
Greenphyl::DumpableTabularData::DumpTable method.

B<ArgsCount>: 3

=over 4

=item $class: (string) (R)

Should be a Greenphyl::DBOject derived object class or similar.

=item $object_ref: (array of any type of object) (R)

The list of objects to process.

=item $parameters: (hash ref) (R)

a hash containing the key 'columns' that contains the columns to dump.
Each key is a column name (label) and each corresponding value is a string
containing the chain (dot-separated) of members to call to fetch the value from
each object.

=back

B<Return>: (array ref)

An array containing a list of hash, one hash per object. Each hash key is
column label and each value is the corresponding string value.

=cut

sub CreateTable
{
    my ($class, $object_ref, $parameters) = @_;
    
    my $table = [];
    if ($parameters->{'columns'})
    {
        foreach my $object (@$object_ref)
        {
            my $object_row = {};
            foreach my $column (keys(%{$parameters->{'columns'}}))
            {
                $object_row->{$column} = $object->getColumnValue($parameters->{'columns'}->{$column});
            }
            push(@$table, $object_row);
        }
    }
    return $table;
}


=pod

=head2 createTable

B<Description>: Returns a table containing the requested columns of given
objects.

This method is requiered and called by
Greenphyl::DumpableTabularData::dumpTable method.

B<ArgsCount>: 2

=over 4

=item $self: (Greenphyl::DBOject derived class or similar) (R)

The object to process.

=item $parameters: (hash ref) (R)

a hash containing the key 'columns' that contains the columns to dump.
Each key is a column name (label) and each corresponding value is a string
containing the chain (dot-separated) of members to call to fetch the value from
current object.

=back

B<Return>: (hash ref)

Each hash key is column label and each value is the corresponding string value.

=cut

sub createTable
{
    my ($self, $parameters) = @_;
    my $object_table = {};
    if ($parameters->{'columns'})
    {
        foreach my $column (keys(%{$parameters->{'columns'}}))
        {
            $object_table->{$column} = $self->getColumnValue($parameters->{'columns'}->{$column});
        }
    }
    return $object_table;
}


=pod

=head2 DumpExcel, DumpCSV, DumpXML

B<Description>: Returns a string in the given format containing a table with
the requested columns of given objects.

B<ArgsCount>: 2

=over 4

=item $object_ref: (array of Greenphyl::DBObject derived class or similar) (R)

The list of objects to process.

=item $parameters: (hash ref) (R)

a hash containing the key 'columns' that contains the columns to dump.
Each key is a column name (label) and each corresponding value is a string
containing the chain (dot-separated) of members to call to fetch the value from
each object.

=back

B<Return>: (string)

a string in the given format containing a table with the requested columns of
given objects.

B<Example>:

    my $excel_fh;
    if (open($excel_fh, '>sequences.xls'))
    {
        binmode($excel_fh);
        print {$excel_fh} Greenphyl::Sequence::DumpExcel([$sequence1, $sequence2], {'columns' => {'01.ID'=>'id','02.Name'=>'name','03.species'=>'species','04.Protein sequence'=>'sequence'}});
        close($excel_fh);
    }
    ...
    print {$csv_fh} Greenphyl::Sequence::DumpCSV([$sequence1, $sequence2], {'columns' => {'01.ID'=>'id','02.Name'=>'name','03.species'=>'species','04.Protein sequence'=>'sequence'}});
    ...
    print {$xml_fh} Greenphyl::Sequence::DumpXML([$sequence1, $sequence2], {'columns' => {'01.ID'=>'id','02.Name'=>'name','03.species'=>'species','04.Protein sequence'=>'sequence'}});

=cut

sub DumpExcel
{
    my ($object_ref, $parameters) = @_;

    my $class = ref($object_ref->[0]);
    my $output;
    eval "\$output = " . $class . "->DumpTable('excel', \$object_ref, \$parameters);";
    if ($@)
    {
        confess "ERROR: Failed to dump objects: $@\n";
    }
    return $output;
}


sub DumpCSV
{
    my ($object_ref, $parameters) = @_;

    my $class = ref($object_ref->[0]);
    my $output;
    eval "\$output = " . $class . "->DumpTable('csv', \$object_ref, \$parameters);";
    if ($@)
    {
        confess "ERROR: Failed to dump objects: $@\n";
    }
    return $output;
}


sub DumpXML
{
    my ($object_ref, $parameters) = @_;
    my $class = ref($object_ref->[0]);

    #convert column names for XML
    my @column_names = keys(%{$parameters->{'columns'}});
    foreach my $column_name (@column_names)
    {
        my $new_column_name = $column_name;
        # remove indexes
        $new_column_name =~ s/^[0-9a-z]{1,2}\.\s*//;
        # remove unwanted characters
        $new_column_name =~ s/\W+/_/g;
        # make sure name does not begin with a number
        $new_column_name =~ s/^([0-9])/_$1/;
        $new_column_name = lc($new_column_name);
        # rename column
        $parameters->{'columns'}->{$new_column_name} = delete($parameters->{'columns'}->{$column_name});
    }
    
    my $output;
    eval "\$output = " . $class . "->DumpTable('xml', \$object_ref, \$parameters);";
    if ($@)
    {
        confess "ERROR: Failed to dump objects: $@\n";
    }
    return $output;
}


sub DumpJSON
{
    my ($object_ref, $parameters) = @_;

    my $class = ref($object_ref->[0]);
    my $output;
    eval "\$output = " . $class . "->DumpTable('json', \$object_ref, \$parameters);";
    if ($@)
    {
        confess "ERROR: Failed to dump objects: $@\n";
    }
    return $output;
}


=pod

=head1 DIAGNOSTICS

=head1 AUTHORS

Mathieu ROUARD (Bioversity), m.rouard@cgiar.org

Valentin GUIGNON (Bioversity), v.guignon@cgiar.org

Matthieu CONTE (Syngenta)

=head1 VERSION

Version 1.0.0

Date 10/05/2012

=head1 SEE ALSO

GreenPhyl documentation. DumpableObject doc.

=cut

return 1; # package return
