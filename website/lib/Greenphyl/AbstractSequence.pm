=pod

=head1 NAME

Greenphyl::AbstractSequence - GreenPhyl Sequence abstraction layer

=head1 SYNOPSIS

    package Greenphyl::Sequence;

    ...

    use base qw(Greenphyl::AbstractSequence);

=head1 REQUIRES

Perl 5.8.0, Greenphyl::[Species, SequenceIPR, Family, IPR, DBXRef, Homology],
BioPerl

=head1 EXPORTS

nothing

=head1 DESCRIPTION

This module implements a GreenPhyl sequence database object.

=cut

package Greenphyl::AbstractSequence;

use strict;
use warnings;

use base qw(
    Greenphyl::CachedDBObject
    Greenphyl::DumpableObject
    Greenphyl::DumpableTabularData
);

use vars qw($AUTOLOAD);

use Carp qw(cluck confess croak);

use Greenphyl;
use Greenphyl::GO;
use Greenphyl::SourceDatabase;
use Greenphyl::DBXRefLinks;
use Greenphyl::Species; # for species code regexp
use Greenphyl::Homology;

use IO::String;
use Bio::SeqIO;
use Bio::Seq;




# Package constants
####################

=pod

=head1 CONSTANTS

B<$OBJECT_PROPERTIES>: (hash ref)

sequence object properties description. See DBObject constructor documentation
for details.

B<$MAX_IPR_LAYERS>: (integer)

Maximum number of layers a sequence can have for IPR domain rendering.

=cut

our $DEBUG = 0;

our $MAX_IPR_LAYERS = 50;
our $MIN_IPR_LAYER_HEIGHT = 10;
our $DEFAULT_IPR_LAYER_HEIGHT = 20;
our $SUPPORTED_DUMP_FORMAT =
    {
        'static'  => {
            'fasta'   => \&DumpFasta,
            'embl'    => \&DumpEMBL,
            'genbank' => \&DumpGenBank,
            'swiss'   => \&DumpSwissprot,
            'scf'     => \&DumpSCF,
            'pir'     => \&DumpPIR,
            'gcg'     => \&DumpGCG,
            'raw'     => \&DumpRaw,
            'ace'     => \&DumpACeDB,
            'game'    => \&DumpGameXML,
            'phd'     => \&DumpPhred,
            'qual'    => \&DumpQualityValues,
            'excel'   => \&DumpExcel,
            'csv'     => \&DumpCSV,
            'xml'     => \&DumpXML,
            'json'    => \&DumpJSON,
        },
        'dynamic' => {
            'fasta'   => \&DumpFasta,
            'embl'    => \&DumpEMBL,
            'genbank' => \&DumpGenBank,
            'swiss'   => \&DumpSwissprot,
            'scf'     => \&DumpSCF,
            'pir'     => \&DumpPIR,
            'gcg'     => \&DumpGCG,
            'raw'     => \&DumpRaw,
            'ace'     => \&DumpACeDB,
            'game'    => \&DumpGameXML,
            'phd'     => \&DumpPhred,
            'qual'    => \&DumpQualityValues,
            'excel'   => \&DumpExcel,
            'csv'     => \&DumpCSV,
            'xml'     => \&DumpXML,
            'json'    => \&DumpJSON,
        },
    };




# Package subs
###############

=pod

=head1 STATIC METHODS

=head2 CONSTRUCTOR

B<Description>: Creates a new instance of a GreenPhyl sequence object.

B<ArgsCount>: 1-2

=over 4

=item $fasta_content: (DBI::db) (O)

A FASTA content to load the sequences from.

=item $dbh: (DBI) (U)

Database handler of the database to load the object from.

=item $parameters: (hash ref) (R)

See Greenphyl::DBObject constructor documentation for details.

=back

B<Return>: (Greenphyl::AbstractSequence)

a new instance.

B<Caller>: General

B<Example>:

    my $sequence = new Greenphyl::AbstractSequence($dbh, {'selectors' => {'accession' => ['LIKE', 'Os01g01050%']}});
    my $fasta_sequence = new Greenphyl::AbstractSequence(">Os01g01050.1_ORYSA\nMINFAVQSRCAPTNSLCSCVALCERNFKLEQVEELASLIKDNLYSKHLVLSTEETLVGIL");

=cut

sub new
{
    my $proto = shift();
    my $class = ref($proto) || $proto;
    
    my $object_properties = $class->GetDefaultClassProperties();

    my ($dbh, $fasta_content);
    my $loader = shift();
    my $parameters = shift();

    if (defined($loader))
    {
        if (!ref($loader))
        {
            # FASTA
            $fasta_content = $loader;
            if ('DBI::db' eq ref($parameters))
            {
                $dbh = $parameters;
                $parameters = shift();
            }
            $parameters ||= {};
        }
        elsif ('DBI::db' eq ref($loader))
        {
            # database
            $dbh = $loader;
        }
        else
        {
            confess "ERROR: invalid parameters! The first parameter must be either undef, a database handler or a FASTA string.\n";
        }
    }

    if ($parameters && ('HASH' ne ref($parameters)))
    {
        confess "ERROR: invalid parameters! The parameter after database handlher or FASTA string must be a hash ref.\n";
    }
    $parameters ||= {};
    
    if ($fasta_content)
    {
        my $stringfh;
        open($stringfh, "<", \$fasta_content)
            or confess "Could not open string for reading: $!";
        
        my $seqio = Bio::SeqIO-> new(
                '-fh'     => $stringfh,
                '-format' => 'fasta',
            );
            
        my $generate_sequence_hash = sub
            {
                my ($bio_seq) = @_;
                my $accession = $bio_seq->id;
                # remove species code
                $accession =~ s/_($Greenphyl::Species::SPECIES_CODE_REGEXP)$//;
                my $species_code = $1;
                my $members = {
                    'id'          => -1,
                    'annotation'  => $bio_seq->desc || '',
                    'accession'   => $accession,
                    'filtered'    => 0,
                    'length'      => length($bio_seq->seq),
                    'polypeptide' => $bio_seq->seq,
                };
                # use dynamic module loading to preserve module loading order (as Greenphyl::Tools::Sequences uses AbstractSequence derived classes)
                eval "use Greenphyl::Tools::Sequences;";
                if ($@)
                {confess $@;}
                my ($locus, $splice) = ExtractLocusAndSpliceForm($accession, $species_code);
                if ($locus)
                {
                    $members->{'locus'}  = $locus;
                    $members->{'splice'} = $splice;
                    $members->{'splice_priority'} = (defined($splice)?0:undef);
                }

                if ($species_code)
                {
                    my $species = new Greenphyl::Species($dbh || GetDatabaseHandler(), {'selectors' => {'code' => $species_code},});
                    if ($species)
                    {
                        $members->{'species_id'} = $species->id;
                        $members->{'species'} = $species;
                    }
                    else
                    {
                        # species not in DB, put back species code
                        $members->{'accession'}  = $accession . '_' . $species_code;
                    }
                }

                return $members;
            };
        if (wantarray())
        {
            my @sequences;
            $parameters->{'load'} = 'none';
            while (my $bio_seq = $seqio->next_seq)
            {
                $parameters->{'members'} = $generate_sequence_hash->($bio_seq);
                push(@sequences, $class->SUPER::new($object_properties, $dbh, $parameters));
            }
            return @sequences;
        }
        else
        {
            # return single sequence
            my $bio_seq = $seqio->next_seq;
            $parameters->{'load'} = 'none';
            $parameters->{'members'} = $generate_sequence_hash->($bio_seq);
            return $class->SUPER::new($object_properties, $dbh, $parameters);
        }
    }
    else
    {
        return $class->SUPER::new($object_properties, $dbh, $parameters);
    }
}




=pod

=head1 METHODS

=head2 getStringValue

B<Description>: returns sequence name.

B<ArgsCount>: 1

=over 4

=item $self: (Greenphyl::AbstractSequence)

the sequence.

=back

B<Return>: (string)

the sequence name (accession).

B<Caller>: internal

=cut

sub getStringValue
{
    my ($self) = @_;
    return $self->{'accession'};
}


=pod

=head2 layerIPR

B<Description>: set IPR layers.

B<ArgsCount>: 1

=over 4

=item $self: (Greenphyl::AbstractSequence)

the sequence.

=back

B<Return>: (integer)

total number of layers.

=cut

sub layerIPR
{
    my ($self) = @_;

    my @sequence_iprs = sort { $a->ipr_start <=> $b->ipr_start } @{$self->ipr_details};

    my @layers; # contains arrays of sequence IPR
    $self->{'layer_count'} ||= 0;
    foreach my $sequence_ipr (@sequence_iprs)
    {
        my $layer_index = 0;
        while ($layer_index < $MAX_IPR_LAYERS)
        {
            # select current layer and initialize it if needed
            my $current_layer = $layers[$layer_index] ||= [];
            # stops if we found a layer with no overlapping domain
            last if !grep _CheckForIPRDomainOverlap($sequence_ipr, $_), @$current_layer;
            ++$layer_index;
        }
        # stay in boundaries
        if ($layer_index >= $MAX_IPR_LAYERS)
        {
            $layer_index = $MAX_IPR_LAYERS - 1;
        }
        # add sequence IPR to layer
        push(@{$layers[$layer_index]}, $sequence_ipr);
        # set sequence IPR layer
        $sequence_ipr->setLayer($layer_index);
        # keep track of highest layer index
        if ($layer_index > $self->{'layer_count'})
        {
            $self->{'layer_count'} = $layer_index;
        }
    }
    $self->{'layer_count'}++;
    
    # initialize heights
    # -default height
    $self->{'layer_height'} = $DEFAULT_IPR_LAYER_HEIGHT;
    # -
    $self->{'layer_height'} -= $self->{'layer_count'} * 3;
    if ($self->{'layer_height'} < $MIN_IPR_LAYER_HEIGHT)
    {
        $self->{'layer_height'} = $MIN_IPR_LAYER_HEIGHT;
    }

    foreach my $sequence_ipr (@sequence_iprs)
    {
        $sequence_ipr->setHeight($self->{'layer_height'});
    }

    return $self->{'layer_count'};
}


=pod

=head2 _CheckForIPRDomainOverlap

B<Description>: tells if 2 sequence IPR domains overlap.

B<ArgsCount>: 2

=over 4

=item $ipr_domain1, $ipr_domain2: (Greenphyl::AbstractSequenceIPR)

the 2 sequence IPR domains.

=back

B<Return>: (boolean)

true if the domains overlap, false otherwise.

B<Caller>: internal

=cut

sub _CheckForIPRDomainOverlap
{
    my ($ipr_domain1, $ipr_domain2 ) = @_;
    if ($ipr_domain1->start > $ipr_domain2->stop)
    {return 0;}
    elsif ($ipr_domain1->stop < $ipr_domain2->start)
    {return 0;}
    else
    {return 1;}
}


=pod

=head2 getLayerCount

B<Description>: returns the number of layers.

B<ArgsCount>: 1

=over 4

=item $self: (Greenphyl::AbstractSequence)

the sequence.

=back

B<Return>: (array ref)

the array of families sorted in level order (ascending).

=cut

sub getLayerCount
{
    my ($self) = shift();
    if (!exists($self->{'layer_count'})
        || !defined($self->{'layer_count'}))
    {
        $self->layerIPR();
    }
    return $self->{'layer_count'};
}
sub layer_count; *layer_count = \&getLayerCount;


=pod

=head2 getDomainLayerHeight


B<Description>: returns the number of layers.

B<ArgsCount>: 1

=over 4

=item $self: (Greenphyl::AbstractSequence)

the sequence.

=back

B<Return>: (array ref)

the array of families sorted in level order (ascending).

=cut

sub getDomainLayerHeight
{
    my ($self) = shift();
    if (!exists($self->{'layer_height'})
        || !defined($self->{'layer_height'}))
    {
        $self->layerIPR();
    }
    return $self->{'layer_height'};
}
sub getDomainHeight;     *getDomainHeight     = \&getDomainLayerHeight;
sub layer_domain_height; *layer_domain_height = \&getDomainLayerHeight;




=pod

=head1 ACCESSORS

=head2 families

B<Description>: getter/setter for families. Wraps default handler to sort
families by classification level.

B<Aliases>: getFamilies, setFamilies, Families

B<ArgsCount>: 1

=over 4

=item $self: (Greenphyl::AbstractSequence)

the sequence.

=back

B<Return>: (array ref)

the array of families sorted in level order (ascending).

=cut

sub families
{
    my ($self) = shift();
    # have the family list already been sorted?
    if (!$self->{'families'})
    {
        # no, call parent and sort results
        $self->{'families'} = $self->SUPER::families(@_);
        if ('ARRAY' ne ref($self->{'families'}))
        {
            $self->{'families'} = defined($self->{'families'}) ? [$self->{'families'}] : [];
        }
        return $self->{'families'} = [sort {return $a->level cmp $b->level} @{$self->{'families'}}];
    }
    else
    {
        return $self->SUPER::families(@_);
    }
}
# aliases
sub getFamilies; *getFamilies = \&families;
sub setFamilies; *setFamilies = \&families;
sub Families;    *Families    = \&families;


=pod

=head2 isInFamily

B<Description>: tells is the sequence is part of the given family.

B<ArgsCount>: 2

=over 4

=item $self: (Greenphyl::AbstractSequence)

the sequence.

=item $family: (Greenphyl::Family)

the family to check.

=back

B<Return>: (boolean)

True if the sequence is in the given family, false otherwise.

=cut

sub isInFamily
{
    my ($self, $family) = @_;

    if (!$self->{'_family_hash'})
    {
        $self->{'_family_hash'} = { (map { ("$_" => $_) } @{$self->families()}) };
    }
    return exists($self->{'_family_hash'}->{"$family"});
}




=pod

=head2 getLocusAndSpliceForm

B<Description>: returns sequence locus (name without splice form) and the splice
form (number).

B<ArgsCount>: 1

=over 4

=item $self: (Greenphyl::AbstractSequence) (R)

Current sequence.

=back

B<Return>: (list of a string and an integer)

Returns the sequence locus name and the splice form as an integer (0 if no
splice form has been detected).

=cut

sub getLocusAndSpliceForm
{
    my ($self) = @_;
    if (!defined($self->locus))
    {
        # use dynamic module loading to preserve module loading order (as Greenphyl::Tools::Sequences uses AbstractSequence derived classes)
        eval "use Greenphyl::Tools::Sequences;";
        if ($@)
        {confess $@;}
        my ($locus, $splice);
        if ($self->species)
        {
            ($locus, $splice) = ExtractLocusAndSpliceForm($self->accession, $self->species->code);
        }
        else
        {
           ($locus, $splice) = ExtractLocusAndSpliceForm($self->accession);
        }
        $self->locus($locus);
        $self->splice($splice);
        if ((!$self->splice_priority) && $self->splice($splice))
        {
            $self->splice_priority(0);
        }
    }
    PrintDebug("Sequence: " . $self->accession . "\nLocus: " . $self->locus . "\nSplice form: " . (defined($self->splice)?$self->splice:''));
    return ($self->locus, $self->splice);
}




=pod

=head2 getSelectedDBXref

B<Description>: returns the sequence DBXRef corresponding to the given database.

B<Aliases>: getUniprot, uniprot

B<ArgsCount>: 2

=over 4

=item $self: (Greenphyl::AbstractSequence) (R)

Current sequence.

=item $db_name: (string) (R)

Database name (as in db table).

=back

B<Return>: (array ref)

Returns an array of corresponding sequence DBXref or an empty array.

=cut

sub getSelectedDBXref
{
    my ($self, $db_name) = @_;

    my $key_name = lc($db_name);
    
    if (!exists($self->{$key_name}))
    {
        # get database identifier
        my $source_db = Greenphyl::SourceDatabase->new(
            $self->{'_dbh'},
            {
                'selectors' =>
                {
                    'name' => $db_name,
                },
            },
        );

        if (!$source_db)
        {
            PrintDebug("Warning: '$db_name' source database not found in db table!");
            cluck "Warning: '$db_name' source database not found in db table!\n";
            return undef;
        }

        # prepare storage array
        my $matching_dbxref = [];
        # check if we already loaded all sequence dbxref
        if (exists($self->{'dbxref'}) && @{$self->{'dbxref'}})
        {
            # yes, filter what we got
            foreach my $dbxref (@{$self->{'dbxref'}})
            {
                if ($source_db->id == $dbxref->db_id)
                {
                    push(@$matching_dbxref, $dbxref);
                }
            }
        }
        else
        {
            # no, fetch from database
            # save previous fetch results as we will use the fetch method
            my $previous_fetch_results = $self->{'_fetch'}->{'dbxref'};
            # fetch only requested DBXRef
            $matching_dbxref = $self->fetch_dbxref({'selectors' => {'db_id' => $source_db->id}});
            # restore previous fetch
            $self->{'_fetch'}->{'dbxref'} = $previous_fetch_results;
        }
        $self->{$key_name} = $matching_dbxref;
    }

    return $self->{$key_name};
}


=pod

=head2 getUniProt

B<Description>: returns the sequence UniProt DBXRef.

B<Aliases>: getUniprot, uniprot

B<ArgsCount>: 1

=over 4

=item $self: (Greenphyl::AbstractSequence) (R)

Current sequence.

=back

B<Return>: (array ref)

Returns an array of sequence UniProt or an empty array.

=cut

sub getUniProt
{
    my ($self) = @_;

    return $self->getSelectedDBXref($Greenphyl::SourceDatabase::SOURCE_DATABASE_UNIPROT);
}
# Aliases
sub getUniprot; *getUniprot = \&getUniProt;
sub uniprot; *uniprot = \&getUniProt;


=pod

=head2 getPubMed

B<Description>: returns the sequence PubMed DBXRef.

B<Aliases>: pubmed, getPubmed, pmid

B<ArgsCount>: 1

=over 4

=item $self: (Greenphyl::AbstractSequence) (R)

Current sequence.

=back

B<Return>: (array ref)

Returns an array of sequence PubMed or an empty array.

=cut

sub getPubMed
{
    my ($self) = @_;

    return $self->getSelectedDBXref($Greenphyl::SourceDatabase::SOURCE_DATABASE_PUBMED);
}
# Aliases
sub getPubmed; *getPubmed = \&getPubMed;
sub pubmed; *pubmed = \&getPubMed;


=pod

=head2 getKEGG

B<Description>: returns the sequence KEGG DBXRef.

B<Aliases>: kegg, getKegg

B<ArgsCount>: 1

=over 4

=item $self: (Greenphyl::AbstractSequence) (R)

Current sequence.

=back

B<Return>: (array ref)

Returns an array of sequence EC or an empty array.

=cut

sub getKEGG
{
    my ($self) = @_;

    return $self->getSelectedDBXref($Greenphyl::SourceDatabase::SOURCE_DATABASE_KEGG);
}
# Aliases
sub getKegg; *getKegg = \&getKEGG;
sub kegg; *kegg = \&getKEGG;


=pod

=head2 getEC

B<Description>: returns the sequence EC DBXRef.

B<Aliases>: ec

B<ArgsCount>: 1

=over 4

=item $self: (Greenphyl::AbstractSequence) (R)

Current sequence.

=back

B<Return>: (array ref)

Returns an array of sequence EC or an empty array.

=cut

sub getEC
{
    my ($self) = @_;

    return $self->getSelectedDBXref($Greenphyl::SourceDatabase::SOURCE_DATABASE_EC);
}
# Aliases
sub ec; *ec = \&getEC;


=pod

=head2 getExternalLinks

B<Description>: returns an external link structure corresponding to current
sequence.

B<ArgsCount>: 1

=over 4

=item $self: (Greenphyl::AbstractSequence) (R)

Current sequence.

=back

B<Return>: (hash ref)

Returns an external link structure. See Greenphyl::DBXRefLinks::GetExternalLinks
for details.

=cut

sub getExternalLinks
{
    my ($self) = @_;
    return Greenphyl::DBXRefLinks::GetExternalLinks($self);
}


=pod

=head2 getGBrowseLink

B<Description>: returns a URL to a GBrowse pointing on current sequence and
a URL to the corresponding image.

B<ArgsCount>: 1

=over 4

=item $self: (Greenphyl::AbstractSequence) (R)

Current sequence.

=back

B<Return>: (list of strings)

Returns 2 URL, one to GBrowse and one to the gene model image.
See Greenphyl::DBXRefLinks::GenerateGBrowseLink for details.

=cut

sub getGBrowseLink
{
    my ($self) = @_;
    return Greenphyl::DBXRefLinks::GenerateGBrowseLink($self);
}


=pod

=head2 convertToBioSeq

B<Description>: returns a Bio::Seq object from Greenphyl::AbstractSequence object.

B<ArgsCount>: 1-2

=over 4

=item $self: (Greenphyl::AbstractSequence) (R)

The sequence to convert.

=item $parameters: (hash) (O)

Optional parameter hash. Keys:
-'without_species_code': if set to true, species code will not be outputted.

=back

B<Return>: (Bio::Seq)

A Bio Perl Bio::Seq object.

=cut

sub convertToBioSeq
{
    my ($self, $parameters) = @_;

    # init Bio::Seq hash data
    my %bioseq_hash = (
        '-seq' => $self->polypeptide,
    );
    
    $parameters ||= {};
    # check if species code should be included (default: yes)
    if ($parameters->{'without_species_code'})
    {
        $bioseq_hash{'-id'}
            = $bioseq_hash{'-accession_number'}
            = $self->accession;
    }
    elsif ($parameters->{'with_family_info'})
    {
    	  my $family_name = $self->families->[0]->name;
    	  $family_name =~s/\W+/_/g;
        $bioseq_hash{'-id'}
            = $bioseq_hash{'-accession_number'}
            = $self->families->[0]->accession . '_' . $family_name . '|' .  $self->accession;
    }
    elsif ($self->species)
    {
        $bioseq_hash{'-id'}
            = $bioseq_hash{'-accession_number'}
            = $self->accession . '_' . $self->species->code;
    }
    else
    {
        $bioseq_hash{'-id'}
            = $bioseq_hash{'-accession_number'}
            = $self->accession;
    }

    # check if sequence annotation should be included
    if ($parameters->{'with_annotation'})
    {
        $bioseq_hash{'-desc'} = $self->annotation;
    }

    # generate Bio::Seq object
    my $bio_seq = Bio::Seq->new(%bioseq_hash);

    return $bio_seq;
}


=pod

=head2 DumpBioSeq

B<Description>: returns the given sequence(s) dumped into string of the given
format.
Supports non-static calls.

B<ArgsCount>: 2-3

=over 4

=item $object_ref: (Greenphyl::AbstractSequence ref or array of Greenphyl::AbstractSequence ref) (R)

Sequence or list of sequences to dump.

=item $format: (string) (R)

the output format.

=item $parameters: (any ref or string) (O)

a reference or a string containing additional parameters.

=back

B<Return>: (string)

The dumped sequence(s) string.

=cut

sub DumpBioSeq
{
    my ($object_ref, $format, $parameters) = @_;

    my $dump_output = '';
    my $string_io = IO::String->new($dump_output);
    my $seq_io = Bio::SeqIO->new('-fh' => $string_io, '-format' => $format);

    if (ref($object_ref) ne 'ARRAY')
    {
        $object_ref = [$object_ref];
    }

    foreach my $object (@$object_ref)
    {
        $seq_io->write_seq($object->convertToBioSeq($parameters));
        $dump_output .= "\n";
    }

    return $dump_output;
}


=pod

=head2 Dump*

B<Description>: returns the given sequence(s) dumped into a * string.
Supports non-static calls.

B<ArgsCount>: 1-2

=over 4

=item $object_ref: (Greenphyl::AbstractSequence ref or array of Greenphyl::AbstractSequence ref) (R)

Sequence or list of sequences to dump.

=item $parameters: (any ref or string) (O)

a reference or a string containing additional parameters.

=back

B<Return>: (string)

The dumped sequence(s) string.

=cut

sub DumpFasta
{
    my ($object_ref, $parameters) = @_;

    return DumpBioSeq($object_ref, 'Fasta', $parameters);
}

sub DumpEMBL
{
    my ($object_ref, $parameters) = @_;

    return DumpBioSeq($object_ref, 'EMBL', $parameters);
}

sub DumpGenBank
{
    my ($object_ref, $parameters) = @_;

    return DumpBioSeq($object_ref, 'GenBank', $parameters);
}

sub DumpSwissprot
{
    my ($object_ref, $parameters) = @_;

    return DumpBioSeq($object_ref, 'swiss', $parameters);
}

sub DumpSCF
{
    my ($object_ref, $parameters) = @_;

    return DumpBioSeq($object_ref, 'SCF', $parameters);
}

sub DumpPIR
{
    my ($object_ref, $parameters) = @_;

    return DumpBioSeq($object_ref, 'PIR', $parameters);
}

sub DumpGCG
{
    my ($object_ref, $parameters) = @_;

    return DumpBioSeq($object_ref, 'GCG', $parameters);
}

sub DumpRaw
{
    my ($object_ref, $parameters) = @_;

    return DumpBioSeq($object_ref, 'raw', $parameters);
}

sub DumpACeDB
{
    my ($object_ref, $parameters) = @_;

    return DumpBioSeq($object_ref, 'ace', $parameters);
}

sub DumpGameXML
{
    my ($object_ref, $parameters) = @_;

    return DumpBioSeq($object_ref, 'game', $parameters);
}

sub DumpPhred
{
    my ($object_ref, $parameters) = @_;

    return DumpBioSeq($object_ref, 'phd', $parameters);
}

sub DumpQualityValues
{
    my ($object_ref, $parameters) = @_;

    return DumpBioSeq($object_ref, 'qual', $parameters);
}


=pod

=head2 DumpExcel, DumpCSV, DumpXML

B<Description>: Returns a string in the given format containing a table with
the requested columns of given sequences.

B<ArgsCount>: 2

=over 4

=item $object_ref: (array of Greenphyl::AbstractSequence) (R)

The list of sequences to process.

=item $parameters: (hash ref) (R)

a hash containing the key 'columns' that contains the columns to dump.
Each key is a column name (label) and each corresponding value is a string
containing the chain (dot-separated) of members to call to fetch the value from
each sequence.

=back

B<Return>: (string)

a string in the given format containing a table with the requested columns of
given sequences.

B<Example>:

    my $excel_fh;
    if (open($excel_fh, '>sequences.xls'))
    {
        binmode($excel_fh);
        print {$excel_fh} Greenphyl::AbstractSequence::DumpExcel([$sequence1, $sequence2], {'columns' => {'01.ID'=>'id','02.Name'=>'accession','03.species'=>'species','04.Protein sequence'=>'polypeptide'}});
        close($excel_fh);
    }
    ...
    print {$csv_fh} Greenphyl::AbstractSequence::DumpCSV([$sequence1, $sequence2], {'columns' => {'01.ID'=>'id','02.Name'=>'accession','03.species'=>'species','04.Protein sequence'=>'polypeptide'}});
    ...
    print {$xml_fh} Greenphyl::AbstractSequence::DumpXML([$sequence1, $sequence2], {'columns' => {'01.ID'=>'id','02.Name'=>'accession','03.species'=>'species','04.Protein sequence'=>'polypeptide'}});

=cut

sub DumpExcel
{
    my ($object_ref, $parameters) = @_;

    return Greenphyl::AbstractSequence->DumpTable('excel', $object_ref, $parameters);
}


sub DumpCSV
{
    my ($object_ref, $parameters) = @_;

    return Greenphyl::AbstractSequence->DumpTable('csv', $object_ref, $parameters);
}


sub DumpXML
{
    my ($object_ref, $parameters) = @_;

    #convert column names for XML
    my @column_names = keys(%{$parameters->{'columns'}});
    foreach my $column_name (@column_names)
    {
        my $new_column_name = $column_name;
        # remove indexes
        $new_column_name =~ s/^[0-9a-z]{1,2}\.\s*//;
        # remove unwanted characters
        $new_column_name =~ s/\W+/_/g;
        # make sure name does not begin with a number
        $new_column_name =~ s/^([0-9])/_$1/;
        $new_column_name = lc($new_column_name);
        # rename column
        $parameters->{'columns'}->{$new_column_name} = delete($parameters->{'columns'}->{$column_name});
    }
    
    return Greenphyl::AbstractSequence->DumpTable('xml', $object_ref, $parameters);
}


sub DumpJSON
{
    return Greenphyl::DumpableTabularData::DumpJSON(@_);
}


=pod

=head1 AUTHORS

Mathieu ROUARD (Bioversity), m.rouard@cgiar.org

Valentin GUIGNON (Bioversity), v.guignon@cgiar.org

Matthieu CONTE (Syngenta)

=head1 VERSION

Version 1.0.0

Date 11/10/2013

=head1 SEE ALSO

GreenPhyl documentation.

=cut

return 1; # package return
