=pod

=head1 NAME

Greenphyl::Variables - GreenPhyl Variables Object

=head1 SYNOPSIS

    use Greenphyl::Variables;
    my $version = Greenphyl::Variables->new($dbh, {'selectors' => {'name' => 'schema_version'}});
    print $version->value();

=head1 REQUIRES

Perl5

=head1 EXPORTS

Nothing

=head1 DESCRIPTION

This module implements a GreenPhyl Variables database object.

=cut

package Greenphyl::Variables;

use strict;
use warnings;

use base qw(Greenphyl::CachedDBObject);

use vars qw($AUTOLOAD);

use Carp qw(cluck confess croak);




# Package constants
####################

=pod

=head1 CONSTANTS

B<$OBJECT_PROPERTIES>: (hash ref)

object properties description. See DBObject constructor documentation for
details.

=cut

# use "generate_table_properties.pl" script to auto-generate the template
# structure.

our $OBJECT_PROPERTIES = {
    'table' => 'variables',
    'key'   => 'name',
    'alternate_keys' => [],
    'load'  => [
        'name',
        'value',
    ],
    'alias' => {
    },
    'base'  => {
        'name' => {
            'property_column' => 'name',
        },
        'value' => {
            'property_column' => 'value',
        },
    },
    'secondary' => {},
    'tertiary' => {},
};




# Package subs
###############

=pod

=head1 STATIC METHODS

=head2 CONSTRUCTOR

B<Description>: Creates a new instance of a GreenPhyl Variables object.

B<ArgsCount>: 2

=over 4

=item $dbh: (DBI::db) (U)

Database handler of the database to load the object from.

=item $parameters: (hash ref) (R)

See Greenphyl::DBObject constructor documentation for details.

=back

B<Return>: (Greenphyl::Variables)

a new instance.

B<Caller>: General

B<Example>:

    my $version = Greenphyl::Variables->new($dbh, {'selectors' => {'name' => 'schema_version'}});

=cut

sub new
{
    my $proto = shift();
    my $class = ref($proto) || $proto;
    
    my $object_properties = $class->GetDefaultClassProperties();

    return $class->SUPER::new($object_properties, @_);
}




=pod

=head1 AUTHORS

Mathieu ROUARD (Bioversity), m.rouard@cgiar.org

Valentin GUIGNON (Bioversity), v.guignon@cgiar.org

Matthieu CONTE (Syngenta)

=head1 VERSION

Version 1.0.0

Date 17/04/2012

=head1 SEE ALSO

GreenPhyl documentation, DBObject documentation.

=cut

return 1; # package return
