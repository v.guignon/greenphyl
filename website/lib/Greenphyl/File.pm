=pod

=head1 NAME

Greenphyl::File - GreenPhyl File Object

=head1 SYNOPSIS

    use Greenphyl::File;
    my $user_file = Greenphyl::File->new($dbh, {'selectors' => {'user_id' => 7, 'name' => 'tree.nwk'}});
    print $user_file->name();

=head1 REQUIRES

Perl5

=head1 EXPORTS

Nothing

=head1 DESCRIPTION

This module implements a GreenPhyl File database object.

=cut

package Greenphyl::File;

use strict;
use warnings;

use base qw(Greenphyl::CachedDBObject);

use vars qw($AUTOLOAD);

use Carp qw(cluck confess croak);




# Package constants
####################

=pod

=head1 CONSTANTS

B<$OBJECT_PROPERTIES>: (hash ref)

object properties description. See DBObject constructor documentation for
details.

=cut

our $OBJECT_PROPERTIES = {
    'table' => 'files',
    'key' => 'id',
    'alternate_keys' => [],
    'load'     => [
        'flags',
        'id',
        'name',
        'path',
        'size',
        'type',
        'url',
        'user_id',
    ],
    'alias' => {
    },
    'base' => {
        'flags' => {
            'property_column' => 'flags',
        },
        'id' => {
            'property_column' => 'id',
        },
        'name' => {
            'property_column' => 'name',
        },
        'path' => {
            'property_column' => 'path',
        },
        'size' => {
            'property_column' => 'size',
        },
        'type' => {
            'property_column' => 'type',
        },
        'url' => {
            'property_column' => 'url',
        },
        'user_id' => {
            'property_column' => 'user_id',
        },
    },
    'secondary' => {
        'user_id' => {
            'object_key'     => 'user_id',
            'property_table'  => 'users',
            'property_key'    => 'id',
            'multiple_values' => 0,
            'property_module' => 'Greenphyl::User',
        },
    },
    'tertiary' => {
    },
};




# Package subs
###############

=pod

=head1 STATIC METHODS

=head2 CONSTRUCTOR

B<Description>: Creates a new instance of a GreenPhyl File object.

B<ArgsCount>: 2

=over 4

=item $dbh: (DBI::db) (U)

Database handler of the database to load the object from.

=item $parameters: (hash ref) (R)

See Greenphyl::DBObject constructor documentation for details.

=back

B<Return>: (Greenphyl::File)

a new instance.

B<Caller>: General

B<Example>:

    my $user_file = Greenphyl::File->new($dbh, {'selectors' => {'user_id' => 7, 'name' => 'tree.nwk'}});

=cut

sub new
{
    my $proto = shift();
    my $class = ref($proto) || $proto;
    
    my $object_properties = $class->GetDefaultClassProperties();

    return $class->SUPER::new($object_properties, @_);
}


=pod

=head1 METHODS

=pod

=head2 getStringValue

B<Description>: returns Species code.

B<ArgsCount>: 1

=over 4

=item $self: (Greenphyl::Species)

the species object.

=back

B<Return>: (string)

the species code.

=cut

sub getStringValue
{
    my ($self) = @_;
    return $self->{'name'};
}


=pod

=head1 AUTHORS

Mathieu ROUARD (Bioversity), m.rouard@cgiar.org

Valentin GUIGNON (Bioversity), v.guignon@cgiar.org

Matthieu CONTE (Syngenta)

=head1 VERSION

Version 1.0.0

Date 10/02/2014

=head1 SEE ALSO

GreenPhyl documentation, DBObject documentation.

=cut

return 1; # package return
