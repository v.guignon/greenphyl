=pod

=head1 NAME

Greenphyl::DBXRefLinks - GreenPhyl DBXRefLinks Library

=head1 SYNOPSIS

    use Greenphyl::DBXRefLinks;

=head1 REQUIRES

Perl5

=head1 EXPORTS

Nothing

=head1 DESCRIPTION

This module contains config variables and procedures used to generate database
cross reference links.

=cut

package Greenphyl::DBXRefLinks;

use strict;
use warnings;

use Carp qw(cluck confess croak);




# Package constants
####################

=pod

=head1 CONSTANTS

B<$EXTERNAL_LINK_CATEGORIES>: (hash ref)

Hash of link categories with title. Keys are category codes, values are hash
with the following key-values:
'title' => the category title to display.

B<$EXTERNAL_LINK_CONFIG>: (hash ref)

Hash of external link configuration. Keys are external link codes as used in
%$SPECIES_EXTERNAL_LINK_CONFIG hash. Values are hash with the following
key-values:
'label'       => the label to display for the link;
'description' => the link (lon) description that can be shown as a link tooltip
                 (should not include HTML tags);
'function'    => a reference to the function to call to generate the link. The
                 function receives as second argument a sequence object to
                 generate a link for;
'category'    => a category code corresponding to a key of
                 %$EXTERNAL_LINK_CATEGORIES hash.

B<$SPECIES_EXTERNAL_LINK_CONFIG>: (hash ref)

Hash associating a species code to a list of external links to generate for that
species.
Keys are species codes (usualy 5-letter codes) and values are arrays of
%$EXTERNAL_LINK_CONFIG keys.

For default settings (ie. when species code is not in config), the key '' is
used.

B<$SPECIES_GBROWSE_LINK_CONFIG>: (hash ref)

Hash associating a species code to a GBrowse link configuration for that
species.
Keys are species codes (usualy 5-letter codes) and values are hash of
3 keys: 'url' which contains the URL to GBrowse to which the sequence name
can be appended, 'image' which is the same kind of link to the corresponding
GBrowse image of the gene and 'function' (optional) which may reformat the gene
name for GBrowse link. That function will recieve as argument the sequence
object and will return the gene name as a string.

=cut

our $EXTERNAL_LINK_CATEGORIES ={
    'gene_expression_db' =>
    {
        'title' => "Gene Expression databases",
    },
    'genome_db'     =>
    {
        'title' => "Genome databases",
    },
    'mutant_db'  =>
    {
        'title' => "Mutant databases",
    },
    'pathway_db' =>
    {
        'title' => "Metabolic Pathway databases",
    },
};

our $EXTERNAL_LINK_CONFIG =
{
    'swissprot' =>
    {
        'description' => 'Swiss-Prot',
        'label'       => 'UniProtKB-SwissProt',
        'function'    => \&GenerateSwissProtLink,
        'category'    => 'genome_db',
    },
    'ncbi' =>
    {
        'description' => 'NCBI',
        'label'       => 'NCBI',
        'function'    => \&GenerateNCBILink,
        'category'    => 'genome_db',
    },
    'genevestigator' =>
    {
        'description' => 'Genevestigator',
        'label'       => 'Genevestigator',
        'function'    => \&GenerateGenevestigatorUniprotLink,
        'category'    => 'gene_expression_db',
    },
    'genevestigator_tair' =>
    {
        'description' => 'Genevestigator',
        'label'       => 'Genevestigator',
        'function'    => \&GenerateGenevestigatorTAIRLink,
        'category'    => 'gene_expression_db',
    },
    'orygenesdb' =>
    {
        'description' => 'OryGenesDB',
        'label'       => 'OryGenesDB',
        'function'    => \&GenerateOryGenesDBLink,
        'category'    => 'genome_db',
    },
    'plantcyc' =>
    {
        'description' => 'PlantCyc',
        'label'       => 'PlantCyc',
        'function'    => \&GeneratePlantCycLink,
        'category'    => 'pathway_db',
    },
    'tair' =>
    {
        'description' => 'TAIR',
        'label'       => 'TAIR',
        'function'    => \&GenerateTAIRLink,
        'category'    => 'genome_db',
    },
    'catdb' =>
    {
        'description' => 'CATdb',
        'label'       => 'CATdb',
        'function'    => \&GenerateCATdbLink,
        'category'    => 'gene_expression_db',
    },
    'nascarrays' =>
    {
        'description' => 'NASCArrays',
        'label'       => 'NASCArrays',
        'function'    => \&GenerateNASCArraysLink,
        'category'    => 'gene_expression_db',
    },
    'eplant' =>
    {
        'description' => 'ePlant',
        'label'       => 'ePlant',
        'function'    => \&GenerateEPlantLink,
        'category'    => 'gene_expression_db',
    },
    'tdna' =>
    {
        'description' => 'T-DNA Express',
        'label'       => 'T-DNA Express',
        'function'    => \&GenerateTDNAExpressLink,
        'category'    => 'mutant_db',
    },
    'germplasm' =>
    {
        'description' => 'TAIR Germplasm',
        'label'       => 'TAIR',
        'function'    => \&GenerateTAIRGermplasmLink,
        'category'    => 'mutant_db',
    },
    'red' =>
    {
        'description' => 'Rice gene Expression Database II',
        'label'       => 'RED II',
        'function'    => \&GenerateRED2Link,
        'category'    => 'gene_expression_db',
    },
    'tigr' =>
    {
        'description' => 'MSU Rice Genome Annotation',
        'label'       => 'TIGR',
        'function'    => \&GenerateTIGRLink,
        'category'    => 'genome_db',
    },
    'maize' =>
    {
        'description' => 'Maize',
        'label'       => 'MaizeSequence',
        'function'    => \&GenerateMaizeSequenceLink,
        'category'    => 'genome_db',
    },
    'chlre' =>
    {
        'description' => 'Chlre',
        'label'       => 'JGI',
        'function'    => \&GenerateChlreLink,
        'category'    => 'genome_db',
    },
    'phytozome_papaya' =>
    {
        'description' => 'Phytozome Carica papaya',
        'label'       => 'Phytozome',
        'function'    => \&GeneratePhytozomePapayaLink,
        'category'    => 'genome_db',
    },
    'musacyc' =>
    {
        'description' => 'musacyc',
        'label'       => 'MusaCyc',
        'function'    => \&GenerateMusaCycLink,
        'category'    => 'pathway_db',
    },
    'gbrowse' =>
    {
        'description' => 'Genome Browser',
        'label'       => 'GBrowse',
        'function'    => \&GenerateGBrowseLink,
        'category'    => 'genome_db',
    },
    'piece' =>
    {
        'description' => 'Plant Intron Exon Comparison and Evolution database',
        'label'       => 'PIECE',
        'function'    => \&GeneratePieceLink,
        'category'    => 'genome_db',
    },
    'banana_hub' =>
    {
        'description' => 'Banana Genome Hub',
        'label'       => 'Banana Genome Hub',
        'function'    => \&GenerateBananaHubLink,
        'category'    => 'genome_db',
    },
   'coffee_hub' =>
    {   
        'description' => 'Coffee Genome Hub',
        'label'       => 'Coffee Genome Hub',
        'function'    => \&GenerateCoffeeHubLink,
        'category'    => 'genome_db',
    },

};

our $SPECIES_EXTERNAL_LINK_CONFIG = {
    ''      => ['swissprot', ],
    'ARATH' => ['swissprot', 'gbrowse',  'genevestigator_tair', 'orygenesdb',
                'plantcyc', 'tair', 'catdb', 'nascarrays', 'eplant', 'tdna',
                'germplasm', 'piece', ],
    'BRADI' => ['swissprot', 'gbrowse', 'piece', ],
    'CARPA' => ['swissprot', 'gbrowse',  'phytozome_papaya', ],
    'CHLRE' => ['swissprot', 'gbrowse',  'ncbi', 'chlre', ],
    'COFCA' => ['swissprot', 'gbrowse', 'coffee_hub', ],
    'CUCSA' => ['swissprot', 'gbrowse', ],
    'CYAME' => ['swissprot',],
    'GLYMA' => ['swissprot', 'gbrowse', ],
    'MALDO' => ['swissprot', 'gbrowse', ],
    'MANES' => ['swissprot', 'gbrowse', 'piece', ],
    'MEDTR' => ['swissprot', 'gbrowse', 'piece', ],
    'MUSAC' => ['swissprot', 'gbrowse', 'musacyc', 'banana_hub', ],
    'MUSBA' => ['swissprot', 'gbrowse', ],
    'ORYSA' => ['swissprot', 'gbrowse',  'genevestigator', 'orygenesdb',
                'plantcyc', 'red', 'tigr', 'piece', ],
    'OSTTA' => ['swissprot',],
    'PHODA' => ['swissprot',],
    'PHYPA' => ['swissprot', 'gbrowse', 'ncbi', ],
    'POPTR' => ['swissprot', 'gbrowse', 'piece', ],
    'RICCO' => ['swissprot', 'gbrowse', ],
    'SELMO' => ['swissprot', 'gbrowse', ],
    'SORBI' => ['swissprot', 'gbrowse', 'ncbi', 'piece',],
    'THECC' => ['swissprot', 'gbrowse', ],
    'VITVI' => ['swissprot', 'gbrowse', 'piece', ],
    'ZEAMA' => ['swissprot', 'gbrowse', 'genevestigator', 'maize', 'piece', ],
};

our $SPECIES_GBROWSE_LINK_CONFIG = {
    'ARATH' =>
        {
            'url'     => 'http://orygenesdb.cirad.fr/cgi-bin/gbrowse/odb_arabidopsis/?name=',
            'image'   => 'http://orygenesdb.cirad.fr/cgi-bin/gbrowse_img/odb_arabidopsis/?type=Locus+ProteinCoding;width=700;name=',
            'function' => \&ExtractLocusName,
        },
    'BRADI' =>
        {
            'url'     => 'http://gbrowse.brachypodium.org/cgi-bin/gbrowse/brachypodium-gbrowse/?name=',
            'image'   => 'http://gbrowse.brachypodium.org/cgi-bin/gbrowse_img/brachypodium-gbrowse/?type=TXN;width=700;name=',
            'function' => \&ExtractLocusName,
        },
    'CARPA' =>
        {
            # Phytozome
            'url'     => 'http://www.phytozome.net/cgi-bin/gbrowse/papaya/?name=',
            'image'   => 'http://www.phytozome.net/cgi-bin/gbrowse_img/papaya/?type=Transcripts;width=700;name=',
            'function' => \&ExtractLocusName,
        },
    'CHLRE' =>
        {
            # Phytozome
            'url'     => 'http://www.phytozome.net/cgi-bin/gbrowse/chlamy/?name=',
            'image'   => 'http://www.phytozome.net/cgi-bin/gbrowse_img/chlamy/?type=Transcripts;width=700;name=',
            'function' => \&ExtractLocusName,
        },
    'COFCA' =>
        {   
            'url'     => 'http://coffee-genome.org/cgi-bin/gbrowse/coffea_canephora/?name=',
            'image'   => 'http://coffee-genome.org/cgi-bin/gbrowse_img/coffea_canephora/?type=manual_curation_mRNA;width=700;name=',
            'function' => \&ProcessCOFCAGeneName,
        },
    'CUCSA' =>
        {
            # Phytozome
            'url'     => 'http://www.phytozome.net/cgi-bin/gbrowse/cucumber/?name=',
            'image'   => 'http://www.phytozome.net/cgi-bin/gbrowse_img/cucumber/?type=Transcripts;width=700;name=',
            'function' => \&ExtractLocusName,
        },
    #'CYAME' =>
    #    {
    #        n/a
    #    },
    'GLYMA' =>
        {
            # Phytozome
            'url'     => 'http://www.phytozome.net/cgi-bin/gbrowse/soybean/?name=',
            'image'   => 'http://www.phytozome.net/cgi-bin/gbrowse_img/soybean/?type=Transcripts;width=700;name=',
            'function' => \&ExtractLocusName,
        },
    'MALDO' =>
        {
            # http://www.rosaceae.org/gb/gbrowse/malus_x_domestica/
            'url'     => 'http://www.rosaceae.org/gb/gbrowse/malus_x_domestica/?name=',
            'image'   => 'http://www.rosaceae.org/gb/gbrowse_img/malus_x_domestica/?type=gene;width=700;name=',
            'function' => \&ExtractLocusName,
        },
    'MANES' =>
        {
            # Phytozome
            'url'     => 'http://www.phytozome.net/cgi-bin/gbrowse/cassava/?name=',
            'image'   => 'http://www.phytozome.net/cgi-bin/gbrowse_img/cassava/?type=Transcripts;width=700;name=',
            'function' => \&ExtractLocusName,
        },
    'MEDTR' =>
        {
            # Phytozome
            'url'     => 'http://www.phytozome.net/cgi-bin/gbrowse/medicago/?name=',
            'image'   => 'http://www.phytozome.net/cgi-bin/gbrowse_img/medicago/?type=Transcripts;width=700;name=',
            'function' => \&ExtractLocusName,
        },
    'MUSAC' =>
        {
            'url'     => 'http://banana-genome.cirad.fr/cgi-bin/gbrowse/musa_acuminata/?name=',
            'image'   => 'http://banana-genome.cirad.fr/cgi-bin/gbrowse_img/musa_acuminata/?type=manual_curation_mRNA_gaze;width=700;name=',
            'function' => \&ProcessMUSACGeneName,
        },
    'MUSBA' =>
        {
            'url'     => 'http://banana-genome.cirad.fr/cgi-bin/gbrowse/musa_balbisiana_pkw/?name=',
            'image'   => 'http://banana-genome.cirad.fr/cgi-bin/gbrowse_img/musa_balbisiana_pkw/?type=manual_curation_mRNA;width=700;name=',
            'function' => \&ProcessMUSACGeneName,
        },
    'ORYSA' =>
        {
            'url'     => 'http://orygenesdb.cirad.fr/cgi-bin/gbrowse/odb_japonica/?name=',
            'image'   => 'http://orygenesdb.cirad.fr/cgi-bin/gbrowse_img/odb_japonica/?type=Locus+ProteinCoding;width=700;name=',
            'function' => \&ExtractLocusName,
        },
    #'OSTTA' =>
    #    {
    #        n/a
    #    },
    #'PHODA' =>
    #    {
    #        n/a
    #    },
    'PHYPA' =>
        {
            # Phytozome
            'url'     => 'http://www.phytozome.net/cgi-bin/gbrowse/physcomitrella/?name=',
            'image'   => 'http://www.phytozome.net/cgi-bin/gbrowse_img/physcomitrella/?type=Transcripts;width=700;name=',
            'function' => \&ExtractLocusName,
        },
    'POPTR' =>
        {
            # Phytozome
            'url'     => 'http://www.phytozome.net/cgi-bin/gbrowse/poplar/?name=',
            'image'   => 'http://www.phytozome.net/cgi-bin/gbrowse_img/poplar/?type=Transcripts;width=700;name=',
            'function' => \&ExtractLocusName,
        },
    'RICCO' =>
        {
            # alternative: http://cassava.psc.riken.jp/gbrowse.pl?select=Rco
            # Phytozome
            'url'     => 'http://www.phytozome.net/cgi-bin/gbrowse/castorbean/?name=',
            'image'   => 'http://www.phytozome.net/cgi-bin/gbrowse_img/castorbean/?type=Transcripts;width=700;name=',
            'function' => \&ExtractLocusName,
        },
    'SELMO' =>
        {
            # Phytozome
            'url'     => 'http://www.phytozome.net/cgi-bin/gbrowse/selaginella/?name=',
            'image'   => 'http://www.phytozome.net/cgi-bin/gbrowse_img/selaginella/?type=Transcripts;width=700;name=',
            'function' => \&ExtractLocusName,
        },
    'SORBI' =>
        {
            'url'     => 'http://orygenesdb.cirad.fr/cgi-bin/gbrowse/odb_sorghum/?name=',
            'image'   => 'http://orygenesdb.cirad.fr/cgi-bin/gbrowse_img/odb_sorghum/?type=Locus+ProteinCoding;width=700;name=',
            'function' => \&ExtractLocusName,
        },
    'THECC' =>
        {
            # CIRAD
            'url'     => 'http://cocoagendb.cirad.fr/gbrowse/cgi-bin/gbrowse/theobroma/?name=',
            'image'   => 'http://gohelle.cirad.fr/gbrowse/cgi-bin/gbrowse_img/theobroma/?type=manual_curation_genes+manual_curation_mRNA;width=700;name=',
            'function' => \&ExtractLocusName,
        },
    'VITVI' =>
        {
            # URGI
            'url'      => 'http://urgi.versailles.inra.fr/gb2/gbrowse/vitis_12x_pub/?name=',
            'image'    => 'http://urgi.versailles.inra.fr/gb2/gbrowse_img/vitis_12x_pub/?type=vvitps+jigsawgaze_no_repeat;width=700;name=',
            'function' => \&ProcessVITVIGeneName,
        },
    'ZEAMA' =>
        {
            'url'     => 'http://orygenesdb.cirad.fr/cgi-bin/gbrowse/odb_maize/?name=',
            'image'   => 'http://orygenesdb.cirad.fr/cgi-bin/gbrowse_img/odb_maize/?type=Locus+ProteinCoding;width=700;name=',
            'function' => \&ExtractLocusName,
        },
};



# Package subs
###############

=pod

=head1 STATIC METHODS

=head2 Generate*Link

B<Description>: Generates an external database link (URL) for a given sequence.

Notes:
-Gene name can be obtained using "$sequence->accession". ex.: "At3g18524.1"
-Locus name can be obtained using "$sequence->locus". ex.: "At3g18524"

B<ArgsCount>: 1

=over 4

=item $sequence: (Greenphyl::Sequence)

a sequence object.

=back

B<Return>: (hash ref)

hash containing the keys 'url' and optionally 'label', 'description' and
'image_url'.

=cut

sub GenerateSwissProtLink
{
    my ($sequence) = @_;

    my $url = '';
    if ($sequence->uniprot && @{$sequence->uniprot})
    {
        $url = 'http://www.expasy.org/uniprot/' . $sequence->uniprot->[0]->accession;
    }
    return {'url' => $url, };
}


sub GenerateNCBILink
{
    my ($sequence) = @_;

    my $accession = $sequence->accession;

    if ($accession =~ /^Sb/i)
    {$accession =~ s/(\w+)\.\d+$/$1/;}

    if ($accession =~ /^Phypa/i)
    {$accession =~ s/Phypa_(\d+)$/PHYPADRAFT_$1/;}

    if ($accession =~ /^Chlre/i)
    {$accession =~ s/Chlre_(\d+)$/CHLREDRAFT_$1/;}

    if ($accession =~ /^Popal/i)
    {$accession =~ s/Popal_(\d+)$/POPTRDRAFT_$1/;}

    return {'url' => "http://www.ncbi.nlm.nih.gov/protein?Term=$accession", };
}


sub GenerateGenevestigatorUniprotLink
{
    my ($sequence) = @_;

    my $url = '';
    if ($sequence->uniprot && @{$sequence->uniprot})
    {
        $url = "https://www.genevestigator.com/gv/directlink.jsp?geneIDs=" . $sequence->uniprot->[0]->accession . "&amp;geneIDType=SwissPro";
    }
    return {'url' => $url, };
}


sub GenerateGenevestigatorTAIRLink
{
    my ($sequence) = @_;

    my $locus = $sequence->locus;

    return {'url' => "https://www.genevestigator.com/gv/directlink.jsp?geneIDs=" . $locus . "&amp;geneIDType=AGI", };
}


sub GenerateOryGenesDBLink
{
    my ($sequence) = @_;

    my $url;
    if ($sequence->accession =~ /^At/i)
    {
        $url = "http://orygenesdb.cirad.fr/cgi-bin/gbrowse/odb_arabidopsis/?name=" . $sequence->accession;
    }
    else
    {
        $url = "http://orygenesdb.cirad.fr/cgi-bin/gbrowse/odb_japonica/?name=" . $sequence->accession;
    }
    return {'url' => $url, };
}


sub GeneratePlantCycLink
{
    my ($sequence) = @_;

    my $url;
    if ($sequence->locus =~ /^At/i)
    {
        $url = "http://pmn.plantcyc.org/PLANT/NEW-IMAGE?type=GENE&amp;object=" . $sequence->locus;
    }
    elsif ($sequence->locus =~ /^Os/i)
    {
        $url = "http://pmn.plantcyc.org/PLANT/NEW-IMAGE?type=GENE&amp;object=LOC_" . $sequence->locus;
    }
    return {'url' => $url, };
}


sub GenerateTAIRLink
{
    my ($sequence) = @_;

    my $url;
    if ($sequence->accession =~ /^At/i)
    {
        $url = "http://www.arabidopsis.org/servlets/TairObject?type=gene&amp;name=" . $sequence->accession;
    }
    return {'url' => $url, };
}


sub GenerateCATdbLink
{
    my ($sequence) = @_;

    my $url;
    if ($sequence->accession =~ /^At/i)
    {
        $url = "http://urgv.evry.inra.fr/cgi-bin/projects/CATdb/cons_diff_id.pl?idnum=" . $sequence->locus;
    }
    return {'url' => $url, };
}


sub GenerateNASCArraysLink
{
    my ($sequence) = @_;

    my $url;
    if ($sequence->accession =~ /^At/i)
    {
        $url = "http://affymetrix.arabidopsis.info/narrays/geneswinger.pl?searchfor=AGI&amp;id=" . $sequence->locus;
    }
    return {'url' => $url, };
}


sub GenerateEPlantLink
{
    my ($sequence) = @_;
    return {'url' => "http://bar.utoronto.ca/eplant/#agi=" . $sequence->locus, };
}


sub GenerateTDNAExpressLink
{
    my ($sequence) = @_;

    my $url;
    if ($sequence->accession =~ /^At/i)
    {
        $url = "http://signal.salk.edu/cgi-bin/tdnaexpress?GENE=" . $sequence->locus;
    }
    return {'url' => $url, };
}


sub GenerateTAIRGermplasmLink
{
    my ($sequence) = @_;

    my $url;
    if ($sequence->accession =~ /^At/i)
    {
        $url = "http://www.arabidopsis.org/servlets/Search?type=germplasm&amp;search_action=search&amp;name_type_1=gene_name&amp;method_1=4&amp;name_1=" . $sequence->accession;
    }
    return {'url' => $url, };
}


sub GenerateRED2Link
{
    my ($sequence) = @_;

    my $url;
    if ($sequence->accession =~ /^Os/i)
    {
        $url = "http://cdna01.dna.affrc.go.jp/cgi-bin/cDNA/EXPRESSION/gene_list.cgi?locus_id=LOC_" . $sequence->locus;
    }
    return {'url' => $url, };
}


sub GenerateTIGRLink
{
    my ($sequence) = @_;

    my $url;
    if ($sequence->accession =~ /^Os/i)
    {
        $url = "http://rice.plantbiology.msu.edu/cgi-bin/gbrowse/rice/?name=LOC_" . $sequence->accession;
    }
    return {'url' => $url, };
}


sub GenerateMaizeSequenceLink
{
    my ($sequence) = @_;

    my $url;
    my $accession = $sequence->accession;
    $accession =~ s/\_P0\d{1}//g;

    $url = "http://www.maizesequence.org/Zea_mays/Gene/Summary?db=core;g=" . $accession;
    return {'url' => $url, };
}


sub GenerateChlreLink
{
    my ($sequence) = @_;
    my $chlre_id = ($sequence->accession =~ /Chlre_(\d+)/);
    return {'url' => "http://genome.jgi-psf.org/cgi-bin/dispGeneModel?db=Chlre4&amp;id=" . $chlre_id, };
}


sub GeneratePhytozomePapayaLink
{
    my ($sequence) = @_;
    return {'url' => "http://www.phytozome.net/cgi-bin/gbrowse/papaya/?Search=Search&amp;name=" . $sequence->accession, };
}


sub GenerateMusaCycLink
{
    my ($sequence) = @_;
    my $accession = $sequence->accession;
    $accession =~ s/[TG](\d)/P$1/g;
    return {'url' => "http://banana-genome.cirad.fr/musacyc_report?" . $accession, };
}

sub GeneratePieceLink
{
    my ($sequence) = @_;
    
    my $url;
    if ($sequence->accession =~ /^Os/i)
    {
        $url = "http://wheat.pw.usda.gov/piece/geneStructureSelection.php?gene_id=LOC_" . $sequence->accession;
    }
    else
    {
        $url  = "http://wheat.pw.usda.gov/piece/geneStructureSelection.php?gene_id=" . $sequence->accession;
    }

    return {'url' => $url, };
}

sub GenerateCoffeeHubLink
{
    my ($sequence) = @_;

    my $url;
    my $accession = $sequence->accession;
    $accession =~ s/p(\d+)/_g$1/;
   $accession =~ s/\.\d+$//;
    $url = "http://www.coffee-genome.org/$accession";

    return {'url' => $url, };
}

sub GenerateBananaHubLink
{
    my ($sequence) = @_;
    
    my $url;
    my $accession = $sequence->accession;
    $accession =~ s/GSMUA_(\w+\d+)T/GSMUA_$1P/;
    $url = "http://banana-genome.cirad.fr/$accession";

    return {'url' => $url, };
}


### ADD NEW LINK GENERATION FUNCTIONS ABOVE THIS COMMENT ###

sub GenerateGBrowseLink
{
    my ($sequence) = @_;

    my ($gbrowse_link, $gbrowse_image_link, $gbrowse_link_label);
    if (exists($SPECIES_GBROWSE_LINK_CONFIG->{$sequence->species->code}))
    {
        my $gene_name = $sequence->name;
        # check if the gene name should be reprocessed
        if (exists($SPECIES_GBROWSE_LINK_CONFIG->{$sequence->species->code}->{'function'}))
        {
            if (!defined(&{$SPECIES_GBROWSE_LINK_CONFIG->{$sequence->species->code}->{'function'}}))
            {
                confess "ERROR: DBXRefLinks:: invalid \$SPECIES_GBROWSE_LINK_CONFIG configuration for species '" . $sequence->species->code . "'! Specified function does not exist!\n";
            }
            $gene_name = $SPECIES_GBROWSE_LINK_CONFIG->{$sequence->species->code}->{'function'}->($sequence);
        }

        $gbrowse_link = $SPECIES_GBROWSE_LINK_CONFIG->{$sequence->species->code}->{'url'} . $gene_name;
        $gbrowse_image_link = $SPECIES_GBROWSE_LINK_CONFIG->{$sequence->species->code}->{'image'} . $gene_name;
        
        # make sure we got valid links
        if ($gbrowse_link !~ m/^http/)
        {
            $gbrowse_link = undef;
        }
        if ($gbrowse_image_link !~ m/^http/)
        {
            $gbrowse_image_link = undef;
        }

        # check if we specified a label in the config
        if (exists($SPECIES_GBROWSE_LINK_CONFIG->{$sequence->species->code}->{'label'}))
        {
            $gbrowse_link_label = $SPECIES_GBROWSE_LINK_CONFIG->{$sequence->species->code}->{'label'}
        }
    }
    return {'url' => $gbrowse_link, 'image_url' => $gbrowse_image_link, 'label' => $gbrowse_link_label,};
}


=pod

=head2 Process*GeneName, Extract*

B<Description>: process a given sequence name to generate the corresponding
gene name for a given species GBrowse.

B<ArgsCount>: 1

=over 4

=item $sequence: (Greenphyl::Sequence)

a sequence object.

=back

B<Return>: (string)

The new gene name to use for GBrowse linking.

=cut

sub ExtractLocusName
{
    my ($sequence) = @_;
	return $sequence->locus;

}

sub ProcessCOFCAGeneName
{
    my ($sequence) = @_;
    my $gene_name = $sequence->locus;
    return "mrna:$gene_name";

}


sub ProcessMUSACGeneName
{
    my ($sequence) = @_;
    my $gene_name = $sequence->accession;
    $gene_name =~ s/T(\d+)/P$1/g; # trick to avoid multiple name hits on gbrowse
	return $gene_name;

}


sub ProcessVITVIGeneName
{
    my ($sequence) = @_;
    my $gene_name = $sequence->locus;
    $gene_name =~ s/GSVIVP00/GSVIVG01/;
    return $gene_name;
}


=pod

=head2 GetExternalLinks

B<Description>: Generates an external link structure for a given sequence.

B<ArgsCount>: 1

=over 4

=item $sequence: (Greenphyl::Sequence)

a sequence object.

=back

B<Return>: (hash ref)

The external link structure. Each key is a link category type and each value is
a category hash with 2 keys:
-'label': the category title;
-'links': an array of of link data hash which have the keys:
   -'label': link label to display;
   -'description': description corresponding to the link;
   -'url': URL of the link.

=cut

sub GetExternalLinks
{
    my ($sequence) = @_;

    if (!$sequence || (!$sequence->isa('Greenphyl::AbstractSequence')))
    {
        confess "ERROR: DBXRefLinks::GetExternalLinks: no valid sequence provided!\n";
    }

    my $external_links = {};

    my $species_code = $sequence->species->code;
    # check if we should use default values (no config entry for given species)
    if (!exists($SPECIES_EXTERNAL_LINK_CONFIG->{$species_code}))
    {
        # species not in config, use default
        $species_code = '';
    }

    foreach my $link_type (@{$SPECIES_EXTERNAL_LINK_CONFIG->{$species_code}})
    {
        if (!exists($EXTERNAL_LINK_CONFIG->{$link_type}))
        {
            confess "ERROR: DBXRefLinks:: invalid \$SPECIES_EXTERNAL_LINK_CONFIG configuration for '" . $sequence->species->code . "'! '$link_type' is not a valid link type as it's not defined in \$EXTERNAL_LINK_CONFIG!\n";
        }

        my $link_config = $EXTERNAL_LINK_CONFIG->{$link_type};
        if (!exists($link_config->{'category'})
            || !exists($link_config->{'label'})
            || !exists($link_config->{'description'})
            || !exists($link_config->{'function'}))
        {
            confess "ERROR: DBXRefLinks:: invalid \$EXTERNAL_LINK_CONFIG configuration for link type '$link_type'! Make sure all keys (category, label, description and function) are defined defined!\n";
        }
        if (ref($link_config->{'function'}) ne 'CODE')
        {
            confess "ERROR: DBXRefLinks:: invalid \$EXTERNAL_LINK_CONFIG configuration for link type '$link_type'! 'function' key does not contain a function reference!\n";
        }
        if (!defined(&{$link_config->{'function'}}))
        {
            confess "ERROR: DBXRefLinks:: invalid \$EXTERNAL_LINK_CONFIG configuration for link type '$link_type'! Specified function does not exist!\n";
        }

        my $category = $link_config->{'category'};
        if (!exists($external_links->{$category}))
        {
            $external_links->{$category} = {};
            $external_links->{$category}->{'label'} = $EXTERNAL_LINK_CATEGORIES->{$category}->{'title'};
            $external_links->{$category}->{'links'} = [];
        }

        # call link function to generate link data from current sequence
        my $link_data = $link_config->{'function'}->($sequence);
        my ($url, $label, $description) = ($link_data->{'url'}, $link_data->{'label'}, $link_data->{'description'});
        $label ||= $link_config->{'label'}; # use default label if not specified by function
        $description ||= $link_config->{'description'}; # use default description if not specified by function

        if ($url)
        {
            push(@{$external_links->{$category}->{'links'}},
                {
                    'label'       => $label,
                    'description' => $description,
                    'url'         => $url,
                },
            );
        }
    }

    return $external_links;
}




=pod

=head1 AUTHORS

Mathieu ROUARD (Bioversity), m.rouard@cgiar.org

Valentin GUIGNON (Bioversity), v.guignon@cgiar.org

Matthieu CONTE (Syngenta)

=head1 VERSION

Version 1.0.0

Date 11/09/2014

=head1 SEE ALSO

GreenPhyl documentation, DBObject documentation.

=cut

return 1; # package return
