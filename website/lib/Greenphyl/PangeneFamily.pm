=pod

=head1 NAME

Greenphyl::PangeneFamily - GreenPhyl Pangene Family Object

=head1 SYNOPSIS

    use Greenphyl::PangeneFamily;
    my $family = Greenphyl::PangeneFamily->new($dbh, {'selectors' => {'id' => 205}});
    print $family->name();

=head1 REQUIRES

Perl5

=head1 EXPORTS

Nothing

=head1 DESCRIPTION

This module implements a GreenPhyl family database object for pangene families.

=cut

package Greenphyl::PangeneFamily;

use strict;
use warnings;

use base qw(Greenphyl::Family);

use vars qw($AUTOLOAD);

use Carp qw(cluck confess croak);

use Greenphyl::Config;
use Greenphyl;
use Greenphyl::Sequence;
use Greenphyl::Genome;


# Package constants
####################

=pod

=head1 CONSTANTS

B<$OBJECT_PROPERTIES>: (hash ref)

sequence object properties description. See DBObject constructor documentation
for details.

=cut

our $DEBUG = 0;


# Package subs
###############

=pod

=head1 STATIC METHODS

=head2 CONSTRUCTOR

B<Description>: Creates a new instance of a GreenPhyl pangene family object.

B<ArgsCount>: 2

=over 4

=item $dbh: (DBI::db) (U)

Database handler of the database to load the object from.

=item $parameters: (hash ref) (R)

See Greenphyl::DBObject constructor documentation for details.

=back

B<Return>: (Greenphyl::PangeneFamily)

a new instance.

B<Caller>: General

B<Example>:

    my $family = new Greenphyl::PangeneFamily($dbh, {'selectors' => {'id' => 205, 'validated' => ['<>', 0]}});

=cut

sub new
{
    my ($proto) = shift();
    my $class = ref($proto) || $proto;

    my $self = $class->SUPER::new(@_);

    if (($self->type ne 'core')
        && ($self->type ne 'soft-core')
        && ($self->type ne 'dispensable')
        && ($self->type ne 'specific'))
    {
        PrintDebug("ERROR: Tried to instanciate a pangene family with a non pangene family ($self, id=" . $self->id . ', type=' . $self->type . ')');
        return undef;
    }

    return $self;
}

=pod

=head1 METHODS

=head2 getPangeneStructure

B<Description>: returns a pangene structure corresponding to this family.

B<ArgsCount>: 1

=over 4

=item $self: (Greenphyl::PangeneFamily) (R)

Current pangene family.

=back

B<Return>: (hash ref)

A pangene structure.

B<Example>:

    $pangene_structure = $family->getPangeneStructure();
    # Provides something like that:
    {
        'genome' => Greenphyl::Genome, # 'MUSAC'
        'as' => 'consensus',
        'consensus' => Greenphyl::Sequence, # 'musac_pan_p000019'
        'genomes' => {
            'MUSAC_zebrina_v1' => {
                'genome' => Greenphyl::Genome, # 'MUSAC_zebrina_v1'
                'as' => 'participating',
                'participating' => Greenphyl::Sequence, # 'Mazeb_scaffold684_t000110'
            },
            'MUSAC_malacensis_v2' => {
                'genome' => Greenphyl::Genome, # 'MUSAC_malacensis_v2'
                'as' => 'representative',
                'representative' => Greenphyl::Sequence, # 'Ma05_t29790.3'
                'participating' => [
                    Greenphyl::Sequence, # 'Ma05_t29790.1'
                    Greenphyl::Sequence, # 'Ma05_t29790.2'
                ],
            },
            ...
        },
    };

=cut

sub getPangeneStructure
{
    my ($self) = @_;

    if (exists($self->{'_pangene_structure'}))
    {
        return $self->{'_pangene_structure'};
    }

    my $sql_query = "
        SELECT " . join(', ', Greenphyl::Sequence->GetDefaultMembers('s', 'sequence_')) . ",
            fs.type AS \"sequence_pan_relationship\",
            " . join(', ', Greenphyl::Genome->GetDefaultMembers('g', 'genome_')) . "
        FROM families_sequences fs
            JOIN sequences s ON (fs.sequence_id = s.id)
            JOIN genomes g ON g.id = s.genome_id
        WHERE
            fs.family_id = ?
    ";
    PrintDebug("SQL QUERY: $sql_query\nBindings: " . $self->id);

    my $sequence_data_array = $self->{'_dbh'}->selectall_arrayref($sql_query, { 'Slice' => {} }, $self->id);

    my $by_genomes = {};
    my $by_sequences = {};
    my $pangenome;

    foreach my $sequence_data (@$sequence_data_array)
    {
        my $sequence_hash = Greenphyl::Sequence->ExtractMembersFromHash($sequence_data, 'sequence_');
        my $sequence = Greenphyl::Sequence->new($self->{'_dbh'}, {'load' => 'none', 'members' => $sequence_hash});
        my $genome_hash = Greenphyl::Genome->ExtractMembersFromHash($sequence_data, 'genome_');
        my $genome = Greenphyl::Genome->new($self->{'_dbh'}, {'load' => 'none', 'members' => $genome_hash});
        if ($genome->representative())
        {
            $pangenome = $genome;
        }

        $by_genomes->{$genome} ||= {'genome' => $genome};
        my $relationship = $sequence_data->{'sequence_pan_relationship'};
        $by_genomes->{$genome}->{$relationship} ||= [];
        push(@{$by_genomes->{$genome}->{$relationship}}, $sequence);
        
        $by_sequences->{$sequence->id} = $sequence;
    }

    my $pangene_structure = {};
    if ($pangenome)
    {
        if (exists($by_genomes->{$pangenome}->{'representative'}))
        {
            if (exists($by_genomes->{$pangenome}->{'consensus'}))
            {
                PrintDebug('Warning: both a representative and a consensus sequence for family ' . $self->id . ' and representative genome ' . $pangenome);
            }
            if (1 < @{$by_genomes->{$pangenome}->{'representative'}})
            {
                PrintDebug('Warning: more than one representative sequence found for family ' . $self->id . ' and representative genome ' . $pangenome);
            }
            $pangene_structure = {
                'genome' => $pangenome,
                'as' => 'representative',
                'representative' => $by_genomes->{$pangenome}->{'representative'}->[0],
                'genomes' => {},
            };
        }
        elsif (exists($by_genomes->{$pangenome}->{'consensus'}))
        {
            if (1 < @{$by_genomes->{$pangenome}->{'consensus'}})
            {
                PrintDebug('Warning: more than one consensus sequence found for family ' . $self->id . ' and representative genome ' . $pangenome);
            }
            $pangene_structure = {
                'genome' => $pangenome,
                'as' => 'consensus',
                'consensus' => $by_genomes->{$pangenome}->{'consensus'}->[0],
                'genomes' => {},
            };
        }
        else
        {
            PrintDebug('Warning: no main sequence found for family ' . $self->id . ' and representative genome ' . $pangenome);
        }

        foreach my $genome (keys(%{$by_genomes}))
        {
            next if ($genome eq ''.$pangenome);

            if (exists($by_genomes->{$genome}->{'representative'}))
            {
                $pangene_structure->{'genomes'}->{$genome} = {
                    'genome' => $by_genomes->{$genome}->{'genom'},                    'as' => 'representative',
                    'representative' => $by_genomes->{$genome}->{'representative'}->[0],
                    'participating' => $by_genomes->{$genome}->{'participating'},
                };
            }
            elsif (exists($by_genomes->{$genome}->{'consensus'}))
            {
                $pangene_structure->{'genomes'}->{$genome} = {
                    'genome' => $by_genomes->{$genome}->{'genom'},
                    'as' => 'consensus',
                    'consensus' => $by_genomes->{$genome}->{'consensus'}->[0],
                    'participating' => $by_genomes->{$genome}->{'participating'},
                };
            }
            else
            {
                $pangene_structure->{'genomes'}->{$genome} = {
                    'genome' => $by_genomes->{$genome}->{'genom'},
                    'as' => 'participating',
                    'participating' => $by_genomes->{$genome}->{'participating'}->[0],
                };
            }
           
        }
    }

    $self->{'_sequences'} = $by_sequences;
    $self->{'_pangene_structure'} = $pangene_structure;

    return $self->{'_pangene_structure'};
}


=pod

=head2 hasSequence

B<Description>: tells if the pangene contains the given sequence.

B<ArgsCount>: 2

=over 4

=item $self: (Greenphyl::PangeneFamily) (R)

Current pangene family.

=item $self: (Greenphyl::Sequence) (R)

The sequence to check.

=back

B<Return>: (boolean)

True if the sequence is part of the pangene.

=cut

sub hasSequence
{
    my ($self, $sequence) = @_;

    my $pangene_structure = $self->getPangeneStructure() or return 0;

    if (exists($self->{'_sequences'}->{$sequence->id}))
    {
        return 1;
    }
   
    return 0;
}


=pod

=head2 getPangenome

B<Description>: returns the pangenome.

B<ArgsCount>: 1

=over 4

=item $self: (Greenphyl::PangeneFamily) (R)

Current pangene family.

=back

B<Return>: (Greenphyl::Genome)

The pangene genome.

B<Example>:

    $pan_genome = $family->getPangenome();

=cut

sub getPangenome
{
    my ($self) = @_;
    my $pangene_structure = $self->getPangeneStructure() or return undef;
    
    return $pangene_structure->{'genome'};
}


=pod

=head2 getPangene

B<Description>: returns the consensus or representative sequence for the
pangene.

B<ArgsCount>: 1

=over 4

=item $self: (Greenphyl::PangeneFamily) (R)

Current pangene family.

=back

B<Return>: (Greenphyl::Sequence)

The pangene sequence.

B<Example>:

    $sequence = $family->getPanSequence();

=cut

sub getPangene
{
    my ($self) = @_;
    my $pangene_structure = $self->getPangeneStructure() or return undef;
    
    my $as = $pangene_structure->{'as'};
    return $pangene_structure->{$as};
}


=pod

=head2 getPangeneRole

B<Description>: returns the role (consensus or representative) of the sequence
for the pangene.

B<ArgsCount>: 1

=over 4

=item $self: (Greenphyl::PangeneFamily) (R)

Current pangene family.

=back

B<Return>: (string)

The pangene sequence role.

=cut

sub getPangeneRole
{
    my ($self) = @_;
    my $pangene_structure = $self->getPangeneStructure() or return undef;
    
    return $pangene_structure->{'as'};
}


=pod

=head2 getRepresentativeSequenceFor

B<Description>: returns the array of sequence represented by the given sequence.

B<ArgsCount>: 2

=over 4

=item $self: (Greenphyl::PangeneFamily) (R)

Current pangene family.

=item $sequence: (Greenphyl::Sequence) (R)

A sequence of the pangene family.

=back

B<Return>: (list of Greenphyl::Sequence objects)

The represented sequences.

B<Example>:

    my $main_sequence = $family->getRepresentativeSequenceFor($sequence);

=cut

sub getRepresentativeSequenceFor
{
    my ($self, $sequence) = @_;

    my $pangene_structure = $self->getPangeneStructure() or return undef;

    # Check if the sequence is part of the pangene family.
    if (!$self->hasSequence($sequence))
    {
        return undef;
    }

    # Check if it is the pangene sequence.
    if ($sequence->id == $pangene_structure->{$pangene_structure->{'as'}}->id)
    {
        # The sequence is the representative and is not represented.
        return undef;
    }

    my $representative_sequence;
    my $as = $pangene_structure->{'genomes'}->{$sequence->genome}->{'as'};
    # If genome is participating, its sequence is represented by the pangene consensus.
    if ($as eq 'participating')
    {
        $representative_sequence = $pangene_structure->{$pangene_structure->{'as'}};
    }
    else
    {
        # Make sure given sequence is not a representative or a consensus.
        # nb.: in current implementation, consensus case should not appear.
        if ($sequence->id != $pangene_structure->{'genomes'}->{$sequence->genome}->{$as}->id)
        {
            # It's not a consensus or a representative, return representative.
            $representative_sequence = $pangene_structure->{'genomes'}->{$sequence->genome}->{$as};
        }
        else
        {
            # It's a consensus or a representative, return pangene sequence.
            $representative_sequence = $pangene_structure->{$pangene_structure->{'as'}};
        }
    }
    
    return $representative_sequence;
}


=pod

=head2 getRepresentedSequencesFor

B<Description>: returns the array of sequence represented by the given sequence.

B<ArgsCount>: 2

=over 4

=item $self: (Greenphyl::PangeneFamily) (R)

Current pangene family.

=item $sequence: (Greenphyl::Sequence) (R)

A sequence of the pangene family.

=back

B<Return>: (array ref of Greenphyl::Sequence objects)

The represented sequences.

B<Example>:

    $sequences = $family->getRepresentedSequencesFor($sequence);

=cut

sub getRepresentedSequencesFor
{
    my ($self, $sequence) = @_;

    my $pangene_structure = $self->getPangeneStructure() or return undef;

    # Check if the sequence is part of the pangene family.
    if (!$self->hasSequence($sequence))
    {
        return undef;
    }

    # Check if it is the pangene sequence.
    if ($sequence->id == $pangene_structure->{$pangene_structure->{'as'}}->id)
    {
        # Return all the sequences.
        return
            [
                sort {
                    ($a->genome->name cmp $b->genome->name)
                    || ($a->accession cmp $b->accession)
                }
                grep { $_->id != $sequence->id }
                values(%{$self->{'_sequences'}})
            ];
    }

    my @represented_sequences;
    my $as = $pangene_structure->{'genomes'}->{$sequence->genome}->{'as'};
    # Representative sequence are not participating.
    if ($as ne 'participating')
    {
        # If we have a consensus or a representative, returns its participating sequences.
        if ((exists($pangene_structure->{'genomes'}->{$sequence->genome}->{'consensus'})
              && ($pangene_structure->{'genomes'}->{$sequence->genome}->{'consensus'}->id == $sequence->id))
            || (exists($pangene_structure->{'genomes'}->{$sequence->genome}->{'representative'})
              && ($pangene_structure->{'genomes'}->{$sequence->genome}->{'representative'}->id == $sequence->id)))
        {
            push(
                @represented_sequences,
                $pangene_structure->{'genomes'}->{$sequence->genome}->{$as},
                @{$pangene_structure->{'genomes'}->{$sequence->genome}->{'participating'}}
            );
        }
    }
    
    return [sort {$a->accession cmp $b->accession} @represented_sequences];
}


=pod

=head2 getRepresentedSequenceStructureFor

B<Description>: returns a structure of sequence represented by the given
sequence.

B<ArgsCount>: 2

=over 4

=item $self: (Greenphyl::PangeneFamily) (R)

Current pangene family.

=item $sequence: (Greenphyl::Sequence) (R)

A sequence of the pangene family.

=back

B<Return>: (hash ref)

A pangene structure.

B<Example>:

    my $represented_sequence_structure = $sequence->getRepresentedSequenceStructure();
    # Provides something like that for pan proteins:
    {
        'genome' => Greenphyl::Genome, # 'MUSAC'
        'as' => 'consensus',
        'consensus' => Greenphyl::Sequence, # 'musac_pan_p000019'
        'genomes' => {
            'MUSAC_zebrina_v1' => {
                'genome' => Greenphyl::Genome, # 'MUSAC_zebrina_v1'
                'as' => 'participating',
                'participating' => Greenphyl::Sequence, # 'Mazeb_scaffold684_t000110'
            },
            'MUSAC_malacensis_v2' => {
                'genome' => Greenphyl::Genome, # 'MUSAC_malacensis_v2'
                'as' => 'representative',
                'representative' => Greenphyl::Sequence, # 'Ma05_t29790.3'
                'participating' => [
                    Greenphyl::Sequence, # 'Ma05_t29790.1'
                    Greenphyl::Sequence, # 'Ma05_t29790.2'
                ],
            },
            ...
        },
    };

    # Provides something like that for representative proteins of genomes:
    {
        'genome' => Greenphyl::Genome, # 'MUSAC_malacensis_v2'
        'as' => 'representative',
        'representative' => Greenphyl::Sequence, # 'Ma05_t29790.3'
        'participating' => [
            Greenphyl::Sequence, # 'Ma05_t29790.1'
            Greenphyl::Sequence, # 'Ma05_t29790.2'
        ],
    };

=cut

sub getRepresentedSequenceStructureFor
{
    my ($self, $sequence) = @_;

    my $pangene_structure = $self->getPangeneStructure() or return undef;

    # Check if the sequence is part of the pangene family.
    if (!$self->hasSequence($sequence))
    {
        return undef;
    }

    # Check if it is the pangene sequence.
    if ($sequence->id == $pangene_structure->{$pangene_structure->{'as'}}->id)
    {
        # Return the whole structure.
        return $pangene_structure;
    }

    my $as = $pangene_structure->{'genomes'}->{$sequence->genome}->{'as'};
    # Representative sequence are not participating.
    if ($as ne 'participating')
    {
        # If we have a consensus or a representative, returns its participating sequences.
        if ((exists($pangene_structure->{'genomes'}->{$sequence->genome}->{'consensus'})
              && ($pangene_structure->{'genomes'}->{$sequence->genome}->{'consensus'}->id == $sequence->id))
            || (exists($pangene_structure->{'genomes'}->{$sequence->genome}->{'representative'})
              && ($pangene_structure->{'genomes'}->{$sequence->genome}->{'representative'}->id == $sequence->id)))
        {
            return $pangene_structure->{'genomes'}->{$sequence->genome};
        }
    }
    
    return undef;
}


=pod

=head2 getSequenceRole

B<Description>: returns the array of sequence represented by the given sequence.

B<ArgsCount>: 2

=over 4

=item $self: (Greenphyl::PangeneFamily) (R)

Current pangene family.

=item $sequence: (Greenphyl::Sequence) (R)

A sequence of the pangene family.

=back

B<Return>: (list of Greenphyl::Sequence objects)

The represented sequences.

B<Example>:

    $role = $family->getSequenceRole($sequence);

=cut

sub getSequenceRole
{
    my ($self, $sequence) = @_;

    my $pangene_structure = $self->getPangeneStructure() or return undef;
    
    # Check if the sequence is part of the pangene family.
    if (!$self->hasSequence($sequence))
    {
        return undef;
    }

    if ($sequence->id == $pangene_structure->{$pangene_structure->{'as'}}->id)
    {
        return $pangene_structure->{'as'};
    }
    
    my $as = $pangene_structure->{$sequence->genome}->{'as'};
    if ($sequence->id == $pangene_structure->{$sequence->genome}->{$as}->id)
    {
      return $as;
    }

    return 'participating';
}

=pod

=head1 DIAGNOSTICS

=head1 AUTHORS

Mathieu ROUARD (Bioversity), m.rouard@cgiar.org

Valentin GUIGNON (Bioversity), v.guignon@cgiar.org

Matthieu CONTE (Syngenta)

=head1 VERSION

Version 3.0.0

Date 06/11/2019

=head1 SEE ALSO

GreenPhyl documentation, DBObject documentation.

=cut

return 1; # package return
