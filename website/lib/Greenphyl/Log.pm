=pod

=head1 NAME

Log - Handles GreenPhyl logs

=head1 SYNOPSIS

    # EXAMPLE 1 (log to both screen and DB)
    use Greenphyl::Log;
    ...
    LogInfo('My log message!');
    LogVerboseInfo('My subsidiary log message!');
    LogWarning('My warning log message!');
    confess LogError('My error log message!');
    LogDebug('My debug log message!');

    # EXAMPLE 2 (log into a file)
    use Greenphyl::Log('./file.log');

    # EXAMPLE 3 (log with more options)
    # or log into a file with options:
    use Greenphyl::Log(
        'logfile'         => 'file.log',
        'logpath'         => '.',
        'nodb'            => 1,
        'nofiletimestamp' => 1,
        'append'          => 0,
        'logtimestamp'    => 0,
    );

    # EXAMPLE 4 (only log into DB)
    use Greenphyl::Log('screen' => 0,);

    # EXAMPLE 5 (log to both screen and file)
    use Greenphyl::Log('screen' => 1, 'logfile' => 'file.log', 'nodb' => 1,);

    # EXAMPLE 6 (inside a script + command line call)
    # script test.pl
    use Greenphyl::Log;
    ...
    GetOptions("help|?"     => \$help,
               "man"        => \$man,
               "debug:s"    => \$debug,
               ...
               @Greenphyl::Log::LOG_GETOPT,
    ) or pod2usage(1);
    ...

    # please note that any of the module parameter can also be used in command
    # line using the prefix '-log-' at the end of the regular command line to
    # avoid conflicts. In that cas, they will replace the default log behavior.

    # The above script can be called using any of the following command lines:
    # -logs to DB + also display log messages (default)
    test.pl -so_script_specific_parameters 1 2 3 -some_other

    # -DB without messages on screen
    test.pl -so_script_specific_parameters 1 2 3 -some_other -log-noscreen

    # -display log messages and log into a file
    test.pl -so_script_specific_parameters 1 2 3 -some_other -log-logfile=test.log -log-logpath=.
    or (more simple)
    test.pl -so_script_specific_parameters 1 2 3 -some_other -log-logfile=./test.log

    # -display log messages and don't log anything (in database nor in file)
    test.pl -so_script_specific_parameters 1 2 3 -some_other -log-screenonly

    # EXAMPLE 7 (inside a script + command line call)
    # script test2.pl
    use Greenphyl::Log('nolog' => 1); # disable logging by default
    ...

    # -by default no logging is done. To enable logging, just add '-log' (and
    #  any other log parameter as needed).
    test.pl -so_script_specific_parameters 1 2 3 -some_other -log

=head1 REQUIRES

Perl5

=head1 EXPORTS

LogInfo, LogWarning, LogError, LogDebug, OpenLog, GetLogs, GetLastLogs,
GetLastLogPID, GetLastLogID, RemoveLogs

=head1 DESCRIPTION

Please refer to OpenLog documentation section for supported command line
arguments.

This module provides function used to log in database or in a file. By default,
the module will use GreenPhyl database for logging but if logging to the
database fails, it will log into a file and if it fails again, it will output
errors to STDERR.

It is possible to force logging to a file and specify path instead of using
Greenphyl config parameters. To do so, use module parameters as described in the
'OpenLog' documentation section.

Also note that any of the module parameter can be "overridden by"/"or used in"
command line using the prefix '-log-' for each parameter and they won't interfer
with other command line arguments.
Ex.: test.pl -some_parameter value -log-screenonly

Some parameters may require a value, like for 'logfile'. The value must be
specified using the '=' signe.
Ex.: test.pl -some_parameter value -log-logfile=test.log -log-logpath=.

If you use 'GetOptions', then add as last argument '@Greenphyl::Log::LOG_GETOPT'
like it is shown in EXAMPLE 6 above.

=cut

package Greenphyl::Log;

use strict;
use warnings;

use lib '../lib';
use lib '../local_lib';

use Carp qw (cluck confess croak);
use Fatal qw/:void open close/;

use Greenphyl;

use base qw(Greenphyl::CachedDBObject Greenphyl::DumpableObject Greenphyl::DumpableTabularData);

use vars qw($AUTOLOAD);




# Package constants
####################

=pod

=head1 CONSTANTS

B<$DEBUG>: (boolean)

If set to non-zero, debug messages will be displayed.

B<$LOG_TABLE>: (string)

Name of the database table to use for logs.

B<$LOG_TYPE_*>: (string)

Type of log messages.

B<$LOG_LABEL_*>: (string)

Labels corresponding to each type of log messages.

B<$LOG_FILE_NAME_DEFAULT>: (string)

Default log file name (without extension) when not specified.

B<$LOG_FILE_EXTENSION>: (string)

Default log file extension.

B<$LOG_FILE_EXTENSION>: (string)

Log file extension always appended to log files unless it has already been
specified in the log file name.

B<%LOG_TYPE_LABELS>: (hash)

Hash of log labels associated to log types.

B<%LOG_TYPE_INDENT>: (hash)

Hash of log space indentation associated to log types.

B<@LOG_SUPPORTED_OPTIONS>: (array)

Array of supported log options in both lib loading or command line (without
prefixing command line dashes).

B<@LOG_GETOPT>: (array)

Array of options for GetOptions (Getopt::Long module) that are used by the log
system and that can be used by scripts to avoid GetOpt errors when using log
parameters.
Ex.:

    GetOptions("help|?"     => \$help,
               "man"        => \$man,
               "debug:s"    => \$debug,
               ...
               @Greenphyl::Log::LOG_GETOPT,
    ) or pod2usage(1);

B<$OBJECT_PROPERTIES>: (hash ref)

sequence object properties description. See DBObject constructor documentation
for details.

=cut

our $DEBUG = 0;

our $LOG_TABLE             = 'greenphyl_logs';

our $LOG_TYPE_INFO         = 'info';
our $LOG_TYPE_VERBOSE      = 'verbose';
our $LOG_TYPE_DEBUG        = 'debug';
our $LOG_TYPE_WARNING      = 'warning';
our $LOG_TYPE_ERROR        = 'error';

our $LOG_LABEL_INFO        = '';
our $LOG_LABEL_DEBUG       = 'DEBUG: ';
our $LOG_LABEL_WARNING     = 'WARNING: ';
our $LOG_LABEL_ERROR       = 'ERROR: ';

our $LOG_FILE_NAME_DEFAULT = 'report';
our $LOG_FILE_EXTENSION    = '.log';

our %LOG_TYPE_LABELS        = (
    $LOG_TYPE_INFO    => $LOG_LABEL_INFO,
    $LOG_TYPE_VERBOSE => $LOG_LABEL_INFO,
    $LOG_TYPE_DEBUG   => $LOG_LABEL_DEBUG,
    $LOG_TYPE_WARNING => $LOG_LABEL_WARNING,
    $LOG_TYPE_ERROR   => $LOG_LABEL_ERROR,
);
our %LOG_TYPE_INDENT        = (
    $LOG_TYPE_INFO    => ' ' x length($LOG_LABEL_INFO),
    $LOG_TYPE_VERBOSE => ' ' x length($LOG_LABEL_INFO),
    $LOG_TYPE_DEBUG   => ' ' x length($LOG_LABEL_DEBUG),
    $LOG_TYPE_WARNING => ' ' x length($LOG_LABEL_WARNING),
    $LOG_TYPE_ERROR   => ' ' x length($LOG_LABEL_ERROR),
);
our @LOG_SUPPORTED_OPTIONS = qw(nolog nodb logfile logpath nofiletimestamp
    logtimestamp append screen noscreen screenonly nodebug noinfo noverbose nowarning noerror);
our $waste_opt;
our @LOG_GETOPT = ('log:s' => \$waste_opt, map {'log-' . $_ . ':s' => \$waste_opt} @LOG_SUPPORTED_OPTIONS);

our $OBJECT_PROPERTIES = {
    'table' => 'greenphyl_logs',
    'key' => 'id',
    'alternate_keys' => [],
    'load'     => [
        'id',
        'message',
        'pid',
        'script',
        'time',
        'type',
    ],
    'alias' => {
    },
    'base' => {
        'id' => {
            'property_column' => 'id',
        },
        'message' => {
            'property_column' => 'message',
        },
        'pid' => {
            'property_column' => 'pid',
        },
        'script' => {
            'property_column' => 'script',
        },
        'time' => {
            'property_column' => 'time',
        },
        'type' => {
            'property_column' => 'type',
        },
    },
    'secondary' => {
    },
    'tertiary' => {
    },
};


# Package variables
####################

=pod

=head1 VARIABLES


B<$g_master_init_done>: (boolean) (private)

When set to true, it means the logger initialization has been done.

B<$g_command_line_args>: (string) (protected)

Contains parameters recognized in command line.

B<$g_log_dbh>: (database handle) (protected)

This is the database handle to use to log data.

B<$g_log_fh>: (glob/file handle) (protected)

This is the log file handle to use to log data when database is not available.

B<$g_log_parameters>: (hash) (protected)

Hash of parameters for the logger. See OpenLog() doc for key details.

B<$g_file_lock>: (scalar) (private)

Scalar used to lock file access.

=cut

my $g_master_init_done;
our $g_command_line_args = '';
our $g_log_dbh;
our $g_log_fh;
our $g_log_parameters;
our $g_use_threads = (grep {$_ eq "threads.pm"} keys(%INC));
my  $g_thread_id = 0;

# if multi-thread, share a file lock
my $g_file_lock;
if ($g_use_threads)
{
    threads::shared::share($g_file_lock);
}

# Pointer to original PrintDebug function
sub _GreenphylPrintDebug;
*_GreenphylPrintDebug = \&Greenphyl::PrintDebug;




# Package subs
###############

=pod

=head1 STATIC METHODS

=head2 CONSTRUCTOR

B<Description>: Creates a new instance of a GreenPhyl log object.

B<ArgsCount>: 2

=over 4

=item $dbh: (DBI) (R)

Database handler of the database to load the object from.

=item $parameters: (hash ref) (R)

See Greenphyl::DBObject constructor documentation for details.

=back

B<Return>: (Greenphyl::Log)

a new instance.

B<Caller>: General

B<Example>:

    my $log = new Greenphyl::Log($dbh, {'selectors' => {'pid' => '1234'}});

=cut

sub new
{
    my ($proto, $dbh, $parameters) = @_;
    my $class = ref($proto) || $proto;

    if (!$parameters || ('HASH' ne ref($parameters)))
    {
        confess "ERROR: invalid parameters! The parameter after database handlher must be a hash ref.\n";
    }

    my $object_properties = $class->GetDefaultClassProperties();

    return $class->SUPER::new($object_properties, $dbh, $parameters);
}


=pod

=head1 STATIC METHODS

=head2 import

B<Description>: this function initialize the logger. It handles the same
parameters as OpenLog. See OpenLog documentation for details.

B<ArgsCount>: n

B<Caller>: perl system (use, import)

B<Example>:

    use Greenphyl::Log ('report.log');

    use Greenphyl::Log ('logfile'         => 'report.log',
                        'logpath'         => '/home/greenphyl/logs',
                        'nofiletimestamp' => 0,
                        'append'          => 1);

=cut

sub import
{
    # process module parameters
    OpenLog(@_);
}


=pod

=head2 _CheckThreadDBH

B<Description>: this function checks if current database handler corresponds to
current thread and if not, create a new connection.

When using multi-threads with Perl, database handlers can not be shared between
thread instances. Each thread must have its own database connection because the
Perl database driver does not support multi-threads.

B<ArgsCount>: 0

B<Caller>: internal

=cut

sub _CheckThreadDBH
{
    if ($g_log_dbh && $g_use_threads && ($g_thread_id != threads->tid()))
    {
        # put back original PrintDebug function to avoid infinite recursion
        no warnings; # disable subroutine redefinition warning in current block
        *Greenphyl::PrintDebug = \&_GreenphylPrintDebug;

        # DB handler will not correspond to current thread, create a new connection
        # print "DEBUG: init new DB connection for thread " . threads->tid() . "\n"; #+debug
        # get active (default) database
        my $active_database = SelectActiveDatabase();
        # create a new independant database handler (to separate logging
        # from other database transactions)
        $g_log_dbh = ConnectToDatabase(GP('DATABASES')->[$active_database]);
        $g_thread_id = threads->tid();

        # restore new PrintDebug function
        *Greenphyl::PrintDebug = \&_PrintDebugLog;
    }
}


=pod

=head2 _ResetConfig

B<Description>: this function clears config.

B<ArgsCount>: 0

B<Caller>: internal

=cut

sub _ResetConfig
{
    $g_master_init_done = 0;
    # check if we got a database handler
    # and if so, if we're running threads, make sure we got the right database handler
    if ($g_log_dbh
        && (!$g_use_threads || ($g_thread_id == threads->tid())))
    {
        $g_log_dbh->disconnect();
        $g_log_dbh = undef;
    }
    if ($g_log_fh)
    {
        close($g_log_fh);
        $g_log_fh = undef;
    }
    $g_log_parameters = {};
}


=pod

=head2 _FormatMessage

B<Description>: formats a message using the appropriate indentation.

B<ArgsCount>: 2

=over 4

=item $message: (string) (R)

The message to log.

=item $message_type: (string) (R)

The type of message to log.

=back

B<Return>: (string)

The formatted message.

B<Caller>: internal

=cut

sub _FormatMessage
{
    my ($message, $message_type) = @_;

    # remove leading/trailing invisible characters
    #+val: we may want to indent lines in messages so disable left trimming # $message =~ s/^\s+//s;
    $message =~ s/\s+$//s;

    # -check for multi-lines message and record it
    if ($message =~ m/[\r\n]/)
    {
        # multi-lines
        my @message_lines = split(/(?:\n|\r\n|\r)/, $message);
        $message =
            $LOG_TYPE_LABELS{$message_type}
            . join("\n" . $LOG_TYPE_INDENT{$message_type}, @message_lines)
            . "\n"
        ;
    }
    else
    {
        # single line
        $message = $LOG_TYPE_LABELS{$message_type} . $message . "\n";
    }

    return $message;
}


=pod

=head2 _LogMessageToDatabase

B<Description>: logs message to database.

B<ArgsCount>: 2

=over 4

=item $message: (string) (R)

The message to log.

=item $message_type: (string) (R)

The type of message to log.

=back

B<Return>: nothing

B<Caller>: internal

=cut

sub _LogMessageToDatabase
{
    my ($message, $message_type) = @_;

    # check thread
    _CheckThreadDBH();

    # log to database
    # $LOG_TABLE (id, time, script, pid, type, message)
    my $sql_query = "INSERT INTO $LOG_TABLE (time, script, pid, type, message) VALUES (now(), ?, ?, ?, ?);";
    $g_log_dbh->do($sql_query, undef, $0, $$, $message_type, $message) or confess $g_log_dbh->errstr;
}


=pod

=head2 _LogMessageToFile

B<Description>: logs message to file.

B<ArgsCount>: 2

=over 4

=item $message: (string) (R)

The message to log.

=item $message_type: (string) (R)

The type of message to log.

=back

B<Return>: nothing

B<Caller>: internal

=cut

sub _LogMessageToFile
{
    my ($message, $message_type) = @_;

    # log message...
    if ($g_log_parameters->{'logtimestamp'})
    {
        # -print date
        my ($second, $minute, $hour, $day_of_month, $month, $year_offset, $day_of_week, $day_of_year, $daylight_savings) = localtime();
        ++$month;
        my $year = 1900 + $year_offset;
        print {$g_log_fh} sprintf("\n##%02i/%02i/%04i %02i:%02i:%02i", $day_of_month, $month, $year, $hour, $minute, $second) . "\n";
    }

    $message = _FormatMessage($message, $message_type);
    # if threads: lock file
    if ($g_use_threads)
    {
        lock($g_file_lock);
        print {$g_log_fh} $message;
    }
    else
    {
        print {$g_log_fh} $message;
    }
}


=pod

=head2 _LogMessageToSTDOUT

B<Description>: logs message to STDOUT.

B<ArgsCount>: 2

=over 4

=item $message: (string) (R)

The message to log.

=item $message_type: (string) (R)

The type of message to log.

=back

B<Return>: nothing

B<Caller>: internal

=cut

sub _LogMessageToSTDOUT
{
    my ($message, $message_type) = @_;
    $message = _FormatMessage($message, $message_type);
    print STDOUT $message;
}


=pod

=head2 _LogMessageToSTDERR

B<Description>: logs message to STDERR.

B<ArgsCount>: 2-3

=over 4

=item $message: (string) (R)

The message to log.

=item $message_type: (string) (R)

The type of message to log.

=item $error: (exception class) (O)

Error object.

=back

B<Return>: nothing

B<Caller>: internal

=cut

sub _LogMessageToSTDERR
{
    my ($message, $message_type, $error) = @_;
    if ($error)
    {
        print STDERR "ERROR: Unable to log message to log file!\n";
        print STDERR "File log error: $error\n";
        print STDERR "Message to log (type $message_type): $message\n";
        print STDERR "\n";
    }
    else
    {
        $message = _FormatMessage($message, $message_type);
        print STDERR $message;
    }
}


=pod

=head2 _LogMessage

B<Description>: this function is used internally to log different types of
messages to the database or to a log file.

B<ArgsCount>: 1-2

=over 4

=item $message: (string) (R)

The message to log.

=item $message_type: (string) (O)

The type of message to log. Should be one of ('info', 'debug', 'warning',
'error'). See global constants "$LOG_TYPE_*".

=back

B<Return>: nothing

B<Caller>: internal

B<Example>:

    _LogMessage('Hello log!', 'info');

=cut

sub _LogMessage
{
    my ($message, $message_type) = @_;

    # check arguments
    if ((0 == @_) || (2 < @_))
    {
        confess "_LogMessage: invalid parameters!\nusage: _LogMessage(message [, message_type]);";
    }

    if (!defined($message))
    {
        warn "WARNING: tried to log an undefined message!\n";
        $message = '';
    }

    # check message type
    if (!$message_type
        || ($message_type !~ m/^(?:$LOG_TYPE_INFO|$LOG_TYPE_VERBOSE|$LOG_TYPE_DEBUG|$LOG_TYPE_WARNING|$LOG_TYPE_ERROR)$/o))
    {
        $message_type = $LOG_TYPE_INFO;
    }

    # display log messages?
    if ($g_log_parameters->{'screen'})
    {
        if (($message_type =~ m/^$LOG_TYPE_DEBUG$/)
            && GP('DEBUG_MODE'))
        {
            _LogMessageToSTDERR($message, $LOG_TYPE_DEBUG);
        }
        elsif ($message_type =~ m/^(?:$LOG_TYPE_WARNING|$LOG_TYPE_ERROR)$/)
        {
            _LogMessageToSTDERR($message, $message_type);
        }
        else
        {
            _LogMessageToSTDOUT($message, $message_type);
        }

        # screen only?
        if (2 == $g_log_parameters->{'screen'})
        {
            return $message;
        }
    }

    # log disabled?
    if ($g_log_parameters->{'nolog'})
    {
        return $message;
    }

    my $error;
    # check if we can log to database
    if (!$g_log_parameters->{'nodb'})
    {
        eval
        {
            _LogMessageToDatabase($message, $message_type);
        };
        $error = Exception::Class->caught();

        # check if an error occured while trying to log to database
        if ($error)
        {
            # switch to log file mode
            if (!$g_log_fh)
            {
                _OpenLogFile();
                warn "\n\nWARNING: Unable to log to database, switching to 'log to file' mode! Log file: '" . $g_log_parameters->{'logfile_path'} . "'\n\n";
                print {$g_log_fh} "WARNING: Unable to log to database: $error\n\n";
            }
        }
    }

    # log to file?
    if ($g_log_parameters->{'nodb'} || $error)
    {
        $error = undef; # reset error
        eval
        {
            _LogMessageToFile($message, $message_type);
        };
        $error = Exception::Class->caught();
    };

    # log to screen?
    if ($error)
    {
        # no db, no file, use STDERR
        _LogMessageToSTDERR($message, $message_type, $error);
    };

    return $message;
}


=pod

=head2 LogInfo, LogVerboseInfo, LogDebug, LogWarning, LogError

B<Description>: functions used to log different types of messages.
"LogInfo" is used for regular log messages;
"LogVerboseInfo" is used for verbose log messages only when verbose mode is set;
"LogDebug" should be used to only log debugging messages;
"LogWarning" should be used to log warning messages;
and "LogError" should be used to log errors.

B<ArgsCount>: 1

=over 4

=item $message: (string) (R)

The message to log.

=back

B<Return>: (string)

The log message.

B<Caller>: general

B<Example>:

    use Greenphyl::Log;
    LogInfo('Hello log!'); # log to database

    or

    use Greenphyl::Log ('report');
    LogInfo('Hello log!'); # log to a file like 'report201102151250.log'

=cut

sub LogInfo
{
    if ($g_log_parameters->{'noinfo'})
    {return '';}
    return _LogMessage(shift(), $LOG_TYPE_INFO);
}
# export
*main::LogInfo = \&LogInfo;

sub LogVerboseInfo
{
    if (!GP('VERBOSE_MODE') || $g_log_parameters->{'noverbose'})
    {return '';}
    return _LogMessage(shift(), $LOG_TYPE_VERBOSE);
}
# export
*main::LogVerboseInfo = \&LogVerboseInfo;

sub LogDebug
{
    # check if caller module has a $DEBUG variable
    # and if it's the case, make sure that variable is set to a true value
    my ($package, $filename, $line) = caller();
    my $local_debug_mode;
    eval '$local_debug_mode = $' . $package . '::DEBUG;';

    if (!GP('DEBUG_MODE')
        || (defined($local_debug_mode) && !$local_debug_mode)
        || $g_log_parameters->{'nodebug'})
    {return '';}
    return _LogMessage(shift(), $LOG_TYPE_DEBUG);
}
# export
*main::LogDebug = \&LogDebug;

sub LogWarning
{
    if ($g_log_parameters->{'nowarning'})
    {return '';}
    return _LogMessage(shift(), $LOG_TYPE_WARNING);
}
# export
*main::LogWarning = \&LogWarning;

sub LogError
{
    if ($g_log_parameters->{'noerror'})
    {return '';}
    return _LogMessage(shift(), $LOG_TYPE_ERROR);
}
# export
*main::LogError = \&LogError;


=pod

=head2 _OpenLogFile

B<Description>: function used internally to prepare logging into a file.

B<ArgsCount>: 0

B<Return>: nothing

B<Caller>: internal

=cut

sub _OpenLogFile
{
    if (!$g_log_fh)
    {
        # not opened, open log file...
        my $log_filename;
        $log_filename = $g_log_parameters->{'logfile'};
        $log_filename ||= $LOG_FILE_NAME_DEFAULT;
        $log_filename =~ s/\Q$LOG_FILE_EXTENSION\E//; # remove extension to avoid duplicates
        if ($g_log_parameters->{'nofiletimestamp'})
        {
            $log_filename .= $LOG_FILE_EXTENSION;
        }
        else
        {
            # -generate timestamp
            my ($second, $minute, $hour, $day_of_month, $month, $year_offset, $day_of_week, $day_of_year, $daylight_savings) = localtime();
            ++$month;
            my $year = 1900 + $year_offset;
            $log_filename = sprintf("%s_%04i%02i%02i%02i%02i%02i%s", $log_filename, $year, $month, $day_of_month, $hour, $minute, $second, $LOG_FILE_EXTENSION);
        }
        # -check for config log path
        my $log_path = $g_log_parameters->{'logpath'};
        #  no log path parameter, was it specified in file name?
        if (!$log_path
            && (!$g_log_parameters->{'logfile'}
                || ($g_log_parameters->{'logfile'} !~ m/\//)))
        {
            # not specified in file name nor as parameter, use default path
            $log_path = GP('LOG_PATH');
        }

        if ($log_path)
        {
            if (!-e $log_path) {
                mkdir($log_path);
            }
            if (-d $log_path && -w $log_path)
            {
                # remove trailing slash
                $log_path =~ s/\/+$//g;
                $log_filename = "$log_path/$log_filename";
            }
            else
            {
                warn "WARNING: log file path '$log_path' is invalid (either it does not exist or is read-only)! Logging to current directory.\n";
            }
        }
        # -open log file
        $g_log_parameters->{'logfile_path'} = $log_filename;
        if ($g_log_parameters->{'append'})
        {
            open($g_log_fh, ">>$log_filename")
                or confess "Failed to open log file '$log_filename' for writing (append)!\n";
        }
        else
        {
            open($g_log_fh, ">$log_filename")
                or confess "Failed to open log file '$log_filename' for writing!\n";
        }
        _GreenphylPrintDebug("Will log into file $log_filename");
        # add a log header
        my ($second, $minute, $hour, $day_of_month, $month, $year_offset, $day_of_week, $day_of_year, $daylight_savings) = localtime();
        ++$month;
        my $year = 1900 + $year_offset;
        print {$g_log_fh} "\nGREENPHYL LOG " . sprintf("%02i/%02i/%04i %02i:%02i:%02i", $day_of_month, $month, $year, $hour, $minute, $second) . "\n\n";
    }
    return $g_log_fh;
}




=pod

=head2 OpenLog

B<Description>: function used to initialize logging. It is not necessary to all
it when using "use" or "import" to load this module.

B<ArgsCount>: 1-2*n

Parameters can be submitted using a hash form. If only 1 scalar is provided, the
logger will assume the user wants to log into a file using the specified name.
Otherwise, a list of key-value pairs should be provided:
-'nolog': disable both database and file logging (nb. 'screen' can still be
 used);
-'nodb': do not log into database but to a file;
-'logfile' (string): file name to use if logging to database fails.
 Note: the file won't be created if everything can be logged into the database;
 Note: the $LOG_FILE_EXTENSION will be appended to the name after the optional
 timestamp;
-'logpath' (string): path to the directory where the log file should be located.
 Note: it will only be used in case of unsuccessfull database logging; Trailing
 slashes are ignored;
-'nofiletimestamp': do not append a time stamp to the file name;
-'logtimestamp': add a timestamp to each block logged;
-'append': append log to the file;
-'screen': also output log messages to screen;
-'noscreen': do not output log messages to screen;
-'screenonly': log to screen only and not to file or DB;


Default behavior:
-logs to screen;
-logs to database;
-uses $LOG_FILE_NAME_DEFAULT . $LOG_FILE_EXTENSION as default base filename;
-uses $Greenphyl::Config::LOG_PATH as default log file path;
-a timestamp is appended to the base file name just before the
 $LOG_FILE_EXTENSION extension;
-log messages are appended to files and log files are not overwritten.

B<Return>: nothing

B<Caller>: general

B<Example>:

    # will append log to a file like 'report201102151250.log' and not to database
    OpenLog('report');

    # if database logging fails, it will append log to a file like
    # 'report201102151250.log'
    OpenLog('logfile' => 'report');

    # will log to the file '/home/greenphyl/report.log' that will be created
    # (blank) and not to database
    OpenLog('logfile' => 'report',
             'logpath' => '/home/greenphyl',
             'nodb' => 1,
             'nofiletimestamp' => 1,
             'append' => 0,
             'logtimestamp' => 1,
    );

=cut

sub OpenLog
{
    # if log system has already been initialized, do nothing
    # ("use Greenphyl::Log;" from different scripts and/or modules)
    if ($g_master_init_done)
    {
        return;
    }

    my %log_parameters = (
        'screen'    => 1,
        # initial log levels
        'nodebug'   => 0,
        'noinfo'    => 0,
        'noverbose' => 0,
        'nowarning' => 0,
        'noerror'   => 0,
    );
    # check how the log system should be initialized...
    # skip package name
    if (@_ && $_[0] eq 'Greenphyl::Log')
    {shift();}

    # only a file name specified, not a hash?
    if (1 == @_)
    {
        # file mode
        $log_parameters{'logfile'} = shift();
        $log_parameters{'nodb'}    = 1;
    }
    elsif (scalar(@_)%2)
    {
        confess "ERROR: Invalid parameters for Greenphyl::Log initialization (odd number of args)! See documentation.\nParameters:\n" . join(', ', @_) . "\n";
    }
    else
    {
        # parameters hash
        %log_parameters = @_;
        # set default screen on
        if (!exists($log_parameters{'screen'}))
        {
            $log_parameters{'screen'} = 1;
        }
    }
    $g_log_parameters = \%log_parameters;

    # check for command line parameters
    $g_command_line_args = '';
    my $argument_index = 0;
    while ($argument_index < @ARGV)
    {
        my $argument = $ARGV[$argument_index++];
        if ($argument =~ m/-?-log-(\w+)(?:(?:=|\s+)(.*))?/)
        {
            if (defined($2))
            {
                _GreenphylPrintDebug("Got command line argument '$1'='$2'");
                $g_log_parameters->{$1} = $2;
                $g_command_line_args .= " --log-$1=$2";
            }
            else
            {
                my $log_parameter = $1;
                # check if next argument is a parameter (starting with dash)
                if (($argument_index < @ARGV)
                    && ($ARGV[$argument_index] !~ m/^\s*-/))
                {
                    # got a value
                    _GreenphylPrintDebug("Got command line argument '$log_parameter' '$ARGV[$argument_index]'");
                    $g_log_parameters->{$log_parameter} = $ARGV[$argument_index];
                    $g_command_line_args .= " --log-$log_parameter=$ARGV[$argument_index]";
                }
                else
                {
                    _GreenphylPrintDebug("Got command line argument '$log_parameter'");
                    $g_log_parameters->{$log_parameter} = 1;
                    $g_command_line_args .= " --log-$log_parameter";
                }
            }
        }
        elsif ($argument =~ m/-?-log(?:[^\w\-]|$)/)
        {
            # force enable logs
            _GreenphylPrintDebug("Got command line argument '-log'");
            $g_log_parameters->{'nolog'} = 0;
            $g_command_line_args .= " --log";
        }
    }

    # append by default unless specified not to do so
    if (!exists($g_log_parameters->{'append'}))
    {
        $g_log_parameters->{'append'} = 1;
    }

    # log disabled?
    if ($g_log_parameters->{'nolog'})
    {
        $g_master_init_done = 1;
        return;
    }

    # no screen?
    if (exists($g_log_parameters->{'noscreen'})
        && $g_log_parameters->{'noscreen'})
    {
        $g_log_parameters->{'screen'}     = 0;
        delete($g_log_parameters->{'screenonly'});
    }

    # screen only?
    if (exists($g_log_parameters->{'screenonly'})
        && $g_log_parameters->{'screenonly'})
    {
        $g_log_parameters->{'screen'}  = 2;
        $g_log_parameters->{'nodb'}    = 1;
        $g_log_parameters->{'logfile'} = undef;
    }

    # log to file?
    if ($g_log_parameters->{'logfile'})
    {
        # disable database logs
        $g_log_parameters->{'nodb'}    = 1;
    }

    # not logging to database?
    if ($g_log_parameters->{'nodb'})
    {
        _GreenphylPrintDebug("Do not log to database");
        # not screen only?
        if (!$g_log_parameters->{'screen'}
            || (1 >= $g_log_parameters->{'screen'}))
        {
            # try to open log file
            _OpenLogFile();
        }
    }
    else
    {
        _GreenphylPrintDebug("Log to database");
        # try open database
        eval
        {
            # get active (default) database
            my $active_database = SelectActiveDatabase();
            # create a new independant database handler (to separate logging
            # from other database transactions)
            $g_log_dbh = ConnectToDatabase(GP('DATABASES')->[$active_database]);
        };

        if (my $error = Exception::Class->caught())
        {
            cluck "WARNING: unable to open database for logging! " . $error . "\n";
        }

        if ($g_log_dbh)
        {
            # check if log table exists and create it if missing
            eval
            {
                _GreenphylPrintDebug("Check if log table is missing (auto-create)...");
                my $sql_query =
"CREATE TABLE IF NOT EXISTS $LOG_TABLE (
  id BIGINT NOT NULL AUTO_INCREMENT PRIMARY KEY,
  time TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  script VARCHAR( 32 ) NOT NULL DEFAULT '',
  pid INT NOT NULL DEFAULT 0,
  type VARCHAR( 8 ) NOT NULL DEFAULT '',
  message TEXT NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;";
                $g_log_dbh->do($sql_query) or confess $g_log_dbh->errstr;
                _GreenphylPrintDebug("Log table ok.");
            };

            if (my $table_creation_error = Exception::Class->caught())
            {
                $g_log_dbh->disconnect();
                $g_log_dbh = undef;
                cluck "WARNING: log table is missing and table creation failed! " . $table_creation_error . "\n";
            }
            else
            {
                # log command line
                _LogMessageToDatabase("PROCESS BEGIN: $0 " . join(" ", @ARGV), $LOG_TYPE_INFO);
            }
        }
    }

    # let log system knows it has already been initialized
    $g_master_init_done = 1;
}
# export
*main::OpenLog = \&OpenLog;




=pod

=head2 GetLogs

B<Description>: returns a list of log messages corresponding to the given
parameters.

B<ArgsCount>: 1

=over 4

Parameters can be submitted using a hash form.
-'from_date' (string): a date from which logs should be retrieved. Format:
'2011-03-21 13:57:29';
-'to_date' (string): a date up to which logs should be retrieved.
Format: '2011-03-21 13:57:29';
-'type' (string): the type of log message that should be retrieved;
-'pid' (integer): the process ID of the log messages;
-'script' (string): the name of the process which issued the log messages;
-'search' (string): a string that must appear in the log messages;
-'limit' (integer): maximum number of messages. Default: 100; 0 = no limit;
-'order' (string): can be 'reverse' (default) or 'chronological'; messages will
  be displayed in chronological order but this affect the way they are sorted
  in the query and that can impact on what is selected when there is a limit;
-'from_log_id' (integer): from log identifier;
-'to_log_id' (integer): to log identifier.

=back

B<Return>: (string) the log messages.

B<Example>:

    print GetLogs('from_date' => '2011-03-01 12:00:00',
                   'to_date'   => '2011-03-01 13:00:00',
                   'type'      => 'debug',
                   'pid'       => 28187,
                   'script'    => 'update.pl',
                   'search'    => 'UPDATE',
                   'limit'     => 0,
                   'order'     => 'reverse');

=cut

sub GetLogs
{
    my (%parameters) = @_;

    if (!$g_log_dbh)
    {
        confess "ERROR: no database handle! Please check your config and make sure the database is available!\n";
    }

    # check thread
    _CheckThreadDBH();

    my @where_clause = ();
    my $order_clause = ' ORDER BY time DESC';
    my $limit_clause = ' LIMIT 100';

    if ($parameters{'from_date'})
    {
        my ($year, $month, $day, $hour, $minutes, $seconds) =
            ($parameters{'from_date'} =~ m/(\d\d\d\d)[\/-](\d\d)[\/-](\d\d)(:? +(\d{1,2}):(\d\d)(?::(\d\d))?)?/);
        if ($year && $month && $day)
        {
            my $where_clause .= "time >= '" . $year . '-' . $month . '-' . $day;
            if (defined($hour) && defined($minutes))
            {
                $where_clause .= " $hour:$minutes";
                if (defined($seconds))
                {
                    $where_clause .= ":$seconds";
                }
            }
            $where_clause .= "'";
            push(@where_clause, $where_clause);
        }
    }

    if ($parameters{'to_date'})
    {
        my ($year, $month, $day, $hour, $minutes, $seconds) =
            ($parameters{'to_date'} =~ m/(\d\d\d\d)[\/-](\d\d)[\/-](\d\d)(:? +(\d{1,2}):(\d\d)(?::(\d\d))?)?/);
        if ($year && $month && $day)
        {
            my $where_clause .= "time <= '" . $year . '-' . $month . '-' . $day;
            if (defined($hour) && defined($minutes))
            {
                $where_clause .= " $hour:$minutes";
                if (defined($seconds))
                {
                    $where_clause .= ":$seconds";
                }
            }
            $where_clause .= "'";
            push(@where_clause, $where_clause);
        }
    }

    if ($parameters{'from_log_id'} && ($parameters{'from_log_id'} =~ m/^\d+$/))
    {
        push(@where_clause, "id >= " . $parameters{'from_log_id'});
    }

    if ($parameters{'to_log_id'} && ($parameters{'to_log_id'} =~ m/^\d+$/))
    {
        push(@where_clause, "id <= " . $parameters{'to_log_id'});
    }

    if ($parameters{'type'} && ($parameters{'type'} =~ m/^\w+$/))
    {
        push(@where_clause, "type LIKE '" . $parameters{'type'} . "'");
    }

    if ($parameters{'pid'} && ($parameters{'pid'} =~ m/^\d+$/))
    {
        push(@where_clause, "pid = " . $parameters{'pid'});
    }

    if ($parameters{'script'} && ($parameters{'script'} =~ m/^[\w.\-\\\/ ]+$/))
    {
        push(@where_clause, "script LIKE '" . $parameters{'script'} . "'");
    }

    if ($parameters{'search'} && ($parameters{'search'} =~ m/^[\w.\-\\\/ ]+$/))
    {
        push(@where_clause, "message LIKE " . $g_log_dbh->quote('%' . $parameters{'search'} . '%'));
    }

    if (defined($parameters{'limit'}) && ($parameters{'limit'} =~ m/^\d+$/))
    {
        if (0 == $parameters{'limit'})
        {
            $limit_clause = ''; # no limit
        }
        else
        {
            $limit_clause = ' LIMIT ' . $parameters{'limit'};
        }
    }

    if (defined($parameters{'order'}))
    {
        if ($parameters{'order'} =~ m/^reverse$/i)
        {
            $order_clause = ' ORDER BY time ASC';
        }
        elsif ($parameters{'order'} =~ m/^chronological$/i)
        {
            $order_clause = ' ORDER BY time DESC';
        }
    }

    my $sql_query = "SELECT message, UNIX_TIMESTAMP(time) AS \"time\", type FROM $LOG_TABLE " . (@where_clause? 'WHERE ' . join(' AND ', @where_clause) : '') . $order_clause . $limit_clause . ';';
    my $results = $g_log_dbh->selectall_arrayref($sql_query, { 'Slice' => {} }) or confess $g_log_dbh->errstr;

    my $log_messages = '';
    map {$log_messages .= ($_->{'type'} eq 'info'?'': uc($_->{'type'}) . ': ') . $_->{'message'} . "\n"} (sort { $a->{'time'} <=> $b->{'time'} } @$results);
    return $log_messages;
}
# export
*main::GetLogs = \&GetLogs;




=pod

=head2 RemoveLogs

B<Description>: remove a list of log messages corresponding to the given
parameters.

B<ArgsCount>: 1

=over 4

Parameters can be submitted using a hash form.
-'from_date' (string): a date from which logs should be retrieved. Format:
'2011-03-21 13:57:29';
-'to_date' (string): a date up to which logs should be retrieved.
Format: '2011-03-21 13:57:29';
-'type' (string): the type of log message that should be retrieved;
-'pid' (integer): the process ID of the log messages;
-'script' (string): the name of the process which issued the log messages;
-'search' (string): a string that must appear in the log messages;
-'from_log_id' (integer): from log identifier;
-'to_log_id' (integer): to log identifier.

=back

B<Return>: nothing

B<Example>:

    RemoveLogs('from_date' => '2011-03-01 12:00:00',
                   'to_date'   => '2011-03-01 13:00:00',
                   'type'      => 'debug',
                   'pid'       => 28187,
                   'script'    => 'update.pl',
                   'search'    => 'UPDATE');

=cut

sub RemoveLogs
{
    my (%parameters) = @_;

    if (!$g_log_dbh)
    {
        confess "ERROR: no database handle! Please check your config and make sure the database is available!\n";
    }

    # check thread
    _CheckThreadDBH();

    my @where_clause = ();

    if ($parameters{'from_date'})
    {
        my ($year, $month, $day, $hour, $minutes, $seconds) =
            ($parameters{'from_date'} =~ m/(\d\d\d\d)[\/-](\d\d)[\/-](\d\d)(:? +(\d{1,2}):(\d\d)(?::(\d\d))?)?/);
        if ($year && $month && $day)
        {
            my $where_clause .= "time >= '" . $year . '-' . $month . '-' . $day;
            if (defined($hour) && defined($minutes))
            {
                $where_clause .= " $hour:$minutes";
                if (defined($seconds))
                {
                    $where_clause .= ":$seconds";
                }
            }
            $where_clause .= "'";
            push(@where_clause, $where_clause);
        }
    }

    if ($parameters{'to_date'})
    {
        my ($year, $month, $day, $hour, $minutes, $seconds) =
            ($parameters{'to_date'} =~ m/(\d\d\d\d)[\/-](\d\d)[\/-](\d\d)(:? +(\d{1,2}):(\d\d)(?::(\d\d))?)?/);
        if ($year && $month && $day)
        {
            my $where_clause .= "time <= '" . $year . '-' . $month . '-' . $day;
            if (defined($hour) && defined($minutes))
            {
                $where_clause .= " $hour:$minutes";
                if (defined($seconds))
                {
                    $where_clause .= ":$seconds";
                }
            }
            $where_clause .= "'";
            push(@where_clause, $where_clause);
        }
    }

    if ($parameters{'from_log_id'} && ($parameters{'from_log_id'} =~ m/^\d+$/))
    {
        push(@where_clause, "id >= " . $parameters{'from_log_id'});
    }

    if ($parameters{'to_log_id'} && ($parameters{'to_log_id'} =~ m/^\d+$/))
    {
        push(@where_clause, "id <= " . $parameters{'to_log_id'});
    }

    if ($parameters{'type'} && ($parameters{'type'} =~ m/^\w+$/))
    {
        push(@where_clause, "type LIKE '" . $parameters{'type'} . "'");
    }

    if ($parameters{'pid'} && ($parameters{'pid'} =~ m/^\d+$/))
    {
        push(@where_clause, "pid = " . $parameters{'pid'});
    }

    if ($parameters{'script'} && ($parameters{'script'} =~ m/^[\w.\-\\\/ ]+$/))
    {
        push(@where_clause, "script LIKE '" . $parameters{'script'} . "'");
    }

    if ($parameters{'search'} && ($parameters{'search'} =~ m/^[\w.\-\\\/ ]+$/))
    {
        push(@where_clause, "message LIKE " . $g_log_dbh->quote('%' . $parameters{'search'} . '%'));
    }

    my $sql_query = "DELETE FROM $LOG_TABLE " . (@where_clause? 'WHERE ' . join(' AND ', @where_clause) : '') . ';';
    _GreenphylPrintDebug("SQL QUERY: $sql_query\n");
    my $deleted_rows = $g_log_dbh->do($sql_query) or confess $g_log_dbh->errstr;

    return $deleted_rows;
}
# export
*main::RemoveLogs = \&RemoveLogs;




=pod

=head2 GetLastLogPID

B<Description>: returns the pid of last entry.

B<ArgsCount>: 0-1

=over 4

$offset: (integer)

select the $offset-th of process before last process. Default: 0.

=back

B<Return>: (integer) last PID

B<Example>:

    print GetLastLogPID();

=cut

sub GetLastLogPID
{
    my ($offset) = @_;

    if (!$g_log_dbh)
    {
        confess "ERROR: no database handle! Please check your config and make sure the database is available!\n";
    }

    # check thread
    _CheckThreadDBH();

    # get last process ID
    my $sql_query = "SELECT pid FROM $LOG_TABLE GROUP BY pid, DATE(time) ORDER BY time DESC";
    if ($offset)
    {
        $sql_query .= " LIMIT $offset, 1;";
    }
    else
    {
        $sql_query .= " LIMIT 1;";
    }
    my ($pid) = $g_log_dbh->selectrow_array($sql_query) or confess $g_log_dbh->errstr;
    return $pid;
}
# export
*main::GetLastLogPID = \&GetLastLogPID;


=pod

=head2 GetLastLogID

B<Description>: returns the log id of last entry.

B<ArgsCount>: 0

B<Return>: (integer) last log ID

B<Example>:

    print GetLastLogID();

=cut

sub GetLastLogID
{
    my ($offset) = @_;

    if (!$g_log_dbh)
    {
        confess "ERROR: no database handle! Please check your config and make sure the database is available!\n";
    }

    # check thread
    _CheckThreadDBH();

    # get last process ID
    my $sql_query = "SELECT id FROM $LOG_TABLE ORDER BY id DESC LIMIT 1;";
    my ($log_id) = $g_log_dbh->selectrow_array($sql_query) or confess $g_log_dbh->errstr;

    return $log_id;
}
# export
*main::GetLastLogID = \&GetLastLogID;


=pod

=head2 GetLastLogs

B<Description>: returns a list of log messages issued by the last process.

B<ArgsCount>: 0-1

=over 4

$offset: (integer)

select the $offset-th of process before last process. Default: 0.

=back

B<Return>: string

B<Example>:

    print GetLastLogs();

=cut

sub GetLastLogs
{
    my ($offset) = @_;

    if (!$g_log_dbh)
    {
        confess "ERROR: no database handle! Please check your config and make sure the database is available!\n";
    }

    # check thread
    _CheckThreadDBH();

    return GetLogs('pid' => GetLastLogPID($offset));
}
# export
*main::GetLastLogs = \&GetLastLogs;


=pod

=head2 GetLogCommandLineArguments

B<Description>: returns string containing the command line arguments used for
current logs.

B<ArgsCount>: 0

B<Return>: string

=cut

sub GetLogCommandLineArguments
{
    return $g_command_line_args;
}
# export
*main::GetLogCommandLineArguments = \&GetLogCommandLineArguments;


=pod

=head2 _PrintDebugLog

B<Description>: Replaces original PrintDebug function to include debug messages
into log.

B<ArgsCount>: 1

B<Return>: see Greenphyl::PrintDebug documentation.

=cut

sub _PrintDebugLog
{
    # check if caller module has a $DEBUG variable
    # and if it's the case, make sure that variable is set to a true value
    my ($package, $filename, $line) = caller();
    my $local_debug_mode;
    eval '$local_debug_mode = $' . $package . '::DEBUG;';

	if ($local_debug_mode)
	{
		# as the caller changes, we have to change $DEBUG according to the
		# original caller
		my $old_debug = $DEBUG;
		$DEBUG = 1;

		if ($g_log_parameters->{'screen'} != 2)
		{
			# do not log to screen as it will be handled by the original function
			my $old_screen_value = $g_log_parameters->{'screen'};
			$g_log_parameters->{'screen'} = 0;
			LogDebug(@_);
			$g_log_parameters->{'screen'} = $old_screen_value;
		}

		# call original method
		my $debug_string = _GreenphylPrintDebug(@_);

		# put back original value
		$DEBUG = $old_debug;
		
		return $debug_string;
	}
}




# CODE START
#############

{
    no warnings; # disable subroutine redefinition warning in current block

    # reassign methods
    *Greenphyl::PrintDebug = \&_PrintDebugLog;

}




END
{
    if ($g_log_dbh && !$ENV{'TESTING'})
    {
        # log end time
        _LogMessageToDatabase("PROCESS END", $LOG_TYPE_INFO);
    }
}

=pod

=head1 AUTHORS

Mathieu ROUARD (Bioversity), m.rouard@cgiar.org

Valentin GUIGNON (Bioversity), v.guignon@cgiar.org

Matthieu CONTE (Syngenta)

Sebastien BRIOIS (Syngenta)

Christian WALDE

=head1 VERSION

Version 2.0.0

Date 18/12/2012

=cut

return 1; # package return
