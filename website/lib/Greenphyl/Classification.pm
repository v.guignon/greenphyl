=pod

=head1 NAME

Greenphyl::Classification - GreenPhyl Classification Object

=head1 SYNOPSIS

    use Greenphyl::Classification;
    my $classification = Greenphyl::Classification->new($dbh, {'selectors' => {'class_id' => 2}});
    print $class->description();

=head1 REQUIRES

Perl5

=head1 EXPORTS

Nothing

=head1 DESCRIPTION

This module implements a GreenPhyl Classification database object.

=cut

package Greenphyl::Classification;

use strict;
use warnings;

use base qw(Greenphyl::CachedDBObject);

use vars qw($AUTOLOAD);

use Carp qw(cluck confess croak);




# Package constants
####################

=pod

=head1 CONSTANTS

B<$OBJECT_PROPERTIES>: (hash ref)

object properties description. See DBObject constructor documentation for
details.

=cut

our $OBJECT_PROPERTIES = {
    'table' => 'classifications',
    'key' => 'class_id',
    'alternate_keys' => [],
    'load'     => [
        'class_description',
        'class_id',
        'class_name',
    ],
    'alias' => {
        'description' => 'class_description',
        'id' => 'class_id',
        'name' => 'class_name',
    },
    'base' => {
        'class_description' => {
            'property_column' => 'class_description',
        },
        'class_id' => {
            'property_column' => 'class_id',
        },
        'class_name' => {
            'property_column' => 'class_name',
        },
    },
    'secondary' => {
#        '<member_name>' => {
#            #'object_key'     => '<key_id>', # not defined means use default 'key'
#            'property_table'  => '<>',
#            'property_key'    => '<>',
#            'multiple_values' => 0,
#            'property_column' => '<property_column>',
#        },
#        '<member2_name>' => {
#            #'object_key'     => '<key_id>',
#            'property_table'  => '<>',
#            'property_key'    => '<>',
#            'multiple_values' => 1,
#            'property_module' => 'Greenphyl::<Object>',
#        },
    },
    'tertiary' => {
#        '<member3_name>' => {
#            #'object_key'       => '<key_id>', # not defined means 'key'
#            'link_table'        => '<>',
#            'link_object_key'   => '<>',
#            'link_property_key' => '<>',
#            'property_table'    => '<>',
#            'property_key'      => '<>',
#            'multiple_values'   => 1,
#            'property_module'   => 'Greenphyl::<Object>',
#        },
    },

};




# Package subs
###############

=pod

=head1 STATIC METHODS

=head2 CONSTRUCTOR

B<Description>: Creates a new instance of a GreenPhyl Classification object.

B<ArgsCount>: 2

=over 4

=item $dbh: (DBI::db) (U)

Database handler of the database to load the object from.

=item $parameters: (hash ref) (R)

See Greenphyl::DBObject constructor documentation for details.

=back

B<Return>: (Greenphyl::Classification)

a new instance.

B<Caller>: General

B<Example>:

    my $classification = new Greenphyl::Classification($dbh, {'selectors' => {'class_id' => 2}});

=cut

sub new
{
    my $proto = shift();
    my $class = ref($proto) || $proto;
    
    my $object_properties = $class->GetDefaultClassProperties();

    return $class->SUPER::new($object_properties, @_);
}




=pod

=head1 AUTHORS

Mathieu ROUARD (Bioversity), m.rouard@cgiar.org

Valentin GUIGNON (Bioversity), v.guignon@cgiar.org

Matthieu CONTE (Syngenta)

=head1 VERSION

Version 1.0.0

Date 17/04/2012

=head1 SEE ALSO

GreenPhyl documentation, DBObject documentation.

=cut

return 1; # package return
