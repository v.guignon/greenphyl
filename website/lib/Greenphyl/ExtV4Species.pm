=pod

=head1 NAME

Greenphyl::ExtV4Species - GreenPhyl v4 Species Object

=head1 SYNOPSIS

    use Greenphyl::ExtV4Species;
    my $v4_species = Greenphyl::ExtV4Species->new($dbh, {'selectors' => {'code' => 'ARATH'}});
    print $v4_species->name();

=head1 REQUIRES

Perl5

=head1 EXPORTS

Nothing

=head1 DESCRIPTION

This module implements a GreenPhyl v4 Species database object.

=cut

package Greenphyl::ExtV4Species;

use strict;
use warnings;

use base qw(
    Greenphyl::CachedDBObject
    Greenphyl::DumpableObject
    Greenphyl::DumpableTabularData
);

use vars qw($AUTOLOAD);

use Carp qw(cluck confess croak);

use Greenphyl;
use Greenphyl::Taxonomy;
use Greenphyl::ExtV4Sequence;
use Greenphyl::SourceDatabase;




# Package constants
####################

=pod

=head1 CONSTANTS

B<$DEBUG>: (boolean)

When set to true, it enables debug mode.

B<$OBJECT_PROPERTIES>: (hash ref)

object properties description. See DBObject constructor documentation for
details.

=cut

our $DEBUG = 0;

our $OBJECT_PROPERTIES = {
    'table' => 'species',
    'key' => 'id',
    'alternate_keys' => ['code', 'taxonomy_id', 'organism'],
    'load'     => [
        # 'chromosome_count',
        'color',
        'common_name',
        # 'genome_size',
        'sequence_count',
        'id',
        'organism',
        'code',
        'taxonomy_id',
        'display_order',
        # 'url_fasta',
        # 'url_institute',
        # 'url_picture',
        # 'version',
        # 'version_notes',
    ],
    'alias' => {
        'species_code' => 'code',
        'species_id' => 'id',
        'name' => 'organism',
        'species_name' => 'organism',
    },
    'base' => {
        'chromosome_count' => {
            'property_column' => 'chromosome_count',
        },
        'common_name' => {
            'property_column' => 'common_name',
        },
        'genome_size' => {
            'property_column' => 'genome_size',
        },
        'organism' => {
            'property_column' => 'organism',
        },
        'sequence_count' => {
            'property_column' => 'sequence_count',
        },
        'id' => {
            'property_column' => 'id',
        },
        'code' => {
            'property_column' => 'code',
        },
        'color' => {
            'property_column' => 'color',
        },
        'taxonomy_id' => {
            'property_column' => 'taxonomy_id',
        },
        'display_order' => {
            'property_column' => 'display_order',
        },
        'url_fasta' => {
            'property_column' => 'url_fasta',
        },
        'url_institute' => {
            'property_column' => 'url_institute',
        },
        'url_picture' => {
            'property_column' => 'url_picture',
        },
        'version' => {
            'property_column' => 'version',
        },
        'version_notes' => {
            'property_column' => 'version_notes',
        },
    },
    'secondary' => {
        'taxonomy' => {
            'object_key'      => 'taxonomy_id',
            'property_table'  => 'taxonomy',
            'property_key'    => 'id',
            'multiple_values' => 0,
            'property_module' => 'Greenphyl::Taxonomy',
        },
        'sequences' => {
            'property_table'  => 'sequences',
            'property_key'    => 'species_id',
            'multiple_values' => 1,
            'property_module' => 'Greenphyl::ExtV4Sequence',
        },
        'sequence_count_by_families_and_species' => {
            'property_table'  => 'sequence_count_by_families_species_cache',
            'property_key'    => 'species_id',
            'multiple_values' => 1,
            'property_module' => 'Greenphyl::SequenceCount',
        },
    },
    'tertiary' => {
        'ipr' => {
            'link_table'        => 'families_ipr_species_cache',
            'link_object_key'   => 'species_id',
            'link_property_key' => 'ipr_id',
            'property_module'   => 'Greenphyl::IPR',
            'property_table'    => 'ipr',
            'property_key'      => 'id',
            'multiple_values'   => 1,
        },
        'lineage' => {
            'object_key'        => 'taxonomy_id',
            'link_table'        => 'lineage',
            'link_object_key'   => 'taxonomy_id',
            'link_property_key' => 'lineage_taxonomy_id',
            'property_table'    => 'taxonomy',
            'property_key'      => 'id',
            'multiple_values'   => 1,
            'property_module'   => 'Greenphyl::Taxonomy',
        },
        'families'   => {
            'link_table'        => 'sequence_count_by_families_species_cache',
            'link_object_key'   => 'species_id',
            'link_property_key' => 'family_id',
            'property_table'    => 'families',
            'property_key'      => 'id',
            'multiple_values'   => 1,
            'property_module'   => 'Greenphyl::ExtV4Family',
        },
    },

};

our $SPECIES_CODE_REGEXP = '(?:[A-Z]{5,5}|PEA)'; # 'PEA' is the only code != 5 char

our $SUPPORTED_DUMP_FORMAT =
    {
        'static'  => {
            'excel'   => \&DumpExcel,
            'csv'     => \&DumpCSV,
            'xml'     => \&DumpXML,
        },
        'dynamic' => {
            'excel'   => \&DumpExcel,
            'csv'     => \&DumpCSV,
            'xml'     => \&DumpXML,
        },
    };



# Package subs
###############

=pod

=head1 STATIC METHODS

=head2 CONSTRUCTOR

B<Description>: Creates a new instance of a GreenPhyl v4 Species object.

B<ArgsCount>: 2

=over 4

=item $dbh: (DBI::db) (U)

Database handler of the database to load the object from.

=item $parameters: (hash ref) (R)

See Greenphyl::DBObject constructor documentation for details.

=back

B<Return>: (Greenphyl::ExtV4Species)

a new instance.

B<Caller>: General

B<Example>:

    my $v4_species = new Greenphyl::ExtV4Species($dbh, {'selectors' => {'species_name' => 'ARATH'}});

=cut

sub new
{
    my $proto = shift();
    my $class = ref($proto) || $proto;
    
    my $object_properties = $class->GetDefaultClassProperties();

    return $class->SUPER::new($object_properties, @_);
}




=pod

=head2 

B<Description>: Creates new instances of GreenPhyl v4 Species objects.

B<ArgsCount>: 3

=over 4

=item $dbh: (DBI::db) (U)

Database handler of the database to load the object from.

=item $selectors: (hash ref) (R)

A selector for taxonomy. Keys are selection fields and values are selection
values. Supported fields are:
'taxonomy_name': name of the taxonomy node that contain the species

=item $only_gp_species: (boolean) (O)

If true (default), only return GreenPhyl species (ie. species with sequences in
database).

=back

B<Return>: (ref of an array of Greenphyl::ExtV4Species)

a list of species objects.

B<Caller>: General

B<Example>:

    my $v4_species_ref = Greenphyl::GetSpeciesByTaxonomy($dbh, {'taxonomy_name' => $taxonomy_name});

=cut

sub GetSpeciesByTaxonomy
{
    my ($dbh, $selectors, $only_gp_species) = @_;
    
    # make sure we got a DB
    if (!$dbh || (ref($dbh) ne 'DBI::db'))
    {
        confess "ERROR: GetSpeciesByTaxonomy: Invalid database handler argument!\n";
    }
    
    if (!defined($only_gp_species))
    {
        $only_gp_species = 1;
    }

    # check taxonomy selector
    if (!$selectors ||(ref($selectors) ne 'HASH'))
    {
        confess "ERROR: GetSpeciesByTaxonomy: Invalid taxonomy selector argument!\n";
    }

    my @species;
    my $taxonomy;
    # taxonomy name?
    if ($selectors->{'taxonomy_name'}
        && ($selectors->{'taxonomy_name'} =~ m/^[\w\s]+$/))
    {
        # load taxonomy node
        $taxonomy = new Greenphyl::Taxonomy($dbh, {'selectors' => {'scientific_name' => ['LIKE', '%' . $selectors->{'taxonomy_name'} . '%']}});
    }
      # taxonomy ID?
    elsif ($selectors->{'taxonomy_id'}
           && ($selectors->{'taxonomy_id'} =~ m/^\d+$/))
    {
        # load taxonomy node
        $taxonomy = new Greenphyl::Taxonomy($dbh, {'selectors' => {'id' => $selectors->{'taxonomy_id'} }});
    }

    # found a taxonomy node?
    if ($taxonomy)
    {
        # get all corresponding leaves:
        # -nodes of the subtree have current phylonode ($taxonomy) in their
        #  lineage
        my $sql_query = "SELECT t.id
                         FROM taxonomy t
                             JOIN lineage l ON (t.id = l.taxonomy_id)
                         WHERE l.lineage_taxonomy_id = ?";
                               #+Val: some species code we use do not correspond to species, like ORYSA
                               # AND p.rank = 'species';";
        my $leaf_ids = $dbh->selectcol_arrayref($sql_query, undef, $taxonomy->id)
            or confess 'ERROR: ' . $dbh->errstr;
        # add given taxonomy ID to the list (in case a species tax_id was provided)
        push(@$leaf_ids, $taxonomy->id);
        if ($only_gp_species)
        {
            @species = new Greenphyl::ExtV4Species($dbh, {'selectors' => {'taxonomy_id' => ['IN', @$leaf_ids], 'sequence_count' => ['>', 0],}});
        }
        else
        {
            @species = new Greenphyl::ExtV4Species($dbh, {'selectors' => {'taxonomy_id' => ['IN', @$leaf_ids]}});
        }
    }
    
    return @species;
}




=pod

=head1 ACCESSORS

=head2 ipr

B<Description>: getter for ipr. Wraps default handler to keep only
one IPR of each kind.

B<ArgsCount>: 1

=over 4

=item $self: (Greenphyl::ExtV4Species)

the species.

=back

B<Return>: (array ref)

the array of IPR objects having the species.

=cut

sub ipr
{
    my ($self) = shift();
    # have the family list already been sorted?
    if (!$self->{'ipr'})
    {
        # no, call parent and make unique and sorted results
        $self->SUPER::ipr(@_);
        my %ipr_with_species = ();
        foreach (@{$self->{'ipr'}})
        {
            $ipr_with_species{$_->id} = $_;
        }
        $self->{'ipr'} = [values(%ipr_with_species)];
    }
    return $self->{'ipr'};
}




=pod

=head2 uniprot_sequences

B<Description>: getter for sequences mapped to UniProt.

B<ArgsCount>: 1

=over 4

=item $self: (Greenphyl::ExtV4Species)

the species.

=back

B<Return>: (array ref)

the array of IPR objects having the species.

=cut

sub uniprot_sequences
{
    my ($self) = shift();

    if (!$self->{'uniprot_sequences'})
    {
    
        # get UniProt database identifier
        my $source_db = Greenphyl::SourceDatabase->new(
            $self->{'_dbh'},
            {
                'selectors' =>
                {
                    'name' => $Greenphyl::SourceDatabase::SOURCE_DATABASE_UNIPROT,
                },
            },
        );

        if (!$source_db)
        {
            PrintDebug("Warning: '$Greenphyl::SourceDatabase::SOURCE_DATABASE_UNIPROT' source database not found in db table!");
            cluck "Warning: '$Greenphyl::SourceDatabase::SOURCE_DATABASE_UNIPROT' source database not found in db table!\n";
            return [];
        }
        
        my $sql_query = '
            SELECT DISTINCT ' . join(', ', Greenphyl::ExtV4Sequence->GetDefaultMembers('s')) . '
            FROM sequences s
                JOIN dbxref_sequences ds ON (s.id = ds.sequence_id)
                JOIN dbxref d ON (ds.dbxref_id = d.id)
            WHERE
                d.db_id = ?
                AND s.species_id = ?
        ';
        PrintDebug("SQL QUERY: $sql_query\nBindings: " . $source_db->id . ', ' . $self->id);
        my $sql_sequences = $self->{'_dbh'}->selectall_arrayref($sql_query, { 'Slice' => {} }, $source_db->id, $self->id)
            or confess 'ERROR: Failed to fetch UniProt sequences: ' . $self->{'_dbh'}->errstr;

        $self->{'uniprot_sequences'} = [];
        foreach my $sql_sequence (@$sql_sequences)
        {
            my $sequence = Greenphyl::ExtV4Sequence->new($self->{'_dbh'}, {'load' => 'none', 'members' => $sql_sequence});
            push(@{$self->{'uniprot_sequences'}}, $sequence);
        }

    }

    return $self->{'uniprot_sequences'};
}


=pod

=head2 families_by_level

B<Description>: Count families containing the species by family level.

B<ArgsCount>: 2

=over 4

=item $self: (Greenphyl::ExtV4Species)

the species.

=item $level: (string)

Requested level.

=back

B<Return>: (array ref)

the array of family corresponding to the given level and containing the species.

=cut

sub families_by_level
{
    my ($self, $level) = shift();

    if (!$self->{'families_by_level'})
    {
        $self->{'families_by_level'} = {};
        foreach my $family (@{$self->families})
        {
            $self->{'families_by_level'}->{$family->level} ||= [];
            push(@{$self->{'families_by_level'}->{$family->level}}, $family);
        }
    }

    return $self->{'families_by_level'}->{$level};
}


=pod

=head2 family_count_at_level

B<Description>: Count families containing the species by family level.

B<ArgsCount>: 2

=over 4

=item $self: (Greenphyl::ExtV4Species)

the species.

=item $level: (string)

Requested level.

=back

B<Return>: (integer)

the number of families corresponding to the given level and containing the
species.

=cut

sub family_count_at_level
{
    my ($self, $level) = @_;
    
    if (!defined($level) || ('' eq $level))
    {
        confess 'ERROR: Missing valid level parameter!';
    }

    if (!$self->{'family_count_at_level'})
    {
        $self->{'family_count_at_level'} = {};
        # check the most efficient way to compute
        # if we already got the families, use them
        if ($self->{'families'})
        {
            foreach my $family (@{$self->families})
            {
                $self->{'families_by_level'}->{$family->level} ||= 0;
                $self->{'families_by_level'}->{$family->level}++;
            }
        }
          # otherwise, query database
        else
        {
            my $sql_query = '
                SELECT
                    f.level AS "level",
                    COUNT(f.id) AS "count"
                FROM families f
                    JOIN sequence_count_by_families_species_cache scfs ON (scfs.family_id = f.id)
                WHERE
                    scfs.sequence_count > 0
                    AND scfs.species_id = ?
                GROUP BY f.level;
            ';
            PrintDebug("SQL QUERY: $sql_query\nBindings: " . $self->id);
            # fetch all in a single array looking like [level, count, level, count, level, count, ...]
            my $counts_by_level = $self->{'_dbh'}->selectcol_arrayref($sql_query, { 'Columns' => [1, 2] }, $self->id)
                or confess 'ERROR: Failed to fetch family count by level: ' . $self->{'_dbh'}->errstr;

            # converts array into a hash having level => count
            $self->{'family_count_at_level'} = {@$counts_by_level};
        }
    }

    return $self->{'family_count_at_level'}->{$level};
}


=pod

=head2 ComputeClassifiedSequenceCounts

B<Description>: Count classified sequences of the species by family level.
(both with and without splice-forms)

B<ArgsCount>: 2

=over 4

=item $self: (Greenphyl::ExtV4Species)

the species.

=item $level: (string)

Requested level.

=back

B<Return>: self

=cut

sub ComputeClassifiedSequenceCounts
{
    my ($self, $level) = @_;
    
    if (!defined($level) || ('' eq $level))
    {
        confess 'ERROR: Missing valid level parameter!';
    }

    $self->{'classified_sequence_count_at_level'} = {};
    $self->{'classified_sequence_count_without_splice_at_level'} = {};
    my $sql_query = '
        SELECT
            f.level AS "level",
            SUM(scfs.sequence_count) AS "count",
            SUM(scfs.sequence_count_without_splice) AS "count_without_splice"
        FROM families f
            JOIN sequence_count_by_families_species_cache scfs ON (scfs.family_id = f.id)
        WHERE
            scfs.species_id = ?
        GROUP BY f.level;
    ';
    PrintDebug("SQL QUERY: $sql_query\nBindings: " . $self->id);
    # fetch all in a single array looking like [level, count, level, count, level, count, ...]
    my $counts_by_level = $self->{'_dbh'}->selectall_arrayref($sql_query, { 'Slice' => {} }, $self->id)
        or confess 'ERROR: Failed to fetch classified sequence count by level: ' . $self->{'_dbh'}->errstr;

    # store results
    foreach my $count (@$counts_by_level)
    {
        $self->{'classified_sequence_count_at_level'}->{$count->{'level'}} = $count->{'count'};
        $self->{'classified_sequence_count_without_splice_at_level'}->{$count->{'level'}} = $count->{'count_without_splice'};
    }

    return $self;
}


=pod

=head2 classified_sequence_count_at_level

B<Description>: Count classified sequences of the species by family level.

B<ArgsCount>: 2

=over 4

=item $self: (Greenphyl::ExtV4Species)

the species.

=item $level: (string)

Requested level.

=back

B<Return>: (integer)

the number of species sequences classified in families at the given level.

=cut

sub classified_sequence_count_at_level
{
    my ($self, $level) = @_;
    
    if (!defined($level) || ('' eq $level))
    {
        confess 'ERROR: Missing valid level parameter!';
    }

    if (!$self->{'classified_sequence_count_at_level'})
    {
        $self->ComputeClassifiedSequenceCounts($level);
    }

    return $self->{'classified_sequence_count_at_level'}->{$level};
}


=pod

=head2 classified_sequence_count_without_splice_at_level

B<Description>: Count classified sequences of the species by family level
without counting splice-forms.

B<ArgsCount>: 2

=over 4

=item $self: (Greenphyl::ExtV4Species)

the species.

=item $level: (string)

Requested level.

=back

B<Return>: (integer)

the number of species sequences classified in families at the given level
without including splice-forms.

=cut

sub classified_sequence_count_without_splice_at_level
{
    my ($self, $level) = @_;
    
    if (!defined($level) || ('' eq $level))
    {
        confess 'ERROR: Missing valid level parameter!';
    }

    if (!$self->{'classified_sequence_count_without_splice_at_level'})
    {
        $self->ComputeClassifiedSequenceCounts($level);
    }

    return $self->{'classified_sequence_count_without_splice_at_level'}->{$level};
}




=pod

=head1 METHODS

=pod

=head2 getStringValue

B<Description>: returns Species code.

B<ArgsCount>: 1

=over 4

=item $self: (Greenphyl::ExtV4Species)

the species object.

=back

B<Return>: (string)

the species code.

=cut

sub getStringValue
{
    my ($self) = @_;
    return $self->{'code'};
}


=pod

=head2 DumpExcel, DumpCSV, DumpXML

B<Description>: Returns a string in the given format containing a table with
the requested columns of given species.

B<ArgsCount>: 2

=over 4

=item $object_ref: (array of Greenphyl::ExtV4Species) (R)

The list of species to process.

=item $parameters: (hash ref) (R)

a hash containing the key 'columns' that contains the columns to dump.
Each key is a column name (label) and each corresponding value is a string
containing the chain (dot-separated) of members to call to fetch the value from
each species.

=back

B<Return>: (string)

a string in the given format containing a table with the requested columns of
given species.

B<Example>:

    my $excel_fh;
    if (open($excel_fh, '>species.xls'))
    {
        binmode($excel_fh);
        print {$excel_fh} Greenphyl::ExtV4Species::DumpExcel([$species1, $species2], {'columns' => {'01.Name'=>'name','02.Code'=>'code','03.Common name'=>'common_name'}});
        close($excel_fh);
    }
    ...

=cut

sub DumpExcel
{
    my ($object_ref, $parameters) = @_;

    return Greenphyl::ExtV4Species->DumpTable('excel', $object_ref, $parameters);
}


sub DumpCSV
{
    my ($object_ref, $parameters) = @_;

    return Greenphyl::ExtV4Species->DumpTable('csv', $object_ref, $parameters);
}


sub DumpXML
{
    my ($object_ref, $parameters) = @_;
    
    return Greenphyl::ExtV4Species->DumpTable('xml', $object_ref, $parameters);
}


=pod

=head1 AUTHORS

Mathieu ROUARD (Bioversity), m.rouard@cgiar.org

Valentin GUIGNON (Bioversity), v.guignon@cgiar.org

Matthieu CONTE (Syngenta)

=head1 VERSION

Version 1.0.0

Date 17/04/2012

=head1 SEE ALSO

GreenPhyl documentation, DBObject documentation.

=cut

return 1; # package return
