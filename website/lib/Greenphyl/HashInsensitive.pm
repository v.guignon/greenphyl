=pod

=head1 NAME

Greenphyl::HashInsensitive - Case insensitive hash

=head1 SYNOPSIS

    use Greenphyl::HashInsensitive;
    tie my %the_hash, 'Greenphyl::HashInsensitive';
    $the_hash{toto} = 5:
    print $the_hash{ToTo} . "\n"; # prints '5'
    $the_hash{TOTO} = 8;
    print $the_hash{tOTo} . "\n"; # prints '8'
    print join(', ', keys(%the_hash)) . "\n"; # prints 'toto'

=head1 REQUIRES

Perl5

=head1 EXPORTS

Nothing.

=head1 DESCRIPTION

This modules implements a hash with case insensitive keys
(ie. $hash{Xy} == $hash{xY}). Key case is preserved (when using "keys(%hash)").
Most of this code was "borrowed" from Hash::Case and Hash::Case::Preserve
(Mark Overmeer). These modules were not used because they require too many
dependencies.

=cut

package Greenphyl::HashInsensitive;

use strict;
use warnings;

use Carp qw( carp cluck confess croak );

sub TIEHASH
{
    my $class = shift;
    my $to    = @_ % 2 ? shift : undef;
    my %opts  = (@_, 'add' => $to);
    (bless {}, $class)->init( \%opts );
}

# loads nothing or an array or a hash into current hash
sub native_init
{
    my ($self, $args) = @_;
    my $add = delete($args->{'add'});

    if (!$add)
    {
    }
    elsif (ref($add) eq 'ARRAY')
    {
        $self->addPairs(@$add);
    }
    elsif (ref($add) eq 'HASH')
    {
        $self->addHashData($add);
    }
    else
    {
        confess "ERROR: Cannot initialize the native hash this way!\n";
    }

    return $self;
}

sub addPairs
{
    my $self = shift;
    $self->STORE(shift, shift) while @_;
    return $self;
}


sub addHashData
{
    my ($self, $data) = @_;
    while (my ($k, $v) = each(%$data))
    {
        $self->STORE($k, $v);
    }
    return $self;
}


sub setHash
{
    my ($self, $hash) = @_; # the native implementation is the default.
    %$self = %$hash;
    return $self;
}

sub init
{
    my ($self, $args) = @_;

    $self->{'HCP_data'} = {};
    $self->{'HCP_keys'} = {};

    my $keep = $args->{'keep'} || 'LAST';
    if ($keep eq 'LAST')
    {
        $self->{'HCP_update'} = 1;
    }
    elsif ($keep eq 'FIRST')
    {
        $self->{'HCP_update'} = 0;
    }
    else
    {
        confess "ERROR: Use 'FIRST' or 'LAST' with the option keep.\n";
    }

    return $self->native_init($args);
}

# Maintain two hashes within this object: one to store the values, and
# one to preserve the casing.  The main object also stores the options.
# The data is kept under lower cased keys.

sub FETCH
{
    return $_[0]->{'HCP_data'}{lc($_[1])};
}

sub STORE
{
    my ($self, $key, $value) = @_;
    my $lckey = lc($key);

    if ($self->{'HCP_update'} || !exists $self->{'HCP_keys'}{$lckey})
    {
        $self->{'HCP_keys'}{$lckey} = $key;
    }

    return $self->{'HCP_data'}{$lckey} = $value;
}

sub FIRSTKEY
{
    my $self = shift;
    my $a = scalar keys(%{$self->{'HCP_keys'}});
    return $self->NEXTKEY;
}

sub NEXTKEY
{
    my $self = shift;
    if (my ($k, $v) = each(%{$self->{'HCP_keys'}}))
    {
        return wantarray ? ($v, $self->{'HCP_data'}{$k}) : $v;
    }
    else
    {
        return ();
    }
}

sub EXISTS
{
    return exists($_[0]->{'HCP_data'}{lc($_[1])});
}

sub DELETE
{
    my $lckey = lc($_[1]);
    delete($_[0]->{'HCP_keys'}{$lckey});
    return delete($_[0]->{'HCP_data'}{$lckey});
}

sub CLEAR
{
    %{$_[0]->{'HCP_data'}} = ();
    %{$_[0]->{'HCP_keys'}} = ();
}

=pod

=head1 AUTHORS

Valentin GUIGNON (CIRAD), valentin.guignon@cirad.fr

=head1 VERSION

Version 1.0.0

Date 27/01/2011

=head1 SEE ALSO

Hash::Case, Hash::Case::Preserve

=cut

return 1;
