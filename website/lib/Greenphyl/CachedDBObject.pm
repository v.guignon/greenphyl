=pod

=head1 NAME

Greenphyl::CachedDBObject - GreenPhyl Cached database Object

=head1 SYNOPSIS

    package Greenphyl::Family;
    use Greenphyl::CachedDBObject;
    use base qw(Greenphyl::CachedDBObject);
    ...
    return 1;

=head1 REQUIRES

Perl5

=head1 EXPORTS

Nothing

=head1 DESCRIPTION

This module implements a GreenPhyl cache for database objects. Fetching objects
into cache is based on object keys but can also work with alternative keys if
needed (for instance, for sequences, one would either use seq_id or seq_textid).

Each object type has its own cache for each database separately. Cache can be
flushed at any time in various ways by calling the ClearCache method (see
method documentation for details).

#+TODO: add support for alternate key tuples (ie. ['accession,db_id'] because
we can't use ['accession', 'db_id'])

=cut

package Greenphyl::CachedDBObject;

use strict;
use warnings;

use base qw(Greenphyl::DBObject);

use vars qw($AUTOLOAD);

use Carp qw(cluck confess croak);

use Greenphyl;




# to disable caching uncomment the 2 following lines:
# return 1;
# __END__




# Package constants
####################

=pod

=head1 CONSTANTS

B<$DEBUG>: (boolean)

enable/disable debug mode.

=cut

our $DEBUG = 0;
our $KEY_SEPARATOR = $Greenphyl::DBObject::KEY_SEPARATOR;




# Package variables
####################

=pod

=head1 VARIABLES

B<$g_global_cache>: (hash ref) (private)

Contains cache for all instances.
First-level keys correspond to module names.
Second-level keys correspond to database handlers.
Third-level keys correspond to object keys as in the property handler hash.
Values are object references.

B<$g_alternate_keys>: (hash ref) (private)

Hash containing association between alternative key values and their
corresponding key values.
First-level keys correspond to module names.
Second-level keys correspond to alternative key name.
Thirs-level keys correspond to alternative key value.
Values are associated object key values.

=cut

my $g_global_cache = {};
my $g_alternate_keys = {};



# Package subs
###############

=pod

=head1 STATIC METHODS

=head2 CONSTRUCTOR

B<Description>: Creates a new instance of a GreenPhyl CachedDBObject.

B<ArgsCount>: 3

=over 4

=item $property_handlers: (hash ref) (R)

see DBObject constructor documentation for details.

=item $dbh: (DBI) (R)

Database handler of the database to load the object from.

=item $parameters: (hash ref) (R)

See Greenphyl::DBObject constructor documentation for details.

=back

B<Return>: (Greenphyl::CachedDBObject)

a new instance.

B<Caller>: Database object modules.

=cut

sub new
{
    my ($proto, $property_handlers, $dbh, $parameters) = @_;
    my $class = ref($proto) || $proto;

    $g_global_cache->{$class}   ||= {};
    $g_alternate_keys->{$class} ||= {};

    # check if we got a database handler, meaning we got something to cache
    if (!$dbh)
    {
        return $class->SUPER::new($property_handlers, undef, $parameters);
    }

    # init DB cache if needed
    $g_global_cache->{$class}->{$dbh} ||= {};

    my $loading_selectors = {};
    if (exists($parameters->{'selectors'}))
    {
        $loading_selectors = $parameters->{'selectors'};
    }
    elsif (exists($parameters->{'members'}))
    {
        $loading_selectors = $parameters->{'members'};
    }

    my ($key_name, $key_value) = $class->GetKeyFromSelectors($property_handlers, $loading_selectors);

    # check if the object is in cache
    my ($self, @new_instances);
    if ($key_value
        && exists($g_global_cache->{$class}->{$dbh}->{$key_value})
        && $g_global_cache->{$class}->{$dbh}->{$key_value})
    {
        PrintDebug($class . "($key_value) loaded from cache!");
        $self = $g_global_cache->{$class}->{$dbh}->{$key_value};
        # if members are provided, add them only for missing properties
        if ($parameters->{'members'})
        {
            foreach my $property_name (keys(%{$parameters->{'members'}}))
            {
                # only missing: no overload from cache data
                $self->{$property_name} ||= $parameters->{'members'}->{$property_name};
            }
        }
        # make sure other selectors are matched and if not, don't use cached value and reload from DB
        my @selectors = keys(%{$parameters->{'selectors'}});
        while ($self && @selectors)
        {
            my $selector = shift(@selectors);
            # check for a single value or something else with an operator
            if (ref($parameters->{'selectors'}->{$selector}))
            {
                # select how to manage the operator to see if selector matches
                my %operator_handlers;
                %operator_handlers = (
                    '='    => sub { my ($a, $b) = @_; return $a eq $b;},
                    '<=>'  => sub { my ($a, $b) = @_; return $a eq $b;},
                    '<>'   => sub { my ($a, $b) = @_; return $a ne $b;},
                    '!='   => sub { return $operator_handlers{'<>'}->(@_);},
                    '<'    => sub { my ($a, $b) = @_; return $a lt $b;},
                    '<='   => sub { my ($a, $b) = @_; return $a le $b;},
                    '>'    => sub { my ($a, $b) = @_; return $a gt $b;},
                    '>='   => sub { my ($a, $b) = @_; return $a ge $b;},
                    'IN'   => sub
                        {
                            my ($a, @values) = @_;
                            # handle "IN (SELECT ...)" case
                            if ($values[0] =~ m/^[\s\r\n]*\(?[\s\r\n]*SELECT[\s\r\n]/is)
                            {
                                # we can't check for SELECT subquery
                                return 0;
                            }
                            foreach (@values)
                            {
                                if ($a eq $_)
                                {
                                    return 1;
                                }
                            }
                            return 0;
                        },
                    'NOT IN' => sub
                        {
                            return !$operator_handlers{'IN'}->(@_);
                        },
                    'LIKE' => \&_MatchUsingMySQLLike,
                    'NOT LIKE' => sub
                        {
                            return !_MatchUsingMySQLLike(@_);
                        },
                );
                my ($operator, @values) = @{$parameters->{'selectors'}->{$selector}};
                if (exists($operator_handlers{$operator}))
                {
                    if (!$operator_handlers{$operator}->($self->{$selector}, @values))
                    {
                        # not matching
                        $self = undef;
                    }
                }
                else
                {
                    # unsupported operator, ask for reload
                    $self = undef;
                    cluck "WARNING: CachedDBObject: could not check cached object against unsupported operator '$operator' provided by selectors! Asssuming cached object does not match.\n";
                }
            }
            else
            {
                # single value check
                if ($parameters->{'selectors'}->{$selector} ne $self->{$selector})
                {
                    # not matched, unset $self
                    $self = undef;
                }
            }
        }
    }
    # check if we succeeded to load the object from cache
    if (!$self)
    {
        PrintDebug("Object ($class) not found in cache, load it normaly" . ($key_value?" (using key: '$key_value')":''));
        # instance creation
        if (wantarray())
        {
            ($self, @new_instances) = $class->SUPER::new($property_handlers, $dbh, $parameters);
        }
        else
        {
            $self = $class->SUPER::new($property_handlers, $dbh, $parameters);
        }
        # only save the first instance since the others will be saved after as
        # the constructor will be called for each of them as single instances
        if ($self && $self->{$key_name})
        {
            PrintDebug("saving " . $class . "($key_name = '" . $self->{$key_name} . "') into cache!");
            $g_global_cache->{$class}->{$dbh}->{$self->{$key_name}} = $self;
        }
        else
        {
            PrintDebug("object ($class) not found in database!");
        }
    }

    # save alternate keys association
    if ($self)
    {
        # -process each alternate key name
        foreach my $alternate_key_name (@{$self->{'_properties'}->{'alternate_keys'}})
        {
            # init alternate key => key associative hash
            $g_alternate_keys->{$class}->{$alternate_key_name} ||= {};
            if ($self->{$alternate_key_name})
            {
                # save value
                $g_alternate_keys->{$class}->{$alternate_key_name}->{$self->{$alternate_key_name}} = $self->{$key_name};
            }
        }
    }

    # check calling context
    if (wantarray())
    {
        # if self is not initialized (no match), return an empty list
        if (!$self || !$self->isa('Greenphyl::DBObject'))
        {
            return ();
        }

        # array of instances
        return ($self, @new_instances);
    }
    else
    {
        # just want one object reference (scalar context)
        return $self;
    }
}


=pod

=head2 _MatchUsingMySQLLike

B<Description>: Tests the given string against a MySQL LIKE expression and
returns the match result.

B<ArgsCount>: 2

=over 4

=item $string: (string) (R)

The string to test.

=item $like_exp: (string) (R)

A MySQL LIKE expression to use.

=back

B<Return>: (boolean)

True if the string matches the expression, false otherwise.

=cut

sub _MatchUsingMySQLLike
{
    my ($string, $like_exp) = @_;
    # translate MySQL matching expression into a PERL regular expression
    # see http://dev.mysql.com/doc/refman/5.0/en/string-literals.html
    # and http://perldoc.perl.org/perlrebackslash.html
    # and http://perldoc.perl.org/perlre.html
    my @regexp_chars = split(//, $like_exp);
    my @regexp;
    while (my $char = shift(@regexp_chars))
    {
        if ("\\" eq $char)
        {
            # escape sequence...
            $char = shift(@regexp_chars);
            if ("0" eq $char)
            {
                # NULL character
                push(@regexp, "\\x00");
            }
            elsif ("'" eq $char)
            {
                # escaped single quote
                push(@regexp, "'");
            }
            elsif ('"' eq $char)
            {
                # escaped double quote
                push(@regexp, '"');
            }
            elsif ('b' eq $char)
            {
                # backspace
                push(@regexp, "\\x08");
            }
            elsif ('n' eq $char)
            {
                # newline
                push(@regexp, "\\n");
            }
            elsif ('r' eq $char)
            {
                # carriage return
                push(@regexp, "\\r");
            }
            elsif ('t' eq $char)
            {
                # tab
                push(@regexp, "\\t");
            }
            elsif ('Z' eq $char)
            {
                # Control+Z (\cZ)
                push(@regexp, "\\x1A");
            }
            elsif ("\\" eq $char)
            {
                # backslash
                push(@regexp, "\\\\");
            }
            elsif ("%" eq $char)
            {
                # percent
                push(@regexp, "%");
            }
            elsif ("_" eq $char)
            {
                # underscore
                push(@regexp, "_");
            }
            else
            {
                # non-special, treat it in the main loop
                unshift(@regexp_chars, $char);
            }
        }
        elsif (("'" eq $char) && @regexp_chars && ("'" eq $regexp_chars[0]))
        {
            # '' case
            shift(@regexp_chars);
            push(@regexp, "'");
        }
        elsif (('"' eq $char) && @regexp_chars && ('"' eq $regexp_chars[0]))
        {
            # "" case
            shift(@regexp_chars);
            push(@regexp, '"');
        }
        elsif ("_" eq $char)
        {
            # single jocker character
            push(@regexp, '.');
        }
        elsif ("%" eq $char)
        {
            # wildcard character
            push(@regexp, '.*');
        }
        elsif ($char =~ m/^[A-Za-z0-1]$/)
        {
            # regular alpha-numeric characters
            push(@regexp, $char);
        }
        else
        {
            # escape other characters
            push(@regexp, sprintf("\\x%X", ord($char)));
        }
    }
    my $regexp = join('', @regexp);
    return $string =~ m/^$regexp$/i;
}


=pod

=head2 ValidateAlternateKeyValue

B<Description>: returns true if the alternate key value can be used as
alternative to find the associated default key value.

Note: this function should be overriden by derivated classes in order to prevent
the use of invalid alternative key values such as empty values or for instance
'Unannotated cluster', 'unknown', 'NULL', ...

B<ArgsCount>: 3

=over 4

=item $class: (CachedDBObject derived class) (R)

a derived class.

=item $alternate_key_name: (string) (R)

the alternate key name.

=item $alternate_key_value: (scalar) (R)

the alternate key value to check and validate.

=back

B<Return>: (boolean)

True if the alternate key value can be used, false otherwise. The default
implementation returns the alternate key value.

=cut

sub ValidateAlternateKeyValue
{
    my ($class, $alternate_key_name, $alternate_key_value) = @_;
    return $alternate_key_value;
}


=pod

=head2 GetKeyFromSelectors

B<Description>: returns the key name and value from a selectors hash if
available.

B<ArgsCount>: 3

=over 4

=item $class: (CachedDBObject derived class) (R)

a derived class.

=item $key_parameters: (hash ref) (R)

a hash containing a key 'key' with a key name as value and an option key
'alternate_keys' containing an array of alternative key names.
The class property handler can be used as $key_parameters.

=item $selectors: (hash ref) (R)

the selectors hash.

=back

B<Return>: (list)

the key name and value and eventually, if used, the alternative key name.

B<Example>:

    my ($key_name, $key_value) = Greenphyl::Sequence->GetKeyFromSelectors($property_handlers, $selectors);

=cut

sub GetKeyFromSelectors
{
    my ($class, $key_parameters, $selectors) = @_;

    # check if a key has been specified (in order to use cache)
    my $key_name = $key_parameters->{'key'} or confess "ERROR: object has no key!\n";
    # check if it's a multiple key
    my @keys = split(/\s*$KEY_SEPARATOR\s*/, $key_name);
    # check if key value can be fetched from selectors
    my @key_values;
    foreach (@keys)
    {
        if (('HASH' eq ref($selectors))
            && exists($selectors->{$_}) # key in selector
            && !ref($selectors->{$_})) # just one value using the 'equal' operator
        {
            push(@key_values, $selectors->{$_});
        }
    }
    my $key_value;
    # make sure we got exactly one value for each key before setting $key_value
    if (scalar(@key_values) == scalar(@keys))
    {
        $key_value = join("\n", @key_values);
    }

    my $alternate_key_name;
    # check if no key has been found
    if (!$key_value
        && exists($key_parameters->{'alternate_keys'}))
    {
        # no key found, try alternative keys
        my @alternative_keys = @{$key_parameters->{'alternate_keys'}};
        while (!$key_value && @alternative_keys)
        {
            $alternate_key_name = shift(@alternative_keys);
            PrintDebug("trying alternative key $alternate_key_name");
            (undef, $key_value) = $class->GetKeyFromSelectors({'key' => $alternate_key_name}, $selectors);

            # check if we found something
            if ($key_value)
            {
                # convert alternative key into key
                if (exists($g_alternate_keys->{$class}->{$alternate_key_name})
                    && $class->ValidateAlternateKeyValue($alternate_key_name, $key_value))
                {
                    $key_value = $g_alternate_keys->{$class}->{$alternate_key_name}->{$key_value};
                    if ($key_value)
                    {PrintDebug("using alternative key $alternate_key_name ('$key_value') to fetch object from cache");}
                }
                else
                {
                    $key_value = undef;
                }
            }
        }
    }
    return ($key_name, $key_value, $alternate_key_name);
}


=pod

=head2 ClearCache

B<Description>: Clear the cache as requested by the parameters.
Support non-static calls: in that case, only the calling object will be removed
from cache.

B<ArgsCount>: 2

=over 4

=item $proto: (CachedDBObject or derived class) (R)

an instance of a derived class name.

=item $cache_element: (any) (R)

* if the $cache_element is a database handler, all cache of any type of object
  for this database will be cleared.

* if the $cache_element is an object type (class/module name), all the cache from
  all database for this type will be cleared.

* if the $cache_element an array ref containing an object type and a database
  handler, all the cache of this object type for this database will be cleared.

* if the $cache_element is a reference to a CachedObject-derivated class, this
  object will be removed from cache.
  
* if no $cache_element is specified, the calling prototype will be used:
  -if it's an object, that object will be removed from cache
  -if it's an object type, all the cache from all database for that object type
   will be cleard
  -if it's CachedDBObject class, all the cache from any database and any type
   of object will be cleared.

=back

B<Return>:

nothing.

B<Examples>:

    # remove an object from cache (equivalent syntaxes):
    $sequence->ClearCache();
    Greenphyl::Sequence->ClearCache($sequence);
    Greenphyl::CachedDBObject->ClearCache($sequence);
    
    # clear all cache concerning a database for a specific object type:
    $sequence->ClearCache($dbh);
    Greenphyl::Sequence->ClearCache($dbh);
    Greenphyl::CachedDBObject->ClearCache(['Greenphyl::Sequence', $dbh]);

    # clear all cache concerning a database for any object type:
    Greenphyl::CachedDBObject->ClearCache($dbh);

    # clear all cache concerning an object type for any database:
    $sequence->ClearCache('Greenphyl::Sequence');
    Greenphyl::Sequence->ClearCache();
    Greenphyl::CachedDBObject->ClearCache('Greenphyl::Sequence');

    # clear all cache from any databases and object types:
    Greenphyl::CachedDBObject->ClearCache();

=cut

sub ClearCache
{
    my ($proto, $cache_element) = @_;

    # arguments check
    if (2 < @_)
    {
        confess "ERROR: CachedDBObject->ClearCache called with a wrong number of arguments!\n";
    }

    # get the class
    my $class = ref($proto) || $proto;

    # check calling context
    if ($cache_element)
    {
        # we got a $cache_element parameter...
        # $cache_element is an object type
        if (exists($g_global_cache->{$cache_element}))
        {
            # clear all database cache for this object type
            PrintDebug("clearing all cache for object type '$cache_element'!");
            $g_global_cache->{$cache_element} = {};
        }
          # $cache_element is a database handler
        elsif (ref($cache_element) eq 'DBI::db')
        {
            # check if called for an object type or any type
            if ($class eq 'Greenphyl::CachedDBObject')
            {
                PrintDebug("clearing database '" . $cache_element->Name . "' ($cache_element) cache for any type of object!");

                # remove database cache for any object type
                foreach (keys(%$g_global_cache))
                {
                    $g_global_cache->{$_}->{$cache_element} = {};
                }
            }
            else
            {
                # remove database cache for specified object type
                PrintDebug("clearing database '" . $cache_element->Name . "' ($cache_element) cache for object type '$class'!");
                $g_global_cache->{$class}->{$cache_element} = {};
            }
        }
          # $cache_element is an array ref
        elsif (ref($cache_element) eq 'ARRAY')
        {
            # clear database cache for the given object type
            if (!(ref($cache_element->[1]) eq 'DBI::db'))
            {
                confess "ERROR: CachedDBObject->ClearCache: Invalid array ref argument! Second array element must be a database handler!\n";
            }
            elsif (!exists($g_global_cache->{$cache_element->[0]}))
            {
                confess "ERROR: CachedDBObject->ClearCache: first array element is not a cached object type!\n";
            }
            PrintDebug("clearing database '" . $cache_element->[1]->Name . "' ($cache_element->[1]) cache for object type '$cache_element->[0]'!");
            $g_global_cache->{$cache_element->[0]}->{$cache_element->[1]} = {};
        }
          # $cache_element is an object reference
        elsif (ref($cache_element)
               && exists($cache_element->{'_dbh'})
               && exists($g_global_cache->{$class}->{$cache_element->{'_dbh'}}))
        {
            # remove object from cache
            PrintDebug("removing object '$cache_element' ($class) from cache!");
            delete($g_global_cache->{$class}->{$cache_element->{'_dbh'}}->{$cache_element});
        }
        else
        {
            confess "ERROR: CachedDBObject->ClearCache: don't know how to handle the given element ($cache_element)! Please check the documentation, you may have not provided the correct input.\n";
        }
    }
    else
    {
        # global cache clear?
        if ($class eq 'Greenphyl::CachedDBObject')
        {
            # clear all cache
            PrintDebug("clearing all cache!");
            foreach (keys(%$g_global_cache))
            {
                $g_global_cache->{$_} = {};
            }
        }
          # object type clear?
        elsif (!ref($proto) && exists($g_global_cache->{$class}))
        {
            # clear all cache for the given object type
            PrintDebug("clearing all cache for object type '$class'!");
            $g_global_cache->{$class} = {};
        }
          # remove instance from cache
        elsif (ref($proto) && exists($proto->{'_dbh'}))
        {
            if (exists($g_global_cache->{$class}))
            {
                PrintDebug("removing object '$proto' ($class) from cache!");
                delete($g_global_cache->{$class}->{$proto->{'_dbh'}}->{$proto});
            }
            else
            {
                confess "ERROR: CachedDBObject->ClearCache: an unexpected error occured: couldn't remove given object from cache! It seems that the object type cache space as been abnormaly removed or has not been initialized correctly!\n";
            }
        }
        else
        {
            confess "ERROR: CachedDBObject->ClearCache: unable to determine which cache to clear!\n";
        }
    }
}


=pod

=head2 reload

B<Description>: reload current instance members from database.

B<ArgsCount>: 1-2

=over 4

=item $self: (CachedDBObject) (R)

the object.

=item $members: (array ref) (O)

an array of base member names to reload.

=back

B<Return>: (DBObject)

The DBObject (evaluated to TRUE if the object exists in database and FALSE otherwise).

=cut

sub reload
{
    my ($self, $members) = @_;
    
    if (!$self)
    {
        confess "ERROR: Invalid object for reload! Not a static function: use '->' operator to call it.\n";
    }

    my $class = ref($self);

    # remove from cache
    $self->ClearCache();
    $self->SUPER::reload($members);
}



=pod

=head1 AUTHORS

Mathieu ROUARD (Bioversity), m.rouard@cgiar.org

Valentin GUIGNON (Bioversity), v.guignon@cgiar.org

Matthieu CONTE (Syngenta)

=head1 VERSION

Version 1.0.0

Date 17/04/2012

=head1 SEE ALSO

GreenPhyl documentation, DBObject documentation.

=cut

return 1; # package return
