=pod

=head1 NAME

Greenphyl::OpenID - GreenPhyl OpenID Object

=head1 SYNOPSIS

    use Greenphyl::OpenID;
    my $openid = Greenphyl::OpenID->new($dbh, {'selectors' => {'name' => 'Google',}});
    print $openid->identity();

=head1 REQUIRES

Perl5

=head1 EXPORTS

Nothing

=head1 DESCRIPTION

This module implements a GreenPhyl OpenID database object.

=cut

package Greenphyl::OpenID;

use strict;
use warnings;

use base qw(Greenphyl::CachedDBObject);

use vars qw($AUTOLOAD);

use Carp qw(cluck confess croak);




# Package constants
####################

=pod

=head1 CONSTANTS

B<$OBJECT_PROPERTIES>: (hash ref)

object properties description. See DBObject constructor documentation for
details.

=cut

our $OBJECT_PROPERTIES = {
    'table' => 'openids',
    'key' => 'name',
    'alternate_keys' => [],
    'load'     => [
        'enabled',
        'icon_index',
        'icon_size',
        'label',
        'name',
        'url',
    ],
    'alias' => {
    },
    'base' => {
        'enabled' => {
            'property_column' => 'enabled',
        },
        'icon_index' => {
            'property_column' => 'icon_index',
        },
        'icon_size' => {
            'property_column' => 'icon_size',
        },
        'label' => {
            'property_column' => 'label',
        },
        'name' => {
            'property_column' => 'name',
        },
        'url' => {
            'property_column' => 'url',
        },
    },
    'secondary' => {
    },
    'tertiary' => {
    },

};




# Package subs
###############

=pod

=head1 STATIC METHODS

=head2 CONSTRUCTOR

B<Description>: Creates a new instance of a GreenPhyl OpenID object.

B<ArgsCount>: 2

=over 4

=item $dbh: (DBI::db) (U)

Database handler of the database to load the object from.

=item $parameters: (hash ref) (R)

See Greenphyl::DBObject constructor documentation for details.

=back

B<Return>: (Greenphyl::OpenID)

a new instance.

B<Caller>: General

B<Example>:

    my $openid = Greenphyl::OpenID->new($dbh, {'selectors' => {'name' => 'Google',}});

=cut

sub new
{
    my $proto = shift();
    my $class = ref($proto) || $proto;
    
    my $object_properties = $class->GetDefaultClassProperties();

    return $class->SUPER::new($object_properties, @_);
}




=pod

=head1 AUTHORS

Mathieu ROUARD (Bioversity), m.rouard@cgiar.org

Valentin GUIGNON (Bioversity), v.guignon@cgiar.org

Matthieu CONTE (Syngenta)

=head1 VERSION

Version 1.0.0

Date 23/01/2014

=head1 SEE ALSO

GreenPhyl documentation, DBObject documentation.

=cut

return 1; # package return
