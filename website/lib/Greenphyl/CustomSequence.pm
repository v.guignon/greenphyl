=pod

=head1 NAME

Greenphyl::CustomSequence - GreenPhyl Sequence Object

=head1 SYNOPSIS

    use Greenphyl::CustomSequence;
    my $sequence = Greenphyl::CustomSequence->new($dbh, {'selectors' => {'accession' => ['LIKE', 'Os01g01050.1']}});
    print $sequence->name();

=head1 REQUIRES

Perl 5.8.0, Greenphyl::[Species, SequenceIPR, Family, IPR, DBXRef, Homology],
BioPerl

=head1 EXPORTS

nothing

=head1 DESCRIPTION

This module implements a GreenPhyl sequence database object.

=cut

package Greenphyl::CustomSequence;

use strict;
use warnings;

use base qw(Greenphyl::AbstractSequence);

use vars qw($AUTOLOAD);

use Carp qw(cluck confess croak);

use Greenphyl;
use Greenphyl::GO;
use Greenphyl::SourceDatabase;
use Greenphyl::DBXRefLinks;
use Greenphyl::Species; # for species code regexp
use Greenphyl::Homology;
use Greenphyl::Tools::Sequences;
use Greenphyl::Tools::Taxonomy;

use IO::String;
use Bio::SeqIO;
use Bio::Seq;




# Package constants
####################

=pod

=head1 CONSTANTS

B<$OBJECT_PROPERTIES>: (hash ref)

sequence object properties description. See DBObject constructor documentation
for details.

B<$MAX_IPR_LAYERS>: (integer)

Maximum number of layers a sequence can have for IPR domain rendering.

=cut

our $DEBUG = 0;

our $OBJECT_PROPERTIES = {
    'table' => 'custom_sequences',
    'key' => 'id',
    'alternate_keys' => ['accession'],
    'load'     => [
        'accession',
        'annotation',
        # 'data',
        'id',
        'last_update',
        'length',
        'locus',
        'organism',
        # 'polypeptide',
        'species_id',
        'splice',
        'splice_priority',
        'user_id',
    ],
    'alias' => {
        'seq_id'       => 'id',
        'sequence_id'  => 'id',
        'seq_length'   => 'length',
        'name'         => 'accession',
        'seq_textid'   => 'accession',
        'textid'       => 'accession',
        'gene_name'    => 'locus',
    },
    'base' => {
        'annotation' => {
            'property_column' => 'annotation',
        },
        'id' => {
            'property_column' => 'id',
        },
        'length' => {
            'property_column' => 'length',
        },
        'accession' => {
            'property_column' => 'accession',
        },
        'data' => {
            'property_column' => 'data',
            'serialized'      => 1,
        },
        'last_update' => {
            'property_column' => 'last_update',
        },
        'locus' => {
            'property_column' => 'locus',
        },
        'organism' => {
            'property_column' => 'organism',
        },
        'splice' => {
            'property_column' => 'splice',
        },
        'splice_priority' => {
            'property_column' => 'splice_priority',
        },
        'polypeptide' => {
            'property_column' => 'polypeptide',
        },
        'species_id' => {
            'property_column' => 'species_id',
        },
        'user_id' => {
            'property_column' => 'user_id',
        },
    },
    'secondary' => {
        'species' => {
            'object_key'      => 'species_id',
            'property_table'  => 'species',
            'property_key'    => 'id',
            'multiple_values' => 0,
            'property_module' => 'Greenphyl::Species',
        },
        'associated_sequence_ids' => {
            'property_table'  => 'custom_sequences_sequences_relationships',
            'property_key'    => 'custom_sequence_id',
            'multiple_values' => 1,
            'property_column' => 'sequence_id',
        },
    },
    'tertiary' => {
        'families' => {
            'link_table'        => 'custom_families_sequences',
            'link_object_key'   => 'custom_sequence_id',
            'link_property_key' => 'custom_family_id',
            'multiple_values'   => 1,
            'property_module'   => 'Greenphyl::CustomFamily',
            'property_table'    => 'custom_families',
            'property_key'      => 'id',
        },
        'synonyms' => {
            'link_table'        => 'custom_sequences_sequences_relationships',
            'link_object_key'   => 'custom_sequence_id',
            'link_property_key' => 'sequence_id',
            'multiple_values'   => 1,
            'property_column'   => 'accession',
            'property_table'    => 'sequences',
            'property_key'      => 'id',
        },
        'sequences' => {
            'link_table'        => 'custom_sequences_sequences_relationships',
            'link_object_key'   => 'custom_sequence_id',
            'link_property_key' => 'sequence_id',
            'property_table'    => 'sequences',
            'property_key'      => 'id',
            'multiple_values'   => 1,
            'property_module'   => 'Greenphyl::Sequence',
        },
    },
    'managed' => {
        'dbxref' => {
            'function' => \&OperateSynonymSequenceMember,
        },
        'ipr' => {
            'function' => \&OperateSynonymSequenceMember,
        },
        'go' => {
            'function' => \&OperateSynonymSequenceMember,
        },
        'filtered' => {
            'constant' => 0,
        },
    },
};

our $SUPPORTED_DUMP_FORMAT = $Greenphyl::AbstractSequence::SUPPORTED_DUMP_FORMAT;

our $SEQUENCE_RELATIONSHIP_OTHER      = 'other';
our $SEQUENCE_RELATIONSHIP_SYNONYM    = 'synonym';
our $SEQUENCE_RELATIONSHIP_ASSOCIATED = 'associated';




# Package subs
###############

=pod

=head1 STATIC METHODS

=head2 CONSTRUCTOR

B<Description>: Creates a new instance of a GreenPhyl sequence object.

B<ArgsCount>: 1-2

=over 4

=item $fasta_content: (DBI::db) (O)

A FASTA content to load the sequences from.

=item $dbh: (DBI) (U)

Database handler of the database to load the object from.

=item $parameters: (hash ref) (R)

See Greenphyl::DBObject constructor documentation for details.

=back

B<Return>: (Greenphyl::CustomSequence)

a new instance.

B<Caller>: General

B<Example>:

    my $sequence = new Greenphyl::CustomSequence($dbh, {'selectors' => {'accession' => ['LIKE', 'Os01g01050%']}});
    my $fasta_sequence = new Greenphyl::CustomSequence(">Os01g01050.1_ORYSA\nMINFAVQSRCAPTNSLCSCVALCERNFKLEQVEELASLIKDNLYSKHLVLSTEETLVGIL");

=cut

sub new
{
    my ($proto) = shift();
    my $class = ref($proto) || $proto;

    return $class->SUPER::new(@_);
}



=pod

=head1 METHODS

=head2 species

B<Description>: returns the species corresponding to this sequence. For custom
species, the organism name given by the custom sequence object is used for
the species.

B<ArgsCount>: 1

=over 4

=item $self: (Greenphyl::CustomSequence)

the sequence.

=back

B<Return>: (Greenphyl::Species)

the associated species (Greenphyl::Species).

=cut

sub species
{
    my ($self) = shift();
    my $species = $self->SUPER::species(@_);
    
    if ($species && (GP('NONUNIPROT_SPECIES_CODE') eq $species->code))
    {
        my $species_data = $species->getHashValue();
        $species_data->{'organism'} = $self->organism;
        $species_data->{'color'} = ComputeSpeciesColor($self->organism);
        # $species_data->{'common_name'} = '';
        $species = Greenphyl::Species->new($self->{'_dbh'}, {'load' => 'none', 'members' => $species_data,});
    }

    return $species;
}
sub getSpecies; *getSpecies = \&species;


=pod

=head2 getAssociatedSequences

B<Description>: returns the non-custom sequence objects related to this sequence.

B<ArgsCount>: 1

=over 4

=item $self: (Greenphyl::CustomSequence)

the sequence.

=back

B<Return>: (array ref)

the associated sequences (Greenphyl::Sequence).

=cut

sub getAssociatedSequences
{
    my ($self, $type) = @_;

    my $associated_sequence_ids;
    if ($type)
    {
        $associated_sequence_ids = $self->fetch_associated_sequence_ids({'selectors' => {'type' => $type}});
    }
    else
    {
        $associated_sequence_ids = $self->associated_sequence_ids;
    }

    my @associated_sequences;
    foreach my $sequence_id (@$associated_sequence_ids)
    {
        my $sequence = Greenphyl::Sequence->new($self->{'_dbh'},{'selectors' => {'id' => $sequence_id,},},);
        if ($sequence)
        {
            push(@associated_sequences, $sequence);
        }
    }

    return \@associated_sequences;
}


=pod

=head2 isOwner

B<Description>: Tells if the given user is the owner of this custom sequence.

B<ArgsCount>: 1-2

=over 4

=item $self: (Greenphyl::CustomSequence) (R)

Current sequence.

=item $user: (Greenphyl::User) (O)

The user to check.
Default: current user.

=back

B<Return>: (boolean)

True if the user is the owner of the sequence.

=cut

sub isOwner
{
    my ($self, $user) = @_;
    
    if (IsRunningCGI())
    {
        eval "use Greenphyl::Web::Access; \$user ||= GetCurrentUser();";
    }
    
    my $is_owner = 0;
    
    if ($user && $user->id && ($user->id == $self->user_id))
    {
        $is_owner = 1;
    }
    
    return $is_owner;
}


=pod

=head2 OperateSynonymSequenceMember

B<Description>: Returns related member value given from synonym sequences.

B<ArgsCount>: 3-4

=over 4

=item $self: (Greenphyl::CustomSequence) (R)

Current sequence.

=item $member: (string) (R)

Member field.

=item $accessor: (string) (R)

Accessor to use.

=item $value: (scalar) (O)

Value to set or selection parameters or SQL directives or...

=back

B<Return>: (any)

Depends on the method used, it will return the same thing as
Greenphyl::Sequence->$accessor$member. In case of an array, all the array from
all the associated synonym sequences are merged and returned.

=cut

sub OperateSynonymSequenceMember
{
    my ($self, $member, $accessor, $value) = @_;

    my $values;
    my $method_name = $accessor . '_' . $member;
    foreach my $sequence (@{$self->getAssociatedSequences($SEQUENCE_RELATIONSHIP_SYNONYM)})
    {
        my $sub_values;
        eval "\$sub_values = \$sequence->$method_name(\$value);";
        if (!$@ && $sub_values)
        {
            if ('ARRAY' eq ref($sub_values))
            {
                $values ||= [];
                push(@$values, @$sub_values);
            }
            elsif (defined($sub_values))
            {
                $values = $sub_values;
            }
        }
    }

    return $values;
}


=pod

=head1 AUTHORS

Mathieu ROUARD (Bioversity), m.rouard@cgiar.org

Valentin GUIGNON (Bioversity), v.guignon@cgiar.org

Matthieu CONTE (Syngenta)

=head1 VERSION

Version 1.0.0

Date 11/10/2013

=head1 SEE ALSO

GreenPhyl documentation.

=cut

return 1; # package return
