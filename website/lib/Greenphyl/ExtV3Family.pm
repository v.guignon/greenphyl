=pod

=head1 NAME

Greenphyl::ExtV3Family - GreenPhyl v3 Family Object

=head1 SYNOPSIS

    use Greenphyl::ExtV3Family;
    my $v3_family = Greenphyl::ExtV3Family->new($dbh, {'selectors' => {'family_id' => 205}});
    print $v3_family->name();

=head1 REQUIRES

Perl5

=head1 EXPORTS

Nothing

=head1 DESCRIPTION

This module implements a GreenPhyl v3 family database object.

=cut

package Greenphyl::ExtV3Family;

use strict;
use warnings;

use base qw(Greenphyl::AbstractFamily);

use vars qw($AUTOLOAD);

use Carp qw(cluck confess croak);

use Greenphyl::Config;
use Greenphyl;



# Package constants
####################

=pod

=head1 CONSTANTS

B<$OBJECT_PROPERTIES>: (hash ref)

sequence object properties description. See DBObject constructor documentation
for details.

=cut

our $DEBUG = 0;
our $OBJECT_PROPERTIES =
    {
        'table' => 'family',
        'relationship' => {
        },
        'key' => 'family_id',
        'alternate_keys' => ['accession',],
        'load'     => [
            'family_id',
            'family_name',
            'accession',
            'type',
            'description',
            'pub_ref',
            'pmid',
            'validated',
            'black_list',
            'inference',
            'tax_id',
            'plant_specific',
            'updated',
            'min_seq_length',
            'max_seq_length',
            'average_seq_length',
            'median_seq_length',
            'alignment_length',
            'seq_count',
            'alignment_seq_count',
        ],
        'alias' => {
            'alignment_sequence_count' => 'alignment_seq_count',
            'average_sequence_length' => 'average_seq_length',
            'black_listed' => 'black_list',
            'id' => 'family_id',
            'max_sequence_length' => 'max_seq_length',
            'median_sequence_length' => 'median_seq_length',
            'min_sequence_length' => 'min_seq_length',
            'name' => 'family_name',
            'family_accession' => 'accession',
            'plant_spe' => 'plant_specific',
            'pubmed' => 'pmid',
            'sequence_count' => 'seq_count',
            'taxonomy_id' => 'tax_id',
            'class' => 'level',
        },
        'base' => {
            'family_id' => {
                'property_column' => 'family_id',
            },
            'family_name' => {
                'property_column' => 'family_name',
            },
            'accession' => {
                'property_column' => 'accession',
            },
            'type' => {
                'property_column' => 'type',
            },
            'description' => {
                'property_column' => 'description',
            },
            'pub_ref' => {
                'property_column' => 'pub_ref',
            },
            'pmid' => {
                'property_column' => 'pmid',
            },
            'validated' => {
                'property_column' => 'validated',
            },
            'black_list' => {
                'property_column' => 'black_list',
            },
            'inference' => {
                'property_column' => 'inference',
            },
            'tax_id' => {
                'property_column' => 'tax_id',
            },
            'plant_specific' => {
                'property_column' => 'plant_specific',
            },
            'updated' => {
                'property_column' => 'updated',
            },
            'min_seq_length' => {
                'property_column' => 'min_seq_length',
            },
            'max_seq_length' => {
                'property_column' => 'max_seq_length',
            },
            'average_seq_length' => {
                'property_column' => 'average_seq_length',
            },
            'median_seq_length' => {
                'property_column' => 'median_seq_length',
            },
            'alignment_length' => {
                'property_column' => 'alignment_length',
            },
            'seq_count' => {
                'property_column' => 'seq_count',
            },
            'alignment_seq_count' => {
                'property_column' => 'alignment_seq_count',
            },
        },
        'secondary' => {
            'level' => {
                'property_table'  => 'found_in',
                'property_key'    => 'family_id',
                'multiple_values' => 0,
                'property_column' => 'class_id',
            },
            'synonyms' => {
                'property_table'  => 'synonym',
                'property_key'    => 'family_id',
                'multiple_values' => 1,
                'property_column' => 'synonym',
            },
        },
        'tertiary' => {
            'dbxref' => {
                'link_table'        => 'family_dbxref',
                'link_object_key'   => 'family_id',
                'link_property_key' => 'dbxref_id',
                'multiple_values'   => 1,
                'property_module'   => 'Greenphyl::ExtV3DBXRef',
                'property_table'    => 'dbxref',
                'property_key'      => 'dbxref_id',
            },
            'sequences' => {
                'link_table'        => 'seq_is_in',
                'link_object_key'   => 'family_id',
                'link_property_key' => 'seq_id',
                'multiple_values'   => 1,
                'property_module'   => 'Greenphyl::ExtV3Sequence',
                'property_table'    => 'sequences',
                'property_key'      => 'seq_id',
            },
            'species' => {
                'link_table'        => 'count_family_seq',
                'link_object_key'   => 'family_id',
                'link_property_key' => 'species_id',
                'multiple_values'   => 1,
                'property_module'   => 'Greenphyl::ExtV3Species',
                'property_table'    => 'species',
                'property_key'      => 'species_id',
            },
        },
    };

our $SUPPORTED_DUMP_FORMAT = $Greenphyl::AbstractFamily::SUPPORTED_DUMP_FORMAT;



# Package subs
###############

=pod

=head1 STATIC METHODS

=head2 CONSTRUCTOR

B<Description>: Creates a new instance of a GreenPhyl family object.

B<ArgsCount>: 2

=over 4

=item $dbh: (DBI::db) (U)

Database handler of the database to load the object from.

=item $parameters: (hash ref) (R)

See Greenphyl::DBObject constructor documentation for details.

=back

B<Return>: (Greenphyl::ExtV3Family)

a new instance.

B<Caller>: General

B<Example>:

    my $v3_family = new Greenphyl::ExtV3Family($dbh, {'selectors' => {'id' => 205, 'validated' => ['<>', 0]}});

=cut

sub new
{
    my ($proto) = shift();
    my $class = ref($proto) || $proto;

    return $class->SUPER::new(@_);
}




=pod

=head1 DIAGNOSTICS

=head1 AUTHORS

Mathieu ROUARD (Bioversity), m.rouard@cgiar.org

Valentin GUIGNON (Bioversity), v.guignon@cgiar.org

Matthieu CONTE (Syngenta)

Sebastien BRIOIS (Syngenta)

=head1 VERSION

Version 1.0.0

Date 02/12/2013

=head1 SEE ALSO

GreenPhyl documentation, DBObject documentation.

=cut

return 1; # package return
