=pod

=head1 NAME

Greenphyl::SequenceIPR - GreenPhyl SequenceIPR Object

=head1 SYNOPSIS

    use Greenphyl::SequenceIPR;
    my @sequence_ipr = Greenphyl::SequenceIPR->new($dbh, {'selectors' => {'sequence_id' => 806}});
    foreach (@sequence_ipr)
    {
        print $_->ipr->code . " starts at " . $_->start . " and ends at " . $_->end . "\n";
    }

=head1 REQUIRES

Perl5

=head1 EXPORTS

Nothing

=head1 DESCRIPTION

This module implements a GreenPhyl SequenceIPR database object.

=cut

package Greenphyl::SequenceIPR;

use strict;
use warnings;

use base qw(Greenphyl::CachedDBObject);

use vars qw($AUTOLOAD);

use Carp qw(cluck confess croak);




# Package constants
####################

=pod

=head1 CONSTANTS

B<$OBJECT_PROPERTIES>: (hash ref)

object properties description. See DBObject constructor documentation for
details.

=cut

our $OBJECT_PROPERTIES = {
    'table' => 'ipr_sequences',
    'key' => 'sequence_id,ipr_id',
    'alternate_keys' => [],
    'load'     => [
        'ipr_id',
        'score',
        'sequence_id',
        'start',
        'end',
    ],
    'alias' => {
        'ipr_score' => 'score',
        'ipr_start' => 'start',
        'ipr_stop'  => 'end',
        'stop'      => 'end',
        'seq_id'    => 'sequence_id',
    },
    'base' => {
        'ipr_id' => {
            'property_column' => 'ipr_id',
        },
        'score' => {
            'property_column' => 'score',
        },
        'start' => {
            'property_column' => 'start',
        },
        'end' => {
            'property_column' => 'end',
        },
        'sequence_id' => {
            'property_column' => 'sequence_id',
        },
    },
    'secondary' => {
        'sequence' => {
            'object_key'      => 'sequence_id',
            'property_table'  => 'sequence',
            'property_key'    => 'id',
            'multiple_values' => 0,
            'property_column' => 'Greenphyl::Sequence',
        },
        'ipr' => {
            'object_key'      => 'ipr_id',
            'property_table'  => 'ipr',
            'property_key'    => 'id',
            'multiple_values' => 0,
            'property_module' => 'Greenphyl::IPR',
        },
    },
    'tertiary' => {
    },
};




# Package subs
###############

=pod

=head1 STATIC METHODS

=head2 CONSTRUCTOR

B<Description>: Creates a new instance of a GreenPhyl SequenceIPR object.

B<ArgsCount>: 2

=over 4

=item $dbh: (DBI::db) (U)

Database handler of the database to load the object from.

=item $parameters: (hash ref) (R)

See Greenphyl::DBObject constructor documentation for details.

=back

B<Return>: (Greenphyl::SequenceIPR)

a new instance.

B<Caller>: General

B<Example>:

    my @sequence_ipr = Greenphyl::SequenceIPR->new($dbh, {'selectors' => {'sequence_id' => 806}});

=cut

sub new
{
    my $proto = shift();
    my $class = ref($proto) || $proto;
    
    my $object_properties = $class->GetDefaultClassProperties();

    return $class->SUPER::new($object_properties, @_);
}




=pod

=head1 ACCESSORS

=head2 length

B<Description>: getter/setter for IPR domain length on sequence.

B<ArgsCount>: 1-2

=over 4

=item $self: (Greenphyl::SequenceIPR)

the sequence IPR object.

=item $length: (integer)

the new domain length.

=back

B<Return>: (integer)

the domain length on the sequence.

=cut

sub length
{
    my ($self, $length) = @_;
    
    # setter?
    if (defined($length))
    {
        # set length
        return $self->{'length'} = $length;
    }

    # has the length been set?
    if (!exists($self->{'length'})
        || !defined($self->{'length'}))
    {
        $self->{'length'} = $self->{'end'} - $self->{'start'};
    }

    return $self->{'length'};
}
*getLength = \&length;
*setLength = \&length;


=pod

=head2 height

B<Description>: getter/setter for IPR domain height on sequence.

B<ArgsCount>: 1-2

=over 4

=item $self: (Greenphyl::SequenceIPR)

the sequence IPR object.

=item $height: (integer)

the domain height for graphic rendering.

=back

B<Return>: (integer)

the domain height for display.

=cut

sub height
{
    my ($self, $height) = @_;
    
    # setter?
    if (defined($height))
    {
        # set height
        return $self->{'height'} = $height;
    }

    # has the height been set?
    if (!exists($self->{'height'})
        || !defined($self->{'height'}))
    {
        # ask sequence to set heights for its IPR
        $self->sequence->layerIPR();
    }

    return $self->{'height'};
}
*getHeight = \&height;
*setHeight = \&height;


=pod

=head2 layer

B<Description>: getter/setter for layer.

B<ArgsCount>: 1-2

=over 4

=item $self: (Greenphyl::SequenceIPR)

the sequence IPR object.

=item $level: (integer)

layer level to use.

=back

B<Return>: (integer)

the layer the IPR is on.

=cut

sub layer
{
    my ($self, $level) = @_;
    
    # setter?
    if (defined($level))
    {
        # set level
        return $self->{'layer'} = $level;
    }

    # has the IPR sequence been layered?
    if (!exists($self->{'layer'})
        || !defined($self->{'layer'}))
    {
        # ask sequence to layer its IPR
        $self->sequence->layerIPR();
    }

    return $self->{'layer'};
}
*getLayer = \&layer;
*setLayer = \&layer;




=pod

=head1 AUTHORS

Mathieu ROUARD (Bioversity), m.rouard@cgiar.org

Valentin GUIGNON (Bioversity), v.guignon@cgiar.org

Matthieu CONTE (Syngenta)

=head1 VERSION

Version 1.0.0

Date 17/04/2012

=head1 SEE ALSO

GreenPhyl documentation, DBObject documentation.

=cut

return 1; # package return
