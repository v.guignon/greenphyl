=pod

=head1 NAME

Greenphyl::Exceptions - Greenphyl exception objects and methods

=head1 SYNOPSIS

    use Greenphyl::Exceptions;

    # try block
    eval
    {
        ...
        if (...)
        {
            ThrowGreenphylError('error' => 'An error occured!', 'log' => 'Log message with sensible content');
        }
        ...
    };

    # catch block
    my $error;
    if ($error = Exception::Class->caught('Greenphyl::Error'))
    {
        # we can display the error message as it has been designed for it
        # note: if we're une debug mode, log message will also be displayed
        print "We got the following error:\n" . $error->message . "\n";

        # we can add special message with sensible data to logs
        cluck($error);
    }
      # otherwise block
    elsif ($error = Exception::Class->caught())
    {
        # unexpected error
        print "We got an error!\n";
        cluck($error);
    }

=head1 REQUIRES

Perl5, Greenphyl

=head1 EXPORTS

ThrowGreenphylError

=head1 DESCRIPTION


=cut

package main;

use Exception::Class (
    'Greenphyl::Error' => {
        'description' => 'GreenPhyl exceptions that can be displayed to the user',
        'fields' => 'log',
        'alias' => 'ThrowGreenphylError',
    },
    'Greenphyl::Redirection' => {
        'description' => 'GreenPhyl user redirection',
        'fields' => 'url',
        'alias' => 'ThrowRedirection',
    },
    'Greenphyl::AccessDenied' => {
        'description' => 'Send when current user does not have the requested access level',
        'fields' => 'access',
        'alias' => 'ThrowAccessDenied',
    },
);




package Greenphyl::Error;

use strict;
use warnings;




# Package subs
###############

=pod

=head1 METHODS

=head2 message

B<Description>: overrides default functions that returns error message to take
in account log message.

B<ArgsCount>: 1

=over 4

=item $self: (Greenphyl::Error) (R)

the error object.

=back

B<Return>: (string)

the error message to display with log details if in debug mode or if
full_message is called.

=cut

sub message
{
    my $self = shift();

    my $message = $self->SUPER::message();

    my $log_message;
    if (Greenphyl::GP('DEBUG_MODE') && ($log_message = $self->log))
    {
        $message .= "\n$log_message";
    }

    return $message;
}

sub error; *error = \&message;


sub full_message
{
    my $self = shift();

    my $message = $self->SUPER::message();

    my $log_message;
    if ($log_message = $self->log)
    {
        $message .= "\n$log_message";
    }

    return $message;
}

################################################################################


package Greenphyl::Redirection;

use strict;
use warnings;




# Package subs
###############

=pod

=head1 METHODS

=head2 redirect

B<Description>: Redirects the user to another given URL by printing the
redirection and exiting.

B<ArgsCount>: 1

=over 4

=item $self: (Greenphyl::Redirection) (R)

the error object.

=back

B<Return>: (nothing)

=cut

sub redirect
{
    my $self = shift();

    my $target_url = $self->url || $self->SUPER::message();

    if (Greenphyl::IsRunningCGI())
    {
        Greenphyl::PrintDebug("Redirecting user to '$target_url'");
        print Greenphyl::Web::Redirect($target_url);
        if ($ENV{'TESTING'})
        {
            die;
        }
        exit(0);
    }
    else
    {
        return "You should have been redirected to :\n$target_url\n";
    }
}

sub message; *message = \&redirect;


################################################################################


package Greenphyl::AccessDenied;

use strict;
use warnings;




# Package subs
###############

=pod

=head1 METHODS

=head2 message

B<Description>: overrides default functions that returns error message to
display either the registration form or the logout form.

B<ArgsCount>: 1

=over 4

=item $self: (Greenphyl::AccessDenied) (R)

the error object.

=back

B<Return>: (string)

the error message to display.

=cut

sub message
{
    my $self = shift();

    my $message = $self->SUPER::message();

    if (Greenphyl::GP('DEBUG_MODE') && (my $access = $self->access))
    {
        if ($access->{'require_access'})
        {
            $message .= "\nRequested access: current user is requested to have "
                . $access->{'access_needs'}
                . " of the following rights: '"
                . join("', '", @{$access->{'require_access'}})
                . "'\n";
        }
    }

    return $message;
}

sub error; *error = \&message;


sub full_message
{
    my $self = shift();

    my $message = $self->SUPER::message();

    if (my $access = $self->access)
    {
        if ($access->{'require_access'})
        {
            $message .= "\nRequested access: current user is requested to have "
                . $access->{'access_needs'}
                . " of the following rights: '"
                . join("', '", @{$access->{'require_access'}})
                . "'\n";
        }
    }

    return $message;
}


=pod

=head1 AUTHORS

Mathieu ROUARD (Bioversity), m.rouard@cgiar.org

Valentin GUIGNON (Bioversity), v.guignon@cgiar.org

Matthieu CONTE (Syngenta)

=head1 VERSION

Version 1.2.0

Date 27/01/2014

=head1 SEE ALSO

GreenPhyl documentation.

=cut

return 1; # package return
