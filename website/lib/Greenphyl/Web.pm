=pod

=head1 NAME

Greenphyl::Web - Contains GreenPhyl Website API

=head1 SYNOPSIS

use Greenphyl::Web;

=head1 REQUIRES

Perl5

=head1 EXPORTS

GetCGI GetSession
LogWebDebugMessage GetWebDebugMessage GetAndClearWebDebugMessage
GetAccessErrorMessage
GetPageAction ProceedPage RenderHTMLFullPage GetPageMode GetLinkTarget
RenderHTMLStandardHeader RenderHTMLMinimalHeader
RenderHTTPResponse RenderHTTPFileHeader
RenderHTMLStandardFooter RenderHTMLMinimalFooter Redirect
GenerateCSSList GenerateLESSList GenerateJavascriptList
CheckHTMLIdentifierOk IsHTMLIdentifierAvailable UseHTMLIdentifier
GetFreeHTMLIdentifier
RenderToggler RenderMessage RenderGreenphylError
GetAlignmentURL GetFamilyFileURL
GetPhylogenyAnalyzesURL GetPhylogenyAnalyzesPath
GetFileContentFromCGI InitAntiSpam CheckAntiSpam RenderIssue

=head1 DESCRIPTION

This module contains GreenPhyl Web API (Application Programming Interface).
Functions provided here are used by GreenPhyl CGI scripts.

=cut

package Greenphyl::Web;

use strict;
use warnings;

use Carp qw (cluck confess croak);

use CGI qw(:standard);
use CGI::Session;

use Greenphyl;
use Greenphyl::Web::Access;
use Greenphyl::Web::Templates;

use base qw(Exporter);
our @EXPORT =
    qw(
        GetCGI GetSession
        LogWebDebugMessage GetWebDebugMessage GetAndClearWebDebugMessage
        GetAccessErrorMessage
        GetPageAction ProceedPage RenderHTMLFullPage GetPageMode GetLinkTarget
        RenderHTMLStandardHeader RenderHTMLMinimalHeader
        RenderHTTPResponse RenderHTTPFileHeader
        RenderHTMLStandardFooter RenderHTMLMinimalFooter Redirect
        GenerateCSSList GenerateLESSList GenerateJavascriptList
        CheckHTMLIdentifierOk IsHTMLIdentifierAvailable UseHTMLIdentifier
        GetFreeHTMLIdentifier
        RenderToggler RenderMessage RenderGreenphylError
        GetAlignmentURL GetFamilyFileURL
        GetPhylogenyAnalyzesURL GetPhylogenyAnalyzesPath
        GetFileContentFromCGI InitAntiSpam CheckAntiSpam RenderIssue
    );




# Package constants
####################

=pod

=head1 CONSTANTS

B<$DEBUG>: (boolean)

Enables/disables local debug mode.

B<%MIME>: (hash of subs)

Keys are HTML MIME types corresponding to allowed MIME types.
Values are functions that returns the associated HTML answer content.

B<%DEFAULT_DOWNLOAD_FILE_NAME>: (hash of strings)

Default file name for download for a given format.

=cut

our $DEBUG = 0;

our %MIME =
    (
        'DISK' => \&_GenerateHTMLResponseForFile,
        'HTML' => \&_GenerateHTMLResponseForHTML,
        'JSON' => \&_GenerateHTMLResponseForJSONOutput,
        'TEXT' => \&_GenerateHTMLResponseForTextOutput,
        'SVG'  => \&_GenerateHTMLResponseForSVG,
    );

our %STREAM_TYPES =
    (
        'default' => 'application/octet-stream',
        'csv'     => 'text/csv',
        'fasta'   => 'text/plain',
        'html'    => 'text/html',
        'json'    => 'application/json',
        'svg'     => 'image/svg+xml',
        'text'    => 'text/plain',
        'xhtml'   => 'application/xhtml+xml',
        'xls'     => 'application/vnd.ms-excel',
        'xml'     => 'application/xml',
    );

our %DEFAULT_DOWNLOAD_FILE_NAME =
    (
        'default' => 'greenphyl.txt',
        'csv'     => 'details.csv',
        'fasta'   => 'sequences.fa',
        'html'    => 'greenphyl.html',
        'json'    => 'data.json',
        'svg'     => 'image.svg',
        'text'    => 'data.txt',
        'xhtml'   => 'greenphyl.xhtml',
        'xls'     => 'details.xls',
        'xml'     => 'details.xml',
    );

our @WEEKDAY = ("Mon", "Tue", "Wed", "Thu", "Fri", "Sat", "Sun");
our @MONTH   = ("Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec");

our $PAGE_MODE_FULL       = 'full';
our $PAGE_MODE_FRAME      = 'frame';
our $PAGE_MODE_BANANA_HUB = 'bananahub';
our $PAGE_MODE_INNER      = 'inner';
our $PAGE_MODE_DEFAULT    = $PAGE_MODE_FULL;




# Package variables
####################

=pod

=head1 VARIABLES

B<$g_cgi>: (CGI)

Global current CGI object.

B<$g_session>: (CGI::Session)

Global current user session object.
GreenPhyl session object contains the following parameters:
    admin: set to a true value if current user is admin;

B<$g_used_html_identifiers>: (hash ref)

Hash to keep track of used HTML identifier.

B<$g_debug_message>: (string)

Global debug message.

B<$g_html_identifier_suffix>: (string)

Global auto-incremented suffix used to generate free HTML identifiers.

B<$g_access_message>: (string)

Global login access error message if one.

=cut

my $g_cgi                    = CGI->new();
# note: session name is set in Greenphyl::Config
my $g_session                = CGI::Session->new('driver:File', $g_cgi, {'Directory' => Greenphyl::GP('SESSIONS_PATH')});
   $g_session->expire('+72h'); # expires the session after 3 days without use
my $g_used_html_identifiers  = {};
my $g_debug_message          = '';
my $g_html_identifier_suffix = 'a';
my $g_access_message         = '';



# Package subs
###############

=pod

=head1 FUNCTIONAL INTERFACE

=head2 GetCGI

B<Description>: Returns GreenPhyl CGI object.

B<ArgsCount>: 0

B<Return>: (CGI)

Current GreenPhyl CGI object.

B<Caller>: general

B<Example>:

     my $cgi = GetCGI();

B<See Also>:

GetSession()

=cut

sub GetCGI
{
    return $g_cgi;
}


=pod 

=head2 GetSession

B<Description>: Returns GreenPhyl CGI::Session object.

B<ArgsCount>: 0

B<Return>: (CGI::Session)

Current GreenPhyl CGI::Session object.

B<Caller>: general

B<Example>:

     my $session = GetSession();

B<See Also>:

GetCGI()

=cut

sub GetSession
{
    return $g_session;
}


=pod

=head2 LogWebDebugMessage

B<Description>: adds a debug message to web debug logs.

B<ArgsCount>: 1

=over 4

=item $debug_message: (string) (R)

the debug message to log.

=back

B<Return>: nothing

=cut

sub LogWebDebugMessage
{
    my ($debug_message) = @_;
    $g_debug_message .= qq|<div class="message">$debug_message</div>\n|;
}


=pod

=head2 GetWebDebugMessage

B<Description>: returns debug messages from web debug logs.

B<ArgsCount>: 0

B<Return>: (string)

The debug messages in HTML format.

=cut

sub GetWebDebugMessage
{
    return $g_debug_message;
}


=pod

=head2 GetAndClearWebDebugMessage

B<Description>: returns current debug messages from web debug logs and clear
logs.

B<ArgsCount>: 0

B<Return>: (string)

Current debug messages in HTML format.

=cut

sub GetAndClearWebDebugMessage
{
    my $current_debug_message = GetWebDebugMessage();
    $g_debug_message = '';
    return $current_debug_message;
}


=pod

=head2 GetPageAction

B<Description>: find which page action is currently set. A page action is set
by a submit button used in a form: the name of the button correspond to the
page action to perform.

B<ArgsCount>: 1

=over 4

=item $allowed_pages: (array ref) (R)

an array (of strings) containing all the available/allowed actions name.

=back

B<Return>: (string)

the page action name or an empty string.

=cut

sub GetPageAction
{
    my ($allowed_pages) = @_;
    
    foreach my $page_action (@$allowed_pages)
    {
        # check if a parameter using the $page_action is set
        # which would mean the corresponding submit button has been used
        if ($page_action
            && ($g_cgi->url_param($page_action) || $g_cgi->param($page_action)))
        {
            return $page_action;
        }
    }
    return '';
}


=pod

=head2 ProceedPage

B<Description>: find which page content to load and render the content using
the specified function given by $page_actions.

B<ArgsCount>: 1

=over 4

=item $page_actions: (hash ref) (R)

A hash containing references to functions to load. Keys are the value of the 'p'
parameter (either from GET or POST data). Values are functions to call for the
associated page or hash in the case the (sub)page to load should be fetched
using a get or post argument name instead of just the 'p' value. This can been
very usefull in case of forms with several submit buttons as each of them can
have a different name corresponding to the page to load.

=back

B<Return>: (string)

the full HTML content.

=cut

sub ProceedPage
{
    my ($page_actions) = @_;
    my $action;

    # get page from GET or from POST if GET is not set
    my $page = $g_cgi->url_param('p') || $g_cgi->param('p') || '';

    # check if an action exists for the page
    if (exists($page_actions->{$page}))
    {
        $action = $page_actions->{$page};
    }
    else
    {
        # default action
        $action = $page_actions->{''};
    }

    # check the kind of action to perform
    if ('CODE' eq ref($action))
    {
        # proceed page by running the sub
        return $action->({'p' => $page});
    }
    elsif ('HASH' eq ref($action))
    {
        # a page has been specified using a parameter name (and not 'p' value)
        my @allowed_pages = keys(%$action);

        my $subpage = GetPageAction(\@allowed_pages);

        # proceed page by running the related sub
        if (exists($action->{$subpage}))
        {
            return $action->{$subpage}->({'p' => $page, 'sp' => $subpage});
        }
    }
    elsif ($action)
    {
        confess "ERROR: Invalid action ('$page') set in \$page_actions argument for ProceedPage!\n";
    }
    Throw('error' => "Invalid page ($page)!");
}


=pod

=head2 _GenerateHTMLResponseForHTML

B<Description>: generates the HTML response for HTML content.

B<ArgsCount>: 0-1

=over 4

=item $args: (hash ref) (O)

A hash containing additional parameters:
    'header_args' => array of arguments for the HTML response header;

=back

B<Return>: (string)

the HTML response for HTML content.

B<Caller>: internal

=cut

sub _GenerateHTMLResponseForHTML
{
    my ($args) = @_;
    my $HTTPResponse = '';

    # no caching for CGI
    my %default_args = (
        '-expires'       => 'now',
        '-last_modified' => 'now',
        # HTTP/1.0
        '-Pragma'        => 'no-cache',
        # HTTP/1.1
        '-Cache_Control' => join(', ',
            'no-store',
            'no-cache',
            'no-transform',
            'must-revalidate',
            'post-check=0',
            'pre-check=0',
        ),
    );

    # the following code has been disabled because of browser caching too much
    #+FIXME: we should create a list of cacheable pages and only use this code
    # for them or maybe manage caching in scripts directly (...as it should be)
    # my ($dev,$ino,$mode,$nlink,$uid,$gid,$rdev,$size,$atime,$mtime,$ctime,$blksize,$blocks) = stat($0);
    # if ($mtime)
    # {
    #     my ($second, $minute, $hour, $day_of_month, $month, $year_offset, $day_of_week, $day_of_year, $daylight_savings) = localtime($mtime);
    #     my $year = 1900 + $year_offset;
    # 
    #     # Format: Mon, 03 Jan 2011 17:45:57 GMT
    #     my $last_modified = sprintf("%s, %02i %s %04i %02i:%02i:%02i GMT", $WEEKDAY[$day_of_week], $day_of_month, $MONTH[$month], $year, $hour, $minute, $second);
    #     PrintDebug("Time modified: $last_modified");
    #     $default_args{'-last_modified'} = $last_modified;
    # }

    if ($args
        && exists($args->{'header_args'})
        && ('ARRAY' eq ref($args->{'header_args'})))
    {
        if (grep(m/^-last_modified$/, @{$args->{'header_args'}}))
        {
            delete($default_args{'-last_modified'});
        }

        if (grep(m/^-expires$/, @{$args->{'header_args'}}))
        {
            delete($default_args{'-expires'});
        }

        $HTTPResponse = $g_session->header(@{$args->{'header_args'}}, %default_args);
    }
    else
    {
        $HTTPResponse = $g_session->header(%default_args);
    }

# Set-Cookie: GreenPhyl%20v5%20dev=2714b036e5857f382197655b3e6fa848; path=/; expires=Fri, 16-Oct-2020 13:42:06 GMT
    # Alter session cookie for new standards.
    if ($g_cgi->https())
    {
        $HTTPResponse =~ s/(Set-Cookie: GreenPhyl%20[^\r\n]*)/$1; SameSite=None; Secure/;
    }
    return $HTTPResponse;
}


=pod

=head2 _GenerateHTMLResponseForTextOutput

B<Description>: generates the HTML response for plain text content.

B<ArgsCount>: 0

B<Return>: (string)

the HTML response for plain text content.

B<Caller>: internal

=cut

sub _GenerateHTMLResponseForTextOutput
{
    return header($STREAM_TYPES{'text'});
}


=pod

=head2 _GenerateHTMLResponseForJSONOutput

B<Description>: generates the HTML response for JSON content.

B<ArgsCount>: 0

B<Return>: (string)

the HTML response for JSON content.

B<Caller>: internal

=cut

sub _GenerateHTMLResponseForJSONOutput
{
    return header($STREAM_TYPES{'json'});
}


=pod

=head2 _GenerateHTMLResponseForSVG

B<Description>: generates the HTML response for SVG content.

B<ArgsCount>: 0

B<Return>: (string)

the HTML response for SVG content.

B<Caller>: internal

=cut

sub _GenerateHTMLResponseForSVG
{
    return header($STREAM_TYPES{'svg'});
}


=pod

=head2 _GenerateHTMLResponseForFile

B<Description>: generates the HTML response for file content.

B<ArgsCount>: 0-1

=over 4

=item $args: (hash ref) (O)

A hash containing additional parameters:
    'file_name' => name of the output file;
    'format'    => the file format (ex.: 'fasta');

=back

B<Return>: (string)

the HTML response for file content.

B<Caller>: internal

=cut

sub _GenerateHTMLResponseForFile
{
    my ($args) = @_;

    my $format   = lc($args->{'format'} || '');
    if (!$format || !exists($STREAM_TYPES{$format}))
    {
        $format = 'default';
    }
    my $stream_type = $STREAM_TYPES{$format};

    my $file_name = $args->{'file_name'};
    # check file name
    if (!$file_name || ($file_name !~ m/^[\w\.\-]+$/))
    {
        $file_name = $DEFAULT_DOWNLOAD_FILE_NAME{$format};
    }

    if (Greenphyl::GP('DEBUG_MODE') && ($stream_type =~ m/text|xml/i))
    {
        if ($stream_type =~ m/text/i)
        {
            $stream_type = $STREAM_TYPES{'text'};
        }
        Greenphyl::PrintDebug("Debug mode one: output to screen instead of output to attachement file '$file_name'");
        return header('-type' => $stream_type);
    }
    return header('-type' => $stream_type, '-attachment' => $file_name);
}


=pod

=head2 GenerateCSSList

B<Description>: generates the list of CSS files to load.

B<ArgsCount>: 0-1

=over 4

=item $args: (hash ref) (O)

A hash containing additional parameters:
    'css'        => array ref of CSS file names without ".css" extension to add
        to the default list.
    'remove_css' => array ref of CSS file names without ".css" extension to
        remove from the default list.

=back

B<Return>: (array ref of hashes)

the list of CSS files to load. Each hash has a key 'name' corresponding to the
CSS file name without the ".css" extension.

B<Caller>: general

B<Example>:

    my $css_list = GenerateCSSList();
    print "First CSS: $css_list->[0]->{'name'}\n";

=cut

sub GenerateCSSList
{
    my ($args) = @_;

    my @css_list;

    # prepare list of unwanted CSS
    my %css_to_remove;
    if ($args->{'remove_css'})
    {
        %css_to_remove = map { $_ => 1 } @{$args->{'remove_css'}};
    }

    # add provided CSS
    # -if production mode, add CSS corresponding to files LESS files
    my @less_to_css;
    if (!Greenphyl::GP('DEBUG_MODE'))
    {
        @less_to_css = @{Greenphyl::GP('LESS_LIST')};
        if ($args->{'less'})
        {
            push(@less_to_css, @{$args->{'less'}});
        }
    }
    foreach (@{Greenphyl::GP('CSS_LIST')}, @less_to_css, @{$args->{'css'}})
    {
        if (!exists($css_to_remove{$_}))
        {
            push(@css_list, { 'name' => $_ });
        }
    }

    return \@css_list;
}


=pod

=head2 GenerateLESSList

B<Description>: generates the list of LESS files to load.

B<ArgsCount>: 0-1

=over 4

=item $args: (hash ref) (O)

A hash containing additional parameters:
    'less'        => array ref of LESS file name without ".less" extension to
        add to the default list.
    'remove_less' => array ref of LESS file name without ".less" extension to
        remove from the default list.

=back

B<Return>: (array ref of hashes)

the list of LESS files to load. Each hash has a key 'name' corresponding to the
LESS file name without the ".less" extension.

B<Caller>: general

B<Example>:

    my $less_list = GenerateLESSList();
    print "First LESS: $less_list->[0]->{'name'}\n";

=cut

sub GenerateLESSList
{
    my ($args) = @_;

    # if production mode, LESS files are replaced by corresponding CSS files
    if (!Greenphyl::GP('DEBUG_MODE'))
    {return [];}

    my @less_list;

    # prepare list of unwanted LESS
    my %less_to_remove;
    if ($args->{'remove_less'})
    {
        %less_to_remove = map { $_ => 1 } @{$args->{'remove_less'}};
    }

    # add provided LESS
    foreach (@{Greenphyl::GP('LESS_LIST')}, @{$args->{'less'}})
    {
        if (!exists($less_to_remove{$_}))
        {
            push(@less_list, { 'name' => $_ });
        }
    }

    return \@less_list;
}


=pod

=head2 GenerateJavascriptList

B<Description>: generates the list of Javascript files to load.

B<ArgsCount>: 0-1

=over 4

=item $args: (hash ref) (O)

A hash containing additional parameters:
    'javascript'        => array ref of javascript file names without ".js"
        extension to add to the default list.
    'remove_javascript' => array ref of Javascript file names without ".js"
        extension to remove from to the default list.

=back

B<Return>: (array ref of hashes)

the list of Javascript files to load. Each hash has a key 'name' corresponding
to the Javascript file name without the ".js" extension.

B<Caller>: general

B<Example>:

    my $javascript_list = GenerateJavascriptList();
    print "First Javascript: $javascript_list->[0]->{'name'}\n";

=cut

sub GenerateJavascriptList
{
    my ($args) = @_;

    my @javascript_list;

    # prepare to skip unwanted Javascript
    my %javascript_to_remove;
    if ($args->{'remove_javascript'})
    {
        %javascript_to_remove = map { $_ => 1 } @{$args->{'remove_javascript'}};
    }

    # add provided Javascript
    foreach my $script (@{Greenphyl::GP('JAVASCRIPT_LIST')}, @{$args->{'javascript'}})
    {
        $script =~ m/
                ([^?]+)   # get script name until a '?' character is met
                (?:\.js)? # remove unecessary ".js" extension
                \??       # if there's a '?' for parameters, match it
                (.*)      # get the rest as script arguments
            /x;
        # only add what's needed
        if (!exists($javascript_to_remove{$1}))
        {
            push(@javascript_list, {'name' => $1, 'parameters' => $2});
        }
    }

    return \@javascript_list;
}


=pod

=head2 _FormatTitle

B<Description>: format the title.

B<ArgsCount>: 1

=over 4

=item $title: (string) (R)

The title to format.

=back

B<Return>: (string)

Formated title.

B<Caller>: internal

=cut

sub _FormatTitle
{
    my ($title) = @_;

    my @title_parts = (GP('SITE_NAME'));
    if ($title)
    {
        push(@title_parts, $title);
    }
    if (Greenphyl::GP('DEBUG_MODE'))
    {
        push(@title_parts, 'Debug Mode');
    }
    return join(' - ', @title_parts);
}


=pod

=head2 RenderHTTPResponse

B<Description>: generates the HTTP header response.

B<ArgsCount>: 0-1

=over 4

=item $args: (hash ref) (O)

A hash containing additional parameters:
    'mime'        => MIME type to use (default: 'HTML');
    'header_args' => additional array of arguments for the HTTP response header
        (only valid for HTML MIME type);

=back

B<Return>: (string)

the HTTP content to send to the client web browser.

B<Caller>: general

B<Example>:

    my $http_header = RenderHTTPResponse();

=cut

sub RenderHTTPResponse
{
    my ($args) = @_;

    # MIME type
    $args->{'mime'} = uc($args->{'mime'}) || 'HTML';
    if (!exists($MIME{$args->{'mime'}}))
    {
        # get HTML MIME code
        $args->{'mime'} = 'HTML';
    }

    # select HTTP header answer to give
    my $http_output = $MIME{ $args->{'mime'} }->($args);

    return $http_output;
}


=pod

=head2 RenderHTMLMinimalHeader

B<Description>: generates the minimal header of CGI page. It only includes HTML
response, head (with css, js,...), and body start tag.

B<ArgsCount>: 0-1

=over 4

=item $args: (hash ref) (O)

A hash containing additional parameters:
    'mime'        => MIME type to use (default: 'HTML');
    'title'       => page title (for header and body). Automatically prefixed
        with "GreenPhyl - ";
    'header_args' => additional array of arguments for the HTML response header
        (only valid for HTML MIME type);
    'body_id'     => HTML ID to use for the body element;

For Javascripts, CSS or LESS parameters please refer respectively ro
GenerateJavascriptList(), GenerateCSSList() and GenerateLESSList()
documentations: these functions share the same $args parameter.

=back

B<Return>: (string)

the HTML content to send to the client web browser.

B<Caller>: general

B<Example>:

    my $iframe_output = RenderHTMLMinimalHeader();

=cut

sub RenderHTMLMinimalHeader
{
    my ($args) = @_;

    if ($args->{'require_access'}
        && @{$args->{'require_access'}})
    {
        # make sure current user is allowed to view the page
        if (!Greenphyl::Web::Access::CheckAccess($args))
        {
            main::ThrowAccessDenied('error' => 'Access denied!', 'access' => $args);
        }
    }

    my $html_output = RenderHTTPResponse($args);

    $args->{'mime'} ||= 'HTML';
    if ('HTML' eq $args->{'mime'})
    {
        $html_output .= ProcessTemplate(
            'minimal_header.tt',
            {
                'title'           => _FormatTitle($args->{'title'}),
                'css_list'        => GenerateCSSList($args),
                'less_list'       => GenerateLESSList($args),
                'javascript_list' => GenerateJavascriptList($args),
                'body_id'         => $args->{'body_id'},
                'document_class'  => $args->{'document_class'},
            }
        );
    }

    return $html_output;
}


=pod

=head2 RenderHTMLStandardHeader

B<Description>: generates the header of each CGI page. It includes HTML
response, head (with css, js,...), body start tag, Greenphyl header and menu
links.

B<ArgsCount>: 0-1

=over 4

=item $args: (hash ref) (O)

A hash containing additional parameters:
    'mime'  => MIME type to use (default: 'HTML');
    'title' => page title (for header and body). Automatically prefixed with
        "GreenPhyl - ";
    'header_args' => additional arguments for the HTML response header (only
        valid for HTML MIME type);
    'body_id'     => HTML ID to use for the body element;

For Javascripts, CSS or LESS parameters please refer respectively ro
GenerateJavascriptList(), GenerateCSSList() and GenerateLESSList()
documentations: these functions share the same $args parameter.

=back

B<Return>: (string)

the HTML content to send to the client web browser.

B<Caller>: general

B<Example>:

    print RenderHTMLStandardHeader();

=cut

sub RenderHTMLStandardHeader
{
    my ($args) = @_;

    if ($args->{'require_access'}
        && @{$args->{'require_access'}})
    {
        # make sure current user is allowed to view the page
        if (!Greenphyl::Web::Access::CheckAccess($args))
        {
            main::ThrowAccessDenied('error' => 'Access denied!', 'access' => $args);
        }
    }

    my $html_output = RenderHTTPResponse($args);

    # database name (only if not the default one)
    my $database_name = Greenphyl::SelectActiveDatabase() ? Greenphyl::GetDatabaseName() : '';

    $args->{'mime'} ||= 'HTML';
    if ('HTML' eq $args->{'mime'})
    {
        $html_output .= ProcessTemplate(
            'global_header.tt',
            {
                'title'           => _FormatTitle($args->{'title'}),
                'database'        => $database_name,
                'css_list'        => GenerateCSSList($args),
                'less_list'       => GenerateLESSList($args),
                'javascript_list' => GenerateJavascriptList($args),
                'body_id'         => $args->{'body_id'},
                'require_access'  => $args->{'require_access'},
                'access_needs'    => $args->{'access_needs'},
            }
        );
    }

    return $html_output;
}


=pod

=head2 RenderHTTPFileHeader

B<Description>: generates the header of downloaded files.

B<ArgsCount>: 0-1

=over 4

=item $args: (hash ref) (O)

A hash containing additional parameters:
    'mime'      => MIME type to use (default: 'DISK');
    'format'    => the type of the file to give to the end user. See
        %DEFAULT_DOWNLOAD_FILE_NAME and %STREAM_TYPES for supported formats;
    'file_name' => the name of the file to give to the end user;

=back

B<Return>: (string)

the HTTP content to send to the client web browser.

B<Caller>: general

B<Example>:

    print RenderHTTPFileHeader();

=cut

sub RenderHTTPFileHeader
{
    my ($args) = @_;

    # force DISK
    $args->{'mime'} ||= 'DISK';
    return RenderHTTPResponse($args);
}


=pod

=head2 RenderHTMLMinimalFooter

B<Description>: generates the minimal footer of CGI page. It only includes the
requiered closing tags (body and html).

B<ArgsCount>: 0-1

=over 4

=item $args: (hash ref) (O)

A hash containing additional parameters:
    'mime'        => MIME type to use (default: 'HTML');

=back

B<Return>: (string)

the HTML content to send to the client web browser.

B<Caller>: general

B<Example>:

    $iframe_output .= RenderHTMLMinimalFooter();

=cut

sub RenderHTMLMinimalFooter
{
    my ($args) = @_;

    # MIME type
    $args->{'mime'} ||= 'HTML';
    $args->{'mime'} = uc($args->{'mime'});
    if (!exists($MIME{$args->{'mime'}}))
    {
        # get HTML MIME code
        $args->{'mime'} = 'HTML';
    }

    # nothing to add for non-HTML content
    if ('HTML' ne $args->{'mime'})
    {
        # just return an empty string
        return '';
    }

    return ProcessTemplate('minimal_footer.tt');
}


=pod

=head2 RenderHTMLFullPage

B<Description>: generates the HTML code of a complete GreenPhyl page (ie.
including header and footer) using a template and its options.

B<ArgsCount>: 1

=over 4

=item $option_hash: (hash ref) (R)

A hash containing template settings:
#+FIXME: to fill

=back

B<Return>: (string)

the HTML content to send to the client web browser.

B<Caller>: general

B<Example>:

    print RenderHTMLFullPage(
        {
            'mime'                => 'HTML',
            'mode'                => $PAGE_MODE_FULL,
            'header_args'         => ['-cookie' => $cookie],
            'css'                 => ['custom.autocomplete'],
            'remove_css'          => ['jquery.autocomplete'],
            'less'                => ['greenphyl3'],
            'remove_less'         => ['greenphyl'],
            'javascript'          => ['ipr?no_autoload=1'],
            'remove_javascript'   => ['swfobject'],
            'title'               => 'Hello World',
            'body_id'             => 'greenphyl_website',
            'before_content'      => '<img class="centered" src="http://www.greenphyl.fr/images/header_line.png"/>',
            'dump_data'           =>
                {
                   'links' => [
                     {
                       'label'       => 'FASTA',
                       'format'      => 'fasta',
                       'file_name'   => 'sequences.fasta',
                       'namespace'   => 'fasta',
                     },
                     {
                       'label'       => 'Excel',
                       'format'      => 'excel',
                       'file_name'   => 'sequences.xls',
                       'namespace'   => 'excel',
                     },
                     {
                       'label'       => 'CSV',
                       'format'      => 'csv',
                       'file_name'   => 'sequences.csv',
                       'namespace'   => 'csv',
                     },
                   ],
                   'object_type' => 'sequences',
                   'fields'      => 'seq_id',
                   'mode'        => 'form',
                   'values'      => ['At1g14920.1', 'Glyma10g37480.1'],
                   'parameters' => { 'columns' => "{'01.ID'=>'id','02.Name'=>'name'}", },
                 },
            'pager_data'          =>
                GeneratePagerData(
                    {
                        'total_entries' => $total_entry_count,
                        'entry_ranges_label' => 'Items per page:',
                    },
                ),
            'before_form_content' => 'pre_form.tt',
            'content'             => 'full_template.tt',
            'form_data'          =>
                {
                    'identifier' => 'my_form',
                    'action'     => 'submitter.cgi',
                    'method'     => 'POST',
                    'enctype'    => 'application/x-www-form-urlencoded',
                    'submit'     => 'Go go go!',
                    'nosubmit'   => 0,
                    'noclear'    => 1,
                },
            'after_form_content'  => '<div id="from_ajax_result_container"></div>',
            'after_content'       => 'pre_footer.tt',
            'param1'             => 'val1',
            'param2'             => 'val2',
            'param3'             => 'val3',
        },
    );

=cut

sub RenderHTMLFullPage
{
    my ($option_hash) = @_;

    # save old debug message before template processing (in case an error occurs)
    my $old_debug_message = GetWebDebugMessage();
    # adds debug message to options
    if ($option_hash)
    {
        $option_hash->{'debug_data'} = {'getMessage' => \&GetAndClearWebDebugMessage};
    }

    # check output mode
    my $mode = $option_hash->{'mode'} || GetPageMode();

    # set body class
    if (!$option_hash->{'document_class'}
        && ($mode =~ m/^(?:$PAGE_MODE_FRAME|$PAGE_MODE_BANANA_HUB|$PAGE_MODE_FULL)$/io))
    {
        $option_hash->{'document_class'} = $mode;
    }

    my $output = '';

    eval
    {
        # inner mode: only generate HTML content without head/body or header/footer.
        if ($PAGE_MODE_INNER eq $mode)
        {
            $output .= RenderHTTPResponse($option_hash);
            $output .= ProcessPage($option_hash);
        }
        elsif ($PAGE_MODE_FRAME eq $mode)
        {
            # frame: use minimal header and footer (good for iframe)
            $option_hash->{'less'} = ['iframe'];
            $output .= RenderHTMLMinimalHeader($option_hash);
            $output .= ProcessPage($option_hash);
            $output .= RenderHTMLMinimalFooter($option_hash);
        }
        elsif ($PAGE_MODE_BANANA_HUB eq $mode)
        {
            if (!$option_hash->{'body_id'} || ($option_hash->{'body_id'} eq 'greenphyl_website'))
            {
                $option_hash->{'body_id'} = 'banana_hub_website';
            }
            $option_hash->{'less'} = ['iframe'];
            $option_hash->{'banana_after_content'} = $option_hash->{'after_content'};
            $option_hash->{'after_content'} = 'banana_footer.tt';
            # banana hub: use minimal header and footer (good for iframe)
            $output .= RenderHTMLMinimalHeader($option_hash);
            $output .= ProcessPage($option_hash);
            $output .= RenderHTMLMinimalFooter($option_hash);
        }
        else
        {
            # $PAGE_MODE_FULL mode as default
            $mode = $PAGE_MODE_DEFAULT;
            # otherwise with standard header and footer
            $output .= RenderHTMLStandardHeader($option_hash);
            $output .= ProcessPage($option_hash);
            $output .= RenderHTMLStandardFooter($option_hash);
        }
    };

    # in case an error occured during template processing, keep debug message
    # to have them displayed on error page
    my $error;
    $error = Exception::Class->caught();
    if ($error)
    {
        $g_debug_message = $old_debug_message . $g_debug_message;
        ref $error ? $error->rethrow : die $error;
    }
    return $output;
}


=pod

=head2 RenderHTMLStandardFooter

B<Description>: generates the footer of each CGI page. It includes logos and
copyright block and requiered closing tags (body and html).

B<ArgsCount>: 0-1

=over 4

=item $args: (hash ref) (O)

A hash containing additional parameters:
    'mime'  => MIME type to use (default: 'HTML');

=back

B<Return>: (string)

the HTML content to send to the client web browser.

B<Caller>: general

B<Example>:

    print RenderHTMLStandardFooter();

=cut

sub RenderHTMLStandardFooter
{
    my ($args) = @_;

    # MIME type
    $args->{'mime'} ||= 'HTML';
    $args->{'mime'} = uc($args->{'mime'});
    if (!exists($MIME{$args->{'mime'}}))
    {
        # get HTML MIME code
        $args->{'mime'} = 'HTML';
    }

    # nothing to add for non-HTML content
    if ('HTML' ne $args->{'mime'})
    {
        # just return an empty string
        return '';
    }

    return ProcessTemplate('global_footer.tt');
}


=pod

=head2 GetPageMode

B<Description>: return current page mode.

B<ArgsCount>: 0

B<Return>: (string)

Current page mode. See $PAGE_MODE_* constants for available values.

B<Caller>: general

B<Example>:

    my $page_mode = GetPageMode();

=cut

sub GetPageMode
{
    my $page_mode =
        GetParameterValuesFromCGI('mode')
        || GetParameterValuesFromCGI('mmode')
        || GetParameterValues('mode')
        || GetParameterValues('mmode')
        || $PAGE_MODE_DEFAULT;
    return $page_mode;
}


=pod

=head2 GetLinkTarget

B<Description>: return HTML target property for an anchor.

B<ArgsCount>: 0

B<Return>: (string)

HTML target property for an anchor or an empty string.

B<Caller>: templates

=cut

sub GetLinkTarget
{
    # if current page is in a frame, target parent
    my $current_mode = GetPageMode();
    if ($PAGE_MODE_FRAME eq $current_mode)
    {
        return ' target="_parent" ';
    }
    return '';
}


=pod

=head2 RenderMessage

B<Description>: render an HTML message.

B<ArgsCount>: 1

=over 4

=item $parameters: (hash ref) (R)

a hash containing the message parameters:
 message => message content. Requiered.
 type => message type.
 with_details => when set to true, it displays additional details like the
     originating template and calling URL.

=back

B<Return>: (string)

the HTML content to send to the client web browser.

B<Caller>: general

B<Example>:

    print RenderMessage({
        'message' => 'Unable to proceed!',
        'type'    => 'warning',
    });

=cut

sub RenderMessage
{
    my ($parameters) = @_;

    if (!$parameters->{'message'})
    {
        confess "ERROR: RenderMessage: no message provided!\n";
    }

    return ProcessTemplate(
        'message.tt',
        $parameters,
    );
}


=pod

=head2 RenderGreenphylError

B<Description>: render a full error report page with the given content.

B<ArgsCount>: 0-1

=over 4

=item $parameters: (hash ref) (O)

a hash containing a title, the message and some other parameters just like the
$option_hash of RenderHTMLFullPage:
 title   => the page title. Default: 'Error'.
 message => message content. If not set, a default error message is set.

=back

B<Return>: (string)

the HTML content to send to the client web browser.

B<Caller>: general

B<Example>:

    print RenderGreenphylError({'title' => 'My Page Error', 'message' => 'A fatal error occured!'});

=cut

sub RenderGreenphylError
{
    my ($parameters) = @_;

    $parameters ||= {};

    if (ref($parameters) ne 'HASH')
    {
        confess "ERROR: RenderGreenphylError: Invalid parameter! Not a hash ref.\n";
    }

    # use error template
    $parameters->{'content'} = 'error.tt';
    $parameters->{'title'} ||= 'Error';

    # check mode
    my $mode = Greenphyl::GetParameterValues('mode');
    if ($mode)
    {
        $parameters->{'mode'} = $mode;
    }

    # display details in debug mode
    if (Greenphyl::GP('DEBUG_MODE'))
    {
        $parameters->{'details'} = 1;
    }

    return RenderHTMLFullPage($parameters);

}


=pod

=head2 RenderToggler

B<Description>: render an open/close HTML block.

B<ArgsCount>: 3-5

=over 4

=item $id: (string) (R)

HTML ID of the elements.

=item $label: (string) (R)

Text to be displayed as clicable title for the block to hide or show th block.

=item $start_visible: (boolean) (R)

Default block visibility status:
    0 = closed at the loading of the page (default).
    1 = start open;

=item $content: (string) (O)

HTML content of the block.

=item $header_content: (string) (O)

HTML content that will always be displayed between the label and the content of
the block. This content will always be displayed even if the content is
colapsed (hidden).

=back

B<Return>: (string)

the HTML content to send to the client web browser.

B<Caller>: general

B<Example>:

    print RenderToggler('infoseq', 'Family composition', 1, $content);

=cut

sub RenderToggler
{
    my ($block_id, $label, $start_visible, $content, $header_content) = @_;

    if (!$block_id)
    {
        confess "ERROR: RenderToggler: no block identifier provided!\n";
    }
    elsif (!CheckHTMLIdentifierOk($block_id))
    {
        cluck "WARNING: RenderToggler: Invalid block identifier provided ('$block_id')!\n";
    }

    $label ||= 'Toggle';

    return ProcessTemplate(
        'div_toggle.tt',
        {
            'id'             => $block_id,
            'label'          => $label,
            'header_content' => $header_content,
            'content'        => $content,
            'start_visible'  => $start_visible,
        }
    );
}


=pod

=head2 CheckHTMLIdentifierOk

B<Description>: check if an identifier is valid.

B<ArgsCount>: 1

=over 4

=item $identifier: (string) (R)

HTML identifier to check.

=back

B<Return>: (boolean)

true if the identifier is valid, false otherwise.

B<Caller>: general

B<Example>:

    if (!CheckHTMLIdentifierOk($my_identifier))
    {
        confess "ERROR: invalid identifier!\n";
    }

=cut

sub CheckHTMLIdentifierOk
{
    my ($identifier) = @_;

    if ($identifier && ($identifier =~ m/^[A-Za-z][\w\-]*$/))
    {
        # valid identifier:
        # -Must begin with a letter A-Z or a-z
        # -Can only be followed by any number of: letters (A-Za-z),
        #  digits (0-9), hyphens ("-"), and/or underscores ("_")
        return 1;
    }
    return 0;
}


=pod

=head2 IsHTMLIdentifierAvailable

B<Description>: tells if an HTML identifier has already been declared as used.

B<ArgsCount>: 1

=over 4

=item $identifier: (string) (R)

HTML identifier to check.

=back

B<Return>: (boolean)

true if the identifier is available, false otherwise.

B<Caller>: general

B<Example>:

    if (IsHTMLIdentifierAvailable($my_identifier))
    {
        ...
    }

=cut

sub IsHTMLIdentifierAvailable
{
    my ($identifier) = @_;
    if (exists($g_used_html_identifiers->{$identifier}))
    {return 0;}
    else
    {return 1;}
}


=pod

=head2 UseHTMLIdentifier

B<Description>: declare an HTML identifier as being used.

B<ArgsCount>: 1

=over 4

=item $identifier: (string) (R)

used HTML identifier.

=back

B<Return>: (string)

the identifier.

B<Caller>: general

B<Example>:

    $my_identifier = UseHTMLIdentifier($my_identifier);

=cut

sub UseHTMLIdentifier
{
    my ($identifier) = @_;
    $g_used_html_identifiers->{$identifier} = 1;
    return $identifier;
}


=pod

=head2 GetFreeHTMLIdentifier

B<Description>: return a free identifier base on the given optional base string.

B<ArgsCount>: 1

=over 4

=item $base_id: (string) (R)

a base string to use to create a free HTML identifier.

=back

B<Return>: (string)

the new free identifier.

B<Caller>: general

B<Example>:

    $my_identifier = GetFreeHTMLIdentifier($base_identifier);

=cut

sub GetFreeHTMLIdentifier
{
    my ($base_id) = @_;
    my $new_identifier = $base_id;
    while (exists($g_used_html_identifiers->{$new_identifier}))
    {
        $new_identifier = $base_id . '_' . $g_html_identifier_suffix++;
    }
    $g_used_html_identifiers->{$new_identifier} = 1;
    return $new_identifier;
}


=pod

=head2 GetAlignmentURL

B<Description>: Returns the URL of the alignment if family has been processed
and the file is available or false (empty string) otherwise.

B<ArgsCount>: 1-2

=over 4

=item $family: (Greenphyl::Family) (R)

A family object.

=item $type: (string) (O)

Requested alignment type. Can be one of:
'raw', 'masking' or 'filtered'.
Default: 'raw'

=back

B<Return>: (string)

The URL of the alignment or false.

B<Caller>: general

B<Example>:

    if (my $alignment_url = GetAlignmentURL($family, 'filtered'))
    {
        ...
    }

=cut

sub GetAlignmentURL
{
    my ($family, $type) = @_;

    # recognized output types
    my %output_type =
        (
            'raw'      => Greenphyl::GP('RAW_ALIGNMENT_FILE_SUFFIX'),
            'masking1' => '.1' . Greenphyl::GP('MASKING_FILE_SUFFIX'),
            'masking2' => '.2' . Greenphyl::GP('MASKING_FILE_SUFFIX'),
            'filtered' => Greenphyl::GP('FILTERED_FILE_SUFFIX'),
            'hmm'      => Greenphyl::GP('HMM_FILE_SUFFIX'),
        );

    if (!$type || !exists($output_type{$type}))
    {
        $type = 'raw';
    }

    # make sure file exists and is readable
    my $alignment_path = Greenphyl::GP('MULTI_ALIGN_PATH') . '/' . $family->getSubPath() . '/' .$family->accession . '/' .$family->accession . $output_type{$type};
    my $alignment_url  = GetURL('multi_align', undef, {'path' => '/' . $family->getSubPath() . '/' . $family->accession . '/' . $family->accession . $output_type{$type}});

    if (!-e $alignment_path)
    {
        # fallback into old naming scheme (without GP0* prefix)
        $alignment_path = Greenphyl::GP('MULTI_ALIGN_PATH') . '/' . $family->id . $output_type{$type};
        $alignment_url  = GetURL('multi_align', undef, {'path' => '/' . $family->id . $output_type{$type}});
    }

    if (-r $alignment_path)
    {return $alignment_url;}
    else
    {return '';}
}


=pod

=head2 GetPhylogenyAnalyzesURL

B<Description>: Returns the URL of the phylogeny tree if family has been
processed and the file is available or false (empty string) otherwise.

B<ArgsCount>: 1-2

=over 4

=item $family: (Greenphyl::Family) (R)

A family object.

=item $type: (string) (O)

Requested phylogeny tree type. Can be one of:
'nwk' or 'xml'.
Default: 'xml'

=back

B<Return>: (string)

The URL of the phylogeny tree or false.

B<Caller>: general

B<Example>:

    if (my $phylogeny_tree_url = GetPhylogenyAnalyzesURL($family, 'xml'))
    {
        ...
    }

=cut

sub GetPhylogenyAnalyzesURL
{
    my ($family, $type) = @_;

    # recognized output types
    my %output_type =
        (
            'nwk' => Greenphyl::GP('PHYLO_NEWICK_TREE_FILE_SUFFIX'),
            'newick' => Greenphyl::GP('PHYLO_NEWICK_TREE_FILE_SUFFIX'),
            'xml' => Greenphyl::GP('PHYLO_XML_TREE_FILE_SUFFIX'),
        );

    if (!$type || !exists($output_type{$type}))
    {
        $type = 'nwk';
    }

    # make sure file exists and is readable
    my $tree_path = Greenphyl::GP('TREES_PATH') . '/' . $family->getSubPath() . '/' . $family->accession . '/' . $family->accession . $output_type{$type};
    if (!-e $tree_path)
    {
        # fallback into old naming scheme (without GP0* prefix)
        $tree_path = Greenphyl::GP('TREES_PATH') . '/' . $family->id() . $output_type{$type};
    }
    my $tree_url  = GetURL('get_tree', {'family_id' => $family->id(), 'format' => $type});

    if (-r $tree_path)
    {return $tree_url;}
    else
    {return '';}
}


=pod

=head2 GetFamilyFileURL

B<Description>: Returns the URL of the requested if family has that file or
false (empty string) otherwise.

B<ArgsCount>: 2

=over 4

=item $family: (Greenphyl::Family) (R)

A family object.

=item $type: (string) (R)

Requested file type.

=back

B<Return>: (string)

The URL of the requested file or false.

B<Caller>: general

B<Example>:

    if (my $hmm_url = GetFamilyFileURL($family, 'hmm'))
    {
        ...
    }

=cut

sub GetFamilyFileURL
{
    my ($family, $type) = @_;

    # recognized output types
    my %output_file_settings =
        (
            'hmm' => {
                'path'   => Greenphyl::GP('HMM_PATH'),
                'prefix' => '',
                'suffix' => Greenphyl::GP('HMM_FILE_SUFFIX'),
                'url'    => 'hmm',
            },
        );

    if (!$type || !exists($output_file_settings{$type}))
    {
        confess 'Invalid file type!' . (defined($type) ? " type: '$type'" : '') . "\n";
    }

    # make sure file exists and is readable
    my $file_path =  $output_file_settings{$type}->{'path'} . '/' . $output_file_settings{$type}->{'prefix'} . $family->accession . $output_file_settings{$type}->{'suffix'};
    my $file_url  = GetURL($output_file_settings{$type}->{'url'}, undef, {'path' => '/' . $output_file_settings{$type}->{'prefix'} . $family->accession . $output_file_settings{$type}->{'suffix'}});

    if (-r $file_path)
    {return $file_url;}
    else
    {return '';}
}


=pod

=head2 GetPhylogenyAnalyzesPath

B<Description>: Returns the path to the phylogeny tree if family has been
processed and the file is available or false (0) otherwise.

B<ArgsCount>: 1-2

=over 4

=item $family: (Greenphyl::Family) (R)

A family object.

=item $type: (string) (O)

Requested phylogeny tree type. Can be one of:
'nwk' or 'xml'.
Default: 'xml'

=back

B<Return>: (string)

The URL of the phylogeny tree or false.

B<Caller>: general

B<Example>:

    if (my $phylogeny_tree_file = GetPhylogenyAnalyzesPath($family, 'xml'))
    {
        ...
    }

=cut

sub GetPhylogenyAnalyzesPath
{
    my ($family, $type) = @_;

    # recognized output types
    my %output_type =
        (
            'nwk'    => Greenphyl::GP('PHYLO_NEWICK_TREE_FILE_SUFFIX'),
            'newick' => Greenphyl::GP('PHYLO_NEWICK_TREE_FILE_SUFFIX'),
            'xml'    => Greenphyl::GP('PHYLO_XML_TREE_FILE_SUFFIX'),
        );

    if (!$type || !exists($output_type{$type}))
    {
        $type = 'xml';
    }

    # make sure file exists and is readable
    my $tree_path = Greenphyl::GP('TREES_PATH') . '/' . $family->getSubPath() . '/' .$family->accession . '/' . $family->accession . $output_type{$type};
    if (!-e $tree_path)
    {
        # fallback into old naming scheme (without GP0* prefix)
        $tree_path = Greenphyl::GP('TREES_PATH') . '/' . $family->id() . $output_type{$type};
    }

    if (-r $tree_path)
    {return $tree_path;}
    else
    {return 0;}
}


=pod

=head1 Greenphyl Package Extension

=head2 _PrintCGIWarnDebug, _PrintCGICluckDebug

B<Description>: These functions are called by Greenphyl::PrintDebug. See the
initilaization part in the end of this module.

=cut

sub _PrintCGIWarnDebug
{
    my ($message) = @_;
    # trim ending spaces and CR/LF
    $message =~ s/[\s\n\r]*$//;
    LogWebDebugMessage($message);
    warn "DEBUG: $message\n";
}

sub _PrintCGICluckDebug
{
    my ($message) = @_;
    # trim ending spaces and CR/LF
    $message =~ s/[\s\n\r]*$//;
    LogWebDebugMessage($message);
    cluck "DEBUG: $message\n";
}


=pod

=head2 SelectActiveDatabaseFromCGI

B<Description>: Sets and returns index of current database.

B<ArgsCount>: 0-1

=over 4

=item $db_index: (integer) (O)

index of the database to use. See Greenphyl::Config (Config.pm) for available
indexes.

=back

B<Return>: (index)

Index of selected (active) database.

B<Caller>: general

B<Example>:

     if (0 == SelectActiveDatabase())
     {
        # default database selected
        ...
     }
     ...
     # activate 3rd database (index=2)
     SelectActiveDatabase(2);

=cut

# keep a pointer to previous version
sub SelectActiveDatabaseWithoutCGI;
*SelectActiveDatabaseWithoutCGI = \&Greenphyl::SelectActiveDatabase;

sub SelectActiveDatabaseFromCGI
{

    my ($db_index) = @_;
    my $session = GetSession();

    # check if a database has been specified by function call
    if (!defined($db_index) && Greenphyl::Web::Access::IsAdmin())
    {
        # not specified by function arguments, check URL
        $db_index = GetParameterValuesFromCGI('db_index');
        if (defined($db_index))
        {
            PrintDebug("Got database index from CGI parameters: $db_index");
        }

        # not in URL, check session object
        if ((!defined($db_index) || ($db_index !~ m/^\d+$/)) && $session)
        {
            PrintDebug("Try to get database index from session...");
            $db_index = $session->param('db_index');
            if (defined($db_index))
            {
                PrintDebug("Got database index from session: $db_index");
            }
        }
    }
    
    # call non-CGI version
    $db_index = SelectActiveDatabaseWithoutCGI($db_index);

    if ($session)
    {
        PrintDebug("Save database index ($db_index) into session object");
        $session->param('db_index', $db_index);
    }

    return $db_index;
}
# replace original function
if ($Greenphyl::IS_CGI)
{
    no warnings; # disable subroutine redefinition warning in current block
    *Greenphyl::SelectActiveDatabase = \&SelectActiveDatabaseFromCGI;
    *main::SelectActiveDatabase = \&SelectActiveDatabaseFromCGI;
}


=pod

=head2 GetParameterValuesFromCGI

B<Description>: gets the a parameter value from CGI GET or POST parameters from
a given optional namespace. It tries first to fetch the parameter from the
namespace from the GET method, then from the namespace from the POST method,
then without namespace from the GET method and finaly without namespace from the
POST method.

The number of returned values depend on the calling context.

Note: This function replaces Greenphyl::GetParameterValues.

B<ArgsCount>: 1-4

=over 4

=item $parameter_name: (string) (R)

Name of the parameter (case sensitive).

=item $namespace: (string) (U)

the namespace the parameter may belong to. Default: no namespace.

=item $split: (string) (U)

If set, split each value using the given expression. Default: no split.

=item $allow_file: (boolean) (O)

If set, values can be loaded from an uploaded file.

=back

B<Return>: in scalar context, a string containing the first parameter value
found or undef. In list context, a list of values or an empty list.

=cut

sub GetParameterValuesFromCGI
{
    my ($parameter_name, $namespace, $split, $allow_file) = @_;

    $namespace ||= '';
    if ($namespace)
    {
        $namespace .= '.';
    }

    PrintDebug("DEBUG: fetching URL parameter '$parameter_name' using namespace '$namespace'");

    my $cgi  = GetCGI();
    my $parameter_name_found = '';
    # get from GET with namespace
    my @values = ($cgi->url_param($namespace . $parameter_name));
    if (!@values)
    {
        # no success, get from POST with namespace
        @values =($cgi->param($namespace . $parameter_name));
        if (!@values)
        {
            # still no success, get from GET without namespace
            @values =($cgi->url_param($parameter_name));
            if (!@values)
            {
                # still no success, get from POST without namespace
                @values =($cgi->param($parameter_name));
            }
            else
            {
                $parameter_name_found = $parameter_name;
            }
        }
        else
        {
            $parameter_name_found = $namespace . $parameter_name;
        }
    }
    elsif (!$allow_file)
    {
        # file can't be provided through GET method
        $parameter_name_found = $namespace . $parameter_name;
    }
    
    PrintDebug("Got " . scalar(@values) . " value(s) from CGI parameters");

    # check for CGI upload (file)
    if ($allow_file && $parameter_name_found)
    {
        PrintDebug("Checking for files...");
        my @old_values = @values;
        @values = ();
        foreach my $value (@old_values)
        {
            # check if field is an uploaded file or not
            if ($value && (!-e $cgi->tmpFileName($value)))
            {
                # not a file, push back value
                push(@values, $value);
                PrintDebug(sprintf("-'%.16s(...)': not a file", $value));
            }
        }

        my @fh = $cgi->upload($parameter_name_found);
        foreach my $fh (@fh)
        {
            PrintDebug("-got a file");
            # undef may be returned if it's not a valid file handle
            if (defined($fh))
            {
                # Upgrade the handle to one compatible with IO::Handle:
                my $io_handle = $fh->handle;
                my ($sh, $file_content);
                if (open($sh,'>', \$file_content))
                {
                    my ($bytes_read, $buffer);
                    while ($bytes_read = $io_handle->read($buffer, 1024000))
                    {
                        print {$sh} $buffer;
                    }
                    push(@values, $file_content);
                    PrintDebug("  File content loaded!");
                }
                else
                {
                    confess "ERROR: Failed to open string handle for file upload: $!";
                }
            }
            else
            {
                PrintDebug("  No file handle!");
            }
        }
        PrintDebug("...done checking files.");
    }

    # check for split
    if (defined($split))
    {
        @values = map {split(/$split/)} @values;
    }
    @values = grep(defined, @values);

    PrintDebug("DEBUG: got " . scalar(@values) . " value(s) (from CGI)");
    PrintDebug("DEBUG: got: '" . join("', '", @values) . "'");
    if (wantarray())
    {
        return @values;
    }
    else
    {
        return $values[0];
    }
}

# replace original function
if ($Greenphyl::IS_CGI)
{
    no warnings; # disable subroutine redefinition warning in current block
    *Greenphyl::GetParameterValues = \&GetParameterValuesFromCGI;
    *main::GetParameterValues = \&GetParameterValuesFromCGI;
}



=pod

=head2 GetFileContentFromCGI

B<Description>: returns the file(s) content of the given CGI field.

B<ArgsCount>: 1-2

=over 4

=item $parameter_name: (string) (R)

Name of the parameter (case sensitive).

=item $namespace: (string) (U)

the namespace the parameter may belong to. Default: no namespace.

=back

B<Return>: (string)

Returns the file content in a string in scalar context. In list context, returns
a list of strings.

=cut

sub GetFileContentFromCGI
{
    my ($parameter_name, $namespace) = @_;


    $namespace ||= '';
    if ($namespace)
    {
        $namespace .= '.';
    }

    my $cgi  = GetCGI();
    my @fhs = $cgi->upload($namespace . $parameter_name);
    if (!@fhs)
    {
        my @fhs = $cgi->upload($parameter_name);
    }

    my @values;
    foreach my $fh (@fhs)
    {
        if (defined($fh))
        {
            # Upgrade the handle to one compatible with IO::Handle:
            my $io_handle = $fh->handle;
            my $buffer;
            my $data = '';
            while ($io_handle->read($buffer, 1024))
            {
                $data .= $buffer;
            }
            push(@values, $data);
        }
    }

    if (wantarray())
    {
        return @values;
    }
    else
    {
        return $values[0];
    }
}


=pod

=head2 HandleCGIErrors

B<Description>: check if an error occured and display an error message.

Note: this function replaces Greenphyl::HandleErrors.

B<ArgsCount>: 0

B<Return>:

nothing.

B<Caller>: general

B<Example>:

    HandleErrors();

=cut

sub HandleCGIErrors
{
    # catch block
    my $error;
    if ($error = Exception::Class->caught('Greenphyl::Redirect'))
    {
        print $error->redirect();
    }
    elsif ($error = Exception::Class->caught('Greenphyl::AccessDenied'))
    {
        my $page_parameters;
        if (Greenphyl::Web::Access::GetCurrentUser())
        {
            # display logout
            $page_parameters = {
                'message' => $error->message,
                'after_content' => 'login_block.tt',
            };
            print RenderGreenphylError($page_parameters);
        }
        else
        {
            # # display registration form
            # # anti-spam check
            # my ($code, $label) = InitAntiSpam();
            # 
            # $page_parameters = GetOpenIDPageParameters({
            #     'before_content' => '<br/>',
            #     'before_form_content' => RenderMessage({'message' => $error->message, 'message_type' => 'error'}),
            #     'title'   => 'Create your GreenPhyl account',
            #     'content' => 'users/register_form.tt',
            #     'form_data' =>
            #         {
            #             'identifier' => 'registration_form',
            #             'action'     => GetURL('account_request', {'p' => 'register'}),
            #             'submit'     => 'Register',                
            #         },
            #     'antispam' => $label,
            # });

            # display login form
            $page_parameters = GetOpenIDPageParameters({
                'before_content' => '<br/>',
                'before_form_content' => RenderMessage({'message' => $error->message, 'message_type' => 'info'}),
                'title'   => 'Login',
                'content' => 'users/login.tt',
                'form_data' =>
                    {
                        'identifier'  => 'login_page_form',
                        'action'     => GetURL('login'),
                        'submit'      => 'Login',
                        'customstyle' => 'center-block',
                    },
            });

            print RenderHTMLFullPage($page_parameters);
        }
        cluck($error);
    }
    elsif (GP('DEBUG_MODE'))
    {
        if ($error = Exception::Class->caught())
        {
            print RenderGreenphylError({'message' => $error});
            cluck($error);
        }
    }
    elsif ($error = Exception::Class->caught('Greenphyl::Error'))
    {
        print RenderGreenphylError({'message' => $error->message});
        cluck($error);
    }
    elsif ($error = Exception::Class->caught())
    {
        print RenderGreenphylError();
        cluck($error);
    }
}
# replace original function
if ($Greenphyl::IS_CGI)
{
    no warnings; # disable subroutine redefinition warning in current block
    *Greenphyl::HandleErrors = \&HandleCGIErrors;
    *main::HandleErrors = \&HandleCGIErrors;
}


=pod

=head2 GetAccessErrorMessage

B<Description>: returns last login attempt error message.

B<ArgsCount>: 0

B<Return>: (string/Greenphyl::Error)

Error message (Greenphyl::Error object) or an empty string.

B<Caller>: general

B<Example>:

    my $error_message = GetAccessErrorMessage();

=cut

sub GetAccessErrorMessage
{
    return $g_access_message;
}


=pod

=head2 Redirect

B<Description>: redirects to another URI. It must be the only thing printed by
current page.

B<ArgsCount>: 1

=over 4

=item $uri: (string) (R)

target URI.

=back

B<Return>: (nothing)

B<Caller>: general

B<Example>:

    Redirect(GetURL('base'));

=cut

sub Redirect
{
    my ($uri) = @_;
    return $g_cgi->redirect('-uri' => $uri);
}


=pod

=head2 InitAntiSpam

B<Description>: 

B<ArgsCount>: 0

B<Return>: (nothing)

B<Caller>: general

=cut

sub InitAntiSpam
{
    my $session = GetSession();

    my $sql_query = "SELECT code, organism FROM species WHERE display_order > 0;";
    my $codes = GetDatabaseHandler()->selectall_arrayref($sql_query, { 'Slice' => {} })
        or confess "ERROR: Failed to get species codes for anti-spam:\n" . GetDatabaseHandler()->errstr;

    my $code = $codes->[int(rand()*scalar(@$codes))];

    $session->param('antispam', $code->{'code'});

    return ($code->{'code'}, $code->{'organism'});
}


=pod

=head2 CheckAntiSpam

B<Description>: 

B<ArgsCount>: 0

B<Return>: (nothing)

B<Caller>: general

B<Example>:

    * In the CGI:
    sub RenderSomeForm
    {

        # anti-spam check
        my ($code, $label) = InitAntiSpam();

        ...

        return RenderHTMLFullPage(
            {
                ...
                'antispam' => $label,
            },
        );
    }

    sub RenderSomeFormSubmission
    {
        # check for robot
        CheckAntiSpam();
        ...
    }

    
    * In the template:
    <label class="spamcheck"><div class="inline-block" style="width: 12ex;">Do not fill:</div> <input name="nofill" id="nofill" type="text" /></label><br/>
    [% INCLUDE antispam.tt %]

=cut

sub CheckAntiSpam
{
    # anti-spam check 1
    my $nofill   = GetParameterValuesFromCGI('nofill');
    if ($nofill && ($nofill =~ m/\S/))
    {
        Throw('error' => "You filled something you should'nt! Please go back and check the form again.");
    }
    
    # anti-spam check 2
    my $antispam = GetParameterValuesFromCGI('antispam') || '';
    my $session = GetSession();
    if (!$session->param('antispam'))
    {
        Throw('error' => "Your form data has been lost either because you disabled cookies or because you filled the form too long ago. You may try again.");
    }
    elsif ($session->param('antispam') ne $antispam)
    {
        my $debug_details = '';
        if (GP('DEBUG_MODE'))
        {
            $debug_details = " (" . $session->param('antispam') . " vs $antispam)";
        }
        $session->param('antispam', '');
        Throw('error' => "You did not enter the right species code$debug_details as requested by the anti-bot check. You may try again. If you go back, please refresh the page to get a new code!");
    }
    $session->param('antispam', '');
}


=pod

=head2 RenderIssue

B<Description>: Renders issue page.

B<ArgsCount>: 0

B<Return>: (string)

Returns an HTML string containing the issue page.

=cut

sub RenderIssue
{
    my $page_parameters = {
            'title'   => 'Issue',
            'content' => 'miscelaneous/issue.tt',
        };

    # change appearance when in popup mode
    my $mode = GetPageMode();
    if ($Greenphyl::Web::PAGE_MODE_FRAME eq $mode)
    {
        $page_parameters->{'title'}   = 'Issue';
        $page_parameters->{'body_id'} = '';
    }

    return RenderHTMLFullPage($page_parameters);
}




# CODE START
#############

if (($Greenphyl::IS_CGI) && (GP('DEBUG_MODE')))
{
    # replace original function
    no warnings; # disable subroutine redefinition warning in current block
    if (GP('DEBUG_MODE') == 2)
    {
        *Greenphyl::_PrintDebug = \&_PrintCGICluckDebug;
    }
    else
    {
        *Greenphyl::_PrintDebug = \&_PrintCGIWarnDebug;
    }
}

# Init access stuff
eval
{
    Greenphyl::Web::InitAccess();
};

my $error;
if ($error = Exception::Class->caught('Greenphyl::Redirection'))
{
    print $error->redirect();
    exit(0);
}
elsif ($error = Exception::Class->caught('Greenphyl::Error'))
{
    $g_access_message = $error;
}

PrintDebug('Session: ' . $g_session->id());

# update database index using Web parameters
SelectActiveDatabaseFromCGI();


# CODE END
###########


=pod

=head1 AUTHORS

Mathieu ROUARD (Bioversity), m.rouard@cgiar.org
Valentin GUIGNON (Bioversity), v.guignon@cgiar.org
Matthieu CONTE (Syngenta)

=head1 VERSION

Version 3.0.0

Date 23/03/12

=head1 SEE ALSO

GreenPhyl documentation.

=cut

return 1; # package return
