=pod

=head1 NAME

Greenphyl::GO - GreenPhyl Gene Ontology Object

=head1 SYNOPSIS

    use Greenphyl::GO;
    my $go = Greenphyl::GO->new($dbh, {'selectors' => {'id' => 42}});
    print $go->description();

=head1 REQUIRES

Perl5

=head1 EXPORTS

Nothing

=head1 DESCRIPTION

This module implements a GreenPhyl GO database object.

=cut

package Greenphyl::GO;

use strict;
use warnings;

use base qw(Greenphyl::CachedDBObject);

use vars qw($AUTOLOAD);

use Carp qw(cluck confess croak);

use Greenphyl;
use Greenphyl::Family;
use Greenphyl::Sequence;




# Package constants
####################

=pod

=head1 CONSTANTS

B<$DEBUG>: (boolean)

Enables debug mode.

B<$OBJECT_PROPERTIES>: (hash ref)

object properties description. See DBObject constructor documentation for
details.

=cut

our $DEBUG = 0;
our $OBJECT_PROPERTIES = {
    'table' => 'go',
    'key' => 'id',
    'alternate_keys' => ['code'],
    'load'     => [
        'code',
        'id',
        'name',
        'namespace',
    ],
    'alias' => {
        'go_id'   => 'id',
        'go_code' => 'code',
        'type'    => 'namespace',
    },
    'base' => {
        'code' => {
            'property_column' => 'code',
        },
        'name' => {
            'property_column' => 'name',
        },
        'id' => {
            'property_column' => 'id',
        },
        'namespace' => {
            'property_column' => 'namespace',
        },
    },
    'secondary' => {
        'alternate_codes' => {
            'property_table'  => 'go_alternates',
            'property_key'    => 'go_id',
            'multiple_values' => 1,
            'property_column' => 'alternate_code',
        },
        'synonyms' => {
            'property_table'  => 'go_synonyms',
            'property_key'    => 'go_id',
            'multiple_values' => 1,
            'property_column' => 'synonym',
        },
        'sequence_sources' => {
            'property_table'  => 'go_sequences_cache',
            'property_key'    => 'go_id',
            'multiple_values' => 1,
            'property_module' => 'Greenphyl::GOSequence',
        },
        'family_sources' => {
            'property_table'  => 'families_go_cache',
            'property_key'    => 'go_id',
            'multiple_values' => 1,
            'property_module' => 'Greenphyl::FamilyGO',
        },
        'lineage_go_codes' => {
            'property_table'  => 'go_lineage',
            'property_key'    => 'go_id',
            'multiple_values' => 0,
            'property_column' => 'lineage_go_codes',
        },
    },
    'tertiary' => {
        'sequences' => {
            'link_table'        => 'go_sequences_cache',
            'link_object_key'   => 'go_id',
            'link_property_key' => 'sequence_id',
            'property_table'    => 'sequences',
            'property_key'      => 'id',
            'multiple_values'   => 1,
            'property_module'   => 'Greenphyl::Sequence',
        },
        'families' => {
            'link_table'        => 'families_go_cache',
            'link_object_key'   => 'go_id',
            'link_property_key' => 'family_id',
            'property_table'    => 'families',
            'property_key'      => 'id',
            'multiple_values'   => 1,
            'property_module'   => 'Greenphyl::Family',
        },
        'ipr' => {
            'link_table'        => 'go_ipr',
            'link_object_key'   => 'go_id',
            'link_property_key' => 'ipr_id',
            'property_table'    => 'ipr',
            'property_key'      => 'id',
            'multiple_values'   => 1,
            'property_module'   => 'Greenphyl::IPR',
        },
        'uniprot' => {
            'link_table'        => 'go_uniprot',
            'link_object_key'   => 'go_id',
            'link_property_key' => 'dbxref_id',
            'property_table'    => 'dbxref',
            'property_key'      => 'id',
            'multiple_values'   => 1,
            'property_module'   => 'Greenphyl::DBXRef',
        },
        'ec' => {
            'link_table'        => 'ec_go',
            'link_object_key'   => 'go_id',
            'link_property_key' => 'dbxref_id',
            'property_table'    => 'dbxref',
            'property_key'      => 'id',
            'multiple_values'   => 0,
            'property_module'   => 'Greenphyl::DBXRef',
        },
        'subject_relationships' => {
            'link_table'        => 'go_relationships',
            'link_object_key'   => 'subject_go_id',
            'link_property_key' => 'object_go_id',
            'property_table'    => 'go',
            'property_key'      => 'id',
            'multiple_values'   => 1,
            'property_module'   => 'Greenphyl::GO',
        },
        'object_relationships' => {
            'link_table'        => 'go_relationships',
            'link_object_key'   => 'object_go_id',
            'link_property_key' => 'subject_go_id',
            'property_table'    => 'go',
            'property_key'      => 'id',
            'multiple_values'   => 1,
            'property_module'   => 'Greenphyl::GO',
        },
    },
};

our $GO_REGEXP = 'GO:\d{7}';
our $FAMILY_GO_THRESHOLD = 50;


# Package subs
###############

=pod

=head1 STATIC METHODS

=head2 CONSTRUCTOR

B<Description>: Creates a new instance of a GreenPhyl GO object.

B<ArgsCount>: 2

=over 4

=item $dbh: (DBI::db) (U)

Database handler of the database to load the object from.

=item $parameters: (hash ref) (R)

See Greenphyl::DBObject constructor documentation for details.

=back

B<Return>: (Greenphyl::GO)

a new instance.

B<Caller>: General

B<Example>:

    my $go = new Greenphyl::GO($dbh, {'selectors' => {'id' => 42}});

=cut

sub new
{
    my $proto = shift();
    my $class = ref($proto) || $proto;
    
    my $object_properties = $class->GetDefaultClassProperties();

    return $class->SUPER::new($object_properties, @_);
}




=pod

=head1 METHODS

=head2 getStringValue

B<Description>: returns GO code.

B<ArgsCount>: 1

=over 4

=item $self: (Greenphyl::GO)

the GO term.

=back

B<Return>: (string)

the GO code.

=cut

sub getStringValue
{
    my ($self) = @_;
    return $self->{'code'};
}


=pod

=head2 getParents

B<Description>: returns parent GO of current GO.

B<Aliases>: parents

B<ArgsCount>: 1

=over 4

=item $self: (Greenphyl::GO)

the GO term.

=back

B<Return>: (array ref)

An array of parent Greenphyl::GO objects or an empty array.

=cut

sub getParents
{
    my ($self) = @_;

    if (!$self->{'parents'})
    {
        my $sql_query = "
            SELECT " . join(', ', Greenphyl::GO->GetDefaultMembers('g')) . "
            FROM go g
                 JOIN go_relationships gr ON (gr.object_go_id = g.id)
            WHERE gr.subject_go_id = ?
                  AND relationship IN ('part_of', 'is_a', 'regulates', 'negatively_regulates', 'positively_regulates')
        ";
        PrintDebug("SQL QUERY: $sql_query\nBindings: " . $self->id);
        my $sql_gos = $self->{'_dbh'}->selectall_arrayref($sql_query, { 'Slice' => {} }, $self->id)
            or confess 'ERROR: Failed to fetch parent GO: ' . $self->{'_dbh'}->errstr;

        my @parent_go;
        foreach my $sql_go (@$sql_gos)
        {
            my $go = Greenphyl::GO->new($self->{'_dbh'}, {'load' => 'none', 'members' => $sql_go});
            push(@parent_go, $go);
        }
        $self->{'parents'} = \@parent_go;
    }

    # return all parent GO
    return $self->{'parents'};
}
# Aliases
sub parents; *parents = \&getParents;


=pod

=head2 getChildren

B<Description>: returns child GO of current GO.

B<Aliases>: children

B<ArgsCount>: 1

=over 4

=item $self: (Greenphyl::GO)

the GO term.

=back

B<Return>: (array ref)

An array of child Greenphyl::GO objects or an empty array.

=cut

sub getChildren
{
    my ($self) = @_;

    if (!$self->{'children'})
    {
        my $sql_query = "
            SELECT " . join(', ', Greenphyl::GO->GetDefaultMembers('g')) . "
            FROM go g
                 JOIN go_relationships gr ON (gr.subject_go_id = g.id)
            WHERE gr.object_go_id = ?
                  AND relationship IN ('part_of', 'is_a', 'regulates', 'negatively_regulates', 'positively_regulates')
        ";
        PrintDebug("SQL QUERY: $sql_query\nBindings: " . $self->id);
        my $sql_gos = $self->{'_dbh'}->selectall_arrayref($sql_query, { 'Slice' => {} }, $self->id)
            or confess 'ERROR: Failed to fetch parent GO: ' . $self->{'_dbh'}->errstr;

        my @parent_go;
        foreach my $sql_go (@$sql_gos)
        {
            my $go = Greenphyl::GO->new($self->{'_dbh'}, {'load' => 'none', 'members' => $sql_go});
            push(@parent_go, $go);
        }
        $self->{'children'} = \@parent_go;
    }

    # return all child GO
    return $self->{'children'};
}
# Aliases
sub children; *children = \&getChildren;


=pod

=head2 getSubjectRelationsWith

B<Description>: returns the relationships between given GO and current GO:
current GO is "getRelationWith" with given GO.

B<ArgsCount>: 2

=over 4

=item $self: (Greenphyl::GO)

the (current) GO term.

=item $other: (Greenphyl::GO)

the related (given) GO term.

=back

B<Return>: (hash ref)

An hash of relationships: keys are relationship labels, values are boolean (1 if
the relationship exists).

=cut

sub getSubjectRelationsWith
{
    my ($self, $other) = @_;
    $self->{'subject_relationships'} ||= {};
    if (!exists($self->{'subject_relationships'}->{$other->code}))
    {
        my $sql_query = '
            SELECT relationship
            FROM go_relationships
            WHERE
              subject_go_id = ?
              AND object_go_id = ?
        ';
        PrintDebug("SQL QUERY: $sql_query\nBindings: " . join(', ', $self->id, $other->id));
        $self->{'subject_relationships'}->{$other->code} =
        {
            map
                {$_ => 1}
                @{$self->{'_dbh'}->selectcol_arrayref($sql_query, undef, $self->id, $other->id)}
        };
    }

    return $self->{'subject_relationships'}->{$other->code};
}


=pod

=head2 getObjectRelationsWith

B<Description>: returns the relationships between given GO and current GO:
given GO is "getRelationWith" with current GO.

B<ArgsCount>: 2

=over 4

=item $self: (Greenphyl::GO)

the (current) GO term.

=item $other: (Greenphyl::GO)

the related (given) GO term.

=back

B<Return>: (hash ref)

An hash of relationships: keys are relationship labels, values are boolean (1 if
the relationship exists).

=cut

sub getObjectRelationsWith
{
    my ($self, $other) = @_;
    $self->{'object_relationships'} ||= {};
    if (!exists($self->{'object_relationships'}->{$other->code}))
    {
        my $sql_query = '
            SELECT relationship
            FROM go_relationships
            WHERE
              object_go_id = ?
              AND subject_go_id = ?
        ';
        PrintDebug("SQL QUERY: $sql_query\nBindings: " . join(', ', $self->id, $other->id));
        $self->{'object_relationships'}->{$other->code} =
        {
            map
                {$_ => 1}
                @{$self->{'_dbh'}->selectcol_array($sql_query, undef, $self->id, $other->id)}
        };
    }

    return $self->{'object_relationships'}->{$other->code};
}


=pod

=head2 getAncestors

B<Description>: returns ancestors GO of current GO.

B<Aliases>: ancestors

B<ArgsCount>: 1

=over 4

=item $self: (Greenphyl::GO)

the GO term.

=back

B<Return>: (array ref)

An array of related Greenphyl::GO objects or an empty array.

=cut

sub getAncestors
{
    my ($self) = @_;

    if (!$self->{'ancestors'})
    {
        my $lineage_go_codes = $self->lineage_go_codes();
        if ($lineage_go_codes)
        {
            my @ancestor_go = Greenphyl::GO->new($self->{'_dbh'}, {'selectors' => {'code' => ['IN', split(',', $lineage_go_codes)]}, });
            $self->{'ancestors'} = \@ancestor_go;
        }
        else
        {
            $self->{'ancestors'} = [];
        }
    }

    # return all ancestors GO
    return $self->{'ancestors'};

}
# Aliases
sub ancestors; *ancestors = \&getAncestors;


=pod

=head2 getDescendant

B<Description>: returns descendant GO of current GO.

B<Aliases>: descendants

B<ArgsCount>: 1

=over 4

=item $self: (Greenphyl::GO)

the GO term.

=back

B<Return>: (array ref)

An array of related Greenphyl::GO objects or an empty array.

=cut

sub getDescendant
{
    my ($self) = @_;

    if (!$self->{'descendants'})
    {
        my $sql_query = "
            SELECT " . join(', ', Greenphyl::GO->GetDefaultMembers('g')) . "
            FROM go g
                 JOIN go_lineage gl ON (gl.go_id = g.id)
            WHERE gl.lineage_go_codes LIKE '%" . $self->code . "%'
        ";

        PrintDebug("SQL QUERY: $sql_query");
        my $sql_gos = $self->{'_dbh'}->selectall_arrayref($sql_query, { 'Slice' => {} })
            or confess 'ERROR: Failed to fetch descendant GO: ' . $self->{'_dbh'}->errstr;

        my @descendant_go;
        foreach my $sql_go (@$sql_gos)
        {
            my $go = Greenphyl::GO->new($self->{'_dbh'}, {'load' => 'none', 'members' => $sql_go});
            push(@descendant_go, $go);
        }
        $self->{'descendants'} = \@descendant_go;
    }

    # return all descendants GO
    return $self->{'descendants'};

}
# Aliases
sub descendants; *descendants = \&getDescendant;


=pod

=head2 getUnderlyingSequences

B<Description>: returns sequences related to current GO.

B<ArgsCount>: 1

=over 4

=item $self: (Greenphyl::GO)

the GO term.

=back

B<Return>: (array ref)

An array of related Greenphyl::Sequence objects or an empty array.

=cut

sub getUnderlyingSequences
{
    my ($self) = @_;

    if (!$self->{'underlying_sequences'})
    {
        # get GO lineage
        my $sql_query = "SELECT go_id FROM go_lineage WHERE lineage_go_codes LIKE '%" . $self->code . "%';";
        PrintDebug("SQL QUERY: $sql_query\nBindings: " . join(', ', ));
        my $underlying_go_ids = $self->{'_dbh'}->selectrow_arrayref($sql_query);

        if (!$underlying_go_ids || !@$underlying_go_ids)
        {
            return 0;
        }

        $sql_query = "
            SELECT " . join(', ', Greenphyl::Sequence->GetDefaultMembers('s')) . "
            FROM sequences s
                JOIN go_sequences_cache gsc ON (gsc.sequence_id = s.id)
                JOIN go g ON (g.id = gsc.go_id)
            WHERE
                g.id IN (" . join(',', @$underlying_go_ids) . ")
            GROUP BY s.id
        ";
        PrintDebug("SQL QUERY: $sql_query");
        my $sql_sequences = $self->{'_dbh'}->selectall_arrayref($sql_query, { 'Slice' => {} })
            or confess $self->{'_dbh'}->errstr;

        my %sequences;
        foreach my $sql_sequence (@$sql_sequences)
        {
            $sequences{$sql_sequence->{'id'}} ||= Greenphyl::Sequence->new($self->{'_dbh'}, {'load' => 'none', 'members' => $sql_sequence});
        }

        $self->{'underlying_sequences'} = [values(%sequences)];
    }

    return $self->{'underlying_sequences'};
}
# aliases
sub underlying_sequences;    *underlying_sequences = \&getUnderlyingSequences;


=pod

=head2 getUnderlyingSequenceCount

B<Description>: returns sequences related to current GO.

B<ArgsCount>: 1

=over 4

=item $self: (Greenphyl::GO)

the GO term.

=back

B<Return>: (array ref)

An array of related Greenphyl::Sequence objects or an empty array.

=cut

sub getUnderlyingSequenceCount
{
    my ($self) = @_;
    if (!$self->{'underlying_sequence_count'})
    {
        # check if we already got underlying sequences
        if ($self->{'underlying_sequences'})
        {
            $self->{'underlying_sequence_count'} = scalar(@{$self->{'underlying_sequences'}});
        }
        else
        {
            # get GO lineage
            my $sql_query = "SELECT go_id FROM go_lineage WHERE lineage_go_codes LIKE '%" . $self->code . "%';";
            PrintDebug("SQL QUERY: $sql_query\nBindings: " . join(', ', ));
            my $underlying_go_ids = $self->{'_dbh'}->selectrow_arrayref($sql_query);

            if (!$underlying_go_ids || !@$underlying_go_ids)
            {
                return 0;
            }

            $sql_query = "
                SELECT count(DISTINCT s.id)
                FROM sequences s
                    JOIN go_sequences_cache gsc ON (gsc.sequence_id = s.id)
                    JOIN go g ON (g.id = gsc.go_id)
                WHERE
                    g.id IN (" . join(',', @$underlying_go_ids) . ")
                GROUP BY s.id
            ";
            PrintDebug("SQL QUERY: $sql_query");
            my ($sequence_count) = $self->{'_dbh'}->selectrow_array($sql_query, undef)
                or confess $self->{'_dbh'}->errstr;

            $self->{'underlying_sequence_count'} = $sequence_count;
        }
    }
    return $self->{'underlying_sequence_count'};
}
# aliases
sub underlying_sequence_count;    *underlying_sequence_count = \&getUnderlyingSequenceCount;


=pod

=head2 getUnderlyingFamilies

B<Description>: returns families related to current GO.

B<Aliases>: families

B<ArgsCount>: 1-2

=over 4

=item $self: (Greenphyl::GO)

the GO term.

=item $selectors: (hash ref)

A selector hash (supports both "{ 'selectors' => { ... } }" and "{ ... }"
structures).

=back

B<Return>: (array ref)

An array of related Greenphyl::Family objects or an empty array.

=cut

sub getUnderlyingFamilies
{
    my ($self, $selectors) = @_;
    if ($selectors)
    {
        # get GO lineage
        my $sql_query = "SELECT go_id FROM go_lineage WHERE lineage_go_codes LIKE '%" . $self->code . "%';";
        PrintDebug("SQL QUERY: $sql_query\nBindings: " . join(', ', ));
        my $underlying_go_ids = $self->{'_dbh'}->selectrow_arrayref($sql_query);

        if (!$underlying_go_ids || !@$underlying_go_ids)
        {
            return 0;
        }

        # if the full selector structure is used, just keep the 'selectors' part
        if (exists($selectors->{'selectors'}))
        {
            $selectors = $selectors->{'selectors'};
        }

        my ($space_holders, $bind_values) = $self->_prepareSelectorsForQuery($selectors);

        $sql_query = "
            SELECT " . join(', ', Greenphyl::Family->GetDefaultMembers('families')) . "
            FROM families families
                JOIN families_go_cache fgc ON (fgc.family_id = families.id)
                JOIN go g ON (g.id = fgc.go_id)
            WHERE
                g.id IN (" . join(',', @$underlying_go_ids) . ")
                " . (@$space_holders?" AND " . join(' AND ', @$space_holders):'') . "
            GROUP BY families.id
        ";
        PrintDebug("SQL QUERY: $sql_query\nBindings: " . join(', ', @$bind_values));
        my $sql_families = $self->{'_dbh'}->selectall_arrayref($sql_query, { 'Slice' => {} }, @$bind_values)
            or confess $self->{'_dbh'}->errstr;

        my %families;
        foreach my $sql_family (@$sql_families)
        {
            $families{$sql_family->{'id'}} ||= Greenphyl::Family->new($self->{'_dbh'}, {'load' => 'none', 'members' => $sql_family});
        }
        return [values(%families)];
    }
    elsif (!$self->{'underlying_families'})
    {
        # get GO lineage
        my $sql_query = "SELECT go_id FROM go_lineage WHERE lineage_go_codes LIKE '%" . $self->code . "%';";
        PrintDebug("SQL QUERY: $sql_query\nBindings: " . join(', ', ));
        my $underlying_go_ids = $self->{'_dbh'}->selectrow_arrayref($sql_query);

        if (!$underlying_go_ids || !@$underlying_go_ids)
        {
            return 0;
        }

        $sql_query = "
            SELECT " . join(', ', Greenphyl::Family->GetDefaultMembers('f')) . "
            FROM families f
                JOIN families_go_cache fgc ON (fgc.family_id = f.id)
                JOIN go g ON (g.id = fgc.go_id)
            WHERE
                g.id IN (" . join(',', @$underlying_go_ids) . ")
            GROUP BY f.id
        ";
        PrintDebug("SQL QUERY: $sql_query");
        my $sql_families = $self->{'_dbh'}->selectall_arrayref($sql_query, { 'Slice' => {} })
            or confess $self->{'_dbh'}->errstr;

        my %families;
        foreach my $sql_family (@$sql_families)
        {
            $families{$sql_family->{'id'}} ||= Greenphyl::Family->new($self->{'_dbh'}, {'load' => 'none', 'members' => $sql_family});
        }

        $self->{'underlying_families'} = [values(%families)];
    }

    return $self->{'underlying_families'};
}
# aliases
sub underlying_families;    *underlying_families = \&getUnderlyingFamilies;


=pod

=head2 getUnderlyingFamilyCount

B<Description>: returns families related to current GO.

B<Aliases>: families

B<ArgsCount>: 1-2

=over 4

=item $self: (Greenphyl::GO)

the GO term.

=item $selectors: (hash ref)

A selector hash (supports both "{ 'selectors' => { ... } }" and "{ ... }"
structures).

=back

B<Return>: (array ref)

An array of related Greenphyl::Family objects or an empty array.

=cut

sub getUnderlyingFamilyCount
{
    my ($self, $selectors) = @_;

    if ($selectors)
    {
        # get GO lineage
        my $sql_query = "SELECT go_id FROM go_lineage WHERE lineage_go_codes LIKE '%" . $self->code . "%';";
        PrintDebug("SQL QUERY: $sql_query\nBindings: " . join(', ', ));
        my $underlying_go_ids = $self->{'_dbh'}->selectcol_arrayref($sql_query);

        if (!$underlying_go_ids || !@$underlying_go_ids)
        {
            return 0;
        }

        # if the full selector structure is used, just keep the 'selectors' part
        if (exists($selectors->{'selectors'}))
        {
            $selectors = $selectors->{'selectors'};
        }

        my ($space_holders, $bind_values) = $self->_prepareSelectorsForQuery($selectors);

        #+FIXME: manage GO association: family has GO if from UniProt or if >60% from IPR
        $sql_query = "
            SELECT COUNT(DISTINCT families.id)
            FROM families families
                JOIN families_go_cache fgc ON (fgc.family_id = families.id)
                JOIN go g ON (g.id = fgc.go_id)
            WHERE
                g.id IN (" . join(',', @$underlying_go_ids) . ")
                AND fgc.percent > $FAMILY_GO_THRESHOLD
                " . (@$space_holders?" AND " . join(' AND ', @$space_holders):'') . "
        ";
        PrintDebug("SQL QUERY: $sql_query\nBindings: " . join(', ', @$bind_values));
        my ($family_count) = ($self->{'_dbh'}->selectrow_array($sql_query, undef, @$bind_values));
        if (!defined($family_count))
        {$family_count = 0;}
        return $family_count;
    }
    elsif (!$self->{'underlying_family_count'})
    {
        # check if we already got underlying families
        if ($self->{'underlying_families'})
        {
            $self->{'underlying_family_count'} = scalar(@{$self->{'underlying_families'}});
        }
        else
        {
            # get GO lineage
            my $sql_query = "SELECT go_id FROM go_lineage WHERE lineage_go_codes LIKE '%" . $self->code . "%';";
            PrintDebug("SQL QUERY: $sql_query\nBindings: " . join(', ', ));
            my $underlying_go_ids = $self->{'_dbh'}->selectrow_arrayref($sql_query);

            if (!$underlying_go_ids || !@$underlying_go_ids)
            {
                return 0;
            }


            $sql_query = "
                SELECT count(DISTINCT f.id)
                FROM families f
                    JOIN families_go_cache fgc ON (fgc.family_id = f.id)
                    JOIN go g ON (g.id = fgc.go_id)
                WHERE
                    g.id IN (" . join(',', @$underlying_go_ids) . ")
                GROUP BY f.id
            ";
            PrintDebug("SQL QUERY: $sql_query");
            my ($family_count) = $self->{'_dbh'}->selectrow_array($sql_query)
                or confess $self->{'_dbh'}->errstr;

            $self->{'underlying_family_count'} = $family_count;
        }
    }
    return $self->{'underlying_family_count'};
}
# aliases
sub underlying_family_count;    *underlying_family_count = \&getUnderlyingFamilyCount;




=pod

=head1 AUTHORS

Mathieu ROUARD (Bioversity), m.rouard@cgiar.org

Valentin GUIGNON (Bioversity), v.guignon@cgiar.org

Matthieu CONTE (Syngenta)

=head1 VERSION

Version 1.1.0

Date 29/04/2013

=head1 SEE ALSO

GreenPhyl documentation, DBObject documentation.

=cut

return 1; # package return
