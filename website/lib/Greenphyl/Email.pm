=pod

=head1 NAME

Greenphyl::Email - Manage e-mail messaging

=head1 SYNOPSIS

    use Greenphyl::Email;

    my $admin_mail = 'v.guignon@cgiar.org';
    ValidateMail($admin_mail) or confess "Not a valid e-mail address: $admin_mail!\n";

    SendMail('email' => $admin_mail, 'subject' => 'Done', 'body' => 'done!');

=head1 REQUIRES

Perl5, sendmail (command line)

=head1 EXPORTS

ValidateMail SendMail

=head1 DESCRIPTION

This module has almost no dependency and can be used to check if an e-mail
address looks valid and it can also send basic e-mails.

=cut

package Greenphyl::Email;


use strict;
use warnings;

use Carp qw(cluck confess croak);

use base qw(Exporter);
our @EXPORT = qw(ValidateMail SendMail); 

use Greenphyl;




# Package constants
####################

=pod

=head1 CONSTANTS

B<$DEBUG>: (boolean)

When set to true, it enables debug mode.

B<$DEFAULT_ADMIN_EMAIL>: (string)

e-mail address used as sender for notification e-mails.

=cut

our $DEBUG = 0;

our $DEFAULT_ADMIN_EMAIL = 'v.guignon@cgiar.org';




# Package subs
###############

=pod

=head1 STATIC METHODS

=head2 ValidateMail

B<Description>: validate a given e-mail address.

B<ArgsCount>: 1

=over 4

=item $target_email: (string) (R)

the target e-mail address to check.

=back

B<Return>: (boolean)

returns true (1) if the mail address is valid, false (0) otherwise.

B<Caller>:

general

B<Example>:

    my $admin_mail = 'v.guignon@cgiar.org';
    ValidateMail($admin_mail) or confess "Not a valid e-mail address: $admin_mail!\n";

=cut

sub ValidateMail
{
    my ($target_email) = @_;

    # check parameters
    if (1 != @_)
    {
        confess "usage: ValidateMail(target_email) or confess 'invalid e-mail address!';";
    }

    # RFC used:
    # -local mailbox: http://tools.ietf.org/html/rfc2822
    # -domain: http://tools.ietf.org/html/rfc1034
    my ($local_mailbox, $domain) = ($target_email =~ m/^(.*)@([a-zA-Z][a-zA-Z0-9\-]*(?:\.[a-zA-Z0-9\-]{1,63})*[a-zA-Z0-9])$/);
    if ((not $local_mailbox)
        || (not $domain)
        || (64 < length($local_mailbox))
        || (255 < length($domain)))
    {
        # invalid local address or domain
        return 0;
    }

    if (($local_mailbox !~ m/^[a-zA-Z0-9!~&'=_#\-\^\$\|\*\+\?\{\}\%\/\`]+(?:\.[a-zA-Z0-9!~&'=_#\-\^\$\|\*\+\?\{\}\%\/\`]+)*$/)
        && ($local_mailbox !~ m/^"(?:[^\\]*(?:\\"[^"]*\\")*(?:\\[^"])*)*"$/))
    {
        # invalid local address
        return 0;
    }

    return 1;
}


=pod

=head2 SendMail

B<Description>: Sends an e-mail.

B<ArgsCount>: hash

=over 4

=item email: (string) (R)

Target e-mail address.

=item subject: (string) (O)

Mail subject.

=item body: (string) (O)

Mail body.

=back

B<Return>: (nothing)

B<Example>:

    SendMail('email' => 'v.guignon@cgiar.org', 'subject' => 'Done', 'body' => 'done!');

=cut

sub SendMail
{
    my (%parameters) = @_;
    
    my $target_email = $parameters{'email'};
    my $mail_subject = $parameters{'subject'};
    my $mail_body    = $parameters{'body'};
    
    if (!$target_email)
    {
        confess "ERROR: SendMail: no target e-mail address provided!\n";
    }
    elsif (!ValidateMail($target_email))
    {
        confess "ERROR: Invalid e-mail address ($target_email)!\n";
    }

    if (!$mail_subject)
    {
        $mail_subject = "[GreenPhyl] Process $$";
    }
    $mail_subject =~ s/[\n\r]+//g;

    if (!$mail_body)
    {
        $mail_body = "-GreenPhyl-";
    }
    $mail_body .= "\n";

    my $sendmail_h;
    if (open($sendmail_h, '|' . GP('SENDMAIL_PATH') . ' -t'))
    {
        print {$sendmail_h} "To: $target_email\n";
        print {$sendmail_h} "From: $DEFAULT_ADMIN_EMAIL\n";
        print {$sendmail_h} "Subject: $mail_subject\n\n";
        print {$sendmail_h} $mail_body;
        close($sendmail_h);
    }
    else
    {
        warn "WARNING: Failed to send notification e-mail!\n$!\n";
    }
}


=pod

=head1 DIAGNOSTICS

=head1 AUTHORS

Mathieu ROUARD (Bioversity), m.rouard@cgiar.org

Valentin GUIGNON (Bioversity), v.guignon@cgiar.org

Matthieu CONTE (Syngenta)

Sebastien BRIOIS (Syngenta)

=head1 VERSION

Version 1.0.0

Date 19/11/2012

=head1 SEE ALSO

GreenPhyl documentation.

=cut

return 1; # package return
