=pod

=head1 NAME

Greenphyl::AbstractFamily - GreenPhyl Family abstraction layer

=head1 SYNOPSIS

    package Greenphyl::Family;

    ...

    use base qw(Greenphyl::AbstractFamily);

=head1 REQUIRES

Perl5

=head1 EXPORTS

Nothing

=head1 DESCRIPTION

This module implements a GreenPhyl family abstraction layer.

=cut

package Greenphyl::AbstractFamily;

use strict;
use warnings;

use base qw(
  Greenphyl::CachedDBObject
  Greenphyl::DumpableObject
  Greenphyl::DumpableTabularData
);

use vars qw($AUTOLOAD);

use Carp qw(cluck confess croak);

use Greenphyl::Config;
use Greenphyl;
use Greenphyl::SourceDatabase;
use Greenphyl::Sequence;
use Greenphyl::AbstractSequence;
use Greenphyl::GO;
use Greenphyl::FamilyIPRStats;
use Greenphyl::SequenceIPR;

use Greenphyl::Tools::Families;
use Greenphyl::Tools::Sequences;
use Greenphyl::Tools::GO;



# Package constants
####################

=pod

=head1 CONSTANTS

B<$OBJECT_PROPERTIES>: (hash ref)

sequence object properties description. See DBObject constructor documentation
for details.

B<$SUPPORTED_DUMP_FORMAT>: (hash)

Contains supported dump format parameters.

B<$FAMILY_REGEXP>: (string)

Family accession matching regular expression.

B<$UNANNOTATED_CLUSTER_NAME>: (string)

Name used for unannotated clusters.

=cut

our $DEBUG = 0;

our $SUPPORTED_DUMP_FORMAT =
    {
        'static'  => {
            'fasta'   => \&DumpFasta,
            'embl'    => \&DumpEMBL,
            'genbank' => \&DumpGenBank,
            'swiss'   => \&DumpSwissprot,
            'scf'     => \&DumpSCF,
            'pir'     => \&DumpPIR,
            'gcg'     => \&DumpGCG,
            'raw'     => \&DumpRaw,
            'ace'     => \&DumpACeDB,
            'game'    => \&DumpGameXML,
            'phd'     => \&DumpPhred,
            'qual'    => \&DumpQualityValues,
            'excel'   => \&DumpExcel,
            'csv'     => \&DumpCSV,
            'xml'     => \&DumpXML,
            'json'    => \&DumpJSON,
        },
        'dynamic' => {
            'fasta'   => \&DumpFasta,
            'embl'    => \&DumpEMBL,
            'genbank' => \&DumpGenBank,
            'swiss'   => \&DumpSwissprot,
            'scf'     => \&DumpSCF,
            'pir'     => \&DumpPIR,
            'gcg'     => \&DumpGCG,
            'raw'     => \&DumpRaw,
            'ace'     => \&DumpACeDB,
            'game'    => \&DumpGameXML,
            'phd'     => \&DumpPhred,
            'qual'    => \&DumpQualityValues,
            'excel'   => \&DumpExcel,
            'csv'     => \&DumpCSV,
            'xml'     => \&DumpXML,
            'json'    => \&DumpJSON,
        },
    };

our $FAMILY_REGEXP = '\w{8}';
our $UNANNOTATED_CLUSTER_NAME       = 'Unannotated cluster';

our $RELATIONSHIP_OTHER             = 'other';
our $RELATIONSHIP_INHERITANCE       = 'inheritance'; # used by ancestor-sibling-child relationships
our $RELATIONSHIP_SIMILAR_TO        = 'similar to'; # used when two families are similar (functions and/or annotations and/or sequences)
our $RELATIONSHIP_REARRANGMENT_OF   = 'rearrangment of'; # used when a family of a level is made from sequences of families of same level
our $RELATIONSHIP_REPLACED_BY       = 'replaced by'; # used by family annotation transfer and obsolete families
our $RELATIONSHIP_DERIVATED_FROM    = 'derivated from'; # used by a subject family that was initially composed by the object family
our @RELATIONSHIPS                  = (
	$RELATIONSHIP_OTHER,
	$RELATIONSHIP_INHERITANCE,
	$RELATIONSHIP_SIMILAR_TO,
	$RELATIONSHIP_REARRANGMENT_OF,
	$RELATIONSHIP_REPLACED_BY,
	$RELATIONSHIP_DERIVATED_FROM,
);






# Package subs
###############

=pod

=head1 STATIC METHODS

=head2 CONSTRUCTOR

B<Description>: Creates a new instance of a GreenPhyl sequence object.

B<ArgsCount>: 1-2

=over 4

=item $fasta_content: (DBI::db) (O)

A FASTA content to load the sequences from.

=item $dbh: (DBI) (U)

Database handler of the database to load the object from.

=item $parameters: (hash ref) (R)

See Greenphyl::DBObject constructor documentation for details.

=back

B<Return>: (Greenphyl::AbstractSequence)

a new instance.

B<Caller>: General

B<Example>:

    my $sequence = new Greenphyl::AbstractSequence($dbh, {'selectors' => {'accession' => ['LIKE', 'Os01g01050%']}});
    my $fasta_sequence = new Greenphyl::AbstractSequence(">Os01g01050.1_ORYSA\nMINFAVQSRCAPTNSLCSCVALCERNFKLEQVEELASLIKDNLYSKHLVLSTEETLVGIL");

=cut

sub new
{
    my $proto = shift();
    my $class = ref($proto) || $proto;
    
    my $object_properties = $class->GetDefaultClassProperties();

    return $class->SUPER::new($object_properties, @_);
}




=pod

=head1 METHODS

=head2 getStringValue

B<Description>: returns family accession.

B<ArgsCount>: 1

=over 4

=item $self: (Greenphyl::AbstractFamily)

the family.

=back

B<Return>: (string)

the family name (accession).

B<Caller>: internal

=cut

sub getStringValue
{
    my ($self) = @_;
    return $self->{'accession'} || $self->{'id'} || '';
}


=pod

=head2 name

B<Description>: returns family name with first letter in upper case.

B<Alias>: getName

B<ArgsCount>: 1

=over 4

=item $self: (Greenphyl::AbstractFamily)

the family.

=back

B<Return>: (string)

the family name.

=cut

sub name
{
    my ($self) = shift();
    PrintDebug('TEST ' . $self->SUPER::name(@_) . ' --> ' . ucfirst($self->SUPER::name(@_)));
    return ucfirst($self->SUPER::name(@_));
}
sub getName; *getName = \&name;


=pod

=head2 setHighlight

B<Description>: choose if a family should be highlighted or not.

B<ArgsCount>: 2

=over 4

=item $self: (Greenphyl::AbstractFamily) (R)

Current family.

=item $highlight: (boolean) (R)

the new highlight status.

=back

B<Return>: (boolean)

Returns the highlight status.

B<Example>:

    $family->setHighlight(1);

=cut

sub setHighlight
{
    my ($self, $highlight) = @_;

    return $self->{'highlight'} = $highlight;
}


=pod

=head2 isHighlighted

B<Description>: tells if a family is highlighted or not.

B<ArgsCount>: 1

=over 4

=item $self: (Greenphyl::AbstractFamily) (R)

Current family.

=back

B<Return>: (boolean)

Returns true if family is highlighted.

B<Example>:

    if ($family->isHighlighted())
    {
        # family is highlighted
        ...
    }

=cut

sub isHighlighted
{
    my ($self) = @_;

    return $self->{'highlight'};
}


=pod

=head2 isAnnotated

B<Description>: tells if a family has been annotated.

B<ArgsCount>: 1

=over 4

=item $self: (Greenphyl::Family) (R)

Current family.

=back

B<Return>: (boolean)

Returns true if family has been annotated.

B<Example>:

    if ($family->isAnnotated())
    {
        # family has been annotated
        ...
    }

=cut

sub isAnnotated
{
    my ($self) = @_;

    if ($self->name
        && ($self->name ne $UNANNOTATED_CLUSTER_NAME)
        && ($self->name !~ m/Unan+otated[\s_\-]*cluster/i))
    {
        return 1;
    }

    return 0;
}


=pod

=head2 getPubMed

B<Description>: returns PubMed DBXRef objects.

B<Alias>: pubmed

B<ArgsCount>: 1

=over 4

=item $self: (Greenphyl::AbstractFamily) (R)

Current family.

=back

B<Return>: (array ref)

Returns an array of Greenphyl::DBXRef objects.

B<Example>:

    foreach my $pmid (@{$family->getPubMed()})
    {
        ...
    }

=cut

sub getPubMed
{
    my ($self) = @_;

    if (!exists($self->{'pubmed'}))
    {
        # get PubMed database identifier
        my $pubmed_db = Greenphyl::SourceDatabase->new(
            $self->{'_dbh'},
            {
                'selectors' => {'name' => 'PubMed'},
            },
        );
        if (!$pubmed_db)
        {
            PrintDebug("Warning: PubMed source database not found in db table!");
            cluck "Warning: PubMed source database not found in db table!\n";
            return [];
        }
        # prepare storage array
        my $pubmed_dbxref = [];
        # check if we already loaded all family dbxref
        if (exists($self->{'dbxref'}) && @{$self->{'dbxref'}})
        {
            # yes, filter what we got
            foreach my $dbxref (@{$self->{'dbxref'}})
            {
                if ($pubmed_db->id == $dbxref->db_id)
                {
                    push(@$pubmed_dbxref, $dbxref);
                }
            }
        }
        else
        {
            # no, fetch from database
            # save previous fetch results as we will use the fetch method
            my $previous_fetch_results = $self->{'_fetch'}->{'dbxref'};
            # fetch PubMed DBXRef
            $pubmed_dbxref = $self->fetch_dbxref({'selectors' => {'db_id' => $pubmed_db->id}});
            # restore previous fetch
            $self->{'_fetch'}->{'dbxref'} = $previous_fetch_results;
        }
        $self->{'pubmed'} = $pubmed_dbxref;
    }

    return $self->{'pubmed'};
}
# Aliases
sub pubmed; *pubmed = \&getPubMed;


=pod

=head2 go

B<Description>: getter/setter for GO. Wraps default handler to sort
GO by percent.

B<Aliases>: getGO, getGo, setGO, setGo, GO, Go

B<ArgsCount>: 1

=over 4

=item $self: (Greenphyl::AbstractFamily) (R)

Current family.

=back

B<Return>: (array ref)

Array of Greenphyl::GO objects.

B<Example>:

    my $gos = $family->getGO()

=cut

sub go
{
    my ($self) = shift();
    # have the go list already been sorted?
    if (!$self->{'go'})
    {
        # no, call parent and sort results
        $self->SUPER::go(@_);
        return $self->{'go'} = [
            sort
                {
                    my $a_percent = $a->family_sources({'selectors' => {'family_id' => $self->id,} })->[0]->percent;
                    my $b_percent = $b->family_sources({'selectors' => {'family_id' => $self->id,} })->[0]->percent;
                    return $b_percent cmp $a_percent;
                }
                @{$self->{'go'}}
        ];
    }
    else
    {
        return $self->SUPER::go(@_);
    }
}
# aliases
sub getGO; *getGO = \&go;
sub getGo; *getGo = \&go;
sub setGO; *setGO = \&go;
sub setGo; *setGo = \&go;
sub GO;    *GO    = \&go;
sub Go;    *Go    = \&go;


=pod

=head2 getGOGroups

B<Description>: Returns a structure of GO groups ready to be processed by
GO templates.

B<ArgsCount>: 1

=over 4

=item $self: (Greenphyl::AbstractFamily) (R)

Current family.

=back

B<Return>: (array ref)

.

B<Example>:

    my $go_groups = $family->getGOGroups()

=cut

sub getGOGroups
{
    my ($self) = @_;
    my $go_data = Greenphyl::Tools::Families::GenerateGOData($self);
    my $sorted_go = [sort {$a->code cmp $b->code} @$go_data];
    my $go_groups = GroupGOByType($sorted_go);
    return $go_groups;
}


=pod

=head2 getIPRDomainConsensus

B<Description>: Returns family IPR consensus data.

B<ArgsCount>: 1

=over 4

=item $self: (Greenphyl::AbstractFamily) (R)

Current family.

B<Return>: (Greenphyl::Sequence)

A fake sequence that represent family consensus sequence with its IPR domains.

=cut

sub getIPRDomainConsensus
{
    my ($self) = @_;
    
    if (!$self->{'domain_consensus'})
    {
        my $dbh = GetDatabaseHandler();

        # get relevant IPR (>30% for specific and >50% others)
        my $sql_query = '
            SELECT isq.ipr_id AS "ipr_id",
                ROUND(AVG(isq.start)) AS "start",
                ROUND(AVG(isq.end)) AS "end"
            FROM ipr_sequences isq
                JOIN families_ipr_cache fic ON (fic.ipr_id = isq.ipr_id)
                JOIN families_sequences fs ON (fs.sequence_id = isq.sequence_id AND fs.family_id = fic.family_id)
            WHERE fic.family_id = ' . $self->id . '
                AND (fic.percent > 50 OR (fic.percent > 30 AND fic.family_specific = 1))
            GROUP BY isq.ipr_id
        ;';

        my $sql_ipr_stats = $dbh->selectall_arrayref($sql_query, { 'Slice' => {} })
            or confess $dbh->errstr;
        PrintDebug("Got " . scalar(@$sql_ipr_stats) . " relevant IPR");

        # create a fake sequence with an ipr_details that will correspond
        # to consensus
        my (@ipr_details, @ipr);
        my $sequence = Greenphyl::Sequence->new(
            undef,
            {
                'load' => 'none',
                'members' => {
                    'annotation'  => '',
                    'filtered'    => 0,
                    'id'          => -1,
                    'length'      => $self->average_sequence_length,
                    'accession'   => $self->accession,
                    'locus'       => '',
                    'splice'      => 0,
                    'splice_priority' => 0,
                    'polypeptide' => 'X' x $self->average_sequence_length,
                    'species_id'  => -1,
                    'ipr'         => \@ipr,
                    'ipr_details' => \@ipr_details,
                }
            }
        );

        # compute each IPR domain position
        foreach my $consensus_ipr_data (@{$sql_ipr_stats})
        {
            # adjust sequence length if needed
            if ($sequence->length < $consensus_ipr_data->{'end'})
            {
                $sequence->setLength($consensus_ipr_data->{'end'});
            }
            
            # fill default fields
            $consensus_ipr_data->{'sequence_id'} = -1;
            $consensus_ipr_data->{'score'} = 1;
            $consensus_ipr_data->{'sequence'} = $sequence;
            
            # store IPR stats
            my $ipr_sequence = Greenphyl::SequenceIPR->new(
                $dbh,
                {
                    'load' => 'none',
                    'members' => $consensus_ipr_data,
                }
            );
            push(@ipr_details, $ipr_sequence);
            push(@ipr, Greenphyl::IPR->new($dbh, {'selectors' => {'id' => $consensus_ipr_data->{'ipr_id'},}}));
        }
        $self->{'domain_consensus'} = $sequence;
    }

    return $self->{'domain_consensus'};
}


=pod

=head2 getCommonSequences

B<Description>: Returns an array of sequences that current family and the given
one have in common.

B<ArgsCount>: 2

=over 4

=item $self: (Greenphyl::AbstractFamily) (R)

Current family.

=item $self: (Greenphyl::AbstractFamily) (R)

An other family.

=back

B<Return>: (array ref)

Returns an array of Greenphyl::Sequence objects.

=cut

sub getCommonSequences
{
    my ($self, $other_family) = @_;

    my $common_sequences = [];

    $self->{'_common_sequences'} ||= {};
    if ((!exists($self->{'_common_sequences'}->{$other_family->id}))
        || ($self->{'_dbh'} != $other_family->{'_dbh'}))
    {
        my $sequence_table = $self->{'_properties'}->{'tertiary'}->{'sequences'}->{'property_table'};
        my $sequence_key = $self->{'_properties'}->{'tertiary'}->{'sequences'}->{'property_key'};
        my $sequence_link_table = $self->{'_properties'}->{'tertiary'}->{'sequences'}->{'link_table'};
        my $sequence_link_property_key = $self->{'_properties'}->{'tertiary'}->{'sequences'}->{'link_property_key'};
        my $sequence_property_module = $self->{'_properties'}->{'tertiary'}->{'sequences'}->{'property_module'};
        my $sequence_link_object_key = $self->{'_properties'}->{'tertiary'}->{'sequences'}->{'link_object_key'};

        # get all related families using a custom SQL query
        my $sql_query = "
            SELECT DISTINCT " . join(', ', $sequence_property_module->GetDefaultMembers('s')) . "
            FROM $sequence_table s
                 JOIN $sequence_link_table fs1 ON (s.$sequence_key = fs1.$sequence_link_property_key)
                 JOIN $sequence_link_table fs2 ON (s.$sequence_key = fs2.$sequence_link_property_key)
            WHERE fs1.$sequence_link_object_key = " . $self->id . "
                  AND fs2.$sequence_link_object_key = " . $other_family->id . "
        ";
        my $sql_sequences = $self->{'_dbh'}->selectall_arrayref($sql_query, { 'Slice' => {} })
            or confess 'ERROR: Failed to fetch common sequences: ' . $self->{'_dbh'}->errstr;
        foreach my $sql_sequence (@$sql_sequences)
        {
            my $sequence = $sequence_property_module->new($self->{'_dbh'}, {'load' => 'none', 'members' => $sql_sequence});
            push(@$common_sequences, $sequence);
        }
        
        if ($self->{'_dbh'} == $other_family->{'_dbh'})
        {
            $self->{'_common_sequences'}->{$other_family->id} = $common_sequences;
        }
    }
    else
    {
        $common_sequences = $self->{'_common_sequences'}->{$other_family->id};
    }

    # return all common sequences
    return $common_sequences;
}


=pod

=head2 getCommonSequenceCount

B<Description>: Returns the number of sequences that current family and the given
one have in common.

B<ArgsCount>: 2

=over 4

=item $self: (Greenphyl::AbstractFamily) (R)

Current family.

=item $self: (Greenphyl::AbstractFamily) (R)

An other family.

=back

B<Return>: (integer)

Returns the number of common sequences between the 2 families.

=cut

sub getCommonSequenceCount
{
    my ($self, $other_family) = @_;

    my $sequence_count;

    $self->{'_common_sequence_count'} ||= {};
    if ((!exists($self->{'_common_sequence_count'}->{$other_family->id}))
        || ($self->{'_dbh'} != $other_family->{'_dbh'}))
    {
        my @common_sequences;
        my $sequence_table = $self->{'_properties'}->{'tertiary'}->{'sequences'}->{'property_table'};
        my $sequence_key = $self->{'_properties'}->{'tertiary'}->{'sequences'}->{'property_key'};
        my $sequence_link_table = $self->{'_properties'}->{'tertiary'}->{'sequences'}->{'link_table'};
        my $sequence_link_property_key = $self->{'_properties'}->{'tertiary'}->{'sequences'}->{'link_property_key'};
        my $sequence_property_module = $self->{'_properties'}->{'tertiary'}->{'sequences'}->{'property_module'};
        my $sequence_link_object_key = $self->{'_properties'}->{'tertiary'}->{'sequences'}->{'link_object_key'};

        # get all related families using a custom SQL query
        my $sql_query = "
            SELECT COUNT(DISTINCT s.$sequence_key)
            FROM $sequence_table s
                 JOIN $sequence_link_table fs1 ON (s.$sequence_key = fs1.$sequence_link_property_key)
                 JOIN $sequence_link_table fs2 ON (s.$sequence_key = fs2.$sequence_link_property_key)
            WHERE fs1.$sequence_link_object_key = " . $self->id . "
                  AND fs2.$sequence_link_object_key = " . $other_family->id . "
        ";

        ($sequence_count) = $self->{'_dbh'}->selectrow_array($sql_query)
            or confess 'ERROR: Failed to count common sequences: ' . $self->{'_dbh'}->errstr;

        if ($self->{'_dbh'} == $other_family->{'_dbh'})
        {
            $self->{'_common_sequence_count'}->{$other_family->id} = $sequence_count;
        }
    }
    else
    {
        $sequence_count = $self->{'_common_sequence_count'}->{$other_family->id};
    }

    # return common sequence count
    return $sequence_count;
}


=pod

=head2 getRelatedFamilies

B<Description>: Returns an array of families that have common sequences with
current family.

B<Alias>: relationships

B<ArgsCount>: 1-2

=over 4

=item $self: (Greenphyl::AbstractFamily) (R)

Current family.

=item $parameters: (hash ref) (O)

Hash of options:
-'level': classification level of related families. Default: any level;
-'level_delta': difference between current family level and other related
  families. Default: any delta;
-'delta_test': a test to apply to level_delta field (ex. '> 0').
  Default: if no parameter has been defined, '!= 0', otherwise no test;
-'type': type of relationship (see family_relationships_cache table, 'type'
  field) Default: $RELATIONSHIP_INHERITANCE;

Note: level, level_delta and delta_test are mutualy exclusive and are treated
in that order of priority.
  
=back

B<Return>: (array ref)

Returns an array of Greenphyl::AbstractFamily objects.

=cut

sub getRelatedFamilies
{

    my ($self, $parameters) = @_;

    $parameters ||= {};

    my $level             = $parameters->{'level'} || 0;
    my $level_delta       = $parameters->{'level_delta'};
    my $relationship_type = $parameters->{'type'};
    my $delta_test        = $parameters->{'delta_test'};

    if ($level !~ m/^\d+$/)
    {
        $level = 0;
    }

    # by default, we don't want sibling families in relationships
    if (!%$parameters)
    {
        $delta_test = '!= 0';
    }

    # default: get inheritance relationships
	my $relationship_regex = join('|', @RELATIONSHIPS);
    if (!$relationship_type
        || ($relationship_type !~ m/^(?:$relationship_regex)$/o))
    {
        $relationship_type = $RELATIONSHIP_INHERITANCE;
    }

    my @related_families;
    my $level_statement = '';
    if ($level)
    {
        $level_statement = 'AND fr.level_delta = ' . int($level - $self->level);
    }
    elsif ($level_delta)
    {
        $level_statement = 'AND fr.level_delta = ' . int($level_delta);
    }
    elsif ($delta_test)
    {
        $level_statement = 'AND fr.level_delta ' . $delta_test;
    }

    my $relationship_table = $self->{'_properties'}->{'relationship'}->{'table'};
    my $relationship_subject = $self->{'_properties'}->{'relationship'}->{'subject'};
    my $relationship_object = $self->{'_properties'}->{'relationship'}->{'object'};

    # get all related families using a custom SQL query
    my $sql_query = "
        SELECT DISTINCT " . join(', ', $self->GetDefaultMembers('f')) . "
        FROM $relationship_table fr
             JOIN " . $self->{'_properties'}->{'table'} . " f ON (f.id = fr.$relationship_object)
        WHERE fr.$relationship_subject = " . $self->id . "
              AND fr.type = '$relationship_type'
              $level_statement
        ORDER BY f.level ASC, f.id ASC
    ";
    PrintDebug("SQL QUERY: $sql_query");
    my $sql_families = $self->{'_dbh'}->selectall_arrayref($sql_query, { 'Slice' => {} })
        or confess 'ERROR: Failed to fetch related families: ' . $self->{'_dbh'}->errstr;
    foreach my $sql_family (@$sql_families)
    {
        my $related_family = ref($self)->new($self->{'_dbh'}, {'load' => 'none', 'members' => $sql_family});
        push(@related_families, $related_family);
    }

    PrintDebug('Found ' . scalar(@related_families) . ' related families for family ' . $self);
    # return related families
    return \@related_families;
}

sub relationships; *relationships = \&getRelatedFamilies;


=pod

=head2 getParentFamilies

B<Description>: Returns an array of parent families of current family.

B<Alias>: getParents, parents

B<ArgsCount>: 1

=over 4

=item $self: (Greenphyl::AbstractFamily) (R)

Current family.

=back

B<Return>: (array ref)

Returns an array of Greenphyl::AbstractFamily objects.

=cut

sub getParentFamilies
{
    my ($self) = @_;
    if (!$self->{'parents'})
    {
        $self->{'parents'} = $self->getRelatedFamilies({'level_delta' => -1,});
    }
    return $self->{'parents'};
}
sub getParents; *getParents = \&getParentFamilies;
sub parents; *parents = \&getParentFamilies;


=pod

=head2 getAncestorFamilies

B<Description>: Returns an array of ancestor families of current family.

B<Alias>: getAncestors, ancestors

B<ArgsCount>: 1

=over 4

=item $self: (Greenphyl::AbstractFamily) (R)

Current family.

=back

B<Return>: (array ref)

Returns an array of Greenphyl::AbstractFamily objects.

=cut

sub getAncestorFamilies
{
    my ($self) = @_;
    if (!$self->{'ancestors'})
    {
        $self->{'ancestors'} = $self->getRelatedFamilies({'delta_test' => '< 0',});
    }
    return $self->{'ancestors'};
}
sub getAncestors; *getAncestors = \&getAncestorFamilies;
sub ancestors; *ancestors = \&getAncestorFamilies;


=pod

=head2 getChildFamilies

B<Description>: Returns an array of child families of current family.

B<Alias>: getChildren, children

B<ArgsCount>: 1

=over 4

=item $self: (Greenphyl::AbstractFamily) (R)

Current family.

=back

B<Return>: (array ref)

Returns an array of Greenphyl::AbstractFamily objects.

=cut

sub getChildFamilies
{
    my ($self) = @_;
    if (!$self->{'children'})
    {
        $self->{'children'} = $self->getRelatedFamilies({'level_delta' => 1,});
    }
    return $self->{'children'};
}
sub getChildren; *getChildren = \&getChildFamilies;
sub children; *children = \&getChildFamilies;


=pod

=head2 getDescendantFamilies

B<Description>: Returns an array of descendant families of current family.

B<Alias>: getDescendants, descendants

B<ArgsCount>: 1

=over 4

=item $self: (Greenphyl::AbstractFamily) (R)

Current family.

=back

B<Return>: (array ref)

Returns an array of Greenphyl::AbstractFamily objects.

=cut

sub getDescendantFamilies
{
    my ($self) = @_;
    if (!$self->{'descendants'})
    {
        $self->{'descendants'} = $self->getRelatedFamilies({'delta_test' => '> 0',});
    }
    return $self->{'descendants'};
}
sub getDescendants; *getDescendants = \&getDescendantFamilies;
sub descendants; *descendants = \&getDescendantFamilies;


=pod

=head2 getSiblingFamilies

B<Description>: Returns an array of sibling families of current family.

B<Alias>: getSiblings, siblings

B<ArgsCount>: 1

=over 4

=item $self: (Greenphyl::AbstractFamily) (R)

Current family.

=back

B<Return>: (array ref)

Returns an array of Greenphyl::AbstractFamily objects.

=cut

sub getSiblingFamilies
{
    my ($self) = @_;
    if (!$self->{'siblings'})
    {
        $self->{'siblings'} = $self->getRelatedFamilies({'level_delta' => 0,});
    }
    return $self->{'siblings'};
}
sub getSiblings; *getSiblings = \&getSiblingFamilies;
sub siblings; *siblings = \&getSiblingFamilies;


=pod

=head2 getFamilySubTree

B<Description>: Returns a set of encapsulated hashes that reflect the children
family hierachical structure of current family.
Note: current family is the tree root.

B<ArgsCount>: 1

=over 4

=item $self: (Greenphyl::AbstractFamily) (R)

Current family.

=back

B<Return>: (hash ref)

Returns an hash containing the 2 keys:
-'family': current family
-'children': an array ref containing hash refs similar to the current one.

=cut

sub getFamilySubTree
{
    my ($self) = @_;

    my $tree_structure = {
        'family'   => $self,
        'children' => [map { $_->getFamilySubTree() }    @{$self->getChildFamilies()}],
    };

    return $tree_structure;
}


=pod

=head2 getFamilyTree

B<Description>: Returns a set of encapsulated hashes that reflect the family
hierachical structure current family is in.
Note: current family may not be the tree root. Actualy, the tree root is a
"level 1" family which would be current family if current family is level 1.

B<ArgsCount>: 1

=over 4

=item $self: (Greenphyl::AbstractFamily) (R)

Current family.

=back

B<Return>: (hash ref)

Returns an hash containing the 2 keys:
-'family': a Greenphyl::AbstractFamily object being the tree root
-'children': an array ref containing hash refs similar to the root one.

=cut

sub getFamilyTree
{
    my ($self) = @_;
    my $root_family = @{$self->getRelatedFamilies(1)}[0] || $self;
    my $tree_structure = {
        'family'   => $root_family,
        'children' => [map { $_->getFamilySubTree() }    @{$root_family->getChildFamilies()}],
    };

    return $tree_structure;
}


=pod

=head2 getLevelDeltaWith

B<Description>: returns level delta between current family and the given one 
other.level - current.level).

B<ArgsCount>: 2

=over 4

=item $self: (Greenphyl::AbstractFamily) (R)

Current family.

=item $other: (Greenphyl::AbstractFamily) (R)

Other family.

=back

B<Return>: (integer)

level delta or undef if no relationship exists.

=cut

sub getLevelDeltaWith
{
    my ($self, $other, $type) = @_;

    $type ||= $RELATIONSHIP_INHERITANCE;
    my $relationship_table = $self->{'_properties'}->{'relationship'}->{'table'};
    my $relationship_subject = $self->{'_properties'}->{'relationship'}->{'subject'};
    my $relationship_object = $self->{'_properties'}->{'relationship'}->{'object'};

    my $sql_query = "
        SELECT level_delta
        FROM $relationship_table fr
        WHERE fr.$relationship_subject = " . $self->id . "
              AND fr.$relationship_object = " . $other->id . "
              AND fr.type = '$type'
    ";
    PrintDebug("SQL QUERY: $sql_query");
    my ($level_delta) = $self->{'_dbh'}->selectrow_array($sql_query);

    return $level_delta;
}


=pod

=head2 getSubPath

B<Description>: Returns current family sub-path part (for temporary directories
of family file storage).

B<ArgsCount>: 1

=over 4

=item $self: (Greenphyl::AbstractFamily) (R)

Current family.

=back

B<Return>: (string)

Returns a part of a path that can be used to locate current family data. The
sub-path does not start nor ends with a '/' (but may include slashes) and looks
like "level1/GP000".

=cut

sub getSubPath
{
    my ($self) = @_;
    return 'level' . ($self->level ? $self->level : 0) . '/' . substr($self->accession, 0, 5);
}


=pod

=head2 DumpFamilySequences

B<Description>: returns the family sequence(s) dumped into string of the given
format.
Supports non-static calls.

B<ArgsCount>: 2-3

=over 4

=item $objects_ref: (Greenphyl::AbstractFamily ref or array of Greenphyl::AbstractFamily ref) (R)

Family or list of families to dump.

=item $format: (string) (R)

the output format.

=item $parameters: (any ref or string) (O)

a reference or a string containing additional parameters like 'without_species_code',
or 'without_splice'.
'without_splice': (boolean) remove sequences splice forms.
'exclude_species': (hash ref) exclude species which codes are given as hash
  keys.
'include_species': (hash ref) only include species which codes are given as hash
  keys.
See Greenphyl::AbstractSequence::convertToBioSeq parameters for other available options.

=back

B<Return>: (string)

The dumped sequence(s) string.

=cut

sub DumpFamilySequences
{
    my ($objects_ref, $format, $parameters) = @_;

    my $dump_output = '';
    
    $parameters ||= {};
    $parameters->{'format'} = $format;

    if (ref($objects_ref) ne 'ARRAY')
    {
        $objects_ref = [$objects_ref];
    }

	PrintDebug("Got " . scalar(@$objects_ref) . " family/families to dump.");

    foreach my $object (@$objects_ref)
    {
        my $sequences = $object->sequences;

		PrintDebug("Got an initial set of " . scalar(@$sequences) . " sequence(s).");

		# check if splice forms should be removed
        if ($parameters->{'without_splice'})
        {
            $sequences = FilterSpliceForm($sequences);
			PrintDebug("Got " . scalar(@$sequences) . " sequence(s) after splice filtration.");
        }

        if ($parameters->{'exclude_species'} && keys(%{$parameters->{'exclude_species'}}))
        {
            $sequences = Greenphyl::Tools::Sequences::ExcludeSpecies($sequences, [values(%{$parameters->{'exclude_species'}})]);
			PrintDebug("Got " . scalar(@$sequences) . " sequence(s) after species exclusion.");
        }

        if ($parameters->{'include_species'} && keys(%{$parameters->{'include_species'}}))
        {
            $sequences = Greenphyl::Tools::Sequences::IncludeSpecies($sequences, [values(%{$parameters->{'include_species'}})]);
			PrintDebug("Got " . scalar(@$sequences) . " sequence(s) after species inclusion.");
        }
		PrintDebug("Got a final set of " . scalar(@$sequences) . " sequence(s) to dump.");

        $dump_output .= Greenphyl::AbstractSequence::DumpBioSeq($sequences, $format, $parameters);
        $dump_output .= "\n";
    }

    return $dump_output;
}


=pod

=head2 Dump*

B<Description>: returns the given sequence(s) dumped into a * string.
Supports non-static calls.

B<ArgsCount>: 1-2

=over 4

=item $objects_ref: (Greenphyl::AbstractSequence ref or array of Greenphyl::AbstractSequence ref) (R)

Sequence or list of sequences to dump.

=item $parameters: (any ref or string) (O)

a reference or a string containing additional parameters.
See Greenphyl::AbstractSequence::convertToBioSeq parameters for available options.

=back

B<Return>: (string)

The dumped sequence(s) string.

=cut

sub DumpFasta
{
    my ($objects_ref, $parameters) = @_;

    return DumpFamilySequences($objects_ref, 'Fasta', $parameters);
}

sub DumpEMBL
{
    my ($objects_ref, $parameters) = @_;

    return DumpFamilySequences($objects_ref, 'EMBL', $parameters);
}

sub DumpGenBank
{
    my ($objects_ref, $parameters) = @_;

    return DumpFamilySequences($objects_ref, 'GenBank', $parameters);
}

sub DumpSwissprot
{
    my ($objects_ref, $parameters) = @_;

    return DumpFamilySequences($objects_ref, 'swiss', $parameters);
}

sub DumpSCF
{
    my ($objects_ref, $parameters) = @_;

    return DumpFamilySequences($objects_ref, 'SCF', $parameters);
}

sub DumpPIR
{
    my ($objects_ref, $parameters) = @_;

    return DumpFamilySequences($objects_ref, 'PIR', $parameters);
}

sub DumpGCG
{
    my ($objects_ref, $parameters) = @_;

    return DumpFamilySequences($objects_ref, 'GCG', $parameters);
}

sub DumpRaw
{
    my ($objects_ref, $parameters) = @_;

    return DumpFamilySequences($objects_ref, 'raw', $parameters);
}

sub DumpACeDB
{
    my ($objects_ref, $parameters) = @_;

    return DumpFamilySequences($objects_ref, 'ace', $parameters);
}

sub DumpGameXML
{
    my ($objects_ref, $parameters) = @_;

    return DumpFamilySequences($objects_ref, 'game', $parameters);
}

sub DumpPhred
{
    my ($objects_ref, $parameters) = @_;

    return DumpFamilySequences($objects_ref, 'phd', $parameters);
}

sub DumpQualityValues
{
    my ($objects_ref, $parameters) = @_;

    return DumpFamilySequences($objects_ref, 'qual', $parameters);
}



=pod

=head2 DumpExcel, DumpCSV, DumpXML

B<Description>: Returns a string in the given format containing a table with
the requested columns of given family.

B<ArgsCount>: 2

=over 4

=item $object_ref: (array of Greenphyl::AbstractFamily) (R)

The list of families to process.

=item $parameters: (hash ref) (R)

a hash containing the key 'columns' that contains the columns to dump.
Each key is a column name (label) and each corresponding value is a string
containing the chain (dot-separated) of members to call to fetch the value from
each family.

=back

B<Return>: (string)

a string in the given format containing a table with the requested columns of
given families.

B<Example>:

    my $excel_fh;
    if (open($excel_fh, '>families.xls'))
    {
        binmode($excel_fh);
        print {$excel_fh} Greenphyl::AbstractFamily::DumpExcel([$family1, $family2], {'columns' => {'01.ID'=>'id','02.Name'=>'name','03.Sequences count'=>'sequence_count','04.Status'=>'validated'}});
        close($excel_fh);
    }
    ...
    print {$csv_fh} Greenphyl::AbstractFamily::DumpCSV([$family1, $family2], {'columns' => {'01.ID'=>'id','02.Name'=>'name','03.Sequences count'=>'sequence_count','04.Status'=>'validated'}});
    ...
    print {$xml_fh} Greenphyl::AbstractFamily::DumpXML([$family1, $family2], {'columns' => {'01.ID'=>'id','02.Name'=>'name','03.Sequences count'=>'sequence_count','04.Status'=>'validated'}});

=cut

sub DumpExcel
{
    my ($object_ref, $parameters) = @_;

    return Greenphyl::AbstractFamily->DumpTable('excel', $object_ref, $parameters);
}


sub DumpCSV
{
    my ($object_ref, $parameters) = @_;

    return Greenphyl::AbstractFamily->DumpTable('csv', $object_ref, $parameters);
}


sub DumpXML
{
    my ($object_ref, $parameters) = @_;

    #convert column names for XML
    my @column_names = keys(%{$parameters->{'columns'}});
    foreach my $column_name (@column_names)
    {
        my $new_column_name = $column_name;
        # remove indexes
        $new_column_name =~ s/^[0-9a-z]{1,2}\.\s*//;
        # remove unwanted characters
        $new_column_name =~ s/\W+/_/g;
        # make sure name does not begin with a number
        $new_column_name =~ s/^([0-9])/_$1/;
        $new_column_name = lc($new_column_name);
        # rename column
        $parameters->{'columns'}->{$new_column_name} = delete($parameters->{'columns'}->{$column_name});
    }

    return Greenphyl::AbstractFamily->DumpTable('xml', $object_ref, $parameters);
}


sub DumpJSON
{
    return Greenphyl::DumpableTabularData::DumpJSON(@_);
}


=pod

=head1 DIAGNOSTICS

=head1 AUTHORS

Mathieu ROUARD (Bioversity), m.rouard@cgiar.org

Valentin GUIGNON (Bioversity), v.guignon@cgiar.org

Matthieu CONTE (Syngenta)

=head1 VERSION

Version 1.0.0

Date 11/10/2013

=head1 SEE ALSO

GreenPhyl documentation, DBObject documentation.

=cut

return 1; # package return
