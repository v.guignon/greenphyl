=pod

=head1 NAME

Greenphyl::Group - GreenPhyl Group Object

=head1 SYNOPSIS

    use Greenphyl::Group;
    my $bioversity_group = Greenphyl::Group->new($dbh, {'selectors' => {'name' => 'Bioversity'}});
    print $bioversity_group->description();

=head1 REQUIRES

Perl5

=head1 EXPORTS

Nothing

=head1 DESCRIPTION

This module implements a GreenPhyl Group database object.

=cut

package Greenphyl::Group;

use strict;
use warnings;

use base qw(Greenphyl::CachedDBObject);

use vars qw($AUTOLOAD);

use Carp qw(cluck confess croak);




# Package constants
####################

=pod

=head1 CONSTANTS

B<$OBJECT_PROPERTIES>: (hash ref)

object properties description. See DBObject constructor documentation for
details.

=cut

# use "generate_table_properties.pl" script to auto-generate the template
# structure.

our $OBJECT_PROPERTIES = {
    'table' => 'groups',
    'key' => 'id',
    'alternate_keys' => [],
    'load'     => [
        'description',
        'id',
        'name',
    ],
    'alias' => {
    },
    'base' => {
        'description' => {
            'property_column' => 'description',
        },
        'id' => {
            'property_column' => 'id',
        },
        'name' => {
            'property_column' => 'name',
        },
    },
    'secondary' => {
    },
    'tertiary' => {
        'users' => {
            'link_table'        => 'groups_users',
            'link_object_key'   => 'group_id',
            'link_property_key' => 'user_id',
            'property_table'    => 'users',
            'property_key'      => 'id',
            'multiple_values'   => 1,
            'property_module'   => 'Greenphyl::User',
        },
    },

};




# Package subs
###############

=pod

=head1 STATIC METHODS

=head2 CONSTRUCTOR

B<Description>: Creates a new instance of a GreenPhyl Group object.

B<ArgsCount>: 2

=over 4

=item $dbh: (DBI::db) (U)

Database handler of the database to load the object from.

=item $parameters: (hash ref) (R)

See Greenphyl::DBObject constructor documentation for details.

=back

B<Return>: (Greenphyl::Group)

a new instance.

B<Caller>: General

B<Example>:

    my $bioversity_group = Greenphyl::Group->new($dbh, {'selectors' => {'name' => 'Bioversity'}});

=cut

sub new
{
    my $proto = shift();
    my $class = ref($proto) || $proto;
    
    my $object_properties = $class->GetDefaultClassProperties();

    return $class->SUPER::new($object_properties, @_);
}




=pod

=head1 AUTHORS

Mathieu ROUARD (Bioversity), m.rouard@cgiar.org

Valentin GUIGNON (Bioversity), v.guignon@cgiar.org

Matthieu CONTE (Syngenta)

=head1 VERSION

Version 1.0.0

Date 23/01/2014

=head1 SEE ALSO

GreenPhyl documentation, DBObject documentation.

=cut

return 1; # package return
