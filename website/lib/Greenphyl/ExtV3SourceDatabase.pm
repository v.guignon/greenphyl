=pod

=head1 NAME

Greenphyl::ExtV3SourceDatabase - GreenPhyl v3 DB Object

=head1 SYNOPSIS

    use Greenphyl::ExtV3SourceDatabase;
    my $source_db = Greenphyl::ExtV3SourceDatabase->new($dbh, {'selectors' => {'name' => 'PubMed'}});
    print $source_db->id();

=head1 REQUIRES

Perl5

=head1 EXPORTS

Nothing

=head1 DESCRIPTION

This module implements a GreenPhyl v3 ExtV3SourceDatabase database object.

=cut

package Greenphyl::ExtV3SourceDatabase;

use strict;
use warnings;

use base qw(Greenphyl::CachedDBObject);

use vars qw($AUTOLOAD);

use Carp qw(cluck confess croak);




# Package constants
####################

=pod

=head1 CONSTANTS

B<$OBJECT_PROPERTIES>: (hash ref)

object properties description. See DBObject constructor documentation for
details.

=cut

our $OBJECT_PROPERTIES = {
    'table' => 'db',
    'key' => 'db_id',
    'alternate_keys' => ['name'],
    'load'     => [
        'db_id',
        'name',
        'description',
        'urlprefix',
        'url',
    ],
    'alias' => {
    },
    'base' => {
        'db_id' => {
            'property_column' => 'db_id',
        },
        'name' => {
            'property_column' => 'name',
        },
        'description' => {
            'property_column' => 'description',
        },
        'url' => {
            'property_column' => 'url',
        },
        'urlprefix' => {
            'property_column' => 'urlprefix',
        },
    },
    'secondary' => {
        'dbxref' => {
            'property_table'  => 'dbxref',
            'property_key'    => 'db_id',
            'multiple_values' => 1,
            'property_module' => 'Greenphyl::ExtV3DBXRef',
        },
    },
    'tertiary' => {
    },
};




# Package subs
###############

=pod

=head1 STATIC METHODS

=head2 CONSTRUCTOR

B<Description>: Creates a new instance of a GreenPhyl Source Database object.

B<ArgsCount>: 2

=over 4

=item $dbh: (DBI::db) (U)

Database handler of the database to load the object from.

=item $parameters: (hash ref) (R)

See Greenphyl::DBObject constructor documentation for details.

=back

B<Return>: (Greenphyl::ExtV3SourceDatabase)

a new instance.

B<Caller>: General

B<Example>:

    my $source_db = new Greenphyl::ExtV3SourceDatabase($dbh, {'selectors' => {'name' => 'PubMed'}});

=cut

sub new
{
    my $proto = shift();
    my $class = ref($proto) || $proto;
    
    my $object_properties = $class->GetDefaultClassProperties();

    return $class->SUPER::new($object_properties, @_);
}




=pod

=head1 AUTHORS

Mathieu ROUARD (Bioversity), m.rouard@cgiar.org

Valentin GUIGNON (Bioversity), v.guignon@cgiar.org

Matthieu CONTE (Syngenta)

=head1 VERSION

Version 1.0.0

Date 02/12/2013

=head1 SEE ALSO

GreenPhyl documentation, DBObject documentation.

=cut

return 1; # package return
