=pod

=head1 NAME

Greenphyl::Taxonomy - GreenPhyl Taxonomy Object

=head1 SYNOPSIS

    use Greenphyl::Taxonomy;
    my $taxonomy = Greenphyl::Taxonomy->new($dbh, {'selectors' => {'tax_id' => 2759}});
    print $taxonomy->name();

=head1 REQUIRES

Perl5

=head1 EXPORTS

Nothing

=head1 DESCRIPTION

This module implements a GreenPhyl Taxonomy database object.

=cut

package Greenphyl::Taxonomy;

use strict;
use warnings;

use base qw(Greenphyl::CachedDBObject);

use vars qw($AUTOLOAD);

use Carp qw(cluck confess croak);




# Package constants
####################

=pod

=head1 CONSTANTS

B<$OBJECT_PROPERTIES>: (hash ref)

object properties description. See DBObject constructor documentation for
details.

=cut

our $OBJECT_PROPERTIES = {
    'table' => 'taxonomy',
    'key' => 'id',
    'alternate_keys' => ['scientific_name'],
    'load'     => [
        'division',
        'level',
        'parent_taxonomy_id',
        'rank',
        'scientific_name',
        'id',
    ],
    'alias' => {
        'tax_id'   => 'id',
        'name' => 'scientific_name',
    },
    'base' => {
        'division' => {
            'property_column' => 'division',
        },
        'level' => {
            'property_column' => 'level',
        },
        'parent_taxonomy_id' => {
            'property_column' => 'parent_taxonomy_id',
        },
        'rank' => {
            'property_column' => 'rank',
        },
        'scientific_name' => {
            'property_column' => 'scientific_name',
        },
        'id' => {
            'property_column' => 'id',
        },
    },
    'secondary' => {
        'parent' => {
            'object_key'      => 'parent_taxonomy_id',
            'property_table'  => 'phylonode',
            'property_key'    => 'tax_id',
            'multiple_values' => 0,
            'property_module' => 'Greenphyl::Taxonomy',
        },
        'children' => {
            'object_key'      => 'tax_id',
            'property_table'  => 'phylonode',
            'property_key'    => 'parent_taxonomy_id',
            'multiple_values' => 1,
            'property_module' => 'Greenphyl::Taxonomy',
        },
    },
    'tertiary' => {
        'lineage' => {
            'object_key'        => 'tax_id',
            'link_table'        => 'lineage',
            'link_object_key'   => 'tax_id',
            'link_property_key' => 'lineage_tax_id',
            'property_table'    => 'phylonode',
            'property_key'      => 'tax_id',
            'multiple_values'   => 1,
            'property_module'   => 'Greenphyl::Taxonomy',
        },
    },
};




# Package subs
###############

=pod

=head1 STATIC METHODS

=head2 CONSTRUCTOR

B<Description>: Creates a new instance of a GreenPhyl Taxonomy object.

B<ArgsCount>: 2

=over 4

=item $dbh: (DBI::db) (U)

Database handler of the database to load the object from.

=item $parameters: (hash ref) (R)

See Greenphyl::DBObject constructor documentation for details.

=back

B<Return>: (Greenphyl::Taxonomy)

a new instance.

B<Caller>: General

B<Example>:

    my $taxonomy = new Greenphyl::Taxonomy($dbh, {'selectors' => {'tax_id' => 2759}});

=cut

sub new
{
    my $proto = shift();
    my $class = ref($proto) || $proto;
    
    my $object_properties = $class->GetDefaultClassProperties();

    return $class->SUPER::new($object_properties, @_);
}




=pod

=head1 METHODS

=pod

=head2 getStringValue

B<Description>: returns Species code.

B<ArgsCount>: 1

=over 4

=item $self: (Greenphyl::Species)

the species object.

=back

B<Return>: (string)

the species code.

=cut

sub getStringValue
{
    my ($self) = @_;
    return $self->{'scientific_name'};
}




=pod

=head1 AUTHORS

Mathieu ROUARD (Bioversity), m.rouard@cgiar.org

Valentin GUIGNON (Bioversity), v.guignon@cgiar.org

Matthieu CONTE (Syngenta)

=head1 VERSION

Version 1.0.0

Date 15/05/2012

=head1 SEE ALSO

GreenPhyl documentation, DBObject documentation.

=cut

return 1; # package return
