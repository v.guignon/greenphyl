package Greenphyl::SequenceFilter;
use parent qw( Exporter );

@EXPORT_OK = qw( filter_text_id );    # symbols to export on request

sub filter_text_id
{
    my ($text_id) = @_;

    return "$1_$2" if $text_id =~ m/(Chlre).*\|(\d+)\|/;
    return "$1"    if $text_id =~ m/(Chlre_\d+)/;

    die "'$text_id' did not match known sequence text id pattern";
}

1;
