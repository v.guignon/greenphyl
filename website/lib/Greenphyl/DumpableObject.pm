=pod

=head1 NAME

Greenphyl::DumpableObject - GreenPhyl dumpable object interface

=head1 SYNOPSIS

    package Greenphyl::Family;
    use parent qw(Greenphyl::CachedDBObject Greenphyl::DumpableObject);
    ...
    our $SUPPORTED_DUMP_FORMAT =
        {
            'static'  =>
            {
                'fasta' => \&DumpFASTA,
            },
            'dynamic' =>
            {
                'fasta' => \&dumpFASTA,
            },
        };
    ...
    sub DumpFASTA
    {
        ...
    }
    ...
    sub dumpFASTA
    {
        ...
    }
    ...
    return 1;
    ...
    $family->dump('fasta');

=head1 REQUIRES

Perl5

=head1 EXPORTS

Nothing

=head1 DESCRIPTION

This module is an interface for objects that can be dumped to various formats.
Children class should have a global variable called $SUPPORTED_DUMP_FORMAT which
is a hash with to keys 'static' and 'dynamic'. Each value of these keys are hash
which keys are supported format name and values are function references that
handle the dump format.

To know if an object $object can be dumped, you can use the following code:
if ($object->can('dump'))
{...}

To know if a class Obj can be dumped, you can use the following code:
if (Greenphyl::Obj->can('Dump'))
{...}

=cut

package Greenphyl::DumpableObject;

use strict;
use warnings;

use Carp qw(cluck confess croak);

use Greenphyl;




# Package constants
####################

=pod

=head1 CONSTANTS

B<$DEBUG>: (boolean)

enable/disable debug mode.

=cut

our $DEBUG = 0;
our $ARRAY_JOIN_STRING = ', ';



# Package subs
###############

=pod

=head1 STATIC METHODS

=head2 GetSupportedFormats

B<Description>: returns the list of supported dump format.

B<ArgsCount>: 1

=over 4

=item $class: (string) (R)

The class name.

=back

B<Return>: (list of string)

the list of supported format types.

B<Example>:

   my @supported_formats = Greenphyl::Sequence->GetSupportedFormats();

=cut

sub GetSupportedFormats
{
    my ($proto) = @_;
    my $class = ref($proto) || $proto;
    if (!$class)
    {confess "ERROR: No class! Maybe you didn't call the class method using the '->' operator.\n";}
    my $dumpable_settings = $class->_GetDumpable();
    return keys(%{$dumpable_settings->{'static'}});
}


=pod

=head2 _GetDumpable

B<Description>: returns the dump settings hash.

B<ArgsCount>: 1

B<Return>: (list of string)

the list of supported format types.

=cut

sub _GetDumpable
{
    my ($proto) = @_;
    my $class = ref($proto) || $proto;
    my $supported_formats;
    eval "\$supported_formats = \$" . $class . "::SUPPORTED_DUMP_FORMAT;";
    if ($@)
    {
        confess "ERROR: Class '$class' can't be dumpped because we can't access the \$SUPPORTED_DUMP_FORMAT hash.\nDetails: $@\n";
    }
    return $supported_formats;
}


=pod

=head2 SupportsFormat

B<Description>: returns true if the given output format is supported.

B<ArgsCount>: 2

=over 4

=item $class: (string) (R)

The class name.

=item $format_name: (string) (R)

Name of the output format to check.

=back

B<Return>: (boolean)

True if the format is supported, false otherwise.

B<Example>:

    if (Greenphyl::Sequence->SupportsFormat('fasta')
    {
        # FASTA format is supported by sequences
        ...
    }

=cut

sub SupportsFormat
{
    my ($class, $format_name) = @_;
    if (!defined($format_name))
    {confess "ERROR: no format type specified! Maybe you didn't call the class method using the '->' operator.\n";}
    my $dumpable_settings = $class->_GetDumpable();
    return exists($dumpable_settings->{'static'}->{$format_name});
}


=pod

=head2 Dump

B<Description>: returns the object(s) dumped into a string.

B<ArgsCount>: 3-4

=over 4

=item $class: (string) (R)

The class name.

=item $format_name: (string) (R)

Name of the output format to use.

=item $objects: (a ref) (R)

If the reference is an array, it will contain the list of objects to dump.
Otherwise, the reference must be of the children class type.

=item $parameters: (any ref or string) (O)

a reference or a string containing additional parameters.

=back

B<Return>: (string)

The dumped object(s) string.

=cut

sub Dump
{
    my ($class, $format_name, $object_ref, $parameters) = @_;
    my $dumpable_settings = $class->_GetDumpable();
    if ($DEBUG)
    {
        if ('ARRAY' eq ref($object_ref))
        {
            PrintDebug("Dumping " . (@$object_ref) . " object(s) (" . ref($object_ref->[0]) . ") into format '$format_name'" . ($parameters?"using parameters $parameters":''));
        }
        else
        {
            PrintDebug("Dumping 1 object $object_ref (" . ref($object_ref) . ") into format '$format_name'" . ($parameters?"using parameters $parameters":''));
        }
    }
    # check if we can dump this format
    if (!$format_name)
    {
        Throw('error' => "Unable to dump object $class: no dump format provided!");
    }
    elsif (!exists($dumpable_settings->{'static'}->{$format_name}))
    {
        Throw('error' => "Unable to dump object $class: unsupported dump format!", 'log' => "Format: '$format_name'");
    }
    return $dumpable_settings->{'static'}->{$format_name}->($object_ref, $parameters);
}


=pod

=head2 _CheckNotEmpty

B<Description>: check if a value is defined and not empty (but can be '0').

B<ArgsCount>: 1

=over 4

=item $value: (scalar)

The value to check.

=back

B<Return>: (boolean)

True if the value is not empty (can contain 0 but not an empty string).

=cut

sub _CheckNotEmpty
{
    my ($value) = @_;
    return (defined($value) && ($value ne ''));
}


=pod

=head2 StringifyObjectMember

B<Description>: produce a string from a given object and a list of members to
proceed.

B<ArgsCount>: 2

=over 4

=item $object: (object)

The object to proceed.

=item $member_chain: (ref on an array of strings)

A chain of member methods or keys or indexes to use to fetch the value to
return from current object.

=back

B<Return>: (string)

a string corresponding to the chain of members to call on the given object.

B<Example>:

    my $sequence_list = Greenphyl::DumpableObject::StringifyObjectMember($family, ['sequences', '*', 'seq_textid']);
    print "Family $family contains sequences:\n$sequence_list\n";

=cut

sub StringifyObjectMember
{
    my ($object, $member_chain) = @_;

    PrintDebug("StringifyObjectMember: BEGIN processing member chain '" . join('.', @$member_chain) . "' for object $object");
    my $value = $object;
    my @members = @$member_chain;
    while (@members)
    {
        my $member = shift(@members);
        if ($member eq '')
        {
            confess "ERROR: invalid column for object $object (" . ref($object) . "): '" . join('.', @members) . "' (check if each dot is preceded and followed by at least one alpha-numeric character)\n";
        }
        PrintDebug("Proceeding member '$member'");

        # make sure we got a reference on something and not an ending value
        if (!defined($value) || !ref($value))
        {
            PrintDebug("WARNING: invalid column for object $object (" . ref($object) . "): '" . join('.', @members) . "' (stopped at member '$member')");
            # not a reference, we can't go further
            last;
        }

        # check which kind of reference we got
        if (ref($value) eq 'ARRAY')
        {
            # we currently got an array
            # check for a star operator
            if ($member eq '*')
            {
                PrintDebug("Group member array values");
                # concatenate all values
                # check if we got scalars or objects
                if (ref($value->[0]) eq 'ARRAY')
                {
                    # concatenate array values
                    $value = join($ARRAY_JOIN_STRING, grep(_CheckNotEmpty($_), @$value));
                }
                else
                {
                    # we got objects, proceed each
                    my @stringified_objects;
                    foreach my $object (@$value)
                    {
                        push(@stringified_objects, StringifyObjectMember($object, \@members));
                    }
                    # clear remaining member list as they were processed
                    @members = ();
                    $value = join($ARRAY_JOIN_STRING, grep(_CheckNotEmpty($_), @stringified_objects));
                }
            }
            else
            {
                # here we are supposed to have a index as member
                if ($member !~ m/^\d+$/)
                {
                    confess "ERROR: invalid column for object $object (" . ref($object) . "): '" . join('.', @members) . "' (invalid index for an array)\n";
                }
                $value = $value->[$member];
            }
        }
        elsif (ref($value) eq 'HASH')
        {
            $value = $value->{$member};
        }
        elsif ($member =~ m/^[a-zA-Z]\w+$/)
        {
            $value = $value->$member;
        }
        else
        {
            confess "ERROR: invalid column for object $object (" . ref($object) . "): '" . join('.', @members) . "' contains invalid characters\n";
        }
    }

    # make sure we got something
    if (!defined($value))
    {
        $value = '';
    }

    # convert arrays and hashes into counts
    if ('ARRAY' eq ref($value))
    {
        PrintDebug('Get the number of elements into array');
        $value = scalar(@$value);
    }
    elsif ('HASH' eq ref($value))
    {
        PrintDebug('Get the number of elements into hash');
        $value = scalar(keys(%$value));
    }
    
    PrintDebug("StringifyObjectMember: END value: '$value'");
    # make sure we return a string
    return '' . $value;
}




=pod

=head1 METHODS

=head2 getSupportedFormats

B<Description>: returns the list of supported dump format.

B<ArgsCount>: 1

=over 4

=item $self: (Greenphyl::<Object>) (R)

Current object.

=back

B<Return>: (list of string)

the list of supported format types.

=cut

sub getSupportedFormats
{
    my ($self) = @_;
    if (!$self || !ref($self))
    {confess "ERROR: getSupportedFormats method called on a non-object element!\n";}
    return keys(%{$self->_GetDumpable()->{'dynamic'}});
}


=pod

=head2 supportsFormat

B<Description>: returns true if the given output format is supported.

B<ArgsCount>: 2

=over 4

=item $self: (Greenphyl::<Object>) (R)

Current object.

=item $format_name: (string) (R)

Name of the output format to check.

=back

B<Return>: (boolean)

True if the format is supported, false otherwise.

=cut

sub supportsFormat
{
    my ($self, $format_name) = @_;
    if (!$self || !ref($self))
    {confess "ERROR: supportsFormat method called on a non-object element!\n";}
    if (!defined($format_name))
    {confess "ERROR: no format type specified!\n";}
    return exists($self->_GetDumpable()->{'dynamic'}->{$format_name});
}


=pod

=head2 dump

B<Description>: returns the object dumped into a string.

B<ArgsCount>: 2-3

=over 4

=item $self: (Greenphyl::<Object>) (R)

Current object.

=item $format_name: (string) (R)

Name of the output format to use.

=item $parameters: (any ref or string) (O)

a reference or a string containing additional parameters.

=back

B<Return>: (string)

The dumped object string.

=cut

sub dump
{
    my ($self, $format_name, $parameters) = @_;
    if ($DEBUG)
    {PrintDebug("Dumping object $self (" . ref($self) . ") into format '$format_name'" . ($parameters?"using parameters $parameters":''));}
    return $self->_GetDumpable()->{'dynamic'}->{$format_name}->($self, $parameters);
}


=pod

=head2 getCGIMember

B<Description>: returns a hash containing the first member value retrived from
current CGI object. This can be usefull when some object values are hard/long
to obtain and should be dumped from a form that already had fetched them.

B<ArgsCount>: 1

=over 4

=item $self: (Greenphyl::DumpableObject derived object)

Current object.

=back

B<Return>: (hash ref)

a hash which keys are member names and values are member values from CGI.

=cut

sub getCGIMember
{
    my ($self) = @_;
    
    if (!exists($self->{'cgi_members'}))
    {
        $self->{'cgi_members'} = {};
        # extract associated values from CGI submitted parameters
        if (IsRunningCGI())
        {
            eval 'use Greenphyl::Web;';
            if ($@)
            {confess $@;}

            my $cgi = GetCGI();
            
            my $self_string = ''.$self;
            # process each parameter to check if it is related to $self
            my @parameter_names = $cgi->param();
            foreach my $parameter_name (@parameter_names)
            {
                # related parameters begin with current object stringified version
                if ($parameter_name =~ m/^$self_string\.(.*)/)
                {
                    # keep only first value
                    $self->{'cgi_members'}->{$1} = $cgi->param($parameter_name);
                }
            }
        }
    }

    return $self->{'cgi_members'};
}


=pod

=head2 getCGIMembers

B<Description>: returns a hash containing member values retrived from current
CGI object. This can be usefull when some object values are hard/long to obtain
and should be dumped from a form that already had fetched them.

B<ArgsCount>: 1

=over 4

=item $self: (Greenphyl::DumpableObject derived object)

Current object.

=back

B<Return>: (hash ref)

a hash which keys are member names and values are member values from CGI.

=cut

sub getCGIMembers
{
    my ($self) = @_;
    
    if (!exists($self->{'cgi_members'}))
    {
        $self->{'cgi_members'} = {};
        # extract associated values from CGI submitted parameters
        if (IsRunningCGI())
        {
            eval 'use Greenphyl::Web;';
            if ($@)
            {confess $@;}

            my $cgi = GetCGI();
            
            my $self_string = ''.$self;
            # process each parameter to check if it is related to $self
            my @parameter_names = $cgi->param();
            foreach my $parameter_name (@parameter_names)
            {
                # related parameters begin with current object stringified version
                if ($parameter_name =~ m/^$self_string\.(.*)/)
                {
                    # may contain just one or more than one value, save to an array
                    $self->{'cgi_members'}->{$1} = [$cgi->param($parameter_name)];
                }
            }
        }
    }

    return $self->{'cgi_members'};
}




=pod

=head1 AUTHORS

Mathieu ROUARD (Bioversity), m.rouard@cgiar.org

Valentin GUIGNON (Bioversity), v.guignon@cgiar.org

Matthieu CONTE (Syngenta)

=head1 VERSION

Version 1.0.0

Date 02/05/2012

=head1 SEE ALSO

GreenPhyl documentation, DBObject documentation.

=cut

return 1; # package return
