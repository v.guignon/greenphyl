=pod

=head1 NAME

Greenphyl::Sequence - GreenPhyl Sequence Object

=head1 SYNOPSIS

    use Greenphyl::Sequence;
    my $sequence = Greenphyl::Sequence->new($dbh, {'selectors' => {'accession' => ['LIKE', 'Os01g01050.1']}});
    print $sequence->name();

=head1 REQUIRES

Perl 5.8.0, Greenphyl::[Species, SequenceIPR, Family, IPR, DBXRef, Homology],
BioPerl

=head1 EXPORTS

nothing

=head1 DESCRIPTION

This module implements a GreenPhyl sequence database object.

=cut

package Greenphyl::Sequence;

use strict;
use warnings;

use base qw(Greenphyl::AbstractSequence);

use vars qw($AUTOLOAD);

use Carp qw(cluck confess croak);

use Greenphyl;
use Greenphyl::GO;
use Greenphyl::SourceDatabase;
use Greenphyl::DBXRefLinks;
use Greenphyl::Species; # for species code regexp
use Greenphyl::Homology;
use Greenphyl::PangeneFamily;
use Greenphyl::Tools::Sequences;

use IO::String;
use Bio::SeqIO;
use Bio::Seq;




# Package constants
####################

=pod

=head1 CONSTANTS

B<$OBJECT_PROPERTIES>: (hash ref)

sequence object properties description. See DBObject constructor documentation
for details.

B<$MAX_IPR_LAYERS>: (integer)

Maximum number of layers a sequence can have for IPR domain rendering.

=cut

our $DEBUG = 0;

our $OBJECT_PROPERTIES = {
    'table' => 'sequences',
    'key' => 'id',
    'alternate_keys' => ['accession'],
    'load'     => [
        'annotation',
        'accession',
        'filtered',
        'genome_id',
        'id',
        'length',
        'locus',
        'representative',
        'splice',
        'splice_priority',
        # 'polypeptide',
        'species_id',
        'type',
    ],
    'alias' => {
        'seq_id'       => 'id',
        'sequence_id'  => 'id',
        'seq_length'   => 'length',
        'name'         => 'accession',
        'seq_textid'   => 'accession',
        'textid'       => 'accession',
        'gene_name'    => 'locus',
    },
    'base' => {
        'annotation' => {
            'property_column' => 'annotation',
        },
        'filtered' => {
            'property_column' => 'filtered',
        },
        'id' => {
            'property_column' => 'id',
        },
        'length' => {
            'property_column' => 'length',
        },
        'accession' => {
            'property_column' => 'accession',
        },
        'locus' => {
            'property_column' => 'locus',
        },
        'representative' => {
            'property_column' => 'representative',
        },
        'splice' => {
            'property_column' => 'splice',
        },
        'splice_priority' => {
            'property_column' => 'splice_priority',
        },
        'polypeptide' => {
            'property_column' => 'polypeptide',
        },
        'species_id' => {
            'property_column' => 'species_id',
        },
        'genome_id' => {
            'property_column' => 'genome_id',
        },
        'type' => {
            'property_column' => 'type',
        },
    },
    'secondary' => {
        'synonyms' => {
            'property_table'  => 'sequence_synonyms',
            'property_key'    => 'sequence_id',
            'multiple_values' => 1,
            'property_column' => 'synonym',
        },
        'species' => {
            'object_key'      => 'species_id',
            'property_table'  => 'species',
            'property_key'    => 'id',
            'multiple_values' => 0,
            'property_module' => 'Greenphyl::Species',
        },
        'genome' => {
            'object_key'      => 'genome_id',
            'property_table'  => 'genome',
            'property_key'    => 'id',
            'multiple_values' => 0,
            'property_module' => 'Greenphyl::Genome',
        },
        'ipr_details' => {
            'property_table'  => 'ipr_sequences',
            'property_key'    => 'sequence_id',
            'multiple_values' => 1,
            'property_module' => 'Greenphyl::SequenceIPR',
        },
        'bbmh' => {
            'property_table'  => 'bbmh',
            'property_key'    => 'query_sequence_id',
            'multiple_values' => 1,
            'property_module' => 'Greenphyl::BBMH',
        },
    },
    'tertiary' => {
        'families' => {
            'link_table'        => 'families_sequences',
            'link_object_key'   => 'sequence_id',
            'link_property_key' => 'family_id',
            'multiple_values'   => 1,
            'property_module'   => 'Greenphyl::Family',
            'property_table'    => 'families',
            'property_key'      => 'id',
        },
        'ipr' => {
            'link_table'        => 'ipr_sequences',
            'link_object_key'   => 'sequence_id',
            'link_property_key' => 'ipr_id',
            'multiple_values'   => 1,
            'property_module'   => 'Greenphyl::IPR',
            'property_table'    => 'ipr',
            'property_key'      => 'id',
        },
        'dbxref' => {
            'link_table'        => 'dbxref_sequences',
            'link_object_key'   => 'sequence_id',
            'link_property_key' => 'dbxref_id',
            'multiple_values'   => 1,
            'property_module'   => 'Greenphyl::DBXRef',
            'property_table'    => 'dbxref',
            'property_key'      => 'id',
        },
        'homologies' => {
            'link_table'        => 'homologs',
            'link_object_key'   => 'subject_sequence_id',
            'link_property_key' => 'homology_id',
            'multiple_values'   => 1,
            'property_module'   => 'Greenphyl::Homology',
            'property_table'    => 'homologies',
            'property_key'      => 'id',
        },
        'homologs' => {
            'link_table'        => 'homologs',
            'link_object_key'   => 'subject_sequence_id',
            'link_property_key' => 'object_sequence_id',
            'multiple_values'   => 1,
            'property_module'   => 'Greenphyl::Sequence',
            'property_table'    => 'sequences',
            'property_key'      => 'id',
        },
        'bbmh_sequences' => {
            'link_table'        => 'bbmh',
            'link_object_key'   => 'query_sequence_id',
            'link_property_key' => 'hit_sequence_id',
            'multiple_values'   => 1,
            'property_module'   => 'Greenphyl::Sequence',
            'property_table'    => 'sequences',
            'property_key'      => 'id',
        },
        'ipr_go_id' => {
            'link_table'        => 'ipr_sequences',
            'link_object_key'   => 'sequence_id',
            'link_property_key' => 'ipr_id',
            'multiple_values'   => 1,
            'property_table'    => 'go_ipr',
            'property_key'      => 'ipr_id',
            'property_column'   => 'go_id',
        },
        'uniprot_go_id' => {
            'link_table'        => 'dbxref_sequences',
            'link_object_key'   => 'sequence_id',
            'link_property_key' => 'dbxref_id',
            'multiple_values'   => 1,
            'property_table'    => 'go_uniprot',
            'property_key'      => 'dbxref_id',
            'property_column'   => 'go_id',
        },
    },
};

our $SUPPORTED_DUMP_FORMAT = $Greenphyl::AbstractSequence::SUPPORTED_DUMP_FORMAT;

our $CONSENSUS_MULTI_ALIGNMENT_SUFFIX = '_multi.aln';
our $CONSENSUS_DISTANCE_MATRIX_SUFFIX = '.distmat';
our $CONSENSUS_MONO_ALIGNMENT_SUFFIX  = '_mono.aln';

our $NO_PANGENE_ACCESSION             = 'n/a';
our $SELF_PANGENE_ACCESSION           = '';



# Package subs
###############

=pod

=head1 STATIC METHODS

=head2 CONSTRUCTOR

B<Description>: Creates a new instance of a GreenPhyl sequence object.

B<ArgsCount>: 1-2

=over 4

=item $fasta_content: (DBI::db) (O)

A FASTA content to load the sequences from.

=item $dbh: (DBI) (U)

Database handler of the database to load the object from.

=item $parameters: (hash ref) (R)

See Greenphyl::DBObject constructor documentation for details.

=back

B<Return>: (Greenphyl::Sequence)

a new instance.

B<Caller>: General

B<Example>:

    my $sequence = new Greenphyl::Sequence($dbh, {'selectors' => {'accession' => ['LIKE', 'Os01g01050%']}});
    my $fasta_sequence = new Greenphyl::Sequence(">Os01g01050.1_ORYSA\nMINFAVQSRCAPTNSLCSCVALCERNFKLEQVEELASLIKDNLYSKHLVLSTEETLVGIL");

    # load from synonym
    $sequence = Greenphyl::Sequence->new(
        GetDatabaseHandler(),
        {
            'load'      => { 'synonyms' => 1,},
            'selectors' => {'sequence_synonyms.synonym' => 'GSMUA_Achr10T23190_001',},
        },
    );

    # load from both synonyms and accessions
    use Greenphyl::Tools::Sequences;
    
    ...
    @sequence_accessions = @{GetAccessionsFromSynonyms(\@sequence_accessions, \@sequence_accessions)};

    $sequence = Greenphyl::Sequence->new(
        GetDatabaseHandler(),
        {
            'selectors' => {'accession' => ['IN', @sequence_accessions],},
            'sql' => {'DISTINCT' => 1,}
        },
    );

=cut

sub new
{
    my ($proto) = shift();
    my $class = ref($proto) || $proto;

    return $class->SUPER::new(@_);
}




=pod

=head2 getGO

B<Description>: returns GO terms (objects) linked to sequence through IPR and
UniProt.

B<Aliases>: go

B<ArgsCount>: 1

=over 4

=item $self: (Greenphyl::Sequence) (R)

Current sequence.

=back

B<Return>: (array ref)

Returns an array of Greenphyl::GO objects.

B<Example>:

    foreach my $go (@{$sequence->getGO()})
    {
        ...
    }

=cut

sub getGO
{
    my ($self) = @_;

    #+FIXME: use go_sequences_cache
    if (!exists($self->{'go'}))
    {
        my $go_array = [
            @{$self->getIPRGO()},
            @{$self->getUniprotGO()},
        ];

        # init GO array
        $self->{'go'} = [];
        # select only one instance of each GO
        my %loaded_go = ();
        foreach my $go (@$go_array)
        {
            if ($go)
            {
                if (!exists($loaded_go{$go}))
                {
                    push(@{$self->{'go'}}, $go);
                }
                $loaded_go{$go} = 1;
            }
        }
    }

    return $self->{'go'};
}
# Aliases
sub go; *go = \&getGO;


=pod

=head2 getIPRGO

B<Description>: returns GO terms (objects) linked to sequence through IPR.

B<Aliases>: ipr_go

B<ArgsCount>: 1

=over 4

=item $self: (Greenphyl::Family) (R)

Current family.

=back

B<Return>: (array ref)

Returns an array of Greenphyl::GO objects.

B<Example>:

    foreach my $ipr_go (@{$family->getIPRGO()})
    {
        ...
    }

=cut

sub getIPRGO
{
    my ($self) = @_;

    #+FIXME: use go_sequences_cache
    if (!exists($self->{'ipr_go'}))
    {
        my $ipr_go_sql_query =  '(
            SELECT DISTINCT gi.go_id
            FROM go_ipr gi
                JOIN ipr_sequences si USING (ipr_id)
            WHERE
                si.sequence_id = ' . $self->{'id'} . '
        )';
        $self->{'ipr_go'} = [
            # returns a list of GO objects
            Greenphyl::GO->new(
                $self->{'_dbh'},
                {
                    'selectors' => {'id' => ['IN', $ipr_go_sql_query]},
                },
            ),
        ];
    }
    return $self->{'ipr_go'};
}
# Aliases
sub ipr_go; *ipr_go = \&getIPRGO;


=pod

=head2 getUniprotGO

B<Description>: returns GO terms (objects) linked to sequence through UniProt.

B<Aliases>: getUniProtGO, uniprot_go

B<ArgsCount>: 1

=over 4

=item $self: (Greenphyl::Sequence) (R)

Current sequence.

=back

B<Return>: (array ref)

Returns an array of Greenphyl::GO objects.

B<Example>:

    foreach my $uniprot_go (@{$sequence->getUniprotGO()})
    {
        ...
    }

=cut

sub getUniprotGO
{
    my ($self) = @_;

    #+FIXME: use go_sequences_cache
    if (!exists($self->{'uniprot_go'}))
    {
        my $uniprot_to_go_sql_query = '(
            SELECT utg.go_id
            FROM go_uniprot utg
                JOIN dbxref_sequences fd USING (dbxref_id)
            WHERE
                fd.sequence_id = ' . $self->{'id'} . '
        )';
        $self->{'uniprot_go'} = [
            Greenphyl::GO->new(
                $self->{'_dbh'},
                {
                    'selectors' => {'id' => ['IN', $uniprot_to_go_sql_query]},
                },
            ),
        ];
    }

    return $self->{'uniprot_go'};
}
# Aliases
sub getUniProtGO; *getUniProtGO = \&getUniprotGO;
sub uniprot_go;   *uniprot_go   = \&getUniprotGO;


=pod

=head2 initHomology

B<Description>: initialize current sequence homology hash.

B<ArgsCount>: 1

=over 4

=item $self: (Greenphyl::Sequence) (R)

Current sequence.

=back

B<Return>: (integer)

The number of homologs.

=cut

sub initHomology
{
    my ($self) = @_;

    if (!$self->getDBKeyValue())
    {return 0;}
    
    # init homology hash
    if (!defined($self->{'_homologies'}))
    {
        # Get homologies between representative sequences.
        $self->{'_homologies'} = {};
        my $sequence_id = $self->getRepresentativeSequence()->id;

        my $sql_query = "
            SELECT " . join(', ', Greenphyl::Homology->GetDefaultMembers('hf', 'homology_')) . ",
                   " . join(', ', Greenphyl::Sequence->GetDefaultMembers('s', 'sequence_')) . "
            FROM homologies hf
                JOIN homologs hs ON (hs.homology_id = hf.id)
                JOIN sequences s ON (s.id = hs.object_sequence_id)
            WHERE
                hs.subject_sequence_id = ?
        ";
        PrintDebug("SQL QUERY: $sql_query\nBindings: " . $sequence_id);

        my $homology_data_array = $self->{'_dbh'}->selectall_arrayref($sql_query, { 'Slice' => {} }, $sequence_id);
        foreach my $homology_data (@$homology_data_array)
        {
            my $sequence_hash = Greenphyl::Sequence->ExtractMembersFromHash($homology_data, 'sequence_');
            my $homolog = Greenphyl::Sequence->new($self->{'_dbh'}, {'load' => 'none', 'members' => $sequence_hash});
            my $homology_hash = Greenphyl::Homology->ExtractMembersFromHash($homology_data, 'homology_');
            my $homology = Greenphyl::Homology->new($self->{'_dbh'}, {'load' => 'none', 'members' => $homology_hash});
            
            my $homolog_sequences = [$self, $homolog];
            my @homolog_sequence_id = ($homolog->id);
            # Add non-representative sequences.
            foreach my $represented_sequence (@{$self->getRepresentedSequences()})
            {
                push(@$homolog_sequences, $represented_sequence);
                push(@homolog_sequence_id, $represented_sequence->id);
            }
            $homology->setSequences($homolog_sequences);
            foreach my $homolog_id (@homolog_sequence_id)
            {
                $self->{'_homologies'}->{$homolog_id} = $homology;
            }
        }
    }
    return scalar(keys(%{$self->{'_homologies'}}));
}


=pod

=head2 getHomologyWith

B<Description>: returns the homology object if there is an homology between
current sequence and a given one.

B<ArgsCount>: 2

=over 4

=item $self: (Greenphyl::Sequence) (R)

Current sequence.

=item $other: (Greenphyl::Sequence) (R)

The other sequence to check.

=back

B<Return>: (Greenphyl::Homology)

The homology object if one or undef.

B<Example>:

    my $homology = $sequence->getHomologyWith($other_sequence);

=cut

sub getHomologyWith
{
    my ($self, $other) = @_;

    if (!$other || (!$other->isa('Greenphyl::Sequence')))
    {
        confess "ERROR: getHomologyWith called without an initialized and valid other object!\n";
    }
    my $homology;

    if ($self->initHomology() && exists($self->{'_homologies'}->{$other->id}))
    {
        $homology = $self->{'_homologies'}->{$other->id};
    }

    return $homology;
}


=pod

=head2 getBBMHSequenceForLocus

B<Description>: .

B<ArgsCount>: 2

=over 4

=item $self: (Greenphyl::Sequence) (R)

Current sequence.

=item $locus: (string) (R)

.

=back

B<Return>: (Greenphyl::Sequence)


=cut

sub getBBMHSequenceForLocus
{
    my ($self, $locus) = @_;

    if (!$locus || ref($locus))
    {
        confess "ERROR: getBBMHSequenceForLocus called without a valid locus!\n";
    }
    
    my $sql_query = "
        SELECT " . join(', ', Greenphyl::Sequence->GetDefaultMembers('hit')) . "
        FROM bbmh b
            JOIN sequences hit ON (b.hit_sequence_id = hit.id)
        WHERE
            b.query_sequence_id = ?
            AND hit.locus = ?
    ";
    my @bindings = ($self->getRepresentativeSequence()->id, $locus);
    PrintDebug("SQL QUERY: $sql_query\nBindings: " . join(', ', @bindings));    
    my $bbmh_sequence_hash = $self->{'_dbh'}->selectrow_hashref($sql_query, undef, @bindings);
    my $bbmh_sequence = Greenphyl::Sequence->new($self->{'_dbh'}, {'load' => 'none', 'members' => $bbmh_sequence_hash});
    return $bbmh_sequence || undef;
}


=pod

=head2 initBBMH

B<Description>: initialize sequence BBMH hash.

B<ArgsCount>: 1

=over 4

=item $self: (Greenphyl::Sequence) (R)

Current sequence.

=back

B<Return>: (integer)

the number of BBMH.

=cut

sub initBBMH
{
    my ($self) = @_;

    if (!$self->getDBKeyValue())
    {return 0;}
    
    # init BBMH hash
    if (!defined($self->{'_bbmh'}))
    {
        foreach my $bbmh (@{$self->bbmh()})
        {
            if ($bbmh)
            {
                $self->{'_bbmh'}->{$bbmh->hit_id} = $bbmh;
            }
        }
    }

    return scalar(keys(%{$self->{'_bbmh'}}));
}


=pod

=head2 getBBMHWith

B<Description>: returns the BBMH object if the 2 given sequences have BBMH.

B<ArgsCount>: 2

=over 4

=item $self: (Greenphyl::Sequence) (R)

Current sequence.

=item $other: (Greenphyl::Sequence) (R)

The other sequence to check.

=back

B<Return>: (Greenphyl::BBMH)

the BBMH object if one or undef.

B<Example>:

    my $bbmh = $sequence->getBBMHWith($other_sequence);

=cut

sub getBBMHWith
{
    my ($self, $other) = @_;

    if (!$other)
    {
        confess "ERROR: getBBMHWith called without an initialized other object!\n";
    }

    my $bbmh;
    # check if we have BBMHs
    if ($self->initBBMH() && exists($self->{'_bbmh'}->{$other->id}))
    {
        $bbmh = $self->{'_bbmh'}->{$other->id};
    }
    return $bbmh;
}


=pod

=head2 getSimilarSequences

B<Description>: returns an array of similar sequences, either by homology or by
BBMH.

B<Aliases>: similar_sequences

B<ArgsCount>: 1-2

=over 4

=item $self: (Greenphyl::Sequence) (R)

Current sequence.

=item $species_ids: (array ref) (O)

An array of species ID that the similar sequences must belong to.
If not set, all species are allowed.
Default: all species.

=back

B<Return>: (array ref)

array of Greenphyl::Sequence or an empty array if no match.

B<Example>:

    my $similar_sequences = $sequence->getSimilarSequences([1,2,7]);

=cut

sub getSimilarSequences
{
    my ($self, $species_ids) = @_;

    my %similar_sequences;
    # get homologs
    foreach (@{$self->homologs()})
    {
        $similar_sequences{$_} ||= $_;
    }

    # get BBMH
    foreach (@{$self->bbmh()})
    {
        $similar_sequences{$_} ||= $_;
    }

    # check if species should be filtered
    if ($species_ids && !ref($species_ids))
    {
        # if just 1 species ID has been provided, wrap it in an array
        $species_ids = [$species_ids];
    }

    my @similar_sequences;
    if ($species_ids)
    {
        # filter species
        my $species_filter = {};
        foreach my $species_id (@$species_ids)
        {
            $species_filter->{$species_id} = 1;
        }

        # only keep requested species
        foreach my $similar_sequence (values(%similar_sequences))
        {
            if (exists($species_filter->{$similar_sequence->species_id}))
            {
                push(@similar_sequences, $similar_sequence);
            }
        }
    }
    else
    {
        # keep all
        @similar_sequences = values(%similar_sequences);
    }

    # sort sequences
    @similar_sequences = sort {$a->species->name cmp $b->species->name} @similar_sequences;

    return \@similar_sequences;
}
# aliases
sub similar_sequences; *similar_sequences = \&getSimilarSequences;


=pod

=head2 getPangeneFamily

B<Description>: returns the pangene family of current sequence.

B<ArgsCount>: 1

=over 4

=item $self: (Greenphyl::Sequence) (R)

Current sequence.

=back

B<Return>: (Greenphyl::PangeneFamily)

A pangene family object.

=cut

sub getPangeneFamily
{
    my ($self) = @_;

    if ($self->{'_pangene_family'})
    {
        return $self->{'_pangene_family'};
    }

    my $sql_query = "
        SELECT " . join(', ', Greenphyl::Family->GetDefaultMembers('f')) . "
        FROM families f
            JOIN families_sequences fs ON (fs.family_id = f.id)
        WHERE
            f.level IS NULL
            AND f.type IN ('core','soft-core','dispensable','specific')
            AND fs.sequence_id = ?
    ";
    PrintDebug("SQL QUERY: $sql_query\nBindings: " . $self->id);
    
    my $family_data_array = $self->{'_dbh'}->selectall_arrayref($sql_query, { 'Slice' => {} }, $self->id);
    if (1 < @$family_data_array)
    {
        PrintDebug("Warning: more than one pangene family for $self!");
    }

    if (@$family_data_array)
    {
        $family_data_array = $family_data_array->[0];
        $self->{'_pangene_family'} = Greenphyl::PangeneFamily->new(
            $self->{'_dbh'},
            {
                'load' => 'none',
                'members' => $family_data_array,
            },
        );
    }

    return $self->{'_pangene_family'};
}


=pod

=head2 isPangene

B<Description>: tells if current sequence is a pangene.

B<Aliases>: is_pangene

B<ArgsCount>: 1

=over 4

=item $self: (Greenphyl::Sequence) (R)

Current sequence.

=back

B<Return>: (boolean)

ture if pangene, false otherwise.

B<Example>:

    if ($sequence->isPangene())
    {
        ...
    }

=cut

sub isPangene
{
    my ($self) = @_;
    if (($self->type eq 'pan protein') || ($self->type eq 'pan dna'))
    {
        return 1;
    }
    else
    {
        return 0;
    }
}
# aliases
sub is_pangene; *is_pangene = \&isPangene;


=pod

=head2 getRepresentativeSequence

B<Description>: returns the representative sequence of current sequence or $self
if self-represented.

B<Aliases>: representative_sequence

B<ArgsCount>: 1-2

=over 4

=item $self: (Greenphyl::Sequence) (R)

Current sequence.

=item $direct_representative: (boolean) (O)

If set, only returns direct representative sequence which may not be the pangene
but rather the representative sequence in current genome (that is used for the
pangene).

=back

B<Return>: (Greenphyl::Sequence)

The representative sequence or self if no representative.

B<Example>:

    my $representative_sequence = $sequence->getRepresentativeSequence();

=cut

sub getRepresentativeSequence
{
    my ($self, $direct_representative) = @_;
    my $representative_sequence = $self;
    # Pan sequences have no representative.
    if (!$self->isPangene())
    {
        my $pangene_family = $self->getPangeneFamily();
        if ($pangene_family)
        {
            if ($direct_representative)
            {
                $representative_sequence = $pangene_family->getRepresentativeSequenceFor($self);
            }
            else
            {
                my $no_infinit = 5;
                while (!$representative_sequence->representative && --$no_infinit)
                {
                    $representative_sequence = $pangene_family->getRepresentativeSequenceFor($representative_sequence);
                }
                if (!$no_infinit)
                {
                  PrintDebug("Warning: unable to find representative sequence for $self!");
                }
            }
        }
    }
    return $representative_sequence;
}
# aliases
sub representative_sequence; *representative_sequence = \&getRepresentativeSequence;


=pod

=head2 getPangeneAccession

B<Description>: returns the accession of pangene in front of th sequence.
For pangene sequences, returns an empty string.
For sequences not related to a pangene, returns 'n/a'.

B<Aliases>: pangene_accession

B<ArgsCount>: 1

=over 4

=item $self: (Greenphyl::Sequence) (R)

Current sequence.

=back

B<Return>: (string)

The representative sequence or self if no representative.

B<Example>:

    my $pangene_accesison = $sequence->getPangeneAccession();

=cut

sub getPangeneAccession
{
    my ($self) = @_;

    # Returns an empty string for pangenes
    my $pangene_accession = $SELF_PANGENE_ACCESSION;

    if (!$self->isPangene())
    {
        my $pangene_family = $self->getPangeneFamily();
        if ($pangene_family)
        {
            $pangene_accession = $self->getRepresentativeSequence()->accession;
        }
        else
        {
            # Not in a pangene, return 'n/a'.
            $pangene_accession = $NO_PANGENE_ACCESSION;
        }
    }
    return $pangene_accession;
}
# aliases
sub pangene_accession; *pangene_accession = \&getPangeneAccession;


=pod

=head2 getRepresentedSequences

B<Description>: returns a array ref of represented sequences.

B<Aliases>: represented_sequences

B<ArgsCount>: 1

=over 4

=item $self: (Greenphyl::Sequence) (R)

Current sequence.

=back

B<Return>: (array ref of Greenphyl::Sequence)

An array of Greenphyl::Sequence or an empty array if no match.

B<Example>:

    my $represented_sequences = $sequence->getRepresentedSequences();

=cut

sub getRepresentedSequences
{
    my ($self) = @_;

    my $represented_sequences = [];
    my $pangene_family = $self->getPangeneFamily();
    if ($pangene_family)
    {
        $represented_sequences = $pangene_family->getRepresentedSequencesFor($self);
    }

    return $represented_sequences;
}
# aliases
sub represented_sequences; *represented_sequences = \&getRepresentedSequences;


=pod

=head2 getRepresentedSequenceStructure

B<Description>: returns a structure of represented sequences.

B<ArgsCount>: 1

=over 4

=item $self: (Greenphyl::Sequence) (R)

Current sequence.

=back

B<Return>: (hash ref)

A pangene structure.

B<Example>:

    my $represented_sequence_structure = $sequence->getRepresentedSequenceStructure();
    # Provides something like that for pan proteins:
    {
        'genome' => Greenphyl::Genome, # 'MUSAC'
        'as' => 'consensus',
        'consensus' => Greenphyl::Sequence, # 'musac_pan_p000019'
        'genomes' => {
            'MUSAC_zebrina_v1' => {
                'genome' => Greenphyl::Genome, # 'MUSAC_zebrina_v1'
                'as' => 'participating',
                'participating' => Greenphyl::Sequence, # 'Mazeb_scaffold684_t000110'
            },
            'MUSAC_malacensis_v2' => {
                'genome' => Greenphyl::Genome, # 'MUSAC_malacensis_v2'
                'as' => 'representative',
                'representative' => Greenphyl::Sequence, # 'Ma05_t29790.3'
                'participating' => [
                    Greenphyl::Sequence, # 'Ma05_t29790.1'
                    Greenphyl::Sequence, # 'Ma05_t29790.2'
                ],
            },
            ...
        },
    };

    # Provides something like that for representative proteins of genomes:
    {
        'genome' => Greenphyl::Genome, # 'MUSAC_malacensis_v2'
        'as' => 'representative',
        'representative' => Greenphyl::Sequence, # 'Ma05_t29790.3'
        'participating' => [
            Greenphyl::Sequence, # 'Ma05_t29790.1'
            Greenphyl::Sequence, # 'Ma05_t29790.2'
        ],
    };

=cut

sub getRepresentedSequenceStructure
{
    my ($self) = @_;

    my $represented_sequence_structure;
    my $pangene_family = $self->getPangeneFamily();
    if ($pangene_family)
    {
        $represented_sequence_structure = $pangene_family->getRepresentedSequenceStructureFor($self);
    }

    return $represented_sequence_structure;
}


=pod

=head2 families

B<Description>: returns an array of families corresponding to GreenPhyl
clustering which have a level <> 0. Other family types are excluded.
Works just like "->families()".

B<Aliases>: gpfamilies

B<ArgsCount>: 1-...

=over 4

=item $self: (Greenphyl::Sequence) (R)

Current sequence.

=back

B<Return>: (array ref)

array of Greenphyl::Family or an empty array if no GreenPhyl family.

=cut

sub families
{
    my ($self, $selector, @other_param) = @_;
    my $families;
    # Check if i'ts a representative sequence (ie. a pan protein or a protein
    # not represented by a pan protein in a family).
    if ($self->representative)
    {
        if (defined($selector))
        {
            if (ref($selector) eq 'HASH')
            {
                if (exists($selector->{'selectors'}))
                {
                    if (!exists($selector->{'selectors'}->{'level'}))
                    {
                        # Only return Greenphyl families and not pangene groups.
                        $selector->{'selectors'}->{'level'} = ['IN', 1, 2, 3, 4];
                    }
                }
                else
                {
                    # Only return Greenphyl families and not pangene groups.
                    $selector->{'selectors'} = {'level' => ['IN', 1, 2, 3, 4]};
                }
            }
        }
        else
        {
            # Only return Greenphyl families and not pangene groups.
            $selector = {'selectors' => {'level' => ['IN', 1, 2, 3, 4]}};
        }
        $families = $self->SUPER::families($selector, @other_param);
    }
    else
    {
        # Otherwise, return the representative protein families.
        my $representative = $self->getRepresentativeSequence();
        if ($representative != $self)
        {
            $families = $representative->families($selector, @other_param);
        }
    }

    return $families;
}
# aliases
sub getFamilies; *getFamilies = \&families;
sub setFamilies; *setFamilies = \&families;
sub fetchFamilies; *fetchFamilies = \&families;
sub Families;    *Families    = \&families;


=pod

=head2 homologs

B<Description>: returns homologs from representative sequence.

B<Aliases>: 

B<ArgsCount>: 1-...

=over 4

=item $self: (Greenphyl::Sequence) (R)

Current sequence.

=back

B<Return>: (array ref)

.

=cut

sub homologs
{
    my ($self, @params) = @_;
    my $representative_sequence = $self;
    # Check if i'ts a representative sequence (ie. a pan protein or a protein
    # not represented by a pan protein in a family).
    if (!$self->representative)
    {
        # Return the representative sequence.
        $representative_sequence = $self->getRepresentativeSequence();
    }
    my $homologs = $representative_sequence->SUPER::homologs(@params);

    return $homologs;
}
# aliases
sub getHomologs; *getHomologs = \&homologs;
sub fetchHomologs; *fetchHomologs = \&homologs;
sub Homologs;    *Homologs    = \&homologs;


=pod

=head2 bbmh

B<Description>: returns BBMH from representative sequence.

B<Aliases>: 

B<ArgsCount>: 1-...

=over 4

=item $self: (Greenphyl::Sequence) (R)

Current sequence.

=back

B<Return>: (array ref)

.

=cut

sub bbmh
{
    my ($self, @params) = @_;
    my $representative_sequence = $self;
    # Check if i'ts a representative sequence (ie. a pan protein or a protein
    # not represented by a pan protein in a family).
    if (!$self->representative)
    {
        # Return the representative sequence.
        $representative_sequence = $self->getRepresentativeSequence();
    }
    my $bbmh = $representative_sequence->SUPER::bbmh(@params);

    return $bbmh;
}
# aliases
sub getBbmh; *getBbmh = \&bbmh;
sub fetchBbmh; *fetchBbmh = \&bbmh;
sub Bbmh;    *Bbmh    = \&bbmh;


=pod

=head2 consensus_analysis

B<Description>: returns consensus files.

B<Aliases>: 

B<ArgsCount>: 1

=over 4

=item $self: (Greenphyl::Sequence) (R)

Current sequence.

=back

B<Return>: (hash ref)

Consensus data.

=cut

sub consensus_analysis
{
    my ($self) = @_;

    if (!$self->{'_consensus_analysis'})
    {
        my $consensus_file_path = GP('PANGENOMES_PATH') . '/' . $self->species->code . '/consensus/' . substr($self->accession, 0, -3) . '/';
        my $consensus_file_base_url = GetURL('downloads', undef, {'path' => '/pangenomes/' . $self->species->code . '/consensus/' . substr($self->accession, 0, -3) . '/' }),;
        my $consensus_analysis = {
            'distance_matrix' => $consensus_file_path . $self->accession . $CONSENSUS_DISTANCE_MATRIX_SUFFIX,
            'distance_matrix_url' => $consensus_file_base_url . $self->accession . $CONSENSUS_DISTANCE_MATRIX_SUFFIX,
            'multi_alignment' => $consensus_file_path . $self->accession . $CONSENSUS_MULTI_ALIGNMENT_SUFFIX,
            'multi_alignment_url' => $consensus_file_base_url . $self->accession . $CONSENSUS_MULTI_ALIGNMENT_SUFFIX,
            'mono_alignment' => $consensus_file_path . $self->accession . $CONSENSUS_MONO_ALIGNMENT_SUFFIX,
            'mono_alignment_url' => $consensus_file_base_url . $self->accession . $CONSENSUS_MONO_ALIGNMENT_SUFFIX,
        };
        if (!-s $consensus_analysis->{'distance_matrix'})
        {
            $consensus_analysis->{'distance_matrix'} = undef;
            $consensus_analysis->{'distance_matrix_url'} = undef;
        }
        else
        {
            # Pre-process distance matrix.
            my ($fh, $line);
            if (open($fh, $consensus_analysis->{'distance_matrix'}))
            {
              my (@sequence_names, @distance_matrix);
              # Skip comments and find matrix starting line.
              while (defined($line = <$fh>) && ($line !~ m/^\s+1/))
              {}

              while (defined($line = <$fh>))
              {
                  # fixes 'nan' value issues (no common AA in alignments, only - or X).
                  $line =~ s/\s-?nan\s/ 999999 /g;
                  my @values = split(/\s+/, $line);
                  # Ignore leading space.
                  shift(@values);
                  # Ignore sequence number.
                  pop(@values);
                  # Get sequence name.
                  my $current_row = scalar(@sequence_names);
                  push(@sequence_names, pop(@values));

                  # Fill matrix with values already parsed.
                  $distance_matrix[$current_row] = [];
                  for (my $parsed_seq_i = 0; $current_row > $parsed_seq_i; ++$parsed_seq_i)
                  {
                      push(@{$distance_matrix[$current_row]}, $distance_matrix[$parsed_seq_i]->[$current_row]);
                  }
                  while (@values)
                  {
                      push(@{$distance_matrix[$current_row]}, shift(@values));
                  }
              }
              close($fh);
              
              $consensus_analysis->{'distance_matrix_data'} = {
                  'headers' => [@sequence_names],
                  'data' => [@distance_matrix],
              };
            }

        }

        if (!-s $consensus_analysis->{'multi_alignment'})
        {
            $consensus_analysis->{'multi_alignment'} = undef;
            $consensus_analysis->{'multi_alignment_url'} = undef;
        }
        if (!-s $consensus_analysis->{'mono_alignment'})
        {
            $consensus_analysis->{'mono_alignment'} = undef;
            $consensus_analysis->{'mono_alignment_url'} = undef;
        }
        $self->{'_consensus_analysis'} = $consensus_analysis;
    }
    
    return $self->{'_consensus_analysis'};
}



=pod

=head2 convertToBioSeq

B<Description>: returns a Bio::Seq object from Greenphyl::AbstractSequence object.

B<ArgsCount>: 1-2

=over 4

=item $self: (Greenphyl::Sequence) (R)

The sequence to convert.

=item $parameters: (hash) (O)

Optional parameter hash. Keys:
-'without_species_code': if set to true, species code will not be outputted.

=back

B<Return>: (Bio::Seq)

A Bio Perl Bio::Seq object.

=cut

sub convertToBioSeq
{
    my ($self, $parameters) = @_;
    my $bio_seq = $self->SUPER::convertToBioSeq($parameters);
    
    # Check for sequence in a pangene.
    my $pangene_accession = $self->getPangeneAccession;
    if ($pangene_accession
        && ($pangene_accession ne $NO_PANGENE_ACCESSION)
        && ($pangene_accession ne $SELF_PANGENE_ACCESSION))
    {
        my $new_id = $bio_seq->id() . '|' . $pangene_accession;
        $bio_seq->id($new_id);
        $bio_seq->accession_number($new_id);
    }

    return $bio_seq;
}


=pod

=head1 AUTHORS

Mathieu ROUARD (Bioversity), m.rouard@cgiar.org

Valentin GUIGNON (Bioversity), v.guignon@cgiar.org

Matthieu CONTE (Syngenta)

=head1 VERSION

Version 3.2.0

Date 13/10/2020

=head1 SEE ALSO

GreenPhyl documentation.

=cut

return 1; # package return
