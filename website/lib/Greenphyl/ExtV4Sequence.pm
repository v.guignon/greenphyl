=pod

=head1 NAME

Greenphyl::ExtV4Sequence - GreenPhyl v4 Sequence Object

=head1 SYNOPSIS

    use Greenphyl::ExtV4Sequence;
    my $v4_sequence = Greenphyl::ExtV4Sequence->new($dbh, {'selectors' => {'accession' => ['LIKE', 'Os01g01050.1']}});
    print $v4_sequence->name();

=head1 REQUIRES

Perl 5.8.0, Greenphyl::[Species, SequenceIPR, Family, IPR, DBXRef, Homology],
BioPerl

=head1 EXPORTS

nothing

=head1 DESCRIPTION

This module implements a GreenPhyl v4 sequence database object.

=cut

package Greenphyl::ExtV4Sequence;

use strict;
use warnings;

use base qw(Greenphyl::AbstractSequence);

use vars qw($AUTOLOAD);

use Carp qw(cluck confess croak);

use Greenphyl;
use Greenphyl::GO;
use Greenphyl::SourceDatabase;
use Greenphyl::DBXRefLinks;
use Greenphyl::ExtV4Species; # for species code regexp
use Greenphyl::Homology;
use Greenphyl::Tools::Sequences;

use IO::String;
use Bio::SeqIO;
use Bio::Seq;




# Package constants
####################

=pod

=head1 CONSTANTS

B<$OBJECT_PROPERTIES>: (hash ref)

sequence object properties description. See DBObject constructor documentation
for details.

B<$MAX_IPR_LAYERS>: (integer)

Maximum number of layers a sequence can have for IPR domain rendering.

=cut

our $DEBUG = 0;

our $OBJECT_PROPERTIES = {
    'table' => 'sequences',
    'key' => 'id',
    'alternate_keys' => ['accession'],
    'load'     => [
        'annotation',
        'filtered',
        'id',
        'length',
        'accession',
        'locus',
        'splice',
        'splice_priority',
        # 'polypeptide',
        'species_id',
    ],
    'alias' => {
        'seq_id'       => 'id',
        'sequence_id'  => 'id',
        'seq_length'   => 'length',
        'name'         => 'accession',
        'seq_textid'   => 'accession',
        'textid'       => 'accession',
        'gene_name'    => 'locus',
    },
    'base' => {
        'annotation' => {
            'property_column' => 'annotation',
        },
        'filtered' => {
            'property_column' => 'filtered',
        },
        'id' => {
            'property_column' => 'id',
        },
        'length' => {
            'property_column' => 'length',
        },
        'accession' => {
            'property_column' => 'accession',
        },
        'locus' => {
            'property_column' => 'locus',
        },
        'splice' => {
            'property_column' => 'splice',
        },
        'splice_priority' => {
            'property_column' => 'splice_priority',
        },
        'polypeptide' => {
            'property_column' => 'polypeptide',
        },
        'species_id' => {
            'property_column' => 'species_id',
        },
    },
    'secondary' => {
        'synonyms' => {
            'property_table'  => 'sequence_synonyms',
            'property_key'    => 'sequence_id',
            'multiple_values' => 1,
            'property_column' => 'synonym',
        },
        'species' => {
            'object_key'      => 'species_id',
            'property_table'  => 'species',
            'property_key'    => 'id',
            'multiple_values' => 0,
            'property_module' => 'Greenphyl::ExtV4Species',
        },
        'ipr_details' => {
            'property_table'  => 'ipr_sequences',
            'property_key'    => 'sequence_id',
            'multiple_values' => 1,
            'property_module' => 'Greenphyl::SequenceIPR',
        },
        'bbmh' => {
            'property_table'  => 'bbmh',
            'property_key'    => 'query_sequence_id',
            'multiple_values' => 1,
            'property_module' => 'Greenphyl::BBMH',
        },
    },
    'tertiary' => {
        'families' => {
            'link_table'        => 'families_sequences',
            'link_object_key'   => 'sequence_id',
            'link_property_key' => 'family_id',
            'multiple_values'   => 1,
            'property_module'   => 'Greenphyl::ExtV4Family',
            'property_table'    => 'families',
            'property_key'      => 'id',
        },
        'ipr' => {
            'link_table'        => 'ipr_sequences',
            'link_object_key'   => 'sequence_id',
            'link_property_key' => 'ipr_id',
            'multiple_values'   => 1,
            'property_module'   => 'Greenphyl::IPR',
            'property_table'    => 'ipr',
            'property_key'      => 'id',
        },
        'dbxref' => {
            'link_table'        => 'dbxref_sequences',
            'link_object_key'   => 'sequence_id',
            'link_property_key' => 'dbxref_id',
            'multiple_values'   => 1,
            'property_module'   => 'Greenphyl::DBXRef',
            'property_table'    => 'dbxref',
            'property_key'      => 'id',
        },
        'homologies' => {
            'link_table'        => 'homologs',
            'link_object_key'   => 'subject_sequence_id',
            'link_property_key' => 'homology_id',
            'multiple_values'   => 1,
            'property_module'   => 'Greenphyl::Homology',
            'property_table'    => 'homologies',
            'property_key'      => 'id',
        },
        'homologs' => {
            'link_table'        => 'homologs',
            'link_object_key'   => 'subject_sequence_id',
            'link_property_key' => 'object_sequence_id',
            'multiple_values'   => 1,
            'property_module'   => 'Greenphyl::ExtV4Sequence',
            'property_table'    => 'sequences',
            'property_key'      => 'id',
        },
        'bbmh_sequences' => {
            'link_table'        => 'bbmh',
            'link_object_key'   => 'query_sequence_id',
            'link_property_key' => 'hit_sequence_id',
            'multiple_values'   => 1,
            'property_module'   => 'Greenphyl::ExtV4Sequence',
            'property_table'    => 'sequences',
            'property_key'      => 'id',
        },
        'ipr_go_id' => {
            'link_table'        => 'ipr_sequences',
            'link_object_key'   => 'sequence_id',
            'link_property_key' => 'ipr_id',
            'multiple_values'   => 1,
            'property_table'    => 'go_ipr',
            'property_key'      => 'ipr_id',
            'property_column'   => 'go_id',
        },
        'uniprot_go_id' => {
            'link_table'        => 'dbxref_sequences',
            'link_object_key'   => 'sequence_id',
            'link_property_key' => 'dbxref_id',
            'multiple_values'   => 1,
            'property_table'    => 'go_uniprot',
            'property_key'      => 'dbxref_id',
            'property_column'   => 'go_id',
        },
    },
};

our $SUPPORTED_DUMP_FORMAT = $Greenphyl::AbstractSequence::SUPPORTED_DUMP_FORMAT;




# Package subs
###############

=pod

=head1 STATIC METHODS

=head2 CONSTRUCTOR

B<Description>: Creates a new instance of a GreenPhyl v4 sequence object.

B<ArgsCount>: 1-2

=over 4

=item $fasta_content: (DBI::db) (O)

A FASTA content to load the sequences from.

=item $dbh: (DBI) (U)

Database handler of the database to load the object from.

=item $parameters: (hash ref) (R)

See Greenphyl::DBObject constructor documentation for details.

=back

B<Return>: (Greenphyl::ExtV4Sequence)

a new instance.

B<Caller>: General

B<Example>:

    my $v4_sequence = new Greenphyl::ExtV4Sequence($dbh, {'selectors' => {'accession' => ['LIKE', 'Os01g01050%']}});
    my $fasta_sequence = new Greenphyl::ExtV4Sequence(">Os01g01050.1_ORYSA\nMINFAVQSRCAPTNSLCSCVALCERNFKLEQVEELASLIKDNLYSKHLVLSTEETLVGIL");

    # load from synonym
    $v4_sequence = Greenphyl::ExtV4Sequence->new(
        GetDatabaseHandler(),
        {
            'load'      => { 'synonyms' => 1,},
            'selectors' => {'sequence_synonyms.synonym' => 'GSMUA_Achr10T23190_001',},
        },
    );

    # load from both synonyms and accessions
    use Greenphyl::Tools::Sequences;
    
    ...
    @sequence_accessions = @{GetAccessionsFromSynonyms(\@sequence_accessions, \@sequence_accessions)};

    $v4_sequence = Greenphyl::ExtV4Sequence->new(
        GetDatabaseHandler(),
        {
            'selectors' => {'accession' => ['IN', @sequence_accessions],},
            'sql' => {'DISTINCT' => 1,}
        },
    );

=cut

sub new
{
    my ($proto) = shift();
    my $class = ref($proto) || $proto;

    return $class->SUPER::new(@_);
}




=pod

=head2 getGO

B<Description>: returns GO terms (objects) linked to sequence through IPR and
UniProt.

B<Aliases>: go

B<ArgsCount>: 1

=over 4

=item $self: (Greenphyl::ExtV4Sequence) (R)

Current sequence.

=back

B<Return>: (array ref)

Returns an array of Greenphyl::GO objects.

B<Example>:

    foreach my $go (@{$sequence->getGO()})
    {
        ...
    }

=cut

sub getGO
{
    my ($self) = @_;

    #+FIXME: use go_sequences_cache
    if (!exists($self->{'go'}))
    {
        my $go_array = [
            @{$self->getIPRGO()},
            @{$self->getUniprotGO()},
        ];

        # init GO array
        $self->{'go'} = [];
        # select only one instance of each GO
        my %loaded_go = ();
        foreach my $go (@$go_array)
        {
            if ($go)
            {
                if (!exists($loaded_go{$go}))
                {
                    push(@{$self->{'go'}}, $go);
                }
                $loaded_go{$go} = 1;
            }
        }
    }

    return $self->{'go'};
}
# Aliases
sub go; *go = \&getGO;


=pod

=head2 getIPRGO

B<Description>: returns GO terms (objects) linked to sequence through IPR.

B<Aliases>: ipr_go

B<ArgsCount>: 1

=over 4

=item $self: (Greenphyl::ExtV4Family) (R)

Current family.

=back

B<Return>: (array ref)

Returns an array of Greenphyl::GO objects.

B<Example>:

    foreach my $ipr_go (@{$family->getIPRGO()})
    {
        ...
    }

=cut

sub getIPRGO
{
    my ($self) = @_;

    #+FIXME: use go_sequences_cache
    if (!exists($self->{'ipr_go'}))
    {
        my $ipr_go_sql_query =  '(
            SELECT DISTINCT gi.go_id
            FROM go_ipr gi
                JOIN ipr_sequences si USING (ipr_id)
            WHERE
                si.sequence_id = ' . $self->{'id'} . '
        )';
        $self->{'ipr_go'} = [
            # returns a list of GO objects
            Greenphyl::GO->new(
                $self->{'_dbh'},
                {
                    'selectors' => {'id' => ['IN', $ipr_go_sql_query]},
                },
            ),
        ];
    }
    return $self->{'ipr_go'};
}
# Aliases
sub ipr_go; *ipr_go = \&getIPRGO;


=pod

=head2 getUniprotGO

B<Description>: returns GO terms (objects) linked to sequence through UniProt.

B<Aliases>: getUniProtGO, uniprot_go

B<ArgsCount>: 1

=over 4

=item $self: (Greenphyl::ExtV4Sequence) (R)

Current sequence.

=back

B<Return>: (array ref)

Returns an array of Greenphyl::GO objects.

B<Example>:

    foreach my $uniprot_go (@{$sequence->getUniprotGO()})
    {
        ...
    }

=cut

sub getUniprotGO
{
    my ($self) = @_;

    #+FIXME: use go_sequences_cache
    if (!exists($self->{'uniprot_go'}))
    {
        my $uniprot_to_go_sql_query = '(
            SELECT utg.go_id
            FROM go_uniprot utg
                JOIN dbxref_sequences fd USING (dbxref_id)
            WHERE
                fd.sequence_id = ' . $self->{'id'} . '
        )';
        $self->{'uniprot_go'} = [
            Greenphyl::GO->new(
                $self->{'_dbh'},
                {
                    'selectors' => {'id' => ['IN', $uniprot_to_go_sql_query]},
                },
            ),
        ];
    }

    return $self->{'uniprot_go'};
}
# Aliases
sub getUniProtGO; *getUniProtGO = \&getUniprotGO;
sub uniprot_go;   *uniprot_go   = \&getUniprotGO;


=pod

=head2 initHomology

B<Description>: initialize current sequence homology hash.

B<ArgsCount>: 1

=over 4

=item $self: (Greenphyl::ExtV4Sequence) (R)

Current sequence.

=back

B<Return>: (integer)

The number of homologs.

=cut

sub initHomology
{
    my ($self) = @_;

    if (!$self->getDBKeyValue())
    {return 0;}
    
    # init homology hash
    if (!defined($self->{'_homologies'}))
    {

        $self->{'_homologies'} = {};
        my $sql_query = "
            SELECT " . join(', ', Greenphyl::Homology->GetDefaultMembers('hf', 'homology_')) . ",
                   " . join(', ', Greenphyl::ExtV4Sequence->GetDefaultMembers('s', 'sequence_')) . "
            FROM homologies hf
                JOIN homologs hs ON (hs.homology_id = hf.id)
                JOIN sequences s ON (s.id = hs.object_sequence_id)
            WHERE
                hs.subject_sequence_id = ?
        ";
        PrintDebug("SQL QUERY: $sql_query\nBindings: " . $self->id);
        
        my $homology_data_array = $self->{'_dbh'}->selectall_arrayref($sql_query, { 'Slice' => {} }, $self->id);
        foreach my $homology_data (@$homology_data_array)
        {
            my $sequence_hash = Greenphyl::ExtV4Sequence->ExtractMembersFromHash($homology_data, 'sequence_');
            my $homolog = Greenphyl::ExtV4Sequence->new($self->{'_dbh'}, {'load' => 'none', 'members' => $sequence_hash});
            my $homology_hash = Greenphyl::Homology->ExtractMembersFromHash($homology_data, 'homology_');
            my $homology = Greenphyl::Homology->new($self->{'_dbh'}, {'load' => 'none', 'members' => $homology_hash});
            $homology->setSequences([$self, $homolog]);
            $self->{'_homologies'}->{$homolog->id} = $homology;
        }
    }
    return scalar(keys(%{$self->{'_homologies'}}));
}


=pod

=head2 getHomologyWith

B<Description>: returns the homology object if there is an homology between
current sequence and a given one.

B<ArgsCount>: 2

=over 4

=item $self: (Greenphyl::ExtV4Sequence) (R)

Current sequence.

=item $other: (Greenphyl::ExtV4Sequence) (R)

The other sequence to check.

=back

B<Return>: (Greenphyl::Homology)

The homology object if one or undef.

B<Example>:

    my $homology = $sequence->getHomologyWith($other_sequence);

=cut

sub getHomologyWith
{
    my ($self, $other) = @_;

    if (!$other || (!$other->isa('Greenphyl::ExtV4Sequence')))
    {
        confess "ERROR: getHomologyWith called without an initialized and valid other object!\n";
    }
    my $homology;

    if ($self->initHomology() && exists($self->{'_homologies'}->{$other->id}))
    {
        $homology = $self->{'_homologies'}->{$other->id};
    }

    return $homology;
}


=pod

=head2 getBBMHSequenceForLocus

B<Description>: .

B<ArgsCount>: 2

=over 4

=item $self: (Greenphyl::ExtV4Sequence) (R)

Current sequence.

=item $locus: (string) (R)

.

=back

B<Return>: (Greenphyl::ExtV4Sequence)


=cut

sub getBBMHSequenceForLocus
{
    my ($self, $locus) = @_;

    if (!$locus || ref($locus))
    {
        confess "ERROR: getBBMHSequenceForLocus called without a valid locus!\n";
    }
    
    my $sql_query = "
        SELECT " . join(', ', Greenphyl::ExtV4Sequence->GetDefaultMembers('hit')) . "
        FROM bbmh b
            JOIN sequences hit ON (b.hit_sequence_id = hit.id)
        WHERE
            b.query_sequence_id = ?
            AND hit.locus = ?
    ";
    my @bindings = ($self->id, $locus);
    PrintDebug("SQL QUERY: $sql_query\nBindings: " . join(', ', @bindings));    
    my $bbmh_sequence_hash = $self->{'_dbh'}->selectrow_hashref($sql_query, undef, @bindings);
    my $bbmh_sequence = Greenphyl::ExtV4Sequence->new($self->{'_dbh'}, {'load' => 'none', 'members' => $bbmh_sequence_hash});
    return $bbmh_sequence || undef;
}


=pod

=head2 initBBMH

B<Description>: initialize sequence BBMH hash.

B<ArgsCount>: 1

=over 4

=item $self: (Greenphyl::ExtV4Sequence) (R)

Current sequence.

=back

B<Return>: (integer)

the number of BBMH.

=cut

sub initBBMH
{
    my ($self) = @_;

    if (!$self->getDBKeyValue())
    {return 0;}
    
    # init BBMH hash
    if (!defined($self->{'_bbmh'}))
    {
        foreach my $bbmh (@{$self->bbmh()})
        {
            if ($bbmh)
            {
                $self->{'_bbmh'}->{$bbmh->hit_id} = $bbmh;
            }
        }
    }

    return scalar(keys(%{$self->{'_bbmh'}}));
}


=pod

=head2 getBBMHWith

B<Description>: returns the BBMH object if the 2 given sequences have BBMH.

B<ArgsCount>: 2

=over 4

=item $self: (Greenphyl::ExtV4Sequence) (R)

Current sequence.

=item $other: (Greenphyl::ExtV4Sequence) (R)

The other sequence to check.

=back

B<Return>: (Greenphyl::BBMH)

the BBMH object if one or undef.

B<Example>:

    my $bbmh = $sequence->getBBMHWith($other_sequence);

=cut

sub getBBMHWith
{
    my ($self, $other) = @_;

    if (!$other)
    {
        confess "ERROR: getBBMHWith called without an initialized other object!\n";
    }

    my $bbmh;
    # check if we have BBMHs
    if ($self->initBBMH() && exists($self->{'_bbmh'}->{$other->id}))
    {
        $bbmh = $self->{'_bbmh'}->{$other->id};
    }
    return $bbmh;
}


=pod

=head2 getSimilarSequences

B<Description>: returns an array of similar sequences, either by homology or by
BBMH.

B<Aliases>: similar_sequences

B<ArgsCount>: 1-2

=over 4

=item $self: (Greenphyl::ExtV4Sequence) (R)

Current sequence.

=item $species_ids: (array ref) (O)

An array of species ID that the similar sequences must belong to.
If not set, all species are allowed.
Default: all species.

=back

B<Return>: (array ref)

array of Greenphyl::ExtV4Sequence or an empty array if no match.

B<Example>:

    my $similar_sequences = $sequence->getSimilarSequences([1,2,7]);

=cut

sub getSimilarSequences
{
    my ($self, $species_ids) = @_;

    my %similar_sequences;
    # get homologs
    foreach (@{$self->homologs()})
    {
        $similar_sequences{$_} ||= $_;
    }

    # get BBMH
    foreach (@{$self->bbmh()})
    {
        $similar_sequences{$_} ||= $_;
    }

    # check if species should be filtered
    if ($species_ids && !ref($species_ids))
    {
        # if just 1 species ID has been provided, wrap it in an array
        $species_ids = [$species_ids];
    }

    my @similar_sequences;
    if ($species_ids)
    {
        # filter species
        my $species_filter = {};
        foreach my $species_id (@$species_ids)
        {
            $species_filter->{$species_id} = 1;
        }

        # only keep requested species
        foreach my $similar_sequence (values(%similar_sequences))
        {
            if (exists($species_filter->{$similar_sequence->species_id}))
            {
                push(@similar_sequences, $similar_sequence);
            }
        }
    }
    else
    {
        # keep all
        @similar_sequences = values(%similar_sequences);
    }

    # sort sequences
    @similar_sequences = sort {$a->species->name cmp $b->species->name} @similar_sequences;

    return \@similar_sequences;
}
# aliases
sub similar_sequences; *similar_sequences = \&getSimilarSequences;


=pod

=head1 AUTHORS

Mathieu ROUARD (Bioversity), m.rouard@cgiar.org

Valentin GUIGNON (Bioversity), v.guignon@cgiar.org

Matthieu CONTE (Syngenta)

=head1 VERSION

Version 2.0.0

Date 11/10/2013

=head1 SEE ALSO

GreenPhyl documentation.

=cut

return 1; # package return
