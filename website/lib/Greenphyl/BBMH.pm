=pod

=head1 NAME

Greenphyl::BBMH - GreenPhyl BBMH Object

=head1 SYNOPSIS

    use Greenphyl::BBMH;
    my $bbmh = Greenphyl::BBMH->new($dbh, {'selectors' => {'query' => 1234, 'hit' => 4567}});
    print $bbmh->score();

=head1 REQUIRES

Perl5

=head1 EXPORTS

Nothing

=head1 DESCRIPTION

This module implements a GreenPhyl BBMH database object.

=cut

package Greenphyl::BBMH;

use strict;
use warnings;

use base qw(Greenphyl::CachedDBObject);

use vars qw($AUTOLOAD);

use Carp qw(cluck confess croak);




# Package constants
####################

=pod

=head1 CONSTANTS

B<$OBJECT_PROPERTIES>: (hash ref)

object properties description. See DBObject constructor documentation for
details.

=cut

our $OBJECT_PROPERTIES = {
    'table' => 'bbmh',
    'key' => 'query_sequence_id,hit_sequence_id',
    'alternate_keys' => [],
    'load'     => [
        'e_value',
        'hit_sequence_id',
        'query_sequence_id',
        'score',
    ],
    'alias' => {
        'evalue'   => 'e_value',
        'hit_id'   => 'hit_sequence_id',
        'query_id' => 'query_sequence_id',
        'eval'     => 'e_value',
        'hit'      => 'hit_sequence_id',
        'query'    => 'query_sequence_id',
    },
    'base' => {
        'e_value' => {
            'property_column' => 'e_value',
        },
        'hit_sequence_id' => {
            'property_column' => 'hit_sequence_id',
        },
        'query_sequence_id' => {
            'property_column' => 'query_sequence_id',
        },
        'score' => {
            'property_column' => 'score',
        },
    },
    'secondary' => {
        'query_sequence' => {
            'object_key'      => 'query_sequence_id',
            'property_table'  => 'sequences',
            'property_key'    => 'id',
            'multiple_values' => 0,
            'property_module' => 'Greenphyl::Sequence',
        },
        'hit_sequence' => {
            'object_key'      => 'hit_sequence_id',
            'property_table'  => 'sequences',
            'property_key'    => 'id',
            'multiple_values' => 0,
            'property_module' => 'Greenphyl::Sequence',
        },
    },
    'tertiary' => {
    },

};




# Package subs
###############

=pod

=head1 STATIC METHODS

=head2 CONSTRUCTOR

B<Description>: Creates a new instance of a GreenPhyl BBMH object.

B<ArgsCount>: 2

=over 4

=item $dbh: (DBI::db) (U)

Database handler of the database to load the object from.

=item $parameters: (hash ref) (R)

See Greenphyl::DBObject constructor documentation for details.

=back

B<Return>: (Greenphyl::BBMH)

a new instance.

B<Caller>: General

B<Example>:

    my $bbmh = new Greenphyl::BBMH($dbh, {'selectors' => {'query' => 1234, 'hit' => 4567}});

=cut

sub new
{
    my $proto = shift();
    my $class = ref($proto) || $proto;
    
    my $object_properties = $class->GetDefaultClassProperties();

    return $class->SUPER::new($object_properties, @_);
}




=pod

=head1 AUTHORS

Mathieu ROUARD (Bioversity), m.rouard@cgiar.org

Valentin GUIGNON (Bioversity), v.guignon@cgiar.org

Matthieu CONTE (Syngenta)

=head1 VERSION

Version 1.0.0

Date 17/05/2012

=head1 SEE ALSO

GreenPhyl documentation, DBObject documentation.

=cut

return 1; # package return
