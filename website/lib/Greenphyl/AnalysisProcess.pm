=pod

=head1 NAME

Greenphyl::AnalysisProcess - GreenPhyl Process Object

=head1 SYNOPSIS

    use Greenphyl::AnalysisProcess;
    my $process = Greenphyl::AnalysisProcess->new($dbh, {'selectors' => {'id' => 42}});
    print $process->name();

=head1 REQUIRES

Perl5

=head1 EXPORTS

Nothing

=head1 DESCRIPTION

This module implements a GreenPhyl AnalysisProcess database object.

=cut

package Greenphyl::AnalysisProcess;

use strict;
use warnings;

use base qw(Greenphyl::CachedDBObject);

use vars qw($AUTOLOAD);

use Carp qw(cluck confess croak);




# Package constants
####################

=pod

=head1 CONSTANTS

B<$OBJECT_PROPERTIES>: (hash ref)

object properties description. See DBObject constructor documentation for
details.

=cut

our $DEBUG = 0;
our $OBJECT_PROPERTIES = {
    'table' => 'processes',
    'key' => 'id',
    'alternate_keys' => [],
    'load'     => [
        'description',
        'duration',
        'id',
        'last_update',
        'name',
        'start_date',
        'status',
    ],
    'alias' => {
    },
    'base' => {
        'description' => {
            'property_column' => 'description',
        },
        'duration' => {
            'property_column' => 'duration',
        },
        'id' => {
            'property_column' => 'id',
        },
        'last_update' => {
            'property_column' => 'last_update',
        },
        'name' => {
            'property_column' => 'name',
        },
        'start_date' => {
            'property_column' => 'start_date',
        },
        'status' => {
            'property_column' => 'status',
        },
    },
    'secondary' => {
        'processing' => {
            'property_table'  => 'family_processing',
            'property_key'    => 'process_id',
            'multiple_values' => 1,
            'property_module' => 'Greenphyl::FamilyProcessing',
        },
    },
    'tertiary' => {
        'families' => {
            'link_table'        => 'family_processing',
            'link_object_key'   => 'process_id',
            'link_property_key' => 'family_id',
            'property_table'    => 'families',
            'property_key'      => 'id',
            'multiple_values'   => 1,
            'property_module'   => 'Greenphyl::Family',
        },
    },
};




# Package subs
###############

=pod

=head1 STATIC METHODS

=head2 CONSTRUCTOR

B<Description>: Creates a new instance of a GreenPhyl AnalysisProcess object.

B<ArgsCount>: 2

=over 4

=item $dbh: (DBI::db) (U)

Database handler of the database to load the object from.

=item $parameters: (hash ref) (R)

See Greenphyl::DBObject constructor documentation for details.

=back

B<Return>: (Greenphyl::AnalysisProcess)

a new instance.

B<Caller>: General

B<Example>:

    my $process = new Greenphyl::AnalysisProcess($dbh, {'selectors' => {'id' => 42}});

=cut

sub new
{
    my $proto = shift();
    my $class = ref($proto) || $proto;
    
    my $object_properties = $class->GetDefaultClassProperties();

    return $class->SUPER::new($object_properties, @_);
}




=pod

=head1 AUTHORS

Mathieu ROUARD (Bioversity), m.rouard@cgiar.org

Valentin GUIGNON (Bioversity), v.guignon@cgiar.org

Matthieu CONTE (Syngenta)

=head1 VERSION

Version 1.0.0

Date 10/05/2013

=head1 SEE ALSO

GreenPhyl documentation, DBObject documentation.

=cut

return 1; # package return
