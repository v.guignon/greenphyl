=pod

=head1 NAME

Greenphyl::ExtV3Species - GreenPhyl v3 Species Object

=head1 SYNOPSIS

    use Greenphyl::ExtV3Species;
    my $v3_species = Greenphyl::ExtV3Species->new($dbh, {'selectors' => {'species_name' => 'ARATH'}});
    print $v3_species->name();

=head1 REQUIRES

Perl5

=head1 EXPORTS

Nothing

=head1 DESCRIPTION

This module implements a GreenPhyl v3 Species database object.

=cut

package Greenphyl::ExtV3Species;

use strict;
use warnings;

use base qw(
    Greenphyl::CachedDBObject
    Greenphyl::DumpableObject
    Greenphyl::DumpableTabularData
);

use vars qw($AUTOLOAD);

use Carp qw(cluck confess croak);

use Greenphyl;
use Greenphyl::Taxonomy;
use Greenphyl::Sequence;
use Greenphyl::SourceDatabase;




# Package constants
####################

=pod

=head1 CONSTANTS

B<$DEBUG>: (boolean)

When set to true, it enables debug mode.

B<$OBJECT_PROPERTIES>: (hash ref)

object properties description. See DBObject constructor documentation for
details.

=cut

our $DEBUG = 0;

our $OBJECT_PROPERTIES = {
    'table' => 'species',
    'key' => 'species_id',
    'alternate_keys' => ['species_name', 'tax_id', 'organism'],
    'load'     => [
        'species_id',
        'species_name',
        'organism',
        'common_name',
        'tax_id',
        'sequence_number',
        'url_institute',
        'url_fasta',
        'url_picture',
        'version',
        'tax_order',
        'version_notes',
        'chromosome_number',
        'genome_size',
    ],
    'alias' => {
        'code' => 'species_name',
        'id'   => 'species_id',
        'name' => 'organism',
    },
    'base' => {
        'species_id' => {
            'property_column' => 'species_id',
        },
        'species_name' => {
            'property_column' => 'species_name',
        },
        'organism' => {
            'property_column' => 'organism',
        },
        'common_name' => {
            'property_column' => 'common_name',
        },
        'tax_id' => {
            'property_column' => 'tax_id',
        },
        'sequence_number' => {
            'property_column' => 'sequence_number',
        },
        'url_institute' => {
            'property_column' => 'url_institute',
        },
        'url_fasta' => {
            'property_column' => 'url_fasta',
        },
        'url_picture' => {
            'property_column' => 'url_picture',
        },
        'version' => {
            'property_column' => 'version',
        },
        'tax_order' => {
            'property_column' => 'tax_order',
        },
        'version_notes' => {
            'property_column' => 'version_notes',
        },
        'chromosome_number' => {
            'property_column' => 'chromosome_number',
        },
        'genome_size' => {
            'property_column' => 'genome_size',
        },
    },
    'secondary' => {
        'sequences' => {
            'property_table'  => 'sequences',
            'property_key'    => 'species_id',
            'multiple_values' => 1,
            'property_module' => 'Greenphyl::ExtV3Sequence',
        },
    },
    'tertiary' => {
        'families'   => {
            'link_table'        => 'count_family_seq',
            'link_object_key'   => 'species_id',
            'link_property_key' => 'family_id',
            'property_table'    => 'family',
            'property_key'      => 'family_id',
            'multiple_values'   => 1,
            'property_module'   => 'Greenphyl::ExtV3Family',
        },
    },

};

our $SPECIES_CODE_REGEXP = '[A-Z][A-Z0-9]{2,5}'; # first character must nnot be a digit

our $SUPPORTED_DUMP_FORMAT =
    {
        'static'  => {
            'excel'   => \&DumpExcel,
            'csv'     => \&DumpCSV,
            'xml'     => \&DumpXML,
        },
        'dynamic' => {
            'excel'   => \&DumpExcel,
            'csv'     => \&DumpCSV,
            'xml'     => \&DumpXML,
        },
    };



# Package subs
###############

=pod

=head1 STATIC METHODS

=head2 CONSTRUCTOR

B<Description>: Creates a new instance of a GreenPhyl Species object.

B<ArgsCount>: 2

=over 4

=item $dbh: (DBI::db) (U)

Database handler of the database to load the object from.

=item $parameters: (hash ref) (R)

See Greenphyl::DBObject constructor documentation for details.

=back

B<Return>: (Greenphyl::Species)

a new instance.

B<Caller>: General

B<Example>:

    my $species = new Greenphyl::Species($dbh, {'selectors' => {'species_name' => 'ARATH'}});

=cut

sub new
{
    my $proto = shift();
    my $class = ref($proto) || $proto;
    
    my $object_properties = $class->GetDefaultClassProperties();

    return $class->SUPER::new($object_properties, @_);
}




=pod

=head1 METHODS

=pod

=head2 getStringValue

B<Description>: returns Species code.

B<ArgsCount>: 1

=over 4

=item $self: (Greenphyl::Species)

the species object.

=back

B<Return>: (string)

the species code.

=cut

sub getStringValue
{
    my ($self) = @_;
    return $self->{'species_name'};
}


=pod

=head2 DumpExcel, DumpCSV, DumpXML

B<Description>: Returns a string in the given format containing a table with
the requested columns of given species.

B<ArgsCount>: 2

=over 4

=item $object_ref: (array of Greenphyl::species) (R)

The list of species to process.

=item $parameters: (hash ref) (R)

a hash containing the key 'columns' that contains the columns to dump.
Each key is a column name (label) and each corresponding value is a string
containing the chain (dot-separated) of members to call to fetch the value from
each species.

=back

B<Return>: (string)

a string in the given format containing a table with the requested columns of
given species.

B<Example>:

    my $excel_fh;
    if (open($excel_fh, '>species.xls'))
    {
        binmode($excel_fh);
        print {$excel_fh} Greenphyl::Species::DumpExcel([$species1, $species2], {'columns' => {'01.Name'=>'name','02.Code'=>'code','03.Common name'=>'common_name'}});
        close($excel_fh);
    }
    ...

=cut

sub DumpExcel
{
    my ($object_ref, $parameters) = @_;

    return Greenphyl::Species->DumpTable('excel', $object_ref, $parameters);
}


sub DumpCSV
{
    my ($object_ref, $parameters) = @_;

    return Greenphyl::Species->DumpTable('csv', $object_ref, $parameters);
}


sub DumpXML
{
    my ($object_ref, $parameters) = @_;
    
    return Greenphyl::Species->DumpTable('xml', $object_ref, $parameters);
}


=pod

=head1 AUTHORS

Mathieu ROUARD (Bioversity), m.rouard@cgiar.org

Valentin GUIGNON (Bioversity), v.guignon@cgiar.org

Matthieu CONTE (Syngenta)

Sebastien BRIOIS (Syngenta)

=head1 VERSION

Version 1.0.0

Date 02/12/2013

=head1 SEE ALSO

GreenPhyl documentation, DBObject documentation.

=cut

return 1; # package return
