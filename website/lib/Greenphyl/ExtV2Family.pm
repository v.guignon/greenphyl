=pod

=head1 NAME

Greenphyl::ExtV2Family - GreenPhyl v2 Family Object

=head1 SYNOPSIS

    use Greenphyl::ExtV2Family;
    my $v2_family = Greenphyl::ExtV2Family->new($dbh, {'selectors' => {'family_id' => 205}});
    print $v2_family->name();

=head1 REQUIRES

Perl5

=head1 EXPORTS

Nothing

=head1 DESCRIPTION

This module implements a GreenPhyl v2 family database object.

=cut

package Greenphyl::ExtV2Family;

use strict;
use warnings;

use base qw(Greenphyl::AbstractFamily);

use vars qw($AUTOLOAD);

use Carp qw(cluck confess croak);

use Greenphyl::Config;
use Greenphyl;



# Package constants
####################

=pod

=head1 CONSTANTS

B<$OBJECT_PROPERTIES>: (hash ref)

sequence object properties description. See DBObject constructor documentation
for details.

=cut

our $DEBUG = 0;
our $OBJECT_PROPERTIES = {
    'table' => 'family',
    'key' => 'family_id',
    'alternate_keys' => [],
    'load'     => [
        'family_id',
        'family_name',
        'family_number',
        'type',
        'description',
        'pub_ref',
        'pmid',
        'validated',
        'black_list',
        'inference',
        'node_id',
        'species_id',
        'plant_spe',
        'updated',
    ],
    'alias' => {
        'black_listed' => 'black_list',
        'id' => 'family_id',
        'name' => 'family_name',
        'number' => 'family_number',
        'sequence_count' => 'family_number',
    },
    'base' => {
        'family_id' => {
            'property_column' => 'family_id',
        },
        'family_name' => {
            'property_column' => 'family_name',
        },
        'family_number' => {
            'property_column' => 'family_number',
        },
        'type' => {
            'property_column' => 'type',
        },
        'description' => {
            'property_column' => 'description',
        },
        'pub_ref' => {
            'property_column' => 'pub_ref',
        },
        'pmid' => {
            'property_column' => 'pmid',
        },
        'validated' => {
            'property_column' => 'validated',
        },
        'black_list' => {
            'property_column' => 'black_list',
        },
        'inference' => {
            'property_column' => 'inference',
        },
        'node_id' => {
            'property_column' => 'node_id',
        },
        'species_id' => {
            'property_column' => 'species_id',
        },
        'plant_spe' => {
            'property_column' => 'plant_spe',
        },
        'updated' => {
            'property_column' => 'updated',
        },
    },
    'secondary' => {
        'level' => {
            'property_table'  => 'found_in',
            'property_key'    => 'family_id',
            'multiple_values' => 0,
            'property_column' => 'class_id',
        },
    },
    'tertiary' => {
        'sequences' => {
            'link_table'        => 'seq_is_in',
            'link_object_key'   => 'family_id',
            'link_property_key' => 'seq_id',
            'multiple_values'   => 1,
            'property_module'   => 'Greenphyl::ExtV2Sequence',
            'property_table'    => 'sequences',
            'property_key'      => 'seq_id',
        },
    },
};

our $SUPPORTED_DUMP_FORMAT = $Greenphyl::AbstractFamily::SUPPORTED_DUMP_FORMAT;



# Package subs
###############

=pod

=head1 STATIC METHODS

=head2 CONSTRUCTOR

B<Description>: Creates a new instance of a GreenPhyl family object.

B<ArgsCount>: 2

=over 4

=item $dbh: (DBI::db) (U)

Database handler of the database to load the object from.

=item $parameters: (hash ref) (R)

See Greenphyl::DBObject constructor documentation for details.

=back

B<Return>: (Greenphyl::ExtV2Family)

a new instance.

B<Caller>: General

B<Example>:

    my $v2_family = new Greenphyl::ExtV2Family($dbh, {'selectors' => {'id' => 205, 'validated' => ['<>', 0]}});

=cut

sub new
{
    my ($proto) = shift();
    my $class = ref($proto) || $proto;

    return $class->SUPER::new(@_);
}




=pod

=head1 DIAGNOSTICS

=head1 AUTHORS

Mathieu ROUARD (Bioversity), m.rouard@cgiar.org

Valentin GUIGNON (Bioversity), v.guignon@cgiar.org

Matthieu CONTE (Syngenta)

Sebastien BRIOIS (Syngenta)

=head1 VERSION

Version 1.0.0

Date 20/05/2014

=head1 SEE ALSO

GreenPhyl documentation, DBObject documentation.

=cut

return 1; # package return
