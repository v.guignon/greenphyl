=pod

=head1 NAME

Greenphyl::ExtV3DBXRef - GreenPhyl v3 DBXRef Object

=head1 SYNOPSIS

    use Greenphyl::ExtV3DBXRef;
    my $v3_dbxref = Greenphyl::ExtV3DBXRef->new($dbh, {'selectors' => {'dbxref_id' => 42}});
    print $v3_dbxref->name();

=head1 REQUIRES

Perl5

=head1 EXPORTS

Nothing

=head1 DESCRIPTION

This module implements a GreenPhyl v3 DBXRef database object.

=cut

package Greenphyl::ExtV3DBXRef;

use strict;
use warnings;

use base qw(Greenphyl::CachedDBObject);

use vars qw($AUTOLOAD);

use Carp qw(cluck confess croak);




# Package constants
####################

=pod

=head1 CONSTANTS

B<$OBJECT_PROPERTIES>: (hash ref)

object properties description. See DBObject constructor documentation for
details.

=cut

our $OBJECT_PROPERTIES = {
    'table' => 'dbxref',
    'key' => 'dbxref_id',
    'alternate_keys' => ['accession'],
    'load'     => [
        'dbxref_id',
        'db_id',
        'alias',
        'accession',
        'description',
        'version',
        'type',
    ],
    'alias' => {
        'id' => 'dbxref_id',
    },
    'base' => {
        'dbxref_id' => {
            'property_column' => 'dbxref_id',
        },
        'db_id' => {
            'property_column' => 'db_id',
        },
        'alias' => {
            'property_column' => 'alias',
        },
        'accession' => {
            'property_column' => 'accession',
        },
        'description' => {
            'property_column' => 'description',
        },
        'version' => {
            'property_column' => 'version',
        },
        'type' => {
            'property_column' => 'type',
        },
    },
    'secondary' => {
        'db' => {
            'object_key'      => 'db_id',
            'property_table'  => 'db',
            'property_key'    => 'db_id',
            'multiple_values' => 0,
            'property_module' => 'Greenphyl::ExtV3SourceDatabase',
        },
    },
    'tertiary' => {
        'sequences' => {
            'link_table'        => 'sequence_dbxref',
            'link_object_key'   => 'dbxref_id',
            'link_property_key' => 'seq_id',
            'property_module'   => 'Greenphyl::ExtV3Sequence',
            'property_table'    => 'sequences',
            'property_key'      => 'seq_id',
            'multiple_values'   => 1,
        },
        'families' => {
            'object_key'        => 'dbxref_id',
            'link_table'        => 'family_dbxref',
            'link_object_key'   => 'dbxref_id',
            'link_property_key' => 'family_id',
            'property_module'   => 'Greenphyl::ExtV3Family',
            'property_table'    => 'family',
            'property_key'      => 'family_id',
            'multiple_values'   => 1,
        },
    },

};




# Package subs
###############

=pod

=head1 STATIC METHODS

=head2 CONSTRUCTOR

B<Description>: Creates a new instance of a GreenPhyl DBXRef object.

B<ArgsCount>: 2

=over 4

=item $dbh: (DBI::db) (U)

Database handler of the database to load the object from.

=item $parameters: (hash ref) (R)

See Greenphyl::DBObject constructor documentation for details.

=back

B<Return>: (Greenphyl::DBXRef)

a new instance.

B<Caller>: General

B<Example>:

    my $dbxref = new Greenphyl::DBXRef($dbh, {'selectors' => {'dbxref_id' => 42}});

=cut

sub new
{
    my $proto = shift();
    my $class = ref($proto) || $proto;
    
    my $object_properties = $class->GetDefaultClassProperties();

    return $class->SUPER::new($object_properties, @_);
}




=pod

=head1 METHODS

=head2 getStringValue

B<Description>: returns database cross-reference accession.

B<ArgsCount>: 1

=over 4

=item $self: (Greenphyl::DBXRef)

the DBXRef.

=back

B<Return>: (string)

the DBXRef accession.

=cut

sub getStringValue
{
    my ($self) = @_;
    # return $self->{'accession'} . ($self->{'version'} ? '-' . $self->{'version'} : '');
    return $self->{'accession'};
}




=pod

=head1 AUTHORS

Mathieu ROUARD (Bioversity), m.rouard@cgiar.org

Valentin GUIGNON (Bioversity), v.guignon@cgiar.org

Matthieu CONTE (Syngenta)

Sebastien BRIOIS (Syngenta)

Sebastien BRIOIS (Syngenta)

=head1 VERSION

Version 1.0.0

Date 02/12/2013

=head1 SEE ALSO

GreenPhyl documentation, DBObject documentation.

=cut

return 1; # package return
