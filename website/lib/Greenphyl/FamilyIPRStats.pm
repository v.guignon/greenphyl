=pod

=head1 NAME

Greenphyl::FamilyIPRStats - GreenPhyl FamilyIPRStats Object

=head1 SYNOPSIS

    use Greenphyl::FamilyIPRStats;
    my $ipr_stats = Greenphyl::FamilyIPRStats->new($dbh, {'selectors' => {'family_id' => 205}});
    print $ipr_stats->perc();

=head1 REQUIRES

Perl5

=head1 EXPORTS

Nothing

=head1 DESCRIPTION

This module implements a GreenPhyl FamilyIPRStats database object.

=cut

package Greenphyl::FamilyIPRStats;

use strict;
use warnings;

use base qw(
    Greenphyl::CachedDBObject
    Greenphyl::DumpableObject
    Greenphyl::DumpableTabularData
);

use vars qw($AUTOLOAD);

use Carp qw(cluck confess croak);




# Package constants
####################

=pod

=head1 CONSTANTS

B<$OBJECT_PROPERTIES>: (hash ref)

object properties description. See DBObject constructor documentation for
details.

=cut

our $OBJECT_PROPERTIES = {
    'table' => 'families_ipr_cache',
    'key' => 'family_id,ipr_id',
    'alternate_keys' => [],
    'load'     => [
        'family_id',
        'ipr_id',
        'sequence_count',
        'percent',
        'family_specific',
    ],
    'alias' => {
        'perc'        => 'percent',
        'occurence'   => 'sequence_count',
        'spec'        => 'family_specific',
        'specific'    => 'family_specific',
        'specificity' => 'family_specific',
    },
    'base' => {
        'family_id' => {
            'property_column' => 'family_id',
        },
        'ipr_id' => {
            'property_column' => 'ipr_id',
        },
        'sequence_count' => {
            'property_column' => 'sequence_count',
        },
        'percent' => {
            'property_column' => 'percent',
        },
        'family_specific' => {
            'property_column' => 'family_specific',
        },
    },
    'secondary' => {
        'family' => {
            'object_key'      => 'family_id',
            'property_table'  => 'family',
            'property_key'    => 'id',
            'multiple_values' => 0,
            'property_module' => 'Greenphyl::Family',
        },
        'ipr' => {
            'object_key'     => 'ipr_id',
            'property_table'  => 'ipr',
            'property_key'    => 'id',
            'multiple_values' => 0,
            'property_module' => 'Greenphyl::IPR',
        },
    },
    'tertiary' => {
    },

};

our $SUPPORTED_DUMP_FORMAT =
    {
        'static'  => {
            'excel'   => \&DumpExcel,
            'csv'     => \&DumpCSV,
            'xml'     => \&DumpXML,
            'json'    => \&DumpJSON,
        },
        'dynamic' => {
            'excel'   => \&DumpExcel,
            'csv'     => \&DumpCSV,
            'xml'     => \&DumpXML,
            'json'    => \&DumpJSON,
        },
    };




# Package subs
###############

=pod

=head1 STATIC METHODS

=head2 CONSTRUCTOR

B<Description>: Creates a new instance of a GreenPhyl FamilyIPRStats object.

B<ArgsCount>: 2

=over 4

=item $dbh: (DBI::db) (U)

Database handler of the database to load the object from.

=item $parameters: (hash ref) (R)

See Greenphyl::DBObject constructor documentation for details.

=back

B<Return>: (Greenphyl::FamilyIPRStats)

a new instance.

B<Caller>: General

B<Example>:

    my $stats = new Greenphyl::FamilyIPRStats($dbh, {'selectors' => {'family_id' => 205}});

=cut

sub new
{
    my $proto = shift();
    my $class = ref($proto) || $proto;
    
    my $object_properties = $class->GetDefaultClassProperties();

    return $class->SUPER::new($object_properties, @_);
}



sub DumpExcel
{
    return Greenphyl::DumpableTabularData::DumpExcel(@_);
}


sub DumpCSV
{
    return Greenphyl::DumpableTabularData::DumpCSV(@_);
}


sub DumpXML
{
    return Greenphyl::DumpableTabularData::DumpXML(@_);
}


sub DumpJSON
{
    return Greenphyl::DumpableTabularData::DumpJSON(@_);
}


=pod

=head1 AUTHORS

Mathieu ROUARD (Bioversity), m.rouard@cgiar.org

Valentin GUIGNON (Bioversity), v.guignon@cgiar.org

Matthieu CONTE (Syngenta)

=head1 VERSION

Version 1.0.0

Date 17/04/2012

=head1 SEE ALSO

GreenPhyl documentation, DBObject documentation.

=cut

return 1; # package return
