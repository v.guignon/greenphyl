=pod

=head1 NAME

Greenphyl::DBObject - Parent module for database objects

=head1 SYNOPSIS

    package Greenphyl::Family;
    use Greenphyl::DBObject;
    use base qw(Greenphyl::DBObject);
    ...
    return 1;

    # loading a single instance
    # load family which has id (family_id) equal to 205
    # and loads default properties plus 'sequence_count' but not 'black_listed'.
    my $family = Greenphyl::Family->new($dbh, 'selectors' => {'id' => 205}, 'load' => {'sequence_count' => 1, 'black_listed' => 0});
    if ($family)
    {
        # here the autoload power kicks in...
        print "Sequence count for family $family: $family->getSeqCount()\n";
        # note: here, "$family" will display the family ID (205) and not
        # something like "HASH(0xc5c52a0)".

        # the following lines are equivalent:
        print $family->id . "\n";
        print $family->family_id . "\n";
        print $family->getId() . "\n";
        print $family->getID() . "\n";
        print $family->getFamilyId() . "\n";

        # change family description
        $family->setDescription('This is a test family');
        # and commit changes to database
        $family->save();

        # dynamically load a missing element:
        my $species_iprs = $family->getIPR();
    }

    # loading several instances
    my @families = Greenphyl::Family->new(
        GetDatabaseHandler(),
        {
            'selectors' => {'family_id' => ['IN', 200, 203, 205]},
        },
    );

    # create a new instance from scratch
    my $sequence = Greenphyl::Sequence->new(
        undef,
        {
            'members' => {'seq_textid' => 'test_sequence'},
        },
    );


=head1 REQUIRES

Perl5

=head1 EXPORTS

Nothing

=head1 DESCRIPTION

This module can be used as parent module for database object such as sequences,
families, ipr, etc.

It provides methods to instanciate database objects given some parameters,
auto-fetch missing properties and provide save facilities.

=head1 CREATING A DERIVATED CLASS

To create a new module for a database object, use the "DBObjectTemplate.pmt"
file as module template. Copy it an rename it into the object name you want and
replace what's needed inside the file (see documentation inside that file).
You may have to also use "generate_table_properties.pl" script to auto-generate
a valid property hash that you can then customize to suite your needs.

=head1 TRICKS

=over 4

Here are some syntaxic or language tricks used in the code and explained in
details.

=item "use overload..."

it allows us to compare object and display a human readable values when we
want to print the object. For instance, when we print a family object usgin

    print $family;

it will display the family ID instead of something like "HASH(0xc5c52a0)" that
wouldn't help us much.

=item my $space_holders = '?, ' x $#{$selectors->{$_}};

the 'x' (ie. "time" or "cross") operator allows us to repeat a sequence a
certain number of times. In the case of SQL queries where several following
space-holders should appear inside parentheses, this is helpfull.
The above single-line instruction is equivalent (in a more efficient way) to:
    my $space_holders = '';
    my $values_array = $selectors->{$_};
    # note: our array also contains the 'operator' which should not be taken in
    # account, that's why we substract '1'.
    my $number_of_cross = scalar(@$values_array) - 1; # equivalent to the index of last element
    for (1 .. $number_of_cross)
    {
        $space_holders .= '?, ';
    }

=item push(@bind_values, @{$selectors->{$_}}[1..$#{$selectors->{$_}}]);

this statement adds several values to @bind_values array.
$selectors: is a hash ref of a hash containing column names as keys and as
 values arrays of string, the first one being an operator and the next ones
 being values the column value should be compared to.
$selectors->{$_}: returns an array ref on a list of selectors (strings)
@{$selectors->{$_}}: dereference the array ref
$#{$selectors->{$_}}: returns the index of the last element of the array
1..$#{$selectors->{$_}}: generates a list of index from 1 to the last index
 of the array
@{$selectors->{$_}}[1..$#{$selectors->{$_}}]: returns the subset of the
 array that excludes the first element. In our case, it just skip the
 'operator' value and returns the rest of values the column value should be
 compared to.

=item my $new_instance = ref($self)->new($self->{'_dbh'}, { 'members' => $row_values, 'load' => 'none'});

This instanciate a new object of the same kind of the "$self" object. It
retrieves the object module using ref($self) then it calls the constructor
method (ie. "new") of the module.

=item eval("use $property_module;");

This dynamically loads the Perl module corresponding to "$property_module". The
module is loaded only once by Perl even if this instruction is called several
times for the same "$property_module". The module is loaded in current
namespace. If the module is missing or invalid (syntax error,...), then "$@" is
guarantied to contain the non-empty error message.

=item my @new_objects = ("$property_module"->new($self->{'_dbh'}, {'selectors' => $selector, 'sql' => $value}));
and $self->{$member} = "$property_module"->new($self->{'_dbh'}, {'selectors' => $selector});

These instructions dynamically call the constructor method (ie. "new") of the
specified module "$property_module" that is determined dynamically. If the
module or the constructor is missing, then Perl may display an error message
saying it can't coherce a string into an array ref because the content of the
string couldn't be related to a module so Perl will treate it as a string.

=item $self->{$key_name} = join($KEY_SEPARATOR, @$self{split(/\s*$KEY_SEPARATOR\s*/o, $key_name)});

when object key is composed by several columns (coma-separated names), it joins
the values of each column using comas into one string.

    @$self{split(/\s*,\s*/, $key_name)}

returns the list of values of each column ($self->{$key_name[0]},
$self->{$key_name[1]}, ...).

=back

=cut

package Greenphyl::DBObject;


use strict;
use warnings;
use vars qw($AUTOLOAD);

# overloading these operators allows to manipulate the object as a simplified
# value. For instance, "print $family;" will not display "HASH(0xc5c52a0)" but
# rather "205" (ie. the family ID).
# An other example would be "if ($family)" would not execute the block if no
# family ID has been set (or family_id = 0).
# see http://perldoc.perl.org/overload.html#Magic-Autogeneration for details.
use overload
    (
        '0+'   => \&_getIntValue,
        '""'   => \&_getStringValue,
        'bool' => \&_getBooleanValue,
        '=='   => \&_isEqual,
        '!='   => \&_isNotEqual,
   );

use Carp qw (cluck confess croak);

use Scalar::Util qw(looks_like_number);

use Storable qw(nfreeze thaw);

use Greenphyl;



# Package constants
####################

=pod

=head1 CONSTANTS

B<$DEBUG>: (boolean)

enable/disable debug mode.

B<$ALL_PROPERTIES_CODE>: (string)

Code to use as selector to load all the object properties.

B<$ALL_PROPERTIES_REGEXP>: (regexp string)

A regular expression matching allowed expression that would mean "load all the
object properties".

B<$KEY_SEPARATOR>: (string)

Character (or string) used to separate column names and values in the case of a
multiple-column key.

B<$MAX_IN_CLAUSE_ELEMENTS>: (integer)

Maximum number of values in some 'IN' clause that can be split up accross
several queries. In some cases, it might be necessary to limit the number of
values in the 'IN' clause due to MySQL configuration variable
'max_allowed_packet'.

=cut

our $DEBUG = 0;
our $ALL_PROPERTIES_CODE   = 'all';
our $ALL_PROPERTIES_REGEXP = '^(?:' . $ALL_PROPERTIES_CODE . '|\*)$';
our $KEY_SEPARATOR = ',';
our $MAX_IN_CLAUSE_ELEMENTS = 2000;
our $EXISTS_SELECTOR_REGEXP = '^\s*(?:NOT\s+)?EXISTS\s*$';



# Package subs
###############

=pod

=head1 STATIC METHODS

=head2 CONSTRUCTOR

B<Description>: Creates a new instance of database object.

B<ArgsCount>: 1-4

=over 4

=item $proto: (string) (R)

Object prototype or class.

=item $property_handlers: (hash ref) (U)

A hash structure containing the following keys:
'table', 'key', 'load', 'alias', 'base', 'secondary' and 'tertiary'.
Key content description:
-'table': the name of the table (string) the DBObject will refer to.
-'key': the name(s) of the database field(s) representing the key value of the
  table. It must be a way to identify a unique object.
  In the case of muktiple key fields, all the field names must be specified
  separated by comas. ex.: 'family_id,species'
-'alternate_keys': an array containing alternative key name in the order they
  should be checked. This parameter is used for caching.
-'load': an array of names of field to load by default when fetching the object
  for the first time.
-'alias': a hash where keys are "alias" names and values are the corresponding
  field as defined in the next hash 'base', 'secondary' and 'tertiary'.
-'base': a hash where keys are field names and values are hash describing the
  field in database: the key 'property_column' is used to get the name of the
  corresponding column in the database table; the key 'serialized' can be used
  in order to manage serialized data (stored into a BLOB) when set to a true
  value.
  Data to be serialized must be either a Greenphyl DBObject or a hash which keys
  are subfield names and values can be either a perl variable, a Greenphyl
  DBObject or an array of GreenPhyl (possibily mixed) DBObjects.
  DBObjects can be stored as hashes of values or as selectors to use to load the
  serialized objects. To use the later case, call object setSelectors() method.
  See FreezeData() and UnfreezeData() for details.
-'secondary': a hash where keys are field names and values are hash describing
 the how to get the field value from a secondary table in database:
   * 'object_key'      => object field name that must be used to identify the
     corresponding secondary property. If omitted, the default object 'key'
     value is used.
   * 'property_table'  => the name of the secondary table holding the secondary
     property.
   * 'property_key'    => the name of the field in that table that must contain
     the value from 'object_key'.
   * 'multiple_values' => boolean value telling if there can be more than one
      secondary value for this property.
   * 'property_column' or 'property_module' => if 'property_column' is specified
     the field value is kept as a scalar value and fetched from the given
     field name; if 'property_module' is specified, the value is loaded using
     the given associated object module name.
   * 'cached' => for cached data in a base column; Must look like:
     'serialized_cache_base_column_name:cache_hash_key_name_with_the_data'.
-'tertiary': a hash where keys are field names and values are hash describing
 the how to get the field value from a tertiary table in database. The "link
 table" refers to the secondary table that is used to create a link between the
 object table and the table of its tertiary property:
   * 'object_key'        => object field name that must be used to identify the
     corresponding link table entry. If omitted, the default object 'key'
     value is used.
   * 'link_table'        => the name of the table linking the object and its
     tertiary property.
   * 'link_object_key'   => the name of the field in link table corresponding
     to 'object_key'.
   * 'link_property_key' => the name of the field in link table corresponding
     to 'property_key'.
   * 'property_table'    => the name of the table holding the tertiary property.
     only necessary when using 'property_column' and 'multiple_values' set to 0.
   * 'property_key'      => the name of the field identifying corresponding
     tertiary properties.
   * 'multiple_values'   => boolean value telling if there can be more than one
      tertiary value for this property.
   * 'property_column' or 'property_module' => if 'property_column' is specified
     the field value is kept as a scalar value and fetched from the given
     field name; if 'property_module' is specified, the value is loaded using
     the given associated object module name.
   * 'cached' => for cached data in a base column; Must look like:
     'serialized_cache_base_column_name:cache_hash_key_name_with_the_data'.

Note: the script "generate_table_properties.pl" can be used to auto-generate the
$property_handlers structure.

Note: property names should be only contain lower case letters with underscores.

Note: when using secondary or tertiary peroperties, make sure the 'object_key'
is also present in the 'load' array.

=item $dbh: (DBI::db) (U)

Database handler of the database to load the object from. If no handler is
provided, a new object is created using the member values provided in the
parameters hash and the object has no link to any database.

=item $parameters: (hash ref) (O)

The hash can contain 4 keys:
-'selectors': either 1) a hash ref where keys are object (table) field names and
 values can be either a scalar value to compare database field to or an array
 containing as first element a MySQL comparator and as following elements the
 values to use for comparison (allows the use of 'LIKE' or 'IN' or
 'IN (SELECT ...)')
 or 2) an array ref alternating hash ref like in 1) or array ref like in 2) and
 a string containing a logic operator ('AND', 'OR', 'XOR').

 Note: selector keys are field name in database and not property names.
 Note: selector keys may contain table name as qualifier (ie. for a
 Greenphyl::Sequence, 'seq_id' equivalent to 'sequences.seq_id' and we could
 also use 'family.family_name' in the selectors if some requested properties
 involve the 'family' table).

 Here are some examples:

 * Selection query: family_id = 42
 Corresponding selector hash ref: {family_id => 42,}
 Other equivalent selector: {'family_id' => ['=', 42],}

 * Selection query: family_name LIKE '%trans%'
 Corresponding selector hash ref: {'family_id' => ['LIKE', '%trans%'],}

 * Selection query: family_id IN (42, 43, 44)
 Corresponding selector: {'family_id' => ['IN', 42, 43, 44],}

 * Selection query: description LIKE '%DAFT%' OR description LIKE '%DRTF%' OR description LIKE '%DATF%'
 Corresponding selector:
 [
     {'description' => '%DAFT%',},
     'OR',
     {'description' => '%DRTF%',},
     {'description' => '%DATF%',},
 ]
 Note: second 'OR' is implicit (when an operator is missing, the last one is
 reused)

 * Selection query: (description LIKE '%DAFT%' OR inference = 'IEA:TF') AND validated > 0
 Corresponding selector:
 [
     [
         {'description' => ['LIKE', '%DAFT%'],},
         'OR',
         {'inference' => 'IEA:TF'},
     ],
     'AND',
     {'validated' => ['>', 0]},
 ]

-'load': a hash containing the status of fields to load or not; keys are table
 field names and values can be set to 1 to load the field from database or 0
 to prevent the field from being loaded (nb. if it should be loaded by default.
 See $property_handlers 'load' field for details).
 If the 'load' value is the string 'all', all object base properties will be
 loaded. If the 'load' value is not a hash and is different from 'all' -for
 instance 'none'- no property will be loaded.
-'members': a hash containing default values for object properties. A row
 fetched from database as a hash structure could be used. It can be useful when
 objects are loaded from an external SQL query and should be instanciated as
 DBObjects.
-'sql': a hash containing some SQL options like:
 'ORDER BY': a scalar or an array of scalar for the 'ORDER BY' clause;
 'LIMIT': an integer corresponding to the limit of rows to return;
 'OFFSET': an integer corresponding to an offset in the result list;
 'DISTINCT': a boolean that enables or disables DISTINCT option.

=back

B<Return>: (Greenphyl::DBObject)

a single new instance in scalar context or a list of instances in list context.

B<Caller>: Database object modules.

B<Example>:

    package Greenphyl::SomeDBObject;
    use base qw(Greenphyl::DBObject);
    ...
    sub new
    {
        my $proto = shift();
        my $class = ref($proto) || $proto;
        
        my $object_properties = $class->GetDefaultClassProperties();

        return $class->SUPER::new($object_properties, @_);
    }
    ...
    return 1;

=cut

sub new
{
    my ($proto, $property_handlers, $dbh, $parameters) = @_;
    my $class = ref($proto) || $proto;

    # parameters check
    if (4 > @_)
    {
        confess "usage: my \$instance = Greenphyl::DBObject->new(property_handlers, dbh, parameters);";
    }
    # check dbh (must be undef or DBI::db)
    if ($dbh && (ref($dbh) ne 'DBI::db'))
    {
        confess "ERROR: invalid database handler!\nGot '" . (ref($dbh)?ref($dbh):$dbh) . "'.\n";
    }
    # check property handlers
    if (!defined($property_handlers))
    {
        $property_handlers = $class->GetDefaultClassProperties();
    }
    if (!defined($property_handlers))
    {
        confess "ERROR: no property handler provided!\n";
    }
    elsif (!ref($property_handlers) ||(ref($property_handlers) ne 'HASH'))
    {
        confess "ERROR: invalid property handler provided: it must be a hash ref!\n";
    }
    elsif (!$property_handlers->{'table'}
           || !$property_handlers->{'key'}
           || !$property_handlers->{'load'}
           || !$property_handlers->{'alias'}
           || !$property_handlers->{'base'}
           || !$property_handlers->{'secondary'}
           || !$property_handlers->{'tertiary'})
    {
        confess "ERROR: invalid property handler structure. Please check DBObject constructor documentation for details.\n";
    }

    $parameters = _ValidateParameters($parameters);

    my $selectors = $parameters->{'selectors'} || {};
    my $requested_properties = $parameters->{'load'} || {};
    my $member_values = $parameters->{'members'} || {};
    my $sql_options = $parameters->{'sql'} || {};

    # instance creation
    my $self = {};
    # transfer provided member values
    %$self = %$member_values;
    $self->{'_dbh'} = $dbh;
    $self->{'_properties'} = $property_handlers;
    $self->{'_fetch'} = {};
    $self->{'_type'} = $class;
    $self->{'_type'} =~ s/^Greenphyl:://;
    $self->{'_modified'} = {};
    # Keep track of provided values and check for serialized data.
    foreach my $provided_member (keys(%$member_values))
    {
        if (exists($property_handlers->{'base'}->{$provided_member}))
        {
            if ($property_handlers->{'base'}->{$provided_member}->{'serialized'}
                && !ref($member_values->{$provided_member}))
            {
                # Unserialize.
                $self->{$provided_member} =
                $member_values->{$provided_member} =
                  UnfreezeData($member_values->{$provided_member});
            }
            $self->{'_modified'}->{$provided_member} = $member_values->{$provided_member};
        }
    }
    bless($self, $class);
    my @new_instances = ($self);
    # check if the object should be loaded from database
    if ($dbh
        && (ref($requested_properties)
            || ($requested_properties =~ m/$ALL_PROPERTIES_REGEXP/io))) # handle 'none' case because it won't match the regexp
    {
        # list properties to load
        my $properties_to_load = $self->_getPropertiesToLoad($requested_properties);

        # qualify elements (add table alias and name columns)
        my ($qualified_selectors, $qualified_properties, $tables) = $self->_qualifyQueryElements($selectors, $properties_to_load, 1);

        # prepare table clause
        my $table_clause = $self->_prepareTableClause($tables);

        # load properties from database
        @new_instances = $self->_fetchProperties($qualified_selectors, $qualified_properties, $table_clause, 0, $sql_options);

        # clear modified flags for loaded values
        # PrintDebug("Clearing loaded properties...");
        foreach my $instance (@new_instances)
        {
            # PrintDebug("...for $instance");
            if ($instance)
            {
                foreach my $loaded_property (@$properties_to_load)
                {
                    # PrintDebug("Clearing modified status of property '$loaded_property'.");
                    delete($instance->{'_modified'}->{$loaded_property});
                }
            }
        }
    }
    else
    {
        # properties provided or no DB handler, no DB loading
        $self->_initKeyValue();
    }

    # check calling context
    if (wantarray())
    {
        # if first object is not initialized (no match), clear the list
        if (@new_instances && !$new_instances[0])
        {
            PrintDebug('No valid instance found in database, clear result list.');
            @new_instances = ();
        }
        # array of instances
        return @new_instances;
    }
    else
    {
        # just want one object reference (scalar context)
        return $self;
    }
}


=pod

=head2 _ValidateParameters

B<Description>: raise an exception if class parameters are not correct.

B<ArgsCount>: 1

=over 4

=item $parameters: (hash) (R)

the parameters to check.

=back

B<Return>: (hash ref)

validated parameters.

=cut

sub _ValidateParameters
{
    my ($parameters) = @_;
    $parameters ||= {};
    if ('HASH' ne ref($parameters))
    {
        confess "ERROR: invalid parameters! The parameter after database handlher must be a hash ref.\n";
    }
    foreach my $parameter_key (keys(%$parameters))
    {
        if ($parameter_key !~ m/^(?:selectors|load|members|sql)$/)
        {
            confess "ERROR: invalid class parameters key '$parameter_key'!\n";
        }
    }

    return $parameters;
}


=pod

=head2 GetDefaultClassProperties

B<Description>: returns the default class properties.

B<ArgsCount>: 1

=over 4

=item $class: (string) (R)

the object class. An object instance can also be used.

=back

B<Return>: (hash ref)

a property hash.

B<Caller>: general

=cut

sub GetDefaultClassProperties
{
    my ($class) = @_;

    if (ref($class))
    {
        $class = ref($class);
    }

    my $property_hash;
    eval '$property_hash = $' . $class . '::OBJECT_PROPERTIES;';
    my $parent_class = $class;
    while (!$@
        && (!defined($property_hash) || (ref($property_hash) ne 'HASH'))
        && $parent_class
        )
    {
        # Try with parent class.
        no strict "refs";
        ($parent_class) = @{$parent_class . '::ISA'};
        use strict "refs";
        eval '$property_hash = $' . $parent_class . '::OBJECT_PROPERTIES;';
    }

    if ($@ || !defined($property_hash) || (ref($property_hash) ne 'HASH'))
    {
        confess "ERROR: failed to retrieve object property hash. Maybe the class '$class' doesn't use the name '\$OBJECT_PROPERTIES' for its property hash?\nError details: " . $@ . "\n";
    }
    return $property_hash;

}


=pod

=head2 GetDefaultMembers

B<Description>: returns the list of default members loaded.

B<ArgsCount>: 1-3

=over 4

=item $class: (string) (R)

the object class. An object instance can also be used.

=item $qualifier: (string) (U)

a qualifier name to use for object table. If specified, members are returned
using the format: $qualifier . '.member AS "member"'
Default: undef

=item $prefix: (string) (O)

A prefix to use to prefix each field alias.
Default: no prefix.

=back

B<Return>: (array)

an array of string representing the member names. These members come from the
property hash 'load' value.

B<Caller>: general

=cut

sub GetDefaultMembers
{
    my ($class, $qualifier, $prefix) = @_;

    if (ref($class))
    {
        $class = ref($class);
    }

    $prefix ||= '';
    my $property_hash = $class->GetDefaultClassProperties();
    if ($qualifier)
    {
        return map { $qualifier . qq|.$_ AS "$prefix$_"| } @{$property_hash->{'load'}};
    }
    else
    {
        return @{$property_hash->{'load'}};
    }
}


=pod

=head2 ExtractMembersFromHash

B<Description>: returns a hash containing only the keys having the given 
prefix and remove that prefix from the new keys.

B<ArgsCount>: 2

=over 4

=item $hash_object: (hash ref) (R)

the object stored in a hash.

=item $prefix: (string) (R)

The object member prefix used.

=back

B<Return>: (hash ref)

The object hash containing only related members.

B<Caller>: general

B<Example>:


    my $sql_query = "
        SELECT DISTINCT " . join(', ', Greenphyl::Sequence->GetDefaultMembers('s', 'sequence_')) . ",
                        s.annotation AS \"sequence_annotation\",
                        " . join(', ', Greenphyl::DBXRef->GetDefaultMembers('d', 'dbxref_')) . "
        FROM dbxref d
            LEFT JOIN dbxref_synonyms ds ON (ds.dbxref_id = d.id)
            JOIN dbxref_sequences sd ON (sd.dbxref_id = d.id)
            JOIN sequences s ON (s.id = sd.sequence_id)
        WHERE d.db_id = " . $db->id . ";";
    # here, without the prefix, we would have name collision between accession and/or id
    my $sequences_and_dbxrefs = $g_dbh->selectall_arrayref($sql_query, { 'Slice' => {} });
    foreach my $sequence_and_dbxref_hash (@$sequences_and_dbxrefs)
    {
        my $sequence_hash = Greenphyl::Sequence->ExtractMembersFromHash($sequence_and_dbxref_hash, 'sequence_');
        my $sequence = Greenphyl::Sequence->new($g_dbh, {'load' => 'none', 'members' => $sequence_hash});
        my $dbxref_hash = Greenphyl::DBXRef->ExtractMembersFromHash($sequence_and_dbxref_hash, 'dbxref_');
        my $dbxref = Greenphyl::DBXRef->new($g_dbh, {'load' => 'none', 'members' => $dbxref_hash});
    }

=cut

sub ExtractMembersFromHash
{
    my ($class, $object, $prefix) = @_;
    if (!$object)
    {
        confess "ERROR: Missing object parameter for ExtractMembersFromHash!\n";
    }
    elsif ('HASH' ne ref($object))
    {
        confess "ERROR: Invalid object parameter for ExtractMembersFromHash ($object)!\n";
    }
    my $property_hash = $class->GetDefaultClassProperties();
    $prefix ||= '_' . $property_hash->{'table'};
    
    my $new_hash = {};
    while (my ($candidate_member, $value) = each(%$object))
    {
        if ($candidate_member =~ m/^$prefix(.*)$/)
        {
            $new_hash->{$1} = $value;
        }
    }
    return $new_hash;
}


=pod

=head2 Count

B<Description>: returns the count of corresponding objects without loading them.
Supports non-static calls.

B<ArgsCount>: 1-3

=over 4

=item $class: (string) (R)

the object class.

=item $dbh: (DBI::db) (U)

See CONSTRUCTOR documentation. If not provided and call is non-static, the
object' dbh will be used.

=item $parameters: (hash ref) (O)

See CONSTRUCTOR documentation.

=back

B<Return>: (integer)

Number of objects.

B<Caller>: general

=cut

sub Count
{
    my ($proto, $dbh, $parameters) = @_;

    if (!$dbh ||(ref($dbh) ne 'DBI::db'))
    {
        if (ref($proto) && $proto->{'_dbh'})
        {
            $dbh = $proto->{'_dbh'};
        }
        else
        {
            confess "ERROR: Count static method called without a valid database object!\n";
        }
    }

    $parameters = _ValidateParameters($parameters);

    my $selectors = $parameters->{'selectors'} || {};
    my $sql_options = $parameters->{'sql'} || {};

    if (!ref($proto))
    {
        my $class = $proto;
        $proto = {};
        # create a fake object in order to call protected membres
        $proto->{'_dbh'}        = $dbh;
        $proto->{'_properties'} = GetDefaultClassProperties($class);
        bless($proto, $class);
    }

    my $requested_properties = $parameters->{'load'} || {};

    # list properties to load
    my $properties_to_load = $proto->_getPropertiesToLoad($requested_properties);

    # qualify selectors
    my ($qualified_selectors, $qualified_properties, $tables) = $proto->_qualifyQueryElements($selectors, ['COUNT', @$properties_to_load], 1);

    # we only want the count, remove other columns
    $qualified_properties = [$qualified_properties->[0]];

    # prepare table clause
    my $table_clause = $proto->_prepareTableClause($tables);

    # make sure SQL options are valid
    $sql_options = $proto->_initSQLOptions($sql_options);

    # load values from database...
    # prepare space holders
    my ($space_holders, $bind_values) = $proto->_prepareSelectorsForQuery($selectors);

    my $sql_query = $proto->_PrepareSQLQuery($qualified_properties, $table_clause, $space_holders, $sql_options);
    if ($sql_options->{'LIMIT'})
    {
        # LIMIT won't affect the COUNT result but rather the number of COUNT
        # result that will be output. Therefore, this might be a missuse offset
        # LIMIT and the developper should be warned.
        PrintDebug("WARNING: 'LIMIT' option used in a COUNT query! It has been ignored.");
    }

    if ($sql_options->{'OFFSET'})
    {
        $sql_query =~ s/\sOFFSET\s+\d+;$//;
        PrintDebug("WARNING: 'OFFSET' option used in a COUNT query! It has been ignored.");
    }

    PrintDebug("SQL Query: $sql_query\nSQL Bindings (" . scalar(@$bind_values) . " values)" . (@$bind_values? ': (' . join(', ', @$bind_values) . ')':''));

    my ($count) = $dbh->selectrow_array($sql_query, undef, @$bind_values)
        or confess "ERROR: in query:\n$sql_query\nWith bindings: " . join(', ', @$bind_values) . "\n" . ($dbh->errstr || '');

    return $count;

}


=pod

=head1 METHODS

=head2 getIntValue

B<Description>: convert object to an integer value. Usually, uses the object
table main key.

B<ArgsCount>: 1

=over 4

=item $self: (DBObject)

the object

=back

B<Return>: (integer)

the key value as an integer value.

=cut

sub getIntValue
{
    my ($self) = @_;
    return int($self->{$self->{'_properties'}->{'key'}});
}
# allow children override
sub _getIntValue
{
    my ($self) = shift();
    return $self->getIntValue(@_);
}

=pod

=head2 getStringValue

B<Description>: convert object into a string. Usually, uses the object
table main key.

B<ArgsCount>: 1

=over 4

=item $self: (DBObject)

the object

=back

B<Return>: (string)

the key value as a string value.

=cut

sub getStringValue
{
    my ($self) = @_;
    return $self->{$self->{'_properties'}->{'key'}} || '';
}
# allow children override
sub _getStringValue
{
    my ($self) = shift();
    return $self->getStringValue(@_);
}

=pod

=head2 getBooleanValue

B<Description>: tells if an object is not empty.

B<ArgsCount>: 1

=over 4

=item $self: (DBObject)

the object.

=back

B<Return>: (boolean)

true if the object is instanciated (ie. has a valid key set), false otherwise.

=cut

sub getBooleanValue
{
    my ($self) = @_;
    return $self->{$self->{'_properties'}->{'key'}};
}
# allow children override
sub _getBooleanValue
{
    my ($self) = shift();
    return $self->getBooleanValue(@_);
}


=pod

=head2 getHashValue

B<Description>: returns the object as a hash. The hash can contain complex
members and sub-members (etc.) structures but none are objects.

B<ArgsCount>: 1-2

=over 4

=item $self: (DBObject)

the object.

=item $remove_subobject: (bool)

If set, (sub-)members are not converted into hashes and removed.

=back

B<Return>: (boolean)

a hash ref.

=cut

sub getHashValue
{
    my ($self, $remove_subobject) = @_;
    my $hash_object = {};
    
    # Recursive method to "flatten" all members and replace sub-objects by
    # hashes or strings.
    sub _hashSubMembers
    {
      my ($member) = @_;
      my $non_object_member;
      if (ref($member) eq 'ARRAY')
      {
          $non_object_member = [];
          foreach my $sub_member (@$member)
          {
            push(@$non_object_member, _hashSubMembers($sub_member));
          }
      }
      elsif (ref($member) eq 'HASH')
      {
          $non_object_member = {};
          foreach my $sub_member_key (keys(%$member))
          {
            my $sub_member = $member->{$sub_member_key};
            $non_object_member->{$sub_member_key} = _hashSubMembers($sub_member);
          }
      }
      elsif (ref($member))
      {
          if ($member->can('getHashValue'))
          {
              $non_object_member = $member->getHashValue();
          }
          else
          {
              $non_object_member = ''.$member;
          }
      }
      else
      {
          $non_object_member = $member;
      }

      return $non_object_member;
    }

    foreach my $property (keys(%{$self->{'_properties'}->{'base'}}))
    {
        if (exists($self->{$property}))
        {
            if (!($remove_subobject && UNIVERSAL::can($self->{$property}, 'isa')))
            {
                $hash_object->{$property} = _hashSubMembers($self->{$property});
            }
        }
    }
    #+FIXME: add secondary and tertiary properties?
    # how to take in account object cross-reference loops?
    # only non-object secondary/tertiary members?
    # foreach my $property (keys(%{$self->{'_properties'}->{'secondary'}}))
    # {
    #     ...
    # }
    return $hash_object;
}
# allow children override
sub _getHashValue
{
    my ($self) = shift();
    return $self->getHashValue(@_);
}

=pod

=head2 isEqual

B<Description>: tells if 2 objects are equal.

B<ArgsCount>: 1-3

=over 4

=item $self: (DBObject) (R)

the object.

=item $second: (any) (U)

a second operand to compare the object to.

=$swapped: (boolean) (O)

if set to true, the comparison was "second eq first" otherwise it was
"first eq second".

=back

B<Return>: (boolean)

true if the objects have the same key value from the same database.
If second operand is not an object, true if the key value corresponds to the
second operand.

=cut

sub isEqual
{
    my ($self, $second, $swapped) = @_;
    if (!defined($second))
    {
        # no second operand!
        return 0;
    }

    # second operand is an object?
    if (ref($second))
    {
        # different types?
        if (!$second->isa(ref($self)))
        {
            return 0;
        }
        # same database?
        if ($self->{'_dbh'} != $second->{'_dbh'})
        {
            return 0;
        }
    }
    return (''.$self->{$self->{'_properties'}->{'key'}} eq ''.$second->{$second->{'_properties'}->{'key'}});
}
# allow children override
sub _isEqual
{
    my ($self) = shift();
    return $self->isEqual(@_);
}

=pod

=head2 isNotEqual

B<Description>: tells if 2 objects are different.

B<ArgsCount>: 1-3

=over 4

=item $self: (DBObject) (R)

the object.

=item $second: (any) (U)

a second operand to compare the object to.

=$swapped: (boolean) (O)

if set to true, the comparison was "second ne first" otherwise it was
"first ne second".

=back

B<Return>: (boolean)

true if the objects have different key values or come from different database.
If second operand is not an object, true if the key value does not correspond
to the second operand.

=cut

sub isNotEqual
{
    my ($self, $second, $swapped) = @_;
    return !$self->isEqual($second, $swapped);
}
# allow children override
sub _isNotEqual
{
    my ($self) = shift();
    return $self->isNotEqual(@_);
}


=pod

=head2 getSelectors

B<Description>: returns the selectors used to load the object.

B<ArgsCount>: 1

=over 4

=item $self: (DBObject) (R)

the object.

=back

B<Return>: (hash)

A selector hash.

=cut

sub getSelectors
{
    my ($self) = @_;
    
    return $self->{'_selectors'};
}


=pod

=head2 setSelectors

B<Description>: sets the selectors used to load the object.

B<ArgsCount>: 2

=over 4

=item $self: (DBObject) (R)

the object.

=item $selectors: (hash ref) (R)

The selectors hash.

=back

B<Return>: (nothing)

=cut

sub setSelectors
{
    my ($self, $selectors) = @_;
    
    if (!$selectors)
    {
        confess "ERROR: setSelectors: missing selectors hash ref!\n";
    }
    if (ref($selectors) ne 'HASH')
    {
        confess "ERROR: setSelectors: selectors parameter is not a hash ref!\n";
    }

    $self->{'_selectors'} = $selectors;
}


=pod

=head2 getObjectType

B<Description>: returns the type of an object.

B<ArgsCount>: 1-2

=over 4

=item $self: (DBObject) (R)

the object.

=item $lower_case: (boolean) (O)

Returns the type in lower case convention (ie. with lower case characters and
underscores only). ex.: 'CustomFamily' becomes 'custom_family'.

=back

B<Return>: (string)

Returns the type of the object which should be the class name without the prefix
"Greenphyl::".

=cut

sub getObjectType
{
    my ($self, $lower_case) = @_;
    
    my $type = $self->{'_type'};

    if ($lower_case)
    {
        # change naming convention (ie. CustomFamily --> custom_family)
        $type =~ s/([a-z])([A-Z])/$1_$2/g;
        $type = lc($type);
    }
    
    return $type;
}


=pod

=head2 getDatabaseHandler

B<Description>: returns the object database.

B<ArgsCount>: 1

=over 4

=item $self: (DBObject) (R)

the object.

=back

B<Return>: (DBI::db)

Returns object database handler if one or undef.

=cut

sub getDatabaseHandler
{
    my ($self) = @_;
    return $self->{'_dbh'};
}


=pod

=head2 setDatabaseHandler

B<Description>: change the object database.

B<ArgsCount>: 2

=over 4

=item $self: (DBObject) (R)

the object.

=item $dbh: (DBI::db) (R)

the new database handler.

=back

B<Return>: nothing

=cut

sub setDatabaseHandler
{
    my ($self, $dbh) = @_;
    $self->{'_dbh'} = $dbh;
}


=pod

=head2 _initKeyValue

B<Description>: initialize the object key value (requiered for several features
like cache). This methods support multiple column key in a coma-separated string
as key property.
We assume here that in the case of a multiple-column key, each property is
available.

B<ArgsCount>: 1

=over 4

=item $self: (DBObject) (R)

the object.

=back

B<Return>: (DBObject)

the object.

B<Caller>: internal

=cut

sub _initKeyValue
{
    my ($self) = @_;

    my $key_name = $self->{'_properties'}->{'key'};
    # check for multiple-column key
    if (($key_name =~ m/$KEY_SEPARATOR/o) && $self->{'_dbh'})
    {
        my $key_missing = 0;
        foreach my $key_subname (split(/\s*$KEY_SEPARATOR\s*/o, $key_name))
        {
            if (!exists($self->{$key_subname}) || !defined($self->{$key_subname}))
            {
                $key_missing = 1;
            }
        }
        if (!$key_missing)
        {
            # see TRICKS pod section for details
            # if there's a warning about undefined string concatenation, it means
            # a column of the key has not been initialized correctly (it may come
            # from a mistake in the object property hash).
            $self->{$key_name} = join($KEY_SEPARATOR, @$self{split(/\s*$KEY_SEPARATOR\s*/o, $key_name)});
        }
    }

    if (!defined($self->{$key_name}))
    {
        # object not instanciated from database, create a temporary identifier
        $self->{$key_name} = '';
    }

    return $self;
}


=pod

=head2 getDBKeyValue

B<Description>: returns the object key value in database.

B<ArgsCount>: 1

=over 4

=item $self: (DBObject) (R)

the object.

=back

B<Return>: (string)

the object key value or undef if not set or if the object does not belong to a
database.

B<Caller>: general

=cut

sub getDBKeyValue
{
    my ($self) = @_;

    my $key_name = $self->{'_properties'}->{'key'};

    if ($self->{'_dbh'})
    {
        if ($self->{$key_name})
        {
            return $self->{$key_name};
        }
        else
        {
            return $self->_initKeyValue()->{$key_name};
        }
    }

    return undef;
}


=pod

=head2 _initSQLOptions

B<Description>: returns an initialized SQL options hash.

B<ArgsCount>: 2

=over 4

=item $self: (DBObject) (R)

the object.

=item $sql_options: (hash) (R)

.

=back

B<Return>: (hash ref)

the initialized SQL options hash.

B<Caller>: internal

=cut

sub _initSQLOptions
{
    my ($self, $sql_options) = @_;
    my $new_sql_options = {};

    $sql_options ||= {};
    if (!ref($sql_options))
    {
        confess "ERROR: invalid SQL options '$sql_options' (not a hash ref)!\n";
    }

    # check SQL options...
    # -DISTINCT
    if ($sql_options->{'DISTINCT'})
    {
        $new_sql_options->{'DISTINCT'} = $sql_options->{'DISTINCT'};
    }
    # -LIMIT
    if (exists($sql_options->{'LIMIT'}))
    {
        $new_sql_options->{'LIMIT'} = int($sql_options->{'LIMIT'});
    }

    # -OFFSET
    if (exists($sql_options->{'OFFSET'}))
    {
        $new_sql_options->{'OFFSET'} = int($sql_options->{'OFFSET'});
        # if an offset is set, limit must also exist
        $new_sql_options->{'LIMIT'} ||= 18446744073709551615; # set to no limit
    }

    # -ORDER BY
    if (exists($sql_options->{'ORDER BY'}))
    {
        if (!ref($sql_options->{'ORDER BY'}))
        {
            $new_sql_options->{'ORDER BY'} = [$sql_options->{'ORDER BY'}];
        }
        else
        {
            $new_sql_options->{'ORDER BY'} = $sql_options->{'ORDER BY'};
        }

        if ('ARRAY' ne ref($new_sql_options->{'ORDER BY'}))
        {
            confess "ERROR: invalid SQL options: 'ORDER BY' value is not an array!\n";
        }

        foreach (@{$new_sql_options->{'ORDER BY'}})
        {
            # make sure we got a valid ORDER BY clause
            if ($_ !~ m/\s*
                        (?:BINARY\s+)?
                        (?:\w+\.)?   # column may be qualified by a table name + dot
                        \w+          # column name
                        \s*(?:\s(?:ASC|DESC)\s*)? # may be followed by ASC or DESC
                        /ix)
            {
                confess "ERROR: invalid SQL options: 'ORDER BY' value contains invalid clause: '$_'!\n";
            }
        }
    }

    return $new_sql_options;
}


=pod

=head2 _getPropertiesToLoad

B<Description>: returns the array of the names of properties to load according
to default object property loading and requested properties parameter.

B<ArgsCount>: 2

=over 4

=item $self: (DBObject) (R)

the object.

=item $requested_properties: (hash or string) (R)

either a hash which keys are property names and values are boolean values
telling if the property should be loaded (1) or not (0) or a string
containing the key-word 'all' or '*' to load all the base properties.
If a string different from 'all' or '*' is given, only the key property will be
loaded.

=back

B<Return>: (array ref)

the array of properties to load.

B<Caller>: internal

=cut

sub _getPropertiesToLoad
{
    my ($self, $requested_properties) = @_;

    if (ref($requested_properties) eq 'HASH')
    {
        # start from default properties to load
        my %properties_to_load = map { $_ => 1 } @{$self->{'_properties'}->{'load'}};

        # add/remove what's requested
        foreach (keys(%$requested_properties))
        {
            if ($requested_properties->{$_})
            {
                $properties_to_load{$_} = 1;
            }
            else
            {
                delete($properties_to_load{$_});
            }
        }

        return [keys(%properties_to_load)];
    }
    elsif ($requested_properties =~ m/$ALL_PROPERTIES_REGEXP/io)
    {
        # load all base properties
        return [keys(%{$self->{'_properties'}->{'base'}})];
    }
    else
    {
        # load nothing except key
        return [$self->{'_properties'}->{'key'}];
    }
}


=pod

=head2 _qualifySelectors

B<Description>: Add column alias to selectors.

B<ArgsCount>: 3

=over 4

=item $self: (DBObject) (R)

the object.

=item $selectors: (array/hash ref) (R)

See constructor "$parameters" documentation ('selectors' section).

=item $table_aliases: (hash ref) (R)

a hash which keys are table names and values are last used alias.

=back

B<Return>: (hash ref)

Qualifier selectors.

B<Caller>: internal

=cut

sub _qualifySelectors
{
    my ($self, $selectors, $table_aliases) = @_;

    my $new_selectors;
    
    # qualify selectors to avoid potential ambiguity using base table alias
    # as all selections are applied to base table

    # check if we got a list of conditions joined with operators or
    # a hash of 'and' conditions
    if ('HASH' eq ref($selectors))
    {
        $new_selectors = {%$selectors};
        # hash of 'and' conditions
        foreach my $selector (keys(%$new_selectors))
        {
            # already qualified?
            if ($selector =~ m/^(\w+)\.\w+/)
            {
                # try to replace qualified name by alias
                if (exists($table_aliases->{$1}) && $table_aliases->{$1})
                {
                    my $table_alias = $table_aliases->{$1};
                    my $new_selector = $selector;
                    $new_selector =~ s/^(\w+)\.//; # remove previous qualifier
                    $new_selector = $table_alias . '.' . $new_selector;
                    $new_selectors->{$new_selector} = delete($new_selectors->{$selector});
                }
            }
            elsif ($selector !~ m/$EXISTS_SELECTOR_REGEXP/io)
            {
                $new_selectors->{$self->{'_properties'}->{'table'} . '.' . $selector} = delete($new_selectors->{$selector});
            }
        }
    }
    elsif ('ARRAY' eq ref($selectors))
    {
        $new_selectors = [];

        # a list of joined conditions
        foreach my $condition (@$selectors)
        {
            # check for sub-selectors
            if (ref($condition))
            {
                $condition = $self->_qualifySelectors($condition, $table_aliases);
            }
            # else we got operators: skip them
            push(@$new_selectors, $condition);
        }
    }
    else
    {
        confess "ERROR: _qualifySelectors: Invalid selector structure! Found '$selectors' where a hash ref or an array ref was expected!\n";
    }

    return $new_selectors;
}


=pod

=head2 _qualifyQueryElements

B<Description>: Add table alias and column alias to columns that should be
returned.

B<ArgsCount>: 3-4

=over 4

=item $self: (DBObject) (R)

the object.

=item $selectors: (hash ref) (R)

See constructor "$parameters" documentation ('selectors' section).

=item $properties: (array ref) (R)

the array of properties to load from database. These properties will be
converted into column names.

=item $must_return_single_value: (boolean) (O)

if set to 1, the procedure will stop if it appears that the query will return
more than one element corresponding to the same object. This is used at
instanciation time to make sure we only get one object from the query.

=back

B<Return>: (list)

A list of 3 elements.
First element is a selector structur (hash) with qualified field names as keys.
Second element is an array of qualified columns to return (with table alias and
column alias).
Third element is an array of qualified tables to use with join statements and
aliases.

B<Caller>: internal

=cut

sub _qualifyQueryElements
{
    my ($self, $selectors, $properties, $must_return_single_value) = @_;

    my $main_table = $self->{'_properties'}->{'table'};
    my $main_table_alias = $self->{'_properties'}->{'table'};
    my @tables = ({'qualified_name' => "$main_table $main_table_alias", 'alias' => $main_table_alias, 'join_clause' => undef});

    # records only last aliases if several aliases exist for a same table
    my $table_aliases = {$main_table => $main_table_alias};

    # make sure we got some properties
    if (!@$properties)
    {
        # load default properties
        PrintDebug("No property requested but we need something, loading default properties");
        $properties = [@{$self->{'_properties'}->{'load'}}];
    }

    my (@qualified_properties, %secondary_properties, %tertiary_properties);
    # see what has been requested
    foreach my $property_name (@$properties)
    {
        # check for an alias
        if (exists($self->{'_properties'}->{'alias'}->{$property_name}))
        {
            # replace alias
            $property_name = $self->{'_properties'}->{'alias'}->{$property_name};
        }

        if ('COUNT' eq $property_name)
        {
            # just a count, not a property
            push(@qualified_properties, qq|COUNT(1) AS "COUNT"|);
        }
        elsif (exists($self->{'_properties'}->{'base'}->{$property_name}))
        {
            # as base, use main table
            my $column_name = $self->{'_properties'}->{'base'}->{$property_name}->{'property_column'};
            if ($column_name =~ m/^\w+$/)
            {
                push(@qualified_properties, qq|$main_table_alias.$column_name AS "$property_name"|);
            }
            else
            {
                push(@qualified_properties, qq|$column_name AS "$property_name"|);
            }
        }
        elsif (exists($self->{'_properties'}->{'secondary'}->{$property_name}))
        {
            # as secondary
            # make sure there's a single property as a column
            if ($must_return_single_value)
            {
                #+Val: removed. That might not be a problem... otherwise, maybe use GROUP BY clause?
                # if ($self->{'_properties'}->{'secondary'}->{$property_name}->{'multiple_values'})
                # {
                #     confess "ERROR: unable to instanciate a single object from multiple columns! Try to load additional members using separate queries.\n";
                # }
                # els
                if (!$self->{'_properties'}->{'secondary'}->{$property_name}->{'property_column'})
                {
                    confess "ERROR: unable to instanciate several objects from a single query! Try to load sub-objects using separate calls.\n";
                }
            }

            my $column_name = $self->{'_properties'}->{'secondary'}->{$property_name}->{'property_column'};
            # check if we already got the join
            my $secondary_table = $self->{'_properties'}->{'secondary'}->{$property_name}->{'property_table'};
            my $object_key = $self->{'_properties'}->{'secondary'}->{$property_name}->{'object_key'}
                             || $self->{'_properties'}->{'key'};
            my $property_key = $self->{'_properties'}->{'secondary'}->{$property_name}->{'property_key'};
            my $on_clause =  "$object_key=$property_key";
            my $table_alias;
            my $join_needed = 1;
            # check if we may have already assigned an alias to that table
            if (exists($secondary_properties{$secondary_table}) && %{$secondary_properties{$secondary_table}})
            {
                # yes, see if it was on the same ON clause
                $table_alias = $secondary_properties{$secondary_table}->{$on_clause};
                if (!$table_alias)
                {
                    # no, not on the same ON clause, we have a different JOIN clause
                    # use a new alias
                    $table_alias = $secondary_properties{$secondary_table}->{$on_clause} = $secondary_table . '_' . (1+scalar(keys(%{$secondary_properties{$secondary_table}})));
                }
                else
                {
                    # we already have the necessary join statement
                    $join_needed = 0;
                }
            }
            else
            {
                # no aliased yet, use table name as first alias
                $secondary_properties{$secondary_table} = {};
                $table_alias = $secondary_properties{$secondary_table}->{$on_clause} = $secondary_table;
            }
            
            if ($join_needed)
            {
                my $secondary_table_qualified_name = $secondary_table . ' ' . $table_alias;
                if ($object_key =~ m/^\w+$/)
                {
                    push(@tables,
                        {
                            'qualified_name' => $secondary_table_qualified_name,
                            'alias' => $table_alias,
                            'join_clause' => "JOIN $secondary_table_qualified_name ON $main_table_alias.$object_key = $table_alias.$property_key",
                        },
                    );
                }
                else
                {
                    push(@tables,
                        {
                            'qualified_name' => $secondary_table_qualified_name,
                            'alias' => $table_alias,
                            'join_clause' => "JOIN $secondary_table_qualified_name ON $object_key = $table_alias.$property_key",
                        },
                    );
                }
            }
            my $qualified_column_name = '';
            # check for special column name
            if ($column_name =~ m/^\w+\(\w\)$/)
            {
                # operations like "SUM()"
                $qualified_column_name = $column_name;
                $qualified_column_name =~ s/^(\w+)\((w+)\)$/$1($table_alias.$2)/;
            }
            elsif ($column_name =~ m/^\w+$/)
            {
                # just a simple column name
                $qualified_column_name = "$table_alias.$column_name";
            }
            else
            {
                # column name contains unsupported instructions
                $qualified_column_name = $column_name;
                # warn
                cluck "WARNING: unsupported column name format '$column_name' for object $self (" . ref($self) . "). Column name has been used \"as is\" without qualifier!\n";
            }

            push(@qualified_properties, qq|$qualified_column_name AS "$property_name"|);
            $table_aliases->{$secondary_table} = $table_alias;
        }
        elsif (exists($self->{'_properties'}->{'tertiary'}->{$property_name}))
        {
            # as tertiary
            # make sure there's a single property as a column
            if ($must_return_single_value)
            {
                if ($self->{'_properties'}->{'tertiary'}->{$property_name}->{'multiple_values'})
                {
                    confess "ERROR: unable to instanciate a single object from multiple columns! Try to load additional members using separate queries.\n";
                }
                elsif (!$self->{'_properties'}->{'tertiary'}->{$property_name}->{'property_column'})
                {
                    confess "ERROR: unable to instanciate several objects from a single query! Try to load sub-objects using separate calls.\n";
                }
            }

            # get property column name if specified or use property key as column name
            my $column_name = $self->{'_properties'}->{'tertiary'}->{$property_name}->{'property_column'}
                              || $self->{'_properties'}->{'tertiary'}->{$property_name}->{'property_key'};
            # get link table
            my $link_table = $self->{'_properties'}->{'tertiary'}->{$property_name}->{'link_table'};
            # get object key (in object table)
            my $object_key = $self->{'_properties'}->{'tertiary'}->{$property_name}->{'object_key'}
                                  || $self->{'_properties'}->{'key'};
            # get link object key (link table)
            my $link_object_key = $self->{'_properties'}->{'tertiary'}->{$property_name}->{'link_object_key'};
            # prepare clause for link table
            my $first_on_clause =  "$object_key=$link_object_key";
            my $link_table_alias;
            my $join_needed = 1;
            # check if we may have already assigned an alias to that table
            if (exists($secondary_properties{$link_table}) && %{$secondary_properties{$link_table}})
            {
                # yes, see if it was on the same ON clause
                $link_table_alias = $secondary_properties{$link_table}->{$first_on_clause};
                if (!$link_table_alias)
                {
                    # no, not on the same ON clause, we have a different JOIN clause
                    # use a new alias
                    $link_table_alias = $secondary_properties{$link_table}->{$first_on_clause} = $link_table . '_' . (1+scalar(keys(%{$secondary_properties{$link_table}})));
                }
                else
                {
                    # we already have the necessary join statement
                    $join_needed = 0;
                }
            }
            else
            {
                # no aliased yet, use table name as first alias
                $secondary_properties{$link_table} = {};
                $link_table_alias = $secondary_properties{$link_table}->{$first_on_clause} = $link_table;
            }

            if ($join_needed)
            {
                my $link_table_qualified_name = $link_table . ' ' . $link_table_alias;
                push(@tables,
                    {
                        'qualified_name' => $link_table_qualified_name,
                        'alias' => $link_table_alias,
                        'join_clause' => "JOIN $link_table_qualified_name ON $main_table_alias.$object_key = $link_table_alias.$link_object_key",
                    },
                );
            }

            # get property key
            my $link_property_key = $self->{'_properties'}->{'tertiary'}->{$property_name}->{'link_property_key'};
            my $tertiary_table = $self->{'_properties'}->{'tertiary'}->{$property_name}->{'property_table'};
            my $property_key = $self->{'_properties'}->{'tertiary'}->{$property_name}->{'property_key'};
            my $second_on_clause =  "$link_property_key=$property_key";
            my $tertiary_table_alias;
            $join_needed = 1;
            # check if we may have already assigned an alias to that table
            if (exists($tertiary_properties{$tertiary_table}) && %{$tertiary_properties{$tertiary_table}})
            {
                # yes, see if it was on the same ON clause
                $tertiary_table_alias = $tertiary_properties{$tertiary_table}->{$second_on_clause};
                if (!$tertiary_table_alias)
                {
                    # no, not on the same ON clause, we have a different JOIN clause
                    # use a new alias
                    $tertiary_table_alias = $tertiary_properties{$tertiary_table}->{$second_on_clause} = $tertiary_table . '_' . (1+scalar(keys(%{$tertiary_properties{$tertiary_table}})));
                }
                else
                {
                    # we already have the necessary join statement
                    $join_needed = 0;
                }
            }
            else
            {
                # no aliased yet, use table name as first alias
                $tertiary_properties{$tertiary_table} = {};
                $tertiary_table_alias = $tertiary_properties{$tertiary_table}->{$second_on_clause} = $tertiary_table;
            }

            if ($join_needed)
            {
                my $tertiary_table_qualified_name = $tertiary_table . ' ' . $tertiary_table_alias;
                push(@tables,
                    {
                        'qualified_name' => $tertiary_table_qualified_name,
                        'alias' => $tertiary_table_alias,
                        'join_clause' => "JOIN $tertiary_table_qualified_name ON $link_table_alias.$link_property_key = $tertiary_table_alias.$property_key",
                    },
                );
            }
            my $qualified_column_name = '';
            # check for special column name
            if ($column_name =~ m/^\w+\(\w+\)$/)
            {
                # operations like "SUM()"
                $qualified_column_name = $column_name;
                $qualified_column_name =~ s/^(\w+)\((w+)\)$/$1($tertiary_table_alias.$2)/;
            }
            elsif ($column_name =~ m/^\w+$/)
            {
                # just a simple column name
                $qualified_column_name = "$tertiary_table_alias.$column_name";
            }
            else
            {
                # column name contains unsupported instructions
                $qualified_column_name = $column_name;
                # warn
                cluck "WARNING: unsupported column name format '$column_name' for object $self (" . ref($self) . "). Column name has been used \"as is\" without qualifier!\n";
            }
            push(@qualified_properties, qq|$qualified_column_name AS "$property_name"|);
            $table_aliases->{$tertiary_table} = $tertiary_table_alias;
        }
        else
        {
            PrintDebug("WARNING: unknown property '$property_name'. Property not found in '$self->{'_properties'}->{'table'}' table.");
        }
    }

    # qualify selectors
    my $new_selectors = $self->_qualifySelectors($selectors, $table_aliases);

    return ($new_selectors, \@qualified_properties, \@tables);
}


=pod

=head2 _prepareTableClause

B<Description>: Returns the content of the 'FROM' SQL clause using the given
array of qualified tables.

B<ArgsCount>: 2

=over 4

=item $self: (DBObject) (R)

the object.

=item $tables: (array ref) (R)

the array of qualified table names (coming from '_qualifyQueryElements'). First
element is the main table name, next elements are all 'JOIN' clausures like
'JOIN table x ON x.field = a.field'.

=back

B<Return>: (string)

the 'FROM' SQL clause.

B<Caller>: internal

=cut

sub _prepareTableClause
{
    my ($self, $tables) = @_;

    # prepare table clause
    my $first_table = $tables->[0];
    my $table_clause = $first_table->{'qualified_name'};
    foreach my $join_table (@$tables[1..$#$tables])
    {
        $table_clause .= " $join_table->{'join_clause'}";
    }

    return $table_clause;
}


=pod

=head2 _prepareSelectorsForQuery

B<Description>: prepare selectors for a "where-clause" with space holders.
It generates two arrays: the first one contains the list of conditions for a
"where-clause" with space holders ('?') that could be joined using " AND " and
the second one contains the list of values for these space holders.

B<ArgsCount>: 2

=over 4

=item $self: (DBObject) (R)

the object.

=item $selectors: (hash ref) (R)

See constructor "$parameters" documentation ('selectors' section).

=back

B<Return>: (list of 2 arrays)

The first array contains the list of conditions for a "where-clause" with space
holders ('?') that could be joined using " AND ";
the second array contains the list of values for these space holders.

B<Caller>: internal

=cut

sub _prepareSelectorsForQuery
{
    my ($self, $selectors) = @_;

    # prepare space holders
    my @space_holders;
    my @bind_values;

    # check if we got a list of conditions joined with operators or
    # a hash of 'and' conditions
    if ('HASH' eq ref($selectors))
    {
        foreach (keys(%$selectors))
        {
            # check for an (EXISTS...) condition
            if ($_ =~ m/$EXISTS_SELECTOR_REGEXP/io)
            {
                # check if we got multiple EXISTS statements
                if ('ARRAY' eq ref($selectors->{$_}))
                {
                    push(@space_holders, "($_ (" . join(") AND $_ (", @{$selectors->{$_}}) . "))");
                }
                else
                {
                    push(@space_holders, "$_ (" . $selectors->{$_} . ")");
                }
            }
              # check if an operator has been specified
            elsif ('ARRAY' eq ref($selectors->{$_}))
            {
                # check for multiple values
                if ($selectors->{$_}->[0] =~ m/^\s*(?:NOT\s+)?IN\s*$/i)
                {
                    if (2 < @{$selectors->{$_}})
                    {
                        # multiple values like "WHERE x IN (1, 2, 3)"
                        # create as many space-holders (?) as needed inside parenthesis
                        # see TRICKS pod section for details
                        my $space_holders = '?, ' x $#{$selectors->{$_}};
                        $space_holders = substr($space_holders, 0, length($space_holders) - 2); # remove last ', '
                        push(@space_holders, "$_ " . $selectors->{$_}->[0] . ' (' . $space_holders . ')');
                        # push values
                        # see TRICKS pod section for details
                        push(@bind_values, @{$selectors->{$_}}[1..$#{$selectors->{$_}}]);
                    }
                    else
                    {
                        # case of IN (1 value)
                        if (!defined($selectors->{$_}->[1]))
                        {
                            $selectors->{$_}->[1] = '';
                        }
                        # remove parenthesis if some
                        $selectors->{$_}->[1] =~ s/^[\s\n\r]*\([\s\n\r]*(.+)[\s\n\r]*\)[\s\n\r]*$/$1/s;
                        # check if it's a subquery
                        if ($selectors->{$_}->[1] =~ m/^SELECT[\s\r\n]/is)
                        {
                            #+FIXME: optimize query with a JOIN instead of the
                            #        IN (SELECT ...) structure when possible
                            push(@space_holders, "$_ " . $selectors->{$_}->[0] . ' (' . $selectors->{$_}->[1] . ')');
                        }
                        else
                        {
                            push(@space_holders, "$_ " . $selectors->{$_}->[0] . ' (?)');
                            push(@bind_values, $selectors->{$_}->[1]);
                        }
                    }
                }
                elsif (1 == @{$selectors->{$_}})
                {
                    # just an operator (or a complex expression) with no space
                    # holder like "IS NOT NULL"
                    push(@space_holders, "$_ " . $selectors->{$_}->[0]);
                }
                elsif (2 == @{$selectors->{$_}})
                {
                    # single value
                    push(@space_holders, "$_ " . $selectors->{$_}->[0] . ' ?');
                    push(@bind_values, $selectors->{$_}->[1]);
                }
                else
                {
                    # multiple values to test
                    # each first item is an operator and the following is a value
                    for (my $selector_group_index = 0; $selector_group_index < @{$selectors->{$_}}; $selector_group_index += 2)
                    {
                        push(@space_holders, "$_ " . $selectors->{$_}->[$selector_group_index] . ' ?');
                        push(@bind_values, $selectors->{$_}->[$selector_group_index+1]);
                        push(@space_holders, 'AND');
                    }
                    # remove last 'AND'
                    pop(@space_holders);
                }
            }
            else
            {
                push(@space_holders, "$_ = ?");
                push(@bind_values, $selectors->{$_});
            }

            # add operator
            push(@space_holders, 'AND');
        }
        # remove last operator
        pop(@space_holders);
    }
    elsif ('ARRAY' eq ref($selectors))
    {
        # a list of joined conditions
        my $need_operator = 0;
        my $last_operator = '';
        foreach my $condition (@$selectors)
        {
            # check for sub-selectors
            if (ref($condition))
            {
                if ($need_operator)
                {
                    # check if we got an operator to propagate
                    if (!$last_operator)
                    {
                        confess "ERROR: Invalid selectors for object $self (" . ref($self) . ")! Got an array of conditions but an operator is missing! Did you forget a 'OR' or a 'AND' operator in your selectors array? ('" . join("', '", @$selectors) . "')\n";
                    }
                    else
                    {
                        # propagate operator
                        push(@space_holders, $last_operator);
                    }
                }
                # add parenthesis for priority
                push(@space_holders, '(');
                my ($space_holders, $bind_values) = $self->_prepareSelectorsForQuery($condition);
                push(@space_holders, @$space_holders);
                push(@bind_values, @$bind_values);
                # close parenthesis
                push(@space_holders, ')');
                $need_operator = 1;
            }
            elsif ($need_operator)
            {
                # else we got an operator
                $last_operator = $condition;
                # make sure it's a supported operator
                if ($last_operator !~ m/^(?:OR|\|\||XOR|AND|\&\&)$/i)
                {
                    confess "ERROR: Invalid selectors for object $self (" . ref($self) . ")! Got an array of conditions with an invalid operator: '$need_operator'! ('" . join("', '", @$selectors) . "')\n";
                }
                push(@space_holders, $last_operator);
                $need_operator = 0;
            }
            else
            {
                confess "ERROR: Invalid selectors for object $self (" . ref($self) . ")! Got an array of conditions but there was an operator at the wrong place! Did you put an extra '$need_operator' operator in your selectors array? ('" . join("', '", @$selectors) . "')\n";
            }
        }
    }
    else
    {
        confess "ERROR: _prepareSelectorsForQuery: Invalid selector structure! Found '$selectors' where a hash ref or an array ref was expected!\n";
    }

    # check consistency
    my $space_holder_count = @{[(join(' ', @space_holders) =~ m/\?/gs)]};
    if ($space_holder_count != scalar(@bind_values))
    {
        PrintDebug("WARNING: number of space holders ($space_holder_count from '" . join(' ', @space_holders) . "') does not correspond to number of bound values (" . scalar(@bind_values) . ")! There might be some mistakes in the object property hash like some fields that should be loaded by default or invalid key configuration in secondary/tertiary properties! SQL Query should be displayed below if you enabled debug mode.");
    }

    return (\@space_holders, \@bind_values);
}


=pod

=head2 _PrepareSQLQuery

B<Description>: Generate the SQL query usgin the given parameters.

B<ArgsCount>: 5

=over 4

=item $self: (DBObject) (R)

the object.

=item $qualified_properties: (array ref) (R)

qualified properties array.

=item $table_clause: (string) (R)

string for the "WHERE" clause (including JOIN statements).

=item $space_holders: (array ref) (R)

array of space holders including fields and comparison operators.
(ex. ["a.family_id = ?", "AND a.family_name LIKE ?"])

=item $sql_options: (hash ref) (R)

a hash of SQL options.

=back

B<Return>: (string)

The SQL query with space holders ('?').

B<Caller>: internal

=cut

sub _PrepareSQLQuery
{
    my ($self, $qualified_properties, $table_clause, $space_holders, $sql_options) = @_;
    my $order_by_clause = '';

    if ($sql_options->{'ORDER BY'})
    {
        if (ref($sql_options->{'ORDER BY'}))
        {
            $order_by_clause = 'ORDER BY ' . join(', ', @{$sql_options->{'ORDER BY'}});
        }
        else
        {
            $order_by_clause = $sql_options->{'ORDER BY'};
        }
    }

    my $sql_query = "SELECT "
                    . ($sql_options->{'DISTINCT'}?'DISTINCT ':'')
                    . join(', ', @$qualified_properties) . "
                     FROM $table_clause
                     WHERE " . (join(' ', @$space_holders) || 'TRUE')
                     . ' ' . $order_by_clause
                     . ' ' . (exists($sql_options->{'LIMIT'})?
                                'LIMIT ' . $sql_options->{'LIMIT'} . ($sql_options->{'OFFSET'}?' OFFSET ' . $sql_options->{'OFFSET'}:'')
                               :''
                             )
                     . ";";

    return $sql_query;
}


=pod

=head2 _fetchProperties

B<Description>: loads the requested properties into current object.

B<ArgsCount>: 4-6

=over 4

=item $self: (DBObject) (R)

the object.

=item $selectors: (hash ref) (R)

See constructor "$parameters" documentation ('selectors' section).

=item $properties: (array ref) (R)

a list of (qualified) properties to fetch (for the SELECT clause).

=item $table_clause: (string) (R)

the 'FROM' SQL clause.

=item $multiple_values: (boolean) (U)

False if several instances corresponding to each row of the SQL query should be
returned. True if all the returned rows should be groupped into a single array
corresponding to the property of one single object.
Default: False

item $sql_options: (hash) (O)

a hash containing some SQL options like:
-'ORDER BY': a scalar or an array of scalar for the 'ORDER BY' clause;
-'LIMIT': an integer corresponding to the limit of rows to return;
-'OFFSET': an integer corresponding to an offset in the result list;
-'DISTINCT': a boolean that enables or disables DISTINCT option.

=back

B<Return>: (list)

a list of DBObjects, the first one being the caller ($self).

B<Caller>: internal

=cut

sub _fetchProperties
{
    my ($self, $selectors, $properties, $table_clause, $multiple_values, $sql_options) = @_;

    # check if object is linked to a database
    if (!$self->{'_dbh'})
    {
        return $self;
    }

    my @new_instances = ();

    # use property table as default
    $table_clause ||= $self->{'_properties'}->{'table'};

    # make sure SQL options are valid
    $sql_options = $self->_initSQLOptions($sql_options);

    # load values from database...
    # prepare space holders
    my ($space_holders, $bind_values) = $self->_prepareSelectorsForQuery($selectors);

    # prepare query
    my $sql_query = $self->_PrepareSQLQuery($properties, $table_clause, $space_holders, $sql_options);
    PrintDebug("SQL Query: $sql_query\nSQL Bindings (" . scalar(@$bind_values) . " values)" . (@$bind_values? ': (' . join(', ', @$bind_values) . ')':''));

    if ($multiple_values)
    {
        # note: multiple value fields can't be serialized (not base fields).
        # prepare storage arrays for each property
        foreach (@$properties)
        {
            my ($property_name) = ($_ =~ m/\sAS\s+["'](.*)["']/i);
            $property_name ||= $_;
            $self->{$property_name} ||= [];
            if (!ref($self->{$property_name}))
            {
                # warn  "WARNING: Trying to load multiple values for member property '$property_name' while there is already one value loaded! Single value converted into an array.\n";
                # $self->{$property_name} = [$self->{$property_name}];
                confess "ERROR: Trying to load multiple values for member property '$property_name' while there is already one value loaded!\n";
            }
        }
        # multiple values: we assume each property has multiple values
        my $row_values = $self->{'_dbh'}->selectall_arrayref($sql_query, { 'Slice' => {} }, @$bind_values)
            or confess 'ERROR: ' . $self->{'_dbh'}->errstr;
        # store values
        foreach my $values (@$row_values)
        {
            map { push(@{$self->{$_}}, $values->{$_}) } keys(%$values);
        }
    }
    else
    {
        # single value for each property but we may have multiple instances
        my $all_values = $self->{'_dbh'}->selectall_arrayref($sql_query, { 'Slice' => {} }, @$bind_values)
            or confess 'ERROR: ' . $self->{'_dbh'}->errstr;
        my $first_row = shift(@$all_values);
        map
        {
            # check for serialized data in base fields.
            if (exists($self->{'_properties'}->{'base'}->{$_})
                && $self->{'_properties'}->{'base'}->{$_}->{'serialized'})
            {
                # serialized, unserialize
                $first_row->{$_} = UnfreezeData($first_row->{$_});
            }
            $self->{$_} = $first_row->{$_};
        }
        keys(%$first_row);
        # check for multiple keys to set virtual key value
        $self->_initKeyValue();
        #+FIXME: check if undef should be returned in case the object has not been instanciated from DB

        foreach my $row_values (@$all_values)
        {
            # instanciate other values using child class (that will call this parent class again)
            if (my $new_instance = ref($self)->new($self->{'_dbh'}, {'members' => $row_values, 'load' => 'none'}))
            {
                $new_instance->_initKeyValue();
                push(@new_instances, $new_instance);
            }
            else
            {
                warn "Warning: Failed to initialize additional object (" . join(', ', @$row_values) . ").";
            }
        }
    }

    return ($self, @new_instances);
}


=pod

=head2 _countProperties

B<Description>: counts the requested properties into current object.

B<ArgsCount>: 4-6

=over 4

=item $self: (DBObject) (R)

the object.

=item $selectors: (hash ref) (R)

See constructor "$parameters" documentation ('selectors' section).

=item $properties: (array ref) (R)

a list of (qualified) properties to fetch (for the SELECT clause).

=item $table_clause: (string) (R)

the 'FROM' SQL clause.

=item $multiple_values: (boolean) (U)

False if several instances corresponding to each row of the SQL query should be
returned. True if all the returned rows should be groupped into a single array
corresponding to the property of one single object.

item $sql_options: (hash) (O)

a hash containing some SQL options like:
-'ORDER BY': a scalar or an array of scalar for the 'ORDER BY' clause;
-'LIMIT': an integer corresponding to the limit of rows to return;
-'OFFSET': an integer corresponding to an offset in the result list;
-'DISTINCT': a boolean that enables or disables DISTINCT option.

=back

B<Return>: (list)

a list of DBObjects, the first one being the caller ($self).

B<Caller>: internal

=cut

sub _countProperties
{
    my ($self, $selectors, $table_clause, $sql_options) = @_;

    # check if object is linked to a database
    if (!$self->{'_dbh'})
    {
        return 0;
    }

    # use property table as default
    $table_clause ||= $self->{'_properties'}->{'table'};

    # make sure SQL options are valid
    $sql_options = $self->_initSQLOptions($sql_options);

    # prepare space holders
    my ($space_holders, $bind_values) = $self->_prepareSelectorsForQuery($selectors);

    # prepare query
    my $sql_query = $self->_PrepareSQLQuery(['COUNT(1)'], $table_clause, $space_holders, $sql_options);
    PrintDebug("SQL Query: $sql_query\nSQL Bindings (" . scalar(@$bind_values) . " values)" . (@$bind_values? ': (' . join(', ', @$bind_values) . ')':''));

    # get count
    my ($count) = $self->{'_dbh'}->selectrow_array($sql_query, undef, @$bind_values)
        or confess 'ERROR: ' . $self->{'_dbh'}->errstr;

    return $count;
}


=pod

=head2 _sortMember

B<Description>: sort member values according to specified sort parameters.

B<ArgsCount>: 3

=over 4

=item $self: (DBObject) (R)

the object.

=item $member: (string) (R)

the member to sort. Must be an array!

=item $parameters: (array ref) (R)

If member contains sub-objects, an array of pairs of sub-object member name
(string) + rule (string or sub ref) to use for sorting.

If member contains only scalars, the array will contain only one value wich will
be the rule (string or sub ref) to use for sorting.

=back

B<Return>: ()

B<Caller>: internal

=cut

sub _sortMember
{
    my ($self, $member, $parameters) = @_;

    # make sure we got a sort order
    if (!$parameters || ('ARRAY' ne ref($parameters)))
    {
        confess "ERROR: invalid sortMember parameter! It should be an array ref.\n";
    }
    
    my %DEFAULT_SORT_RULES =
    (
        # alphabetic
        'ASC'  => sub {my ($a, $b) = @_; $a cmp $b;},
        'DESC' => sub {my ($a, $b) = @_; $b cmp $a;},
        # numeric
        'NASC'  => sub {my ($a, $b) = @_; $a <=> $b;},
        'NDESC' => sub {my ($a, $b) = @_; $b <=> $a;},
        # insensistive alphabetic
        'IASC'  => sub {my ($a, $b) = @_; uc($a) cmp uc($b);},
        'IDESC' => sub {my ($a, $b) = @_; uc($b) cmp uc($a);},
        # alpha-numeric
        'ANASC' => sub {
            my ($a, $b) = @_; 
            if (looks_like_number($a))
            {
                if (looks_like_number($b))
                {
                    return $a <=> $b;
                }
                else
                {
                    return 1;
                }
            }
            elsif (looks_like_number($b))
            {
                return -1;
            }
            else
            {
                return $a cmp $b;
            }
        },
        'ANDESC' => sub {
            my ($a, $b) = @_; 
            if (looks_like_number($a))
            {
                if (looks_like_number($b))
                {
                    return $b <=> $a;
                }
                else
                {
                    return -1;
                }
            }
            elsif (looks_like_number($b))
            {
                return 1;
            }
            else
            {
                return $b cmp $a;
            }
        },
    );
    
    my @sorted_array;
    # check if we got something to sort
    if (@{$self->{$member}})
    {
        # check if members are only scalars
        if (!ref($self->{$member}->[0]))
        {
            # scalars
            if (0 == @$parameters)
            {
                # sort using default
                @sorted_array = sort @{$self->{$member}};
            }
            elsif (1 == @$parameters)
            {
                # check sort type
                if ('CODE' eq ref($parameters->[0]))
                {
                    # sort using given function
                    @sorted_array = sort {$parameters->[0]->($a, $b);} @{$self->{$member}};
                }
                elsif (exists($DEFAULT_SORT_RULES{uc($parameters->[0])}))
                {
                    @sorted_array = sort {$DEFAULT_SORT_RULES{uc($parameters->[0])}->($a, $b);} @{$self->{$member}};
                }
                else
                {
                    confess "ERROR: invalid sortMember parameter: unknown or unsupported rule '" . $parameters->[0] . "'!\n";
                }
            }
            else
            {
                confess "ERROR: invalid sortMember parameter: scalar value can be sorted using no sub-member name and only one rule!\n";
            }
        }
        else
        {
            # objects
            my @sort_parameters = @$parameters;
            @sorted_array = @{$self->{$member}};
            
            while (@sort_parameters)
            {
                my $sort_method = pop(@sort_parameters);
                # check sort type
                if ('CODE' eq ref($sort_method))
                {
                    # sort using given function
                    @sorted_array = sort {$sort_method->($a, $b);} @sorted_array;
                }
                elsif (exists($DEFAULT_SORT_RULES{uc($sort_method)}))
                {
                    # sort given member using provided rule
                    # get member to sort
                    my $sub_member  = pop(@sort_parameters);
                    @sorted_array = sort
                        {
                            # transfer $a and $b member values to $a and $b
                            my $ma = $a->$sub_member();
                            my $mb = $b->$sub_member();
                            $DEFAULT_SORT_RULES{uc($sort_method)}->($ma, $mb);
                        }
                        @sorted_array;
                }
                else
                {
                    confess "ERROR: invalid sortMember parameter: unknown or unsupported rule '" . $sort_method . "'!\n";
                }
            }
        }
    }
    
    return \@sorted_array;
}


=pod

=head2 _getSelfSelectors

B<Description>: Returns the selectors hash that can identify current object in
the database.

B<ArgsCount>: 1

=over 4

=item $self: (DBObject) (R)

the object.

=back

B<Return>: (hash ref)

The selectors hash structure that identify current object ($self) in database
or undef if object has not been initialized.

B<Caller>: internal

=cut

sub _getSelfSelectors
{
    my ($self) = @_;

    my $selectors;

    # load it from database
    my $key = $self->{'_properties'}->{'key'};

    # check for multiple keys
    if ($key =~ m/$KEY_SEPARATOR/o)
    {
        # multiple keys (like in 'sequence_id,ipr_id')
        my @keys = split(/\s*$KEY_SEPARATOR\s*/o, $key);
        my @values = split(/\s*$KEY_SEPARATOR\s*/o, $self->{$key});
        while (@keys)
        {
            $selectors->{shift(@keys)} = shift(@values);
        }
    }
    elsif ($self->{$key})
    {
        $selectors = {$key => $self->{$key}};
    }

    return $selectors;
}


=pod

=head2 _orderMemberValues

B<Description>: .

B<ArgsCount>: 3

=over 4

=item $self: (DBObject) (R)

the object.

=item $member: (string) (R)

the member to process.

=back

B<Return>: (scalar)

Member value or undef

B<Caller>: internal

=cut

sub _orderMemberValues
{
    my ($self, $members, $order_bys) = @_;

    my @values = @{$members};

    # array or single column?
    if (!ref($order_bys))
    {
        # transfer single column into an array
        $order_bys = [$order_bys];
    }

    # order
    my @sorting = @{$order_bys};
    while (my $column_sort = pop(@sorting))
    {
        my ($column, $order) = ($column_sort =~ m/\s*(?:BINARY\s+)?(?:\w+\.)?(\w+)(?:\s+(ASC|DESC))?\s*$/i);
        if (!$column)
        {
            confess "ERROR: invalid SQL sort option '$column_sort' for object $self (" . ref($self) . ")\n";
        }

        if ($order && ('DESC' eq $order))
        {
            @values = sort
                {
                    return $b->$column() cmp $a->$column();
                }
                @values;
        }
        else
        {
            @values = sort
                {
                    return ($a->$column() cmp $b->$column());
                }
                @values;
        }
    }

    return \@values;
}


=pod

=head2 _offsetMemberValues

B<Description>: .

B<ArgsCount>: 3

=over 4

=item $self: (DBObject) (R)

the object.

=item $member: (string) (R)

the member to process.

=back

B<Return>: (scalar)

Member value or undef

B<Caller>: internal

=cut

sub _offsetMemberValues
{
    my ($self, $members, $offset) = @_;

    my @values = @{$members};

    if ($offset < @values)
    {
        @values = splice(@values, $offset);
    }
    else
    {
        @values = ();
    }

    return \@values;
}


=pod

=head2 _limitMemberValues

B<Description>: .

B<ArgsCount>: 3

=over 4

=item $self: (DBObject) (R)

the object.

=item $member: (string) (R)

the member to process.

=back

B<Return>: (scalar)

Member value or undef

B<Caller>: internal

=cut

sub _limitMemberValues
{
    my ($self, $members, $limit) = @_;

    my @values = @{$members};

    @values = splice(@values, 0, $limit);

    return \@values;
}


=pod

=head2 _distinctMemberValues

B<Description>: .

B<ArgsCount>: 3

=over 4

=item $self: (DBObject) (R)

the object.

=item $member: (string) (R)

the member to process.

=back

B<Return>: (scalar)

Member value or undef

B<Caller>: internal

=cut

sub _distinctMemberValues
{
    my ($self, $members) = @_;

    # get current values and process SQL options manually
    my @values = @{$members};

    my @unique_values = ();
    my %values_found;
    foreach (@values)
    {
        if (!exists($values_found{''.$_}))
        {
            $values_found{''.$_} = 1;
            push(@unique_values, $_);
        }
    }

    return \@values;
}


=pod

=head2 _processPropertyFetch

B<Description>: .

B<ArgsCount>: 3

=over 4

=item $self: (DBObject) (R)

the object.

=item $member: (string) (R)

the member to process.

=item $value: (scalar) (O)

the new value.

=back

B<Return>: (scalar)

Member value or undef

B<Caller>: internal

=cut

sub _processPropertyFetch
{
    my ($self, $member, $parameters) = @_;

    my $sql_options = {};
    if ($parameters)
    {
        $sql_options = $parameters->{'sql'};
    }

    PrintDebug("Fetching member '$member'");
    # fetch last selection?
    if (defined($parameters))
    {
        # no, we have new SQL options to process

        # check if we already got all values from this member and we can do the
        # selection without querying the database
        if (exists($self->{$member}) && !($parameters->{'selectors'}))
        {
            # the member doesn't need to be fetched from database
            PrintDebug("SQL options on member '$member' can be processed without querying database");

            # make sure we have an array of something
            if ('ARRAY' ne ref($self->{$member}))
            {
                # not an array, return single value
                return $self->{'_fetch'}->{$member} = $self->{$member};
            }

            # get current values and process SQL options manually
            my @values = @{$self->{$member}};

            # process options
            if (exists($sql_options->{'ORDER BY'}))
            {
                @values = @{$self->_orderMemberValues(\@values, $sql_options->{'ORDER BY'})};
            }

            if (exists($sql_options->{'OFFSET'}))
            {
                @values = @{$self->_offsetMemberValues(\@values, $sql_options->{'OFFSET'})};
            }

            if (exists($sql_options->{'LIMIT'}))
            {
                @values = @{$self->_limitMemberValues(\@values, $sql_options->{'LIMIT'})};
            }

            if (exists($sql_options->{'DISTINCT'}))
            {
                @values = @{$self->_distinctMemberValues(\@values)};
            }

            $self->{'_fetch'}->{$member} = \@values;
        }
        else
        {
            # we must fetch member from databse using SQL options, return undef
            PrintDebug("Proceed fetching member '$member' with SQL options from database");
            $self->{'_fetch'}->{$member} = undef;
        }
    }

    return $self->{'_fetch'}->{$member};
}


=pod

=head2 _processBaseProperty

B<Description>: setter/getter for a base property. In the getter case, if the
property has not already been loaded, it will be loaded frome database.

B<ArgsCount>: 2-4

=over 4

=item $self: (DBObject) (R)

the object.

=item $member: (string) (R)

the member to process.

=item $accessor: (string) (U)

the type of accessor (default behavior depends on the $value parameter).

=item $value: (scalar or hash ref) (O)

if not a hash ref, the new value. If it's a hash ref, then it is considered as
containing SQL conditions like:
-'ORDER BY': a scalar or an array of scalar for the 'ORDER BY' clause;
-'LIMIT': an integer corresponding to the limit of rows to return;
-'OFFSET': an integer corresponding to an offset in the result list;
-'DISTINCT': a boolean that enables or disables DISTINCT option.

=back

B<Return>: (scalar)

Member value.

B<Caller>: internal

=cut

sub _processBaseProperty
{
    my ($self, $member, $accessor, $value) = @_;

    # it's a base property, get column name
    my $column_name = $self->{'_properties'}->{'base'}->{$member}->{'property_column'};
    
    # check for fetch, set or get
    if (defined($value) && ($accessor !~ m/fetch|sort|count/))
    {
        # setter
        if (defined($accessor) && ('get' eq $accessor))
        {
            # error: use of get with a value!
            confess "ERROR: Tried to use getter with a value as argument! Did you want to use setter instead?\n";
        }
        # keep track of modified base values
        $self->{'_modified'}->{$column_name} = $self->{$column_name};
        return $self->{$column_name} = $value;
    }
    else
    {
        # getter or fetch or sort or count

        # if it's a fetch or a sort, base properties are single values and are
        # not affected by SQL options. Just return current member value.

        # check if field is loaded and if not, load it
        if (!exists($self->{$column_name}))
        {
            if (my $selectors = $self->_getSelfSelectors())
            {
                $self->_fetchProperties($selectors, [$column_name]);
                # Check for serialized data still serialized.
                if (($self->{'_properties'}->{'base'}->{$member}->{'serialized'})
                    && (!ref($self->{$column_name})))
                {
                    # serialized, unserialize
                    $self->{$column_name} = UnfreezeData($self->{$column_name});
                }
            }
            else
            {
                # the object doesn't seem to have been initialized since no key has been set
                PrintDebug("WARNING: trying to fetch a property ('$column_name') from an uninitialized object!");
                cluck "WARNING: trying to fetch a property ('$column_name') from an uninitialized object!";
            }
        }

        # if 'count', return 1 if defined
        if ($accessor =~ m/count/)
        {
            return defined($self->{$column_name}) ? 1 : 0;
        }
        else
        {
            return $self->{$column_name};
        }
    }
}


=pod

=head2 _processSecondaryProperty

B<Description>: setter/getter for a secondary property. In the getter case, if
the property has not already been loaded, it will be loaded frome database.

B<ArgsCount>: 4

=over 4

=item $self: (DBObject) (R)

the object.

=item $member: (string) (R)

the member to process.

=item $accessor: (string) (U)

the type of accessor (default behavior depends on the $value parameter).

=item $value: (scalar) (O)

the new value.

=back

B<Return>: (scalar)

Member value.

B<Caller>: internal

=cut

sub _processSecondaryProperty
{
    my ($self, $member, $accessor, $value) = @_;

    # check for fetch, set or get
    if (defined($value) && ($accessor !~ m/fetch|sort|count/))
    {
        # setter
        if ('get' eq $accessor)
        {
            # error: use of get with a value!
            confess "ERROR: Tried to use getter with a value as argument! Did you want to use setter instead?\n";
        }
        return $self->{$member} = $value;
    }
    else
    {
        # getter or fetch
        
        # Check for serialized cached data first.
        if (!exists($self->{$member})
            && exists($self->{'_properties'}->{'secondary'}->{$member}->{'cached'})
            && $self->{'_properties'}->{'secondary'}->{$member}->{'cached'})
        {
            my ($cache_column, $cache_key) = split(/:/, $self->{'_properties'}->{'secondary'}->{$member}->{'cached'});
            if (!defined($cache_column) || !defined($cache_key))
            {
              confess "ERROR: Invalid cached property setting for secondary member $member! The setting should follow the form 'cached_colum_name:hash_key_name'.\n";
            }
            if (ref($self->$cache_column) ne 'HASH')
            {
              confess "ERROR: Invalid cached data in $cache_column column for object $self (" . ref($self) . "). Maybe it has not been unserialized.\n";
            }
            $self->{$member} = $self->$cache_column->{$cache_key};
        }

        # check if field is loaded and if not, load it
        if ((!exists($self->{$member}) || ($accessor =~ m/fetch|count/)) && $self->{'_dbh'})
        {
            # load it from database

            # prepare keys...
            # check if a different key (of current object) from default one should be used
            my $object_key_name = $self->{'_properties'}->{'secondary'}->{$member}->{'object_key'}
                                  || $self->{'_properties'}->{'key'};
            if ($object_key_name =~ m/\w$KEY_SEPARATOR\w/o)
            {
                confess "ERROR: it is not possible to get a member of an object using multiple keys ('$object_key_name')! Please make sure the secondary member '$member' as been correctly set up in the property configuration hash for class " . ref($self);
            }
            my $object_key_value = $self->{$object_key_name};
            if (!$object_key_value && ($object_key_name =~ m/^\w+$/))
            {
                $object_key_value = $self->$object_key_name();
            }

            if (!$object_key_value)
            {
                return undef;
                # when no member key is available, return undef (and dot not crash)
                # confess "ERROR: object key not loaded! Unable to fetch secondary property '$member' for object " . ref($self) . " ('$self')!\n";
            }

            my $secondary_key = $self->{'_properties'}->{'secondary'}->{$member}->{'property_key'};
            my $selector = {$secondary_key => $object_key_value};

            # check if we should load columns or objects
            if (my $property_module = $self->{'_properties'}->{'secondary'}->{$member}->{'property_module'})
            {
                # objects
                # load module (if necessary)
                # see TRICKS pod section for details
                eval("use $property_module;");
                # make sure there was no issue
                if ($@)
                {
                    confess "ERROR: DBObject: unable to load module '$property_module'!\nYou may check if this module is installed or check the property configuration for '$AUTOLOAD'.\n$@\n";
                }

                # single/multiple values?
                if ($self->{'_properties'}->{'secondary'}->{$member}->{'multiple_values'})
                {
                    # multiple

                    # in case of a fetch, also add SQL options and selector
                    my $sql_options;
                    if ($value)
                    {
                        $sql_options = $value->{'sql'};
                        if ($value->{'selectors'})
                        {
                            my $old_selector = $selector;
                            $selector = $value->{'selectors'};
                            # check what type of selector we got
                            if ('HASH' eq ref($selector))
                            {
                                # it's a hash, add or replace key selector
                                foreach my $key_field (keys(%$old_selector))
                                {
                                    $selector->{$key_field} = $old_selector->{$key_field};
                                }
                            }
                            elsif ('ARRAY' eq ref($selector))
                            {
                                # add using 'AND' condition
                                $selector = [$old_selector, 'AND', $selector];
                            }
                        }
                    }

                    # if it's a 'count' return now
                    if ('count' eq $accessor)
                    {
                        return "$property_module"->Count($self->{'_dbh'}, {'selectors' => $selector, 'sql' => $sql_options});
                    }

                    # see TRICKS pod section for details
                    my @new_objects = ("$property_module"->new($self->{'_dbh'}, {'selectors' => $selector, 'sql' => $sql_options}));

                    # make sure we got a valid (first) object
                    # nb.: in case of several object returned, if the first one
                    #      is good the others are good too. If the first one is bad,
                    #      then, there shouldn't be other objects.
                    if ($new_objects[0])
                    {
                        if ('fetch' eq $accessor)
                        {
                            $self->{'_fetch'}->{$member} = \@new_objects;
                        }
                        else
                        {
                            $self->{$member} = \@new_objects;
                        }
                    }
                    else
                    {
                        if ('fetch' eq $accessor)
                        {
                            $self->{'_fetch'}->{$member} = [];
                        }
                        else
                        {
                            $self->{$member} = [];
                        }
                    }
                }
                else
                {
                    # single
                    
                    # see TRICKS pod section for details
                    $self->{$member} = "$property_module"->new($self->{'_dbh'}, {'selectors' => $selector});
                    # make sure we got a valid object
                    if (!$self->{$member})
                    {
                        # not a valid object, clear member
                        $self->{$member} = undef;
                    }

                    # if it's a 'count', return now
                    if ('count' eq $accessor)
                    {
                        return defined($self->{$member}) ? 1 : 0;
                    }
                    # if it's a fetch, a single value is not affected by SQL
                    # options. Just return current member value.
                    if ('fetch' eq $accessor)
                    {
                        $self->{'_fetch'}->{$member} = $self->{$member};
                    }

                }
            }
            elsif (my $column_name = $self->{'_properties'}->{'secondary'}->{$member}->{'property_column'})
            {
                # columns
                my $member_name;
                if ('fetch' eq $accessor)
                {
                    # fetch member with SQL options in a temporary member field
                    $self->_fetchProperties(
                        $selector,
                        [qq|$column_name AS "_fetch_$member"|], # temporary member
                        $self->{'_properties'}->{'secondary'}->{$member}->{'property_table'},
                        $self->{'_properties'}->{'secondary'}->{$member}->{'multiple_values'},
                        $value, # sql options
                    );
                    return $self->{'_fetch'}->{$member} = delete($self->{"_fetch_$member"});
                }
                else
                {
                    $self->_fetchProperties(
                        $selector,
                        [qq|$column_name AS "$member"|],
                        $self->{'_properties'}->{'secondary'}->{$member}->{'property_table'},
                        $self->{'_properties'}->{'secondary'}->{$member}->{'multiple_values'},
                    );
                }
            }
            else
            {
                confess "ERROR: Object property settings do not describe how to load the secondary member '$member'! Please check the property setting hash in the related module.\n";
            }
        }

        if ('fetch' eq $accessor)
        {
            return $self->{'_fetch'}->{$member};
        }
        elsif (('sort' eq $accessor)
               && ($self->{'_properties'}->{'secondary'}->{$member}->{'multiple_values'}))
        {
            return $self->_sortMember($member, $value);
        }
        elsif ('count' eq $accessor)
        {
            if ($self->{'_properties'}->{'secondary'}->{$member}->{'multiple_values'})
            {
                return scalar(@{$self->{$member}});
            }
            elsif (defined($self->{$member}))
            {
                return 1;
            }
            else
            {
                return 0;
            }
        }
        else
        {
            return $self->{$member};
        }
    }
}


=pod

=head2 _processTertiaryProperty

B<Description>: setter/getter for a tertiray property. In the getter case, if
the property has not already been loaded, it will be loaded frome database.

B<ArgsCount>: 4

=over 4

=item $self: (DBObject) (R)

the object.

=item $member: (string) (R)

the member to process.

=item $accessor: (string) (U)

the type of accessor (default behavior depends on the $value parameter).

=item $value: (scalar) (O)

the new value.

=back

B<Return>: (scalar)

Member value.

B<Caller>: internal

=cut

sub _processTertiaryProperty
{
    my ($self, $member, $accessor, $value) = @_;

    # check for fetch, set or get
    if (defined($value) && ($accessor !~ m/fetch|sort|count/))
    {
        # setter
        if ('get' eq $accessor)
        {
            # error: use of get with a value!
            confess "ERROR: Tried to use getter with a value as argument! Did you want to use setter instead?\n";
        }
        return $self->{$member} = $value;
    }
    else
    {
        # getter or fetch

        # Check for serialized cached data first.
        if (!exists($self->{$member})
            && exists($self->{'_properties'}->{'tertiary'}->{$member}->{'cached'})
            && $self->{'_properties'}->{'tertiary'}->{$member}->{'cached'})
        {
            my ($cache_column, $cache_key) = split(/:/, $self->{'_properties'}->{'tertiary'}->{$member}->{'cached'});
            if (!defined($cache_column) || !defined($cache_key))
            {
              confess "ERROR: Invalid cached property setting for tertiary member $member! The setting should follow the form 'cached_colum_name:hash_key_name'.\n";
            }
            if (ref($self->$cache_column) ne 'HASH')
            {
              confess "ERROR: Invalid cached data in $cache_column column for object $self (" . ref($self) . "). Maybe it has not been unserialized.\n";
            }
            $self->{$member} = $self->$cache_column->{$cache_key};
        }

        # check if field is loaded and if not, load it
        if ((!exists($self->{$member}) || ($accessor =~ m/fetch|count/)) && $self->{'_dbh'})
        {
            # load it from database

            # check if we should load columns or objects
            if (my $property_module = $self->{'_properties'}->{'tertiary'}->{$member}->{'property_module'})
            {
                # objects
                # load module (if necessary)
                # see TRICKS pod section for details
                eval("use $property_module;");
                # make sure there was no issue
                if ($@)
                {
                    confess "ERROR: DBObject: unable to load module '$property_module'!\nYou may check if this module is installed or check the property configuration for '$AUTOLOAD'.\n$@\n";
                }

                # get key value to use to identify object in intermediate table
                my $object_key_name = $self->{'_properties'}->{'tertiary'}->{$member}->{'object_key'};
                $object_key_name ||= $self->{'_properties'}->{'key'}; # if no object key specified, use default
                if ($object_key_name =~ m/$KEY_SEPARATOR/o)
                {
                    confess "ERROR: it is not possible to get a member of an object using multiple keys ('$object_key_name')! Please make sure the tertiary member '$member' as been correctly set up in the property configuration hash for class " . ref($self);
                }
                my $object_key_value = $self->{$object_key_name} || $self->getDBKeyValue();
                if (!$object_key_value)
                {
                    confess "ERROR: object key not loaded! Unable to fetch tertiary property '$member' for object " . ref($self) . " ('$self')!\n";
                }
                my $property_key_name = $self->{'_properties'}->{'tertiary'}->{$member}->{'property_key'};
                my $link_object_key_name = $self->{'_properties'}->{'tertiary'}->{$member}->{'link_object_key'};
                my $link_property_key_name = $self->{'_properties'}->{'tertiary'}->{$member}->{'link_property_key'};
                my $link_property_table = $self->{'_properties'}->{'tertiary'}->{$member}->{'link_table'};
                # qualify table and query elements in sub-query to avoid name collisions
                my $property_key_selection = "SELECT tertiary_table_alias.$link_property_key_name FROM $link_property_table tertiary_table_alias WHERE tertiary_table_alias.$link_object_key_name = '$object_key_value';";
                PrintDebug("SQL Query (tertiary property): $property_key_selection");
                my $property_key_value = $self->{'_dbh'}->selectcol_arrayref($property_key_selection)
                    or confess 'ERROR: ' . $self->{'_dbh'}->errstr;
                my $selector = {};
                if (@$property_key_value)
                {
                    # single/multiple values?
                    if ($self->{'_properties'}->{'tertiary'}->{$member}->{'multiple_values'})
                    {
                        # multiple
                        if (('fetch' eq $accessor) || ('count' eq $accessor))
                        {
                            $self->{'_fetch'}->{$member} = [];

                            # note: we can't splice if we use SQL options
                            # this may in some rare cases rise an issue due to MySQL 'max_allowed_packet'
                            $selector->{$property_key_name} = ['IN', @$property_key_value];

                            # in case of a fetch, also add SQL options and selector
                            my $sql_options;
                            if ($value)
                            {
                                $sql_options = $value->{'sql'};
                                if ($value->{'selectors'})
                                {
                                    my $old_selector = $selector;
                                    $selector = $value->{'selectors'};
                                    # check what type of selector we got
                                    if ('HASH' eq ref($selector))
                                    {
                                        # it's a hash, add or replace key selector
                                        foreach my $key_field (keys(%$old_selector))
                                        {
                                            $selector->{$key_field} = $old_selector->{$key_field};
                                        }
                                    }
                                    elsif ('ARRAY' eq ref($selector))
                                    {
                                        # add using 'AND' condition
                                        $selector = [$old_selector, 'AND', $selector];
                                    }
                                }
                            }
                            
                            # if count return now
                            if ('count' eq $accessor)
                            {
                                return "$property_module"->Count($self->{'_dbh'}, {'selectors' => $selector});
                            }

                            # see TRICKS pod section for details
                            my @new_objects = ("$property_module"->new($self->{'_dbh'}, {'selectors' => $selector, 'sql' => $sql_options}));
                            # make sure we got a valid (first) object
                            # nb.: in case of several object returned, if the first one
                            #      is good the others are good too. If the first one is bad,
                            #      then, there shouldn't be other objects.
                            if ($new_objects[0])
                            {
                                $self->{'_fetch'}->{$member} = \@new_objects;
                            }
                        }
                        else
                        {
                            $self->{$member} = [];
                            my @reminding_values = @$property_key_value;
                            while (@reminding_values)
                            {
                                $selector->{$property_key_name} = ['IN', splice(@reminding_values, 0, $MAX_IN_CLAUSE_ELEMENTS)];

                                # see TRICKS pod section for details
                                my @new_objects = ("$property_module"->new($self->{'_dbh'}, {'selectors' => $selector}));
                                # make sure we got a valid (first) object
                                # nb.: in case of several object returned, if the first one
                                #      is good the others are good too. If the first one is bad,
                                #      then, there shouldn't be other objects.
                                if ($new_objects[0])
                                {
                                    push(@{$self->{$member}}, @new_objects);
                                }
                            }
                        }
                    }
                    else
                    {
                        # single

                        $selector->{$property_key_name} = ['IN', @$property_key_value];

                        $self->{$member} = "$property_module"->new($self->{'_dbh'}, {'selectors' => $selector});
                        # make sure we got a valid object
                        if (!$self->{$member})
                        {
                            # not a valid object, clear member
                            $self->{$member} = undef;
                            PrintDebug("Remove uninitialized fetched member object ($member) for $self");
                        }

                        # if it's a 'count', return now
                        if ('count' eq $accessor)
                        {
                            return defined($self->{$member}) ? 1 : 0;
                        }
                        # if it's a fetch, a single value is not affected by SQL
                        # options. Just return current member value.
                        if ('fetch' eq $accessor)
                        {
                            $self->{'_fetch'}->{$member} = $self->{$member};
                        }

                    }
                }
                else
                {
                    # no match
                    PrintDebug("No matching member object ($member) for $self");

                    # if it's a 'count', return now
                    if ('count' eq $accessor)
                    {
                        return 0;
                    }

                    if ($self->{'_properties'}->{'tertiary'}->{$member}->{'multiple_values'})
                    {
                        $self->{$member} = [];
                    }
                    else
                    {
                        $self->{$member} = undef;
                    }

                    if ('fetch' eq $accessor)
                    {
                        $self->{'_fetch'}->{$member} = $self->{$member};
                    }
                }
            }
            elsif (my $column_name = $self->{'_properties'}->{'tertiary'}->{$member}->{'property_column'})
            {
                # columns
                my ($qualified_selectors, $qualified_properties, $tables) = $self->_qualifyQueryElements({$self->{'_properties'}->{'key'} => $self->{$self->{'_properties'}->{'key'}}}, [$member]);

                # prepare table clause
                my $table_clause = $self->_prepareTableClause($tables);

                if ('count' eq $accessor)
                {
                    # count properties from database
                    return $self->_countProperties(
                        $qualified_selectors,
                        $table_clause,
                    );
                }

                # load properties from database
                $self->_fetchProperties(
                    $qualified_selectors,
                    $qualified_properties,
                    $table_clause,
                    $self->{'_properties'}->{'tertiary'}->{$member}->{'multiple_values'},
                );
            }
            else
            {
                confess "ERROR: Object property settings do not describe how to load the tertiary member '$member'! Please check the property setting hash in the related module.\n";
            }

        }

        if ('fetch' eq $accessor)
        {
            return $self->{'_fetch'}->{$member};
        }
        elsif (('sort' eq $accessor)
               && ($self->{'_properties'}->{'tertiary'}->{$member}->{'multiple_values'}))
        {
            return $self->_sortMember($member, $value);
        }
        elsif ('count' eq $accessor)
        {
            if ($self->{'_properties'}->{'tertiary'}->{$member}->{'multiple_values'})
            {
                if (defined($self->{$member}))
                {
                    return scalar(@{$self->{$member}});
                }
                else
                {
                    return 0;
                }
            }
            elsif (defined($self->{$member}))
            {
                return 1;
            }
            else
            {
                return 0;
            }
        }
        else
        {
            return $self->{$member};
        }
    }
}




=pod

=head2 discardChanges

B<Description>: ignore any member that has been modified before calling
discardChanges when save() will be called.

B<ArgsCount>: 1

=over 4

=item $self: (DBObject) (R)

the object.

=back

B<Return>: (DBObject)

The DBObject (evaluated to TRUE if save was successful and FALSE otherwise).

=cut

sub discardChanges
{
    my ($self) = @_;
    $self->{'_modified'} = {};
    return $self;
}




=pod

=head2 save

B<Description>: insert or update an object instance into the database.

B<ArgsCount>: 1-2

=over 4

=item $self: (DBObject) (R)

the object.

=item $alternate_update: (bool) (O)

If set to a true value, an alternative methode is used to try to update if it
already exists. Default method just insert the new value and updates in case of
duplicate keys. For tables where unique key constraints are not set, the default
method wont work so an alternative method is available in order to check if the
object entry already exists and needs to be updated.

=back

B<Return>: (DBObject)

The DBObject (evaluated to TRUE if save was successful and FALSE otherwise).

=cut

sub save
{
    my ($self, $alternate_update) = @_;
    
    # check if object is linked to a database
    if (!$self->{'_dbh'})
    {
        PrintDebug("WARNING: tried to save a non-database object (" . ref($self) . ")! Aborted!");
        return $self;
    }
    
    if (!scalar(keys(%{$self->{'_modified'}})))
    {
        PrintDebug("WARNING: Object not modified! Nothing to save!");
        return $self;
    }

    PrintDebug("Saving object '$self' (" . ref($self) . ")");
    my (@members, @member_updates, @space_holders, @values, %keys);

    my $key_name = $self->{'_properties'}->{'key'};
    # multiple keys?
    if ($key_name =~ m/$KEY_SEPARATOR/o)
    {
        # multiple, make sure each key is available
        if (!$self->getDBKeyValue())
        {
            confess "ERROR: cannot save object data (" . ref($self) . "): missing key values!\n";
        }
        # add multiple keys
        foreach my $subkey_name (split(/\s*$KEY_SEPARATOR\s*/o, $key_name))
        {
            if (!$self->{'_modified'}->{$subkey_name})
            {
                push(@members, $subkey_name);
                push(@space_holders, '?');
                push(@values, $self->{$subkey_name});
                push(@member_updates, "$subkey_name = ?");
            }
            # Add keys for alternate update.
            $keys{$subkey_name} = $self->{$subkey_name};
        }
    }
    elsif ($self->{$key_name})
    {
        if (!$self->{'_modified'}->{$key_name})
        {
            #single key, add key if one
            push(@members, $key_name);
            push(@space_holders, '?');
            push(@values, $self->{$key_name});
            push(@member_updates, "$key_name = ?");
        }
        # Add key for alternate update.
        $keys{$key_name} = $self->{$key_name};
    }

    # get available member values
    foreach my $member (keys(%{$self->{'_modified'}}))
    {
        if (exists($self->{$member}))
        {
            push(@members, $member);
            # defined or NULL?
            if (defined($self->{$member}))
            {
                # for timestamps, no space-holders
                if ($self->{$member} =~
                    m/^
                        (?:
                            CURDATE
                            |CURRENT_DATE
                            |CURRENT_TIME
                            |CURRENT_TIMESTAMP
                            |CURTIME
                            |LOCALTIME
                            |LOCALTIMESTAMP
                            |NOW
                            |UNIX_TIMESTAMP
                            |UTC_DATE
                            |UTC_TIME
                            |UTC_TIMESTAMP
                            |SYSDATE
                        )
                        (?:\(\s*\))? # optional parentheses
                    $/ix)
                {
                    push(@space_holders, $self->{$member});
                    push(@member_updates, "$member = " . $self->{$member});
                    delete($self->{$member}); # reload/compute next time accessed
                }
                else
                {
                    push(@space_holders, '?');
                    my $value = $self->{$member};
                    # check for serialized data
                    if ($self->{'_properties'}->{'base'}->{$member}->{'serialized'})
                    {
                        # serialize value
                        $value = FreezeData($value);
                    }
                    push(@values, $value);
                    push(@member_updates, "$member = ?");
                }
            }
            else
            {
                push(@space_holders, 'NULL');
                push(@member_updates, "$member = NULL");
            }
        }
    }

    my $sql_query;
    # Alternate update
    if ($alternate_update)
    {
        # Check if element already exists.
        $sql_query =
            "SELECT TRUE FROM "
            . $self->{'_properties'}->{'table'}
            . " WHERE "
            . join(' AND ', map {$_ . ' = ?'} keys(%keys))
            . ";";
        PrintDebug("SQL Query: $sql_query\nSQL Bindings (" . join(', ', values(%keys)) . ')');

        my $exists = $self->{'_dbh'}->selectall_arrayref($sql_query, { 'Slice' => {} }, values(%keys))
            or confess "ERROR: in query:\n$sql_query\nWith bindings: " . join(', ', values(%keys)) . "\n" . ($self->{'_dbh'}->errstr || '');
        if (@$exists)
        {
            $sql_query =
                "UPDATE " . $self->{'_properties'}->{'table'}
                . ' SET '
                . join(',', @member_updates)
                . " WHERE "
                . join(' AND ', map {$_ . ' = ?'} keys(%keys))
                . ";";
            PrintDebug("SQL Query: $sql_query\nSQL Bindings: '" . join("', '", @values, values(%keys)) . "'");

            # we double values for both insert and update space-holders
            $self->{'_dbh'}->do($sql_query, undef, @values, values(%keys))
                or confess "ERROR: in query:\n$sql_query\nWith bindings: " . join(', ', @values, values(%keys)) . "\n" . ($self->{'_dbh'}->errstr || '');
        }
        else
        {
            $sql_query =
                "INSERT INTO " . $self->{'_properties'}->{'table'}
                . " (" . join(', ', @members)
                . ") VALUE (" . join(', ', @space_holders)
                . ");";
            PrintDebug("SQL Query: $sql_query\nSQL Bindings (" . scalar(@values) . " values)" . (@values ? ': (' . join(', ', @values) . ')':''));

            # we double values for both insert and update space-holders
            $self->{'_dbh'}->do($sql_query, undef, @values)
                or confess "ERROR: in query:\n$sql_query\nWith bindings: " . join(', ', @values) . "\n" . ($self->{'_dbh'}->errstr || '');
        }
    }
    else
    {
        $sql_query =
            "INSERT INTO " . $self->{'_properties'}->{'table'}
            . " (" . join(', ', @members)
            . ") VALUE (" . join(', ', @space_holders)
            . ") ON DUPLICATE KEY UPDATE " . join(', ', @member_updates) . ";";
        PrintDebug("SQL Query: $sql_query\nSQL Bindings (" . (2 * scalar(@values)) . " values)" . (@values ? ': (' . join(', ', @values, @values) . ')':''));

        # we double values for both insert and update space-holders
        $self->{'_dbh'}->do($sql_query, undef, @values, @values)
            or confess "ERROR: in query:\n$sql_query\nWith bindings: " . join(', ', @values, @values) . "\n" . ($self->{'_dbh'}->errstr || '');
    }

    # single key and key is missing?
    if (($key_name !~ m/$KEY_SEPARATOR/o) && (!$self->{$key_name}))
    {
        $self->{$key_name} = $self->{'_dbh'}->last_insert_id(undef, undef, $self->{'_properties'}->{'table'}, $key_name);
        PrintDebug("New key set (" . ref($self) . "): " . $self->{$key_name});
    }
    $self->_initKeyValue();
    
    # clear modified flags
    $self->{'_modified'} = {};
    
    return $self;
}




=pod

=head2 reload

B<Description>: reload current instance members from database.

B<ArgsCount>: 1-2

=over 4

=item $self: (DBObject) (R)

the object.

=item $members: (array ref) (O)

an array of base member names to reload.

=back

B<Return>: (DBObject)

The DBObject (evaluated to TRUE if the object exists in database and FALSE otherwise).

=cut

sub reload
{
    my ($self, $members) = @_;
    
    if (!$self || !ref($self))
    {
        confess "ERROR: Invalid object for reload! Not a static function: use '->' operator to call it.\n";
    }
    
    if (!$self->{'_dbh'})
    {
        confess "ERROR: 'reload()' called on a non-database object!\n";
    }

    my $selectors = $self->_getSelfSelectors();

    if (!$members)
    {
        # clear all
        $members = [];
        foreach my $property_name (keys(%{$self}))
        {
             if ($property_name =~ m/^[a-z]/)
             {
                 push(@$members, $property_name);
             }
        }
        $self->{'_fetch'} = {};
        $self->{'_modified'} = {};
    }
    elsif ('ARRAY' ne ref($members))
    {
        confess "reload: invalid member argument!\n";
    }

    # clear previous values and check base members to reload
    my @base_members;
    foreach my $property_name (@$members)
    {
        if (exists($self->{'_properties'}->{'base'}->{$property_name}))
        {
            push(@base_members, $property_name);
        }
        else
        {
            delete($self->{'_fetch'}->{$property_name});
        }
        delete($self->{'_modified'}->{$property_name});
        delete($self->{$property_name});
    }


    # reload base members (others will be reloaded on demand)
    if (@base_members)
    {
        # qualify elements (add table alias and name columns)
        my ($qualified_selectors, $qualified_properties, $tables) = $self->_qualifyQueryElements($selectors, \@base_members, 1);

        # prepare table clause
        my $table_clause = $self->_prepareTableClause($tables);

        # reload properties from database
        $self->_fetchProperties($qualified_selectors, $qualified_properties, $table_clause, 0);

        foreach my $member (@base_members)
        {
            # check for serialized data
            if ($self->{'_properties'}->{'base'}->{$member}->{'serialized'})
            {
                # serialized, unserialize
                $self->{$member} = UnfreezeData($self->{$member});
            }
        }

        # reinit key (in case of multi-column keys)
        $self->_initKeyValue();

        PrintDebug("Object $self (" . ref($self) . ") cleared and reloaded.");
    }
    else
    {
        PrintDebug("Object $self (" . ref($self) . ") member(s) '" . join("', '", @$members) . "' cleared.");
    }

    return $self;
}




=pod

=head2 delete

B<Description>: remove current object from database and clear its id.

B<ArgsCount>: 1

=over 4

=item $self: (DBObject) (R)

the object.

=back

B<Return>: (DBObject)

The DBObject (evaluated to FALSE).

=cut

sub remove
{
    my ($self) = @_;
    
    # check if object is linked to a database
    if (!$self->{'_dbh'})
    {
        PrintDebug("WARNING: tried to remove a non-database object (" . ref($self) . ")! Aborted!");
        return $self;
    }

    my $selectors = $self->_getSelfSelectors();

    if (!$selectors)
    {
        PrintDebug("WARNING: tried to remove a non-saved object (" . ref($self) . ")! Aborted!");
        return $self;
    }
    
    
    my @selectors = keys(%$selectors);
    my @values;
    foreach my $key (@selectors)
    {
        push(@values, delete($self->{$key}));
    }

    my $sql_query = "
        DELETE FROM " . $self->{'_properties'}->{'table'} . "
        WHERE " . join(' AND ', map { "$_ = ?"} (@selectors)) . ";
    ";
    PrintDebug("SQL Query: $sql_query\nBindings: " . join(', ', @values));
    $self->{'_dbh'}->do($sql_query, undef, @values);

    # reinit key (in case of multi-column keys)
    $self->_initKeyValue();

    return $self;
}
sub delete; *delete = \&remove;





=pod

=head2 

B<Description>: 

B<ArgsCount>: 1

=over 4

=item $data: (hash ref) (R)

hash of elements to freeze.

=back

B<Return>: (string)

Frozen elements string that can be unfrozen by UnfreezeData.

=cut

sub FreezeData
{
    my ($data) = @_;
    
    if (!$data)
    {
        return undef;
    }

    my $freezable_hash = {};
    foreach my $element_name (keys(%$data))
    {
        # check if we got an object or an array of objects
        if ((ref($data->{$element_name}) eq 'ARRAY')
            && (@{$data->{$element_name}})
            && ref($data->{$element_name}->[0])
            && UNIVERSAL::can($data->{$element_name}->[0], 'isa')
            && $data->{$element_name}->[0]->isa('Greenphyl::DBObject')
           )
        {
            # array of GreenPhyl objects, translate them into hashes
            PrintDebug("Array of GreenPhyl objects to freeze");
            my @object_hashes;
            foreach my $object (@{$data->{$element_name}})
            {
                if ($object->getSelectors())
                {
                    push(@object_hashes,
                        {
                            'selectors' => $object->getSelectors(),
                            'type'   => ref($object),
                        }
                    );
                }
                else
                {
                    push(@object_hashes,
                        {
                            'object' => $object->getHashValue(1),
                            'type'   => ref($object),
                        }
                    );
                }
            }
            $freezable_hash->{$element_name} = \@object_hashes;
        }
        elsif (ref($data->{$element_name})
               && UNIVERSAL::can($data->{$element_name}, 'isa')
               && $data->{$element_name}->isa('Greenphyl::DBObject')
              )
        {
            # single GreenPhyl object
            PrintDebug("Single GreenPhyl object to freeze");
            if ($data->{$element_name}->getSelectors())
            {
                $freezable_hash->{$element_name} = {
                    'selectors' => $data->{$element_name}->getSelectors(),
                    'type'   => ref($data->{$element_name}),
                };
            }
            else
            {
                # hash object
                $freezable_hash->{$element_name} = {
                    'object' => $data->{$element_name}->getHashValue(1),
                    'type'   => ref($data->{$element_name}),
                };
            }
        }
        else
        {
            # other type of element, just copy
            PrintDebug("Other type of object to freeze (" . (ref($data->{$element_name}) || $data->{$element_name}) . ")");
            $freezable_hash->{$element_name} = $data->{$element_name};
        }
    }

    return nfreeze($freezable_hash);
}




=pod

=head2 

B<Description>: 

B<ArgsCount>: 1

=over 4

=item $data: (hash ref) (R)

hash of elements to freeze.

=back

B<Return>: (string)

Frozen elements string that can be unfrozen by UnfreezeData.

=cut

sub UnfreezeData
{
    my ($data) = @_;
    
    if (!$data)
    {
        # No data to unfreeze or data already unfreezed.
        return;
    }

    if (ref($data))
    {
        use Data::Dumper;
        confess "ERROR: Trying to unfreeze a non-string value!\n" . ref($data) . "\n" . Dumper([$data]);
    }
    
    my $unfrozen_hash = thaw($data);
    my %loaded_class;
    foreach my $element_name (keys(%$unfrozen_hash))
    {
        PrintDebug("Thawing on '$element_name'");
        # check if we got an object or an array of objects
        if ((ref($unfrozen_hash->{$element_name}) eq 'ARRAY')
            && (@{$unfrozen_hash->{$element_name}})
            && (ref($unfrozen_hash->{$element_name}->[0]) eq 'HASH')
            && (exists($unfrozen_hash->{$element_name}->[0]->{'type'}))
           )
        {
            # array of GreenPhyl objects, load objects
            if (!$unfrozen_hash->{$element_name}->[0]->{'type'})
            {
                cluck "ERROR: Unable to unfreeze object in '$element_name': no type provided!";
                next;
            }
            PrintDebug("Array of GreenPhyl objects to thaw ($element_name) / $unfrozen_hash->{$element_name}->[0]->{'type'}");
            my $objects = [];
            foreach my $object_hash (@{$unfrozen_hash->{$element_name}})
            {
                # check if the class has already been loaded
                if (!exists($loaded_class{$object_hash->{'type'}}))
                {
                    PrintDebug("Load GreenPhyl class for " . $object_hash->{'type'});
                    eval "use $object_hash->{'type'};";
                    if ($@)
                    {
                        cluck "Failed to load class '$object_hash->{'type'}'!\n";
                        $loaded_class{$object_hash->{'type'}} = 0;
                    }
                    else
                    {
                        $loaded_class{$object_hash->{'type'}} = 1;
                    }
                }

                if ($loaded_class{$object_hash->{'type'}})
                {
                    my $object;
                    my $object_dbh = GetDatabaseHandler();
                    # No database handler for external sources.
                    if ($object_hash->{'type'} =~ m/::Ext/)
                    {
                        $object_dbh = undef;
                    }
                    if (exists($object_hash->{'object'}) && $object_hash->{'object'})
                    {
                        PrintDebug("Loading object ($element_name) from frozen hash data");
                        $object = "$object_hash->{'type'}"->new(
                            $object_dbh,
                            {
                                'load' => 'none',
                                'members' => $object_hash->{'object'},
                            }
                        );
                    }
                    elsif (exists($object_hash->{'selectors'}) && $object_hash->{'selectors'})
                    {
                        PrintDebug("Loading object ($element_name) from database using selectors");
                        $object = "$object_hash->{'type'}"->new(
                            $object_dbh,
                            {
                                'selectors' => $object_hash->{'selectors'},
                            }
                        );
                        if ($object)
                        {
                            $object->setSelectors($object_hash->{'selectors'});
                        }
                    }

                    if ($object)
                    {
                        push(@$objects, $object);
                    }
                    else
                    {
                        PrintDebug("Unable to load frozen object '$element_name' (" . $object_hash->{'type'} . ")");
                    }
                }
            }
            $unfrozen_hash->{$element_name} = $objects;
        }
        elsif ((ref($unfrozen_hash->{$element_name}) eq 'HASH')
                && (exists($unfrozen_hash->{$element_name}->{'type'}))
              )
        {
            # single GreenPhyl object
            PrintDebug("Single GreenPhyl object to thaw ($element_name / $unfrozen_hash->{$element_name}->{'type'})");
            # load object
            eval "use $unfrozen_hash->{$element_name}->{'type'};";
            if ($@)
            {
                cluck "Failed to load class '$unfrozen_hash->{$element_name}->{'type'}'!\n";
                $loaded_class{$unfrozen_hash->{$element_name}->{'type'}} = 0;
            }
            else
            {
                $loaded_class{$unfrozen_hash->{$element_name}->{'type'}} = 1;
            }

            if ($loaded_class{$unfrozen_hash->{$element_name}->{'type'}})
            {
                my $object_dbh = GetDatabaseHandler();
                # No database handler for external sources.
                if ($unfrozen_hash->{$element_name}->{'type'} =~ m/::Ext/)
                {
                    $object_dbh = undef;
                }
                if ($unfrozen_hash->{$element_name}->{'object'})
                {
                    PrintDebug("Loading single object ($element_name) from frozen hash data");
                    $unfrozen_hash->{$element_name} = "$unfrozen_hash->{$element_name}->{'type'}"->new(
                        $object_dbh,
                        {
                            'load' => 'none',
                            'members' => $unfrozen_hash->{$element_name}->{'object'},
                        }
                    );
                }
                elsif (exists($unfrozen_hash->{$element_name}->{'selectors'}))
                {
                    PrintDebug("Loading single object ($element_name) from database using selectors");
                    my $selectors = $unfrozen_hash->{$element_name}->{'selectors'};
                    $unfrozen_hash->{$element_name} = "$unfrozen_hash->{$element_name}->{'type'}"->new(
                        $object_dbh,
                        {
                            'selectors' => $selectors,
                        }
                    );
                    if ($unfrozen_hash->{$element_name})
                    {
                        $unfrozen_hash->{$element_name}->setSelectors($selectors);
                    }
                }
                else
                {
                    PrintDebug("Unable to load frozen object '$element_name' (" . $unfrozen_hash->{$element_name}->{'type'} . "): no method given!");
                }

                if (!$unfrozen_hash->{$element_name})
                {
                    PrintDebug("Unable to load frozen object '$element_name'");
                }
            }
        }
    }

    return $unfrozen_hash;
}




=pod

=head1 AUTOLOAD

B<Description>: Autogeneration of setters and getters.

B<Example>:

    my $family = Greenphyl::Family->new($dbh, {'id' => 205}, {'sequence_count' => 1, 'black_listed' => 0});
    if ($family)
    {
        # here the autoload power kicks in...
        my $sequence_count = $family->getSeqCount();

        my $family_id = $family->id();
        # note: the following lines are equivalent:
        $family_id = $family->id;
        $family_id = $family->family_id;
        $family_id = $family->getId();
        $family_id = $family->getID();
        $family_id = $family->getFamilyId();

        # change family description
        $family->setDescription('This is a test family');

        # dynamically load a missing element:
        my $family_iprs = $family->getIPR();
    }

=cut

sub AUTOLOAD
{
    my ($self, $value) = @_;
    (my $method_name = $AUTOLOAD) =~ s/.*://; # strip fully-qualified portion
    return unless $method_name =~ m/[a-z]/;  # skip DESTROY and all-cap methods

    # static calls
    if (!ref($self))
    {
        confess "ERROR: unknown/unhandled static method $method_name!\n";
    }

    # not an object
    if (ref($self) =~ m/^(ARRAY|HASH|SCALAR|GLOB)$/)
    {
        confess "ERROR: not an object: $self\n";
    }

    my ($accessor, $member) = ($method_name =~ m/^(set|get|fetch|sort|count|)(.+)/);

    # implicit fetch?
    if (!$accessor && defined($value) && (ref($value) eq 'HASH'))
    {
        $accessor = 'fetch';
    }

    # convert member name using the method naming convention to variable
    # naming convention
    # id          -> id
    # Id          -> id
    # ID          -> id (all caps not allowed anyway...)
    # DBId        -> db_id
    # DB_Id       -> db_id
    # Family_Name -> family_name
    # Family_ID   -> family_id
    # Family_Id   -> family_id
    # FamilyID    -> family_id
    # FamilyId    -> family_id
    $member =~ s/([A-Z]*?)_*([A-Z](?:[a-z]|[A-Z]*$))/lc($1) . '_' . lc($2)/eg; # Turn 'T' into '_t' as in 'TotoTutuID' -> 'toto_tutuID'
    $member =~ s/^_+//; # remove starting '_' if some

    # check for alias
    if (exists($self->{'_properties'}->{'alias'}->{$member}))
    {
        $member = $self->{'_properties'}->{'alias'}->{$member};
    }
    # PrintDebug("AUTOLOAD: method '$method_name' => accessor=$accessor, member=$member");

    my $return_values;
    my $handler_found = 0;
    # check if we got a fetch that we can process
    if (('fetch' eq $accessor)
        && ($return_values = $self->_processPropertyFetch($member, $value)))
    {
        # we were able to fetch the value, do nothing else
        # otherwise, try to fetch the value using other methods
        $handler_found = 1;
    }
      # check if the member exists as a base property
    elsif (exists($self->{'_properties'}->{'base'}->{$member}))
    {
        # it's a base property
        $return_values = $self->_processBaseProperty($member, $accessor, $value);
        $handler_found = 1;
    }
      # or as a seconday property
    elsif (exists($self->{'_properties'}->{'secondary'}->{$member}))
    {
        # it's a secondary property
        $return_values = $self->_processSecondaryProperty($member, $accessor, $value);
        $handler_found = 1;
    }
      # or as a tertiary property
    elsif (exists($self->{'_properties'}->{'tertiary'}->{$member}))
    {
        # it's a tertiary property
        $return_values = $self->_processTertiaryProperty($member, $accessor, $value);
        $handler_found = 1;
    }
      # or as a self-managed property
    elsif (exists($self->{'_properties'}->{'managed'}->{$member}))
    {
        # it's a managed property
        if (exists($self->{'_properties'}->{'managed'}->{$member}->{'function'}))
        {
            $return_values = $self->{'_properties'}->{'managed'}->{$member}->{'function'}->($self, $member, $accessor, $value);
        }
        elsif (exists($self->{'_properties'}->{'managed'}->{$member}->{'constant'}))
        {
            $return_values = $self->{'_properties'}->{'managed'}->{$member}->{'constant'};
        }
        else
        {
            confess "ERROR: invalid configuration for managed member field '$method_name' for object type " . ref($self) . "!\n";
        }
        $handler_found = 1;
    }

    if ($handler_found)
    {
        # PrintDebug("Got $member = $return_values");
        # if we got an array ref and users seems to expect a list of values
        # we return a list
        if (wantarray()
            && ('ARRAY' eq ref($return_values)))
        {
            # return a list instead of an array ref
            return @$return_values;
        }
        else
        {
            return $return_values;
        }
    }

    confess "ERROR: member field not defined or unknown method '$method_name' for object type " . ref($self) . "!\n";
    # return undef;
}


=pod

=head1 DIAGNOSTICS

=head1 AUTHORS

Mathieu ROUARD (Bioversity), m.rouard@cgiar.org

Valentin GUIGNON (Bioversity), v.guignon@cgiar.org

Matthieu CONTE (Syngenta)

=head1 VERSION

Version 2.0.0

Date 05/12/2013

=head1 SEE ALSO

GreenPhyl documentation.

=cut

return 1; # package return
