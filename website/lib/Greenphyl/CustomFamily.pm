=pod

=head1 NAME

Greenphyl::CustomFamily - GreenPhyl CustomFamily Object

=head1 SYNOPSIS

    use Greenphyl::CustomFamily;
    my $custom_family = Greenphyl::CustomFamily->new($dbh, {'selectors' => {'name' => 'Test family',}});
    print $custom_family->accession();

=head1 REQUIRES

Perl5

=head1 EXPORTS

Nothing

=head1 DESCRIPTION

This module implements a GreenPhyl CustomFamily database object.

=cut

package Greenphyl::CustomFamily;

use strict;
use warnings;

use base qw(Greenphyl::AbstractFamily);

use vars qw($AUTOLOAD);

use Carp qw(cluck confess croak);

use Greenphyl;
use Greenphyl::Family;
use Greenphyl::Sequence;
use Greenphyl::Species;
use Greenphyl::Tools::Families;
use Greenphyl::Tools::Users; # password hash
use Greenphyl::Tools::Taxonomy;

use Storable qw(thaw);


# Package constants
####################

=pod

=head1 CONSTANTS

B<$OBJECT_PROPERTIES>: (hash ref)

object properties description. See DBObject constructor documentation for
details.

B<$SUPPORTED_DUMP_FORMAT>: (hash)

Contains supported dump format parameters.

=cut

our $DEBUG = 0;

our $OBJECT_PROPERTIES = {
    'table' => 'custom_families',
    'relationship' => {
        'table' => 'custom_family_relationships',
        'subject' => 'subject_custom_family_id',
        'object' => 'object_custom_family_id',
    },
    'key' => 'id',
    'alternate_keys' => ['accession'],
    'load'     => [
        'access',
        'accession',
        'creation_date',
        # 'data',
        'description',
        'flags',
        'id',
        'inferences',
        'name',
        'last_update',
        'level',
        'type',
        'user_id',
        'password_hash',
        'salt',
    ],
    'alias' => {
    },
    'base' => {
        'access' => {
            'property_column' => 'access',
        },
        'accession' => {
            'property_column' => 'accession',
        },
        'creation_date' => {
            'property_column' => 'creation_date',
        },
        'data' => {
            'property_column' => 'data',
            'serialized'      => 1,
        },
        'description' => {
            'property_column' => 'description',
        },
        'flags' => {
            'property_column' => 'flags',
        },
        'id' => {
            'property_column' => 'id',
        },
        'inferences' => {
            'property_column' => 'inferences',
        },
        'name' => {
            'property_column' => 'name',
        },
        'level' => {
            'property_column' => 'level',
        },
        'last_update' => {
            'property_column' => 'last_update',
        },
        'type' => {
            'property_column' => 'type',
        },
        'user_id' => {
            'property_column' => 'user_id',
        },
        'password_hash' => {
            'property_column' => 'password_hash',
        },
        'salt' => {
            'property_column' => 'salt',
        },
    },
    'secondary' => {
        'user' => {
            'object_key'     => 'user_id',
            'property_table'  => 'users',
            'property_key'    => 'id',
            'multiple_values' => 0,
            'property_module' => 'Greenphyl::User',
        },
        'sequence_count' => {
            'property_table'  => 'custom_families_sequences',
            'property_key'    => 'custom_family_id',
            'multiple_values' => 0,
            'property_column' => 'count(custom_sequence_id)',
        },
        'associated_family_ids' => {
            'property_table'  => 'custom_families_families_relationships',
            'property_key'    => 'custom_family_id',
            'multiple_values' => 1,
            'property_column' => 'family_id',
        },
    },
    'tertiary' => {
        'sequences' => {
            'link_table'        => 'custom_families_sequences',
            'link_object_key'   => 'custom_family_id',
            'link_property_key' => 'custom_sequence_id',
            'property_table'    => 'custom_sequences',
            'property_key'      => 'id',
            'multiple_values'   => 1,
            'property_module'   => 'Greenphyl::CustomSequence',
        },
        'families' => {
            'link_table'        => 'custom_families_families_relationships',
            'link_object_key'   => 'custom_family_id',
            'link_property_key' => 'family_id',
            'property_table'    => 'families',
            'property_key'      => 'id',
            'multiple_values'   => 1,
            'property_module'   => 'Greenphyl::Family',
        },
		'dbxref' => {
			'link_table'        => 'custom_families_dbxref',
			'link_object_key'   => 'custom_family_id',
			'link_property_key' => 'dbxref_id',
			'multiple_values'   => 1,
			'property_module'   => 'Greenphyl::DBXRef',
			'property_table'    => 'dbxref',
			'property_key'      => 'id',
		},
    },
    'managed' => {
        'plant_specific' => {
            'function' => \&operateNestedFamilyMember,
            'constant' => 0,
        },
        'alignment_length' => {
            'function' => \&operateNestedFamilyMember,
            'constant' => undef,
        },
        'alignment_sequence_count' => {
            'function' => \&operateNestedFamilyMember,
            'constant' => undef,
        },
        'average_sequence_length' => {
            'function' => \&operateNestedFamilyMember,
            'constant' => undef,
        },
        'branch_average' => {
            'function' => \&operateNestedFamilyMember,
            'constant' => undef,
        },
        'branch_median' => {
            'function' => \&operateNestedFamilyMember,
            'constant' => undef,
        },
        'branch_standard_deviation' => {
            'function' => \&operateNestedFamilyMember,
            'constant' => undef,
        },
        'masked_alignment_length' => {
            'function' => \&operateNestedFamilyMember,
            'constant' => undef,
        },
        'masked_alignment_sequence_count' => {
            'function' => \&operateNestedFamilyMember,
            'constant' => undef,
        },
        'max_sequence_length' => {
            'function' => \&operateNestedFamilyMember,
            'constant' => undef,
        },
        'median_sequence_length' => {
            'function' => \&operateNestedFamilyMember,
            'constant' => undef,
        },
        'min_sequence_length' => {
            'function' => \&operateNestedFamilyMember,
            'constant' => undef,
        },
    },
};

our %NESTED_FAMILY_MAPPING = (
    'ExtV3Family' => {
        'median_sequence_length'          => 'median_seq_length',
        'max_sequence_length'             => 'max_seq_length',
        'alignment_sequence_count'        => 'alignment_seq_count',
        'masked_alignment_sequence_count' => 'alignment_seq_count',
        'min_sequence_length'             => 'min_seq_length',
        'average_sequence_length'         => 'average_seq_length',
        'alignment_length'                => 'alignment_length',
        'masked_alignment_length'         => 'alignment_length',
    },
    'ExtV2Family' => {
    },
    'ExtV1Family' => {
    },
);

our $SUPPORTED_DUMP_FORMAT = $Greenphyl::AbstractFamily::SUPPORTED_DUMP_FORMAT;

our $CUSTOM_FAMILY_TYPE_NOT_AVAILABLE = 'N/A';
our $CUSTOM_FAMILY_TYPE_SUPERFAMILY   = 'superfamily'; # level 1
our $CUSTOM_FAMILY_TYPE_FAMILY        = 'family';      # level 2
our $CUSTOM_FAMILY_TYPE_SUBFAMILY     = 'subfamily';   # level 3
our $CUSTOM_FAMILY_TYPE_GROUP         = 'group';       # level 4

our $CUSTOM_FAMILY_ACCESS_PRIVATE         = 'private';
our $CUSTOM_FAMILY_ACCESS_GROUP_READ      = 'restricted to group - read only';
our $CUSTOM_FAMILY_ACCESS_GROUP_WRITE     = 'restricted to group - editable';
our $CUSTOM_FAMILY_ACCESS_PASSWORD_READ   = 'restricted by password - read only';
our $CUSTOM_FAMILY_ACCESS_PASSWORD_WRITE  = 'restricted by password - editable';
our $CUSTOM_FAMILY_ACCESS_PUBLIC_READ     = 'public - read only';
our $CUSTOM_FAMILY_ACCESS_PUBLIC_WRITE    = 'public - editable';
our @CUSTOM_FAMILY_ACCESS = (
    $CUSTOM_FAMILY_ACCESS_PRIVATE,
    $CUSTOM_FAMILY_ACCESS_GROUP_READ,
    $CUSTOM_FAMILY_ACCESS_GROUP_WRITE,
    $CUSTOM_FAMILY_ACCESS_PASSWORD_READ,
    $CUSTOM_FAMILY_ACCESS_PASSWORD_WRITE,
    $CUSTOM_FAMILY_ACCESS_PUBLIC_READ,
    $CUSTOM_FAMILY_ACCESS_PUBLIC_WRITE,
);

our $CUSTOM_FAMILY_FLAG_LISTED           = 'listed';          # should be displayed in lists of families (if matching)
our $CUSTOM_FAMILY_FLAG_LISTED_VALUE     = 1;
our $CUSTOM_FAMILY_FLAG_OBSOLETE         = 'obsolete';        # this family should not be used any more; See its relationships for replacement.
our $CUSTOM_FAMILY_FLAG_PEER_REVIEWED    = 'peer-reviewed';   # the family has been peer-reviewed
our $CUSTOM_FAMILY_FLAG_PANEL_VALIDATED  = 'panel-validated'; # the family has been validated by a panel
our $CUSTOM_FAMILY_FLAG_CONFLICTING_DATA = 'conflicting';     # some evidences show that the family is not what it says
our @CUSTOM_FAMILY_FLAG = (
    $CUSTOM_FAMILY_FLAG_LISTED,
    $CUSTOM_FAMILY_FLAG_OBSOLETE,
    $CUSTOM_FAMILY_FLAG_CONFLICTING_DATA,
    $CUSTOM_FAMILY_FLAG_PEER_REVIEWED,
    $CUSTOM_FAMILY_FLAG_PANEL_VALIDATED,
);




# Package subs
###############

=pod

=head1 STATIC METHODS

=head2 CONSTRUCTOR

B<Description>: Creates a new instance of a GreenPhyl CustomFamily object.

B<ArgsCount>: 2

=over 4

=item $dbh: (DBI::db) (U)

Database handler of the database to load the object from.

=item $parameters: (hash ref) (R)

See Greenphyl::DBObject constructor documentation for details.

=back

B<Return>: (Greenphyl::CustomFamily)

a new instance.

B<Caller>: General

B<Example>:

    my $custom_family = new Greenphyl::CustomFamily($dbh, {'selectors' => {'name' => 'Test family'}});

=cut

sub new
{
    my ($proto) = shift();
    my $class = ref($proto) || $proto;

    return $class->SUPER::new(@_);
}




=pod

=head1 METHODS

=head2 setUnfiltered

B<Description>: If true, disables automatic filtering: related custom families
will not be removed if current user has no read access.

B<ArgsCount>: 2

=over 4

=item $self: (Greenphyl::CustomFamily) (R)

Current family.

=item $unfiltered: (boolean) (R)

the new unfiltered status.

=back

B<Return>: (boolean)

Returns the unfiltered status.

B<Example>:

    $family->setUnfiltered(1);

=cut

sub setUnfiltered
{
    my ($self, $unfiltered) = @_;

    return $self->{'unfiltered'} = $unfiltered;
}


=pod

=head2 isUnfiltered

B<Description>: tells if related custom families of a custom family are filtered
or not. (filtered means current used needs at least read access on the related
families)

B<ArgsCount>: 1

=over 4

=item $self: (Greenphyl::CustomFamily) (R)

Current family.

=back

B<Return>: (boolean)

Returns true if related families are filtered.

=cut

sub isUnfiltered
{
    my ($self) = @_;

    return $self->{'unfiltered'};
}


=pod

=head2 hasReadAccess

B<Description>: Tells if the given user is allowed to view this custom family.

B<ArgsCount>: 1-2

=over 4

=item $self: (Greenphyl::CustomFamily) (R)

Current family.

=item $user: (Greenphyl::User) (O)

The user to check.
Default: current user.

=back

B<Return>: (boolean)

True if the user can view the family.

=cut

sub hasReadAccess
{
    my ($self, $user) = @_;

    if (IsRunningCGI())
    {
        eval "use Greenphyl::Web::Access; \$user ||= GetCurrentUser();";
    }

    my $has_access = 0;
    # check access
    if ($user && $user->id && ($user->id == $self->user_id) || IsAdmin())
    {
        # owner has full access
        $has_access = 1;
    }
    elsif ($CUSTOM_FAMILY_ACCESS_PRIVATE eq $self->access)
    {
        # private, only user can access
        $has_access = 0;
        # Throw('error' => "This family is private and only its author can view it!");
    }
    elsif ($CUSTOM_FAMILY_ACCESS_GROUP_READ eq $self->access)
    {
        # restricted to group - read only
        Throw('error' => "Not implemented yet!");
    }
    elsif ($CUSTOM_FAMILY_ACCESS_GROUP_WRITE eq $self->access)
    {
        # restricted to group - editable
        Throw('error' => "Not implemented yet!");
    }
    elsif (($CUSTOM_FAMILY_ACCESS_PASSWORD_READ eq $self->access)
           || ($CUSTOM_FAMILY_ACCESS_PASSWORD_WRITE eq $self->access))
    {
        # restricted by password - read only
        # restricted by password - editable
        my $session = Greenphyl::Web::GetSession();
        my $custom_family_access = $session->param('custom_family_access');
        $custom_family_access ||= {};
        if ($custom_family_access->{$self->id}
            && ($custom_family_access->{$self->id} eq $self->password_hash))
        {
            $has_access = 1;
        }
    }
    elsif (($CUSTOM_FAMILY_ACCESS_PUBLIC_READ eq $self->access)
           || ($CUSTOM_FAMILY_ACCESS_PUBLIC_WRITE eq $self->access))
    {
        # public - read only
        # public - editable by registered users
        $has_access = 1;
    }
    else
    {
        Throw('error' => "Unsupported custom family access type!", 'log' => "access: '" . $self->access . "'");
    }

    return $has_access;
}


=pod

=head2 hasWriteAccess

B<Description>: Tells if the given user is allowed to edit this custom family.

B<ArgsCount>: 1-2

=over 4

=item $self: (Greenphyl::CustomFamily) (R)

Current family.

=item $user: (Greenphyl::User) (O)

The user to check.
Default: current user.

=back

B<Return>: (boolean)

True if the user can edit the family.

=cut

sub hasWriteAccess
{
    my ($self, $user) = @_;
    
    if (IsRunningCGI())
    {
        eval "use Greenphyl::Web::Access; \$user ||= GetCurrentUser();";
    }
    
    my $has_access = 0;
    # check access
    if (!$user || !$user->id)
    {
        # anonymous are not allowed to edit families
        $has_access = 0;
    }
    elsif (($user->id == $self->user_id) || IsAdmin())
    {
        # owner has full access
        $has_access = 1;
    }
    elsif ($CUSTOM_FAMILY_ACCESS_PRIVATE eq $self->access)
    {
        # private, only user can access
        $has_access = 0;
        # Throw('error' => "This family is private and only its author can view it!");
    }
    elsif ($CUSTOM_FAMILY_ACCESS_GROUP_READ eq $self->access)
    {
        # restricted to group - read only
        $has_access = 0;
    }
    elsif ($CUSTOM_FAMILY_ACCESS_GROUP_WRITE eq $self->access)
    {
        # restricted to group - editable
        Throw('error' => "Not implemented yet!");
    }
    elsif ($CUSTOM_FAMILY_ACCESS_PASSWORD_READ eq $self->access)
    {
        # restricted by password - read only
        $has_access = 0;
    }
    elsif ($CUSTOM_FAMILY_ACCESS_PASSWORD_WRITE eq $self->access)
    {
        # restricted by password - editable
        my $session = Greenphyl::Web::GetSession();
        my $custom_family_access = $session->param('custom_family_access');
        $custom_family_access ||= {};
        if ($custom_family_access->{$self->id}
            && ($custom_family_access->{$self->id} eq $self->password_hash))
        {
            $has_access = 1;
        }
    }
    elsif ($CUSTOM_FAMILY_ACCESS_PUBLIC_READ eq $self->access)
    {
        # public - read only
        $has_access = 0;
    }
    elsif ($CUSTOM_FAMILY_ACCESS_PUBLIC_WRITE eq $self->access)
    {
        # public - editable by registered users
        $has_access = 1;
    }
    else
    {
        Throw('error' => "Unsupported custom family access type!", 'log' => "access: '" . $self->access . "'");
    }

    return $has_access;
}


=pod

=head2 isOwner

B<Description>: Tells if the given user is the owner of this custom family.

B<ArgsCount>: 1-2

=over 4

=item $self: (Greenphyl::CustomFamily) (R)

Current family.

=item $user: (Greenphyl::User) (O)

The user to check.
Default: current user.

=back

B<Return>: (boolean)

True if the user is the owner of the family.

=cut

sub isOwner
{
    my ($self, $user) = @_;
    
    if (IsRunningCGI())
    {
        eval "use Greenphyl::Web::Access; \$user ||= GetCurrentUser();";
    }
    
    my $is_owner = 0;
    
    if ($user && $user->id && ($user->id == $self->user_id))
    {
        $is_owner = 1;
    }
    
    return $is_owner;
}


=pod

=head2 password

B<Description>: Tells if the family has a password or sets it.

B<ArgsCount>: 1-2

=over 4

=item $self: (Greenphyl::CustomFamily) (R)

Current family.

=item $password: (string) (O)

The new password.

=back

B<Return>: (boolean)

True if the family has a password (returns password hash).

=cut

sub password
{
    my ($self, $password) = @_;

    if (defined($password) && ($password ne ''))
    {
        my ($password_hash, $salt) = HashPassword($password);
        $self->password_hash($password_hash);
        $self->salt($salt);

        eval
        {
            my $session = Greenphyl::Web::GetSession();
            my $custom_family_access = $session->param('custom_family_access');
            $custom_family_access ||= {};
            $custom_family_access->{$self->id} = $password_hash;
        };
    }

    return $self->password_hash;
}


=pod

=head2 species

B<Description>: Return species.

B<ArgsCount>: 1

=over 4

=item $self: (Greenphyl::CustomFamily) (R)

Current family.

=back

B<Return>: (array ref)

The list of Greenphyl::Species related to current family.

=cut

sub species
{
    my ($self) = @_;
    
    my @species_list;
    my $custom_species;
    # get regular species first
    my $sql_query = '
        SELECT DISTINCT ' . join(', ', Greenphyl::Species->GetDefaultMembers('s')) . '
        FROM species s
            JOIN custom_sequences seq ON (seq.species_id = s.id)
            JOIN custom_families_sequences fs ON (seq.id = fs.custom_sequence_id)
        WHERE fs.custom_family_id = ' . $self->id . '
    ';
    my $sql_species = $self->{'_dbh'}->selectall_arrayref($sql_query, { 'Slice' => {} })
        or confess 'ERROR: Failed to fetch custom family species: ' . $self->{'_dbh'}->errstr;
    foreach my $sql_species (@$sql_species)
    {
        # check for custom species
        my $species = Greenphyl::Species->new($self->{'_dbh'}, {'load' => 'none', 'members' => $sql_species});
        if (GP('NONUNIPROT_SPECIES_CODE') eq $species->code)
        {
            $custom_species = $species;
        }
        else
        {
            push(@species_list, $species);
        }
    }
    
    # get custom species
    if ($custom_species)
    {
        $sql_query = '
            SELECT seq.organism AS "organism", COUNT(seq.id) AS "sequence_count"
            FROM custom_sequences seq
                JOIN custom_families_sequences fs ON (seq.id = fs.custom_sequence_id)
                JOIN species s ON (seq.species_id = s.id)
            WHERE fs.custom_family_id = ' . $self->id . '
              AND s.code = \'' . GP('NONUNIPROT_SPECIES_CODE') . '\'
            GROUP BY seq.organism
        ';
        my $organisms = $self->{'_dbh'}->selectall_arrayref($sql_query, { 'Slice' => {} })
            or confess 'ERROR: Failed to fetch custom family custom species: ' . $self->{'_dbh'}->errstr;

        foreach my $organism (@$organisms)
        {
            my $species_data = $custom_species->getHashValue();
            $species_data->{'organism'} = $organism->{'organism'};
            $species_data->{'sequence_count'} = $organism->{'sequence_count'};
            $species_data->{'color'} = ComputeSpeciesColor($organism->{'organism'});
            # $species_data->{'common_name'} = '';
            my $species = Greenphyl::Species->new($self->{'_dbh'}, {'load' => 'none', 'members' => $species_data,});
            push(@species_list, $species);
        }
    }

    return \@species_list;
}


=pod

=head2 getAssociatedFamilies

B<Description>: returns the non-custom family objects related to this family.

B<ArgsCount>: 1

=over 4

=item $self: (Greenphyl::CustomFamily)

the sequence.

=back

B<Return>: (array ref)

the associated families (array of Greenphyl::Family).

=cut

sub getAssociatedFamilies
{
    my ($self, $type) = @_;

    my $associated_family_ids;
    if ($type)
    {
        $associated_family_ids = $self->fetch_associated_family_ids({'selectors' => {'type' => $type}});
    }
    else
    {
        $associated_family_ids = $self->associated_family_ids;
    }

    my @associated_families;
    foreach my $family_id (@$associated_family_ids)
    {
        my $family = Greenphyl::Family->new($self->{'_dbh'},{'selectors' => {'id' => $family_id,},},);
        if ($family)
        {
            push(@associated_families, $family);
        }
    }

    return \@associated_families;
}


=pod

=head2 hasAnalyzes

B<Description>: returns true if the user has provided alignment or phylogeny
data.
The string value returned correspond to the last analysis level provided:
* 'alignment':  only up to the alignment;
* 'phylogeny':  only up to the phylogeny;

B<Alias>: hasAnalyses

B<ArgsCount>: 1

=over 4

=item $self: (Greenphyl::CustomFamily) (R)

Current custom family.

=back

B<Return>: (string)

the last analysis level available (see string values in method description).
An empty string if no analysis is available.

=cut

sub hasAnalyzes
{
    my ($self) = @_;

    my $ALIGNMENT_LEVEL  = 'alignment';
    my $PHYLOGENY_LEVEL  = 'phylogeny';

    my $available_analysis_level = '';

    my $phylo_data = Greenphyl::Tools::Families::GenerateCustomPhylogenomicAnalysisData($self);

    if (($phylo_data->{'alignment_tree_url'}) || ($phylo_data->{'tree_url'}))
    {
        $available_analysis_level = $PHYLOGENY_LEVEL;
    }
    elsif ($phylo_data->{'alignment_url'})
    {
        $available_analysis_level = $ALIGNMENT_LEVEL;
    }
    
    return $available_analysis_level;
}
sub hasAnalyses; *hasAnalyses = \&hasAnalyzes;


=pod

=head2 operateNestedFamilyMember

B<Description>: Returns nested family (v1, v2 or v3) members.

B<ArgsCount>: 4

=over 4

=item $self: (Greenphyl::CustomFamily) (R)

Current family.

=item $member: (string) (R)

Member field.

=item $accessor: (string) (R)

Accessor to use.

=item $value: (scalar) (O)

Value to set or selection parameters or SQL directives or...

=back

B<Return>: (any type)

Returns the requested member if one or the 'constant' value if defined in the
managed member of $OBJECT_PROPERTIES or undef.

=cut

sub operateNestedFamilyMember
{
    my ($self, $member, $accessor, $value) = @_;
    
    my $values;
    my $method_name = $accessor . '_' . $member;
    my $nested_family;
    if ($self->data)
    {
        if (exists($self->data->{'family_v4'}))
        {
            $nested_family = $self->data->{'family_v4'};
        }
        elsif (exists($self->data->{'family_v3'}))
        {
            $nested_family = $self->data->{'family_v3'};
        }
        elsif (exists($self->data->{'family_v2'}))
        {
            $nested_family = $self->data->{'family_v2'};
        }
        elsif (exists($self->data->{'family_v1'}))
        {
            $nested_family = $self->data->{'family_v1'};
        }
    }

    if ($nested_family)
    {
        PrintDebug("Getting member '$member' of nested family '$nested_family' in $self");
        # check for mapping
        if ((exists($NESTED_FAMILY_MAPPING{$nested_family->getObjectType()}))
            && (exists($NESTED_FAMILY_MAPPING{$nested_family->getObjectType()}->{$member})))
        {
            $method_name = $accessor . '_' . $NESTED_FAMILY_MAPPING{$nested_family->getObjectType()}->{$member};
        }
        
        eval "\$values = \$nested_family->$method_name(\$value);";
        if ($@)
        {
            PrintDebug("Failed to get member '$member' ($self)");
            $values = $self->{'_properties'}->{'managed'}->{$member}->{'constant'};
        }
    }
    else
    {
        PrintDebug("No nested family available for member '$member'");
        $values = $self->{'_properties'}->{'managed'}->{$member}->{'constant'};
    }

    return $values;
}




=pod

=head1 AUTHORS

Mathieu ROUARD (Bioversity), m.rouard@cgiar.org

Valentin GUIGNON (Bioversity), v.guignon@cgiar.org

Matthieu CONTE (Syngenta)

Sebastien BRIOIS (Syngenta)

=head1 VERSION

Version 1.0.0

Date 29/08/2013

=head1 SEE ALSO

GreenPhyl documentation, DBObject documentation.

=cut

return 1; # package return
