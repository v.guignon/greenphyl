=pod

=head1 NAME

Greenphyl::MyListItem - GreenPhyl MyListItem Object

=head1 SYNOPSIS

    use Greenphyl::MyListItem;
    my $item = Greenphyl::MyListItem->new($dbh, {'selectors' => {'my_list_id' => 806, 'foreign_id' => 42}});
    print $item->type;

=head1 REQUIRES

Perl5

=head1 EXPORTS

Nothing

=head1 DESCRIPTION

This module implements a GreenPhyl MyListItem database object.

=cut

package Greenphyl::MyListItem;

use strict;
use warnings;

use base qw(Greenphyl::CachedDBObject);

use vars qw($AUTOLOAD);

use Carp qw(cluck confess croak);

use Greenphyl;




# Package constants
####################

=pod

=head1 CONSTANTS

B<$OBJECT_PROPERTIES>: (hash ref)

object properties description. See DBObject constructor documentation for
details.

=cut

our $OBJECT_PROPERTIES = {
    'table' => 'my_list_items',
    'key' => 'my_list_id,foreign_id,type',
    'alternate_keys' => [],
    'load'     => [
        'foreign_id',
        'my_list_id',
        'type',
    ],
    'alias' => {
    },
    'base' => {
        'foreign_id' => {
            'property_column' => 'foreign_id',
        },
        'my_list_id' => {
            'property_column' => 'my_list_id',
        },
        'type' => {
            'property_column' => 'type',
        },
    },
    'secondary' => {
        'mylist' => {
            'object_key'     => 'my_list_id',
            'property_table'  => 'my_lists',
            'property_key'    => 'id',
            'multiple_values' => 0,
            'property_module' => 'Greenphyl::MyList',
        },
    },
    'tertiary' => {
        'user' => {
            'object_key'        => 'my_list_id',
            'link_table'        => 'my_lists',
            'link_object_key'   => 'id',
            'link_property_key' => 'user_id',
            'property_table'    => 'users',
            'property_key'      => 'id',
            'multiple_values'   => 0,
            'property_module'   => 'Greenphyl::User',
        },
    },

};

our $ITEM_TYPES_TO_CLASS = {
    'Sequence'        => 'Greenphyl::Sequence',
    'Family'          => 'Greenphyl::Family',
    'CustomSequence'  => 'Greenphyl::CustomSequence',
    'CustomFamily'    => 'Greenphyl::CustomFamily',
};

our $DEBUG = 0;



# Package subs
###############

=pod

=head1 STATIC METHODS

=head2 CONSTRUCTOR

B<Description>: Creates a new instance of a GreenPhyl MyListItem object.

B<ArgsCount>: 2

=over 4

=item $dbh: (DBI::db) (U)

Database handler of the database to load the object from.

=item $parameters: (hash ref) (R)

See Greenphyl::DBObject constructor documentation for details.

=back

B<Return>: (Greenphyl::MyListItem)

a new instance.

B<Caller>: General

B<Example>:

    my $item = Greenphyl::MyListItem->new($dbh, {'selectors' => {'my_list_id' => 806, 'foreign_id' => 42}});

=cut

sub new
{
    my $proto = shift();
    my $class = ref($proto) || $proto;
    
    my $object_properties = $class->GetDefaultClassProperties();

    return $class->SUPER::new($object_properties, @_);
}




=pod

=head1 METHODS

=head2 getObject

B<Description>: returns the corresponding GreenPhyl specific object
corresponding to current item.

B<Alias>: object

B<ArgsCount>: 1

=over 4

=item $self: (Greenphyl::MyListItem) (R)

Current MyList item object.

=back

B<Return>: (Greenphyl::* ref)

a Greenphyl::* object.

=cut

sub getObject
{
    my ($self) = @_;

    my $object;
    # check if item type is supported
    if (exists($ITEM_TYPES_TO_CLASS->{$self->type}))
    {
        my $object_module = $ITEM_TYPES_TO_CLASS->{$self->type};
        my $object_properties;
        my $object_table;
        eval "use $object_module; \$object_properties = \$" . $object_module . "::OBJECT_PROPERTIES;";
        if ($@)
        {
            confess $@;
        }
        
        if (!$object_properties || !($object_table = $object_properties->{'table'}))
        {
            confess "ERROR: failed to get item table!";
        }

        my $sql_query = "
            SELECT " . join(', ', $object_module->GetDefaultMembers('o')) . "
            FROM $object_table o
            WHERE o." . $object_properties->{'key'} . " = ?
            ;
        ";
        PrintDebug("SQL Query: $sql_query\nBindings: " . $self->foreign_id);
        my $sql_object = $self->{'_dbh'}->selectrow_hashref($sql_query, undef, $self->foreign_id)
            or confess 'ERROR: Failed to fetch list item associated object: ' . $self->{'_dbh'}->errstr;
        $object = $object_module->new($self->{'_dbh'}, {'load' => 'none', 'members' => $sql_object});
    }
    elsif ($self->type)
    {
        confess "ERROR: unsupported item type '" . $self->type . "'!";
    }
    else
    {
        confess "ERROR: undefined item type!";
    }

    return $object;
}
sub object; *object = \&getObject;




=pod

=head1 AUTHORS

Mathieu ROUARD (Bioversity), m.rouard@cgiar.org

Valentin GUIGNON (Bioversity), v.guignon@cgiar.org

Matthieu CONTE (Syngenta)

=head1 VERSION

Version 1.0.0

Date 29/10/2013

=head1 SEE ALSO

GreenPhyl documentation, DBObject documentation.

=cut

return 1; # package return
