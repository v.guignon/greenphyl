=pod

=head1 NAME

Greenphyl::ExtV3Sequence - GreenPhyl v3 Sequence Object

=head1 SYNOPSIS

    use Greenphyl::ExtV3Sequence;
    my $v3_sequence = Greenphyl::ExtV3Sequence->new($dbh, {'selectors' => {'seq_textid' => ['LIKE', 'Os01g01050.1']}});
    print $v3_sequence->name();

=head1 REQUIRES

Perl 5.8.0, Greenphyl v3,
BioPerl

=head1 EXPORTS

nothing

=head1 DESCRIPTION

This module implements a GreenPhyl v3 sequence database object to use with an
external v3 database.

=cut

package Greenphyl::ExtV3Sequence;

use strict;
use warnings;

use base qw(Greenphyl::AbstractSequence);

use vars qw($AUTOLOAD);

use Carp qw(cluck confess croak);

use Greenphyl;




# Package constants
####################

=pod

=head1 CONSTANTS

B<$OBJECT_PROPERTIES>: (hash ref)

sequence object properties description. See DBObject constructor documentation
for details.

=cut

our $DEBUG = 0;

our $OBJECT_PROPERTIES = {
    'table' => 'sequences',
    'key' => 'seq_id',
    'alternate_keys' => ['seq_textid'],
    'load'     => [
        'seq_id',
        'seq_textid',
        'alias',
        'seq_length',
        'species_id',
        'annotation',
        'sequence',
        'hidden',
        'scanned',
        'gene_name',
        'synonym',
        'filtered',
    ],
    'alias' => {
        'id'           => 'seq_id',
        'sequence_id'  => 'seq_id',
        'length'       => 'seq_length',
        'name'         => 'seq_textid',
        'accession'    => 'seq_textid',
        'textid'       => 'seq_textid',
        'locus'        => 'gene_name',
        'polypeptide'  => 'sequence',
    },
    'base' => {
        'seq_id' => {
            'property_column' => 'seq_id',
        },
        'seq_textid' => {
            'property_column' => 'seq_textid',
        },
        'alias' => {
            'property_column' => 'alias',
        },
        'seq_length' => {
            'property_column' => 'seq_length',
        },
        'species_id' => {
            'property_column' => 'species_id',
        },
        'annotation' => {
            'property_column' => 'annotation',
        },
        'sequence' => {
            'property_column' => 'sequence',
        },
        'hidden' => {
            'property_column' => 'hidden',
        },
        'scanned' => {
            'property_column' => 'scanned',
        },
        'gene_name' => {
            'property_column' => 'gene_name',
        },
        'synonym' => {
            'property_column' => 'synonym',
        },
        'filtered' => {
            'property_column' => 'filtered',
        },
    },
    'secondary' => {
        'synonyms' => {
            'property_table'  => 'sequence_synonyms',
            'property_key'    => 'sequence_id',
            'multiple_values' => 1,
            'property_column' => 'synonym',
        },
        'species' => {
            'object_key'      => 'species_id',
            'property_table'  => 'species',
            'property_key'    => 'species_id',
            'multiple_values' => 0,
            'property_module' => 'Greenphyl::ExtV3Species',
        },
    },
    'tertiary' => {
        'families' => {
            'link_table'        => 'seq_is_in',
            'link_object_key'   => 'seq_id',
            'link_property_key' => 'family_id',
            'multiple_values'   => 1,
            'property_module'   => 'Greenphyl::ExtV3Family',
            'property_table'    => 'family',
            'property_key'      => 'family_id',
        },
        'dbxref' => {
            'link_table'        => 'sequence_dbxref',
            'link_object_key'   => 'seq_id',
            'link_property_key' => 'dbxref_id',
            'multiple_values'   => 1,
            'property_module'   => 'Greenphyl::ExtV3DBXRef',
            'property_table'    => 'dbxref',
            'property_key'      => 'dbxref_id',
        },
    },
};

our $SUPPORTED_DUMP_FORMAT = $Greenphyl::AbstractSequence::SUPPORTED_DUMP_FORMAT;




# Package subs
###############

=pod

=head1 STATIC METHODS

=head2 CONSTRUCTOR

B<Description>: Creates a new instance of a GreenPhyl v3 sequence object.

B<ArgsCount>: 1-2

=over 4

=item $dbh: (DBI) (U)

Database handler of the database to load the object from.

=item $parameters: (hash ref) (R)

See Greenphyl::DBObject constructor documentation for details.

=back

B<Return>: (Greenphyl::ExtV3Sequence)

a new instance.

B<Caller>: General

B<Example>:

    my $v3_sequence = new Greenphyl::Sequence($dbh, {'selectors' => {'seq_textid' => ['LIKE', 'Os01g01050%']}});

=cut

sub new
{
    my ($proto) = shift();
    my $class = ref($proto) || $proto;

    return $class->SUPER::new(@_);
}


=pod

=head1 METHODS

=head2 getStringValue

B<Description>: returns sequence name.

B<ArgsCount>: 1

=over 4

=item $self: (Greenphyl::AbstractSequence)

the sequence.

=back

B<Return>: (string)

the sequence name (seq_textid).

B<Caller>: internal

=cut

sub getStringValue
{
    my ($self) = @_;
    return $self->{'seq_textid'};
}


=pod

=head1 AUTHORS

Mathieu ROUARD (Bioversity), m.rouard@cgiar.org

Valentin GUIGNON (Bioversity), v.guignon@cgiar.org

Matthieu CONTE (Syngenta)

Sebastien BRIOIS (Syngenta)

=head1 VERSION

Version 1.0.0

Date 02/12/2013

=head1 SEE ALSO

GreenPhyl documentation.

=cut

return 1; # package return
