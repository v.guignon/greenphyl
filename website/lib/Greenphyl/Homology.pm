=pod

=head1 NAME

Greenphyl::Homology - GreenPhyl Homology Object

=head1 SYNOPSIS

    use Greenphyl::Homology;
    my $homology = Greenphyl::Homology->new($dbh, {'selectors' => {'family_id' => 205}});
    print $homology->type();

=head1 REQUIRES

Perl5

=head1 EXPORTS

Nothing

=head1 DESCRIPTION

This module implements a GreenPhyl Homology database object.

=cut

package Greenphyl::Homology;

use strict;
use warnings;

use base qw(Greenphyl::CachedDBObject);

use vars qw($AUTOLOAD);

use Carp qw(cluck confess croak);

use Greenphyl;



# Package constants
####################

=pod

=head1 CONSTANTS

B<$OBJECT_PROPERTIES>: (hash ref)

object properties description. See DBObject constructor documentation for
details.

=cut

our $OBJECT_PROPERTIES = {
    'table' => 'homologies',
    'key' => 'id',
    'alternate_keys' => [],
    'load'     => [
        'distance',
        'family_id',
        'id',
        'i_duplication',
        'kvalue',
        'score',
        'speciation',
        't_duplication',
        'type',
        'u_duplication',
    ],
    'alias' => {
        'homology_id' => 'id',
    },
    'base' => {
        'distance' => {
            'property_column' => 'distance',
        },
        'family_id' => {
            'property_column' => 'family_id',
        },
        'id' => {
            'property_column' => 'id',
        },
        'i_duplication' => {
            'property_column' => 'i_duplication',
        },
        'kvalue' => {
            'property_column' => 'kvalue',
        },
        'score' => {
            'property_column' => 'score',
        },
        'speciation' => {
            'property_column' => 'speciation',
        },
        't_duplication' => {
            'property_column' => 't_duplication',
        },
        'type' => {
            'property_column' => 'type',
        },
        'u_duplication' => {
            'property_column' => 'u_duplication',
        },
    },
    'secondary' => {
        'object_sequence_id' => {
            'property_table'  => 'homologs',
            'property_key'    => 'homology_id',
            'multiple_values' => 0,
            'property_column' => 'object_sequence_id',
        },
        'subject_sequence_id' => {
            'property_table'  => 'homologs',
            'property_key'    => 'homology_id',
            'multiple_values' => 0,
            'property_column' => 'subject_sequence_id',
        },
    },
    'tertiary' => {
        'sequences' => {
            'link_table'        => 'homologs',
            'link_object_key'   => 'homology_id',
            'link_property_key' => 'subject_sequence_id',
            'property_table'    => 'sequences',
            'property_key'      => 'id',
            'multiple_values'   => 1,
            'property_module'   => 'Greenphyl::Sequence',
        },
    },

};




# Package subs
###############

=pod

=head1 STATIC METHODS

=head2 CONSTRUCTOR

B<Description>: Creates a new instance of a GreenPhyl Homology object.

B<ArgsCount>: 2

=over 4

=item $dbh: (DBI::db) (U)

Database handler of the database to load the object from.

=item $parameters: (hash ref) (R)

See Greenphyl::DBObject constructor documentation for details.

=back

B<Return>: (Greenphyl::Homology)

a new instance.

B<Caller>: General

B<Example>:

    my $homology = new Greenphyl::Homology($dbh, {'selectors' => {'family_id' => 205}});

=cut

sub new
{
    my $proto = shift();
    my $class = ref($proto) || $proto;
    
    my $object_properties = $class->GetDefaultClassProperties();

    return $class->SUPER::new($object_properties, @_);
}


=pod

=head2 getHomologs

B<Description>: returns the array of sequences homolog to a given on.

B<Aliases>: homologs

B<ArgsCount>: 2

=over 4

=item $self: (Greenphyl::Homology) (R)

Current homology.

=item $sequence: (Greenphyl::Sequence) (R)

The known sequence from which the homologs are requested.

=back

B<Return>: (array ref)

Returns an array of sequence UniProt or an empty array.

=cut

sub getHomologs
{
    my ($self, $sequence) = @_;

    my %homologs;
    foreach my $homolog (@{$self->sequences()})
    {
        if ($homolog->id != $sequence->id)
        {
            $homologs{$homolog->id} = $homolog;
        }
    }

    return [values(%homologs)];
}
# Aliases
sub homologs; *homologs = \&getHomologs;




=pod

=head1 AUTHORS

Mathieu ROUARD (Bioversity), m.rouard@cgiar.org

Valentin GUIGNON (Bioversity), v.guignon@cgiar.org

Matthieu CONTE (Syngenta)

=head1 VERSION

Version 1.0.0

Date 16/05/2012

=head1 SEE ALSO

GreenPhyl documentation, DBObject documentation.

=cut

return 1; # package return
