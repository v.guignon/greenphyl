=pod

=head1 NAME

TreeNode - Unrooted or rooted tree node structure

=head1 SYNOPSIS

    # creates the tree structure:
    #    B
    #   /   D
    #  A   /
    #   \ /
    #    C---E
    #     \
    #      F
    #
    my $root_node = Greenphyl::TreeNode->loadNewickTree("(B:1,(D:2,E:1.5,F:1)C:1)A:1;");

=head1 REQUIRES

Perl5

=head1 DESCRIPTION

This class can be used to create rooted/unrooted trees in memory and can be
used to load Newick trees for instance. Nodes connected to a node 'X' are
stored in 'X' {NEIGHBORHOOD} array. The array contains a list of sub-arrays
containing info about each neighbor indexed using "$NEIGHBOR_REF_INDEX",
"$NEIGHBOR_BRANCH_SUPPORT_INDEX" and "$NEIGHBOR_BRANCH_LENGTH_INDEX".
"$NEIGHBOR_REF_INDEX" indexes the neighbor TreeNode object reference,
"$NEIGHBOR_BRANCH_SUPPORT_INDEX" indexes the branch support between 'X' and
its neighbor and "$NEIGHBOR_BRANCH_LENGTH_INDEX" indexes the length of the
branch between 'X' and its neighbor. Any TreeNode name, any branch support and
any branch length is never undef but may be an empty string.

Note: if an internal node name is a number, its value will be used as branch
support value and its name will be changed into an empty string.

=cut

package Greenphyl::TreeNode;


use strict;
use warnings;
use Carp qw (cluck confess croak);




# Package constants
####################

=pod

=head1 CONSTANTS

B<$NEIGHBOR_REF_INDEX>:  (integer, protected)
offset of the neighbor node reference in any array of the neighborhood.

B<$NEIGHBOR_BRANCH_SUPPORT_INDEX>:  (integer, protected)
data offset of support value of the branch between current node and a neighbor
node of any array of the neighborhood.

B<$NEIGHBOR_BRANCH_LENGTH_INDEX>:  (integer, protected)
data offset of the length of a branch between current node and a neighbor node
of any array of the neighborhood.

=cut

our $NEIGHBOR_REF_INDEX            = 0;
our $NEIGHBOR_BRANCH_SUPPORT_INDEX = 1;
our $NEIGHBOR_BRANCH_LENGTH_INDEX  = 2;




# Package subs
###############

=pod

=head1 STATIC METHODS

=head2 CONSTRUCTOR

B<Description>: Supports non-static calls.
Creates a new instance of TreeNode.

B<ArgsCount>: 1

=over 4

=item node_name: (array ref) U

node name (or label).

=back

B<Return>: Greenphyl::TreeNode

a new instance.

B<Caller>: general

B<Example>:

    my $node = Greenphyl::TreeNode->new('A');

=cut

sub new
{
    my ($proto, $node_name) = @_;
    my $class = ref($proto) || $proto;
    # parameters check
    if ((1 > @_) || (2 < @_))
    {
        confess "usage: my \$instance = Greenphyl::TreeNode->new(node_label);";
    }

    if (not defined $node_name)
    {
        $node_name = '';
    }

    # instance creation
    my $self = {};
    bless($self, $class);

    $self->{NEIGHBORHOOD} = [];
    $self->{NODE_NAME} = $node_name;

    return $self;
}




=pod

=head1 ACCESSORS

=head2 getName

B<Description>: Returns the node label.

B<ArgsCount>: 0

B<Return>: a string or undef if the node has no name.

B<Example>:

    my $node = Greenphyl::TreeNode->new('A');
    print "Node name: " . $node->getName() . "\n";

=cut

sub getName
{
    my ($self) = @_;
    # check parameters
    if ((1 != @_) || (not ref($self)))
    {
        confess "usage: my \$value = \$instance->getName();";
    }
    return $self->{NODE_NAME};
}




=pod

=head2 setName


B<Description>: set the label of the node.

B<ArgsCount>: 1

=over 4

=item name: (string) new node label.

=back

B<Example>:

    my $node = Greenphyl::TreeNode->new('A');
    $node->setName('B');
    print "Node A is now called " . $node->getName() . "\n"; # display "Node A is now called B"

=cut

sub setName
{
    my ($self, $name) = @_;
    # check parameters
    if ((2 != @_) || (not ref($self)))
    {
        confess "usage: \$instance->setName(name);";
    }
    # empty name?
    if (not defined $name)
    {
        $name = '';
    }
    $self->{NODE_NAME} = $name;
}




=pod

=head1 METHODS

=head2 loadTreeArray

B<Description>: supports both static and non-static calls.
loads a tree structure from a reference to a tree in an array
form. The node name is set to the value of the last element of the array.
A tree in an array form is an array following the rules below:

   array_tree_ref --> [subtree]
   subtree --> 'leaf_label' | internal_node | 'leaf_label', subtree | internal_node, subtree
   internal_node --> [subtree], 'node_label'

Note: node (and leaves) label can contain a distance between the node and its
parent. That distance is specified right after the label using colon (':')
followed by the distance value in float format. Ex.: 'A:12.5'

Note: if an internal node names is a number, its value will be used as branch
support and its name will be replaced by an empty string.

Note: a branch support can also be specified inside square brackets either
concatenated to the node name or its branch length. Ex.: 'A[0.75]:12.5' or
'A:12.5[0.75]'

B<ArgsCount>: 1-2

=over 4

=item tree_array_ref: (array ref) (R)
a reference to a tree in an array form.

=item parent_node: (Greenphyl::TreeNode) (U)
a reference to the parent node.

=back

B<Return>: (Greenphyl::TreeNode) self or a new node.

B<Example>:

    # creates the tree structure:
    #    B
    #   /   D
    #  A   /
    #   \ /
    #    C---E
    #     \
    #      F
    #
    my $node = Greenphyl::TreeNode->new();
    $node = $node->loadTreeArray([['B:1', ['D:2', 'E:1.5', 'F:1'], 'C:1'], 'A:1']); # now $node has for label 'A'
    # or
    my $other_node = Greenphyl::TreeNode->loadTreeArray([['B:1', ['D:2', 'E:1.5', 'F:1'], 'C:1'], 'A:1']);

=cut

sub loadTreeArray
{
    my ($self, $tree_array_ref, $parent_node_ref) = @_;
    # check parameters
    if ((2 > @_) || (3 < @_))
    {
        confess "usage: my \$node = Greenphyl::TreeNode->loadTreeArray([['B', 'C'], 'A']);";
    }

    # check for static calls
    if (not ref($self))
    {
        $self = Greenphyl::TreeNode->new();
    }

    #print "DEBUG: Working on subtree:\n" . Data::Dumper->Dump($tree_array_ref) . "--------------------\n\n"; #+debug

    # check argument
    if ((not defined $tree_array_ref) || (ref($tree_array_ref) ne 'ARRAY'))
    {
        confess "usage: my \$node = Greenphyl::TreeNode->loadTreeArray(array_ref);";
    }
    # check for an invalid array (last element must be a scalar)
    if ((1 > @$tree_array_ref)
        || (2 < @$tree_array_ref)
        || (ref($tree_array_ref->[$#$tree_array_ref]))
        || (not defined $tree_array_ref->[0])
        || (not defined $tree_array_ref->[$#$tree_array_ref])
        || ((2 == @$tree_array_ref) && ('ARRAY' ne ref($tree_array_ref->[0]))))
    {
        confess "invalid array ref provided to loadTreeArray. The array must contain 1 or 2 elements and the last element must be a defined scalar value. Any element being an array must follow the same rules.";
    }

    our $number_regexp = '[+\-]?(?:(?:\d*\.?\d+)|(?:\d+\.?\d*))(?:e[+\-]?\d+)?';
    # function used to extract support value from a branch length or a node name
    # takes 2 parameters (none must be undef):
    # -branch length string
    # -node name string
    # returns a list of 3 elements (none is undef):
    # -new branch length string
    # -new node name string
    # - branch support value
    #
    # Note: values are matched using m/^[+\-]?(?:(?:\d*\.?\d+)|(?:\d+\.?\d*))(?:e[+\-]?\d+)?$/i
    my $extractSupportValue;
    $extractSupportValue = sub
    {
        my ($branch_length, $node_name) = @_;
        # check if a branch support is stored in branch length (get what's inside the last square brackets if some)
        my ($support_value) = ($branch_length =~ m/\[([^\[]*)\]$/);
        if (defined $support_value)
        {
            # remove support value from branch length
            $branch_length =~ s/\[([^\[]*)\]$//;
        }
        else
        {
            # try to get support value from node name (get what's inside the last square brackets if some)
            ($support_value) = ($node_name =~ m/\[($number_regexp)\]$/oi);
            # if not found inside square brackets, check if node name is a number
            if (defined $support_value)
            {
                # remove support value from node name
                $node_name =~ s/\[([^\[]*)\]$//;
            }
            elsif ($node_name =~ m/^$number_regexp$/oi)
            {
                # got a number, use it as support value
                $support_value = $node_name;
                # clear node name
                $node_name = '';
            }
        }
        # check if a valid branch support value was found
        if (not defined $support_value)
        {
            # not valid, clear support value
            $support_value = '';
        }
        # check for NHX data
        my ($nhx_support) = ($support_value =~ m/^&&NHX[^\]]*:B=($number_regexp)/oi);
        if (defined $nhx_support)
        {
            # got NHX support value
            $support_value = $nhx_support;
        }
        elsif ($support_value !~ m/^$number_regexp$/oi)
        {
            # not a value, clear support value
            $support_value = '';
        }

        return ($branch_length, $node_name, $support_value);
    };

    my ($support_value, $branch_length) = ('', '');
    # clear neighborhood
    $self->{NEIGHBORHOOD} = []; # array of [$NEIGHBOR_REF_INDEX, $NEIGHBOR_BRANCH_SUPPORT_INDEX, $NEIGHBOR_BRANCH_LENGTH_INDEX]
    # set new name and get branch length
    ($self->{NODE_NAME}, $branch_length) = ($tree_array_ref->[$#$tree_array_ref] =~ m/^([^:]*):?(.*)$/);
    # check if it's an internal node ((one parent and at least a child) or (two children))
    if ((not $parent_node_ref)
        || ((2 == @$tree_array_ref)
            && (($parent_node_ref) || (2 == @{$tree_array_ref->[0]}))))
    {
        ($branch_length, $self->{NODE_NAME}, $support_value) = &$extractSupportValue($branch_length, $self->{NODE_NAME});
    }
    # check if a valid branch length was found
    if ($branch_length !~ m/^$number_regexp$/oi)
    {
        # not valid, clear length
        $branch_length = '';
    }


    # add parent node as neighbor
    if (defined $parent_node_ref)
    {
        #print "DEBUG: add parent node '$parent_node_ref->{NODE_NAME}' to neighborhood of '$self->{NODE_NAME}'\n"; #+debug
        push @{$self->{NEIGHBORHOOD}}, ([$parent_node_ref, $support_value, $branch_length]);
    }

    # add child nodes if some
    if (2 == @$tree_array_ref)
    {
        my $child_nodes = $tree_array_ref->[0];
        for (my $child_node_index = 0; @$child_nodes > $child_node_index; ++$child_node_index)
        {
            my $child_data = $child_nodes->[$child_node_index];
            my ($child_node_name, $child_support_value, $child_branch_length);
            # check wich kind of node we got
            if (ref($child_data) eq 'ARRAY')
            {
                # subtree, get subtree node
                ++$child_node_index; # note: the following node is the subtree root
                # get branch length
                ($child_node_name, $child_branch_length) = ($child_nodes->[$child_node_index] =~ m/^([^:]*):?(.*)$/);
                ($child_branch_length, $child_node_name, $child_support_value) = &$extractSupportValue($child_branch_length, $child_node_name);
                # check if a valid branch length was found
                if ($child_branch_length !~ m/^$number_regexp$/oi)
                {
                    $child_branch_length = '';
                }
                #print "DEBUG: add subtree '" . $child_node_name . "' to neighborhood of '$self->{NODE_NAME}'\n"; #+debug
                # add subtree node as neighbor (nb. [$NEIGHBOR_REF_INDEX, $NEIGHBOR_BRANCH_SUPPORT_INDEX, $NEIGHBOR_BRANCH_LENGTH_INDEX])
                push @{$self->{NEIGHBORHOOD}}, ([Greenphyl::TreeNode->loadTreeArray([$child_data, $child_nodes->[$child_node_index]], $self), $child_support_value, $child_branch_length]);
            }
            else
            {
                # leaf node
                # get leaf name and branch length
                ($child_node_name, $child_branch_length) = ($child_data =~ m/^([^:]*):?(.*)$/);
                # note: no branch support for leaves
                my $leaf_node = Greenphyl::TreeNode->new($child_node_name);
                # check if a valid branch length was found
                if ($child_branch_length !~ m/^$number_regexp$/oi)
                {
                    $child_branch_length = '';
                }
                #print "DEBUG: add leaf '$child_node_name' to neighborhood of '$self->{NODE_NAME}'\n"; #+debug
                # add leaf node as neighbor (nb. [$NEIGHBOR_REF_INDEX, $NEIGHBOR_BRANCH_SUPPORT_INDEX, $NEIGHBOR_BRANCH_LENGTH_INDEX])
                push @{$self->{NEIGHBORHOOD}}, ([$leaf_node, '', $child_branch_length]);
                #print "DEBUG: add node '$self->{NODE_NAME}' to neighborhood of '$leaf_node->{NODE_NAME}'\n"; #+debug
                push @{$leaf_node->{NEIGHBORHOOD}}, ([$self, '', $child_branch_length]);
            }
        }
    }
    return $self;
}


=pod

=head2 loadNewickTree

B<Description>: supports both static and non-static calls.
Loads a tree structure from a Newick string to a TreeNode object.

Note: a branch support can also be specified inside square brackets either
concatenated to the node name or its branch length. Ex.: 'A[0.75]:12.5' or
'A:12.5[0.75]'

B<ArgsCount>: 1

=over 4

=item newick_data: (string) (R)
a string containing the Newick.

=back

B<Return>: (Greenphyl::TreeNode) root node.

B<Example>:

    # creates the tree structure:
    #    B
    #   /   D
    #  A   /
    #   \ /
    #    C---E
    #     \
    #      F
    #
    my $node = Greenphyl::TreeNode->new();
    $node = $node->loadNewickTree("(B:1,(D:2,E:1.5,F:1)C:1)A:1;");
    # or
    my $other_node = Greenphyl::TreeNode->loadNewickTree("(B:1,(D:2,E:1.5,F:1)C:1)A:1;");

=cut

sub loadNewickTree
{
    my ($self, $newick_data) = @_;
    # check parameters
    if (2 != @_)
    {
        confess "usage: my \$node = Greenphyl::TreeNode->loadNewickTree(newick_string);";
    }

    # check for static calls
    if (not ref($self))
    {
        $self = Greenphyl::TreeNode->new();
    }

    # check argument
    if ((not defined $newick_data) || ref($newick_data))
    {
        confess "usage: my \$node = Greenphyl::TreeNode->loadNewickTree(array_ref);";
    }

    # reformat Newick into Perl data:
    # -remove EOL/CR chars
    $newick_data =~ s/[\n\r]//g;
    # -escape special characters
    $newick_data =~ s/\\/\\\\/g;
    $newick_data =~ s/'/\\'/g;
    # -put names and annotations inside simple quotes
    $newick_data =~ s/([(,)]?)\s*([^(,)]*)\s*([,);])/$1'$2'$3/g;
    $newick_data =~ s/([(,)])([,);])/$1''$2/g; # for follwing delimiters skipped in the line above (ie. got a $3$1 pattern but the regexp starts on $1 since $3 has been processed)
    $newick_data =~ s/([^\\])'\\'((?:\\'|[^'])*)\\'/$1'$2/g; # to remove quotes from quoted names
    # -add coma after closing parenthesis (except the last one and inside groups of closing parenthesis)
    $newick_data =~ s/\)([^;\)])/\),$1/g;
    # -replace parenthesis by brackets (ie. Phylip subtrees by Perl array ref)
    $newick_data =~ tr/\(\)/[]/;
    # -insert tree into an array ref
    $newick_data =~ s/^(.*);.*$/[$1];/m;

    # try to load the tree array from converted Newick data
    my $tree_array;
    eval "\$tree_array = $newick_data";
    # check for an error indicating an invalid format
    if ($@)
    {
        confess "Failed to load Newick tree! Parsing error!\n" . $@;
    }

    $self = $self->loadTreeArray($tree_array);

    return $self;
}




=pod

=head2 getNewickTree

B<Description>: returns a newick representation of the tree rooted from this
node.

B<ArgsCount>: 0

B<Return>: (string) the tree in Newick format.

B<Example>:

    #    B
    #   /   D
    #  A   /
    #   \ /
    #    C---E
    #     \
    #      F
    #
    my $node = Greenphyl::TreeNode->loadTreeArray([['B:1', ['D:2', 'E:1.5', 'F:1'], 'C:1'], 'A:1']);
    print $node->getNewickTree(); # prints "(B:1,(D:2,E:1.5,F:1)C:1)A:1;"

=cut

sub getNewickTree
{
    my ($self) = @_;
    # check parameters
    if ((1 > @_) || (2 < @_) || (not ref($self)))
    {
        confess "usage: my \$newick_string = \$node->getNewickTree();";
    }

    # function used to get Newick format of each subtree/node recursively
    my $GetNodeNewick;
    $GetNodeNewick = sub
    {
        my ($node, $parent_node) = @_;
        # check if node is an internal node
        if (1 < @{$node->{NEIGHBORHOOD}})
        {
            # internal node
            my $nodes_str = '';
            # loop on each neighbor
            foreach my $child_node_data (@{$node->{NEIGHBORHOOD}})
            {
                # get neighbor node object
                my $child_node = $child_node_data->[$NEIGHBOR_REF_INDEX];
                # get branch support value
                my $branch_support = $child_node_data->[$NEIGHBOR_BRANCH_SUPPORT_INDEX];
                # get branch length
                my $branch_length = $child_node_data->[$NEIGHBOR_BRANCH_LENGTH_INDEX];
                if ((not $parent_node) || ($parent_node != $child_node))
                {
                    # check for leaf
                    if (1 < @{$child_node->{NEIGHBORHOOD}})
                    {
                        # not leaf, add branch support inside square brackets to node name if there is a node name and a branch support
                        if (('' ne $child_node->getName()) && ('' ne $branch_support))
                        {
                            $nodes_str .= &$GetNodeNewick($child_node, $node) . "[$branch_support]";
                        }
                        else
                        {
                            # no name or no branch support, do not use square brackets
                            $nodes_str .= &$GetNodeNewick($child_node, $node) . $branch_support;
                        }
                    }
                    else
                    {
                        # leaf node, do not add branch support
                        $nodes_str .= &$GetNodeNewick($child_node, $node);
                    }
                    # add branch length if one
                    if ('' ne $branch_length)
                    {
                        $nodes_str .= ':' . $branch_length;
                    }
                    # close subtree
                    $nodes_str .= ',';
                }
            }
            # remove last ','
            $nodes_str =~ s/,$//;
            return "($nodes_str)" . $node->getName();
        }
        else
        {
            # leaf
            my $nodes_str = $node->getName();
            # check if leaf is tree root
            if ((1 == @{$node->{NEIGHBORHOOD}})
                && ((not $parent_node) || ($parent_node != $node->{NEIGHBORHOOD}->[0]->[$NEIGHBOR_REF_INDEX])))
            {
                # tree rooted on the leaf
                my $branch_length = $node->{NEIGHBORHOOD}->[0]->[$NEIGHBOR_BRANCH_LENGTH_INDEX];
                # add branch length
                if ('' ne $branch_length)
                {
                    $branch_length = ':' . $branch_length;
                }
                $nodes_str = '(' . &$GetNodeNewick($node->{NEIGHBORHOOD}->[0]->[$NEIGHBOR_REF_INDEX], $node) . $branch_length . ')' . $nodes_str;
            }
            return $nodes_str;
        }
    };
    my $newick_data = &$GetNodeNewick($self);
#    # check if the tree is rooted with an unamed root
#    if ((3 != @{$self->{NEIGHBORHOOD}}) && ('' eq $self->getName()))
#    {
#        # add root name and ";" to tree end
#        return $newick_data . "root;";
#    }
#    else
#    {
        # add ";" to tree end
        $newick_data =~ s/(.{1,80})([,)])/$1$2\n/g;
        chomp($newick_data);
        return "$newick_data;";
#    }
}


=pod

=head2 getNHXTree

B<Description>: returns a New Hampshire eXtended representation of the tree
rooted from this node (extended Newick).

B<ArgsCount>: 0

B<Return>: (string) the tree in NHX format.

B<Example>:

    #      B
    #     /   D
    # 90 A   /
    #     \ /
    #  75  C---E
    #       \
    #        F
    #
    my $node = Greenphyl::TreeNode->loadTreeArray([['B:1', ['D:2', 'E:1.5', 'F:1'], 'C:1'], 'A:1']);
    print $node->getNHXTree(); # prints "(B:1,(D:2,E:1.5,F:1)C:1[&&NHX:B=75])A:1[&&NHX:B=90];"

=cut

sub getNHXTree
{
    my ($self) = @_;
    # check parameters
    if ((1 > @_) || (2 < @_) || (not ref($self)))
    {
        confess "usage: my \$newick_ext_string = \$node->getNHXTree();";
    }

    #+FIXME: handle other NHX-specific codes than bootstrap values

    # function used to get NHX format of each subtree/node recursively
    my $GetNodeNHX;
    $GetNodeNHX = sub
    {
        my ($node, $parent_node) = @_;
        # check if node is an internal node
        if (1 < @{$node->{NEIGHBORHOOD}})
        {
            # internal node
            my $nodes_str = '';
            # loop on each neighbor
            foreach my $child_node_data (@{$node->{NEIGHBORHOOD}})
            {
                # get neighbor node object
                my $child_node = $child_node_data->[$NEIGHBOR_REF_INDEX];
                # get branch support value
                my $branch_support = $child_node_data->[$NEIGHBOR_BRANCH_SUPPORT_INDEX];
                # get branch length
                my $branch_length = $child_node_data->[$NEIGHBOR_BRANCH_LENGTH_INDEX];
                if ((not $parent_node) || ($parent_node != $child_node))
                {
                    $nodes_str .= &$GetNodeNHX($child_node, $node);
                    # add support and branch length if one
                    if ('' ne $branch_support)
                    {
                        $nodes_str .= ':' . $branch_length . "[&&NHX:B=$branch_support]";
                    }
                    elsif ('' ne $branch_length)
                    {
                        $nodes_str .= ':' . $branch_length;
                    }
                    # close subtree
                    $nodes_str .= ',';
                }
            }
            # remove last ','
            $nodes_str =~ s/,$//;
            return "($nodes_str)" . $node->getName();
        }
        else
        {
            # leaf
            my $nodes_str = $node->getName();
            # check if leaf is tree root
            if ((1 == @{$node->{NEIGHBORHOOD}})
                && ((not $parent_node) || ($parent_node != $node->{NEIGHBORHOOD}->[0]->[$NEIGHBOR_REF_INDEX])))
            {
                # tree rooted on the leaf
                my $branch_length = $node->{NEIGHBORHOOD}->[0]->[$NEIGHBOR_BRANCH_LENGTH_INDEX];
                # add branch length
                if ('' ne $branch_length)
                {
                    $branch_length = ':' . $branch_length;
                }
                $nodes_str = '(' . &$GetNodeNHX($node->{NEIGHBORHOOD}->[0]->[$NEIGHBOR_REF_INDEX], $node) . $branch_length . ')' . $nodes_str;
            }
            return $nodes_str;
        }
    };
    my $newick_data = &$GetNodeNHX($self);
    # check if the tree is rooted with an unamed root
    if ((3 != @{$self->{NEIGHBORHOOD}}) && ('' eq $self->getName()))
    {
        # add root name and ";" to tree end
        return $newick_data . "[&&NHX:E=1.1.1.1:D=N];";
    }
    else
    {
        # add ";" to tree end
        return "$newick_data;";
    }
}



=pod

=head2 getHTMLTree

B<Description>: returns an HTML unordered list representing current tree
hierarchy.

B<ArgsCount>: 0

B<Return>: (string) the tree in HTML format.

B<Example>:

    #      B
    #     /   D
    #    A   /
    #     \ /
    #      C---E
    #       \
    #        F
    #
    my $node = Greenphyl::TreeNode->loadTreeArray([['B:1', ['D:2', 'E:1.5', 'F:1'], 'C:1'], 'A:1']);
    print $node->getHTMLTree();
    # prints: 
    # <div class="node root-element" id="node_a"><span class="label">A</span>
    #   <ul>
    #     <li><div class="leaf first-element" id="node_b"><span class="label">B</span></div></li>
    #     <li>
    #       <div class="node last-element" id="node_c"><span class="label">C</span>
    #         <ul>
    #           <li><div class="leaf first-element" id="node_d"><span class="label">D</span></div></li>
    #           <li><div class="leaf middle-element" id="node_e"><span class="label">E</span></div></li>
    #           <li><div class="leaf last-element" id="node_f"><span class="label">F</span></div></li>
    #         </ul>
    #       </div>
    #     </li>
    #   </ul>
    # </div>

=cut

sub getHTMLTree
{
    my ($self) = @_;
    # check parameters
    if ((1 > @_) || (2 < @_) || (not ref($self)))
    {
        confess "usage: my \$html_string = \$node->getHTMLTree();";
    }


    # function used to get NHX format of each subtree/node recursively
    my $GetNodeHTML;
    $GetNodeHTML = sub
    {
        my ($node, $parent_node, $child_style, $indentation) = @_;

        # get children
        my @children_nodes = ();
        # loop on each neighbor
        foreach my $child_node_data (@{$node->{NEIGHBORHOOD}})
        {
            # get neighbor node object
            my $child_node = $child_node_data->[$NEIGHBOR_REF_INDEX];
            # make sure we only keep children
            if ((not $parent_node) || ($parent_node != $child_node))
            {
                push(@children_nodes, $child_node);
            }
        }

        # init HTML
        my $nodes_str = '';
        # generate an HTML id from the node name by replacing all non-word characters by '_',
        # lowercasing the name and prepending the string 'node_'
        # ex.: node 'Bla#123' will have HTML id 'node_bla_123'
        my $node_text_id = $node->getName();
        $node_text_id =~ s/\W/_/g;
        $node_text_id = 'node_' . lc($node_text_id);

        # check if node is an internal node
        if (@children_nodes)
        {
            $nodes_str = "$indentation<div class=\"node $child_style\" id=\"$node_text_id\">\n";
            $nodes_str   .= "$indentation  <span class=\"label\">" . $node->getName() . "</span>\n";

            # internal node
            $nodes_str .= "$indentation  <ul>\n";
            if (1 == @children_nodes)
            {
                # single child
                $nodes_str .= "$indentation    <li>\n";
                $nodes_str .= &$GetNodeHTML(shift(@children_nodes), $node, 'single-element', $indentation . '      ');
                $nodes_str .= "$indentation    </li>\n";
            }
            else
            {
                # first child
                $nodes_str .= "$indentation    <li>\n";
                $nodes_str .= &$GetNodeHTML(shift(@children_nodes), $node, 'first-element', $indentation . '      ');
                $nodes_str .= "$indentation    </li>\n";
                
                # get last child
                my $last_child = pop(@children_nodes);
                
                # middle children
                foreach my $child_node (@children_nodes)
                {
                    $nodes_str .= "$indentation    <li>\n";
                    $nodes_str .= &$GetNodeHTML($child_node, $node, 'middle-element', $indentation . '      ');
                    $nodes_str .= "$indentation    </li>\n";
                }

                # last child
                $nodes_str .= "$indentation    <li>\n";
                $nodes_str .= &$GetNodeHTML($last_child, $node, 'last-element', $indentation . '      ');
                $nodes_str .= "$indentation    </li>\n";
            }
            $nodes_str .= "$indentation  </ul>\n";
            $nodes_str .= "$indentation</div>\n";
        }
        else
        {
            # leaf
            $nodes_str .= "$indentation<div class=\"leaf $child_style\" id=\"$node_text_id\">";
            $nodes_str .= "<span class=\"label\">" . $node->getName() . "</span>";
            $nodes_str .= "</div>\n";
        }
        return $nodes_str;
    };
    return &$GetNodeHTML($self, $self, 'root-element', '');
}




=pod

=head2 isLeaf

B<Description>: tells if this node is a leaf.

B<ArgsCount>: 0

B<Return>: (boolean) 1 if the node is a leaf.

B<Example>:

    if ($node->isLeaf()) {print "Tree node is a leaf!\n";}

=cut

sub isLeaf
{
    my ($self) = @_;
    # check parameters
    if ((1 > @_) || (2 < @_) || (not ref($self)))
    {
        confess "usage: if (\$node->isLeaf()) {...}";
    }
    if (1 < @{$self->{NEIGHBORHOOD}})
    {
        # internal node
        return 0;
    }
    else
    {
        # leaf
        return 1;
    }
}




=pod

=head2 isInternalNode

B<Description>: tells if this nodes is an internal node.

B<ArgsCount>: 0

B<Return>: (boolean) 1 if the node is not a leaf.

B<Example>:

    if ($node->isInternalNode()) {print "Tree node is an internal node!\n";}

=cut

sub isInternalNode
{
    my ($self) = @_;
    # check parameters
    if ((1 > @_) || (2 < @_) || (not ref($self)))
    {
        confess "usage: if (\$node->isInternalNode()) {...}";
    }
    if (1 < @{$self->{NEIGHBORHOOD}})
    {
        # internal node
        return 1;
    }
    else
    {
        # leaf
        return 0;
    }
}




=pod

=head2 isNeighborNode

B<Description>: tells if the given nodes is a neighbor node of this one node.

B<ArgsCount>: 1

=over 4

=item node: (Greenphyl::TreeNode) (R)
the node to check.

=back

B<Return>: (boolean) 1 if the 2 nodes are neighbors, 0 otherwise.

B<Example>:

    if ($node1->isNeighborNode($node2)) {print "Theses nodes are neighbors!\n";}

=cut

sub isNeighborNode
{
    my ($self, $node) = @_;
    # check parameters
    if ((2 != @_) || (not ref($node)) || (not ref($self)))
    {
        confess "usage: if (\$node->isNeighborNode(other_node)) {...}";
    }
    my $node_index = 0;
    while ($node_index < @{$self->{NEIGHBORHOOD}})
    {
        if ($node == $self->{NEIGHBORHOOD}->[$node_index]->[$NEIGHBOR_REF_INDEX])
        {
            return 1;
        }
        ++$node_index;
    }

    return 0;
}




=pod

=head2 getNeighborNodesCount

B<Description>: returns the number of node directly connected to this one.

B<ArgsCount>: 0

B<Return>: (integer) the number of neighbor nodes.

B<Example>:

    print "This tree node has " . $node->getNeighborNodesCount() . " neighbors.\n";

=cut

sub getNeighborNodesCount
{
    my ($self) = @_;
    # check parameters
    if ((1 > @_) || (2 < @_) || (not ref($self)))
    {
        confess "usage: my \$nodes_count = \$node->getNeighborNodesCount();";
    }

    return scalar(@{$self->{NEIGHBORHOOD}});
}




=pod

=head2 getNeighborNodes

B<Description>: returns all the nodes directly connected to this one.

B<ArgsCount>: 0

B<Return>: (array of nodes) the neighbor nodes.

B<Example>:

    my @neighbor_nodes = $node->getNeighborNodes();

=cut

sub getNeighborNodes
{
    my ($self) = @_;
    # check parameters
    if ((1 > @_) || (2 < @_) || (not ref($self)))
    {
        confess "usage: my \@nodes = \$node->getNeighborNodes();";
    }

    my @nodes;
    foreach my $node_data (@{$self->{NEIGHBORHOOD}})
    {
        push @nodes, ($node_data->[$NEIGHBOR_REF_INDEX]);
    }
    return @nodes;
}



=pod

=head2 getNeighborNode

B<Description>: returns the requested neighbor node.

B<ArgsCount>: 1

=over 4

=item neighbor_number: (integer) (R)
the neighbor node index. This number must be above or equal to 0 and less than
getNeighborNodesCount() value.

=back

B<Return>: (Greenphyl::TreeNode) the requested neighbor node.

B<Example>:

    my $next_node = $node->getNeighborNode(1);

=cut

sub getNeighborNode
{
    my ($self, $neighbor_index) = @_;
    # check parameters
    if ((2 != @_) || (not ref($self)))
    {
        confess "usage: my \$neighbor_node = \$node->getNeighborNode(integer);";
    }
    $neighbor_index = int($neighbor_index);
    if ((0 > $neighbor_index) || (@{$self->{NEIGHBORHOOD}} <= $neighbor_index))
    {
        confess "invalid neighbor node index!";
    }

    return  $self->{NEIGHBORHOOD}->[$neighbor_index]->[$NEIGHBOR_REF_INDEX];
}




=pod

=head2 getNeighborNodeIndex

B<Description>: returns the index of the requested neighbor node.

B<ArgsCount>: 1

=over 4

=item neighbor_node: (Greenphyl::TreeNode) (R)
the neighbor node. Must be a neighbor node.

=back

B<Return>: (integer) the requested neighbor node index.

B<Example>:

    my $neighbor_node_index = $node->getNeighborNode($neighbor_node);

=cut

sub getNeighborNodeIndex
{
    my ($self, $neighbor_node) = @_;
    # check parameters
    if ((2 != @_) || (not ref($self)))
    {
        confess "usage: my \$node_index = \$node->getNeighborNodeIndex(neighbor_node);";
    }
    my $node_index = 0;
    while ($node_index < @{$self->{NEIGHBORHOOD}})
    {
        if ($neighbor_node == $self->{NEIGHBORHOOD}->[$node_index]->[$NEIGHBOR_REF_INDEX])
        {
            return $node_index;
        }
        ++$node_index;
    }
    confess "Failed to retrieve neighbor node index!";
}




=pod

=head2 getTreeNodesCount

B<Description>: returns the number of nodes in the tree.

B<ArgsCount>: 0-1

=over 4

=item ignore_nodes: (list of Greenphyl::TreeNode) (U)
a list of nodes (generaly only parent node) to ignore.

=back

B<Return>: (integer) the total number of tree nodes.

B<Example>:

    my $nodes_count = $node->getTreeNodesCount();

=cut

sub getTreeNodesCount
{
    my ($self, @ignore_nodes) = @_;
    # check parameters
    if ((1 > @_) || (not ref($self)))
    {
        confess "usage: my \$nodes_count = \$node->getTreeNodesCount();";
    }
    my $nodes_count = 1;
    # work on each neighbor node
    foreach my $neighbor_node_data (@{$self->{NEIGHBORHOOD}})
    {
        my $neighbor_node = $neighbor_node_data->[$NEIGHBOR_REF_INDEX];
        # check if node should be ignored
        my $ignore_list_index = 0;
        while (($ignore_list_index < @ignore_nodes) && ($ignore_nodes[$ignore_list_index] != $neighbor_node))
        {
            if ((not ref($ignore_nodes[$ignore_list_index])) || ('Greenphyl::TreeNode' ne ref($ignore_nodes[$ignore_list_index])))
            {
                confess "invalid node to ignore! (not a TreeNode)";
            }
            ++$ignore_list_index;
        }
        if ($ignore_list_index >= @ignore_nodes)
        {
            # node should not be ignored
            $nodes_count += $neighbor_node->getTreeNodesCount($self, @ignore_nodes);
        }
    }
    return $nodes_count;
}




=pod

=head2 getTreeNodes

B<Description>: returns the list of all the tree nodes.

B<ArgsCount>: 0-1

=over 4

=item ignore_nodes: (list of Greenphyl::TreeNode) (U)
a list of nodes (generaly only parent node) to ignore.

=back

B<Return>: (array of Greenphyl::TreeNode) the list of tree nodes.

B<Example>:

    #    B
    #   /   D
    #  A   /
    #   \ /
    #    C---E
    #     \
    #      F
    #
    my $node = Greenphyl::TreeNode->loadTreeArray([['B:1', ['D:2', 'E:1.5', 'F:1'], 'C:1'], 'A:1']);
    my @tree_nodes = $node->getTreeNodes();

=cut

sub getTreeNodes
{
    my ($self, @ignore_nodes) = @_;
    # check parameters
    if ((1 > @_) || (not ref($self)))
    {
        confess "usage: my \@nodes = \$node->getTreeNodes();";
    }
    my @nodes = ($self);
    # work on each neighbor node
    foreach my $neighbor_node_data (@{$self->{NEIGHBORHOOD}})
    {
        my $neighbor_node = $neighbor_node_data->[$NEIGHBOR_REF_INDEX];
        # check if node should be ignored
        my $ignore_list_index = 0;
        while (($ignore_list_index < @ignore_nodes) && ($ignore_nodes[$ignore_list_index] != $neighbor_node))
        {
            if ((not ref($ignore_nodes[$ignore_list_index])) || ('Greenphyl::TreeNode' ne ref($ignore_nodes[$ignore_list_index])))
            {
                confess "invalid node to ignore! (not a TreeNode)";
            }
            ++$ignore_list_index;
        }
        if ($ignore_list_index >= @ignore_nodes)
        {
            # node should not be ignored
            push @nodes, ($neighbor_node->getTreeNodes($self, @ignore_nodes));
        }
    }
    return @nodes;
}




=pod

=head2 getTreeNodesNames

B<Description>: returns the list of names of all the tree nodes.

B<ArgsCount>: 0-1

=over 4

=item ignore_nodes: (list of Greenphyl::TreeNode) (U)
a list of nodes (generaly only parent node) to ignore.

=back

B<Return>: (array of string) the list of names of tree nodes.

B<Example>:

    my @tree_nodes_names = $node->getTreeNodesNames();

=cut

sub getTreeNodesNames
{
    my ($self, @ignore_nodes) = @_;
    # check parameters
    if ((1 > @_) || (not ref($self)))
    {
        confess "usage: my \@nodes_names = \$node->getTreeNodesNames();";
    }
    my @nodes_names = ($self->getName());
    foreach my $neighbor_node_data (@{$self->{NEIGHBORHOOD}})
    {
        my $neighbor_node = $neighbor_node_data->[$NEIGHBOR_REF_INDEX];
        # check if node should be ignored
        my $ignore_list_index = 0;
        while (($ignore_list_index < @ignore_nodes) && ($ignore_nodes[$ignore_list_index] != $neighbor_node))
        {
            if ((not ref($ignore_nodes[$ignore_list_index])) || ('Greenphyl::TreeNode' ne ref($ignore_nodes[$ignore_list_index])))
            {
                confess "invalid node to ignore! (not a TreeNode)";
            }
            ++$ignore_list_index;
        }
        if ($ignore_list_index >= @ignore_nodes)
        {
            # node should not be ignored
            push @nodes_names, ($neighbor_node->getTreeNodesNames($self, @ignore_nodes));
        }
    }
    return @nodes_names;
}




=pod

=head2 getLeafNodesCount

B<Description>: returns the number of leaf nodes in the tree.

B<ArgsCount>: 0-1

=over 4

=item ignore_nodes: (list of Greenphyl::TreeNode) (U)
a list of nodes (generaly only parent node) to ignore.

=back

B<Return>: (integer) the number of leaves.

B<Example>:

    #    B
    #   /   D
    #  A   /
    #   \ /
    #    C---E
    #     \
    #      F
    #
    my $node = Greenphyl::TreeNode->loadTreeArray([['B:1', ['D:2', 'E:1.5', 'F:1'], 'C:1'], 'A:1']);
    my $leaf_nodes_count = $node->getLeafNodesCount(); # returns 4

=cut

sub getLeafNodesCount
{
    my ($self, @ignore_nodes) = @_;
    # check parameters
    if ((1 > @_) || (not ref($self)))
    {
        confess "usage: my \$leaves_count = \$node->getLeafNodesCount();";
    }
    my $leaves_count = 0;
    # check if this node is a leaf
    if (1 >= @{$self->{NEIGHBORHOOD}})
    {
        ++$leaves_count;
    }
    # work on each neighbor node
    foreach my $neighbor_node_data (@{$self->{NEIGHBORHOOD}})
    {
        my $neighbor_node = $neighbor_node_data->[$NEIGHBOR_REF_INDEX];
        # check if node should be ignored
        my $ignore_list_index = 0;
        while (($ignore_list_index < @ignore_nodes) && ($ignore_nodes[$ignore_list_index] != $neighbor_node))
        {
            if ((not ref($ignore_nodes[$ignore_list_index])) || ('Greenphyl::TreeNode' ne ref($ignore_nodes[$ignore_list_index])))
            {
                confess "invalid node to ignore! (not a TreeNode)";
            }
            ++$ignore_list_index;
        }
        if ($ignore_list_index >= @ignore_nodes)
        {
            # node should not be ignored
            $leaves_count += $neighbor_node->getLeafNodesCount($self, @ignore_nodes);
        }
    }
    return $leaves_count;
}




=pod

=head2 getLeafNodes

B<Description>: returns all the leaf nodes of the tree.

B<ArgsCount>: 0-1

=over 4

=item ignore_nodes: (list of Greenphyl::TreeNode) (U)
a list of nodes (generaly only parent node) to ignore.

=back

B<Return>: (array of Greenphyl::TreeNode) the list of leaves.

B<Example>:

    #    B
    #   /   D
    #  A   /
    #   \ /
    #    C---E
    #     \
    #      F
    #
    my $node = Greenphyl::TreeNode->loadTreeArray([['B:1', ['D:2', 'E:1.5', 'F:1'], 'C:1'], 'A:1']);
    my @leaf_nodes = $node->getLeafNodes();

=cut

sub getLeafNodes
{
    my ($self, @ignore_nodes) = @_;
    # check parameters
    if ((1 > @_) || (not ref($self)))
    {
        confess "usage: my \@leaves = \$node->getLeafNodes();";
    }
    my @leaves;
    # check if this node is a leaf
    if (1 >= @{$self->{NEIGHBORHOOD}})
    {
        $leaves[0] = $self;
    }
    # work on each neighbor node
    foreach my $neighbor_node_data (@{$self->{NEIGHBORHOOD}})
    {
        my $neighbor_node = $neighbor_node_data->[$NEIGHBOR_REF_INDEX];
        # check if node should be ignored
        my $ignore_list_index = 0;
        while (($ignore_list_index < @ignore_nodes) && ($ignore_nodes[$ignore_list_index] != $neighbor_node))
        {
            if ((not ref($ignore_nodes[$ignore_list_index])) || ('Greenphyl::TreeNode' ne ref($ignore_nodes[$ignore_list_index])))
            {
                confess "invalid node to ignore! (not a TreeNode)";
            }
            ++$ignore_list_index;
        }
        if ($ignore_list_index >= @ignore_nodes)
        {
            # node should not be ignored
            push @leaves, ($neighbor_node->getLeafNodes($self, @ignore_nodes));
        }
    }
    return @leaves;
}




=pod

=head2 getLeafNodesNames

B<Description>: returns all the names of tree leaves.

B<ArgsCount>: 0-1

=over 4

=item ignore_nodes: (list of Greenphyl::TreeNode) (U)
a list of nodes (generaly only parent node) to ignore.

=back

B<Return>: (array of string) the list of leaves label.

B<Example>:

    #    B
    #   /   D
    #  A   /
    #   \ /
    #    C---E
    #     \
    #      F
    #
    my $node = Greenphyl::TreeNode->loadTreeArray([['B:1', ['D:2', 'E:1.5', 'F:1'], 'C:1'], 'A:1']);
    print join(', ', $node->getLeafNodesNames()); # display "B, D, E, F"

=cut

sub getLeafNodesNames
{
    my ($self, @ignore_nodes) = @_;
    # check parameters
    if ((1 > @_) || (not ref($self)))
    {
        confess "usage: my \@leaves_names = \$node->getLeafNodesNames();";
    }
    my @leaves_names;
    # check if this node is a leaf
    if (1 >= @{$self->{NEIGHBORHOOD}})
    {
        $leaves_names[0] = $self->getName();
    }
    # work on each neighbor node
    foreach my $neighbor_node_data (@{$self->{NEIGHBORHOOD}})
    {
        my $neighbor_node = $neighbor_node_data->[$NEIGHBOR_REF_INDEX];
        # check if node should be ignored
        my $ignore_list_index = 0;
        while (($ignore_list_index < @ignore_nodes) && ($ignore_nodes[$ignore_list_index] != $neighbor_node))
        {
            if ((not ref($ignore_nodes[$ignore_list_index])) || ('Greenphyl::TreeNode' ne ref($ignore_nodes[$ignore_list_index])))
            {
                confess "invalid node to ignore! (not a TreeNode)";
            }
            ++$ignore_list_index;
        }
        if ($ignore_list_index >= @ignore_nodes)
        {
            # node should not be ignored
            push @leaves_names, ($neighbor_node->getLeafNodesNames($self, @ignore_nodes));
        }
    }
    return @leaves_names;
}




=pod

=head2 getInternalNodesCount

B<Description>: returns the number of internal nodes.

B<ArgsCount>: 0-1

=over 4

=item ignore_nodes: (list of Greenphyl::TreeNode) (U)
a list of nodes (generaly only parent node) to ignore.

=back

B<Return>: (integer) the count of internal nodes.

B<Example>:

    #    B
    #   /   D
    #  A   /
    #   \ /
    #    C---E
    #     \
    #      F
    #
    my $node = Greenphyl::TreeNode->loadTreeArray([['B:1', ['D:2', 'E:1.5', 'F:1'], 'C:1'], 'A:1']);
    my $internal_nodes_count = $node->getInternalNodesCount();

=cut

sub getInternalNodesCount
{
    my ($self, @ignore_nodes) = @_;
    # check parameters
    if ((1 > @_) || (not ref($self)))
    {
        confess "usage: my \$internal_nodes_count = \$node->getInternalNodesCount();";
    }
    my $inodes_count = 0;
    # check if more than a neighbor node
    if (1 < @{$self->{NEIGHBORHOOD}})
    {
        ++$inodes_count;
    }
    # work on each neighbor node
    foreach my $neighbor_node_data (@{$self->{NEIGHBORHOOD}})
    {
        my $neighbor_node = $neighbor_node_data->[$NEIGHBOR_REF_INDEX];
        # check if node should be ignored
        my $ignore_list_index = 0;
        while (($ignore_list_index < @ignore_nodes) && ($ignore_nodes[$ignore_list_index] != $neighbor_node))
        {
            if ((not ref($ignore_nodes[$ignore_list_index])) || ('Greenphyl::TreeNode' ne ref($ignore_nodes[$ignore_list_index])))
            {
                confess "invalid node to ignore! (not a TreeNode)";
            }
            ++$ignore_list_index;
        }
        if ($ignore_list_index >= @ignore_nodes)
        {
            # node should not be ignored
            $inodes_count += $neighbor_node->getInternalNodesCount($self, @ignore_nodes);
        }
    }
    return $inodes_count;
}




=pod

=head2 getInternalNodes

B<Description>: returns the list of internal nodes.

B<ArgsCount>: 0-1

=over 4

=item ignore_nodes: (list of Greenphyl::TreeNode) (U)
a list of nodes (generaly only parent node) to ignore.

=back

B<Return>: (array of Greenphyl::TreeNode) the list of internal nodes.

B<Example>:

    #    B
    #   /   D
    #  A   /
    #   \ /
    #    C---E
    #     \
    #      F
    #
    my $node = Greenphyl::TreeNode->loadTreeArray([['B:1', ['D:2', 'E:1.5', 'F:1'], 'C:1'], 'A:1']);
    my @internal_nodes = $node->getInternalNodes();

=cut

sub getInternalNodes
{
    my ($self, @ignore_nodes) = @_;
    # check parameters
    if ((1 > @_) || (not ref($self)))
    {
        confess "usage: my \@nodes = \$node->getInternalNodes();";
    }
    my @inodes;
    # check if more than a neighbor node
    if (1 < @{$self->{NEIGHBORHOOD}})
    {
        $inodes[0] = $self;
    }
    # work on each neighbor node
    foreach my $neighbor_node_data (@{$self->{NEIGHBORHOOD}})
    {
        my $neighbor_node = $neighbor_node_data->[$NEIGHBOR_REF_INDEX];
        # check if node should be ignored
        my $ignore_list_index = 0;
        while (($ignore_list_index < @ignore_nodes) && ($ignore_nodes[$ignore_list_index] != $neighbor_node))
        {
            if ((not ref($ignore_nodes[$ignore_list_index])) || ('Greenphyl::TreeNode' ne ref($ignore_nodes[$ignore_list_index])))
            {
                confess "invalid node to ignore! (not a TreeNode)";
            }
            ++$ignore_list_index;
        }
        if ($ignore_list_index >= @ignore_nodes)
        {
            # node should not be ignored
            push @inodes, ($neighbor_node->getInternalNodes($self, @ignore_nodes));
        }
    }
    return @inodes;
}





=pod

=head2 getInternalNodesNames

B<Description>: returns the list of internal nodes labels.

B<ArgsCount>: 0-1

=over 4

=item ignore_nodes: (list of Greenphyl::TreeNode) (U)
a list of nodes (generaly only parent node) to ignore.

=back

B<Return>: (array of string) the list of internal nodes labels.

B<Example>:

    #    B
    #   /   D
    #  A   /
    #   \ /
    #    C---E
    #     \
    #      F
    #
    my $node = Greenphyl::TreeNode->loadTreeArray([['B:1', ['D:2', 'E:1.5', 'F:1'], 'C:1'], 'A:1']);
    my @internal_nodes_names = $node->getInternalNodesNames();

=cut

sub getInternalNodesNames
{
    my ($self, @ignore_nodes) = @_;
    # check parameters
    if ((1 > @_) || (not ref($self)))
    {
        confess "usage: my \@nodes = \$node->getInternalNodesNames();";
    }
    my @inodes_names;
    # check if more than a neighbor node
    if (1 < @{$self->{NEIGHBORHOOD}})
    {
        $inodes_names[0] = $self->getName();
    }
    # work on each neighbor node
    foreach my $neighbor_node_data (@{$self->{NEIGHBORHOOD}})
    {
        my $neighbor_node = $neighbor_node_data->[$NEIGHBOR_REF_INDEX];
        # check if node should be ignored
        my $ignore_list_index = 0;
        while (($ignore_list_index < @ignore_nodes) && ($ignore_nodes[$ignore_list_index] != $neighbor_node))
        {
            if ((not ref($ignore_nodes[$ignore_list_index])) || ('Greenphyl::TreeNode' ne ref($ignore_nodes[$ignore_list_index])))
            {
                confess "invalid node to ignore! (not a TreeNode)";
            }
            ++$ignore_list_index;
        }
        if ($ignore_list_index >= @ignore_nodes)
        {
            # node should not be ignored
            push @inodes_names, ($neighbor_node->getInternalNodesNames($self, @ignore_nodes));
        }
    }
    return @inodes_names;
}




=pod

=head2 getBranchLength

B<Description>: Returns the length of the branch between current node and the
specified one.

Note: the value comes from the data loaded and may not be a number.

B<ArgsCount>: 1

=over 4

=item neighbor: (integer or Greenphyl::TreeNode) (R)
the neighor node or its index in this node neighborhood.
This index number must be above or equal to 0 and less than
this node getNeighborNodesCount() value.

=back

B<Return>: (string) the value of the length in a string.

B<Example>:

    my $length = $node->getBranchLength(1);

=cut

sub getBranchLength
{
    my ($self, $neighbor) = @_;
    # check parameters
    if ((2 != @_) || (not ref($self)))
    {
        confess "usage: my \$length = \$node->getBranchLength(integer/TreeNode);";
    }

    # check what kind of index the user provided
    if (ref($neighbor) eq 'Greenphyl::TreeNode')
    {
        my $neighbor_index = 0;
        while (($neighbor_index < @{$self->{NEIGHBORHOOD}}) && ($neighbor != $self->{NEIGHBORHOOD}->[$neighbor_index]->[$NEIGHBOR_REF_INDEX]))
        {
            ++$neighbor_index;
        }
        if ($neighbor_index >= @{$self->{NEIGHBORHOOD}})
        {
            confess "given node (" . $neighbor->getName() . ") is not a neighbor node of the current one (" . $self->getName() . ")!";
        }
        return $self->{NEIGHBORHOOD}->[$neighbor_index]->[$NEIGHBOR_BRANCH_LENGTH_INDEX];
    }
    else
    {
        my $neighbor_index = int($neighbor);
        # check index value
        if ((0 > $neighbor_index) || (@{$self->{NEIGHBORHOOD}} <= $neighbor_index))
        {
            confess "invalid neighbor node index!";
        }
        return $self->{NEIGHBORHOOD}->[$neighbor_index]->[$NEIGHBOR_BRANCH_LENGTH_INDEX];
    }
}




=pod

=head2 setBranchLength

B<Description>: Sets the length of the branch between current node and the
specified one.

Warning: this function does not set the value on the other side of the branch;
user has to call both $node1->setBranchLength($node2,...) and $node2->setBranchLength($node1,...)

B<ArgsCount>: 2

=over 4

=item neighbor: (integer or Greenphyl::TreeNode) (R)
the neighor node or its index in this node neighborhood.
This index number must be above or equal to 0 and less than
this node getNeighborNodesCount() value.

=item neighbor: (float) (U)
the new branch length value. If omitted, the length value is cleared.

=back

B<Example>:

    $node->setBranchLength($other_node, 0.85);

=cut

sub setBranchLength
{
    my ($self, $neighbor, $length) = @_;
    # check parameters
    if ((3 != @_) || (not ref($self)))
    {
        confess "usage: \$node->setBranchLength(integer/TreeNode, value);";
    }

    # check what kind of index the user provided
    if (ref($neighbor) eq 'Greenphyl::TreeNode')
    {
        my $neighbor_index = 0;
        while (($neighbor_index < @{$self->{NEIGHBORHOOD}}) && ($neighbor != $self->{NEIGHBORHOOD}->[$neighbor_index]->[$NEIGHBOR_REF_INDEX]))
        {
            ++$neighbor_index;
        }
        if ($neighbor_index >= @{$self->{NEIGHBORHOOD}})
        {
            confess "given node (" . $neighbor->getName() . ") is not a neighbor node if the current one (" . $self->getName() . ")!";
        }
        $self->{NEIGHBORHOOD}->[$neighbor_index]->[$NEIGHBOR_BRANCH_LENGTH_INDEX] = $length;
    }
    else
    {
        my $neighbor_index = int($neighbor);
        # check index value
        if ((0 > $neighbor_index) || (@{$self->{NEIGHBORHOOD}} <= $neighbor_index))
        {
            confess "invalid neighbor node index!";
        }
        $self->{NEIGHBORHOOD}->[$neighbor_index]->[$NEIGHBOR_BRANCH_LENGTH_INDEX] = $length;
    }
}




=pod

=head2 getBranchSupport

B<Description>: Returns the support of the branch between current node and the
specified one.

Note: the value comes from the data loaded and may not be a number.

B<ArgsCount>: 1

=over 4

=item neighbor: (integer or Greenphyl::TreeNode) (R)
the neighor node or its index in this node neighborhood.
This index number must be above or equal to 0 and less than
this node getNeighborNodesCount() value.

=back

B<Return>: (string) the support value in a string.

B<Example>:

    my $support = $node->getBranchSupport(1);

=cut

sub getBranchSupport
{
    my ($self, $neighbor) = @_;
    # check parameters
    if ((2 != @_) || (not ref($self)))
    {
        confess "usage: my \$support = \$node->getBranchSupport(integer/TreeNode);";
    }

    # check what kind of index the user provided
    if (ref($neighbor) eq 'Greenphyl::TreeNode')
    {
        my $neighbor_index = 0;
        while (($neighbor_index < @{$self->{NEIGHBORHOOD}}) && ($neighbor != $self->{NEIGHBORHOOD}->[$neighbor_index]->[$NEIGHBOR_REF_INDEX]))
        {
            ++$neighbor_index;
        }
        if ($neighbor_index >= @{$self->{NEIGHBORHOOD}})
        {
            confess "given node (" . $neighbor->getName() . ") is not a neighbor node if the current one (" . $self->getName() . ")!";
        }
        return $self->{NEIGHBORHOOD}->[$neighbor_index]->[$NEIGHBOR_BRANCH_SUPPORT_INDEX];
    }
    else
    {
        my $neighbor_index = int($neighbor);
        # check index value
        if ((0 > $neighbor_index) || (@{$self->{NEIGHBORHOOD}} <= $neighbor_index))
        {
            confess "invalid neighbor node index!";
        }
        return $self->{NEIGHBORHOOD}->[$neighbor_index]->[$NEIGHBOR_BRANCH_SUPPORT_INDEX];
    }
}




=pod

=head2 setBranchSupport

B<Description>: Sets the support of the branch between current node and the
specified one.

Warning: this function does not set the value on the other side of the branch;
user has to call both $node1->setBranchSupport($node2,...) and $node2->setBranchSupport($node1,...)

B<ArgsCount>: 2

=over 4

=item neighbor: (integer or Greenphyl::TreeNode) (R)
the neighor node or its index in this node neighborhood.
This index number must be above or equal to 0 and less than
this node getNeighborNodesCount() value.

=item neighbor: (float) (U)
the new branch support value. If omitted, the support value is cleared.

=back

B<Example>:

    $node->setBranchSupport($other_node, 0.85);

=cut

sub setBranchSupport
{
    my ($self, $neighbor, $support) = @_;
    # check parameters
    if ((3 != @_) || (not ref($self)))
    {
        confess "usage: \$node->setBranchSupport(integer/TreeNode, value);";
    }

    # check what kind of index the user provided
    if (ref($neighbor) eq 'Greenphyl::TreeNode')
    {
        my $neighbor_index = 0;
        while (($neighbor_index < @{$self->{NEIGHBORHOOD}}) && ($neighbor != $self->{NEIGHBORHOOD}->[$neighbor_index]->[$NEIGHBOR_REF_INDEX]))
        {
            ++$neighbor_index;
        }
        if ($neighbor_index >= @{$self->{NEIGHBORHOOD}})
        {
            confess "given node (" . $neighbor->getName() . ") is not a neighbor node if the current one (" . $self->getName() . ")!";
        }
        $self->{NEIGHBORHOOD}->[$neighbor_index]->[$NEIGHBOR_BRANCH_SUPPORT_INDEX] = $support;
    }
    else
    {
        my $neighbor_index = int($neighbor);
        # check index value
        if ((0 > $neighbor_index) || (@{$self->{NEIGHBORHOOD}} <= $neighbor_index))
        {
            confess "invalid neighbor node index!";
        }
        $self->{NEIGHBORHOOD}->[$neighbor_index]->[$NEIGHBOR_BRANCH_SUPPORT_INDEX] = $support;
    }
}




=pod

=head2 swapBranchSupportAndLength

B<Description>: swap branch support and branch length values in the whole tree.

B<ArgsCount>: 0

B<Example>:

    $node->swapBranchSupportAndLength();

=cut

sub swapBranchSupportAndLength
{
    my ($self) = @_;
    # check parameters
    if ((1 != @_) || (not ref($self)))
    {
        confess "usage: \$node->swapBranchSupportAndLength();";
    }


    # function used to swap branch values recursively
    my $SwapBranchValues;
    $SwapBranchValues = sub
    {
        my ($node, $parent_node) = @_;
        # loop on each neighbor
        foreach my $child_node_data (@{$node->{NEIGHBORHOOD}})
        {
            # get neighbor node object
            if ((not $parent_node) || ($parent_node != $child_node_data->[$NEIGHBOR_REF_INDEX]))
            {
                &$SwapBranchValues($child_node_data->[$NEIGHBOR_REF_INDEX], $node);
            }
            # swap branch values...
            # -get branch support value
            my $branch_support = $child_node_data->[$NEIGHBOR_BRANCH_SUPPORT_INDEX];
            # -get branch length
            my $branch_length = $child_node_data->[$NEIGHBOR_BRANCH_LENGTH_INDEX];
            # -swap
            if ($child_node_data->[$NEIGHBOR_REF_INDEX]->isLeaf() || $node->isLeaf())
            {
                # no support values for leaves
                $child_node_data->[$NEIGHBOR_BRANCH_SUPPORT_INDEX] = '';
            }
            else
            {
                $child_node_data->[$NEIGHBOR_BRANCH_SUPPORT_INDEX] = $branch_length;
            }
            $child_node_data->[$NEIGHBOR_BRANCH_LENGTH_INDEX] = $branch_support;
        }
    };
    &$SwapBranchValues($self);
}




=pod

=head2 attach

B<Description>: Connects a node to another.

B<ArgsCount>: 1-3

=over 4

=item attach_node: (Greenphyl::TreeNode) (R)
a node to attach to this one.

=item branch_length: (integer) (U)
the new branch length.

=item branch_support: (integer) (U)
the new branch support.

=back

B<Example>:

    $node->attach($other_node, 3.5, 0.85);

=cut

sub attach
{
    my ($self, $other_node, $branch_length, $branch_support) = @_;
    # check parameters
    if ((2 > @_) || (4 < @_) || (not ref($self)))
    {
        confess "usage: \$node->attach(other_node, distance, support);";
    }

    # check given node
    if ((not ref($other_node)) || ('Greenphyl::TreeNode' ne ref($other_node)))
    {
        confess "invalid node to attach! (not a TreeNode)";
    }

    # make sure we're not connecting the node to itself
    if ($self == $other_node)
    {
        confess "trying to attach a node to itself!";
    }

    # check if branch length was specified
    if (not defined $branch_length)
    {
        $branch_length = '';
    }
    # check if branch support was specified
    if (not defined $branch_support)
    {
        $branch_support = '';
    }
    # connect child
    push @{$other_node->{NEIGHBORHOOD}}, [$self, $branch_support, $branch_length];
    push @{$self->{NEIGHBORHOOD}}, [$other_node, $branch_support, $branch_length];
}




=pod

=head2 detach

B<Description>: Detach a node from its neighbor(s).

B<ArgsCount>: 0-n

=over 4

=item keep_nodes: (boolean) (U)
if set to 1, the following list of neighbor nodes should be kept.
Otherwise, these nodes should be the only ones detached.
If not set, 1 (true) is assumed.

=item neighbor_nodes: (list of Greenphyl::TreeNode) (U)
a list of neighbor nodes.

=back

B<Example>:

    $node->detach();

=cut

sub detach
{
    my ($self, $keep_nodes, @nodes) = @_;
    # check parameters
    if ((1 > @_) || (not ref($self)))
    {
        confess "usage: \$node->detach();";
    }

    if (not defined $keep_nodes)
    {
        $keep_nodes = 1;
    }

    # work on each neighbor node
    my $neighbor_node_index = 0;
    while ($neighbor_node_index < @{$self->{NEIGHBORHOOD}})
    {
        my $neighbor_node = $self->{NEIGHBORHOOD}->[$neighbor_node_index]->[$NEIGHBOR_REF_INDEX];
        # check if node is in the list
        my $list_index = 0;
        while (($list_index < @nodes) && ($nodes[$list_index] != $neighbor_node))
        {
            if ((not ref($nodes[$list_index])) || ('Greenphyl::TreeNode' ne ref($nodes[$list_index])))
            {
                confess "invalid node in the provided list! (not a TreeNode)";
            }
            ++$list_index;
        }
        if (($keep_nodes && ($list_index >= @nodes))
            || ((not $keep_nodes) && ($list_index < @nodes)))
        {
            # node should not be kept
            # remove this node from its neighbor, find its index in neighbor neighborhood
            my $this_node_index = 0;
            while (($this_node_index < @{$neighbor_node->{NEIGHBORHOOD}}) && ($neighbor_node->{NEIGHBORHOOD}->[$this_node_index]->[$NEIGHBOR_REF_INDEX] != $self))
            {
                ++$this_node_index;
            }
            if ($this_node_index < @{$neighbor_node->{NEIGHBORHOOD}})
            {
                splice(@{$neighbor_node->{NEIGHBORHOOD}}, $this_node_index, 1);
            }
            # remove neighbor from this node
            splice(@{$self->{NEIGHBORHOOD}}, $neighbor_node_index, 1);
            # note on $neighbor_node_index: we stay at the same position on the neighbor array since following nodes has been shifted
        }
        else
        {
            # node kept, process next one
            ++$neighbor_node_index;
        }
    }
}




=pod

=head2 flipSubtree

B<Description>: flip the substree rooted on current node (self) and going
through the given node.

B<ArgsCount>: 1

=over 4

=item node: (TreeNode) (R)
second node of the root branch of the subtree to be flipped.

=back

B<Example>:

    $node->flipSubtree($node->getNeighborNode(0));

=cut

sub flipSubtree
{
    my ($self, $node) = @_;
    # check parameters
    if ((2 != @_) || (not ref($self)))
    {
        confess "usage: \$node->flipSubtree(neighbor_node);";
    }

    # check for leaf
    if (2 < $node->getNeighborNodesCount())
    {
        # not a leaf, flip each child node
        my @neighborhood;
        my $this_node_data;
        foreach my $node_data (@{$node->{NEIGHBORHOOD}})
        {
            if ($node_data->[$NEIGHBOR_REF_INDEX] != $self)
            {
                # flip child node
                $node->flipSubtree($node_data->[$NEIGHBOR_REF_INDEX]);
                unshift @neighborhood, $node_data;
            }
            else
            {
                # do not go (back) on parent node
                $this_node_data = $node_data;
            }
        }
        # add parent node as first node
        unshift @neighborhood, $this_node_data;
        $node->{NEIGHBORHOOD} = \@neighborhood;
    }
}


=pod

=head2 swapSubtrees

B<Description>: swap the 2 substrees connected on the given node which do not
contain current node (self).

B<ArgsCount>: 2

=over 4

=item node: (TreeNode) (R)
node of the first subtree to be swapped.

=item node: (TreeNode) (R)
node of the second subtree to be swapped.

=back

B<Example>:

    $node->swapSubtrees($node->getNeighborNode(0));

=cut

sub swapSubtrees
{
    my ($self, $node1, $node2) = @_;
    # check parameters
    if ((3 != @_) || (not ref($self)))
    {
        confess "usage: \$node->swapSubtrees(neighbor_node1, neighbor_node2);";
    }

    if (!$self->isNeighborNode($node1) || !$self->isNeighborNode($node2))
    {
        confess "Invalid node argument! At least one of the 2 provided nodes is not child of current node!";
    }
    # swap subtrees
    my ($node1_data, $node2_data, $node1_index, $node2_index);
    for (my $node_index = 0; $node_index < @{$self->{NEIGHBORHOOD}}; ++$node_index)
    {
        if ($self->{NEIGHBORHOOD}->[$node_index]->[$NEIGHBOR_REF_INDEX] == $node1)
        {
            $node1_data = $self->{NEIGHBORHOOD}->[$node_index];
            $node1_index = $node_index;
        }
        elsif ($self->{NEIGHBORHOOD}->[$node_index]->[$NEIGHBOR_REF_INDEX] == $node2)
        {
            $node2_data = $self->{NEIGHBORHOOD}->[$node_index];
            $node2_index = $node_index;
        }
    }
    if ($node1_data && $node2_data)
    {
        $self->{NEIGHBORHOOD}->[$node1_index] = $node2_data;
        $self->{NEIGHBORHOOD}->[$node2_index] = $node1_data;
    }
    else
    {
        confess "Branch swap failed! Unable to find the 2 child nodes\n";
    }
}




=pod

=head1 AUTHORS

Valentin GUIGNON (LIRMM), valentin.guignon@lirmm.fr

=head1 VERSION

Version 0.10.6

Date 09/04/2008

=cut

return 1; # package return
