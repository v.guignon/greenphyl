=pod

=head1 NAME

Greenphyl - Contains GreenPhyl API

=head1 SYNOPSIS

use Greenphyl;

=head1 REQUIRES

Perl5

=head1 EXPORTS

GP Throw IsRunningCGI PrintDebug GetParameterValues GetAction HandleErrors
ConnectToDatabase GetDatabaseHandler SelectActiveDatabase
DisconnectDatabase GetDatabaseName TestTableExists GetTableInfo
GetTableColumnValues ResetDatabaseHandlers
SaveCurrentTransaction InsertIgnore InsertUpdate EscapeSQLRegExp
WriteTemporaryFile ValidateSequenceName
GeneratePagerData GetPagedObjects RemovePagerParameters
Prompt GetDuration GetCurrentDate GetProgress
GetURL GetWebFile

=head1 DESCRIPTION

This module contains GreenPhyl API (Application Programming Interface).
Functions provided here are used by GreenPhyl objects and scripts.

=cut

# for debugging: BEGIN { $Exporter::Verbose=1 };

package Greenphyl;

use strict;
use warnings;

use Carp qw (cluck confess croak);

use DBI;

use Greenphyl::Config;
use Greenphyl::Exceptions;

use Data::SpreadPagination;
use URI::Escape;

use base qw(Exporter);
our @EXPORT =
    qw(
        GP Throw IsRunningCGI PrintDebug GetParameterValues GetAction HandleErrors
        ConnectToDatabase GetDatabaseHandler SelectActiveDatabase
        DisconnectDatabase GetDatabaseName TestTableExists GetTableInfo
        GetTableColumnValues ResetDatabaseHandlers
        SaveCurrentTransaction InsertIgnore InsertUpdate EscapeSQLRegExp
        WriteTemporaryFile ValidateSequenceName
        GeneratePagerData GetPagedObjects RemovePagerParameters
        Prompt GetDuration GetCurrentDate GetProgress
        GetURL GetWebFile
    );




# Package constants
####################

=pod

=head1 CONSTANTS

B<$DEBUG>: (boolean)

Enables/disables local debug mode.

B<$CONFIG_NAMESPACE>: (string)

Name of the namespace from which config constant should be fetched.
See also GP function.

B<$IS_MOD_PERL>: (boolean)

Tells if GreenPhyl is currently running as Apache Perl mod (true).

B<$IS_CGI>: (boolean)

Tells if GreenPhyl is currently running from CGI (true) or command line (false).

=cut

our $DEBUG = 0;
our $CONFIG_NAMESPACE = 'Greenphyl::Config';
our $IS_MOD_PERL      = exists($ENV{'MOD_PERL'});
our $IS_CGI           = $IS_MOD_PERL || (exists($ENV{'GATEWAY_INTERFACE'}) && $ENV{'GATEWAY_INTERFACE'});
our $MAX_SEQUENCE_NAME_LENGTH   = 100;
our $CURRENT_PAGE_PARAMETER     = 'cp';
our $ENTRIES_PER_PAGE_PARAMETER = 'epp';
our $COMPRESSED_EXTENSIONS_REGEXP = '(?:\.zip|\.gz|\.bz2|\.tgz|\.tar\.gz|\.tbz|\.tar\.bz2)';




# Package variables
####################

=pod

=head1 VARIABLES

B<$g_active_dbh_index>: (integer)

Index of current (active) database.

B<$g_dbh_array>: (DBI)

Array of database handlers.

B<$g_table_info_cache>: (hash ref)

Cache hash of table info.

B<$g_last_progess>: (interger)

Last time progress has been returned.

=cut

my $g_active_dbh_index;
my $g_dbh_array        = [];
my $g_table_info_cache = {};
my $g_last_progess     = 0;




# Package subs
###############

=pod

=head1 FUNCTIONAL INTERFACE

=head2 GP

B<Description>: Returns the value of a GreenPhyl configuration constant. This is
a shortcut to the syntax "$Greenphyl::Config::CONSTANT_NAME". If the conctant
is not defined, it raise an error.

Note: the name of this function is in capital letters and does not start by an
action verb so it does not satisfy the function naming conventions but it's on
purpose: it's short, easy to remember and should be seen as the shortcut for
"GreenPhyl::Config::" namespace that returns constants.

B<ArgsCount>: 1

=over 4

=item $config_constant_name: (string) (R)

Name of the constant in GreenPhyl config file without namespace or type.

=back

B<Return>: (any type of scalar)

the configuration constant value.

B<Caller>: general

B<Example>:

    my $first_database_name = GP('DATABASES')->[0]{'name'};

=cut

sub GP
{
    my ($config_constant_name) = @_;
    no strict 'refs';
    my $constant_ref = \${$CONFIG_NAMESPACE . "::$config_constant_name"};
    # make sure the constant exists
    if (!defined($$constant_ref))
    {
        confess "ERROR: use of uninitialized config constant: '$config_constant_name'\n       Either it's a mistyping or the constant should be defined in lib/Greenphyl/Config.pm file.\n       Please check your code!\n";
    }
    return $$constant_ref;
}


=head2 Throw

B<Description>: throws a GreenPhyl exception that can be handled by the API.
This is a wrapper for the ThrowGreenphylError() defined by Exception::Class in
Greenphyl::Exceptions package.
Note: We use this wrapper to resolve some function package resolution error at
runtime.

B<ArgsCount>: 1

=over 4

=item @parameters: (hash) (R)

a hash (as list) containing error parameters. First requiered parameter is the
'error' parameter which contains the error message. The second optional
parameter is 'log' which should contain a message that will only be displayed
in logs and debug mode if enabled.

=back

B<Caller>: general

B<Example>:

    Throw('error' => 'I got this error message!', 'log' => 'and that is for log only');

=cut

sub Throw
{
    main::ThrowGreenphylError(@_);
}


=head2 IsRunningCGI

B<Description>: tells if running in CGI mode.

B<ArgsCount>: 0

B<Return>: (boolean)

true if running in CGI mode.

B<Caller>: general

B<Example>:

     if (IsRunningCGI())
     {
         ...
     }

=cut

sub IsRunningCGI
{
    return $IS_CGI;
}


=head2 PrintDebug

B<Description>: displays a message if DEBUG_MODE is set. If calling package or
script has a $DEBUG variable, that variable must be set to a true value in
order to display debug message.

Basically $Greenphyl::DEBUG_MODE enables/disables site-wide debug messages and
debug messages can be locally enabled/disabled inside packages usgin a package
variable "our $DEBUG = 0/1;". No need to test if local $DEBUG mode is set true
before calling PrintDebug.

$Greenphyl::DEBUG_MODE can be set to 0=no debug, 1=debug using warning and
2=debug with cluck (more verbose).

B<ArgsCount>: 1

=over 4

=item $message: (string) (R)

Message to display.

=back

B<Return>: nothing

B<Caller>: general

B<Example>:

     PrintDebug('We got here!');

=cut

sub _PrintNoDebug
{}

sub _PrintWarnDebug
{
    my ($message) = @_;
    # trim ending spaces and CR/LF
    $message =~ s/[\s\n\r]*$//;
    warn "DEBUG: $message\n";
}

sub _PrintCluckDebug
{
    my ($message) = @_;
    # trim ending spaces and CR/LF
    $message =~ s/[\s\n\r]*$//;
    cluck "DEBUG: $message\n";
}

sub _PrintDebug;

sub PrintDebug
{
    # check if caller module has a $DEBUG variable
    # and if it's the case, make sure that variable is set to a true value
    my ($package, $filename, $line) = caller();
    my $local_debug_mode;
    eval '$local_debug_mode = $' . $package . '::DEBUG;';
    if (defined($local_debug_mode) && (0 == $local_debug_mode))
    {
        # caller has a $DEBUG variable set to 0: do not log
        return;
    }

    # caller has no $DEBUG or $DEBUG is set to a true value: log debug message
    _PrintDebug(@_);
}


=head2 GetParameterValues

B<Description>: gets the a parameter value from command line from a given
optional namespace. It tries from the namespace first and then without.

The number of returned values depend on the calling context.

Note: This function is overridden by Greenphyl::Web::GetParameterValuesFromCGI
when using Greenphyl::Web and running in CGI mode.

B<ArgsCount>: 1-4

=over 4

=item $parameter_name: (string) (R)

Name of the parameter (case sensitive).

=item $namespace: (string) (U)

the namespace the parameter may belong to. Default: no namespace.

=item $split: (string) (O)

If set, split each value using the given expression. Default: no split.

=item $allow_file: (boolean) (O)

If set, values can be loaded from a file "FILENAME" when a parameter has a
value of the form "file:FILENAME".
Note: this feature is not supported in scalar (call) context.
IMPORTANT NOTE: when used, $split value must also be set in order to split file
values.

=back

B<Return>: in scalar context, a string containing the first parameter value
found or undef. In list context, a list of values or an empty list.

=cut

sub GetParameterValues
{
    my ($parameter_name, $namespace, $split, $allow_file) = @_;

    $namespace ||= '';
    if ($namespace)
    {
        $namespace .= '.';
    }

    PrintDebug("DEBUG: fetching command line parameter '$parameter_name' using namespace '$namespace'");

    if (wantarray())
    {
        my (@values, @valid_values);
        # get values using namespace
        if (@values = grep(m/^$namespace$parameter_name=/, @ARGV))
        {
            foreach my $value (@values)
            {
                if (($value) = ($value =~ m/^$namespace$parameter_name=(.+)/))
                {
                    push(@valid_values, $value);
                }
            }
        }

        # if it did not work with the namespace, try without
        if (!@values && $namespace && (@values = grep(m/^$parameter_name=/, @ARGV)))
        {
            foreach my $value (@values)
            {
                if (($value) = ($value =~ m/^$parameter_name=(.+)/))
                {
                    push(@valid_values, $value);
                }
            }
        }

        # check for split
        if (defined($split))
        {
            @valid_values = map {split(/$split/)} @valid_values;
            # process files
            if ($allow_file)
            {
                @valid_values = map
                    {
                        if ($_ =~ m/^file:(.*)/)
                        {
                            # open file
                            my $fh;
                            if ($1 && (-r $1) && open($fh, $1))
                            {
                                # get content
                                # note: we don't use "$/" to split records because we want to use regexp
                                local $/;
                                my $file_content = <$fh>;
                                # return the splitted content
                                split(/$split/, $file_content);
                            }
                            elsif ($1)
                            {
                                # return nothing
                                cluck "WARNING: unable to read input parameter file '$1'!\n";
                            }
                        }
                        else
                        {
                            # return original value
                            $_;
                        }
                    }
                    @valid_values
                ;
            }
        }
        # only return defined values
        return grep(defined, @valid_values);
    }
    else
    {
        my $value;
        if (($value) = grep(m/^$namespace$parameter_name=/, @ARGV))
        {
            ($value) = ($value =~ m/^$namespace$parameter_name=(.+)/);
        }
        if (!$value && $namespace)
        {
            if (($value) = grep(m/^$parameter_name=/, @ARGV))
            {
                ($value) = ($value =~ m/^$parameter_name=(.+)/);
            }
        }

        # check for split
        if (defined($split))
        {
            $value =~ s/$split.*//;
        }

        return $value;
    }
}


=pod

=head2 GetURL

B<Description>: Returns the requested URL with specified parameters in the query
string.
The URL parameter 'mmode'(=string) can be used to force links to use the given
mode.


B<ArgsCount>: 1-3

=item $url_name: (string) (R)

The requested URL name.

=item $query: (hash) (U)

Additional parameters to pass to the query string.

=item $parameters: (hash) (O)

Additional parameters.
-'path' (string): append the given string to the URL before the query string;
-'append' (string): a string to append to the URL;
-'anchor' (string): an anchor to append to the URL (do not include the '#').

=back

B<Return>: (string)

The full requested URL.

B<Caller>: general

B<Example>:

    # special URLs
    my $current_url = GetURL('current')); # without query string
    my $current_query_url = GetURL('current_query')); # includes query string
    # configured URLs (see Greenphyl::URLConfig)
    my $url = GetURL('documents', {'page' => 'about',}));
    my $taxonomy_url = GetURL('uniprot_taxonomy', undef, { 'path' => $species->taxonomy_id, });

=cut

sub GetURL
{
    my ($url, $query, $parameters) = @_;

    if (!$url || ref($url))
    {
        confess "ERROR: GetURL: Missing or invalid 'url' argument!\n";
    }
    elsif (!exists(GP('URLS')->{$url}))
    {
        confess "ERROR: requested URL '$url' does not exist! Check the name or update GreenPhyl config.\n";
    }

    my $full_url = GP('URLS')->{$url}->{'url'};
    $parameters ||= {};
    $query ||= {};

    # insert path if requested
    if ($parameters->{'path'})
    {
        $full_url =~ s/^(.*?)((?:\?.*)?)$/$1$parameters->{'path'}$2/;
    }


    # check for mode propagation (only to CGI scripts)
    if (IsRunningCGI()
        && ((GP('URLS')->{$url}->{'type'} == $Greenphyl::URLConfig::CGI_URL_TYPE)
            || (GP('URLS')->{$url}->{'type'} == $Greenphyl::URLConfig::ACTION_URL_TYPE)))
    {
        # get and set master mode
        my $master_mode = $query->{'mmode'} ||= Greenphyl::Web::GetParameterValuesFromCGI('mmode');
        # if not set, remove from hash
        if (!$query->{'mmode'})
        {delete($query->{'mmode'});}

        if ($query->{'mode'})
        {
            # a new mode has been specified
            # keep track of master mode if one
            if ($master_mode)
            {
                $query->{'mmode'} = $master_mode;
            }
        }
        elsif ($parameters->{'keep_mode'})
        {
            # keep current mode
            $query->{'mode'} = Greenphyl::Web::GetPageMode();
        }
        elsif ($master_mode)
        {
            # no mode specified, fallback to parent mode if one
            $query->{'mode'} = $master_mode;
        }

        # default mode, no need to add it
        if ($query->{'mode'} && $master_mode && ($query->{'mode'} eq $master_mode))
        {
            delete($query->{'mode'});
        }
    }

    # replace requested parameters
    if ($parameters->{'set'} && %{$parameters->{'set'}})
    {
        while ( my ($param_name, $param_value) = each(%{$parameters->{'set'}}) )
        {
            $full_url =~ s/\?$param_name(?:=[^&;]*)/?/; # remove parameter as the first one
            $full_url =~ s/[&;]+$param_name(?:=[^&;]*)//g; # remove subsequence parameters
            $query->{$param_name} = $param_value;
        }
    }

    # check if we got additional parameters
    if (keys(%$query))
    {
        my $url_join = '?';
        # check if URL already has a query string
        if ((GP('URLS')->{$url}->{'type'} == $Greenphyl::URLConfig::ACTION_URL_TYPE)
            || ($full_url =~ m/\?/))
        {
            $url_join = '&';
        }

        # add parameters
        $full_url .= $url_join . join('&', map { $_ . '=' . uri_escape($query->{$_}) } keys(%$query));
    }

    # append what was requested
    if ($parameters->{'append'})
    {
        $full_url .= $parameters->{'append'};
    }

    # anchor
    my $anchor = $parameters->{'anchor'} || GP('URLS')->{$url}->{'anchor'} || '';
    if ($anchor ne '')
    {
        $anchor = '#' . $anchor;
    }

    return $full_url . $anchor;
}


=pod

=head2 ExpandFile

B<Description>: uncompressed a given file. Supported extensions are:
.zip, .gz, .bz2, .tgz, .tar.gz, .tbz, .tar.bz2

B<ArgsCount>: 1

=item $file_path: (string) (R)

path to the file.

=back

B<Return>: (string)

The path to the uncompressed file.

B<Caller>: general

B<Example>:

    my $zipped_file_path = ...
    ...
    my $file_path = ExpandFile($zipped_file_path);

=cut

sub ExpandFile
{
    my ($file_path) = @_;

    my ($extension) = ($file_path =~ m/($COMPRESSED_EXTENSIONS_REGEXP?)/i);

    if (!$extension)
    {
        PrintDebug("File not compressed (or unsupported format): '$file_path'");
        return $file_path;
    }

    # check for compression
    if ($extension =~ m/\.bz2/i)
    {
        PrintDebug("Uncompressing file '" . $file_path . $extension . "'...");
        # uncompress
        my $command = "bunzip2 '$file_path$extension'";
        LogDebug("COMMAND: $command");
        if (system($command))
        {
            confess "Failed to uncompress file '" . $file_path . $extension . "' using bunzip2!\n$!";
        }
        PrintDebug("...uncompress done.");
    }
    elsif ($extension =~ m/\.gz/i)
    {
        PrintDebug("Uncompressing file '" . $file_path . $extension . "'...");
        # uncompress
        my $command = "gunzip '$file_path$extension'";
        LogDebug("COMMAND: $command");
        if (system($command))
        {
            confess "Failed to uncompress file '" . $file_path . $extension . "' using gunzip!\n$!";
        }
        PrintDebug("...uncompress done.");
    }
    elsif ($extension =~ m/\.zip/i)
    {
        PrintDebug("Uncompressing file '" . $file_path . $extension . "'...");
        # uncompress
        my $command = "unzip '$file_path$extension'";
        LogDebug("COMMAND: $command");
        if (system($command))
        {
            confess "Failed to uncompress file '" . $file_path . $extension . "' using unzip!\n$!";
        }
        PrintDebug("...uncompress done.");
    }
    elsif ($extension =~ m/\.tgz|\.tar\.gz/i)
    {
        PrintDebug("Uncompressing file '" . $file_path . $extension . "'...");
        # uncompress
        my $command = "tar zxvf '$file_path$extension'";
        LogDebug("COMMAND: $command");
        if (system($command))
        {
            confess "Failed to uncompress file '" . $file_path . $extension . "' using tar!\n$!";
        }
        PrintDebug("...uncompress done.");
    }
    elsif ($extension =~ m/\.tbz|\.tar\.bz2/i)
    {
        PrintDebug("Uncompressing file '" . $file_path . $extension . "'...");
        # uncompress
        my $command = "tar jxvf '$file_path$extension'";
        LogDebug("COMMAND: $command");
        if (system($command))
        {
            confess "Failed to uncompress file '" . $file_path . $extension . "' using tar!\n$!";
        }
        PrintDebug("...uncompress done.");
    }

    # uncompressed version
    my $uncompressed_file_path = $file_path;
    $uncompressed_file_path =~ s/$COMPRESSED_EXTENSIONS_REGEXP$//;

    return $uncompressed_file_path;
}


=pod

=head2 GetWebFile

B<Description>: Returns a valid path to a requested file. If a URL is provided,
the file can be fetched from the web automatically and if that file was
compressed (but not the requested file), the file will be uncompressed (working
for .zip, .gz, .tgz and .tar.gz).
This function supports "no prompt" mode and the default behavior is:
-if the file is missing, get it from the web (if no URL given, rise an
 exception)
-if the compressed version of the file to fetch from the web already exists, it
 won't be overwritten (and will be uncompressed).

B<ArgsCount>: 1

=item $parameters: (hash ref) (R)

Hash of parameters supporting the following keys:
-'file_path' (string): path to the file (user-provided);
-'default_file_path' (string): default file path (generaly from config);
-'source_url' (string): URL to use to fetch the file (may be a compressed file, in that
 case, the URL must include a string with the compression extension like '.zip'
 for instance; if the original URL does not have such a string, a fake one
 should be added using '#.zip' for instance);
-'file_description' (string): file description used in prompt message. For instance:
 "Uniprot to GO cross-reference file";
-'no_prompt' (boolean): if set, disable prompting.

=back

B<Return>: (string)

The path to the requested file.

B<Caller>: general

B<Example>:

    my $ipr2go_file_path = ...
    ...
    $ipr2go_file_path = GetWebFile({
        'file_path' => $ipr2go_file_path,
        'default_file_path' => GP('IPR_TO_GO_FILE_PATH'),
        'source_url' => GetURL('ipr_to_gene_ontology'),
        'file_description' => 'IPR to GO cross-reference file',
        'no_prompt' => $g_no_prompt,
    });

=cut

sub GetWebFile
{
    my ($parameters) = @_;

    my $default_file_path = $parameters->{'default_file_path'};
    my $file_path = $parameters->{'file_path'};

    # save given path as default if no default was provided
    $default_file_path ||= $file_path;

    # use default path of no path was provided
    $file_path ||= $default_file_path;

    # uncompressed version
    my $uncompressed_file_path = $file_path;
    $uncompressed_file_path =~ s/$COMPRESSED_EXTENSIONS_REGEXP$//;

    # file description
    my $file_description = $parameters->{'file_description'} || $uncompressed_file_path;

    # do not try forever
    my $max_tries = 3;
    # loop until we got a valid uncompressed file
    while ((!$uncompressed_file_path || (!-f $uncompressed_file_path) || (-z $uncompressed_file_path))
           && $max_tries--)
    {
        if (!$max_tries)
        {PrintDebug('Last try!');}

        # make sure we got a valid file name
        if (!$file_path)
        {
            $uncompressed_file_path = $file_path = Prompt("Enter $file_description path: ", { 'default' => $default_file_path, 'constraint' => '\w+' }, $parameters->{'no_prompt'});
            # uncompressed version
            $uncompressed_file_path =~ s/$COMPRESSED_EXTENSIONS_REGEXP$//;
        }

        # make sure we got a file name
        if ($file_path)
        {
            # check if the file is missing
            # if the file is missing ask to get it from the web
            if (((!-f $uncompressed_file_path) || (-z $uncompressed_file_path))
                && $parameters->{'source_url'}
                && Prompt("$file_description is missing! Fetch it from the web? (y/n)", { 'default' => 'y', 'constraint' => '[yYnN]' }, $parameters->{'no_prompt'}) =~ m/y/i)
            {
                # remove file if empty
                if (-f $uncompressed_file_path && -z $uncompressed_file_path)
                {
                    unlink($uncompressed_file_path) or cluck "Failed to remove file '$uncompressed_file_path': $!\n";
                }

                # check if we'll download a compressed file
                my ($extension) = ($parameters->{'source_url'} =~ m/($COMPRESSED_EXTENSIONS_REGEXP?)/);
                # make sure the target file has not the same extension
                if ($file_path =~ m/\Q$extension\E$/)
                {
                    # same, don't add compressed extension to downloaded file
                    $extension = '';
                }
                $file_path .= $extension;

                # remove wget target file if empty
                if (-f $file_path && -z $file_path)
                {
                    unlink($file_path) or cluck "Failed to remove file '$file_path': $!\n";
                }

                # fetch file from the web
                my $command = "wget -O '" . $file_path . "' '" . $parameters->{'source_url'} . "'";
                PrintDebug("COMMAND: $command");
                if (system($command))
                {
                    confess "Failed to fetch file '" . $file_path . "' using wget!\n$!";
                }
            }

            # check if web file exists and should just be uncompressed
            if (-f $file_path)
            {
                # is it comlpressed?
                if ($file_path =~ m/$COMPRESSED_EXTENSIONS_REGEXP/)
                {
                    # file is available and should just be expanded
                    if ((-f $uncompressed_file_path)
                        && (Prompt("File '" . $uncompressed_file_path . "' already exists! Overwrite? (y/n)", { 'default' => 'n', 'constraint' => '[yYnN]' }, $parameters->{'no_prompt'}) =~ m/y/i))
                    {
                        unlink($uncompressed_file_path) or cluck "Failed to remove file '$uncompressed_file_path': $!\n";
                    }

                    if (!-f $uncompressed_file_path)
                    {
                        $uncompressed_file_path = ExpandFile($file_path);
                    }
                }
                  # just rename file?
                elsif ($file_path ne $uncompressed_file_path)
                {
                    if (system("mv '$file_path' '$uncompressed_file_path'"))
                    {
                        confess "Failed to rename '$file_path' into '$uncompressed_file_path'!\n";
                    }
                }
            }
        }
    }

    if (!$file_path || !-e $file_path || !-e $uncompressed_file_path)
    {
        confess "No valid path provided for $file_description!";
    }
    elsif (-z $uncompressed_file_path)
    {
        confess "Got an empty $file_description file ($uncompressed_file_path)!";
    }

    PrintDebug("Returned file path: " . $uncompressed_file_path);
    return $uncompressed_file_path;
}


=pod

=head2 GetAction

B<Description>: find which action is currently set. An action can be set
either from a submit button used in a form (the name of the button correspond
to the action to perform) or a command line argument.

B<ArgsCount>: 1

=over 4

=item $allowed_actions: (array ref) (R)

an array (of strings) containing all the available/allowed actions name.

=back

B<Return>: (string)

the action name or an empty string.

=cut

sub GetAction
{
    my ($allowed_actions) = @_;

    if (IsRunningCGI())
    {
        return Greenphyl::Web::GetPageAction($allowed_actions);
    }
    else
    {
        foreach my $action (@$allowed_actions)
        {
            # check if a parameter using the $action is set
            if ($action && grep(m/^$action(?:\W.*)?$/, @ARGV))
            {
                return $action;
            }
        }
    }
    return '';
}


=pod

=head2 HandleErrors

B<Description>: check if an error occured and display an error message.

B<ArgsCount>: 0

B<Return>:

nothing.

B<Caller>: general

B<Example>:

    eval { ... };
    HandleErrors();

=cut

sub HandleErrors
{
    # catch block
    my $error;
    $error = Exception::Class->caught();
    if ($error)
    {
        print "ERROR: " . $error . "\n";
    }
}


=pod

=head2 GetDBIndexFromName

B<Description>: Returns the numeric index of the given database name or alias.

B<ArgsCount>: 1

=over 4

=item $db_name: (string) (R)

The database name.

=back

B<Return>: (integer)

Index of requested database or an index out of GP('DATABASES') index range.

B<Caller>: internal

=cut

sub GetDBIndexFromName
{
    my ($db_name) = @_;

    # got a name, find corresponding index
    my $name_index = 0;
    while ($name_index < scalar(@{GP('DATABASES')})
        && (GP('DATABASES')->[$name_index]->{'name'}     ne $db_name)
        && (GP('DATABASES')->[$name_index]->{'database'} ne $db_name))
    {
        ++$name_index;
    }

    return $name_index;
}


=pod

=head2 SelectActiveDatabase

B<Description>: Sets and returns index of current database.
Note: this function is overloaded by Greenphyl::Web::SelectActiveDatabaseFromCGI
to get active database from session object or from the URL.

Note: active database can be specified in command line using the parameter:
db_index=<index>

B<ArgsCount>: 0-1

=over 4

=item $db_index: (integer) (O)

index of the database to use. See Greenphyl::Config (Config.pm) for available
indexes.

=back

B<Return>: (index)

Index of selected (active) database.

B<Caller>: general

B<Example>:

     if (0 == SelectActiveDatabase())
     {
        # default database selected
        ...
     }
     ...
     # activate 3rd database (index=2)
     SelectActiveDatabase(2);

=cut

sub SelectActiveDatabase
{
    my ($db_index) = @_;

    # check if a database has been specified by function call
    if (!defined($db_index))
    {
        # if not defined, check command line
        if (my @command_line_db_index = grep {m/^-?db[_\-]?index=\w+$/} @ARGV)
        {
            ($db_index) = ($command_line_db_index[0] =~ m/-?db[_\-]?index=(\w+)/);
        }
    }

    $db_index ||= 0; # set value to 0 if it isn't set yet

    # check for named database
    if ($db_index =~ m/[a-z]/i)
    {
        $db_index = GetDBIndexFromName($db_index);
    }

    # make sure we got a valid index
    if ((0 > $db_index) || (scalar(@{GP('DATABASES')}) <= $db_index))
    {
        $db_index = 0;
    }

    PrintDebug("Active database: $db_index (" . GP('DATABASES')->[$db_index]->{'name'} . ', db=' . GP('DATABASES')->[$db_index]->{'database'} . ")");
    return $g_active_dbh_index = $db_index;
}


=pod

=head2 ConnectToDatabase

B<Description>: Returns a database handler.

B<ArgsCount>: 1

=over 4

=item $db_settings: (hash ref) (R)

hash containing the settings to use to connect to the database.
Keys:
    host     => database host name or IP
    port     => database port
    login    => database login
    password => associated password
    database => name of the database

=back

B<Return>: (DBI::db)

A database handler.

B<Caller>: general

B<Example>:

     my $dbh = ConnectToDatabase();

B<See Also>:

GetDatabaseHandler()

=cut

sub ConnectToDatabase
{
    my ($db_settings) = @_;

    # check if database settings were specified
    if (!defined($db_settings))
    {
        confess "ERROR: No database settings specified!\n";
    }
    elsif ('HASH' ne ref($db_settings))
    {
        confess "ERROR: Invalid database settings specified (got " . (ref($db_settings)?ref($db_settings):$db_settings) . ")!\n";
    }

    $db_settings->{'port'} ||= '3306'; # default MySQL port

    # connect
    my $dbh = DBI->connect( "dbi:mysql:database=" . $db_settings->{'database'} . ";host=" . $db_settings->{'host'} . ";port=" . $db_settings->{'port'}, $db_settings->{'login'}, $db_settings->{'password'} );

    if (!$dbh)
    {
        confess "ERROR: Unable to connect to database '" . $db_settings->{'host'} . '/' . $db_settings->{'database'} . "'!\n$DBI::errstr\n";
    }

    # maximum length of 'long' type fields (LONG, BLOB, CLOB, MEMO, etc.)
    $dbh->{'LongReadLen'} = 512 * 1024; # 512Kb

    # more explicit error reporting
    $dbh->{'HandleError'} = sub { confess(shift()) };

    if (GP('DB_PROFILING'))
    {
        # Profile enables the collection and reporting of method call timing statistics
        $dbh->{'Profile'} = 31; # activates full profiling

        # double mention to avoid warning
        $DBI::Profile::ON_DESTROY_DUMP = $DBI::Profile::ON_DESTROY_DUMP = sub
            {
                my ($result) = @_;
                my $time_cutoff = GP('DB_PROFILING_CUTOFF');
                my ($time) = ($result =~ m/DBI::Profile: ([\d\.]+?)s/);

                # important: skips profile display on "fast" queries
                return if ($time && ($time < $time_cutoff));

                my $fh;
                if (open($fh, '>>', $Greenphyl::Config::DATA_PATH . '/dbi_profile.log'))
                {
                    print {$fh} shift();
                    close($fh);
                }
            };
    }

    return $dbh;
}


=pod

=head2 GetDatabaseHandler

B<Description>: Returns GreenPhyl active database handler.

B<ArgsCount>: 0-1

=over 4

=item $db_index: (integer) (O)

index of the database to use.

=back

B<Return>: (DBI)

Current GreenPhyl database handler.

B<Caller>: general

B<Example>:

     my $dbh = GetDatabaseHandler();

B<See Also>:

ConnectToDatabase(), SelectActiveDatabase()

=cut

sub GetDatabaseHandler
{
    my ($db_index) = @_;

    # check if database index has been specified
    if (!defined($db_index))
    {
        # none specified, return default
        $db_index = $g_active_dbh_index;
    }

    # check for named database
    if ($db_index =~ m/[a-z]/i)
    {
        $db_index = GetDBIndexFromName($db_index);
    }

    # check index
    if ((0 > $db_index) || (scalar(@{GP('DATABASES')}) <= $db_index))
    {
        confess "ERROR: Invalid database index! Please check your config file or the database index you provided.\n";
    }

    # connect to database if needed
    if (!$g_dbh_array->[$db_index])
    {
        $g_dbh_array->[$db_index] = ConnectToDatabase(GP('DATABASES')->[$db_index]);
    }

    PrintDebug("Return handler for database: $db_index (" . GP('DATABASES')->[$db_index]->{'name'} . ', db=' . GP('DATABASES')->[$db_index]->{'database'} . ")");
    return $g_dbh_array->[$db_index];
}


=pod

=head2 ResetDatabaseHandlers

B<Description>: Reset all database handler (usefull in multithreads).

B<ArgsCount>: 0

B<Return>: nothing

B<Caller>: general

=cut

sub ResetDatabaseHandlers
{
    my $db_count = scalar(@{GP('DATABASES')});
    for (my $i = 0; $i < $db_count; ++$i)
    {
        $g_dbh_array->[$i] = undef;
    }

    PrintDebug("All DB handles cleared.");
}


=pod

=head2 DisconnectDatabase

B<Description>: disconnect database handler.

B<ArgsCount>: 0-1

=over 4

=item $db_index: (integer) (O)

index of the database to use.

=back

B<Return>: nothing.

B<Caller>: general

B<Example>:

     my $dbh = GetDatabaseHandler();
     ...
     DisconnectDatabase();

B<See Also>:

GetDatabaseHandler(), ConnectToDatabase(), SelectActiveDatabase()

=cut

sub DisconnectDatabase
{
    my ($db_index) = @_;

    # check if database index has been specified
    if (!defined($db_index))
    {
        # none specified, return default
        $db_index = $g_active_dbh_index;
    }

    # check index
    if ((0 > $db_index) || (scalar(@{GP('DATABASES')}) <= $db_index))
    {
        confess "ERROR: Invalid database index! Please check your config file or the database index you provided.\n";
    }

    # disconnect to database if needed
    if ($g_dbh_array->[$db_index])
    {
        $g_dbh_array->[$db_index]->disconnect()
            or cluck $g_dbh_array->[$db_index]->errstr;
        $g_dbh_array->[$db_index] = undef;
    }
}


=pod

=head2 GetDatabaseName

B<Description>: Returns GreenPhyl active database name.

B<ArgsCount>: 0-1

=over 4

=item $db_index: (integer) (O)

index of the database to use instead of the active one.

=back

B<Return>: (string)

Database name.

B<Caller>: general

B<Example>:

     my $dbh = GetDatabaseHandler();

B<See Also>:

GetDatabaseHandler(), SelectActiveDatabase()

=cut

sub GetDatabaseName
{
    my ($db_index) = @_;

    # check if database index has been specified
    if (!defined($db_index))
    {
        # none specified, return default
        $db_index = $g_active_dbh_index;
    }

    # check index
    if ((0 > $db_index) || (scalar(@{GP('DATABASES')}) <= $db_index))
    {
        confess "ERROR: Invalid database index! Please check your config file or the database index you provided.\n";
    }

    # return name
    return GP('DATABASES')->[$db_index]->{'name'};
}


=pod

=head2 TestTableExists

B<Description>: Returns true if the requested table exists.

B<ArgsCount>: 1-2

=over 4

=item $table_name: (string) (R)

name of the table to check.

=item $db_index: (integer) (O)

index of the database to use instead of the active one.

=back

B<Return>: (boolean)

True if the table exists, false otherwise.

B<Caller>: general

B<Example>:

     if (TestTableExists('transfered_families'))
     {
        ...
     }

=cut

sub TestTableExists
{
    my ($table_name, $db_index) = @_;

    $db_index ||= 0; # set value to 0 if it isn't set yet
    # make sure we got a valid index
    if ((0 > $db_index) || (scalar(@{GP('DATABASES')}) <= $db_index))
    {
        $db_index = 0;
    }

    my $dbh = GetDatabaseHandler($db_index);

    # check if table exists
    return $dbh->selectrow_array("SELECT TRUE FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_SCHEMA = '" . GP('DATABASES')->[$db_index]->{'database'} . "' AND TABLE_NAME = '$table_name';");
}


=pod

=head2 GetTableInfo

B<Description>: For given table name, this function returns a hash looking like:

{
    <field_name_1> => {
        'required'    => 0/1,
        'comment'     => <mysql field comment + default value>,
        'COLUMN_NAME' => <field_name> # not really useful but provided anyway
    },
    <field_name_2> => {
        ...
    },
}

Note: the 'id' column is always ignored and does not appear in the returned
hash.

B<ArgsCount>: 1-2

=over 4

=item $table_name: (string) (R)

name of the table to check.

=item $dbh: (DBI::db) (O)

the database handler to use.

=back

B<Return>: (hash ref)

A hash like this:
{
    <field_name_1> => {
        'name' => <field_name>
        'type' => <field_type>
        'default'  => <default value>,
        'comment'  => <mysql field comment>,
        'required'    => 0/1,
    },
    <field_name_2> => {
        ...
    },
}

B<Caller>: general

=cut

sub GetTableInfo
{
    my ($table_name, $dbh) = @_;

    if (!exists($g_table_info_cache->{$table_name}))
    {
        $dbh ||= GetDatabaseHandler();
        my $sql_query = qq{
            SELECT
                COLUMN_NAME AS "name",
                COLUMN_TYPE AS "type",
                IF (IS_NULLABLE='NO' AND COLUMN_DEFAULT IS NULL, 1, 0) AS "required",
                COLUMN_DEFAULT AS "default",
                COLUMN_COMMENT AS "comment"
            FROM information_schema.COLUMNS
            WHERE
                TABLE_SCHEMA = schema()
                AND TABLE_NAME = '$table_name'
                AND COLUMN_NAME != 'id'
        };
        # PrintDebug("SQL Query: $sql_query");

        $g_table_info_cache->{$table_name} = $dbh->selectall_hashref($sql_query, 'name');
    }

    return %{$g_table_info_cache->{$table_name}};
}


=pod

=head2 GetTableColumnValues

B<Description>: For given table and column, this function returns a list of
possible values if it's a set or an enum, otherwise, it returns the column type.

B<ArgsCount>: 2-3

=over 4

=item $table_name: (string) (R)

name of the table to check.

=item $column_name: (string) (R)

name of the column to check.

=item $dbh: (DBI::db) (O)

the database handler to use.

=back

B<Return>: (list)

A list of string in case of a set or an enum, otherwise a single string
containing the column type. If the table or the column do not exist, returns
undef.

=cut

sub GetTableColumnValues
{
    my ($table_name, $column_name, $dbh) = @_;

    # get table definition to extract possible values for sets and enums
    my %table_data = GetTableInfo($table_name, $dbh);

    if (!%table_data
        || !exists($table_data{$column_name})
        || !exists($table_data{$column_name}->{'type'}))
    {
        return undef;
    }

    my @values;
    # example of types:
    # 'type' => 'varchar(255)'
    # 'type' => 'enum(\'private\',\'restricted to group - read only\',\'restricted to group - editable\',\'public - read only\',\'public - editable\')'
    # 'type' => 'set(\'Other\',\'IEA:InterPro\',\'IEA:PIRSF\',\'IEA:UniProtKB\',\'IEA:UniProtKB-KW\',\'IEA:KEGG\',\'IEA:MEROPS\',\'IEA:TF\',\'PubMed\',\'TAIR/TIGR\')'

    if ($table_data{$column_name}->{'type'} =~ m/^(?:enum|set)\((.*)\)$/i)
    {
        my $possible_values = $1;
        $possible_values =~ s/^'//;
        $possible_values =~ s/'$//;
        @values = split(/','/, $possible_values);
    }
    else
    {
        @values = ($table_data{$column_name}->{'type'});
    }

    return @values;
}


=pod

=head2 SaveCurrentTransaction

B<Description>: Save current transaction and restart a new one.

B<ArgsCount>: 0-2

=over 4

=item $dbh: (DBI::db) (U)

Database handler.
Default: current default database handler.

=item $restart_transaction: (boolean) (O)

If true or undef, a new transaction will be started, otherwise no new
transaction will be started.
Default: true.

=back

B<Return>: nothing

B<Caller>: general

B<Example>:

    my $dbh = GetDatabaseHandler();
    # start SQL transaction
    if ($dbh->{'AutoCommit'})
    {
        $dbh->begin_work() or croak $dbh->errstr;
    }
    # set warning state (not fatal)
    $dbh->{'HandleError'} = undef;

    ...

    SaveCurrentTransaction();
    # or
    SaveCurrentTransaction($g_dbh);

=cut

sub SaveCurrentTransaction
{
    my ($dbh, $restart_transaction) = @_;

    $dbh ||= GetDatabaseHandler();

    # check if caller module has a $DEBUG variable
    # and if it's the case, make sure that variable is set to a true value
    my ($package, $filename, $line) = caller();
    my $local_debug_mode;
    eval '$local_debug_mode = $' . $package . '::DEBUG;';
    if ((!defined($local_debug_mode) && GP('DEBUG_MODE'))
        || (defined($local_debug_mode) && $local_debug_mode))
    {
        #+FIXME: do not rollback in debug mode. Just rollback when the program exits.
        # caller is in debug mode: no commit should occure!
        # rollback
        PrintDebug("DEBUG MODE: ROLLBACK!");
        $dbh->rollback() or confess($dbh->errstr);
        PrintDebug("...rollback done!");
    }
    else
    {
        # commit SQL transaction
        PrintDebug("Committing transaction...");
        $dbh->commit() or confess($dbh->errstr);
        PrintDebug("...commit done!");
    }

    # start a new SQL transaction
    if ((!defined($restart_transaction) || $restart_transaction)
		&& $dbh->{'AutoCommit'})
    {
        PrintDebug("Starting a new transaction");
        $dbh->begin_work() or croak $dbh->errstr;
    }

}


=pod

=head2 InsertIgnore

B<Description>: .

B<ArgsCount>: 0-1

=over 4

=item $dbh: (DBI::db) (O)

Database handler.
Default: current default database handler.

=back

B<Return>: (int) number of modified rows.

B<Caller>: general

B<Example>:

    InsertIgnore('table', {'field_name' => 'value', 'field_name2' => 'value2', });
    # or
    InsertIgnore('my_table', {'field_name' => 'value', }, $g_dbh);

=cut

sub InsertIgnore
{
    my ($table, $values_hash, $dbh) = @_;

    $dbh ||= GetDatabaseHandler();

    my $field_names  = join(', ', grep { defined($values_hash->{$_}) } keys(%$values_hash));
    my @field_values = grep { defined($_) } values(%$values_hash);
    my $value_spaceholders = join(', ', map {'?'} grep { defined($_) } values(%$values_hash));

    my $sql_query = "INSERT IGNORE INTO $table ($field_names) VALUES ($value_spaceholders);";
    PrintDebug("SQL QUERY: $sql_query\nBindings: " . join(', ', @field_values));
    return ($dbh->do(
        $sql_query,
        undef,
        @field_values
    ) or confess "ERROR: Failed to insert values in table '$table'!\n" . $dbh->errstr);
}


=pod

=head2 InsertUpdate

B<Description>: .

B<ArgsCount>: 0-1

=over 4

=item $dbh: (DBI::db) (O)

Database handler.
Default: current default database handler.

=back

B<Return>: (int) number of modified rows.

B<Caller>: general

B<Example>:

    InsertIgnore('table', {'field_name' => 'value', 'field_name2' => 'value2', });
    # or
    InsertIgnore('my_table', {'field_name' => 'value', }, $g_dbh);

=cut

sub InsertUpdate
{
    my ($table, $values_hash, $dbh) = @_;

    $dbh ||= GetDatabaseHandler();

    my $field_names  = join(', ', grep { defined($values_hash->{$_}) } keys(%$values_hash));
    my @field_values = grep { defined($_) } values(%$values_hash);
    my $value_spaceholders = join(', ', map {'?'} grep { defined($_) } values(%$values_hash));

    my $sql_query = "
        INSERT INTO $table ($field_names)
        VALUES ($value_spaceholders)
        ON DUPLICATE KEY UPDATE "
        . join(', ', map { $_ . ' = ?' } grep { defined($values_hash->{$_}) } keys(%$values_hash))
        . ";"
    ;
    PrintDebug("SQL QUERY: $sql_query\nBindings: " . join(', ', @field_values, @field_values));
    return ($dbh->do(
        $sql_query,
        undef,
        @field_values,
        @field_values,
    ) or confess "ERROR: Failed to insert values in table '$table'!\n" . $dbh->errstr);
}


=pod

=head2 EscapeSQLRegExp

B<Description>: Escape meta characters in order to use them in a MySQL REGEXP
expression.

B<ArgsCount>: 1

=over 4

=item $string: (string or array ref of strings) (R)

String(s) to escape.

=back

B<Return>: (string or array ref of strings)

The escaped string(s).

B<Caller>: general

=cut

sub EscapeSQLRegExp
{
    my ($strings) = @_;

    if (ref($strings))
    {
        my @escaped_strings;
        foreach my $string (@$strings)
        {
            $string =~ s/(\W)/\\$1/g;
            push(@escaped_strings, $string);
        }
        return \@escaped_strings;
    }
    else
    {
        $strings =~ s/(\W)/\\$1/g;
    }
    return $strings;
}


=pod

=head2 WriteTemporaryFile

B<Description>: Creates a temporary file and writes the given content into it.
Returns the temporary file name and path.
If the file already exists, it is overwritten. If no file name is provided,
a new one is generated and returned.

B<ArgsCount>: 1-2

=over 4

=item $file_content: (string) (U)

content to put in the temporary file.

=item $file_name: (string) (O)

The temporary file name. If not specified, a new temporary file is generated.

=back

B<Return>: (string)

Full path to the temporary file name.

B<Caller>: general

B<Example>:

    my ($temp_file_name, $temp_fh) = WriteTemporaryFile('some data');
    my $temp_file_name2 = WriteTemporaryFile('some other data');
    my $temp_file_name3 = WriteTemporaryFile('some more data', 'toto.fasta');

=cut

sub WriteTemporaryFile
{
    use File::Temp qw(tempfile);
    my ($file_content, $file_name) = @_;
    my $temp_file_fh;

    # parameters check
    # make sure we got something to write
    if (!defined($file_content))
    {
        $file_content = '';
    }

    # make sure we got a valid file path
    if (!$file_name)
    {
        # generate a name
        ($temp_file_fh, $file_name) =
            tempfile(
                'tempXXXXXX',
                'DIR' => GP('TEMP_OUTPUT_PATH'),
                'SUFFIX' => '.tmp',
            );
    }
    else
    {
        # check the path and file name...
        # -remove invalid characters
        $file_name =~ s/[^\w\-\.\/]//g; # remove non-allowed characters
        $file_name =~ s/\.\.//; # remove relative '..'
        # -if it does not start with the full path, prepend it
        if ($file_name !~ m~^/~)
        {
            $file_name = GP('TEMP_OUTPUT_PATH') . '/' . $file_name;
        }
        # check if the file already exists and warn if some
        if (-e $file_name)
        {
            cluck "WARNING: file '$file_name' already exists and will be overwritten!\n";
        }
        open($temp_file_fh, ">$file_name")
            or confess "ERROR: unable to create temporary file '$file_name': $!\n";
    }

    print {$temp_file_fh} $file_content;

    # return file name and if needed the file handler on the open file
    if (wantarray())
    {
        return ($file_name, $temp_file_fh);
    }
    else
    {
        close($temp_file_fh);
        return $file_name;
    }
}


=pod

=head2 ValidateSequenceName

B<Description>: Tells if a sequence name is valid or not. A valid name must not
contain special characters and should be only composed by letters, numbers,
dots, dashes and underscores in a limit of $MAX_SEQUENCE_NAME_LENGTH characters
and with at least 5 characters.

B<ArgsCount>: 1

=over 4

=item $sequence_name: (string) (R)

The sequence name to check.

=back

B<Return>: (boolean)

true if the name is valid, false otherwise.

B<Caller>: General (Static)

B<Example>:

    if (ValidateSequenceName('My_funny_sequence.name'))
    {
        ...
    }

=cut

sub ValidateSequenceName
{
    my ($sequence_name) = @_;
    return ($sequence_name =~ m/^[\w\.\-]{5,$MAX_SEQUENCE_NAME_LENGTH}$/);
}


=pod

=head2 RemovePagerParameters

B<Description>: Removes page parameters from a URL.

B<ArgsCount>: 1

=over 4

=item $page_url: (string) (R)

The URL to clean up.

=back

B<Return>: (string)

the URL without page parameters.

B<Caller>: General (Static)

B<Example>:

    my $url = CGI::url('-query' => 1);
    $url = RemovePagerParameters($url);

=cut

sub RemovePagerParameters
{
    my ($page_url) = @_;
    # remove current page index and entries per page parameters
    $page_url =~ s/([?&;])$CURRENT_PAGE_PARAMETER=[^&;]+(?:&|;|$)/$1/io;
    $page_url =~ s/([?&;])$ENTRIES_PER_PAGE_PARAMETER=[^&;]+(?:&|;|$)/$1/io;
    # append query string if needed
    if ($page_url !~ m/\?/)
    {$page_url .= '?';}
    return $page_url;
}


=pod

=head2 GeneratePagerData

B<Description>: Creates a pager_data structure that can be used with
pagination.tt or with RenderHTMLFullPage for instance.

B<ArgsCount>: 1

=over 4

=item $parameters: (hash ref) (R)

A has of parameters with the following keys:
-Required keys:
  'total_entries': total number of entries.
-Optional keys:
  'page_url': URL of the page that displays the page content with pager.
    Default: current URL if CGI mode, empty otherwise
  'current_page': index of current page.
    Default: 0
  'entry_ranges': array of integers corresponding to the number of entries per
    pages that are allowed. If set to an empty array or if the final array
    contains one or less element, the entry range dropdown won't be displayed.
    Default: [25, 50, 100, 200, 500, 1000, 2000]
  'entries_per_page': current number of entries per page.
    Default: 50
  'entry_ranges_label': label to display before the dropdown that selects the
     entry ranges specified by 'entry_ranges'.
     Default: if not set, the template will display a default text.
  'max_page_links': maximum number of page links that can be displayed by the
    pager, including 'previous' and 'next' page links.

=back

B<Return>: (hash ref)

A pager_data hash containing the following keys:
  'pager': a Data::SpreadPagination object;
  'page_link': target page URL;
  'entry_ranges': array of allowed values for entry ranges;
  'entry_ranges_label': label to display for entry range selection.


B<Caller>: General (Static)

B<Example>:

    my $pager_data = GeneratePagerData({'total_entries' => 3080});

    my $current_page      = $pager_data->{'pager'}->current_page;
    my $entries_per_page  = $pager_data->{'pager'}->entries_per_page;
    my $total_entries     = $pager_data->{'pager'}->total_entries;
    my $first_page        = $pager_data->{'pager'}->first_page;
    my $last_page         = $pager_data->{'pager'}->last_page;
    my $previous_page     = $pager_data->{'pager'}->previous_page;
    my $next_page         = $pager_data->{'pager'}->next_page;
    my $first_entry_index = $pager_data->{'pager'}->first;
    my $last_entry_index  = $pager_data->{'pager'}->last;
    my $page_ranges       = $pager_data->{'pager'}->page_ranges;
    my $max_number_pages  = $pager_data->{'pager'}->max_pages();

    # print full page spread (without links or associated actions):
    if ($first_page)
    {
        print "$first_page ";
    }
    if ($previous_page)
    {
        print "$previous_page ";
    }
    foreach my $page ($pager_data->{'pager'}->pages_in_spread())
    {
        if (!defined $page)
        {
          print "... ";
        }
        elsif ($page == $pager_data->{'pager'}->current_page)
        {
          print "[$page] ";
        }
        else
        {
          print "$page ";
        }
    }
    if ($previous_page)
    {
        print "$previous_page ";
    }
    if ($last_page)
    {
        print "$last_page ";
    }

B<See Also>:

* Data::Page documentation:

    http://search.cpan.org/~lbrocard/Data-Page-2.02/lib/Data/Page.pm

* Data::SpreadPagination documentation:

    http://search.cpan.org/~knew/Data-SpreadPagination-0.1.2/lib/Data/SpreadPagination.pm

=cut

sub GeneratePagerData
{
    my ($parameters) = @_;

    $parameters ||= {};
    if ('HASH' ne ref($parameters))
    {
        confess "ERROR: Invalid parameter for GeneratePagerData()!\n";
    }

    # get current page index if one
    my $current_page      = GetParameterValues($CURRENT_PAGE_PARAMETER);
    if (!$current_page || ($current_page !~ /^\d+$/))
    {
        if ($current_page && ($current_page !~ /^\d+$/))
        {
            PrintDebug("Warning: invalid page index: '$current_page'!");
        }
        $current_page = $parameters->{'current_page'} || 0;
    }

    # get current entries per page if specified
    my $entries_per_page = GetParameterValues($ENTRIES_PER_PAGE_PARAMETER);
    if (!$entries_per_page || ($entries_per_page !~ /^\d+$/))
    {
        if ($entries_per_page && ($entries_per_page !~ /^\d+$/))
        {
            PrintDebug("Warning: invalid entries-per-page number: '$entries_per_page'!");
        }
        $entries_per_page = $parameters->{'entries_per_page'} || 50;
    }

    # get page path
    my $page_url = $parameters->{'page_url'} || '';
    # if not specified, guess it
    if (!$page_url && IsRunningCGI())
    {
        $page_url = CGI::url('-path_info' => 1, '-query' => 1);
        # remove page index and fpp from path
        $page_url = RemovePagerParameters($page_url);
    }
    my $total_entries = $parameters->{'total_entries'};
    my $entry_ranges = $parameters->{'entry_ranges'} || [25, 50, 100, 200, 500, 1000, 2000];
    my @entry_ranges = ();
    foreach (@$entry_ranges)
    {
        if ((0 < $_) && ($total_entries > $_))
        {
            push(@entry_ranges, $_);
        }
    }
    @entry_ranges = sort {int($a) <=> int($b)} (@entry_ranges);

    my $pager = Data::SpreadPagination->new(
        {
            'totalEntries'      => $total_entries,
            'entriesPerPage'    => $entries_per_page,
            'currentPage'       => $current_page,
            'maxPages'          => $parameters->{'max_page_links'} || 10,
        },
    );

    my $pager_data =
        {
            'pager'              => $pager,
            'page_link'          => $page_url,
            'entry_ranges'       => \@entry_ranges,
            'entry_ranges_label' => $parameters->{'entry_ranges_label'},
        };

    return $pager_data;
}


=pod

=head2 GetPagedObjects

B<Description>: Returns the objects corresponding to the request and to current
page and also the pager_data structure for template pagination.

B<ArgsCount>: 2

=over 4

=item $class: (string) (R)

The name of the class to use.

=item $query_parameters: (hash) (R)

An object parameter hash like describbed in DBObject CONSTRUCTOR documentation
("$parameters" parameter).

=back

B<Return>: (list)

A list of 2 elements: first, the array ref of loaded objects and
then the pager_data hash structure.

B<Caller>: General (Static)

B<Example>:

    my $query_parameters = {'selectors' => {'description' => ['LIKE', '%TAIR%'],}, 'sql' => {'ORDER BY' => 'family_id'}};
    my ($families, $pager_data) = GetPagedObjects('Greenphyl::Family', $query_parameters);
    $pager_data->{'entry_ranges_label'} = 'Families per page:';
    my $total_family_count = $pager_data->{'pager'}->total_entries;

=cut

sub GetPagedObjects
{
    my ($class, $query_parameters) = @_;

    $query_parameters ||= {};
    $query_parameters->{'sql'} ||= {};

    # remove unnecessary SQL options
    if ($query_parameters->{'sql'}->{'LIMIT'})
    {delete($query_parameters->{'sql'}->{'LIMIT'});}
    if ($query_parameters->{'sql'}->{'OFFSET'})
    {delete($query_parameters->{'sql'}->{'OFFSET'});}

    my $selectors = $query_parameters->{'selectors'};
    # get total number of corresponding families
    my $total_entries_count;
    eval "
        \$total_entries_count =
            " . $class.  "->Count(
                GetDatabaseHandler(),
                \$query_parameters,
            );";
    if ($@)
    {
        confess "ERROR: $@\n";
    }

    my $pager_data = GeneratePagerData({'total_entries' => $total_entries_count,},);
    my $page_index = $pager_data->{'pager'}->current_page - 1;
    my $entries_per_page = $pager_data->{'pager'}->entries_per_page;

    if (!$page_index || (0 > $page_index))
    {$page_index = 0;}

    $query_parameters->{'sql'}->{'OFFSET'} = $entries_per_page * $page_index,
    $query_parameters->{'sql'}->{'LIMIT'} = $entries_per_page,

    my $objects;
    eval "
        \$objects =
            [
                new " . $class . "(
                    GetDatabaseHandler(),
                    \$query_parameters,
                )
            ];";
    if ($@)
    {
        confess "ERROR: $@\n";
    }

    return ($objects, $pager_data);
}


=pod

=head2 Prompt

B<Description>: Prompt the user for something. Skipped if in CGI mode or if
$no_prompt parameter is used (returns default value if one or undef).

B<ArgsCount>: 1-3

=over 4

=item $prompt_message: (string) (R)

Query message to display to the user.

=item $parameters: (hash ref) (U)

A reference to a hash containing additional optional parameters as key=>value:
-'default'    => default value to use if the user just hit "Enter";
-'constraint' => a regular expression used to validate the value entered by the
  user;
-'no_trim'    => if set to non-zero, the string entered by the user is not
  trimed so leading and ending spaces are kept;

=item $no_prompt: (boolean) (O)

If set, no prompt will be displayed and the return value will be the default
value or undef otherwise.

=back

B<Return>: (scalar)

the value entered by the user

B<Example>:

    Prompt("Continue? [yYnN]", { 'default' => 'Y', 'constraint' => '[yYnN]', });
    Prompt("Where are you?", { 'default' => 'In front of my computer', 'constraint' => '\w{3,}', 'no_trim' = 1, });

=cut

sub Prompt
{
    my ($prompt_message, $parameters, $no_prompt) = (@_);

    if (!defined $prompt_message)
    {
        confess "ERROR: No message provided!\n";
    }

    if (!defined $parameters)
    {
        $parameters = {};
    }
    elsif ('HASH' ne ref($parameters))
    {
        confess "ERROR: Parameters not passed has a hash reference! If you just need to set '\$no_prompt', add 'undef' as '\$parameters' argument.\n";
    }

    my ($default_value, $constraint, $no_trim) = ($parameters->{'default'}, $parameters->{'constraint'}, $parameters->{'no_trim'});

    if (not defined $constraint)
    {
        # if no constraint set, asks the user to provide at least 1 character
        $constraint = '.';
    }

    # set no prompt in CGI mode
    $no_prompt ||= IsRunningCGI();

    my $user_value = '';

    # check if no prompt is requested
    if ($no_prompt)
    {
        # no prompt, return default value if one or undef
        $user_value = $parameters->{'default'};
    }
    else
    {
        # prompt user
        do
        {
            if (($constraint ne '') && ($user_value ne ''))
            {
                # an attempt has already been made, let the user know he/she did not give a good answer
                print "Invalid value! Try again...\n";
            }
            # query user
            print "$prompt_message ";
            # display default value if set
            if ((defined $default_value) && ($default_value ne ''))
            {
                print "(default: $default_value) ";
            }
            # get user input
            chomp($user_value = <STDIN>);
            if (!defined($user_value))
            {
                confess 'ERROR: missing user input stream!';
            }

            # trim if needed
            if (not $no_trim)
            {
                $user_value =~ s/^[\s\t]*//;
                $user_value =~ s/[\s\t]*$//;
            }
            # check if user wants default value
            if (($user_value eq '') && (defined $default_value))
            {
                $user_value = $default_value;
                print "* $default_value *\n";
            }
        } while (($user_value !~ m/$constraint/i)
                 && ((not defined $default_value) || ($user_value ne $default_value)));
    }

    return $user_value;
}


=pod

=head2 GetDuration

B<Description>: Returns a string of current date in the format HH:MM:SS DD/MM/YYYY.

B<ArgsCount>: 1

=over 4

=item $duration (integer) (R)

Duration in seconds.

=back

B<Return>: (string)

A descriptive duration time that may include days, hours, minutes or seconds.

B<Example>:

    my $start_time = time();
    ...
    print 'duration: ' . GetDuration(time() - $start_time) . "\n";

=cut

sub GetDuration
{
    my $duration = shift;
    if (60 > $duration)
    {
        # less than a minute
        return sprintf('%2.2d second(s)', $duration);
    }
    elsif (3600 > $duration)
    {
        # less than an hour
        return sprintf('%d:%02d', int($duration/60), ($duration%60));
    }
    elsif (86400 > $duration)
    {
        # less than a day
        return sprintf('%d:%02d:%02d', int($duration/3600), int($duration/60)%60, ($duration%60));
    }
    else
    {
        # more than a day
        return sprintf('%d days and %d:%02d:%02d', int($duration/86400), int($duration/3600)%24, int($duration/60)%60, ($duration%60));
    }
}


=pod

=head2 GetCurrentDate

B<Description>: Returns a string of current date in the format HH:MM:SS DD/MM/YYYY.

B<ArgsCount>: 0

B<Return>: (string)

Current date in the format HH:mm:SS DD/MM/YYYY (D=day, M=month, Y=year, H=hour, m=minute, S=second).

B<Example>:

    print 'date: ' . GetCurrentDate() . "\n";

=cut

sub GetCurrentDate
{
    my ($second, $minute, $hour, $day_of_month, $month, $year_offset, $day_of_week, $day_of_year, $daylight_savings) = localtime();
    ++$month;
    my $year = 1900 + $year_offset;
    return sprintf("%02i:%02i:%02i %02i/%02i/%04i", $hour, $minute, $second, $day_of_month, $month, $year);
}


=pod

=head2 GetProgress

B<Description>: Returns a string containing progress status. To prevent too many
report line, it will only return a new line every $interval seconds.

B<ArgsCount>: 1-3

=over 4

=item $progress_elements (integer) (R)

Either a hash ref or an array ref of hash ref. Each hash must contain the
following keys:
-'label': (string) label for progress
-'current': (integer) current element index
-'total': (integer) total number of elements

=item $line_length (integer) (U)

Contains the max number of columns in a line.

=item $interval (integer) (O)

Delay between 2 returned non-empty strings.
Default: 2sec.

=back


B<Return>: (string)

The progress status.

B<Example>:

    print GetProgress({'label' => 'sequences:', 'current' => 34, 'total' => 500,});
    print GetProgress(
        [
            {'label' => 'families:', 'current' => 42, 'total' => 806,},
            {'label' => 'sequences:', 'current' => 33, 'total' => 10000,},
        ],
        80,
        5, # seconds
    );

=cut

sub GetProgress
{
    if (!@_)
    {
        confess "ERROR: GetProgress called without arguments!\n";
    }

    my @progress_elements;
    if ('ARRAY' eq ref($_[0]))
    {
        push(@progress_elements, @{shift()});
    }
    elsif ('HASH' eq ref($_[0]))
    {
        push(@progress_elements, shift());
    }
    else
    {
        confess "ERROR: usage: my \$progress = GetProgress(progress_hashref or arrayref of progress_hashref, line_length);";
    }

    my $line_length = shift() || 80;
    my $interval = shift() || 2;

    # do not return something if the delay between 2 reports is not over
    if ($interval >= (time() - $g_last_progess))
    {
        return '';
    }
    $g_last_progess = time();

    my $progress = "\rProgress: ";
    foreach my $progress_element (@progress_elements)
    {
        $progress .=
            ($progress_element->{'label'} ? $progress_element->{'label'} . ' ' : '')
            . sprintf("%.0f%%    ", 100.*$progress_element->{'current'}/($progress_element->{'total'} || 1) || 0)
        ;
    }


    return sprintf('%- ' . $line_length . 's', $progress);
}


# CODE START
#############

if (IsRunningCGI())
{
    if (GP('DEBUG_MODE'))
    {cluck "DEBUG: GreenPhyl API Running in CGI mode.\n";}
}

# enables debug message only when debug mode set
if (GP('DEBUG_MODE'))
{
    if (GP('DEBUG_MODE') == 2)
    {
        *_PrintDebug = \&_PrintCluckDebug;
    }
    else
    {
        *_PrintDebug = \&_PrintWarnDebug;
    }
}
else
{
    *_PrintDebug = \&_PrintNoDebug;
}

$g_active_dbh_index = SelectActiveDatabase();
if (!IsRunningCGI())
{
    warn "Note: Working on database " . GetDatabaseName() . "\n";
}


# CODE END
###########


=pod

=head1 AUTHORS

Mathieu ROUARD (Bioversity), m.rouard@cgiar.org
Valentin GUIGNON (Bioversity), v.guignon@cgiar.org
Matthieu CONTE (Syngenta)

=head1 VERSION

Version 3.0.0

Date 23/03/12

=head1 SEE ALSO

GreenPhyl documentation.

=cut

return 1; # package return
