-- phpMyAdmin SQL Dump
-- version 3.2.4
-- http://www.phpmyadmin.net
--
-- Host: medoc.cirad.fr:3306
-- Generation Time: Oct 18, 2013 at 05:45 PM
-- Server version: 5.1.41
-- PHP Version: 5.3.1
--
-- Dernières modifications: 02/01/2018
--
-- Pour generer un dump:
--   le faire via phpMyAdmin ou
--   mysqldump -d --routines --triggers -h medoc -u greenphyl greenphyl_prod -p |egrep -v "^SET " >gp_mysql_schema.sql
--
-- Pour obtenir un PNG:
--   -retirer les "COMMENT" dans les définitions de colonnes
--   -retirer les routines et triggers
--   sqlt-diagram -f MySQL -o gp_mysql_schema.png gp_mysql_schema.sql
--
-- --------------------------------------------------------

--
-- Table structure for table 'bbmh'
--

DROP TABLE IF EXISTS bbmh;
CREATE TABLE IF NOT EXISTS bbmh (
  query_sequence_id mediumint(8) unsigned NOT NULL COMMENT 'Query BLAST sequence',
  hit_sequence_id mediumint(8) unsigned NOT NULL COMMENT 'Matching sequence found by BLAST',
  score float NOT NULL COMMENT 'BLAST score',
  e_value double NOT NULL COMMENT 'BLAST result e-value',
  PRIMARY KEY (query_sequence_id,hit_sequence_id),
  KEY hit (hit_sequence_id)
);

-- --------------------------------------------------------

--
-- Table structure for table 'custom_families'
--

DROP TABLE IF EXISTS custom_families;
CREATE TABLE IF NOT EXISTS custom_families (
  id mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  accession char(12) NOT NULL,
  `level` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `type` enum('N/A','superfamily','family','subfamily','group') NOT NULL,
  description text NOT NULL,
  inferences set('Other','IEA:InterPro','IEA:PIRSF','IEA:UniProtKB','IEA:UniProtKB-KW','IEA:KEGG','IEA:MEROPS','IEA:TF','PubMed','TAIR/TIGR') NOT NULL DEFAULT '' COMMENT 'annotation source',
  user_id smallint(5) unsigned NOT NULL,
  `access` enum('private','restricted to group - read only','restricted to group - editable','restricted by password - read only','restricted by password - editable','public - read only','public - editable') NOT NULL,
  password_hash char(40) DEFAULT NULL,
  salt char(40) DEFAULT NULL,
  `data` blob NULL DEFAULT NULL,
  flags SET('listed', 'obsolete', 'peer-reviewed', 'panel-validated', 'conflicting') NOT NULL DEFAULT '',
  creation_date TIMESTAMP NULL DEFAULT NULL,
  last_update TIMESTAMP ON UPDATE CURRENT_TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (id),
  UNIQUE KEY user_id (user_id,accession),
  KEY accession (accession)
);

-- --------------------------------------------------------

--
-- Table structure for table 'custom_families_families_relationships'
--

DROP TABLE IF EXISTS custom_families_families_relationships;
CREATE TABLE IF NOT EXISTS custom_families_families_relationships (
  custom_family_id mediumint(8) unsigned NOT NULL,
  family_id mediumint(8) unsigned NOT NULL,
  `type` enum('other', 'inheritance', 'similar to', 'rearrangment of', 'replaced by', 'derivated from') NOT NULL,
  UNIQUE KEY custom_family_id (custom_family_id,family_id),
  KEY family_id (family_id)
);

-- --------------------------------------------------------

--
-- Table structure for table 'custom_families_history'
--

DROP TABLE IF EXISTS custom_families_history;
CREATE TABLE IF NOT EXISTS custom_families_history (
  id mediumint(8) unsigned NOT NULL,
  `name` varchar(255) NOT NULL,
  accession char(12) NOT NULL,
  `level` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `type` enum('N/A','superfamily','family','subfamily','group') NOT NULL,
  description text NOT NULL,
  inferences set('Other','IEA:InterPro','IEA:PIRSF','IEA:UniProtKB','IEA:UniProtKB-KW','IEA:KEGG','IEA:MEROPS','IEA:TF','PubMed','TAIR/TIGR') NOT NULL DEFAULT '',
  user_id smallint(5) unsigned NOT NULL,
  `access` enum('private','restricted to group - read only','restricted to group - editable','public - read only','public - editable') NOT NULL,
  `data` blob NULL DEFAULT NULL,
  flags varchar(255) NOT NULL,
  creation_date DATETIME NULL DEFAULT NULL,
  last_update TIMESTAMP NULL DEFAULT NULL,
  transaction_date timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'date of transaction',
  transaction_type enum('n/a','insert','update','delete') NOT NULL COMMENT 'type of transaction that changed this value',
  transaction_account varchar(63) NOT NULL COMMENT 'user account that committed the transaction',
  transaction_user varchar(63) NOT NULL COMMENT 'authenticated user that committed the transaction',
  KEY accession (accession),
  KEY id (id)
);

-- --------------------------------------------------------

--
-- Table structure for table 'custom_families_sequences'
--

DROP TABLE IF EXISTS custom_families_sequences;
CREATE TABLE IF NOT EXISTS custom_families_sequences (
  custom_family_id mediumint(8) unsigned NOT NULL,
  custom_sequence_id int(10) unsigned NOT NULL,
  UNIQUE KEY custom_family_id_2 (custom_family_id,custom_sequence_id),
  KEY custom_family_id (custom_family_id),
  KEY sequence_id (custom_sequence_id)
);

-- --------------------------------------------------------

--
-- Table structure for table 'custom_families_sequences_history'
--

DROP TABLE IF EXISTS custom_families_sequences_history;
CREATE TABLE IF NOT EXISTS custom_families_sequences_history (
  custom_family_id mediumint(8) unsigned NOT NULL,
  custom_sequence_id int(10) unsigned NOT NULL,
  transaction_date timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'date of transaction',
  transaction_type enum('n/a','insert','update','delete') NOT NULL COMMENT 'type of transaction that changed this value',
  transaction_account varchar(63) NOT NULL COMMENT 'user account that committed the transaction',
  transaction_user varchar(63) NOT NULL COMMENT 'authenticated user that committed the transaction',
  KEY custom_family_id (custom_family_id),
  KEY sequence_id (custom_sequence_id)
);

-- --------------------------------------------------------

--
-- Table structure for table 'custom_family_relationships'
--

DROP TABLE IF EXISTS custom_family_relationships;
CREATE TABLE IF NOT EXISTS custom_family_relationships (
  subject_custom_family_id mediumint(8) unsigned NOT NULL,
  object_custom_family_id mediumint(8) unsigned NOT NULL,
  level_delta tinyint(3) NOT NULL COMMENT 'Difference between object_custom_family.level and subject_family.level; level_delta=-1 means subject_family is a direct child of object_family; all children of a subject_custom_family will be selected using level_delta>0 and all ancestors using level_delt',
  `type` enum('other', 'inheritance', 'similar to', 'rearrangment of', 'replaced by', 'derivated from') NOT NULL COMMENT 'Type of relationship',
  PRIMARY KEY (subject_custom_family_id,object_custom_family_id),
  KEY subject_custom_family_id (subject_custom_family_id),
  KEY object_custom_family_id (object_custom_family_id)
);

-- --------------------------------------------------------

--
-- Table structure for table 'custom_family_relationships_history'
--

DROP TABLE IF EXISTS custom_family_relationships_history;
CREATE TABLE IF NOT EXISTS custom_family_relationships_history (
  subject_custom_family_id mediumint(8) unsigned NOT NULL,
  object_custom_family_id mediumint(8) unsigned NOT NULL,
  level_delta tinyint(3) NOT NULL COMMENT 'Difference between object_custom_family.level and subject_family.level; level_delta=-1 means subject_family is a direct child of object_family; all children of a subject_custom_family will be selected using level_delta>0 and all ancestors using level_delt',
  `type` varchar(63) NOT NULL COMMENT 'Type of relationship',
  transaction_date timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'date of transaction',
  transaction_type enum('n/a','insert','update','delete') NOT NULL COMMENT 'type of transaction that changed this value',
  transaction_account varchar(63) NOT NULL COMMENT 'user account that committed the transaction',
  transaction_user varchar(63) NOT NULL COMMENT 'authenticated user that committed the transaction',
  KEY subject_custom_family_id (subject_custom_family_id),
  KEY object_custom_family_id (object_custom_family_id)
);

-- --------------------------------------------------------

--
-- Table structure for table 'custom_sequences'
--

DROP TABLE IF EXISTS custom_sequences;
CREATE TABLE IF NOT EXISTS custom_sequences (
  id int(10) unsigned NOT NULL AUTO_INCREMENT,
  accession varchar(127) NOT NULL DEFAULT '',
  locus varchar(127) DEFAULT NULL,
  splice tinyint(3) unsigned DEFAULT NULL,
  splice_priority tinyint(3) unsigned DEFAULT NULL,
  `length` int(10) unsigned NOT NULL DEFAULT '0',
  species_id smallint(4) unsigned NOT NULL,
  organism VARCHAR(63) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  annotation text,
  polypeptide longtext,
  user_id smallint(5) unsigned NOT NULL,
  `data` BLOB NULL DEFAULT NULL,
  last_update timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (id),
  UNIQUE KEY accession_2 (accession,user_id),
  KEY species_id (species_id)
);

-- --------------------------------------------------------

--
-- Table structure for table 'custom_sequences_sequences_relationships'
--

DROP TABLE IF EXISTS custom_sequences_sequences_relationships;
CREATE TABLE IF NOT EXISTS custom_sequences_sequences_relationships (
  custom_sequence_id int(10) unsigned NOT NULL,
  sequence_id mediumint(8) unsigned NOT NULL,
  `type` enum('other','synonym','associated') NOT NULL DEFAULT 'synonym',
  UNIQUE KEY custom_sequence_id (custom_sequence_id,sequence_id),
  KEY custom_sequence_id_2 (custom_sequence_id),
  KEY sequence_id (sequence_id)
);

-- --------------------------------------------------------

--
-- Table structure for table 'db'
--

DROP TABLE IF EXISTS db;
CREATE TABLE IF NOT EXISTS db (
  id tinyint(3) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  description varchar(255) CHARACTER SET latin1 DEFAULT NULL,
  query_url_prefix varchar(255) DEFAULT NULL,
  site_url varchar(255) DEFAULT NULL,
  PRIMARY KEY (id),
  UNIQUE KEY `name` (`name`)
);

-- --------------------------------------------------------

--
-- Table structure for table 'dbxref'
--

DROP TABLE IF EXISTS dbxref;
CREATE TABLE IF NOT EXISTS dbxref (
  id int(10) unsigned NOT NULL AUTO_INCREMENT,
  `db_id` tinyint(3) unsigned NOT NULL,
  accession varchar(255) DEFAULT NULL,
  description text NOT NULL,
  dataset varchar(31) DEFAULT NULL,
  version varchar(255) DEFAULT NULL,
  PRIMARY KEY (id),
  UNIQUE KEY `db_id` (`db_id`,accession,version)
);

-- --------------------------------------------------------

--
-- Table structure for table 'custom_families_dbxref'
--

DROP TABLE IF EXISTS custom_families_dbxref;
CREATE TABLE custom_families_dbxref (
  custom_family_id MEDIUMINT(8) UNSIGNED NOT NULL,
  dbxref_id INT(10) UNSIGNED NOT NULL,
  PRIMARY KEY (custom_family_id,dbxref_id),
  KEY custom_family_id (custom_family_id),
  KEY dbxref_id (dbxref_id)
);

-- --------------------------------------------------------

--
-- Table structure for table 'dbxref_families'
--

DROP TABLE IF EXISTS dbxref_families;
CREATE TABLE IF NOT EXISTS dbxref_families (
  family_id mediumint(10) unsigned NOT NULL,
  dbxref_id int(10) unsigned NOT NULL,
  PRIMARY KEY (family_id,dbxref_id),
  KEY dbxref_id (dbxref_id),
  KEY family_id (family_id)
);

-- --------------------------------------------------------

--
-- Table structure for table 'dbxref_sequences'
--

DROP TABLE IF EXISTS dbxref_sequences;
CREATE TABLE IF NOT EXISTS dbxref_sequences (
  sequence_id mediumint(8) unsigned NOT NULL,
  dbxref_id int(10) unsigned NOT NULL,
  PRIMARY KEY (sequence_id,dbxref_id),
  KEY dbxref_id (dbxref_id)
);

-- --------------------------------------------------------

--
-- Table structure for table 'dbxref_synonyms'
--

DROP TABLE IF EXISTS dbxref_synonyms;
CREATE TABLE IF NOT EXISTS dbxref_synonyms (
  dbxref_id int(10) unsigned NOT NULL,
  `synonym` varchar(255) NOT NULL,
  PRIMARY KEY (dbxref_id,`synonym`)
);

-- --------------------------------------------------------

--
-- Table structure for table 'ec_go'
--

DROP TABLE IF EXISTS ec_go;
CREATE TABLE IF NOT EXISTS ec_go (
  dbxref_id int(10) unsigned NOT NULL,
  go_id mediumint(8) unsigned NOT NULL,
  PRIMARY KEY (dbxref_id,go_id),
  KEY go_id (go_id),
  KEY dbxref_id (dbxref_id)
);

-- --------------------------------------------------------

--
-- Table structure for table 'evidence_codes'
--

DROP TABLE IF EXISTS evidence_codes;
CREATE TABLE IF NOT EXISTS evidence_codes (
  `code` varchar(3) NOT NULL COMMENT 'Evidence code (usually 2 or 3 characters)',
  `category` varchar(31) NOT NULL,
  description varchar(127) NOT NULL,
  PRIMARY KEY (`code`)
);

-- --------------------------------------------------------

--
-- Table structure for table 'families'
--

DROP TABLE IF EXISTS families;
CREATE TABLE IF NOT EXISTS families (
  id mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL DEFAULT '' COMMENT 'Human-readable family name',
  accession char(12) NOT NULL DEFAULT '' COMMENT 'Family accession (cross-release identifier); GreenPhyl family accession must be upper case and begin with GP',
  `level` enum('1','2','3','4') DEFAULT NULL COMMENT 'Family classification level',
  `type` enum('N/A','superfamily','family','subfamily','group','core','soft-core','dispensable','specific') NOT NULL
    COMMENT 'Family cluster type or pangenome type. Core=100%, soft-core=95-99%, dispensable=up-to-94%, specific=single genome.',
  description text NOT NULL COMMENT 'Family description and comments',
  inferences set('Other','IEA:InterPro','IEA:PIRSF','IEA:UniProtKB','IEA:UniProtKB-KW','IEA:KEGG','IEA:MEROPS','IEA:TF','PubMed','TAIR/TIGR') NOT NULL DEFAULT '' COMMENT 'annotation source',
  validated enum('N/A','high','normal','unknown','suspicious','clustering error') NOT NULL COMMENT 'Human validation status',
  black_listed tinyint(1) NOT NULL DEFAULT '0' COMMENT 'Black-listed families are not considered as valid families',
  taxonomy_id int(10) unsigned DEFAULT NULL COMMENT 'Family taxonomy root node (the most specific)',
  plant_specific tinyint(3) NOT NULL COMMENT 'Tells if the family is specific to plants',
  min_sequence_length smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Minimum sequence length',
  max_sequence_length smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Maximum sequence length',
  average_sequence_length smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Average sequence length',
  median_sequence_length smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Median sequence length',
  alignment_length smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Number of columns in the initial alignment',
  masked_alignment_length smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Number of columns in the alignment after masking',
  sequence_count smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Number of sequences in the initial alignment',
  alignment_sequence_count smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Number of sequences in the initial alignment',
  masked_alignment_sequence_count smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Number of sequences in the alignment after masking',
  branch_average float NULL DEFAULT NULL COMMENT 'Phylogenetic tree branch depth average',
  branch_median float NULL DEFAULT NULL COMMENT 'Phylogenetic tree branch depth median',
  branch_standard_deviation float NULL DEFAULT NULL COMMENT 'Phylogenetic tree branch depth standard deviation',
  PRIMARY KEY (id),
  KEY tax_id (taxonomy_id),
  UNIQUE KEY accession (accession),
  KEY `level` (`level`)
);

-- --------------------------------------------------------

--
-- Table structure for table 'families_cache'
--

DROP TABLE IF EXISTS families_cache;
CREATE TABLE IF NOT EXISTS families_cache (
  id mediumint(8) unsigned NOT NULL,
  `name` varchar(255) NOT NULL DEFAULT '' COMMENT 'Human-readable family name',
  accession char(12) NOT NULL DEFAULT '' COMMENT 'Family accession (cross-release identifier); GreenPhyl family accession must be upper case and begin with GP',
  `level` TINYINT NULL DEFAULT NULL COMMENT 'Family classification level',
  `type` enum('N/A','superfamily','family','subfamily','group','core','soft-core','dispensable','specific') NOT NULL
    COMMENT 'Family cluster type or pangenome type. Core=100%, soft-core=95-99%, dispensable=up-to-94%, specific=single genome.',
  description text NOT NULL COMMENT 'Family description and comments',
  inferences set('Other','IEA:InterPro','IEA:PIRSF','IEA:UniProtKB','IEA:UniProtKB-KW','IEA:KEGG','IEA:MEROPS','IEA:TF','PubMed','TAIR/TIGR') NOT NULL DEFAULT '' COMMENT 'annotation source',
  validated enum('N/A','high','normal','unknown','suspicious','clustering error') NOT NULL COMMENT 'Human validation status',
  black_listed tinyint(1) NOT NULL DEFAULT '0' COMMENT 'Black-listed families are not considered as valid families',
  taxonomy_id int(10) unsigned DEFAULT NULL COMMENT 'Family taxonomy root node (the most specific)',
  plant_specific tinyint(3) NOT NULL COMMENT 'Tells if the family is specific to plants',
  min_sequence_length smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Minimum sequence length',
  max_sequence_length smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Maximum sequence length',
  average_sequence_length smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Average sequence length',
  median_sequence_length smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Median sequence length',
  alignment_length smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Number of columns in the initial alignment',
  masked_alignment_length smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Number of columns in the alignment after masking',
  sequence_count smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Number of sequences in the initial alignment',
  alignment_sequence_count smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Number of sequences in the initial alignment',
  masked_alignment_sequence_count smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Number of sequences in the alignment after masking',
  branch_average float NULL DEFAULT NULL COMMENT 'Phylogenetic tree branch depth average',
  branch_median float NULL DEFAULT NULL COMMENT 'Phylogenetic tree branch depth median',
  branch_standard_deviation float NULL DEFAULT NULL COMMENT 'Phylogenetic tree branch depth standard deviation',
  ipr_cache LONGBLOB COMMENT '',
  sequence_counts_cache LONGBLOB COMMENT '',
  homologies_cache LONGBLOB COMMENT '',
  sequences_cache MEDIUMBLOB COMMENT '',
  panfamily_cache MEDIUMBLOB COMMENT '',
  KEY (id),
  KEY tax_id (taxonomy_id),
  KEY accession (accession),
  KEY `level` (`level`)
  )
  PARTITION BY LIST(`level`) (
    PARTITION level1 VALUES IN (1),
    PARTITION level2 VALUES IN (2),
    PARTITION level3 VALUES IN (3),
    PARTITION level4 VALUES IN (4),
    PARTITION pangene VALUES IN (NULL)
  );


-- --------------------------------------------------------

--
-- Table structure for table 'families_go_cache'
--

DROP TABLE IF EXISTS families_go_cache;
CREATE TABLE IF NOT EXISTS families_go_cache (
  go_id mediumint(8) unsigned NOT NULL,
  family_id mediumint(8) unsigned NOT NULL,
  percent tinyint(3) unsigned DEFAULT NULL COMMENT 'Percentage of sequences of the family having the GO',
  PRIMARY KEY (go_id,family_id),
  KEY go_id (go_id),
  KEY family_id (family_id)
);

-- --------------------------------------------------------

--
-- Table structure for table 'families_history'
--

DROP TABLE IF EXISTS families_history;
CREATE TABLE IF NOT EXISTS families_history (
  id mediumint(8) unsigned NOT NULL,
  `name` varchar(255) NOT NULL DEFAULT '' COMMENT 'Human-readable family name',
  accession char(8) NOT NULL DEFAULT '' COMMENT 'Family accession (cross-release identifier); GreenPhyl family accession must be upper case and begin with GP',
  `level` enum('1','2','3','4') DEFAULT NULL COMMENT 'Family classification level',
  `type` enum('N/A','superfamily','family','subfamily','group') NOT NULL COMMENT 'Family cluster type',
  description text NOT NULL COMMENT 'Family description and comments',
  inferences set('Other','IEA:InterPro','IEA:PIRSF','IEA:UniProtKB','IEA:UniProtKB-KW','IEA:KEGG','IEA:MEROPS','IEA:TF','PubMed','TAIR/TIGR') NOT NULL DEFAULT '' COMMENT 'annotation source',
  validated enum('N/A','high','normal','unknown','suspicious','clustering error') NOT NULL COMMENT 'Human validation status',
  black_listed tinyint(1) NOT NULL DEFAULT '0' COMMENT 'Black-listed families are not considered as valid families',
  taxonomy_id int(10) unsigned DEFAULT NULL COMMENT 'Family taxonomy root node (the most specific)',
  plant_specific enum('N/A','not plant-specific','plant-specific') NOT NULL COMMENT 'Tells if the family is specific to plants',
  min_sequence_length smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Minimum sequence length',
  max_sequence_length smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Maximum sequence length',
  average_sequence_length smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Average sequence length',
  median_sequence_length smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Median sequence length',
  alignment_length smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Number of columns in the initial alignment',
  masked_alignment_length smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Number of columns in the alignment after masking',
  sequence_count smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Number of sequences in the initial alignment',
  alignment_sequence_count smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Number of sequences in the initial alignment',
  masked_alignment_sequence_count smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Number of sequences in the alignment after masking',
  transaction_date timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  transaction_type enum('n/a','insert','update','delete') NOT NULL,
  transaction_account varchar(63) NOT NULL COMMENT 'user account that committed the transaction',
  transaction_user varchar(63) NOT NULL COMMENT 'authenticated user that committed the transaction',
  KEY tax_id (taxonomy_id),
  KEY accession (accession),
  KEY `level` (`level`),
  KEY id (id),
  KEY id_2 (id)
);

-- --------------------------------------------------------

--
-- Table structure for table 'families_ipr_cache'
--

DROP TABLE IF EXISTS families_ipr_cache;
CREATE TABLE IF NOT EXISTS families_ipr_cache (
  family_id mediumint(10) unsigned NOT NULL DEFAULT '0',
  ipr_id mediumint(10) unsigned NOT NULL DEFAULT '0',
  sequence_count mediumint(8) unsigned DEFAULT NULL,
  percent tinyint(3) unsigned DEFAULT NULL COMMENT 'Percentage of sequence having the IPR for the family',
  family_specific tinyint(1) DEFAULT NULL COMMENT 'IPR specific to the family',
  PRIMARY KEY (family_id,ipr_id),
  KEY ipr_id (ipr_id)
);

-- --------------------------------------------------------

--
-- Table structure for table 'families_ipr_species_cache'
--

DROP TABLE IF EXISTS families_ipr_species_cache;
CREATE TABLE IF NOT EXISTS families_ipr_species_cache (
  family_id mediumint(10) unsigned NOT NULL DEFAULT '0',
  ipr_id mediumint(10) unsigned NOT NULL DEFAULT '0',
  species_id smallint(4) unsigned NOT NULL,
  PRIMARY KEY (family_id,ipr_id,species_id),
  KEY ipr_id (ipr_id),
  KEY species_id (species_id)
);

-- --------------------------------------------------------

--
-- Table structure for table 'families_sequences'
--

DROP TABLE IF EXISTS families_sequences;
CREATE TABLE IF NOT EXISTS families_sequences (
  sequence_id mediumint(8) unsigned NOT NULL DEFAULT '0',
  family_id mediumint(10) unsigned NOT NULL DEFAULT '0',
  `type` enum('participating','representative','consensus') NOT NULL DEFAULT 'participating' COMMENT 'Sequence role',
  PRIMARY KEY (sequence_id,family_id),
  KEY family_id (family_id),
  KEY sequence_id (sequence_id)
);

-- --------------------------------------------------------

--
-- Table structure for table 'family_annotations'
--

DROP TABLE IF EXISTS family_annotations;
CREATE TABLE IF NOT EXISTS family_annotations (
  id mediumint(8) NOT NULL AUTO_INCREMENT,
  user_id smallint(5) unsigned DEFAULT NULL COMMENT '0 = automatic 1= admin ',
  user_name varchar(255) NOT NULL DEFAULT '',
  accession char(8) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `type` enum('N/A','superfamily','family','subfamily','group') DEFAULT NULL COMMENT 'Family cluster type',
  synonyms varchar(255) DEFAULT NULL,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `status` enum('in progress','submitted','validated','denied') NOT NULL,
  confidence_level enum('N/A','high','normal','unknown','suspicious','clustering error') DEFAULT NULL COMMENT 'Human annotation confidence level',
  inferences set('IEA:InterPro','IEA:PIRSF','IEA:UniProtKB','IEA:UniProtKB-KW','IEA:KEGG','IEA:MEROPS','IEA:TF','PubMed','TAIR/TIGR','Other') DEFAULT NULL COMMENT 'annotation source',
  dbxref mediumtext,
  description text NOT NULL COMMENT 'Family description and comments',
  comments text NOT NULL COMMENT 'User comments about his/her annotation or the family',
  PRIMARY KEY (id),
  UNIQUE KEY user_id (user_id,accession),
  KEY family_id (accession)
);

-- --------------------------------------------------------

--
-- Table structure for table 'family_processing'
--

DROP TABLE IF EXISTS family_processing;
CREATE TABLE IF NOT EXISTS family_processing (
  family_id mediumint(8) unsigned NOT NULL,
  steps_to_perform set('other','fasta','hmm','mafft','trimal','phyml','rap','web transfer','statistics','orthology','meme/mast','plant specificity','taxonomy specificity') NOT NULL,
  steps_performed set('other','fasta','hmm','maft','trimal','phyml','rap','web transfer','statistics','orthology','meme/mast','plant specificity','taxonomy specificity') NOT NULL,
  `status` enum('pending','in progress','done','canceled','error','warning') NOT NULL,
  process_id smallint(5) unsigned NOT NULL,
  UNIQUE KEY family_id_2 (family_id,process_id),
  KEY family_id (family_id),
  KEY process_id (process_id)
);

-- --------------------------------------------------------

--
-- Table structure for table 'family_relationships_cache'
--

DROP TABLE IF EXISTS family_relationships_cache;
CREATE TABLE IF NOT EXISTS family_relationships_cache (
  subject_family_id mediumint(8) unsigned NOT NULL,
  object_family_id mediumint(8) unsigned NOT NULL,
  level_delta tinyint(4) NOT NULL COMMENT 'Difference between object_family.level and subject_family.level; level_delta=-1 means subject_family is a direct child of object_family; all children of a subject_family will be selected using level_delta>0 and all ancestors using level_delta<0',
  `type` enum('other', 'inheritance', 'similar to', 'rearrangment of', 'replaced by', 'derivated from') NOT NULL COMMENT 'type of relationship',
  PRIMARY KEY (subject_family_id,object_family_id),
  KEY subject_family_id (subject_family_id),
  KEY object_family_id (object_family_id)
);

-- --------------------------------------------------------

--
-- Table structure for table 'family_synonyms'
--

DROP TABLE IF EXISTS family_synonyms;
CREATE TABLE IF NOT EXISTS family_synonyms (
  family_id mediumint(10) unsigned NOT NULL,
  `synonym` varchar(255) NOT NULL COMMENT 'synonym of the family name in family table',
  PRIMARY KEY (family_id,`synonym`)
);

-- --------------------------------------------------------

--
-- Table structure for table 'family_transfer_history'
--

DROP TABLE IF EXISTS family_transfer_history;
CREATE TABLE IF NOT EXISTS family_transfer_history (
  old_family_id mediumint(10) unsigned NOT NULL,
  new_family_id mediumint(10) unsigned NOT NULL,
  transaction_date timestamp NULL DEFAULT NULL,
  transaction_type enum('none','annotation_transfer') NOT NULL,
  sequences_in_old_family int(11) NOT NULL,
  sequences_in_new_family int(11) NOT NULL,
  additional_info longtext NOT NULL,
  KEY new_family_id (new_family_id),
  KEY old_family_id (old_family_id)
);

-- --------------------------------------------------------

--
-- Table structure for table 'go'
--

DROP TABLE IF EXISTS go;
CREATE TABLE IF NOT EXISTS go (
  id mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
  `code` char(10) NOT NULL,
  namespace enum('cellular_component','molecular_function','biological_process') NOT NULL,
  `name` varchar(255) NOT NULL,
  PRIMARY KEY (id),
  UNIQUE KEY `code` (`code`)
);

-- --------------------------------------------------------

--
-- Table structure for table 'go_alternates'
--

DROP TABLE IF EXISTS go_alternates;
CREATE TABLE IF NOT EXISTS go_alternates (
  go_id mediumint(8) unsigned NOT NULL,
  alternate_code char(10) NOT NULL,
  UNIQUE KEY go_id_2 (go_id,alternate_code),
  KEY go_id (go_id)
);

-- --------------------------------------------------------

--
-- Table structure for table 'go_ipr'
--

DROP TABLE IF EXISTS go_ipr;
CREATE TABLE IF NOT EXISTS go_ipr (
  ipr_id mediumint(10) unsigned NOT NULL,
  go_id mediumint(8) unsigned NOT NULL,
  evidence_code varchar(3) DEFAULT NULL,
  PRIMARY KEY (ipr_id,go_id),
  KEY go_id (go_id),
  KEY ipr_id (ipr_id),
  KEY evidence_code (evidence_code)
);

-- --------------------------------------------------------

--
-- Table structure for table 'go_lineage'
--

DROP TABLE IF EXISTS go_lineage;
CREATE TABLE IF NOT EXISTS go_lineage (
  go_id mediumint(8) unsigned NOT NULL,
  lineage_go_codes mediumtext NOT NULL COMMENT 'up to 46 coma-separated GO codes',
  UNIQUE KEY go_id (go_id)
);

-- --------------------------------------------------------

--
-- Table structure for table 'go_relationships'
--

DROP TABLE IF EXISTS go_relationships;
CREATE TABLE IF NOT EXISTS go_relationships (
  subject_go_id mediumint(8) unsigned NOT NULL,
  object_go_id mediumint(8) unsigned NOT NULL,
  relationship varchar(255) NOT NULL COMMENT 'Relationships like "is_a", "part_of", "regulates",...',
  UNIQUE KEY subject_go_id_3 (subject_go_id,object_go_id,relationship),
  KEY subject_go_id (subject_go_id,object_go_id),
  KEY subject_go_id_2 (subject_go_id),
  KEY object_go_id (object_go_id)
);

-- --------------------------------------------------------

--
-- Table structure for table 'go_sequences_cache'
--

DROP TABLE IF EXISTS go_sequences_cache;
CREATE TABLE IF NOT EXISTS go_sequences_cache (
  go_id mediumint(8) unsigned NOT NULL,
  sequence_id mediumint(8) unsigned NOT NULL,
  ipr_id mediumint(8) unsigned DEFAULT NULL,
  uniprot_id int(10) unsigned DEFAULT NULL,
  ec_id int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (go_id,sequence_id),
  KEY go_id (go_id),
  KEY sequence_id (sequence_id),
  KEY ec_id (ec_id),
  KEY uniprot_id (uniprot_id),
  KEY ipr_id (ipr_id)
);

-- --------------------------------------------------------

--
-- Table structure for table 'go_synonyms'
--

DROP TABLE IF EXISTS go_synonyms;
CREATE TABLE IF NOT EXISTS go_synonyms (
  go_id mediumint(8) unsigned NOT NULL,
  `synonym` varchar(255) NOT NULL,
  KEY go_id (go_id)
);

-- --------------------------------------------------------

--
-- Table structure for table 'go_uniprot'
--

DROP TABLE IF EXISTS go_uniprot;
CREATE TABLE IF NOT EXISTS go_uniprot (
  dbxref_id int(10) unsigned NOT NULL,
  go_id mediumint(8) unsigned NOT NULL,
  evidence_code varchar(3) DEFAULT NULL,
  PRIMARY KEY (dbxref_id,go_id),
  KEY go_id (go_id),
  KEY dbxref_id (dbxref_id),
  KEY evidence_code (evidence_code)
);

-- --------------------------------------------------------

--
-- Table structure for table 'greenphyl_logs'
--

DROP TABLE IF EXISTS greenphyl_logs;
CREATE TABLE IF NOT EXISTS greenphyl_logs (
  id bigint(20) NOT NULL AUTO_INCREMENT,
  `time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  script varchar(31) NOT NULL DEFAULT '',
  pid int(11) NOT NULL DEFAULT '0',
  `type`  ENUM('debug','info','warning','error') CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT 'info',
  message text NOT NULL,
  PRIMARY KEY (id)
);

-- --------------------------------------------------------

--
-- Table structure for table 'groups'
--

DROP TABLE IF EXISTS `groups`;
CREATE TABLE IF NOT EXISTS `groups` (
  id smallint(5) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  description text NOT NULL,
  PRIMARY KEY (id),
  UNIQUE KEY `name` (`name`)
);

-- --------------------------------------------------------

--
-- Table structure for table 'groups_users'
--

DROP TABLE IF EXISTS groups_users;
CREATE TABLE IF NOT EXISTS groups_users (
  group_id smallint(5) unsigned NOT NULL,
  user_id smallint(5) unsigned NOT NULL,
  UNIQUE KEY group_id (group_id,user_id),
  KEY user_id (user_id)
);

-- --------------------------------------------------------

--
-- Table structure for table 'homologies'
--

DROP TABLE IF EXISTS homologies;
CREATE TABLE IF NOT EXISTS homologies (
  id int(10) unsigned NOT NULL AUTO_INCREMENT,
  family_id mediumint(10) unsigned NOT NULL,
  `type` enum('orthology','super-orthology','ultra-paralogy') NOT NULL,
  score float DEFAULT NULL,
  kvalue int(11) NOT NULL,
  distance float NOT NULL,
  speciation int(11) NOT NULL,
  i_duplication int(11) NOT NULL,
  t_duplication int(11) NOT NULL,
  u_duplication int(11) NOT NULL,
  PRIMARY KEY (id),
  KEY `type` (`type`),
  KEY family_id (family_id)
);

-- --------------------------------------------------------

--
-- Table structure for table 'homologs'
--

DROP TABLE IF EXISTS homologs;
CREATE TABLE IF NOT EXISTS homologs (
  homology_id int(10) unsigned NOT NULL,
  subject_sequence_id mediumint(8) unsigned NOT NULL COMMENT 'Sequence we focus on',
  object_sequence_id mediumint(8) unsigned NOT NULL COMMENT 'Related sequence',
  PRIMARY KEY (subject_sequence_id,object_sequence_id),
  KEY homology_id (homology_id),
  KEY subject_sequence_id (subject_sequence_id),
  KEY object_sequence_id (object_sequence_id)
);

-- --------------------------------------------------------

--
-- Table structure for table 'ipr'
--

DROP TABLE IF EXISTS ipr;
CREATE TABLE IF NOT EXISTS ipr (
  id mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
  `code` char(9) NOT NULL COMMENT 'Code assigned by Interpro for a protein pattern',
  description varchar(255) DEFAULT NULL COMMENT 'Annotation of the protein pattern',
  `type` varchar(63) NOT NULL COMMENT 'IPR pattern might be family, domain etc.',
  version tinyint(3) unsigned DEFAULT NULL COMMENT 'Version of interpro used with interproscan',
  pirsf_code char(11) DEFAULT NULL COMMENT 'IPR having as source PIR database',
  pirsf_description varchar(255) DEFAULT NULL COMMENT 'Annotation of the PIR homeomorphic family',
  PRIMARY KEY (id),
  UNIQUE KEY ipr_code (`code`)
);

-- --------------------------------------------------------

--
-- Table structure for table 'ipr_sequences'
--

DROP TABLE IF EXISTS ipr_sequences;
CREATE TABLE IF NOT EXISTS ipr_sequences (
  sequence_id mediumint(8) unsigned NOT NULL DEFAULT '0',
  ipr_id mediumint(8) unsigned NOT NULL DEFAULT '0',
  `start` int(10) unsigned NOT NULL DEFAULT '0',
  `end` int(10) unsigned NOT NULL DEFAULT '0',
  score double DEFAULT NULL,
  PRIMARY KEY (sequence_id,ipr_id),
  KEY ipr_id (ipr_id),
  KEY sequence_id (sequence_id)
);

-- --------------------------------------------------------

--
-- Table structure for table 'lineage'
--

DROP TABLE IF EXISTS lineage;
CREATE TABLE IF NOT EXISTS lineage (
  taxonomy_id int(10) unsigned NOT NULL,
  lineage_taxonomy_id int(10) unsigned NOT NULL,
  UNIQUE KEY unique_lineage (taxonomy_id,lineage_taxonomy_id),
  KEY tax_id (taxonomy_id),
  KEY lineage_tax_id (lineage_taxonomy_id)
);

-- --------------------------------------------------------

--
-- Table structure for table 'my_lists'
--

DROP TABLE IF EXISTS my_lists;
CREATE TABLE IF NOT EXISTS my_lists (
  id int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL DEFAULT 'my list',
  description text NOT NULL,
  user_id smallint(5) unsigned NOT NULL,
  PRIMARY KEY (id),
  KEY user_id (user_id)
);

-- --------------------------------------------------------

--
-- Table structure for table 'my_list_items'
--

DROP TABLE IF EXISTS my_list_items;
CREATE TABLE IF NOT EXISTS my_list_items (
  my_list_id int(10) unsigned NOT NULL,
  foreign_id int(10) unsigned NOT NULL,
  `type` enum('Sequence','Family','CustomSequence','CustomFamily') NOT NULL,
  UNIQUE KEY my_list_id (my_list_id,foreign_id,`type`)
);

-- --------------------------------------------------------

--
-- Table structure for table 'processes'
--

DROP TABLE IF EXISTS processes;
CREATE TABLE IF NOT EXISTS processes (
  id smallint(5) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(63) NOT NULL,
  description text NOT NULL,
  `status` enum('to perform','in progress','paused','stopped','done','canceled') NOT NULL COMMENT 'Process status note: a ''paused'' process has been manually halted while a ''stopped'' process has been halted by a software error.',
  start_date timestamp NULL DEFAULT NULL,
  last_update timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  duration int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Duration in seconds',
  PRIMARY KEY (id)
);

-- --------------------------------------------------------

--
-- Table structure for table 'sequences'
--

DROP TABLE IF EXISTS sequences;
CREATE TABLE IF NOT EXISTS sequences (
  id mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
  accession varchar(127) NOT NULL DEFAULT '',
  locus varchar(127) DEFAULT NULL,
  splice tinyint(3) unsigned DEFAULT NULL,
  splice_priority tinyint(3) unsigned DEFAULT NULL,
  `length` int(10) unsigned NOT NULL DEFAULT '0',
  species_id smallint(4) unsigned NOT NULL,
  genome_id smallint(4) unsigned NOT NULL,
  annotation text,
  polypeptide longtext,
  representative tinyint(1) NOT NULL DEFAULT '0' COMMENT 'species representative sequence to be displayed',
  filtered tinyint(1) NOT NULL DEFAULT '0' COMMENT 'meme filtration procedure',
  `type` enum('protein','dna','pan protein','pan dna') NOT NULL COMMENT 'Type of sequence',
  PRIMARY KEY (id),
  UNIQUE KEY seq_textid_2 (accession),
  KEY species_id (species_id),
  KEY genome_id (genome_id)
);

-- --------------------------------------------------------

--
-- Table structure for table 'sequences_cache'
--

DROP TABLE IF EXISTS sequences_cache;
CREATE TABLE IF NOT EXISTS sequences_cache (
  id mediumint(8) unsigned NOT NULL,
  accession varchar(127) NOT NULL DEFAULT '',
  locus varchar(127) DEFAULT NULL,
  splice tinyint(3) unsigned DEFAULT NULL,
  splice_priority tinyint(3) unsigned DEFAULT NULL,
  `length` int(10) unsigned NOT NULL DEFAULT '0',
  species_id smallint(4) unsigned NOT NULL,
  genome_id smallint(4) unsigned NOT NULL,
  annotation text,
  polypeptide longtext,
  representative tinyint(1) NOT NULL DEFAULT '0',
  filtered tinyint(1) NOT NULL DEFAULT '0',
  `type` enum('protein','dna','pan protein','pan dna') NOT NULL,
  bbmh_cache LONGBLOB,
  homologs_cache LONGBLOB,
  ipr_cache LONGBLOB,
  level_1_family_id mediumint(8) unsigned,
  level_2_family_id mediumint(8) unsigned,
  level_3_family_id mediumint(8) unsigned,
  level_4_family_id mediumint(8) unsigned,
  pangene_family_id mediumint(8) unsigned,
  KEY (id),
  KEY seq_textid_2 (accession),
  KEY species_id (species_id),
  KEY genome_id (genome_id)
  )
  PARTITION BY LIST(genome_id) (
    PARTITION AMBTC_v1_0                         VALUES IN (1),
    PARTITION MUSAC_Macba2                       VALUES IN (2),
    PARTITION MUSAC_Macbu1                       VALUES IN (3),
    PARTITION MUSAC_Macma2                       VALUES IN (4),
    PARTITION MUSAC_Macze1                       VALUES IN (5),
    PARTITION MUSAC                              VALUES IN (6),
    PARTITION ARATH_Araport11                    VALUES IN (37),
    PARTITION BETVU_RefBeet_v1_2                 VALUES IN (38),
    PARTITION BRANA_Darmor_v8_1                  VALUES IN (39),
    PARTITION BRANA_Tapidor_v6_3                 VALUES IN (40),
    PARTITION BRANA_v5_0                         VALUES IN (41),
    PARTITION BRANA                              VALUES IN (42),
    PARTITION BRAOL_A2_v1_0                      VALUES IN (43),
    PARTITION BRAOL_HDEM                         VALUES IN (44),
    PARTITION BRAOL_TO1000                       VALUES IN (45),
    PARTITION BRAOL                              VALUES IN (46),
    PARTITION BRARR_Z1                           VALUES IN (47),
    PARTITION BRARR_v3_0                         VALUES IN (48),
    PARTITION BRARR                              VALUES IN (49),
    PARTITION CAJCA_Asha_v1_0                    VALUES IN (50),
    PARTITION CAPAN_CM334_v2_0                   VALUES IN (51),
    PARTITION CAPAN_Glabriusculum_Chiltepin_v2_0 VALUES IN (52),
    PARTITION CAPAN_Zunla_1_v2_0                 VALUES IN (53),
    PARTITION CAPAN                              VALUES IN (54),
    PARTITION CHEQI_JGI_v1_0                     VALUES IN (55),
    PARTITION CICAR_CDC_Frontier_kabuli          VALUES IN (56),
    PARTITION CICAR_ICC_4958_desi                VALUES IN (57),
    PARTITION CICAR                              VALUES IN (58),
    PARTITION CITMA_v1_0                         VALUES IN (59),
    PARTITION CITME_v1_0                         VALUES IN (60),
    PARTITION CITSI_v2_0                         VALUES IN (61),
    PARTITION COCNU_C3B02_v2_0                   VALUES IN (62),
    PARTITION COCNU_CATD                         VALUES IN (63),
    PARTITION COCNU                              VALUES IN (64),
    PARTITION COFAR_RB_v1_0                      VALUES IN (65),
    PARTITION COFCA_v1_0                         VALUES IN (66),
    PARTITION CUCME_DHL92_v3_6_1                 VALUES IN (67),
    PARTITION CUCSA_ASM407_v2_0                  VALUES IN (68),
    PARTITION CUCSA_Gy14_v2_0                    VALUES IN (69),
    PARTITION CUCSA_PI183967                     VALUES IN (70),
    PARTITION CUCSA                              VALUES IN (71),
    PARTITION DAUCA_JGI_v2_0                     VALUES IN (72),
    PARTITION DIORT_TDr96_F1_v1_0                VALUES IN (73),
    PARTITION ELAGV_RefSeq_v1_0                  VALUES IN (74),
    PARTITION FRAVE_v4_0a1                       VALUES IN (75),
    PARTITION HELAN_XRQ_v1_2                     VALUES IN (76),
    PARTITION HORVU_IBSC_v2_0                    VALUES IN (77),
    PARTITION IPOTF_NSP306                       VALUES IN (78),
    PARTITION IPOTF_Y22                          VALUES IN (79),
    PARTITION IPOTF                              VALUES IN (80),
    PARTITION IPOTR_NSP323                       VALUES IN (81),
    PARTITION MALDO_Borkh_v3_0_a1                VALUES IN (82),
    PARTITION MALDO_GDDH13_v1_1                  VALUES IN (83),
    PARTITION MALDO_HFTH1                        VALUES IN (84),
    PARTITION MALDO                              VALUES IN (85),
    PARTITION MANES_AM560_2_v6_1                 VALUES IN (86),
    PARTITION MEDTR_A17_r5_0                     VALUES IN (87),
    PARTITION MEDTR_Mt4_0v2                      VALUES IN (88),
    PARTITION MEDTR                              VALUES IN (89),
    PARTITION MUSBA_Mba1_1                       VALUES IN (90),
    PARTITION OLEEU_v1_0                         VALUES IN (91),
    PARTITION ORYGL_v1_0                         VALUES IN (92),
    PARTITION ORYSA_IR8_v1_0                     VALUES IN (93),
    PARTITION ORYSA_Kitaake_v3_1                 VALUES IN (94),
    PARTITION ORYSA_MH63_v1_0                    VALUES IN (95),
    PARTITION ORYSA_N22_v1_0                     VALUES IN (96),
    PARTITION ORYSA_Nipponbare_v7_0              VALUES IN (97),
    PARTITION ORYSA_R498_IGDB_v3_0               VALUES IN (98),
    PARTITION ORYSA_ZS97_v1_0                    VALUES IN (99),
    PARTITION ORYSA                              VALUES IN (100),
    PARTITION PHAVU_v2_0                         VALUES IN (101),
    PARTITION PHODC_v3_0                         VALUES IN (102),
    PARTITION SACSP_AP85_441_v1_0                VALUES IN (103),
    PARTITION SOLLC_ITAG3_2                      VALUES IN (104),
    PARTITION SOLTU_PGSC_v4_03                   VALUES IN (105),
    PARTITION SORBI_BTx623_v3_1_1                VALUES IN (106),
    PARTITION SORBI_Tx430_v1_0                   VALUES IN (107),
    PARTITION SORBI                              VALUES IN (108),
    PARTITION SOYBN_Wm82_a2_v1_0                 VALUES IN (109),
    PARTITION SOYBN_ZH13                         VALUES IN (110),
    PARTITION SOYBN                              VALUES IN (111),
    PARTITION THECC_Criollo_v2_0                 VALUES IN (112),
    PARTITION THECC_Matina1_6_v1_1               VALUES IN (113),
    PARTITION THECC                              VALUES IN (114),
    PARTITION TRITU_Svevo_v2_0                   VALUES IN (115),
    PARTITION TRITU_Zavitan_v1_1                 VALUES IN (116),
    PARTITION TRITU                              VALUES IN (117),
    PARTITION VITVI_12X_2                        VALUES IN (118),
    PARTITION VITVI_CabernetSauvignon_v1_0       VALUES IN (119),
    PARTITION VITVI_Carmenere_v1_0               VALUES IN (120),
    PARTITION VITVI                              VALUES IN (121),
    PARTITION MAIZE_B73_v4_0                     VALUES IN (122),
    PARTITION MAIZE_CML247_v1_0                  VALUES IN (123),
    PARTITION MAIZE_Mo17_v1_0                    VALUES IN (124),
    PARTITION MAIZE_PH207_v1_0                   VALUES IN (125),
    PARTITION MAIZE                              VALUES IN (126),
    PARTITION BRADI_ABR2_337_v1_ABR2             VALUES IN (127),
    PARTITION BRADI_ABR3_343_v1_ABR3             VALUES IN (128),
    PARTITION BRADI_ABR4_364_v1_ABR4             VALUES IN (129),
    PARTITION BRADI_ABR5_379_v1_ABR5             VALUES IN (130),
    PARTITION BRADI_ABR6_336_v1_ABR6_r           VALUES IN (131),
    PARTITION BRADI_ABR7_369_v1_ABR7             VALUES IN (132),
    PARTITION BRADI_ABR8_356_v1_ABR8             VALUES IN (133),
    PARTITION BRADI_ABR9_333_v1_ABR9_r           VALUES IN (134),
    PARTITION BRADI_Adi_10_381_v1_Adi_10         VALUES IN (135),
    PARTITION BRADI_Adi_12_359_v1_Adi_12         VALUES IN (136),
    PARTITION BRADI_Adi_2_372_v1_Adi_2           VALUES IN (137),
    PARTITION BRADI_Arn1_355_v1_Arn1             VALUES IN (138),
    PARTITION BRADI_Bd18_1_362_v1_Bd18_1         VALUES IN (139),
    PARTITION BRADI_Bd1_1_349_v1_Bd1_1           VALUES IN (140),
    PARTITION BRADI_Bd21_3_378_v1_Bd21_3_r       VALUES IN (141),
    PARTITION BRADI_Bd21v2_1_283_Bd21v2          VALUES IN (142),
    PARTITION BRADI_Bd29_1_346_v1_Bd29_1         VALUES IN (143),
    PARTITION BRADI_Bd2_3_353_v1_Bd2_3           VALUES IN (144),
    PARTITION BRADI_Bd30_1_344_v1_Bd30_1         VALUES IN (145),
    PARTITION BRADI_Bd3_1_328_v1_Bd3_1_r         VALUES IN (146),
    PARTITION BRADI_BdTR10c_374_v1_BdTR10C       VALUES IN (147),
    PARTITION BRADI_BdTR11a_380_v1_BdTR11A       VALUES IN (148),
    PARTITION BRADI_BdTR11g_357_v1_BdTR11G       VALUES IN (149),
    PARTITION BRADI_BdTR11i_363_v1_BdTR11I       VALUES IN (150),
    PARTITION BRADI_BdTR12c_352_v1_BdTR12c       VALUES IN (151),
    PARTITION BRADI_BdTR13a_334_v1_BdTR13a       VALUES IN (152),
    PARTITION BRADI_BdTR13c_365_v1_BdTR13C       VALUES IN (153),
    PARTITION BRADI_BdTR1i_345_v1_BdTR1i         VALUES IN (154),
    PARTITION BRADI_BdTR2b_376_v1_BdTR2B         VALUES IN (155),
    PARTITION BRADI_BdTR2g_367_v1_BdTR2G         VALUES IN (156),
    PARTITION BRADI_BdTR3c_354_v1_BdTR3C         VALUES IN (157),
    PARTITION BRADI_BdTR5i_370_v1_BdTR5I         VALUES IN (158),
    PARTITION BRADI_BdTR7a_329_v1_BdTR7a         VALUES IN (159),
    PARTITION BRADI_BdTR8i_348_v1_BdTR8i         VALUES IN (160),
    PARTITION BRADI_BdTR9k_358_v1_BdTR9K         VALUES IN (161),
    PARTITION BRADI_Bis_1_338_v1_Bis_1           VALUES IN (162),
    PARTITION BRADI_Foz1_366_v1_Foz1             VALUES IN (163),
    PARTITION BRADI_Gaz_8_339_v1_Gaz_8           VALUES IN (164),
    PARTITION BRADI_Jer1_375_v1_Jer1             VALUES IN (165),
    PARTITION BRADI_Kah_1_351_v1_Kah_1           VALUES IN (166),
    PARTITION BRADI_Kah_5_342_v1_Kah_5           VALUES IN (167),
    PARTITION BRADI_Koz_1_330_v1_Koz_1           VALUES IN (168),
    PARTITION BRADI_Koz_3_368_v1_Koz_3           VALUES IN (169),
    PARTITION BRADI_Luc1_347_v1_Luc1             VALUES IN (170),
    PARTITION BRADI_Mig3_377_v1_Mig3             VALUES IN (171),
    PARTITION BRADI_Mon3_350_v1_Mon3             VALUES IN (172),
    PARTITION BRADI_Mur1_373_v1_Mur1             VALUES IN (173),
    PARTITION BRADI_Per1_326_v1_Per1             VALUES IN (174),
    PARTITION BRADI_Ron2_360_v1_RON2             VALUES IN (175),
    PARTITION BRADI_S8iiC_340_v1_S8iiC           VALUES IN (176),
    PARTITION BRADI_Sig2_371_v1_Sig2             VALUES IN (177),
    PARTITION BRADI_Tek_2_341_v1_Tek_2           VALUES IN (178),
    PARTITION BRADI_Tek_4_332_v1_Tek_4           VALUES IN (179),
    PARTITION BRADI_Uni2_327_v1_Uni2             VALUES IN (180),
    PARTITION BRADI                              VALUES IN (181)
  );

-- --------------------------------------------------------

--
-- Table structure for table 'sequence_count_by_families_species_cache'
--

DROP TABLE IF EXISTS sequence_count_by_families_species_cache;
CREATE TABLE IF NOT EXISTS sequence_count_by_families_species_cache (
  family_id mediumint(8) unsigned NOT NULL,
  species_id smallint(5) unsigned NOT NULL COMMENT 'species id (same as in table species)',
  sequence_count mediumint(10) unsigned NOT NULL COMMENT 'Number of sequence by family by species',
  sequence_count_without_splice mediumint(8) unsigned NOT NULL,
  PRIMARY KEY (family_id,species_id),
  KEY species (species_id)
);

-- --------------------------------------------------------

--
-- Table structure for table 'sequence_count_by_families_genomes_cache'
--

DROP TABLE IF EXISTS sequence_count_by_families_genomes_cache;
CREATE TABLE IF NOT EXISTS sequence_count_by_families_genomes_cache (
  family_id mediumint(8) unsigned NOT NULL,
  genome_id smallint(5) unsigned NOT NULL COMMENT 'genome id (same as in table genomes)',
  sequence_count mediumint(10) unsigned NOT NULL COMMENT 'Number of sequence by family by genome',
  sequence_count_without_splice mediumint(8) unsigned NOT NULL,
  PRIMARY KEY (family_id,genome_id),
  KEY genomes (genome_id)
);

-- --------------------------------------------------------

--
-- Table structure for table 'sequence_synonyms'
--

DROP TABLE IF EXISTS sequence_synonyms;
CREATE TABLE IF NOT EXISTS sequence_synonyms (
  sequence_id mediumint(8) unsigned NOT NULL,
  `synonym` varchar(255) NOT NULL COMMENT 'Synonym of the sequence accession in the sequences table',
  `rank` tinyint(3) unsigned NOT NULL DEFAULT '1' COMMENT 'Synonym rank (low value, high priority)',
  PRIMARY KEY (sequence_id,`synonym`)
);

-- --------------------------------------------------------

--
-- Table structure for table 'sequence_transfer_history'
--

DROP TABLE IF EXISTS sequence_transfer_history;
CREATE TABLE IF NOT EXISTS sequence_transfer_history (
  sequence_id mediumint(8) unsigned NOT NULL,
  old_accession varchar(127) NOT NULL,
  new_accession varchar(127) NOT NULL,
  transaction_date timestamp NULL DEFAULT NULL,
  KEY seq_id (sequence_id)
);

-- --------------------------------------------------------

--
-- Table structure for table 'species'
--

DROP TABLE IF EXISTS species;
CREATE TABLE IF NOT EXISTS species (
  id smallint(5) unsigned NOT NULL AUTO_INCREMENT,
  `code` varchar(7) NOT NULL COMMENT 'Species code in capital letters',
  organism varchar(63) CHARACTER SET latin1 DEFAULT NULL COMMENT 'species full name',
  common_name varchar(63) CHARACTER SET latin1 NOT NULL DEFAULT '',
  taxonomy_id int(10) unsigned DEFAULT NULL COMMENT 'NCBI taxonomy identifier',
  url_picture varchar(255) CHARACTER SET latin1 NOT NULL,
  display_order int(10) NOT NULL DEFAULT '0' COMMENT 'Position number of the species on the homepage tree',
  color char(6) NOT NULL DEFAULT '808080' COMMENT '24bit RGB hexadecimal color code for the species',
  PRIMARY KEY (id),
  UNIQUE KEY code (code),
  KEY taxonomy_id (taxonomy_id)
);

-- --------------------------------------------------------

--
-- Table structure for table 'taxonomy'
--

DROP TABLE IF EXISTS taxonomy;
CREATE TABLE IF NOT EXISTS taxonomy (
  id int(10) unsigned NOT NULL DEFAULT '0',
  scientific_name varchar(63) CHARACTER SET latin1 NOT NULL DEFAULT '',
  `rank` varchar(63) CHARACTER SET latin1 NOT NULL DEFAULT '',
  division varchar(63) CHARACTER SET latin1 NOT NULL DEFAULT '',
  parent_taxonomy_id int(10) unsigned DEFAULT NULL,
  `level` smallint(3) unsigned NOT NULL,
  PRIMARY KEY (id),
  UNIQUE KEY unique_name (scientific_name,`rank`,division),
  KEY parent_node (parent_taxonomy_id)
);

-- --------------------------------------------------------

--
-- Table structure for table 'taxonomy_synonyms'
--

DROP TABLE IF EXISTS taxonomy_synonyms;
CREATE TABLE IF NOT EXISTS taxonomy_synonyms (
  taxonomy_id int(10) unsigned NOT NULL,
  `name` varchar(255) NOT NULL,
  synonym_order tinyint(3) unsigned NOT NULL COMMENT 'Lower values for higher priorities',
  KEY tax_id (taxonomy_id)
);

-- --------------------------------------------------------

--
-- Table structure for table 'genomes'
--

DROP TABLE IF EXISTS genomes;
CREATE TABLE IF NOT EXISTS genomes (
  id smallint(5) unsigned NOT NULL AUTO_INCREMENT,
  accession varchar(127) NOT NULL COMMENT 'Genome name',
  species_id smallint(4) unsigned NOT NULL,
  representative tinyint(1) NOT NULL DEFAULT '0' COMMENT 'species representative genome',
  sequence_count mediumint(10) NOT NULL,
  chromosome_count tinyint(3) unsigned DEFAULT NULL COMMENT 'Number of chromosome of the genome as the haploid number (n)',
  ploidy tinyint(3) unsigned DEFAULT NULL COMMENT 'Ploidy of the genome (diploid=2, triploid=3, etc.)',
  genome_size float DEFAULT NULL COMMENT 'in Mbp',
  complete_busco float DEFAULT NULL COMMENT 'percent',
  fragment_busco float DEFAULT NULL COMMENT 'percent',
  missing_busco float DEFAULT NULL COMMENT 'missing',
  url_institute varchar(127) CHARACTER SET latin1 NOT NULL,
  url_fasta varchar(1023) CHARACTER SET latin1 NOT NULL,
  version varchar(31) NOT NULL DEFAULT '',
  version_notes text NOT NULL COMMENT 'complementary notes about the version',
  PRIMARY KEY (id),
  UNIQUE KEY accession (accession)
);

-- --------------------------------------------------------

--
-- Table structure for table 'users'
--

DROP TABLE IF EXISTS `users`;
CREATE TABLE IF NOT EXISTS `users` (
  id smallint(5) unsigned NOT NULL AUTO_INCREMENT,
  login varchar(255) NOT NULL,
  display_name varchar(255) NOT NULL,
  email varchar(255) DEFAULT NULL,
  password_hash char(40) NOT NULL,
  salt char(40) NOT NULL,
  flags set('administrator','annotator','registered user','disabled') NOT NULL DEFAULT '',
  description text NOT NULL,
  PRIMARY KEY (id),
  UNIQUE KEY login (login),
  UNIQUE KEY email (email)
);

-- --------------------------------------------------------

--
-- Table structure for table 'user_openids'
--

DROP TABLE IF EXISTS user_openids;
CREATE TABLE user_openids (
  user_id smallint(5) unsigned NOT NULL,
  openid_url varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  openid_identity varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  priority tinyint(3) unsigned NOT NULL DEFAULT '0',
  INDEX (user_id)
);

-- --------------------------------------------------------

--
-- Table structure for table 'openids'
--

DROP TABLE IF EXISTS openids;
CREATE TABLE openids (
  `name` VARCHAR(255) NOT NULL,
  label VARCHAR(255) DEFAULT NULL COMMENT 'NULL means the user has nothing to enter and there is no field to substitute in the URL',
  url VARCHAR(1024) DEFAULT NULL COMMENT 'Should use {username} in the url as placeholder for user identifier',
  enabled BOOL NOT NULL DEFAULT '1',
  icon_size ENUM('small', 'large') NOT NULL DEFAULT 'small',
  icon_index SMALLINT(3) UNSIGNED NOT NULL COMMENT 'index in the icon image'
);

-- --------------------------------------------------------

--
-- Table structure for table files
--

CREATE TABLE IF NOT EXISTS files (
  id mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(128) NOT NULL,
  path varchar(256) NOT NULL,
  url varchar(256) DEFAULT NULL,
  `type` varchar(64) DEFAULT NULL,
  flags set('missing','private') NOT NULL,
  size int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'file size in bytes',
  user_id smallint(5) unsigned DEFAULT NULL,
  PRIMARY KEY (id),
  KEY user_id (user_id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Table used to manage user-uploaded files' AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table 'variables'
--

DROP TABLE IF EXISTS `variables`;
CREATE TABLE IF NOT EXISTS `variables` (
  `name` varchar(127) CHARACTER SET latin1 NOT NULL,
  `value` longtext CHARACTER SET latin1 NOT NULL,
  PRIMARY KEY (`name`)
);

--
-- Constraints for dumped tables
--

--
-- Constraints for table bbmh
--
ALTER TABLE bbmh
  ADD CONSTRAINT bbmh_ibfk_2 FOREIGN KEY (hit_sequence_id) REFERENCES sequences (id) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT bbmh_ibfk_1 FOREIGN KEY (query_sequence_id) REFERENCES sequences (id) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table custom_families
--
ALTER TABLE custom_families
  ADD CONSTRAINT custom_families_ibfk_1 FOREIGN KEY (user_id) REFERENCES `users` (id) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table custom_families_families_relationships
--
ALTER TABLE custom_families_families_relationships
  ADD CONSTRAINT custom_families_families_relationships_ibfk_2 FOREIGN KEY (family_id) REFERENCES families (id) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT custom_families_families_relationships_ibfk_1 FOREIGN KEY (custom_family_id) REFERENCES custom_families (id) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table custom_families_sequences
--
ALTER TABLE custom_families_sequences
  ADD CONSTRAINT custom_families_sequences_ibfk_2 FOREIGN KEY (custom_sequence_id) REFERENCES custom_sequences (id) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT custom_families_sequences_ibfk_1 FOREIGN KEY (custom_family_id) REFERENCES custom_families (id) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table custom_family_relationships
--
ALTER TABLE custom_family_relationships
  ADD CONSTRAINT custom_family_relationships_ibfk_1 FOREIGN KEY (subject_custom_family_id) REFERENCES custom_families (id) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT custom_family_relationships_ibfk_2 FOREIGN KEY (object_custom_family_id) REFERENCES custom_families (id) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table custom_sequences
--
ALTER TABLE custom_sequences
  ADD CONSTRAINT custom_sequences_ibfk_1 FOREIGN KEY (species_id) REFERENCES species (id) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table custom_sequences_sequences_relationships
--
ALTER TABLE custom_sequences_sequences_relationships
  ADD CONSTRAINT custom_sequences_sequences_relationships_ibfk_3 FOREIGN KEY (custom_sequence_id) REFERENCES custom_sequences (id) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT custom_sequences_sequences_relationships_ibfk_2 FOREIGN KEY (sequence_id) REFERENCES sequences (id) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table dbxref
--
ALTER TABLE dbxref
  ADD CONSTRAINT dbxref_ibfk_1 FOREIGN KEY (`db_id`) REFERENCES db (id) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table custom_families_dbxref
--
ALTER TABLE custom_families_dbxref
  ADD FOREIGN KEY (custom_family_id) REFERENCES custom_families (id) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD FOREIGN KEY (dbxref_id) REFERENCES dbxref (id) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table dbxref_families
--
ALTER TABLE dbxref_families
  ADD CONSTRAINT dbxref_families_ibfk_4 FOREIGN KEY (family_id) REFERENCES families (id) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT dbxref_families_ibfk_5 FOREIGN KEY (dbxref_id) REFERENCES dbxref (id) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table dbxref_sequences
--
ALTER TABLE dbxref_sequences
  ADD CONSTRAINT dbxref_sequences_ibfk_5 FOREIGN KEY (dbxref_id) REFERENCES dbxref (id) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT dbxref_sequences_ibfk_6 FOREIGN KEY (sequence_id) REFERENCES sequences (id) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table dbxref_synonyms
--
ALTER TABLE dbxref_synonyms
  ADD CONSTRAINT dbxref_synonyms_ibfk_1 FOREIGN KEY (dbxref_id) REFERENCES dbxref (id) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table ec_go
--
ALTER TABLE ec_go
  ADD CONSTRAINT ec_go_ibfk_1 FOREIGN KEY (dbxref_id) REFERENCES dbxref (id) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT ec_go_ibfk_2 FOREIGN KEY (go_id) REFERENCES go (id) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT ec_go_ibfk_3 FOREIGN KEY (dbxref_id) REFERENCES dbxref (id) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table families
--
ALTER TABLE families
  ADD CONSTRAINT families_ibfk_1 FOREIGN KEY (taxonomy_id) REFERENCES taxonomy (id) ON DELETE SET NULL ON UPDATE CASCADE;

--
-- Constraints for table families_go_cache
--
ALTER TABLE families_go_cache
  ADD CONSTRAINT families_go_cache_ibfk_1 FOREIGN KEY (family_id) REFERENCES families (id) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT families_go_cache_ibfk_2 FOREIGN KEY (go_id) REFERENCES go (id) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table families_ipr_cache
--
ALTER TABLE families_ipr_cache
  ADD CONSTRAINT families_ipr_cache_ibfk_3 FOREIGN KEY (family_id) REFERENCES families (id) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT families_ipr_cache_ibfk_4 FOREIGN KEY (ipr_id) REFERENCES ipr (id) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table families_ipr_species_cache
--
ALTER TABLE families_ipr_species_cache
  ADD CONSTRAINT families_ipr_species_cache_ibfk_3 FOREIGN KEY (family_id) REFERENCES families (id) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT families_ipr_species_cache_ibfk_4 FOREIGN KEY (ipr_id) REFERENCES ipr (id) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT families_ipr_species_cache_ibfk_5 FOREIGN KEY (species_id) REFERENCES species (id) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table families_sequences
--
ALTER TABLE families_sequences
  ADD CONSTRAINT families_sequences_ibfk_3 FOREIGN KEY (family_id) REFERENCES families (id) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT families_sequences_ibfk_4 FOREIGN KEY (sequence_id) REFERENCES sequences (id) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table family_annotations
--
ALTER TABLE family_annotations
  ADD CONSTRAINT family_annotations_ibfk_1 FOREIGN KEY (user_id) REFERENCES `users` (id) ON DELETE SET NULL ON UPDATE CASCADE;

--
-- Constraints for table family_processing
--
ALTER TABLE family_processing
  ADD CONSTRAINT family_processing_ibfk_1 FOREIGN KEY (family_id) REFERENCES families (id) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT family_processing_ibfk_2 FOREIGN KEY (process_id) REFERENCES processes (id) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table family_relationships_cache
--
ALTER TABLE family_relationships_cache
  ADD CONSTRAINT family_relationships_cache_ibfk_1 FOREIGN KEY (subject_family_id) REFERENCES families (id) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT family_relationships_cache_ibfk_2 FOREIGN KEY (object_family_id) REFERENCES families (id) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table family_synonyms
--
ALTER TABLE family_synonyms
  ADD CONSTRAINT family_synonyms_ibfk_1 FOREIGN KEY (family_id) REFERENCES families (id) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table family_transfer_history
--
ALTER TABLE family_transfer_history
  ADD CONSTRAINT family_transfer_history_ibfk_1 FOREIGN KEY (new_family_id) REFERENCES families (id) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table go_alternates
--
ALTER TABLE go_alternates
  ADD CONSTRAINT go_alternates_ibfk_1 FOREIGN KEY (go_id) REFERENCES go (id) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table go_ipr
--
ALTER TABLE go_ipr
  ADD CONSTRAINT go_ipr_ibfk_3 FOREIGN KEY (ipr_id) REFERENCES ipr (id) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT go_ipr_ibfk_4 FOREIGN KEY (go_id) REFERENCES go (id) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT go_ipr_ibfk_5 FOREIGN KEY (evidence_code) REFERENCES evidence_codes (`code`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table go_lineage
--
ALTER TABLE go_lineage
  ADD CONSTRAINT go_lineage_ibfk_1 FOREIGN KEY (go_id) REFERENCES go (id) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table go_relationships
--
ALTER TABLE go_relationships
  ADD CONSTRAINT go_relationships_ibfk_1 FOREIGN KEY (subject_go_id) REFERENCES go (id) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT go_relationships_ibfk_2 FOREIGN KEY (object_go_id) REFERENCES go (id) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table go_sequences_cache
--
ALTER TABLE go_sequences_cache
  ADD CONSTRAINT go_sequences_cache_ibfk_1 FOREIGN KEY (ipr_id) REFERENCES ipr (id) ON DELETE SET NULL ON UPDATE CASCADE,
  ADD CONSTRAINT go_sequences_cache_ibfk_2 FOREIGN KEY (uniprot_id) REFERENCES dbxref (id) ON DELETE SET NULL ON UPDATE CASCADE,
  ADD CONSTRAINT go_sequences_cache_ibfk_3 FOREIGN KEY (ec_id) REFERENCES dbxref (id) ON DELETE SET NULL ON UPDATE CASCADE,
  ADD CONSTRAINT go_sequences_cache_ibfk_4 FOREIGN KEY (go_id) REFERENCES go (id) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT go_sequences_cache_ibfk_5 FOREIGN KEY (sequence_id) REFERENCES sequences (id) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table go_synonyms
--
ALTER TABLE go_synonyms
  ADD CONSTRAINT go_synonyms_ibfk_1 FOREIGN KEY (go_id) REFERENCES go (id) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table go_uniprot
--
ALTER TABLE go_uniprot
  ADD CONSTRAINT go_uniprot_ibfk_6 FOREIGN KEY (evidence_code) REFERENCES evidence_codes (`code`) ON DELETE SET NULL ON UPDATE CASCADE,
  ADD CONSTRAINT go_uniprot_ibfk_4 FOREIGN KEY (go_id) REFERENCES go (id) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT go_uniprot_ibfk_5 FOREIGN KEY (dbxref_id) REFERENCES dbxref (id) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table groups_users
--
ALTER TABLE groups_users
  ADD CONSTRAINT groups_users_ibfk_2 FOREIGN KEY (user_id) REFERENCES `users` (id) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT groups_users_ibfk_1 FOREIGN KEY (group_id) REFERENCES `groups` (id) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table homologies
--
ALTER TABLE homologies
  ADD CONSTRAINT homologies_ibfk_1 FOREIGN KEY (family_id) REFERENCES families (id) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table homologs
--
ALTER TABLE homologs
  ADD CONSTRAINT homologs_ibfk_1 FOREIGN KEY (subject_sequence_id) REFERENCES sequences (id) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT homologs_ibfk_2 FOREIGN KEY (object_sequence_id) REFERENCES sequences (id) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT homologs_ibfk_3 FOREIGN KEY (homology_id) REFERENCES homologies (id) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table ipr_sequences
--
ALTER TABLE ipr_sequences
  ADD CONSTRAINT ipr_sequences_ibfk_1 FOREIGN KEY (sequence_id) REFERENCES sequences (id) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT ipr_sequences_ibfk_2 FOREIGN KEY (ipr_id) REFERENCES ipr (id) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table lineage
--
ALTER TABLE lineage
  ADD CONSTRAINT lineage_ibfk_4 FOREIGN KEY (lineage_taxonomy_id) REFERENCES taxonomy (id) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT lineage_ibfk_3 FOREIGN KEY (taxonomy_id) REFERENCES taxonomy (id) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table my_lists
--
ALTER TABLE my_lists
  ADD CONSTRAINT my_lists_ibfk_1 FOREIGN KEY (user_id) REFERENCES `users` (id) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table my_list_items
--
ALTER TABLE my_list_items
  ADD CONSTRAINT my_list_items_ibfk_1 FOREIGN KEY (my_list_id) REFERENCES my_lists (id) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table sequences
--
ALTER TABLE sequences
  ADD CONSTRAINT sequences_ibfk_1 FOREIGN KEY (species_id) REFERENCES species (id) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT sequences_ibfk_2 FOREIGN KEY (genome_id) REFERENCES genomes (id) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table sequence_count_by_families_species_cache
--
ALTER TABLE sequence_count_by_families_species_cache
  ADD CONSTRAINT sequence_count_by_families_species_cache_ibfk_3 FOREIGN KEY (family_id) REFERENCES families (id) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT sequence_count_by_families_species_cache_ibfk_4 FOREIGN KEY (species_id) REFERENCES species (id) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table sequence_count_by_families_genomes_cache
--
ALTER TABLE sequence_count_by_families_genomes_cache
  ADD CONSTRAINT sequence_count_by_families_genomes_cache_ibfk_3 FOREIGN KEY (family_id) REFERENCES families (id) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT sequence_count_by_families_genomes_cache_ibfk_4 FOREIGN KEY (genome_id) REFERENCES genomes (id) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table sequence_synonyms
--
ALTER TABLE sequence_synonyms
  ADD CONSTRAINT sequence_synonyms_ibfk_1 FOREIGN KEY (sequence_id) REFERENCES sequences (id) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table species
--
ALTER TABLE species
  ADD CONSTRAINT species_ibfk_1 FOREIGN KEY (taxonomy_id) REFERENCES taxonomy (id) ON DELETE SET NULL ON UPDATE CASCADE;

--
-- Constraints for table taxonomy
--
ALTER TABLE taxonomy
  ADD CONSTRAINT taxonomy_ibfk_1 FOREIGN KEY (parent_taxonomy_id) REFERENCES taxonomy (id) ON DELETE SET NULL ON UPDATE CASCADE;

--
-- Constraints for table taxonomy_synonyms
--
ALTER TABLE taxonomy_synonyms
  ADD CONSTRAINT taxonomy_synonyms_ibfk_1 FOREIGN KEY (taxonomy_id) REFERENCES taxonomy (id) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table genomes
--
ALTER TABLE genomes
  ADD CONSTRAINT genomes_ibfk_1 FOREIGN KEY (species_id) REFERENCES species (id) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table user_openids
--
ALTER TABLE user_openids
  ADD FOREIGN KEY (user_id) REFERENCES `users` (id) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD UNIQUE (openid_url, openid_identity),
  ADD UNIQUE (user_id, priority);

--
-- Constraints for table openids
--
ALTER TABLE openids
  ADD UNIQUE (name);