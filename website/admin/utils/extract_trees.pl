#!/usr/bin/perl

use strict;
use warnings;

use Carp qw (cluck confess croak);

my ($fhi, $fho);
open($fhi, 'final_trees_colored.nwk') or confess "ERROR: Failed to open final_trees_colored.nwk!\n";

while (my $line = <$fhi>)
{
    chomp($line);
    my ($family, $tree) = ($line =~ m/(GP\w+)\s+(.*)/);
    if (!$family)
    {
        confess "unparsed line:\n$line\n";
    }
    print "-$family\n";
    # my $filename = $family . "_phylogeny_tree.nwk";
    my $filename = $family . "_rap_gene_tree.nwk";
    my $path = 'level1/' . substr($family, 0, 5) . '/' . $family;
    if (!-d 'level1')
    {
        mkdir 'level1';
    }
    if (!-d 'level1/' . substr($family, 0, 5))
    {
        mkdir 'level1/' . substr($family, 0, 5);
    }
    if (!-d 'level1/' . substr($family, 0, 5) . '/' . $family)
    {
        mkdir 'level1/' . substr($family, 0, 5) . '/' . $family;
    }
    open($fho, '>', "$path/$filename") or confess "ERROR: Failed to create output file '$filename'\n";
    $tree =~ s/\[&&NHX:[^\]]*\]//g;
    print {$fho} $tree . "\n";
    close($fho);
}
close($fhi);