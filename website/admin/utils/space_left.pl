#!/usr/bin/env perl

=pod

=head1 NAME

space_left.pl - preserve free disk space

=head1 SYNOPSIS

    space_left.pl
    space_left.pl -reserve 10
    space_left.pl -keep-free 1

=head1 REQUIRES

Perl5

=head1 DESCRIPTION

Makes sure we keep having free space for computation.

=cut

use strict;
use warnings;

use FindBin;
use lib "$FindBin::Bin/../../lib";
use lib "$FindBin::Bin/../../local_lib";
use Carp qw (cluck confess croak);

use Greenphyl;

use Getopt::Long;
use Pod::Usage;

# use bignum;

++$|; #no buffering




# Script global constants
##########################

=pod

=head1 CONSTANTS

B<$REV_STRING>: (string)

Current revision (auto-set by GIT).

B<$DEBUG>: (boolean)

When set to true, it enables debug mode.

B<$USER_MENU>: (string)

User menu main page.

=cut

# SVN management part
my $REV_STRING = '$id$';

our $DEBUG = 0;
our $DEFAULT_RESERVATION_PATH = '/work/guignon/temp';
our $FILENAME_PREFIX = 'resa';
our $DEFAULT_SLEEP_DELAY = 60; # seconds




# Script global functions
##########################

=pod

=head1 FUNCTIONS

=head2 GetFreeSpace

B<Description>: Returns the amount of free space in bytes.

B<ArgsCount>: 1

=over 4

=item $target_path: (string) (R)

Path to check.

=back

B<Return>: (integer)

Free space in bytes.

=cut

sub GetFreeSpace
{
    my ($target_path) = @_;

    if (!$target_path)
    {
        confess "ERROR: No path to check!\n";
    }

    my $free_space_output = `df -l $target_path`;
    my ($free_space) = ($free_space_output =~ m/\S+\s+\d+\s+\d+\s+(\d+)\s/ms);
    if (!defined($free_space))
    {
        confess "ERROR: Failed to parse free space!\n";
    }
    return int($free_space*1024);
}


=pod

=head2 ReserveSpace

B<Description>: 

B<ArgsCount>: 3

=over 4

=item $target_path: (string) (R)

Path where space should be reserved.

=back

B<Return>: nothing

=cut

sub ReserveSpace
{
    my ($target_path, $amount, $blocks) = @_;

    if (!$target_path)
    {
        confess "ERROR: No path to use!\n";
    }
    
    my $reserved_space = 0;
    
    my $filename_index = 0;
    my $fh;
    my $buffer = '0' x 8388608;
    my $buffer_size = length($buffer);
    my $new_filename = sprintf("$target_path/$FILENAME_PREFIX%06x", $filename_index++);

    while ($reserved_space < $amount)
    {
        while (-e $new_filename)
        {
            $new_filename = sprintf("$target_path/$FILENAME_PREFIX%06x", $filename_index++);
        }
        
        
        if (open($fh, ">$new_filename"))
        {
            for (0..int(0.5 + (1.0*$blocks)/$buffer_size))
            {
                print {$fh} $buffer;
                $reserved_space += $buffer_size;
            }
            
            close($fh);
        }
    }

}


=pod

=head2 GetNextReserveFileName

B<Description>: 

B<ArgsCount>: 3

=over 4

=item $target_path: (string) (R)

Path where space should be reserved.

=back

B<Return>: nothing

=cut

sub GetNextReserveFileName
{
    my ($target_path) = @_;

    if (!$target_path)
    {
        confess "ERROR: No path to use!\n";
    }
    my @file_list = glob "$target_path/$FILENAME_PREFIX*";
    
    my $filename;
    while (($filename = pop(@file_list))
        && ($filename !~ m/$FILENAME_PREFIX[a-f0-9]{6}$/))
    {}

    return $filename;
}




# Script options
#################

=pod

=head1 OPTIONS

    space_left.pl [-help | -man]

    space_left.pl [-debug] -reserve <SIZE>

    space_left.pl [-debug] -keep-free <SIZE>

=head2 Parameters

=over 4

=item B<-help>:

Prints a brief help message and exits.

=item B<-man>:

Prints the manual page and exits.

=item B<-debug> (integer):

Executes the script in debug mode. The integer value is optional and is used
to set debug level (use 0 to force disable debug mode).
Default: 0 (not in debug mode).

=item B<-reserve> (float):

Amount of gigabytes to hard-reserve for future use.

=item B<-keep-free> (float):

Amount of gigabytes to keep as free space.

=back

=cut


# CODE START
#############

# options processing
my ($man, $help, $debug) = (0, 0, undef);
my $menu_mode;
my ($reserve_size, $keep_free_size, $target_path);

# parse options and print usage if there is a syntax error.
GetOptions('help|?'        => \$help,
           'man'           => \$man,
           'debug:s'       => \$debug,
           'r|reserve=f'   => \$reserve_size,
           'k|keep-free=f' => \$keep_free_size,
           'p|path=s'      => \$target_path,
) or pod2usage(1);
if ($help) {pod2usage('-verbose' => 1, '-exitval' => 0);}
if ($man) {pod2usage('-verbose' => 2, '-exitval' => 0);}

# change debug mode if requested/forced
if (defined($debug))
{
    # just '-debug' ?
    if ($debug eq '')
    {
        $debug = 1;
    }
    $DEBUG = $debug;
}

$target_path ||= $DEFAULT_RESERVATION_PATH;

eval
{
    printf("Current free space:   % 18i bytes\n", GetFreeSpace($target_path));

    if ($keep_free_size)
    {
        $keep_free_size *= 1024*1024*1024;
    }

    if ($reserve_size)
    {
        $reserve_size *= 1024*1024*1024;
        my $block_size = $keep_free_size || $reserve_size / 100.;
        print "Reserving $reserve_size bytes by blocks of $block_size bytes\n";
        ReserveSpace($target_path, $reserve_size, $block_size);
    }
    
    if ($keep_free_size)
    {
        printf("Requested free space: % 18i bytes\n", $keep_free_size);
        my $reserve_file;

        while ($reserve_file = GetNextReserveFileName($target_path))
        {
            while (GetFreeSpace($target_path) >= $keep_free_size)
            {
                sleep(1*$DEFAULT_SLEEP_DELAY);
            }
            printf("Space needed: %i bytes\n", $keep_free_size - GetFreeSpace($target_path));
            print "Freeing $reserve_file\n";
            unlink($reserve_file) or warn "WARNING: unable to remove '$reserve_file'";
            sleep(5*$DEFAULT_SLEEP_DELAY);
        }

        if (GetFreeSpace($target_path) < $keep_free_size)
        {
            warn "Unable to free more space!\n";
        }
    }
};

# catch
HandleErrors();

exit(0);

# CODE END
###########


=pod

=head1 AUTHORS

Mathieu ROUARD (Bioversity), m.rouard@cgiar.org

Valentin GUIGNON (Bioversity), v.guignon@cgiar.org

Matthieu CONTE (Syngenta)

Sebastien BRIOIS (Syngenta)

=head1 VERSION

Version 1.0.0

Date 14/03/2014

=head1 SEE ALSO

GreenPhyl documentation.

=cut
