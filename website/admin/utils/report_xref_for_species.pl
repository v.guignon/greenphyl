#!/usr/bin/env perl
# report_data_for_species.pl --help
#
# created on 13/05/2013 - sebastien briois

use strict;
use warnings;

use Getopt::Long;
use Pod::Usage;

use Data::Dumper;

use FindBin;
use lib "$FindBin::Bin/../../lib";
use lib "$FindBin::Bin/../../local_lib";

use Greenphyl;
use Greenphyl::Log;

$|++; # Autoflush

my $g_dbh = GetDatabaseHandler();

{
    # Script parameters
    my ( $species );
    
    # Define script options
    GetOptions(
        'help|h'            => sub { pod2usage( -verbose => 1 ) },
        'man'               => sub { pod2usage( -verbose => 2 ) },
        'species=s'         => \$species, 
    ) or pod2usage(2);
    
    pod2usage('--species must be specified')
        unless defined $species;
    
    run( $species );
}

sub run
{
    my ($species) = @_;
    
    my $sequences = $g_dbh->selectall_arrayref(
        qq{
            SELECT sequences.id, sequences.accession
            FROM 
                sequences
                JOIN species ON species_id = species.id
            WHERE species.code = ?
            ORDER BY accession
        }, { Slice => {} }, $species
    );
    
    foreach my $sequence (@$sequences) {
        my $xref = $g_dbh->selectall_arrayref(
            qq{
                SELECT distinct accession, db.name
                FROM 
                    dbxref
                    JOIN db ON db_id = db.id
                    JOIN dbxref_sequences on sequence_id = ?
            }, { Slice => {} }, $sequence->{'id'}
        );  
        foreach (@$xref) {
            print $sequence->{'accession'} . "\t";
            print $_->{'accession'} . "\t";
            print $_->{'name'} . "\n";
        }
        
        my $interpro = $g_dbh->selectall_arrayref(
            qq{SELECT distinct code FROM ipr JOIN ipr_sequences ON sequence_id = ?},
            { Slice => {} }, $sequence->{'id'}
        );
        
        foreach (@$interpro) {
            print $sequence->{'accession'} . "\t";
            print $_->{'code'} . "\tinterpro\n";
        }
    }
}
