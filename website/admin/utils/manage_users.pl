#!/usr/bin/env perl

=pod

=head1 NAME

manage_users.pl - Manages GreenPhyl users

=head1 SYNOPSIS

    manage_users.pl -add admin -password toto1234 -email "v.guignon@cgiar.org" -flags "administrator,registered user"
    manage_users.pl -remove toto

=head1 REQUIRES

Perl5

=head1 DESCRIPTION

This script can be used both interactively ("-menu" or not) and without prompts
("-q" or "-noprompt"). It allows to manage GreenPhyl user accounts.

=cut

use strict;
use warnings;

use FindBin;
use lib "$FindBin::Bin/../../lib";
use lib "$FindBin::Bin/../../local_lib";
use Carp qw (cluck confess croak);

use Greenphyl;
use Greenphyl::Log('nolog' => 1,);
use Greenphyl::User;
use Greenphyl::Tools::Users;

use Getopt::Long;
use Pod::Usage;

++$|; #no buffering




# Script global constants
##########################

=pod

=head1 CONSTANTS

B<$REV_STRING>: (string)

Current revision (auto-set by GIT).

B<$DEBUG>: (boolean)

When set to true, it enables debug mode.

B<$USER_MENU>: (string)

User menu main page.

=cut

# SVN management part
my $REV_STRING = '$id$';

our $DEBUG = 0;
our $USER_MENU = "
 GreenPhyl User Management Menu
================================
0. Exit
1. List users
2. Add a new user
3. Remove an existing user
";




# Script global functions
##########################

=pod

=head1 FUNCTIONS

=head2 DisplayUserManagementMenu

B<Description>: Display GreenPhyl user management menu.

B<ArgsCount>: 0

B<Return>: nothing

=cut

sub DisplayUserManagementMenu
{
    my $menu_item = -1;
    while ($menu_item)
    {
        print $USER_MENU;
        $menu_item = Prompt("Please select an item [0-3]", { 'default' => '0', 'constraint' => '^[0-3qx]$', });
        print "\n";
        if (1 == $menu_item)
        {
            # user list
            ListUsers();
        }
        elsif (2 == $menu_item)
        {
            # add user
            AddUser();
        }
        elsif (3 == $menu_item)
        {
            # remove user
            RemoveUser();
        }
        elsif ($menu_item =~ m/q|x/i)
        {
            $menu_item = 0;
        }
    }
    print "\nBye!\n\n";
}


=pod

=head2 ListUsers

B<Description>: Displays the list of GreenPhyl users currently in database.

B<ArgsCount>: 0

B<Return>: nothing

=cut

sub ListUsers
{
    my $users = [Greenphyl::User->new(
            GetDatabaseHandler(),
            { 'sql' => {'ORDER BY' => 'id ASC'}, },
        )
    ];

    if (!@$users)
    {
        print "No user in database!\n";
    }
    else
    {
        my $user_line_format = "% 4s |% 16s |% 16s |% 32s | %s\n";
        print '' . ('-' x 79) . "\n";
        printf($user_line_format, 'ID', 'Login', 'Name', 'e-mail', 'Flags');
        print '' . ('-' x 79) . "\n";
        foreach my $user (@$users)
        {
            printf($user_line_format, $user->login, $user->display_name, $user->email, $user->flags);
        }
        print '' . ('-' x 79) . "\n";
    }
}


=pod

=head2 AddUser

B<Description>: Adds a new GreenPhyl user to database. If some informations are
missing, the user may be prompted for them.

B<ArgsCount>: 0-1

=over 4

=item $parameters: (hash ref) (O)

Parameters hash. Supported keys are:
-'id': user ID. If provided, 'login' will be ignored;
-'login': user login;
-'no_prompt': prompting status.

=back

B<Return>: nothing

=cut

sub AddUser
{
    my ($parameters) = @_;

    # parameters check
    $parameters ||= {};
    if (ref($parameters) ne 'HASH')
    {
        confess "usage: AddUser(parameter_hash);";
    }
    my $no_prompt = $parameters->{'no_prompt'};

    # get login
    if (!$parameters->{'login'})
    {
        $parameters->{'login'} = Prompt("New login:", { 'constraint' => '^\w{3,255}$', }, $no_prompt);
        if (!$parameters->{'login'})
        {
            LogError("No new user login provided!");
            return;
        }
    }

    # get display name
    if (!$parameters->{'display_name'})
    {
        $parameters->{'display_name'} = Prompt("New display name:", { 'constraint' => '^\w{3,255}$', }, $no_prompt);
        if (!$parameters->{'display_name'})
        {
            LogError("No new user display name provided!");
            return;
        }
    }

    # get password
    if (!defined($parameters->{'password'}))
    {
        $parameters->{'password'} = Prompt("New password:", { 'default' => '', 'constraint' => '^[^\n\r\t]{4,255}$', }, $no_prompt);
        if (!$parameters->{'password'})
        {
            LogWarning("No new user password provided!");
        }
    }

    # get e-mail
    if (!defined($parameters->{'email'}))
    {
        $parameters->{'email'} = Prompt("User e-mail address:", { 'default' => '', }, $no_prompt);
    }
    # no e-mail?
    if ($parameters->{'email'} =~ /^\s*$/)
    {
        $parameters->{'email'} = undef;
    }

    # get description
    if (!defined($parameters->{'description'}))
    {
        $parameters->{'description'} = Prompt("Account description:", { 'default' => '', }, $no_prompt);
    }

    # get flags
    if (!defined($parameters->{'flags'}))
    {
        print "Select the user flags:\n";
        my @flags;
        if (Prompt("Administrator account? [Y/N]", { 'default' => 'N', 'constraint' => '^[yYnN]', }, $no_prompt) =~ m/Y/i)
        {
            push(@flags, $Greenphyl::User::USER_FLAG_ADMINISTRATOR);
        }
        if (Prompt("Annotator? [Y/N]", { 'default' => 'N', 'constraint' => '^[yYnN]', }, $no_prompt) =~ m/Y/i)
        {
            push(@flags, $Greenphyl::User::USER_FLAG_ANNOTATOR);
        }
        if (Prompt("Registered user? [Y/N]", { 'default' => 'Y', 'constraint' => '^[yYnN]', }, $no_prompt) =~ m/Y/i)
        {
            push(@flags, $Greenphyl::User::USER_FLAG_REGISTERED);
        }
        if (Prompt("Disable account? [Y/N]", { 'default' => 'N', 'constraint' => '^[yYnN]', }, $no_prompt) =~ m/Y/i)
        {
            push(@flags, $Greenphyl::User::USER_FLAG_DISABLED);
        }
        $parameters->{'flags'} = join(',', \@flags) || '';
    }

    $parameters->{'flags'} =~ s/^\s+//;
    $parameters->{'flags'} =~ s/\s+$//;
    $parameters->{'flags'} =~ s/\s*,[\s,]+/,/g;

    my $create_ok = CreateUser($parameters);
    my $user = Greenphyl::User->new(
            GetDatabaseHandler(),
            {'selectors' => {'login' => $parameters->{'login'}},}
        )
    ;

    if ($create_ok && $user)
    {
        LogInfo("User '" . $parameters->{'login'} . "' has been added.\n");
        LogVerboseInfo("User ID: " . $user->id . "\n");
    }
    else
    {
        LogWarning("Failed to create user '" . $parameters->{'login'} . "'!\n");
    }
}


=pod

=head2 RemoveUser

B<Description>: removes a given GreenPhyl user from database.

B<ArgsCount>: 0-1

=over 4

=item $parameters: (hash ref) (O)

Parameters hash. Supported keys are:
-'id': user ID. If provided, 'login' will be ignored;
-'login': user login;
-'no_prompt': prompting status.

=back

B<Return>: nothing

=cut

sub RemoveUser
{
    my ($parameters) = @_;

    # parameters check
    $parameters ||= {};
    if (ref($parameters) ne 'HASH')
    {
        confess "usage: RemoveUser(parameter_hash);";
    }
    my $no_prompt = $parameters->{'no_prompt'};

    # get login
    if (!$parameters->{'login'} && !$parameters->{'id'})
    {
        $parameters->{'login'} = Prompt("Login of user to remove", { 'constraint' => '^\w{3,255}$', }, $no_prompt);
        if (!$parameters->{'login'})
        {
            LogError("No user login provided!");
            return;
        }
    }

    my $dbh = GetDatabaseHandler();
    my $selectors = {};
    if ($parameters->{'id'})
    {
        $selectors = {'id' => $parameters->{'id'},};
    }
    else
    {
        $selectors = {'login' => $parameters->{'login'},};
    }

    my $user = Greenphyl::User->new(
            $dbh,
            {'selectors' => $selectors,}
        )
    ;

    if ($user)
    {
        my $sql_query = "DELETE FROM users WHERE id = ?;";
        LogDebug("SQL Query: $sql_query\nBindings: " . $user->id);
        $dbh->do($sql_query, undef, $user->id)
            or confess LogError("Failed to remove user " . $user->login . ":\n" . $dbh->errstr);
        LogInfo("User '" . $user->login . "' (ID="  . $user->id . ") has been removed.");
    }
    else
    {
        LogWarning("User '" . join(',', values(%$selectors)) . "' not found!");
    }
}




# Script options
#################

=pod

=head1 OPTIONS

    manage_users.pl [-help | -man]

    manage_users.pl [-debug] -menu

    manage_users.pl [-debug] [-noprompt] -add <LOGIN> [-name <DISPLAY_NAME>]
                                                      [-password <PASSWORD>]
                                                      [-email <EMAIL>]
                                                      [-desc <DESCRIPTION>]
                                                      [-flags "<FLAG>,...]"]

    manage_users.pl [-debug] [-noprompt] -remove <LOGIN>

=head2 Parameters

=over 4

=item B<-help>:

Prints a brief help message and exits.

=item B<-man>:

Prints the manual page and exits.

=item B<-debug> (integer):

Executes the script in debug mode. The integer value is optional and is used
to set debug level (use 0 to force disable debug mode).
Default: 0 (not in debug mode).

=item B<-menu> (boolean):

Use interactive interface.

=item B<-add> (string):

Name (login) of the user to add to database.

=item B<-name> (string):

User display name.

=item B<-password> (string):

User password.

=item B<-email> (string):

User e-mail address.

=item B<-desc> (string):

Administrative description (not displayed to users others than admins).

=item B<-flags> (string):

One or more coma-separated flags. Available flags are ones of
Greenphyl::User::USER_FLAG_* values. See Greenphyl::User for available values.

=item B<-remove> (string):

Name (login) of the user to remove from database.

=item B<-log>:

Enable logging.

=item B<-log-*> (any):

Logging arguments.

=back

=cut


# CODE START
#############

 # for regression tests
 if ($ENV{'TESTING'} && $ENV{'TESTING_NO_RUN'})
 {return 1;}

# options processing
my ($man, $help, $debug, $no_prompt) = (0, 0, undef, undef);
my $menu_mode;
my ($user_add, $user_display_name, $user_remove, $user_password, @user_flags, $user_email, $user_description);

# parse options and print usage if there is a syntax error.
GetOptions('help|?'     => \$help,
           'man'        => \$man,
           'debug:s'    => \$debug,
           'q|noprompt' => \$no_prompt,
           'm|menu'       => \$menu_mode,
           'a|add=s'      => \$user_add,
           'n|name=s'     => \$user_display_name,
           'r|remove=s'   => \$user_remove,
           'p|password=s' => \$user_password,
           'e|email=s'    => \$user_email,
           'd|desc|description=s'     => \$user_description,
           'f|flags=s{,}' => \@user_flags,
           @Greenphyl::Log::LOG_GETOPT, # skip log parameters
) or pod2usage(1);
if ($help) {pod2usage('-verbose' => 1, '-exitval' => 0);}
if ($man) {pod2usage('-verbose' => 2, '-exitval' => 0);}

# change debug mode if requested/forced
if (defined($debug))
{
    # just '-debug' ?
    if ($debug eq '')
    {
        $debug = 1;
    }
    $DEBUG = $debug;
}

eval
{
    if ($menu_mode)
    {
        # menu mode
        DisplayUserManagementMenu();
    }
    else
    {
        if ($user_remove)
        {
            RemoveUser({
                'login'     => $user_remove,
                'no_prompt' => $no_prompt,
            });
        }

        if ($user_add)
        {
            AddUser({
                'login'        => $user_add,
                'display_name' => ($user_display_name || $user_add),
                'password'     => $user_password,
                'email'        => $user_email,
                'description'  => $user_description,
                'flags'        => (join(',', @user_flags) || ''),
                'no_prompt'    => $no_prompt,
            });
        }
    }
};

# catch
HandleErrors();

exit(0);

# CODE END
###########


=pod

=head1 AUTHORS

Mathieu ROUARD (Bioversity), m.rouard@cgiar.org

Valentin GUIGNON (Bioversity), v.guignon@cgiar.org

Matthieu CONTE (Syngenta)

Sebastien BRIOIS (Syngenta)

=head1 VERSION

Version 2.0.0

Date 15/05/2014

=head1 SEE ALSO

GreenPhyl documentation.

=cut
