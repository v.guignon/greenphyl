#!/usr/bin/perl

=pod

=head1 NAME

process_families.pl - Run batch processing on a set of families.

=head1 SYNOPSIS

    # Generate a list of FASTA files without species code (in sequence name)
    # and without splice-forms of level-1 families that have more than 4
    # sequences and store the files in 'test_directory' directory.
    process_families.pl -generate -fasta -without-species-code -without-splice min_sequences=5 level=1 -f test_directory -log-screenonly

    # Run (using 8 threads) 'auto_cd_hit.pl' script on level-1 families that
    # have between 5 and 500 sequences using associated FASTA stored in the
    # 'test_directory' directory.
    process_families.pl -t 8 -run 'auto_cd_hit.pl -i {fasta}' -f test_directory min_sequences=5 max_sequences=500 level=1 -log-screenonly
    # if you need to launch above command using qsub (you need to double-quote "-run" command):
    qsub -b y -cwd -N A-CDHIT -sync no -V -q greenphyl.q process_families.pl -t 8 -run "'auto_cd_hit.pl -i {fasta}'" -f test_directory min_sequences=5 max_sequences=500 level=1 -log

    # Get the name, the number of species and the list of species codes of a
    # list of families (accession) stored in the file 'family_list.txt' and
    # store the result in 'family_data.csv' file.
    perl process_families.pl accession=file:family_list.txt -generate -info "{accession};{name};{species};{species.*.code}" -o family_data.csv

    # Not recommanded but when needed: generates separate information files
    # using a given list of families file 'family_list.txt' (one single command
    # on 3 lines):
    xargs -r -x -L 1 -I XXX -a family_list.txt perl process_families.pl accession=XXX -generate -info "Family: {accession}
    Sequences:
    {sequences.*.name}" -o XXX_info.txt

    # get the list of all UniProt of family 42 using its sequences:
    perl process_families.pl -generate -info '{accession}: {sequences.*.uniprot.*.accession}' family_id=42

=head1 REQUIRES

Perl5

=head1 DESCRIPTION

This script can be used either to:
-generate or update family FASTA files for a selected set of families
-get a list of family accession or family info corresponding to specific critera
-execute a command on each FASTA (or accession or other family member)

=cut

use strict;
use warnings;

use FindBin;
use lib "$FindBin::Bin/../../lib";
use lib "$FindBin::Bin/../../local_lib";

use Carp qw (cluck confess croak);

use threads;
use threads::shared;

use Greenphyl;
use Greenphyl::Log('nolog' => 1,);
use Greenphyl::Email;
use Greenphyl::AbstractFamily;
use Greenphyl::Family;
use Greenphyl::Tools::Families;

use Getopt::Long;
use Pod::Usage;

use LockFile::Simple qw(trylock  unlock);
use File::Basename;
use Time::HiRes qw( time sleep );

++$|;

# Script global constants
##########################

=pod

=head1 CONSTANTS

B<$DEBUG>: (boolean)

If set to non-zero, debug messages will be displayed.

B<$DEFAULT_THREADS_COUNT>: (integer)

default number of threads that can be run in parallel.

B<$FASTA_FILE_EXT>: (string)

Extension used of fasta files.

=cut

our $DEBUG                = 0;
my $DEFAULT_THREADS_COUNT = 32;
my $FASTA_FILE_EXT        = '.fa';




# Script global variables
##########################

=pod

=head1 VARIABLES

B<@gs_selected_families>: (array of string)

Array of remaining fasta filenames to process.

B<$g_debug>: (boolean)

Debug flag.

B<$g_computation_start_date>: (boolean)

Time when the program has been launched.

B<$g_testing>: (boolean)

Testing mode flag (no execution, just tell what will be done).

B<$gs_family_page_counter>: (integer)

Index of current set of families loaded.

B<$gs_processed_family_counter>: (integer)

Number of family processed.

B<$g_delimiter>: (char)

Delimiter character used to separate family information entries ("-info" mode).

B<$g_list_fh>: (GLOB)

File handle to use to output family info ("-info" mode).

B<$g_family_to_skip>: (hash ref)

Hash of families that can be skipped in "-info" mode with "-complete" parameter.

=cut

my @gs_selected_families :shared;
my $g_debug;
my $g_computation_start_date;
my $g_testing;
my $gs_family_page_counter :shared;
my $gs_processed_family_counter :shared;
my $g_family_selection_hash;
my $g_delimiter = "\n";
my $g_list_fh;
my $g_family_to_skip = {};




# Script global functions
##########################

=pod

=head1 FUNCTIONS

=head2 ReplaceFamilyTokens

B<Description>: Replace family tokens in a string with the corresponding family
fields values.

B<ArgsCount>: 2-3

=over 4

=item arg: $family (Greenphyl::Family) (R)

A family object.

=item arg: $string (string) (R)

A string that contain family tokens.

=item arg: $parameters (hash ref) (O)

Additional parameters. 'fasta_directory' can hold the path of the directory
where FASTA files should be stored.

=back

B<Return>: (string)

The string with token replaced by family values.

=cut

sub ReplaceFamilyTokens
{
    my ($family, $string, $parameters) = @_;
    
    $parameters ||={};

    # first, empty curly-brackets with accession
    $string =~ s/{}/$family->accession/ge;

    # then replace special tokens
    # -FASTA
    if ($string =~ m/{fasta}/i)
    {
        # get FASTA file
        my $fasta_file_path = GetFamilyFASTAPath($family, $parameters);

        # update command line
        $string =~ s/{fasta}/$fasta_file_path/g;
    }

    # replace all family members
    $string =~
        s/
            {([\w\.\*]+)}
        /
            $family->StringifyObjectMember([split('\.', $1)]) || ''
        /gex;
    return $string;
}


=pod

=head2 GetFamilyFASTAPath

B<Description>: Returns the expected path of a family FASTA file.

B<ArgsCount>: 1-2

=over 4

=item arg: $family (Greenphyl::Family) (R)

A family object.

=item arg: $parameters (hash ref) (O)

Additional parameters. 'fasta_directory' can hold the path of the directory
where FASTA files should be stored.

=back

B<Return>: nothing

=cut

sub GetFamilyFASTAPath
{
    my ($family, $parameters) = @_;

    $parameters ||= {};

    # check FASTA file
    my $fasta_file_path = $parameters->{'fasta_directory'} || GP('TEMP_OUTPUT_PATH');
    # remove trailing slash
    $fasta_file_path =~ s/\/+$//;
    # Complete path
    $fasta_file_path .= '/' . $family->getSubPath() . '/' . $family->accession . $FASTA_FILE_EXT;

    return $fasta_file_path;
}


=pod

=head2 RunCommandThread

B<Description>:
Runs a thread that launch the given command on families to process.

B<ArgsCount>: 3

=over 4

=item arg: $slot_number (integer) (R)

Number of the thread in the thread list.

=item arg: $command (string) (R)

Command line.

=item arg: $parameters (hash ref) (R)

Additional parameters.

=back

B<Return>:

nothing.

=cut

sub RunCommandThread
{
    # since this procedure is run from threads, modules should be reloaded.
    use strict;
    use warnings;

    use lib '../lib';
    use lib '../local_lib';
    use Carp qw (cluck confess croak);
    use Greenphyl;


    my ($slot_number, $command, $parameters) = @_;

    # check if other families should be loaded in case family limits is below number of threads
    {
        lock(@gs_selected_families);
        if (!@gs_selected_families)
        {
            # array of families is empty, try to refill it...
            # get a database handler for current thread
            GetDatabaseHandler();
            my $families = GetNextSetOfFamilies();
            ShareFamilyList($families);
            # free database handler to avoid conflicts with other threads
            DisconnectDatabase();
        }
    }

FAMILY_LOOP:
    while (@gs_selected_families)
    {
        # pick a family
        my $family;
        {
            lock(@gs_selected_families);
            $family = shift(@gs_selected_families);
            lock($gs_processed_family_counter);
            ++$gs_processed_family_counter;
        }
        # make sure we got a valid family
        if ($family)
        {
            LogDebug("Using slot $slot_number to process '" . $family->accession . "'\n");
            RunCommand($slot_number, $family, $command, $parameters);
            LogDebug("Done processing '" . $family->accession . "'\n");
        }

        # check time limitations
        if (defined($parameters->{'timeout'})
            && $parameters->{'timeout'}
            && (((time() - $g_computation_start_date)/60) >= $parameters->{'timeout'}))
        {
            PrintDebug("Slot $slot_number: Timeout reached!");
            last FAMILY_LOOP;
        }

        # check if other families should be loaded
        {
            lock(@gs_selected_families);
            if (!@gs_selected_families)
            {
                # array of families is empty, try to refill it...
                # get a database handler for current thread
                GetDatabaseHandler();
                my $families = GetNextSetOfFamilies();
                ShareFamilyList($families);
                # free database handler to avoid conflicts with other threads
                DisconnectDatabase();
            }
        }
    }

}



=pod

=head2 RunCommand

B<Description>:
execute a command on the given family. This procedure is used by threads.

B<ArgsCount>: 3-4

=over 4

=item $slot_number: (integer) (R)

Execution slot number to clear when done.

=item $family: (string) (R)

The Greenphyl::Family object to process.

=item arg: $command (string) (R)

The command line to execute that includes space-holders using the syntax
'{member_name}' for family members or '{fasta}' for the family FASTA file name.

=item arg: $parameters (hash ref) (O)

Additional parameters for family processing.

Available keys are:
'fasta_directory' => (string) path of the directory to use for FASTA files.
    Default: $Greenphyl::Config::TEMP_OUTPUT_PATH;
'timeout'            => (integer) timeout (minutes) after which a thread will
 no longer continue.

=back

B<Return>:

nothing.

B<Example>:

    RunCommand(4, 'GP000042.fa', 'auto_cd_hit.pl -i {}');

=cut

sub RunCommand
{
    my ($slot_number, $family, $command, $parameters) = @_;

    if (!defined($slot_number))
    {
        LogWarning("RunCommand: thread number not specified!");
        return;
    }

    eval
    {
        # check arguments
        if (!$family)
        {
            LogWarning("RunCommand: Thread execution aborted: got an invalid family!");
            return;
        }
        
        if (not defined $slot_number)
        {
            LogWarning("RunCommand: Slot number not specified! Execution aborted for family '$family'!");
            return;
        }

        if (!$command)
        {
            LogWarning("RunCommand: Missing 'command' argument for family '$family'!");
            return;
        }

        $parameters ||= {'space-holder' => 'family',};
        if ('HASH' ne ref($parameters))
        {
            LogWarning("RunCommand: Invalid 'parameters' argument (not a hash ref) for family '$family'!");
            return;
        }

        # prepare command line
        my $command_line = $command;
        my ($lock_manager, $lock_name);
        
        # check for FASTA processing
        if ($command_line =~ m/{fasta}/i)
        {
            # check FASTA file
            my $fasta_file_path = GetFamilyFASTAPath($family, $parameters);

            # check file
            if (!-e $fasta_file_path)
            {
                LogWarning("#$slot_number: RunCommand: Fasta file '$fasta_file_path' does not exists! Execution aborted!");
                return;
            }
            elsif (!-r $fasta_file_path)
            {
                LogWarning("#$slot_number: RunCommand: Fasta file '$fasta_file_path' is not readable! Execution aborted!");
                return;
            }
            
            # check/set lock FASTA file
            # init lock manager:
            # -stale => 1: detect stale locks and break them if necessary
            # -hold  => 0: max life time (sec) of a lock. 0 prevents any forced unlocking.
            # $lock_manager = LockFile::Simple->make('-stale' => 1, '-hold' => 0);
            # Important: stale-lock detection is based on process ID and can't
            # be used in a cluster environment as process can be on different
            # nodes and can't know if the other is still running or not!
            # CONSEQUENCE: stale locks must be removed manually.
            # $ find /some/path/ -type f -name '*.lock' -exec rm -f {} \;
            $lock_manager = LockFile::Simple->make('-stale' => 0, '-hold' => 0);
            $lock_name = $fasta_file_path;
            # try to lock and skip if already locked
            if (!$lock_manager->trylock($lock_name))
            {
                LogInfo("#$slot_number: skipped! (locked by another process)");
                return;
            }
            LogInfo("#$slot_number: Lock FASTA");

        }

        $command_line = ReplaceFamilyTokens($family, $command_line, $parameters);

        LogInfo("#$slot_number: -Greenphyl Family $family->{'accession'}");

        # start timer to compute execution duration
        my $start_time = time();

        LogInfo("#$slot_number: Processing...");
        eval
        {
            my $exit_code;
            if ($g_testing)
            {
                LogInfo("#$slot_number: COMMAND: $command_line");
                sleep(0.05); # sleep 50 mili-seconds to let the system finish its stuff
                $exit_code = 0;
            }
            else
            {
#                LogDebug("#$slot_number: COMMAND: $command_line");
                LogInfo("#$slot_number: COMMAND: $command_line");
                $exit_code = system($command_line);
            }

            if ($exit_code)
            {
                LogInfo("#$slot_number: An error occured! See log for details.");
            }
        };

        my $error;
        if ($error = Exception::Class->caught())
        {
            LogWarning("#$slot_number: Problem with family '$family': $!");
        };
        LogInfo("#$slot_number: Done!\n#$slot_number: Duration: " . (time() - $start_time) . " seconds");

        if ($lock_manager && $lock_name)
        {
            # release lock
            $lock_manager->unlock($lock_name);
            LogInfo("#$slot_number: Lock released");
        }
    };

    my $error;
    if ($error = Exception::Class->caught())
    {
        LogError("#$slot_number: An unexpected error occured!\n$error");
    };
}


=pod

=head2 ProcessFamiliesByThread

B<Description>: Process an array of families.

B<ArgsCount>: 3-4

=over 4

=item arg: $nb_slots (integer) (R)

Number of thread to use to process families.

=item arg: $families (array ref) (R)

Array of families.

=item arg: $command (string) (R)

The command line to execute that includes space-holders using the syntax
'{member_name}' for family members or '{fasta}' for the family FASTA file name.

=item arg: $parameters (hash ref) (O)

Additional parameters given to RunCommandThread.

=back

B<Return>: nothing

=cut

sub ProcessFamiliesByThread
{
    my ($nb_slots, $families, $command, $parameters) = @_;

    # check arguments
    if (!$nb_slots || (0 >= int($nb_slots)))
    {
        LogWarning("ProcessFamiliesByThread: no number of thread provided!");
        return;
    }

    if (!$families || ('ARRAY' ne ref($families)) || !@$families)
    {
        LogWarning("ProcessFamiliesByThread: no family provided!");
        return;
    }

    if (!$command || ref($command))
    {
        LogWarning("ProcessFamiliesByThread: no valid command provided!");
        return;
    }

    $parameters ||= {};
    if ($parameters && ('HASH' ne ref($parameters)))
    {
        LogWarning("ProcessFamiliesByThread: invalid 'parameters' argument (not a hash ref)!");
        return;
    }

    # fill @gs_selected_families with current set of families
    ShareFamilyList($families);
    # free database handler to avoid conflicts between threads
    DisconnectDatabase();

    $g_computation_start_date = time();
    LogInfo("Processing " . scalar(@gs_selected_families) . " families...");

    if (@gs_selected_families)
    {
        my @threads_list = ();
        # launch threads
        PrintDebug("Working with $nb_slots threads");
        for (my $slot_number = 0; $slot_number < $nb_slots; ++$slot_number)
        {
            push(@threads_list, threads->create(\&RunCommandThread, $slot_number, $command, $parameters));
        }
        # wait for last threads to end
        # PrintDebug("Almost done...");
        foreach my $thread (@threads_list)
        {
            $thread->join();
        }
    }

    LogInfo("All threads DONE!");

    return;
}


=pod

=head2 WriteFamilyFASTA

B<Description>: Generate a family FASTA file.

B<ArgsCount>: 2-3

=over 4

=item arg: $family (Greenphyl::Family) (R)

a family object.

=item arg: $fasta_file_path (string) (R)

FASTA file path.

=item arg: $parameters (hash ref) (O)

Additional parameters.

=back

B<Return>: nothing

=cut

sub WriteFamilyFASTA
{
    my ($family, $fasta_file_path, $parameters) = @_;

    # check directory
    my $fasta_directory = $fasta_file_path;
    if ($fasta_directory =~ m/\//)
    {
        $fasta_directory =~ s/\/[^\/]+$//;
        if ($fasta_directory && !-e $fasta_directory)
        {
            if (system("mkdir -p $fasta_directory"))
            {
                LogError("Failed to create output FASTA directory for '$fasta_file_path'!\n$!");
            }
        }
    }
    
    # create file
    my $fasta_fh;
    if (open($fasta_fh, ">$fasta_file_path"))
    {
        print {$fasta_fh} $family->DumpFasta($parameters);
        close($fasta_fh);
    }
    else
    {
        LogError("Failed to open output FASTA file '$fasta_file_path'!\n$!");
    }
}


=pod

=head2 GenerateFamilyFASTA

B<Description>: Generate family FASTA files.

B<ArgsCount>: 1-2

=over 4

=item arg: $families (array ref) (R)

Array of families.

=item arg: $parameters (hash ref) (O)

Additional parameters.

=back

B<Return>: nothing

=cut

sub GenerateAllFamilyFASTA
{
    my ($families, $parameters) = @_;

    # check arguments
    if (!$families || ('ARRAY' ne ref($families)) || !@$families)
    {
        LogWarning("GenerateFamilyFASTA: no family provided!");
        return;
    }

    $parameters ||= {};
    if ($parameters && ('HASH' ne ref($parameters)))
    {
        LogWarning("GenerateFamilyFASTA: invalid 'parameters' argument (not a hash ref)!");
        return;
    }

    LogInfo("Generating FASTA for " . scalar(@$families) . " families...");

    foreach my $family (@$families)
    {
        my $fasta_file_path = GetFamilyFASTAPath($family, $parameters);
        
        # check generation mode and if file exists
        if ($parameters->{'-complete'} && (-e $fasta_file_path) && (!-z $fasta_file_path))
        {
            # skip existing files
            LogInfo("'$fasta_file_path' file exists, skipping.");
        }
        else
        {
            LogInfo("Creating '$fasta_file_path'");
            WriteFamilyFASTA($family, $fasta_file_path, $parameters);
        }
        ++$gs_processed_family_counter;
    }

    LogInfo("DONE!");

    return;
}


=pod

=head2 GetFamilyInfoListOutputFH

B<Description>: Returns the file handler to use for info output.

B<ArgsCount>: 1

=over 4

=item arg: $parameters (hash ref) (O)

Additional parameters. If '-complete' parameter is true, then only info of
missing families will be appended to the file.

=back

B<Return>: (GLOB)

Output file handler.

=cut

sub GetFamilyInfoListOutputFH
{
    if (!$g_list_fh)
    {
        my ($parameters) = @_;

        # check arguments
        $parameters ||= {};
        if ($parameters && ('HASH' ne ref($parameters)))
        {
            LogWarning("GetFamilyInfoListOutputFH: invalid 'parameters' argument (not a hash ref)!");
            return;
        }

        # check output mode
        if ($parameters->{'info_file_path'})
        {
            if ($parameters->{'-complete'})
            {
                if (!open($g_list_fh, "+<" . $parameters->{'info_file_path'}))
                {
                    LogError("Failed to open family info file '" . $parameters->{'info_file_path'} . "'!\n$!");
                }
                
                # parse existing family accessions
                while (my $line = <$g_list_fh>)
                {
                    if ($line =~ m/($Greenphyl::AbstractFamily::FAMILY_REGEXP)/)
                    {
                        $g_family_to_skip->{$1} = 1;
                    }
                    else
                    {
                        LogWarning("Family accession not found in line '$line'!");
                    }
                }
            }
            elsif (!open($g_list_fh, ">" . $parameters->{'info_file_path'}))
            {
                LogError("Failed to open output accession list file '" . $parameters->{'info_file_path'} . "'!\n$!");
            }
        }
        else
        {
            if ($parameters->{'-complete'})
            {
                # get family accessions from STDIN
                while (my $line = <STDIN>)
                {
                    if ($line =~ m/($Greenphyl::AbstractFamily::FAMILY_REGEXP)/)
                    {
                        $g_family_to_skip->{$1} = 1;
                    }
                    else
                    {
                        LogWarning("Family accession not found in line '$line'!");
                    }
                }
            }
            $g_list_fh = *STDOUT;
        }
    }
    
    return $g_list_fh;
}


=pod

=head2 GetFamilyToSkip

B<Description>: Returns the file handler to use for info output.

B<ArgsCount>: 1

=over 4

=item arg: $parameters (hash ref) (O)

Additional parameters. If '-complete' parameter is true, then only info of
missing families will be appended to the file.

=back

B<Return>: (GLOB)

Output file handler.

=cut

sub GetFamilyToSkip
{
    if (!$g_family_to_skip)
    {
        my ($parameters) = @_;
        GetFamilyInfoListOutputFH($parameters);
    }
    
    return $g_family_to_skip;
}


=pod

=head2 GenerateFamilyInfoList

B<Description>: Generate or complete family info list to STDOUT or into a
file if "-o" command line parameter has been used.
In "-complete" mode, each file line must begin with a family accession or
otherwise any matching accession in the reste of the line will be taken in
account.

B<ArgsCount>: 1-2

=over 4

=item arg: $families (array ref) (R)

Array of families.

=item arg: $parameters (hash ref) (O)

Additional parameters. If '-complete' parameter is true, then only info of
missing families will be appended to the file.

=back

B<Return>: nothing

=cut

sub GenerateFamilyInfoList
{
    my ($families, $parameters) = @_;

    # check arguments
    if (!$families || ('ARRAY' ne ref($families)) || !@$families)
    {
        LogWarning("GenerateFamilyInfoList: no family provided!");
        return;
    }

    $parameters ||= {};
    if ($parameters && ('HASH' ne ref($parameters)))
    {
        LogWarning("GenerateFamilyInfoList: invalid 'parameters' argument (not a hash ref)!");
        return;
    }

    # check output mode
    my $list_fh = GetFamilyInfoListOutputFH($parameters);
    my $family_to_skip = GetFamilyToSkip($parameters);

    my $info_string = $parameters->{'info'} || '{}';
    foreach my $family (@$families)
    {
        if (!exists($family_to_skip->{$family->accession}))
        {
            my $family_info_string = ReplaceFamilyTokens($family, $info_string, $parameters);
            print {$list_fh} "$family_info_string$g_delimiter";
            ++$gs_processed_family_counter;
        }
    }

    return;
}


=pod

=head2 GetNextSetOfFamilies

B<Description>: Returns the next set of families to process using the global
family selection parameter hash $g_family_selection_hash. The global shared
family set counter "$gs_family_page_counter" is automatically increased.

B<ArgsCount>: 0

B<Return> (array ref):

An array containing the next set of families or nothing (empty array) if all
families have already been processed.

=cut

sub GetNextSetOfFamilies
{
    lock($gs_family_page_counter);
    # compute next offset
    if ($g_family_selection_hash->{'LIMIT'})
    {
        $g_family_selection_hash->{'OFFSET'} = $gs_family_page_counter * $g_family_selection_hash->{'LIMIT'};
    }
    elsif ($gs_family_page_counter)
    {
        return [];
    }
    PrintDebug("$$: Get the next set (#$gs_family_page_counter) of families");
    ++$gs_family_page_counter;
    my $families = LoadFamilies($g_family_selection_hash);
    PrintDebug("$$: Got " . scalar(@$families) . " famil" . (1<@$families?'ies':'y'));
    return $families;
}


=pod

=head2 ShareFamilyList

B<Description>: Transform family dynamic (database) objects into semi-static
objects that can be shared by threads. Shared families are added to the shared
array "@gs_selected_families";

B<ArgsCount>: 1

B<ArgsCount>: 1

=over 4

=item arg: $families (array ref) (R)

Array of families to share.

=back

B<Return> nothing.

=cut

sub ShareFamilyList
{
    my ($families) = @_;
    # lock array: we don't want to 
    lock(@gs_selected_families);
    my $thread_dbi = undef; # ConnectToDatabase(GP('DATABASES')->[0]);
    # clone family instances to make them thread-safe
    foreach my $family (@$families)
    {
        # detach the object from its database handler as database handlers can
        # not be shared
        $family->setDatabaseHandler($thread_dbi);
        # create a shared clone
        my $shared_family = shared_clone($family);
        # store it into the shared family array
        push(@gs_selected_families, $shared_family);
    }
}




# Script options
#################

=pod

=head1 OPTIONS

    process_families.pl <FAMILY_SELECTORS>
        <
          <
            <-generate | -complete>
            -info ['FORMAT'] [-null | -delimiter <DELIMITER>]
            | -fasta [-without-species-code] [-without-splice] [with-family-info]
                     [-with-sequence-annotation]
                     [<-exclude-species | -include-species> SPECIES_CODES_LIST]
          >
          | -run '<COMMAND>' [-test]
        >
        [-f <FASTA_DIRECTORY>]
        [-o <INFO_FILE>]
        [-t <THREAD_COUNT>]
        [-timeout <MINUTES_TIMEOUT>]
        [-email <email@address>]

=head2 Parameters

=over 4

=item B<FAMILY_SELECTORS> (multiple options):

Family selectors are a list of parameters that can be used in combination to
select a specific set of families. Theses parameters are defined in
Greenphyl::Tools::Families::PrepareFamilyLoadingQuery function. When
parameters support multiple values, they can be specified using one of the
following methods (not valid with text values):
parameter=value
parameter=value1,value2,value3
"parameter=value 1,value 2,value 3" (note: quotes required when spaces are used)
parameter=value1 parameter=value2 parameter=value3

When multiple values contain text (with coma or spaces), the only valid method
to specify multiple values is:
"parameter=value1" "parameter=value2" "parameter=value3"

Some selectors support files as input. To specify a file, use the format
"file:<FILE_PATH>".
Ex.: accession=file:my_families.txt

Selectors supporting files:
family_id, accession, species_id, species_code, sequence_id, sequence_accession,
sequence_accession_like, ipr_id, ipr_code, dbxref_id, dbxref_accession, go_id
and go_code.

=over 4

=item B<family_id> (integer): one or several numeric family identifier (DB)
  separated by coma or semicolon or spaces.

=item B<name> (text): on or more family names. Several names can be specified
  by duplicating this parameter.

=item B<accession> (string): one or several family accession separated by coma
  or semicolon or spaces.

=item B<level> (integer): one or more family clustering levels separated by
  coma or semicolon or spaces.

=item B<type> (string): one or more cluster types separated by coma or
  semicolon or spaces.
  Possible valid values are: 'N/A','superfamily','family','subfamily','group'.

=item B<description> (text): one or more words that should be found in family
  descriptions. This parameter is case-insensitive. Several expressions parts
  can be specified by duplicating this parameter.
  ex.: "description=transcription factor" "description=PLATZ"

=item B<validated> (string): one or more validation status separated by coma or
  semicolon or spaces.
  Possible valid values are: 'N/A','high','normal','unknown', 'suspicious',
  'clustering error'.
  
=item B<black_listed> (integer): if set to 1, only select black-listed
  families, if set to 0, only select non-black-listed families.

=item B<inference> (string): one or more validation status separated by coma or
  semicolon or spaces.

=item B<taxonomy_id> (integer): one or more taxonomy identifier (DB) separated
  by coma or semicolon or spaces used to select families that are specific to a
  given taxonomy element.

=item B<plant_specific> (integer): plant-specific score value the family must
  have.

=item B<min_plant_specific> (integer): minimum (inclusive) plant-specific score
  value the family must have.

=item B<max_plant_specific> (integer): maximum (inclusive) plant-specific score
  value the family must have.

=item B<max_sequences> (integer): maximum number of sequences in family.

=item B<min_sequences> (integer): maximum number of sequences in family.

=item B<with_phylogeny> (boolean): set to true to only select families with
  homologies.

=item B<without_phylogeny> (boolean): set to true to only select families
  without homologies.

=item B<sequence_id> (integer): one or more sequence identifier (DB) separated
  by coma or semicolon or spaces.

=item B<sequence_accession> (string): one or more sequence accession separated
  by coma or semicolon or spaces. The check is case sensitive and must be an
  exact complete match.

=item B<sequence_accession_like> (string): one or more sequence (partial)
 accession separated by coma or semicolon or spaces. Case insensitive match.
 Note: the characters '_' and '%' are treated as SQL jocker characters. A '%'
 character is appended to the given accesion (in order to match splice-forms
 for instance).

=item B<species_id> (integer): one or more species identifier (DB) separated by
  coma or semicolon or spaces. Selection is relative to species_selection
  parameter.

=item B<species_code> (string): one or more species codes (3-5 characters)
  separated by coma or semicolon or spaces. Selection is relative to
  species_selection parameter.

=item B<species_selection> (string): can be one of:
  -'unfilter_species' or not defined: no species filtration;
  -'all_species': family must contain sequences from all the given species;
  -'species_specific': family must be specific to one of the given species;

=item B<ipr_id> (integer): one or more IPR identifier (DB) separated by coma or
  semicolon or spaces used to select families that contain sequences bearing the
  given IPR domains.

=item B<ipr_code> (string): one or more IPR code separated by coma or semicolon
  or spaces used to select families that contain sequences bearing the given IPR
  domains.
  
=item B<uniprot> (integer): if set to 1, the families must have sequences with a
  UniProt accession.

=item B<dbxref_id> (integer): one or more cross-reference identifier (DB)
  separated by coma or semicolon or spaces used to select families that have
  those cross-references (from dbxref_families table).
  
=item B<dbxref_accession> (string): one or more cross-reference accessions
  separated by coma or semicolon or spaces used to select families that have
  those cross-references (from dbxref_families table).
  
=item B<go_id> (integer): TODO

=item B<go_code> (string): TODO

=back

=item B<-fasta>:

output FASTA files.

=item B<-without-species-code>:

When outputing FASTA files, do not include species code in sequence names.

=item B<-without-splice>:

When outputing FASTA files, do not output splice forms.

=item B<-with-sequence-annotations>:

When outputing FASTA files, include sequence annotation.

=item B<-with-family-info>:

When outputing FASTA files, replace sequence name by family code and family name (plus sequence name)

=item B<-exclude-species>:

When outputing FASTA files, exclude sequences from the given list of species
codes. Several species codes can be specified either using multiple
"-exclude-species" parameters or by separating codes with comas.
ex.1: -exclude-species=MALDO,ZEAMA
ex.2: -exclude-species=MALDO -exclude-species=ZEAMA

=item B<-include-species>:

When outputing FASTA files, only include sequences from the given list of
species codes. Several species codes can be specified either using multiple
"-include-species" parameters or by separating codes with comas.
ex.1: -include-species=ORYSA,ARATH
ex.2: -include-species=ORYSA -include-species=ARATH

=item B<-info> (string):

Output a list of requested family info, one per line (-generate or -comlplete).
Any curly-brackets block ("{member_name}") in the FORMAT string will be replaced
with the corresponding family member value.
Some special members are also supported: {species} (generates a coma-separated
array of species codes), {speices_count} (total number of species in family) and
{fasta} (path to the family FASTA file).
If no string is given, accessions will be output by default. 
Ex.: -info '{accession} ({sequence_count} sequences): {name}'

=item B<-null>:

For "-info" only. Separate each family info entry by a null character.

=item B<-delimiter> (string):

For "-info" only. Separate each family info entry with the given string.

=item B<-generate>:

Output files and overwrite existing files.

=item B<-complete>:

Only output missing files and do not overwrite existing files.

=item B<-run> (string):

Execute for each family the given command and replace any curly-brackets block
("{member_name}") with the corresponding family member name of the FASTA file
name if '{fasta}' is used.
ex.: process_families.pl -run 'echo "{accession} has {sequence_count} sequences in {fasta} file."'

=item B<-test>:

If used, each command line to run on a family will be displayed instead of being
executed. No external program execution will be performed.

=item B<-f> (string):

Directory where the FASTA file are.

=item B<-o> (string):

Name of the file that contains family info.

=item B<-t> (integer):

Number of concurrent threads that can be used when performing commands on
multiple FASTA files or families.
For "-run" mode only.

=item B<-timeout> (integer):

allowed processing time in minutes. After that delay, no more remaining family
will be processed.
For "-run" mode only.

=item B<-email> (string):

e-mail address that will be notified when all process is done.

=back

=cut


# CODE START
#############

# options processing
my ($man, $help) = (0, 0);
my $debug;
my $generate = 0;
my $complete = 0;
my $to_fasta = 0;
my $without_species_code = 0;
my $without_splice = 0;
my $with_family_info = 0;
my $with_annotations = 0;
my $to_info = undef;
my $command = '';
my $fasta_directory = '';
my $info_file_path = '';
my $thread_count = $DEFAULT_THREADS_COUNT;
my $email = '';
my $timeout = 0;
my $process_name = '';
my $null_delimiter = 0;
my $delimiter = '';
my @include_species;
my @exclude_species;

# parse options and print usage if there is a syntax error.
GetOptions("help|?"      => \$help,
           "man"         => \$man,
           "debug:s"     => \$debug,
           "test"        => \$g_testing,
           "generate"    => \$generate,
           "complete"    => \$complete,
           "info:s"      => \$to_info,
           "fasta"       => \$to_fasta,
           "without-species-code|without-species-codes" => \$without_species_code,
           "with-family-info" => \$with_family_info,
           "without-splice|without-splices|without-splice-forms|without-splice-form" => \$without_splice,
           "with-sequence-annotations|with-annotations|with-annotation" => \$with_annotations,
           "run|command=s"  => \$command,
           "f=s"         => \$fasta_directory,
           "o=s"         => \$info_file_path,
           "t=i"         => \$thread_count,
           "email=s"     => \$email,
           "timeout"     => \$timeout,
           "null"        => \$null_delimiter,
           "delimiter=s" => \$delimiter,
           "include-species=s" => \@include_species,
           "exclude-species=s" => \@exclude_species,
           @Greenphyl::Log::LOG_GETOPT,
) or pod2usage(1);
if ($help) {pod2usage('-verbose' => 1, '-exitval' => 0);}
if ($man) {pod2usage('-verbose' => 2, '-exitval' => 0);}

# change debug mode?
if (defined($debug))
{
    # just '-debug' ?
    if ($debug eq '')
    {
        $debug = 1;
    }
    $DEBUG = $debug;
}

# parameters check
if (!$generate && !$complete && !$command)
{
    LogError('Missing parameters!');
    pod2usage(1);
}

# check for null delimiter
if ($null_delimiter)
{
    $g_delimiter = "\0";
}
elsif ($delimiter)
{
    $g_delimiter = $delimiter;
}

# Prepare parameter hash
my $parameters = {};
$parameters->{'info_file_path'} = $info_file_path;
$parameters->{'fasta_directory'} = $fasta_directory;
$parameters->{'without_species_code'} = $without_species_code;
$parameters->{'without_splice'} = $without_splice;
$parameters->{'with_annotation'} = $with_annotations;
$parameters->{'with_family_info'} = $with_family_info;
$parameters->{'timeout'} = $timeout;

if ($to_fasta)
{
    if (@include_species && @exclude_species)
    {
        LogError("You can't specify both a list of species to include and a list if species to exclude!");
        pod2usage(1);
    }
    $parameters->{'include_species'} = {};
    $parameters->{'exclude_species'} = {};

    # split multiple codes provided inside a single string
    @include_species = split(/[\s,]+/, join(',', @include_species));
    @exclude_species = split(/[\s,]+/, join(',', @exclude_species));

    foreach my $species_code (@include_species)
    {
        # check if species exists
        my $species = Greenphyl::Species->new(
            GetDatabaseHandler(),
            {
                'selectors' => {'code' => uc($species_code),},
            },
        );
        if ($species)
        {
            $parameters->{'include_species'}->{$species->code} = $species;
        }
        else
        {
            LogWarning("Invalid species code ignored: '$species_code'");
        }
    }

    foreach my $species_code (@exclude_species)
    {
        # check if species exists
        my $species = Greenphyl::Species->new(
            GetDatabaseHandler(),
            {
                'selectors' => {'code' => uc($species_code),},
            },
        );
        if ($species)
        {
            $parameters->{'exclude_species'}->{$species->code} = $species;
        }
        else
        {
            LogWarning("Invalid species code ignored: '$species_code'");
        }
    }
    
    LogInfo("Filtering species:\n-Include only: " . join(', ', keys(%{$parameters->{'include_species'}})) . "\n-Exclude: " . join(', ', keys(%{$parameters->{'exclude_species'}})) . "\n");
}
elsif (@include_species || @exclude_species)
{
    LogWarning("A list of species to exclude or to include has been specified but this option will be ignored because it is only available for FASTA output!");
}


# Check for family selection
$gs_family_page_counter = 0;
$g_family_selection_hash = GetFamilyLoadingParameters();
if (HaveFamiliesBeenFiltered($g_family_selection_hash))
{
    LogInfo("Family filter:");
    my ($filter, $value);
    while ( ($filter, $value) = each(%$g_family_selection_hash) )
    {
        my $filter_string = "    $filter: ";
        if (!ref($value))
        {
            $filter_string .= $value;
        }
        elsif ('ARRAY' eq ref($value))
        {
            $filter_string .= '[' . join(', ', @$value) . ']';
        }
        elsif ('HASH' eq ref($value))
        {
            $filter_string .= '[' . join(', ', map {"$_=" . $value->{$_}} keys(%$value)) . ']';
        }
        else
        {
            $filter_string .= '...';
        }
        LogInfo($filter_string);
    }
}
LogInfo("Parameters:");
my ($param_name, $param_value);
while (($param_name, $param_value) = each(%$parameters))
{
    my $param_string = "    $param_name: ";
    if (!ref($param_value))
    {
        $param_string .= $param_value;
    }
    elsif ('ARRAY' eq ref($param_value))
    {
        $param_string .= '[' . join(', ', @$param_value) . ']';
    }
    elsif ('HASH' eq ref($param_value))
    {
        $param_string .= '[' . join(', ', map {"$_=" . $param_value->{$_}} keys(%$param_value)) . ']';
    }
    else
    {
        $param_string .= '...';
    }
    LogInfo($param_string);
}

my $families = GetNextSetOfFamilies();

if (HaveFamiliesBeenFiltered($g_family_selection_hash) && (0 == @$families))
{
    confess "WARNING: No family matches your criteria!\n";
}
else
{
    #+FIXME: add support for process tracking
    # check if there is a process name
    # load process if exists
    # check if process stat has been set
    # if 'perform'
    #  -create process entry
    #  -mark all selected families to process
    #  -if steps to perform, set them
    # if 'resume'
    #  -load process entry
    #  -update it
    #  -get list of families to process
    # if 'pause'
    #  -load process entry
    #  -update it
    #  -get list of families to process and pause them
    # if 'cancel'
    #  -load process entry
    #  -update it
    #  -get list of families to process and cancel them
}

# loop on all matching families
$gs_processed_family_counter = 0;
while (@$families)
{
    # check mode and process families
    if ($generate || $complete)
    {
        if ($complete)
        {
            # complete
            $parameters->{'-complete'} = 1;
        }

        # proceed
        if ($to_fasta)
        {
            GenerateAllFamilyFASTA($families, $parameters);
        }
        elsif (defined($to_info))
        {
            if ($to_info =~ m/\w/)
            {
                $parameters->{'info'} = $to_info;
            }
            GenerateFamilyInfoList($families, $parameters);
        }
        else
        {
            LogError('Missing parameters!');
            pod2usage(1);
        }
    }
    elsif ($command)
    {
        $thread_count ||= $DEFAULT_THREADS_COUNT;
        ProcessFamiliesByThread($thread_count, $families, $command, $parameters);
    }

    $families = GetNextSetOfFamilies();
}


# check for e-mail notification
if ($email)
{
    SendMail(
        'email'   => $email,
        'subject' => "[GreenPhyl] Process Families $$: done",
        'body'    => "All requested families have been processed!",
    );
}

LogInfo("All families ($gs_processed_family_counter) processed!");

exit(0);

# CODE END
###########


=pod

=head1 AUTHORS

Mathieu ROUARD (Bioversity), m.rouard@cgiar.org

Valentin GUIGNON (Bioversity), v.guignon@cgiar.org

Matthieu CONTE (Syngenta)

Sebastien BRIOIS (Syngenta)

=head1 VERSION

Version 1.0.0

Date 14/11/2012

=head1 SEE ALSO

GreenPhyl documentation.

=cut
