#!/usr/bin/env perl

# Script qui lit un FASTA et ressort une copie en excluant les séquences
# marqueés filtrées en base.

use strict;
use warnings;

use FindBin;
use lib "$FindBin::Bin/../../lib";
use lib "$FindBin::Bin/../../local_lib";
use Carp qw (cluck confess croak);

use Getopt::Long;
use Pod::Usage;

use Bio::SeqIO;

use Greenphyl;
use Greenphyl::Log('nolog' => 1,);
use Greenphyl::Sequence;

++$|; #no buffering

our $DEBUG = 0;

my ($help, $man, $debug);
my ($in_filename, $out_filename);

GetOptions("help|?"  => \$help,
           "man"     => \$man,
           "debug:s" => \$debug,
           "i|in:s"  => \$in_filename,
           "o|out:s" => \$out_filename,
           @Greenphyl::Log::LOG_GETOPT, # skip log parameters
) or pod2usage(1);
if ($help) {pod2usage('-verbose' => 1, '-exitval' => 0);}
if ($man) {pod2usage('-verbose' => 2, '-exitval' => 0);}

my $seqi_obj = Bio::SeqIO->new(
    -file => $in_filename,
    -format => "fasta"
);
my $seqo_obj = Bio::SeqIO->new(
    -file => ">$out_filename",
    -format => "fasta"
);

LogInfo("Filtering $in_filename to $out_filename");

while (my $seq_obj = $seqi_obj->next_seq)
{
    my $sequence = Greenphyl::Sequence->new(
            GetDatabaseHandler(),
            {
                'selectors' => { 'accession' => $seq_obj->display_id, },
            },
        );

    if (!$sequence)
    {
        confess LogError("Failed to load sequence " . $seq_obj->display_id . " from database!");
    }
    else
    {
        if ($sequence->filtered)
        {
            LogInfo("Filtering " . $seq_obj->display_id);
        }
        else
        {
            $seqo_obj->write_seq($seq_obj);
        }
    }
}

LogInfo("Done.");
HandleErrors();

exit(0);

# CODE END
