#!/usr/bin/env perl
# report_data_for_species.pl --help
#
# created on 13/05/2013 - sebastien briois

use strict;
use warnings;

use Getopt::Long;
use Pod::Usage;

use Data::Dumper;

use FindBin;
use lib "$FindBin::Bin/../../lib";
use lib "$FindBin::Bin/../../local_lib";

use Greenphyl;
use Greenphyl::Log;

$|++; # Autoflush

my $g_dbh = GetDatabaseHandler();

{
    # Script parameters
    my ( $species );
    
    # Define script options
    GetOptions(
        'help|h'            => sub { pod2usage( -verbose => 1 ) },
        'man'               => sub { pod2usage( -verbose => 2 ) },
        'species=s'         => \$species, 
    ) or pod2usage(2);
    
    pod2usage('--species must be specified')
        unless defined $species;
    
    run( $species );
}

sub run
{
    my ($species) = @_;
    
    my $sequences = $g_dbh->selectall_arrayref(
        qq{
            SELECT DISTINCT
                accession,
                ipr_id, uniprot_id,
                go_id, go.code, go.namespace, go.name
            FROM sequences
                JOIN species ON species.id = species_id
                JOIN go_sequences_cache ON sequences.id = sequence_id
                JOIN go on go_id = go.id
            WHERE species.code = ?
            ORDER BY accession
        }, { Slice => {} }, $species
    );
    
    foreach my $sequence (@$sequences) {
        if ( $sequence->{'ipr_id'} ) {
            print $sequence->{'accession'} . "\tIEA\t";
            print $sequence->{'code'} . "\t";
            print $sequence->{'namespace'} . "\t";
            print $sequence->{'name'} . "\n";
        }
        elsif ( $sequence->{'uniprot_id'} ) {
            my $evidence_code = get_evidence_code_for( $sequence->{'go_id'}, $sequence->{'uniprot_id'} );
            print $sequence->{'accession'} . "\t";
            print $evidence_code . "\t";
            print $sequence->{'code'} . "\t";
            print $sequence->{'namespace'} . "\t";
            print $sequence->{'name'} . "\n";
        }
        else {
            print $sequence->{'accession'} . "\t\t\t\t\n";
        }
    }
}

sub get_evidence_code_for
{
    my ($go_id, $uniprot_id) = @_;
    
    my ($evidence_code) = @{$g_dbh->selectcol_arrayref(
        qq{SELECT evidence_code FROM go_uniprot WHERE go_id = ? AND dbxref_id = ?}, 
        undef, $go_id, $uniprot_id
    )};
    
    return $evidence_code;
}
