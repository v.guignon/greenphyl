#!/usr/bin/env perl

=pod

=head1 NAME

view_custom_family_data.pl - Dumps custom family data object

=head1 SYNOPSIS

    Dumps custom family data object

=head1 REQUIRES

Perl5

=head1 DESCRIPTION

Dumps custom family data object.

=cut

use strict;
use warnings;

use FindBin;
use lib "$FindBin::Bin/../../lib";
use lib "$FindBin::Bin/../../local_lib";
use Carp qw (cluck confess croak);

use Greenphyl;
use Greenphyl::Tools::Families;

use Getopt::Long;
use Pod::Usage;
use Data::Dumper;

++$|; #no buffering




# Script global constants
##########################

=pod

=head1 CONSTANTS

B<$REV_STRING>: (string)

Current revision (auto-set by GIT).

B<$DEBUG>: (boolean)

When set to true, it enables debug mode.

=cut

# SVN management part
my $REV_STRING = '$id$';

our $DEBUG = 0;




# Script options
#################

=pod

=head1 OPTIONS

view_custom_family_data.pl [-help | -man | -debug] <accession=<accession> | custom_family_id=<custom_family_id> >

=over 4

=item B<-help>:

Prints a brief help message and exits.

=item B<-man>:

Prints the manual page and exits.

=item B<-debug>:

Executes the script in debug mode.
Default: 0 (not in debug mode).

=item B<accession> (string):

Custom family accession name.

=item B<custom_family_id> (string):

Custom family database identifier.

=back

=cut


# CODE START
#############

# options processing
my ($man, $help, $debug) = (0, 0, undef);
# parse options and print usage if there is a syntax error.

GetOptions('help|?'     => \$help,
           'man'        => \$man,
           'debug:s'    => \$debug,
) or pod2usage(1);
if ($help) {pod2usage('-verbose' => 1, '-exitval' => 0);}
if ($man) {pod2usage('-verbose' => 2, '-exitval' => 0);}

# change debug mode if requested/forced
if (defined($debug))
{
    # just '-debug' ?
    if ($debug eq '')
    {
        $debug = 1;
    }
    $DEBUG = $debug;
}

eval
{
    my $custom_family = LoadCustomFamily();
    print Dumper([$custom_family->data]);
};

# catch
HandleErrors();

exit(0);

__END__
# CODE END
###########

=pod

=head1 AUTHORS

Mathieu ROUARD (Bioversity), m.rouard@cgiar.org

Valentin GUIGNON (Bioversity), v.guignon@cgiar.org

Matthieu CONTE (Syngenta)

Sebastien BRIOIS (Syngenta)

=head1 VERSION

Version 1.0.0

Date 02/12/2014

=head1 SEE ALSO

GreenPhyl documentation.

=cut
