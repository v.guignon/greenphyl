#!/usr/bin/perl

=pod

=head1 NAME

dump_genome.pl - Dump sequences of a given genome.

=head1 SYNOPSIS

    dump_genome.pl -genome MUSAC -o MUSAC.faa

=head1 REQUIRES

Perl5

=head1 DESCRIPTION

=cut

use strict;
use warnings;

use lib "../../lib";
use lib '../../local_lib';
use Carp qw (cluck confess croak);

# use Greenphyl;
# use Greenphyl::Log('nolog' => 1,);
use DBI;

use Getopt::Long;
use Pod::Usage;

use Data::Dumper;




# Script global constants
##########################

=pod

=head1 CONSTANTS

B<$DEBUG>: (boolean)

When set to true, it enables debug mode.

B<$CONSTANT_NAME>: ([constant nature]) #+++

[constant description and use]. #+++

=cut

our $DEBUG = 0;
our %GENOMES = (
  'AMBTC_v1.0'          => 'AMBTC_v1.0.faa',
  'ARATH_Araport11'     => 'ARATH_Araport11.faa',
  'BETVU_RefBeet_v1.2'  => 'BETVU_RefBeet_v1.2.faa',
  'BRADI'               => 'BRADI_pan.faa',
  'BRANA'               => 'BRANA_pan.faa',
  'BRAOL'               => 'BRAOL_pan.faa',
  'BRARR'               => 'BRARR_pan.faa',
  'CAJCA_Asha_v1.0'     => 'CAJCA_Asha_v1.0.faa',
  'CAPAN'               => 'CAPAN_pan.faa',
  'CHEQI_JGI_v1.0'      => 'CHEQI_JGI_v1.0.faa',
  'CICAR'               => 'CICAR_pan.faa',
  'CITMA_v1.0'          => 'CITMA_v1.0.faa',
  'CITME_v1.0'          => 'CITME_v1.0.faa',
  'CITSI_v2.0'          => 'CITSI_v2.0.faa',
  'COCNU'               => 'COCNU_pan.faa',
  'COFAR_RB_v1.0'       => 'COFAR_RB_v1.0.faa',
  'COFCA_v1.0'          => 'COFCA_v1.0.faa',
  'CUCME_DHL92_v3.6.1'  => 'CUCME_DHL92_v3.6.1.faa',
  'CUCSA'               => 'CUCSA_pan.faa',
  'DAUCA_JGI_v2.0'      => 'DAUCA_JGI_v2.0.faa',
  'DIORT_TDr96_F1_v1.0' => 'DIORT_TDr96_F1_v1.0.faa',
  'ELAGV_RefSeq_v1.0'   => 'ELAGV_RefSeq_v1.0.faa',
  'FRAVE_v4.0a1'        => 'FRAVE_v4.0a1.faa',
  'HELAN_XRQ_v1.2'      => 'HELAN_XRQ_v1.2.faa',
  'HORVU_IBSC_v2.0'     => 'HORVU_IBSC_v2.0.faa',
  'IPOTF'               => 'IPOTF_pan.faa',
  'IPOTR_NSP323'        => 'IPOTR_NSP323.faa',
  'MAIZE'               => 'MAIZE_pan.faa',
  'MALDO'               => 'MALDO_pan.faa',
  'MANES_AM560_2_v6.1'  => 'MANES_AM560_2_v6.1.faa',
  'MEDTR'               => 'MEDTR_pan.faa',
  'MUSAC'               => 'MUSAC_pan.faa',
  'MUSBA_Mba1.1'        => 'MUSBA_Mba1.1.faa',
  'OLEEU_v1.0'          => 'OLEEU_v1.0.faa',
  'ORYGL_v1.0'          => 'ORYGL_v1.0.faa',
  'ORYSA'               => 'ORYSA_pan.faa',
  'PHAVU_v2.0'          => 'PHAVU_v2.0.faa',
  'PHODC_v3.0'          => 'PHODC_v3.0.faa',
  'SACSP_AP85_441_v1.0' => 'SACSP_AP85_441_v1.0.faa',
  'SOLLC_ITAG3.2'       => 'SOLLC_ITAG3.2.faa',
  'SOLTU_PGSC_v4.03'    => 'SOLTU_PGSC_v4.03.faa',
  'SORBI'               => 'SORBI_pan.faa',
  'SOYBN'               => 'SOYBN_pan.faa',
  'THECC'               => 'THECC_pan.faa',
  'TRITU'               => 'TRITU_pan.faa',
  'VITVI'               => 'VITVI_pan.faa',
);




# Script global variables
##########################

=pod

=head1 VARIABLES

B<[g_variable_name]>: ([variable nature]) #+++

[variable description, use and default value]. #+++
Default: [variable default value if one] #+++

#--- Example:
#--- B<$g_output_method>: (integer)
#---
#--- used to store current output method;
#--- 0=raw text (default), 1=image, 2=html.
#---
#---     ...
#---
#--- my $g_output_method = 0;
#---

=cut

my $g_dbh;




# Script global functions
##########################

=pod

=head1 FUNCTIONS

=head2 DumpGenome

B<Description>: [function description]. #+++

B<ArgsCount>: [count of arguments] #+++

=over 4

=item $variable_name: ([variable nature]) ([requirements]) #+++ see below

#--- requirement can be:
#--- (R)=required,
#--- (O)=optional
#--- (U)=optional and must be undef if omitted
[variable description]. #+++

=item $variable_name2: ([variable nature]) ([requirements]) #+++

[variable description]. #+++

=back

B<Return>: ([return type]) #+++

[return description]. #+++

B<Example>:

    [example code] #+++

=cut

sub DumpGenome
{
    my ($genome, $output) = @_;
    my ($sql_query, $rows, @bind_values);
    my $fh = *STDOUT;
    if ($output)
    {
      open($fh , '>', $output) or confess "ERROR: Failed to open output file '$output'!\n";
    }
    $sql_query = "SELECT s.accession, s.annotation, s.polypeptide FROM sequences s JOIN genomes g ON g.id = s.genome_id WHERE g.accession = ?;";
    @bind_values = ($genome);
    $rows = $g_dbh->selectall_arrayref($sql_query, { 'Slice' => {} }, @bind_values);
    foreach my $row (@$rows)
    {
        print {$fh} ">" . $row->{'accession'} . ($row->{'annotation'} ? '|' . $row->{'annotation'} : '') . "\n";
        my $polypeptide = $row->{'polypeptide'};
        $polypeptide =~ s/(\w{80})/$1\n/g;
        print {$fh} $polypeptide . "\n";
    }
    
    close $fh if ($output);
}


=pod

=head2 ListGenomes

B<Description>: [function description]. #+++

B<ArgsCount>: [count of arguments] #+++

=over 4

=item $variable_name: ([variable nature]) ([requirements]) #+++ see below

#--- requirement can be:
#--- (R)=required,
#--- (O)=optional
#--- (U)=optional and must be undef if omitted
[variable description]. #+++

=item $variable_name2: ([variable nature]) ([requirements]) #+++

[variable description]. #+++

=back

B<Return>: ([return type]) #+++

[return description]. #+++

B<Example>:

    [example code] #+++

=cut

sub ListGenomes
{
    my $sql_query = "SELECT g.accession FROM genomes g ORDER BY g.accession ASC;";
    my $genomes = $g_dbh->selectall_arrayref($sql_query, { 'Slice' => {} });
    return $genomes;
}




# Script options
#################

=pod

=head1 OPTIONS

#--- describes parameters given to the script
#+++ command line syntax
#--- requirement of the option and its parameter can be:
#--- required: name or nature inside <>
#--- optional: name or nature inside []
#--- alternative between 2 elements: elements separated by a |

=head2 Parameters

=over 4

#+++=item B<-help>:
#+++
#+++Prints a brief help message and exits.
#+++
#+++=item B<-man>:
#+++
#+++Prints the manual page and exits.
#+++
#+++=item B<-debug> (integer):
#+++
#+++Executes the script in debug mode. The integer value is optional and is used
#+++to set debug level (use 0 to force disable debug mode).
#+++Default: 0 (not in debug mode).
#+++

=item B<[option_name]> ([option nature]): #+++

[option description]. #+++
Default: [option default value if one] #+++

#--- remove if log not used
#+++=item B<-log>:
#+++
#+++Enable logging.
#+++
#+++=item B<-log-*> (any):
#+++
#+++Logging arguments.

=back
#--- Example:
#---
#--- Template.pl [-help | -man]
#---
#--- Template.pl [-debug [debug_level]] [-size <width> [height]]
#---
#--- =over 4
#---
#--- =item B<-help>:
#---
#--- Prints a brief help message and exits.
#---
#--- =item B<-man>:
#---
#--- Prints the manual page and exits.
#---
#--- =item B<-debug> (integer):
#---
#--- Executes the script in debug mode. If an integer value is specified, it will
#--- be the debug level. If "-debug" option was used without specifying a debug
#--- level, level 1 is assumed.
#--- Default: 0 (not in debug mode).
#---
#---=item B<-size> (positive_real) (positive_real):
#---
#--- Set the dimensions of the object that will be drawn. The first value is
#--- the width; the height is the second value if specified, otherwise it will
#--- assume height and width are equal to the first value.
#--- Default: width and height are set to 1.
#---
#---=back

=cut


# CODE START
#############

# options processing
my ($man, $help, $debug) = (0, 0, undef);
my ($genome, $output);

# parse options and print usage if there is a syntax error.
GetOptions('help|?'  => \$help,
           'man'     => \$man,
           'debug:s' => \$debug,
           'g|genome=s'  => \$genome,
           'o|output=s'  => \$output,
) or pod2usage(1);
if ($help) {pod2usage('-verbose' => 1, '-exitval' => 0);}
if ($man) {pod2usage('-verbose' => 2, '-exitval' => 0);}

# change debug mode if requested/forced
if (defined($debug))
{
    # just '-debug' ?
    if ($debug eq '')
    {
        $debug = 1;
    }
    $DEBUG = $debug;
}


# connect
$g_dbh = DBI->connect( "dbi:mysql:database=greenphyl_v5;host=localhost;port=3306", 'greenphyl', 'atlas76');

if (!$g_dbh)
{
    confess "ERROR: Unable to connect to database!\n$DBI::errstr\n";
}

# maximum length of 'long' type fields (LONG, BLOB, CLOB, MEMO, etc.)
$g_dbh->{'LongReadLen'} = 512 * 1024; # 512Kb

# more explicit error reporting
$g_dbh->{'HandleError'} = sub { confess(shift()) };

my ($sql_query, @bind_values, $rows);
if ($genome)
{
    if ($genome eq 'auto')
    {
        foreach $genome (keys(%GENOMES))
        {
            my $file_output = $GENOMES{$genome};
            # Add path if specified.
            if (($output =~ m/\/$/) && (-d $output))
            {
                $file_output = $output . $file_output;
            }
            DumpGenome($genome, $file_output);
        }
    }
    else
    {
        DumpGenome($genome, $output);
    }
}
else
{
    my $genomes = ListGenomes();
    print "Genomes:\n";
    foreach my $genome (@$genomes)
    {
        print "  - " . $genome->{'accession'} . "\n";
    }
}

exit(0);

# CODE END
###########


=pod

=head1 AUTHORS

Valentin GUIGNON (Bioversity), v.guignon@cgiar.org

=head1 VERSION

Version 1.0.0

Date 27/05/2021

=head1 SEE ALSO

GreenPhyl documentation.

=cut
