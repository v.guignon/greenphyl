#!/usr/bin/perl

=pod

=head1 NAME

Perform BLAST analysis of a multi-FASTA against GreenPhyl database.

=head1 SYNOPSIS

     perl large_scale_match_searches.pl -i test.fasta -o temp/output.blast -p blastp -evalue=1e-10 -cluster_queue=greenphyl -K=5 -outfmt=0 -d=/bank/greenphyl/CHLRE

=head1 REQUIRES

Perl5

=head1 DESCRIPTION

Takes as input a fasta file and runs a program (blast by default) against the
gene family catalogue of GreenPhyl. It provides as output a summary of the
results.

=cut

use strict;
use warnings;

use FindBin;
use lib "$FindBin::Bin/../../lib";
use lib "$FindBin::Bin/../../local_lib";
use Carp qw (cluck confess croak);

use Getopt::Long qw(:config pass_through);
use Pod::Usage;

use Greenphyl;
use Greenphyl::Sequence;
use Greenphyl::Family;
use Greenphyl::Log('nolog' => 1,);

use Bio::SeqIO;
use Bio::SearchIO;

++$|; #no buffering




# Script global constants
##########################

=pod

=head1 CONSTANTS

B<$REV_STRING>: (string)

Current revision (auto-set by GIT).

B<$DEBUG>: (boolean)

When set to true, it enables debug mode.

=cut

# SVN management part
my $REV_STRING = '$id$';

my $DEBUG = 0;



# Script global functions
##########################

=pod

=head1 FUNCTIONS

=head2 ComputeFamilyStats

B<Description>: Find best matching families.

B<ArgsCount>: 2

=over 4

=item $results: (hash ref) (R)

Result structure.

=item $family_level: (integer) (O)

Family level of interest.
Default: 0

=back

B<Return>: (hash ref)

Hash of best matching families.

B<Example>:

    ComputeFamilyStats($results, $family_level);

=cut

sub ComputeFamilyStats
{
    my ($results, $family_level) = @_;

    $family_level ||= 0;

    my $dbh = GetDatabaseHandler();
    
    my $matches = $results->{'sequences'};
    my $best_families = {};

    LogDebug("Compute Family Statistics\n");
    foreach my $query_sequence (keys(%$matches))
    {
        # finds matching families using hit sequences...
        LogDebug("$query_sequence matches " . (keys(%{$matches->{$query_sequence}})?join(', ', keys(%{$matches->{$query_sequence}})):'no sequence in BLAST bank') . "\n");
        my $matching_families = {};
        foreach my $hit_accession (keys(%{$matches->{$query_sequence}}))
        {
            my $hit_sequence = Greenphyl::Sequence->new($dbh, {'selectors' => {'accession' => $hit_accession}});
            if ($hit_sequence)
            {
                # store in array all hit sequences
                if ($hit_sequence->families->[$family_level])
                {
                    $matching_families->{$hit_sequence->families->[$family_level]->id} ||= [];
                    push(@{$matching_families->{$hit_sequence->families->[$family_level]->id}}, $hit_sequence);
                    LogDebug("-match: '$hit_sequence' in family '" . $hit_sequence->families->[$family_level]->name . "' (" . $hit_sequence->families->[$family_level]->id . ")\n");
                }
                else
                {
                    $matching_families->{'orphan'} ||= [];
                    push(@{$matching_families->{'orphan'}}, $hit_sequence);
                    LogDebug("-match: '$hit_sequence' with no family\n");
                }
            }
            else
            {
                LogWarning("Sequence '$hit_accession' surprisingly not found in DB!\n");
            }
        }
        LogDebug("\n");

        # find the best family
        my $total_matching_sequences_count = scalar(keys(%{$matches->{$query_sequence}}));
        my $best_family_id    = 0;
        my $best_family_ratio = 0;
        my $best_matching_sequences = [];
        foreach my $family_id (keys(%$matching_families))
        {
            my $matched_sequences = $matching_families->{$family_id};
            my $matching_ratio = scalar(@$matched_sequences)/$total_matching_sequences_count;
            if ($matching_ratio > 0.5)
            {
                $best_family_id    = $family_id;
                $best_family_ratio = $matching_ratio;
                $best_matching_sequences = $matched_sequences;
                last;
            }
            elsif ($matching_ratio > $best_family_ratio)
            {
                $best_family_id    = $family_id;
                $best_family_ratio = $matching_ratio;
                $best_matching_sequences = $matched_sequences;
            }
        }
        
        # store best family for current sequence in $best_families hash
        $best_families->{$best_family_id} ||= [];
        push(
            @{$best_families->{$best_family_id}},
            {
                'accession' => $query_sequence,
                'ratio'     => $best_family_ratio,
                'sequences' => $best_matching_sequences,
            },
        );
    }

    return $best_families;
}


=pod

=head2 ComputeConfidence

B<Description>: Display human-readable BLAST results.

B<ArgsCount>: 1

=over 4

=item $query_sequence_stats: (array ref) (R)

Array of matching sequence statistics.

=back

B<Return>: (integer)

Confidence percent (range 0-100).

B<Example>:

    ComputeConfidence($query_sequence_stats);

=cut

sub ComputeConfidence
{
    my ($query_sequence_stats) = @_;
    my $confidence = 0;
    my $matching_sequence_count = scalar(@$query_sequence_stats);
    
    foreach my $sequence_stats (@$query_sequence_stats)
    {
        $confidence += $sequence_stats->{'ratio'};
    }
    $confidence /= $matching_sequence_count;

    return 100*$confidence;
}


=pod

=head2 DisplayResults

B<Description>: Display human-readable BLAST results.

B<ArgsCount>: 1

=over 4

=item $results: (hash ref) (R)

Result structure.

=back

B<Return>: nothing

B<Example>:

    DisplayResults($results);

=cut

sub DisplayResults
{
    my ($results) = @_;

    my $dbh = GetDatabaseHandler();

    my $sequence_count = scalar(keys(%{$results->{'sequences'}}));
    my $family_stats = ComputeFamilyStats($results);

    # Family statistics
    LogInfo("\nFamily Statistics\n");
#    my $label_ouput_format         = "%-6s %-30.30s %-7s %-5s %-10s %s\n";
#    my $ouput_format               = "%6s %30.30s %7s %5.1f %10.1f %s\n";
#    my $ouput_format_no_confidence = "%6s %30.30s %7s %5.1f %10s %s\n";
    my $label_ouput_format         = "%s\t%s\t%s\t%s\t%s\t%s\n";
    my $ouput_format               = "%s\t%s\t%s\t%f\t%f\t%s\n";
    my $ouput_format_no_confidence = "%s\t%s\t%s\t%f\t%s\t%s\n";
    LogInfo(sprintf($label_ouput_format, 'FamilyID', 'Name', 'Matches', 'Ratio', 'Confidence', 'Sequences'));
    foreach my $family_id (keys(%$family_stats))
    {
        # we don't want to process orphans or unmatched sequences right now
        if ($family_id =~ m/^[1-9]\d*$/)
        {
            my $family = Greenphyl::Family->new($dbh, {'selectors' => {'id' => $family_id}});
            my $matching_sequence_count = scalar(@{$family_stats->{$family_id}});
            my $sequence_list = join(', ', map {$_->{'accession'}} @{$family_stats->{$family_id}});
            if ($family)
            {
                LogInfo(sprintf($ouput_format, $family->accession, $family->name, $matching_sequence_count, 100*$matching_sequence_count/$sequence_count, ComputeConfidence($family_stats->{$family_id}), $sequence_list));
            }
        }
    }

    # check if we got orphan sequences
    if ($family_stats->{'orphan'})
    {
        my $matching_sequence_count = scalar(@{$family_stats->{'orphan'}});
        my $sequence_list = join(', ', map {$_->{'accession'}} @{$family_stats->{'orphan'}});
        LogInfo(sprintf($ouput_format_no_confidence, '-', 'orphans', $matching_sequence_count, 100*$matching_sequence_count/$sequence_count, 'n/a', $sequence_list));
    }

    # check if we got sequences with no match
    if ($family_stats->{0})
    {
        my $matching_sequence_count = scalar(@{$family_stats->{0}});
        my $sequence_list = join(', ', map {$_->{'accession'}} @{$family_stats->{0}});
        LogInfo(sprintf($ouput_format_no_confidence, '-', 'no match', $matching_sequence_count, 100*$matching_sequence_count/$sequence_count, 'n/a', $sequence_list));
    }

    LogInfo("\n");

    # Query sequence best family source sequence infos
    LogInfo("\nQuery sequence: source family sequences\n");
    foreach my $family_id (keys(%$family_stats))
    {
        # we don't want to process orphans or unmatched sequences
        if ($family_id =~ m/^[1-9]\d*$/)
        {
            foreach my $query_sequence (@{$family_stats->{$family_id}})
            {
                LogInfo($query_sequence->{'accession'} . ': ' . join(', ', @{$query_sequence->{'sequences'}}) . "\n");
            }
        }
    }
    
    LogInfo("\n");
}


# Script options
#################

=pod

=head1 OPTIONS

large_scale_match_searches.pl [-help|-man]

large_scale_match_searches.pl [-debug] [db_index=<db_index>]
                              -inputfile <INPUT_FILE>
                              -program <blastall|blastp|blastx>
                              -outputfile <OUTPUT_FILE>

=head2 Parameters

=over 4

=item B<-help>:

Prints a brief help message and exits.

=item B<-man>:

Prints the manual page and exits.

=item B<-debug> (integer):

Executes the script in debug mode.

=item B<db_index> (integer):

Index of the database to use (see Greenphyl::Config).
Default: 0

=item B<-inputfile> (string):

Input multi-FASTA file name (and path).

=item B<-program> (string):

Name of the BLAST program to use. Can only be one of 'blastall', 'blastp' or
'blastx'.

=item B<-outputfile> (string):

BLAST output file name (and path).

=back

=cut


# CODE START
#############

# options processing
my ($man, $help, $debug, $input_filename, $program_name, $output_filename) = (0, 0, 0, '', 'blastall', '');
# parse options and print usage if there is a syntax error.
GetOptions("help|?"         => \$help,
           "man"            => \$man,
           "debug"          => \$debug,
           "inputfile|i=s"  => \$input_filename,
           "program|p=s"    => \$program_name,
           "outputfile|o=s" => \$output_filename,
) or pod2usage(2);
if ($help) {pod2usage(0);}
if ($man) {pod2usage(-verbose => 2);}

# make sure an input file has been specified
if (!-r $input_filename)
{
    LogError("Missing input FASTA file!\n");
    pod2usage(0);
}

# make sure output file does not exist
if (-e $output_filename)
{
    #+FIXME: add prompt to ask user if he wants to re-use current output
    LogWarning("Output file '$output_filename' already exists and will be used! Program won't be launched.\n");
}


# check program
my %PROGRAMS =
(
    'blastall' => 'Greenphyl::Run::Blast',
    'blastp'   => 'Greenphyl::Run::Blast',
    'blastx'   => 'Greenphyl::Run::Blast',    
);

if (!exists($PROGRAMS{$program_name}))
{
    confess LogError("Invalid or unsupported program: '$program_name'");
}

eval
{

    # load program library (dynamic)
    eval "use $PROGRAMS{$program_name};";
    if ($@)
    {
        confess LogError("Unable to load program library: $@");
    }

    # instantiate program
    my $program;
    $program = $PROGRAMS{$program_name}->new($program_name);

    # validate parameters
    $program->validateParameters(@ARGV);

    # validate inputs
    $program->validateInputFiles($input_filename);

    # set output
    $program->setOutputFiles($output_filename);

    if (!-e $output_filename)
    {
        # run
        $program->run();
    }

    # get results
    my $results = $program->getResults();

    # display results
    DisplayResults($results);
};

HandleErrors();

exit(0);

# CODE END
###########


=pod

=head1 AUTHORS

Mathieu ROUARD (Bioversity), m.rouard@cgiar.org

Valentin GUIGNON (Bioversity), v.guignon@cgiar.org

=head1 VERSION

Version 2.0.0

Date 17/07/2013

=head1 SEE ALSO

GreenPhyl documentation.

=cut
