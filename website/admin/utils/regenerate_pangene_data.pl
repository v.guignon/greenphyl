#!/usr/bin/perl

=pod

=head1 NAME

regenerate_pangene_data.pl - Regenerate pangene output files.

=head1 SYNOPSIS

    regenerate_pangene_data.pl

=head1 REQUIRES

Perl5

=head1 DESCRIPTION

=cut

use strict;
use warnings;

use lib "../../lib";
use lib '../../local_lib';
use Carp qw (cluck confess croak);

# use Greenphyl;
# use Greenphyl::Log('nolog' => 1,);
use DBI;

use Getopt::Long;
use Pod::Usage;

use Data::Dumper;




# Script global constants
##########################

=pod

=head1 CONSTANTS

B<$DEBUG>: (boolean)

When set to true, it enables debug mode.

B<$CONSTANT_NAME>: ([constant nature]) #+++

[constant description and use]. #+++

=cut

our $DEBUG = 0;
our $ROW_LIMIT = 1000;
our $MAFFT   = '/srv/projects/greenphyl/dev/pipeline/dependencies/mafft/mafft-7.313-without-extensions/core/mafft';
our $DISTMAT = '/srv/projects/greenphyl/dev/pipeline/dependencies/EMBOSS-6.6.0/emboss/distmat';
our $MAFFT_BINARIES = '/srv/projects/greenphyl/dev/pipeline/dependencies/mafft/mafft-7.313-without-extensions/binaries';
our $RESUME = 1;



# Script global variables
##########################

=pod

=head1 VARIABLES

B<[g_variable_name]>: ([variable nature]) #+++

[variable description, use and default value]. #+++
Default: [variable default value if one] #+++

#--- Example:
#--- B<$g_output_method>: (integer)
#---
#--- used to store current output method;
#--- 0=raw text (default), 1=image, 2=html.
#---
#---     ...
#---
#--- my $g_output_method = 0;
#---

=cut

my $g_dbh;




# Script global functions
##########################

=pod

=head1 FUNCTIONS

=head2 ProcessPangene

B<Description>: [function description]. #+++

B<ArgsCount>: [count of arguments] #+++

=over 4

=item $variable_name: ([variable nature]) ([requirements]) #+++ see below

#--- requirement can be:
#--- (R)=required,
#--- (O)=optional
#--- (U)=optional and must be undef if omitted
[variable description]. #+++

=item $variable_name2: ([variable nature]) ([requirements]) #+++

[variable description]. #+++

=back

B<Return>: ([return type]) #+++

[return description]. #+++

B<Example>:

    [example code] #+++

=cut

sub ProcessPangene
{
    my ($pangene, $group_id, $species_code, $output_dir) = @_;

    # Check missing directory.
    my $fasta_all_filename = $pangene . '_all.faa';
    my $fasta_representative_filename = $pangene . '_representatives.faa';
    $output_dir .= 'pangenomes/';
    if (!-d $output_dir) {
        mkdir $output_dir or confess "Failed to create directory $output_dir\n";
    }
    $output_dir .= $species_code . '/';
    if (!-d $output_dir) {
        mkdir $output_dir or confess "Failed to create directory $output_dir\n";
    }
    $output_dir .= 'consensus/';
    if (!-d $output_dir) {
        mkdir $output_dir or confess "Failed to create directory $output_dir\n";
    }
    $output_dir .= substr($pangene, 0, -3) . '/';
    if (!-d $output_dir) {
        mkdir $output_dir or confess "Failed to create directory $output_dir\n";
    }

    my @genomes = ();
    my $non_representative = 0;

    if (!$RESUME || (!-e $output_dir . $fasta_all_filename) || (!-e $output_dir . $fasta_representative_filename))
    {
        my ($sql_query, $rows, @bind_values);

        $sql_query = "
          SELECT
            fs.type,
            s.accession,
            s.polypeptide,
            s.genome_id
          FROM
            sequences s
              JOIN families_sequences fs ON fs.sequence_id = s.id AND fs.type != 'consensus'
          WHERE
            fs.family_id = ?
          ORDER BY s.genome_id ASC, fs.type DESC, s.accession ASC
          ;"
        ;

        @bind_values = ($group_id);
        $rows = $g_dbh->selectall_arrayref($sql_query, { 'Slice' => {} }, @bind_values);
            
        my ($fh_all, $fh_representative);
        open($fh_all , '>', $output_dir . $fasta_all_filename) or confess "ERROR: Failed to open output file '$output_dir$fasta_all_filename'!\n";
        open($fh_representative , '>', $output_dir . $fasta_representative_filename) or confess "ERROR: Failed to open output file '$output_dir$fasta_representative_filename'!\n";
        
        my $current_genome = 0;
        foreach my $row (@$rows)
        {
            # All sequences.
            print {$fh_all} ">" . $row->{'accession'} . "\n";
            my $polypeptide = $row->{'polypeptide'};
            $polypeptide =~ s/(\w{80})/$1\n/g;
            print {$fh_all} $polypeptide . "\n";
            
            # Work on representatives.
            if ($current_genome != $row->{'genome_id'})
            {
                $current_genome = $row->{'genome_id'};
                push(@genomes, $current_genome);
                print {$fh_representative} ">" . $row->{'accession'} . "\n";
                my $polypeptide = $row->{'polypeptide'};
                $polypeptide =~ s/(\w{80})/$1\n/g;
                print {$fh_representative} $polypeptide . "\n";
            }
            else
            {
                ++$non_representative;
            }
        }

        close $fh_all;
        close $fh_representative;
    }
    
    # Compute files.
    my $cmd;
    my $multigenic_alignment_filename = $pangene . '_multi.aln';
    my $distmat_file = $pangene . '.distmat';
    my $monogenic_alignment_filename = $pangene . '_mono.aln';

    if (!$RESUME || !-e "$output_dir$multigenic_alignment_filename")
    {
        $cmd = "$MAFFT --quiet --auto --preservecase $output_dir$fasta_all_filename >$output_dir$multigenic_alignment_filename 2>$output_dir$multigenic_alignment_filename.log";
        # print "    COMMAND: $cmd\n";
        if (0 != system($cmd))
        {
            if (not $!)
            {
                confess "MAFFT failed. See $output_dir$multigenic_alignment_filename.log\n";
            }
            confess "MAFFT failed: $!\n";
        }
    }

    if (!$RESUME || !-e "$output_dir$distmat_file")
    {
        $cmd = "$DISTMAT -sequence $output_dir$multigenic_alignment_filename  -protmethod 1  -outfile $output_dir$distmat_file";
        # print "    COMMAND: $cmd\n";
        if (0 != system($cmd))
        {
            confess "DISTMAT failed: $!\n";
        }
    }

    if (!$RESUME || !-e "$output_dir$monogenic_alignment_filename")
    {
        if ((1 == @genomes) || (!$non_representative))
        {
            # Same FASTA, just copy alignement.
            $cmd = "cp $output_dir$multigenic_alignment_filename $output_dir$monogenic_alignment_filename";
            # print "    COMMAND: $cmd\n";
            if (0 != system($cmd))
            {
                confess "File copy failed: $!\n";
            }
        }
        else
        {
            $cmd = "$MAFFT --quiet --auto --preservecase $output_dir$fasta_representative_filename >$output_dir$monogenic_alignment_filename 2>$output_dir$monogenic_alignment_filename.log";
            # print "    COMMAND: $cmd\n";
            if (0 != system($cmd))
            {
                if (not $!)
                {
                    confess "2nd MAFFT failed. See $output_dir$monogenic_alignment_filename.log\n";
                }
                confess "MAFFT failed: $!\n";
            }
        }
    }

    # if (1 < @genomes)
    # {
    # }
    # elsif (1 == @genomes)
    # {
    # }
    # else
    # {
    #     confess "ERROR: no sequence in pangene $pangene\n";
    # }

}




# Script options
#################

=pod

=head1 OPTIONS

#--- describes parameters given to the script
#+++ command line syntax
#--- requirement of the option and its parameter can be:
#--- required: name or nature inside <>
#--- optional: name or nature inside []
#--- alternative between 2 elements: elements separated by a |

=head2 Parameters

=over 4

#+++=item B<-help>:
#+++
#+++Prints a brief help message and exits.
#+++
#+++=item B<-man>:
#+++
#+++Prints the manual page and exits.
#+++
#+++=item B<-debug> (integer):
#+++
#+++Executes the script in debug mode. The integer value is optional and is used
#+++to set debug level (use 0 to force disable debug mode).
#+++Default: 0 (not in debug mode).
#+++

=item B<[option_name]> ([option nature]): #+++

[option description]. #+++
Default: [option default value if one] #+++

#--- remove if log not used
#+++=item B<-log>:
#+++
#+++Enable logging.
#+++
#+++=item B<-log-*> (any):
#+++
#+++Logging arguments.

=back
#--- Example:
#---
#--- Template.pl [-help | -man]
#---
#--- Template.pl [-debug [debug_level]] [-size <width> [height]]
#---
#--- =over 4
#---
#--- =item B<-help>:
#---
#--- Prints a brief help message and exits.
#---
#--- =item B<-man>:
#---
#--- Prints the manual page and exits.
#---
#--- =item B<-debug> (integer):
#---
#--- Executes the script in debug mode. If an integer value is specified, it will
#--- be the debug level. If "-debug" option was used without specifying a debug
#--- level, level 1 is assumed.
#--- Default: 0 (not in debug mode).
#---
#---=item B<-size> (positive_real) (positive_real):
#---
#--- Set the dimensions of the object that will be drawn. The first value is
#--- the width; the height is the second value if specified, otherwise it will
#--- assume height and width are equal to the first value.
#--- Default: width and height are set to 1.
#---
#---=back

=cut


# CODE START
#############

# options processing
my ($man, $help, $debug) = (0, 0, undef);
my ($output);

# parse options and print usage if there is a syntax error.
GetOptions('help|?'  => \$help,
           'man'     => \$man,
           'debug:s' => \$debug,
           'o|output=s'  => \$output,
) or pod2usage(1);
if ($help) {pod2usage('-verbose' => 1, '-exitval' => 0);}
if ($man) {pod2usage('-verbose' => 2, '-exitval' => 0);}

# change debug mode if requested/forced
if (defined($debug))
{
    # just '-debug' ?
    if ($debug eq '')
    {
        $debug = 1;
    }
    $DEBUG = $debug;
}
$ENV{"MAFFT_BINARIES"} = $MAFFT_BINARIES; 

# connect
$g_dbh = DBI->connect( "dbi:mysql:database=greenphyl_v5;host=localhost;port=3306", 'greenphyl', 'atlas76');

if (!$g_dbh)
{
    confess "ERROR: Unable to connect to database!\n$DBI::errstr\n";
}

# maximum length of 'long' type fields (LONG, BLOB, CLOB, MEMO, etc.)
$g_dbh->{'LongReadLen'} = 512 * 1024; # 512Kb

# more explicit error reporting
$g_dbh->{'HandleError'} = sub { confess(shift()) };

my ($sql_query, @bind_values, $rows);
if ($output && ($output =~ m/\/$/) && (-d $output))
{
    # Loop on all pangenes.
    $sql_query = "
      SELECT
        s.accession,
        fs.family_id,
        sp.code
      FROM
        sequences s
          JOIN families_sequences fs ON fs.sequence_id = s.id AND fs.type = 'consensus'
          JOIN species sp ON sp.id = s.species_id
      WHERE
        s.type = 'pan protein'
      ORDER BY s.accession
    ";
# AND s.accession = 'brana_pan_p007260'
    my $offset = 0;
    my $rows;
    while (($rows = $g_dbh->selectall_arrayref($sql_query . "  LIMIT $offset, $ROW_LIMIT;", { 'Slice' => {} }))
      && @$rows)
    {
        print "Processing pangenes at offset $offset:\n";
        $offset += $ROW_LIMIT;
        foreach my $row (@$rows)
        {
            print '  ' . $row->{'accession'} . "\n";
            ProcessPangene($row->{'accession'}, $row->{'family_id'}, $row->{'code'}, $output);
        }
    }
    print "Done.\n";
}

exit(0);

# CODE END
###########


=pod

=head1 AUTHORS

Valentin GUIGNON (Bioversity), v.guignon@cgiar.org

=head1 VERSION

Version 1.0.0

Date 02/06/2021

=head1 SEE ALSO

GreenPhyl documentation.

=cut
