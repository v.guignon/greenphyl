#!/usr/bin/env perl

=pod

=head1 NAME

[Script name - short description] #+++
#--- example: welcome.pl - Welcome Message Script

=head1 SYNOPSIS

    #+++ Code of how to call this script

=head1 REQUIRES

#--- Module requierments
#--- example:
[Perl5.004, POSIX, Win32] #+++

=head1 DESCRIPTION

#--- What this script can do, how to use it, ...
[Script description]. #+++

=cut

use strict;
use warnings;

use FindBin;
use lib "$FindBin::Bin/../../lib";
use lib "$FindBin::Bin/../../local_lib";
use Carp qw (cluck confess croak);

use Getopt::Long;
use Pod::Usage;

use Greenphyl;
use Greenphyl::Log('nolog' => 1,);
use Greenphyl::User;
use Greenphyl::CustomFamily;


++$|; #no buffering




# Script global constants
##########################

=pod

=head1 CONSTANTS

B<$REV_STRING>: (string)

Current revision (auto-set by GIT).

B<$DEBUG>: (boolean)

When set to true, it enables debug mode.

B<$CONSTANT_NAME>: ([constant nature]) #+++

[constant description and use]. #+++

=cut

# SVN management part
my $REV_STRING = '$id$';

our $DEBUG = 1;




# Script global variables
##########################

=pod

=head1 VARIABLES

B<[g_variable_name]>: ([variable nature]) #+++

[variable description, use and default value]. #+++
Default: [variable default value if one] #+++

#--- Example:
#--- B<$g_output_method>: (integer)
#---
#--- used to store current output method;
#--- 0=raw text (default), 1=image, 2=html.
#---
#---     ...
#---
#--- my $g_output_method = 0;
#---

=cut

#+++ my [$g_variable_name] = ["value"];




# Script global functions
##########################

=pod

=head1 FUNCTIONS

=head2 [SubName] #+++

B<Description>: [function description]. #+++

B<ArgsCount>: [count of arguments] #+++

=over 4

=item $variable_name: ([variable nature]) ([requirements]) #+++ see below

#--- requirement can be:
#--- (R)=required,
#--- (O)=optional
#--- (U)=optional and must be undef if omitted
[variable description]. #+++

=item $variable_name2: ([variable nature]) ([requirements]) #+++

[variable description]. #+++

=back

B<Return>: ([return type]) #+++

[return description]. #+++

B<Example>:

    [example code] #+++

=cut

#+++sub [SubName] #+++
#+++{
#+++    my ([...]) = @_; #+++ add missing arguments
#--- if needed, parameters check:
#+++    # parameters check
#+++    if (0 != @_)
#+++    {
#+++        confess "usage: SubName();"; #+++
#+++    }
#+++}




# Script options
#################

=pod

=head1 OPTIONS

#--- describes parameters given to the script
#+++ command line syntax
#--- requirement of the option and its parameter can be:
#--- required: name or nature inside <>
#--- optional: name or nature inside []
#--- alternative between 2 elements: elements separated by a |

=head2 Parameters

=over 4

#+++=item B<-help>:
#+++
#+++Prints a brief help message and exits.
#+++
#+++=item B<-man>:
#+++
#+++Prints the manual page and exits.
#+++
#+++=item B<-debug> (integer):
#+++
#+++Executes the script in debug mode. The integer value is optional and is used
#+++to set debug level (use 0 to force disable debug mode).
#+++Default: 0 (not in debug mode).
#+++

=item B<[option_name]> ([option nature]): #+++

[option description]. #+++
Default: [option default value if one] #+++

#--- remove if log not used
#+++=item B<-log>:
#+++
#+++Enable logging.
#+++
#+++=item B<-log-*> (any):
#+++
#+++Logging arguments.

=back
#--- Example:
#---
#--- Template.pl [-help | -man]
#---
#--- Template.pl [-debug [debug_level]] [-size <width> [height]]
#---
#--- =over 4
#---
#--- =item B<-help>:
#---
#--- Prints a brief help message and exits.
#---
#--- =item B<-man>:
#---
#--- Prints the manual page and exits.
#---
#--- =item B<-debug> (integer):
#---
#--- Executes the script in debug mode. If an integer value is specified, it will
#--- be the debug level. If "-debug" option was used without specifying a debug
#--- level, level 1 is assumed.
#--- Default: 0 (not in debug mode).
#---
#---=item B<-size> (positive_real) (positive_real):
#---
#--- Set the dimensions of the object that will be drawn. The first value is
#--- the width; the height is the second value if specified, otherwise it will
#--- assume height and width are equal to the first value.
#--- Default: width and height are set to 1.
#---
#---=back

=cut


# CODE START
#############

# options processing
my ($man, $help, $debug, $no_prompt) = (0, 0, undef, undef);

my @transfer_users;

# parse options and print usage if there is a syntax error.
GetOptions('help|?'     => \$help,
           'man'        => \$man,
           'debug:s'    => \$debug,
           'q|noprompt' => \$no_prompt,
           'user=s'     => \@transfer_users,
           @Greenphyl::Log::LOG_GETOPT, # skip log parameters
) or pod2usage(1);
if ($help) {pod2usage('-verbose' => 1, '-exitval' => 0);}
if ($man) {pod2usage('-verbose' => 2, '-exitval' => 0);}

# change debug mode if requested/forced
if (defined($debug))
{
    # just '-debug' ?
    if ($debug eq '')
    {
        $debug = 1;
    }
    $DEBUG = $debug;
}


eval
{
    my $prod_db_name = 'greenphyl_test_val'; # candidate production DB
    my $dev_db_name = 'greenphyl_dev';
    my $prod_dbh =  GetDatabaseHandler($prod_db_name);
    my $dev_dbh  =  GetDatabaseHandler($dev_db_name);
    
    # start new transaction
    if ($DEBUG)
    {
        LogWarning('Running in DEBUG mode, no SQL transaction will be saved!');
    }
    $prod_dbh->begin_work() or croak $prod_dbh->errstr;

    my $sql_query = "
        UPDATE sequences s, $dev_db_name.sequences sdev SET polypeptide = 
    ";


    # 1) transfer users
    # get old and new users from dev
    my $sql_query = "
        SELECT " . join(', ', Greenphyl::User->GetDefaultMembers('du')) . ",
            NOT EXISTS (SELECT TRUE FROM $prod_db_name.users pu WHERE pu.name = du.name LIMIT 1) AS new
        FROM users du
    ";
    my $sql_dev_users = $dev_dbh->selectall_arrayref($sql_query, { 'Slice' => {} });
    
    my $user_transfer_data = {};
    my %user_name_to_id;
    foreach my $sql_dev_user (@$sql_dev_users)
    {
        $user_name_to_id{$sql_dev_user->{'name'}} = $sql_dev_user->{'id'};

        if (!$sql_dev_user->{'new'})
        {
            # get associated prod user ID
            $user_transfer_data->{$sql_dev_user->{'id'}} = Greenphyl::User->new($prod_dbh, {'selectors' => {'name' => $sql_dev_user->{'name'},}});
        }
        elsif (grep {$sql_dev_user->{'name'}} @transfer_users)
        {
            # new user that should be transfered, add user to prod
            # remove dev id
            my $dev_user_id = delete($sql_dev_user->{'id'});
    
            # save it in prod
            my $new_user = Greenphyl::User->new($prod_dbh, {'load' => 'none', 'members' => $sql_dev_user});
            $new_user->save();
    
            # store association
            $user_transfer_data->{$dev_user_id} = $new_user;
            LogDebug("Transfered dev user id $dev_user_id to prod user id " . $new_user->id);
        }
        else
        {
            LogDebug("Skipping user " . $sql_dev_user->{'name'});
        }
    }
    
    # Note: we don't transfer user openids as they are server-name specific
    
    # transfer user data
    my $csequence_transfer_data = {};
    my $cfamily_transfer_data = {};
    foreach my $user_to_transfer (@transfer_users)
    {
        my $dev_user_id = $user_name_to_id{$user_to_transfer};
        my $prod_user_id = $user_transfer_data->{$dev_user_id}->id;
        LogDebug("Transfering data of user $user_to_transfer ($dev_user_id to $prod_user_id)");

        # 3) transfer Custom Sequence data (custom_sequences)
        $sql_query = "
            SELECT " . join(', ', Greenphyl::CustomSequence->GetDefaultMembers('ds')) . ",
                NOT EXISTS (SELECT TRUE FROM $prod_db_name.custom_sequences ps WHERE ps.accession = ds.accession AND ps.user_id = $prod_user_id LIMIT 1) AS new
            FROM custom_sequences ds
            WHERE
                user_id = $dev_user_id
        ";
        my $sql_dev_csequences = $dev_dbh->selectall_arrayref($sql_query, { 'Slice' => {} });

        foreach my $sql_dev_csequence (@$sql_dev_csequences)
        {
            if (!$sql_dev_csequence->{'new'})
            {
                # get associated prod sequence ID
                $csequence_transfer_data->{$sql_dev_csequence->{'id'}} = Greenphyl::CustomSequence->new($prod_dbh, {'selectors' => {'accession' => $sql_dev_csequence->{'accession'}, 'user_id' => $prod_user_id, }});
            }
            else
            {
                # add new sequence to prod
                # remove dev id
                my $dev_csequence_id = delete($sql_dev_csequence->{'id'});
                # update owner id
                $sql_dev_csequence->{'user_id'} = $prod_user_id;
        
                # save it in prod
                my $new_csequence = Greenphyl::CustomSequence->new($prod_dbh, {'load' => 'none', 'members' => $sql_dev_csequence});
                $new_csequence->save();
        
                # store association
                $csequence_transfer_data->{$dev_csequence_id} = $new_csequence;
                LogDebug("Transfered dev custom sequence id $dev_csequence_id to prod custom sequence id " . $new_csequence->id);
            }
        }

        # custom_sequences_sequences_relationships
        $sql_query = "
            SELECT (cssr.custom_sequence_id, cssr.sequence_id, cssr.type)
            FROM custom_sequences_sequences_relationships cssr
                JOIN custom_sequences cs ON cs.id = cssr.custom_sequence_id
            WHERE
                cs.user_id = $dev_user_id
        ";
        my $sql_dev_cssrs = $dev_dbh->selectall_arrayref($sql_query, { 'Slice' => {} });

        foreach my $sql_dev_cssr (@$sql_dev_cssrs)
        {
            $sql_query = "
                INSERT IGNORE INTO custom_sequences_sequences_relationships
                    (custom_sequence_id, sequence_id, type)
                VALUES
                    (?, ?, ?)
            ";
            $prod_dbh->do($sql_query, undef, $csequence_transfer_data->{$sql_dev_cssr->{'custom_sequence_id'}}->id, $sql_dev_cssr->{'sequence_id'}, $sql_dev_cssr->{'type'})
                or cluck LogWarning("Failed to add custom sequence - sequence relationship (" . $csequence_transfer_data->{$sql_dev_cssr->{'custom_sequence_id'}}->id . " - $sql_dev_cssr->{'sequence_id'})");
        }


        # 4) transfer Custom Family data (custom_families)
        $sql_query = "
            SELECT " . join(', ', Greenphyl::CustomFamily->GetDefaultMembers('df')) . ",
                NOT EXISTS (SELECT TRUE FROM $prod_db_name.custom_families pf WHERE pf.accession = df.accession AND pf.user_id = $prod_user_id LIMIT 1) AS new
            FROM custom_families df
            WHERE
                user_id = $dev_user_id
        ";
        my $sql_dev_cfamilies = $dev_dbh->selectall_arrayref($sql_query, { 'Slice' => {} });

        foreach my $sql_dev_cfamily (@$sql_dev_cfamilies)
        {
            if (!$sql_dev_cfamily->{'new'})
            {
                # get associated prod family ID
                $cfamily_transfer_data->{$sql_dev_cfamily->{'id'}} = Greenphyl::CustomFamily->new($prod_dbh, {'selectors' => {'accession' => $sql_dev_cfamily->{'accession'}, 'user_id' => $prod_user_id, }});
            }
            else
            {
                # add new family to prod
                # remove dev id
                my $dev_cfamily_id = delete($sql_dev_cfamily->{'id'});
                # update owner id
                $sql_dev_cfamily->{'user_id'} = $prod_user_id;
        
                # save it in prod
                my $new_cfamily = Greenphyl::CustomFamily->new($prod_dbh, {'load' => 'none', 'members' => $sql_dev_cfamily});
                $new_cfamily->save();
        
                # store association
                $cfamily_transfer_data->{$dev_cfamily_id} = $new_cfamily;
                LogDebug("Transfered dev custom family id $dev_cfamily_id to prod custom family id " . $new_cfamily->id);
            }
        }


# custom_families_families_relationships
# custom_family_relationships
# custom_families_sequences

        # # X) transfer My List data (my_lists, my_list_items)
        # # clear dev my_list data
        # $sql_query = "DELETE FROM my_lists;";
        # $dev_dbh->do($sql_query)
        #     or confess LogError("Failed clear my_lists: " . $dev_dbh->errstr);
        # 
        # $sql_query = "
        #     SELECT *
        #     FROM my_lists
        # ";
        # my $prod_my_lists = $prod_dbh->selectall_arrayref($sql_query, { 'Slice' => {} });
        # foreach my $prod_my_list (@$prod_my_lists)
        # {
        #     my @values = (
        #         $prod_my_list->{'id'},
        #         $prod_my_list->{'name'},
        #         $prod_my_list->{'description'},
        #         $user_transfer_data->{$prod_my_list->{'user_id'}}->id,
        #     );
        #     $sql_query = "
        #         INSERT INTO my_lists (id, name, description, user_id)
        #         VALUES (?, ?, ?, ?);
        #     ";
        #     LogDebug("SQL Query: $sql_query\nBindings: " . join(', ', @values));
        #     $dev_dbh->do($sql_query, undef, @values)
        #         or confess LogError("Failed to add new my_lists: " . $dev_dbh->errstr);
        # }
        # 
        # $sql_query = "
        #     SELECT *
        #     FROM my_list_items
        # ";
        # my $prod_my_list_items = $prod_dbh->selectall_arrayref($sql_query, { 'Slice' => {} });
        # foreach my $prod_my_list_item (@$prod_my_list_items)
        # {
        #     my $foreign_id = $prod_my_list_item->{'foreign_id'};
        #     if ($prod_my_list_item->{'type'} eq 'CustomFamily')
        #     {
        #         $foreign_id = $cfamily_transfer_data->{$prod_my_list_item->{'foreign_id'}}->id;
        #     }
        #     elsif ($prod_my_list_item->{'type'} eq 'CustomSequence')
        #     {
        #         $foreign_id = $csequence_transfer_data->{$prod_my_list_item->{'foreign_id'}}->id;
        #     }        
        #     
        #     my @values = (
        #         $prod_my_list_item->{'my_list_id'},
        #         $foreign_id,
        #         $prod_my_list_item->{'type'},
        #     );
        # 
        #     $sql_query = "
        #         INSERT INTO my_list_items (my_list_id, foreign_id, type)
        #         VALUES (?, ?, ?, ?);
        #     ";
        #     LogDebug("SQL Query: $sql_query\nBindings: " . join(', ', @values));
        #     $dev_dbh->do($sql_query, undef, @values)
        #         or confess LogError("Failed to add new my_lists: " . $dev_dbh->errstr);
        # }
    }
    
    
    SaveCurrentTransaction($prod_dbh);

};

# catch
HandleErrors();

exit(0);

__END__
# CODE END
###########

=pod

=head1 AUTHORS

Mathieu ROUARD (Bioversity), m.rouard@cgiar.org

Valentin GUIGNON (Bioversity), v.guignon@cgiar.org

Matthieu CONTE (Syngenta)

Sebastien BRIOIS (Syngenta)

=head1 VERSION

Version 1.0.0

Date 03/04/2014

=head1 SEE ALSO

GreenPhyl documentation.

=cut
