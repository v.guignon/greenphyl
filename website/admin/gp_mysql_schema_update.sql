/*
-- -----------------------------------------------------------------------------
-- Script SQL de mise à jour du schéma GreenPhyl.
--
-- Ce script peut être exécuté de bout en bout pour mettre à jour la structure
-- d'une base de données pour qu'elle corresponde à la version actuelle du site
-- et des scripts.
--
-- Le processus est encapsulé dans une transaction ce qui permet d'annuler toute
-- modification en cas de problème rencontré lors de la mise à jour.
--
-- Les anciennes modifications sont laissées en commentaires au cas où on
-- utiliserait d'anciennes versions de base de données.
--
-- Ce script peut être appelé depuis un interpréteur MySQL en faisant:
-- >source gp_mysql_schema_update.sql
--
-- RAPPELS:
-- UNSIGNED TINYINT:    3 digits max (0-255)
-- UNSIGNED SMALLINT:   5 digits max (0-65535)
-- UNSIGNED MEDIUMINT:  8 digits max (0-16777215)
-- UNSIGNED INT:       10 digits max (0-4294967295)
-- UNSIGNED BIGINT:    20 digits max (0-18446744073709551613)
--
-- 
-- Notes sur les types SET:
-- 
-- -pour sélectionner:
-- SELECT inferences FROM families LIMIT 1;
--   retourne une seule chaine de caractère avec les noms des inférences
--   concaténées et séparées par des virgules.
-- Pour obtenir la valeur numérique* du set:
--   SELECT inferences+0 FROM families LIMIT 1;
-- 
-- -Pour tester:
-- 
--   # tout ce qui est 'IEA:InterPro' et éventuellement autre chose (les 2
--   # méthodes sont équivalentes):
--   SELECT * FROM families WHERE inferences LIKE '%IEA:InterPro%';
--   SELECT * FROM families WHERE inferences & 2; # plus efficace
--   
--   # tout ce qui est 'IEA:InterPro' OU 'IEA:PIRSF' et éventuellement autre
--   # chose (les 2 méthodes sont équivalentes):
--   SELECT * FROM families WHERE (inferences LIKE '%IEA:InterPro%' OR inferences LIKE '%IEA:PIRSF%');
--   SELECT * FROM families WHERE inferences & 6; # beaucoup plus efficace
-- 
--   # tout ce qui est 'IEA:InterPro' ET 'IEA:PIRSF' et éventuellement autre
--   # chose (les 2 méthodes sont équivalentes):
--   SELECT * FROM families WHERE (inferences LIKE '%IEA:InterPro%' AND inferences LIKE '%IEA:PIRSF%');
--   SELECT * FROM families WHERE (inferences & 6) = 6; # beaucoup plus efficace
-- 
--   # tout ce qui est exclusivement 'IEA:InterPro' ET 'IEA:PIRSF' et rien
--   # d'autre (les 2 méthodes sont équivalentes):
--   SELECT * FROM families WHERE inferences = 'IEA:InterPro%IEA:PIRSF'; # l'ordre est important
--   SELECT * FROM families WHERE inferences = 6; # plus efficace
-- 
-- -Pour insérer:
-- INSERT INTO families(inferences) VALUES('IEA:InterPro,IEA:PIRSF') WHERE id = 42;
-- INSERT INTO families(inferences) VALUES(6) WHERE id = 42;
-- 
-- -Pour udpater:
--   # remplace par les nouvelles valeurs:
--   UPDATE families SET inferences = 'IEA:InterPro,IEA:PIRSF' WHERE id = 42;
--   UPDATE families SET inferences = 2 WHERE id = 42;
-- 
--   # ajouter une nouvelle valeur (2 méthodes équivalentes):
--   UPDATE families SET inferences = CONCAT(inferences,",IEA:InterPro") WHERE id = 42;
--   UPDATE families SET inferences = inferences | 2 WHERE id = 42; # plus efficace
-- 
--   # idem mais avec plusieurs valeurs (2 méthodes équivalentes):
--   UPDATE families SET inferences = CONCAT_WS(',',inferences,'IEA:InterPro','IEA:PIRSF') WHERE id = 42;
--   UPDATE families SET inferences = inferences | 6 WHERE id = 42; # plus efficace
-- 
--   # pour retirer un élément du set (2 méthodes équivalentes):
--   UPDATE families SET inferences = REPLACE(inferences,'IEA:InterPro','') WHERE id = 42;
--   UPDATE families SET inferences = inferences & ~2 WHERE id = 42; # plus efficace
--   # explication: inferences = "toutes les valeurs de" inferences "qui sont egales a" "tout ce qui n'est pas" 2
-- 
-- 
-- *nb.: on peut aussi utiliser un entier mais attention: il ne correspondra
--   pas à l'indexe d'une valeur mais à la combinaison des valeurs binaires
--   associées!
-- Ainsi dans SET('Other', 'IEA:InterPro', 'IEA:PIRSF', 'IEA:UniProtKB', ...)
--   nous avons:
--   'Other'         = 0b0001 = 1;
--   'IEA:InterPro'  = 0b0010 = 2;
--   'IEA:PIRSF'     = 0b0100 = 4;
--   'IEA:UniProtKB' = 0b1000 = 8;
--   etc., par puissances de 2.
--   Donc:
--     'IEA:InterPro,IEA:PIRSF' sera équivalent à 2+4=6
--     'IEA:InterPro,IEA:UniProtKB' sera équivalent à 2+8=10
--     'IEA:InterPro,IEA:PIRSF,IEA:UniProtKB' sera équivalent à 2+4+8=14
--   Rappel sur les opérateurs binaires:
--     &=ET binaire
--     |=OU binaire
--     ~=NOT binaire
--     ^=OU EXCLUSIF (XOR) binaire (nb.: rien à voir avec le shérif de l'espace ;) )
-- 
-- Plus de détails: ftp://ftp.itb.ac.id/pub/mysql/tech-resources/articles/mysql-set-datatype.html
--     
-- -----------------------------------------------------------------------------
*/

-- start with no error
SET @error_count := 0;

-- to have a faster INFORMATION_SCHEMA table
SET GLOBAL innodb_stats_on_metadata=0;

-- -----------------------------------------------------------------------------
-- PROCEDURES
--
DELIMITER $$

-- Select what updates should be made
DROP PROCEDURE IF EXISTS updateSchemaTemp$$
CREATE PROCEDURE updateSchemaTemp()
  BEGIN
    DECLARE schema_version VARCHAR(15);
    -- handler for "table does not exist"
    -- DECLARE CONTINUE HANDLER FOR SQLSTATE '42S02' SET schema_version := NULL;

    SELECT variables.value INTO schema_version FROM variables WHERE name = 'schema_version';

    IF (schema_version IS NULL) THEN
      SELECT CONCAT('WARNING: unable to detect schema version ') AS 'Message';
    ELSE
      SELECT CONCAT('Detected schema version ', schema_version) AS 'Message';
    END IF;

    -- note: when adding new update procedures, do not forget to add a
    --       corresponding drop statement at the end of this file!
    CASE schema_version
      WHEN '2.0' THEN
        BEGIN
          CALL updateFrom20To30();
        END;
      WHEN '3.0' THEN
        BEGIN
          SELECT 'Schema is up to date.' AS 'Message';
        END;
      ELSE
        BEGIN
          IF (schema_version IS NULL) THEN
            CALL updateFromNullTo20();
          ELSE
            SELECT 'Unsupported schema version: requires manual update.' AS 'Message';
          END IF;
        END;
    END CASE;
  END;
$$

-- -----------------------------------------------------------------------------
-- BEGIN OF OLD STUFF
-- --
-- -- change limitations
-- -- table sequences
-- --
--
-- ALTER TABLE sequences
--   MODIFY COLUMN seq_textid varchar(30) CHARACTER SET latin1 NOT NULL DEFAULT '';
--
-- -- --------------------------------------------------------
--
-- --
-- -- rename in table species
-- -- 'path_institut' et 'path_fasta' et ajout de nouvelles colonnes
-- --
--
-- ALTER TABLE species
--   CHANGE COLUMN path_institut url_institute varchar(100) CHARACTER SET latin1 NOT NULL,
--   CHANGE COLUMN path_fasta url_fasta varchar(1000) CHARACTER SET latin1 NOT NULL,
--   ADD COLUMN url_picture varchar(200) CHARACTER SET latin1 NOT NULL AFTER url_fasta,
--   ADD COLUMN version_notes TEXT NOT NULL,
--   ADD COLUMN chromosome_number smallint(6) NULL DEFAULT NULL,
--   ADD COLUMN genome_size float NULL DEFAULT NULL;
--
-- -- UPDATE species SET url_picture = CONCAT('img/species/', LOWER(species_name), '.png');
--
-- -- --------------------------------------------------------
--
-- --
-- -- rename in table sequences for column
-- -- 'Alias' en 'alias'
-- --
--
-- ALTER TABLE sequences
--   CHANGE Alias alias VARCHAR(255) CHARACTER SET utf8 NULL DEFAULT NULL COMMENT 'Uniprot gene name';
--
-- -- --------------------------------------------------------
--
-- --
-- -- replacement of tables "scores" et "orthologs"
-- --
--
-- -- 1) new tables:
-- -- -- if the table already exists, need to change column 'duplication' and change homology_id type
-- -- ALTER TABLE homologs DROP FOREIGN KEY homologs_ibfk_1;
-- -- ALTER TABLE homologs
-- --     MODIFY COLUMN homology_id int(10);
-- -- ALTER TABLE homologies
-- --     MODIFY COLUMN homology_id int(10) NOT NULL AUTO_INCREMENT COMMENT 'homologs homology id',
-- --     DROP COLUMN duplication,
-- --     ADD COLUMN i_duplication int NOT NULL,
-- --     ADD COLUMN t_duplication int NOT NULL;
--
-- -- create table homologies
-- CREATE TABLE IF NOT EXISTS homologies (
--   homology_id   int(10) NOT NULL AUTO_INCREMENT COMMENT 'homologs homology id',
--   family_id     mediumint(10) unsigned NOT NULL,
--   type          ENUM ('orthology', 'super-orthology', 'ultra-paralogy') NOT NULL,
--   score         float     NULL,
--   kvalue        int       NOT NULL,
--   distance      float     NOT NULL,
--   speciation    int       NOT NULL,
--   i_duplication int       NOT NULL,
--   t_duplication int       NOT NULL,
--   PRIMARY KEY (homology_id),
--   KEY type (type)
-- ) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Contains scores of orthology, subtree neighbour, etc.';
--
-- -- create table homologs
-- CREATE TABLE IF NOT EXISTS homologs (
--   homology_id int(10) NOT NULL,
--   seq_id      mediumint(10) unsigned NOT NULL,
--   PRIMARY KEY (homology_id, seq_id)
-- ) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Contains homology relationships';
--
-- -- constraints
-- ALTER TABLE homologs
--   ADD CONSTRAINT FOREIGN KEY (homology_id) REFERENCES homologies (homology_id) ON DELETE CASCADE ON UPDATE CASCADE;
-- ALTER TABLE homologs
--   ADD CONSTRAINT FOREIGN KEY (seq_id) REFERENCES sequences (seq_id) ON DELETE CASCADE ON UPDATE CASCADE;
--
-- ALTER TABLE homologies
--   ADD CONSTRAINT FOREIGN KEY (family_id) REFERENCES family (family_id) ON DELETE CASCADE ON UPDATE CASCADE;
--
-- -- 2) Fill tables with data contained in obsolete tables
-- -- *if the database contained a  table 'scores':
-- --ALTER TABLE homologies
-- --    ADD CONSTRAINT UNIQUE temp_constraint (type, speciation);
-- --INSERT IGNORE INTO homologies (family_id, type, score, kvalue, distance, speciation, duplication)
-- --    SELECT sii.family_id, 'orthology', sc.score, 0, 0., least(sc.query_id, sc.subject_id), greatest(sc.query_id, sc.subject_id)
-- --    FROM scores sc JOIN seq_is_in sii ON sii.seq_id = sc.query_id
-- --    WHERE type_id = 1;
-- -- INSERT INTO homologs SELECT homology_id, speciation FROM homologies;
-- -- INSERT INTO homologs SELECT homology_id, duplication FROM homologies;
-- -- ALTER TABLE homologies DROP KEY temp_constraint;
-- -- UPDATE homologies SET speciation = 0, duplication  = 0;
--
-- -- * if the database contained a table 'orthologs':
-- -- ALTER TABLE homologies
-- --   ADD COLUMN query mediumint(10) NOT NULL,
-- --   ADD COLUMN hit mediumint(10) NOT NULL,
-- --   ADD CONSTRAINT UNIQUE temp_constraint2 (query, hit);
-- -- ALTER TABLE homologies ADD INDEX ( query );
-- -- ALTER TABLE homologies ADD INDEX ( hit );
-- -- INSERT IGNORE INTO homologies (family_id, type, score, kvalue, distance, speciation, query, hit)
-- --     SELECT sii.family_id, 'orthology', 0, 0, ot.distance, ot.speciation, least(ot.query, ot.hit), greatest(ot.query, ot.hit)
-- --     FROM orthologs ot JOIN seq_is_in sii ON sii.seq_id = ot.query
-- --     WHERE type = 1;
-- -- INSERT IGNORE INTO homologies (family_id, type, score, kvalue, distance, speciation, query, hit)
-- --     SELECT sii.family_id, 'ultra-paralogy', 0, 0, ot.distance, ot.speciation, least(ot.query, ot.hit), greatest(ot.query, ot.hit)
-- --     FROM orthologs ot JOIN seq_is_in sii ON sii.seq_id = ot.query
-- --     WHERE type = 2;
-- -- INSERT IGNORE INTO homologs SELECT homology_id, query FROM homologies;
-- -- INSERT IGNORE INTO homologs SELECT homology_id, hit FROM homologies;
-- -- ALTER TABLE homologies DROP KEY temp_constraint2
-- --     DROP COLUMN query,
-- --     DROP COLUMN hit;
--
-- -- --------------------------------------------------------
--
-- --
-- -- add table parameters
-- --
--
-- CREATE TABLE IF NOT EXISTS variables (
--   name varchar(128) CHARACTER SET latin1 NOT NULL,
--   value longtext CHARACTER SET latin1 NOT NULL DEFAULT '',
--   PRIMARY KEY (name)
-- ) ENGINE=InnoDB DEFAULT CHARSET=utf8;
--
-- -- --------------------------------------------------------
--
-- --
-- -- add table sequences_history
-- --
--
-- CREATE TABLE IF NOT EXISTS sequences_history (
--   seq_id mediumint(10) UNSIGNED NOT NULL,
--   old_seq_textid varchar(30) CHARACTER SET utf8 NOT NULL,
--   new_seq_textid varchar(30) CHARACTER SET utf8 NOT NULL,
--   transaction_date TIMESTAMP NULL DEFAULT NULL,
--   INDEX (seq_id)
-- ) ENGINE=InnoDB DEFAULT CHARSET=utf8;
--
-- -- --------------------------------------------------------
--
-- --
-- -- add table transfered_families
-- --
--
-- CREATE TABLE IF NOT EXISTS transfered_families (
--   old_family_id mediumint(10) unsigned NOT NULL,
--   new_family_id mediumint(10) unsigned NOT NULL,
--   transfer_date TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
--   PRIMARY KEY (old_family_id)
-- ) ENGINE=InnoDB  DEFAULT CHARSET=utf8;
--
-- -- --------------------------------------------------------
--
-- --
-- -- add table family_history
-- --
--
-- CREATE TABLE IF NOT EXISTS family_history (
--   old_family_id           mediumint(10) unsigned NOT NULL,
--   new_family_id           mediumint(10) unsigned NOT NULL,
--   transaction_date        timestamp              NULL     DEFAULT NULL,
--   transaction_type        ENUM('none', 'annotation_transfer') NOT NULL,
--   sequences_in_old_family int                    NOT NULL,
--   sequences_in_new_family int                    NOT NULL,
--   additional_info         longtext               NOT NULL DEFAULT '',
--   INDEX (new_family_id),
--   INDEX (old_family_id)
-- ) ENGINE=InnoDB  DEFAULT CHARSET=utf8;
--
-- ALTER TABLE family_history
--   ADD CONSTRAINT FOREIGN KEY (new_family_id) REFERENCES family (family_id) ON DELETE CASCADE ON UPDATE CASCADE;
--
-- -- --------------------------------------------------------
--
-- --
-- -- add management of gene family specificity
-- --
--
-- ALTER TABLE family
--   CHANGE plant_spe plant_specific int(1) DEFAULT NULL COMMENT '0=not plant-specific; 1=plant-specific.',
--   CHANGE node_id   tax_id         int(5) unsigned NULL DEFAULT NULL COMMENT 'allow to group/phylum',
--   DROP COLUMN species_id;
--
-- --
-- -- ajout des infos statistiques
-- --
--
-- ALTER TABLE family
--   ADD COLUMN min_seq_length      INT UNSIGNED NOT NULL DEFAULT 0,
--   ADD COLUMN max_seq_length      INT UNSIGNED NOT NULL DEFAULT 0,
--   ADD COLUMN average_seq_length  INT UNSIGNED NOT NULL DEFAULT 0,
--   ADD COLUMN median_seq_length   INT UNSIGNED NOT NULL DEFAULT 0,
--   ADD COLUMN alignment_length    INT UNSIGNED NOT NULL DEFAULT 0,
--   ADD COLUMN seq_count           INT UNSIGNED NOT NULL DEFAULT 0,
--   ADD COLUMN alignment_seq_count INT UNSIGNED NOT NULL DEFAULT 0;
--
-- -- --------------------------------------------------------
--
-- --
-- -- change the way phylogeny nodes are handled
-- --
--
-- DROP TABLE phylonode_species;
--
-- CREATE TABLE IF NOT EXISTS phylonode_synonym (
--   tax_id        int(5) unsigned NOT NULL DEFAULT 0,
--   name          varchar(30) CHARACTER SET latin1 NOT NULL,
--   synonym_order tinyint(3) unsigned NOT NULL COMMENT 'Lower values for higher priorities',
--   INDEX  (tax_id)
-- ) ENGINE=InnoDB  DEFAULT CHARSET=utf8;
--
-- TRUNCATE phylonode;
--
-- ALTER TABLE phylonode
--   DROP COLUMN tax_id;
--
-- ALTER TABLE phylonode
--   CHANGE node_id    tax_id          int(5) unsigned NOT NULL DEFAULT 0,
--   CHANGE name       scientific_name varchar(30) CHARACTER SET latin1 NOT NULL DEFAULT '',
--   CHANGE other_name rank            varchar(30) CHARACTER SET latin1 NOT NULL DEFAULT '',
--   ADD COLUMN        division        varchar(30) CHARACTER SET latin1 NOT NULL DEFAULT '',
--   ADD COLUMN        parent_node     int(5) unsigned NULL DEFAULT NULL,
--   ADD COLUMN        level           smallint(3) unsigned NOT NULL;
--
-- ALTER TABLE phylonode
--   ADD CONSTRAINT UNIQUE unique_name (scientific_name),
--   ADD INDEX parent_node (parent_node),
--   ADD CONSTRAINT FOREIGN KEY (parent_node) REFERENCES phylonode (tax_id) ON DELETE CASCADE ON UPDATE CASCADE;
--
--
-- ALTER TABLE species
--   CHANGE tax_id tax_id    int(5) unsigned NULL DEFAULT NULL;
--
-- INSERT IGNORE INTO phylonode (tax_id, scientific_name) SELECT sp.tax_id, sp.organism FROM species sp;
--
-- UPDATE family SET tax_id = NULL;
--
-- ALTER TABLE family
--   ADD CONSTRAINT FOREIGN KEY (tax_id) REFERENCES phylonode (tax_id);
--
-- CREATE TABLE IF NOT EXISTS lineage (
--   tax_id         int(5) unsigned NOT NULL DEFAULT 0,
--   lineage_tax_id int(5) unsigned NOT NULL DEFAULT 0,
--   INDEX  (tax_id)
-- ) ENGINE=InnoDB  DEFAULT CHARSET=utf8;
--
-- ALTER TABLE lineage
--   ADD CONSTRAINT FOREIGN KEY (tax_id) REFERENCES phylonode (tax_id),
--   ADD CONSTRAINT FOREIGN KEY (lineage_tax_id) REFERENCES phylonode (tax_id),
--   ADD CONSTRAINT UNIQUE unique_lineage (tax_id, lineage_tax_id);
--
--
-- -- ERROR 1424 (HY000): Recursive stored functions and triggers are not allowed.
-- ---- ex.: SELECT getLineage(3649);
-- --
-- --DROP FUNCTION IF EXISTS getLineage;
-- --delimiter //
-- --CREATE FUNCTION getLineage (in_tax_id INT)
-- --RETURNS TEXT
-- --READS SQL DATA
-- --BEGIN
-- --    DECLARE parent_node_id INT;
-- --    SELECT parent_node INTO parent_node_id    FROM phylonode WHERE tax_id = in_tax_id;
-- --    IF parent_node_id IS NULL THEN
-- --        RETURN CONCAT(parent_node_id);
-- --    ELSE
-- --        RETURN CONCAT(getLineage(parent_node_id), ' ', parent_node_id);
-- --    END IF;
-- --END//
-- --delimiter ;
--
-- -- --------------------------------------------------------
--
-- --
-- -- Procedure updateFamilyStats
-- --
--
-- DROP PROCEDURE IF EXISTS updateFamilyStats;
-- delimiter //
-- CREATE PROCEDURE updateFamilyStats(IN v_family_id MEDIUMINT)
-- BEGIN
--   DECLARE l_min_seq_length         INT;
--   DECLARE l_max_seq_length         INT;
--   DECLARE l_average_seq_length     INT;
--   DECLARE l_median_seq_length      INT;
--   DECLARE l_seq_count              INT;
--
--   SELECT
--     min(s.seq_length),
--     max(s.seq_length),
--     avg(s.seq_length),
--     count(s.seq_id)
--   INTO
--     l_min_seq_length,
--     l_max_seq_length,
--     l_average_seq_length,
--     l_seq_count
--   FROM family f
--       JOIN seq_is_in sii USING(family_id)
--       JOIN sequences s USING(seq_id)
--   WHERE family_id = v_family_id;
--
--   SET @sql = CONCAT(" SELECT s.seq_length
--   INTO @v_median_low_seq_length
--   FROM seq_is_in sii
--       JOIN sequences s USING(seq_id)
--   WHERE family_id = ", v_family_id, "
--   ORDER BY s.seq_length ASC
--   LIMIT ", CAST((v_seq_count+1)/2 - 1 AS UNSIGNED INTEGER), ", 1");
--   PREPARE STMT FROM @sql;
--   EXECUTE STMT;
--
--   SET @sql = CONCAT(" SELECT s.seq_length
--   INTO @v_median_high_seq_length
--   FROM seq_is_in sii
--       JOIN sequences s USING(seq_id)
--   WHERE family_id = ", v_family_id, "
--   ORDER BY s.seq_length ASC
--   LIMIT ", CAST(v_seq_count/2 AS UNSIGNED INTEGER), ", 1");
--   PREPARE STMT2 FROM @sql;
--   EXECUTE STMT2;
--
--   SET l_median_seq_length := ((@v_median_low_seq_length + @v_median_high_seq_length)/2. + 0.5);
--
--   UPDATE family
--   SET
--     min_seq_length     = l_min_seq_length,
--     max_seq_length     = l_max_seq_length,
--     average_seq_length = l_average_seq_length,
--     median_seq_length  = l_median_seq_length,
--     seq_count          = l_seq_count
--   WHERE family_id = v_family_id;
--
-- END//
-- delimiter ;
--
--
-- DROP PROCEDURE IF EXISTS updateFamiliesStats;
-- delimiter //
-- CREATE PROCEDURE updateFamiliesStats()
-- BEGIN
--   DECLARE l_family_id MEDIUMINT;
--   DECLARE done INT DEFAULT 0;
--   DECLARE family_cursor CURSOR FOR SELECT family_id FROM family WHERE black_list = 0;
--   DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = 1;
--
--   OPEN family_cursor;
--
--   family_update_loop: LOOP
--     FETCH family_cursor INTO l_family_id;
--     IF done THEN
--       LEAVE family_update_loop;
--     END IF;
--     CALL updateFamilyStats(v_family_id);
--   END LOOP;
--
--   CLOSE family_cursor;
--
--   INSERT INTO variables
--     SELECT 'last_families_stats_update', NOW()
--     ON DUPLICATE KEY UPDATE
--         value = NOW();
-- END//
-- delimiter ;
--
-- -- --------------------------------------------------------
--
-- ALTER TABLE sequences
--     CHANGE seq_textid seq_textid VARCHAR( 100 )
--     CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL DEFAULT ''
--
--
-- -- --------------------------------------------------------
--
-- CREATE TABLE ec_to_go (
-- dbxref_id MEDIUMINT( 10 ) NOT NULL ,
-- go_id MEDIUMINT( 10 ) NOT NULL ,
-- PRIMARY KEY ( dbxref_id , go_id )
-- ) ENGINE = InnoDB;
--
--
-- -- --------------------------------------------------------
--
-- ALTER TABLE dbxref ADD type VARCHAR( 20 ) NULL
--
-- -- --------------------------------------------------------
--
-- ALTER TABLE count_family_seq
--   DROP FOREIGN KEY count_family_seq_ibfk_2; -- may be count_family_seq_ibfk_4
-- ALTER TABLE count_family_seq CHANGE species species_id SMALLINT( 4 ) UNSIGNED NOT NULL COMMENT 'species id (same as in table species)';
-- ALTER TABLE count_family_seq
--   ADD CONSTRAINT count_family_seq_ibfk_2 FOREIGN KEY (species_id) REFERENCES species (species_id) ON DELETE NO ACTION ON UPDATE NO ACTION;
--
-- -- --------------------------------------------------------
-- -- Family accession
-- ALTER TABLE family CHANGE family_number accession CHAR(8) NOT NULL DEFAULT '' COMMENT 'family accession (cross-release identifier); GreenPhyl family accession must be upper case and begin with GP.';
-- UPDATE family f SET f.accession = CONCAT('GP', RIGHT(CONCAT('000000', f.family_id), 6));

-- --------------------------------------------------------
-- faster text search
-- ALTER TABLE go ENGINE = MYISAM;
-- ALTER TABLE go ADD FULLTEXT(go_desc);
-- MYISAM is better for high read volumes, InnoDB for high update volumes due to table vs row locking
-- InnoDB is journaled and can recover from crashes where MYISAM can't
-- MYISAM has full-text indexing, InnoDB doesn't
-- InnoDB has transaction support, commits and rollbacks, MYISAM lacks these.

-- END OF OLD STUFF
-- -----------------------------------------------------------------------------

-- CREATE FUNCTION getStringItem(string VARCHAR(255), delimiter VARCHAR(12), item_index INT) RETURNS VARCHAR(255)
--   BEGIN
--     RETURN REPLACE(
--              SUBSTRING(
--                SUBSTRING_INDEX(string, delimiter, item_index),
--                LENGTH(SUBSTRING_INDEX(string, delimiter, item_index - 1)) + 1
--              ),
--              delimiter, ''
--            );
--   END
-- $$


-- +--------------------+-------------------+-------------------------+---------------+---------------+------------------+-------------+------------------+-------------------------------+-------------------------+-----------------------+------------------------+
-- | CONSTRAINT_CATALOG | CONSTRAINT_SCHEMA | CONSTRAINT_NAME         | TABLE_CATALOG | TABLE_SCHEMA  | TABLE_NAME       | COLUMN_NAME | ORDINAL_POSITION | POSITION_IN_UNIQUE_CONSTRAINT | REFERENCED_TABLE_SCHEMA | REFERENCED_TABLE_NAME | REFERENCED_COLUMN_NAME |
-- +--------------------+-------------------+-------------------------+---------------+---------------+------------------+-------------+------------------+-------------------------------+-------------------------+-----------------------+------------------------+
-- | NULL               | greenphyl_dev     | count_family_seq_ibfk_3 | NULL          | greenphyl_dev | count_family_seq | family_id   |                1 |                             1 | greenphyl_dev           | family                | family_id              |
-- +--------------------+-------------------+-------------------------+---------------+---------------+------------------+-------------+------------------+-------------------------------+-------------------------+-----------------------+------------------------+


/*
* changeTableKey
*********************
This procedure can be used to rename a table key without having to remove
foreign key constraint before. However, in order to prevent unexpected behaviour
the number of foreign key constraint should be known before and passed to the
procedure.

To get the initial number of constraints, use the following query:
SELECT COUNT(CONSTRAINT_NAME) FROM INFORMATION_SCHEMA.KEY_COLUMN_USAGE WHERE CONSTRAINT_SCHEMA = SCHEMA() AND REFERENCED_TABLE_SCHEMA = SCHEMA() AND REFERENCED_TABLE_NAME = <table> AND REFERENCED_COLUMN_NAME = <key_name>;

*/
DROP PROCEDURE IF EXISTS changeTableKey$$
CREATE PROCEDURE changeTableKey(
    IN key_table_name VARCHAR(255),
    IN old_key_name VARCHAR(255),
    IN new_key_name VARCHAR(255),
    IN new_key_definition VARCHAR(255),
    IN on_update_delete VARCHAR(255),
    IN expected_constraints TINYINT)
  BEGIN
    DECLARE l_removed_constraints INT DEFAULT 0;
    DECLARE l_constraint_name VARCHAR(255);
    DECLARE l_constraint_table VARCHAR(255);
    DECLARE l_constraint_column VARCHAR(255);
    DECLARE l_query VARCHAR(255);
    DECLARE done INT DEFAULT 0;
    DECLARE constraint_cursor CURSOR FOR SELECT CONSTRAINT_NAME, TABLE_NAME, COLUMN_NAME FROM INFORMATION_SCHEMA.KEY_COLUMN_USAGE WHERE CONSTRAINT_SCHEMA = SCHEMA() AND REFERENCED_TABLE_SCHEMA = SCHEMA() AND REFERENCED_TABLE_NAME = key_table_name AND REFERENCED_COLUMN_NAME = old_key_name;
    DECLARE CONTINUE HANDLER FOR NOT FOUND SET done := 1;

    -- make sure the key exists
    IF (TRUE = EXISTS (SELECT TRUE FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_SCHEMA = SCHEMA() AND TABLE_NAME = key_table_name AND COLUMN_NAME = old_key_name)) THEN
      SELECT CONCAT('Preparing key "', old_key_name, '" of table "', key_table_name, '" to be renamed into "', new_key_name, '"') AS 'Message';
      -- create a temporary table to store previous keys associations
      CREATE TEMPORARY TABLE IF NOT EXISTS temp_restore_constraints (
          query TEXT NOT NULL
      ) ENGINE=InnoDB DEFAULT CHARSET=utf8;
      -- remove foreign key contraints...
      SELECT CONCAT('Temporary remove constraints...') AS 'Message';
      OPEN constraint_cursor;
      constraint_cursor_loop: LOOP
        FETCH constraint_cursor INTO l_constraint_name, l_constraint_table, l_constraint_column;
        IF done THEN
          LEAVE constraint_cursor_loop;
        END IF;

        -- save constraint
        INSERT INTO temp_restore_constraints VALUES(CONCAT('ALTER TABLE ', l_constraint_table, ' ADD CONSTRAINT FOREIGN KEY (', l_constraint_column, ') REFERENCES ', key_table_name, ' (', new_key_name, ') ON DELETE ', on_update_delete, ' ON UPDATE ', on_update_delete, ';'));
        -- temporary drop constraint
        SELECT CONCAT('Dropping contstraint "', l_constraint_name, '" from table "', l_constraint_table, '"') AS 'Message';
        SET @drop_fkey_query := CONCAT('ALTER TABLE ', l_constraint_table, ' DROP FOREIGN KEY ', l_constraint_name);
        PREPARE drop_fkey_stmt FROM @drop_fkey_query;
        EXECUTE drop_fkey_stmt;
        DEALLOCATE PREPARE drop_fkey_stmt;
        -- check how many constraint we removed
        SET l_removed_constraints := l_removed_constraints + 1;
        IF (l_removed_constraints > expected_constraints) THEN
          SELECT CONCAT('WARNING: number of constraints is above what was expected (', expected_constraints, ') for key ', old_key_name, ' in table ', key_table_name) AS 'Message';
        END IF;
      END LOOP;

      CLOSE constraint_cursor;
      SELECT CONCAT('...done temporary remove constraints.') AS 'Message';

      IF (l_removed_constraints != expected_constraints) THEN
        SELECT CONCAT('WARNING: number of constraints (', l_removed_constraints, ') do not match what was expected (', expected_constraints, ') for key ', old_key_name, ' in table ', key_table_name) AS 'Message';
      END IF;
      -- rename key
      SELECT CONCAT('Renaming key "', old_key_name, '" into "', new_key_name, '"') AS 'Message';
      SET @rename_key_query := CONCAT('ALTER TABLE ', key_table_name, ' CHANGE ', old_key_name, ' ', new_key_name, ' ', new_key_definition);
      PREPARE rename_key_stmt FROM @rename_key_query;
      EXECUTE rename_key_stmt;
      DEALLOCATE PREPARE rename_key_stmt;

      -- Fixes "ERROR 1005 (HY000): Can't create table ... (errno: 150)" error
      SET @add_index_query := CONCAT('ALTER TABLE ', key_table_name, ' ADD INDEX temp_index (', new_key_name, ')');
      PREPARE add_index_stmt FROM @add_index_query;
      EXECUTE add_index_stmt;
      DEALLOCATE PREPARE add_index_stmt;
      SET @drop_index_query := CONCAT('ALTER TABLE ', key_table_name, ' DROP INDEX temp_index');
      PREPARE drop_index_stmt FROM @drop_index_query;
      EXECUTE drop_index_stmt;
      DEALLOCATE PREPARE drop_index_stmt;

      -- restore constraints
      SELECT CONCAT('Restore previous constraints...') AS 'Message';
      BEGIN
        -- declare the cursor after the table has been created
        DECLARE constraint_query_cursor CURSOR FOR SELECT query FROM temp_restore_constraints;

        SELECT CONCAT('Restore previous ', COUNT(query), ' constraint(s)...') AS 'Message' FROM temp_restore_constraints;
        SET done := 0;
        OPEN constraint_query_cursor;
        constraint_query_cursor_loop: LOOP
          FETCH constraint_query_cursor INTO l_query;
          IF done THEN
            LEAVE constraint_query_cursor_loop;
          END IF;

          -- make sure we renamed the key
          SET @restore_constraint_query := l_query;
          SELECT CONCAT('-', l_query) AS 'Message';
          PREPARE restore_constraint_stmt FROM @restore_constraint_query;
          EXECUTE restore_constraint_stmt;
          DEALLOCATE PREPARE restore_constraint_stmt;
        END LOOP;
      END;

      -- remove temporary table
      DROP TABLE temp_restore_constraints;

      SELECT CONCAT('...done restoring previous constraints.') AS 'Message';
    ELSEIF (TRUE = EXISTS(SELECT TRUE FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_SCHEMA = SCHEMA() AND TABLE_NAME = key_table_name AND COLUMN_NAME = new_key_name)) THEN
      SELECT CONCAT('Warning: "', key_table_name, '" key "', old_key_name, '" already changed into "', new_key_name, '". No change made.') AS 'Message';
    ELSE
      SELECT CONCAT('Error: unable to change "', key_table_name, '" key because column "', old_key_name, '" was not found!') AS 'Message';
      SET @error_count := @error_count + 1;
    END IF;

  END;
$$


/*
* changeTableKey
*****************
Removes all the foreign key constraint from a given table that are related to a
given column and another table.

*/
DROP PROCEDURE IF EXISTS dropForeignKeyConstraints$$
CREATE PROCEDURE dropForeignKeyConstraints(
    IN subject_table_name VARCHAR(255),
    IN subject_column_name VARCHAR(255),
    IN foreign_key_table_name VARCHAR(255))
  BEGIN
    -- make sure column exists
    IF (columnExists(subject_table_name, subject_column_name)
        AND tableExists(foreign_key_table_name)) THEN
      -- remove foreign key contraints...
      CALL _dropForeignKeyConstraints(subject_table_name, subject_column_name, foreign_key_table_name);
    ELSE
      SELECT CONCAT('Table "', subject_table_name, '" or column "', subject_column_name, '" (forein table "', foreign_key_table_name, '") not available!') AS 'Message';      
    END IF;

  END;
$$


DROP PROCEDURE IF EXISTS _dropForeignKeyConstraints$$
CREATE PROCEDURE _dropForeignKeyConstraints(
    IN subject_table_name VARCHAR(255),
    IN subject_column_name VARCHAR(255),
    IN foreign_key_table_name VARCHAR(255))
  BEGIN
    DECLARE l_constraint_name VARCHAR(255);
    DECLARE done INT DEFAULT 0;
    DECLARE constraint_cursor CURSOR FOR SELECT CONSTRAINT_NAME FROM INFORMATION_SCHEMA.KEY_COLUMN_USAGE WHERE CONSTRAINT_SCHEMA = SCHEMA() AND REFERENCED_TABLE_SCHEMA = SCHEMA() AND TABLE_NAME = subject_table_name AND COLUMN_NAME = subject_column_name AND REFERENCED_TABLE_NAME = foreign_key_table_name;
    DECLARE CONTINUE HANDLER FOR NOT FOUND SET done := 1;

    SELECT CONCAT('Remove contraints on column "', subject_column_name, '" of table "', subject_table_name, '" linked to table "', foreign_key_table_name, '"') AS 'Message';

    -- remove foreign key contraints...
    OPEN constraint_cursor;
    constraint_cursor_loop: LOOP
      FETCH constraint_cursor INTO l_constraint_name;
      IF done THEN
        LEAVE constraint_cursor_loop;
      END IF;
    
      -- drop constraint
      SELECT CONCAT('Dropping contstraint "', l_constraint_name, '"') AS 'Message';
      SET @drop_fkey_query := CONCAT('ALTER TABLE ', subject_table_name, ' DROP FOREIGN KEY ', l_constraint_name);
      PREPARE drop_fkey_stmt FROM @drop_fkey_query;
      EXECUTE drop_fkey_stmt;
      DEALLOCATE PREPARE drop_fkey_stmt;
    END LOOP;

    CLOSE constraint_cursor;
    SELECT '...done removing column contraints.' AS 'Message';
  END;
$$



/*
* tableExists
**************
Tells if a table exists.

*/
DROP FUNCTION IF EXISTS tableExists$$
CREATE FUNCTION tableExists(subject_table_name VARCHAR(255)) RETURNS BOOL
  BEGIN
    DECLARE l_table_exists BOOL DEFAULT FALSE;

    SELECT TRUE INTO l_table_exists FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_SCHEMA = SCHEMA() AND TABLE_NAME = subject_table_name;

    RETURN COALESCE(l_table_exists, FALSE);
  END;
$$




/*
* columnExists
***************
Tells if a column exists in the given table.

*/
DROP FUNCTION IF EXISTS columnExists$$
CREATE FUNCTION columnExists(subject_table_name VARCHAR(255), subject_column_name VARCHAR(255)) RETURNS BOOL
  BEGIN
    DECLARE l_column_exists BOOL DEFAULT FALSE;

    SELECT TRUE INTO l_column_exists FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_SCHEMA = SCHEMA() AND TABLE_NAME = subject_table_name AND COLUMN_NAME = subject_column_name;

    RETURN COALESCE(l_column_exists, FALSE);
  END;
$$




/*
* constraintExists
*******************
Tells if a constraint exists between a table column and another table.

*/
DROP FUNCTION IF EXISTS constraintExists$$
CREATE FUNCTION constraintExists(
    subject_table_name VARCHAR(255),
    subject_column_name VARCHAR(255),
    foreign_key_table_name VARCHAR(255)) RETURNS BOOL
  BEGIN
    DECLARE l_constraint_exists BOOL DEFAULT FALSE;

    SELECT TRUE INTO l_constraint_exists FROM INFORMATION_SCHEMA.KEY_COLUMN_USAGE WHERE CONSTRAINT_SCHEMA = SCHEMA() AND REFERENCED_TABLE_SCHEMA = SCHEMA() AND TABLE_NAME = subject_table_name AND COLUMN_NAME = subject_column_name AND REFERENCED_TABLE_NAME = foreign_key_table_name;

    RETURN COALESCE(l_constraint_exists, FALSE);
  END;
$$


/*
* upgradeGreenPhylUser
***********************
Upgrade GreenPhyl user data from old version using "annot_user" table to the new
one using "users" table.

*/
DROP PROCEDURE IF EXISTS upgradeGreenPhylUser$$
CREATE PROCEDURE upgradeGreenPhylUser()
  BEGIN
    -- upgrade user accounts
    DECLARE l_password VARCHAR(255);
    DECLARE l_email VARCHAR(255);
    DECLARE l_group INT(10);
    DECLARE l_username VARCHAR(255);
    DECLARE done INT DEFAULT 0;
    DECLARE user_cursor CURSOR FOR SELECT passwd, email, group_ann, username FROM annot_user;
    DECLARE CONTINUE HANDLER FOR NOT FOUND SET done := 1;

    OPEN user_cursor;
    user_cursor_loop: LOOP
      FETCH user_cursor INTO l_password, l_email, l_group, l_username;
      IF done THEN
        LEAVE user_cursor_loop;
      END IF;

      -- add user
      SELECT CONCAT('Upgrading user "', l_username, '"') AS 'Message';
      IF (1 = l_group) THEN
        -- admin
        CALL addGreenPhylUser(l_username, l_email, l_password, 'administrator,annotator,registered user', '');
      ELSE
        -- regular user
        CALL addGreenPhylUser(l_username, l_email, l_password, 'annotator,registered user', '');
      END IF;
    END LOOP;

    CLOSE user_cursor;
  END;
$$





/*
* updateFromNullTo20
*********************
Update GreenPhyl DB schema from older version to 2.0.

*/
DROP PROCEDURE IF EXISTS updateFromNullTo20$$
CREATE PROCEDURE updateFromNullTo20()
  BEGIN
    SELECT 'You have to perform manual updates to update to 2.0 and then do the query:\n  INSERT INTO variables VALUES (\'schema_version\', \'2.0\');' AS 'Message';
    -- +FIXME: do a complete schema check
    -- INSERT INTO variables VALUES ('schema_version', '2.0');
    -- ALTER TABLE family CHANGE family_number accession CHAR(8) NOT NULL DEFAULT '' COMMENT 'family accession (cross-release identifier); GreenPhyl family accession must be upper case and begin with GP.';
    -- UPDATE family f SET f.accession = CONCAT('GP', RIGHT(CONCAT('000000', f.family_id), 6));
  END;
$$



/*
* updateFrom20To30
*******************
Update GreenPhyl DB schema from 2.0 to 3.0
*/
DROP PROCEDURE IF EXISTS updateFrom20To30$$
CREATE PROCEDURE updateFrom20To30()
  BEGIN

  -- note: GET DIAGNOSTICS is only available starting from MySQL 5.6.4
  --       There's no way to get last error message ("SHOW ERRORS;" won't help)
  -- DECLARE CONTINUE HANDLER FOR SQLEXCEPTION
  --   BEGIN
  --     SET @error_count := @error_count + 1;
  --     SELECT CONCAT('An error occured and has been caught! Current error count: ', @error_count) AS 'ERROR';
  --   END
  -- ;
  DECLARE CONTINUE HANDLER FOR 1000
    BEGIN
      SET @error_count := @error_count + 1;
      SELECT 'hashchk' AS 'ERROR';
    END
  ;
  
  DECLARE CONTINUE HANDLER FOR 1001
    BEGIN
      SET @error_count := @error_count + 1;
      SELECT 'isamchk' AS 'ERROR';
    END
  ;
  
  DECLARE CONTINUE HANDLER FOR 1002
    BEGIN
      SET @error_count := @error_count + 1;
      SELECT 'NO' AS 'ERROR';
    END
  ;
  
  DECLARE CONTINUE HANDLER FOR 1003
    BEGIN
      SET @error_count := @error_count + 1;
      SELECT 'YES' AS 'ERROR';
    END
  ;
  
  DECLARE CONTINUE HANDLER FOR 1004
    BEGIN
      SET @error_count := @error_count + 1;
      SELECT 'Can''t create file ''%s'' (errno: %d)' AS 'ERROR';
    END
  ;
  
--  DECLARE CONTINUE HANDLER FOR 1005
--    BEGIN
--      SET @error_count := @error_count + 1;
--      SELECT 'Can''t create table ''%s'' (errno: %d)' AS 'ERROR';
--    END
--  ;
  
  DECLARE CONTINUE HANDLER FOR 1006
    BEGIN
      SET @error_count := @error_count + 1;
      SELECT 'Can''t create database ''%s'' (errno: %d)' AS 'ERROR';
    END
  ;
  
  DECLARE CONTINUE HANDLER FOR 1007
    BEGIN
      SET @error_count := @error_count + 1;
      SELECT 'Can''t create database ''%s''; database exists' AS 'ERROR';
    END
  ;
  
  DECLARE CONTINUE HANDLER FOR 1008
    BEGIN
      SET @error_count := @error_count + 1;
      SELECT 'Can''t drop database ''%s''; database doesn''t exist' AS 'ERROR';
    END
  ;
  
  DECLARE CONTINUE HANDLER FOR 1009
    BEGIN
      SET @error_count := @error_count + 1;
      SELECT 'Error dropping database (can''t delete ''%s'', errno: %d)' AS 'ERROR';
    END
  ;
  
  DECLARE CONTINUE HANDLER FOR 1010
    BEGIN
      SET @error_count := @error_count + 1;
      SELECT 'Error dropping database (can''t rmdir ''%s'', errno: %d)' AS 'ERROR';
    END
  ;
  
  DECLARE CONTINUE HANDLER FOR 1011
    BEGIN
      SET @error_count := @error_count + 1;
      SELECT 'Error on delete of ''%s'' (errno: %d)' AS 'ERROR';
    END
  ;
  
  DECLARE CONTINUE HANDLER FOR 1012
    BEGIN
      SET @error_count := @error_count + 1;
      SELECT 'Can''t read record in system table' AS 'ERROR';
    END
  ;
  
  DECLARE CONTINUE HANDLER FOR 1013
    BEGIN
      SET @error_count := @error_count + 1;
      SELECT 'Can''t get status of ''%s'' (errno: %d)' AS 'ERROR';
    END
  ;
  
  DECLARE CONTINUE HANDLER FOR 1014
    BEGIN
      SET @error_count := @error_count + 1;
      SELECT 'Can''t get working directory (errno: %d)' AS 'ERROR';
    END
  ;
  
  DECLARE CONTINUE HANDLER FOR 1015
    BEGIN
      SET @error_count := @error_count + 1;
      SELECT 'Can''t lock file (errno: %d)' AS 'ERROR';
    END
  ;
  
  DECLARE CONTINUE HANDLER FOR 1016
    BEGIN
      SET @error_count := @error_count + 1;
      SELECT 'Can''t open file: ''%s'' (errno: %d)' AS 'ERROR';
    END
  ;
  
  DECLARE CONTINUE HANDLER FOR 1017
    BEGIN
      SET @error_count := @error_count + 1;
      SELECT 'Can''t find file: ''%s'' (errno: %d)' AS 'ERROR';
    END
  ;
  
  DECLARE CONTINUE HANDLER FOR 1018
    BEGIN
      SET @error_count := @error_count + 1;
      SELECT 'Can''t read dir of ''%s'' (errno: %d)' AS 'ERROR';
    END
  ;
  
  DECLARE CONTINUE HANDLER FOR 1019
    BEGIN
      SET @error_count := @error_count + 1;
      SELECT 'Can''t change dir to ''%s'' (errno: %d)' AS 'ERROR';
    END
  ;
  
  DECLARE CONTINUE HANDLER FOR 1020
    BEGIN
      SET @error_count := @error_count + 1;
      SELECT 'Record has changed since last read in table ''%s''' AS 'ERROR';
    END
  ;
  
  DECLARE CONTINUE HANDLER FOR 1021
    BEGIN
      SET @error_count := @error_count + 1;
      SELECT 'Disk full (%s); waiting for someone to free some space...' AS 'ERROR';
    END
  ;
  
  DECLARE CONTINUE HANDLER FOR 1022
    BEGIN
      SET @error_count := @error_count + 1;
      SELECT 'Can''t write; duplicate key in table ''%s''' AS 'ERROR';
    END
  ;
  
  DECLARE CONTINUE HANDLER FOR 1023
    BEGIN
      SET @error_count := @error_count + 1;
      SELECT 'Error on close of ''%s'' (errno: %d)' AS 'ERROR';
    END
  ;
  
  DECLARE CONTINUE HANDLER FOR 1024
    BEGIN
      SET @error_count := @error_count + 1;
      SELECT 'Error reading file ''%s'' (errno: %d)' AS 'ERROR';
    END
  ;
  
  DECLARE CONTINUE HANDLER FOR 1025
    BEGIN
      SET @error_count := @error_count + 1;
      SELECT 'Error on rename of ''%s'' to ''%s'' (errno: %d)' AS 'ERROR';
    END
  ;
  
  DECLARE CONTINUE HANDLER FOR 1026
    BEGIN
      SET @error_count := @error_count + 1;
      SELECT 'Error writing file ''%s'' (errno: %d)' AS 'ERROR';
    END
  ;
  
  DECLARE CONTINUE HANDLER FOR 1027
    BEGIN
      SET @error_count := @error_count + 1;
      SELECT '''%s'' is locked against change' AS 'ERROR';
    END
  ;
  
  DECLARE CONTINUE HANDLER FOR 1028
    BEGIN
      SET @error_count := @error_count + 1;
      SELECT 'Sort aborted' AS 'ERROR';
    END
  ;
  
  DECLARE CONTINUE HANDLER FOR 1029
    BEGIN
      SET @error_count := @error_count + 1;
      SELECT 'View ''%s'' doesn''t exist for ''%s''' AS 'ERROR';
    END
  ;
  
  DECLARE CONTINUE HANDLER FOR 1030
    BEGIN
      SET @error_count := @error_count + 1;
      SELECT 'Got error %d from storage engine' AS 'ERROR';
    END
  ;
  
  DECLARE CONTINUE HANDLER FOR 1031
    BEGIN
      SET @error_count := @error_count + 1;
      SELECT 'Table storage engine for ''%s'' doesn''t have this option' AS 'ERROR';
    END
  ;
  
  DECLARE CONTINUE HANDLER FOR 1032
    BEGIN
      SET @error_count := @error_count + 1;
      SELECT 'Can''t find record in ''%s''' AS 'ERROR';
    END
  ;
  
  DECLARE CONTINUE HANDLER FOR 1033
    BEGIN
      SET @error_count := @error_count + 1;
      SELECT 'Incorrect information in file: ''%s''' AS 'ERROR';
    END
  ;
  
  DECLARE CONTINUE HANDLER FOR 1034
    BEGIN
      SET @error_count := @error_count + 1;
      SELECT 'Incorrect key file for table ''%s''; try to repair it' AS 'ERROR';
    END
  ;
  
  DECLARE CONTINUE HANDLER FOR 1035
    BEGIN
      SET @error_count := @error_count + 1;
      SELECT 'Old key file for table ''%s''; repair it!' AS 'ERROR';
    END
  ;
  
  DECLARE CONTINUE HANDLER FOR 1036
    BEGIN
      SET @error_count := @error_count + 1;
      SELECT 'Table ''%s'' is read only' AS 'ERROR';
    END
  ;
  
  DECLARE CONTINUE HANDLER FOR 1037
    BEGIN
      SET @error_count := @error_count + 1;
      SELECT 'Out of memory; restart server and try again (needed %d bytes)' AS 'ERROR';
    END
  ;
  
  DECLARE CONTINUE HANDLER FOR 1038
    BEGIN
      SET @error_count := @error_count + 1;
      SELECT 'Out of sort memory; increase server sort buffer size' AS 'ERROR';
    END
  ;
  
  DECLARE CONTINUE HANDLER FOR 1039
    BEGIN
      SET @error_count := @error_count + 1;
      SELECT 'Unexpected EOF found when reading file ''%s'' (errno: %d)' AS 'ERROR';
    END
  ;
  
  DECLARE CONTINUE HANDLER FOR 1040
    BEGIN
      SET @error_count := @error_count + 1;
      SELECT 'Too many connections' AS 'ERROR';
    END
  ;
  
  DECLARE CONTINUE HANDLER FOR 1041
    BEGIN
      SET @error_count := @error_count + 1;
      SELECT 'Out of memory; check if mysqld or some other process uses all available memory; if not, you may have to use ''ulimit'' to allow mysqld to use more memory or you can add more swap space' AS 'ERROR';
    END
  ;
  
  DECLARE CONTINUE HANDLER FOR 1042
    BEGIN
      SET @error_count := @error_count + 1;
      SELECT 'Can''t get hostname for your address' AS 'ERROR';
    END
  ;
  
  DECLARE CONTINUE HANDLER FOR 1043
    BEGIN
      SET @error_count := @error_count + 1;
      SELECT 'Bad handshake' AS 'ERROR';
    END
  ;
  
  DECLARE CONTINUE HANDLER FOR 1044
    BEGIN
      SET @error_count := @error_count + 1;
      SELECT 'Access denied for user ''%s''@''%s'' to database ''%s''' AS 'ERROR';
    END
  ;
  
  DECLARE CONTINUE HANDLER FOR 1045
    BEGIN
      SET @error_count := @error_count + 1;
      SELECT 'Access denied for user ''%s''@''%s'' (using password: %s)' AS 'ERROR';
    END
  ;
  
  DECLARE CONTINUE HANDLER FOR 1046
    BEGIN
      SET @error_count := @error_count + 1;
      SELECT 'No database selected' AS 'ERROR';
    END
  ;
  
  DECLARE CONTINUE HANDLER FOR 1047
    BEGIN
      SET @error_count := @error_count + 1;
      SELECT 'Unknown command' AS 'ERROR';
    END
  ;
  
  DECLARE CONTINUE HANDLER FOR 1048
    BEGIN
      SET @error_count := @error_count + 1;
      SELECT 'Column ''%s'' cannot be null' AS 'ERROR';
    END
  ;
  
  DECLARE CONTINUE HANDLER FOR 1049
    BEGIN
      SET @error_count := @error_count + 1;
      SELECT 'Unknown database ''%s''' AS 'ERROR';
    END
  ;
  
  DECLARE CONTINUE HANDLER FOR 1050
    BEGIN
      SET @error_count := @error_count + 1;
      SELECT 'Table ''%s'' already exists' AS 'ERROR';
    END
  ;
  
  DECLARE CONTINUE HANDLER FOR 1051
    BEGIN
      SET @error_count := @error_count + 1;
      SELECT 'Unknown table ''%s''' AS 'ERROR';
    END
  ;
  
  DECLARE CONTINUE HANDLER FOR 1052
    BEGIN
      SET @error_count := @error_count + 1;
      SELECT 'Column ''%s'' in %s is ambiguous' AS 'ERROR';
    END
  ;
  
  DECLARE CONTINUE HANDLER FOR 1053
    BEGIN
      SET @error_count := @error_count + 1;
      SELECT 'Server shutdown in progress' AS 'ERROR';
    END
  ;
  
  DECLARE CONTINUE HANDLER FOR 1054
    BEGIN
      SET @error_count := @error_count + 1;
      SELECT 'Unknown column ''%s'' in ''%s''' AS 'ERROR';
    END
  ;
  
  DECLARE CONTINUE HANDLER FOR 1055
    BEGIN
      SET @error_count := @error_count + 1;
      SELECT '''%s'' isn''t in GROUP BY' AS 'ERROR';
    END
  ;
  
  DECLARE CONTINUE HANDLER FOR 1056
    BEGIN
      SET @error_count := @error_count + 1;
      SELECT 'Can''t group on ''%s''' AS 'ERROR';
    END
  ;
  
  DECLARE CONTINUE HANDLER FOR 1057
    BEGIN
      SET @error_count := @error_count + 1;
      SELECT 'Statement has sum functions and columns in same statement' AS 'ERROR';
    END
  ;
  
  DECLARE CONTINUE HANDLER FOR 1058
    BEGIN
      SET @error_count := @error_count + 1;
      SELECT 'Column count doesn''t match value count' AS 'ERROR';
    END
  ;
  
  DECLARE CONTINUE HANDLER FOR 1059
    BEGIN
      SET @error_count := @error_count + 1;
      SELECT 'Identifier name ''%s'' is too long' AS 'ERROR';
    END
  ;
  
  DECLARE CONTINUE HANDLER FOR 1060
    BEGIN
      SET @error_count := @error_count + 1;
      SELECT 'Duplicate column name ''%s''' AS 'ERROR';
    END
  ;
  
  DECLARE CONTINUE HANDLER FOR 1061
    BEGIN
      SET @error_count := @error_count + 1;
      SELECT 'Duplicate key name ''%s''' AS 'ERROR';
    END
  ;
  
  DECLARE CONTINUE HANDLER FOR 1062
    BEGIN
      SET @error_count := @error_count + 1;
      SELECT 'Duplicate entry ''%s'' for key %d' AS 'ERROR';
    END
  ;
  
  
  DECLARE CONTINUE HANDLER FOR 1063
    BEGIN
      SET @error_count := @error_count + 1;
      SELECT 'Incorrect column specifier for column ''%s''' AS 'ERROR';
    END
  ;
  
  DECLARE CONTINUE HANDLER FOR 1064
    BEGIN
      SET @error_count := @error_count + 1;
      SELECT '%s near ''%s'' at line %d' AS 'ERROR';
    END
  ;
  
  DECLARE CONTINUE HANDLER FOR 1065
    BEGIN
      SET @error_count := @error_count + 1;
      SELECT 'Query was empty' AS 'ERROR';
    END
  ;
  
  DECLARE CONTINUE HANDLER FOR 1066
    BEGIN
      SET @error_count := @error_count + 1;
      SELECT 'Not unique table/alias: ''%s''' AS 'ERROR';
    END
  ;
  
  DECLARE CONTINUE HANDLER FOR 1067
    BEGIN
      SET @error_count := @error_count + 1;
      SELECT 'Invalid default value for ''%s''' AS 'ERROR';
    END
  ;
  
  DECLARE CONTINUE HANDLER FOR 1068
    BEGIN
      SET @error_count := @error_count + 1;
      SELECT 'Multiple primary key defined' AS 'ERROR';
    END
  ;
  
  DECLARE CONTINUE HANDLER FOR 1069
    BEGIN
      SET @error_count := @error_count + 1;
      SELECT 'Too many keys specified; max %d keys allowed' AS 'ERROR';
    END
  ;
  
  DECLARE CONTINUE HANDLER FOR 1070
    BEGIN
      SET @error_count := @error_count + 1;
      SELECT 'Too many key parts specified; max %d parts allowed' AS 'ERROR';
    END
  ;
  
  DECLARE CONTINUE HANDLER FOR 1071
    BEGIN
      SET @error_count := @error_count + 1;
      SELECT 'Specified key was too long; max key length is %d bytes' AS 'ERROR';
    END
  ;
  
  DECLARE CONTINUE HANDLER FOR 1072
    BEGIN
      SET @error_count := @error_count + 1;
      SELECT 'Key column ''%s'' doesn''t exist in table' AS 'ERROR';
    END
  ;
  
  DECLARE CONTINUE HANDLER FOR 1073
    BEGIN
      SET @error_count := @error_count + 1;
      SELECT 'BLOB column ''%s'' can''t be used in key specification with the used table type' AS 'ERROR';
    END
  ;
  
  DECLARE CONTINUE HANDLER FOR 1074
    BEGIN
      SET @error_count := @error_count + 1;
      SELECT 'Column length too big for column ''%s'' (max = %lu); use BLOB or TEXT instead' AS 'ERROR';
    END
  ;
  
  DECLARE CONTINUE HANDLER FOR 1075
    BEGIN
      SET @error_count := @error_count + 1;
      SELECT 'Incorrect table definition; there can be only one auto column and it must be defined as a key' AS 'ERROR';
    END
  ;
  
  DECLARE CONTINUE HANDLER FOR 1076
    BEGIN
      SET @error_count := @error_count + 1;
      SELECT '%s: ready for connections. Version: ''%s'' socket: ''%s'' port: %d' AS 'ERROR';
    END
  ;
  
  DECLARE CONTINUE HANDLER FOR 1077
    BEGIN
      SET @error_count := @error_count + 1;
      SELECT '%s: Normal shutdown' AS 'ERROR';
    END
  ;
  
  DECLARE CONTINUE HANDLER FOR 1078
    BEGIN
      SET @error_count := @error_count + 1;
      SELECT '%s: Got signal %d. Aborting!' AS 'ERROR';
    END
  ;
  
  DECLARE CONTINUE HANDLER FOR 1079
    BEGIN
      SET @error_count := @error_count + 1;
      SELECT '%s: Shutdown complete' AS 'ERROR';
    END
  ;
  
  DECLARE CONTINUE HANDLER FOR 1080
    BEGIN
      SET @error_count := @error_count + 1;
      SELECT '%s: Forcing close of thread %ld user: ''%s''' AS 'ERROR';
    END
  ;
  
  DECLARE CONTINUE HANDLER FOR 1081
    BEGIN
      SET @error_count := @error_count + 1;
      SELECT 'Can''t create IP socket' AS 'ERROR';
    END
  ;
  
  DECLARE CONTINUE HANDLER FOR 1082
    BEGIN
      SET @error_count := @error_count + 1;
      SELECT 'Table ''%s'' has no index like the one used in CREATE INDEX; recreate the table' AS 'ERROR';
    END
  ;
  
  DECLARE CONTINUE HANDLER FOR 1083
    BEGIN
      SET @error_count := @error_count + 1;
      SELECT 'Field separator argument is not what is expected; check the manual' AS 'ERROR';
    END
  ;
  
  DECLARE CONTINUE HANDLER FOR 1084
    BEGIN
      SET @error_count := @error_count + 1;
      SELECT 'You can''t use fixed rowlength with BLOBs; please use ''fields terminated by''' AS 'ERROR';
    END
  ;
  
  DECLARE CONTINUE HANDLER FOR 1085
    BEGIN
      SET @error_count := @error_count + 1;
      SELECT 'The file ''%s'' must be in the database directory or be readable by all' AS 'ERROR';
    END
  ;
  
  DECLARE CONTINUE HANDLER FOR 1086
    BEGIN
      SET @error_count := @error_count + 1;
      SELECT 'File ''%s'' already exists' AS 'ERROR';
    END
  ;
  
  DECLARE CONTINUE HANDLER FOR 1087
    BEGIN
      SET @error_count := @error_count + 1;
      SELECT 'Records: %ld Deleted: %ld Skipped: %ld Warnings: %ld' AS 'ERROR';
    END
  ;
  
  DECLARE CONTINUE HANDLER FOR 1088
    BEGIN
      SET @error_count := @error_count + 1;
      SELECT 'Records: %ld Duplicates: %ld' AS 'ERROR';
    END
  ;
  
  DECLARE CONTINUE HANDLER FOR 1089
    BEGIN
      SET @error_count := @error_count + 1;
      SELECT 'Incorrect prefix key; the used key part isn''t a string, the used length is longer than the key part, or the storage engine doesn''t support unique prefix keys' AS 'ERROR';
    END
  ;
  
  DECLARE CONTINUE HANDLER FOR 1090
    BEGIN
      SET @error_count := @error_count + 1;
      SELECT 'You can''t delete all columns with ALTER TABLE; use DROP TABLE instead' AS 'ERROR';
    END
  ;
  
  DECLARE CONTINUE HANDLER FOR 1091
    BEGIN
      SET @error_count := @error_count + 1;
      SELECT 'Can''t DROP ''%s''; check that column/key exists' AS 'ERROR';
    END
  ;
  
  DECLARE CONTINUE HANDLER FOR 1092
    BEGIN
      SET @error_count := @error_count + 1;
      SELECT 'Records: %ld Duplicates: %ld Warnings: %ld' AS 'ERROR';
    END
  ;
  
  DECLARE CONTINUE HANDLER FOR 1093
    BEGIN
      SET @error_count := @error_count + 1;
      SELECT 'You can''t specify target table ''%s'' for update in FROM clause' AS 'ERROR';
    END
  ;
  
  DECLARE CONTINUE HANDLER FOR 1094
    BEGIN
      SET @error_count := @error_count + 1;
      SELECT 'Unknown thread id: %lu' AS 'ERROR';
    END
  ;
  
  DECLARE CONTINUE HANDLER FOR 1095
    BEGIN
      SET @error_count := @error_count + 1;
      SELECT 'You are not owner of thread %lu' AS 'ERROR';
    END
  ;
  
  DECLARE CONTINUE HANDLER FOR 1096
    BEGIN
      SET @error_count := @error_count + 1;
      SELECT 'No tables used' AS 'ERROR';
    END
  ;
  
  DECLARE CONTINUE HANDLER FOR 1097
    BEGIN
      SET @error_count := @error_count + 1;
      SELECT 'Too many strings for column %s and SET' AS 'ERROR';
    END
  ;
  
  DECLARE CONTINUE HANDLER FOR 1098
    BEGIN
      SET @error_count := @error_count + 1;
      SELECT 'Can''t generate a unique log-filename %s.(1-999)' AS 'ERROR';
    END
  ;
  
  DECLARE CONTINUE HANDLER FOR 1099
    BEGIN
      SET @error_count := @error_count + 1;
      SELECT 'Table ''%s'' was locked with a READ lock and can''t be updated' AS 'ERROR';
    END
  ;
  
  DECLARE CONTINUE HANDLER FOR 1100
    BEGIN
      SET @error_count := @error_count + 1;
      SELECT 'Table ''%s'' was not locked with LOCK TABLES' AS 'ERROR';
    END
  ;
  
  DECLARE CONTINUE HANDLER FOR 1101
    BEGIN
      SET @error_count := @error_count + 1;
      SELECT 'BLOB/TEXT column ''%s'' can''t have a default value' AS 'ERROR';
    END
  ;
  
  DECLARE CONTINUE HANDLER FOR 1102
    BEGIN
      SET @error_count := @error_count + 1;
      SELECT 'Incorrect database name ''%s''' AS 'ERROR';
    END
  ;
  
  DECLARE CONTINUE HANDLER FOR 1103
    BEGIN
      SET @error_count := @error_count + 1;
      SELECT 'Incorrect table name ''%s''' AS 'ERROR';
    END
  ;
  
  DECLARE CONTINUE HANDLER FOR 1104
    BEGIN
      SET @error_count := @error_count + 1;
      SELECT 'The SELECT would examine more than MAX_JOIN_SIZE rows; check your WHERE and use SET SQL_BIG_SELECTS=1 or SET MAX_JOIN_SIZE=# if the SELECT is okay' AS 'ERROR';
    END
  ;
  
  DECLARE CONTINUE HANDLER FOR 1105
    BEGIN
      SET @error_count := @error_count + 1;
      SELECT 'Unknown error' AS 'ERROR';
    END
  ;
  
  DECLARE CONTINUE HANDLER FOR 1106
    BEGIN
      SET @error_count := @error_count + 1;
      SELECT 'Unknown procedure ''%s''' AS 'ERROR';
    END
  ;
  
  DECLARE CONTINUE HANDLER FOR 1107
    BEGIN
      SET @error_count := @error_count + 1;
      SELECT 'Incorrect parameter count to procedure ''%s''' AS 'ERROR';
    END
  ;
  
  DECLARE CONTINUE HANDLER FOR 1108
    BEGIN
      SET @error_count := @error_count + 1;
      SELECT 'Incorrect parameters to procedure ''%s''' AS 'ERROR';
    END
  ;
  
  DECLARE CONTINUE HANDLER FOR 1109
    BEGIN
      SET @error_count := @error_count + 1;
      SELECT 'Unknown table ''%s'' in %s' AS 'ERROR';
    END
  ;
  
  DECLARE CONTINUE HANDLER FOR 1110
    BEGIN
      SET @error_count := @error_count + 1;
      SELECT 'Column ''%s'' specified twice' AS 'ERROR';
    END
  ;
  
  DECLARE CONTINUE HANDLER FOR 1111
    BEGIN
      SET @error_count := @error_count + 1;
      SELECT 'Invalid use of group function' AS 'ERROR';
    END
  ;
  
  DECLARE CONTINUE HANDLER FOR 1112
    BEGIN
      SET @error_count := @error_count + 1;
      SELECT 'Table ''%s'' uses an extension that doesn''t exist in this MySQL version' AS 'ERROR';
    END
  ;
  
  DECLARE CONTINUE HANDLER FOR 1113
    BEGIN
      SET @error_count := @error_count + 1;
      SELECT 'A table must have at least 1 column' AS 'ERROR';
    END
  ;
  
  DECLARE CONTINUE HANDLER FOR 1114
    BEGIN
      SET @error_count := @error_count + 1;
      SELECT 'The table ''%s'' is full' AS 'ERROR';
    END
  ;
  
  DECLARE CONTINUE HANDLER FOR 1115
    BEGIN
      SET @error_count := @error_count + 1;
      SELECT 'Unknown character set: ''%s''' AS 'ERROR';
    END
  ;
  
  DECLARE CONTINUE HANDLER FOR 1116
    BEGIN
      SET @error_count := @error_count + 1;
      SELECT 'Too many tables; MySQL can only use %d tables in a join' AS 'ERROR';
    END
  ;
  
  DECLARE CONTINUE HANDLER FOR 1117
    BEGIN
      SET @error_count := @error_count + 1;
      SELECT 'Too many columns' AS 'ERROR';
    END
  ;
  
  DECLARE CONTINUE HANDLER FOR 1118
    BEGIN
      SET @error_count := @error_count + 1;
      SELECT 'Row size too large. The maximum row size for the used table type, not counting BLOBs, is %ld. You have to change some columns to TEXT or BLOBs' AS 'ERROR';
    END
  ;
  
  DECLARE CONTINUE HANDLER FOR 1119
    BEGIN
      SET @error_count := @error_count + 1;
      SELECT 'Thread stack overrun: Used: %ld of a %ld stack. Use ''mysqld -O thread_stack=#'' to specify a bigger stack if needed' AS 'ERROR';
    END
  ;
  
  DECLARE CONTINUE HANDLER FOR 1120
    BEGIN
      SET @error_count := @error_count + 1;
      SELECT 'Cross dependency found in OUTER JOIN; examine your ON conditions' AS 'ERROR';
    END
  ;
  
  DECLARE CONTINUE HANDLER FOR 1121
    BEGIN
      SET @error_count := @error_count + 1;
      SELECT 'Table handler doesn''t support NULL in given index. Please change column ''%s'' to be NOT NULL or use another handler' AS 'ERROR';
    END
  ;
  
  DECLARE CONTINUE HANDLER FOR 1122
    BEGIN
      SET @error_count := @error_count + 1;
      SELECT 'Can''t load function ''%s''' AS 'ERROR';
    END
  ;
  
  DECLARE CONTINUE HANDLER FOR 1123
    BEGIN
      SET @error_count := @error_count + 1;
      SELECT 'Can''t initialize function ''%s''; %s' AS 'ERROR';
    END
  ;
  
  DECLARE CONTINUE HANDLER FOR 1124
    BEGIN
      SET @error_count := @error_count + 1;
      SELECT 'No paths allowed for shared library' AS 'ERROR';
    END
  ;
  
  DECLARE CONTINUE HANDLER FOR 1125
    BEGIN
      SET @error_count := @error_count + 1;
      SELECT 'Function ''%s'' already exists' AS 'ERROR';
    END
  ;
  
  DECLARE CONTINUE HANDLER FOR 1126
    BEGIN
      SET @error_count := @error_count + 1;
      SELECT 'Can''t open shared library ''%s'' (errno: %d %s)' AS 'ERROR';
    END
  ;
  
  DECLARE CONTINUE HANDLER FOR 1127
    BEGIN
      SET @error_count := @error_count + 1;
      SELECT 'Can''t find symbol ''%s'' in library' AS 'ERROR';
    END
  ;
  
  DECLARE CONTINUE HANDLER FOR 1128
    BEGIN
      SET @error_count := @error_count + 1;
      SELECT 'Function ''%s'' is not defined' AS 'ERROR';
    END
  ;
  
  DECLARE CONTINUE HANDLER FOR 1129
    BEGIN
      SET @error_count := @error_count + 1;
      SELECT 'Host ''%s'' is blocked because of many connection errors; unblock with ''mysqladmin flush-hosts''' AS 'ERROR';
    END
  ;
  
  DECLARE CONTINUE HANDLER FOR 1130
    BEGIN
      SET @error_count := @error_count + 1;
      SELECT 'Host ''%s'' is not allowed to connect to this MySQL server' AS 'ERROR';
    END
  ;
  
  DECLARE CONTINUE HANDLER FOR 1131
    BEGIN
      SET @error_count := @error_count + 1;
      SELECT 'You are using MySQL as an anonymous user and anonymous users are not allowed to change passwords' AS 'ERROR';
    END
  ;
  
  DECLARE CONTINUE HANDLER FOR 1132
    BEGIN
      SET @error_count := @error_count + 1;
      SELECT 'You must have privileges to update tables in the mysql database to be able to change passwords for others' AS 'ERROR';
    END
  ;
  
  DECLARE CONTINUE HANDLER FOR 1133
    BEGIN
      SET @error_count := @error_count + 1;
      SELECT 'Can''t find any matching row in the user table' AS 'ERROR';
    END
  ;
  
  DECLARE CONTINUE HANDLER FOR 1134
    BEGIN
      SET @error_count := @error_count + 1;
      SELECT 'Rows matched: %ld Changed: %ld Warnings: %ld' AS 'ERROR';
    END
  ;
  
  DECLARE CONTINUE HANDLER FOR 1135
    BEGIN
      SET @error_count := @error_count + 1;
      SELECT 'Can''t create a new thread (errno %d); if you are not out of available memory, you can consult the manual for a possible OS-dependent bug' AS 'ERROR';
    END
  ;
  
  DECLARE CONTINUE HANDLER FOR 1136
    BEGIN
      SET @error_count := @error_count + 1;
      SELECT 'Column count doesn''t match value count at row %ld' AS 'ERROR';
    END
  ;
  
  DECLARE CONTINUE HANDLER FOR 1137
    BEGIN
      SET @error_count := @error_count + 1;
      SELECT 'Can''t reopen table: ''%s''' AS 'ERROR';
    END
  ;
  
  DECLARE CONTINUE HANDLER FOR 1138
    BEGIN
      SET @error_count := @error_count + 1;
      SELECT 'Invalid use of NULL value' AS 'ERROR';
    END
  ;
  
  DECLARE CONTINUE HANDLER FOR 1139
    BEGIN
      SET @error_count := @error_count + 1;
      SELECT 'Got error ''%s'' from regexp' AS 'ERROR';
    END
  ;
  
  DECLARE CONTINUE HANDLER FOR 1140
    BEGIN
      SET @error_count := @error_count + 1;
      SELECT 'Mixing of GROUP columns (MIN(),MAX(),COUNT(),...) with no GROUP columns is illegal if there is no GROUP BY clause' AS 'ERROR';
    END
  ;
  
  DECLARE CONTINUE HANDLER FOR 1141
    BEGIN
      SET @error_count := @error_count + 1;
      SELECT 'There is no such grant defined for user ''%s'' on host ''%s''' AS 'ERROR';
    END
  ;
  
  DECLARE CONTINUE HANDLER FOR 1142
    BEGIN
      SET @error_count := @error_count + 1;
      SELECT '%s command denied to user ''%s''@''%s'' for table ''%s''' AS 'ERROR';
    END
  ;
  
  DECLARE CONTINUE HANDLER FOR 1143
    BEGIN
      SET @error_count := @error_count + 1;
      SELECT '%s command denied to user ''%s''@''%s'' for column ''%s'' in table ''%s''' AS 'ERROR';
    END
  ;
  
  DECLARE CONTINUE HANDLER FOR 1144
    BEGIN
      SET @error_count := @error_count + 1;
      SELECT 'Illegal GRANT/REVOKE command; please consult the manual to see which privileges can be used' AS 'ERROR';
    END
  ;
  
  DECLARE CONTINUE HANDLER FOR 1145
    BEGIN
      SET @error_count := @error_count + 1;
      SELECT 'The host or user argument to GRANT is too long' AS 'ERROR';
    END
  ;
  
--  DECLARE CONTINUE HANDLER FOR 1146
--    BEGIN
--      -- SET @error_count := @error_count + 1;
--      -- SELECT 'Table ''%s.%s'' doesn''t exist' AS 'ERROR';
--      -- Skipping this error as it is raised by testing procedures and we don't
--      -- want these procedures to stop the process!
--      SELECT 'Skipping "Table ''%s.%s'' doesn''t exist" error' AS 'WARNING';
--    END
--  ;
  
  DECLARE CONTINUE HANDLER FOR 1147
    BEGIN
      SET @error_count := @error_count + 1;
      SELECT 'There is no such grant defined for user ''%s'' on host ''%s'' on table ''%s''' AS 'ERROR';
    END
  ;
  
  DECLARE CONTINUE HANDLER FOR 1148
    BEGIN
      SET @error_count := @error_count + 1;
      SELECT 'The used command is not allowed with this MySQL version' AS 'ERROR';
    END
  ;
  
  DECLARE CONTINUE HANDLER FOR 1149
    BEGIN
      SET @error_count := @error_count + 1;
      SELECT 'You have an error in your SQL syntax; check the manual that corresponds to your MySQL server version for the right syntax to use' AS 'ERROR';
    END
  ;
  
  DECLARE CONTINUE HANDLER FOR 1150
    BEGIN
      SET @error_count := @error_count + 1;
      SELECT 'Delayed insert thread couldn''t get requested lock for table %s' AS 'ERROR';
    END
  ;
  
  DECLARE CONTINUE HANDLER FOR 1151
    BEGIN
      SET @error_count := @error_count + 1;
      SELECT 'Too many delayed threads in use' AS 'ERROR';
    END
  ;
  
  DECLARE CONTINUE HANDLER FOR 1152
    BEGIN
      SET @error_count := @error_count + 1;
      SELECT 'Aborted connection %ld to db: ''%s'' user: ''%s'' (%s)' AS 'ERROR';
    END
  ;
  
  DECLARE CONTINUE HANDLER FOR 1153
    BEGIN
      SET @error_count := @error_count + 1;
      SELECT 'Got a packet bigger than ''max_allowed_packet'' bytes' AS 'ERROR';
    END
  ;
  
  DECLARE CONTINUE HANDLER FOR 1154
    BEGIN
      SET @error_count := @error_count + 1;
      SELECT 'Got a read error from the connection pipe' AS 'ERROR';
    END
  ;
  
  DECLARE CONTINUE HANDLER FOR 1155
    BEGIN
      SET @error_count := @error_count + 1;
      SELECT 'Got an error from fcntl()' AS 'ERROR';
    END
  ;
  
  DECLARE CONTINUE HANDLER FOR 1156
    BEGIN
      SET @error_count := @error_count + 1;
      SELECT 'Got packets out of order' AS 'ERROR';
    END
  ;
  
  DECLARE CONTINUE HANDLER FOR 1157
    BEGIN
      SET @error_count := @error_count + 1;
      SELECT 'Couldn''t uncompress communication packet' AS 'ERROR';
    END
  ;
  
  DECLARE CONTINUE HANDLER FOR 1158
    BEGIN
      SET @error_count := @error_count + 1;
      SELECT 'Got an error reading communication packets' AS 'ERROR';
    END
  ;
  
  DECLARE CONTINUE HANDLER FOR 1159
    BEGIN
      SET @error_count := @error_count + 1;
      SELECT 'Got timeout reading communication packets' AS 'ERROR';
    END
  ;
  
  DECLARE CONTINUE HANDLER FOR 1160
    BEGIN
      SET @error_count := @error_count + 1;
      SELECT 'Got an error writing communication packets' AS 'ERROR';
    END
  ;
  
  DECLARE CONTINUE HANDLER FOR 1161
    BEGIN
      SET @error_count := @error_count + 1;
      SELECT 'Got timeout writing communication packets' AS 'ERROR';
    END
  ;
  
  DECLARE CONTINUE HANDLER FOR 1162
    BEGIN
      SET @error_count := @error_count + 1;
      SELECT 'Result string is longer than ''max_allowed_packet'' bytes' AS 'ERROR';
    END
  ;
  
  DECLARE CONTINUE HANDLER FOR 1163
    BEGIN
      SET @error_count := @error_count + 1;
      SELECT 'The used table type doesn''t support BLOB/TEXT columns' AS 'ERROR';
    END
  ;
  
  DECLARE CONTINUE HANDLER FOR 1164
    BEGIN
      SET @error_count := @error_count + 1;
      SELECT 'The used table type doesn''t support AUTO_INCREMENT columns' AS 'ERROR';
    END
  ;
  
  DECLARE CONTINUE HANDLER FOR 1165
    BEGIN
      SET @error_count := @error_count + 1;
      SELECT 'INSERT DELAYED can''t be used with table ''%s'' because it is locked with LOCK TABLES' AS 'ERROR';
    END
  ;
  
  DECLARE CONTINUE HANDLER FOR 1166
    BEGIN
      SET @error_count := @error_count + 1;
      SELECT 'Incorrect column name ''%s''' AS 'ERROR';
    END
  ;
  
  DECLARE CONTINUE HANDLER FOR 1167
    BEGIN
      SET @error_count := @error_count + 1;
      SELECT 'The used storage engine can''t index column ''%s''' AS 'ERROR';
    END
  ;
  
  DECLARE CONTINUE HANDLER FOR 1168
    BEGIN
      SET @error_count := @error_count + 1;
      SELECT 'Unable to open underlying table which is differently defined or of non-MyISAM type or doesn''t exist' AS 'ERROR';
    END
  ;
  
  DECLARE CONTINUE HANDLER FOR 1169
    BEGIN
      SET @error_count := @error_count + 1;
      SELECT 'Can''t write, because of unique constraint, to table ''%s''' AS 'ERROR';
    END
  ;
  
  DECLARE CONTINUE HANDLER FOR 1170
    BEGIN
      SET @error_count := @error_count + 1;
      SELECT 'BLOB/TEXT column ''%s'' used in key specification without a key length' AS 'ERROR';
    END
  ;
  
  DECLARE CONTINUE HANDLER FOR 1171
    BEGIN
      SET @error_count := @error_count + 1;
      SELECT 'All parts of a PRIMARY KEY must be NOT NULL; if you need NULL in a key, use UNIQUE instead' AS 'ERROR';
    END
  ;
  
  DECLARE CONTINUE HANDLER FOR 1172
    BEGIN
      SET @error_count := @error_count + 1;
      SELECT 'Result consisted of more than one row' AS 'ERROR';
    END
  ;
  
  DECLARE CONTINUE HANDLER FOR 1173
    BEGIN
      SET @error_count := @error_count + 1;
      SELECT 'This table type requires a primary key' AS 'ERROR';
    END
  ;
  
  DECLARE CONTINUE HANDLER FOR 1174
    BEGIN
      SET @error_count := @error_count + 1;
      SELECT 'This version of MySQL is not compiled with RAID support' AS 'ERROR';
    END
  ;
  
  DECLARE CONTINUE HANDLER FOR 1175
    BEGIN
      SET @error_count := @error_count + 1;
      SELECT 'You are using safe update mode and you tried to update a table without a WHERE that uses a KEY column' AS 'ERROR';
    END
  ;
  
  DECLARE CONTINUE HANDLER FOR 1176
    BEGIN
      SET @error_count := @error_count + 1;
      SELECT 'Key ''%s'' doesn''t exist in table ''%s''' AS 'ERROR';
    END
  ;
  
  DECLARE CONTINUE HANDLER FOR 1177
    BEGIN
      SET @error_count := @error_count + 1;
      SELECT 'Can''t open table' AS 'ERROR';
    END
  ;
  
  DECLARE CONTINUE HANDLER FOR 1178
    BEGIN
      SET @error_count := @error_count + 1;
      SELECT 'The storage engine for the table doesn''t support %s' AS 'ERROR';
    END
  ;
  
  DECLARE CONTINUE HANDLER FOR 1179
    BEGIN
      SET @error_count := @error_count + 1;
      SELECT 'You are not allowed to execute this command in a transaction' AS 'ERROR';
    END
  ;
  
  DECLARE CONTINUE HANDLER FOR 1180
    BEGIN
      SET @error_count := @error_count + 1;
      SELECT 'Got error %d during COMMIT' AS 'ERROR';
    END
  ;
  
  DECLARE CONTINUE HANDLER FOR 1181
    BEGIN
      SET @error_count := @error_count + 1;
      SELECT 'Got error %d during ROLLBACK' AS 'ERROR';
    END
  ;
  
  DECLARE CONTINUE HANDLER FOR 1182
    BEGIN
      SET @error_count := @error_count + 1;
      SELECT 'Got error %d during FLUSH_LOGS' AS 'ERROR';
    END
  ;
  
  DECLARE CONTINUE HANDLER FOR 1183
    BEGIN
      SET @error_count := @error_count + 1;
      SELECT 'Got error %d during CHECKPOINT' AS 'ERROR';
    END
  ;
  
  DECLARE CONTINUE HANDLER FOR 1184
    BEGIN
      SET @error_count := @error_count + 1;
      SELECT 'Aborted connection %ld to db: ''%s'' user: ''%s'' host: ''%s'' (%s)' AS 'ERROR';
    END
  ;
  
  DECLARE CONTINUE HANDLER FOR 1185
    BEGIN
      SET @error_count := @error_count + 1;
      SELECT 'The storage engine for the table does not support binary table dump' AS 'ERROR';
    END
  ;
  
  DECLARE CONTINUE HANDLER FOR 1186
    BEGIN
      SET @error_count := @error_count + 1;
      SELECT 'Binlog closed, cannot RESET MASTER' AS 'ERROR';
    END
  ;
  
  DECLARE CONTINUE HANDLER FOR 1187
    BEGIN
      SET @error_count := @error_count + 1;
      SELECT 'Failed rebuilding the index of dumped table ''%s''' AS 'ERROR';
    END
  ;
  
  DECLARE CONTINUE HANDLER FOR 1188
    BEGIN
      SET @error_count := @error_count + 1;
      SELECT 'Error from master: ''%s''' AS 'ERROR';
    END
  ;
  
  DECLARE CONTINUE HANDLER FOR 1189
    BEGIN
      SET @error_count := @error_count + 1;
      SELECT 'Net error reading from master' AS 'ERROR';
    END
  ;
  
  DECLARE CONTINUE HANDLER FOR 1190
    BEGIN
      SET @error_count := @error_count + 1;
      SELECT 'Net error writing to master' AS 'ERROR';
    END
  ;
  
  DECLARE CONTINUE HANDLER FOR 1191
    BEGIN
      SET @error_count := @error_count + 1;
      SELECT 'Can''t find FULLTEXT index matching the column list' AS 'ERROR';
    END
  ;
  
  DECLARE CONTINUE HANDLER FOR 1192
    BEGIN
      SET @error_count := @error_count + 1;
      SELECT 'Can''t execute the given command because you have active locked tables or an active transaction' AS 'ERROR';
    END
  ;
  
  DECLARE CONTINUE HANDLER FOR 1193
    BEGIN
      SET @error_count := @error_count + 1;
      SELECT 'Unknown system variable ''%s''' AS 'ERROR';
    END
  ;
  
  DECLARE CONTINUE HANDLER FOR 1194
    BEGIN
      SET @error_count := @error_count + 1;
      SELECT 'Table ''%s'' is marked as crashed and should be repaired' AS 'ERROR';
    END
  ;
  
  DECLARE CONTINUE HANDLER FOR 1195
    BEGIN
      SET @error_count := @error_count + 1;
      SELECT 'Table ''%s'' is marked as crashed and last (automatic?) repair failed' AS 'ERROR';
    END
  ;
  
  DECLARE CONTINUE HANDLER FOR 1196
    BEGIN
      SET @error_count := @error_count + 1;
      SELECT 'Some non-transactional changed tables couldn''t be rolled back' AS 'ERROR';
    END
  ;
  
  DECLARE CONTINUE HANDLER FOR 1197
    BEGIN
      SET @error_count := @error_count + 1;
      SELECT 'Multi-statement transaction required more than ''max_binlog_cache_size'' bytes of storage; increase this mysqld variable and try again' AS 'ERROR';
    END
  ;
  
  DECLARE CONTINUE HANDLER FOR 1198
    BEGIN
      SET @error_count := @error_count + 1;
      SELECT 'This operation cannot be performed with a running slave; run STOP SLAVE first' AS 'ERROR';
    END
  ;
  
  DECLARE CONTINUE HANDLER FOR 1199
    BEGIN
      SET @error_count := @error_count + 1;
      SELECT 'This operation requires a running slave; configure slave and do START SLAVE' AS 'ERROR';
    END
  ;
  
  DECLARE CONTINUE HANDLER FOR 1200
    BEGIN
      SET @error_count := @error_count + 1;
      SELECT 'The server is not configured as slave; fix in config file or with CHANGE MASTER TO' AS 'ERROR';
    END
  ;
  
  DECLARE CONTINUE HANDLER FOR 1201
    BEGIN
      SET @error_count := @error_count + 1;
      SELECT 'Could not initialize master info structure; more error messages can be found in the MySQL error log' AS 'ERROR';
    END
  ;
  
  DECLARE CONTINUE HANDLER FOR 1202
    BEGIN
      SET @error_count := @error_count + 1;
      SELECT 'Could not create slave thread; check system resources' AS 'ERROR';
    END
  ;
  
  DECLARE CONTINUE HANDLER FOR 1203
    BEGIN
      SET @error_count := @error_count + 1;
      SELECT 'User %s already has more than ''max_user_connections'' active connections' AS 'ERROR';
    END
  ;
  
  DECLARE CONTINUE HANDLER FOR 1204
    BEGIN
      SET @error_count := @error_count + 1;
      SELECT 'You may only use constant expressions with SET' AS 'ERROR';
    END
  ;
  
  DECLARE CONTINUE HANDLER FOR 1205
    BEGIN
      SET @error_count := @error_count + 1;
      SELECT 'Lock wait timeout exceeded; try restarting transaction' AS 'ERROR';
    END
  ;
  
  DECLARE CONTINUE HANDLER FOR 1206
    BEGIN
      SET @error_count := @error_count + 1;
      SELECT 'The total number of locks exceeds the lock table size' AS 'ERROR';
    END
  ;
  
  DECLARE CONTINUE HANDLER FOR 1207
    BEGIN
      SET @error_count := @error_count + 1;
      SELECT 'Update locks cannot be acquired during a READ UNCOMMITTED transaction' AS 'ERROR';
    END
  ;
  
  DECLARE CONTINUE HANDLER FOR 1208
    BEGIN
      SET @error_count := @error_count + 1;
      SELECT 'DROP DATABASE not allowed while thread is holding global read lock' AS 'ERROR';
    END
  ;
  
  DECLARE CONTINUE HANDLER FOR 1209
    BEGIN
      SET @error_count := @error_count + 1;
      SELECT 'CREATE DATABASE not allowed while thread is holding global read lock' AS 'ERROR';
    END
  ;
  
  DECLARE CONTINUE HANDLER FOR 1210
    BEGIN
      SET @error_count := @error_count + 1;
      SELECT 'Incorrect arguments to %s' AS 'ERROR';
    END
  ;
  
  DECLARE CONTINUE HANDLER FOR 1211
    BEGIN
      SET @error_count := @error_count + 1;
      SELECT '''%s''@''%s'' is not allowed to create new users' AS 'ERROR';
    END
  ;
  
  DECLARE CONTINUE HANDLER FOR 1212
    BEGIN
      SET @error_count := @error_count + 1;
      SELECT 'Incorrect table definition; all MERGE tables must be in the same database' AS 'ERROR';
    END
  ;
  
  DECLARE CONTINUE HANDLER FOR 1213
    BEGIN
      SET @error_count := @error_count + 1;
      SELECT 'Deadlock found when trying to get lock; try restarting transaction' AS 'ERROR';
    END
  ;
  
  DECLARE CONTINUE HANDLER FOR 1214
    BEGIN
      SET @error_count := @error_count + 1;
      SELECT 'The used table type doesn''t support FULLTEXT indexes' AS 'ERROR';
    END
  ;
  
  DECLARE CONTINUE HANDLER FOR 1215
    BEGIN
      SET @error_count := @error_count + 1;
      SELECT 'Cannot add foreign key constraint' AS 'ERROR';
    END
  ;
  
  DECLARE CONTINUE HANDLER FOR 1216
    BEGIN
      SET @error_count := @error_count + 1;
      SELECT 'Cannot add or update a child row: a foreign key constraint fails' AS 'ERROR';
    END
  ;
  
  DECLARE CONTINUE HANDLER FOR 1217
    BEGIN
      SET @error_count := @error_count + 1;
      SELECT 'Cannot delete or update a parent row: a foreign key constraint fails' AS 'ERROR';
    END
  ;
  
  DECLARE CONTINUE HANDLER FOR 1218
    BEGIN
      SET @error_count := @error_count + 1;
      SELECT 'Error connecting to master: %s' AS 'ERROR';
    END
  ;
  
  DECLARE CONTINUE HANDLER FOR 1219
    BEGIN
      SET @error_count := @error_count + 1;
      SELECT 'Error running query on master: %s' AS 'ERROR';
    END
  ;
  
  DECLARE CONTINUE HANDLER FOR 1220
    BEGIN
      SET @error_count := @error_count + 1;
      SELECT 'Error when executing command %s: %s' AS 'ERROR';
    END
  ;
  
  DECLARE CONTINUE HANDLER FOR 1221
    BEGIN
      SET @error_count := @error_count + 1;
      SELECT 'Incorrect usage of %s and %s' AS 'ERROR';
    END
  ;
  
  DECLARE CONTINUE HANDLER FOR 1222
    BEGIN
      SET @error_count := @error_count + 1;
      SELECT 'The used SELECT statements have a different number of columns' AS 'ERROR';
    END
  ;
  
  DECLARE CONTINUE HANDLER FOR 1223
    BEGIN
      SET @error_count := @error_count + 1;
      SELECT 'Can''t execute the query because you have a conflicting read lock' AS 'ERROR';
    END
  ;
  
  DECLARE CONTINUE HANDLER FOR 1224
    BEGIN
      SET @error_count := @error_count + 1;
      SELECT 'Mixing of transactional and non-transactional tables is disabled' AS 'ERROR';
    END
  ;
  
  DECLARE CONTINUE HANDLER FOR 1225
    BEGIN
      SET @error_count := @error_count + 1;
      SELECT 'Option ''%s'' used twice in statement' AS 'ERROR';
    END
  ;
  
  DECLARE CONTINUE HANDLER FOR 1226
    BEGIN
      SET @error_count := @error_count + 1;
      SELECT 'User ''%s'' has exceeded the ''%s'' resource (current value: %ld)' AS 'ERROR';
    END
  ;
  
  DECLARE CONTINUE HANDLER FOR 1227
    BEGIN
      SET @error_count := @error_count + 1;
      SELECT 'Access denied; you need the %s privilege for this operation' AS 'ERROR';
    END
  ;
  
  DECLARE CONTINUE HANDLER FOR 1228
    BEGIN
      SET @error_count := @error_count + 1;
      SELECT 'Variable ''%s'' is a SESSION variable and can''t be used with SET GLOBAL' AS 'ERROR';
    END
  ;
  
  DECLARE CONTINUE HANDLER FOR 1229
    BEGIN
      SET @error_count := @error_count + 1;
      SELECT 'Variable ''%s'' is a GLOBAL variable and should be set with SET GLOBAL' AS 'ERROR';
    END
  ;
  
  DECLARE CONTINUE HANDLER FOR 1230
    BEGIN
      SET @error_count := @error_count + 1;
      SELECT 'Variable ''%s'' doesn''t have a default value' AS 'ERROR';
    END
  ;
  
  DECLARE CONTINUE HANDLER FOR 1231
    BEGIN
      SET @error_count := @error_count + 1;
      SELECT 'Variable ''%s'' can''t be set to the value of ''%s''' AS 'ERROR';
    END
  ;
  
  DECLARE CONTINUE HANDLER FOR 1232
    BEGIN
      SET @error_count := @error_count + 1;
      SELECT 'Incorrect argument type to variable ''%s''' AS 'ERROR';
    END
  ;
  
  DECLARE CONTINUE HANDLER FOR 1233
    BEGIN
      SET @error_count := @error_count + 1;
      SELECT 'Variable ''%s'' can only be set, not read' AS 'ERROR';
    END
  ;
  
  DECLARE CONTINUE HANDLER FOR 1234
    BEGIN
      SET @error_count := @error_count + 1;
      SELECT 'Incorrect usage/placement of ''%s''' AS 'ERROR';
    END
  ;
  
  DECLARE CONTINUE HANDLER FOR 1235
    BEGIN
      SET @error_count := @error_count + 1;
      SELECT 'This version of MySQL doesn''t yet support ''%s''' AS 'ERROR';
    END
  ;
  
  DECLARE CONTINUE HANDLER FOR 1236
    BEGIN
      SET @error_count := @error_count + 1;
      SELECT 'Got fatal error %d from master when reading data from binary log: ''%s''' AS 'ERROR';
    END
  ;
  
  DECLARE CONTINUE HANDLER FOR 1237
    BEGIN
      SET @error_count := @error_count + 1;
      SELECT 'Slave SQL thread ignored the query because of replicate-*-table rules' AS 'ERROR';
    END
  ;
  
  DECLARE CONTINUE HANDLER FOR 1238
    BEGIN
      SET @error_count := @error_count + 1;
      SELECT 'Variable ''%s'' is a %s variable' AS 'ERROR';
    END
  ;
  
  DECLARE CONTINUE HANDLER FOR 1239
    BEGIN
      SET @error_count := @error_count + 1;
      SELECT 'Incorrect foreign key definition for ''%s'': %s' AS 'ERROR';
    END
  ;
  
  DECLARE CONTINUE HANDLER FOR 1240
    BEGIN
      SET @error_count := @error_count + 1;
      SELECT 'Key reference and table reference don''t match' AS 'ERROR';
    END
  ;
  
  DECLARE CONTINUE HANDLER FOR 1241
    BEGIN
      SET @error_count := @error_count + 1;
      SELECT 'Operand should contain %d column(s)' AS 'ERROR';
    END
  ;
  
  DECLARE CONTINUE HANDLER FOR 1242
    BEGIN
      SET @error_count := @error_count + 1;
      SELECT 'Subquery returns more than 1 row' AS 'ERROR';
    END
  ;
  
  DECLARE CONTINUE HANDLER FOR 1243
    BEGIN
      SET @error_count := @error_count + 1;
      SELECT 'Unknown prepared statement handler (%.*s) given to %s' AS 'ERROR';
    END
  ;
  
  DECLARE CONTINUE HANDLER FOR 1244
    BEGIN
      SET @error_count := @error_count + 1;
      SELECT 'Help database is corrupt or does not exist' AS 'ERROR';
    END
  ;
  
  DECLARE CONTINUE HANDLER FOR 1245
    BEGIN
      SET @error_count := @error_count + 1;
      SELECT 'Cyclic reference on subqueries' AS 'ERROR';
    END
  ;
  
  DECLARE CONTINUE HANDLER FOR 1246
    BEGIN
      SET @error_count := @error_count + 1;
      SELECT 'Converting column ''%s'' from %s to %s' AS 'ERROR';
    END
  ;
  
  DECLARE CONTINUE HANDLER FOR 1247
    BEGIN
      SET @error_count := @error_count + 1;
      SELECT 'Reference ''%s'' not supported (%s)' AS 'ERROR';
    END
  ;
  
  DECLARE CONTINUE HANDLER FOR 1248
    BEGIN
      SET @error_count := @error_count + 1;
      SELECT 'Every derived table must have its own alias' AS 'ERROR';
    END
  ;
  
  DECLARE CONTINUE HANDLER FOR 1249
    BEGIN
      SET @error_count := @error_count + 1;
      SELECT 'Select %u was reduced during optimization' AS 'ERROR';
    END
  ;
  
  DECLARE CONTINUE HANDLER FOR 1250
    BEGIN
      SET @error_count := @error_count + 1;
      SELECT 'Table ''%s'' from one of the SELECTs cannot be used in %s' AS 'ERROR';
    END
  ;
  
  DECLARE CONTINUE HANDLER FOR 1251
    BEGIN
      SET @error_count := @error_count + 1;
      SELECT 'Client does not support authentication protocol requested by server; consider upgrading MySQL client' AS 'ERROR';
    END
  ;
  
  DECLARE CONTINUE HANDLER FOR 1252
    BEGIN
      SET @error_count := @error_count + 1;
      SELECT 'All parts of a SPATIAL index must be NOT NULL' AS 'ERROR';
    END
  ;
  
  DECLARE CONTINUE HANDLER FOR 1253
    BEGIN
      SET @error_count := @error_count + 1;
      SELECT 'COLLATION ''%s'' is not valid for CHARACTER SET ''%s''' AS 'ERROR';
    END
  ;
  
  DECLARE CONTINUE HANDLER FOR 1254
    BEGIN
      SET @error_count := @error_count + 1;
      SELECT 'Slave is already running' AS 'ERROR';
    END
  ;
  
  DECLARE CONTINUE HANDLER FOR 1255
    BEGIN
      SET @error_count := @error_count + 1;
      SELECT 'Slave already has been stopped' AS 'ERROR';
    END
  ;
  
  DECLARE CONTINUE HANDLER FOR 1256
    BEGIN
      SET @error_count := @error_count + 1;
      SELECT 'Uncompressed data size too large; the maximum size is %d (probably, length of uncompressed data was corrupted)' AS 'ERROR';
    END
  ;
  
  DECLARE CONTINUE HANDLER FOR 1257
    BEGIN
      SET @error_count := @error_count + 1;
      SELECT 'ZLIB: Not enough memory' AS 'ERROR';
    END
  ;
  
  DECLARE CONTINUE HANDLER FOR 1258
    BEGIN
      SET @error_count := @error_count + 1;
      SELECT 'ZLIB: Not enough room in the output buffer (probably, length of uncompressed data was corrupted)' AS 'ERROR';
    END
  ;
  
  DECLARE CONTINUE HANDLER FOR 1259
    BEGIN
      SET @error_count := @error_count + 1;
      SELECT 'ZLIB: Input data corrupted' AS 'ERROR';
    END
  ;
  
  DECLARE CONTINUE HANDLER FOR 1260
    BEGIN
      SET @error_count := @error_count + 1;
      SELECT '%d line(s) were cut by GROUP_CONCAT()' AS 'ERROR';
    END
  ;
  
  DECLARE CONTINUE HANDLER FOR 1261
    BEGIN
      SET @error_count := @error_count + 1;
      SELECT 'Row %ld doesn''t contain data for all columns' AS 'ERROR';
    END
  ;
  
  DECLARE CONTINUE HANDLER FOR 1262
    BEGIN
      SET @error_count := @error_count + 1;
      SELECT 'Row %ld was truncated; it contained more data than there were input columns' AS 'ERROR';
    END
  ;
  
  DECLARE CONTINUE HANDLER FOR 1263
    BEGIN
      SET @error_count := @error_count + 1;
      SELECT 'Column set to default value; NULL supplied to NOT NULL column ''%s'' at row %ld' AS 'ERROR';
    END
  ;
  
  DECLARE CONTINUE HANDLER FOR 1264
    BEGIN
      SET @error_count := @error_count + 1;
      SELECT 'Out of range value for column ''%s'' at row %ld' AS 'ERROR';
    END
  ;
  
  DECLARE CONTINUE HANDLER FOR 1265
    BEGIN
      SET @error_count := @error_count + 1;
      SELECT 'Data truncated for column ''%s'' at row %ld' AS 'ERROR';
    END
  ;
  
  DECLARE CONTINUE HANDLER FOR 1266
    BEGIN
      SET @error_count := @error_count + 1;
      SELECT 'Using storage engine %s for table ''%s''' AS 'ERROR';
    END
  ;
  
  DECLARE CONTINUE HANDLER FOR 1267
    BEGIN
      SET @error_count := @error_count + 1;
      SELECT 'Illegal mix of collations (%s,%s) and (%s,%s) for operation ''%s''' AS 'ERROR';
    END
  ;
  
  DECLARE CONTINUE HANDLER FOR 1268
    BEGIN
      SET @error_count := @error_count + 1;
      SELECT 'Cannot drop one or more of the requested users' AS 'ERROR';
    END
  ;
  
  DECLARE CONTINUE HANDLER FOR 1269
    BEGIN
      SET @error_count := @error_count + 1;
      SELECT 'Can''t revoke all privileges for one or more of the requested users' AS 'ERROR';
    END
  ;
  
  DECLARE CONTINUE HANDLER FOR 1270
    BEGIN
      SET @error_count := @error_count + 1;
      SELECT 'Illegal mix of collations (%s,%s), (%s,%s), (%s,%s) for operation ''%s''' AS 'ERROR';
    END
  ;
  
  DECLARE CONTINUE HANDLER FOR 1271
    BEGIN
      SET @error_count := @error_count + 1;
      SELECT 'Illegal mix of collations for operation ''%s''' AS 'ERROR';
    END
  ;
  
  DECLARE CONTINUE HANDLER FOR 1272
    BEGIN
      SET @error_count := @error_count + 1;
      SELECT 'Variable ''%s'' is not a variable component (can''t be used as XXXX.variable_name)' AS 'ERROR';
    END
  ;
  
  DECLARE CONTINUE HANDLER FOR 1273
    BEGIN
      SET @error_count := @error_count + 1;
      SELECT 'Unknown collation: ''%s''' AS 'ERROR';
    END
  ;
  
  DECLARE CONTINUE HANDLER FOR 1274
    BEGIN
      SET @error_count := @error_count + 1;
      SELECT 'SSL parameters in CHANGE MASTER are ignored because this MySQL slave was compiled without SSL support; they can be used later if MySQL slave with SSL is started' AS 'ERROR';
    END
  ;
  
  DECLARE CONTINUE HANDLER FOR 1275
    BEGIN
      SET @error_count := @error_count + 1;
      SELECT 'Server is running in --secure-auth mode, but ''%s''@''%s'' has a password in the old format; please change the password to the new format' AS 'ERROR';
    END
  ;
  
  DECLARE CONTINUE HANDLER FOR 1276
    BEGIN
      SET @error_count := @error_count + 1;
      SELECT 'Field or reference ''%s%s%s%s%s'' of SELECT #%d was resolved in SELECT #%d' AS 'ERROR';
    END
  ;
  
  DECLARE CONTINUE HANDLER FOR 1277
    BEGIN
      SET @error_count := @error_count + 1;
      SELECT 'Incorrect parameter or combination of parameters for START SLAVE UNTIL' AS 'ERROR';
    END
  ;
  
  DECLARE CONTINUE HANDLER FOR 1278
    BEGIN
      SET @error_count := @error_count + 1;
      SELECT 'It is recommended to use --skip-slave-start when doing step-by-step replication with START SLAVE UNTIL; otherwise, you will get problems if you get an unexpected slave''s mysqld restart' AS 'ERROR';
    END
  ;
  
  DECLARE CONTINUE HANDLER FOR 1279
    BEGIN
      SET @error_count := @error_count + 1;
      SELECT 'SQL thread is not to be started so UNTIL options are ignored' AS 'ERROR';
    END
  ;
  
  DECLARE CONTINUE HANDLER FOR 1280
    BEGIN
      SET @error_count := @error_count + 1;
      SELECT 'Incorrect index name ''%s''' AS 'ERROR';
    END
  ;
  
  DECLARE CONTINUE HANDLER FOR 1281
    BEGIN
      SET @error_count := @error_count + 1;
      SELECT 'Incorrect catalog name ''%s''' AS 'ERROR';
    END
  ;
  
  DECLARE CONTINUE HANDLER FOR 1282
    BEGIN
      SET @error_count := @error_count + 1;
      SELECT 'Query cache failed to set size %lu; new query cache size is %lu' AS 'ERROR';
    END
  ;
  
  DECLARE CONTINUE HANDLER FOR 1283
    BEGIN
      SET @error_count := @error_count + 1;
      SELECT 'Column ''%s'' cannot be part of FULLTEXT index' AS 'ERROR';
    END
  ;
  
  DECLARE CONTINUE HANDLER FOR 1284
    BEGIN
      SET @error_count := @error_count + 1;
      SELECT 'Unknown key cache ''%s''' AS 'ERROR';
    END
  ;
  
  DECLARE CONTINUE HANDLER FOR 1285
    BEGIN
      SET @error_count := @error_count + 1;
      SELECT 'MySQL is started in --skip-name-resolve mode; you must restart it without this switch for this grant to work' AS 'ERROR';
    END
  ;
  
  DECLARE CONTINUE HANDLER FOR 1286
    BEGIN
      SET @error_count := @error_count + 1;
      SELECT 'Unknown table engine ''%s''' AS 'ERROR';
    END
  ;
  
  DECLARE CONTINUE HANDLER FOR 1287
    BEGIN
      SET @error_count := @error_count + 1;
      SELECT '''%s'' is deprecated and will be removed in a future release. Please use %s instead' AS 'ERROR';
    END
  ;
  
  DECLARE CONTINUE HANDLER FOR 1288
    BEGIN
      SET @error_count := @error_count + 1;
      SELECT 'The target table %s of the %s is not updatable' AS 'ERROR';
    END
  ;
  
  DECLARE CONTINUE HANDLER FOR 1289
    BEGIN
      SET @error_count := @error_count + 1;
      SELECT 'The ''%s'' feature is disabled; you need MySQL built with ''%s'' to have it working' AS 'ERROR';
    END
  ;
  
  DECLARE CONTINUE HANDLER FOR 1290
    BEGIN
      SET @error_count := @error_count + 1;
      SELECT 'The MySQL server is running with the %s option so it cannot execute this statement' AS 'ERROR';
    END
  ;
  
  DECLARE CONTINUE HANDLER FOR 1291
    BEGIN
      SET @error_count := @error_count + 1;
      SELECT 'Column ''%s'' has duplicated value ''%s'' in %s' AS 'ERROR';
    END
  ;
  
  DECLARE CONTINUE HANDLER FOR 1292
    BEGIN
      SET @error_count := @error_count + 1;
      SELECT 'Truncated incorrect %s value: ''%s''' AS 'ERROR';
    END
  ;
  
  DECLARE CONTINUE HANDLER FOR 1293
    BEGIN
      SET @error_count := @error_count + 1;
      SELECT 'Incorrect table definition; there can be only one TIMESTAMP column with CURRENT_TIMESTAMP in DEFAULT or ON UPDATE clause' AS 'ERROR';
    END
  ;
  
  DECLARE CONTINUE HANDLER FOR 1294
    BEGIN
      SET @error_count := @error_count + 1;
      SELECT 'Invalid ON UPDATE clause for ''%s'' column' AS 'ERROR';
    END
  ;
  
  DECLARE CONTINUE HANDLER FOR 1295
    BEGIN
      SET @error_count := @error_count + 1;
      SELECT 'This command is not supported in the prepared statement protocol yet' AS 'ERROR';
    END
  ;
  
  DECLARE CONTINUE HANDLER FOR 1296
    BEGIN
      SET @error_count := @error_count + 1;
      SELECT 'Got error %d ''%s'' from %s' AS 'ERROR';
    END
  ;
  
  DECLARE CONTINUE HANDLER FOR 1297
    BEGIN
      SET @error_count := @error_count + 1;
      SELECT 'Got temporary error %d ''%s'' from %s' AS 'ERROR';
    END
  ;
  
  DECLARE CONTINUE HANDLER FOR 1298
    BEGIN
      SET @error_count := @error_count + 1;
      SELECT 'Unknown or incorrect time zone: ''%s''' AS 'ERROR';
    END
  ;
  
  DECLARE CONTINUE HANDLER FOR 1299
    BEGIN
      SET @error_count := @error_count + 1;
      SELECT 'Invalid TIMESTAMP value in column ''%s'' at row %ld' AS 'ERROR';
    END
  ;
  
  DECLARE CONTINUE HANDLER FOR 1300
    BEGIN
      SET @error_count := @error_count + 1;
      SELECT 'Invalid %s character string: ''%s''' AS 'ERROR';
    END
  ;
  
  DECLARE CONTINUE HANDLER FOR 1301
    BEGIN
      SET @error_count := @error_count + 1;
      SELECT 'Result of %s() was larger than max_allowed_packet (%ld) - truncated' AS 'ERROR';
    END
  ;
  
  DECLARE CONTINUE HANDLER FOR 1302
    BEGIN
      SET @error_count := @error_count + 1;
      SELECT 'Conflicting declarations: ''%s%s'' and ''%s%s''' AS 'ERROR';
    END
  ;
  
  DECLARE CONTINUE HANDLER FOR 1303
    BEGIN
      SET @error_count := @error_count + 1;
      SELECT 'Can''t create a %s from within another stored routine' AS 'ERROR';
    END
  ;
  
  DECLARE CONTINUE HANDLER FOR 1304
    BEGIN
      SET @error_count := @error_count + 1;
      SELECT '%s %s already exists' AS 'ERROR';
    END
  ;
  
--  DECLARE CONTINUE HANDLER FOR 1305
--    BEGIN
--      -- SET @error_count := @error_count + 1;
--      -- SELECT '%s %s does not exist' AS 'ERROR';
--      -- Skipping this error as it is raised by testing procedures and we don't
--      -- want these procedures to stop the process!
--      SELECT 'Skipping "doesn''t exist" error' AS 'WARNING';
--    END
--  ;
  
  DECLARE CONTINUE HANDLER FOR 1306
    BEGIN
      SET @error_count := @error_count + 1;
      SELECT 'Failed to DROP %s %s' AS 'ERROR';
    END
  ;
  
  DECLARE CONTINUE HANDLER FOR 1307
    BEGIN
      SET @error_count := @error_count + 1;
      SELECT 'Failed to CREATE %s %s' AS 'ERROR';
    END
  ;
  
  DECLARE CONTINUE HANDLER FOR 1308
    BEGIN
      SET @error_count := @error_count + 1;
      SELECT '%s with no matching label: %s' AS 'ERROR';
    END
  ;
  
  DECLARE CONTINUE HANDLER FOR 1309
    BEGIN
      SET @error_count := @error_count + 1;
      SELECT 'Redefining label %s' AS 'ERROR';
    END
  ;
  
  DECLARE CONTINUE HANDLER FOR 1310
    BEGIN
      SET @error_count := @error_count + 1;
      SELECT 'End-label %s without match' AS 'ERROR';
    END
  ;
  
  DECLARE CONTINUE HANDLER FOR 1311
    BEGIN
      SET @error_count := @error_count + 1;
      SELECT 'Referring to uninitialized variable %s' AS 'ERROR';
    END
  ;
  
  DECLARE CONTINUE HANDLER FOR 1312
    BEGIN
      SET @error_count := @error_count + 1;
      SELECT 'PROCEDURE %s can''t return a result set in the given context' AS 'ERROR';
    END
  ;
  
  DECLARE CONTINUE HANDLER FOR 1313
    BEGIN
      SET @error_count := @error_count + 1;
      SELECT 'RETURN is only allowed in a FUNCTION' AS 'ERROR';
    END
  ;
  
  DECLARE CONTINUE HANDLER FOR 1314
    BEGIN
      SET @error_count := @error_count + 1;
      SELECT '%s is not allowed in stored procedures' AS 'ERROR';
    END
  ;
  
  DECLARE CONTINUE HANDLER FOR 1315
    BEGIN
      SET @error_count := @error_count + 1;
      SELECT 'The update log is deprecated and replaced by the binary log; SET SQL_LOG_UPDATE has been ignored. This option will be removed in MySQL 5.6.' AS 'ERROR';
    END
  ;
  
  DECLARE CONTINUE HANDLER FOR 1316
    BEGIN
      SET @error_count := @error_count + 1;
      SELECT 'The update log is deprecated and replaced by the binary log; SET SQL_LOG_UPDATE has been translated to SET SQL_LOG_BIN. This option will be removed in MySQL 5.6.' AS 'ERROR';
    END
  ;
  
  DECLARE CONTINUE HANDLER FOR 1317
    BEGIN
      SET @error_count := @error_count + 1;
      SELECT 'Query execution was interrupted' AS 'ERROR';
    END
  ;
  
  DECLARE CONTINUE HANDLER FOR 1318
    BEGIN
      SET @error_count := @error_count + 1;
      SELECT 'Incorrect number of arguments for %s %s; expected %u, got %u' AS 'ERROR';
    END
  ;
  
  DECLARE CONTINUE HANDLER FOR 1319
    BEGIN
      SET @error_count := @error_count + 1;
      SELECT 'Undefined CONDITION: %s' AS 'ERROR';
    END
  ;
  
  DECLARE CONTINUE HANDLER FOR 1320
    BEGIN
      SET @error_count := @error_count + 1;
      SELECT 'No RETURN found in FUNCTION %s' AS 'ERROR';
    END
  ;
  
  DECLARE CONTINUE HANDLER FOR 1321
    BEGIN
      SET @error_count := @error_count + 1;
      SELECT 'FUNCTION %s ended without RETURN' AS 'ERROR';
    END
  ;
  
  DECLARE CONTINUE HANDLER FOR 1322
    BEGIN
      SET @error_count := @error_count + 1;
      SELECT 'Cursor statement must be a SELECT' AS 'ERROR';
    END
  ;
  
  DECLARE CONTINUE HANDLER FOR 1323
    BEGIN
      SET @error_count := @error_count + 1;
      SELECT 'Cursor SELECT must not have INTO' AS 'ERROR';
    END
  ;
  
  DECLARE CONTINUE HANDLER FOR 1324
    BEGIN
      SET @error_count := @error_count + 1;
      SELECT 'Undefined CURSOR: %s' AS 'ERROR';
    END
  ;
  
  DECLARE CONTINUE HANDLER FOR 1325
    BEGIN
      SET @error_count := @error_count + 1;
      SELECT 'Cursor is already open' AS 'ERROR';
    END
  ;
  
  DECLARE CONTINUE HANDLER FOR 1326
    BEGIN
      SET @error_count := @error_count + 1;
      SELECT 'Cursor is not open' AS 'ERROR';
    END
  ;
  
  DECLARE CONTINUE HANDLER FOR 1327
    BEGIN
      SET @error_count := @error_count + 1;
      SELECT 'Un
  DECLAREd variable: %s' AS 'ERROR';
    END
  ;
  
  DECLARE CONTINUE HANDLER FOR 1328
    BEGIN
      SET @error_count := @error_count + 1;
      SELECT 'Incorrect number of FETCH variables' AS 'ERROR';
    END
  ;
  
  DECLARE CONTINUE HANDLER FOR 1329
    BEGIN
      SET @error_count := @error_count + 1;
      SELECT 'No data - zero rows fetched, selected, or processed' AS 'ERROR';
    END
  ;
  
  DECLARE CONTINUE HANDLER FOR 1330
    BEGIN
      SET @error_count := @error_count + 1;
      SELECT 'Duplicate parameter: %s' AS 'ERROR';
    END
  ;
  
  DECLARE CONTINUE HANDLER FOR 1331
    BEGIN
      SET @error_count := @error_count + 1;
      SELECT 'Duplicate variable: %s' AS 'ERROR';
    END
  ;
  
  DECLARE CONTINUE HANDLER FOR 1332
    BEGIN
      SET @error_count := @error_count + 1;
      SELECT 'Duplicate condition: %s' AS 'ERROR';
    END
  ;
  
  DECLARE CONTINUE HANDLER FOR 1333
    BEGIN
      SET @error_count := @error_count + 1;
      SELECT 'Duplicate cursor: %s' AS 'ERROR';
    END
  ;
  
  DECLARE CONTINUE HANDLER FOR 1334
    BEGIN
      SET @error_count := @error_count + 1;
      SELECT 'Failed to ALTER %s %s' AS 'ERROR';
    END
  ;
  
  DECLARE CONTINUE HANDLER FOR 1335
    BEGIN
      SET @error_count := @error_count + 1;
      SELECT 'Subquery value not supported' AS 'ERROR';
    END
  ;
  
  DECLARE CONTINUE HANDLER FOR 1336
    BEGIN
      SET @error_count := @error_count + 1;
      SELECT '%s is not allowed in stored function or trigger' AS 'ERROR';
    END
  ;
  
  DECLARE CONTINUE HANDLER FOR 1337
    BEGIN
      SET @error_count := @error_count + 1;
      SELECT 'Variable or condition declaration after cursor or handler declaration' AS 'ERROR';
    END
  ;
  
  DECLARE CONTINUE HANDLER FOR 1338
    BEGIN
      SET @error_count := @error_count + 1;
      SELECT 'Cursor declaration after handler declaration' AS 'ERROR';
    END
  ;
  
  DECLARE CONTINUE HANDLER FOR 1339
    BEGIN
      SET @error_count := @error_count + 1;
      SELECT 'Case not found for CASE statement' AS 'ERROR';
    END
  ;
  
  DECLARE CONTINUE HANDLER FOR 1340
    BEGIN
      SET @error_count := @error_count + 1;
      SELECT 'Configuration file ''%s'' is too big' AS 'ERROR';
    END
  ;
  
  DECLARE CONTINUE HANDLER FOR 1341
    BEGIN
      SET @error_count := @error_count + 1;
      SELECT 'Malformed file type header in file ''%s''' AS 'ERROR';
    END
  ;
  
  DECLARE CONTINUE HANDLER FOR 1342
    BEGIN
      SET @error_count := @error_count + 1;
      SELECT 'Unexpected end of file while parsing comment ''%s''' AS 'ERROR';
    END
  ;
  
  DECLARE CONTINUE HANDLER FOR 1343
    BEGIN
      SET @error_count := @error_count + 1;
      SELECT 'Error while parsing parameter ''%s'' (line: ''%s'')' AS 'ERROR';
    END
  ;
  
  DECLARE CONTINUE HANDLER FOR 1344
    BEGIN
      SET @error_count := @error_count + 1;
      SELECT 'Unexpected end of file while skipping unknown parameter ''%s''' AS 'ERROR';
    END
  ;
  
  DECLARE CONTINUE HANDLER FOR 1345
    BEGIN
      SET @error_count := @error_count + 1;
      SELECT 'EXPLAIN/SHOW can not be issued; lacking privileges for underlying table' AS 'ERROR';
    END
  ;
  
  DECLARE CONTINUE HANDLER FOR 1346
    BEGIN
      SET @error_count := @error_count + 1;
      SELECT 'File ''%s'' has unknown type ''%s'' in its header' AS 'ERROR';
    END
  ;
  
  DECLARE CONTINUE HANDLER FOR 1347
    BEGIN
      SET @error_count := @error_count + 1;
      SELECT '''%s.%s'' is not %s' AS 'ERROR';
    END
  ;
  
  DECLARE CONTINUE HANDLER FOR 1348
    BEGIN
      SET @error_count := @error_count + 1;
      SELECT 'Column ''%s'' is not updatable' AS 'ERROR';
    END
  ;
  
  DECLARE CONTINUE HANDLER FOR 1349
    BEGIN
      SET @error_count := @error_count + 1;
      SELECT 'View''s SELECT contains a subquery in the FROM clause' AS 'ERROR';
    END
  ;
  
  DECLARE CONTINUE HANDLER FOR 1350
    BEGIN
      SET @error_count := @error_count + 1;
      SELECT 'View''s SELECT contains a ''%s'' clause' AS 'ERROR';
    END
  ;
  
  DECLARE CONTINUE HANDLER FOR 1351
    BEGIN
      SET @error_count := @error_count + 1;
      SELECT 'View''s SELECT contains a variable or parameter' AS 'ERROR';
    END
  ;
  
  DECLARE CONTINUE HANDLER FOR 1352
    BEGIN
      SET @error_count := @error_count + 1;
      SELECT 'View''s SELECT refers to a temporary table ''%s''' AS 'ERROR';
    END
  ;
  
  DECLARE CONTINUE HANDLER FOR 1353
    BEGIN
      SET @error_count := @error_count + 1;
      SELECT 'View''s SELECT and view''s field list have different column counts' AS 'ERROR';
    END
  ;
  
  DECLARE CONTINUE HANDLER FOR 1354
    BEGIN
      SET @error_count := @error_count + 1;
      SELECT 'View merge algorithm can''t be used here for now (assumed undefined algorithm)' AS 'ERROR';
    END
  ;
  
  DECLARE CONTINUE HANDLER FOR 1355
    BEGIN
      SET @error_count := @error_count + 1;
      SELECT 'View being updated does not have complete key of underlying table in it' AS 'ERROR';
    END
  ;
  
  DECLARE CONTINUE HANDLER FOR 1356
    BEGIN
      SET @error_count := @error_count + 1;
      SELECT 'View ''%s.%s'' references invalid table(s) or column(s) or function(s) or definer/invoker of view lack rights to use them' AS 'ERROR';
    END
  ;
  
  DECLARE CONTINUE HANDLER FOR 1357
    BEGIN
      SET @error_count := @error_count + 1;
      SELECT 'Can''t drop or alter a %s from within another stored routine' AS 'ERROR';
    END
  ;
  
  DECLARE CONTINUE HANDLER FOR 1358
    BEGIN
      SET @error_count := @error_count + 1;
      SELECT 'GOTO is not allowed in a stored procedure handler' AS 'ERROR';
    END
  ;
  
  DECLARE CONTINUE HANDLER FOR 1359
    BEGIN
      SET @error_count := @error_count + 1;
      SELECT 'Trigger already exists' AS 'ERROR';
    END
  ;
  
  DECLARE CONTINUE HANDLER FOR 1360
    BEGIN
      SET @error_count := @error_count + 1;
      SELECT 'Trigger does not exist' AS 'ERROR';
    END
  ;
  
  DECLARE CONTINUE HANDLER FOR 1361
    BEGIN
      SET @error_count := @error_count + 1;
      SELECT 'Trigger''s ''%s'' is view or temporary table' AS 'ERROR';
    END
  ;
  
  DECLARE CONTINUE HANDLER FOR 1362
    BEGIN
      SET @error_count := @error_count + 1;
      SELECT 'Updating of %s row is not allowed in %strigger' AS 'ERROR';
    END
  ;
  
  DECLARE CONTINUE HANDLER FOR 1363
    BEGIN
      SET @error_count := @error_count + 1;
      SELECT 'There is no %s row in %s trigger' AS 'ERROR';
    END
  ;
  
  DECLARE CONTINUE HANDLER FOR 1364
    BEGIN
      SET @error_count := @error_count + 1;
      SELECT 'Field ''%s'' doesn''t have a default value' AS 'ERROR';
    END
  ;
  
  DECLARE CONTINUE HANDLER FOR 1365
    BEGIN
      SET @error_count := @error_count + 1;
      SELECT 'Division by 0' AS 'ERROR';
    END
  ;
  
  DECLARE CONTINUE HANDLER FOR 1366
    BEGIN
      SET @error_count := @error_count + 1;
      SELECT 'Incorrect %s value: ''%s'' for column ''%s'' at row %ld' AS 'ERROR';
    END
  ;
  
  DECLARE CONTINUE HANDLER FOR 1367
    BEGIN
      SET @error_count := @error_count + 1;
      SELECT 'Illegal %s ''%s'' value found during parsing' AS 'ERROR';
    END
  ;
  
  DECLARE CONTINUE HANDLER FOR 1368
    BEGIN
      SET @error_count := @error_count + 1;
      SELECT 'CHECK OPTION on non-updatable view ''%s.%s''' AS 'ERROR';
    END
  ;
  
  DECLARE CONTINUE HANDLER FOR 1369
    BEGIN
      SET @error_count := @error_count + 1;
      SELECT 'CHECK OPTION failed ''%s.%s''' AS 'ERROR';
    END
  ;
  
  DECLARE CONTINUE HANDLER FOR 1370
    BEGIN
      SET @error_count := @error_count + 1;
      SELECT '%s command denied to user ''%s''@''%s'' for routine ''%s''' AS 'ERROR';
    END
  ;
  
  DECLARE CONTINUE HANDLER FOR 1371
    BEGIN
      SET @error_count := @error_count + 1;
      SELECT 'Failed purging old relay logs: %s' AS 'ERROR';
    END
  ;
  
  DECLARE CONTINUE HANDLER FOR 1372
    BEGIN
      SET @error_count := @error_count + 1;
      SELECT 'Password hash should be a %d-digit hexadecimal number' AS 'ERROR';
    END
  ;
  
  DECLARE CONTINUE HANDLER FOR 1373
    BEGIN
      SET @error_count := @error_count + 1;
      SELECT 'Target log not found in binlog index' AS 'ERROR';
    END
  ;
  
  DECLARE CONTINUE HANDLER FOR 1374
    BEGIN
      SET @error_count := @error_count + 1;
      SELECT 'I/O error reading log index file' AS 'ERROR';
    END
  ;
  
  DECLARE CONTINUE HANDLER FOR 1375
    BEGIN
      SET @error_count := @error_count + 1;
      SELECT 'Server configuration does not permit binlog purge' AS 'ERROR';
    END
  ;
  
  DECLARE CONTINUE HANDLER FOR 1376
    BEGIN
      SET @error_count := @error_count + 1;
      SELECT 'Failed on fseek()' AS 'ERROR';
    END
  ;
  
  DECLARE CONTINUE HANDLER FOR 1377
    BEGIN
      SET @error_count := @error_count + 1;
      SELECT 'Fatal error during log purge' AS 'ERROR';
    END
  ;
  
  DECLARE CONTINUE HANDLER FOR 1378
    BEGIN
      SET @error_count := @error_count + 1;
      SELECT 'A purgeable log is in use, will not purge' AS 'ERROR';
    END
  ;
  
  DECLARE CONTINUE HANDLER FOR 1379
    BEGIN
      SET @error_count := @error_count + 1;
      SELECT 'Unknown error during log purge' AS 'ERROR';
    END
  ;
  
  DECLARE CONTINUE HANDLER FOR 1380
    BEGIN
      SET @error_count := @error_count + 1;
      SELECT 'Failed initializing relay log position: %s' AS 'ERROR';
    END
  ;
  
  DECLARE CONTINUE HANDLER FOR 1381
    BEGIN
      SET @error_count := @error_count + 1;
      SELECT 'You are not using binary logging' AS 'ERROR';
    END
  ;
  
  DECLARE CONTINUE HANDLER FOR 1382
    BEGIN
      SET @error_count := @error_count + 1;
      SELECT 'The ''%s'' syntax is reserved for purposes internal to the MySQL server' AS 'ERROR';
    END
  ;
  
  DECLARE CONTINUE HANDLER FOR 1383
    BEGIN
      SET @error_count := @error_count + 1;
      SELECT 'WSAStartup Failed' AS 'ERROR';
    END
  ;
  
  DECLARE CONTINUE HANDLER FOR 1384
    BEGIN
      SET @error_count := @error_count + 1;
      SELECT 'Can''t handle procedures with different groups yet' AS 'ERROR';
    END
  ;
  
  DECLARE CONTINUE HANDLER FOR 1385
    BEGIN
      SET @error_count := @error_count + 1;
      SELECT 'Select must have a group with this procedure' AS 'ERROR';
    END
  ;
  
  DECLARE CONTINUE HANDLER FOR 1386
    BEGIN
      SET @error_count := @error_count + 1;
      SELECT 'Can''t use ORDER clause with this procedure' AS 'ERROR';
    END
  ;
  
  DECLARE CONTINUE HANDLER FOR 1387
    BEGIN
      SET @error_count := @error_count + 1;
      SELECT 'Binary logging and replication forbid changing the global server %s' AS 'ERROR';
    END
  ;
  
  DECLARE CONTINUE HANDLER FOR 1388
    BEGIN
      SET @error_count := @error_count + 1;
      SELECT 'Can''t map file: %s, errno: %d' AS 'ERROR';
    END
  ;
  
  DECLARE CONTINUE HANDLER FOR 1389
    BEGIN
      SET @error_count := @error_count + 1;
      SELECT 'Wrong magic in %s' AS 'ERROR';
    END
  ;
  
  DECLARE CONTINUE HANDLER FOR 1390
    BEGIN
      SET @error_count := @error_count + 1;
      SELECT 'Prepared statement contains too many placeholders' AS 'ERROR';
    END
  ;
  
  DECLARE CONTINUE HANDLER FOR 1391
    BEGIN
      SET @error_count := @error_count + 1;
      SELECT 'Key part ''%s'' length cannot be 0' AS 'ERROR';
    END
  ;
  
  DECLARE CONTINUE HANDLER FOR 1392
    BEGIN
      SET @error_count := @error_count + 1;
      SELECT 'View text checksum failed' AS 'ERROR';
    END
  ;
  
  DECLARE CONTINUE HANDLER FOR 1393
    BEGIN
      SET @error_count := @error_count + 1;
      SELECT 'Can not modify more than one base table through a join view ''%s.%s''' AS 'ERROR';
    END
  ;
  
  DECLARE CONTINUE HANDLER FOR 1394
    BEGIN
      SET @error_count := @error_count + 1;
      SELECT 'Can not insert into join view ''%s.%s'' without fields list' AS 'ERROR';
    END
  ;
  
  DECLARE CONTINUE HANDLER FOR 1395
    BEGIN
      SET @error_count := @error_count + 1;
      SELECT 'Can not delete from join view ''%s.%s''' AS 'ERROR';
    END
  ;
  
  DECLARE CONTINUE HANDLER FOR 1396
    BEGIN
      SET @error_count := @error_count + 1;
      SELECT 'Operation %s failed for %s' AS 'ERROR';
    END
  ;
  
  DECLARE CONTINUE HANDLER FOR 1397
    BEGIN
      SET @error_count := @error_count + 1;
      SELECT 'XAER_NOTA: Unknown XID' AS 'ERROR';
    END
  ;
  
  DECLARE CONTINUE HANDLER FOR 1398
    BEGIN
      SET @error_count := @error_count + 1;
      SELECT 'XAER_INVAL: Invalid arguments (or unsupported command)' AS 'ERROR';
    END
  ;
  
  DECLARE CONTINUE HANDLER FOR 1399
    BEGIN
      SET @error_count := @error_count + 1;
      SELECT 'XAER_RMFAIL: The command cannot be executed when global transaction is in the %s state' AS 'ERROR';
    END
  ;
  
  DECLARE CONTINUE HANDLER FOR 1400
    BEGIN
      SET @error_count := @error_count + 1;
      SELECT 'XAER_OUTSIDE: Some work is done outside global transaction' AS 'ERROR';
    END
  ;
  
  DECLARE CONTINUE HANDLER FOR 1401
    BEGIN
      SET @error_count := @error_count + 1;
      SELECT 'XAER_RMERR: Fatal error occurred in the transaction branch - check your data for consistency' AS 'ERROR';
    END
  ;
  
  DECLARE CONTINUE HANDLER FOR 1402
    BEGIN
      SET @error_count := @error_count + 1;
      SELECT 'XA_RBROLLBACK: Transaction branch was rolled back' AS 'ERROR';
    END
  ;
  
  DECLARE CONTINUE HANDLER FOR 1403
    BEGIN
      SET @error_count := @error_count + 1;
      SELECT 'There is no such grant defined for user ''%s'' on host ''%s'' on routine ''%s''' AS 'ERROR';
    END
  ;
  
  DECLARE CONTINUE HANDLER FOR 1404
    BEGIN
      SET @error_count := @error_count + 1;
      SELECT 'Failed to grant EXECUTE and ALTER ROUTINE privileges' AS 'ERROR';
    END
  ;
  
  DECLARE CONTINUE HANDLER FOR 1405
    BEGIN
      SET @error_count := @error_count + 1;
      SELECT 'Failed to revoke all privileges to dropped routine' AS 'ERROR';
    END
  ;
  
  DECLARE CONTINUE HANDLER FOR 1406
    BEGIN
      SET @error_count := @error_count + 1;
      SELECT 'Data too long for column ''%s'' at row %ld' AS 'ERROR';
    END
  ;
  
  DECLARE CONTINUE HANDLER FOR 1407
    BEGIN
      SET @error_count := @error_count + 1;
      SELECT 'Bad SQLSTATE: ''%s''' AS 'ERROR';
    END
  ;
  
  DECLARE CONTINUE HANDLER FOR 1408
    BEGIN
      SET @error_count := @error_count + 1;
      SELECT '%s: ready for connections. Version: ''%s'' socket: ''%s'' port: %d %s' AS 'ERROR';
    END
  ;
  
  DECLARE CONTINUE HANDLER FOR 1409
    BEGIN
      SET @error_count := @error_count + 1;
      SELECT 'Can''t load value from file with fixed size rows to variable' AS 'ERROR';
    END
  ;
  
  DECLARE CONTINUE HANDLER FOR 1410
    BEGIN
      SET @error_count := @error_count + 1;
      SELECT 'You are not allowed to create a user with GRANT' AS 'ERROR';
    END
  ;
  
  DECLARE CONTINUE HANDLER FOR 1411
    BEGIN
      SET @error_count := @error_count + 1;
      SELECT 'Incorrect %s value: ''%s'' for function %s' AS 'ERROR';
    END
  ;
  
  DECLARE CONTINUE HANDLER FOR 1412
    BEGIN
      SET @error_count := @error_count + 1;
      SELECT 'Table definition has changed, please retry transaction' AS 'ERROR';
    END
  ;
  
  DECLARE CONTINUE HANDLER FOR 1413
    BEGIN
      SET @error_count := @error_count + 1;
      SELECT 'Duplicate handler 
  DECLAREd in the same block' AS 'ERROR';
    END
  ;
  
  DECLARE CONTINUE HANDLER FOR 1414
    BEGIN
      SET @error_count := @error_count + 1;
      SELECT 'OUT or INOUT argument %d for routine %s is not a variable or NEW pseudo-variable in BEFORE trigger' AS 'ERROR';
    END
  ;
  
  DECLARE CONTINUE HANDLER FOR 1415
    BEGIN
      SET @error_count := @error_count + 1;
      SELECT 'Not allowed to return a result set from a %s' AS 'ERROR';
    END
  ;
  
  DECLARE CONTINUE HANDLER FOR 1416
    BEGIN
      SET @error_count := @error_count + 1;
      SELECT 'Cannot get geometry object from data you send to the GEOMETRY field' AS 'ERROR';
    END
  ;
  
  DECLARE CONTINUE HANDLER FOR 1417
    BEGIN
      SET @error_count := @error_count + 1;
      SELECT 'A routine failed and has neither NO SQL nor READS SQL DATA in its declaration and binary logging is enabled; if non-transactional tables were updated, the binary log will miss their changes' AS 'ERROR';
    END
  ;
  
  DECLARE CONTINUE HANDLER FOR 1418
    BEGIN
      SET @error_count := @error_count + 1;
      SELECT 'This function has none of DETERMINISTIC, NO SQL, or READS SQL DATA in its declaration and binary logging is enabled (you *might* want to use the less safe log_bin_trust_function_creators variable)' AS 'ERROR';
    END
  ;
  
  DECLARE CONTINUE HANDLER FOR 1419
    BEGIN
      SET @error_count := @error_count + 1;
      SELECT 'You do not have the SUPER privilege and binary logging is enabled (you *might* want to use the less safe log_bin_trust_function_creators variable)' AS 'ERROR';
    END
  ;
  
  DECLARE CONTINUE HANDLER FOR 1420
    BEGIN
      SET @error_count := @error_count + 1;
      SELECT 'You can''t execute a prepared statement which has an open cursor associated with it. Reset the statement to re-execute it.' AS 'ERROR';
    END
  ;
  
  DECLARE CONTINUE HANDLER FOR 1421
    BEGIN
      SET @error_count := @error_count + 1;
      SELECT 'The statement (%lu) has no open cursor.' AS 'ERROR';
    END
  ;
  
  DECLARE CONTINUE HANDLER FOR 1422
    BEGIN
      SET @error_count := @error_count + 1;
      SELECT 'Explicit or implicit commit is not allowed in stored function or trigger.' AS 'ERROR';
    END
  ;
  
  DECLARE CONTINUE HANDLER FOR 1423
    BEGIN
      SET @error_count := @error_count + 1;
      SELECT 'Field of view ''%s.%s'' underlying table doesn''t have a default value' AS 'ERROR';
    END
  ;
  
  DECLARE CONTINUE HANDLER FOR 1424
    BEGIN
      SET @error_count := @error_count + 1;
      SELECT 'Recursive stored functions and triggers are not allowed.' AS 'ERROR';
    END
  ;
  
  DECLARE CONTINUE HANDLER FOR 1425
    BEGIN
      SET @error_count := @error_count + 1;
      SELECT 'Too big scale %d specified for column ''%s''. Maximum is %lu.' AS 'ERROR';
    END
  ;
  
  DECLARE CONTINUE HANDLER FOR 1426
    BEGIN
      SET @error_count := @error_count + 1;
      SELECT 'Too big precision %d specified for column ''%s''. Maximum is %lu.' AS 'ERROR';
    END
  ;
  
  DECLARE CONTINUE HANDLER FOR 1427
    BEGIN
      SET @error_count := @error_count + 1;
      SELECT 'For float(M,D), double(M,D) or decimal(M,D), M must be >= D (column ''%s'').' AS 'ERROR';
    END
  ;
  
  DECLARE CONTINUE HANDLER FOR 1428
    BEGIN
      SET @error_count := @error_count + 1;
      SELECT 'You can''t combine write-locking of system tables with other tables or lock types' AS 'ERROR';
    END
  ;
  
  DECLARE CONTINUE HANDLER FOR 1429
    BEGIN
      SET @error_count := @error_count + 1;
      SELECT 'Unable to connect to foreign data source: %s' AS 'ERROR';
    END
  ;
  
  DECLARE CONTINUE HANDLER FOR 1430
    BEGIN
      SET @error_count := @error_count + 1;
      SELECT 'There was a problem processing the query on the foreign data source. Data source error: %s' AS 'ERROR';
    END
  ;
  
  DECLARE CONTINUE HANDLER FOR 1431
    BEGIN
      SET @error_count := @error_count + 1;
      SELECT 'The foreign data source you are trying to reference does not exist. Data source error: %s' AS 'ERROR';
    END
  ;
  
  DECLARE CONTINUE HANDLER FOR 1432
    BEGIN
      SET @error_count := @error_count + 1;
      SELECT 'Can''t create federated table. The data source connection string ''%s'' is not in the correct format' AS 'ERROR';
    END
  ;
  
  DECLARE CONTINUE HANDLER FOR 1433
    BEGIN
      SET @error_count := @error_count + 1;
      SELECT 'The data source connection string ''%s'' is not in the correct format' AS 'ERROR';
    END
  ;
  
  DECLARE CONTINUE HANDLER FOR 1434
    BEGIN
      SET @error_count := @error_count + 1;
      SELECT 'Can''t create federated table. Foreign data src error: %s' AS 'ERROR';
    END
  ;
  
  DECLARE CONTINUE HANDLER FOR 1435
    BEGIN
      SET @error_count := @error_count + 1;
      SELECT 'Trigger in wrong schema' AS 'ERROR';
    END
  ;
  
  DECLARE CONTINUE HANDLER FOR 1436
    BEGIN
      SET @error_count := @error_count + 1;
      SELECT 'Thread stack overrun: %ld bytes used of a %ld byte stack, and %ld bytes needed. Use ''mysqld -O thread_stack=#'' to specify a bigger stack.' AS 'ERROR';
    END
  ;
  
  DECLARE CONTINUE HANDLER FOR 1437
    BEGIN
      SET @error_count := @error_count + 1;
      SELECT 'Routine body for ''%s'' is too long' AS 'ERROR';
    END
  ;
  
  DECLARE CONTINUE HANDLER FOR 1438
    BEGIN
      SET @error_count := @error_count + 1;
      SELECT 'Cannot drop default keycache' AS 'ERROR';
    END
  ;
  
  DECLARE CONTINUE HANDLER FOR 1439
    BEGIN
      SET @error_count := @error_count + 1;
      SELECT 'Display width out of range for column ''%s'' (max = %lu)' AS 'ERROR';
    END
  ;
  
  DECLARE CONTINUE HANDLER FOR 1440
    BEGIN
      SET @error_count := @error_count + 1;
      SELECT 'XAER_DUPID: The XID already exists' AS 'ERROR';
    END
  ;
  
  DECLARE CONTINUE HANDLER FOR 1441
    BEGIN
      SET @error_count := @error_count + 1;
      SELECT 'Datetime function: %s field overflow' AS 'ERROR';
    END
  ;
  
  DECLARE CONTINUE HANDLER FOR 1442
    BEGIN
      SET @error_count := @error_count + 1;
      SELECT 'Can''t update table ''%s'' in stored function/trigger because it is already used by statement which invoked this stored function/trigger.' AS 'ERROR';
    END
  ;
  
  DECLARE CONTINUE HANDLER FOR 1443
    BEGIN
      SET @error_count := @error_count + 1;
      SELECT 'The definition of table ''%s'' prevents operation %s on table ''%s''.' AS 'ERROR';
    END
  ;
  
  DECLARE CONTINUE HANDLER FOR 1444
    BEGIN
      SET @error_count := @error_count + 1;
      SELECT 'The prepared statement contains a stored routine call that refers to that same statement. It''s not allowed to execute a prepared statement in such a recursive manner' AS 'ERROR';
    END
  ;
  
  DECLARE CONTINUE HANDLER FOR 1445
    BEGIN
      SET @error_count := @error_count + 1;
      SELECT 'Not allowed to set autocommit from a stored function or trigger' AS 'ERROR';
    END
  ;
  
  DECLARE CONTINUE HANDLER FOR 1446
    BEGIN
      SET @error_count := @error_count + 1;
      SELECT 'Definer is not fully qualified' AS 'ERROR';
    END
  ;
  
  DECLARE CONTINUE HANDLER FOR 1447
    BEGIN
      SET @error_count := @error_count + 1;
      SELECT 'View ''%s''.''%s'' has no definer information (old table format). Current user is used as definer. Please recreate the view!' AS 'ERROR';
    END
  ;
  
  DECLARE CONTINUE HANDLER FOR 1448
    BEGIN
      SET @error_count := @error_count + 1;
      SELECT 'You need the SUPER privilege for creation view with ''%s''@''%s'' definer' AS 'ERROR';
    END
  ;
  
  DECLARE CONTINUE HANDLER FOR 1449
    BEGIN
      SET @error_count := @error_count + 1;
      SELECT 'The user specified as a definer (''%s''@''%s'') does not exist' AS 'ERROR';
    END
  ;
  
  DECLARE CONTINUE HANDLER FOR 1450
    BEGIN
      SET @error_count := @error_count + 1;
      SELECT 'Changing schema from ''%s'' to ''%s'' is not allowed.' AS 'ERROR';
    END
  ;
  
  DECLARE CONTINUE HANDLER FOR 1451
    BEGIN
      SET @error_count := @error_count + 1;
      SELECT 'Cannot delete or update a parent row: a foreign key constraint fails (%s)' AS 'ERROR';
    END
  ;
  
  DECLARE CONTINUE HANDLER FOR 1452
    BEGIN
      SET @error_count := @error_count + 1;
      SELECT 'Cannot add or update a child row: a foreign key constraint fails (%s)' AS 'ERROR';
    END
  ;
  
  DECLARE CONTINUE HANDLER FOR 1453
    BEGIN
      SET @error_count := @error_count + 1;
      SELECT 'Variable ''%s'' must be quoted with `...`, or renamed' AS 'ERROR';
    END
  ;
  
  DECLARE CONTINUE HANDLER FOR 1454
    BEGIN
      SET @error_count := @error_count + 1;
      SELECT 'No definer attribute for trigger ''%s''.''%s''. The trigger will be activated under the authorization of the invoker, which may have insufficient privileges. Please recreate the trigger.' AS 'ERROR';
    END
  ;
  
  DECLARE CONTINUE HANDLER FOR 1455
    BEGIN
      SET @error_count := @error_count + 1;
      SELECT '''%s'' has an old format, you should re-create the ''%s'' object(s)' AS 'ERROR';
    END
  ;
  
  DECLARE CONTINUE HANDLER FOR 1456
    BEGIN
      SET @error_count := @error_count + 1;
      SELECT 'Recursive limit %d (as set by the max_sp_recursion_depth variable) was exceeded for routine %s' AS 'ERROR';
    END
  ;
  
  DECLARE CONTINUE HANDLER FOR 1457
    BEGIN
      SET @error_count := @error_count + 1;
      SELECT 'Failed to load routine %s. The table mysql.proc is missing, corrupt, or contains bad data (internal code %d)' AS 'ERROR';
    END
  ;
  
  DECLARE CONTINUE HANDLER FOR 1458
    BEGIN
      SET @error_count := @error_count + 1;
      SELECT 'Incorrect routine name ''%s''' AS 'ERROR';
    END
  ;
  
  DECLARE CONTINUE HANDLER FOR 1459
    BEGIN
      SET @error_count := @error_count + 1;
      SELECT 'Table upgrade required. Please do "REPAIR TABLE `%s`" or dump/reload to fix it!' AS 'ERROR';
    END
  ;
  
  DECLARE CONTINUE HANDLER FOR 1460
    BEGIN
      SET @error_count := @error_count + 1;
      SELECT 'AGGREGATE is not supported for stored functions' AS 'ERROR';
    END
  ;
  
  DECLARE CONTINUE HANDLER FOR 1461
    BEGIN
      SET @error_count := @error_count + 1;
      SELECT 'Can''t create more than max_prepared_stmt_count statements (current value: %lu)' AS 'ERROR';
    END
  ;
  
  DECLARE CONTINUE HANDLER FOR 1462
    BEGIN
      SET @error_count := @error_count + 1;
      SELECT '`%s`.`%s` contains view recursion' AS 'ERROR';
    END
  ;
  
  DECLARE CONTINUE HANDLER FOR 1463
    BEGIN
      SET @error_count := @error_count + 1;
      SELECT 'non-grouping field ''%s'' is used in %s clause' AS 'ERROR';
    END
  ;
  
  DECLARE CONTINUE HANDLER FOR 1464
    BEGIN
      SET @error_count := @error_count + 1;
      SELECT 'The used table type doesn''t support SPATIAL indexes' AS 'ERROR';
    END
  ;
  
  DECLARE CONTINUE HANDLER FOR 1465
    BEGIN
      SET @error_count := @error_count + 1;
      SELECT 'Triggers can not be created on system tables' AS 'ERROR';
    END
  ;
  
  DECLARE CONTINUE HANDLER FOR 1466
    BEGIN
      SET @error_count := @error_count + 1;
      SELECT 'Leading spaces are removed from name ''%s''' AS 'ERROR';
    END
  ;
  
  DECLARE CONTINUE HANDLER FOR 1467
    BEGIN
      SET @error_count := @error_count + 1;
      SELECT 'Failed to read auto-increment value from storage engine' AS 'ERROR';
    END
  ;
  
  DECLARE CONTINUE HANDLER FOR 1468
    BEGIN
      SET @error_count := @error_count + 1;
      SELECT 'user name' AS 'ERROR';
    END
  ;
  
  DECLARE CONTINUE HANDLER FOR 1469
    BEGIN
      SET @error_count := @error_count + 1;
      SELECT 'host name' AS 'ERROR';
    END
  ;
  
  DECLARE CONTINUE HANDLER FOR 1470
    BEGIN
      SET @error_count := @error_count + 1;
      SELECT 'String ''%s'' is too long for %s (should be no longer than %d)' AS 'ERROR';
    END
  ;
  
  DECLARE CONTINUE HANDLER FOR 1471
    BEGIN
      SET @error_count := @error_count + 1;
      SELECT 'The target table %s of the %s is not insertable-into' AS 'ERROR';
    END
  ;
  
  DECLARE CONTINUE HANDLER FOR 1472
    BEGIN
      SET @error_count := @error_count + 1;
      SELECT 'Table ''%s'' is differently defined or of non-MyISAM type or doesn''t exist' AS 'ERROR';
    END
  ;
  
  DECLARE CONTINUE HANDLER FOR 1473
    BEGIN
      SET @error_count := @error_count + 1;
      SELECT 'Too high level of nesting for select' AS 'ERROR';
    END
  ;
  
  DECLARE CONTINUE HANDLER FOR 1474
    BEGIN
      SET @error_count := @error_count + 1;
      SELECT 'Name ''%s'' has become ''''' AS 'ERROR';
    END
  ;
  
  DECLARE CONTINUE HANDLER FOR 1475
    BEGIN
      SET @error_count := @error_count + 1;
      SELECT 'First character of the FIELDS TERMINATED string is ambiguous; please use non-optional and non-empty FIELDS ENCLOSED BY' AS 'ERROR';
    END
  ;
  
  DECLARE CONTINUE HANDLER FOR 1476
    BEGIN
      SET @error_count := @error_count + 1;
      SELECT 'The foreign server, %s, you are trying to create already exists.' AS 'ERROR';
    END
  ;
  
  DECLARE CONTINUE HANDLER FOR 1477
    BEGIN
      SET @error_count := @error_count + 1;
      SELECT 'The foreign server name you are trying to reference does not exist. Data source error: %s' AS 'ERROR';
    END
  ;
  
  DECLARE CONTINUE HANDLER FOR 1478
    BEGIN
      SET @error_count := @error_count + 1;
      SELECT 'Table storage engine ''%s'' does not support the create option ''%s''' AS 'ERROR';
    END
  ;
  
  DECLARE CONTINUE HANDLER FOR 1479
    BEGIN
      SET @error_count := @error_count + 1;
      SELECT 'Syntax error: %s PARTITIONING requires definition of VALUES %s for each partition' AS 'ERROR';
    END
  ;
  
  DECLARE CONTINUE HANDLER FOR 1480
    BEGIN
      SET @error_count := @error_count + 1;
      SELECT 'Only %s PARTITIONING can use VALUES %s in partition definition' AS 'ERROR';
    END
  ;
  
  DECLARE CONTINUE HANDLER FOR 1481
    BEGIN
      SET @error_count := @error_count + 1;
      SELECT 'MAXVALUE can only be used in last partition definition' AS 'ERROR';
    END
  ;
  
  DECLARE CONTINUE HANDLER FOR 1482
    BEGIN
      SET @error_count := @error_count + 1;
      SELECT 'Subpartitions can only be hash partitions and by key' AS 'ERROR';
    END
  ;
  
  DECLARE CONTINUE HANDLER FOR 1483
    BEGIN
      SET @error_count := @error_count + 1;
      SELECT 'Must define subpartitions on all partitions if on one partition' AS 'ERROR';
    END
  ;
  
  DECLARE CONTINUE HANDLER FOR 1484
    BEGIN
      SET @error_count := @error_count + 1;
      SELECT 'Wrong number of partitions defined, mismatch with previous setting' AS 'ERROR';
    END
  ;
  
  DECLARE CONTINUE HANDLER FOR 1485
    BEGIN
      SET @error_count := @error_count + 1;
      SELECT 'Wrong number of subpartitions defined, mismatch with previous setting' AS 'ERROR';
    END
  ;
  
  DECLARE CONTINUE HANDLER FOR 1486
    BEGIN
      SET @error_count := @error_count + 1;
      SELECT 'Constant, random or timezone-dependent expressions in (sub)partitioning function are not allowed' AS 'ERROR';
    END
  ;
  
  DECLARE CONTINUE HANDLER FOR 1487
    BEGIN
      SET @error_count := @error_count + 1;
      SELECT 'Expression in RANGE/LIST VALUES must be constant' AS 'ERROR';
    END
  ;
  
  DECLARE CONTINUE HANDLER FOR 1488
    BEGIN
      SET @error_count := @error_count + 1;
      SELECT 'Field in list of fields for partition function not found in table' AS 'ERROR';
    END
  ;
  
  DECLARE CONTINUE HANDLER FOR 1489
    BEGIN
      SET @error_count := @error_count + 1;
      SELECT 'List of fields is only allowed in KEY partitions' AS 'ERROR';
    END
  ;
  
  DECLARE CONTINUE HANDLER FOR 1490
    BEGIN
      SET @error_count := @error_count + 1;
      SELECT 'The partition info in the frm file is not consistent with what can be written into the frm file' AS 'ERROR';
    END
  ;
  
  DECLARE CONTINUE HANDLER FOR 1491
    BEGIN
      SET @error_count := @error_count + 1;
      SELECT 'The %s function returns the wrong type' AS 'ERROR';
    END
  ;
  
  DECLARE CONTINUE HANDLER FOR 1492
    BEGIN
      SET @error_count := @error_count + 1;
      SELECT 'For %s partitions each partition must be defined' AS 'ERROR';
    END
  ;
  
  DECLARE CONTINUE HANDLER FOR 1493
    BEGIN
      SET @error_count := @error_count + 1;
      SELECT 'VALUES LESS THAN value must be strictly increasing for each partition' AS 'ERROR';
    END
  ;
  
  DECLARE CONTINUE HANDLER FOR 1494
    BEGIN
      SET @error_count := @error_count + 1;
      SELECT 'VALUES value must be of same type as partition function' AS 'ERROR';
    END
  ;
  
  DECLARE CONTINUE HANDLER FOR 1495
    BEGIN
      SET @error_count := @error_count + 1;
      SELECT 'Multiple definition of same constant in list partitioning' AS 'ERROR';
    END
  ;
  
  DECLARE CONTINUE HANDLER FOR 1496
    BEGIN
      SET @error_count := @error_count + 1;
      SELECT 'Partitioning can not be used stand-alone in query' AS 'ERROR';
    END
  ;
  
  DECLARE CONTINUE HANDLER FOR 1497
    BEGIN
      SET @error_count := @error_count + 1;
      SELECT 'The mix of handlers in the partitions is not allowed in this version of MySQL' AS 'ERROR';
    END
  ;
  
  DECLARE CONTINUE HANDLER FOR 1498
    BEGIN
      SET @error_count := @error_count + 1;
      SELECT 'For the partitioned engine it is necessary to define all %s' AS 'ERROR';
    END
  ;
  
  DECLARE CONTINUE HANDLER FOR 1499
    BEGIN
      SET @error_count := @error_count + 1;
      SELECT 'Too many partitions (including subpartitions) were defined' AS 'ERROR';
    END
  ;
  
  DECLARE CONTINUE HANDLER FOR 1500
    BEGIN
      SET @error_count := @error_count + 1;
      SELECT 'It is only possible to mix RANGE/LIST partitioning with HASH/KEY partitioning for subpartitioning' AS 'ERROR';
    END
  ;
  
  DECLARE CONTINUE HANDLER FOR 1501
    BEGIN
      SET @error_count := @error_count + 1;
      SELECT 'Failed to create specific handler file' AS 'ERROR';
    END
  ;
  
  DECLARE CONTINUE HANDLER FOR 1502
    BEGIN
      SET @error_count := @error_count + 1;
      SELECT 'A BLOB field is not allowed in partition function' AS 'ERROR';
    END
  ;
  
  DECLARE CONTINUE HANDLER FOR 1503
    BEGIN
      SET @error_count := @error_count + 1;
      SELECT 'A %s must include all columns in the table''s partitioning function' AS 'ERROR';
    END
  ;
  
  DECLARE CONTINUE HANDLER FOR 1504
    BEGIN
      SET @error_count := @error_count + 1;
      SELECT 'Number of %s = 0 is not an allowed value' AS 'ERROR';
    END
  ;
  
  DECLARE CONTINUE HANDLER FOR 1505
    BEGIN
      SET @error_count := @error_count + 1;
      SELECT 'Partition management on a not partitioned table is not possible' AS 'ERROR';
    END
  ;
  
  DECLARE CONTINUE HANDLER FOR 1506
    BEGIN
      SET @error_count := @error_count + 1;
      SELECT 'Foreign key clause is not yet supported in conjunction with partitioning' AS 'ERROR';
    END
  ;
  
  DECLARE CONTINUE HANDLER FOR 1507
    BEGIN
      SET @error_count := @error_count + 1;
      SELECT 'Error in list of partitions to %s' AS 'ERROR';
    END
  ;
  
  DECLARE CONTINUE HANDLER FOR 1508
    BEGIN
      SET @error_count := @error_count + 1;
      SELECT 'Cannot remove all partitions, use DROP TABLE instead' AS 'ERROR';
    END
  ;
  
  DECLARE CONTINUE HANDLER FOR 1509
    BEGIN
      SET @error_count := @error_count + 1;
      SELECT 'COALESCE PARTITION can only be used on HASH/KEY partitions' AS 'ERROR';
    END
  ;
  
  DECLARE CONTINUE HANDLER FOR 1510
    BEGIN
      SET @error_count := @error_count + 1;
      SELECT 'REORGANIZE PARTITION can only be used to reorganize partitions not to change their numbers' AS 'ERROR';
    END
  ;
  
  DECLARE CONTINUE HANDLER FOR 1511
    BEGIN
      SET @error_count := @error_count + 1;
      SELECT 'REORGANIZE PARTITION without parameters can only be used on auto-partitioned tables using HASH PARTITIONs' AS 'ERROR';
    END
  ;
  
  DECLARE CONTINUE HANDLER FOR 1512
    BEGIN
      SET @error_count := @error_count + 1;
      SELECT '%s PARTITION can only be used on RANGE/LIST partitions' AS 'ERROR';
    END
  ;
  
  DECLARE CONTINUE HANDLER FOR 1513
    BEGIN
      SET @error_count := @error_count + 1;
      SELECT 'Trying to Add partition(s) with wrong number of subpartitions' AS 'ERROR';
    END
  ;
  
  DECLARE CONTINUE HANDLER FOR 1514
    BEGIN
      SET @error_count := @error_count + 1;
      SELECT 'At least one partition must be added' AS 'ERROR';
    END
  ;
  
  DECLARE CONTINUE HANDLER FOR 1515
    BEGIN
      SET @error_count := @error_count + 1;
      SELECT 'At least one partition must be coalesced' AS 'ERROR';
    END
  ;
  
  DECLARE CONTINUE HANDLER FOR 1516
    BEGIN
      SET @error_count := @error_count + 1;
      SELECT 'More partitions to reorganize than there are partitions' AS 'ERROR';
    END
  ;
  
  DECLARE CONTINUE HANDLER FOR 1517
    BEGIN
      SET @error_count := @error_count + 1;
      SELECT 'Duplicate partition name %s' AS 'ERROR';
    END
  ;
  
  DECLARE CONTINUE HANDLER FOR 1518
    BEGIN
      SET @error_count := @error_count + 1;
      SELECT 'It is not allowed to shut off binlog on this command' AS 'ERROR';
    END
  ;
  
  DECLARE CONTINUE HANDLER FOR 1519
    BEGIN
      SET @error_count := @error_count + 1;
      SELECT 'When reorganizing a set of partitions they must be in consecutive order' AS 'ERROR';
    END
  ;
  
  DECLARE CONTINUE HANDLER FOR 1520
    BEGIN
      SET @error_count := @error_count + 1;
      SELECT 'Reorganize of range partitions cannot change total ranges except for last partition where it can extend the range' AS 'ERROR';
    END
  ;
  
  DECLARE CONTINUE HANDLER FOR 1521
    BEGIN
      SET @error_count := @error_count + 1;
      SELECT 'Partition function not supported in this version for this handler' AS 'ERROR';
    END
  ;
  
  DECLARE CONTINUE HANDLER FOR 1522
    BEGIN
      SET @error_count := @error_count + 1;
      SELECT 'Partition state cannot be defined from CREATE/ALTER TABLE' AS 'ERROR';
    END
  ;
  
  DECLARE CONTINUE HANDLER FOR 1523
    BEGIN
      SET @error_count := @error_count + 1;
      SELECT 'The %s handler only supports 32 bit integers in VALUES' AS 'ERROR';
    END
  ;
  
  DECLARE CONTINUE HANDLER FOR 1524
    BEGIN
      SET @error_count := @error_count + 1;
      SELECT 'Plugin ''%s'' is not loaded' AS 'ERROR';
    END
  ;
  
  DECLARE CONTINUE HANDLER FOR 1525
    BEGIN
      SET @error_count := @error_count + 1;
      SELECT 'Incorrect %s value: ''%s''' AS 'ERROR';
    END
  ;
  
  DECLARE CONTINUE HANDLER FOR 1526
    BEGIN
      SET @error_count := @error_count + 1;
      SELECT 'Table has no partition for value %s' AS 'ERROR';
    END
  ;
  
  DECLARE CONTINUE HANDLER FOR 1527
    BEGIN
      SET @error_count := @error_count + 1;
      SELECT 'It is not allowed to specify %s more than once' AS 'ERROR';
    END
  ;
  
  DECLARE CONTINUE HANDLER FOR 1528
    BEGIN
      SET @error_count := @error_count + 1;
      SELECT 'Failed to create %s' AS 'ERROR';
    END
  ;
  
  DECLARE CONTINUE HANDLER FOR 1529
    BEGIN
      SET @error_count := @error_count + 1;
      SELECT 'Failed to drop %s' AS 'ERROR';
    END
  ;
  
  DECLARE CONTINUE HANDLER FOR 1530
    BEGIN
      SET @error_count := @error_count + 1;
      SELECT 'The handler doesn''t support autoextend of tablespaces' AS 'ERROR';
    END
  ;
  
  DECLARE CONTINUE HANDLER FOR 1531
    BEGIN
      SET @error_count := @error_count + 1;
      SELECT 'A size parameter was incorrectly specified, either number or on the form 10M' AS 'ERROR';
    END
  ;
  
  DECLARE CONTINUE HANDLER FOR 1532
    BEGIN
      SET @error_count := @error_count + 1;
      SELECT 'The size number was correct but we don''t allow the digit part to be more than 2 billion' AS 'ERROR';
    END
  ;
  
  DECLARE CONTINUE HANDLER FOR 1533
    BEGIN
      SET @error_count := @error_count + 1;
      SELECT 'Failed to alter: %s' AS 'ERROR';
    END
  ;
  
  DECLARE CONTINUE HANDLER FOR 1534
    BEGIN
      SET @error_count := @error_count + 1;
      SELECT 'Writing one row to the row-based binary log failed' AS 'ERROR';
    END
  ;
  
  DECLARE CONTINUE HANDLER FOR 1535
    BEGIN
      SET @error_count := @error_count + 1;
      SELECT 'Table definition on master and slave does not match: %s' AS 'ERROR';
    END
  ;
  
  DECLARE CONTINUE HANDLER FOR 1536
    BEGIN
      SET @error_count := @error_count + 1;
      SELECT 'Slave running with --log-slave-updates must use row-based binary logging to be able to replicate row-based binary log events' AS 'ERROR';
    END
  ;
  
  DECLARE CONTINUE HANDLER FOR 1537
    BEGIN
      SET @error_count := @error_count + 1;
      SELECT 'Event ''%s'' already exists' AS 'ERROR';
    END
  ;
  
  DECLARE CONTINUE HANDLER FOR 1538
    BEGIN
      SET @error_count := @error_count + 1;
      SELECT 'Failed to store event %s. Error code %d from storage engine.' AS 'ERROR';
    END
  ;
  
  DECLARE CONTINUE HANDLER FOR 1539
    BEGIN
      SET @error_count := @error_count + 1;
      SELECT 'Unknown event ''%s''' AS 'ERROR';
    END
  ;
  
  DECLARE CONTINUE HANDLER FOR 1540
    BEGIN
      SET @error_count := @error_count + 1;
      SELECT 'Failed to alter event ''%s''' AS 'ERROR';
    END
  ;
  
  DECLARE CONTINUE HANDLER FOR 1541
    BEGIN
      SET @error_count := @error_count + 1;
      SELECT 'Failed to drop %s' AS 'ERROR';
    END
  ;
  
  DECLARE CONTINUE HANDLER FOR 1542
    BEGIN
      SET @error_count := @error_count + 1;
      SELECT 'INTERVAL is either not positive or too big' AS 'ERROR';
    END
  ;
  
  DECLARE CONTINUE HANDLER FOR 1543
    BEGIN
      SET @error_count := @error_count + 1;
      SELECT 'ENDS is either invalid or before STARTS' AS 'ERROR';
    END
  ;
  
  DECLARE CONTINUE HANDLER FOR 1544
    BEGIN
      SET @error_count := @error_count + 1;
      SELECT 'Event execution time is in the past. Event has been disabled' AS 'ERROR';
    END
  ;
  
  DECLARE CONTINUE HANDLER FOR 1545
    BEGIN
      SET @error_count := @error_count + 1;
      SELECT 'Failed to open mysql.event' AS 'ERROR';
    END
  ;
  
  DECLARE CONTINUE HANDLER FOR 1546
    BEGIN
      SET @error_count := @error_count + 1;
      SELECT 'No datetime expression provided' AS 'ERROR';
    END
  ;
  
  DECLARE CONTINUE HANDLER FOR 1547
    BEGIN
      SET @error_count := @error_count + 1;
      SELECT 'Column count of mysql.%s is wrong. Expected %d, found %d. The table is probably corrupted' AS 'ERROR';
    END
  ;
  
  DECLARE CONTINUE HANDLER FOR 1548
    BEGIN
      SET @error_count := @error_count + 1;
      SELECT 'Cannot load from mysql.%s. The table is probably corrupted' AS 'ERROR';
    END
  ;
  
  DECLARE CONTINUE HANDLER FOR 1549
    BEGIN
      SET @error_count := @error_count + 1;
      SELECT 'Failed to delete the event from mysql.event' AS 'ERROR';
    END
  ;
  
  DECLARE CONTINUE HANDLER FOR 1550
    BEGIN
      SET @error_count := @error_count + 1;
      SELECT 'Error during compilation of event''s body' AS 'ERROR';
    END
  ;
  
  DECLARE CONTINUE HANDLER FOR 1551
    BEGIN
      SET @error_count := @error_count + 1;
      SELECT 'Same old and new event name' AS 'ERROR';
    END
  ;
  
  DECLARE CONTINUE HANDLER FOR 1552
    BEGIN
      SET @error_count := @error_count + 1;
      SELECT 'Data for column ''%s'' too long' AS 'ERROR';
    END
  ;
  
  DECLARE CONTINUE HANDLER FOR 1553
    BEGIN
      SET @error_count := @error_count + 1;
      SELECT 'Cannot drop index ''%s'': needed in a foreign key constraint' AS 'ERROR';
    END
  ;
  
  DECLARE CONTINUE HANDLER FOR 1554
    BEGIN
      SET @error_count := @error_count + 1;
      SELECT 'The syntax ''%s'' is deprecated and will be removed in MySQL %s. Please use %s instead' AS 'ERROR';
    END
  ;
  
  DECLARE CONTINUE HANDLER FOR 1555
    BEGIN
      SET @error_count := @error_count + 1;
      SELECT 'You can''t write-lock a log table. Only read access is possible' AS 'ERROR';
    END
  ;
  
  DECLARE CONTINUE HANDLER FOR 1556
    BEGIN
      SET @error_count := @error_count + 1;
      SELECT 'You can''t use locks with log tables.' AS 'ERROR';
    END
  ;
  
  DECLARE CONTINUE HANDLER FOR 1557
    BEGIN
      SET @error_count := @error_count + 1;
      SELECT 'Upholding foreign key constraints for table ''%s'', entry ''%s'', key %d would lead to a duplicate entry' AS 'ERROR';
    END
  ;
  
  DECLARE CONTINUE HANDLER FOR 1558
    BEGIN
      SET @error_count := @error_count + 1;
      SELECT 'Column count of mysql.%s is wrong. Expected %d, found %d. Created with MySQL %d, now running %d. Please use mysql_upgrade to fix this error.' AS 'ERROR';
    END
  ;
  
  DECLARE CONTINUE HANDLER FOR 1559
    BEGIN
      SET @error_count := @error_count + 1;
      SELECT 'Cannot switch out of the row-based binary log format when the session has open temporary tables' AS 'ERROR';
    END
  ;
  
  DECLARE CONTINUE HANDLER FOR 1560
    BEGIN
      SET @error_count := @error_count + 1;
      SELECT 'Cannot change the binary logging format inside a stored function or trigger' AS 'ERROR';
    END
  ;
  
  DECLARE CONTINUE HANDLER FOR 1561
    BEGIN
      SET @error_count := @error_count + 1;
      SELECT 'The NDB cluster engine does not support changing the binlog format on the fly yet' AS 'ERROR';
    END
  ;
  
  DECLARE CONTINUE HANDLER FOR 1562
    BEGIN
      SET @error_count := @error_count + 1;
      SELECT 'Cannot create temporary table with partitions' AS 'ERROR';
    END
  ;
  
  DECLARE CONTINUE HANDLER FOR 1563
    BEGIN
      SET @error_count := @error_count + 1;
      SELECT 'Partition constant is out of partition function domain' AS 'ERROR';
    END
  ;
  
  DECLARE CONTINUE HANDLER FOR 1564
    BEGIN
      SET @error_count := @error_count + 1;
      SELECT 'This partition function is not allowed' AS 'ERROR';
    END
  ;
  
  DECLARE CONTINUE HANDLER FOR 1565
    BEGIN
      SET @error_count := @error_count + 1;
      SELECT 'Error in DDL log' AS 'ERROR';
    END
  ;
  
  DECLARE CONTINUE HANDLER FOR 1566
    BEGIN
      SET @error_count := @error_count + 1;
      SELECT 'Not allowed to use NULL value in VALUES LESS THAN' AS 'ERROR';
    END
  ;
  
  DECLARE CONTINUE HANDLER FOR 1567
    BEGIN
      SET @error_count := @error_count + 1;
      SELECT 'Incorrect partition name' AS 'ERROR';
    END
  ;
  
  DECLARE CONTINUE HANDLER FOR 1568
    BEGIN
      SET @error_count := @error_count + 1;
      SELECT 'Transaction isolation level can''t be changed while a transaction is in progress' AS 'ERROR';
    END
  ;
  
  DECLARE CONTINUE HANDLER FOR 1569
    BEGIN
      SET @error_count := @error_count + 1;
      SELECT 'ALTER TABLE causes auto_increment resequencing, resulting in duplicate entry ''%s'' for key ''%s''' AS 'ERROR';
    END
  ;
  
  DECLARE CONTINUE HANDLER FOR 1570
    BEGIN
      SET @error_count := @error_count + 1;
      SELECT 'Internal scheduler error %d' AS 'ERROR';
    END
  ;
  
  DECLARE CONTINUE HANDLER FOR 1571
    BEGIN
      SET @error_count := @error_count + 1;
      SELECT 'Error during starting/stopping of the scheduler. Error code %u' AS 'ERROR';
    END
  ;
  
  DECLARE CONTINUE HANDLER FOR 1572
    BEGIN
      SET @error_count := @error_count + 1;
      SELECT 'Engine cannot be used in partitioned tables' AS 'ERROR';
    END
  ;
  
  DECLARE CONTINUE HANDLER FOR 1573
    BEGIN
      SET @error_count := @error_count + 1;
      SELECT 'Cannot activate ''%s'' log' AS 'ERROR';
    END
  ;
  
  DECLARE CONTINUE HANDLER FOR 1574
    BEGIN
      SET @error_count := @error_count + 1;
      SELECT 'The server was not built with row-based replication' AS 'ERROR';
    END
  ;
  
  DECLARE CONTINUE HANDLER FOR 1575
    BEGIN
      SET @error_count := @error_count + 1;
      SELECT 'Decoding of base64 string failed' AS 'ERROR';
    END
  ;
  
  DECLARE CONTINUE HANDLER FOR 1576
    BEGIN
      SET @error_count := @error_count + 1;
      SELECT 'Recursion of EVENT DDL statements is forbidden when body is present' AS 'ERROR';
    END
  ;
  
  DECLARE CONTINUE HANDLER FOR 1577
    BEGIN
      SET @error_count := @error_count + 1;
      SELECT 'Cannot proceed because system tables used by Event Scheduler were found damaged at server start' AS 'ERROR';
    END
  ;
  
  DECLARE CONTINUE HANDLER FOR 1578
    BEGIN
      SET @error_count := @error_count + 1;
      SELECT 'Only integers allowed as number here' AS 'ERROR';
    END
  ;
  
  DECLARE CONTINUE HANDLER FOR 1579
    BEGIN
      SET @error_count := @error_count + 1;
      SELECT 'This storage engine cannot be used for log tables"' AS 'ERROR';
    END
  ;
  
  DECLARE CONTINUE HANDLER FOR 1580
    BEGIN
      SET @error_count := @error_count + 1;
      SELECT 'You cannot ''%s'' a log table if logging is enabled' AS 'ERROR';
    END
  ;
  
  DECLARE CONTINUE HANDLER FOR 1581
    BEGIN
      SET @error_count := @error_count + 1;
      SELECT 'Cannot rename ''%s''. When logging enabled, rename to/from log table must rename two tables: the log table to an archive table and another table back to ''%s''' AS 'ERROR';
    END
  ;
  
  DECLARE CONTINUE HANDLER FOR 1582
    BEGIN
      SET @error_count := @error_count + 1;
      SELECT 'Incorrect parameter count in the call to native function ''%s''' AS 'ERROR';
    END
  ;
  
  DECLARE CONTINUE HANDLER FOR 1583
    BEGIN
      SET @error_count := @error_count + 1;
      SELECT 'Incorrect parameters in the call to native function ''%s''' AS 'ERROR';
    END
  ;
  
  DECLARE CONTINUE HANDLER FOR 1584
    BEGIN
      SET @error_count := @error_count + 1;
      SELECT 'Incorrect parameters in the call to stored function ''%s''' AS 'ERROR';
    END
  ;
  
  DECLARE CONTINUE HANDLER FOR 1585
    BEGIN
      SET @error_count := @error_count + 1;
      SELECT 'This function ''%s'' has the same name as a native function' AS 'ERROR';
    END
  ;
  
  DECLARE CONTINUE HANDLER FOR 1586
    BEGIN
      SET @error_count := @error_count + 1;
      SELECT 'Duplicate entry ''%s'' for key ''%s''' AS 'ERROR';
    END
  ;
  
  DECLARE CONTINUE HANDLER FOR 1587
    BEGIN
      SET @error_count := @error_count + 1;
      SELECT 'Too many files opened, please execute the command again' AS 'ERROR';
    END
  ;
  
  DECLARE CONTINUE HANDLER FOR 1588
    BEGIN
      SET @error_count := @error_count + 1;
      SELECT 'Event execution time is in the past and ON COMPLETION NOT PRESERVE is set. The event was dropped immediately after creation.' AS 'ERROR';
    END
  ;
  
  DECLARE CONTINUE HANDLER FOR 1589
    BEGIN
      SET @error_count := @error_count + 1;
      SELECT 'Event execution time is in the past and ON COMPLETION NOT PRESERVE is set. The event was dropped immediately after creation.' AS 'ERROR';
    END
  ;
  
  DECLARE CONTINUE HANDLER FOR 1590
    BEGIN
      SET @error_count := @error_count + 1;
      SELECT 'The incident %s occured on the master. Message: %s' AS 'ERROR';
    END
  ;
  
  DECLARE CONTINUE HANDLER FOR 1591
    BEGIN
      SET @error_count := @error_count + 1;
      SELECT 'Table has no partition for some existing values' AS 'ERROR';
    END
  ;
  
  DECLARE CONTINUE HANDLER FOR 1592
    BEGIN
      SET @error_count := @error_count + 1;
      SELECT 'Statement may not be safe to log in statement format.' AS 'ERROR';
    END
  ;
  
  DECLARE CONTINUE HANDLER FOR 1593
    BEGIN
      SET @error_count := @error_count + 1;
      SELECT 'Fatal error: %s' AS 'ERROR';
    END
  ;
  
  DECLARE CONTINUE HANDLER FOR 1594
    BEGIN
      SET @error_count := @error_count + 1;
      SELECT 'Relay log read failure: %s' AS 'ERROR';
    END
  ;
  
  DECLARE CONTINUE HANDLER FOR 1595
    BEGIN
      SET @error_count := @error_count + 1;
      SELECT 'Relay log write failure: %s' AS 'ERROR';
    END
  ;
  
  DECLARE CONTINUE HANDLER FOR 1596
    BEGIN
      SET @error_count := @error_count + 1;
      SELECT 'Failed to create %s' AS 'ERROR';
    END
  ;
  
  DECLARE CONTINUE HANDLER FOR 1597
    BEGIN
      SET @error_count := @error_count + 1;
      SELECT 'Master command %s failed: %s' AS 'ERROR';
    END
  ;
  
  DECLARE CONTINUE HANDLER FOR 1598
    BEGIN
      SET @error_count := @error_count + 1;
      SELECT 'Binary logging not possible. Message: %s' AS 'ERROR';
    END
  ;
  
  DECLARE CONTINUE HANDLER FOR 1599
    BEGIN
      SET @error_count := @error_count + 1;
      SELECT 'View `%s`.`%s` has no creation context' AS 'ERROR';
    END
  ;
  
  DECLARE CONTINUE HANDLER FOR 1600
    BEGIN
      SET @error_count := @error_count + 1;
      SELECT 'Creation context of view `%s`.`%s'' is invalid' AS 'ERROR';
    END
  ;
  
  DECLARE CONTINUE HANDLER FOR 1601
    BEGIN
      SET @error_count := @error_count + 1;
      SELECT 'Creation context of stored routine `%s`.`%s` is invalid' AS 'ERROR';
    END
  ;
  
  DECLARE CONTINUE HANDLER FOR 1602
    BEGIN
      SET @error_count := @error_count + 1;
      SELECT 'Corrupted TRG file for table `%s`.`%s`' AS 'ERROR';
    END
  ;
  
  DECLARE CONTINUE HANDLER FOR 1603
    BEGIN
      SET @error_count := @error_count + 1;
      SELECT 'Triggers for table `%s`.`%s` have no creation context' AS 'ERROR';
    END
  ;
  
  DECLARE CONTINUE HANDLER FOR 1604
    BEGIN
      SET @error_count := @error_count + 1;
      SELECT 'Trigger creation context of table `%s`.`%s` is invalid' AS 'ERROR';
    END
  ;
  
  DECLARE CONTINUE HANDLER FOR 1605
    BEGIN
      SET @error_count := @error_count + 1;
      SELECT 'Creation context of event `%s`.`%s` is invalid' AS 'ERROR';
    END
  ;
  
  DECLARE CONTINUE HANDLER FOR 1606
    BEGIN
      SET @error_count := @error_count + 1;
      SELECT 'Cannot open table for trigger `%s`.`%s`' AS 'ERROR';
    END
  ;
  
  DECLARE CONTINUE HANDLER FOR 1607
    BEGIN
      SET @error_count := @error_count + 1;
      SELECT 'Cannot create stored routine `%s`. Check warnings' AS 'ERROR';
    END
  ;
  
  DECLARE CONTINUE HANDLER FOR 1608
    BEGIN
      SET @error_count := @error_count + 1;
      SELECT 'Ambiguous slave modes combination. %s' AS 'ERROR';
    END
  ;
  
  DECLARE CONTINUE HANDLER FOR 1609
    BEGIN
      SET @error_count := @error_count + 1;
      SELECT 'The BINLOG statement of type `%s` was not preceded by a format description BINLOG statement.' AS 'ERROR';
    END
  ;
  
  DECLARE CONTINUE HANDLER FOR 1610
    BEGIN
      SET @error_count := @error_count + 1;
      SELECT 'Corrupted replication event was detected' AS 'ERROR';
    END
  ;
  
  DECLARE CONTINUE HANDLER FOR 1611
    BEGIN
      SET @error_count := @error_count + 1;
      SELECT 'Invalid column reference (%s) in LOAD DATA' AS 'ERROR';
    END
  ;
  
  DECLARE CONTINUE HANDLER FOR 1612
    BEGIN
      SET @error_count := @error_count + 1;
      SELECT 'Being purged log %s was not found' AS 'ERROR';
    END
  ;
  
  DECLARE CONTINUE HANDLER FOR 1613
    BEGIN
      SET @error_count := @error_count + 1;
      SELECT 'XA_RBTIMEOUT: Transaction branch was rolled back: took too long' AS 'ERROR';
    END
  ;
  
  DECLARE CONTINUE HANDLER FOR 1614
    BEGIN
      SET @error_count := @error_count + 1;
      SELECT 'XA_RBDEADLOCK: Transaction branch was rolled back: deadlock was detected' AS 'ERROR';
    END
  ;
  
  DECLARE CONTINUE HANDLER FOR 1615
    BEGIN
      SET @error_count := @error_count + 1;
      SELECT 'Prepared statement needs to be re-prepared' AS 'ERROR';
    END
  ;
  
  DECLARE CONTINUE HANDLER FOR 1616
    BEGIN
      SET @error_count := @error_count + 1;
      SELECT 'DELAYED option not supported for table ''%s''' AS 'ERROR';
    END
  ;
  
  DECLARE CONTINUE HANDLER FOR 1617
    BEGIN
      SET @error_count := @error_count + 1;
      SELECT 'The master info structure does not exist' AS 'ERROR';
    END
  ;
  
  DECLARE CONTINUE HANDLER FOR 1618
    BEGIN
      SET @error_count := @error_count + 1;
      SELECT '<%s> option ignored' AS 'ERROR';
    END
  ;
  
  DECLARE CONTINUE HANDLER FOR 1619
    BEGIN
      SET @error_count := @error_count + 1;
      SELECT 'Built-in plugins cannot be deleted' AS 'ERROR';
    END
  ;
  
  DECLARE CONTINUE HANDLER FOR 1620
    BEGIN
      SET @error_count := @error_count + 1;
      SELECT 'Plugin is busy and will be uninstalled on shutdown' AS 'ERROR';
    END
  ;
  
  DECLARE CONTINUE HANDLER FOR 1621
    BEGIN
      SET @error_count := @error_count + 1;
      SELECT '%s variable ''%s'' is read-only. Use SET %s to assign the value' AS 'ERROR';
    END
  ;
  
  DECLARE CONTINUE HANDLER FOR 1622
    BEGIN
      SET @error_count := @error_count + 1;
      SELECT 'Storage engine %s does not support rollback for this statement. Transaction rolled back and must be restarted' AS 'ERROR';
    END
  ;
  
  DECLARE CONTINUE HANDLER FOR 1623
    BEGIN
      SET @error_count := @error_count + 1;
      SELECT 'Unexpected master''s heartbeat data: %s' AS 'ERROR';
    END
  ;
  
  DECLARE CONTINUE HANDLER FOR 1624
    BEGIN
      SET @error_count := @error_count + 1;
      SELECT 'The requested value for the heartbeat period %s %s' AS 'ERROR';
    END
  ;
  
  DECLARE CONTINUE HANDLER FOR 1625
    BEGIN
      SET @error_count := @error_count + 1;
      SELECT 'Bad schema for mysql.ndb_replication table. Message: %s' AS 'ERROR';
    END
  ;
  
  DECLARE CONTINUE HANDLER FOR 1626
    BEGIN
      SET @error_count := @error_count + 1;
      SELECT 'Error in parsing conflict function. Message: %s' AS 'ERROR';
    END
  ;
  
  DECLARE CONTINUE HANDLER FOR 1627
    BEGIN
      SET @error_count := @error_count + 1;
      SELECT 'Write to exceptions table failed. Message: %s"' AS 'ERROR';
    END
  ;
  
  DECLARE CONTINUE HANDLER FOR 1628
    BEGIN
      SET @error_count := @error_count + 1;
      SELECT 'Comment for table ''%s'' is too long (max = %lu)' AS 'ERROR';
    END
  ;
  
  DECLARE CONTINUE HANDLER FOR 1629
    BEGIN
      SET @error_count := @error_count + 1;
      SELECT 'Comment for field ''%s'' is too long (max = %lu)' AS 'ERROR';
    END
  ;
  
  DECLARE CONTINUE HANDLER FOR 1630
    BEGIN
      SET @error_count := @error_count + 1;
      SELECT 'FUNCTION %s does not exist. Check the ''Function Name Parsing and Resolution'' section in the Reference Manual' AS 'ERROR';
    END
  ;
  
  DECLARE CONTINUE HANDLER FOR 1631
    BEGIN
      SET @error_count := @error_count + 1;
      SELECT 'Database' AS 'ERROR';
    END
  ;
  
  DECLARE CONTINUE HANDLER FOR 1632
    BEGIN
      SET @error_count := @error_count + 1;
      SELECT 'Table' AS 'ERROR';
    END
  ;
  
  DECLARE CONTINUE HANDLER FOR 1633
    BEGIN
      SET @error_count := @error_count + 1;
      SELECT 'Partition' AS 'ERROR';
    END
  ;
  
  DECLARE CONTINUE HANDLER FOR 1634
    BEGIN
      SET @error_count := @error_count + 1;
      SELECT 'Subpartition' AS 'ERROR';
    END
  ;
  
  DECLARE CONTINUE HANDLER FOR 1635
    BEGIN
      SET @error_count := @error_count + 1;
      SELECT 'Temporary' AS 'ERROR';
    END
  ;
  
  DECLARE CONTINUE HANDLER FOR 1636
    BEGIN
      SET @error_count := @error_count + 1;
      SELECT 'Renamed' AS 'ERROR';
    END
  ;
  
  DECLARE CONTINUE HANDLER FOR 1637
    BEGIN
      SET @error_count := @error_count + 1;
      SELECT 'Too many active concurrent transactions' AS 'ERROR';
    END
  ;
  
  DECLARE CONTINUE HANDLER FOR 1638
    BEGIN
      SET @error_count := @error_count + 1;
      SELECT 'Non-ASCII separator arguments are not fully supported' AS 'ERROR';
    END
  ;
  
  DECLARE CONTINUE HANDLER FOR 1639
    BEGIN
      SET @error_count := @error_count + 1;
      SELECT 'debug sync point wait timed out' AS 'ERROR';
    END
  ;
  
  DECLARE CONTINUE HANDLER FOR 1640
    BEGIN
      SET @error_count := @error_count + 1;
      SELECT 'debug sync point hit limit reached ' AS 'ERROR';
    END
  ;




  -- --------------------------------------------------------
  -- Major database schema changes - 04/09/2012
  -- --------------------------------------------------------
  -- CHARACTER SET utf8 COLLATE utf8_general_ci
  -- ALTER DATABASE greenphyl_ DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;

  -- ID normalization
  BEGIN
    SELECT 'Normalize table indices...' AS 'Message';
    -- type change: drop constraint that will added back later
    CALL dropForeignKeyConstraints('dbxref',          'db_id', 'db');
    CALL dropForeignKeyConstraints('ipr_to_go',       'go_id', 'go');
    CALL dropForeignKeyConstraints('uniprot_to_go',   'go_id', 'go');
    CALL dropForeignKeyConstraints('ec_to_go',        'go_id', 'go');
    CALL dropForeignKeyConstraints('uniprot_to_go',   'dbxref_id', 'dbxref');
    CALL dropForeignKeyConstraints('ec_to_go',        'dbxref_id', 'dbxref');
    CALL dropForeignKeyConstraints('sequence_dbxref', 'dbxref_id', 'dbxref');
    CALL dropForeignKeyConstraints('sequence_dbxref', 'seq_id', 'sequences');
    CALL dropForeignKeyConstraints('seq_is_in',       'seq_id', 'sequences');
    CALL dropForeignKeyConstraints('seq_is_in',       'family_id', 'family');
    -- make sure we did not alread processed homologs table (no need to drop the
    -- constraint which is long)
    IF (NOT columnExists('homologs', 'object_sequence_id')) THEN
      CALL dropForeignKeyConstraints('homologs',        'homology_id', 'homologies');
    END IF;
    CALL dropForeignKeyConstraints('phylonode',       'parent_node', 'phylonode');
    CALL dropForeignKeyConstraints('lineage',         'tax_id', 'phylonode');
    CALL dropForeignKeyConstraints('lineage',         'lineage_tax_id', 'phylonode');
    -- ignore previous errors
    -- SET @error_count := 0;

    SELECT '   (constraints removed)' AS 'Message';
    IF (columnExists('db', 'db_id')) THEN
      CALL changeTableKey('db',         'db_id',       'id', 'TINYINT(3) UNSIGNED NOT NULL AUTO_INCREMENT', 'CASCADE', 0);
    END IF;
    IF (columnExists('dbxref', 'dbxref_id')) THEN
      CALL changeTableKey('dbxref',     'dbxref_id',   'id', 'INT(10) UNSIGNED NOT NULL AUTO_INCREMENT', 'CASCADE', 1);
    END IF;
    IF (columnExists('family', 'family_id')) THEN
      CALL changeTableKey('family',     'family_id',   'id', 'MEDIUMINT(8) UNSIGNED NOT NULL AUTO_INCREMENT', 'CASCADE', 8);
    END IF;
    IF (columnExists('go', 'go_id')) THEN
      CALL changeTableKey('go',         'go_id',       'id', 'MEDIUMINT(8) UNSIGNED NOT NULL AUTO_INCREMENT', 'CASCADE', 0);
    END IF;
    IF (columnExists('homologies', 'homology_id')) THEN
      CALL changeTableKey('homologies', 'homology_id', 'id', 'INT(10) UNSIGNED NOT NULL AUTO_INCREMENT', 'CASCADE', 0);
    END IF;
    IF (columnExists('ipr', 'ipr_id')) THEN
      CALL changeTableKey('ipr',        'ipr_id',      'id', 'MEDIUMINT(8) UNSIGNED NOT NULL AUTO_INCREMENT', 'CASCADE', 4);
    END IF;
    IF (columnExists('phylonode', 'tax_id')) THEN
      CALL changeTableKey('phylonode',  'tax_id',      'id', 'INT(10) UNSIGNED NOT NULL DEFAULT \'0\'', 'CASCADE', 2);
    END IF;
    IF (columnExists('sequences', 'seq_id')) THEN
      CALL changeTableKey('sequences',  'seq_id',      'id', 'MEDIUMINT(8) UNSIGNED NOT NULL AUTO_INCREMENT', 'CASCADE', 5);
    END IF;
    IF (columnExists('species', 'species_id')) THEN
      CALL changeTableKey('species',    'species_id',  'id', 'SMALLINT(5) UNSIGNED NOT NULL AUTO_INCREMENT', 'CASCADE', 3);
    END IF;
    SELECT '...done normalize table indices.' AS 'Message';
  END;


  -- make sure no error occured
  IF (0 = @error_count) THEN


    -- families table (and related)
    SELECT 'Check families table...' AS 'Message';
    IF ((0 = @error_count) AND (NOT tableExists('family_synonyms'))) THEN
      BEGIN
        SELECT 'Update families table...' AS 'Message';
        -- normalize table name
        RENAME TABLE family TO families;
        -- remove constraint to change it later
        CALL dropForeignKeyConstraints('families', 'tax_id', 'phylonode');
        -- for ENUM transcription (prepare type change)...
        -- UPDATE families SET validated = validated + 1;
        UPDATE families SET type = type + 1 WHERE type IS NOT NULL;
        UPDATE families SET plant_specific = plant_specific + 1 WHERE plant_specific IS NOT NULL;
        -- for SET transcription (no NULL and update the case)
        UPDATE families SET inference = '' WHERE inference IS NULL OR inference LIKE 'N/A' OR inference LIKE 'NA';
        UPDATE families SET inference = 'IEA:InterPro'     WHERE inference LIKE 'IEA:InterPro';
        UPDATE families SET inference = 'IEA:PIRSF'        WHERE inference LIKE 'IEA:PIRSF';
        UPDATE families SET inference = 'IEA:UniProtKB'    WHERE inference LIKE 'IEA:UniProtKB';
        UPDATE families SET inference = 'IEA:UniProtKB-KW' WHERE inference LIKE 'IEA:UniProtKB-KW';
        UPDATE families SET inference = 'IEA:KEGG'         WHERE inference LIKE 'IEA:KEGG';
        UPDATE families SET inference = 'IEA:MEROPS'       WHERE inference LIKE 'IEA:MEROPS';
        UPDATE families SET inference = 'IEA:TF'           WHERE inference LIKE 'IEA:TF';
        UPDATE families SET inference = 'PubMed'           WHERE inference LIKE 'PubMed';
        UPDATE families SET inference = 'TAIR/TIGR'        WHERE inference LIKE 'TAIR/TIGR';
        UPDATE families SET inference = 'Other'            WHERE inference LIKE 'Other';
        -- description update: no NULL
        UPDATE families SET description = '' WHERE description IS NULL;
        -- update table structure
        ALTER TABLE families
          CHANGE family_name         name VARCHAR(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT ''
            COMMENT 'Human-readable family name',
          CHANGE accession           accession CHAR(8) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT ''
            COMMENT 'Family accession (cross-release identifier); GreenPhyl family accession must be upper case and begin with GP',
          CHANGE type                type ENUM('N/A', 'superfamily', 'family', 'subfamily', 'group') NOT NULL
            COMMENT 'Family cluster type',
          CHANGE description         description TEXT CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL
            COMMENT 'Family description and comments',
          CHANGE validated           validated ENUM('N/A', 'high', 'normal', 'unknown', 'suspicious', 'clustering error') NOT NULL
            COMMENT 'Human validation status',
          CHANGE black_list          black_listed BOOL NOT NULL DEFAULT '0'
            COMMENT 'Black-listed families are not considered as valid families',
          CHANGE inference           inferences SET('Other', 'IEA:InterPro', 'IEA:PIRSF', 'IEA:UniProtKB', 'IEA:UniProtKB-KW', 'IEA:KEGG', 'IEA:MEROPS', 'IEA:TF', 'PubMed', 'TAIR/TIGR') NOT NULL DEFAULT ''
            COMMENT 'annotation source',
          CHANGE tax_id              taxonomy_id INT(10) UNSIGNED NULL DEFAULT '0'
            COMMENT 'Family taxonomy root node (the most specific)',
          CHANGE plant_specific plant_specific TINYINT(3) NULL DEFAULT NULL
            COMMENT 'Percent of confidence for plant-specificity of current family',
          CHANGE min_seq_length      min_sequence_length SMALLINT(5) UNSIGNED NOT NULL DEFAULT '0'
            COMMENT 'Minimum sequence length',
          CHANGE max_seq_length      max_sequence_length SMALLINT(5) UNSIGNED NOT NULL DEFAULT '0'
            COMMENT 'Maximum sequence length',
          CHANGE average_seq_length  average_sequence_length SMALLINT(5) UNSIGNED NOT NULL DEFAULT '0'
            COMMENT 'Average sequence length',
          CHANGE median_seq_length   median_sequence_length SMALLINT(5) UNSIGNED NOT NULL DEFAULT '0'
            COMMENT 'Median sequence length',
          CHANGE alignment_length    alignment_length SMALLINT(5) UNSIGNED NOT NULL DEFAULT '0'
            COMMENT 'Number of columns in the initial alignment',
          CHANGE seq_count           sequence_count SMALLINT(5) UNSIGNED NOT NULL DEFAULT '0'
            COMMENT 'Number of sequences in the family',
          CHANGE alignment_seq_count alignment_sequence_count SMALLINT(5) UNSIGNED NOT NULL DEFAULT '0'
            COMMENT 'Number of sequences in the initial alignment',
          ADD                        level ENUM('1', '2', '3', '4') NULL DEFAULT NULL
            COMMENT 'Family classification level' AFTER accession,
          ADD                        masked_alignment_length SMALLINT(5) UNSIGNED NOT NULL DEFAULT '0'
            COMMENT 'Number of columns in the alignment after masking' AFTER alignment_length,
          ADD                        masked_alignment_sequence_count SMALLINT(5) UNSIGNED NOT NULL DEFAULT '0'
            COMMENT 'Number of sequences in the alignment after masking' AFTER alignment_sequence_count,
          ADD branch_average FLOAT NULL DEFAULT NULL
            COMMENT 'Phylogenetic tree branch depth average',
          ADD branch_median FLOAT NULL DEFAULT NULL
            COMMENT 'Phylogenetic tree branch depth median',
          ADD branch_standard_deviation FLOAT NULL DEFAULT NULL
            COMMENT 'Phylogenetic tree branch depth standard deviation',
          DROP pub_ref, -- we can use family_dbxref instead if necessary
          DROP pmid, -- we can use family_dbxref instead if necessary
          DROP updated; -- we will use the family_annotations table data instead
        -- index accessions
        ALTER TABLE families ADD INDEX (accession);
        -- SET NULL: we don't want taxonomy updates to remove families
        ALTER TABLE families ADD FOREIGN KEY (taxonomy_id) REFERENCES phylonode (id) ON DELETE SET NULL ON UPDATE CASCADE;
        -- transfer external table classification into "families" table and remove obsolete classification tables
        UPDATE families f JOIN found_in fi ON (f.id = fi.family_id) SET f.level = fi.class_id;
        ALTER TABLE families ADD INDEX (level);
        INSERT INTO variables VALUES ('classification', '1:MCL_I1.2:MCL Pipeline clusters with inflation 1.2;2:MCL_I2:MCL Pipeline clusters with inflation 2;3:MCL_I3:MCL Pipeline clusters with inflation 3;4:MCL_I5:MCL Pipeline clusters with inflation 5');
        DROP TABLE found_in;
        DROP TABLE classifications;
        -- Family DBXRef
        RENAME TABLE family_dbxref TO dbxref_families;
        ALTER TABLE dbxref_families ADD INDEX (family_id);
        -- normalize names
        RENAME TABLE synonym TO family_synonyms;

        -- transfer history
        RENAME TABLE family_history TO family_transfer_history;

        -- family history
        -- requires the appropriate triggers to be set after table creation
        CREATE TABLE IF NOT EXISTS family_history LIKE families;
        ALTER TABLE family_history
          CHANGE id id MEDIUMINT(8) UNSIGNED NOT NULL,
          ADD transaction_date TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP
            COMMENT 'date of transaction',
          ADD transaction_type ENUM('n/a', 'insert', 'update', 'delete') CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL
            COMMENT 'type of transaction that changed this value',
          ADD transaction_account VARCHAR(63) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL
            COMMENT 'user account that committed the transaction',
          ADD transaction_user VARCHAR(63) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL
            COMMENT 'authenticated user that committed the transaction'
        ;
        ALTER TABLE family_history DROP PRIMARY KEY;
        ALTER TABLE family_history ADD INDEX (id);

        SELECT '...done update families table.' AS 'Message';
      END;
    END IF;


    -- sequences table (and related)
    SELECT 'Check sequences table...' AS 'Message';
    IF ((0 = @error_count) AND (NOT tableExists('dbxref_sequences'))) THEN
      BEGIN
        SELECT 'Update sequences table...' AS 'Message';
        CREATE TABLE IF NOT EXISTS sequence_synonyms (
          sequence_id MEDIUMINT(8) UNSIGNED NOT NULL,
          synonym VARCHAR(255) NOT NULL COMMENT 'Synonym of the sequence accession in the sequences table',
          rank TINYINT(3) UNSIGNED NOT NULL DEFAULT '1' COMMENT 'Synonym rank (low value, high priority)',
          PRIMARY KEY (sequence_id,synonym)
        ) ENGINE=InnoDB DEFAULT CHARSET=utf8;
        ALTER TABLE sequence_synonyms
          ADD FOREIGN KEY (sequence_id) REFERENCES sequences (id) ON DELETE CASCADE ON UPDATE CASCADE;
        INSERT INTO sequence_synonyms (sequence_id, synonym) SELECT id, synonym FROM sequences WHERE synonym IS NOT NULL;
        ALTER TABLE sequences
          CHANGE seq_textid accession VARCHAR(127) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '',
          CHANGE seq_length length INT(10) UNSIGNED NOT NULL DEFAULT '0',
          CHANGE annotation annotation TEXT CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
          CHANGE sequence   polypeptide LONGTEXT CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
          CHANGE gene_name  locus VARCHAR(127) CHARACTER SET utf8 COLLATE utf8_general_ci AFTER accession,
          ADD splice TINYINT(3) UNSIGNED NULL DEFAULT NULL AFTER locus
            COMMENT 'Index of the splice form',
          ADD splice_priority TINYINT(3) UNSIGNED NULL DEFAULT NULL AFTER splice
            COMMENT 'Splice form ranking: lower values, higher priorities',
          DROP alias,
          DROP hidden,
          DROP scanned,
          DROP synonym;

        ALTER TABLE sequences_history
          CHANGE seq_id sequence_id MEDIUMINT(8) UNSIGNED NOT NULL,
          CHANGE old_seq_textid old_accession VARCHAR(127) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL ,
          CHANGE new_seq_textid new_accession VARCHAR(127) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL;
        RENAME TABLE sequences_history TO sequence_transfer_history;

        RENAME TABLE sequence_dbxref TO dbxref_sequences;
        ALTER TABLE dbxref_sequences CHANGE seq_id sequence_id MEDIUMINT(8) UNSIGNED NOT NULL;


        SELECT '...done update sequences table.' AS 'Message';
      END;
    END IF;
    IF (NOT constraintExists('dbxref_sequences', 'sequence_id', 'sequences')) THEN
      ALTER TABLE dbxref_sequences ADD FOREIGN KEY (sequence_id) REFERENCES sequences (id) ON DELETE CASCADE ON UPDATE CASCADE;
    END IF;


    -- species table
    SELECT 'Check species table...' AS 'Message';
    IF ((0 = @error_count) AND (NOT columnExists('species', 'color'))) THEN
      BEGIN
        SELECT 'Update species table...' AS 'Message';
        ALTER TABLE species
          CHANGE species_name code VARCHAR(15) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL
            COMMENT 'Species code in capital letters',
          CHANGE sequence_number sequence_count MEDIUMINT(8) UNSIGNED NOT NULL DEFAULT '0'
            COMMENT 'Number of sequences in database for the species',
          CHANGE chromosome_number chromosome_count TINYINT(3) UNSIGNED NULL DEFAULT NULL
            COMMENT 'Number of chromosome of the species'
            AFTER sequence_count,
          CHANGE genome_size genome_size FLOAT DEFAULT NULL
            COMMENT 'Genome size in megabasepair (Mbp)'
            AFTER chromosome_count,
          CHANGE tax_order display_order INT(10) NOT NULL DEFAULT '0'
            COMMENT 'Position number of the species on the homepage tree'
            AFTER url_picture,
          CHANGE version version VARCHAR(31) NOT NULL DEFAULT '',
          ADD color CHAR(6) NOT NULL DEFAULT '808080'
            COMMENT '24bit RGB hexadecimal color code for the species'
            AFTER display_order;
        -- taxonomy constraint
        CALL dropForeignKeyConstraints('species', 'tax_id', 'phylonode');
        ALTER TABLE species CHANGE tax_id taxonomy_id INT(10) UNSIGNED NULL DEFAULT '0';
        ALTER TABLE species ADD FOREIGN KEY (taxonomy_id) REFERENCES phylonode (id) ON DELETE SET NULL ON UPDATE CASCADE;
        SELECT '...done update species table.' AS 'Message';
      END;
    END IF;


    -- bbmh table
    SELECT 'Check bbmh table...' AS 'Message';
    IF ((0 = @error_count) AND (NOT columnExists('bbmh', 'e_value'))) THEN
      BEGIN
        SELECT 'Update bbmh table...' AS 'Message';
        CALL dropForeignKeyConstraints('bbmh', 'query', 'sequences');
        CALL dropForeignKeyConstraints('bbmh', 'hit', 'sequences');
        ALTER TABLE bbmh CHANGE query query_sequence_id MEDIUMINT(8) UNSIGNED NOT NULL  COMMENT 'Query BLAST sequence';
        ALTER TABLE bbmh CHANGE hit hit_sequence_id MEDIUMINT(8) UNSIGNED NOT NULL COMMENT 'Matching sequence found by BLAST';
        ALTER TABLE bbmh CHANGE score score FLOAT NOT NULL COMMENT 'BLAST score';
        ALTER TABLE bbmh CHANGE eval e_value DOUBLE NOT NULL COMMENT 'BLAST result e-value';
        ALTER TABLE bbmh ADD FOREIGN KEY (query_sequence_id) REFERENCES sequences (id) ON DELETE CASCADE ON UPDATE CASCADE;
        ALTER TABLE bbmh ADD FOREIGN KEY (hit_sequence_id) REFERENCES sequences (id) ON DELETE CASCADE ON UPDATE CASCADE;
        SELECT '...done update bbmh table.' AS 'Message';
      END;
    END IF;


    -- homologs
    SELECT 'Check homologs table...' AS 'Message';
    IF ((0 = @error_count) AND (NOT columnExists('homologs', 'object_sequence_id'))) THEN
      BEGIN
        SELECT 'Update homologs table...' AS 'Message';
        RENAME TABLE homologs TO homologs_old;
        ALTER TABLE homologs_old ADD object_sequence_id MEDIUMINT(8) UNSIGNED NULL DEFAULT NULL;
        UPDATE homologs_old h1 JOIN homologs_old h2 ON (h1.homology_id = h2.homology_id) SET h1.object_sequence_id = h2.seq_id WHERE h2.seq_id != h1.seq_id;
        CREATE TABLE IF NOT EXISTS homologs (
          homology_id INT(10) UNSIGNED NOT NULL,
          subject_sequence_id MEDIUMINT(8) UNSIGNED NOT NULL COMMENT 'Sequence we focus on',
          object_sequence_id MEDIUMINT(8) UNSIGNED NOT NULL COMMENT 'Related sequence',
          PRIMARY KEY (subject_sequence_id,object_sequence_id),
          KEY homology_id (homology_id),
          KEY subject_sequence_id (subject_sequence_id),
          KEY object_sequence_id (object_sequence_id)
        ) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Contains homology relationships';
        ALTER TABLE homologs ADD FOREIGN KEY (subject_sequence_id) REFERENCES sequences (id) ON DELETE CASCADE ON UPDATE CASCADE;
        ALTER TABLE homologs ADD FOREIGN KEY (object_sequence_id) REFERENCES sequences (id) ON DELETE CASCADE ON UPDATE CASCADE;
        INSERT IGNORE INTO homologs (homology_id, subject_sequence_id, object_sequence_id) (SELECT CAST(homology_id AS UNSIGNED), seq_id, object_sequence_id FROM homologs_old);
        DROP TABLE homologs_old;
        -- remove unused homologies
        DELETE FROM homologies WHERE NOT EXISTS (SELECT TRUE FROM homologs hs WHERE homologies.id = hs.homology_id LIMIT 1);
        ALTER TABLE homologies
          ADD COLUMN u_duplication INT(11) NOT NULL
        ;
        SELECT '...done update homologs table.' AS 'Message';
      END;
    END IF;
    IF (NOT constraintExists('homologs', 'homology_id', 'homologies')) THEN
      SELECT 'Add foreign key constraint on homologs for homology_id.' AS 'Message';
      ALTER TABLE homologs ADD FOREIGN KEY (homology_id) REFERENCES homologies (id) ON DELETE CASCADE ON UPDATE CASCADE;
    END IF;


    -- phylonode and lineage tables
    SELECT 'Check taxonomy table...' AS 'Message';
    IF ((0 = @error_count) AND (NOT columnExists('lineage', 'lineage_taxonomy_id'))) THEN
      BEGIN
        SELECT 'Update taxonomy table...' AS 'Message';
        RENAME TABLE phylonode TO taxonomy;
        ALTER TABLE taxonomy CHANGE parent_node parent_taxonomy_id INT(10) UNSIGNED NULL DEFAULT NULL;
        RENAME TABLE phylonode_synonym TO taxonomy_synonyms;
        ALTER TABLE taxonomy_synonyms
          CHANGE name name VARCHAR(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL;
        -- lineage
        ALTER TABLE lineage
          CHANGE tax_id taxonomy_id INT(10) UNSIGNED NOT NULL DEFAULT '0',
          CHANGE lineage_tax_id lineage_taxonomy_id INT(10) UNSIGNED NOT NULL DEFAULT '0'
        ;
        SELECT '...done update taxonomy table.' AS 'Message';
        -- synonyms
        ALTER TABLE taxonomy_synonyms
          CHANGE tax_id taxonomy_id INT(10) UNSIGNED NOT NULL
        ;
      END;
    END IF;
    IF (NOT constraintExists('taxonomy', 'parent_taxonomy_id', 'taxonomy')) THEN
      ALTER TABLE taxonomy ADD CONSTRAINT FOREIGN KEY (parent_taxonomy_id) REFERENCES taxonomy (id);
    END IF;
    IF (NOT constraintExists('lineage', 'taxonomy_id', 'taxonomy')) THEN
      ALTER TABLE lineage ADD CONSTRAINT FOREIGN KEY (taxonomy_id) REFERENCES taxonomy (id);
    END IF;
    IF (NOT constraintExists('lineage', 'lineage_taxonomy_id', 'taxonomy')) THEN
      ALTER TABLE lineage ADD CONSTRAINT FOREIGN KEY (lineage_taxonomy_id) REFERENCES taxonomy (id);
    END IF;


    -- db table
    SELECT 'Check db table...' AS 'Message';
    IF ((0 = @error_count) AND (NOT columnExists('db', 'site_url'))) THEN
      BEGIN
        SELECT 'Update db table...' AS 'Message';
        ALTER TABLE db
          CHANGE urlprefix query_url_prefix VARCHAR(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
          CHANGE url       site_url VARCHAR(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL;
        SELECT '...done update db table.' AS 'Message';
      END;
    END IF;


    -- dbxref table and related
    SELECT 'Check dbxref table...' AS 'Message';
    IF ((0 = @error_count) AND (columnExists('dbxref', 'type'))) THEN
      BEGIN
        SELECT 'Update dbxref table...' AS 'Message';
        CREATE TABLE IF NOT EXISTS dbxref_synonyms (
          dbxref_id INT(10) UNSIGNED NOT NULL,
          synonym VARCHAR(255) NOT NULL,
          PRIMARY KEY (dbxref_id, synonym)
        ) ENGINE = InnoDB;
        INSERT INTO dbxref_synonyms (dbxref_id, synonym) SELECT id, alias FROM dbxref WHERE alias IS NOT NULL AND alias != '';
        ALTER TABLE dbxref
          ADD dataset VARCHAR(31) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' AFTER description,
          CHANGE db_id db_id TINYINT(3) UNSIGNED NOT NULL,
          CHANGE accession accession VARCHAR(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
          CHANGE version version VARCHAR(31) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
          DROP alias,
          DROP type
        ;
        UPDATE dbxref SET version = NULL WHERE version = '0.0' OR version = '';
        SELECT '...done update dbxref table.' AS 'Message';
      END;
    END IF;
    IF (NOT constraintExists('dbxref', 'db_id', 'db')) THEN
      ALTER TABLE dbxref ADD FOREIGN KEY (db_id) REFERENCES db (id) ON DELETE CASCADE ON UPDATE CASCADE;
    END IF;
    IF (NOT constraintExists('dbxref_sequences', 'dbxref_id', 'dbxref')) THEN
      ALTER TABLE dbxref_sequences ADD FOREIGN KEY (dbxref_id) REFERENCES dbxref (id) ON DELETE CASCADE ON UPDATE CASCADE;
    END IF;

    SELECT 'Check evidence_codes table...' AS 'Message';
    IF ((0 = @error_count) AND (NOT tableExists('evidence_codes'))) THEN
      BEGIN
        SELECT 'Add evidence_codes table...' AS 'Message';
        CREATE TABLE evidence_codes (
          code VARCHAR(3) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL
            COMMENT 'Evidence code',
          category VARCHAR(31) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
          description VARCHAR(127) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
          PRIMARY KEY (code)
        ) ENGINE = InnoDB CHARACTER SET utf8 COLLATE utf8_general_ci;
        
        SELECT 'Fill evidence_codes table...' AS 'Message';
        INSERT INTO evidence_codes (code, category, description) VALUES
            ('EXP', 'experimental', 'Inferred from Experiment'),
            ('IDA', 'experimental', 'Inferred from Direct Assay'),
            ('IPI', 'experimental', 'Inferred from Physical Interaction'),
            ('IMP', 'experimental', 'Inferred from Mutant Phenotype'),
            ('IGI', 'experimental', 'Inferred from Genetic Interaction'),
            ('IEP', 'experimental', 'Inferred from Expression Pattern'),
            ('ISS', 'computational analysis', 'Inferred from Sequence or Structural Similarity'),
            ('ISO', 'computational analysis', 'Inferred from Sequence Orthology'),
            ('ISA', 'computational analysis', 'Inferred from Sequence Alignment'),
            ('ISM', 'computational analysis', 'Inferred from Sequence Model'),
            ('IGC', 'computational analysis', 'Inferred from Genomic Context'),
            ('IBA', 'computational analysis', 'Inferred from Biological aspect of Ancestor'),
            ('IBD', 'computational analysis', 'Inferred from Biological aspect of Descendant'),
            ('IKR', 'computational analysis', 'Inferred from Key Residues'),
            ('IRD', 'computational analysis', 'Inferred from Rapid Divergence'),
            ('RCA', 'computational analysis', 'inferred from Reviewed Computational Analysis'),
            ('TAS', 'author statements', 'Traceable Author Statement'),
            ('NAS', 'author statements', 'Non-traceable Author Statement'),
            ('IC',  'curatorial statements', 'Inferred by Curator'),
            ('ND',  'curatorial statements', 'No biological Data available'),
            ('IEA', 'automatically-assigned statements', 'Inferred from Electronic Annotation'),
            ('NR',  'obsolete', 'Not Recorded')
        ;
        SELECT '...done setting up evidence_codes table.' AS 'Message';
    END IF;

    -- GO
    SELECT 'Check GO table and related tables...' AS 'Message';
    IF ((0 = @error_count) AND (NOT tableExists('go_lineage'))) THEN
      BEGIN
        SELECT 'Update GO table and create related tables...' AS 'Message';
        -- normalize values
        UPDATE go SET go_type = 'biological_process' WHERE go_type LIKE 'biological_process' OR go_type LIKE 'Process' OR go_type LIKE 'Biological';
        UPDATE go SET go_type = 'cellular_component' WHERE go_type LIKE 'cellular_component' OR go_type LIKE 'Component' OR go_type LIKE 'Cellular';
        UPDATE go SET go_type = 'molecular_function' WHERE go_type LIKE 'molecular_function' OR go_type LIKE 'Function' OR go_type LIKE 'Molecular';
        -- remove incomplete/unclassified GO terms
        DELETE FROM go WHERE go_type IS NULL OR go_type = '';

        ALTER TABLE go
          CHANGE go_code code CHAR(10) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL
            COMMENT 'GO public ID including "GO:" part',
          CHANGE go_type namespace ENUM('cellular_component', 'molecular_function', 'biological_process') CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
          CHANGE go_desc name VARCHAR(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL
        ;
        ALTER TABLE go ADD UNIQUE (code);

        -- GO synonyms and alternate IDs
        CREATE TABLE go_synonyms (
          go_id MEDIUMINT(8) UNSIGNED NOT NULL,
          synonym VARCHAR(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
          INDEX (go_id),
          UNIQUE KEY synonym (synonym)
        ) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT 'GO term name synonym from the "synonym" GO term tag';
        ALTER TABLE go_synonyms ADD FOREIGN KEY (go_id) REFERENCES go (id) ON DELETE CASCADE ON UPDATE CASCADE;

        CREATE TABLE go_alternates (
          go_id MEDIUMINT(8) UNSIGNED NOT NULL,
          alternate_code CHAR(10) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
          INDEX (go_id),
          UNIQUE (go_id, alternate_code)
        ) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT 'GO term alternate ID from the "alt_id" GO term tag';
        ALTER TABLE go_alternates ADD FOREIGN KEY (go_id) REFERENCES go (id) ON DELETE CASCADE ON UPDATE CASCADE;

        -- relationships between GO terms
        CREATE TABLE go_relationships (
          subject_go_id MEDIUMINT(8) UNSIGNED NOT NULL,
          object_go_id MEDIUMINT(8) UNSIGNED NOT NULL,
          relationship VARCHAR(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT 'Relationships like "is_a", "part_of", "regulates",...',
          INDEX (subject_go_id, object_go_id)
        ) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT 'Includes GO term relationships from both GO and custom GreenPhyl relationships';
        ALTER TABLE go_relationships
          ADD INDEX (subject_go_id),
          ADD INDEX (object_go_id)
        ;
        ALTER TABLE go_relationships
          ADD FOREIGN KEY (subject_go_id) REFERENCES go (id) ON DELETE CASCADE ON UPDATE CASCADE,
          ADD FOREIGN KEY (object_go_id) REFERENCES go (id) ON DELETE CASCADE ON UPDATE CASCADE,
          ADD UNIQUE (subject_go_id, object_go_id, relationship)
        ;

        CREATE TABLE go_lineage (
          go_id MEDIUMINT(8) UNSIGNED NOT NULL,
          lineage_go_codes VARCHAR(65527) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT 'up to 5957 coma-separated GO codes',
          UNIQUE (go_id)
        ) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT 'Stores the list of ancestors for a given GO';

        ALTER TABLE go_lineage
          ADD FOREIGN KEY (go_id) REFERENCES go (id) ON DELETE CASCADE ON UPDATE CASCADE
        ;
        -- To tell if a GO "beta" is an ancestor of a GO "alpha":
        -- SELECT FIND_IN_SET(go_beta.code, go_alpha.lineage_go_codes) FROM go_lineage go_alpha, go go_beta WHERE go_alpha.go_id = ? AND go_beta.id = ?;
        -- To fill:
        -- INSERT INTO go_lineage VALUES (19248, CONCAT_WS(',', 'GO:0070569', 'GO:0016779', 'GO:0016772', 'GO:0016740', 'GO:0003824', 'GO:0003674', 'GO:0008152', 'GO:0008150'));
        -- Lineage relationship to take in account:
        -- 'part_of', 'is_a', 'regulates', 'negatively_regulates', 'positively_regulates'

        SELECT '...done update GO tables.' AS 'Message';
      END;
    END IF;



    SELECT 'GO associations...' AS 'Message';
    -- GO <--> IPR
    IF ((0 = @error_count) AND (NOT tableExists('go_ipr'))) THEN
      BEGIN
        SELECT ' -ipr_to_go...' AS 'Message';
        RENAME TABLE ipr_to_go TO go_ipr;
        ALTER TABLE go_ipr
          CHANGE go_id go_id MEDIUMINT(8) UNSIGNED NOT NULL,
          CHANGE evidence_code evidence_code VARCHAR(3) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
          ADD INDEX (ipr_id),
          ADD INDEX (evidence_code)
        ;
        ALTER TABLE go_ipr
          ADD FOREIGN KEY (evidence_code) REFERENCES evidence_codes (code)
            ON DELETE SET NULL ON UPDATE CASCADE
        ;

        SELECT ' ...ipr_to_go done.' AS 'Message';
      END;
    END IF;
    IF (NOT constraintExists('go_ipr', 'go_id', 'go')) THEN
      ALTER TABLE go_ipr ADD FOREIGN KEY (go_id) REFERENCES go (id) ON DELETE CASCADE ON UPDATE CASCADE;
    END IF;


    -- GO <--> UniProt
    IF ((0 = @error_count) AND (NOT tableExists('go_uniprot'))) THEN
      BEGIN
        SELECT ' -uniprot_to_go...' AS 'Message';
        RENAME TABLE uniprot_to_go TO go_uniprot;
        ALTER TABLE go_uniprot
          CHANGE go_id go_id MEDIUMINT(8) UNSIGNED NOT NULL,
          CHANGE evidence_code evidence_code VARCHAR(3) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
          ADD INDEX (evidence_code),
          ADD INDEX (dbxref_id)
        ;
        ALTER TABLE go_uniprot
          ADD FOREIGN KEY (evidence_code) REFERENCES evidence_codes (code)
            ON DELETE SET NULL ON UPDATE CASCADE
        ;
        SELECT ' ...uniprot_to_go done.' AS 'Message';
      END;
    END IF;
    IF (NOT constraintExists('go_uniprot', 'go_id', 'go')) THEN
      ALTER TABLE go_uniprot ADD FOREIGN KEY (go_id) REFERENCES go (id) ON DELETE CASCADE ON UPDATE CASCADE;
    END IF;
    IF (NOT constraintExists('go_uniprot', 'dbxref_id', 'dbxref')) THEN
      ALTER TABLE go_uniprot ADD FOREIGN KEY (dbxref_id) REFERENCES dbxref (id) ON DELETE CASCADE ON UPDATE CASCADE;
    END IF;


    -- GO <--> EC
    IF ((0 = @error_count) AND (NOT tableExists('ec_go'))) THEN
      BEGIN
        SELECT ' -ec_to_go...' AS 'Message';
        RENAME TABLE ec_to_go TO ec_go;
        ALTER TABLE ec_go
          CHANGE go_id go_id MEDIUMINT(8) UNSIGNED NOT NULL
          -- ADD INDEX (dbxref_id),
          -- ADD INDEX (go_id)
        ;
        SELECT ' ...ec_to_go done.' AS 'Message';
      END;
    END IF;
    IF (NOT constraintExists('ec_go', 'go_id', 'go')) THEN
      ALTER TABLE ec_go ADD FOREIGN KEY (go_id) REFERENCES go (id) ON DELETE CASCADE ON UPDATE CASCADE;
    END IF;
    IF (NOT constraintExists('ec_go', 'dbxref_id', 'dbxref')) THEN
      ALTER TABLE ec_go ADD FOREIGN KEY (dbxref_id) REFERENCES dbxref (id) ON DELETE CASCADE ON UPDATE CASCADE;
    END IF;

    SELECT '...done GO associations.' AS 'Message';

    -- IPR
    SELECT 'Check ipr table...' AS 'Message';
    IF ((0 = @error_count) AND (NOT columnExists('ipr', 'pirsf_description'))) THEN
      BEGIN
        SELECT 'IPR update...' AS 'Message';
        ALTER TABLE ipr
          CHANGE ipr_code code CHAR(9) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL
            COMMENT 'Code assigned by Interpro for a protein pattern',
          CHANGE ipr_longdesc description VARCHAR(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL
            COMMENT 'Annotation of the protein pattern',
          CHANGE version version TINYINT(3) UNSIGNED NULL DEFAULT NULL
            COMMENT 'Version of interpro used with interproscan',
          CHANGE pirsf pirsf_code CHAR(11) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL
            COMMENT 'IPR having as source PIR database',
          CHANGE pirsf_longdesc pirsf_description VARCHAR(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL
            COMMENT 'Annotation of the PIR homeomorphic family';
        SELECT '...done IPR update.' AS 'Message';
      END;
    END IF;


    -- IPR <--> Sequences
    IF ((0 = @error_count) AND (NOT columnExists('ipr_sequences', 'score'))) THEN
      BEGIN
        RENAME TABLE seq_has_an TO ipr_sequences;
        ALTER TABLE ipr_sequences
          CHANGE seq_id sequence_id MEDIUMINT(8) UNSIGNED NOT NULL,
          CHANGE ipr_id ipr_id MEDIUMINT(8) UNSIGNED NOT NULL,
          CHANGE ipr_start start INT(10) UNSIGNED NOT NULL DEFAULT '0',
          CHANGE ipr_stop end INT(10) UNSIGNED NOT NULL DEFAULT '0',
          CHANGE ipr_score score DOUBLE NULL DEFAULT NULL;
        ALTER TABLE ipr_sequences ADD INDEX (sequence_id);
        ALTER TABLE ipr_sequences ADD FOREIGN KEY (sequence_id) REFERENCES sequences (id) ON DELETE CASCADE ON UPDATE CASCADE;
        ALTER TABLE ipr_sequences ADD FOREIGN KEY (ipr_id) REFERENCES ipr (id) ON DELETE CASCADE ON UPDATE CASCADE;
      END;
    END IF;


    -- Families <--> Sequences
    IF ((0 = @error_count) AND (NOT columnExists('families_sequences', 'sequence_id'))) THEN
      BEGIN
        RENAME TABLE seq_is_in TO families_sequences;
        ALTER TABLE families_sequences CHANGE seq_id sequence_id MEDIUMINT(8) UNSIGNED NOT NULL;
        ALTER TABLE families_sequences
          ADD INDEX (sequence_id);
      END;
    END IF;
    IF (NOT constraintExists('families_sequences', 'sequence_id', 'sequences')) THEN
      ALTER TABLE families_sequences ADD FOREIGN KEY (sequence_id) REFERENCES sequences (id) ON DELETE CASCADE ON UPDATE CASCADE;
    END IF;
    IF (NOT constraintExists('families_sequences', 'family_id', 'families')) THEN
      ALTER TABLE families_sequences ADD FOREIGN KEY (family_id) REFERENCES families (id) ON DELETE CASCADE ON UPDATE CASCADE;
    END IF;

    -- user stuff
    SELECT 'Check user stuff...' AS 'Message';
    IF ((0 = @error_count) AND (NOT tableExists('users'))) THEN
      BEGIN
        SELECT 'User authentication...' AS 'Message';

        CREATE TABLE IF NOT EXISTS users (
          id SMALLINT(5) UNSIGNED NOT NULL AUTO_INCREMENT,
          name varchar(255) NOT NULL,
          email VARCHAR(255) NULL DEFAULT NULL,
          password_hash CHAR(40) NOT NULL,
          salt CHAR(40) NOT NULL,
          flags SET('administrator', 'annotator', 'registered user', 'disabled') NOT NULL DEFAULT '',
          description TEXT CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
          PRIMARY KEY (id),
          UNIQUE KEY name (name),
          UNIQUE KEY email (email)
        ) ENGINE=InnoDB DEFAULT CHARSET=utf8;

        CREATE TABLE IF NOT EXISTS user_openids (
          user_id smallint(5) unsigned NOT NULL,
          openid_url varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
          openid_identity varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
          priority tinyint(3) unsigned NOT NULL DEFAULT '0',
          INDEX (user_id)
        );

        CREATE TABLE IF NOT EXISTS openids (
          name VARCHAR(255) NOT NULL,
          label VARCHAR(255) DEFAULT NULL COMMENT 'NULL means the user has nothing to enter and there is no field to substitute in the URL',
          url VARCHAR(1024) DEFAULT NULL COMMENT 'Should use {username} in the url as placeholder for user identifier',
          enabled BOOL NOT NULL DEFAULT '1',
          icon_size ENUM('small', 'large') NOT NULL DEFAULT 'small',
          icon_index smallint(3) unsigned NOT NULL COMMENT 'index in the icon image'
        );

        ALTER TABLE user_openids
          ADD FOREIGN KEY (user_id) REFERENCES users (id) ON DELETE CASCADE ON UPDATE CASCADE,
          ADD UNIQUE (openid_url, openid_identity),
          ADD UNIQUE (user_id, priority)
        ;

        ALTER TABLE openids
          ADD UNIQUE (name)
        ;

        -- upgrade user accounts
        CALL upgradeGreenPhylUser();

        -- remove old table
        DROP TABLE annot_user;

        CREATE TABLE groups (
          id SMALLINT(5) UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY,
          name VARCHAR(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
          description TEXT CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
          UNIQUE (name)
        ) ENGINE = InnoDB CHARACTER SET utf8 COLLATE utf8_general_ci;

        CREATE TABLE groups_users (
          group_id SMALLINT(5) UNSIGNED NOT NULL,
          user_id SMALLINT(5) UNSIGNED NOT NULL,
          UNIQUE (group_id, user_id)
        ) ENGINE = InnoDB;

        ALTER TABLE groups_users
          ADD FOREIGN KEY (group_id) REFERENCES groups(id) ON DELETE CASCADE ON UPDATE CASCADE,
          ADD FOREIGN KEY (user_id) REFERENCES users (id) ON DELETE CASCADE ON UPDATE CASCADE
        ;

        CREATE TABLE IF NOT EXISTS files (
          id mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
          name varchar(128) NOT NULL,
          path varchar(256) NOT NULL,
          url varchar(256) DEFAULT NULL,
          type varchar(64) DEFAULT NULL,
          flags set('missing','private') NOT NULL,
          size int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'file size in bytes',
          user_id smallint(5) unsigned DEFAULT NULL,
          PRIMARY KEY (id),
          KEY user_id (user_id)
        ) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Table used to manage user-uploaded files' AUTO_INCREMENT=1 ;

        SELECT '...done user authentication.' AS 'Message';
      END;
    END IF;

    -- Annotation tables
    IF ((0 = @error_count) AND (NOT tableExists('sequence_annotations'))) THEN
      BEGIN
        SELECT 'Annotation-related tables...' AS 'Message';
        RENAME TABLE annot_family TO family_annotations;
        -- class_id to type
        UPDATE family_annotations SET class_id = class_id + 1;
        UPDATE family_annotations SET conf_level = conf_level + 1;
        UPDATE family_annotations SET status_ann = status_ann + 1;
        -- for SET transcription (no NULL and update the case)
        UPDATE family_annotations SET inference = '' WHERE inference IS NULL OR inference LIKE 'N/A' OR inference LIKE 'NA';
        UPDATE family_annotations SET inference = 'IEA:InterPro'     WHERE inference LIKE 'IEA:InterPro';
        UPDATE family_annotations SET inference = 'IEA:PIRSF'        WHERE inference LIKE 'IEA:PIRSF';
        UPDATE family_annotations SET inference = 'IEA:UniProtKB'    WHERE inference LIKE 'IEA:UniProtKB';
        UPDATE family_annotations SET inference = 'IEA:UniProtKB-KW' WHERE inference LIKE 'IEA:UniProtKB-KW';
        UPDATE family_annotations SET inference = 'IEA:KEGG'         WHERE inference LIKE 'IEA:KEGG';
        UPDATE family_annotations SET inference = 'IEA:MEROPS'       WHERE inference LIKE 'IEA:MEROPS';
        UPDATE family_annotations SET inference = 'IEA:TF'           WHERE inference LIKE 'IEA:TF';
        UPDATE family_annotations SET inference = 'PubMed'           WHERE inference LIKE 'PubMed';
        UPDATE family_annotations SET inference = 'TAIR/TIGR'        WHERE inference LIKE 'TAIR/TIGR';
        UPDATE family_annotations SET inference = 'Other'            WHERE inference LIKE 'Other';
        -- update table structure
        ALTER TABLE family_annotations
            CHANGE num_enr     id MEDIUMINT(8) NOT NULL AUTO_INCREMENT,
            CHANGE user_id     user_id SMALLINT(5) UNSIGNED NULL DEFAULT NULL
              COMMENT 'NULL value means "anonymous"',
            CHANGE family_id   accession CHAR(8) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
            CHANGE family_name name VARCHAR(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
            CHANGE class_id    type ENUM( 'N/A', 'superfamily', 'family', 'subfamily', 'group' ) NULL DEFAULT NULL
              COMMENT 'Family cluster type',
            CHANGE synonym synonyms VARCHAR(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
            CHANGE inference inferences SET('IEA:InterPro', 'IEA:PIRSF', 'IEA:UniProtKB', 'IEA:UniProtKB-KW', 'IEA:KEGG', 'IEA:MEROPS', 'IEA:TF', 'PubMed', 'TAIR/TIGR', 'Other' ) NULL DEFAULT NULL
              COMMENT 'Annotation sources',
            CHANGE date date TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
            CHANGE conf_level confidence_level ENUM('N/A', 'high', 'normal', 'unknown', 'suspicious', 'clustering error') NULL
              COMMENT 'Human annotation confidence level',
            CHANGE cross_ref dbxref MEDIUMTEXT CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
            CHANGE status_ann status ENUM('in progress', 'submitted', 'validated', 'denied') NOT NULL,
            ADD user_name VARCHAR(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '',
            ADD description TEXT CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT ''
              COMMENT 'Family description and comments',
            ADD comments TEXT CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT ''
              COMMENT 'User comments about his/her annotation or the family',
            DROP sp_specific,
            DROP auto_annot,
            DROP phylo,
            DROP go_ref,
            DROP INDEX annotation_index2
        ;
        -- ALTER TABLE family_annotations
        --     ADD UNIQUE (user_id,accession)
        -- ;
        UPDATE family_annotations SET accession = CONCAT('GP', RIGHT(CONCAT('000000', accession), 6));

        -- no more sequence annotations
        DROP TABLE sequence_annotations;

        SELECT '...done annotation-related tables.' AS 'Message';
      END;
    END IF;

    -- Custom family tables
    IF ((0 = @error_count) AND (NOT tableExists('custom_family_relationships'))) THEN
      BEGIN
        SELECT 'Custom family tables...' AS 'Message';

        CREATE TABLE custom_families (
          id MEDIUMINT(8) UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY,
          name VARCHAR(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
          accession CHAR(12) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
          level TINYINT(3) UNSIGNED NOT NULL DEFAULT 0,
          type ENUM('N/A', 'superfamily', 'family', 'subfamily', 'group') NOT NULL,
          description TEXT CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
          inference SET('Other', 'IEA:InterPro', 'IEA:PIRSF', 'IEA:UniProtKB', 'IEA:UniProtKB-KW', 'IEA:KEGG', 'IEA:MEROPS', 'IEA:TF', 'PubMed', 'TAIR/TIGR') NOT NULL DEFAULT '',
          user_id SMALLINT(5) UNSIGNED NOT NULL,
          access ENUM('private', 'restricted to group - read only', 'restricted to group - editable', 'restricted by code - read only', 'restricted by code - editable', 'public - read only', 'public - editable') NOT NULL,
          password_hash CHAR(40) NULL DEFAULT NULL,
          salt CHAR(40) NULL DEFAULT NULL,
          data blob,
          flags set('listed','obsolete','peer-reviewed','panel-validated','conflicting') NOT NULL DEFAULT '',
          creation_date timestamp NULL DEFAULT NULL,
          last_update timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
        ) ENGINE = InnoDB CHARACTER SET utf8 COLLATE utf8_general_ci;

        ALTER TABLE custom_families
          ADD INDEX (accession),
          ADD INDEX (user_id),
          ADD UNIQUE (
            user_id,
            accession
          ),
          ADD FOREIGN KEY (user_id) REFERENCES users (id) ON DELETE CASCADE ON UPDATE CASCADE
        ;

        CREATE TABLE IF NOT EXISTS custom_family_history LIKE custom_families;
        ALTER TABLE custom_family_history
          CHANGE id id MEDIUMINT(8) UNSIGNED NOT NULL,
          ADD transaction_date TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP
            COMMENT 'date of transaction',
          ADD transaction_type ENUM('n/a', 'insert', 'update', 'delete') CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL
            COMMENT 'type of transaction that changed this value',
          ADD transaction_account VARCHAR(63) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL
            COMMENT 'user account that committed the transaction',
          ADD transaction_user VARCHAR(63) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL
            COMMENT 'authenticated user that committed the transaction'
        ;
        ALTER TABLE custom_family_history DROP PRIMARY KEY;
        ALTER TABLE custom_family_history DROP INDEX user_id;
        ALTER TABLE custom_family_history ADD INDEX (id);

        CREATE TABLE custom_sequences (
          id INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
          accession VARCHAR(127) NOT NULL DEFAULT '',
          locus VARCHAR(127) DEFAULT NULL,
          splice TINYINT(3) UNSIGNED DEFAULT NULL,
          splice_priority TINYINT(3) UNSIGNED DEFAULT NULL,
          length INT(10) UNSIGNED NOT NULL DEFAULT '0',
          species_id SMALLINT(5) UNSIGNED NOT NULL DEFAULT '0',
          organism VARCHAR(63) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
          annotation TEXT,
          polypeptide LONGTEXT,
          user_id SMALLINT(5) UNSIGNED NOT NULL,
          data BLOB NULL DEFAULT NULL,
          last_update timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
          PRIMARY KEY (id),
          UNIQUE KEY (accession),
          KEY (species_id),
          KEY (user_id),
          CONSTRAINT FOREIGN KEY (species_id) REFERENCES species (id) ON DELETE CASCADE ON UPDATE CASCADE,
          CONSTRAINT FOREIGN KEY (user_id) REFERENCES users (id) ON DELETE CASCADE ON UPDATE CASCADE
        ) ENGINE = InnoDB CHARACTER SET utf8 COLLATE utf8_general_ci;

        ALTER TABLE custom_sequences ADD UNIQUE (accession,user_id);

        CREATE TABLE custom_sequences_sequences_relationships (
          custom_sequence_id INT(10) UNSIGNED NOT NULL,
          sequence_id MEDIUMINT(8) UNSIGNED NOT NULL,
          type ENUM('other','synonym','associated') NOT NULL DEFAULT 'synonym',
          UNIQUE KEY (custom_sequence_id, sequence_id),
          KEY (custom_sequence_id),
          KEY (sequence_id),
          CONSTRAINT FOREIGN KEY (custom_sequence_id) REFERENCES custom_sequences (id) ON DELETE CASCADE ON UPDATE CASCADE,
          CONSTRAINT FOREIGN KEY (sequence_id) REFERENCES sequences (id) ON DELETE CASCADE ON UPDATE CASCADE
        ) ENGINE=InnoDB DEFAULT CHARSET=utf8;

        CREATE TABLE custom_families_sequences (
          custom_family_id MEDIUMINT(8) UNSIGNED NOT NULL,
          custom_sequence_id INT(10) UNSIGNED NOT NULL,
          UNIQUE (custom_family_id, custom_sequence_id)
        ) ENGINE = InnoDB CHARACTER SET utf8 COLLATE utf8_general_ci;

        ALTER TABLE custom_families_sequences
          ADD INDEX (custom_family_id),
          ADD INDEX (sequence_id),
          ADD UNIQUE (custom_family_id, sequence_id),
          ADD FOREIGN KEY (custom_family_id) REFERENCES custom_families (id) ON DELETE CASCADE ON UPDATE CASCADE,
          ADD FOREIGN KEY (custom_sequence_id) REFERENCES custom_sequences (id) ON DELETE CASCADE ON UPDATE CASCADE
        ;

        CREATE TABLE IF NOT EXISTS custom_families_sequences_history LIKE custom_families_sequences;
        ALTER TABLE custom_families_sequences_history
          ADD transaction_date TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP
            COMMENT 'date of transaction',
          ADD transaction_type ENUM('n/a', 'insert', 'update', 'delete') CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL
            COMMENT 'type of transaction that changed this value',
          ADD transaction_account VARCHAR(64) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL
            COMMENT 'user account that committed the transaction',
          ADD transaction_user VARCHAR(64) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL
            COMMENT 'authenticated user that committed the transaction'
        ;
        ALTER TABLE custom_families_sequences_history DROP INDEX custom_family_id_2;

        CREATE TABLE IF NOT EXISTS custom_family_relationships (
          subject_custom_family_id MEDIUMINT(8) UNSIGNED NOT NULL,
          object_custom_family_id MEDIUMINT(8) UNSIGNED NOT NULL,
          level_delta TINYINT(3) NOT NULL COMMENT 'Difference between object_custom_family.level and subject_family.level; level_delta=-1 means subject_family is a direct child of object_family; all children of a subject_custom_family will be selected using level_delta>0 and all ancestors using level_delta<0',
          type 	ENUM('other','inheritance','similarity','rearrangment') NOT NULL COMMENT 'Type of relationship',
          PRIMARY KEY (subject_custom_family_id, object_custom_family_id),
          KEY (subject_custom_family_id),
          KEY (object_custom_family_id)
        ) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Store the relationship between custom families.';
        ALTER TABLE custom_family_relationships
            ADD FOREIGN KEY (subject_custom_family_id) REFERENCES custom_families (id) ON DELETE CASCADE ON UPDATE CASCADE,
            ADD FOREIGN KEY (object_custom_family_id) REFERENCES custom_families (id) ON DELETE CASCADE ON UPDATE CASCADE
        ;

        CREATE TABLE IF NOT EXISTS custom_family_relationships_history LIKE custom_family_relationships;
        ALTER TABLE custom_family_relationships_history
          ADD transaction_date TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP
            COMMENT 'date of transaction',
          ADD transaction_type ENUM('n/a', 'insert', 'update', 'delete') CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL
            COMMENT 'type of transaction that changed this value',
          ADD transaction_account VARCHAR(64) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL
            COMMENT 'user account that committed the transaction',
          ADD transaction_user VARCHAR(64) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL
            COMMENT 'authenticated user that committed the transaction'
        ;
        ALTER TABLE custom_family_relationships_history DROP PRIMARY KEY;        

        CREATE TABLE custom_families_families_relationships (
          custom_family_id MEDIUMINT(8) UNSIGNED NOT NULL,
          family_id MEDIUMINT(8) UNSIGNED NOT NULL,
          type ENUM('other','similar to','derivates from') NOT NULL,
          UNIQUE (custom_family_id, family_id)
        ) ENGINE = InnoDB;

        ALTER TABLE custom_families_families_relationships
          ADD FOREIGN KEY (custom_family_id) REFERENCES custom_families(id) ON DELETE CASCADE ON UPDATE CASCADE,
          ADD FOREIGN KEY (family_id) REFERENCES families (id) ON DELETE CASCADE ON UPDATE CASCADE
        ;

        SELECT '...done custom family tables.' AS 'Message';
      END;
    END IF;


    -- My List tables
    IF ((0 = @error_count) AND (NOT tableExists('my_list_items'))) THEN
      BEGIN
        SELECT 'My list tables...' AS 'Message';
        CREATE TABLE my_lists (
          id INT(10) UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY,
          name VARCHAR(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT 'my list',
          description TEXT CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
          user_id SMALLINT(5) UNSIGNED NOT NULL,
          INDEX (user_id)
        ) ENGINE = InnoDB CHARACTER SET utf8 COLLATE utf8_general_ci;

        ALTER TABLE my_lists
          ADD FOREIGN KEY (user_id) REFERENCES users(id) ON DELETE CASCADE ON UPDATE CASCADE
        ;

        CREATE TABLE my_list_items (
          my_list_id INT(10) UNSIGNED NOT NULL,
          foreign_id INT(10) UNSIGNED NOT NULL,
          type ENUM('Sequence','Family','CustomSequence','CustomFamily') NOT NULL,
          UNIQUE (my_list_id,foreign_id,type)
        ) ENGINE = InnoDB;

        ALTER TABLE my_list_items
            ADD FOREIGN KEY (my_list_id) REFERENCES my_lists(id) ON DELETE CASCADE ON UPDATE CASCADE
        ;

        SELECT '...done my list tables.' AS 'Message';
      END;
    END IF;

    -- Processing tables
    IF ((0 = @error_count) AND (NOT tableExists('family_processing'))) THEN
      BEGIN
        SELECT 'Processing tables...' AS 'Message';
            CREATE TABLE processes (
              id SMALLINT(5) UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY,
              name VARCHAR(63) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
              description TEXT CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '',
              status ENUM('pending', 'in progress', 'paused', 'stopped', 'done', 'canceled') NOT NULL COMMENT 'Process status note: a ''paused'' process has been manually halted while a ''stopped'' process has been halted by a software error.',
              start_date TIMESTAMP NULL DEFAULT NULL,
              last_update TIMESTAMP ON UPDATE CURRENT_TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP,
              duration INT(10) UNSIGNED NOT NULL DEFAULT '0' COMMENT 'Duration in seconds'
            ) ENGINE = InnoDB CHARACTER SET utf8 COLLATE utf8_general_ci;

            CREATE TABLE family_processing (
              family_id MEDIUMINT(8) UNSIGNED NOT NULL,
              steps_to_perform SET('other', 'fasta', 'hmm', 'maft', 'trimal', 'phyml', 'rap', 'web transfer', 'statistics', 'orthology', 'meme/mast', 'plant specificity', 'taxonomy specificity') NOT NULL,
              steps_performed SET('other', 'fasta', 'hmm', 'maft', 'trimal', 'phyml', 'rap', 'web transfer', 'statistics', 'orthology', 'meme/mast', 'plant specificity', 'taxonomy specificity') NOT NULL,
              status ENUM('pending', 'in progress', 'done', 'canceled', 'error', 'warning') NOT NULL,
              last_update TIMESTAMP ON UPDATE CURRENT_TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP,
              process_id SMALLINT(5) UNSIGNED NOT NULL
            ) ENGINE = InnoDB CHARACTER SET utf8 COLLATE utf8_general_ci;

            ALTER TABLE family_processing
              ADD INDEX (family_id),
              ADD INDEX (process_id),
              ADD UNIQUE (family_id, process_id)
            ;

            ALTER TABLE family_processing ADD FOREIGN KEY (family_id) REFERENCES families (
              id
            ) ON DELETE CASCADE ON UPDATE CASCADE;

            ALTER TABLE family_processing ADD FOREIGN KEY (process_id) REFERENCES processes (
              id
            ) ON DELETE CASCADE ON UPDATE CASCADE;

        SELECT '...done processing tables.' AS 'Message';
      END;
    END IF;

    
    -- cache tables...
    -- ----------------
    SELECT 'Cache tables...' AS 'Message';
    IF ((0 = @error_count) AND (NOT tableExists('family_relationships_cache'))) THEN
      BEGIN
        SELECT 'Normalize existing cache tables...' AS 'Message';
        -- sequence count by family and by species
        RENAME TABLE count_family_seq TO sequence_count_by_families_species_cache;
        ALTER TABLE sequence_count_by_families_species_cache
          CHANGE species_id species_id SMALLINT(5) UNSIGNED NOT NULL,
          CHANGE number sequence_count MEDIUMINT(8) UNSIGNED NOT NULL
            COMMENT 'Number of sequence by family by species',
          ADD sequence_count_without_splice MEDIUMINT(8) UNSIGNED NOT NULL
            COMMENT 'Number of sequence by family by species without counting splice forms'
        ;

        -- families and IPR
        RENAME TABLE ipr_stat TO families_ipr_cache;
        ALTER TABLE families_ipr_cache CHANGE occurence sequence_count MEDIUMINT(8) UNSIGNED NULL DEFAULT NULL,
          CHANGE perc percent TINYINT(3) UNSIGNED NULL DEFAULT NULL
            COMMENT 'Percentage of sequence having the IPR for the family',
          CHANGE spec family_specific BOOL NULL DEFAULT NULL
            COMMENT 'IPR specific to the family'
        ;

        -- families and IPR and species
        RENAME TABLE family_has_an TO families_ipr_species_cache;

        SELECT '...done normalizing existing cache tables.' AS 'Message';

        DROP TABLE go_family_cache;


        SELECT 'Create new cache tables...' AS 'Message';


        -- go and sequences
        CREATE TABLE IF NOT EXISTS go_sequences_cache (
          go_id MEDIUMINT(8) UNSIGNED NOT NULL,
          sequence_id MEDIUMINT(8) UNSIGNED NOT NULL,
          ipr_id MEDIUMINT(8) UNSIGNED NULL DEFAULT NULL,
          uniprot_id INT(10) UNSIGNED NULL DEFAULT NULL,
          ec_id INT(10) UNSIGNED NULL DEFAULT NULL,
          PRIMARY KEY (go_id, sequence_id),
          KEY go_id (go_id),
          KEY sequence_id (sequence_id)
        ) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Cache associating GOs and families with a given sequence coverage. This table is computed using go_and_sequence_cache table';
        ALTER TABLE go_sequences_cache
          ADD INDEX (ipr_id),
          ADD INDEX (uniprot_id),
          ADD INDEX (ec_id)
        ;
        ALTER TABLE go_sequences_cache
          ADD FOREIGN KEY (go_id) REFERENCES go (id)
            ON DELETE CASCADE ON UPDATE CASCADE,
          ADD FOREIGN KEY (sequence_id) REFERENCES sequences (id)
            ON DELETE CASCADE ON UPDATE CASCADE,
          ADD FOREIGN KEY (ipr_id) REFERENCES ipr(id)
            ON DELETE SET NULL ON UPDATE CASCADE,
          ADD FOREIGN KEY (uniprot_id) REFERENCES dbxref (id)
            ON DELETE SET NULL ON UPDATE CASCADE,
          ADD FOREIGN KEY (ec_id) REFERENCES dbxref (id)
            ON DELETE SET NULL ON UPDATE CASCADE
        ;


        -- go and families
        CREATE TABLE IF NOT EXISTS families_go_cache (
          go_id MEDIUMINT(8) UNSIGNED NOT NULL,
          family_id MEDIUMINT(8) UNSIGNED NOT NULL,
          percent TINYINT(3) UNSIGNED NULL DEFAULT NULL COMMENT 'Percentage of sequences of the family having the GO',
          PRIMARY KEY (go_id, family_id),
          KEY go_id (go_id),
          KEY family_id (family_id)
        ) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Cache associating GOs and families with a given sequence coverage. This table is computed using go_and_sequences_cache table';
        ALTER TABLE families_go_cache
          ADD FOREIGN KEY (go_id) REFERENCES go (id)
            ON DELETE CASCADE ON UPDATE CASCADE,
          ADD FOREIGN KEY (family_id) REFERENCES families (id)
            ON DELETE CASCADE ON UPDATE CASCADE
        ;


        -- sequence_no_splice_by_family_species_cache
        CREATE TABLE IF NOT EXISTS sequence_no_splice_by_family_species_cache LIKE sequence_count_by_family_species_cache;


        -- family_relationships_cache
        CREATE TABLE IF NOT EXISTS family_relationships_cache (
          subject_family_id MEDIUMINT(8) UNSIGNED NOT NULL,
          object_family_id MEDIUMINT(8) UNSIGNED NOT NULL,
          level_delta TINYINT(3) NOT NULL COMMENT 'Difference between object_family.level and subject_family.level; level_delta=-1 means subject_family is a direct child of object_family; all children of a subject_family will be selected using level_delta>0 and all ancestors using level_delta<0',
          type 	ENUM('inheritance','similarity','rearrangment') NOT NULL COMMENT 'Type of relationship',
          PRIMARY KEY (subject_family_id, object_family_id),
          KEY (subject_family_id),
          KEY (object_family_id)
        ) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Cache storing the relationship between families.';
        ALTER TABLE family_relationships_cache
            ADD FOREIGN KEY (subject_family_id) REFERENCES families (id) ON DELETE CASCADE ON UPDATE CASCADE,
            ADD FOREIGN KEY (object_family_id) REFERENCES families (id) ON DELETE CASCADE ON UPDATE CASCADE
        ;


        SELECT '...done creating new cache tables.' AS 'Message';
      END;
    END IF;

    IF (0 = @error_count) THEN
      -- upgrade successfull
      -- INSERT INTO variables VALUES ('schema_version', '3.0') ON DUPLICATE KEY UPDATE value = '3.0';
      SELECT 'Successfully updated to v3.0!' AS 'Message';
    ELSE
      SELECT 'Warning: Updated to v3.0 was not complete! Please check log messages.' AS 'Message';
    END IF;

  ELSE
    -- an error occured during ID updates
    SELECT 'An error occured during table ID normalization and no other updates were run' AS 'Message';
  END IF;

END;
$$


-- Update existing procedures or create new ones
DROP PROCEDURE IF EXISTS addGreenPhylUser$$
CREATE PROCEDURE addGreenPhylUser(
    IN gp_user_name VARCHAR(255),
    IN gp_user_email VARCHAR(255),
    IN gp_user_password VARCHAR(255),
    IN gp_user_flags VARCHAR(255),
    IN gp_description VARCHAR(255)
  )
  BEGIN
    DECLARE l_user_salt VARCHAR(255);
    DECLARE l_user_password_hash VARCHAR(255);

    -- generate random salt on 40 characters (using MD5 to get a hexadecimal string value)
    SET l_user_salt := MD5(RAND());
    -- generate password hash
    SET l_user_password_hash := hashGreenPhylPassword(gp_user_password, l_user_salt);
    -- store new user
    INSERT INTO users (name, email, password_hash, salt, flags, description) VALUES (
      gp_user_name,
      gp_user_email,
      l_user_password_hash,
      l_user_salt,
      gp_user_flags,
      gp_description
    );
  END
$$

DROP FUNCTION IF EXISTS hashGreenPhylPassword $$
CREATE FUNCTION hashGreenPhylPassword(gp_password VARCHAR(255), gp_salt CHAR(40)) RETURNS CHAR(40)
  BEGIN
    -- use AES to encrypt password using given salt (enforce security against hash-dictionnary brute-force attacks)
    -- and generate a SHA1 hash
    RETURN SHA1(AES_ENCRYPT(gp_password, gp_salt));
  END
$$

DROP FUNCTION IF EXISTS authenticateGreenPhylUser $$
CREATE FUNCTION authenticateGreenPhylUser(gp_user_name VARCHAR(255), gp_user_password VARCHAR(255)) RETURNS BOOL
  BEGIN
    DECLARE l_authenticated BOOL DEFAULT FALSE;

    IF (EXISTS (SELECT TRUE FROM users u WHERE u.name = gp_user_name AND NOT u.flags NOT LIKE '%disabled%' AND u.password_hash = hashGreenPhylPassword(gp_user_password, u.salt) LIMIT 1)) THEN
      SET @authenticated_user := gp_user_name; -- save current login in current connection variable space
      SET l_authenticated := TRUE;
    END IF;

    RETURN l_authenticated;
  END
$$


DELIMITER ;
-- END OF PROCEDURES
-- -----------------------------------------------------------------------------

-- -----------------------------------------------------------------------------
-- MAIN PART
-- ---------
-- initial error status
SET @error_count := 0;

-- Process update
CALL updateSchemaTemp();

SELECT 'Removing temporary procedures...' AS 'Message';
-- DROP FUNCTION constraintExists;
-- DROP FUNCTION columnExists;
-- DROP FUNCTION tableExists;
-- DROP PROCEDURE IF EXISTS dropForeignKeyConstraints;
-- DROP PROCEDURE IF EXISTS changeTableKey;
-- DROP PROCEDURE IF EXISTS updateFrom20To30;
-- DROP PROCEDURE IF EXISTS updateFromNullTo20;
-- DROP PROCEDURE IF EXISTS updateSchemaTemp;
-- DROP PROCEDURE IF EXISTS upgradeGreenPhylUser;

-- Check for errors
SELECT CONCAT('Error found: ', @error_count) AS 'Message';

SELECT '... All done.' AS 'Message';

-- -----------------------------------------------------------------------------
