#!/usr/bin/perl

=pod

=head1 NAME

runMeme.pl

=head1 SYNOPSIS

    filtration.pl

=head1 REQUIRES

Perl5

=head1 DESCRIPTION

.

=cut

use strict;
use warnings;

use lib "../lib";
use lib "../../lib";

use Readonly;
use Carp;
use Getopt::Long;
use Pod::Usage;

use Statistics::Descriptive;
use File::Temp qw/ tempfile tempdir /;
use Bio::SeqIO;

use Greenphyl::Config;
use Greenphyl::ConnectMysql;
use Greenphyl::Utils;



# SVN management part
######################
my $REV_STRING = '$id$';



# Script global constants
##########################

=pod

=head1 CONSTANTS

B<$DEBUG>: (boolean)

If debug is set to a true value, debug messages will be displayed and
modification to database won't be committed.

B<$FILTERING_VERSION>: (string)

Version of the Filtering script.

=cut

Readonly our $DEBUG                               => 0;
Readonly our $FILTERING_VERSION                   => '2.0.' . $REV_STRING;
Readonly our $LOG_FILENAME                        => 'filtration.log';
Readonly our $FASTA_EXT                           => '.fa';
Readonly our $OUTPUT_STAT_EXT                     => '.csv';
Readonly our $OUTPUT_STAT_BASENAME                => 'stat_';
Readonly our $DEFAULT_OUTPUT_GLOBAL_STAT_FILENAME => 'stats' . $OUTPUT_STAT_EXT;
Readonly our $MAST_OUTPUT_FILENAME                => 'mast.html';
Readonly our @ANCESTRAL_SPECIES                   => ('PHYPA', 'SELMO', 'OSTTA', 'CHLRE', 'CYAME');
Readonly our $ANCESTRAL_SPECIES_REGEXP            => join('|', @ANCESTRAL_SPECIES);
Readonly our $PRIORITY_SPLICE_REGEXP              => '\.P?0*1$';

Readonly our $KEPT_SEQ_FILTER_VALUE               => 0;
Readonly our $DROPPED_SEQ_FILTER_VALUE            => 1;
Readonly our $DROPPED_DOUBTFUL_SEQ_FILTER_VALUE   => 2;

Readonly our $CSV_COLUMN_SEPARATOR                => ';';


# Script global variables
##########################

=pod

=head1 VARIABLES

B<$g_dbh>: (DBI)

Database connection object.

=cut

my $g_dbh;
my $g_log_fh;
my $g_stats_fh;
my $g_debug       = $DEBUG;



# Script global functions
##########################

=pod

=head1 FUNCTIONS

=head2 XXX

B<Description>: XXX.

B<ArgsCount>: 1

=over 4

=item arg: $x (XXX) (O)

XXX

=back

B<Return>: (list)

B<Example>:


=cut

sub log10
{
    my $n = shift;
    if (!$n)
    {
        confess "ERROR: invalid value for log10 ('" . (defined($n)?$n:'undef') . "')!\n";
    }
    return log($n)/log(10);
}
    
    
sub getMemeStats
{
    # get arguments
    my ($family, $mast_filepath, $data_source, $output_directory) = @_;

    # open family log
    my $family_log_fh;
    open($family_log_fh, ">$output_directory/$OUTPUT_STAT_BASENAME$family$OUTPUT_STAT_EXT") or confess "ERROR: Failed to open family log file '$OUTPUT_STAT_BASENAME$family$OUTPUT_STAT_EXT'\n$!\n";
    print {$family_log_fh} "SEQUENCE" . $CSV_COLUMN_SEPARATOR . "SPECIES" . $CSV_COLUMN_SEPARATOR . "LENGTH" . $CSV_COLUMN_SEPARATOR . "EVALUE" . $CSV_COLUMN_SEPARATOR . "LOG_EVALUE" . $CSV_COLUMN_SEPARATOR . "STATUS\n";

    # prepare output
    my ($family_size, $kept_sequences_count, $dropped_sequences_count, $dropped_splice_seq_count, $dropped_doubtful_seq_count, $mean_evalue, $median_evalue, $standev_evalue, $evalue_min, $evalue_max, $mean_length, $median_length, $standev_length, $length_min, $length_max) =
       (           0,                     0,                        0,                         0,                           0,            0,              0,               0,          '',          '',            0,              0,               0,          '',          '');

    my $meme_sequences_count = 0;
    my $source_seq_count     = 0;
    my (@meme_sequences, @length, @evalues);
    my (%dropped_sequences, %kept_sequences);


    # log
    print {$g_log_fh} "FAMILY: '$family'\n MAST: '$mast_filepath'\n";

    # check input MAST file
    if (!-e $mast_filepath)
    {
        print {$g_log_fh} "WARNING: '$mast_filepath' file not found!\n";
        print "WARNING: '$mast_filepath' file not found!\n";
        close($family_log_fh);
        return ($family_size, $kept_sequences_count, $dropped_sequences_count, $dropped_splice_seq_count, $dropped_doubtful_seq_count, $mean_evalue, $median_evalue, $standev_evalue, $evalue_min, $evalue_max, $mean_length, $median_length, $standev_length, $length_min, $length_max);
    }

    # parse MAST html file
    if ($g_debug)
    {
        print "DEBUG: Processing MAST output...\n";
    }
    my $mast_fh;
    open($mast_fh, $mast_filepath) or confess "ERROR: unable to open MAST input file '$mast_filepath'!\n$!\n";
    foreach my $line (<$mast_fh>)
    {
        chomp($line);

        # check sequence count
        if ($line =~ m/Database contains (\d+) sequence/)
        {
            $meme_sequences_count = $1;
            print {$g_log_fh} " Family members in MEME output: $meme_sequences_count\n";
            if (ref($data_source) eq 'DBI::db')
            {
                # get family size from database
                my $sql_query     = "SELECT COUNT(1) FROM seq_is_in WHERE family_id = $family;";
                ($source_seq_count) = ($data_source->selectrow_array($sql_query));
                print {$g_log_fh} " Family members from database: $source_seq_count\n";
            }
            else
            {
                # get family size from FASTA file
                $source_seq_count = count_sequences_in_fasta($data_source);
                print {$g_log_fh} " Family members from FASTA file ('$data_source'): $source_seq_count\n";
            }

			#check is meme file contains the same number of sequences
            if ($source_seq_count != $meme_sequences_count)
            {
                warn "WARNING: MEME output does not correspond to source data!\n";
                print {$g_log_fh} "WARNING: MEME output does not correspond to source data!\n";
                close($family_log_fh);
                return ($family_size, $kept_sequences_count, $dropped_sequences_count, $dropped_splice_seq_count, $dropped_doubtful_seq_count, $mean_evalue, $median_evalue, $standev_evalue, $evalue_min, $evalue_max, $mean_length, $median_length, $standev_length, $length_min, $length_max);
            }
        }

        # parse sequence name
        if (($line =~ m/^<A NAME\=a.*\><\/A>(\S+).*/i)
            || ($line =~ m/^(\d+\.m\d+\_RICCO)/i)) # specific to 'Ricinus communis'
		{
            # create a new sequence data
            $meme_sequences[scalar(@meme_sequences)] = {};
            my $meme_sequence_name = $1;
            if ($g_debug)
            {
                print "DEBUG: start meme_sequence_name: $meme_sequence_name\n";
            }

            # allow to print species of dropped sequence
            if ($meme_sequence_name =~ m/_(\D{5})\Z/)
            {
                $meme_sequences[$#meme_sequences]->{'species'} = $1;
                 # remove species code
                $meme_sequence_name =~ s/_\D{5}\Z//;
                if ($g_debug)
                {
                    print "DEBUG: new meme_sequence_name: $meme_sequence_name\n";
                    print "DEBUG: species: " . $meme_sequences[$#meme_sequences]->{'species'} . "\n";
                }
            }

            $meme_sequences[$#meme_sequences]->{'id'} = $meme_sequence_name;

            # detect splice form with ".splice number"
            if ( $meme_sequence_name =~ /(\S+)\.(\d{1})\Z/ )
            {
                $meme_sequences[$#meme_sequences]->{'id'}     = $1;
                $meme_sequences[$#meme_sequences]->{'splice'} = $2;
                if ($g_debug)
                {
                    print "DEBUG: id: $1\n";
                    print "DEBUG: splice: $2\n";
                }
            }
            # detect splice form for MAIZE sequence
            elsif ( $meme_sequence_name =~ /(GRMZM\S+\_P0)(\d{1})\Z/ )
            {
                $meme_sequences[$#meme_sequences]->{'id'}     = $1;
                $meme_sequences[$#meme_sequences]->{'splice'} = $2;
                if ($g_debug)
                {
                    print "DEBUG: id: $1\n";
                    print "DEBUG: splice: $2\n";
                }
            }
            $meme_sequences[$#meme_sequences]->{'fullname'} = $meme_sequence_name;
        }

        # parse sequence length
        if ($line =~ m/^\s+LENGTH\s\=\s(\S+).*/)
        {
            my $length = $1;
            $meme_sequences[$#meme_sequences]->{'length'} = $length;
            push @length, $length;
            if ($g_debug)
            {
                print "DEBUG: length: $length\n";
            }
        }

        # parse evalue score
        if ($line =~ m/E-VALUE\s*=\s*([-+]?\d*(?:\.\d*)?(?:[Ee](?:[-+]?\d+))?)/i)
        {
            my $evalue = $1;
            if ($g_debug)
            {
                print "DEBUG: evalue: $evalue\n";
            }
            if ('' ne $evalue)
            {
                $meme_sequences[$#meme_sequences]->{'evalue'} = $evalue;
                push @evalues, $evalue;
            }
            else
            {
                warn "WARNING: Failed to parse line:\n$line\n\n";
                print {$g_log_fh} " WARNING: Failed to parse line:\n$line\n\n";
            }
        }
    } # end while in meme/mast file
    if (!@evalues || !@length)
    {
        warn "ERROR: Failed to parse '$mast_filepath' file!\n";
        print {$g_log_fh} " ERROR: Failed to parse '$mast_filepath' file!\n";
        return ($family_size, $kept_sequences_count, $dropped_sequences_count, $dropped_splice_seq_count, $dropped_doubtful_seq_count, $mean_evalue, $median_evalue, $standev_evalue, $evalue_min, $evalue_max, $mean_length, $median_length, $standev_length, $length_min, $length_max);
    }

    if ($g_debug)
    {
        print "DEBUG: MAST output processed!\n";
    }

    # make stats on log, exponential value and sequence length
    ($mean_evalue, $median_evalue, $standev_evalue) = computeStats(@evalues);
    ($mean_length, $median_length, $standev_length) = computeStats(@length);

    # Fix cutoffs
    my ($evalue_cut, $min_length_cut, $max_length_cut) = fixCutOffs($median_evalue, $standev_evalue, $median_length, $standev_length);

    if ($g_debug)
    {
        print "DEBUG: median_evalue=$median_evalue\n       standev_evalue=$standev_evalue\n       median_length=$median_length\n       standev_length=$standev_length\n       evalue_cut=$evalue_cut\n       min_length_cut=$min_length_cut\n       max_length_cut=$max_length_cut\n";
    }


    my %kept_splice_form = ();

    # sort by e-values
    sub sort_evalue
    {
        if (!defined($a->{'evalue'}))
        {
            if (!defined($b->{'evalue'}))
            {
                return 0;
            }
            return 1;
        }
        if (!defined($b->{'evalue'}))
        {
            return -1;
        }
        return $a->{'evalue'} <=> $b->{'evalue'};
    }
    @meme_sequences = sort sort_evalue @meme_sequences;
    # loop on parsing meme_sequences
    foreach my $meme_seq_data (@meme_sequences)
    {
        my $sequence_base_name = $meme_seq_data->{'id'}; # sequence ID without splice form number (e.g. xxx.1)
        my $seq_id             = $meme_seq_data->{'fullname'};
        my $species            = $meme_seq_data->{'species'};
        my $length             = $meme_seq_data->{'length'};
        my $evalue             = $meme_seq_data->{'evalue'};
        my $seq_status         = '';

        if (defined($evalue) && ($evalue ne ''))
        {
            if ($evalue_min eq '')
            {
                $evalue_min = $evalue_max = $evalue;
            }
            if ($evalue < $evalue_min)
            {
                $evalue_min = $evalue;
            }
            if ($evalue > $evalue_max)
            {
                $evalue_max = $evalue;
            }
        }
        if (defined($length) && ($length ne ''))
        {
            if ($length_min eq '')
            {
                $length_min = $length_max = $length;
            }
            if ($length < $length_min)
            {
                $length_min = $length;
            }
            if ($length > $length_max)
            {
                $length_max = $length;
            }
        }

        # Add more permissive cutoff on e-value for ancestral species
        if (($evalue) && ($species =~ m/$ANCESTRAL_SPECIES_REGEXP/o))
        {
            $evalue = 10**(log10($evalue)*1.2); # add 20% to the power of 10
            if ($g_debug)
            {
                print "DEBUG: Ancestral species: more permissive cutoff applies.\n";
            }
        }

        # detect classification errors for potential removal from this family
        if ((!defined $evalue)
            || ('' eq $evalue)
            || (($evalue > $evalue_cut) && ($length <= $min_length_cut))) # if evalue is above cutoff and size less than cutoff
        {
			$dropped_sequences{$seq_id} = $meme_seq_data;
            if (ref($data_source) eq 'DBI::db')
            {
                setFilterNumber($seq_id, $DROPPED_DOUBTFUL_SEQ_FILTER_VALUE);
            }
			++$dropped_doubtful_seq_count;
            $seq_status = 'dropped and doubtful';
        }
        # filter on cutoffs (expo + length)
        elsif (($evalue >= $evalue_cut) # e-value >= e-value cutoff
               || ($length >= $max_length_cut) # length > max cutoff
               || ($length <= $min_length_cut) # length < min cutoff
              )
        {
            $dropped_sequences{$seq_id} = $meme_seq_data;
            if (ref($data_source) eq 'DBI::db')
            {
                setFilterNumber($seq_id, $DROPPED_SEQ_FILTER_VALUE);
            }
            $seq_status = 'dropped';
        }
        # only keep '.1' splice or exclude iteratively splices with a lower e-value than the current one
        elsif ($meme_seq_data->{'splice'})
        {
            my $splice_number      = $meme_seq_data->{'splice'};
            my $temp_name          = $sequence_base_name;

            # avoid case sensitive problem with arabidospis sequence
            if ($temp_name =~ /^At/i)
            {
                $temp_name = uc($temp_name);
            }

            # keep '.1' splice or the best e-value for a splice
            # check if a previous splice has already been processed
            if (exists($kept_splice_form{$temp_name}))
            {
                # if previous splice was not a '.1' splice
                # and the new splice has a better e-value
                # keep new splice and drop previous one
                if (($kept_splice_form{$temp_name}->{'splice'} !~ m/$PRIORITY_SPLICE_REGEXP/oi)
                    && ($kept_splice_form{$temp_name}->{'evalue'} > $evalue))
                {
                    # add previous splice form to dropped sequences hash
                    $dropped_sequences{$kept_splice_form{$temp_name}->{'fullname'}} = $kept_sequences{$kept_splice_form{$temp_name}->{'fullname'}};
                    if (ref($data_source) eq 'DBI::db')
                    {
                        setFilterNumber($dropped_sequences{$kept_splice_form{$temp_name}->{'fullname'}}, $DROPPED_SEQ_FILTER_VALUE);
                    }
                    # drop previous splice from kept sequences hash
                    delete($kept_sequences{$kept_splice_form{$temp_name}->{'fullname'}});
                    # keep new one in kept sequences hash
                    $kept_sequences{$seq_id} = $meme_seq_data;
                    # replace previous one in kept splice hash
                    $kept_splice_form{$temp_name} = $meme_seq_data;
                    $seq_status = 'replace kept splice';
                }
                else
                {
                    # drop
                    $dropped_sequences{$seq_id} = $meme_seq_data; # drop previous splice found
                    if (ref($data_source) eq 'DBI::db')
                    {
                        setFilterNumber($seq_id, $DROPPED_SEQ_FILTER_VALUE);
                    }
                    ++$dropped_splice_seq_count;
                    $seq_status = 'dropped splice';
                }
            }
            else
            {
                # no previous splice processed, keep current
                $kept_splice_form{$temp_name} = $meme_seq_data;
                $kept_sequences{$seq_id} = $meme_seq_data; 
                $seq_status = 'kept new splice';
            }
        }
        else
        {
            $kept_sequences{$seq_id} = $meme_seq_data; 
            $seq_status = 'kept';
        }
        print {$family_log_fh} "\"$seq_id\"$CSV_COLUMN_SEPARATOR$species$CSV_COLUMN_SEPARATOR$length$CSV_COLUMN_SEPARATOR\"$evalue\"$CSV_COLUMN_SEPARATOR\"" . ($evalue?log10($evalue):'-INF') . "\"$CSV_COLUMN_SEPARATOR\"$seq_status\"\n";
    }

    $family_size = $meme_sequences_count;
    $dropped_sequences_count = scalar(keys(%dropped_sequences));
    $kept_sequences_count    = scalar(keys(%kept_sequences));
    
    #+DEBUG my $sequence_fasta_number = createFilteredFasta($family, @dropped_sequences);

    print {$g_log_fh} " Done.\n";

    # add stats
    print {$family_log_fh} "\n";
    print {$family_log_fh} <<"___496_FAMILY_END_LOG___";
SEQUENCES_COUNT:$CSV_COLUMN_SEPARATOR$family_size
KEPT_SEQUENCES:$CSV_COLUMN_SEPARATOR$kept_sequences_count
DROPPED_SEQUENCES:$CSV_COLUMN_SEPARATOR$dropped_sequences_count
DROPPED_SPLICE:$CSV_COLUMN_SEPARATOR$dropped_splice_seq_count
DROPPED_DOUBTFUL:$CSV_COLUMN_SEPARATOR$dropped_doubtful_seq_count
MEAN_EVALUE:$CSV_COLUMN_SEPARATOR$mean_evalue
MEDIAN_EVALUE:$CSV_COLUMN_SEPARATOR$median_evalue
STANDARD_DEVIATION_EVALUE:$CSV_COLUMN_SEPARATOR$standev_evalue
MEAN_LENGTH:$CSV_COLUMN_SEPARATOR$mean_length
MEDIAN_LENGTH:$CSV_COLUMN_SEPARATOR$median_length
STANDARD_DEVIATION_LENGTH:$CSV_COLUMN_SEPARATOR$standev_length
___496_FAMILY_END_LOG___

    close($family_log_fh);

    print {$g_log_fh} " Family initial sequences: $family_size\n Kept sequences: $kept_sequences_count\n Dropped sequences: $dropped_sequences_count\n Dropped splice sequences: $dropped_splice_seq_count\n Dropped doubtful sequences: $dropped_doubtful_seq_count\n Mean e-value: $mean_evalue\n Median e-value: $median_evalue\n Standard deviation e-value: $standev_evalue\n Min e-value: $evalue_min\n Max e-value: $evalue_max\n Mean length: $mean_length\n Median length: $median_length\n Standard deviation length: $standev_length\n Min length: $length_min\n Max length: $length_max\n\n";

    return ($family_size, $kept_sequences_count, $dropped_sequences_count, $dropped_splice_seq_count, $dropped_doubtful_seq_count, $mean_evalue, $median_evalue, $standev_evalue, $evalue_min, $evalue_max, $mean_length, $median_length, $standev_length, $length_min, $length_max);
}


=pod

=head2 XXX

B<Description>: XXX.

B<ArgsCount>: 1

=over 4

=item arg: $x (XXX) (O)

XXX

=back

B<Return>: nothing

B<Example>:


=cut

sub computeStats
{
    my @data = @_;

    my $stat = Statistics::Descriptive::Full->new();
    $stat->add_data(@data);
    my $mean               = $stat->mean();
    my $median             = $stat->median();
    my $standard_deviation = $stat->standard_deviation();

    if (!defined($mean) || !defined($median) || !defined($standard_deviation))
    {
        confess "ERROR: computeStats() computation failed!\nUsed values:\n----------\n'" . join("'\n'", @data) . "'\n----------\n";
    }

    return  ($mean, $median, $standard_deviation);
}


=pod

=head2 XXX

B<Description>: XXX.

B<ArgsCount>: 1

=over 4

=item arg: $x (XXX) (O)

XXX

=back

B<Return>: nothing

B<Example>:


=cut

sub fixCutOffs
{
    my ($median_evalue, $standev_evalue, $median_length, $standev_length) = @_;

    my $output = '';

    # value for size of standard deviation
    my $value = 1;

    # will define the group consistency and will weight the standard deviation
    # if high consistency ( meme evalue <= 1e-80), standard deviation will be multiplied by 1 etc.
    # parameters are arbitrary

    if ($median_evalue >= 1e-80)
    {
        $output = "Group with high consistency\n";
        #$value = 1;
    }
    elsif (($median_evalue < 1e-80) && ($median_evalue >= 1e-50))
    {
        $output = "Group with acceptable consistency\n";
        #$value = 1.5;
    }
    elsif (($median_evalue < 1e-50) &&($median_evalue >= 1e-20))
    {
        $output = "Group with poor consistency\n" ;
        #$value = 2;
    }
    elsif ($median_evalue < 1e-20)
    {
        $output = "Group with no consistency\n";
        #$value = 3;
    }

    # over this score, sequences will be dropped
    my $evalue_cut = $median_evalue + ( $standev_evalue * $value );

    if ($g_debug)
    {
        print "DEBUG: evalue_cut=$evalue_cut\n";
    }
    #print "EXPO: Median :$median_expo VALEUR ecart type :$value ecart_type:$standev_expo CUT: $expo_cut\n";
    #print "LOG : Median :$median_evalue  VALEUR ecart type :$value ecart_type:$standev_log CUT: $log_cut\n";

    # fix filter on  Length
    my $min_length_cut = $median_length - $standev_length;
    my $max_length_cut = $median_length + ( $standev_length * 3 );

    return ($evalue_cut, $min_length_cut, $max_length_cut);
}




=pod

=head2 XXX

B<Description>: XXX.

B<ArgsCount>: 1

=over 4

=item arg: $x (XXX) (O)

XXX

=back

B<Return>: nothing

B<Example>:


=cut

sub setFilterNumber
{
    my ($seq_textid, $number) = @_;
    my $sql_query = "SELECT seq_id FROM sequences WHERE seq_textid = '$seq_textid';";
	my ($id)    = ($g_dbh->selectrow_array($sql_query));
	my $update  = "UPDATE sequences SET filtered = $number WHERE seq_id = $id";

	$g_dbh->do($update) or die "update failed";

    return;
}




=pod

=head2 XXX

B<Description>: XXX.

B<ArgsCount>: 1

=over 4

=item arg: $x (XXX) (O)

XXX

=back

B<Return>: nothing

B<Example>:


=cut

sub createFilteredFasta
{
    my ($fam, @dropped_sequences) = @_;

    my %dropped_sequences = map { $_ => 1 } @dropped_sequences;

    my $output;

    #generate fasta: get all seq from family and test if not in dropped list @dropped_sequences
    my ( $tmp_fh, $tmp_filename ) = tempfile( 'XXXX', DIR => $Greenphyl::Config::TEMP_OUTPUT_PATH, SUFFIX => '.fa' );
    my $seq_out = Bio::SeqIO->newFh( -format => 'fasta', -file => ">$tmp_filename" );

	my $sql_query = " SELECT S.sequence, SP.species_name, S.seq_textid
                    FROM sequences S, species SP , seq_is_in SI
                    WHERE S.species_id = SP.species_id
                    AND S.seq_id = SI.seq_id
                    AND SI.family_id = '$fam' ";

		my @rep = @{$g_dbh->selectall_arrayref($sql_query)};
		next if !scalar(@rep);

        my $sequence_fasta_number;
        for (@rep)
        {
            my $seq         = new Bio::Seq( -display_id => join( "_", $_->[2], $_->[1] ), -seq => $_->[0] );
            my $seqtext_id  = $_->[2];
            my $cpt         = 0; # to detect dropped sequences

            next if $dropped_sequences{$seqtext_id};

            print $seq_out $seq;
            $sequence_fasta_number++;
        }
		close $tmp_filename;
		system("chmod 664 $tmp_filename") and confess "Cannot change permissions: $!";
		system("cp $tmp_filename $Greenphyl::Config::DATA_PATH/pipe/IN/$fam.fa") and die "Cannot transfer: $!";
		$output = "$Greenphyl::Config::DATA_PATH/pipe/IN/$fam.fa created\n";

		# $output .= classifyBySequenceNumber($tmp_filename, $fam, $sequence_fasta_number);

		print {$g_log_fh} $output;
		return;
}


=pod

=head2 XXX

B<Description>: XXX.

B<ArgsCount>: 1

=over 4

=item arg: $x (XXX) (O)

XXX

=back

B<Return>: nothing

B<Example>:


=cut

sub classifyBySequenceNumber
{

        my ($tmp_filename, $fam, $sequence_fasta_number) = @_;

        my $output;

        if ( $sequence_fasta_number < 50)
		{
		    $output .= copyFile($tmp_filename, $fam, 50, 49);
	    }
	    elsif($sequence_fasta_number < 100)
	    {
	        $output .= copyFile($tmp_filename, $fam, 100, 50);
	    }
	    elsif($sequence_fasta_number < 150)
	    {
	        $output .= copyFile($tmp_filename, $fam, 150, 50);
	    }
	    elsif($sequence_fasta_number < 300)
	    {
	        $output .= copyFile($tmp_filename, $fam, 300, 150);
	    }
	    elsif($sequence_fasta_number < 500)
	    {
	        $output .= copyFile($tmp_filename, $fam, 500, 200);
	    }
	    elsif($sequence_fasta_number < 1000)
	    {
	        $output .= copyFile($tmp_filename, $fam, 1000, 5000);
	    }
	    else
	    {
	        $output .= copyFile($tmp_filename, $fam, 1000);
	    }
	    return $output;
}



=pod

=head2 XXX

B<Description>: XXX.

B<ArgsCount>: 1

=over 4

=item arg: $x (XXX) (O)

XXX

=back

B<Return>: nothing

B<Example>:


=cut

# If range parameter is not provided, it puts a + to the name of the folder
sub copyFile
{

    my ($tmp_filename, $fam, $sequence_fasta_number, $range) = @_;
    my $output;

    my $max = $sequence_fasta_number - 1;
    my $min = $sequence_fasta_number - $range if ($range);
    $min = '+' if (!$range);

    my $folder = "seq$min-$max";
    my $directory = $Greenphyl::Config::DATA_PATH . '/pipe/' . $folder;

    system("mkdir $folder") if (! -d $directory );

    system("cp $tmp_filename $directory/$fam.fa") and warn "Cannot transfer: $!";
	$output = "$directory/$fam.fa created";

    return $output;
}




# Script options
#################

=pod

=head1 OPTIONS

    filtration.pl [-help] [-man] [-debug] -input <FAMILY | DIRECTORY>

=head2 Parameters

=over 4

=item B<-help>:

display this help message.

=item B<-man>:

display user manual.

=item B<-debug>:

set debug mode on.

=item B<FAMILY>:

a GreenPhyl family name.

=back

=cut

# CODE START
#############

print "\nGreenPhyl Filtering v$FILTERING_VERSION\n\n";

my ($help, $man, $generate_fasta, $output_directory, $output_filename, $input,
    $no_database);

GetOptions(
    'help|?'     => \$help,
    'man'        => \$man,
    'debug'      => \$g_debug,
    'nodatabase' => \$no_database,
    'input|i=s'  => \$input,
    'stats|s=s'  => \$output_filename,
    'fasta'      => \$generate_fasta,
    'output|o=s' => \$output_directory,
) or pod2usage(2);
if ($help) { pod2usage(1); }
if ($man) { pod2usage( -verbose => 2 ); }

$g_debug ||= $DEBUG;

# parameters check
if (!$input)
{
    print STDERR "ERROR: No input file or directory specified!\n";
    pod2usage(2);
}
# remove trailing slash
$input =~ s/\/+$//;
$output_directory =~ s/\/+$//;

# open log files
open($g_log_fh, ">$LOG_FILENAME") or confess "ERROR: Failed to open log file '$LOG_FILENAME'\n$!\n";
print "Log file: '$LOG_FILENAME'\n";
print {$g_log_fh} "GreenPhyl Filtering v$FILTERING_VERSION\nDate: " . getCurrentDate() . "\n\n";

# open output file
$output_filename ||= $DEFAULT_OUTPUT_GLOBAL_STAT_FILENAME;
open($g_stats_fh, ">$output_filename") or confess "ERROR: Failed to open global statistics output file '$output_filename'\n$!\n";
print "Global statistics: '$output_filename'\n";
print {$g_log_fh} "Global Output Statistics: '$output_filename'\n";
print {$g_stats_fh} "FAMILY" . $CSV_COLUMN_SEPARATOR . "SIZE" . $CSV_COLUMN_SEPARATOR . "KEPT" . $CSV_COLUMN_SEPARATOR . "DROPPED" . $CSV_COLUMN_SEPARATOR . "DROPPED_SPLICE" . $CSV_COLUMN_SEPARATOR . "DROPPED_DOUBTFUL" . $CSV_COLUMN_SEPARATOR . "MEAN_EVALUE" . $CSV_COLUMN_SEPARATOR . "MEDIAN_EVALUE" . $CSV_COLUMN_SEPARATOR . "STANDARD_DEVIATION_EVALUE" . $CSV_COLUMN_SEPARATOR . "EVALUE_MIN" . $CSV_COLUMN_SEPARATOR . "EVALUE_MAX" . $CSV_COLUMN_SEPARATOR . "MEAN_LENGTH" . $CSV_COLUMN_SEPARATOR . "MEDIAN_LENGTH" . $CSV_COLUMN_SEPARATOR . "STANDARD_DEVIATION_LENGTH" . $CSV_COLUMN_SEPARATOR . "LENGTH_MIN" . $CSV_COLUMN_SEPARATOR . "LENGTH_MAX\n";

# connect to database
my $mysql;
if (!$no_database)
{
    $mysql    = new Greenphyl::ConnectMysql();
    $g_dbh      = $mysql->DBconnect();
    print "Working on database: " . $Greenphyl::Config::DATABASES->[0]->{'database'} . " (" . $Greenphyl::Config::DATABASES->[0]->{'host'} . ")\n";
    print {$g_log_fh} "Database: " . $Greenphyl::Config::DATABASES->[0]->{'name'} . ", mysql://" . $Greenphyl::Config::DATABASES->[0]->{'login'} . ":" . ('*' x length($Greenphyl::Config::DATABASES->[0]->{'password'})) . "@" . $Greenphyl::Config::DATABASES->[0]->{'host'} . "/" . $Greenphyl::Config::DATABASES->[0]->{'database'} . "\n";
}


# begin computation
my $start_date = time();

# check if a single family was specified
if ((-d $input) && (-f $input . '/' . $MAST_OUTPUT_FILENAME))
{
    # set input to a single file
    $input .= '/' . $MAST_OUTPUT_FILENAME;
}

# check if it should run on only one specified family
if (-f $input)
{
    # last number in $input should be the family name
    my ($family) = ($input =~ m/(\d*)\/?[^\/]+$/o);

    # check data source
    my ($fasta_path, $family_path);
    if ($no_database)
    {
        # find where the FASTA might be
        # extract path parts
        ($fasta_path, $family_path) = ($input =~ m/^(.*?)\/?([^\/]*)\/[^\/\n]+$/o);
        if ($fasta_path)
        {
            # we did match more than a family path
            if (-f $fasta_path . '/' . $family . $FASTA_EXT)
            {
                # the FASTA file is in the directory above the family directory
                $fasta_path .= '/' . $family . $FASTA_EXT
            }
            elsif (-f $fasta_path . '/' . $family_path . '/' . $family . $FASTA_EXT)
            {
                # the FASTA file is in the family directory
                $fasta_path .= '/' . $family_path . '/' . $family . $FASTA_EXT
            }
            else
            {
                confess "ERROR: unable to find family FASTA file ('$fasta_path/$family$FASTA_EXT')!\n";
            }
        }
        else
        {
            # no more than a subdirectory
            # check in current directory
            if (-f $family . $FASTA_EXT)
            {
                $fasta_path = $family . $FASTA_EXT;
            }
            elsif (-f $family_path . '/' . $family . $FASTA_EXT)
            {
                $fasta_path = $family_path . '/' . $family . $FASTA_EXT;
            }
            else
            {
                confess "ERROR: unable to find family FASTA file ('$family$FASTA_EXT')!\n";
            }
        }
    }
    
    
    print {$g_log_fh} "Processing single family '$family' ('$input').\n\n";
    print "Processing specified family '$family' ('$input')...";
    my @output_stats = getMemeStats($family, $input, ($no_database ? $fasta_path : $g_dbh), $output_directory);
    if (@output_stats)
    {
        print {$g_stats_fh} '"' . join("\"$CSV_COLUMN_SEPARATOR\"", $family, @output_stats) . "\"\n";
        print "done.\n";
    }
}
else
{
    # process all available families
    my @families;
    # check if database should be used
    if ($no_database)
    {
        if (-d $input)
        {
            print "Listing available MEME families in directory '$input'...\n";
            print {$g_log_fh} "Processing MEME directory '$input'.\n\n";
            # gather families from MEME output directory
            my ($meme_dh, $childname);
            opendir($meme_dh, $input) or confess "ERROR: Failed to open directory '$input'!\n$!\n";
            while ($childname = readdir($meme_dh))
            {
                # check for a family directory (directories having numeric name)
                if (($childname =~ m/^\d+$/) && (-d "$input/$childname"))
                {
                    # got a family directory
                    # check if MAST output is available
                    if (-f "$input/$childname/$MAST_OUTPUT_FILENAME")
                    {
                        # add to family list
                        if ($g_debug)
                        {
                            print "DEBUG: Adding family '$childname'\n";
                        }
                        push(@families, $childname);
                    }
                    else
                    {
                        if ($g_debug)
                        {
                            print "DEBUG: Missing $MAST_OUTPUT_FILENAME for family '$childname'\n";
                        }
                        warn "WARNING: Missing '$MAST_OUTPUT_FILENAME' for family '$childname'!\n";
                    }
                }
                elsif ($g_debug)
                {
                    print "DEBUG: Skipping '$childname'\n";
                }
            }
            closedir($meme_dh);
            print "done.\n";
        }
        else
        {
            confess "ERROR: input '$input' is not a directory or a $MAST_OUTPUT_FILENAME file!\n";
        }
    }
    else
    {
        print {$g_log_fh} "Processing database families.\n\n";
        # gather families from database
        my $sql_query = "SELECT f.family_id
                         FROM family f
                              LEFT JOIN found_in fi USING (family_id)
                         WHERE f.validated IN (1, 2, 3)
                               AND fi.class_id = 1;";
        @families = @{$g_dbh->selectcol_arrayref($sql_query)};
    }

    my $computation_start_date = time();
    my $processed_family_count = 0;
    print "Processing " . scalar(@families) . " families...\n";
    print "Progress: 0%" if (!$g_debug);
    # process each family
    foreach my $family (@families)
    {
        my $mast_filepath = "$input/$family/$MAST_OUTPUT_FILENAME";
        my @output_stats = getMemeStats($family, $mast_filepath, $no_database ? "$input/$family$FASTA_EXT" : $g_dbh, $output_directory);
        if (@output_stats)
        {
            print {$g_stats_fh} '"' . join("\"$CSV_COLUMN_SEPARATOR\"", $family, @output_stats) . "\"\n";
        }
        # print progress
        my $computation_duration = time() - $computation_start_date;
        my $progress_percent = 100. * ++$processed_family_count / scalar(@families);
        my $remaining_time = $progress_percent ? '(' . $processed_family_count . '/' . scalar(@families) .  ' ' . getDuration((100 - $progress_percent) * $computation_duration / $progress_percent) . ' remaining)' : '';
        printf("\rProgress: %.1f%% %s                    ", $progress_percent, $remaining_time) if (!$g_debug);

    }
}

close($g_stats_fh);
close($g_log_fh);

print "\nDone!\n";

exit(0);

# CODE END
###########

=pod

=head1 DIAGNOSTICS

=head1 AUTHORS

Mathieu ROUARD (Bioversity-France), m.rouard@cgiar.org

Valentin GUIGNON (Bioversity-France), v.guignon@cgiar.org

Matthieu CONTE

Christian WALDE

=head1 VERSION

Version 2.0.x

$Id$

=head1 SEE ALSO

GreenPhyl documentation

=cut
