#!/usr/bin/perl

=pod

=head1 NAME

runMeme.pl

=head1 SYNOPSIS

    filtration.pl

=head1 REQUIRES

Perl5

=head1 DESCRIPTION

.

=cut

use strict;
use warnings;

use lib "../lib";
use lib "../../lib";

use Carp;
use Getopt::Long;
use Pod::Usage;

use Statistics::Descriptive;
use File::Temp qw/ tempfile tempdir /;
use Bio::SeqIO;
use Tie::IxHash;

use Greenphyl::Config;
use Greenphyl::ConnectMysql;

use Data::Dumper;

<<<<<<< .mine
# todo : configure path to store logs
open (my $LOG, ">filtration.log") or die $!; 
open (my $LOG_ERR, ">filtration_error.log") or die $!; 
open (my $LOG_WARN, ">filtration_warning.log") or die $!; 
=======
>>>>>>> .r1363

# SVN management part
######################
my ($REV_STRING) = ('$Revision$' =~ m/(\d+)/);

<<<<<<< .mine
# todo : add getOpt for parameters
my $family = shift;      
=======
>>>>>>> .r1363

<<<<<<< .mine
if ($family)
{
    print $LOG "\nFAMILY: $family\t";
    my $output = statMeme($family);
}
else
{
    #my @req1 = ($mysql->select("SELECT distinct(family_id) FROM found_in WHERE class_id = 1"));
    my @req1 = ($mysql->select("SELECT family_id FROM family WHERE validated IN (1,2,3)"));
   
    foreach $family (@req1)  
    { 
        print $LOG "\nFAMILY: $family\t";
        my $output = statMeme($family);
        print $LOG $output if ($output);           
    }
}
=======
>>>>>>> .r1363

# Script global constants
##########################

=pod

=head1 CONSTANTS

B<$DEBUG>: (boolean)

If debug is set to a true value, debug messages will be displayed and
modification to database won't be committed.

B<$FILTERING_VERSION>: (string)

Version of the Filtering script.

=cut

my $DEBUG                    = 0;

my $FILTERING_VERSION          = '2.0.' . $REV_STRING;




# Script global variables
##########################

=pod

=head1 VARIABLES

B<$mysql>: (Greenphyl::ConnectMysql)

GreenPhyl MySQL connection object.

=cut

my $mysql;
my $dbh;
my $log_fh;
my $log_err_fh;
my $log_warn_fh;




# Script global functions
##########################

=pod

=head1 FUNCTIONS

=head2 XXX

B<Description>: XXX.

B<ArgsCount>: 1

=over 4

=item arg: $x (XXX) (O)

XXX

=back

B<Return>: nothing

B<Example>:


=cut

sub computeMemeStat
{
    my ($fam)     = @_;
<<<<<<< .mine
      
    my $file = $meme_dir . "/$fam.html";    
    print $LOG_ERR "$fam : $file not found \n" unless (-e $file);
=======

    my $file = $Greenphyl::Config::MEME . "/mast.$fam.html";
    print {$log_err_fh} "$fam : $file not found \n" unless (-e $file);
>>>>>>> .r1363
    return unless (-e $file);

    my $output;

    my $cpt_eject_family    =   0; #nb ejected seq from family
    my $cpt_nophylo         =   0; #nb ejected seq from phylo
	my $test_if_MEME_ok     =   0; #to control if MEME file is updated
    my $cpt                 =   0;
    my $count               =   0;
    my $size;

    open(IN, $file) or die "Error: unable to open input file $file";

    my ( %result, @length, @exponentiel_numbers, @evalues, %hash );
    tie %result, "Tie::IxHash";

    # parse fichier mast. recup: id (name + splice number), length, evalue et valeur expo
    while (<IN>)
    {
        chomp;
        my ( $meme_sequence_name, $length, $eval, $exponentiel_number );

        # sequences number
        if ( $_ =~ /Database\scontains\s(\d*)\ssequences/ )
        {
            $count = $1;
			my $sql     = "SELECT COUNT(1) FROM seq_is_in WHERE family_id = $fam;";
			($size)  = $mysql->select($sql);

			#check is meme file contains the same number of sequences
			print {$log_err_fh} "$fam : MEME not up to date\n" if ( $count != $size);
			
			
			#TODO : create fasta file for MEME

			return if ( $count != $size);
        }

        # parse sequence name
        if ( ($_ =~ m/^<A NAME\=a.*\><\/A>(\S+).*/i) || ($_ =~ m/^(\d+\.m\d+\_RICCO)/i))
		{
            $cpt++; #unique id for the hash
            $meme_sequence_name = $1; 

            #allow to print species of ejected sequence
            if ( $meme_sequence_name =~ /_(\D{5})\Z/ )
            {
                $result{$cpt}{'species'} = $1;
                 # remove species code
                $meme_sequence_name =~ s/_\D{5}\Z//;
            }

            $result{$cpt}{'id'} = $meme_sequence_name;

            # detect splice form with ".splice number"
            if ( $meme_sequence_name =~ /(\S+)\.(\d{1})\Z/ )
            {
                $result{$cpt}{'id'}     = $1;
                $result{$cpt}{'splice'} = $2;
            }

            # detect splice form for MAIZE sequence
            elsif ( $meme_sequence_name =~ /(GRMZM\S+\_P0)(\d{1})\Z/ )
            {
                $result{$cpt}{'id'}     = $1;
                $result{$cpt}{'splice'} = $2;
                #print "$1--$2\n";
            }
            $result{$cpt}{'fullname'} = $meme_sequence_name;
        }

        # parse sequence length
        if ( $_ =~ /^\s+LENGTH\s\=\s(\S+).*/ )
        {
            $length = $1;
            $result{$cpt}{'length'} = $length;
            push @length, $length;
        }

        # parse evalue score
        if ( $_ =~ /E-VALUE\s=\s+([+-]?(?=\d|\.\d)\d*(\.\d*)?([Ee]([+-]?\d+))?)/ )
        {
            $eval = $1;
            $result{$cpt}{'eval'} = $eval;

            # make log evalue
            if ($eval != 0)
            {
                my $log_evalue     = -log($eval);
                $result{$cpt}{'log'} = $log_evalue;
                push @evalues, $log_evalue;
            }
            #use expo value
            if ( $eval =~ /e\-(\d+)/ )
            {	#print "$1\n";
                $exponentiel_number = $1;
                $result{$cpt}{'expo_n'} = $exponentiel_number;
                push @exponentiel_numbers, $exponentiel_number;
            }
        }
    }#end while in meme/mast file

<<<<<<< .mine
 
    # make stat on log, exponential valu and sequence length
=======


    # make stat on log, exponential value and sequence length
>>>>>>> .r1363
    my ($median_log, $ecart_log)        = calculateStats(@evalues);
    my ($median_expo, $ecart_expo)      = calculateStats(@exponentiel_numbers);
    my ($median_length, $ecart_length)  = calculateStats(@length);

    # Fix cutoffs
    my ($log_cut, $expo_cut, $min_cut, $max_cut) = fixCutOffs($median_expo, $median_log, $ecart_log, $ecart_expo, $median_length, $ecart_length);        
<<<<<<< .mine
    
=======


>>>>>>> .r1363
    my ($eject_cpt, $splice_eject, %splice_cpt);
    my @filtered_sequences;
    my @error_classif;
    my @total_sequences;

    # loop on parsing results
    for my $meme_id ( keys %result )
    {
        my $expo_value   = $result{$meme_id}{'expo_n'};
        my $length_value = $result{$meme_id}{'length'};
        my $seq_id       = $result{$meme_id}{'fullname'};
        my $species      = $result{$meme_id}{'species'};
        my $evalue       = $result{$meme_id}{'eval'};
        my $length       = $result{$meme_id}{'length'};

        push @total_sequences, $seq_id;

        ## ADD more permissive cutoff on evalue (or log) for ancestral species
        if ($expo_value)
        {
            if ( $species eq  'PHYPA' || $species eq 'SELMO' || $species eq 'OSTTA' || $species eq 'CHLRE'|| $species eq 'CYAME')
            {
                my $test    =   ($expo_value * 20 / 100);
                $expo_value =   $expo_value + $test;
            }
        }

        # detect classif errors for potential removal from this family
        if ((!$expo_value) && ( $evalue != 0 )
            || ( $expo_value < $expo_cut ) && ( $length_value <= $min_cut ) # if exponential is under cutoff and size less than cutoff
            || $expo_value == 0)
        {

			push @filtered_sequences, $seq_id;
			setFilterNumber($seq_id, 2);
			$cpt_eject_family++;
			$cpt_nophylo++;
        }

        # filter on cutoffs (expo + length)
        elsif ((( $expo_value <= $expo_cut )    # if exponential value <= exponential cutoff
            || ( !$expo_value )                 # evalue not an exponential
            || ( $length_value >= $max_cut )    # length > max cutoff
            || ( $length_value <= $min_cut )    # length < min cutoff
            )
            && ( ($evalue != 0) && ( $expo_value <= 100))
            )
        {
            push @filtered_sequences, $seq_id;
            setFilterNumber($seq_id, 1);
            $eject_cpt++;
            $cpt_nophylo++;
        }


        #Eject iteratively splices with a lower value than the current one
        elsif ( $result{$meme_id}{'splice'} )
        {
            my $base_sequence_name = $result{$meme_id}{'id'};       # sequence id without splice form number (e.g. xxx.1)
            my $splice_number      = $result{$meme_id}{'splice'};
            my $temp_name = $base_sequence_name;

            #to avoid case sensitive problem with arabidospis sequence
            if ($temp_name=~ /^At/i)
            {
                $temp_name = uc($temp_name);
            }
<<<<<<< .mine
            
             
            # keep the best evalue for a splice  
            #TODO keep .1 (or best evalue if .1 is absent)     
=======


            # keep the best evalue for a splice
>>>>>>> .r1363
            $hash{$temp_name} = [ $evalue, $splice_number ];

            if (exists $splice_cpt{$temp_name} )
            {
                if ( $base_sequence_name =~ m/(GRMZM)/ )
                {
<<<<<<< .mine
                    push @filtered_sequences, "$base_sequence_name$hash{$temp_name}->[1]";    # eject previous best sequence                                 
=======
                    push @filtered_sequences, "$base_sequence_name$hash{$temp_name}->[1]";    # eject previous best sequence
                    #print "filtered : $base_sequence_name$hash{$temp_name}->[1]\n\n";

>>>>>>> .r1363
                }
                else
                {
                    push @filtered_sequences, "$base_sequence_name.$hash{$temp_name}->[1]";    # eject previous best sequence
                }
                setFilterNumber($seq_id, 1);
                $splice_eject++;
                $cpt_nophylo++;
            }
            else
            {
                $splice_cpt{$temp_name}++;
            }
        }
    }
<<<<<<< .mine
    
=======


>>>>>>> .r1363
    my $sequence_fasta_number = createFilteredFasta($fam, @filtered_sequences);
<<<<<<< .mine

      
=======

>>>>>>> .r1363
    if ( ($size - $cpt_nophylo) != $sequence_fasta_number )
    {
        print {$log_warn_fh} "warning: fasta file does not contain the same number of sequences (" . $sequence_fasta_number . " instead of " . ($size - $cpt_nophylo) . ")\n";
    }

    print $LOG "family size: $size \t $sequence_fasta_number sequences in fasta \t Ejected from phylo: $cpt_nophylo  (Eject from fam: $cpt_eject_family + filter:$eject_cpt + Splice:$splice_eject)\n";
    return;
}


=pod

=head2 XXX

B<Description>: XXX.

B<ArgsCount>: 1

=over 4

=item arg: $x (XXX) (O)

XXX

=back

B<Return>: nothing

B<Example>:


=cut

sub calculateStats
{
    my @data = @_;

    my $stat = Statistics::Descriptive::Full->new();
    $stat->add_data(@data);
    my $median = $stat->median();
    my $ecart  = $stat->standard_deviation();

    return  ($median, $ecart);
}


=pod

=head2 XXX

B<Description>: XXX.

B<ArgsCount>: 1

=over 4

=item arg: $x (XXX) (O)

XXX

=back

B<Return>: nothing

B<Example>:


=cut

sub fixCutOffs
{
    my ($median_expo, $median_log, $ecart_log, $ecart_expo, $median_length, $ecart_length) = @_;

    my $output;

    #value for size of 'ecart type'
    my $value = 0;

    # will define the group consistency and will weight the standard deviation
    # if high consistency ( meme evalue <= e-80), standard deviation will be multiplied by 1 etc.
    # parameters are arbitrary

    if ( $median_expo >= 80 )
    {
        $output = "Group with high consistency\n";
        $value = 1;
    }
    elsif ( $median_expo < 80 and $median_expo >= 50 )
    {
        $output = "Group with acceptable consistency\n";
        $value = 1.5;
    }
    elsif ( $median_expo < 50 and $median_expo >= 20 )
    {
        $output = "Group with poor consistency\n" ;
        $value = 2;
    }
    elsif ( $median_expo < 20 )
    {
        $output = "Group with no consistency\n";
        $value = 3;
    }

    #under this score, sequences will be ejected
    my $log_cut  = $median_log - ( $ecart_log * $value );
    my $expo_cut = $median_expo - ( $ecart_expo * $value);

    ### fix filter on  Length
    my $min_cut = $median_length - $ecart_length;
    my $max_cut = $median_length + ( $ecart_length * 3 );

    return ($log_cut, $expo_cut, $min_cut, $max_cut);
}




=pod

=head2 XXX

B<Description>: XXX.

B<ArgsCount>: 1

=over 4

=item arg: $x (XXX) (O)

XXX

=back

B<Return>: nothing

B<Example>:


=cut

sub setFilterNumber
{
    my ($seq_textid, $number) = @_;
    my $sql     = "SELECT seq_id FROM sequences WHERE seq_textid = '$seq_textid'";
	my ($id)    = $mysql->select($sql);
	my $update  = "UPDATE sequences SET filtered = $number WHERE seq_id = $id";

	$dbh->do($update) or die "update failed";

    return;
}




=pod

=head2 XXX

B<Description>: XXX.

B<ArgsCount>: 1

=over 4

=item arg: $x (XXX) (O)

XXX

=back

B<Return>: nothing

B<Example>:


=cut

sub createFilteredFasta
{
    my ($fam, @filtered_sequences) = @_;

    my %filtered_sequences = map { $_ => 1 } @filtered_sequences;

    my $output;

    #generate fasta: get all seq from family and test if not in ejected list @filtered_sequences
    my ( $tmp_fh, $tmp_filename ) = tempfile( 'XXXX', DIR => $Greenphyl::Config::TMP, SUFFIX => '.fa' );
    my $seq_out = Bio::SeqIO->newFh( -format => 'fasta', -file => ">$tmp_filename" );

	my $sql = " SELECT S.sequence, SP.species_name, S.seq_textid
                    FROM sequences S, species SP , seq_is_in SI
                    WHERE S.species_id = SP.species_id
                    AND S.seq_id = SI.seq_id
                    AND SI.family_id = '$fam' ";

		my @rep = $mysql->select($sql);
		next if !scalar(@rep);

        my $sequence_fasta_number;
        for (@rep)
        {
            my $seq         = new Bio::Seq( -display_id => join( "_", $_->[2], $_->[1] ), -seq => $_->[0] );
            my $seqtext_id  = $_->[2];
            my $cpt         = 0; # to detect ejected sequences

            next if $filtered_sequences{$seqtext_id};

            print $seq_out $seq;
            $sequence_fasta_number++;
        }
		close $tmp_filename;
		
		system("chmod 664 $tmp_filename") and confess "Cannot change permissions: $!";  
		system("cp $tmp_filename $Greenphyl::Config::DATA/pipe/IN/$fam.fa") and die "Cannot transfer: $!";
		$output = "$Greenphyl::Config::DATA/pipe/IN/$fam.fa created\n";

		print {$log_fh} $output;
		return $sequence_fasta_number;
}

<<<<<<< .mine
=======

=pod

=head2 XXX

B<Description>: XXX.

B<ArgsCount>: 1

=over 4

=item arg: $x (XXX) (O)

XXX

=back

B<Return>: nothing

B<Example>:


=cut

sub classifyBySequenceNumber
{

        my ($tmp_filename, $fam, $sequence_fasta_number) = @_;

        my $output;

        if ( $sequence_fasta_number < 50)
		{
		    $output .= copyFile($tmp_filename, $fam, 50, 49);
	    }
	    elsif($sequence_fasta_number < 100)
	    {
	        $output .= copyFile($tmp_filename, $fam, 100, 50);
	    }
	    elsif($sequence_fasta_number < 150)
	    {
	        $output .= copyFile($tmp_filename, $fam, 150, 50);
	    }
	    elsif($sequence_fasta_number < 300)
	    {
	        $output .= copyFile($tmp_filename, $fam, 300, 150);
	    }
	    elsif($sequence_fasta_number < 500)
	    {
	        $output .= copyFile($tmp_filename, $fam, 500, 200);
	    }
	    elsif($sequence_fasta_number < 1000)
	    {
	        $output .= copyFile($tmp_filename, $fam, 1000, 5000);
	    }
	    else
	    {
	        $output .= copyFile($tmp_filename, $fam, 1000);
	    }
	    return $output;
}



=pod

=head2 XXX

B<Description>: XXX.

B<ArgsCount>: 1

=over 4

=item arg: $x (XXX) (O)

XXX

=back

B<Return>: nothing

B<Example>:


=cut

# If range parameter is not provided, it puts a + to the name of the folder
sub copyFile
{

    my ($tmp_filename, $fam, $sequence_fasta_number, $range) = @_;
    my $output;

    my $max = $sequence_fasta_number - 1;
    my $min = $sequence_fasta_number - $range if ($range);
    $min = '+' if (!$range);

    my $folder = "seq$min-$max";
    my $directory = $Greenphyl::Config::DATA . '/pipe/' . $folder;

    system("mkdir $folder") if (! -d $directory );

    system("cp $tmp_filename $directory/$fam.fa") and warn "Cannot transfer: $!";
	$output = "$directory/$fam.fa created";

    return $output;
}


>>>>>>> .r1363


# Script options
#################

=pod

=head1 OPTIONS

    filtration.pl [-help] [-man] [-debug] [FAMILY]

=head2 Parameters

=over 4

=item B<-help>:

display this help message.

=item B<-man>:

display user manual.

=item B<-debug>:

set debug mode on.

=item B<FAMILY>:

a GreenPhyl family name.

=back

=cut

# CODE START
#############

print "\nGreenPhyl Filtering v$FILTERING_VERSION\n\n";

my ($help, $man, $debug, $family);

GetOptions(
    'help|?'             => \$help,
    'man'                => \$man,
    'debug'              => \$debug,
) or pod2usage(2);
if ($help) { pod2usage(1); }
if ($man) { pod2usage( -verbose => 2 ); }

$DEBUG ||= $debug;

$family = shift;


my $start_date = time();

$mysql    = new Greenphyl::ConnectMysql();
$dbh      = $mysql->DBconnect();

open($log_fh, ">filtration.log") or confess $!;
open($log_err_fh, ">filtration_error.log") or confess $!;
open($log_warn_fh, ">filtration_warning.log") or confess $!;

if ($family)
{
    print {$log_fh} "\nFAMILY: $family\t";
    my $output = computeMemeStat($family);
    if ($output)
    {
        print {$log_fh} $output;
    }
}
else
{
    my @families = ($mysql->select("SELECT f.family_id
                                FROM family f
                                    LEFT JOIN found_in fi USING (family_id)
                                WHERE f.validated IN (1, 2, 3) AND fi.class_id = 1;"));
    foreach $family (@families)
    {
        print {$log_fh} "\nFAMILY: $family\t";
        my $output = computeMemeStat($family);
        if ($output)
        {
            print {$log_fh} $output;
        }
    }
}

close($log_fh);
close($log_err_fh);
close($log_warn_fh);

exit(0);

# CODE END
###########

=pod

=head1 DIAGNOSTICS

=head1 AUTHORS

Mathieu ROUARD (Bioversity-France), m.rouard@cgiar.org

Valentin GUIGNON (Bioversity-France), v.guignon@cgiar.org

Matthieu CONTE

Christian WALDE

Christelle ALUOME

=head1 VERSION

Version 2.0.x

$Id: run_ipr_statistics.pl 1341 2010-11-16 14:28:46Z guignon $

=head1 SEE ALSO

GreenPhyl documentation

=cut
