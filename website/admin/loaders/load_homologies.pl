#!/usr/bin/perl

=pod

=head1 NAME

load_homologs.pl - Loads homologies into database

=head1 SYNOPSIS

    load_homologs.pl

=head1 REQUIRES

Perl5, BioPerl, Greenphyl

=head1 DESCRIPTION

This script can be used to insert homology computation results into GreenPhyl
database.

=cut

use strict;
use warnings;
use Carp qw (cluck confess croak);
use Readonly;

use lib '../../lib';
use lib '../../local_lib';

use Bio::SeqIO;
use Bio::SearchIO;

use Greenphyl::Config;
use Greenphyl::Log('nolog' => 1,);
use Greenphyl::Utils;



# Script global constants
##########################

=pod

=head1 CONSTANTS

B<$RAP_FINAL_XML_TREE_FILENAME_SUFFIX>: (string)

suffix added to RAP final tree (XML format).

B<$RAP_STATS_FILENAME_SUFFIX>: (string)

suffix added to RAP statistices file (tabular format).

=cut

Readonly my $RAP_FINAL_XML_TREE_FILENAME_SUFFIX => '_rap_final_tree.xml';
Readonly my $RAP_STATS_FILENAME_SUFFIX          => '_rap_stats_tree.nwk';
Readonly my $HOMOLOGY_TYPE_ORTHOLOGY            => 'orthology';
Readonly my $HOMOLOGY_TYPE_SUPER_ORTHOLOGY      => 'super-orthology';
Readonly my $HOMOLOGY_TYPE_ULTRA_PARALOGY       => 'ultra-paralogy';


# Script global variables
##########################

=pod

=head1 VARIABLES

B<$g_dbh>: (DBI::db)

Database handle.

=cut

my $g_dbh;



# Script global functions
##########################

=pod

=head1 FUNCTIONS

=head2 processMainDirectory

B<Description>: Process a directory containing phylogeny result directories.
Each phylogeny result directory has the family name the results belong to.

B<ArgsCount>: 1

=over 4

=item $phylo_directory: (string) (R)

path to the direcotry. Trailing slash is not required.

=back

B<Return>: (integer)

The number of processed families.

B<Example>:

    processMainDirectory('/data/greenphyl/phylo_results');

=cut

sub processMainDirectory
{
    my $phylo_directory = shift;
    if (!$phylo_directory)
    {
        confess "usage: processMainDirectory(directory);";
    }

    my $dir_handle;
    my $processed_families = 0;

    # remove trailing slashes
    $phylo_directory =~ s/\/+$//;

    opendir($dir_handle, $phylo_directory) or confess "ERROR: unable to open the directory containing phylogeny result directories ('$phylo_directory')!\n$!\n";
    readdir($dir_handle); # reads .
    readdir($dir_handle); # reads ..

    LogInfo('Processing homology directory "' . $phylo_directory . '"');

    while (my $family_name = readdir($dir_handle))
    {
        # only process subdirectories
        if (-d $phylo_directory . '/' . $family_name)
        {
            LogDebug('Processing family "' . $family_name . '"');
            $processed_families += processPhylogenyResultDirectory($phylo_directory . '/' . $family_name, $family_name);
        }
    }
    LogInfo('Done with "' . $phylo_directory . '"!');
    LogInfo('Processed families: ' . $processed_families);

    closedir($dir_handle);

    return $processed_families;
}


=pod

=head2 processPhylogenyResultDirectory

B<Description>: Process a single directory containing phylogeny results.

B<ArgsCount>: 2

=over 4

=item $phylo_directory: (string) (R)

path to the direcotry. Trailing slash is not required.

=item $family_name: (string) (R)

Name of the family.

=back

B<Return>: (integer)

1 if the directory could have been processed.

B<Example>:

    processPhylogenyResultDirectory('/data/greenphyl/phylo_results/23456', '23456');

=cut

sub processPhylogenyResultDirectory
{
    my ($phylo_directory, $family_name) = @_;

    if (!$phylo_directory || !$family_name)
    {
        confess "usage: processPhylogenyResultDirectory(directory, family_name);";
    }

    # remove trailing slashes
    $phylo_directory =~ s/\/+$//;
    my $base_filename = $phylo_directory . '/' . $family_name . '/' . $family_name;
    my $rap_xml_file_path = $base_filename . $RAP_FINAL_XML_TREE_FILENAME_SUFFIX;
    my $rap_stats_file_path = $base_filename . $RAP_STATS_FILENAME_SUFFIX;
    LogDebug('Check if files exist:  "' . $rap_xml_file_path . '" and "' . $rap_stats_file_path . '"');
    # check if final xml tree file and newick files exist and are not null
    if ((-e $rap_xml_file_path)
        && (-s $rap_xml_file_path)
        && (-e $rap_stats_file_path)
        && (-s $rap_stats_file_path))
    {
        LogDebug('Files ok');
        print " Processing family '$family_name'...\n";
        # load homology data
        processRapFile($rap_stats_file_path, $family_name);
        print " done\n";
        return 1;
    }

    return 0;
}


=pod

=head2 processRapFile

B<Description>: Process a RAP statistics result file and store results into
database.

B<ArgsCount>: 2

=over 4

=item $rap_stats_file_path: (string) (R)

path to the RAP statistics result file.

=item $family_name: (string) (R)

Name of the family.

=back

B<Return>: nothing

B<Example>:

    processRapFile('/data/greenphyl/phylo_results/23456/23456_rap_stats_tree.nwk', '23456');

=cut

sub processRapFile
{
	my ($file_path, $family_name) = @_;

    if (!$file_path)
    {
        confess "usage: processRapFile(stats_file_path, family_name);";
    }

    LogDebug('Processing RAP statistics file "' . $file_path . '"');

    my $stats_fh;
	open($stats_fh, $file_path) or confess "ERROR: failed to open RAP statistics file '$file_path'!\n $!\n";

    # skip first line (column headers)
    my $line = <$stats_fh>;
	while ($line = <$stats_fh>)
	{
        # GENE1   kVALUE  DIST    SPEC    T-DUP   I-DUP   ORTHO   ULTRAP  GENE2
		my ($query_name,
            $kvalue,
            $distance,
            $speciation,
            $tdup,
            $idup,
            $orthology,
            $ultraparalogy,
            $hit_name) = (split(/\t/, $line));

        # remove species code from names
		$query_name =~ s/\_[A-Z]{5}$//g;
		$hit_name   =~ s/\_[A-Z]{5}$//g;

        # retrieve family ID in database
		my $sql_query = "SELECT family_id FROM family WHERE family_id = ?;";
		my ($family_id) = $g_dbh->selectrow_array($sql_query, undef, $family_name);

        # retrieve query ID in database
		$sql_query = "SELECT seq_id FROM sequences WHERE seq_textid LIKE ?;";
		my ($query_id) = $g_dbh->selectrow_array($sql_query, undef, $query_name);

        # retrieve hit ID in database
		$sql_query = "SELECT seq_id FROM sequences WHERE seq_textid LIKE ?;";
		my ($hit_id) = $g_dbh->selectrow_array($sql_query, undef, $hit_name);

		if (!$family_id)
        {
            cluck "ERROR: Family '$family_id' not found in database!\n";
        }
		elsif (!$query_id)
        {
            cluck "ERROR: Query sequence '$query_name' not found in database!\n";
        }
		elsif (!$hit_id)
        {
            cluck "ERROR: Hit sequence '$hit_name' not found in database!\n";
        }
		else
		{
			my $duplication = $tdup + $idup;

            my $homology_type = undef;
			if ($orthology =~ m/true/i)
            {
                $homology_type = $HOMOLOGY_TYPE_ORTHOLOGY;
            }
            elsif ($ultraparalogy =~ m/true/i)
            {
                $homology_type = $HOMOLOGY_TYPE_ULTRA_PARALOGY;
            }

            LogInfo("add homology: family_id=$family_id, query=$query_name, hit=$hit_name, homolgy=$homology_type");

			$sql_query = "
                INSERT INTO homologies (family_id,
                                        type,
                                        score,
                                        kvalue,
                                        distance,
                                        speciation,
                                        duplication)
                VALUES (?, ?, ?, ?, ?, ?);";
			$g_dbh->do($sql_query,
                undef,
                $family_id,
                $kvalue,
                $distance,
                $speciation,
                $duplication,
                $homology_type);
		}
	}
    LogDebug('Done processing RAP statistics file');
}


# Script options
#################

=pod

=head1 OPTIONS

load_homologs.pl [-help | -man] [-directory <DIRECTORY>]

=over 4

=item B<-help>:

Prints a brief help message and exits.

=item B<-man>:

Prints the manual page and exits.

=item B<-directory> (string):

Directory containing phylogeny result directories.

=back

=cut


# CODE START
#############

# options processing
my ($man, $help, $phylo_directory) = (0, 0, '');

# parse options and print usage if there is a syntax error.
GetOptions("help|?"        => \$help,
           "man"           => \$man,
           "directory|d=s" => \$phylo_directory,)
    or pod2usage(2);
if ($help) {pod2usage(1);}
if ($man) {pod2usage(-verbose => 2);}

if (!$phylo_directory)
{
    warn "No phylogeny result input directory specified!\n";
    pod2usage(1);
}
elsif (!-d $phylo_directory)
{
    confess "Invalid directory specified: '$phylo_directory'\n";
}

$g_dbh = connectToDatabase();
$g_dbh->begin_work() or croak $g_dbh->errstr;

processMainDirectory($phylo_directory);

$g_dbh->commit() or confess($g_dbh->errstr);
$g_dbh->disconnect();

exit(0);

# CODE END
###########
