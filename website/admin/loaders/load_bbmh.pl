#!/usr/bin/perl

use strict;
$|++;

use Bio::SeqIO;
use Bio::SearchIO; 
use Greenphyl::ConnectMysql;

my $mysql= new Greenphyl::ConnectMysql();
my $dbh = $mysql->DBconnect();

my $file  = shift;


my $sql_query = "SELECT seq_id FROM sequences WHERE seq_textid = ?";
my $o_sql_query = $dbh->prepare($sql_query);
 
my $sql_query2 = "insert into bbmh (query, hit, score, eval) values(?,?,?,?)";
my $o_sql_query2 = $dbh->prepare($sql_query2);

#test public spe
#my $sql_query3 = "SELECT species_id FROM sequences WHERE seq_id = ?";
#my $o_sql_query3 = $dbh->prepare($sql_query3);

open(IN, $file);

while(<IN>) 
{
		my ($query, @seq_ids) = (split(/\t/,$_));
		for my $hit (@seq_ids) 
		{
			$hit = ucfirst(lc($hit));
		#print "$query\n";
			if ($hit =~ /(\S*)\((\S+)\s{1}(\S*)\)/)
			{
			my $seq_id = $1;
			my $score  = $2;
			my $eval   = $3;
				
			
			$o_sql_query->execute($query);
			my ($rep_q) = $o_sql_query->fetchrow_array();
			
			$o_sql_query->execute($seq_id);
			my $rep_h = $o_sql_query->fetchrow_array();
			
			#test public spe
			#$o_sql_query3->execute($rep_h);
			#my $spe = $o_sql_query3->fetchrow_array();
			
				#if ($spe < 18)
				#{
					if ($rep_h)
					{
					#print "Load $query ($rep_q) - $seq_id ($rep_h) - SPE $spe - $score -$eval\n";
					$o_sql_query2->execute($rep_q, $rep_h, $score,$eval);
					}
				#}
			}
		}	
}	
$o_sql_query->finish();
$o_sql_query2->finish();
$mysql->disconnect();
