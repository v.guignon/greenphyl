#!/usr/bin/perl
#create by Matthieu
#script permettant de charger une liste
use strict;
use lib '../../lib';
use lib '../../local_lib';
use Bio::Seq;
use Bio::SearchIO;
use Bio::SeqIO;

use CGI qw/:standard start_Tr start_table start_div start_form/;
use Greenphyl::ConnectMysql;      # crappy module to connect the database and perform requests
use Greenphyl::Web::Greenphyl;    # contains generic methods like template page

my $mysql = new Greenphyl::ConnectMysql();
my $con   = $mysql->DBconnect();
my $file  = shift;
my $fam;

my $sql = "UPDATE `family` SET  `plant_spe` = '0' WHERE `plant_spe` = '1'";
print "$sql\n";
$mysql->insert($sql);

open( IN, $file );
while (<IN>)
{
    $fam = $_;
    my $sql = "SELECT family_id FROM family WHERE family_id = $fam";
    my ($rep) = $mysql->requete($sql);
    if ($rep)
    {
        my $sql = "UPDATE `family` SET  `plant_spe` = '1' WHERE `family_id` = '$fam'";
        print "$sql\n";
        $mysql->insert($sql);
    }
    else
    {
        print "$fam not in FAMILY table\n";
    }
}

