#!/usr/bin/perl

=pod

=head1 NAME

phyloxml4pipe.pl 
 
=head1 SYNOPSIS

phyloxml4pipe.pl -d <path_to_main_dir> -l -f -i > log.txt

Options:

    -d (dir)  : path to the main directory that contains each directory by family
    
    -l (load) : load data in the db. If not specified, it will create only the xml file
    
    -f (force): do not prompt when a family (scores related to each of its sequences) will be updated
    
    -i (ignore): ignores the check for missing files and proceeds even if it would destroy data

=head1 REQUIRES

Perl5

=head1 DESCRIPTION

will process all the directories present in the directory provided as input and will extract homologous score data from Rio text files, load into the db (if specified with -l) 
and will create a phyloxml file with orthologous socres embedded.


=cut

use strict;
use warnings;
use Carp qw (cluck confess croak);
use Getopt::Long;
use Pod::Usage;
use Bio::TreeIO;
use File::Basename;
use IO::Dir;
use List::Util 'max';
use FileHandle;

use lib '../../lib';
use lib '../../local_lib';

use List::Compare;
use Greenphyl::Web::Greenphyl;
use Greenphyl::ConnectMysql;

my $gp   = Greenphyl::Web::Greenphyl->new;
my $dbh  = $gp->{mysql}->{dbh};
my $time = time;                             # used for log_skip

my ( $help, $man, $load, $dir, $force, $debug_g, $ignore_missing );

GetOptions(
    'help|?'   => \$help,
    'man'      => \$man,
    'debug'    => \$debug_g,
    'dir|d=s'  => \$dir,
    'load|l'   => \$load,                    # if i want data to be loaded in the db
    'force|f'  => \$force,                   # if i don't want to be prompted for update in db
    'ignore|i' => \$ignore_missing,          # i want to proceed even if files are missing
) or pod2usage(2);
if ($help) { pod2usage(1); }
if ($man) { pod2usage( -verbose => 2 ); }

# options checking
if ( not defined $dir )
{
    pod2usage(2);

}
elsif ( !-d $dir )
{
    printError("directory does not exist! [ $dir ]");
}

# Script global variables
##########################

=pod

=head1 VARIABLES

B<directory>: (string)

directory path where files are stored for a gene family.

=cut

my $DIRECTORY;
my $FIRST_COLUMN_WIDTH = 30;
my $INDENTATION        = 4;

my $log = process_all_subdirectories($dir);

# Log main steps of the process
print $log if $log;

# Script global functions
##########################

=pod

=head1 FUNCTIONS

=head2 process_all_subdirectories

B<Description>: process_all_subdirectories file 

=cut

sub process_all_subdirectories
{
    my ($all_dir) = @_;

    # check arguments
    confess "usage: printWarning(message);" if @_ != 1;

    #loop over all the subdir one by one
    my @dir_names = get_directories_in_dir($all_dir);

    my @results = map process_directory( $all_dir, $_ ), @dir_names;

    my $output = join "\n", @results;

    return $output;
}

sub process_directory
{
    my ( $all_dir, $family_id ) = @_;

    my $output = "\n$family_id : ";

    #check if one of the sequences belonging to this family is already in the score table
    my $sql = "SELECT s.query_id from seq_is_in si, scores s where si.seq_id = s.query_id AND family_id = ?";
    my $sequence_id = $dbh->selectrow_array( $sql, undef, $family_id );

    #if so, warn, ask for update
    my $seq_delete = ask_for_update( $family_id, $sequence_id );

    # if we have a sequence if present, but no deletion query, then the user declined the update, so we should skip this directory
    return log_skip( $family_id, 'no_update' ) if $sequence_id and !$seq_delete;

    # find sdi file (sdi.xml or tree_0.xml)
    my $input = find_sdi_file("$all_dir/$family_id/$family_id");

    #then
    $output .= process_sdi_file( $input, $family_id, $seq_delete );

    return $output;
}

sub ask_for_update
{
    my ( $family_id, $sequence_id ) = @_;

    return if !$sequence_id;

    my $answer = 'y';    # default answer if we don't prompt
    $answer = promptUser("$family_id scores is already in the db. would you like to continue [n]") if !$force;

    return if $answer ne 'y';

    #remove all scores for all sequences of the family in scores table (remove + insert = update)
    my $sql = "SELECT s.query_id from seq_is_in si, scores s where si.seq_id = s.query_id AND family_id = ? group by query_id";
    my $seqids_to_remove = $dbh->selectall_arrayref( $sql, { Slice => {} }, $family_id );
    $seqids_to_remove = join ',', map { $_->{query_id} } @{$seqids_to_remove};
    $seqids_to_remove ||= -1;    # this should work in mysql, in case there's nothing to delete

    my $seq_delete = "DELETE FROM scores WHERE query_id in ( $seqids_to_remove ) ";
    printDebug($seq_delete);

    return $seq_delete;
}

sub find_sdi_file
{
    my ($path_root_name) = @_;

    return $path_root_name . "_tree_0.xml"   if -e $path_root_name . "_tree_0.xml";
    return $path_root_name . "_tree.sdi.xml" if -e $path_root_name . "_tree.sdi.xml";
    return;
}

=pod

=head2 process_sdi_file

B<Description>: process file 

=cut

sub process_sdi_file
{
    my ( $sdi_file, $family_id, $seq_delete ) = @_;

    # check arguments
    return "SDI file not found: not processed (update was not performed)" if !$sdi_file;

    my $output;
    my $filename;
    ( $filename, $DIRECTORY ) = fileparse($sdi_file);

    #printDebug("Filename  : " . $filename );
    printDebug("Directory : $DIRECTORY");

    # get the list of text files generated in RIO that are in the same directory as the sdi file
    my @list = glob( $DIRECTORY . 'rio_*.txt' );

    #check if all the expected files are in the list (sequences not filtered)
    my $missing = check_for_expected_files( $family_id, \@list );
    return log_skip( $family_id, 'missing', $missing ) if $missing and !$ignore_missing;

    if ( $seq_delete and $load )
    {
        $output .= "...will be updated\n" if $seq_delete;
        $dbh->do($seq_delete);
    }

    $output .= "SDI file processed\n";
    my %orthologs = insert_sequence_rel_in_db(@list);
    my $phyloxmlfile = create_phyloxml_file_with_rio_scores( $filename, $sdi_file, \%orthologs );
    $output .= "RIO file created: $phyloxmlfile\n";

    my $target_root = "/apps/GreenPhyl/v2/htdocs/img";
    $target_root = "../../htdocs/img" if $^O eq "MSWin32";

    $sdi_file =~ /\/(\d+)_/;
    $output .= transfer_file( $DIRECTORY, "$1.mask.aln", "$target_root/alm/", 'multiple alignment' );
    $output .= "\n";

    $phyloxmlfile =~ /([^\/]*)$/;
    my $tree_file = $1;
    $output .= transfer_file( $DIRECTORY, $tree_file, "$target_root/trees/", 'gene tree' );
    $output .= "\n";

    return $output;
}

sub transfer_file
{
    my ( $source, $file, $target, $name ) = @_;

    return "transfer of $name failed, source file does not exist"  if !-e $source . $file;
    return "transfer of $name failed, source file is not readable" if !-r $source . $file;

    system "cp $source$file $target$file";
    system "chmod 644 $target$file";

    return "transfer of $name failed, target file does not exist"    if !-e $target . $file;
    return "transfer of $name done, but target file is not readable" if !-r $target . $file;

    return "transfer of $name done";
}

sub log_skip
{
    my ( $family_id, $reason, $message ) = @_;

    die "log_skip needs a reason" if !$reason;

    #makes it a proper object instead of a magic variable (filehandle)
    my $skip_log = FileHandle->new( "skip_$time.log", ">>" );
    $skip_log->print("$family_id\n");

    $message .= "Sequences for family $family_id already present, user did decline update, skipping.\n" if $reason eq 'no_update';
    $message .= "Skipping family $family_id.\n" if $reason eq 'missing';

    return $message;
}

sub check_for_expected_files
{
    my ( $family_id, $list ) = @_;

    return if !$family_id;

    my $all_non_filtered_sequences = "SELECT se.seq_textid from seq_is_in si, sequences se where se.seq_id = si.seq_id AND family_id = ? AND filtered = 0";
    my @all_non_filtered_sequences = @{ $dbh->selectall_arrayref( $all_non_filtered_sequences, { Slice => {} }, $family_id ) };

    #compare @all_non_filtered_sequences and @list and spot those who are missing
    @all_non_filtered_sequences = map { $_->{seq_textid} } @all_non_filtered_sequences;

    #return the $1 by making it the value of the last line of the block
    # () is an empty list, so @new_list remains unchanged
    my @new_list = map { m/rio_(.*)\.txt/; $1 or (); } @{$list};

    # Get those items which appear (at least once) only in the first list.
    my @missing = List::Compare->new( \@all_non_filtered_sequences, \@new_list )->get_unique;

    #if missing, raise warning on the sequence name
    return if !@missing;

    my $missing       = join ', ', @missing;
    my $missing_count = @missing;
    my $all_count     = @all_non_filtered_sequences;
    my $output        = "rio files missing! ($missing_count/$all_count) [ $missing ]\n";

    return $output;
}

=pod

=head2 insert_sequence_rel_in_db

B<Description>: parse the text file created by Rio for a sequence and insert its sequence relationship in the the greenphyl db.

=cut

sub insert_sequence_rel_in_db
{
    my @files = @_;

    my %orthologs = map process_rio_file($_), @files;

    # same as my %hash = map { $_->{query}, $_->{subject} } @array;
    # map transforms the array in a hash

    return %orthologs;
}

sub process_rio_file
{
    my ($rio_file) = @_;

    printDebug( 'Text file parsed:' . $rio_file );
    ##check size
    printWarning(" $rio_file is empty ") if -z $rio_file;

    # IN is a global identifier, like a function name, once initialized it carries the value EVERYWHERE
    # $IN is a variable; once the scope { } of the function ends, it gets destroyed
    open( my $IN, '<', $rio_file ) or die "Cannot open $rio_file : $! ";

    $rio_file =~ /rio_([\w\.-]+).txt$/;
    my $ref_seq = $1;

    my $sql = "SELECT seq_id FROM sequences WHERE seq_textid = ?";
    my $ref_seqid = $dbh->selectrow_array( $sql, undef, $ref_seq );

    my %ortholog;

    # read text file for scores
    while (<$IN>)
    {
        if ( $_ =~ /^([\w\.-]+)\s+(\d{1,3})/ )    # Os06g45330.1		83	-	-
        {
            my $sequence        = $1;
            my $orthology_score = $2;
            $ortholog{$sequence} = $orthology_score if $orthology_score > 30;
        }

        extract_and_insert_scores( $_, $ref_seqid ) if $load;
    }
    close $IN;

    return if !keys %ortholog;
    return ( $ref_seq => \%ortholog );            # return an array
}

sub extract_and_insert_scores
{
    my ( $line, $ref_seqid ) = @_;

    # Load score into DB
    return if $line !~ /^([\w\.-]+)\s+(\d{1,3})\s+(\d{1,3})\s+(\d{1,3})\s+(\S+)/;    # Os06g45330.1		83	20	83 5.234

    my $sequence        = $1;
    my $orthology_score = $2;
    my $subtree         = $3;
    my $superortholog   = $4;
    my $distance        = $5;
    my $sql             = "SELECT seq_id FROM sequences WHERE seq_textid = ?";
    my $seq_id          = $dbh->selectrow_array( $sql, undef, $sequence );

    insert_score( 'orthology_score', $ref_seqid, $seq_id, $orthology_score ) if $orthology_score > 30;    #arbitrary thresold
    insert_score( 'subtree',         $ref_seqid, $seq_id, $subtree )         if $subtree;
    insert_score( 'superortholog',   $ref_seqid, $seq_id, $superortholog )   if $superortholog;
    insert_score( 'distance',        $ref_seqid, $seq_id, $distance )        if $distance;

    return;
}

sub insert_score
{
    my ( $type, $ref_seqid, $seq_id, $score ) = @_;

    my %scores_ids = (
        orthology_score => 1,
        subtree         => 2,
        superortholog   => 3,
        distance        => 4,
    );
    my $score_id = $scores_ids{$type};
    
    # TODO: check if this sequences has already a score.
    # A sequence is supposed to be analyses by the pipeline at only one clustering level. otherwise problem
    
    my $sql = "INSERT INTO scores(query_id, subject_id, type_id, score) VALUES( ?, ?, ?, ? )";
    my @params = ( $ref_seqid, $seq_id, $score_id, $score );

    printDebug("$type: $sql ; @params");
    $dbh->do( $sql, undef, @params );

    return;
}

=pod

=head2 create_phyloxml_file_with_rio_scores

B<Description>: create a phyloxml file

=cut

sub create_phyloxml_file_with_rio_scores
{
    my ( $filename, $file_full_path, $orthologs ) = @_;

    # check arguments
    confess "usage: printWarning(message);" if 3 != @_;

    my $treeio = Bio::TreeIO->new(
        -format => 'phyloxml',
        -file   => $file_full_path
    );

    my $tree  = $treeio->next_tree;
    my @nodes = $tree->get_nodes;

    my %sequences_seen;
    record_seen_sequences( $_, $treeio, \%sequences_seen ) for @nodes;

    generate_xml_for_relations( $orthologs, $treeio, $tree, \%sequences_seen );

    # because the input file can be xxxx.sdi.xml or xxxxtree_0.xml
    $filename =~ s/sdi/rio/         if $filename =~ /sdi/;
    $filename =~ s/tree_0/tree.rio/ if $filename =~ /tree_0/;

    # write_tree
    my $output  = $DIRECTORY . $filename;
    my $treeout = Bio::TreeIO->new(
        -format => 'phyloxml',
        -file   => ">$output"
    );
    $treeout->write_tree($tree);

    return $output if -e $output;

    printStageEnd("Failed!");
    return;
}

sub generate_xml_for_relations
{
    my ( $orthologs, $treeio, $tree, $sequences_seen ) = @_;

    my %relation_seen;
    for my $keys ( keys %{$orthologs} )
    {
        next if !defined $sequences_seen->{$keys};

        #print $keys ."\n";

        my $ortholog = $orthologs->{$keys};

        for my $keys2 ( keys %{$ortholog} )
        {

            #print "$keys \t $keys2 \n";
            next if $relation_seen{$keys2}{$keys};

            $relation_seen{$keys}{$keys2}++;

            my $score = $ortholog->{$keys2};

            $treeio->add_phyloXML_annotation(
                '-obj' => $tree,
                '-xml' =>
                    qq{<sequence_relation id_ref_0="$keys" id_ref_1="$keys2" type="orthology"><confidence type="rio">$score</confidence></sequence_relation>}
            );
        }
    }

    return;
}

sub record_seen_sequences
{
    my ( $n, $treeio, $sequences_seen ) = @_;

    #print $n->id;

    return if !$n->sequence;

    # get sequence object for the node
    my $seq = $n->sequence->[0];

    # or get annotation using path
    my ($name) = $treeio->read_annotation( '-obj' => $seq, '-path' => 'name' );

    #print $name2, "\n";

    #keep track sequence ids of the tree
    $sequences_seen->{$name}++;

    # set id_source. this is mandatory to link with sequence relation
    # the spaces are necessary or the XML does not parse. I have no idea why.
    $treeio->add_attribute(
        '-obj'  => $seq,
        '-attr' => "id_source = \"$name\"",
    );

    return;
}

=pod

=head2 printWarning

B<Description>: display a warning message on STDERR.

B<ArgsCount>: 1

=over 4

=item $message: (string) (R)

warning message to display.

=back

B<Example>:

    printWarning("the file $filename has been replaced!\nYou have been warned!");

=cut

sub printWarning
{
    my ($message) = @_;

    # check arguments
    confess "usage: printWarning(message);" if 1 != @_;

    $message = make_stderr_message($message);
    print STDERR "WARNING$message";

    return;
}

=pod

=head2 printError

B<Description>: display an error message on STDERR.

B<ArgsCount>: 1

=over 4

=item $message: (string) (R)

error message to display.

=back

B<Example>:

    printError("Computation failed!");

=cut

sub printError
{
    my ($message) = @_;

    # check arguments
    confess "usage: printError(message);" if 1 != @_;

    $message = make_stderr_message($message);
    print STDERR "ERROR$message";

    return;
}

=pod

=head2 printDebug

B<Description>: display a debug message on STDERR if debug mode is ON.

B<ArgsCount>: 1

=over 4

=item $message: (string) (R)

debug message to display.

=back

B<Example>:

    printDebug("Starting the process using paramaters:!\na=25\nb=50\nc=3");

=cut

sub printDebug
{
    my ($message) = @_;

    # check arguments
    confess "usage: printDebug(message);" if 1 != @_;

    # check if debugging is disabled
    return if !$debug_g;

    $message = make_stderr_message($message);
    print STDERR "DEBUG$message";

    return;
}

sub make_stderr_message
{
    my ($message) = @_;

    # remove trailing invisible characters
    $message =~ s/\s+$//s;

    # check for multi-lines warning
    $message = prepare_multiline_message($message);

    return ": $message\n";
}

sub prepare_multiline_message
{
    my ($message) = @_;

    return $message if $message !~ m/[\r\n]/;

    my @message_lines = split( /(?:\n|\r\n|\r)/, $message );
    $message = shift @message_lines;
    $message .= "\n";
    $message .= join( "\n         ", @message_lines );

    return $message;
}

=pod

=head2 printStageStart

B<Description>: display the name of a stage when started.

B<ArgsCount>: 1

=over 4

=item $stage_name: (string) (R)

Name of the stage without any EOL character.

=back

B<Example>:

    printStageStart('Alignment');

=cut

sub printStageStart
{
    my ($stage_name) = @_;

    # check arguments
    confess "usage: printStageStart(stage_name);" if 1 != @_;

    # compute spaces to add
    my $reminding_spaces_count = $FIRST_COLUMN_WIDTH - $INDENTATION - length($stage_name);
    $reminding_spaces_count = max( 0, $reminding_spaces_count );
    print ' ' x $INDENTATION . $stage_name . '.' x $reminding_spaces_count;

    # in case of debug mode, add a line-feed
    print STDERR "\n" if $debug_g;

    return;
}

=pod

=head2 printStageEnd

B<Description>: display the status of a stage when terminating.

B<ArgsCount>: 1

=over 4

=item $status: (string) (R)

Status of the stage execution.

=back

B<Example>:

    printStageEnd('OK');

=cut

sub printStageEnd
{
    my ($status) = @_;

    # check arguments
    confess "usage: printStageEnd(status);" if 1 != @_;

    # indent if debug mode
    print ' ' x $FIRST_COLUMN_WIDTH if $debug_g;

    # remove trailing invisible characters
    $status =~ s/\s+$//s;
    print $status . "\n";

    return;
}

sub promptUser
{

    #-------------------------------------------------------------------#
    #  two possible input arguments - $promptString, and $defaultValue  #
    #  make the input arguments local variables.                        #
    #-------------------------------------------------------------------#

    my ( $promptString, $defaultValue ) = @_;

    #-------------------------------------------------------------------#
    #  if there is a default value, use the first print statement; if   #
    #  no default is provided, print the second string.                 #
    #-------------------------------------------------------------------#

    $promptString .= "[$defaultValue]" if $defaultValue;
    print "$promptString: ";

    $| = 1;          # force a flush after our print
    $_ = <STDIN>;    # get the input from STDIN (presumably the keyboard)

    #------------------------------------------------------------------#
    # remove the newline character from the end of the input the user  #
    # gave us.                                                         #
    #------------------------------------------------------------------#

    chomp;

    #-----------------------------------------------------------------#
    #  if we had a $default value, and the user gave us input, then   #
    #  return the input; if we had a default, and they gave us no     #
    #  no input, return the $defaultValue.                            #
    #                                                                 #
    #  if we did not have a default value, then just return whatever  #
    #  the user gave us.  if they just hit the <enter> key,           #
    #  the calling routine will have to deal with that.               #
    #-----------------------------------------------------------------#

    my $result = $defaultValue;
    $result ||= '';
    $result = $_ if $_;

    return $result;
}

sub get_directories_in_dir
{
    my ($dir) = @_;

    #initializes the class with a value of $dir
    tie my %dir, 'IO::Dir', $dir;
    my @directories = grep is_directory_and_not_dots("$dir/$_"), keys %dir;

    return @directories;
}

sub is_directory_and_not_dots
{
    my ($path) = @_;

    return if !-d $path;
    return if $path =~ m@/\.{1,2}$@;

    return 1;
}

=pod

=head1 AUTHORS

Mathieu ROUARD (Bioversity-France), m.rouard@cgiar.org
Christian  WALDE 

=head1 VERSION

Version 0.0.1

Date 19/05/2010

=cut
