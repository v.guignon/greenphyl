-- --------------------------------------------------------
--
-- Procedures et fonctions
--
-- --------------------------------------------------------

SET GLOBAL log_bin_trust_function_creators = 1;

DELIMITER $$
--
-- Procedures
--
DROP PROCEDURE IF EXISTS addGreenPhylUser$$
CREATE DEFINER=CURRENT_USER PROCEDURE addGreenPhylUser(
    IN gp_user_login VARCHAR(255),
    IN gp_user_display_name VARCHAR(255),
    IN gp_user_email VARCHAR(255),
    IN gp_user_password VARCHAR(255),
    IN gp_user_flags VARCHAR(255),
    IN gp_description VARCHAR(255)
  )
BEGIN
    DECLARE l_user_salt VARCHAR(255);
    DECLARE l_user_password_hash VARCHAR(255);

    SET l_user_salt := MD5(RAND());

    IF ((gp_user_password IS NULL) OR (0 = LENGTH(gp_user_password))) THEN
      SET l_user_password_hash := NULL;
      SET l_user_salt := NULL;
    ELSE
      SET l_user_password_hash := hashGreenPhylPassword(gp_user_password, l_user_salt);
    END IF;
    
    INSERT INTO users (login, display_name, email, password_hash, salt, flags, description) VALUES (
      gp_user_login,
      gp_user_display_name,
      gp_user_email,
      l_user_password_hash,
      l_user_salt,
      gp_user_flags,
      gp_description
    );
  END$$

DROP PROCEDURE IF EXISTS clearFamilySpecificity$$
CREATE DEFINER=CURRENT_USER PROCEDURE clearFamilySpecificity(IN v_family_id MEDIUMINT)
BEGIN
  UPDATE families
  SET
    plant_specific = NULL,
    taxonomy_id = NULL
  WHERE id = v_family_id;
END$$

DROP PROCEDURE IF EXISTS clearFamilyStats$$
CREATE DEFINER=CURRENT_USER PROCEDURE clearFamilyStats(IN v_family_id MEDIUMINT)
BEGIN
  UPDATE families
  SET
    min_sequence_length      = 0,
    max_sequence_length      = 0,
    average_sequence_length  = 0,
    median_sequence_length   = 0,
    sequence_count           = 0,
    alignment_length         = 0,
    alignment_sequence_count = 0,
    masked_alignment_length  = 0,
    masked_alignment_sequence_count = 0
  WHERE id = v_family_id;
END$$

DROP PROCEDURE IF EXISTS countSequencesByFamiliesSpecies$$
CREATE DEFINER=CURRENT_USER PROCEDURE countSequencesByFamiliesSpecies()
BEGIN
  TRUNCATE sequence_count_by_families_species_cache;
  INSERT INTO sequence_count_by_families_species_cache (family_id, species_id, sequence_count)
    SELECT fs.family_id, s.species_id, count(s.id)
      FROM families_sequences fs JOIN sequences s ON fs.sequence_id = s.id
      GROUP BY fs.family_id ASC, s.species_id ASC;
  INSERT INTO variables
    SELECT 'last_count_family_seq_update', NOW()
    ON DUPLICATE KEY UPDATE
        value=NOW();
END$$

DROP PROCEDURE IF EXISTS countSequencesByFamiliesGenomes$$
CREATE DEFINER=CURRENT_USER PROCEDURE countSequencesByFamiliesGenomes()
BEGIN
  TRUNCATE sequence_count_by_families_genomes_cache;
  INSERT INTO sequence_count_by_families_genomes_cache (family_id, genome_id, sequence_count)
    SELECT fs.family_id, s.genome_id, count(s.id)
      FROM families_sequences fs JOIN sequences s ON fs.sequence_id = s.id
      GROUP BY fs.family_id ASC, s.genome_id ASC;
  INSERT INTO variables
    SELECT 'last_count_family_seq_genome_update', NOW()
    ON DUPLICATE KEY UPDATE
        value=NOW();
END$$

DROP PROCEDURE IF EXISTS updateFamiliesStats$$
CREATE DEFINER=CURRENT_USER PROCEDURE updateFamiliesStats()
BEGIN
  DECLARE v_family_id MEDIUMINT;
  DECLARE done INT DEFAULT 0;
  DECLARE family_cursor CURSOR FOR SELECT id FROM families WHERE black_listed = 0;
  DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = 1;

  OPEN family_cursor;

  family_update_loop: LOOP
    FETCH family_cursor INTO v_family_id;
    IF done THEN
      LEAVE family_update_loop;
    END IF;
    CALL updateFamilyStats(v_family_id);
  END LOOP;

  CLOSE family_cursor;
  
  INSERT INTO variables
    SELECT 'last_families_stats_update', NOW()
    ON DUPLICATE KEY UPDATE
        value = NOW();
END$$

DROP PROCEDURE IF EXISTS updateFamilyStats$$
CREATE DEFINER=CURRENT_USER PROCEDURE updateFamilyStats(IN v_family_id MEDIUMINT)
BEGIN
  DECLARE v_min_seq_length         INT;
  DECLARE v_max_seq_length         INT;
  DECLARE v_average_seq_length     INT;
  DECLARE v_median_seq_length      INT;
  DECLARE v_seq_count              INT;

  SELECT
    min(s.length),
    max(s.length),
    avg(s.length),
    count(s.id)
  INTO
    v_min_seq_length,
    v_max_seq_length,
    v_average_seq_length,
    v_seq_count
  FROM families f
      JOIN families_sequences fs ON f.id = fs.family_id
      JOIN sequences s ON s.id = fs.sequence_id
  WHERE fs.family_id = v_family_id;

  SET @sql = CONCAT("SELECT s.length
  INTO @v_median_low_seq_length
  FROM families_sequences fs
      JOIN sequences s ON s.id = fs.sequence_id
  WHERE fs.family_id = ", v_family_id, "
  ORDER BY s.length ASC
  LIMIT ", CAST((v_seq_count+1)/2 - 1 AS UNSIGNED INTEGER), ", 1");
  PREPARE STMT FROM @sql;
  EXECUTE STMT;

  SET @sql = CONCAT("SELECT s.length
  INTO @v_median_high_seq_length
  FROM families_sequences fs
      JOIN sequences s ON s.id = fs.sequence_id
  WHERE fs.family_id = ", v_family_id, "
  ORDER BY s.length ASC
  LIMIT ", CAST(v_seq_count/2 AS UNSIGNED INTEGER), ", 1");
  PREPARE STMT2 FROM @sql;
  EXECUTE STMT2;

  SET v_median_seq_length := ((@v_median_low_seq_length + @v_median_high_seq_length)/2. + 0.5);

  UPDATE families
  SET
    min_sequence_length     = v_min_seq_length,
    max_sequence_length     = v_max_seq_length,
    average_sequence_length = v_average_seq_length,
    median_sequence_length  = v_median_seq_length,
    sequence_count          = v_seq_count
  WHERE id = v_family_id;

END$$

--
-- Functions
--
DROP FUNCTION IF EXISTS authenticateGreenPhylUser$$
CREATE DEFINER=CURRENT_USER FUNCTION authenticateGreenPhylUser(gp_user_login VARCHAR(255), gp_user_password VARCHAR(255)) RETURNS tinyint(1)
BEGIN
    DECLARE l_authenticated BOOL DEFAULT FALSE;

    IF (EXISTS (SELECT TRUE FROM users u WHERE u.login = gp_user_login AND u.flags NOT LIKE '%disabled%' AND u.password_hash IS NOT NULL AND u.salt IS NOT NULL AND u.password_hash = hashGreenPhylPassword(gp_user_password, u.salt) LIMIT 1)) THEN
      SET @authenticated_user := gp_user_login; 
      SET l_authenticated := TRUE;
    END IF;

    RETURN l_authenticated;
  END$$

DROP FUNCTION IF EXISTS hashGreenPhylPassword$$
CREATE DEFINER=CURRENT_USER FUNCTION hashGreenPhylPassword(gp_password VARCHAR(255), gp_salt CHAR(40)) RETURNS char(40) CHARSET utf8
BEGIN
    RETURN SHA1(AES_ENCRYPT(gp_password, gp_salt));
  END$$


DROP FUNCTION IF EXISTS IdToChar;
CREATE FUNCTION IdToChar(
  	id INT UNSIGNED
  ) 
  RETURNS CHAR(4)
  DETERMINISTIC
BEGIN
  IF (id IS NULL) THEN
    RETURN NULL;
  END IF;

  RETURN CONCAT(CHAR((id & 0x0FF000000) / 0x01000000), CHAR((id & 0x0FF0000) / 0x010000), CHAR((id & 0x0FF00) / 0x0100), CHAR(id & 0x0FF));
END$$

DROP FUNCTION IF EXISTS IdIn;
CREATE FUNCTION IdIn(
  	id INT UNSIGNED,
    serialized_ids LONGBLOB
  ) 
  RETURNS INT UNSIGNED
  DETERMINISTIC
BEGIN
  DECLARE position INT UNSIGNED;
  DECLARE char_id BINARY(4);

  IF (id IS NULL) OR (serialized_ids IS NULL) THEN
    RETURN NULL;
  END IF;

  SET char_id = IdToChar(id);
  SELECT LOCATE(char_id, serialized_ids) INTO position;
  IF (position != 1) THEN
    WHILE (position != 0) AND (((position - 1) MOD 4) != 0) DO
      SELECT LOCATE(char_id, serialized_ids, position + (4 - ((position - 1) MOD 4))) INTO position;
    END WHILE;
  END IF;

	RETURN (position);
END$$

DELIMITER ;
