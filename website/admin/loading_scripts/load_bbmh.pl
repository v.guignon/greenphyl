#!/usr/bin/env perl

=pod

=head1 NAME

load_bbmh.pl - Stores (preprocessed) BBMH in database

=head1 SYNOPSIS

    load_bbmh.pl -bbmh-dir=data/fasta/bbmh/preprocessed -bbmh-dir data/fasta/bbmh/preprocessed -log

=head1 REQUIRES

Perl5

=head1 DESCRIPTION

Loads BBMH data from preprocessed BBMH files with format:

  <query_seq_name>    <hit_seq_name>    <score>    <evalue>
  <query_seq_name>    <hit_seq_name>    <score>    <evalue>
  ...

Used table:
-sequences

Modified table:
-bbmh

=cut

use strict;
use warnings;

use FindBin;
use lib "$FindBin::Bin/../../lib";
use lib "$FindBin::Bin/../../local_lib";
use Carp qw (cluck confess croak);


use Greenphyl;
use Greenphyl::Log('nolog' => 1,);
use Greenphyl::Species;

use Getopt::Long;
use Pod::Usage;

++$|; #no buffering




# Script global constants
##########################

=pod

=head1 CONSTANTS

B<$REV_STRING>: (string)

Current revision (auto-set by GIT).

B<$DEBUG>: (boolean)

When set to true, it enables debug mode.

B<$CONSTANT_NAME>: ([constant nature]) #+++

[constant description and use]. #+++

=cut

# SVN management part
my $REV_STRING = '$id$';

our $DEBUG = 0;




# Script global functions
##########################

=pod

=head1 FUNCTIONS

=head2 InsertBBMHData

B<Description>: Inserts BBMH data from the given species code and file into
database.

B<ArgsCount>: 2

=over 4

=item $species_code: (string) (R)

Species code.

=item $bbmh_file_path: (string) (R)

Path to the corresponding preprocessed BBMH file.

=back

B<Return>: nothing

=cut

sub InsertBBMHData
{
    my ($species_code, $bbmh_file_path) = @_;

    my $dbh = GetDatabaseHandler();

    LogInfo("Loading $bbmh_file_path...");
    my @bbmh_values;

    my $sql_sequence_id = $dbh->prepare("SELECT id FROM sequences WHERE accession = ?;");

    my $prebbmh_fh;
    open($prebbmh_fh, $bbmh_file_path)
        or confess LogError("Can't open file '$bbmh_file_path': $!");

PREBBMH_LINE:
    while (<$prebbmh_fh>)
    {
        chomp;

        my ($query_name, $hit_name, $score, $evalue) = split( /\s+/ );

        # Retrieve sequence ID for this query sequence accession
        $sql_sequence_id->execute($query_name);
        my ($query_id) = $sql_sequence_id->fetchrow_array();
        if (!$query_id)
        {
            LogWarning("Unable to get sequence ID for query '$query_name'!");
            next PREBBMH_LINE;
        }

        # Retrieve sequence ID for this hit sequence accession
        $sql_sequence_id->execute($hit_name);
        my $hit_id = $sql_sequence_id->fetchrow_array();

        if (!$hit_id)
        {
            LogWarning("Unable to get sequence ID for hit '$hit_name'!");
            next PREBBMH_LINE;
        }

        # Both sequence IDs could be retrieved, row can be inserted.
        push(@bbmh_values, [$query_id, $hit_id, $score, $evalue]);
    }

    close($prebbmh_fh);

    if (!@bbmh_values)
    {
        LogInfo("...done loading file.");
        LogWarning("No BBMH found in '$bbmh_file_path'!");
    }
    else
    {
        my $total_rows = 0;
        while (@bbmh_values)
        {
            my @bbmh_values_slice = splice(@bbmh_values, 0, 100);
            # Insert collected bbmh values
            my $insert_query = qq{
                INSERT IGNORE INTO bbmh (query_sequence_id, hit_sequence_id, score, e_value)
                VALUES
            };

            # Build SQL VALUES statement:
            # (A,B,C,D),(E,F,G,H),(I,J,K,L) ...
            $insert_query .= join(',', map{'(' . join(',', @{$_}) . ')'} @bbmh_values_slice);

            my $rows = $dbh->do($insert_query)
               or LogWarning("Failed to insert BBMH from '$bbmh_file_path'!\nInserted $total_rows bbmh while remaining " . scalar(@bbmh_values) . " bbmh.\n" . $dbh->errstr);
            $total_rows += $rows;
        }
        LogInfo("...done loading file.\n$total_rows inserted from '$bbmh_file_path'.");
    }
}




# Script options
#################

=pod

=head1 OPTIONS

load_bbmh.pl [-help | -man]

load_bbmh.pl [-debug] <-bbmh-dir <BBMH_PATH> > [-species [CODES]]

=head2 Parameters

=over 4

=item B<-help>:

Prints a brief help message and exits.

=item B<-man>:

Prints the manual page and exits.

=item B<-debug> (integer):

Executes the script in debug mode. The integer value is optional and is used
to set debug level (use 0 to force disable debug mode).
Default: 0 (not in debug mode).

=item B<-bbmh-dir> (string):

Path to directory containing preprocessed BBMH output files.

=item B<-species> (string):

A list of comma separated species codes or multiple "-species" parameters. Only
these species will be processed.

=item B<-log>:

Enable logging.

=item B<-log-*> (any):

Logging arguments.

=back

=cut


# CODE START
#############

# options processing
my ($man, $help, $debug) = (0, 0, undef);

# Script parameters
my ($bbmh_dir, @filtered_species);

# parse options and print usage if there is a syntax error.
GetOptions('help|?'     => \$help,
           'man'        => \$man,
           'debug:s'    => \$debug,
           'bbmh-dir=s' => \$bbmh_dir,
           'species=s'  => \@filtered_species,
           @Greenphyl::Log::LOG_GETOPT, # skip log parameters
) or pod2usage(1);
if ($help) {pod2usage('-verbose' => 1, '-exitval' => 0);}
if ($man) {pod2usage('-verbose' => 2, '-exitval' => 0);}

# change debug mode if requested/forced
if (defined($debug))
{
    # just '-debug' ?
    if ($debug eq '')
    {
        $debug = 1;
    }
    $DEBUG = $debug;
}

if (!defined $bbmh_dir)
{
    pod2usage("--bbmh-dir must be specified");
}

if (!-e $bbmh_dir)
{
    pod2usage("--bbmh-dir does not exist");
}

eval
{
    my $bbmh_dh;
    opendir($bbmh_dh, $bbmh_dir)
        or confess LogError("Cannot opendir '$bbmh_dir': $!");

    # get available species
    my %species_code_files;
    foreach my $filename (readdir($bbmh_dh))
    {
        my ($species_code) = ($filename =~ m/^($Greenphyl::Species::SPECIES_CODE_REGEXP)\.bbmh$/);
        if ($species_code)
        {
            $species_code_files{$species_code} = "$bbmh_dir/$filename";
        }
        elsif (!-d "$bbmh_dir/$filename")
        {
            LogVerboseInfo("Skipping file '$filename'");
        }
    }
    closedir($bbmh_dh);

    my $species_codes_to_process = [keys(%species_code_files)];
    LogVerboseInfo("Available BBMH-processed species: " . join(', ', @$species_codes_to_process));

    # filter species if needed
    if (@filtered_species)
    {
        @filtered_species = split(/,/,join(',',@filtered_species));
        # check requested species
        foreach (@filtered_species)
        {
            if (!exists($species_code_files{$_}))
            {
                confess LogError("Preprocessed BBMH file is missing for species '$_'!");
            }
        }

        $species_codes_to_process = \@filtered_species;
    }

    LogInfo("Processing species: " . join(', ', @$species_codes_to_process));

    my $species_counter = 0;
    foreach my $species_code (@$species_codes_to_process)
    {
        LogInfo("Processing species $species_code...");
        InsertBBMHData($species_code, $species_code_files{$species_code});
        LogInfo("...done processing species $species_code (". ++$species_counter . "/" . scalar(@$species_codes_to_process) . ").");
    }

};

# catch
HandleErrors();

exit(0);

# CODE END
###########


=pod

=head1 AUTHORS

Mathieu ROUARD (Bioversity), m.rouard@cgiar.org

Valentin GUIGNON (Bioversity), v.guignon@cgiar.org

Matthieu CONTE (Syngenta)

Sebastien BRIOIS (Syngenta)

=head1 VERSION

Version 1.0.0

Date 30/10/2013

=head1 SEE ALSO

GreenPhyl documentation.

=cut
