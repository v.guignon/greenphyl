#!/usr/bin/env perl

=pod

=head1 NAME

formatdb.pl - Run formatdb on GreenPhyl FASTA to generate all needed BLAST banks

=head1 SYNOPSIS

    formatdb.pl

=head1 REQUIRES

Perl5, BLAST formatdb, blastdb_aliastool

=head1 DESCRIPTION

Run formatdb on RefSeq FASTA to generate BLAST banks.

=cut

use strict;
use warnings;

use FindBin;
use lib "$FindBin::Bin/../../lib";
use lib "$FindBin::Bin/../../local_lib";
use Carp qw (cluck confess croak);

use Greenphyl;
use Greenphyl::Log('nolog' => 1,);

use Greenphyl::Species;
use Greenphyl::Tools::Taxonomy;
use Greenphyl::Run::Blast;

use Getopt::Long;
use Pod::Usage;


# Script global constants
##########################

=pod

=head1 CONSTANTS

B<$REV_STRING>: (string)

Current revision (auto-set by GIT).

B<$DEBUG>: (boolean)

When set to true, it enables debug mode.

B<$REFSEQ_BANKS>: (array ref)

Array of RefSeq protein bank names.

=cut

# SVN management part
my $REV_STRING = '$id$';

our $DEBUG = 0;
our $MONOCOT_TAX_ID = 4447;  # NCBI Taxonomy ID for monocots
our $DICOT_TAX_ID   = 71240; # NCBI Taxonomy ID for dicots
our $ALL_SPECIES_BANK_NAME    = 'All';
our $DICOTYLEDONS_BANK_NAME   = 'Dicotyledons';
our $MONOCOTYLEDONS_BANK_NAME = 'Monocotyledons';
our $GREENPHYL_BANK_PREFIX = 'GreenPhyl ';
our $GREENPHYL_BANK_SUFFIX = ' species BLAST bank';




# Script global variables
##########################

=pod

=head1 VARIABLES

B<$g_dbh>: (DBI::db)

Current database handler.

=cut

our $g_dbh = GetDatabaseHandler();



# Script global functions
##########################

=pod

=head1 FUNCTIONS

=head2 CheckSpeciesFASTA

B<Description>: .

B<ArgsCount>: 2-3

=over 4

=item $fasta_directory: (string) (R)

Path where FASTA files should be found.

=item $species_list: (array ref) (R)

List of species (Greenphyl::Species) to work on.

=item $regenerate: (boolean) (O)

If true, replace/update existing BLAST bank data.

=back

B<Return>: (integer)

Numbre of missing species FASTA.

=cut

sub CheckSpeciesFASTA
{
    my ($fasta_directory, $species_list, $regenerate) = @_;

    my $missing_bank = 0;

    # loop on all species
    foreach my $species (@$species_list)
    {
        my $fasta_file_path = "$fasta_directory/" . $species->code;
        LogInfo("Checking species " . $species->name . " (" . $species->code . ")...");
        if (!-r $fasta_file_path)
        {
            LogWarning("..." . $species->name . " FASTA is missing ($fasta_file_path)!");
            ++$missing_bank;
        }
        else
        {
            # check if BLAST bank files are already there or if they should be regenerated
            if ($regenerate
                || (!-r $fasta_file_path . $Greenphyl::Run::Blast::BLAST_PIN_EXT)
                || (!-r $fasta_file_path . $Greenphyl::Run::Blast::BLAST_PHR_EXT)
                || (!-r $fasta_file_path . $Greenphyl::Run::Blast::BLAST_PSQ_EXT))
            {
                my $bank_title = $species->name;
                if ($species->common_name)
                {
                    $bank_title .= ' (' . $species->common_name . ')';
                }
                Greenphyl::Run::Blast::FormatDB($fasta_file_path, undef, {'name' => , $bank_title});
                LogInfo("..." . $species->name . " BLAST bank has been regenerated.");
            }
            else
            {
                LogInfo("..." . $species->name . " OK");
            }
        }
    }

    return $missing_bank;
}


=pod2

=head2 GenerateAliasBankForSpecies

B<Description>: .

B<ArgsCount>: 2-3

=over 4

=item $fasta_directory: (string) (R)

Path where FASTA files should be found.

=item $species_list: (array ref) (R)

List of species (Greenphyl::Species) to work on.

=item $regenerate: (boolean) (O)

If true, replace/update existing BLAST bank data.

=back

B<Return>: (integer)

Numbre of missing species FASTA.

=cut

sub GenerateAliasBankForSpecies
{
    my ($fasta_directory, $species_list, $bank_file_name, $bank_title, $regenerate) = @_;

    my $bank_file_path = "$fasta_directory/$bank_file_name$Greenphyl::Run::Blast::BLAST_ALIAS_EXT";

    $bank_title ||= '';
    $bank_title =~ s/'//g;
    
    if ($regenerate || (!-r $bank_file_path))
    {
        # remove file if needed
        if (-r $bank_file_path)
        {
            unlink($bank_file_path);
        }

        # generate alias
        my $db_alias_command = GP('BLAST_PLUS_PATH') . "/blastdb_aliastool -dblist '" . join(' ', map { $fasta_directory . '/' . $_->code } @$species_list) . "' -dbtype prot -out $fasta_directory/$bank_file_name" . ($bank_title ? " -title '$bank_title'" : '');
        LogDebug("Generating '$bank_file_name' alias using command line:\n$db_alias_command");
        if (system($db_alias_command))
        {
            confess LogError("Failed to generate '$bank_file_name' alias database!\n" . ((0 > $?) ? $! : ''));
        }
    }
}



# Script options
#################

=pod

=head1 OPTIONS

#--- describes parameters given to the script
#+++ command line syntax
#--- requirement of the option and its parameter can be:
#--- required: name or nature inside <>
#--- optional: name or nature inside []
#--- alternative between 2 elements: elements separated by a |

=head2 Parameters

=over 4

#+++=item B<-help>:
#+++
#+++Prints a brief help message and exits.
#+++
#+++=item B<-man>:
#+++
#+++Prints the manual page and exits.
#+++
#+++=item B<-debug> (integer):
#+++
#+++Executes the script in debug mode. The integer value is optional and is used
#+++to set debug level (use 0 to force disable debug mode).
#+++Default: 0 (not in debug mode).
#+++

=item B<[option_name]> ([option nature]): #+++

[option description]. #+++
Default: [option default value if one] #+++

#--- remove if log not used
#+++=item B<-log>:
#+++
#+++Enable logging.
#+++
#+++=item B<-log-*> (any):
#+++
#+++Logging arguments.

=back
#--- Example:
#---
#--- Template.pl [-help | -man]
#---
#--- Template.pl [-debug [debug_level]] [-size <width> [height]]
#---
#--- =over 4
#---
#--- =item B<-help>:
#---
#--- Prints a brief help message and exits.
#---
#--- =item B<-man>:
#---
#--- Prints the manual page and exits.
#---
#--- =item B<-debug> (integer):
#---
#--- Executes the script in debug mode. If an integer value is specified, it will
#--- be the debug level. If "-debug" option was used without specifying a debug
#--- level, level 1 is assumed.
#--- Default: 0 (not in debug mode).
#---
#---=item B<-size> (positive_real) (positive_real):
#---
#--- Set the dimensions of the object that will be drawn. The first value is
#--- the width; the height is the second value if specified, otherwise it will
#--- assume height and width are equal to the first value.
#--- Default: width and height are set to 1.
#---
#---=back

=cut


# CODE START
#############

# options processing
my ($man, $help, $debug, $no_prompt) = (0, 0, undef, undef);

my ($fasta_directory, $regenerate);

# parse options and print usage if there is a syntax error.
GetOptions('help|?'       => \$help,
           'man'          => \$man,
           'debug:s'      => \$debug,
           'q|noprompt'   => \$no_prompt,
           'i|f|fasta|fasta-dir=s' => \$fasta_directory,
           'r|regenerate' => \$regenerate,
           @Greenphyl::Log::LOG_GETOPT, # skip log parameters
) or pod2usage(1);
if ($help) {pod2usage('-verbose' => 1, '-exitval' => 0);}
if ($man) {pod2usage('-verbose' => 2, '-exitval' => 0);}

# change debug mode if requested/forced
if (defined($debug))
{
    # just '-debug' ?
    if ($debug eq '')
    {
        $debug = 1;
    }
    $DEBUG = $debug;
}

$fasta_directory ||= GP('BLAST_BANKS_PATH');
$fasta_directory =~ s/\/+$//; # remove trailing slash

eval
{
    my @species_list = new Greenphyl::Species($g_dbh, {'selectors' => { 'display_order' => ['>', 0], }, });

    LogInfo("Generating BLAST bank files for the " . scalar(@species_list) . " GreenPhyl species in '$fasta_directory'");

    # check each single-species BLAST bank
    my $missing_bank = CheckSpeciesFASTA($fasta_directory, \@species_list, $regenerate);
    if ($missing_bank)
    {
        confess LogError("$missing_bank species FASTA " . (1 == $missing_bank ? 'is' : 'are') . " missing! Stopping formatdb process.");
    }

    # generate ALL BLAST bank
    LogInfo("Generating $ALL_SPECIES_BANK_NAME BLAST bank");
    GenerateAliasBankForSpecies($fasta_directory, \@species_list, $ALL_SPECIES_BANK_NAME, "$GREENPHYL_BANK_PREFIX$ALL_SPECIES_BANK_NAME$GREENPHYL_BANK_SUFFIX", $regenerate);

    # generate Monocotyledons BLAST bank
    LogInfo("Generating $MONOCOTYLEDONS_BANK_NAME BLAST bank");
    my $monocotyledons = GetUnderlyingSpecies($MONOCOT_TAX_ID);
    if (!@$monocotyledons)
    {
        confess LogError("Failed to find Monocotyledons species!");
    }
    GenerateAliasBankForSpecies($fasta_directory, $monocotyledons, $MONOCOTYLEDONS_BANK_NAME, "$GREENPHYL_BANK_PREFIX$MONOCOTYLEDONS_BANK_NAME$GREENPHYL_BANK_SUFFIX", $regenerate);

    # generate Dicotyledons BLAST bank
    LogInfo("Generating $DICOTYLEDONS_BANK_NAME BLAST bank");
    my $dicotyledons = GetUnderlyingSpecies($DICOT_TAX_ID);
    if (!@$dicotyledons)
    {
        confess LogError("Failed to find Dicotyledons species!");
    }
    GenerateAliasBankForSpecies($fasta_directory, $dicotyledons, $DICOTYLEDONS_BANK_NAME, "$GREENPHYL_BANK_PREFIX$DICOTYLEDONS_BANK_NAME$GREENPHYL_BANK_SUFFIX", $regenerate);

};

# catch
HandleErrors();

exit(0);

# CODE END
###########


=pod

=head1 DIAGNOSTICS

#--- Give and explain here every error message the the script may generate.
#--- Use: (W)=warning (optional), (D)=deprecation (optional),
#---      (S)=severe warning (mandatory), (F)=fatal error (trappable),
#---      (X)=very fatal error (non-trappable).
#--- example:
#---
#--- =over 4
#---
#--- =item *
#---
#--- Negative radius;
#--- (F) Can not draw a circle with a negative radius.
#---
#---=back #+++

=head1 AUTHORS

Mathieu ROUARD (Bioversity), m.rouard@cgiar.org

Valentin GUIGNON (Bioversity), v.guignon@cgiar.org

Matthieu CONTE (Syngenta)

Sebastien BRIOIS (Syngenta)

=head1 VERSION

Version [version.subversion.build] #+++

Date [DD/MM/YYY] #+++

=head1 SEE ALSO

#--- other documentation or objects related to this package
GreenPhyl documentation.

=cut
