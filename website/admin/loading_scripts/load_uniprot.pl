#!/usr/bin/env perl

=pod

=head1 NAME

load_uniprot.pl - Fetches and loads UniProt entries into database

=head1 SYNOPSIS

    load_uniprot.pl

=head1 REQUIRES

Perl5

=head1 DESCRIPTION

Send requests to UniProt for each species taxonomy ID. Parse XML response and
load parsed data into the database (dbxref, dbxref_sequences).

Requiers load_ext_db_ref.pl to have been run and db table filled.

Modified table(s):
-dbxref
-dbxref_sequences
-dbxref_synonyms

=cut

use strict;
use warnings;

use FindBin;
use lib "$FindBin::Bin/../../lib";
use lib "$FindBin::Bin/../../local_lib";
use Carp qw (cluck confess croak);

use Greenphyl;
use Greenphyl::Log('nolog' => 1,);
use Greenphyl::Species;
use Greenphyl::SourceDatabase;
use Greenphyl::Sequence;
use Greenphyl::Tools::Sequences;
use Greenphyl::Run::Blast;
use Greenphyl::DBXRef;

use Getopt::Long;
use Pod::Usage;

use XML::DOM;
use XML::LibXML::Reader;
use XML::Simple qw(:strict);
use URI::Escape ('uri_escape');

use Bio::Tools::Run::StandAloneBlast;
use IO::String;
use Bio::SeqIO;

++$|; #no buffering




# Script global constants
##########################

=pod

=head1 CONSTANTS

B<$REV_STRING>: (string)

Current revision (auto-set by GIT).

B<$DEBUG>: (boolean)

When set to true, it enables debug mode.

B<$CONSTANT_NAME>: ([constant nature]) #+++

[constant description and use]. #+++

=cut

# SVN management part
my $REV_STRING = '$id$';

our $DEBUG = 0;




# Script global variables
##########################

=pod

=head1 VARIABLES

B<$g_dbh>: (DBI::db)

Global database handler.

=cut

my $g_dbh = GetDatabaseHandler();




# Script global functions
##########################

=pod

=head1 FUNCTIONS

=head2 RemoveUniprotEntries

B<Description>: Removes all previous UniProt and PubMed entries.

B<ArgsCount>: 1

=over 4

=item $db_sources: (hash ref) (R)

Hash of Greenphyl::SourceDatabase objects. DB names are used as keys.

=back

B<Return>: nothing

=cut

sub RemoveUniprotEntries
{
    my ($db_sources) = @_;

    LogInfo("Cleaning previous dbxref entries for UNIPROT and PubMed...");

    my $entry_count = $g_dbh->do(
        qq{DELETE FROM dbxref WHERE db_id IN (?, ?)}, 
        undef,
        $db_sources->{$Greenphyl::SourceDatabase::SOURCE_DATABASE_UNIPROT}->id,
        $db_sources->{$Greenphyl::SourceDatabase::SOURCE_DATABASE_PUBMED}->id,
    );

    LogInfo("...$entry_count dbxref entries have been removed.");
}


=pod

=head2 FetchUniProtEntries

B<Description>: Send request to UniProt webservice using given taxonomy ID to
fetch XML data if not already available localy. Returns parsed xml content if
found or die if uniprot entries could not be retrieved from uniprot.org.

B<ArgsCount>: 1

=over 4

=item $species: (Greenphyl::Species) (R)

The species to work on.

=back

B<Return>: (hash ref)

Parsed XML content.

=cut

sub FetchUniProtEntries
{
    my ($species) = @_;
    
    # check UniProt directory
    my $uniprot_path = GP('DATA_PATH') . '/uniprots'; 
    LogInfo("Making sure uniprot directory exists: $uniprot_path");
    LogVerboseInfo("Execution:\n" . `mkdir -p $uniprot_path`);

    # check species UniProt file
    my $species_uniprot_path = "$uniprot_path/" . $species->code;
    if (!-e $species_uniprot_path)
    {
        my $uri = 'http://www.uniprot.org/uniprot/?format=xml&query='
                . '(existence:"evidence at protein level"'
                . ' OR existence:"evidence at transcript level"'
                . ' OR reviewed:yes)'
                . ' AND taxonomy:' . $species->taxonomy_id;
        LogInfo("Downloading UniProt file in $species_uniprot_path\nURL: $uri");
        my $wget_string = "wget --quiet '$uri' -O $species_uniprot_path";
        LogVerboseInfo("Execution:\n" . `$wget_string`);
        LogInfo("Download done.");
    }
    else
    {
        LogInfo("Local UniProt file '$species_uniprot_path' found. No download will be done.");
    }

    if (!-e $species_uniprot_path)
    {
        confess LogError("Can't find $species_uniprot_path");
    }
    if (-z $species_uniprot_path)
    {
        confess LogError("'$species_uniprot_path' is empty!");
    }

    LogInfo("Parsing XML data...");
    my $xml_data = XMLin(
        $species_uniprot_path,
        'ForceArray' => 1, # Ensure every hash value is wrapped in an arrayref
        'KeyAttr'    => [],
    );
    LogInfo("...done parsing!");
    
    return $xml_data;
}


=pod

=head2 StoreUniProtEntry

B<Description>: Store parsed UniProt entry into database.

B<ArgsCount>: 3

=over 4

=item $entry: (hash ref) (R)

Contains parsed XML data of a UniProt entry.

=item $species: (Greenphyl::Species) (R)

Current species we work on.

=item $db_sources: (hash ref) (R)

Hash of Greenphyl::SourceDatabase objects. DB names are used as keys.

=back

B<Return>: (integer)

Number of sequences associated to the UniProt entry.

=cut

sub StoreUniProtEntry
{
    my ($entry, $species, $db_sources) = @_;
    
    my $uniprot_data = ExtractUniProtData($entry, $db_sources);
    my $sequences = GetRelatedSequences($uniprot_data->{'accession'}, $entry, $species);
    
    # Skip this uniprot if no related sequence is found in DB
    if (!$sequences ||!@$sequences)
    {
        LogWarning("No matching sequence found for UniProt $uniprot_data->{'accession'}!");
        return 0;
    }
    # Try to insert UniProt DBXRef entry
    InsertIgnore('dbxref', $uniprot_data);
    my $dbxref = Greenphyl::DBXRef->new($g_dbh,
        {
            'selectors' => {
                'accession' => $uniprot_data->{'accession'},
                'db_id'     => $uniprot_data->{'db_id'},
            }
        }
    );
    
    # DBXRef not found, something wrong happened
    if (!$dbxref)
    {
        LogError("Unable to insert or retrieve UniProt $uniprot_data->{'accession'} (having " . scalar(@$sequences) . " matching sequences)");
        return 0
    }

    # Try to insert DBXRef synonym
    my $synonyms = ExtractSynonymData($entry);
    foreach my $synonym (@$synonyms)
    {
        InsertIgnore(
            'dbxref_synonyms',
            {
                'dbxref_id' => $dbxref->id,
                'synonym'   => $synonym,
            },
        );
    }

    # Try to insert relations between sequences and UniProt
    foreach my $sequence (@$sequences)
    {
        InsertIgnore(
            'dbxref_sequences',
            {
                'sequence_id' => $sequence->id,
                'dbxref_id'   => $dbxref->id
            },
        );
    }

    # Try to insert pubmeds and their relations with the sequences
    my $pubmed_data_array = ExtractPubMedData($entry, $db_sources);
    foreach my $pubmed_data (@$pubmed_data_array)
    {
        InsertIgnore('dbxref', $pubmed_data);
        $dbxref = Greenphyl::DBXRef->new($g_dbh,
            {
                'selectors' => {
                    'accession' => $pubmed_data->{'accession'},
                    'db_id'     => $pubmed_data->{'db_id'},
                }
            }
        );

        if (!$dbxref)
        {
            next; # pubmed not found / created
        }

        foreach my $sequence (@$sequences)
        {
            InsertIgnore(
                'dbxref_sequences',
                {
                    'sequence_id' => $sequence->id,
                    'dbxref_id'   => $dbxref->id
                },
            );
        }
    }

    # Everything went fine - return number of sequences linked to this entry
    return scalar(@$sequences);
}


=pod

=head2 ExtractUniProtData

B<Description>: Extract UniProt fields from an XML entry and returns a
simplified hash containg the fields corresponding to the dbxref table columns.

B<ArgsCount>: 2

=over 4

=item $entry: (hash ref) (R)

Contains parsed XML data of a UniProt entry.

=item $db_sources: (hash ref) (R)

Hash of Greenphyl::SourceDatabase objects. DB names are used as keys.

=back

B<Return>: (hash ref)

Hash containing the fields corresponding to the dbxref table columns.

=cut

sub ExtractUniProtData
{
    my ($entry, $db_sources) = @_;
    
    my ($accession) = @{$entry->{'accession'}};
     
    my ($description) = map
        {
            @{ $_->{'fullName'} }
        }
        map
            {
                @{ $_->{'recommendedName'} ? $_->{'recommendedName'} : $_->{'submittedName'} }
            }
            grep
                {
                    defined $_->{'recommendedName'} || defined $_->{'submittedName'}
                }
                @{ $entry->{'protein'} }
    ;

    my $version = $entry->{'version'};
    my $dataset = $entry->{'dataset'};

    if ($description && (ref($description) eq 'HASH'))
    {
        $description = $description->{'content'};
    }

    return {
        'db_id'       => $db_sources->{$Greenphyl::SourceDatabase::SOURCE_DATABASE_UNIPROT}->id,
        'accession'   => $accession,
        'description' => $description,
        'dataset'     => $dataset,
        'version'     => $version,
    };
}


=pod

=head2 ExtractSynonymData

B<Description>: Extract UniProt synonmys from an XML entry and returns them in
an array.

B<ArgsCount>: 1

=over 4

=item $entry: (hash ref) (R)

Contains parsed XML data of a UniProt entry.

=back

B<Return>: (array ref)

An array of synonyms as strings.

=cut

sub ExtractSynonymData
{
    my ($entry) = @_;
    
    my @synonyms = grep
        {$_}
        map
            {
                $_->{'content'}
            }
            grep
                {
                    ($_->{'type'} eq 'primary') || ($_->{'type'} eq 'synonym')
                }
                map
                    {
                        @{ $_->{'name'} }
                    }
                    @{ $entry->{'gene'} }
    ;

    return \@synonyms;
}


=pod

=head2 ExtractPubMedData

B<Description>: Extract UniProt PubMed from an XML entry and returns them in
an array.

B<ArgsCount>: 2

=over 4

=item $entry: (hash ref) (R)

Contains parsed XML data of a UniProt entry.

=item $db_sources: (hash ref) (R)

Hash of Greenphyl::SourceDatabase objects. DB names are used as keys.

=back

B<Return>: (Array ref)

Array of hash, each hash having the keys that correspond to the dbxref table
columns.

=cut

sub ExtractPubMedData
{
    my ($entry, $db_sources) = @_;
    
    my @pubmed;
    
    my @citations = map
        { @{ $_->{'citation'} } }
        @{ $entry->{'reference'} }
    ;

    foreach my $citation (@citations)
    {
        my @pubmed_ref = grep
            { $_->{'type'} eq 'PubMed' }
            @{ $citation->{'dbReference'} }
        ;

        foreach (@pubmed_ref)
        {
            push(
                @pubmed,
                {
                    'db_id'       => $db_sources->{$Greenphyl::SourceDatabase::SOURCE_DATABASE_PUBMED}->id,
                    'accession'   => $_->{'id'},
                    'description' => $citation->{'title'}->[0],
                },
            );
        }
    }

    return \@pubmed;
}


=pod

=head2 GetRelatedSequences

B<Description>: Finds sequences associated to the UniProt accession.

B<ArgsCount>: 3

=over 4

=item $uniprot_accession: (string) (R)

Accession of the UniProt entry.

=item $entry: (hash ref) (R)

Contains parsed XML data of a UniProt entry.

=item $species: (Greenphyl::Species) (R)

Current species we work on.

=back

B<Return>: (array ref)

Array of Greenphyl::Sequence corresponding to the UniProt entry.

=cut


sub GetRelatedSequences
{
    my ($uniprot_accession, $entry, $species) = @_;
    
    my $sequences;
    
    my @locus = map {
            $_->{'content'}
        } grep {
            $_->{'type'} =~ /ordered\ locus|primary/
        } map {
            @{ $_->{'name'} }
        } @{ $entry->{'gene'} }
    ;

    # try to find sequences using locus or sequence.
    if (scalar(@locus) > 0)
    {
        @locus = @{GetAccessionsFromSynonyms(\@locus, \@locus)};
        my $selectors = [
            {'species_id' => $species->id},
            'AND',
            [
                {'locus' => ['IN', @locus]},
                'OR',
                {'accession' => ['IN', @locus]},
                'OR',
                {'polypeptide' => $entry->{'sequence'}->[0]->{'content'}},
            ],
        ];
        $sequences = [
            Greenphyl::Sequence->new(
                $g_dbh,
                {
                    'selectors' => $selectors,
                },
            )
        ];
    }

    # if no sequence found yet, try using DIAMOND
    if ((!$sequences || (!@$sequences)) && GP('DIAMOND_COMMAND'))
    {
        my $input_temp_file  = GP('TEMP_OUTPUT_PATH') . "/tmp_in$$";
        my $output_temp_file = GP('TEMP_OUTPUT_PATH') . "/tmp_out$$";
        if (-e $input_temp_file)
        {
            unlink($input_temp_file);
        }
        if (-e $output_temp_file)
        {
            unlink($output_temp_file);
        }

        my $fasta_content = ">$uniprot_accession\n" . $entry->{'sequence'}->[0]->{'content'};

        $input_temp_file = WriteTemporaryFile($fasta_content, $input_temp_file);

        my $cmd =
            GP('DIAMOND_COMMAND')
            . " blastp -d "
            . GP('BLAST_BANKS_PATH')
            . '/'
            . $species->code
            . " -e 1e-50 -q $input_temp_file -o $output_temp_file";
        LogDebug("COMMAND: $cmd");
        LogInfo("Running Diamond...");
        if (0 != system($cmd))
        {
            if (not $!)
            {
                cluck LogError("Failed to run Diamond!");
            }
            cluck LogWarning("WARNING: Diamond execution failed for UNIPROT entry $uniprot_accession!");
        }
        else
        {
            # find corresponding sequences in database
            # open Diamond file
            my $blast_sio = new Bio::SearchIO('-format' => 'blasttable', '-file' => $output_temp_file);
            if (!$blast_sio)
            {
                LogError("ERROR: Failed to parse DIAMOND file '$output_temp_file'!");
                return;
            }
            # find hit
            if (my $result = $blast_sio->next_result)
            {
                if (my $hit = $result->next_hit)
                {
                    $sequences = [
                        Greenphyl::Sequence->new(
                            $g_dbh,
                            {
                                'selectors' =>
                                {
                                    'species_id' => $species->id,
                                    'accession'  => ['LIKE', $hit->name],
                                },
                            },
                        )
                    ];
                    if (!@$sequences)
                    {
                        LogWarning("Got a Diamond hit but accession '" . $hit->name . "' not found in database!");
                    }
                }
                else
                {
                    LogDebug("Diamond: No hit");
                }
            }
            else
            {
                LogDebug("Diamond: No result");
            }
        }
        LogInfo("...Diamond done.");
        
    }

    return $sequences;
}




# Script options
#################

=pod

=head1 OPTIONS

    load_uniprot.pl [-help | -man]

    load_uniprot.pl [-debug] [-species <SPECIES>]

=head2 Parameters

=over 4

=item B<-help>:

Prints a brief help message and exits.

=item B<-man>:

Prints the manual page and exits.

=item B<-debug> (integer):

Executes the script in debug mode. The integer value is optional and is used
to set debug level (use 0 to force disable debug mode).
Default: 0 (not in debug mode).

=item B<-species> (string):

A list of coma-separated species codes. It is also possible to use several
"-species" parameters. Only UniProt for the given species will be processed.
Default: all GreenPhyl species are processed (the ones with sequence_count > 0).

=item B<-cleandata> (flag):

If set, all previous UniProt and PubMed entries will be removed (regardless of
the species selection).

=item B<-log>:

Enable logging.

=item B<-log-*> (any):

Logging arguments.

=back

=cut


# CODE START
#############

# options processing
my ($man, $help, $debug, $no_prompt) = (0, 0, undef, undef);
my (@species_codes, $cleandata);

# parse options and print usage if there is a syntax error.
GetOptions('help|?'     => \$help,
           'man'        => \$man,
           'debug:s'    => \$debug,
           'q|noprompt' => \$no_prompt,
           'species=s'  => \@species_codes,
           'cleandata'  => \$cleandata,
           @Greenphyl::Log::LOG_GETOPT, # skip log parameters
) or pod2usage(1);
if ($help) {pod2usage('-verbose' => 1, '-exitval' => 0);}
if ($man) {pod2usage('-verbose' => 2, '-exitval' => 0);}

# change debug mode if requested/forced
if (defined($debug))
{
    # just '-debug' ?
    if ($debug eq '')
    {
        $debug = 1;
    }
    $DEBUG = $debug;
}

@species_codes = split(/[,\s]+/, uc(join(',', @species_codes)));

eval
{
    # Get species (filtered if species_code is given)
    my $selectors = {'display_order' => ['>', 0],};
    if (@species_codes)
    {
        foreach my $species_code (@species_codes)
        {
            if ($species_code !~ m/^$Greenphyl::Species::SPECIES_CODE_REGEXP$/)
            {
                confess "Invalid species code: '$species_code'";
            }
        }

        $selectors->{'code'} = ['IN', @species_codes];
    }

    my @species = Greenphyl::Species->new(
        $g_dbh,
        {
            'selectors' => $selectors,
        },
    );
    
    if (!@species)
    {
        confess "No corresponding species found!" . (@species_codes ? ' ' . join(', ', @species_codes) . '.' : '');
    }

    
    LogInfo("Working on " . scalar(@species) . "species (" . join(', ', map {$_->code} @species) . ")");
    
    # Get db sources
    my @db_sources = Greenphyl::SourceDatabase->new($g_dbh);
    my %db_sources = map {$_->name => $_} @db_sources;
    if (!@db_sources
        || !exists($db_sources{$Greenphyl::SourceDatabase::SOURCE_DATABASE_UNIPROT})
        || !exists($db_sources{$Greenphyl::SourceDatabase::SOURCE_DATABASE_PUBMED}))
    {
        confess "Missing source databases! Did you run load_ext_db_ref.pl?";
    }

    # Delete uniprot entries if cleandata option is given
    if ($cleandata)
    {
        RemoveUniprotEntries(\%db_sources);
    }

    # Send request to uniprot service for each species
    foreach my $species (@species)
    {
        LogInfo("Loading Uniprots for " . $species->code . "...");

        # Retrieve UniProts data for the given species
        my $xml_data = FetchUniProtEntries($species);

        # Parse xml entries and insert data into database
        my $entry_count = 0;
        LogInfo("Processing " . scalar(@{$xml_data->{'entry'}}) . " UniProt entries");
        foreach my $entry (@{$xml_data->{'entry'}})
        {
            $entry_count += StoreUniProtEntry($entry, $species, \%db_sources);
        }

        LogInfo("...done. Loaded $entry_count UniProt entries for " . $species->code . ".");
    }

};

# catch
HandleErrors();

exit(0);

__END__

# CODE END
###########


=pod

=head1 AUTHORS

Mathieu ROUARD (Bioversity), m.rouard@cgiar.org

Valentin GUIGNON (Bioversity), v.guignon@cgiar.org

Matthieu CONTE (Syngenta)

Sebastien BRIOIS (Syngenta)

=head1 VERSION

Version 2.0.0

Date 13/02/2013

=head1 SEE ALSO

GreenPhyl documentation.

=cut
