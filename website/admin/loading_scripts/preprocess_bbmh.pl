#!/usr/bin/env perl

=pod

=head1 NAME

preprocess_bbmh.pl - Pre-process BBMH output files

=head1 SYNOPSIS

    preprocess_bbmh.pl --help

    preprocess_bbmh.pl --blast-dir=data/fasta/bbmh --output-dir=/data/fasta/bbmh -log -log-screen

    preprocess_bbmh.pl --blast-dir=data/fasta/bbmh --output-dir=/data/fasta/bbmh --species=ARATH,ORYSA,ZEAMA -log -log-screen -log-logfile=bbmh_arath_orysa_zeama.log

=head1 REQUIRES

Perl5

=head1 DESCRIPTION

Pre-processes raw BBMH (Best BLAST Mutual Hit) output files (from
blastp_all_vs_all.pl) located in a same directory and named using species code:
"SPECO_vs_SPECO" (ex. ZEAMA_vs_ARATH) and generates ".bbmh" preprocessed files.

=cut

use strict;
use warnings;

use FindBin;
use lib "$FindBin::Bin/../../lib";
use lib "$FindBin::Bin/../../local_lib";
use Carp qw (cluck confess croak);

use Getopt::Long;
use Pod::Usage;

use Bio::SearchIO;

use Greenphyl;
use Greenphyl::Log('nolog' => 1,);
use Greenphyl::Species;

++$|; #no buffering




# Script global constants
##########################

=pod

=head1 CONSTANTS

B<$REV_STRING>: (string)

Current revision (auto-set by GIT).

B<$DEBUG>: (boolean)

When set to true, it enables debug mode.

=cut

# SVN management part
my $REV_STRING = '$id$';

our $DEBUG = 0;




# Script global functions
##########################

=pod

=head1 FUNCTIONS

=head2 CollectBestHits

B<Description>: find best hits in a BLAST file.

B<ArgsCount>: 1

=over 4

=item $blast_file_path: (string) (R)

Path to output BLAST file.

=back

B<Return>: (hash ref)

Hash of best hits. Keys are query names and values are hash:
{
    'hit'    => hit name
    'score'  => bit score
    'evalue' => e-value
}

=cut

sub CollectBestHits
{
    my ($blast_file_path) = @_;

    LogVerboseInfo("    Collecting best hits in $blast_file_path...");
    # Collect best hits from BLAST
    my $blast_results = new Bio::SearchIO(
        '-format' => 'blasttable',
        '-file'   => $blast_file_path,
    );

    if (!$blast_results)
    {
        LogWarning("Unable to parse BLAST output for '$blast_file_path': $!");
        return undef;
    }

    my %hit_for_query;

    while (my $result = $blast_results->next_result())
    {
        my ($hit, $hsp);

        if (($hit = $result->next_hit()) && ($hsp = $hit->next_hsp()))
        {
            $hit_for_query{$result->query_name} = {
                'hit'    => $hit->name,
                'score'  => $hsp->bits,
                'evalue' => $hsp->evalue,
            };
        }
    }

    $blast_results->close();

    LogVerboseInfo("    ...done collecting best hits");
    return \%hit_for_query;
}


=pod

=head2 GetBBMH

B<Description>: returns an array of BBMH between species code A and species
code B.

B<ArgsCount>: 3

=over 4

=item $input_path: (string) (R)

Path where raw BBMH files can be found.

=item $variable_name2: ([variable nature]) ([requirements]) #+++

[variable description]. #+++

=back

B<Return>: ([return type]) #+++

[return description]. #+++

B<Example>:

    [example code] #+++

=cut

sub GetBBMH
{
    my ($input_path, $code_a, $code_b) = @_;

    # Collect best hits from BLAST results file A_vs_B
    my $best_hits_for_species_a = CollectBestHits($input_path . '/' . $code_a . '_vs_' . $code_b . '.m8');

    # Collect best hits from BLAST results file B_vs_A
    my $best_hits_for_species_b = CollectBestHits($input_path . '/' . $code_b . '_vs_' . $code_a . '.m8');

    # Collect best mutual hits between A and B
    my @bbmh_data;

BEST_HIT_SEARCH:
    while (my ($query_name, $hit_info) = each(%$best_hits_for_species_a))
    {
        my $hit_name = $hit_info->{'hit'};

        if ((!exists($best_hits_for_species_b->{$hit_name}))
            || ($best_hits_for_species_b->{$hit_name}->{'hit'} ne $query_name))
        {
            next BEST_HIT_SEARCH;
        }

        # From here we have our BBMH data
        if ($query_name && $hit_name)
        {
            push(@bbmh_data,
                [
                    $query_name,
                    $hit_name,
                    $hit_info->{'score'},
                    $hit_info->{'evalue'},
                ]
            );
        }
    }

    return \@bbmh_data;
}

=pod

=head2 ProcessSpeciesBBMH

B<Description>: Process raw BBMH files of given species and store preprocessing
result into one preprocessed ".bbmh" file per species.

B<ArgsCount>: 3

=over 4

=item $input_path: (string) (R)

Path of output BLAST files (raw BBMH files) without trailing slash.

=item $output_path: (string) (R)

Path where pre-processed BBMH files will be output without trailing slash.

=item $species_codes: (array ref) (R)

Array of species codes to process.

=back

B<Return>: nothing

=cut

sub ProcessSpeciesBBMH
{
    my ($input_path, $output_path, $species_codes, $against_species_codes) = @_;

    my $output_fh;

    $against_species_codes ||= $species_codes;
    LogInfo("Processing species:\n" . join(', ', @$species_codes) . "\nAgainst species: " . join(', ', @$against_species_codes));

    # Find BBMH in between all species
    my $species_counter = 0;
    foreach my $code_a (@$species_codes)
    {
        LogInfo("Processing species $code_a...");
        my $output_filename = "$output_path/$code_a.bbmh";
        if (open($output_fh, ">$output_filename"))
        {
CODE_B_PROCESSING:
            foreach my $code_b (@$against_species_codes)
            {
                # Do not process "A_vs_A" files
                if ($code_a eq $code_b)
                {
                    next CODE_B_PROCESSING;
                }

                LogInfo("  Storing BBMH between $code_a and $code_b...");
                if (my $bbmh_data = GetBBMH($input_path, $code_a, $code_b))
                {
                    foreach (@$bbmh_data)
                    {
                        print {$output_fh} join("\t", @{$_}) . "\n";
                    }
                }
                LogInfo("  ...done storing.");
            }
            close($output_fh);
        }
        else
        {
            LogError("Failed to create output BBMH file '$output_filename': $!");
        }
        LogInfo("...done with species $code_a (". ++$species_counter . "/" . scalar(@$species_codes) . ").");
    }
}




# Script options
#################

=pod

=head1 OPTIONS

preprocess_bbmh.pl [-help | -man]
preprocess_bbmh.pl [-debug] <-blast-dir <BBMH_PATH> > <-output-dir <OUT_PATH> > [-species <CODES>]

=head2 Parameters

=over 4

=item B<-help>:

Prints a brief help message and exits.

=item B<-man>:

Prints the manual page and exits.

=item B<-debug> (integer):

Executes the script in debug mode. The integer value is optional and is used
to set debug level (use 0 to force disable debug mode).
Default: 0 (not in debug mode).

=item B<-log>:

Enable logging.

=item B<-log-*> (any):

Logging arguments.

=item B<-blast-dir> (string):

Path to blast results directory (containing <speciesA>_vs_<speciesB> files).

=item B<-output-dir> (string):

Path to directory containing bbmh output files.

=item B<-species> (string):

A list of comma separated species codes or multiple "-species" parameters Only
these species will be processed.

=back

=cut


# CODE START
#############

# options processing
my ($man, $help, $debug, $no_prompt) = (0, 0, undef, undef);

# Script parameters
my ($input_path, $output_path, @filtered_species, $against_all);

# parse options and print usage if there is a syntax error.
GetOptions('help|?'       => \$help,
           'man'          => \$man,
           'debug:s'      => \$debug,
           'blast-dir=s'  => \$input_path,
           'output-dir=s' => \$output_path,
           'species=s'    => \@filtered_species,
           'vs-all'       => \$against_all,
           @Greenphyl::Log::LOG_GETOPT, # skip log parameters
) or pod2usage(1);
if ($help) {pod2usage('-verbose' => 1, '-exitval' => 0);}
if ($man) {pod2usage('-verbose' => 2, '-exitval' => 0);}

if (defined($debug))
{
    # just '-debug' ?
    if ($debug eq '')
    {
        $debug = 1;
    }
    $DEBUG = $debug;
}

if (!defined $input_path)
{
    pod2usage("-blast-dir must be specified!");
}
$input_path =~ s/\/+$//;

if (!-e $input_path)
{
    pod2usage("-blast-dir does not exist!");
}

if (!defined $output_path)
{
    pod2usage("-output-dir must be specified!");
}
$output_path =~ s/\/+$//;

if (!-e $output_path)
{
    pod2usage("-output-dir does not exist!");
}
elsif (!-w $output_path)
{
    pod2usage("-output-dir is not writeable!");
}


eval
{
    LogInfo("Processing BBMH directory: $input_path\nWill output to: $output_path");
    # Get species codes (all or filtered species)
    # Loop on *_vs_* blast results
    my $blast_dh;
    opendir($blast_dh, $input_path)
        or confess LogError("Cannot opendir '$input_path': $!");

    my %species_codes;
    foreach my $filename (readdir($blast_dh))
    {
        my ($species_code_a, $species_code_b) = ($filename =~ m/^($Greenphyl::Species::SPECIES_CODE_REGEXP)_vs_($Greenphyl::Species::SPECIES_CODE_REGEXP)\.m8$/);
        if ($species_code_a && $species_code_b)
        {
            $species_codes{$species_code_a} ||= {};
            $species_codes{$species_code_a}->{$species_code_b} = 1;
        }
        elsif (!-d "$input_path/$filename")
        {
            LogVerboseInfo("Skipping file '$filename'");
        }
    }
    closedir($blast_dh);

    my $species_codes_to_process = [sort(keys(%species_codes))];
    LogVerboseInfo("Available species BBMH: " . join(', ', @$species_codes_to_process));

    # filter species if needed
    if (@filtered_species)
    {
        @filtered_species = split(/,/,join(',',@filtered_species));
        # check requested species
        foreach (@filtered_species)
        {
            if (!exists($species_codes{$_}))
            {
                confess LogError("BBMH output files are missing for species '$_'!");
            }
        }

        $species_codes_to_process = \@filtered_species;
    }

    if ($against_all)
    {
        # selected species vs ALL
        ProcessSpeciesBBMH($input_path, $output_path, $species_codes_to_process, [sort(keys(%species_codes))]);
    }
    else
    {
        ProcessSpeciesBBMH($input_path, $output_path, $species_codes_to_process);
    }
};

# catch
HandleErrors();

exit(0);

# CODE END
###########


=pod

=head1 DIAGNOSTICS

=head1 AUTHORS

Mathieu ROUARD (Bioversity), m.rouard@cgiar.org

Valentin GUIGNON (Bioversity), v.guignon@cgiar.org

Matthieu CONTE (Syngenta)

Sebastien BRIOIS (Syngenta)

=head1 VERSION

Version 1.0.0

Date 30/10/2013

=head1 SEE ALSO

GreenPhyl documentation.

=cut
