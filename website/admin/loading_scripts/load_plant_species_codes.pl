#!/usr/bin/perl

=pod

=head1 NAME

load_plant_species_code.pl - Loads plant species codes into database

=head1 SYNOPSIS

    load_plant_species_code.pl

=head1 REQUIRES

Perl5

=head1 DESCRIPTION

This script loads plant species codes into the database. The following tables
are completed/updated:
-species
-taxonomy
-taxonomy_synonyms

=cut

use strict;
use warnings;

use FindBin;
use lib "$FindBin::Bin/../../lib";
use lib "$FindBin::Bin/../../local_lib";
use Carp qw (cluck confess croak);

use Greenphyl;
use Greenphyl::Log('nolog' => 1,);
use Greenphyl::Load::Taxonomy;
use Greenphyl::Tools::Taxonomy;

use Getopt::Long;
use Pod::Usage;

++$|; #no buffering




# Script global constants
##########################

=pod

=head1 CONSTANTS

B<$REV_STRING>: (string)

Current revision (auto-set by GIT).

B<$DEBUG>: (boolean)

When set to true, it enables debug mode.

=cut

# SVN management part
my $REV_STRING = '$id$';

our $DEBUG = 0;




# Script global variables
##########################

=pod

=head1 VARIABLES

B<$g_dbh>: (DBI::db)

Global database handler.

=cut

our $g_dbh = GetDatabaseHandler();




# Script global functions
##########################

=pod

=head1 FUNCTIONS

=head2 LoadSpeciesList

B<Description>: Loads species codes and their associated NCBI taxonomy ID from
UniProt species list text file.

B<ArgsCount>: 1

=over 4

=item $parameters: (hash ref) (R)

Hash of parameters. Supported parameters are:
-'no_prompt': if set to true, disables user prompting and uses default behavior.

=back

B<Return>: (hash ref)

Taxonomy hash which keys are species code and values are NCBI taxonomy IDs.

=cut

sub LoadSpeciesList
{
    my ($parameters) = @_;
    my $no_prompt = $parameters->{'no_prompt'};

    if ((!-e GP('SPECIES_CODE_PATH'))
        && (Prompt("'speclist.txt' is missing! Download it? [Y/N]", { 'default' => 'Y', 'constraint' => '^[ynYN]$', }, $no_prompt) =~ m/Y/i))
    {
        my $command = "wget -O '" . GP('SPECIES_CODE_PATH') . "' " . GetURL('species_codes_list');
        if (system($command))
        {
            confess LogError("Failed to download species list:\n$?");
        }
    }

    # hash keys=species code, values=taxonomy_id
    my $taxonomy_data = {};
    my $fh;
    if (open($fh, GP('SPECIES_CODE_PATH')))
    {
        my $current_code;
        while (my $line = <$fh>)
        {
            my ($current_code, $taxonomy_id, $data_type, $data);
            # check if we matched a code definition
            if ($line =~ m/^[A-Z0-9]{3,6}\s+/)
            {
                # match the full line
                ($current_code, $taxonomy_id, $data_type, $data) = ($line =~ m/
                    ^
                      ([A-Z][A-Z0-9]{2,5})\s+    # species code excluding "Virtual" codes starting with "9"
                      E\s+                       # only match eukaryota
                      (\d+):\s+                  # match taxonomy code
                      ([NCS])=(.+?)\s*           # match the data
                    $
                    /x);

                # add taxon
                if ($current_code)
                {
                    $taxonomy_data->{$current_code} = $taxonomy_id;
                }
            }
            elsif ($current_code)
            {
                # parse data
                ($data_type, $data) = ($line =~ m/^\s+([NCS])=(.+?)\s*$/);
                # check if not a data line
                if (!$data_type)
                {
                    # stop data for current code
                    $current_code = undef;
                }
            }

            # check parsing
            if (!$current_code)
            {
                LogDebug('Ignored line: ' . $line);
            }
        }
        close($fh);
    }
    else
    {
        confess "ERROR: unable to open speclist.txt file ('" . GP('SPECIES_CODE_PATH') . "')!\n$!\n";
    }

    return $taxonomy_data;
}


=pod2

=head2 StorePlantTaxonomy

B<Description>: Stores or update the given plant species (Viridiplantae) into
database.

B<ArgsCount>: 1-2

=over 4

=item $taxonomy_data: (hash ref) (R)

A taxonomy hash which keys are species code and values are NCBI taxonomy IDs.

=item $all_divisions: (bool) (O)

If set to true, all the given species are inserted, regardless their division.
Otherwise, only Viridiplantae are stored.

=back

B<Return>: (hash ref)

A hash containing inserted or updated species codes as keys and their associated
GreenPhyl taxonomy IDs as values.

=cut

sub StorePlantTaxonomy
{
    my ($taxonomy_data, $all_divisions) = @_;

    my $processed_species = {};

    my $processed_species_count = 0;
    my $total_species_count = scalar(keys(%$taxonomy_data));

    while (my ($species_code, $taxonomy_id) = each(%$taxonomy_data))
    {
        my $ncbi_taxonomy;
        my $species_taxonomy_filename = GP('SPECIES_PATH') . '/taxonomy/' . $species_code . '_taxonomy.xml';
        eval { $ncbi_taxonomy = GetTaxonomyData($taxonomy_id, $species_taxonomy_filename); };

        if (!$ncbi_taxonomy)
        {
            LogError("Failed to fetch taxonomy data for '$species_code' ($taxonomy_id)!");
        }
        elsif (!exists($ncbi_taxonomy->{'Taxon'}))
        {
            LogError("Invalid taxonomy data for '$species_code' ($taxonomy_id)!");
        }
          # make sure it is a plant (nb.: we don't use the 'Division' field because it's obsolete and not reliable!)
        elsif ($all_divisions || $ncbi_taxonomy->{'Taxon'}->[0]->{'Lineage'} =~ m/Viridiplantae/)
        {
            if ($ncbi_taxonomy->{'Taxon'}->[0]->{'TaxId'} != $taxonomy_id)
            {
                LogInfo("Got a synonym taxonomy identifier '$species_code': $taxonomy_id --> $ncbi_taxonomy->{'Taxon'}->[0]->{'TaxId'}");
                # change taxonomy identifier to use the right one
                $taxonomy_id = $ncbi_taxonomy->{'Taxon'}->[0]->{'TaxId'};
            }

            LogVerboseInfo("Storing species '$species_code' ($taxonomy_id)");
            StoreTaxonomyData($ncbi_taxonomy);
            my $common_name = '';
            if (exists($ncbi_taxonomy->{'Taxon'}->[0]->{'OtherNames'})
                && exists($ncbi_taxonomy->{'Taxon'}->[0]->{'OtherNames'}->{'CommonName'}))
            {
                if ('ARRAY' eq ref($ncbi_taxonomy->{'Taxon'}->[0]->{'OtherNames'}->{'CommonName'}))
                {
                    $common_name = $ncbi_taxonomy->{'Taxon'}->[0]->{'OtherNames'}->{'CommonName'}->[0];
                }
                else
                {
                    $common_name = $ncbi_taxonomy->{'Taxon'}->[0]->{'OtherNames'}->{'CommonName'};
                }
            }
            
            # compute species color
            my $species_color = ComputeSpeciesColor($species_code);

            # check taxonomy lineage insertion
            my $incomplete_species_id;
            my $sql_query = "SELECT id FROM taxonomy WHERE id = '$taxonomy_id';";
            my ($stored_taxonomy_id) = $g_dbh->selectrow_array($sql_query);
            if (!$stored_taxonomy_id)
            {
                LogError("Failed to store species '$species_code' ($taxonomy_id) taxonomy!");
            }
            else
            {
                # check if species already exists without taxonomy_id
                $sql_query = "SELECT id, taxonomy_id, color FROM species WHERE code = '$species_code';";
                my ($species_id_check, $taxonomy_id_check, $color_check) = $g_dbh->selectrow_array($sql_query);
                if (!$taxonomy_id_check || ($taxonomy_id_check != $stored_taxonomy_id))
                {
                    $incomplete_species_id = $species_id_check;
                }
                
                # check for color update (color not set or set to default)
                if (!$color_check || ($color_check eq '808080'))
                {
                    $sql_query = "UPDATE species SET color = '$species_color' WHERE code = '$species_code';";
                    $g_dbh->do($sql_query) or LogWarning("Failed to update species color ('$species_color') for species '$species_code'" . $g_dbh->errstr);
                }
            }

            if ($incomplete_species_id)
            {
                $sql_query = "UPDATE species SET taxonomy_id = '$stored_taxonomy_id' WHERE code = '$species_code';";
                $g_dbh->do($sql_query) or LogWarning("Failed to update taxonomy_id ('$stored_taxonomy_id') for species '$species_code'" . $g_dbh->errstr);
            }
            else
            {
                my $species_data = {
                    'code'          => $species_code,
                    'organism'      => $ncbi_taxonomy->{'Taxon'}->[0]->{'ScientificName'},
                    'common_name'   => $common_name,
                    'taxonomy_id'   => $stored_taxonomy_id,
                    'url_picture'   => '',
                    'display_order' => 0, # Not an "active" species.
                    'color'         => $species_color,
                };
                InsertIgnore('species', $species_data);
            }
            $processed_species->{$species_code} = $stored_taxonomy_id;
        }

        print GetProgress(
            {'label' => 'Processed species:', 'current' => ++$processed_species_count, 'total' => $total_species_count,},
            80,
            2, # seconds
        );
    }

    # print 100%
    print GetProgress(
        {'label' => 'Processed species:', 'current' => $total_species_count, 'total' => $total_species_count,},
        80,
        0, # seconds
    );

    return $processed_species;
}


=pod2

=head2 InsertNonUniprotSpecies

B<Description>: Insert the species code for non-UniProt species.

B<ArgsCount>: 0

B<Return>: (hash ref)

A hash containing inserted or updated species codes as keys and their associated
GreenPhyl taxonomy IDs as values.

=cut

sub InsertNonUniprotSpecies
{
    my $non_uniprot_hash = {};
    # check if species already exists
    my $sql_query = "SELECT id FROM species WHERE code = '" . GP('NONUNIPROT_SPECIES_CODE') . "';";
    my ($species_id) = $g_dbh->selectrow_array($sql_query);

    # skip if non-uniprot species already exists
    if (!$species_id)
    {
        my $species_data = {
            'code' => GP('NONUNIPROT_SPECIES_CODE'),
            'organism' => 'Non-UniProt species or species unknown',
            'common_name' => 'n/a',
            # 'taxonomy_id' => undef,
            'url_picture'   => '',
            'display_order' => 0, # Not an "active" species.
            'color'         => '404040',
        };
        InsertIgnore('species', $species_data);
        $non_uniprot_hash->{GP('NONUNIPROT_SPECIES_CODE')} = undef;
    }

    return $non_uniprot_hash;
}




# Script options
#################

=pod

=head1 OPTIONS

    load_plant_species_code.pl [-help | -man]

    load_plant_species_code.pl [-debug [debug_level]] [<-code <CODE> -taxid <TAXID>>]

=head2 Parameters

=over 4

=item B<-help>:

Prints a brief help message and exits.

=item B<-man>:

Prints the manual page and exits.

=item B<-debug> (integer):

Executes the script in debug mode. The integer value is optional and is used
to set debug level (use 0 to force disable debug mode).
Default: 0 (not in debug mode).

=item B<-code>:

Code of a species to add.

=item B<-taxid>:

Corresponding NCBI taxonomy ID of the given species.

=item B<-log>:

Enable logging.

=item B<-log-*> (any):

Logging arguments.

=back

=cut


# CODE START
#############

# options processing
my ($man, $help, $debug, $no_prompt) = (0, 0, undef, undef);
my ($species_code, $taxonomy_id);
# parse options and print usage if there is a syntax error.

GetOptions('help|?'     => \$help,
           'man'        => \$man,
           'debug:s'    => \$debug,
           'q|noprompt' => \$no_prompt,
           'code=s'     => \$species_code,
           't|taxid|taxonomy=i' => \$taxonomy_id,
           @Greenphyl::Log::LOG_GETOPT, # skip log parameters
) or pod2usage(1);
if ($help) {pod2usage('-verbose' => 1, '-exitval' => 0);}
if ($man) {pod2usage('-verbose' => 2, '-exitval' => 0);}

# change debug mode if requested/forced
if (defined($debug))
{
    # just '-debug' ?
    if ($debug eq '')
    {
        $debug = 1;
    }
    $DEBUG = $debug;
}

# start SQL transaction
if ($g_dbh->{'AutoCommit'})
{
    $g_dbh->begin_work() or croak $g_dbh->errstr;
}
# set warning state (not fatal)
$g_dbh->{'HandleError'} = undef;

eval
{
    my ($taxonomy_data, $all_divisions);
    if ($species_code && $taxonomy_id)
    {
        LogInfo("User-provided species: '$species_code' ($taxonomy_id)");
        $taxonomy_data = {$species_code => $taxonomy_id};
        $all_divisions = 1;
    }
    else
    {
        # Load and parse speclist.txt
        LogInfo("Loading species...");
        $taxonomy_data = LoadSpeciesList({'no_prompt' => $no_prompt});
        LogInfo("...done loading species (" . scalar(keys(%$taxonomy_data)) . " species)");
    }

    LogInfo("Storing species...");
    my $processed_species = StorePlantTaxonomy($taxonomy_data, $all_divisions);
    LogInfo("...done storing species");

    LogInfo("Adding non-UniProt species...");
    InsertNonUniprotSpecies();
    LogInfo("...done adding non-UniProt species");

    LogInfo("Added " . scalar(grep {$_} (values(%$processed_species))) . " species to database.");
    LogInfo("Failed species: " . join(',', grep {!$processed_species->{$_}} (keys(%$processed_species))));

    if (!$DEBUG)
    {SaveCurrentTransaction($g_dbh);}

};


# catch
HandleErrors();

exit(0);

# CODE END
###########


=pod

=head1 AUTHORS

Mathieu ROUARD (Bioversity), m.rouard@cgiar.org

Valentin GUIGNON (Bioversity), v.guignon@cgiar.org

Matthieu CONTE (Syngenta)

Sebastien BRIOIS (Syngenta)

=head1 VERSION

Version 1.3.0

Date 30/10/2019

=head1 SEE ALSO

GreenPhyl documentation.

=cut
