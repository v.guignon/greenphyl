#!/usr/bin/env perl

=pod

=head1 NAME

load_families.pl - Parse MCL file and save generated families into database

=head1 SYNOPSIS

    load_families.pl -mcl-file=tribemcl1.out -level=1 -min-sequences=3

=head1 REQUIRES

Perl5

=head1 DESCRIPTION

Create families from a given MCL file at a given level into database using
accession prefix 'LF'.

=cut

use strict;
use warnings;

use FindBin;
use lib "$FindBin::Bin/../../lib";
use lib "$FindBin::Bin/../../local_lib";
use Carp qw (cluck confess croak);

use Greenphyl;
use Greenphyl::Log('nolog' => 1,);
use Greenphyl::AbstractFamily;
use Greenphyl::Family;

use Getopt::Long;
use Pod::Usage;

++$|; #no buffering




# Script global constants
##########################

=pod

=head1 CONSTANTS

B<$REV_STRING>: (string)

Current revision (auto-set by GIT).

B<$DEBUG>: (boolean)

When set to true, it enables debug mode.

B<$FAMILY_LOADING_PREFIX>: (string)

Prefix used for accession of loaded families (LF) not yet processed by
annotation transfer

=cut

# SVN management part
my $REV_STRING = '$id$';

our $DEBUG = 0;

our $FAMILY_LOADING_PREFIX = 'LF';

our $SEQUENCE_SET_SIZE = 1000;


# Script global variables
##########################

=pod

=head1 VARIABLES

B<$g_dbh>: (DBI::db)

Global database handler.

=cut

my $g_dbh = GetDatabaseHandler();




# Script global functions
##########################

=pod

=head1 FUNCTIONS

=head2 [SubName] #+++

B<Description>: [function description]. #+++

B<ArgsCount>: [count of arguments] #+++

=over 4

=item $variable_name: ([variable nature]) ([requirements]) #+++ see below

#--- requirement can be:
#--- (R)=required,
#--- (O)=optional
#--- (U)=optional and must be undef if omitted
[variable description]. #+++

=item $variable_name2: ([variable nature]) ([requirements]) #+++

[variable description]. #+++

=back

B<Return>: ([return type]) #+++

[return description]. #+++

B<Example>:

    [example code] #+++

=cut

sub LoadFamilies
{
    my ($mcl_file_path, $level, $min_sequences) = @_;

    my $sql_query = "
        SELECT MAX(CONVERT(SUBSTR(accession, " . (1 + length($FAMILY_LOADING_PREFIX)). "), UNSIGNED INTEGER))
        FROM families
        WHERE accession LIKE '$FAMILY_LOADING_PREFIX%'
    ";
    LogDebug("SQL Query: $sql_query");
    my ($max_accession) = $g_dbh->selectrow_array($sql_query);
        # or LogError('Failed to get last available family accession from database! ' . $g_dbh->errstr);

    # Set next_accession
    my $next_accession = sprintf($FAMILY_LOADING_PREFIX . '%06s', ($max_accession ? ++$max_accession : 1));
    LogInfo("Starting family accession: '$next_accession'");

    my $mcl_fh;
    open($mcl_fh, $mcl_file_path)
        or confess LogError("Can't open file '$mcl_file_path': $!");

    LogInfo("Loading families for level $level...");
    my $stored_family_count = 0;
    my $mcl_line;
MCL_LINE:
    while ($mcl_line = <$mcl_fh>)
    {
        chomp($mcl_line);

        my @sequence_accessions = split(/\s+/, $mcl_line);

        # Skip small families
        if (scalar(@sequence_accessions) < $min_sequences)
        {
            next MCL_LINE;
        }

        LogInfo("Create family '$next_accession'");

        # Create all other families
        InsertIgnore(
            'families',
            {
                'accession' => $next_accession,
                'name'      => $Greenphyl::AbstractFamily::UNANNOTATED_CLUSTER_NAME,
                'level'     => $level,
            }
        );
        
        my $family = Greenphyl::Family->new(
            $g_dbh,
            {
                'selectors' => {
                    'accession' => $next_accession,
                },
            }
        );

        if (!$family)
        {
            LogError("Failed to get new family '$next_accession'!");
            next MCL_LINE;
        }
        ++$stored_family_count;

        # Link newly created family with all its sequences...
        my %unique_sequence_set = map {$_ => 1} @sequence_accessions;
        my $initial_sequence_count = scalar(keys(%unique_sequence_set));
        if ($initial_sequence_count != scalar(@sequence_accessions))
        {
            LogWarning('Some sequences appear more than once in the family MCL line!');
        }
        @sequence_accessions = keys(%unique_sequence_set);
        my $total_related_sequence = 0;
        LogInfo("Loading family $family (" . $family->id . ") ($initial_sequence_count sequences)...");
        while (my @sequence_accession_subset = splice(@sequence_accessions, 0, $SEQUENCE_SET_SIZE))
        {
            $sql_query = "
                SELECT id
                FROM sequences
                WHERE accession IN ('" . join("', '", @sequence_accession_subset) . "')
                ;
            ";
            LogDebug("SQL Query: $sql_query");
            my $sequence_ids = $g_dbh->selectcol_arrayref($sql_query)
                or confess LogError("Failed to get sequence IDs: " . $g_dbh->errstr);

            LogDebug("Got a set of " . scalar(@$sequence_ids) . " sequence IDs.");
            if (scalar(@sequence_accession_subset) != scalar(@$sequence_ids))
            {
                # Get missing accessions.
                $sql_query = "
                    SELECT accession
                    FROM sequences
                    WHERE accession IN ('" . join("', '", @sequence_accession_subset) . "')
                    ;
                ";
                LogDebug("SQL Query: $sql_query");
                my $loaded_accessions = $g_dbh->selectcol_arrayref($sql_query)
                    or confess LogError("Failed to get sequence accessions: " . $g_dbh->errstr);
                my %loaded_accessions = map {$_ => 1} @$loaded_accessions;
                my @missing_accesions = grep { !exists($loaded_accessions{$_}) } @sequence_accession_subset;
                confess LogError("Failed to get all sequence IDs (" . scalar(@$sequence_ids) . '/' .  scalar(@sequence_accession_subset) . ")!\nSequence accessions:\n'" . join("', '", @missing_accesions) . "'");
            }

            if ((scalar(@sequence_accession_subset) != $SEQUENCE_SET_SIZE) && scalar(@sequence_accessions))
            {
                confess LogError("Splice failed: (" . scalar(@sequence_accession_subset) . '/' .  $SEQUENCE_SET_SIZE . ")!\nSequence accessions:\n'" . join("', '", @sequence_accession_subset) . "'");
            }

            # Insert into families_sequences in batch...
            # -get value pairs
            my @families_sequences = map { '(' . $family->id . ', ' . $_ . ')' } @$sequence_ids;
            $sql_query = "
                INSERT IGNORE INTO families_sequences (family_id, sequence_id)
                VALUES
                " . join(', ', @families_sequences) . "
                ;
            ";
            # LogDebug("SQL Query: $sql_query");
            my $inserted_relationships = $g_dbh->do($sql_query)
                or confess LogError("Failed to add " . scalar(@$sequence_ids) . " sequences to family " . $family->id . "!\n" . $g_dbh->errstr);

            if (scalar(@$sequence_ids) != $inserted_relationships)
            {
                # confess
                LogWarning("Number of inserted sequence relationships ($inserted_relationships) does not correspond to number of sequences (" . scalar(@$sequence_ids) . ")!");
            }

            LogInfo("Added $inserted_relationships sequences (over " . @$sequence_ids . " ids, remaining " . @sequence_accessions . ") to family " . $family->id);
            $total_related_sequence += $inserted_relationships;
        }

        if ($total_related_sequence != $initial_sequence_count)
        {
            LogWarning(($initial_sequence_count - $total_related_sequence) . " sequences have not been added to family $next_accession (" . $family->id . ")!");
        }
        LogInfo("...done loading family $next_accession (" . $family->id . "): inserted $total_related_sequence sequences/$initial_sequence_count");

        # prepare next family accession for next family
        $next_accession++;
    }
    LogInfo("...done loading families: loaded $stored_family_count families");

    close($mcl_fh);

    # update statistics
    # $g_dbh->do('CALL countSequencesByFamiliesSpecies();');
    # $g_dbh->do('CALL countSequencesByFamiliesGenomes();');
    # $g_dbh->do('CALL updateFamiliesStats();');

}




# Script options
#################

=pod

=head1 OPTIONS

    load_families.pl [-help | -man]
    load_families.pl [-debug] <-mcl-file=<PATH> > <-level=<1,2,3,4> > <-min-sequences=<LEVEL> >

=head2 Parameters

=over 4

=item B<-help>:

Prints a brief help message and exits.

=item B<-man>:

Prints the manual page and exits.

=item B<-debug> (integer):

Executes the script in debug mode. The integer value is optional and is used
to set debug level (use 0 to force disable debug mode).
Default: 0 (not in debug mode).

=item B<-mcl-file> (string)

MCL output file.

=item B<-level> (integer)

Clustering level of families that will be created. Value in [1,2,3,4].

=item B<-min-sequences> (integer)

Minimum number of sequences required for creating a family.

=item B<-reset> (flag)

Reste families table index (id set to MAX(id) + 1).

=item B<-log>:

Enable logging.

=item B<-log-*> (any):

Logging arguments.

=back

=cut


# CODE START
#############

# options processing
my ($man, $help, $debug) = (0, 0, undef);
my ($mcl_file_path, $level, $min_sequences, $reset_index);

# parse options and print usage if there is a syntax error.
GetOptions('help|?'          => \$help,
           'man'             => \$man,
           'debug:s'         => \$debug,
           'mcl-file=s'      => \$mcl_file_path,
           'level=s',        => \$level,
           'min-sequences=s' => \$min_sequences,
           'reset'           => \$reset_index,
           @Greenphyl::Log::LOG_GETOPT, # skip log parameters
) or pod2usage(1);
if ($help) {pod2usage('-verbose' => 1, '-exitval' => 0);}
if ($man) {pod2usage('-verbose' => 2, '-exitval' => 0);}

# change debug mode if requested/forced
if (defined($debug))
{
    # just '-debug' ?
    if ($debug eq '')
    {
        $debug = 1;
    }
    $DEBUG = $debug;
}

if (!defined($mcl_file_path))
{
    pod2usage("-mcl-file must be specified!");
}

if (!-e $mcl_file_path)
{
    pod2usage("-mcl-file does not exist!");
}

if (!defined($level))
{
    pod2usage("-level must be specified!");
}

if (!$level || ($level !~ m/^[1-4]$/))
{
    pod2usage("-level must be a number between 1 and 4 (inclusive)!");
}

if (!defined($min_sequences))
{
    pod2usage("-min-sequences must be specified!");
}

if (!$min_sequences || ($min_sequences !~ m/^\d+$/))
{
    pod2usage("-min-sequences must be a positive number!");
}


eval
{
    # set warning state (not fatal)
    $g_dbh->{'HandleError'} = undef;

    # Starts with deleting all families with given level
    LogInfo("Deleting level $level families...");
    $g_dbh->do("DELETE FROM families WHERE families.level=$level;")
        or confess LogError("Failed to clear family level $level! " . $g_dbh->errstr);
    LogInfo("...done deleting families.");
    
    if ($reset_index)
    {
        # reset index
        LogInfo("Reset family index...");
        my ($family_index) = $g_dbh->selectrow_array('SELECT MAX(id) + 1 AS "ID" FROM families;');
        $family_index ||= 1;

        $g_dbh->do("ALTER TABLE families AUTO_INCREMENT = $family_index;")
            or confess LogError("Failed to reset family index! " . $g_dbh->errstr);
        LogInfo("...done reseting family index.");
    }

    # Load families from given MCL results file
    LoadFamilies($mcl_file_path, $level, $min_sequences);

    LogInfo("Work completed!");
};

# catch
HandleErrors();

exit(0);

# CODE END
###########


=pod

=head1 AUTHORS

Mathieu ROUARD (Bioversity), m.rouard@cgiar.org

Valentin GUIGNON (Bioversity), v.guignon@cgiar.org

Matthieu CONTE (Syngenta)

Sebastien BRIOIS (Syngenta)

=head1 VERSION

Version 1.0.0

Date 12/11/2013

=head1 SEE ALSO

GreenPhyl documentation, TribeMCL documentation.

=cut
