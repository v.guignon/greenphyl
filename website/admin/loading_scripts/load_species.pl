#!/usr/bin/env perl

=pod

=head1 NAME

load_species.pl - Load species data and sequences.

=head1 SYNOPSIS

    load_species.pl -fasta data/fasta/ARATH\
      -chromosome_count 5\
      -code ARATH\
      -color '0020ff'\
      -common_name 'Thale-cress'\
      -display_order 22\
      -genome_size 125\
      -organism 'Arabidopsis thaliana'\
      -sequence_count 0\
      -taxonomy_id 3702\
      -url_fasta 'ftp://ftp.arabidopsis.org/home/tair/Genes/TAIR10_genome_release/'\
      -url_institute 'http://www.arabidopsis.org/'\
      -url_picture '/img/species/arath.png'\
      -version 10\
      -version_notes 'representative gene models'\
      -log -log-logfile=arath.log -log-screen

=head1 REQUIRES

Perl5

=head1 DESCRIPTION

Loads a species into species table. This script adapts to the mysql table
specification (field names, required/optional fields).

Loads taxonomy table from given organism:
  1) looks for taxonomy ID at NCBI
  2) get taxonomy tree using ncbi webservice and insert data into taxonomy table

Load sequences table for created species if a FASTA file path is given.

Modified table(s):
-sequences
-species
-taxonomy

=cut

use strict;
use warnings;

use FindBin;
use lib "$FindBin::Bin/../../lib";
use lib "$FindBin::Bin/../../local_lib";
use Carp qw (cluck confess croak);

use Getopt::Long;
use Pod::Usage;
use LWP::UserAgent;
use XML::Simple qw(:strict);
use Bio::SeqIO;

use Greenphyl;
use Greenphyl::Log('nolog' => 1,);
use Greenphyl::Species;
use Greenphyl::Load::Taxonomy;

++$|; #no buffering




# Script global constants
##########################

=pod

=head1 CONSTANTS

B<$REV_STRING>: (string)

Current revision (auto-set by GIT).

B<$DEBUG>: (boolean)

When set to true, it enables debug mode.

=cut

# SVN management part
my $REV_STRING = '$id$';

our $DEBUG = 0;




# Script global variables
##########################

=pod

=head1 VARIABLES

B<$g_dbh>: (DBI::db)

Global database handler.

=cut

my $g_dbh = GetDatabaseHandler();




# Script global functions
##########################

=pod

=head1 FUNCTIONS

=head2 StoreSpecies

B<Description>: Stores a species into database. If the species already exists,
nothing will happen.

B<ArgsCount>: 1

=over 4

=item $species_args: (hash ref) (R)

hash of "species field" => "field value" for the species to store.

=back

B<Return>: (Greenphyl::Species)

Species loaded from database.

=cut

sub StoreSpecies
{
    my ($species_args) = @_;

    LogInfo('Storing species ' . $species_args->{'organism'} . '...');

    # Get NCBI taxonomy ID from submitted organism (eg: 'arabidopsis thaliana')
    $species_args->{'taxonomy_id'} ||= GetNCBITaxonomyID($species_args->{'organism'});

    # Try to load NBCI taxonomy and lineage for this taxonomy ID
    if (!$species_args->{'taxonomy_id'} || !StoreTaxonomyForSpecies($species_args->{'taxonomy_id'}, $species_args->{'code'}))
    {
        my $error_msg =
            "Could not insert species '"
            . (defined $species_args->{'organism'} ? $species_args->{'organism'} : 'undefined')
            . "' using this taxonomy ID : '"
            . (defined $species_args->{'taxonomy_id'} ? $species_args->{'taxonomy_id'} : 'undefined')
            . "'!"
        ;
        confess LogError($error_msg);
    }
    LogVerboseInfo("Got taxonomy ID " . $species_args->{'taxonomy_id'});

    my %fields = GetTableInfo('species');
    my %species_field_values;
    foreach my $field (keys(%fields))
    {
        if (exists($species_args->{$field}))
        {
            $species_field_values{$field} = $species_args->{$field};
        }
    }

    # Insert species into database
    my $num_rows = InsertUpdate('species', \%species_field_values);
    if (!$num_rows)
    {
        LogWarning("Species " . $species_args->{'code'} . " has not been inserted (maybe already in database).");
    }

    # Retrieve created species
    my $species = Greenphyl::Species->new(
        $g_dbh,
        {
            'selectors' => {
                'code' => $species_args->{'code'},
            },
        }
    );

    if (!$species)
    {
        confess LogError("Failed to retrieve inserted species!");
    }

    LogInfo('...done storing species.');

    return $species;
}


=pod

=head2 StoreSequences

B<Description>: stores species sequence from a FASTA file into database.

B<ArgsCount>: 1-2

=over 4

=item $species: (Greenphyl::Species) (R)

Species object.

=item $fasta_file_path: (string) (O)

Path to FASTA file containing species sequences.
Default: GP('BLAST_BANKS_PATH') . '/' . $species->code

=back

B<Return>: nothing

=cut

sub StoreSequences
{
    my ($species, $fasta_file_path) = @_;

    LogInfo("Storing species sequences...");

    # Load species sequences from FASTA file
    $fasta_file_path ||= GP('BLAST_BANKS_PATH') . '/' . $species->code;

    if (-e $fasta_file_path)
    {
        LogVerboseInfo("Loading sequences for species " . $species->code . " using FASTA file '$fasta_file_path'");
        my $input_fasta_fh = Bio::SeqIO->newFh(
            '-file'   => $fasta_file_path,
            '-format' => 'Fasta'
        );

        my $fasta_count = 0;
        my $insert_count = 0;
        while (<$input_fasta_fh>)
        {
            # fixes BioPerl name parsing bug: first word of piped annotation is
            # counted in sequence name when no space is used after sequence name
            my $accession = $_->display_id;
            my $annotation = $_->desc;
            if ($accession =~ m/^([^\|]+)\|(.*)$/)
            {
                $accession = $1;
                $annotation = $2 . ' ' . $annotation;
            }
            elsif ($accession =~ m/^([^\|]+),(.*)$/)
            {
                # coma case
                $accession = $1;
                $annotation = $2 . ' ' . $annotation;
            }

            # warn if unexpected characters are found
            if ($accession !~ m/^[\w\.]+$/)
            {
                LogWarning("Got an accession with unexpected characters: '$accession'");
            }

            # warn if the sequence doesn't seem correct
            my $polypeptide = $_->seq;
            $polypeptide =~ s/\*+$//g; # remove trailing stars
            $polypeptide = uc($polypeptide);
            if ($polypeptide !~ m/^M/)
            {
                LogWarning("Missing starting methionine for '$accession'");
            }

            if (8 >= length($polypeptide))
            {
                LogWarning("Sequence abnormaly short for '$accession'");
            }

            if ($polypeptide !~ m/^[GPAVLIMCFYWHKRQNEDSTX]+$/)
            {
                LogWarning("Sequence contains invalid characters for '$accession'");
            }

            # remove starting space and pipe from annotations
            $annotation =~ s/^\s*\|?\s*//;

            my $num_rows = InsertUpdate(
                'sequences',
                {
                    'species_id'  => $species->id,
                    'genome_id'   => undef,
                    'accession'   => $accession,
                    'polypeptide' => $polypeptide,
                    'length'      => length($polypeptide),
                    'annotation'  => $annotation,
                    'type'        => 'pan protein',
                }
            );

            if (!$num_rows)
            {
                LogVerboseInfo("Sequence ignored: $accession");
            }

            ++$fasta_count;
            $insert_count += $num_rows;
        }

        LogInfo("Sequence insertion: $insert_count inserted / $fasta_count in FASTA");

        # my $sql_query = "
        #     UPDATE species s
        #     SET s.sequence_count = (SELECT COUNT(q.id) FROM sequences q WHERE q.species_id = s.id)
        #     WHERE s.id = " . $species->id . ";";
        # $g_dbh->do($sql_query)
        #     or LogWarning("Failed to update species sequence count!\n" . $g_dbh->errstr);
    }
    else
    {
        LogWarning("FASTA File not found: '$fasta_file_path' - no sequences loaded for species " . $species->code . '(ID=' . $species->id . ')');
    }

    LogInfo("...done storing sequences.");
}


=pod

=head2 PrintUsage

B<Description>: Displays script usage and exists.

B<ArgsCount>: 1

=over 4

=item $script_options: (hash ref) (R)

Allowed script options.

=back

B<Return>: nothing

=cut

sub PrintUsage
{
    my $script_options = shift();

    print "Usage:\n"
        . "\tload_species.pl [--option=value]\n"
        . "Options:\n"
        . "\t--help\n"
        . "\t--fasta <FASTA_PATH>\n"
    ;

    # Print script options: <option_name> <required|optional> <description>
    # All coming from MySql table information
    foreach my $field (sort keys %$script_options)
    {
        printf(
            "\t--%-20s %8s   %-100s\n",
            $field,
            ($script_options->{$field}->{'required'} ? 'required' : 'optional'),
            $script_options->{$field}->{'comment'},
        );
    }
    exit(1);
}



# Script options
#################

=pod

=head1 OPTIONS

load_species.pl [-help | -man]
load_species.pl [-debug] <[species_table_fields <VALUE>] ...> [-fasta <FASTA_PATH>]

=head2 Parameters

=over 4

=item B<-help>:

Prints a brief help message and exits.

=item B<-man>:

Prints the manual page and exits.

=item B<-debug> (integer):

Executes the script in debug mode. The integer value is optional and is used
to set debug level (use 0 to force disable debug mode).
Default: 0 (not in debug mode).


=item B<species_table_fields> (any):

except 'id', any species table field can be specified in command line as
argument with a value. Some fields are required in the table constraints and
must be specified in command line (others are optional).

=item B<-log>:

Enable logging.

=item B<-log-*> (any):

Logging arguments.

=back

=cut


# CODE START
#############

# Submitted arguments will go in script_args
my %script_args;

# Get mysql info from table schema
my %table_info = GetTableInfo('species');
my %fields;
foreach my $field_info (values(%table_info))
{
    $fields{$field_info->{'name'}} = {
        'comment' => $field_info->{'comment'}
            . (
                defined($field_info->{'default'})
                ? " - default: '" . $field_info->{'default'} . "'"
                : ''
            ),
        'COLUMN_NAME' => $field_info->{'name'},
        'required' => $field_info->{'required'},
    };
}

# options processing...
# Build script options dynamically:
# help option + table fields as options
GetOptions(\%script_args,
    'help|?',
    'man',
    'force',
    'debug:s',
    'fasta=s',
    (map { $_ . '=s' } keys %fields),
    (grep {!ref} @Greenphyl::Log::LOG_GETOPT),
) or PrintUsage(\%fields);

# Display usage on demand
if ($script_args{'help'} || $script_args{'man'})
{
    PrintUsage(\%fields);

}
if (!$script_args{'force'})
{
    confess "This script has been deprecated. Use '-force' to run it anyway.\n";
}

# Display usage and exit if any required parameter is missing
foreach my $required_field (grep { $fields{$_}->{'required'} } keys(%fields))
{
    if (!defined($script_args{$required_field}))
    {
        LogError("'--$required_field' must be specified!");
        PrintUsage(\%fields);
    }
}

# Run script if all required parameters are provided
LogInfo("Inserting $script_args{'organism'}...");


# change debug mode if requested/forced
if ($script_args{'debug'})
{
    $DEBUG = 1;
}

eval
{
    # start SQL transaction
    if ($g_dbh->{'AutoCommit'})
    {
        $g_dbh->begin_work() or croak $g_dbh->errstr;
    }
    # set warning state (not fatal)
    $g_dbh->{'HandleError'} = undef;

    my $species = StoreSpecies(\%script_args);

    if ($script_args{'fasta'})
    {
        StoreSequences($species, $script_args{'fasta'});
    }

    if (!$DEBUG)
    {SaveCurrentTransaction($g_dbh);}
};

# catch
HandleErrors();


exit(0);

# CODE END
###########


=pod

=head1 AUTHORS

Mathieu ROUARD (Bioversity), m.rouard@cgiar.org

Valentin GUIGNON (Bioversity), v.guignon@cgiar.org

Matthieu CONTE (Syngenta)

Sebastien BRIOIS (Syngenta)

=head1 VERSION

Version 2.0.0

Date 11/10/2019

=head1 SEE ALSO

GreenPhyl documentation. Database 'species' table.

=cut
