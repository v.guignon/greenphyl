#!/usr/bin/env perl

=pod

=head1 NAME

load_species_genomes.pl - Load species genomes data and sequences.

=head1 SYNOPSIS

    load_species_genomes.pl -species-data=ORYSA.yml -log -log-logfile=orysa.log -log-screen

=head1 REQUIRES

Perl5

=head1 DESCRIPTION

Loads species genomes into species and genomes tables. It supports both single
genome species and multi-genome species (pan genes). It loads the associated
sequences and their relationships (in the case of pan genes).

Loads taxonomy table from given organism:
  1) looks for taxonomy ID at NCBI
  2) get taxonomy tree using ncbi webservice and insert data into taxonomy table

Species data file format (YAML):

Example 1: AMBTC.yml
----------
species:
  code: AMBTC
  organism: Amborella trichopoda
  common_name:
  taxonomy_id: 13333
  url_picture: /img/species/ambtc.png
  display_order: 17
  color: 00d0d0

genomes:
  -AMBTC_v1.0

AMBTC_v1.0:
  fasta: /gs7k1/projects/greenphyl/data/v5/genomes/AMBTC
  chromosome_count: 26
  ploidy: 2
  genome_size: 706
  complete_busco: 94.7
  fragment_busco: 3.6
  missing_busco: 1.7
  url_institute: http://www.amborella.org/
  url_fasta: ftp://amborella-project.huck.psu.edu/Public/Amborella
  version: 1.0
  version_notes:
----------

Example 2: MUSAC.yml
----------
species:
  code: MUSAC
  organism: Musa acuminata
  common_name: Banana
  taxonomy_id: 4641
  url_picture: /img/species/musac.png
  display_order: 8
  color: c09000

genomes:
  -MUSAC_Macma2
  -MUSAC_Macze1
  -MUSAC_Macba2
  -MUSAC_Macbu1

pangene:
  path: /gs7k1/projects/greenphyl/data/v5/pangenomes/MUSAC/

MUSAC_Macma2:
  fasta: /gs7k1/projects/greenphyl/data/v5/genome_sources/MUSAC/MUSAC_malaccensis.faa
  chromosome_count: 22
  ploidy: 2
  genome_size: 650
  complete_busco: 98.5
  fragment_busco: 0.9
  missing_busco: 0.6
  url_institute: https://banana-genome-hub.southgreen.fr/
  url_fasta: https://banana-genome-hub.southgreen.fr/download
  version: 2.0
  version_notes:

MUSAC_Macze1:
  fasta: /gs7k1/projects/greenphyl/data/v5/genome_sources/MUSAC/MUSAC_zebrina.faa
  chromosome_count: 22
  ploidy: 2
  genome_size: 650
  complete_busco: 60.3
  fragment_busco: 23.2
  missing_busco: 16.5
  url_institute: https://banana-genome-hub.southgreen.fr/
  url_fasta: https://banana-genome-hub.southgreen.fr/node/263184
  version: 1.0
  version_notes:

MUSAC_Macba2:
  fasta: /gs7k1/projects/greenphyl/data/v5/genome_sources/MUSAC/ MUSAC_banksii.faa
  chromosome_count: 22
  ploidy: 2
  genome_size: 650
  complete_busco: 71.2
  fragment_busco: 8.4
  missing_busco: 20.4
  url_institute: https://banana-genome-hub.southgreen.fr/
  url_fasta: https://banana-genome-hub.southgreen.fr/download
  version: 2.0
  version_notes:

MUSAC_Macbu1:
  fasta: /gs7k1/projects/greenphyl/data/v5/genome_sources/MUSAC/MUSAC_burmannicoides.faa
  chromosome_count: 22
  ploidy: 2
  genome_size: 650
  complete_busco: 71.9
  fragment_busco: 14.5
  missing_busco: 13.6
  url_institute: https://banana-genome-hub.southgreen.fr/
  url_fasta: https://banana-genome-hub.southgreen.fr/node/263185
  version: 1.0
  version_notes:

----------


Modified table(s):
-sequences
-families
-families_sequences
-species
-genomes
-taxonomy

=cut

use strict;
use warnings;

use FindBin;
use lib "$FindBin::Bin/../../lib";
use lib "$FindBin::Bin/../../local_lib";
use Carp qw (cluck confess croak);

use Getopt::Long;
use Pod::Usage;
use LWP::UserAgent;
use XML::Simple qw(:strict);
use Bio::SeqIO;
use YAML::XS 'LoadFile';

use Greenphyl;
use Greenphyl::Log('nolog' => 1,);
use Greenphyl::Species;
use Greenphyl::Genome;
use Greenphyl::Sequence;
use Greenphyl::Family;
use Greenphyl::Taxonomy;
use Greenphyl::Load::Taxonomy;

++$|; #no buffering




# Script global constants
##########################

=pod

=head1 CONSTANTS

B<$REV_STRING>: (string)

Current revision (auto-set by GIT).

B<$DEBUG>: (boolean)

When set to true, it enables debug mode.

=cut

# SVN management part
my $REV_STRING = '$id$';

our $DEBUG = 0;




# Script global variables
##########################

=pod

=head1 VARIABLES

B<$g_dbh>: (DBI::db)

Global database handler.

=cut

my $g_dbh = GetDatabaseHandler();




# Script global functions
##########################

=pod

=head1 FUNCTIONS

=head2 GetAliases

B<Description>: Returns transcript (na) to protein (aa) aliases.

B<ArgsCount>: 2

=over 4

=item $species_data: (hash ref) (R)

hash of species data to store.

=item $pangene: (hash) (R)

Pangene parameters.

=back

B<Return>: (hash ref)

Keys are nucliec acid names and values are amino-acid names.

=cut

sub GetAliases
{
    my ($species, $pangene) = @_;

    my $pangene_path = $pangene->{'path'};
    $pangene_path =~ s/\/+$//;

    my $aliases = {};
    my $sequence_aliases_file = $pangene_path . '/' . $species->code . '_alias.txt';
    if (-e $sequence_aliases_file)
    {
        my $fh;
        open($fh, $sequence_aliases_file)
            or confess "Can't open file '$sequence_aliases_file': $!";
        while (my $line = <$fh>)
        {
            chomp($line);
            my ($aa_seqname, $na_seqname) = split(/[ \t]+/, $line);
            if (exists($aliases->{$na_seqname}))
            {
                LogWarning("Alias already in use for '$na_seqname' (=$aa_seqname vs $aliases->{$na_seqname})");
            }
            $aliases->{$na_seqname} = $aa_seqname;
        }
        close($fh);
    }
    else
    {
        confess LogError("Sequence alias file not found: '$sequence_aliases_file' - no sequences loaded for species " . $species->code . '(ID=' . $species->id . ')');
    }
    return $aliases;
}


=pod

=head2 StoreSpecies

B<Description>: Stores a species into database. If the species already exists,
nothing will happen.

B<ArgsCount>: 1

=over 4

=item $species_data: (hash ref) (R)

hash of species data to store.

=back

B<Return>: (Greenphyl::Species)

Species loaded from database.

=cut

sub StoreSpecies
{
    my ($species_data) = @_;

    # Get NCBI taxonomy ID from submitted organism (eg: 'arabidopsis thaliana')
    $species_data->{'taxonomy_id'} ||= GetNCBITaxonomyID($species_data->{'organism'});

    # Try to load NBCI taxonomy and lineage for this taxonomy ID
    if (!$species_data->{'taxonomy_id'} || !StoreTaxonomyForSpecies($species_data->{'taxonomy_id'}, $species_data->{'code'}))
    {
        my $error_msg =
            "Could not insert species '"
            . (defined $species_data->{'organism'} ? $species_data->{'organism'} : 'undefined')
            . "' using this taxonomy ID : '"
            . (defined $species_data->{'taxonomy_id'} ? $species_data->{'taxonomy_id'} : 'undefined')
            . "'!"
        ;
        confess LogError($error_msg);
    }
    LogVerboseInfo("Got taxonomy ID " . $species_data->{'taxonomy_id'});

    my $taxonomy = Greenphyl::Taxonomy->new(
        $g_dbh,
        {
            'selectors' => {
                'id' => $species_data->{'taxonomy_id'},
            },
        }
    );
    if (!$taxonomy)
    {
        confess LogError("Invalid taxonomy data. Check the given species taxonomy id and the associated XML file (" .  GP('SPECIES_PATH') . '/taxonomy/' . $species_data->{'code'} . '_taxonomy.xml' . "). You may need to delete the XML file to reload taxonomy data.");
    }

    my %fields = GetTableInfo('species');
    my %species_field_values;
    foreach my $field (keys(%fields))
    {
        if (exists($species_data->{$field}))
        {
            $species_field_values{$field} = $species_data->{$field};
        }
    }

    # Insert species into database
    my $num_rows = InsertUpdate('species', \%species_field_values);
    if (!$num_rows)
    {
        LogWarning("Species " . $species_data->{'code'} . " has not been inserted (maybe already in database).");
    }

    # Retrieve created species
    my $species = Greenphyl::Species->new(
        $g_dbh,
        {
            'selectors' => {
                'code' => $species_data->{'code'},
            },
        }
    );

    if (!$species)
    {
        confess LogError("Failed to retrieve inserted species!");
    }

    LogInfo('...done storing species.');

    return $species;
}


=pod

=head2 StoreGenome

B<Description>: Stores a genome into database. If the genome already exists,
nothing will happen.

B<ArgsCount>: 1

=over 4

=item $species: (Greenphyl::Species) (R)

A species object of the genome.

=item $genome_data: (hash ref) (R)

hash of genome data to store.

=back

B<Return>: (Greenphyl::Genome)

Genome loaded from database.

=cut

sub StoreGenome
{
    my ($species, $genome_data) = @_;

    my %fields = GetTableInfo('genomes');
    my %genomes_field_values;
    foreach my $field (keys(%fields))
    {
        if (exists($genome_data->{$field}))
        {
            $genomes_field_values{$field} = $genome_data->{$field};
        }
    }
    $genomes_field_values{'species_id'} = $species->id;
    $genomes_field_values{'representative'} = 0;

    # Insert genome into database
    my $num_rows = InsertUpdate('genomes', \%genomes_field_values);
    if (!$num_rows)
    {
        LogWarning("Genome " . $genome_data->{'accession'} . " has not been inserted (maybe already in database).");
    }

    # Retrieve created genome
    my $genome = Greenphyl::Genome->new(
        $g_dbh,
        {
            'selectors' => {
                'accession' => $genome_data->{'accession'},
            },
        }
    );

    if (!$genome)
    {
        confess LogError("Failed to retrieve inserted genome!");
    }

    LogInfo('...done storing genome.');

    return $genome;
}


=pod

=head2 StoreGenomeSequences

B<Description>: stores species sequence from a FASTA file into database.

B<ArgsCount>: 1-2

=over 4

=item $species: (Greenphyl::Species) (R)

Species object.

=item $fasta_file_path: (string) (O)

Path to FASTA file containing species sequences.
Default: GP('BLAST_BANKS_PATH') . '/' . $species->code

=back

B<Return>: nothing

=cut

sub StoreGenomeSequences
{
    my ($species, $genome, $genome_data) = @_;

    LogInfo("Storing genome sequences for $genome ($species)...");
    my $fasta_file_path = $genome_data->{'fasta'};

    my $sequence_accessions_to_ids = {};

    # Load species sequences from FASTA file
    if (-e $fasta_file_path)
    {
        LogVerboseInfo("Loading sequences for species " . $species->code . " using FASTA file '$fasta_file_path'");
        my $input_fasta_fh = Bio::SeqIO->newFh(
            '-file'   => $fasta_file_path,
            '-format' => 'Fasta'
        );

        my $fasta_count = 0;
        my $insert_count = 0;
        while (<$input_fasta_fh>)
        {
            # fixes BioPerl name parsing bug: first word of piped annotation is
            # counted in sequence name when no space is used after sequence name
            my $accession = $_->display_id;
            my $annotation = $_->desc;
            if ($accession =~ m/^([^\|]+)\|(.*)$/)
            {
                $accession = $1;
                $annotation = $2 . ' ' . $annotation;
            }
            elsif ($accession =~ m/^([^\|]+),(.*)$/)
            {
                # coma case
                $accession = $1;
                $annotation = $2 . ' ' . $annotation;
            }

            # warn if unexpected characters are found
            if ($accession !~ m/^[\w\.\-]+$/)
            {
                LogWarning("Got an accession with unexpected characters: '$accession'");
            }

            # warn if the sequence doesn't seem correct
            my $polypeptide = $_->seq;
            $polypeptide =~ s/\*+$//g; # remove trailing stars
            $polypeptide = uc($polypeptide);
            if ($polypeptide !~ m/^M/i)
            {
                LogWarning("Missing starting methionine for '$accession'");
            }

            if (8 >= length($polypeptide))
            {
                LogWarning("Sequence abnormaly short for '$accession'");
            }

            if ($polypeptide !~ m/^[GPAVLIMCFYWHKRQNEDSTX]+$/)
            {
                LogWarning("Sequence contains invalid characters for '$accession'");
            }

            # remove starting space and pipe from annotations
            $annotation =~ s/^\s*\|?\s*//;

            my $num_rows = InsertUpdate(
                'sequences',
                {
                    'species_id'  => $species->id,
                    'genome_id'   => $genome->id,
                    'accession'   => $accession,
                    'polypeptide' => $polypeptide,
                    'length'      => length($polypeptide),
                    'annotation'  => $annotation,
                    'type'        => 'protein',
                }
            );

            if (!$num_rows)
            {
                LogVerboseInfo("Sequence ignored: $accession");
            }

            # Retrieve created or update sequence.
            my $sequence = [Greenphyl::Sequence->new(
                $g_dbh,
                {
                    'selectors' => {
                        'accession' => $accession,
                    },
                }
            )];
            if (!@$sequence)
            {
                confess LogError("Failed to retrieve inserted/updated sequence '$accession' ($genome, $species).");
            }
            elsif (1 != @$sequence)
            {
                confess LogError("Duplicated sequence accession: '$accession' ($genome, $species).");
            }
            $sequence = $sequence->[0];

            $sequence_accessions_to_ids->{$accession} = $sequence->id;

            ++$fasta_count;
            $insert_count += $num_rows;
        }

        LogInfo("Sequence insertion: $insert_count inserted / $fasta_count in FASTA");

        my $sql_query = "
            UPDATE genomes g
            SET g.sequence_count = (SELECT COUNT(q.id) FROM sequences q WHERE q.genome_id = g.id)
            WHERE g.id = " . $genome->id . ";";
        $g_dbh->do($sql_query)
            or LogWarning("Failed to update genome sequence count!\n" . $g_dbh->errstr);
    }
    else
    {
        confess LogError("FASTA File not found: '$fasta_file_path' - no sequences loaded for species " . $species->code . '(ID=' . $species->id . ')');
    }

    LogInfo("...done storing sequences.");
    return $sequence_accessions_to_ids;
}


=pod

=head2 StorePangene

B<Description>: stores species sequence from a FASTA file into database.

B<ArgsCount>: 1-2

=over 4

=item $species: (Greenphyl::Species) (R)

Species object.

=item $pangene: (hash) (R)

Pangene parameters.

=back

B<Return>: nothing

=cut

sub StorePangene
{
    my ($species, $genomes, $pangene, $sequence_accession_mapping, $aliases) = @_;

    LogInfo("Creating a pangene genome for species $species...");
    my %genomes_field_values = (
        'accession'      => $species->code,
        'species_id'     => $species->id,
        'sequence_count' => 0,
        'representative' => 1,
        'url_institute'  => '',
        'url_fasta'      => '',
    );
    if (!InsertUpdate('genomes', \%genomes_field_values))
    {
        LogWarning("Genome " . $species->{'code'} . " has not been inserted (maybe already in database).");
    }
    # Retrieve created genome
    my $genome = Greenphyl::Genome->new(
        $g_dbh,
        {
            'selectors' => {
                'accession' => $species->code,
            },
        }
    );

    if (!$genome)
    {
        confess LogError("Failed to retrieve inserted pangene genome for $species!");
    }
    
    LogInfo("Storing pangene sequences for species $species...");

    my $pangene_path = $pangene->{'path'};
    $pangene_path =~ s/\/+$//;

    my $pangene_to_sequence_accessions = {};
    my $pangene_genomes = {};
    my $representatives = {};

    # Get relationships from "CODE_seq_map.txt" .
    my $pangene_relationships = $pangene_path . '/' . $species->code . '_seq_map.txt';
    if (-e $pangene_relationships)
    {
        my $fh;
        open($fh, $pangene_relationships)
            or confess "Can't open file '$pangene_relationships': $!";
        while (my $line = <$fh>)
        {
            chomp($line);
            my ($seq_accession, @seqs) = split("\t", $line);
            # Convert transcript name used by get_homologues-est into protein names.
            if (exists($aliases->{$seq_accession}))
            {
                $seq_accession = $aliases->{$seq_accession};
            }
            my $last_seq = $seqs[$#seqs];
            # Skip orphans.
            if (('orphan' ne $last_seq) && ('out' ne $last_seq))
            {
                # Convert transcript name used by get_homologues-est into protein names.
                if (exists($aliases->{$last_seq}))
                {
                    $last_seq = $aliases->{$last_seq};
                }
                # Keep track of associations.
                $pangene_to_sequence_accessions->{$last_seq} ||= [];
                push(@{$pangene_to_sequence_accessions->{$last_seq}}, $seq_accession);
                # And genomes involved.
                $pangene_genomes->{$last_seq}->{$sequence_accession_mapping->{$seq_accession}->{'genome'}} = 1;
                if (1 < @seqs)
                {
                    # More than one sequence.
                    my $representative_seq = $seqs[0];
                    if ('matched' ne $representative_seq)
                    {
                        # we have a representative.
                        # Convert transcript name used by get_homologues-est into protein names.
                        if (exists($aliases->{$representative_seq}))
                        {
                            $representative_seq = $aliases->{$representative_seq};
                        }                        
                        $representatives->{$representative_seq} = 1;
                    }
                }
            }
        }
        close($fh);
    }
    else
    {
        confess LogError("Sequence mapping file not found: '$pangene_relationships' - no sequences loaded for species " . $species->code . '(ID=' . $species->id . ')');
    }

    my $pan_accessions_to_ids = {};

    # Get complete FASTA (which includes "orphan" sequences).
    my $pangene_fasta = $pangene_path . '/' . $species->code . '_complete.fasta';
    if (-e $pangene_fasta)
    {
        my $input_fasta_fh = Bio::SeqIO->newFh(
            '-file'   => $pangene_fasta,
            '-format' => 'Fasta'
        );

        my $fasta_count = 0;
        my $insert_count = 0;
        while (<$input_fasta_fh>)
        {
            # fixes BioPerl name parsing bug: first word of piped annotation is
            # counted in sequence name when no space is used after sequence name
            my $accession = $_->display_id;
            my $annotation = $_->desc;
            if ($accession =~ m/^([^\|]+)\|(.*)$/)
            {
                $accession = $1;
                $annotation = $2 . ' ' . $annotation;
            }
            elsif ($accession =~ m/^([^\|]+),(.*)$/)
            {
                # coma case
                $accession = $1;
                $annotation = $2 . ' ' . $annotation;
            }

            # warn if unexpected characters are found
            if ($accession !~ m/^[\w\.\-]+$/)
            {
                LogWarning("Got an accession with unexpected characters: '$accession'");
            }

            # warn if the sequence doesn't seem correct
            my $polypeptide = $_->seq;
            $polypeptide =~ s/\*+$//g; # remove trailing stars
            $polypeptide = uc($polypeptide);
            if ($polypeptide !~ m/^M/i)
            {
                LogWarning("Missing starting methionine for '$accession'");
            }

            if (8 >= length($polypeptide))
            {
                LogWarning("Sequence abnormaly short for '$accession'");
            }

            if ($polypeptide !~ m/^[GPAVLIMCFYWHKRQNEDSTX]+$/)
            {
                LogWarning("Sequence contains invalid characters for '$accession'");
            }

            # remove starting space and pipe from annotations
            $annotation =~ s/^\s*\|?\s*//;

            my $num_rows = InsertUpdate(
                'sequences',
                {
                    'species_id'     => $species->id,
                    'genome_id'      => $genome->id,
                    'accession'      => $accession,
                    'polypeptide'    => $polypeptide,
                    'representative' => 1,
                    'length'         => length($polypeptide),
                    'annotation'     => $annotation,
                    'type'           => 'pan protein',
                }
            );

            if (!$num_rows)
            {
                LogVerboseInfo("Sequence ignored: $accession");
            }

            # Retrieve created or update sequence.
            my $sequence = [Greenphyl::Sequence->new(
                $g_dbh,
                {
                    'selectors' => {
                        'accession' => $accession,
                    },
                }
            )];
            if (!@$sequence)
            {
                confess LogError("Failed to retrieve inserted/updated pangene sequence '$accession' ($species).");
            }
            elsif (1 != @$sequence)
            {
                confess LogError("Duplicated pangene sequence accession: '$accession' ($species).");
            }
            $sequence = $sequence->[0];

            $pan_accessions_to_ids->{$accession} = $sequence->id;

            ++$fasta_count;
            $insert_count += $num_rows;
        }

        LogInfo("Sequence insertion: $insert_count inserted / $fasta_count in FASTA");

        my $sql_query = "
            UPDATE genomes g
            SET g.sequence_count = (SELECT COUNT(q.id) FROM sequences q WHERE q.genome_id = g.id)
            WHERE g.id = " . $genome->id . ";";
        $g_dbh->do($sql_query)
            or LogWarning("Failed to update genome sequence count!\n" . $g_dbh->errstr);
    }
    else
    {
        confess LogError("FASTA File not found: '$pangene_fasta' - no pangene sequence loaded for species " . $species->code . '(ID=' . $species->id . ')');
    }

    # Store relationships.
    my $genome_count = scalar(keys(%$genomes));
    my $soft_core_limit = int($genome_count * 0.95 + 0.5);
    #  2 genomes: core=2, dispensable=1
    #  3 genomes: core=3, dispensable=2, specific=1
    #  4 genomes: core=4, dispensable=3-2, specific=1
    # 10 genomes: core=10, dispensable=9-2, specific=1
    # 11 genomes: core=11, soft-core=10, dispensable=9-2, specific=1
    # 20 genomes: core=20, soft-core=19, dispensable=18-2, specific=1
    # 50 genomes: core=50, soft-core=48, dispensable=47-2, specific=1
    # 53 genomes: core=53, soft-core=50, dispensable=49-2, specific=1
    
    LogInfo(
        sprintf(
            "Counting %d genomes for %s.\n  core: %d genomes\n  soft-core: <%d and >=%d\n  dispensable: <%d\n  specific: 1",
            $genome_count,
            $species->code,
            $genome_count,
            $genome_count,
            $soft_core_limit,
            $soft_core_limit
        )
    );
    foreach my $pan_accession (keys(%$pan_accessions_to_ids))
    {
        # Count genomes in the pangene.
        my $pangene_genome_count = scalar(keys(%{$pangene_genomes->{$pan_accession}}));
        my ($family_accession) = ($pan_accession =~ m/(\d+)$/);
        $family_accession = $species->code . $family_accession;

        # Possible types: 'core','soft-core','dispensable','specific'.
        # core=100%, soft-core=95-99%, dispensable=up-to-94%, specific=single genome.
        my $family_type = '';
        if ($pangene_genome_count >= $genome_count)
        {
            $family_type = 'core';
        }
        elsif ($pangene_genome_count >= $soft_core_limit)
        {
            $family_type = 'soft-core';
        }
        elsif ((1 == $pangene_genome_count) && (2 < $genome_count))
        {
            $family_type = 'specific';
        }
        else
        {
            $family_type = 'dispensable';
        }
        # Create a new family for the gene associations.
        LogInfo("Create cluster '$family_accession' ($pangene_genome_count genomes, $family_type)");

        # Create all other families
        InsertUpdate(
            'families',
            {
                'accession' => $family_accession,
                'name'      => $pan_accession . ' pangene cluster',
                'level'     => undef,
                'type'      => $family_type,
            }
        );

        my $family = Greenphyl::Family->new(
            $g_dbh,
            {
                'selectors' => {
                    'accession' => $family_accession,
                },
            }
        );

        if (!$family)
        {
            confess LogError("Failed to get new cluster '$family_accession'!");
        }

        # Clean relationships.
        my $sql_query = "
            DELETE FROM families_sequences WHERE family_id = " . $family->id . ";
        ";
        # LogDebug("SQL Query: $sql_query");
        $g_dbh->do($sql_query)
            or confess LogError("Failed to clean cluster relationships for cluster " . $family->id . "!\n" . $g_dbh->errstr);

        # Add pangene sequence.
        my $link_type = (1 == $pangene_genome_count) ? 'representative' : 'consensus';
        $sql_query = "
            INSERT INTO families_sequences (family_id, sequence_id, type)
            VALUES ("
            . $family->id
            . ", "
            . $pan_accessions_to_ids->{$pan_accession}
            . ", '$link_type')
            ;
        ";
        # LogDebug("SQL Query: $sql_query");
        $g_dbh->do($sql_query)
            or confess LogError("Failed to add pangene sequence $pan_accession to cluster " . $family->id . "!\n" . $g_dbh->errstr);

        # Add genome sequences.
        my @families_sequences = ();
        foreach my $sequence_accession (@{$pangene_to_sequence_accessions->{$pan_accession}})
        {
            # Can be: 'participating','representative','consensus'.
            my $relationship_type = 'participating';
            if (exists($representatives->{$sequence_accession}))
            {
                $relationship_type = 'representative';
            }

            push(
              @families_sequences,
              '(' . $family->id . ', '
              . $sequence_accession_mapping->{$sequence_accession}->{'id'} . ',\''
              . $relationship_type . '\')'
            );
        }
        while (@families_sequences)
        {
            my @families_sequences_slice = splice(@families_sequences, 0, 100);
            $sql_query = "
                INSERT INTO families_sequences (family_id, sequence_id, type)
                VALUES
                " . join(', ', @families_sequences_slice) . "
                ;
            ";
            # LogDebug("SQL Query: $sql_query");
            my $inserted_relationships = $g_dbh->do($sql_query)
                or confess LogError("Failed to add genome sequences for $pan_accession to cluster " . $family->id . "!\n" . $g_dbh->errstr . "\nSQL: " . $sql_query);
        }
    }

    LogInfo("...done storing pangenes.");
}




# Script options
#################

=pod

=head1 OPTIONS

load_species.pl [-help | -man]
load_species.pl [-debug] [-species-data <SPECIES_DATA_FILE_YML>]

=head2 Parameters

=over 4

=item B<-help>:

Prints a brief help message and exits.

=item B<-man>:

Prints the manual page and exits.

=item B<-debug> (integer):

Executes the script in debug mode. The integer value is optional and is used
to set debug level (use 0 to force disable debug mode).
Default: 0 (not in debug mode).


=item B<-species-data> (string):

File path to the YAML species data file. Supported file content described at the
begining of this script.

=item B<-log>:

Enable logging.

=item B<-log-*> (any):

Logging arguments.

=back

=cut


# CODE START
#############

my ($man, $help, $debug) = (0, 0, undef);
my ($species_data_file_path);
# options processing...
GetOptions('help|?'          => \$help,
           'man'             => \$man,
           'debug:s'         => \$debug,
           'species-data=s'  => \$species_data_file_path,
           @Greenphyl::Log::LOG_GETOPT, # skip log parameters
) or pod2usage(1);
if ($help) {pod2usage('-verbose' => 1, '-exitval' => 0);}
if ($man) {pod2usage('-verbose' => 2, '-exitval' => 0);}


if (!defined($species_data_file_path))
{
    pod2usage("-species_data_file_path must be specified!");
}

if (!-e $species_data_file_path)
{
    pod2usage("-species_data_file_path does not exist!");
}

if (!-s $species_data_file_path)
{
    pod2usage("-species_data_file_path is empty!");
}

my $species_data = LoadFile($species_data_file_path);

if (!$species_data
    || 'HASH' ne ref($species_data)
    || !exists($species_data->{'species'}))
{
    confess LogError("Failed to load species data! Invalid YAML?");
}

# Run script if all required parameters are provided
LogInfo("Working on $species_data->{'species'}->{'organism'} ($species_data->{'species'}->{'code'})...");

eval
{
    # start SQL transaction
    if ($g_dbh->{'AutoCommit'})
    {
        $g_dbh->begin_work() or croak $g_dbh->errstr;
    }
    # set warning state (not fatal)
    $g_dbh->{'HandleError'} = undef;

    my $sequence_accession_mapping = {};
    my $species = StoreSpecies($species_data->{'species'});
    my $genomes = {};
    my $aliases = {};
    if (exists($species_data->{'pangene'}))
    {
        $aliases = GetAliases($species, $species_data->{'pangene'});
    }

    if ('ARRAY' ne ref($species_data->{'genomes'}))
    {
        confess LogError("A format error has been found in $species_data_file_path in the 'genomes' section! A list of genomes was expected but not found. Maybe you forgot to add a space between the dash and the genome names (ie. '-ARATH_Araport11' instead of '- ARATH_Araport11')?");
    }

    foreach my $genome_name (@{$species_data->{'genomes'}})
    {
        LogInfo("Working on genome '$genome_name'...");
        if (!exists($species_data->{$genome_name}))
        {
            confess LogError("Genome '$genome_name' details not found in the species data file!");
        }
        $species_data->{$genome_name}->{'accession'} = $genome_name;
        my $genome = StoreGenome($species, $species_data->{$genome_name});
        $genomes->{$genome_name} = $genome;
        my $sequence_ids = StoreGenomeSequences($species, $genome, $species_data->{$genome_name});
        foreach my $sequence_accession (keys(%$sequence_ids))
        {
            $sequence_accession_mapping->{$sequence_accession} = {
                'id' => $sequence_ids->{$sequence_accession},
                'genome' => $genome_name,
            };
        }
    }

    if (exists($species_data->{'pangene'}))
    {
        StorePangene($species, $genomes, $species_data->{'pangene'}, $sequence_accession_mapping, $aliases);
    }
    elsif (1 == @{$species_data->{'genomes'}})
    {
        # Mark genome as species representatives.
        $genomes->{$species_data->{'genomes'}->[0]}->representative(1);
        $genomes->{$species_data->{'genomes'}->[0]}->save();
        # Mark genome sequences as species representatives.
        my $sql_query = "
            UPDATE sequences s
            SET s.representative = 1
            WHERE s.genome_id = " . $genomes->{$species_data->{'genomes'}->[0]}->id . ";";
        $g_dbh->do($sql_query)
            or LogWarning("Failed to mark genome '" . $species_data->{'genomes'}->[0] . "' sequences as representative for " . $species_data->{'code'} . "!\n" . $g_dbh->errstr);
    }
    else
    {
        LogWarning("Species " . $species_data->{'code'} . " has more than 1 genome but has no pangene!");
    }

    if (!$DEBUG)
    {
        SaveCurrentTransaction($g_dbh);
    }
    else
    {
        LogDebug('Rollback transaction (debug mode).');
    }
};

# catch
HandleErrors();
LogInfo("...Done!");


exit(0);

# CODE END
###########


=pod

=head1 AUTHORS

Mathieu ROUARD (Bioversity), m.rouard@cgiar.org

Valentin GUIGNON (Bioversity), v.guignon@cgiar.org

Matthieu CONTE (Syngenta)

=head1 VERSION

Version 2.0.0

Date 11/10/2019

=head1 SEE ALSO

GreenPhyl documentation. Database 'species' table.

=cut
