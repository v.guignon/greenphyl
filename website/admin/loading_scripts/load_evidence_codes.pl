#!/usr/bin/env perl

=pod

=head1 NAME

load_evidence_codes.pl - Fills evidence_codes table

=head1 SYNOPSIS

    load_evidence_codes.pl

=head1 REQUIRES

Perl5

=head1 DESCRIPTION

Add evidence codes entries to evidence_codes table.

This script updates the following table:
-evidence_codes

=cut

use strict;
use warnings;

use FindBin;
use lib "$FindBin::Bin/../../lib";
use lib "$FindBin::Bin/../../local_lib";
use Carp qw (cluck confess croak);

use Getopt::Long;
use Pod::Usage;

use Greenphyl;
use Greenphyl::Log('nolog' => 1,);

++$|; #no buffering




# Script global constants
##########################

=pod

=head1 CONSTANTS

B<$REV_STRING>: (string)

Current revision (auto-set by GIT).

B<$DEBUG>: (boolean)

When set to true, it enables debug mode.

B<$EVIDENCE_CODES_INSERT_QUERY>: (string)

Evidence codes insertion query.

=cut

# SVN management part
my $REV_STRING = '$id$';

our $DEBUG = 0;
our $EVIDENCE_CODES_INSERT_QUERY = q{
    INSERT INTO evidence_codes (code, category, description) VALUES
        ('EXP', 'experimental',           'Inferred from Experiment')
      , ('IDA', 'experimental',           'Inferred from Direct Assay')
      , ('IPI', 'experimental',           'Inferred from Physical Interaction')
      , ('IMP', 'experimental',           'Inferred from Mutant Phenotype')
      , ('IGI', 'experimental',           'Inferred from Genetic Interaction')
      , ('IEP', 'experimental',           'Inferred from Expression Pattern')
      , ('ISS', 'computational analysis', 'Inferred from Sequence or Structural Similarity')
      , ('ISO', 'computational analysis', 'Inferred from Sequence Orthology')
      , ('ISA', 'computational analysis', 'Inferred from Sequence Alignment')
      , ('ISM', 'computational analysis', 'Inferred from Sequence Model')
      , ('IGC', 'computational analysis', 'Inferred from Genomic Context')
      , ('IBA', 'computational analysis', 'Inferred from Biological aspect of Ancestor')
      , ('IBD', 'computational analysis', 'Inferred from Biological aspect of Descendant')
      , ('IKR', 'computational analysis', 'Inferred from Key Residues')
      , ('IRD', 'computational analysis', 'Inferred from Rapid Divergence')
      , ('RCA', 'computational analysis', 'inferred from Reviewed Computational Analysis')
      , ('TAS', 'author statements',      'Traceable Author Statement')
      , ('NAS', 'author statements',      'Non-traceable Author Statement')
      , ('IC',  'curatorial statements',  'Inferred by Curator')
      , ('ND',  'curatorial statements',  'No biological Data available')
      , ('IEA', 'automatically-assigned statements', 'Inferred from Electronic Annotation')
      , ('NR',  'obsolete',               'Not Recorded')
    ON DUPLICATE KEY UPDATE
      code = VALUES(code),
      category = VALUES(category),
      description = VALUES(description)
    ;
};


# Script global variables
##########################

=pod

=head1 VARIABLES

B<$g_dbh>: (DBI::db)

Global database handler.

=cut

my $g_dbh = GetDatabaseHandler();




# Script options
#################

=pod

=head1 OPTIONS

    load_evidence_codes.pl [-help|-man]
    load_evidence_codes.pl [-debug] [db_index=<db_index>]

=head2 Parameters

=over 4

=item B<-help>:

Prints a brief help message and exits.

=item B<-man>:

Prints the manual page and exits.

=item B<-debug> (integer):

Executes the script in debug mode.

=item B<db_index> (integer):

Index of the database to use (see Greenphyl::Config).
Default: 0

=back

=cut


# CODE START
#############

# for regression tests
if ($ENV{'TESTING'} && $ENV{'TESTING_NO_RUN'})
{return 1;}

# options processing
my ($man, $help, $debug) = (0, 0, undef, undef);
# parse options and print usage if there is a syntax error.
GetOptions('help|?'           => \$help,
           'man'              => \$man,
           'debug:s'          => \$debug,
           @Greenphyl::Log::LOG_GETOPT, # skip log parameters
) or pod2usage(1);
if ($help) {pod2usage('-verbose' => 1, '-exitval' => 0);}
if ($man) {pod2usage('-verbose' => 2, '-exitval' => 0);}

# change debug mode if requested/forced
if (defined($debug))
{
    # just '-debug' ?
    if ($debug eq '')
    {
        $debug = 1;
    }
    $DEBUG = $debug;
}

eval
{
    LogInfo("Loading/updating evidence codes...");

#    # start SQL transaction
#    if ($g_dbh->{'AutoCommit'})
#    {
#        $g_dbh->begin_work() or croak $g_dbh->errstr;
#    }

    LogDebug("Running in DEBUG mode, no database commit will be issued!");

    my $sql_query = $EVIDENCE_CODES_INSERT_QUERY;
    LogDebug("SQL QUERY: $sql_query");
    if (!$g_dbh->do($sql_query))
    {
        LogWarning("Failed to insert/update external database references!" . $g_dbh->errstr);
    }

#    if (!$DEBUG)
#    {SaveCurrentTransaction($g_dbh);}

    LogInfo("Done.");
};

HandleErrors();

exit(0);

# CODE END
###########


=pod

=head1 AUTHORS

Mathieu ROUARD (Bioversity), m.rouard@cgiar.org

Valentin GUIGNON (Bioversity), v.guignon@cgiar.org

Matthieu CONTE (Syngenta)

Sebastien BRIOIS (Syngenta)

=head1 VERSION

Version 1.0.0

Date 18/11/2013

=head1 SEE ALSO

GreenPhyl documentation.

=cut
