#!/usr/bin/env perl

=pod

=head1 NAME

load_interpro.pl - Loads sequences InterPro domains mapping into database

=head1 SYNOPSIS

    load_interpro.pl -ipr-result-dir /data/ipr/ARATH   -log -log-logfile=ipr_arath.log -log-screen

=head1 REQUIRES

Perl5

=head1 DESCRIPTION

Loads sequences InterPro domains mapping into database.

Used table:
-sequences

Modified tables:
-ipr
-ipr_sequences

=cut

use strict;
use warnings;

use FindBin;
use lib "$FindBin::Bin/../../lib";
use lib "$FindBin::Bin/../../local_lib";
use Carp qw (cluck confess croak);

use Greenphyl;
use Greenphyl::Log('nolog' => 1,);

use Getopt::Long;
use Pod::Usage;

++$|; #no buffering




# Script global constants
##########################

=pod

=head1 CONSTANTS

B<$REV_STRING>: (string)

Current revision (auto-set by GIT).

B<$DEBUG>: (boolean)

When set to true, it enables debug mode.

=cut

# SVN management part
my $REV_STRING = '$id$';

our $DEBUG = 0;




# Script global variables
##########################

=pod

=head1 VARIABLES

B<$g_dbh>: (DBI::db)

Global database handler.

=cut

my $g_dbh = GetDatabaseHandler();




# Script global functions
##########################

=pod

=head1 FUNCTIONS

=head2 GetInterProEntries

B<Description>: Get interpro entry list from EBI FTP.
File content looks like:

    Active_site
    IPR000126 Peptidase S1B, active site
    IPR000138 Hydroxymethylglutaryl-CoA lyase, active site
    IPR000169 Cysteine peptidase, cysteine active site
    IPR000180 Peptidase M19, renal dipeptidase, active site

B<ArgsCount>: 0

B<Return>: (hash ref)

A hash which keys are IPR codes and values are hash:
{
 'desc' => IPR description
 'type' => IPR type
}

=cut

sub GetInterProEntries
{
    my $interpro_file = GP('DATA_PATH') . '/ipr/interpro_entry_list';

    if (!-e $interpro_file)
    {
        LogInfo("Download ftp://ftp.ebi.ac.uk/pub/databases/interpro/entry.list");
        my $command_line = "wget --quiet ftp://ftp.ebi.ac.uk/pub/databases/interpro/entry.list -O $interpro_file 2>&1";
        LogVerboseInfo("Command: $command_line");
        my $command_output = `$command_line`;
        LogVerboseInfo("Output:\n$command_output\n----------");
    }

    my $ipr_fh;
    open($ipr_fh, "<$interpro_file")
        or confess LogError("Can't open '$interpro_file': $!");

    my %interpro_entries;
    #First line contains headers.
    my $line = <$ipr_fh>;
    if ($line !~ m/^ENTRY_AC[ \t]+ENTRY_TYPE[ \t]+ENTRY_NAME$/)
    {
        confess LogError('Unsupported interpro_entry_list file format!');
    }

    while ($line = <$ipr_fh>)
    {
        chomp($line);

        my ($ipr_code, $ipr_type, $ipr_desc) = split(/[ \t]+/, $line, 3);

        $interpro_entries{$ipr_code} = {
            'desc' => $ipr_desc,
            'type' => $ipr_type,
        };
    }

    close($ipr_fh);

    return \%interpro_entries;
}


=pod

=head2 LoadInterProScanResults

B<Description>: [function description]. #+++

B<ArgsCount>: 2

=over 4

=item $ipr_result_file: (string) (R)

IPR scan result file path.

=item $ipr_entries: (hash ref) (R)

A hash like the one returned by GetInterProEntries().

=back

B<Return>: (integer)

Number of IPR inserted into database.

=cut

sub LoadInterProScanResults
{
    my ($ipr_result_file, $ipr_entries) = @_;

    # open file
    my ($ipr_result_fh);
    open($ipr_result_fh, '<', $ipr_result_file)
        or confess LogError("Can't open '$ipr_result_file': $!\n");

    my %id_for_sequence;
    my @ipr_sequences; # to be inserted into 'ipr_sequences' table

IPR_RESULT_LINE:
    while (<$ipr_result_fh>)
    {
        chomp;
        my @line = split( /\t/ );

        # Read sequence accessions (separated with '|' when multiple on one line)
        my @sequences = split(/\|/, $line[0]);
        foreach my $sequence (@sequences)
        {
            $id_for_sequence{$sequence} ||= $g_dbh->selectrow_array(
                "SELECT id FROM sequences WHERE accession = ?;", undef, $sequence
            );
            if (!defined($id_for_sequence{$sequence}))
            {
                LogWarning("Skipped sequence $sequence. Not found in database.");
                delete($id_for_sequence{$sequence});
            }
        }

        # Exclude sequences that don't have an ID in the database
        @sequences = grep { defined $id_for_sequence{$_} } @sequences;
        if (scalar(@sequences) == 0)
        {
            next IPR_RESULT_LINE;
        }

        # Go to next line if no IPR code can be read
        my $ipr_code = $line[11];

        if (!$ipr_code)
        {
            next IPR_RESULT_LINE;
        }
        
        if ($ipr_code !~ m/^IPR/)
        {
            confess LogError("Invalid IPR code found: '$ipr_code'.");
        }

        if (!exists($ipr_entries->{$ipr_code}))
        {
            LogWarning("Skipped '$ipr_code'. No entry found in interpro DB.");
            next IPR_RESULT_LINE;
        }

        # Load and/or retrieve ipr ID from database.
        $ipr_entries->{$ipr_code}->{'id'} ||= StoreIPR({
            'code'        => $ipr_code,
            'description' => $ipr_entries->{$ipr_code}->{'desc'},
            'type'        => $ipr_entries->{$ipr_code}->{'type'},
            'version'     => '78', #+FIXME: get version from interpro webservice
        });

        # Check IPR was found.
        if (!$ipr_entries->{$ipr_code}->{'id'})
        {
            confess LogError("Failed to fetch IPR $ipr_code from datatbase.");
        }

        # Link sequences to their ipr
        foreach my $sequence (@sequences)
        {
            push(@ipr_sequences, [
                $id_for_sequence{$sequence},     # sequence_id
                $ipr_entries->{$ipr_code}->{'id'}, # ipr_id
                $line[6],                        # ipr start for this sequence
                $line[7],                        # ipr stop for this sequence
                $line[8],                        # ipr score for this sequence
            ]);
        }
    }

    close($ipr_result_fh);
    return StoreIPRSequences(\@ipr_sequences);
}


=pod

=head2 StoreIPR

B<Description>: Store an IPR into database. If the IPR already exists, no
insertion or update will occure and the current id will be returned.

B<ArgsCount>: 1

=over 4

=item $data: (hash ref) (R)

A hash where keys are ipr table column names and values are the row values for
the IPR to insert.

=back

B<Return>: (integer)

IPR database identifier (ipr.id) of the given IPR (from $data).

=cut

sub StoreIPR
{
    my ($data) = @_;

    # Remove key/value pairs from $data if value is undefined
    delete(@$data{ grep { !defined($data->{$_}) } keys(%$data) });

    my $field_keys   = join(',', keys(%$data));
    my $field_values = join(',', map {'?'} values(%$data));
    my $update_fileds = join(',', map { "$_ = ?" } keys(%$data));

    # my $sql_query = "INSERT IGNORE INTO ipr ($field_keys) VALUES ($field_values);";
    my $sql_query = "INSERT INTO ipr ($field_keys) VALUES ($field_values) ON DUPLICATE KEY UPDATE $update_fileds;";

    LogDebug('SQL Query: ' . $sql_query . "\nBindings: " . join(', ', values(%$data)));
    
    $g_dbh->do($sql_query, undef, values(%$data), values(%$data));

    my $ipr_id = $g_dbh->selectrow_array(
        "SELECT id FROM ipr WHERE " . join(" AND ", map { "$_ = ?" } keys(%$data)),
        undef,
        values(%$data),
    );

    return $ipr_id;
}


=pod

=head2 StoreIPRSequences

B<Description>: Store IPR-sequence mapping into database.

B<ArgsCount>: 1

=over 4

=item $ipr_sequences: (array ref) (R)

Array of IPR-sequence mapping which are sequences of (sequence_id, ipr_id,
start, end, score) aggregated in a same array.

=back

B<Return>: (integer)

Number of relationships stored.

=cut

sub StoreIPRSequences
{
    my ($ipr_sequences) = @_;

    if (scalar(@$ipr_sequences) == 0)
    {
        return 0;
    }

    # Work by batch of 100 IPR.
    my @ipr_sequences_batches = @$ipr_sequences;
    my $insert_count = 0;
    while (@ipr_sequences_batches)
    {
        my @ipr_sequences_batches_slice = splice(@ipr_sequences_batches, 0, 100);
        my $sql_query = '
            INSERT IGNORE INTO ipr_sequences (sequence_id,ipr_id,start,end,score)
            VALUES '
            . join(',', map {'("' . join('","', @{$_}) . '")'} @ipr_sequences_batches_slice) . '
            ;'
        ;
        $insert_count += $g_dbh->do($sql_query);
    }

    return $insert_count;
}



# Script options
#################

=pod

=head1 OPTIONS

        load_interpro.pl [-help | -man]
        
        load_interpro.pl [-debug] <-ipr-result-dir <IPR_PATH>>
    
=head2 Parameters

=over 4

=item B<-help>:

Prints a brief help message and exits.

=item B<-man>:

Prints the manual page and exits.

=item B<-debug> (integer):

Executes the script in debug mode. The integer value is optional and is used
to set debug level (use 0 to force disable debug mode).
Default: 0 (not in debug mode).


=item B<ipr-result-dir> (string):

Path to either a directory containing IPR result files or containing species
directories of IP result files.

=item B<-log>:

Enable logging.

=item B<-log-*> (any):

Logging arguments.

=back

=cut


# CODE START
#############

# options processing
my ($man, $help, $debug) = (0, 0, undef);
my $ipr_result_dir;
# parse options and print usage if there is a syntax error.
GetOptions('help|?'             => \$help,
           'man'                => \$man,
           'debug:s'            => \$debug,
           'ipr-result-dir|i=s' => \$ipr_result_dir,
           @Greenphyl::Log::LOG_GETOPT, # skip log parameters
) or pod2usage(1);
if ($help) {pod2usage('-verbose' => 1, '-exitval' => 0);}
if ($man) {pod2usage('-verbose' => 2, '-exitval' => 0);}

if (!defined($ipr_result_dir))
{
    pod2usage("-ipr-result-dir must be specified!");
}

if (!-d $ipr_result_dir)
{
    pod2usage("-ipr-result-dir must be a directory!");
}


# change debug mode if requested/forced
if (defined($debug))
{
    # just '-debug' ?
    if ($debug eq '')
    {
        $debug = 1;
    }
    $DEBUG = $debug;
}

eval
{
    # Retrieve intrepro entries from EBI FTP
    LogInfo("Fetching interpro entries... ");
    my $ipr_entries = GetInterProEntries();
    LogInfo(scalar(keys %$ipr_entries) . " interpro entries loaded.");

    # Loop on files in given directory and its sub-directories (up to 1 level)
    my $ipr_dh;
    opendir($ipr_dh, $ipr_result_dir)
        or confess LogError("Can't opendir '$ipr_result_dir': $!\n");

    foreach (grep {$_ =~ m/\.tsv$/} readdir($ipr_dh))
    {
        my $path = "$ipr_result_dir/$_";
        my $rows_inserted = 0;

        LogInfo("Loading $path...");

        if (-f $path)
        {
            $rows_inserted += LoadInterProScanResults($path, $ipr_entries);
        }
        elsif (-d $path)
        {
            my $species_dh;
            opendir($species_dh, $path)
                or confess LogError("Can't opendir '$path': $!\n");

            foreach my $file (grep{ -f } map { "$path/$_" } readdir($species_dh))
            {
                $rows_inserted += LoadInterProScanResults($file, $ipr_entries);
            }
            closedir($species_dh);
        }
        LogInfo("$rows_inserted IPR inserted from '$path'");
    }

    closedir($ipr_dh);
};

# catch
HandleErrors();

exit(0);

# CODE END
###########


=pod

=head1 AUTHORS

Mathieu ROUARD (Bioversity), m.rouard@cgiar.org

Valentin GUIGNON (Bioversity), v.guignon@cgiar.org

Matthieu CONTE (Syngenta)

Sebastien BRIOIS (Syngenta)

=head1 VERSION

Version 1.0.0

Date 06/11/2013

=head1 SEE ALSO

GreenPhyl documentation.

=cut
