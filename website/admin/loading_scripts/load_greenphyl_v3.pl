#!/usr/bin/env perl

=pod

=head1 NAME

load_greenphyl_v3.pl - Loads GreenPhyl v3 sequences and families into v4 DB

=head1 SYNOPSIS

    load_greenphyl_v3.pl

=head1 REQUIRES

Perl5, GreenPhyl v3 database

=head1 DESCRIPTION

Loads GreenPhyl v3 sequences and families into v4 DB as custom sequences and
families. Extra family-data is stored as a frozen Perl structure into the
custom_families.data field. See Greenphyl::CustomFamily for details.

=cut

use strict;
use warnings;

use FindBin;
use lib "$FindBin::Bin/../../lib";
use lib "$FindBin::Bin/../../local_lib";
use Carp qw (cluck confess croak);

use Greenphyl;
use Greenphyl::Log('nolog' => 1,);
use Greenphyl::User;
use Greenphyl::Species;
use Greenphyl::Sequence;
use Greenphyl::Tools::Sequences;
use Greenphyl::AbstractFamily;
use Greenphyl::ExtV3Sequence;
use Greenphyl::ExtV3Family;

use Getopt::Long;
use Pod::Usage;

use Storable qw(nfreeze);

++$|; #no buffering




# Script global constants
##########################

=pod

=head1 CONSTANTS

B<$REV_STRING>: (string)

Current revision (auto-set by GIT).

B<$DEBUG>: (boolean)

When set to true, it enables debug mode.

B<$CONSTANT_NAME>: ([constant nature]) #+++

[constant description and use]. #+++

=cut

# SVN management part
my $REV_STRING = '$id$';

our $DEBUG = 0;
our $SEQUENCE_BATCH_SIZE = 5000;
our $FAMILY_BATCH_SIZE   = 1000;

our %TYPE_CONVERSION_HASH = (
    '0' => 'N/A',
    '1' => 'superfamily',
    '2' => 'family',
    '3' => 'subfamily',
    '4' => 'group',
);

our %INFERENCE_CONVERSION_HASH = (
    ''                 => '',
	' '                => '',
	'IEA:INTERPRO'     => 'IEA:InterPro',
	'IEA:KEGG'         => 'IEA:KEGG',
	'IEA:MEROPS'       => 'IEA:MEROPS',
	'IEA:PIRSF'        => 'IEA:PIRSF',
	'IEA:TF'           => 'IEA:TF',
	'IEA:UNIPROTKB'    => 'IEA:UniProtKB',
	'IEA:UNIPROTKB-KW' => 'IEA:UniProtKB-KW',
	'N/A'              => '',
	'OTHER'            => 'Other',
	'PUBMED'           => 'PubMed',
	'TAIR/TIGR'        => 'TAIR/TIGR',
);

our $FAMILY_SUFFIX = '_V3';



# Script global variables
##########################

=pod

=head1 VARIABLES

B<[g_variable_name]>: ([variable nature]) #+++

[variable description, use and default value]. #+++
Default: [variable default value if one] #+++

#--- Example:
#--- B<$g_output_method>: (integer)
#---
#--- used to store current output method;
#--- 0=raw text (default), 1=image, 2=html.
#---
#---     ...
#---
#--- my $g_output_method = 0;
#---

=cut

our $g_dbh = GetDatabaseHandler();
our $g_v3_user;




# Script global functions
##########################

=pod

=head1 FUNCTIONS

=head2 FindAssociatedSequence

B<Description>: [function description]. #+++

B<ArgsCount>: [count of arguments] #+++

=over 4

=item $variable_name: ([variable nature]) ([requirements]) #+++ see below

#--- requirement can be:
#--- (R)=required,
#--- (O)=optional
#--- (U)=optional and must be undef if omitted
[variable description]. #+++

=item $variable_name2: ([variable nature]) ([requirements]) #+++

[variable description]. #+++

=back

B<Return>: ([return type]) #+++

[return description]. #+++

B<Example>:

    [example code] #+++

=cut

sub FindAssociatedSequence
{
    my ($v3_sequence, $v4_species) = @_;

    my $sql_query;
    # -try by accession
    my $v4_sequence = Greenphyl::Sequence->new($g_dbh, {'selectors' => {'accession' => ['LIKE', $v3_sequence->seq_textid],}});

    if (!$v4_sequence)
    {
        # -try using previous accessions
        LogDebug(" trying with previous accessions");
        $sql_query = "SELECT sequence_id FROM sequence_transfer_history WHERE old_accession LIKE ?;";
        my ($sequence_id) = $g_dbh->selectrow_array($sql_query, undef, $v3_sequence->seq_textid);
        # confess LogError("Failed to query sequence_transfer_history table for v3 sequence '$v3_sequence'!\n" . $g_dbh->errstr);
        if ($sequence_id)
        {
            $v4_sequence = Greenphyl::Sequence->new($g_dbh,
                {'selectors' => {'id'  => $sequence_id,},},
            );
        }
    }

    if (!$v4_sequence)
    {
        # -try using synonyms
        LogDebug(" trying with synonyms");
        $sql_query = "SELECT sequence_id FROM sequence_synonyms WHERE synonym LIKE ?";
        # also use v3 synonyms
        if (@{$v3_sequence->synonyms})
        {
            $sql_query .= ' OR synonym LIKE ?' x scalar(@{$v3_sequence->synonyms});
        }
        my ($sequence_id) = $g_dbh->selectrow_array($sql_query, undef, $v3_sequence->seq_textid, @{$v3_sequence->synonyms});
        # confess LogError("Failed to query sequence_synonyms table for v3 sequence '$v3_sequence'!\n" . $g_dbh->errstr);
        if ($sequence_id)
        {
            $v4_sequence = Greenphyl::Sequence->new($g_dbh,
                {'selectors' => {'id'  => $sequence_id,},},
            );
        }
    }

    if (!$v4_sequence)
    {
        # -try by content and species
        LogDebug(" trying with sequence content");
        $v4_sequence = Greenphyl::Sequence->new($g_dbh,
            {
                'selectors' =>
                {
                    'species_id'  => $v4_species->id,
                    'polypeptide' => $v3_sequence->sequence,
                },
            },
        );
    }

    if (!$v4_sequence)
    {
        LogInfo("No associated sequence found for v3 sequence " . $v3_sequence);
    }
    
    return $v4_sequence;
}


=pod

=head2 ConvertFamilyType

B<Description>: [function description]. #+++

B<ArgsCount>: [count of arguments] #+++

=over 4

=item $variable_name: ([variable nature]) ([requirements]) #+++ see below

#--- requirement can be:
#--- (R)=required,
#--- (O)=optional
#--- (U)=optional and must be undef if omitted
[variable description]. #+++

=item $variable_name2: ([variable nature]) ([requirements]) #+++

[variable description]. #+++

=back

B<Return>: ([return type]) #+++

[return description]. #+++

B<Example>:

    [example code] #+++

=cut

sub ConvertFamilyType
{
    my ($v3_family) = @_;

    my $type = $v3_family->type || '0';
    if (!exists($TYPE_CONVERSION_HASH{$type}))
    {
        LogWarning("Unable to convert type '$type' for family '$v3_family' (" . $v3_family->family_id . ")");
        $type = '0';
    }
    return $TYPE_CONVERSION_HASH{$type};
}


=pod

=head2 ConvertFamilyInference

B<Description>: [function description]. #+++

B<ArgsCount>: [count of arguments] #+++

=over 4

=item $variable_name: ([variable nature]) ([requirements]) #+++ see below

#--- requirement can be:
#--- (R)=required,
#--- (O)=optional
#--- (U)=optional and must be undef if omitted
[variable description]. #+++

=item $variable_name2: ([variable nature]) ([requirements]) #+++

[variable description]. #+++

=back

B<Return>: ([return type]) #+++

[return description]. #+++

B<Example>:

    [example code] #+++

=cut

sub ConvertFamilyInference
{
    my ($v3_family) = @_;

    my $inference = $v3_family->inference ||'';
    if (!exists($INFERENCE_CONVERSION_HASH{uc($inference)}))
    {
        LogWarning("Unable to convert inference '$inference' for family '$v3_family' (" . $v3_family->family_id . ")");
        $inference = '';
    }
    return $INFERENCE_CONVERSION_HASH{uc($inference)};
}


=pod

=head2 ExtractFamilyData

B<Description>: [function description]. #+++

B<ArgsCount>: [count of arguments] #+++

=over 4

=item $variable_name: ([variable nature]) ([requirements]) #+++ see below

#--- requirement can be:
#--- (R)=required,
#--- (O)=optional
#--- (U)=optional and must be undef if omitted
[variable description]. #+++

=item $variable_name2: ([variable nature]) ([requirements]) #+++

[variable description]. #+++

=back

B<Return>: ([return type]) #+++

[return description]. #+++

B<Example>:

    [example code] #+++

=cut

sub ExtractFamilyData
{
    my ($v3_family) = @_;

    my $synonyms = $v3_family->synonyms;

    my $dbxrefs = [];
    foreach my $v3_dbxref (@{$v3_family->dbxref})
    {
        my $dbxref_hash = $v3_dbxref->getHashValue();
        $dbxref_hash->{'db'} = $v3_dbxref->db->name;
        push(@$dbxrefs, $dbxref_hash);
    }

    # freeze data for database storing
    my $family_data = {
        'family_v3' => $v3_family,
        'synonyms'  => $synonyms,
        'dbxref'    => $dbxrefs,
    };

    return $family_data;
}


=pod

=head2 GetSequenceAssociation

B<Description>: [function description]. #+++

B<ArgsCount>: [count of arguments] #+++

=over 4

=item $variable_name: ([variable nature]) ([requirements]) #+++ see below

#--- requirement can be:
#--- (R)=required,
#--- (O)=optional
#--- (U)=optional and must be undef if omitted
[variable description]. #+++

=item $variable_name2: ([variable nature]) ([requirements]) #+++

[variable description]. #+++

=back

B<Return>: ([return type]) #+++

[return description]. #+++

B<Example>:

    [example code] #+++

=cut

sub GetSequenceAssociation
{
    my ($v3_dbh) = @_;

    # get sequence count
    my $sql_query = "SELECT COUNT(1) FROM sequences WHERE filtered = 0;";
    my ($total_sequence_count) = $v3_dbh->selectrow_array($sql_query);

    LogInfo("Preparing v3.seq_textid-v4.id assocation hash...");
    $sql_query = "SELECT accession, id FROM custom_sequences WHERE user_id = " . $g_v3_user->id . ";";
    my $accession_to_id_array = $g_dbh->selectcol_arrayref($sql_query, { 'Columns' => [1, 2], });
    my %accession_to_id = @$accession_to_id_array;
    LogInfo("...assocation hash done.");

    LogInfo("Processing v3 sequences...");
    my $sequence_association_hash = {};
    my ($missing_sequence_species, $already_inserted, $no_association) = (0, 0, 0);
    my $offset = 0;
    $sql_query = "SELECT seq_id, seq_textid FROM sequences WHERE filtered = 0 LIMIT $SEQUENCE_BATCH_SIZE OFFSET $offset;";
    my $v3_sequences = $v3_dbh->selectall_arrayref($sql_query, { 'Slice' => {} });

    if (!@$v3_sequences)
    {
        LogError("No sequence found in v3 database!");
        return;
    }

    while (@$v3_sequences)
    {
        LogVerboseInfo("Working on sequences [$offset, " . ($offset + $#$v3_sequences) . "]...");
        foreach my $v3_sequence (@$v3_sequences)
        {
            LogVerboseInfo("-working on sequence '" . $v3_sequence->{'seq_textid'} . "'");
            $sequence_association_hash->{$v3_sequence->{'seq_id'}} = $accession_to_id{$v3_sequence->{'seq_textid'}};
        } # end of foreach

        LogVerboseInfo("...done with current batch of sequences.");
        print GetProgress(
            {'label' => 'Processed sequences:', 'current' => $offset + scalar(@$v3_sequences), 'total' => $total_sequence_count,},
            80,
            1, # seconds
        );

        # get next batch of sequences
        $offset += $SEQUENCE_BATCH_SIZE;
        $sql_query = "SELECT seq_id, seq_textid FROM sequences WHERE filtered = 0 LIMIT $SEQUENCE_BATCH_SIZE OFFSET $offset;";
        $v3_sequences = $v3_dbh->selectall_arrayref($sql_query, { 'Slice' => {} });
    } # end of while
    if ($total_sequence_count)
    {
        # print 100%
        print GetProgress(
            {'label' => 'Processed sequences:', 'current' => $total_sequence_count, 'total' => $total_sequence_count,},
            80,
            0, # seconds
        ) . "\n";
    }
    LogInfo("...done processing v3 sequences.");
    LogInfo("Transfered sequences: " . scalar(keys(%$sequence_association_hash)) . "\nUntransfered sequences (missing species): $missing_sequence_species\nUntransfered sequences (duplicates): $already_inserted\nSequences without association: $no_association");

    return $sequence_association_hash;
}


=pod

=head2 TransferSequences

B<Description>: [function description]. #+++

B<ArgsCount>: [count of arguments] #+++

=over 4

=item $variable_name: ([variable nature]) ([requirements]) #+++ see below

#--- requirement can be:
#--- (R)=required,
#--- (O)=optional
#--- (U)=optional and must be undef if omitted
[variable description]. #+++

=item $variable_name2: ([variable nature]) ([requirements]) #+++

[variable description]. #+++

=back

B<Return>: ([return type]) #+++

[return description]. #+++

B<Example>:

    [example code] #+++

=cut

sub TransferSequences
{
    my ($v3_dbh) = @_;

    LogInfo("Processing v3 sequences...");
    # get sequence count
    my $sql_query = "SELECT COUNT(1) FROM sequences WHERE filtered = 0;";
    my ($total_sequence_count) = $v3_dbh->selectrow_array($sql_query);

    my $sequence_association_hash = {};
    my ($missing_sequence_species, $already_inserted, $no_association) = (0, 0, 0);
    my $offset = 0;
    my $v3_sequences = [Greenphyl::ExtV3Sequence->new($v3_dbh, {'selectors' => {'filtered' => 0}, 'sql' => {'OFFSET' => $offset, 'LIMIT' => $SEQUENCE_BATCH_SIZE}})];
    if (!@$v3_sequences)
    {
        LogError("No sequence found in v3 database!");
        return;
    }

    while (@$v3_sequences)
    {
        LogVerboseInfo("Working on sequences [$offset, " . ($offset + $#$v3_sequences) . "]...");
SEQUENCE_TRANSFER_LOOP:
        foreach my $v3_sequence (@$v3_sequences)
        {
            LogVerboseInfo("-working on sequence '" . $v3_sequence->seq_textid . "'");
            
            # make sure we don't process twice the same sequence
            if (exists($sequence_association_hash->{$v3_sequence->seq_id}))
            {
                ++$already_inserted;
                LogWarning("Sequence '$v3_sequence' (" . $v3_sequence->seq_id . ") already processed!");
                next SEQUENCE_TRANSFER_LOOP;
            }
            
            # create a new v4 custom_sequence object associated to GreenPhyl v3 user
            my ($locus, $splice_form) = ExtractLocusAndSpliceForm($v3_sequence->seq_textid, $v3_sequence->species->species_name);
            my $v4_species = Greenphyl::Species->new($g_dbh, {'selectors' => {'code' => $v3_sequence->species->species_name,}});
            if (!$v4_species)
            {
                ++$missing_sequence_species;
                LogError("Species not found in database: " . $v3_sequence->species->species_name . "\nSequence '$v3_sequence' not added!");
                next SEQUENCE_TRANSFER_LOOP;
            }
            my $custom_sequence = Greenphyl::CustomSequence->new(
                $g_dbh,
                {
                    'load' => 'none',
                    'members' => {
                        'accession'       => $v3_sequence->seq_textid,
                        'locus'           => $locus,
                        'splice'          => $splice_form,
                        'splice_priority' => (defined($splice_form)?0:undef),
                        'length'          => $v3_sequence->seq_length,
                        'species_id'      => $v4_species->id,
                        'annotation'      => $v3_sequence->annotation,
                        'polypeptide'     => $v3_sequence->sequence,
                        'user_id'         => $g_v3_user->id,
                    },
                },
                
            );
            # add custom sequence to database
            $custom_sequence->save();

            # store ID association
            $sequence_association_hash->{$v3_sequence->seq_id} = $custom_sequence->id;

            # find corresponding v4 sequence...
            #+FIXME: maybe include support for a mapping file (by species)
            #+FIXME: maybe allow some specified species to be skipped
            my $v4_sequence = FindAssociatedSequence($v3_sequence, $v4_species);
            
            if ($v4_sequence)
            {
                LogVerboseInfo(" found associated v4 sequence '" . $v4_sequence->accession . "'");
                # store link if none already set
                $sql_query = "
                        SELECT TRUE
                        FROM custom_sequences_sequences_relationships cssr
                            JOIN custom_sequences cs ON (cs.id = cssr.custom_sequence_id)
                        WHERE cs.user_id = " . $g_v3_user->id . "
                            AND cssr.sequence_id = " . $v4_sequence->id . "
                            AND cssr.type = 'synonym'
                        LIMIT 1
                        ;";
                # LogDebug("SQL Query: $sql_query");
                my ($relationship_found) = $g_dbh->selectrow_array($sql_query);
                    # or confess LogError("Failed to search for an already set sequence relationship!\n" . $g_dbh->errstr);
                if (!$relationship_found)
                {
                    LogVerboseInfo("Add a relationship between '$custom_sequence' (" . $custom_sequence->id . ") and '$v4_sequence' (" . $v4_sequence->id . ")");
                    $sql_query = "
                        INSERT INTO
                            custom_sequences_sequences_relationships (custom_sequence_id, sequence_id, type)
                        VALUES (" . $custom_sequence->id . ", " . $v4_sequence->id . ", 'synonym')
                        ;";
                    $g_dbh->do($sql_query) or LogError("Failed to insert v3-v4 sequence relationship!\n" . $g_dbh->errstr);
                }
            }
            else
            {
                ++$no_association;
                LogVerboseInfo(" no associated v4 sequence found");
            }
        } # end of foreach

        LogVerboseInfo("...done with current batch of sequences.");
        print GetProgress(
            {'label' => 'Processed sequences:', 'current' => $offset + scalar(@$v3_sequences), 'total' => $total_sequence_count,},
            80,
            5, # seconds
        );

        # get next batch of sequences
        $offset += $SEQUENCE_BATCH_SIZE;
        $v3_sequences = [Greenphyl::ExtV3Sequence->new($v3_dbh, {'selectors' => {'filtered' => 0}, 'sql' => {'OFFSET' => $offset, 'LIMIT' => $SEQUENCE_BATCH_SIZE}})];
    } # end of while
    if ($total_sequence_count)
    {
        # print 100%
        print GetProgress(
            {'label' => 'Processed sequences:', 'current' => $total_sequence_count, 'total' => $total_sequence_count,},
            80,
            0, # seconds
        ) . "\n";
    }

    LogInfo("...done processing v3 sequences.");
    LogInfo("Transfered sequences: " . scalar(keys(%$sequence_association_hash)) . "\nUntransfered sequences (missing species): $missing_sequence_species\nUntransfered sequences (duplicates): $already_inserted\nSequences without association: $no_association");

    return $sequence_association_hash;
}


=pod

=head2 TransferFamilies

B<Description>: [function description]. #+++

B<ArgsCount>: [count of arguments] #+++

=over 4

=item $variable_name: ([variable nature]) ([requirements]) #+++ see below

#--- requirement can be:
#--- (R)=required,
#--- (O)=optional
#--- (U)=optional and must be undef if omitted
[variable description]. #+++

=item $variable_name2: ([variable nature]) ([requirements]) #+++

[variable description]. #+++

=back

B<Return>: ([return type]) #+++

[return description]. #+++

B<Example>:

    [example code] #+++

=cut

sub TransferFamilies
{
    my ($v3_dbh, $sequence_association_hash) = @_;

    LogInfo("Processing v3 families...");
    # get sequence count
    my $sql_query = "SELECT COUNT(1) FROM family WHERE black_list = 0;";
    my ($total_family_count) = $v3_dbh->selectrow_array($sql_query);

    my $family_association_hash = {};
    my $offset = 0;
    my $v3_families = [Greenphyl::ExtV3Family->new($v3_dbh, {'selectors' => {'black_list' => 0}, 'sql' => {'OFFSET' => $offset, 'LIMIT' => $FAMILY_BATCH_SIZE}})];

    if (!@$v3_families)
    {
        LogError("No family found in v3 database!");
        return;
    }

    while (@$v3_families)
    {
        LogVerboseInfo("Working on families [$offset, " . ($offset + $#$v3_families) . "]...");
FAMILY_TRANSFER_LOOP:
        foreach my $v3_family (@$v3_families)
        {
            LogVerboseInfo("-working on family '" . $v3_family->accession . "'");

            # make sure we don't process twice the same family
            if (exists($family_association_hash->{$v3_family->family_id}))
            {
                LogWarning("Family '$v3_family' (" . $v3_family->family_id . ") already processed!");
                next FAMILY_TRANSFER_LOOP;
            }

            # create a new v4 custom_family object associated to GreenPhyl v3 user
            my $new_accession = $v3_family->accession . $FAMILY_SUFFIX; # Append '_V3' to accession
            my $custom_family = Greenphyl::CustomFamily->new(
                $g_dbh,
                {
                    'load' => 'none',
                    'members' => {
                        'accession'       => $new_accession,
                        'name'            => $v3_family->family_name,
                        'level'           => $v3_family->level,
                        'type'            => ConvertFamilyType($v3_family),
                        'description'     => $v3_family->description || '',
                        'inferences'      => ConvertFamilyInference($v3_family),
                        'data'            => ExtractFamilyData($v3_family),
                        'flags'           => $Greenphyl::CustomFamily::CUSTOM_FAMILY_FLAG_OBSOLETE . (1 == $v3_family->level ? ',' . $Greenphyl::CustomFamily::CUSTOM_FAMILY_FLAG_LISTED : ''),
                        'access'          => $Greenphyl::CustomFamily::CUSTOM_FAMILY_ACCESS_PUBLIC_READ,
                        'user_id'         => $g_v3_user->id,
                    },
                },
            );
            # add custom family to database
            $custom_family->save();

            # store ID association
            $family_association_hash->{$v3_family->family_id} = $custom_family->id;
            
            # associate sequences
            my @custom_sequence_ids;
            foreach my $v3_sequence (@{$v3_family->fetchSequences({'selectors' => {'filtered' => 0} })})
            {
                if (!exists($sequence_association_hash->{$v3_sequence->seq_id}))
                {
                    LogWarning("Custom sequence for sequence '$v3_sequence' (" . $v3_sequence->seq_id . ") not available for family '$v3_family'!");
                }
                else
                {
                    push(@custom_sequence_ids, $sequence_association_hash->{$v3_sequence->seq_id});
                }
            }

            if (@custom_sequence_ids)
            {
                my $sql_query = "
                    INSERT INTO custom_families_sequences
                        (custom_family_id, custom_sequence_id)
                    VALUES (" . join('), (', map {$custom_family->id . ', ' . $_} @custom_sequence_ids) . ");
                ";
                # LogDebug("SQL Query: $sql_query");
                $g_dbh->do($sql_query)
                    or confess LogError("Failed to insert custom family-sequence relationships (family: '$custom_family' , " . $custom_family->id . ")!\n" . $g_dbh->errstr);
            }
            else
            {
                LogWarning("New custom family '$v3_family' has no associated sequence!");
            }

        } # end of foreach

        LogVerboseInfo("...done with current batch of families.");
        print GetProgress(
            {'label' => 'Processed families:', 'current' => $offset + scalar(@$v3_families), 'total' => $total_family_count,},
            80,
            5, # seconds
        );
        
        # get next batch of families
        $offset += $FAMILY_BATCH_SIZE;
        $v3_families = [Greenphyl::ExtV3Family->new($v3_dbh, {'selectors' => {'black_list' => 0}, 'sql' => {'OFFSET' => $offset, 'LIMIT' => $FAMILY_BATCH_SIZE}})];
    } # end of while
    if ($total_family_count)
    {
        # print 100%
        print GetProgress(
            {'label' => 'Processed families:', 'current' => $total_family_count, 'total' => $total_family_count,},
            80,
            0, # seconds
        ) . "\n";
    }
    LogInfo("...done processing v3 families.");
    LogInfo("Transfered families: " . scalar(keys(%$family_association_hash)));

    return $family_association_hash;
}



=pod

=head2 GetFamilyAssociation

B<Description>: [function description]. #+++

B<ArgsCount>: [count of arguments] #+++

=over 4

=item $variable_name: ([variable nature]) ([requirements]) #+++ see below

#--- requirement can be:
#--- (R)=required,
#--- (O)=optional
#--- (U)=optional and must be undef if omitted
[variable description]. #+++

=item $variable_name2: ([variable nature]) ([requirements]) #+++

[variable description]. #+++

=back

B<Return>: ([return type]) #+++

[return description]. #+++

B<Example>:

    [example code] #+++

=cut

sub GetFamilyAssociation
{
    my ($v3_dbh, $sequence_association_hash) = @_;

    LogInfo("Retrieving v3-v4 family associations...");
    my $family_association_hash = {};

    # get v3 accession->id association
    my $sql_query = "SELECT family_id, accession FROM family WHERE black_list = 0;";
    my $v3_family_accession_to_id_array = $v3_dbh->selectcol_arrayref($sql_query, { 'Columns' => [1, 2], });
    my %v3_accession_to_id = @$v3_family_accession_to_id_array;

    # get v4 accession->id association
    $sql_query = "SELECT id, accession FROM custom_families WHERE user_id = " . $g_v3_user->id . ";";
    my $v4_family_accession_to_id_array = $g_dbh->selectcol_arrayref($sql_query, { 'Columns' => [1, 2], });
    my %v4_accession_to_id = @$v4_family_accession_to_id_array;
    
    
    foreach my $v3_accession (keys(%v3_accession_to_id))
    {
        my $v4_accession = $v3_accession . $FAMILY_SUFFIX;
        if (!exists($v4_accession_to_id{$v4_accession}))
        {
            LogWarning("Corresponding custom family not found in v4 database for v3 family '$v3_accession'!");
        }
        else
        {
            $family_association_hash->{$v3_accession_to_id{$v3_accession}} = $v4_accession_to_id{$v4_accession};
        }
    }
    LogInfo("...done retrieving v3-v4 family associations.");

    return $family_association_hash;
}

=pod

=head2 TransferFamilyRelationships

B<Description>: [function description]. #+++

B<ArgsCount>: [count of arguments] #+++

=over 4

=item $variable_name: ([variable nature]) ([requirements]) #+++ see below

#--- requirement can be:
#--- (R)=required,
#--- (O)=optional
#--- (U)=optional and must be undef if omitted
[variable description]. #+++

=item $variable_name2: ([variable nature]) ([requirements]) #+++

[variable description]. #+++

=back

B<Return>: ([return type]) #+++

[return description]. #+++

B<Example>:

    [example code] #+++

=cut

sub TransferFamilyRelationships
{
    my ($v3_dbh, $family_association_hash) = @_;

    LogInfo("Processing v3 family relationships...");

    my $total_family_count = scalar(keys(%$family_association_hash));
    my $family_counter = 0;
    my $previous_relationship_sh = $v3_dbh->prepare("SELECT object_family_id, level_delta FROM family_relationships_cache WHERE subject_family_id = ?;");

FAMILY_RELATIONSHIP_TRANSFER_LOOP:
    foreach my $v3_family_id (keys(%$family_association_hash))
    {
        if (!exists($family_association_hash->{$v3_family_id}))
        {
            LogWarning("v3-v4 family association is missing for v3 family ID $v3_family_id (black_listed?)! No relationship can be transfered for that family!");
            next FAMILY_RELATIONSHIP_TRANSFER_LOOP;
        }

        $previous_relationship_sh->execute($v3_family_id);
        my $v3_relationships = $previous_relationship_sh->fetchall_arrayref();
        my @v4_relationships;
CURRENT_FAMILY_RELATIONSHIP_LOOP:
        foreach my $v3_relationship (@$v3_relationships)
        {
            if (!exists($family_association_hash->{$v3_relationship->[0]}))
            {
                LogWarning("v3-v4 family association is missing for v3 family ID " . $v3_relationship->[0] . " (black_listed?)! Relationship with v3 family ID $v3_family_id will not be transfered!");
                next CURRENT_FAMILY_RELATIONSHIP_LOOP;
            }
            push(@v4_relationships, "($family_association_hash->{$v3_family_id}, $family_association_hash->{$v3_relationship->[0]}, $v3_relationship->[1], '$Greenphyl::AbstractFamily::RELATIONSHIP_INHERITANCE')");
        }
        if (@v4_relationships)
        {
            my $sql_query = "
                INSERT INTO custom_family_relationships
                    (subject_custom_family_id, object_custom_family_id, level_delta, type)
                VALUES "
                . join(', ', @v4_relationships)
                . ";"
            ;
            $g_dbh->do($sql_query)
                or LogError("Failed to transfer family relationships!\n" . $g_dbh->errstr);
        }

        print GetProgress(
            {'label' => 'Processed families:', 'current' => ++$family_counter, 'total' => $total_family_count,},
            80,
            2, # seconds
        );
    } # end of foreach
    if ($total_family_count)
    {
        # print 100%
        print GetProgress(
            {'label' => 'Processed families:', 'current' => $total_family_count, 'total' => $total_family_count,},
            80,
            0, # seconds
        ) . "\n";
    }
    LogInfo("...done processing v3 family relationships.");
}




# Script options
#################

=pod

=head1 OPTIONS

#--- describes parameters given to the script
#+++ command line syntax
#--- requirement of the option and its parameter can be:
#--- required: name or nature inside <>
#--- optional: name or nature inside []
#--- alternative between 2 elements: elements separated by a |

=head2 Parameters

=over 4

#+++=item B<-help>:
#+++
#+++Prints a brief help message and exits.
#+++
#+++=item B<-man>:
#+++
#+++Prints the manual page and exits.
#+++
#+++=item B<-debug> (integer):
#+++
#+++Executes the script in debug mode. The integer value is optional and is used
#+++to set debug level (use 0 to force disable debug mode).
#+++Default: 0 (not in debug mode).
#+++

=item B<[option_name]> ([option nature]): #+++

[option description]. #+++
Default: [option default value if one] #+++

#--- remove if log not used
#+++=item B<-log>:
#+++
#+++Enable logging.
#+++
#+++=item B<-log-*> (any):
#+++
#+++Logging arguments.

=back
#--- Example:
#---
#--- Template.pl [-help | -man]
#---
#--- Template.pl [-debug [debug_level]] [-size <width> [height]]
#---
#--- =over 4
#---
#--- =item B<-help>:
#---
#--- Prints a brief help message and exits.
#---
#--- =item B<-man>:
#---
#--- Prints the manual page and exits.
#---
#--- =item B<-debug> (integer):
#---
#--- Executes the script in debug mode. If an integer value is specified, it will
#--- be the debug level. If "-debug" option was used without specifying a debug
#--- level, level 1 is assumed.
#--- Default: 0 (not in debug mode).
#---
#---=item B<-size> (positive_real) (positive_real):
#---
#--- Set the dimensions of the object that will be drawn. The first value is
#--- the width; the height is the second value if specified, otherwise it will
#--- assume height and width are equal to the first value.
#--- Default: width and height are set to 1.
#---
#---=back

=cut


# CODE START
#############

# for regression tests
if ($ENV{'TESTING'} && $ENV{'TESTING_NO_RUN'})
{return 1;}

# options processing
my ($man, $help, $debug, $no_prompt) = (0, 0, undef, undef);

my ($v3_host, $v3_port, $v3_login, $v3_password, $v3_database);

my ($nocleanup, $resumefamilies, $resumerelationships);

# parse options and print usage if there is a syntax error.
GetOptions('help|?'       => \$help,
           'man'          => \$man,
           'debug:s'      => \$debug,
           'q|noprompt'   => \$no_prompt,
           'h|host=s'     => \$v3_host,
           'p|port=s'     => \$v3_port,
           'l|login=s'    => \$v3_login,
           'password=s'   => \$v3_password,
           'd|database=s' => \$v3_database,
           'nocleanup'    => \$nocleanup,
           'resumefamilies' => \$resumefamilies,
           'resumerelationships' => \$resumerelationships,
           @Greenphyl::Log::LOG_GETOPT, # skip log parameters
) or pod2usage(1);
if ($help) {pod2usage('-verbose' => 1, '-exitval' => 0);}
if ($man) {pod2usage('-verbose' => 2, '-exitval' => 0);}

# change debug mode if requested/forced
if (defined($debug))
{
    # just '-debug' ?
    if ($debug eq '')
    {
        $debug = 1;
    }
    $DEBUG = $debug;
}

$v3_host     ||= Prompt("GreenPhyl v3 database host name?", { 'default' => GP('DATABASES')->[0]->{'host'}, 'constraint' => '\w', }, $no_prompt);
$v3_port     ||= Prompt("GreenPhyl v3 database port?", { 'default' => '3306', 'constraint' => '^\d+$', }, $no_prompt);
$v3_database ||= Prompt("GreenPhyl v3 database name?", { 'default' => 'greenphyl_v3', 'constraint' => '\w', }, $no_prompt);
$v3_login    ||= Prompt("GreenPhyl v3 database login?", { 'default' => 'greenphyl', 'constraint' => '\w', }, $no_prompt);
$v3_password ||= Prompt("GreenPhyl v3 database password?", { 'default' => '', }, $no_prompt);

if (!$v3_host || !$v3_port || !$v3_login || !$v3_database)
{
    warn "Missing v3 connection parameter(s)!\n";
    pod2usage(1);
}

eval
{
    my $v3_dbh = ConnectToDatabase({
        'host'     => $v3_host,
        'port'     => $v3_port,
        'login'    => $v3_login,
        'password' => $v3_password,
        'database' => $v3_database,
    });

    # Get v3 user
    $g_v3_user = Greenphyl::User->new($g_dbh, {'selectors' => {'name' => 'greenphyl_v3'}});
    if (!$g_v3_user)
    {
        LogInfo("Creating GreenPhyl v3 User...");
        my $db_index = SelectActiveDatabase();
        if (system("../utils/manage_users.pl -add greenphyl_v3 -flags 'disabled' -q" . ($db_index? " db_index=$db_index" : '')))
        {
            confess LogError("Failed to create GreenPhyl v3 user!\n" . $@);
        }
        $g_v3_user = Greenphyl::User->new($g_dbh, {'selectors' => {'name' => 'greenphyl_v3'}});
        if (!$g_v3_user)
        {
            confess LogError("Failed to load GreenPhyl v3 user!");
        }
    }
    else
    {
        LogVerboseInfo("GreenPhyl v3 User found in database.");
    }
    
    my $sql_query;
    # clean previous data
    if (!$nocleanup)
    {
        if (!$resumerelationships)
        {
            LogInfo("Removing previous custom v3 families...");
            $sql_query = 'DELETE FROM custom_families WHERE user_id = ' . $g_v3_user->id . ';';
            if (!$g_dbh->do($sql_query))
            {
                LogWarning('Failed to remove previous v3 families!');
            }
            LogInfo("...done clearing v3 families.");
        }

        if (!$resumefamilies && !$resumerelationships)
        {
            LogInfo("Removing previous custom v3 sequences.");
            $sql_query = 'DELETE FROM custom_sequences WHERE user_id = ' . $g_v3_user->id . ';';
            if (!$g_dbh->do($sql_query))
            {
                LogWarning('Failed to remove previous v3 sequences!');
            }
            LogInfo("...done clearing v3 sequences.");
        }
    }

    # Transfer sequences
    my $sequence_association_hash;
    if ($resumefamilies || $resumerelationships)
    {
        $sequence_association_hash = GetSequenceAssociation($v3_dbh);
    }
    else
    {
        $sequence_association_hash = TransferSequences($v3_dbh);
    }

    # transfer families
    my $family_association_hash;
    if ($resumerelationships)
    {
        $family_association_hash = GetFamilyAssociation($v3_dbh, $sequence_association_hash);
    }
    else
    {
        $family_association_hash = TransferFamilies($v3_dbh, $sequence_association_hash);
    }
    
    # transfer relationships
    TransferFamilyRelationships($v3_dbh, $family_association_hash);
    
};

# catch
HandleErrors();

exit(0);

# CODE END
###########


=pod

=head1 AUTHORS

Mathieu ROUARD (Bioversity), m.rouard@cgiar.org

Valentin GUIGNON (Bioversity), v.guignon@cgiar.org

Matthieu CONTE (Syngenta)

Sebastien BRIOIS (Syngenta)

=head1 VERSION

Version 1.0.0

Date 02/12/2013

=head1 SEE ALSO

GreenPhyl documentation.

=cut
