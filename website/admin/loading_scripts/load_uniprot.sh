#!/bin/bash

wait_mysql() {
while ! mysqladmin ping -h marquenterre.cirad.fr  -u greenphyl -patlas76 --silent
do
  echo .
  sleep 1
done
}

wait_mysql
perl /gs7k1/projects/greenphyl/dev/website/admin/loading_scripts/load_uniprot.pl -species=AMBTC,ARATH,BETVU,BRADI,BRANA -log -log-logfile=uniprot1.log -log-screen
wait_mysql
perl /gs7k1/projects/greenphyl/dev/website/admin/loading_scripts/load_uniprot.pl -species=BRAOL,BRARR,CAJCA,CAPAN,CHEQI -log -log-logfile=uniprot2.log -log-screen
wait_mysql
perl /gs7k1/projects/greenphyl/dev/website/admin/loading_scripts/load_uniprot.pl -species=CICAR,CITMA,CITME,CITSI,COCNU -log -log-logfile=uniprot3.log -log-screen
wait_mysql
perl /gs7k1/projects/greenphyl/dev/website/admin/loading_scripts/load_uniprot.pl -species=COFAR,COFCA,CUCME,CUCSA,DAUCA -log -log-logfile=uniprot4.log -log-screen
wait_mysql
# DIORT removed: empty data from UniProt
perl /gs7k1/projects/greenphyl/dev/website/admin/loading_scripts/load_uniprot.pl -species=ELAGV,FRAVE,HELAN,HORVU,MAIZE -log -log-logfile=uniprot5.log -log-screen
wait_mysql
perl /gs7k1/projects/greenphyl/dev/website/admin/loading_scripts/load_uniprot.pl -species=IPOTF,IPOTR,MALDO,MANES,MEDTR -log -log-logfile=uniprot6.log -log-screen
wait_mysql
perl /gs7k1/projects/greenphyl/dev/website/admin/loading_scripts/load_uniprot.pl -species=MUSAC,MUSBA,OLEEU,ORYGL,ORYSA -log -log-logfile=uniprot7.log -log-screen
wait_mysql
perl /gs7k1/projects/greenphyl/dev/website/admin/loading_scripts/load_uniprot.pl -species=PHAVU,PHODC,SACSP,SOLLC,SOLTU -log -log-logfile=uniprot8.log -log-screen
wait_mysql
perl /gs7k1/projects/greenphyl/dev/website/admin/loading_scripts/load_uniprot.pl -species=SORBI,SOYBN,THECC,TRITU,VITVI -log -log-logfile=uniprot9.log -log-screen
