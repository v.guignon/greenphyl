#!/usr/bin/env perl

=pod

=head1 NAME

load_greenphyl_v5.pl - Loads GreenPhyl v5 sequences and families into v5.1 DB

=head1 SYNOPSIS

    load_greenphyl_v5.pl

=head1 REQUIRES

Perl5

=head1 DESCRIPTION

Transfer GreenPhyl v5 sequences and families into v5.1 DB as custom sequences and
families. Extra family-data is stored as a frozen Perl structure into the
custom_families.data field. See Greenphyl::CustomFamily for details.

=cut

use strict;
use warnings;

use FindBin;
use lib "$FindBin::Bin/../../lib";
use lib "$FindBin::Bin/../../local_lib";
use Carp qw (cluck confess croak);

use Greenphyl;
use Greenphyl::Log('nolog' => 1,);
use Greenphyl::User;
use Greenphyl::Genome;
use Greenphyl::Sequence;
use Greenphyl::Tools::Sequences;
use Greenphyl::AbstractFamily;

use Getopt::Long;
use Pod::Usage;

use Storable qw(nfreeze);

++$|; #no buffering




# Script global constants
##########################

=pod

=head1 CONSTANTS

B<$REV_STRING>: (string)

Current revision (auto-set by GIT).

B<$DEBUG>: (boolean)

When set to true, it enables debug mode.

B<$CONSTANT_NAME>: ([constant nature]) #+++

[constant description and use]. #+++

=cut

# SVN management part
my $REV_STRING = '$id$';

our $DEBUG = 0;
our $SEQUENCE_BATCH_SIZE = 5000;
our $FAMILY_BATCH_SIZE   = 1000;

our %TYPE_CONVERSION_HASH = (
    '0' => 'N/A',
    '1' => 'superfamily',
    '2' => 'family',
    '3' => 'subfamily',
    '4' => 'group',
);

our %INFERENCE_CONVERSION_HASH = (
    ''                 => '',
    ' '                => '',
    'IEA:INTERPRO'     => 'IEA:InterPro',
    'IEA:KEGG'         => 'IEA:KEGG',
    'IEA:MEROPS'       => 'IEA:MEROPS',
    'IEA:PIRSF'        => 'IEA:PIRSF',
    'IEA:TF'           => 'IEA:TF',
    'IEA:UNIPROTKB'    => 'IEA:UniProtKB',
    'IEA:UNIPROTKB-KW' => 'IEA:UniProtKB-KW',
    'N/A'              => '',
    'OTHER'            => 'Other',
    'PUBMED'           => 'PubMed',
    'TAIR/TIGR'        => 'TAIR/TIGR',
);

our $FAMILY_SUFFIX = '_V5';


our %GENOME_MAPPING = (
    'AMBTC' => 'AMBTC_v1.0', # Amborella trichopoda
    'ARATH' => 'ARATH_Araport11', # Arabidopsis thaliana
    'BRADI' => 'BRADI_Bd21v2_1_283_Bd21v2', # Brachypodium distachyon
    'CAJCA' => 'CAJCA_Asha_v1.0', # Cajanus cajan
    # 'CARPA' => '', # Carica papaya
    # 'CHLRE' => '', # Chlamydomonas reinhardtii
    'CICAR' => 'CICAR_CDC_Frontier_kabuli', # Cicer arietinum
    'CITSI' => 'CITSI_v2.0', # Citrus sinensis
    'COFCA' => 'COFCA_v1.0', # Coffea canephora
    'CUCSA' => 'CUCSA_ASM407_v2.0', # Cucumis sativus
    # 'CYAME' => '', # Cyanidioschyzon merolae
    'ELAGV' => 'ELAGV_RefSeq_v1.0', # Elaeis guineensis
    'GLYMA' => 'SOYBN_Wm82_a2_v1.0', # Glycine max
    # 'GOSRA' => '', # Gossypium raimondii
    'HORVU' => 'HORVU_IBSC_v2.0', # Hordeum vulgare
    # 'LOTJA' => '', # Lotus japonicus
    'MABAN' => 'MUSAC_Macba2', # Musa acuminata banksii Banksii
    'MABUR' => 'MUSAC_Macbu1', # Musa acuminata burmanica Calcutta 4
    'MALDO' => 'MALDO_Borkh_v3.0.a1', # Malus domestica
    'MANES' => 'MANES_AM560_2_v6.1', # Manihot esculenta
    'MAZEB' => 'MUSAC_Macze1', # Musa acuminata zebrina Maia Oa
    'MEDTR' => 'MEDTR_Mt4.0v2', # Medicago truncatula
    'MUSAC' => 'MUSAC_Macma2', # Musa acuminata malaccensis DH Pahang
    'MUSBA' => 'MUSBA_Mba1.1', # Musa balbisiana Pisang Klutuk Wulung
    'ORYSA' => 'ORYSA_Nipponbare_v7.0', # Oryza sativa
    # 'OSTTA' => '', # Ostreococcus tauri
    'PHAVU' => 'PHAVU_v2.0', # Phaseolus vulgaris
    'PHODA' => 'PHODC_v3.0', # Phoenix dactylifera
    'PHODC' => 'PHODC_v3.0', # Phoenix dactylifera
    # 'PHYPA' => '', # Physcomitrella patens
    # 'PICAB' => '', # Picea abies
    # 'POPTR' => '', # Populus trichocarpa
    # 'RICCO' => '', # Ricinus communis
    # 'SELMO' => '', # Selaginella moellendorffii
    # 'SETIT' => '', # Setaria italica
    'SOLLY' => 'SOLLC_ITAG3.2', # Solanum lycopersicum
    'SOLLC' => 'SOLLC_ITAG3.2', # Solanum lycopersicum
    'SOLTU' => 'SOLTU_PGSC_v4.03', # Solanum tuberosum
    'SORBI' => 'SORBI_BTx623_v3.1.1', # Sorghum bicolor
    'THECC' => 'THECC_Criollo_v2.0', # Theobroma cacao
    'VITVI' => 'VITVI_12X.2', # Vitis vinifera
    'ZEAMA' => 'MAIZE_B73_v4.0', # Zea mays
    'MAIZE' => 'MAIZE_B73_v4.0', # Zea mays
);


# Script global variables
##########################

=pod

=head1 VARIABLES

B<[g_variable_name]>: ([variable nature]) #+++

[variable description, use and default value]. #+++
Default: [variable default value if one] #+++

#--- Example:
#--- B<$g_output_method>: (integer)
#---
#--- used to store current output method;
#--- 0=raw text (default), 1=image, 2=html.
#---
#---     ...
#---
#--- my $g_output_method = 0;
#---

=cut

our $g_dbh = GetDatabaseHandler();
our $g_v5_user;




# Script global functions
##########################

=pod

=head1 FUNCTIONS

=head2 ExtractFamilyData

B<Description>: [function description]. #+++

B<ArgsCount>: [count of arguments] #+++

=over 4

=item $variable_name: ([variable nature]) ([requirements]) #+++ see below

#--- requirement can be:
#--- (R)=required,
#--- (O)=optional
#--- (U)=optional and must be undef if omitted
[variable description]. #+++

=item $variable_name2: ([variable nature]) ([requirements]) #+++

[variable description]. #+++

=back

B<Return>: ([return type]) #+++

[return description]. #+++

B<Example>:

    [example code] #+++

=cut

sub ExtractFamilyData
{
    my ($v5_family) = @_;

    my $synonyms = $v5_family->synonyms;

    my $dbxrefs = [];
    foreach my $v5_dbxref (@{$v5_family->dbxref})
    {
        my $dbxref_hash = $v5_dbxref->getHashValue();
        $dbxref_hash->{'db'} = $v5_dbxref->db->name;
        push(@$dbxrefs, $dbxref_hash);
    }

    # freeze data for database storing
    my $family_data = {
        'family_v5' => $v5_family,
        'synonyms'  => $synonyms,
        'dbxref'    => $dbxrefs,
    };

    return $family_data;
}


=pod

=head2 TransferSequences

B<Description>: [function description]. #+++

B<ArgsCount>: [count of arguments] #+++

=over 4

=item $variable_name: ([variable nature]) ([requirements]) #+++ see below

#--- requirement can be:
#--- (R)=required,
#--- (O)=optional
#--- (U)=optional and must be undef if omitted
[variable description]. #+++

=item $variable_name2: ([variable nature]) ([requirements]) #+++

[variable description]. #+++

=back

B<Return>: ([return type]) #+++

[return description]. #+++

B<Example>:

    [example code] #+++

=cut

sub TransferSequences
{
    LogInfo("Processing v5 sequences...");
    # get sequence count
    my $sql_query = "SELECT COUNT(1) FROM sequences;";
    my ($total_sequence_count) = $g_dbh->selectrow_array($sql_query);
    my @species = Greenphyl::Species->new($g_dbh, {'selectors' => {'display_order' => 1,}, 'sql' => {'ORDER BY' => 'code'}});

    my $sequence_association_hash = {};
    my $progress = 0;
    foreach my $species (@species)
    {
        LogInfo("Working on species " . $species);
        my $offset = 0;
        # Do not load v5 fields (type, representative, genome_id).
        my $v5_sequences = [Greenphyl::Sequence->new($g_dbh, {'selectors' => {'species_id' => $species->id}, 'sql' => {'LIMIT' => $SEQUENCE_BATCH_SIZE, 'OFFSET' => $offset}})];
        if (!@$v5_sequences)
        {
            LogError("No sequence found!");
            next;
        }
        while (@$v5_sequences)
        {
            LogVerboseInfo("Working on sequences [$offset, " . ($offset + $#$v5_sequences) . "] of species $species->id...");
SEQUENCE_TRANSFER_LOOP:
            foreach my $v5_sequence (@$v5_sequences)
            {
                ++$progress;
                LogVerboseInfo("-working on sequence '" . $v5_sequence->accession . "'");

                # make sure we don't process twice the same sequence
                if (exists($sequence_association_hash->{$v5_sequence->id}))
                {
                    LogWarning("Sequence '$v5_sequence' (" . $v5_sequence->id . ") already processed!");
                    next SEQUENCE_TRANSFER_LOOP;
                }

                # Make sure sequence does not already exist.
                my $custom_sequence = Greenphyl::CustomSequence->new(
                    $g_dbh,
                    {
                        'selectors' =>
                        {
                            'accession'       => $v5_sequence->accession,
                            'species_id'      => $v5_sequence->species_id,
                            'user_id'         => $g_v5_user->id,
                        },
                    },
                );
                if (!$custom_sequence)
                {
                    # create a new v5 custom_sequence object associated to GreenPhyl v5 user
                    my $members = {
                        'accession'       => $v5_sequence->accession,
                        'locus'           => $v5_sequence->locus,
                        'splice'          => $v5_sequence->splice,
                        'splice_priority' => $v5_sequence->splice_priority,
                        'length'          => $v5_sequence->length,
                        'species_id'      => $v5_sequence->species_id,
                        'annotation'      => $v5_sequence->annotation,
                        'polypeptide'     => $v5_sequence->polypeptide,
                        'user_id'         => $g_v5_user->id,
                        'data'            => {
                            'v5' => {
                                'representative' => $v5_sequence->representative,
                                'type' => $v5_sequence->type,
                                'genome_id' => $v5_sequence->genome_id,
                            },
                        },
                    };
                    # adds pangene data.
                    if ($v5_sequence->isPangene)
                    {
                        $members->{'data'}->{'v5'}->{'represented_sequences'} = map {$_->accession} @{$v5_sequence->getRepresentedSequences()};
                    }
                    else
                    {
                        my $pangene_accession = $v5_sequence->getPangeneAccession();
                        if ($pangene_accession && ($pangene_accession ne $Greenphyl::Sequence::NO_PANGENE_ACCESSION))
                        {
                            $members->{'data'}->{'v5'}->{'pangene'} = $pangene_accession;
                        }
                        
                        my $represented_sequences = $v5_sequence->getRepresentedSequences();
                        if ($represented_sequences && @$represented_sequences)
                        {
                            $members->{'data'}->{'v5'}->{'represented_sequences'} = map {$_->accession} @$represented_sequences;
                        }
                    }
                    
                    $custom_sequence = Greenphyl::CustomSequence->new(
                        $g_dbh,
                        {
                            'load' => 'none',
                            'members' => $members,
                        },
                    );
                    # add custom sequence to database
                    $custom_sequence->save();
                }
                else
                {
                    LogVerboseInfo(" v5 sequence '" . $v5_sequence->accession . "' already transfered");
                }

                # store ID association
                $sequence_association_hash->{$v5_sequence->id} = $custom_sequence->id;

                # add corresponding v5 sequence...
                # store link if none already set
                $sql_query = "
                        SELECT TRUE
                        FROM custom_sequences_sequences_relationships cssr
                            JOIN custom_sequences cs ON (cs.id = cssr.custom_sequence_id)
                        WHERE cs.user_id = " . $g_v5_user->id . "
                            AND cssr.sequence_id = " . $v5_sequence->id . "
                            AND cssr.type = 'synonym'
                        LIMIT 1
                        ;";
                # LogDebug("SQL Query: $sql_query");
                my ($relationship_found) = $g_dbh->selectrow_array($sql_query);
                # or confess LogError("Failed to search for an already set sequence relationship!\n" . $g_dbh->errstr);
                # Only add for non-pansequences.
                if (!$relationship_found && ($v5_sequence->type eq 'protein'))
                {
                    LogVerboseInfo("Add a sequence relationship for '$custom_sequence' (" . $custom_sequence->id . ")");
                    $sql_query = "
                        INSERT INTO
                            custom_sequences_sequences_relationships (custom_sequence_id, sequence_id, type)
                        VALUES (" . $custom_sequence->id . ", " . $v5_sequence->id . ", 'synonym')
                        ;";
                    $g_dbh->do($sql_query) or LogError("Failed to insert v5-v5 sequence relationship!\n" . $g_dbh->errstr);
                }
            } # end of foreach

            LogVerboseInfo("...done with current batch of sequences.");
            print GetProgress(
                {'label' => "Processed sequences ($progress):", 'current' => $progress, 'total' => $total_sequence_count,},
                80,
                5, # seconds
            );

            # get next batch of sequences
            $offset += $SEQUENCE_BATCH_SIZE;
            $v5_sequences = [Greenphyl::Sequence->new($g_dbh, {'selectors' => {'species_id' => $species->id}, 'sql' => {'LIMIT' => $SEQUENCE_BATCH_SIZE, 'OFFSET' => $offset}})];
        } # end of while
    } # end of foreach
    if ($total_sequence_count)
    {
        # print 100%
        print GetProgress(
            {'label' => 'Processed sequences:', 'current' => $total_sequence_count, 'total' => $total_sequence_count,},
            80,
            0, # seconds
        ) . "\n";
    }

    LogInfo("...done processing v5 sequences.");
    return $sequence_association_hash;
}


=pod

=head2 TransferFamilies

B<Description>: [function description]. #+++

B<ArgsCount>: [count of arguments] #+++

=over 4

=item $variable_name: ([variable nature]) ([requirements]) #+++ see below

#--- requirement can be:
#--- (R)=required,
#--- (O)=optional
#--- (U)=optional and must be undef if omitted
[variable description]. #+++

=item $variable_name2: ([variable nature]) ([requirements]) #+++

[variable description]. #+++

=back

B<Return>: ([return type]) #+++

[return description]. #+++

B<Example>:

    [example code] #+++

=cut

sub TransferFamilies
{
    my ($sequence_association_hash, $family_suffix) = @_;

    LogInfo("Processing v5 families...");
    # get sequence count
    my $sql_query = "SELECT COUNT(1) FROM families WHERE black_listed = 0 AND level IN (1, 2, 3, 4);";
    my ($total_family_count) = $g_dbh->selectrow_array($sql_query);

    my $family_association_hash = {};
    my $offset = 0;
    my $v5_families = [Greenphyl::Family->new($g_dbh, {'selectors' => {'black_listed' => 0, 'level' => ['IN', 1, 2, 3, 4]}, 'sql' => {'OFFSET' => $offset, 'LIMIT' => $FAMILY_BATCH_SIZE}})];

    if (!@$v5_families)
    {
        LogError("No family found in v5 database!");
        return;
    }

    while (@$v5_families)
    {
        LogVerboseInfo("Working on families [$offset, " . ($offset + $#$v5_families) . "]...");
FAMILY_TRANSFER_LOOP:
        foreach my $v5_family (@$v5_families)
        {
            LogVerboseInfo("-working on family '" . $v5_family->accession . "'");

            # make sure we don't process twice the same family
            if (exists($family_association_hash->{$v5_family->family_id}))
            {
                LogWarning("Family '$v5_family' (" . $v5_family->family_id . ") already processed!");
                next FAMILY_TRANSFER_LOOP;
            }

            # create a new v5 custom_family object associated to GreenPhyl v5 user
            my $new_accession = $v5_family->accession . '_' . $family_suffix;
            my $custom_family = Greenphyl::CustomFamily->new(
                $g_dbh,
                {
                    'load' => 'none',
                    'members' => {
                        'accession'       => $new_accession,
                        'name'            => $v5_family->name,
                        'level'           => $v5_family->level,
                        'type'            => $v5_family->type,
                        'description'     => $v5_family->description,
                        'inferences'      => $v5_family->inferences,
                        'data'            => ExtractFamilyData($v5_family),
                        'flags'           => $Greenphyl::CustomFamily::CUSTOM_FAMILY_FLAG_OBSOLETE . (1 == $v5_family->level ? ',' . $Greenphyl::CustomFamily::CUSTOM_FAMILY_FLAG_LISTED : ''),
                        'access'          => $Greenphyl::CustomFamily::CUSTOM_FAMILY_ACCESS_PUBLIC_READ,
                        'user_id'         => $g_v5_user->id,
                    },
                },
            );
            # add custom family to database
            $custom_family->save();

            # store ID association
            $family_association_hash->{$v5_family->family_id} = $custom_family->id;

            # associate sequences
            my $offset_seq = 0;
            my $v5_sequences = $v5_family->fetchSequences({'selectors' => {'filtered' => 0}, 'sql' => {'OFFSET' => $offset_seq, 'LIMIT' => $SEQUENCE_BATCH_SIZE} });
            while (@$v5_sequences)
            {
                my @custom_sequence_ids;
                foreach my $v5_sequence (@$v5_sequences)
                {
                    if (!exists($sequence_association_hash->{$v5_sequence->id}))
                    {
                        LogWarning("Custom sequence for sequence '$v5_sequence' (" . $v5_sequence->id . ") not available for family '$v5_family'!");
                    }
                    else
                    {
                        push(@custom_sequence_ids, $sequence_association_hash->{$v5_sequence->id});
                    }
                }
                
                if (@custom_sequence_ids)
                {
                    my $sql_query = "
                        INSERT INTO custom_families_sequences
                            (custom_family_id, custom_sequence_id)
                        VALUES (" . join('), (', map {$custom_family->id . ', ' . $_} @custom_sequence_ids) . ");
                    ";
                    # LogDebug("SQL Query: $sql_query");
                    $g_dbh->do($sql_query)
                        or confess LogError("Failed to insert custom family-sequence relationships (family: '$custom_family' , " . $custom_family->id . ")!\n" . $g_dbh->errstr);
                }
                else
                {
                    LogWarning("New custom family '$v5_family' has unassociated sequences!");
                }
                # get next batch of sequences
                $offset_seq += $SEQUENCE_BATCH_SIZE;
                $v5_sequences = $v5_family->fetchSequences({'selectors' => {'filtered' => 0}, 'sql' => {'OFFSET' => $offset_seq, 'LIMIT' => $SEQUENCE_BATCH_SIZE} });
            }
        } # end of foreach

        LogVerboseInfo("...done with current batch of families.");
        print GetProgress(
            {'label' => 'Processed families:', 'current' => $offset + scalar(@$v5_families), 'total' => $total_family_count,},
            80,
            5, # seconds
        );

        # get next batch of families
        $offset += $FAMILY_BATCH_SIZE;
        $v5_families = [Greenphyl::Family->new($g_dbh, {'selectors' => {'black_listed' => 0, 'level' => ['IN', 1, 2, 3, 4]}, 'sql' => {'OFFSET' => $offset, 'LIMIT' => $FAMILY_BATCH_SIZE}})];
    } # end of while
    if ($total_family_count)
    {
        # print 100%
        print GetProgress(
            {'label' => 'Processed families:', 'current' => $total_family_count, 'total' => $total_family_count,},
            80,
            0, # seconds
        ) . "\n";
    }
    LogInfo("...done processing v5 families.");
    LogInfo("Transfered families: " . scalar(keys(%$family_association_hash)));

    return $family_association_hash;
}


=pod

=head2 TransferFamilyRelationships

B<Description>: [function description]. #+++

B<ArgsCount>: [count of arguments] #+++

=over 4

=item $variable_name: ([variable nature]) ([requirements]) #+++ see below

#--- requirement can be:
#--- (R)=required,
#--- (O)=optional
#--- (U)=optional and must be undef if omitted
[variable description]. #+++

=item $variable_name2: ([variable nature]) ([requirements]) #+++

[variable description]. #+++

=back

B<Return>: ([return type]) #+++

[return description]. #+++

B<Example>:

    [example code] #+++

=cut

sub TransferFamilyRelationships
{
    my ($family_association_hash) = @_;

    LogInfo("Processing v5 family relationships...");

    my $total_family_count = scalar(keys(%$family_association_hash));
    my $family_counter = 0;
    my $previous_relationship_sh = $g_dbh->prepare("SELECT object_family_id, level_delta FROM family_relationships_cache WHERE subject_family_id = ?;");

FAMILY_RELATIONSHIP_TRANSFER_LOOP:
    foreach my $v5_family_id (keys(%$family_association_hash))
    {
        if (!exists($family_association_hash->{$v5_family_id}))
        {
            LogWarning("v5-v5 family association is missing for v5 family ID $v5_family_id (black_listed?)! No relationship can be transfered for that family!");
            next FAMILY_RELATIONSHIP_TRANSFER_LOOP;
        }

        $previous_relationship_sh->execute($v5_family_id);
        my $v5_relationships = $previous_relationship_sh->fetchall_arrayref();
        my @v5p_relationships;
CURRENT_FAMILY_RELATIONSHIP_LOOP:
        foreach my $v5_relationship (@$v5_relationships)
        {
            if (!exists($family_association_hash->{$v5_relationship->[0]}))
            {
                LogWarning("v5-v5 family association is missing for v5 family ID " . $v5_relationship->[0] . " (black_listed?)! Relationship with v5 family ID $v5_family_id will not be transfered!");
                next CURRENT_FAMILY_RELATIONSHIP_LOOP;
            }
            push(@v5p_relationships, "($family_association_hash->{$v5_family_id}, $family_association_hash->{$v5_relationship->[0]}, $v5_relationship->[1], '$Greenphyl::AbstractFamily::RELATIONSHIP_INHERITANCE')");
        }
        if (@v5p_relationships)
        {
            my $sql_query = "
                INSERT INTO custom_family_relationships
                    (subject_custom_family_id, object_custom_family_id, level_delta, type)
                VALUES "
                . join(', ', @v5p_relationships)
                . ";"
            ;
            # LogDebug("SQL Query: $sql_query");
            $g_dbh->do($sql_query)
                or LogError("Failed to transfer family relationships!\n" . $g_dbh->errstr);
        }

        print GetProgress(
            {'label' => 'Processed families:', 'current' => ++$family_counter, 'total' => $total_family_count,},
            80,
            2, # seconds
        );
    } # end of foreach
    if ($total_family_count)
    {
        # print 100%
        print GetProgress(
            {'label' => 'Processed families:', 'current' => $total_family_count, 'total' => $total_family_count,},
            80,
            0, # seconds
        ) . "\n";
    }
    LogInfo("...done processing v5 family relationships.");
}




# Script options
#################

=pod

=head1 OPTIONS

#--- describes parameters given to the script
#+++ command line syntax
#--- requirement of the option and its parameter can be:
#--- required: name or nature inside <>
#--- optional: name or nature inside []
#--- alternative between 2 elements: elements separated by a |

=head2 Parameters

=over 4

#+++=item B<-help>:
#+++
#+++Prints a brief help message and exits.
#+++
#+++=item B<-man>:
#+++
#+++Prints the manual page and exits.
#+++
#+++=item B<-debug> (integer):
#+++
#+++Executes the script in debug mode. The integer value is optional and is used
#+++to set debug level (use 0 to force disable debug mode).
#+++Default: 0 (not in debug mode).
#+++

=item B<[option_name]> ([option nature]): #+++

[option description]. #+++
Default: [option default value if one] #+++

#--- remove if log not used
#+++=item B<-log>:
#+++
#+++Enable logging.
#+++
#+++=item B<-log-*> (any):
#+++
#+++Logging arguments.

=back
#--- Example:
#---
#--- Template.pl [-help | -man]
#---
#--- Template.pl [-debug [debug_level]] [-size <width> [height]]
#---
#--- =over 4
#---
#--- =item B<-help>:
#---
#--- Prints a brief help message and exits.
#---
#--- =item B<-man>:
#---
#--- Prints the manual page and exits.
#---
#--- =item B<-debug> (integer):
#---
#--- Executes the script in debug mode. If an integer value is specified, it will
#--- be the debug level. If "-debug" option was used without specifying a debug
#--- level, level 1 is assumed.
#--- Default: 0 (not in debug mode).
#---
#---=item B<-size> (positive_real) (positive_real):
#---
#--- Set the dimensions of the object that will be drawn. The first value is
#--- the width; the height is the second value if specified, otherwise it will
#--- assume height and width are equal to the first value.
#--- Default: width and height are set to 1.
#---
#---=back

=cut


# CODE START
#############

# for regression tests
if ($ENV{'TESTING'} && $ENV{'TESTING_NO_RUN'})
{return 1;}

# options processing
my ($man, $help, $debug, $no_prompt) = (0, 0, undef, undef);

my ($nocleanup, $resumefamilies, $resumerelationships, $v5_user_login, $family_suffix, $seqmapping_file);

# parse options and print usage if there is a syntax error.
GetOptions('help|?'          => \$help,
           'man'             => \$man,
           'debug:s'         => \$debug,
           'q|noprompt'      => \$no_prompt,
           'nocleanup'       => \$nocleanup,
           'resumefamilies'  => \$resumefamilies,
           'resumerelationships' => \$resumerelationships,
           'seqmapping=s' => \$seqmapping_file,
           'account=s' => \$v5_user_login,
           'suffix=s' => \$family_suffix,
           @Greenphyl::Log::LOG_GETOPT, # skip log parameters
) or pod2usage(1);
if ($help) {pod2usage('-verbose' => 1, '-exitval' => 0);}
if ($man) {pod2usage('-verbose' => 2, '-exitval' => 0);}

# change debug mode if requested/forced
if (defined($debug))
{
    # just '-debug' ?
    if ($debug eq '')
    {
        $debug = 1;
    }
    $DEBUG = $debug;
}

if (!$v5_user_login)
{
    $v5_user_login     ||= Prompt("What will be the v5 account for the import?", { 'default' => 'greenphyl_v5', 'constraint' => '\w', }, $no_prompt);
}

if (!$family_suffix)
{
    $family_suffix     ||= Prompt("What suffix to add to family names?", { 'default' => 'V5', 'constraint' => '\w', }, $no_prompt);
    # Remove leading underscores.
    $family_suffix =~ s/^_+//;
}

eval
{
    # Get v5 user
    $g_v5_user = Greenphyl::User->new($g_dbh, {'selectors' => {'login' => $v5_user_login}});
    if (!$g_v5_user)
    {
        confess LogError("Failed to load GreenPhyl v5 user!");
    }
    else
    {
        LogVerboseInfo("GreenPhyl v5 User found in database.");
    }

    my $sql_query;
    # clean previous data
    if (!$nocleanup)
    {
        if (!$resumerelationships)
        {
            LogInfo("Removing previous custom v5 families...");
            $sql_query = 'DELETE FROM custom_families WHERE user_id = ' . $g_v5_user->id . ';';
            if (!$g_dbh->do($sql_query))
            {
                LogWarning('Failed to remove previous v5 families!');
            }
            LogInfo("...done clearing v5 families.");
        }

        if (!$resumefamilies && !$resumerelationships)
        {
            LogInfo("Removing previous custom v5 sequences.");
            $sql_query = 'DELETE FROM custom_sequences WHERE user_id = ' . $g_v5_user->id . ';';
            if (!$g_dbh->do($sql_query))
            {
                LogWarning('Failed to remove previous v5 sequences!');
            }
            LogInfo("...done clearing v5 sequences.");
        }
    }

    my $sequence_association_hash =  TransferSequences();
    my $family_association_hash = TransferFamilies($sequence_association_hash, $family_suffix);
    TransferFamilyRelationships($family_association_hash);

};

# catch
HandleErrors();

exit(0);

# CODE END
###########


=pod

=head1 AUTHORS

Mathieu ROUARD (Bioversity), m.rouard@cgiar.org

Valentin GUIGNON (Bioversity), v.guignon@cgiar.org

Matthieu CONTE (Syngenta)

Sebastien BRIOIS (Syngenta)

=head1 VERSION

Version 1.0.0

Date 18/07/2022

=head1 SEE ALSO

GreenPhyl documentation.

=cut
