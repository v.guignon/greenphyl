#!/bin/sh
genomes="AMBTC_v1.0 ARATH_Araport11 BETVU_RefBeet_v1.2 BRADI_ABR2_337_v1.ABR2 BRADI_ABR3_343_v1.ABR3 BRADI_ABR4_364_v1.ABR4 BRADI_ABR5_379_v1.ABR5 BRADI_ABR6_336_v1.ABR6_r BRADI_ABR7_369_v1.ABR7 BRADI_ABR8_356_v1.ABR8 BRADI_ABR9_333_v1.ABR9_r BRADI_Adi_10_381_v1.Adi-10 BRADI_Adi_12_359_v1.Adi-12 BRADI_Adi_2_372_v1.Adi-2 BRADI_Arn1_355_v1.Arn1 BRADI_Bd18_1_362_v1.Bd18-1 BRADI_Bd1_1_349_v1.Bd1-1 BRADI_Bd21_3_378_v1.Bd21-3_r BRADI_Bd21v2_1_283_Bd21v2 BRADI_Bd29_1_346_v1.Bd29-1 BRADI_Bd2_3_353_v1.Bd2-3 BRADI_Bd30_1_344_v1.Bd30-1 BRADI_Bd3_1_328_v1.Bd3-1_r BRADI_BdTR10c_374_v1.BdTR10C BRADI_BdTR11a_380_v1.BdTR11A BRADI_BdTR11g_357_v1.BdTR11G BRADI_BdTR11i_363_v1.BdTR11I BRADI_BdTR12c_352_v1.BdTR12c BRADI_BdTR13a_334_v1.BdTR13a BRADI_BdTR13c_365_v1.BdTR13C BRADI_BdTR1i_345_v1.BdTR1i BRADI_BdTR2b_376_v1.BdTR2B BRADI_BdTR2g_367_v1.BdTR2G BRADI_BdTR3c_354_v1.BdTR3C BRADI_BdTR5i_370_v1.BdTR5I BRADI_BdTR7a_329_v1.BdTR7a BRADI_BdTR8i_348_v1.BdTR8i BRADI_BdTR9k_358_v1.BdTR9K BRADI_Bis_1_338_v1.Bis-1 BRADI_Foz1_366_v1.Foz1 BRADI_Gaz_8_339_v1.Gaz-8 BRADI_Jer1_375_v1.Jer1 BRADI_Kah_1_351_v1.Kah-1 BRADI_Kah_5_342_v1.Kah-5 BRADI_Koz_1_330_v1.Koz-1 BRADI_Koz_3_368_v1.Koz-3 BRADI_Luc1_347_v1.Luc1 BRADI_Mig3_377_v1.Mig3 BRADI_Mon3_350_v1.Mon3 BRADI_Mur1_373_v1.Mur1 BRADI_Per1_326_v1.Per1 BRADI_Ron2_360_v1.RON2 BRADI_S8iiC_340_v1.S8iiC BRADI_Sig2_371_v1.Sig2 BRADI_Tek_2_341_v1.Tek-2 BRADI_Tek_4_332_v1.Tek-4 BRADI_Uni2_327_v1.Uni2 BRANA_Darmor_v8.1 BRANA_Tapidor_v6.3 BRANA_v5.0 BRAOL_A2_v1.0 BRAOL_HDEM BRAOL_TO1000 BRARR_Z1 BRARR_v3.0 CAJCA_Asha_v1.0 CAPAN_CM334_v2.0 CAPAN_Glabriusculum_Chiltepin_v2.0 CAPAN_Zunla_1_v2.0 CHEQI_JGI_v1.0 CICAR_CDC_Frontier_kabuli CICAR_ICC_4958_desi CITMA_v1.0 CITME_v1.0 CITSI_v2.0 COCNU_C3B02_v2.0 COCNU_CATD COFAR_RB_v1.0 COFCA_v1.0 CUCME_DHL92_v3.6.1 CUCSA_ASM407_v2.0 CUCSA_Gy14_v2.0 CUCSA_PI183967 DAUCA_JGI_v2.0 DIORT_TDr96_F1_v1.0 ELAGV_RefSeq_v1.0 FRAVE_v4.0a1 HELAN_XRQ_v1.2 HORVU_IBSC_v2.0 IPOTF_NSP306 IPOTF_Y22 IPOTR_NSP323 MAIZE_B73_v4.0 MAIZE_CML247_v1.0 MAIZE_Mo17_v1.0 MAIZE_PH207_v1.0 MALDO_Borkh_v3.0.a1 MALDO_GDDH13_v1.1 MALDO_HFTH1 MANES_AM560_2_v6.1 MEDTR_A17_r5.0 MEDTR_Mt4.0v2 MUSAC_Macba2 MUSAC_Macbu1 MUSAC_Macma2 MUSAC_Macze1 MUSBA_Mba1.1 OLEEU_v1.0 ORYGL_v1.0 ORYSA_IR8_v1.0 ORYSA_Kitaake_v3.1 ORYSA_MH63_v1.0 ORYSA_N22_v1.0 ORYSA_Nipponbare_v7.0 ORYSA_R498_IGDB_v3.0 ORYSA_ZS97_v1.0 PHAVU_v2.0 PHODC_v3.0 SACSP_AP85_441_v1.0 SOLLC_ITAG3.2 SOLTU_PGSC_v4.03 SORBI_BTx623_v3.1.1 SORBI_Tx430_v1.0 SOYBN_Wm82_a2_v1.0 SOYBN_ZH13 THECC_Criollo_v2.0 THECC_Matina1_6_v1.1 TRITU_Svevo_v2.0 TRITU_Zavitan_v1.1 VITVI_12X.2 VITVI_CabernetSauvignon_v1.0 VITVI_Carmenere_v1.0"
pangenomes="BRADI BRANA BRAOL BRARR CAPAN CICAR COCNU CUCSA IPOTF MAIZE MALDO MEDTR MUSAC ORYSA SORBI SOYBN THECC TRITU VITVI"

for a in $genomes
do
  b=${a:0:5}
  echo "Init genome $a ($b)..."
  if [ ! -e "../genome_sources/$b/$a.faa" ]; then
    echo "Error: Missing genome FASTA ../genome_sources/$b/$a.faa. Exiting."
    exit 1
  fi
  if [ -d "$a" ]; then
    echo "Warning: $a already exists. Skipping..."
    continue
  fi
  mkdir $a
  if [ ! -d "$a" ]; then
    echo "Error: Failed to create $a directory. Exiting."
    exit 1
  fi
  cd $a
  perl ~droc/Genome/cc2-login/converter/split_fasta.pl --file  ../../genome_sources/$b/$a.faa
  perl -p -i.bak -e 's/[^gpavlimcfywhkrqnedst\n\r]+//gi if $_ !~ /^>/' fasta*.fna
  for i in $PWD/*fna; do echo qsub -b y -q bigmem.q -l mem_free=24G -N IPR$a  -V interproscan.sh -i $i -cpu 8 -dp --iprlookup --pathways --goterms -d /gs7k1/projects/greenphyl/data/v5/ipr/$a/ -T /work/interproscan/temp >> iprscan.sh;done
  chmod +x iprscan.sh
  cd ..
done

for a in $pangenomes
do
  echo "Init pangenome $a"
  if [ ! -e "../pangenomes/$a/${a}_complete.fasta" ]; then
    echo "Error: Missing pangenome FASTA ../pangenomes/$a/${a}_complete.fasta. Exiting."
    exit 1
  fi
  if [ -d "$a" ]; then
    echo "Warning: $a already exists. Skipping..."
    continue
  fi
  mkdir $a
  if [ ! -d "$a" ]; then
    echo "Error: Failed to create $a directory. Exiting."
    exit 1
  fi
  cd $a
  perl ~droc/Genome/cc2-login/converter/split_fasta.pl --file  ../../pangenomes/$a/${a}_complete.fasta
  perl -p -i.bak -e 's/[^gpavlimcfywhkrqnedst\n\r]+//gi if $_ !~ /^>/' fasta*.fna
  for i in $PWD/*fna; do echo qsub -b y -q bigmem.q -l mem_free=24G -N IPR$a  -V interproscan.sh -i $i -cpu 8 -dp --iprlookup --pathways --goterms -d /gs7k1/projects/greenphyl/data/v5/ipr/$a/ -T /work/interproscan/temp >> iprscan.sh;done
  chmod +x iprscan.sh
  cd ..
done

module load bioinfo/interproscan/5.41-78.0

for a in $genomes
do
  echo "Launch IPR scan for genome $a..."
  cd $a
    ./iprscan.sh
  cd ..
done

for a in $pangenomes
do
  echo "Launch IPR scan for pangenome $a..."
  cd $a
    ./iprscan.sh
  cd ..
done
