#!/usr/bin/env perl

=pod

=head1 NAME

load_ext_db_ref.pl - Loads external database references

=head1 SYNOPSIS

    load_ext_db_ref.pl

=head1 REQUIRES

Perl5, GreenPhyl API v3

=head1 DESCRIPTION

This script updates the following table:
-db

=cut

use strict;
use warnings;

use FindBin;
use lib "$FindBin::Bin/../../lib";
use lib "$FindBin::Bin/../../local_lib";
use Carp qw (cluck confess croak);

use Getopt::Long;
use Pod::Usage;

use Greenphyl;
use Greenphyl::Log('nolog' => 1,);
use Greenphyl::SourceDatabase;

++$|; #no buffering




# Script global constants
##########################

=pod

=head1 CONSTANTS

B<$REV_STRING>: (string)

Current revision (auto-set by GIT).

B<$DEBUG>: (boolean)

When set to true, it enables debug mode.

B<$DB_INSERT_QUERY>: (string)

External database references insertion query.

=cut

# SVN management part
my $REV_STRING = '$id$';

our $DEBUG = 0;
our $DB_INSERT_QUERY = qq{
  INSERT INTO db (id, name, description, query_url_prefix, site_url) VALUES
      (1, '$Greenphyl::SourceDatabase::SOURCE_DATABASE_UNIPROT', NULL, 'http://www.uniprot.org/uniprot/',                                     'http://www.uniprot.org/')
    , (2, '$Greenphyl::SourceDatabase::SOURCE_DATABASE_PUBMED',  NULL, 'http://www.ncbi.nlm.nih.gov/sites/entrez?db=pubmed&cmd=search&term=', 'http://www.ncbi.nlm.nih.gov/pubmed/')
    , (3, '$Greenphyl::SourceDatabase::SOURCE_DATABASE_KEGG',    NULL, 'http://www.genome.jp/dbget-bin/www_bget?ko:',                         'http://www.genome.jp/kegg/kegg2.html')
    , (4, '$Greenphyl::SourceDatabase::SOURCE_DATABASE_EC',      NULL, 'http://www.genome.jp/dbget-bin/www_bget?enzyme+',                     'http://www.genome.jp/kegg/kegg2.html')
    , (5, '$Greenphyl::SourceDatabase::SOURCE_DATABASE_GO',      NULL, 'http://amigo.geneontology.org/amigo/term/',                           'http://www.geneontology.org/')
    , (6, '$Greenphyl::SourceDatabase::SOURCE_DATABASE_IPR',     NULL, 'https://www.ebi.ac.uk/interpro/entry/',                               'https://www.ebi.ac.uk/interpro/')
  ON DUPLICATE KEY UPDATE
    description      = VALUES(description),
    query_url_prefix = VALUES(query_url_prefix),
    site_url         = VALUES(site_url)
  ;
};


# Script global variables
##########################

=pod

=head1 VARIABLES

B<$g_dbh>: (DBI::db)

Global database handler.

=cut

my $g_dbh = GetDatabaseHandler();




# Script options
#################

=pod

=head1 OPTIONS

    load_ext_db_ref.pl [-help|-man]
    load_ext_db_ref.pl [-debug] [db_index=<db_index>]

=head2 Parameters

=over 4

=item B<-help>:

Prints a brief help message and exits.

=item B<-man>:

Prints the manual page and exits.

=item B<-debug> (integer):

Executes the script in debug mode.

=item B<db_index> (integer):

Index of the database to use (see Greenphyl::Config).
Default: 0

=back

=cut


# CODE START
#############

# for regression tests
if ($ENV{'TESTING'} && $ENV{'TESTING_NO_RUN'})
{return 1;}

# options processing
my ($man, $help, $debug) = (0, 0, undef, undef);
# parse options and print usage if there is a syntax error.
GetOptions('help|?'           => \$help,
           'man'              => \$man,
           'debug:s'          => \$debug,
           @Greenphyl::Log::LOG_GETOPT, # skip log parameters
) or pod2usage(1);
if ($help) {pod2usage('-verbose' => 1, '-exitval' => 0);}
if ($man) {pod2usage('-verbose' => 2, '-exitval' => 0);}

# change debug mode if requested/forced
if (defined($debug))
{
    # just '-debug' ?
    if ($debug eq '')
    {
        $debug = 1;
    }
    $DEBUG = $debug;
}

eval
{
    LogInfo("Loading/updating DB...");

#    # start SQL transaction
#    if ($g_dbh->{'AutoCommit'})
#    {
#        $g_dbh->begin_work() or croak $g_dbh->errstr;
#    }

    LogDebug("Running in DEBUG mode, no database commit will be issued!");

    my $sql_query = $DB_INSERT_QUERY;
    LogDebug("SQL QUERY: $sql_query");
    if (!$g_dbh->do($sql_query))
    {
        LogWarning("Failed to insert/update external database references!" . $g_dbh->errstr);
    }

#    if (!$DEBUG)
#    {SaveCurrentTransaction($g_dbh);}

    LogInfo("Done.");
};

HandleErrors();

exit(0);

# CODE END
###########


=pod

=head1 AUTHORS

Mathieu ROUARD (Bioversity), m.rouard@cgiar.org

Valentin GUIGNON (Bioversity), v.guignon@cgiar.org

Matthieu CONTE (Syngenta)

Sebastien BRIOIS (Syngenta)

=head1 VERSION

Version 1.0.0

Date 22/10/2013

=head1 SEE ALSO

GreenPhyl documentation.

=cut
