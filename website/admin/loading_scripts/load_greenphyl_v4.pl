#!/usr/bin/env perl

=pod

=head1 NAME

load_greenphyl_v4.pl - Loads GreenPhyl v4 sequences and families into v5 DB

=head1 SYNOPSIS

    load_greenphyl_v4.pl

=head1 REQUIRES

Perl5, GreenPhyl v4 database

=head1 DESCRIPTION

Loads GreenPhyl v4 sequences and families into v4 DB as custom sequences and
families. Extra family-data is stored as a frozen Perl structure into the
custom_families.data field. See Greenphyl::CustomFamily for details.

=cut

use strict;
use warnings;

use FindBin;
use lib "$FindBin::Bin/../../lib";
use lib "$FindBin::Bin/../../local_lib";
use Carp qw (cluck confess croak);

use Greenphyl;
use Greenphyl::Log('nolog' => 1,);
use Greenphyl::User;
use Greenphyl::ExtV4Species;
use Greenphyl::Genome;
use Greenphyl::Sequence;
use Greenphyl::ExtV4Sequence;
use Greenphyl::Tools::Sequences;
use Greenphyl::AbstractFamily;
use Greenphyl::ExtV4Family;

use Getopt::Long;
use Pod::Usage;

use Storable qw(nfreeze);

++$|; #no buffering




# Script global constants
##########################

=pod

=head1 CONSTANTS

B<$REV_STRING>: (string)

Current revision (auto-set by GIT).

B<$DEBUG>: (boolean)

When set to true, it enables debug mode.

B<$CONSTANT_NAME>: ([constant nature]) #+++

[constant description and use]. #+++

=cut

# SVN management part
my $REV_STRING = '$id$';

our $DEBUG = 0;
our $SEQUENCE_BATCH_SIZE = 5000;
our $FAMILY_BATCH_SIZE   = 1000;

our %TYPE_CONVERSION_HASH = (
    '0' => 'N/A',
    '1' => 'superfamily',
    '2' => 'family',
    '3' => 'subfamily',
    '4' => 'group',
);

our %INFERENCE_CONVERSION_HASH = (
    ''                 => '',
    ' '                => '',
    'IEA:INTERPRO'     => 'IEA:InterPro',
    'IEA:KEGG'         => 'IEA:KEGG',
    'IEA:MEROPS'       => 'IEA:MEROPS',
    'IEA:PIRSF'        => 'IEA:PIRSF',
    'IEA:TF'           => 'IEA:TF',
    'IEA:UNIPROTKB'    => 'IEA:UniProtKB',
    'IEA:UNIPROTKB-KW' => 'IEA:UniProtKB-KW',
    'N/A'              => '',
    'OTHER'            => 'Other',
    'PUBMED'           => 'PubMed',
    'TAIR/TIGR'        => 'TAIR/TIGR',
);

our $FAMILY_SUFFIX = '_V4';


our %GENOME_MAPPING = (
    'AMBTC' => 'AMBTC_v1.0', # Amborella trichopoda
    'ARATH' => 'ARATH_Araport11', # Arabidopsis thaliana
    'BRADI' => 'BRADI_Bd21v2_1_283_Bd21v2', # Brachypodium distachyon
    'CAJCA' => 'CAJCA_Asha_v1.0', # Cajanus cajan
    # 'CARPA' => '', # Carica papaya
    # 'CHLRE' => '', # Chlamydomonas reinhardtii
    'CICAR' => 'CICAR_CDC_Frontier_kabuli', # Cicer arietinum
    'CITSI' => 'CITSI_v2.0', # Citrus sinensis
    'COFCA' => 'COFCA_v1.0', # Coffea canephora
    'CUCSA' => 'CUCSA_ASM407_v2.0', # Cucumis sativus
    # 'CYAME' => '', # Cyanidioschyzon merolae
    'ELAGV' => 'ELAGV_RefSeq_v1.0', # Elaeis guineensis
    'GLYMA' => 'SOYBN_Wm82_a2_v1.0', # Glycine max
    # 'GOSRA' => '', # Gossypium raimondii
    'HORVU' => 'HORVU_IBSC_v2.0', # Hordeum vulgare
    # 'LOTJA' => '', # Lotus japonicus
    'MABAN' => 'MUSAC_Macba2', # Musa acuminata banksii Banksii
    'MABUR' => 'MUSAC_Macbu1', # Musa acuminata burmanica Calcutta 4
    'MALDO' => 'MALDO_Borkh_v3.0.a1', # Malus domestica
    'MANES' => 'MANES_AM560_2_v6.1', # Manihot esculenta
    'MAZEB' => 'MUSAC_Macze1', # Musa acuminata zebrina Maia Oa
    'MEDTR' => 'MEDTR_Mt4.0v2', # Medicago truncatula
    'MUSAC' => 'MUSAC_Macma2', # Musa acuminata malaccensis DH Pahang
    'MUSBA' => 'MUSBA_Mba1.1', # Musa balbisiana Pisang Klutuk Wulung
    'ORYSA' => 'ORYSA_Nipponbare_v7.0', # Oryza sativa
    # 'OSTTA' => '', # Ostreococcus tauri
    'PHAVU' => 'PHAVU_v2.0', # Phaseolus vulgaris
    'PHODA' => 'PHODC_v3.0', # Phoenix dactylifera
    'PHODC' => 'PHODC_v3.0', # Phoenix dactylifera
    # 'PHYPA' => '', # Physcomitrella patens
    # 'PICAB' => '', # Picea abies
    # 'POPTR' => '', # Populus trichocarpa
    # 'RICCO' => '', # Ricinus communis
    # 'SELMO' => '', # Selaginella moellendorffii
    # 'SETIT' => '', # Setaria italica
    'SOLLY' => 'SOLLC_ITAG3.2', # Solanum lycopersicum
    'SOLLC' => 'SOLLC_ITAG3.2', # Solanum lycopersicum
    'SOLTU' => 'SOLTU_PGSC_v4.03', # Solanum tuberosum
    'SORBI' => 'SORBI_BTx623_v3.1.1', # Sorghum bicolor
    'THECC' => 'THECC_Criollo_v2.0', # Theobroma cacao
    'VITVI' => 'VITVI_12X.2', # Vitis vinifera
    'ZEAMA' => 'MAIZE_B73_v4.0', # Zea mays
    'MAIZE' => 'MAIZE_B73_v4.0', # Zea mays
);


# Script global variables
##########################

=pod

=head1 VARIABLES

B<[g_variable_name]>: ([variable nature]) #+++

[variable description, use and default value]. #+++
Default: [variable default value if one] #+++

#--- Example:
#--- B<$g_output_method>: (integer)
#---
#--- used to store current output method;
#--- 0=raw text (default), 1=image, 2=html.
#---
#---     ...
#---
#--- my $g_output_method = 0;
#---

=cut

our $g_dbh = GetDatabaseHandler();
our $g_v4_user;




# Script global functions
##########################

=pod

=head1 FUNCTIONS

=head2 FindAssociatedSequence

B<Description>: [function description]. #+++

B<ArgsCount>: [count of arguments] #+++

=over 4

=item $variable_name: ([variable nature]) ([requirements]) #+++ see below

#--- requirement can be:
#--- (R)=required,
#--- (O)=optional
#--- (U)=optional and must be undef if omitted
[variable description]. #+++

=item $variable_name2: ([variable nature]) ([requirements]) #+++

[variable description]. #+++

=back

B<Return>: ([return type]) #+++

[return description]. #+++

B<Example>:

    [example code] #+++

=cut

sub FindAssociatedSequence
{
    my ($v4_sequence, $v5_genome, $sequence_mapping) = @_;

    $sequence_mapping ||= {};

    my $v4p_sequence;
    # Check if a mapping has already been provided.
    if (exists($sequence_mapping->{$v4_sequence->accession}))
    {
        if ($sequence_mapping->{$v4_sequence->accession})
        {
            $v4p_sequence = Greenphyl::Sequence->new($g_dbh, {'selectors' => {'accession' => $sequence_mapping->{$v4_sequence->accession},}});
        }
        else
        {
            LogInfo("No associated sequence for v4 sequence " . $v4_sequence);
            return undef;
        }
    }

    my $sql_query;
    # -try by accession
    if (!$v4p_sequence)
    {
       $v4p_sequence = Greenphyl::Sequence->new($g_dbh, {'selectors' => {'accession' => ['LIKE', $v4_sequence->accession],}});
    }

    # if (!$v4p_sequence)
    # {
    #     # -try using previous accessions
    #     LogDebug(" trying with previous accessions");
    #     $sql_query = "SELECT sequence_id FROM sequence_transfer_history WHERE old_accession LIKE ?;";
    #     my ($sequence_id) = $g_dbh->selectrow_array($sql_query, undef, $v4_sequence->accession);
    #     # confess LogError("Failed to query sequence_transfer_history table for v4 sequence '$v4_sequence'!\n" . $g_dbh->errstr);
    #     if ($sequence_id)
    #     {
    #         $v4p_sequence = Greenphyl::Sequence->new($g_dbh,
    #             {'selectors' => {'id'  => $sequence_id,},},
    #         );
    #     }
    # }

    if (!$v4p_sequence)
    {
        # -try using synonyms
        LogDebug(" trying with synonyms");
        $sql_query = "SELECT sequence_id FROM sequence_synonyms WHERE synonym LIKE ?";
        # also use v4 synonyms
        if (@{$v4_sequence->synonyms})
        {
            $sql_query .= ' OR synonym LIKE ?' x scalar(@{$v4_sequence->synonyms});
        }
        my ($sequence_id) = $g_dbh->selectrow_array($sql_query, undef, $v4_sequence->accession, @{$v4_sequence->synonyms});
        # confess LogError("Failed to query sequence_synonyms table for v4 sequence '$v4_sequence'!\n" . $g_dbh->errstr);
        if ($sequence_id)
        {
            $v4p_sequence = Greenphyl::Sequence->new($g_dbh,
                {'selectors' => {'id'  => $sequence_id,},},
            );
        }
    }

    if (!$v4p_sequence)
    {
        # -try by content and species
        LogDebug(" trying with sequence content");
        $v4p_sequence = Greenphyl::Sequence->new($g_dbh,
            {
                'selectors' =>
                {
                    'genome_id'  => $v5_genome->id,
                    'polypeptide' => $v4_sequence->polypeptide,
                },
            },
        );
    }

    if (!$v4p_sequence)
    {
        LogInfo("No associated sequence found for v4 sequence " . $v4_sequence);
    }
    else
    {
        LogInfo("Got associated v5 sequence " . $v4p_sequence . " for v4 sequence " . $v4_sequence);
    }

    return $v4p_sequence;
}




=pod

=head2 ExtractFamilyData

B<Description>: [function description]. #+++

B<ArgsCount>: [count of arguments] #+++

=over 4

=item $variable_name: ([variable nature]) ([requirements]) #+++ see below

#--- requirement can be:
#--- (R)=required,
#--- (O)=optional
#--- (U)=optional and must be undef if omitted
[variable description]. #+++

=item $variable_name2: ([variable nature]) ([requirements]) #+++

[variable description]. #+++

=back

B<Return>: ([return type]) #+++

[return description]. #+++

B<Example>:

    [example code] #+++

=cut

sub ExtractFamilyData
{
    my ($v4_family) = @_;

    my $synonyms = $v4_family->synonyms;

    my $dbxrefs = [];
    foreach my $v4_dbxref (@{$v4_family->dbxref})
    {
        my $dbxref_hash = $v4_dbxref->getHashValue();
        $dbxref_hash->{'db'} = $v4_dbxref->db->name;
        push(@$dbxrefs, $dbxref_hash);
    }

    # freeze data for database storing
    my $family_data = {
        'family_v4' => $v4_family,
        'synonyms'  => $synonyms,
        'dbxref'    => $dbxrefs,
    };

    return $family_data;
}


=pod

=head2 GetSequenceAssociation

B<Description>: [function description]. #+++

B<ArgsCount>: [count of arguments] #+++

=over 4

=item $variable_name: ([variable nature]) ([requirements]) #+++ see below

#--- requirement can be:
#--- (R)=required,
#--- (O)=optional
#--- (U)=optional and must be undef if omitted
[variable description]. #+++

=item $variable_name2: ([variable nature]) ([requirements]) #+++

[variable description]. #+++

=back

B<Return>: ([return type]) #+++

[return description]. #+++

B<Example>:

    [example code] #+++

=cut

sub GetSequenceAssociation
{
    my ($v4_dbh) = @_;

    # get sequence count
    my $sql_query = "SELECT COUNT(1) FROM sequences WHERE filtered = 0;";
    my ($total_sequence_count) = $v4_dbh->selectrow_array($sql_query);

    LogInfo("Preparing v4.accession-v4.id assocation hash...");
    $sql_query = "SELECT accession, id FROM custom_sequences WHERE user_id = " . $g_v4_user->id . ";";
    my $accession_to_id_array = $g_dbh->selectcol_arrayref($sql_query, { 'Columns' => [1, 2], });
    my %accession_to_id = @$accession_to_id_array;
    LogInfo("...assocation hash done.");

    LogInfo("Processing v4 sequences...");
    my $sequence_association_hash = {};
    my ($missing_sequence_species, $already_inserted, $no_association) = (0, 0, 0);
    my $offset = 0;
    my $v4_sequences = [Greenphyl::ExtV4Sequence->new($v4_dbh, {'selectors' => {'filtered' => 0}, 'sql' => {'OFFSET' => $offset, 'LIMIT' => $SEQUENCE_BATCH_SIZE}})];

    if (!@$v4_sequences)
    {
        LogError("No sequence found in v4 database!");
        return;
    }

    while (@$v4_sequences)
    {
        LogVerboseInfo("Working on sequences [$offset, " . ($offset + $#$v4_sequences) . "]...");
        foreach my $v4_sequence (@$v4_sequences)
        {
            LogVerboseInfo("-working on sequence '" . $v4_sequence->accession . "'");
            $sequence_association_hash->{$v4_sequence->id} = $accession_to_id{$v4_sequence->accession};
        } # end of foreach

        LogVerboseInfo("...done with current batch of sequences.");
        print GetProgress(
            {'label' => 'Processed sequences:', 'current' => $offset + scalar(@$v4_sequences), 'total' => $total_sequence_count,},
            80,
            1, # seconds
        );

        # get next batch of sequences
        $offset += $SEQUENCE_BATCH_SIZE;
        $v4_sequences = [Greenphyl::ExtV4Sequence->new($v4_dbh, {'selectors' => {'filtered' => 0}, 'sql' => {'OFFSET' => $offset, 'LIMIT' => $SEQUENCE_BATCH_SIZE}})];
    } # end of while

    if ($total_sequence_count)
    {
        # print 100%
        print GetProgress(
            {'label' => 'Processed sequences:', 'current' => $total_sequence_count, 'total' => $total_sequence_count,},
            80,
            0, # seconds
        ) . "\n";
    }
    LogInfo("...done processing v4 sequences.");
    LogInfo("Transfered sequences: " . scalar(keys(%$sequence_association_hash)) . "\nUntransfered sequences (missing species): $missing_sequence_species\nUntransfered sequences (duplicates): $already_inserted\nSequences without association: $no_association");

    return $sequence_association_hash;
}


=pod

=head2 GetSpeciesMapping

B<Description>: [function description]. #+++

B<ArgsCount>: [count of arguments] #+++

=over 4

=item $variable_name: ([variable nature]) ([requirements]) #+++ see below

#--- requirement can be:
#--- (R)=required,
#--- (O)=optional
#--- (U)=optional and must be undef if omitted
[variable description]. #+++

=item $variable_name2: ([variable nature]) ([requirements]) #+++

[variable description]. #+++

=back

B<Return>: ([return type]) #+++

[return description]. #+++

B<Example>:

    [example code] #+++

=cut

sub GetSpeciesMapping
{
    my ($v4_dbh, $genome_mapping) = @_;

    LogInfo("Get species mapping...");
    my $species_mapping = {};
    my @v4_species_list = (Greenphyl::ExtV4Species->new($v4_dbh, {'selectors' => {'sequence_count' => ['>', 0]}}));

    foreach my $v4_species (@v4_species_list)
    {
        my $species = Greenphyl::Species->new(
            $g_dbh,
            {
                'selectors' => {
                    'code' => $v4_species->code,
                },
            }
        );

        if (!$species && exists($genome_mapping->{$v4_species->id}))
        {
            # node found through species code, try using genome mapping.
            $species = $genome_mapping->{$v4_species->id}->species;
        }

        if (!$species)
        {
            confess LogError("Failed to map species " . $v4_species->code . "!");
        }
        $species_mapping->{$v4_species->id} = $species;
    }

    return $species_mapping;
}


=pod

=head2 GetGenomeMapping

B<Description>: [function description]. #+++

B<ArgsCount>: [count of arguments] #+++

=over 4

=item $variable_name: ([variable nature]) ([requirements]) #+++ see below

#--- requirement can be:
#--- (R)=required,
#--- (O)=optional
#--- (U)=optional and must be undef if omitted
[variable description]. #+++

=item $variable_name2: ([variable nature]) ([requirements]) #+++

[variable description]. #+++

=back

B<Return>: ([return type]) #+++

[return description]. #+++

B<Example>:

    [example code] #+++

=cut

sub GetGenomeMapping
{
    my ($v4_dbh) = @_;

    LogInfo("Get genome mapping...");
    my $genome_mapping = {};
    my $selectors = {'display_order' => ['>', 0],};
    my @v4_species_list = (Greenphyl::ExtV4Species->new(
        $v4_dbh,
        {
            'selectors' => $selectors,
        },
    ));

    foreach my $v4_species (@v4_species_list)
    {
        if (exists($GENOME_MAPPING{$v4_species->code}))
        {
            my $genome = Greenphyl::Genome->new(
                $g_dbh,
                {
                    'selectors' => {
                        'accession' => $GENOME_MAPPING{$v4_species->code},
                    },
                }
            );
            if (!$genome)
            {
                confess LogError("Failed to map genome (" . $GENOME_MAPPING{$v4_species->code} . ") for species " . $v4_species->code . "!");
            }

            $genome_mapping->{$v4_species->id} = $genome;
        }
    }

    return $genome_mapping;
}


=pod

=head2 TransferSequences

B<Description>: [function description]. #+++

B<ArgsCount>: [count of arguments] #+++

=over 4

=item $variable_name: ([variable nature]) ([requirements]) #+++ see below

#--- requirement can be:
#--- (R)=required,
#--- (O)=optional
#--- (U)=optional and must be undef if omitted
[variable description]. #+++

=item $variable_name2: ([variable nature]) ([requirements]) #+++

[variable description]. #+++

=back

B<Return>: ([return type]) #+++

[return description]. #+++

B<Example>:

    [example code] #+++

=cut

sub TransferSequences
{
    my ($v4_dbh, $species_mapping, $genome_mapping, $sequence_mapping) = @_;

    $sequence_mapping ||= {};

    LogInfo("Processing v4 sequences...");
    # get sequence count
    my $sql_query = "SELECT COUNT(1) FROM sequences WHERE filtered = 0;";
    my ($total_sequence_count) = $v4_dbh->selectrow_array($sql_query);

    my ($missing_sequence_species, $already_inserted, $no_association) = (0, 0, 0);
    my $sequence_association_hash = {};
    my $progress = 0;
    foreach my $species_id (sort(keys(%$species_mapping)))
    {
        LogVerboseInfo("Working on species ID " . $species_id);
        my $offset = 0;
        # Do not load v5 fields (type, representative, genome_id).
        my $v4_sequences = [Greenphyl::ExtV4Sequence->new($v4_dbh, {'selectors' => {'filtered' => 0, 'species_id' => $species_id}, 'sql' => {'LIMIT' => $SEQUENCE_BATCH_SIZE, 'OFFSET' => $offset}})];
        if (!@$v4_sequences)
        {
            LogError("No sequence found in v4 database!");
            return;
        }
        while (@$v4_sequences)
        {
            ++$progress;
            LogVerboseInfo("Working on sequences [$offset, " . ($offset + $#$v4_sequences) . "] of species $species_id...");
SEQUENCE_TRANSFER_LOOP:
            foreach my $v4_sequence (@$v4_sequences)
            {
                LogVerboseInfo("-working on sequence '" . $v4_sequence->accession . "'");

                # make sure we don't process twice the same sequence
                if (exists($sequence_association_hash->{$v4_sequence->id}))
                {
                    ++$already_inserted;
                    LogWarning("Sequence '$v4_sequence' (" . $v4_sequence->id . ") already processed!");
                    next SEQUENCE_TRANSFER_LOOP;
                }

                # Make sure sequence does not already exist.
                my $custom_sequence = Greenphyl::CustomSequence->new(
                    $g_dbh,
                    {
                        'selectors' =>
                        {
                            'accession'       => $v4_sequence->accession,
                            'species_id'      => $species_mapping->{$v4_sequence->species_id}->id,
                            'user_id'         => $g_v4_user->id,
                        },
                    },
                );
                if (!$custom_sequence)
                {
                    # create a new v4 custom_sequence object associated to GreenPhyl v4 user
                    $custom_sequence = Greenphyl::CustomSequence->new(
                        $g_dbh,
                        {
                            'load' => 'none',
                            'members' => {
                                'accession'       => $v4_sequence->accession,
                                'locus'           => $v4_sequence->locus,
                                'splice'          => $v4_sequence->splice,
                                'splice_priority' => $v4_sequence->splice_priority,
                                'length'          => $v4_sequence->length,
                                'species_id'      => $species_mapping->{$v4_sequence->species_id}->id,
                                'annotation'      => $v4_sequence->annotation,
                                'polypeptide'     => $v4_sequence->polypeptide,
                                'user_id'         => $g_v4_user->id,
                            },
                        },
                    );
                    # add custom sequence to database
                    $custom_sequence->save();
                }
                else
                {
                    LogVerboseInfo(" v4 sequence '" . $v4_sequence->accession . "' already transfered to v5");
                }

                # store ID association
                $sequence_association_hash->{$v4_sequence->id} = $custom_sequence->id;

                # find corresponding v4 sequence...
                #+FIXME: maybe include support for a mapping file (by species)
                #+FIXME: maybe allow some specified species to be skipped
                if (exists($genome_mapping->{$v4_sequence->species_id}))
                {
                    my $v5_genome = $genome_mapping->{$v4_sequence->species_id};

                    my $v4p_sequence = FindAssociatedSequence($v4_sequence, $v5_genome, $sequence_mapping);

                    if ($v4p_sequence)
                    {
                        LogVerboseInfo(" found associated v4 sequence '" . $v4p_sequence->accession . "'");
                        # store link if none already set
                        $sql_query = "
                                SELECT TRUE
                                FROM custom_sequences_sequences_relationships cssr
                                    JOIN custom_sequences cs ON (cs.id = cssr.custom_sequence_id)
                                WHERE cs.user_id = " . $g_v4_user->id . "
                                    AND cssr.sequence_id = " . $v4p_sequence->id . "
                                    AND cssr.type = 'synonym'
                                LIMIT 1
                                ;";
                        # LogDebug("SQL Query: $sql_query");
                        my ($relationship_found) = $g_dbh->selectrow_array($sql_query);
                            # or confess LogError("Failed to search for an already set sequence relationship!\n" . $g_dbh->errstr);
                        if (!$relationship_found)
                        {
                            LogVerboseInfo("Add a relationship between '$custom_sequence' (" . $custom_sequence->id . ") and '$v4p_sequence' (" . $v4p_sequence->id . ")");
                            $sql_query = "
                                INSERT INTO
                                    custom_sequences_sequences_relationships (custom_sequence_id, sequence_id, type)
                                VALUES (" . $custom_sequence->id . ", " . $v4p_sequence->id . ", 'synonym')
                                ;";
                            $g_dbh->do($sql_query) or LogError("Failed to insert v4-v4 sequence relationship!\n" . $g_dbh->errstr);
                        }
                    }
                    else
                    {
                        ++$no_association;
                        LogVerboseInfo(" no associated v4 sequence found");
                    }
                }
            } # end of foreach

            LogVerboseInfo("...done with current batch of sequences.");
            print GetProgress(
                {'label' => 'Processed sequences:', 'current' => $progress, 'total' => $total_sequence_count,},
                80,
                5, # seconds
            );

            # get next batch of sequences
            $offset += $SEQUENCE_BATCH_SIZE;
            $v4_sequences = [Greenphyl::ExtV4Sequence->new($v4_dbh, {'selectors' => {'filtered' => 0, 'species_id' => $species_id}, 'sql' => {'LIMIT' => $SEQUENCE_BATCH_SIZE, 'OFFSET' => $offset}})];
        } # end of while
    } # end of foreach
    if ($total_sequence_count)
    {
        # print 100%
        print GetProgress(
            {'label' => 'Processed sequences:', 'current' => $total_sequence_count, 'total' => $total_sequence_count,},
            80,
            0, # seconds
        ) . "\n";
    }

    LogInfo("...done processing v4 sequences.");
    LogInfo("Transfered sequences: " . scalar(keys(%$sequence_association_hash)) . "\nUntransfered sequences (missing species): $missing_sequence_species\nUntransfered sequences (duplicates): $already_inserted\nSequences without association: $no_association");

    return $sequence_association_hash;
}


=pod

=head2 TransferFamilies

B<Description>: [function description]. #+++

B<ArgsCount>: [count of arguments] #+++

=over 4

=item $variable_name: ([variable nature]) ([requirements]) #+++ see below

#--- requirement can be:
#--- (R)=required,
#--- (O)=optional
#--- (U)=optional and must be undef if omitted
[variable description]. #+++

=item $variable_name2: ([variable nature]) ([requirements]) #+++

[variable description]. #+++

=back

B<Return>: ([return type]) #+++

[return description]. #+++

B<Example>:

    [example code] #+++

=cut

sub TransferFamilies
{
    my ($v4_dbh, $sequence_association_hash, $family_suffix) = @_;

    LogInfo("Processing v4 families...");
    # get sequence count
    my $sql_query = "SELECT COUNT(1) FROM families WHERE black_listed = 0;";
    my ($total_family_count) = $v4_dbh->selectrow_array($sql_query);

    my $family_association_hash = {};
    my $offset = 0;
    my $v4_families = [Greenphyl::ExtV4Family->new($v4_dbh, {'selectors' => {'black_listed' => 0}, 'sql' => {'OFFSET' => $offset, 'LIMIT' => $FAMILY_BATCH_SIZE}})];

    if (!@$v4_families)
    {
        LogError("No family found in v4 database!");
        return;
    }

    while (@$v4_families)
    {
        LogVerboseInfo("Working on families [$offset, " . ($offset + $#$v4_families) . "]...");
FAMILY_TRANSFER_LOOP:
        foreach my $v4_family (@$v4_families)
        {
            LogVerboseInfo("-working on family '" . $v4_family->accession . "'");

            # make sure we don't process twice the same family
            if (exists($family_association_hash->{$v4_family->family_id}))
            {
                LogWarning("Family '$v4_family' (" . $v4_family->family_id . ") already processed!");
                next FAMILY_TRANSFER_LOOP;
            }

            # create a new v4 custom_family object associated to GreenPhyl v4 user
            my $new_accession = $v4_family->accession . '_' . $family_suffix;
            my $custom_family = Greenphyl::CustomFamily->new(
                $g_dbh,
                {
                    'load' => 'none',
                    'members' => {
                        'accession'       => $new_accession,
                        'name'            => $v4_family->name,
                        'level'           => $v4_family->level,
                        'type'            => $v4_family->type,
                        'description'     => $v4_family->description,
                        'inferences'      => $v4_family->inferences,
                        'data'            => ExtractFamilyData($v4_family),
                        'flags'           => $Greenphyl::CustomFamily::CUSTOM_FAMILY_FLAG_OBSOLETE . (1 == $v4_family->level ? ',' . $Greenphyl::CustomFamily::CUSTOM_FAMILY_FLAG_LISTED : ''),
                        'access'          => $Greenphyl::CustomFamily::CUSTOM_FAMILY_ACCESS_PUBLIC_READ,
                        'user_id'         => $g_v4_user->id,
                    },
                },
            );
            # add custom family to database
            $custom_family->save();

            # store ID association
            $family_association_hash->{$v4_family->family_id} = $custom_family->id;

            # associate sequences
            my $offset_seq = 0;
            my $v4_sequences = $v4_family->fetchSequences({'selectors' => {'filtered' => 0}, 'sql' => {'OFFSET' => $offset_seq, 'LIMIT' => $SEQUENCE_BATCH_SIZE} });
            while (@$v4_sequences)
            {
                my @custom_sequence_ids;
                foreach my $v4_sequence (@$v4_sequences)
                {
                    if (!exists($sequence_association_hash->{$v4_sequence->id}))
                    {
                        LogWarning("Custom sequence for sequence '$v4_sequence' (" . $v4_sequence->id . ") not available for family '$v4_family'!");
                    }
                    else
                    {
                        push(@custom_sequence_ids, $sequence_association_hash->{$v4_sequence->id});
                    }
                }
                
                if (@custom_sequence_ids)
                {
                    my $sql_query = "
                        INSERT INTO custom_families_sequences
                            (custom_family_id, custom_sequence_id)
                        VALUES (" . join('), (', map {$custom_family->id . ', ' . $_} @custom_sequence_ids) . ");
                    ";
                    # LogDebug("SQL Query: $sql_query");
                    $g_dbh->do($sql_query)
                        or confess LogError("Failed to insert custom family-sequence relationships (family: '$custom_family' , " . $custom_family->id . ")!\n" . $g_dbh->errstr);
                }
                else
                {
                    LogWarning("New custom family '$v4_family' has unassociated sequences!");
                }
                # get next batch of sequences
                $offset_seq += $SEQUENCE_BATCH_SIZE;
                $v4_sequences = $v4_family->fetchSequences({'selectors' => {'filtered' => 0}, 'sql' => {'OFFSET' => $offset_seq, 'LIMIT' => $SEQUENCE_BATCH_SIZE} });
            }
        } # end of foreach

        LogVerboseInfo("...done with current batch of families.");
        print GetProgress(
            {'label' => 'Processed families:', 'current' => $offset + scalar(@$v4_families), 'total' => $total_family_count,},
            80,
            5, # seconds
        );

        # get next batch of families
        $offset += $FAMILY_BATCH_SIZE;
        $v4_families = [Greenphyl::ExtV4Family->new($v4_dbh, {'selectors' => {'black_listed' => 0}, 'sql' => {'OFFSET' => $offset, 'LIMIT' => $FAMILY_BATCH_SIZE}})];
    } # end of while
    if ($total_family_count)
    {
        # print 100%
        print GetProgress(
            {'label' => 'Processed families:', 'current' => $total_family_count, 'total' => $total_family_count,},
            80,
            0, # seconds
        ) . "\n";
    }
    LogInfo("...done processing v4 families.");
    LogInfo("Transfered families: " . scalar(keys(%$family_association_hash)));

    return $family_association_hash;
}



=pod

=head2 GetFamilyAssociation

B<Description>: [function description]. #+++

B<ArgsCount>: [count of arguments] #+++

=over 4

=item $variable_name: ([variable nature]) ([requirements]) #+++ see below

#--- requirement can be:
#--- (R)=required,
#--- (O)=optional
#--- (U)=optional and must be undef if omitted
[variable description]. #+++

=item $variable_name2: ([variable nature]) ([requirements]) #+++

[variable description]. #+++

=back

B<Return>: ([return type]) #+++

[return description]. #+++

B<Example>:

    [example code] #+++

=cut

sub GetFamilyAssociation
{
    my ($v4_dbh, $family_suffix) = @_;

    LogInfo("Retrieving v4-v4 family associations...");
    my $family_association_hash = {};

    # get v4 accession->id association
    my $sql_query = "SELECT accession, id FROM families WHERE black_listed = 0;";
    my $v4_family_accession_to_id_array = $v4_dbh->selectcol_arrayref($sql_query, { 'Columns' => [1, 2], });
    my %v4_accession_to_id = @$v4_family_accession_to_id_array;

    # get v4 accession->id association
    $sql_query = "SELECT accession, id FROM custom_families WHERE user_id = " . $g_v4_user->id . ";";
    my $v4p_family_accession_to_id_array = $g_dbh->selectcol_arrayref($sql_query, { 'Columns' => [1, 2], });
    my %v4p_accession_to_id = @$v4p_family_accession_to_id_array;

    foreach my $v4_accession (keys(%v4_accession_to_id))
    {
        my $v4p_accession = $v4_accession . '_' . $family_suffix;
        if (!exists($v4p_accession_to_id{$v4p_accession}))
        {
            LogWarning("Corresponding custom family ($v4p_accession) not found in v4 database for v4 family '$v4_accession'!");
        }
        else
        {
            $family_association_hash->{$v4_accession_to_id{$v4_accession}} = $v4p_accession_to_id{$v4p_accession};
        }
    }
    LogInfo("...done retrieving v4-v4 family associations.");

    return $family_association_hash;
}

=pod

=head2 TransferFamilyRelationships

B<Description>: [function description]. #+++

B<ArgsCount>: [count of arguments] #+++

=over 4

=item $variable_name: ([variable nature]) ([requirements]) #+++ see below

#--- requirement can be:
#--- (R)=required,
#--- (O)=optional
#--- (U)=optional and must be undef if omitted
[variable description]. #+++

=item $variable_name2: ([variable nature]) ([requirements]) #+++

[variable description]. #+++

=back

B<Return>: ([return type]) #+++

[return description]. #+++

B<Example>:

    [example code] #+++

=cut

sub TransferFamilyRelationships
{
    my ($v4_dbh, $family_association_hash) = @_;

    LogInfo("Processing v4 family relationships...");

    my $total_family_count = scalar(keys(%$family_association_hash));
    my $family_counter = 0;
    my $previous_relationship_sh = $v4_dbh->prepare("SELECT object_family_id, level_delta FROM family_relationships_cache WHERE subject_family_id = ?;");

FAMILY_RELATIONSHIP_TRANSFER_LOOP:
    foreach my $v4_family_id (keys(%$family_association_hash))
    {
        if (!exists($family_association_hash->{$v4_family_id}))
        {
            LogWarning("v4-v4 family association is missing for v4 family ID $v4_family_id (black_listed?)! No relationship can be transfered for that family!");
            next FAMILY_RELATIONSHIP_TRANSFER_LOOP;
        }

        $previous_relationship_sh->execute($v4_family_id);
        my $v4_relationships = $previous_relationship_sh->fetchall_arrayref();
        my @v4p_relationships;
CURRENT_FAMILY_RELATIONSHIP_LOOP:
        foreach my $v4_relationship (@$v4_relationships)
        {
            if (!exists($family_association_hash->{$v4_relationship->[0]}))
            {
                LogWarning("v4-v4 family association is missing for v4 family ID " . $v4_relationship->[0] . " (black_listed?)! Relationship with v4 family ID $v4_family_id will not be transfered!");
                next CURRENT_FAMILY_RELATIONSHIP_LOOP;
            }
            push(@v4p_relationships, "($family_association_hash->{$v4_family_id}, $family_association_hash->{$v4_relationship->[0]}, $v4_relationship->[1], '$Greenphyl::AbstractFamily::RELATIONSHIP_INHERITANCE')");
        }
        if (@v4p_relationships)
        {
            my $sql_query = "
                INSERT INTO custom_family_relationships
                    (subject_custom_family_id, object_custom_family_id, level_delta, type)
                VALUES "
                . join(', ', @v4p_relationships)
                . ";"
            ;
            # LogDebug("SQL Query: $sql_query");
            $g_dbh->do($sql_query)
                or LogError("Failed to transfer family relationships!\n" . $g_dbh->errstr);
        }

        print GetProgress(
            {'label' => 'Processed families:', 'current' => ++$family_counter, 'total' => $total_family_count,},
            80,
            2, # seconds
        );
    } # end of foreach
    if ($total_family_count)
    {
        # print 100%
        print GetProgress(
            {'label' => 'Processed families:', 'current' => $total_family_count, 'total' => $total_family_count,},
            80,
            0, # seconds
        ) . "\n";
    }
    LogInfo("...done processing v4 family relationships.");
}




# Script options
#################

=pod

=head1 OPTIONS

#--- describes parameters given to the script
#+++ command line syntax
#--- requirement of the option and its parameter can be:
#--- required: name or nature inside <>
#--- optional: name or nature inside []
#--- alternative between 2 elements: elements separated by a |

=head2 Parameters

=over 4

#+++=item B<-help>:
#+++
#+++Prints a brief help message and exits.
#+++
#+++=item B<-man>:
#+++
#+++Prints the manual page and exits.
#+++
#+++=item B<-debug> (integer):
#+++
#+++Executes the script in debug mode. The integer value is optional and is used
#+++to set debug level (use 0 to force disable debug mode).
#+++Default: 0 (not in debug mode).
#+++

=item B<[option_name]> ([option nature]): #+++

[option description]. #+++
Default: [option default value if one] #+++

#--- remove if log not used
#+++=item B<-log>:
#+++
#+++Enable logging.
#+++
#+++=item B<-log-*> (any):
#+++
#+++Logging arguments.

=back
#--- Example:
#---
#--- Template.pl [-help | -man]
#---
#--- Template.pl [-debug [debug_level]] [-size <width> [height]]
#---
#--- =over 4
#---
#--- =item B<-help>:
#---
#--- Prints a brief help message and exits.
#---
#--- =item B<-man>:
#---
#--- Prints the manual page and exits.
#---
#--- =item B<-debug> (integer):
#---
#--- Executes the script in debug mode. If an integer value is specified, it will
#--- be the debug level. If "-debug" option was used without specifying a debug
#--- level, level 1 is assumed.
#--- Default: 0 (not in debug mode).
#---
#---=item B<-size> (positive_real) (positive_real):
#---
#--- Set the dimensions of the object that will be drawn. The first value is
#--- the width; the height is the second value if specified, otherwise it will
#--- assume height and width are equal to the first value.
#--- Default: width and height are set to 1.
#---
#---=back

=cut


# CODE START
#############

# for regression tests
if ($ENV{'TESTING'} && $ENV{'TESTING_NO_RUN'})
{return 1;}

# options processing
my ($man, $help, $debug, $no_prompt) = (0, 0, undef, undef);

my ($v4_host, $v4_port, $v4_login, $v4_password, $v4_database);

my ($nocleanup, $resumefamilies, $resumerelationships, $v5_user_login, $family_suffix, $seqmapping_file);

# parse options and print usage if there is a syntax error.
GetOptions('help|?'          => \$help,
           'man'             => \$man,
           'debug:s'         => \$debug,
           'q|noprompt'      => \$no_prompt,
           'h|host=s'        => \$v4_host,
           'p|port=s'        => \$v4_port,
           'l|login=s'       => \$v4_login,
           'password=s'      => \$v4_password,
           'd|database=s'    => \$v4_database,
           'nocleanup'       => \$nocleanup,
           'resumefamilies'  => \$resumefamilies,
           'resumerelationships' => \$resumerelationships,
           'seqmapping=s' => \$seqmapping_file,
           'account=s' => \$v5_user_login,
           'suffix=s' => \$family_suffix,
           @Greenphyl::Log::LOG_GETOPT, # skip log parameters
) or pod2usage(1);
if ($help) {pod2usage('-verbose' => 1, '-exitval' => 0);}
if ($man) {pod2usage('-verbose' => 2, '-exitval' => 0);}

# change debug mode if requested/forced
if (defined($debug))
{
    # just '-debug' ?
    if ($debug eq '')
    {
        $debug = 1;
    }
    $DEBUG = $debug;
}

if (!$v5_user_login)
{
    $v5_user_login     ||= Prompt("What will be the v5 account for the import?", { 'default' => 'greenphyl_v4', 'constraint' => '\w', }, $no_prompt);
}

if (!$family_suffix)
{
    $family_suffix     ||= Prompt("What suffix to add to family names?", { 'default' => 'V4', 'constraint' => '\w', }, $no_prompt);
    # Remove leading underscores.
    $family_suffix =~ s/^_+//;
}

$v4_host     ||= Prompt("GreenPhyl v4 database host name?", { 'default' => GP('DATABASES')->[0]->{'host'}, 'constraint' => '\w', }, $no_prompt);
$v4_port     ||= Prompt("GreenPhyl v4 database port?", { 'default' => '3306', 'constraint' => '^\d+$', }, $no_prompt);
$v4_database ||= Prompt("GreenPhyl v4 database name?", { 'default' => 'greenphyl_v4_prod', 'constraint' => '\w', }, $no_prompt);
$v4_login    ||= Prompt("GreenPhyl v4 database login?", { 'default' => 'greenphyl', 'constraint' => '\w', }, $no_prompt);
$v4_password ||= Prompt("GreenPhyl v4 database password?", { 'default' => '', }, $no_prompt);

if (!$v4_host || !$v4_port || !$v4_login || !$v4_database)
{
    warn "Missing v4 connection parameter(s)!\n";
    pod2usage(1);
}

eval
{
    my $v4_dbh = ConnectToDatabase({
        'host'     => $v4_host,
        'port'     => $v4_port,
        'login'    => $v4_login,
        'password' => $v4_password,
        'database' => $v4_database,
    });

    # Get v4 user
    $g_v4_user = Greenphyl::User->new($g_dbh, {'selectors' => {'login' => $v5_user_login}});
    if (!$g_v4_user)
    {
        LogInfo("Creating GreenPhyl v5 Account for import...");
        my $db_index = SelectActiveDatabase();
        if (system("../utils/manage_users.pl -add $v5_user_login -flags 'disabled' -q" . ($db_index? " db_index=$db_index" : '')))
        {
            confess LogError("Failed to create GreenPhyl v4 user!\n" . $@);
        }
        $g_v4_user = Greenphyl::User->new($g_dbh, {'selectors' => {'login' => $v5_user_login}});
        if (!$g_v4_user)
        {
            confess LogError("Failed to load GreenPhyl v4 user!");
        }
    }
    else
    {
        LogVerboseInfo("GreenPhyl v4 User found in database.");
    }

    my $sql_query;
    # clean previous data
    if (!$nocleanup)
    {
        if (!$resumerelationships)
        {
            LogInfo("Removing previous custom v4 families...");
            $sql_query = 'DELETE FROM custom_families WHERE user_id = ' . $g_v4_user->id . ';';
            if (!$g_dbh->do($sql_query))
            {
                LogWarning('Failed to remove previous v4 families!');
            }
            LogInfo("...done clearing v4 families.");
        }

        if (!$resumefamilies && !$resumerelationships)
        {
            LogInfo("Removing previous custom v4 sequences.");
            $sql_query = 'DELETE FROM custom_sequences WHERE user_id = ' . $g_v4_user->id . ';';
            if (!$g_dbh->do($sql_query))
            {
                LogWarning('Failed to remove previous v4 sequences!');
            }
            LogInfo("...done clearing v4 sequences.");
        }
    }

    # Transfer sequences
    my $sequence_association_hash;

    # transfer families
    my $family_association_hash;
    if ($resumerelationships)
    {
        $family_association_hash = GetFamilyAssociation($v4_dbh, $family_suffix);
    }
    else
    {
        if ($resumefamilies)
        {
            $sequence_association_hash = GetSequenceAssociation($v4_dbh);
        }
        else
        {
            my $genome_mapping = GetGenomeMapping($v4_dbh);
            my $species_mapping = GetSpeciesMapping($v4_dbh, $genome_mapping);

            my $sequence_mapping = {};
            if ($seqmapping_file)
            {
                my $fh;
                open($fh, $seqmapping_file)
                    or confess "Can't open file '$seqmapping_file': $!";
                while (my $line = <$fh>)
                {
                    chomp($line);
                    my ($old_seqname, $new_seqname) = split(/[ \t]+/, $line);
                    if (exists($sequence_mapping->{$old_seqname}))
                    {
                        LogWarning("Old accession already mapped for '$old_seqname' ($new_seqname vs $sequence_mapping->{$old_seqname})");
                    }
                    $sequence_mapping->{$old_seqname} = $new_seqname;
                }
                close($fh);
            }

            $sequence_association_hash = TransferSequences($v4_dbh, $species_mapping, $genome_mapping, $sequence_mapping);
        }

        $family_association_hash = TransferFamilies($v4_dbh, $sequence_association_hash, $family_suffix);
    }

    # transfer relationships
    TransferFamilyRelationships($v4_dbh, $family_association_hash);

};

# catch
HandleErrors();

exit(0);

# CODE END
###########


=pod

=head1 AUTHORS

Mathieu ROUARD (Bioversity), m.rouard@cgiar.org

Valentin GUIGNON (Bioversity), v.guignon@cgiar.org

Matthieu CONTE (Syngenta)

Sebastien BRIOIS (Syngenta)

=head1 VERSION

Version 1.0.0

Date 01/12/2014

=head1 SEE ALSO

GreenPhyl documentation.

=cut
