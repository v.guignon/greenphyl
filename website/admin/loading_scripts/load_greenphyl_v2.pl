#!/usr/bin/env perl

=pod

=head1 NAME

load_greenphyl_v2.pl - Loads GreenPhyl v2 sequences and families into v4 DB

=head1 SYNOPSIS

    load_greenphyl_v2.pl

=head1 REQUIRES

Perl5, GreenPhyl v2 database

=head1 DESCRIPTION

Loads GreenPhyl v2 sequences and families into v4 DB as custom sequences and
families. Extra family-data is stored as a frozen Perl structure into the
custom_families.data field. See Greenphyl::CustomFamily for details.

=cut

use strict;
use warnings;

use FindBin;
use lib "$FindBin::Bin/../../lib";
use lib "$FindBin::Bin/../../local_lib";
use Carp qw (cluck confess croak);

use Greenphyl;
use Greenphyl::Log('nolog' => 1,);
use Greenphyl::User;
use Greenphyl::Species;
use Greenphyl::Sequence;
use Greenphyl::Tools::Sequences;
use Greenphyl::ExtV2Sequence;
use Greenphyl::ExtV2Family;
use Greenphyl::File;

use Getopt::Long;
use Pod::Usage;

use Storable qw(nfreeze);

++$|; #no buffering




# Script global constants
##########################

=pod

=head1 CONSTANTS

B<$REV_STRING>: (string)

Current revision (auto-set by GIT).

B<$DEBUG>: (boolean)

When set to true, it enables debug mode.

B<$CONSTANT_NAME>: ([constant nature]) #+++

[constant description and use]. #+++

=cut

# SVN management part
my $REV_STRING = '$id$';

our $DEBUG = 1;
our $SEQUENCE_BATCH_SIZE = 5000;
our $FAMILY_BATCH_SIZE   = 1000;

our $LOCAL_ALIGNMENT_FILE_PATH  = '/NAS/greenphyl/data/v4/v2/alm/';
our $LOCAL_TREE_FILE_PATH  = '/NAS/greenphyl/data/v4/v2/trees/';
our $LOCAL_MEME_FILE_PATH  = '/NAS/greenphyl/data/v4/v2/MEME/mast.';
our $LOCAL_COPY_PATH  = '/NAS/greenphyl/data/v4/v2/copy/';
our $REMOTE_FILE_PATH = '/apps/www/greenphyl/data/v2';
# our $URL_TO_FILE      = 'http://greenphyl-dev.cirad.fr/v2_files/';
our $URL_TO_FILE      = 'http://www.greenphyl.org/v2_files/';

our $MASK_EXTENSION = '.mask.aln';
our $RIO_EXTENSION  = '_tree.rio.xml';
our $MEME_EXTENSION = '.html';

our @FILES_TO_TRANSFER = (
    {
        'local_path' => $LOCAL_ALIGNMENT_FILE_PATH,
        'extension' => $MASK_EXTENSION,
        'type' => 'text/fasta-alignment',
        'field' => 'alignment',
        'subdir' => 'alignments/',
    },
    {
        'local_path' => $LOCAL_TREE_FILE_PATH,
        'extension' => $RIO_EXTENSION,
        'type' => 'application/xml',
        'field' => 'tree',
        'subdir' => 'trees/',
    },
    {
        'local_path' => $LOCAL_MEME_FILE_PATH,
        'extension' => $MEME_EXTENSION,
        'type' => 'text/html',
        'field' => 'meme',
        'subdir' => 'meme/',
    },
);

our $FAMILY_PREFIX = 'GPV2_';

our %FAMILY_TYPE_CONVERSION = (
    0 => 'N/A',
    1 => 'superfamily',
    2 => 'family',
    3 => 'subfamily',
    4 => 'group',
);



# Script global variables
##########################

=pod

=head1 VARIABLES

B<[g_variable_name]>: ([variable nature]) #+++

[variable description, use and default value]. #+++
Default: [variable default value if one] #+++

#--- Example:
#--- B<$g_output_method>: (integer)
#---
#--- used to store current output method;
#--- 0=raw text (default), 1=image, 2=html.
#---
#---     ...
#---
#--- my $g_output_method = 0;
#---

=cut

our $g_dbh = GetDatabaseHandler();
our $g_v2_user;




# Script global functions
##########################

=pod

=head1 FUNCTIONS

=head2 FindAssociatedSequence

B<Description>: [function description]. #+++

B<ArgsCount>: [count of arguments] #+++

=over 4

=item $variable_name: ([variable nature]) ([requirements]) #+++ see below

#--- requirement can be:
#--- (R)=required,
#--- (O)=optional
#--- (U)=optional and must be undef if omitted
[variable description]. #+++

=item $variable_name2: ([variable nature]) ([requirements]) #+++

[variable description]. #+++

=back

B<Return>: ([return type]) #+++

[return description]. #+++

B<Example>:

    [example code] #+++

=cut

sub FindAssociatedSequence
{
    my ($v2_sequence, $v4_species) = @_;

    my $sql_query;
    # -try by accession
    my $v4_sequence = Greenphyl::Sequence->new($g_dbh, {'selectors' => {'accession' => ['LIKE', $v2_sequence->seq_textid],}});

    if (!$v4_sequence)
    {
        # -try using previous accessions
        LogDebug(" trying with previous accessions");
        $sql_query = "SELECT sequence_id FROM sequence_transfer_history WHERE old_accession LIKE ?;";
        my ($sequence_id) = $g_dbh->selectrow_array($sql_query, undef, $v2_sequence->seq_textid);
        # confess LogError("Failed to query sequence_transfer_history table for v2 sequence '$v2_sequence'!\n" . $g_dbh->errstr);
        if ($sequence_id)
        {
            $v4_sequence = Greenphyl::Sequence->new($g_dbh,
                {'selectors' => {'id'  => $sequence_id,},},
            );
        }
    }

    if (!$v4_sequence)
    {
        # -try using synonyms
        LogDebug(" trying with synonyms");
        $sql_query = "SELECT sequence_id FROM sequence_synonyms WHERE synonym LIKE ?";
        # also use v2 synonyms
        if ($v2_sequence->locus)
        {
            $sql_query .= ' OR synonym LIKE ? ';
        }
        my ($sequence_id) = $g_dbh->selectrow_array($sql_query, undef, $v2_sequence->seq_textid, $v2_sequence->locus . '%');
        # confess LogError("Failed to query sequence_synonyms table for v2 sequence '$v2_sequence'!\n" . $g_dbh->errstr);
        if ($sequence_id)
        {
            $v4_sequence = Greenphyl::Sequence->new($g_dbh,
                {'selectors' => {'id'  => $sequence_id,},},
            );
        }
    }

    if (!$v4_sequence)
    {
        # -try by content and species
        LogDebug(" trying with sequence content");
        $v4_sequence = Greenphyl::Sequence->new($g_dbh,
            {
                'selectors' =>
                {
                    'species_id'  => $v4_species->id,
                    'polypeptide' => $v2_sequence->sequence,
                },
            },
        );
    }

    if (!$v4_sequence)
    {
        LogInfo("No associated sequence found for v2 sequence " . $v2_sequence);
    }
    
    return $v4_sequence;
}


=pod

=head2 ConvertFamilyInference

B<Description>: [function description]. #+++

B<ArgsCount>: [count of arguments] #+++

=over 4

=item $variable_name: ([variable nature]) ([requirements]) #+++ see below

#--- requirement can be:
#--- (R)=required,
#--- (O)=optional
#--- (U)=optional and must be undef if omitted
[variable description]. #+++

=item $variable_name2: ([variable nature]) ([requirements]) #+++

[variable description]. #+++

=back

B<Return>: ([return type]) #+++

[return description]. #+++

B<Example>:

    [example code] #+++

=cut

sub ConvertFamilyInference
{
    my ($v2_family) = @_;

    my @inferences;
    if ($v2_family->pmid && ($v2_family->pmid =~ m/^\d+$/))
    {
        push(@inferences, 'PubMed');
    }

    if ($v2_family->pub_ref && ($v2_family->pub_ref =~ m/\w/))
    {
        # 'IEA:PIRSF','IEA:UniProtKB-KW','IEA:MEROPS'
        if ($v2_family->pub_ref =~ m/interpro/i)
        {
            push(@inferences, 'IEA:InterPro');
        }
        
        if (($v2_family->pub_ref =~ m/kegg/i)
            || ($v2_family->pub_ref =~ m/www\.genome\.jp/i))
        {
            push(@inferences, 'IEA:KEGG');
        }

        if ($v2_family->pub_ref =~ m/tf\.cbi/i)
        {
            push(@inferences, 'IEA:TF');
        }
        
        if ($v2_family->pub_ref =~ m/arabidopsis/i)
        {
            push(@inferences, 'TAIR/TIGR');
        }
        
        if ($v2_family->pub_ref =~ m/uniprot/i)
        {
            push(@inferences, 'IEA:UniProtKB');
        }

        if (!@inferences)
        {
            push(@inferences, 'Other');
        }
    }

    return join(',', @inferences);
}


=pod

=head2 FindFile

B<Description>: [function description]. #+++

B<ArgsCount>: [count of arguments] #+++

=over 4

=item $variable_name: ([variable nature]) ([requirements]) #+++ see below

#--- requirement can be:
#--- (R)=required,
#--- (O)=optional
#--- (U)=optional and must be undef if omitted
[variable description]. #+++

=item $variable_name2: ([variable nature]) ([requirements]) #+++

[variable description]. #+++

=back

B<Return>: ([return type]) #+++

[return description]. #+++

B<Example>:

    [example code] #+++

=cut

sub FindFile
{
    my ($path, $extension, $v2_family) = @_;
    my $family_id = $v2_family->family_id;

    my $file_path = $path . $family_id . $extension;
    if (!-e $file_path)
    {
        $file_path = $path . $family_id;
        if (-e $file_path)
        {
            if (opendir(my $dh, $file_path))
            {
                my $entry;
                while (defined($entry = readdir($dh)) && ($entry !~ m/$extension/))
                {}
                closedir($dh);
                
                if ($entry && ($entry =~ m/$extension/))
                {
                    PrintDebug("Found file $entry for $extension");
                    $file_path .= '/' . $entry;
                }
            }
            else
            {
                cluck "Can't opendir $file_path: $!\n";
            }
        }
    }

    if (-e $file_path)
    {
        PrintDebug("Found file for family $family_id\n");
        return $file_path;
    }
    else
    {
        PrintDebug("No file found for family $family_id\n");
        return undef;
    }
}


=pod

=head2 StoreFile

B<Description>: [function description]. #+++

B<ArgsCount>: [count of arguments] #+++

=over 4

=item $variable_name: ([variable nature]) ([requirements]) #+++ see below

#--- requirement can be:
#--- (R)=required,
#--- (O)=optional
#--- (U)=optional and must be undef if omitted
[variable description]. #+++

=item $variable_name2: ([variable nature]) ([requirements]) #+++

[variable description]. #+++

=back

B<Return>: ([return type]) #+++

[return description]. #+++

B<Example>:

    [example code] #+++

=cut

sub StoreFile
{
    my ($parameters) = @_;

    if (!-e $LOCAL_COPY_PATH . $parameters->{'subdir'})
    {
        if (system("mkdir -p " . $LOCAL_COPY_PATH . $parameters->{'subdir'}))
        {
            warn "ERROR: $@\n";
        }
    }

    my $source_file = $parameters->{'path'};
    my $file_name   = $parameters->{'new_name'};
    my $file_url    = $URL_TO_FILE     . $parameters->{'subdir'} . $file_name;
    my $target_file = $LOCAL_COPY_PATH . $parameters->{'subdir'} . $file_name;

    my $command = "ln $source_file $target_file";
    PrintDebug("COMMAND: $command\n");
    if ((!-e $target_file) && system($command))
    {
        warn "WARNING: Failed to create file $target_file!\n";
        return undef;
    }
    else
    {
        # file copied
        my $db_file = Greenphyl::File->new(
            GetDatabaseHandler(),
            {
                'selectors' => {
                    'name'    => $file_name,
                    'path'    => $REMOTE_FILE_PATH . $parameters->{'subdir'},
                    'type'    => $parameters->{'type'},
                    'user_id' => $g_v2_user->id,
                },
            },
        );

        if (!$db_file)
        {
            $db_file = Greenphyl::File->new(
                GetDatabaseHandler(),
                {
                    'load' => 'none',
                    'members' => {
                        'id'      => 0,
                        'name'    => $file_name,
                        'path'    => $REMOTE_FILE_PATH . $parameters->{'subdir'},
                        'url'     => $file_url,
                        'type'    => $parameters->{'type'},
                        'flags'   => '',
                        'size'    => (-s $target_file),
                        'user_id' => $g_v2_user->id,
                    },
                },
            );
            $db_file->save;
        }

        return $db_file;
    }
}


=pod

=head2 ExtractFamilyData

B<Description>: [function description]. #+++

B<ArgsCount>: [count of arguments] #+++

=over 4

=item $variable_name: ([variable nature]) ([requirements]) #+++ see below

#--- requirement can be:
#--- (R)=required,
#--- (O)=optional
#--- (U)=optional and must be undef if omitted
[variable description]. #+++

=item $variable_name2: ([variable nature]) ([requirements]) #+++

[variable description]. #+++

=back

B<Return>: ([return type]) #+++

[return description]. #+++

B<Example>:

    [example code] #+++

=cut

sub ExtractFamilyData
{
    my ($v2_family, $new_accession) = @_;

    # family data
    my $family_data = {
        'family_v2' => $v2_family,
    };

    # add related files like:
    # alm/61052.mask.aln
    # trees/60927_tree.rio.xml
    # MEME/mast.89332.html
    foreach my $file_to_transfer (@FILES_TO_TRANSFER)
    {
        if (my $file_path = FindFile($file_to_transfer->{'local_path'}, $file_to_transfer->{'extension'}, $v2_family))
        {
            my $new_name = $new_accession . $file_to_transfer->{'extension'};
            if (my $db_file = StoreFile({
                    'path' => $file_path,
                    'new_name' => $new_name,
                    'type' => $file_to_transfer->{'type'},
                    'subdir' => $file_to_transfer->{'subdir'},
                }))
            {
                $family_data->{$file_to_transfer->{'field'}} = {
                    'type' => 'Greenphyl::File',
                    'selectors' => {'id' => $db_file->id},
                };
            }
        }
    }

    return $family_data;
}


=pod

=head2 GetSequenceAssociation

B<Description>: [function description]. #+++

B<ArgsCount>: [count of arguments] #+++

=over 4

=item $variable_name: ([variable nature]) ([requirements]) #+++ see below

#--- requirement can be:
#--- (R)=required,
#--- (O)=optional
#--- (U)=optional and must be undef if omitted
[variable description]. #+++

=item $variable_name2: ([variable nature]) ([requirements]) #+++

[variable description]. #+++

=back

B<Return>: ([return type]) #+++

[return description]. #+++

B<Example>:

    [example code] #+++

=cut

sub GetSequenceAssociation
{
    my ($v2_dbh) = @_;

    # get sequence count
    my $sql_query = "SELECT COUNT(1) FROM sequences WHERE hidden = 0;";
    my ($total_sequence_count) = $v2_dbh->selectrow_array($sql_query);

    LogInfo("Preparing v2.seq_textid-v4.id assocation hash...");
    $sql_query = "SELECT accession, id FROM custom_sequences WHERE user_id = " . $g_v2_user->id . ";";
    my $accession_to_id_array = $g_dbh->selectcol_arrayref($sql_query, { 'Columns' => [1, 2], });
    my %accession_to_id = @$accession_to_id_array;
    LogInfo("...assocation hash done.");

    LogInfo("Processing v2 sequences...");
    my $sequence_association_hash = {};
    my ($missing_sequence_species, $already_inserted, $no_association) = (0, 0, 0);
    my $offset = 0;
    $sql_query = "SELECT seq_id, seq_textid FROM sequences WHERE hidden = 0 LIMIT $SEQUENCE_BATCH_SIZE OFFSET $offset;";
    my $v2_sequences = $v2_dbh->selectall_arrayref($sql_query, { 'Slice' => {} });

    if (!@$v2_sequences)
    {
        LogError("No sequence found in v2 database!");
        return;
    }

    while (@$v2_sequences)
    {
        LogVerboseInfo("Working on sequences [$offset, " . ($offset + $#$v2_sequences) . "]...");
        foreach my $v2_sequence (@$v2_sequences)
        {
            LogVerboseInfo("-working on sequence '" . $v2_sequence->{'seq_textid'} . "'");
            if (!$accession_to_id{$v2_sequence->{'seq_textid'}})
            {
                confess LogError("Unable to find associated custom sequence for v2 sequence " . $v2_sequence->{'seq_textid'} . " (" . $v2_sequence->{'seq_id'} . ")!");
            }
            $sequence_association_hash->{$v2_sequence->{'seq_id'}} = $accession_to_id{$v2_sequence->{'seq_textid'}};
        } # end of foreach

        LogVerboseInfo("...done with current batch of sequences.");
        print GetProgress(
            {'label' => 'Processed sequences:', 'current' => $offset + scalar(@$v2_sequences), 'total' => $total_sequence_count,},
            80,
            1, # seconds
        );

        # get next batch of sequences
        $offset += $SEQUENCE_BATCH_SIZE;
        $sql_query = "SELECT seq_id, seq_textid FROM sequences WHERE hidden = 0 LIMIT $SEQUENCE_BATCH_SIZE OFFSET $offset;";
        $v2_sequences = $v2_dbh->selectall_arrayref($sql_query, { 'Slice' => {} });
    } # end of while
    if ($total_sequence_count)
    {
        # print 100%
        print GetProgress(
            {'label' => 'Processed sequences:', 'current' => $total_sequence_count, 'total' => $total_sequence_count,},
            80,
            0, # seconds
        ) . "\n";
    }
    LogInfo("...done processing v2 sequences.");
    LogInfo("Transfered sequences: " . scalar(keys(%$sequence_association_hash)) . "\nUntransfered sequences (missing species): $missing_sequence_species\nUntransfered sequences (duplicates): $already_inserted\nSequences without association: $no_association");

    return $sequence_association_hash;
}


=pod

=head2 TransferSequences

B<Description>: [function description]. #+++

B<ArgsCount>: [count of arguments] #+++

=over 4

=item $variable_name: ([variable nature]) ([requirements]) #+++ see below

#--- requirement can be:
#--- (R)=required,
#--- (O)=optional
#--- (U)=optional and must be undef if omitted
[variable description]. #+++

=item $variable_name2: ([variable nature]) ([requirements]) #+++

[variable description]. #+++

=back

B<Return>: ([return type]) #+++

[return description]. #+++

B<Example>:

    [example code] #+++

=cut

sub TransferSequences
{
    my ($v2_dbh) = @_;

    LogInfo("Processing v2 sequences...");
    # get sequence count
    my $sql_query = "SELECT COUNT(1) FROM sequences WHERE hidden = 0;";
    my ($total_sequence_count) = $v2_dbh->selectrow_array($sql_query);

    my $sequence_association_hash = {};
    my ($missing_sequence_species, $already_inserted, $no_association) = (0, 0, 0);
    my $offset = 0;
    my $v2_sequences = [Greenphyl::ExtV2Sequence->new($v2_dbh, {'selectors' => {'hidden' => 0}, 'load' => {'locus' => 1, }, 'sql' => {'OFFSET' => $offset, 'LIMIT' => $SEQUENCE_BATCH_SIZE}})];

    if (!@$v2_sequences)
    {
        LogError("No sequence found in v2 database!");
        return;
    }

    while (@$v2_sequences)
    {
        LogVerboseInfo("Working on sequences [$offset, " . ($offset + $#$v2_sequences) . "]...");
SEQUENCE_TRANSFER_LOOP:
        foreach my $v2_sequence (@$v2_sequences)
        {
            LogVerboseInfo("-working on sequence '" . $v2_sequence->seq_textid . "'");
            if ($v2_sequence->species_code eq 'ORYZA')
            {
                $v2_sequence->species_code('ORYSA');
            }

            # make sure we don't process twice the same sequence
            if (exists($sequence_association_hash->{$v2_sequence->seq_id}))
            {
                ++$already_inserted;
                LogWarning("Sequence '$v2_sequence' (" . $v2_sequence->seq_id . ") already processed!");
                next SEQUENCE_TRANSFER_LOOP;
            }

            # create a new v4 custom_sequence object associated to GreenPhyl v2 user
            my ($locus, $splice_form) = ExtractLocusAndSpliceForm($v2_sequence->seq_textid, $v2_sequence->species_code);
            my $v4_species = Greenphyl::Species->new($g_dbh, {'selectors' => {'code' => $v2_sequence->species_code,}});
            if (!$v4_species)
            {
                ++$missing_sequence_species;
                LogError("Species not found in database: " . $v2_sequence->species_code . "\nSequence '$v2_sequence' not added!");
                next SEQUENCE_TRANSFER_LOOP;
            }
            my $custom_sequence = Greenphyl::CustomSequence->new(
                $g_dbh,
                {
                    'load' => 'none',
                    'members' => {
                        'accession'       => $v2_sequence->seq_textid,
                        'locus'           => $locus,
                        'splice'          => $splice_form,
                        'splice_priority' => (defined($splice_form)?0:undef),
                        'length'          => $v2_sequence->seq_length,
                        'species_id'      => $v4_species->id,
                        'annotation'      => $v2_sequence->annotation,
                        'polypeptide'     => $v2_sequence->sequence,
                        'user_id'         => $g_v2_user->id,
                    },
                },
            );
            # add custom sequence to database
            $custom_sequence->save();

            # store ID association
            $sequence_association_hash->{$v2_sequence->seq_id} = $custom_sequence->id;

            # find corresponding v4 sequence...
            #+FIXME: maybe include support for a mapping file (by species)
            #+FIXME: maybe allow some specified species to be skipped
            my $v4_sequence = FindAssociatedSequence($v2_sequence, $v4_species);
            
            if ($v4_sequence)
            {
                LogVerboseInfo(" found associated v4 sequence '" . $v4_sequence->accession . "'");
                # store link if none already set
                $sql_query = "
                        SELECT TRUE
                        FROM custom_sequences_sequences_relationships cssr
                            JOIN custom_sequences cs ON (cs.id = cssr.custom_sequence_id)
                        WHERE cs.user_id = " . $g_v2_user->id . "
                            AND cssr.sequence_id = " . $v4_sequence->id . "
                            AND cssr.type = 'synonym'
                        LIMIT 1
                        ;";
                # LogDebug("SQL Query: $sql_query");
                my ($relationship_found) = $g_dbh->selectrow_array($sql_query);
                    # or confess LogError("Failed to search for an already set sequence relationship!\n" . $g_dbh->errstr);
                if (!$relationship_found)
                {
                    LogVerboseInfo("Add a relationship between '$custom_sequence' (" . $custom_sequence->id . ") and '$v4_sequence' (" . $v4_sequence->id . ")");
                    $sql_query = "
                        INSERT INTO
                            custom_sequences_sequences_relationships (custom_sequence_id, sequence_id, type)
                        VALUES (" . $custom_sequence->id . ", " . $v4_sequence->id . ", 'synonym')
                        ;";
                    $g_dbh->do($sql_query) or LogError("Failed to insert v2-v4 sequence relationship!\n" . $g_dbh->errstr);
                }
            }
            else
            {
                ++$no_association;
                LogVerboseInfo(" no associated v4 sequence found");
            }
        } # end of foreach

        LogVerboseInfo("...done with current batch of sequences.");
        print GetProgress(
            {'label' => 'Processed sequences:', 'current' => $offset + scalar(@$v2_sequences), 'total' => $total_sequence_count,},
            80,
            5, # seconds
        );

        # get next batch of sequences
        $offset += $SEQUENCE_BATCH_SIZE;
        $v2_sequences = [Greenphyl::ExtV2Sequence->new($v2_dbh, {'selectors' => {'hidden' => 0}, 'load' => {'locus' => 1, }, 'sql' => {'OFFSET' => $offset, 'LIMIT' => $SEQUENCE_BATCH_SIZE}})];
    } # end of while
    if ($total_sequence_count)
    {
        # print 100%
        print GetProgress(
            {'label' => 'Processed sequences:', 'current' => $total_sequence_count, 'total' => $total_sequence_count,},
            80,
            0, # seconds
        ) . "\n";
    }

    LogInfo("...done processing v2 sequences.");
    LogInfo("Transfered sequences: " . scalar(keys(%$sequence_association_hash)) . "\nUntransfered sequences (missing species): $missing_sequence_species\nUntransfered sequences (duplicates): $already_inserted\nSequences without association: $no_association");

    return $sequence_association_hash;
}


=pod

=head2 TransferFamilies

B<Description>: [function description]. #+++

B<ArgsCount>: [count of arguments] #+++

=over 4

=item $variable_name: ([variable nature]) ([requirements]) #+++ see below

#--- requirement can be:
#--- (R)=required,
#--- (O)=optional
#--- (U)=optional and must be undef if omitted
[variable description]. #+++

=item $variable_name2: ([variable nature]) ([requirements]) #+++

[variable description]. #+++

=back

B<Return>: ([return type]) #+++

[return description]. #+++

B<Example>:

    [example code] #+++

=cut

sub TransferFamilies
{
    my ($v2_dbh, $sequence_association_hash) = @_;

    LogInfo("Processing v2 families...");
    # get sequence count
    my $sql_query = "SELECT COUNT(1) FROM family WHERE black_list = 0;";
    my ($total_family_count) = $v2_dbh->selectrow_array($sql_query);

    my $family_association_hash = {};
    my $offset = 0;
    my $v2_families = [Greenphyl::ExtV2Family->new($v2_dbh, {'selectors' => {'black_list' => 0}, 'sql' => {'OFFSET' => $offset, 'LIMIT' => $FAMILY_BATCH_SIZE}})];

    if (!@$v2_families)
    {
        LogError("No family found in v2 database!");
        return;
    }

    while (@$v2_families)
    {
        LogVerboseInfo("Working on families [$offset, " . ($offset + $#$v2_families) . "]...");
FAMILY_TRANSFER_LOOP:
        foreach my $v2_family (@$v2_families)
        {
            LogVerboseInfo("-working on family '" . $v2_family->family_id . "'");

            # make sure we don't process twice the same family
            if (exists($family_association_hash->{$v2_family->family_id}))
            {
                LogWarning("Family '$v2_family' (" . $v2_family->family_id . ") already processed!");
                next FAMILY_TRANSFER_LOOP;
            }

            # create a new v4 custom_family object associated to GreenPhyl v2 user
            my $new_accession = $FAMILY_PREFIX. $v2_family->family_number . ($v2_family->level ? '_' . $v2_family->level : '');
            my $family_type = $FAMILY_TYPE_CONVERSION{$v2_family->type || 0};
            my $custom_family = Greenphyl::CustomFamily->new(
                $g_dbh,
                {
                    'load' => 'none',
                    'members' => {
                        'accession'       => $new_accession,
                        'name'            => $v2_family->family_name || $Greenphyl::AbstractFamily::UNANNOTATED_CLUSTER_NAME,
                        'level'           => $v2_family->level || 0,
                        'type'            => $family_type,
                        'description'     => $v2_family->description || '',
                        'inferences'      => ConvertFamilyInference($v2_family),
                        'data'            => ExtractFamilyData($v2_family, $new_accession),
                        'flags'           => $Greenphyl::CustomFamily::CUSTOM_FAMILY_FLAG_OBSOLETE . (1 == $v2_family->level ? ',' . $Greenphyl::CustomFamily::CUSTOM_FAMILY_FLAG_LISTED : ''),
                        'access'          => $Greenphyl::CustomFamily::CUSTOM_FAMILY_ACCESS_PUBLIC_READ,
                        'user_id'         => $g_v2_user->id,
                    },
                },
            );
            # add custom family to database
            $custom_family->save();

            # store ID association
            $family_association_hash->{$v2_family->family_id} = $custom_family->id;
            
            # associate sequences
            my @custom_sequence_ids;
            foreach my $v2_sequence (@{$v2_family->fetchSequences({'selectors' => {'hidden' => 0} })})
            {
                if (!exists($sequence_association_hash->{$v2_sequence->seq_id}))
                {
                    LogWarning("Custom sequence for sequence '$v2_sequence' (" . $v2_sequence->seq_id . ") not available for family '$v2_family'!");
                }
                else
                {
                    PrintDebug("Adding sequence " . $v2_sequence->seq_textid . "(" . $v2_sequence->seq_id . " -> " . $sequence_association_hash->{$v2_sequence->seq_id} . ") to list of sequence for family " . $custom_family);
                    push(@custom_sequence_ids, $sequence_association_hash->{$v2_sequence->seq_id});
                }
            }

            if (@custom_sequence_ids)
            {
                my $sql_query = "
                    INSERT IGNORE INTO custom_families_sequences
                        (custom_family_id, custom_sequence_id)
                    VALUES (" . join('), (', map {$custom_family->id . ', ' . $_} @custom_sequence_ids) . ");
                ";
                LogDebug("SQL Query: $sql_query");
                $g_dbh->do($sql_query)
                    or confess LogError("Failed to insert custom family-sequence relationships (family: '$custom_family' , " . $custom_family->id . ")!\n" . $g_dbh->errstr);
            }
            else
            {
                LogWarning("New custom family '$v2_family' has no associated sequence!");
            }

        } # end of foreach

        LogVerboseInfo("...done with current batch of families.");
        print GetProgress(
            {'label' => 'Processed families:', 'current' => $offset + scalar(@$v2_families), 'total' => $total_family_count,},
            80,
            5, # seconds
        );
        
        # get next batch of families
        $offset += $FAMILY_BATCH_SIZE;
        $v2_families = [Greenphyl::ExtV2Family->new($v2_dbh, {'selectors' => {'black_list' => 0}, 'sql' => {'OFFSET' => $offset, 'LIMIT' => $FAMILY_BATCH_SIZE}})];
    } # end of while
    if ($total_family_count)
    {
        # print 100%
        print GetProgress(
            {'label' => 'Processed families:', 'current' => $total_family_count, 'total' => $total_family_count,},
            80,
            0, # seconds
        ) . "\n";
    }
    LogInfo("...done processing v2 families.");
    LogInfo("Transfered families: " . scalar(keys(%$family_association_hash)));

    return $family_association_hash;
}



=pod

=head2 GetFamilyAssociation

B<Description>: [function description]. #+++

B<ArgsCount>: [count of arguments] #+++

=over 4

=item $variable_name: ([variable nature]) ([requirements]) #+++ see below

#--- requirement can be:
#--- (R)=required,
#--- (O)=optional
#--- (U)=optional and must be undef if omitted
[variable description]. #+++

=item $variable_name2: ([variable nature]) ([requirements]) #+++

[variable description]. #+++

=back

B<Return>: ([return type]) #+++

[return description]. #+++

B<Example>:

    [example code] #+++

=cut

sub GetFamilyAssociation
{
    my ($v2_dbh, $sequence_association_hash) = @_;

    LogInfo("Retrieving v2-v4 family associations...");
    my $family_association_hash = {};

    # get v2 accession->id association
    my $sql_query = "SELECT f.family_id, COALESCE(CONCAT(f.family_number, '_', fi.class_id), f.family_number) FROM family f LEFT JOIN found_in fi USING (family_id) WHERE f.black_list = 0;";
    my $v2_family_accession_to_id_array = $v2_dbh->selectcol_arrayref($sql_query, { 'Columns' => [1, 2], });
    my %v2_accession_to_id = @$v2_family_accession_to_id_array;

    # get v4 accession->id association
    $sql_query = "SELECT id, accession FROM custom_families WHERE user_id = " . $g_v2_user->id . ";";
    my $v4_family_accession_to_id_array = $g_dbh->selectall_arrayref($sql_query, { 'Columns' => [1, 2], });
    my %v4_accession_to_id = @$v4_family_accession_to_id_array;
    
    
    foreach my $v2_accession (keys(%v2_accession_to_id))
    {
        my $v4_accession = $FAMILY_PREFIX . $v2_accession;
        if (!exists($v4_accession_to_id{$v4_accession}))
        {
            LogWarning("Corresponding custom family NOT found in v4 database for v2 family '$v2_accession'!");
        }
        else
        {
            $family_association_hash->{$v2_accession_to_id{$v2_accession}} = $v4_accession_to_id{$v4_accession};
        }
    }
    LogInfo("...done retrieving v2-v4 family associations.");

    return $family_association_hash;
}

=pod

=head2 TransferFamilyRelationships

B<Description>: [function description]. #+++

B<ArgsCount>: [count of arguments] #+++

=over 4

=item $variable_name: ([variable nature]) ([requirements]) #+++ see below

#--- requirement can be:
#--- (R)=required,
#--- (O)=optional
#--- (U)=optional and must be undef if omitted
[variable description]. #+++

=item $variable_name2: ([variable nature]) ([requirements]) #+++

[variable description]. #+++

=back

B<Return>: ([return type]) #+++

[return description]. #+++

B<Example>:

    [example code] #+++

=cut

sub TransferFamilyRelationships
{
    my ($v2_dbh, $family_association_hash) = @_;

    LogInfo("Processing v2 family relationships...");

    my $total_family_count = scalar(keys(%$family_association_hash));
    my $family_counter = 0;
    my $previous_relationship_sh = $v2_dbh->prepare(qq|
        SELECT
            sii2.family_id AS "object_family_id",
            CAST(fo.class_id AS SIGNED) - CAST(fs.class_id AS SIGNED) AS "level_delta"
        FROM seq_is_in sii1
            JOIN found_in fs ON fs.family_id = sii1.family_id
            JOIN seq_is_in sii2 ON sii1.seq_id = sii2.seq_id AND sii1.family_id != sii2.family_id
            JOIN found_in fo ON fo.family_id = sii2.family_id
        WHERE sii1.family_id = ?
        GROUP BY sii2.family_id;
    |);

FAMILY_RELATIONSHIP_TRANSFER_LOOP:
    foreach my $v2_family_id (keys(%$family_association_hash))
    {
        if (!exists($family_association_hash->{$v2_family_id}))
        {
            LogWarning("v2-v4 family association is missing for v2 family ID $v2_family_id (black_listed?)! No relationship can be transfered for that family!");
            next FAMILY_RELATIONSHIP_TRANSFER_LOOP;
        }

        $previous_relationship_sh->execute($v2_family_id);
        my $v2_relationships = $previous_relationship_sh->fetchall_arrayref();
        my @v4_relationships;
CURRENT_FAMILY_RELATIONSHIP_LOOP:
        foreach my $v2_relationship (@$v2_relationships)
        {
            if (!exists($family_association_hash->{$v2_relationship->[0]}))
            {
                LogWarning("v2-v4 family association is missing for v2 family ID " . $v2_relationship->[0] . " (black_listed?)! Relationship with v2 family ID $v2_family_id will not be transfered!");
                next CURRENT_FAMILY_RELATIONSHIP_LOOP;
            }
            push(@v4_relationships, "($family_association_hash->{$v2_family_id}, $family_association_hash->{$v2_relationship->[0]}, $v2_relationship->[1], 'inheritance')");
        }

        if (@v4_relationships)
        {
            my $sql_query = "
                INSERT IGNORE INTO custom_family_relationships
                    (subject_custom_family_id, object_custom_family_id, level_delta, type)
                VALUES "
                . join(', ', @v4_relationships)
                . ";"
            ;
            LogDebug("SQL Query: $sql_query");
            $g_dbh->do($sql_query)
                or LogError("Failed to transfer family relationships!\n" . $g_dbh->errstr);
        }
        else
        {
            LogWarning("No relationship found for v2 family ID $v2_family_id");
        }

        print GetProgress(
            {'label' => 'Processed families:', 'current' => ++$family_counter, 'total' => $total_family_count,},
            80,
            2, # seconds
        );
    } # end of foreach
    if ($total_family_count)
    {
        # print 100%
        print GetProgress(
            {'label' => 'Processed families:', 'current' => $total_family_count, 'total' => $total_family_count,},
            80,
            0, # seconds
        ) . "\n";
    }
    LogInfo("...done processing v2 family relationships.");
}




# Script options
#################

=pod

=head1 OPTIONS

#--- describes parameters given to the script
#+++ command line syntax
#--- requirement of the option and its parameter can be:
#--- required: name or nature inside <>
#--- optional: name or nature inside []
#--- alternative between 2 elements: elements separated by a |

=head2 Parameters

=over 4

#+++=item B<-help>:
#+++
#+++Prints a brief help message and exits.
#+++
#+++=item B<-man>:
#+++
#+++Prints the manual page and exits.
#+++
#+++=item B<-debug> (integer):
#+++
#+++Executes the script in debug mode. The integer value is optional and is used
#+++to set debug level (use 0 to force disable debug mode).
#+++Default: 0 (not in debug mode).
#+++

=item B<[option_name]> ([option nature]): #+++

[option description]. #+++
Default: [option default value if one] #+++

#--- remove if log not used
#+++=item B<-log>:
#+++
#+++Enable logging.
#+++
#+++=item B<-log-*> (any):
#+++
#+++Logging arguments.

=back
#--- Example:
#---
#--- Template.pl [-help | -man]
#---
#--- Template.pl [-debug [debug_level]] [-size <width> [height]]
#---
#--- =over 4
#---
#--- =item B<-help>:
#---
#--- Prints a brief help message and exits.
#---
#--- =item B<-man>:
#---
#--- Prints the manual page and exits.
#---
#--- =item B<-debug> (integer):
#---
#--- Executes the script in debug mode. If an integer value is specified, it will
#--- be the debug level. If "-debug" option was used without specifying a debug
#--- level, level 1 is assumed.
#--- Default: 0 (not in debug mode).
#---
#---=item B<-size> (positive_real) (positive_real):
#---
#--- Set the dimensions of the object that will be drawn. The first value is
#--- the width; the height is the second value if specified, otherwise it will
#--- assume height and width are equal to the first value.
#--- Default: width and height are set to 1.
#---
#---=back

=cut


# CODE START
#############

# for regression tests
if ($ENV{'TESTING'} && $ENV{'TESTING_NO_RUN'})
{return 1;}

# options processing
my ($man, $help, $debug, $no_prompt) = (0, 0, undef, undef);

my ($v2_host, $v2_port, $v2_login, $v2_password, $v2_database);

my ($nocleanup, $resumefamilies, $resumerelationships);

# parse options and print usage if there is a syntax error.
GetOptions('help|?'       => \$help,
           'man'          => \$man,
           'debug:s'      => \$debug,
           'q|noprompt'   => \$no_prompt,
           'h|host=s'     => \$v2_host,
           'p|port=s'     => \$v2_port,
           'l|login=s'    => \$v2_login,
           'password=s'   => \$v2_password,
           'd|database=s' => \$v2_database,
           'nocleanup'    => \$nocleanup,
           'resumefamilies' => \$resumefamilies,
           'resumerelationships' => \$resumerelationships,
           @Greenphyl::Log::LOG_GETOPT, # skip log parameters
) or pod2usage(1);
if ($help) {pod2usage('-verbose' => 1, '-exitval' => 0);}
if ($man) {pod2usage('-verbose' => 2, '-exitval' => 0);}

# change debug mode if requested/forced
if (defined($debug))
{
    # just '-debug' ?
    if ($debug eq '')
    {
        $debug = 1;
    }
    $DEBUG = $debug;
}

$v2_host     ||= Prompt("GreenPhyl v2 database host name?", { 'default' => 'lomagne.cirad.fr', 'constraint' => '\w', }, $no_prompt);
$v2_port     ||= Prompt("GreenPhyl v2 database port?", { 'default' => '3306', 'constraint' => '^\d+$', }, $no_prompt);
$v2_database ||= Prompt("GreenPhyl v2 database name?", { 'default' => 'greenphyl_v2', 'constraint' => '\w', }, $no_prompt);
$v2_login    ||= Prompt("GreenPhyl v2 database login?", { 'default' => 'greenphyl', 'constraint' => '\w', }, $no_prompt);
$v2_password ||= Prompt("GreenPhyl v2 database password?", { 'default' => '', }, $no_prompt);

if (!$v2_host || !$v2_port || !$v2_login || !$v2_database)
{
    warn "Missing v2 connection parameter(s)!\n";
    pod2usage(1);
}

eval
{
    my $v2_dbh = ConnectToDatabase({
        'host'     => $v2_host,
        'port'     => $v2_port,
        'login'    => $v2_login,
        'password' => $v2_password,
        'database' => $v2_database,
    });

    # Get v2 user
    $g_v2_user = Greenphyl::User->new($g_dbh, {'selectors' => {'login' => 'greenphyl_v2'}});
    if (!$g_v2_user)
    {
        LogInfo("Creating GreenPhyl v2 User...");
        my $db_index = SelectActiveDatabase();
        if (system("../utils/manage_users.pl -add greenphyl_v2 -flags 'disabled' -q" . ($db_index? " db_index=$db_index" : '')))
        {
            confess LogError("Failed to create GreenPhyl v2 user!\n" . $@);
        }
        $g_v2_user = Greenphyl::User->new($g_dbh, {'selectors' => {'login' => 'greenphyl_v2'}});
        if (!$g_v2_user)
        {
            confess LogError("Failed to load GreenPhyl v2 user!");
        }
    }
    else
    {
        LogVerboseInfo("GreenPhyl v2 User found in database.");
    }
    
    my $sql_query;
    # clean previous data
    if (!$nocleanup)
    {
        if (!$resumerelationships)
        {
            LogInfo("Removing previous custom v2 families...");
            $sql_query = 'DELETE FROM custom_families WHERE user_id = ' . $g_v2_user->id . ';';
            if (!$g_dbh->do($sql_query))
            {
                LogWarning('Failed to remove previous v2 families!');
            }
            LogInfo("...done clearing v2 families.");
        }

        if (!$resumefamilies && !$resumerelationships)
        {
            LogInfo("Removing previous custom v2 sequences.");
            $sql_query = 'DELETE FROM custom_sequences WHERE user_id = ' . $g_v2_user->id . ';';
            if (!$g_dbh->do($sql_query))
            {
                LogWarning('Failed to remove previous v2 sequences!');
            }
            LogInfo("...done clearing v2 sequences.");
        }
    }

    # Transfer sequences
    LogInfo("Transfering v2 sequences...");
    my $sequence_association_hash;
    if ($resumefamilies || $resumerelationships)
    {
        $sequence_association_hash = GetSequenceAssociation($v2_dbh);
    }
    else
    {
        $sequence_association_hash = TransferSequences($v2_dbh);
    }
    LogInfo("...done transfering v2 sequences.");

    # transfer families
    LogInfo("Transfering v2 families...");
    my $family_association_hash;
    if ($resumerelationships)
    {
        $family_association_hash = GetFamilyAssociation($v2_dbh, $sequence_association_hash);
    }
    else
    {
        $family_association_hash = TransferFamilies($v2_dbh, $sequence_association_hash);
    }
    LogInfo("...done transfering v2 families.");

    # transfer relationships
    LogInfo("Transfering relationships...");
    TransferFamilyRelationships($v2_dbh, $family_association_hash);
    LogInfo("...done transfering relationships.");

};

# catch
HandleErrors();

exit(0);

# CODE END
###########


=pod

=head1 AUTHORS

Mathieu ROUARD (Bioversity), m.rouard@cgiar.org

Valentin GUIGNON (Bioversity), v.guignon@cgiar.org

Matthieu CONTE (Syngenta)

Sebastien BRIOIS (Syngenta)

=head1 VERSION

Version 1.0.0

Date 01/03/2014

=head1 SEE ALSO

GreenPhyl documentation.

=cut
