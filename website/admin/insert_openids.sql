INSERT INTO openids(name, label, url, enabled, icon_size, icon_index) VALUES
(
  'OpenID',
  'Enter your OpenID provider URL.',
  NULL,
  true,
  'large',
  0
),
(
  'Google',
  NULL,
  'https://www.google.com/accounts/o8/id',
  true,
  'large',
  1
),
(
  'Yahoo',
  NULL,
  'http://me.yahoo.com/',
  true,
  'large',
  2
),
(
  'MyOpenID',
  'Enter your MyOpenID username.',
  'http://{username}.myopenid.com/',
  true,
  'small',
  1
),
(
  'Blogger',
  'Enter your Blogger account.',
  'http://{username}.blogspot.com/',
  true,
  'small',
  2
),
(
  'Verisign',
  'Enter your Verisign username.',
  'http://{username}.pip.verisignlabs.com/',
  true,
  'small',
  3
),
(
  'Flickr',        
  'Enter your Flickr username.',
  'http://flickr.com/{username}/',
  false,
  'small',
  4
),
(
  'Wordpress',
  'Enter your Wordpress.com username.',
  'http://{username}.wordpress.com/',
  true,
  'small',
  5
),
(
  'myspace',
  'Enter your myspace username.',
  'http://www.myspace.com/{username}',
  true,
  'small',
  6
),
(
  'AOL',
  'Enter your AOL screenname.',
  'http://openid.aol.com/{username}',
  true,
  'small',
  7
),
(
  'Orange',
  NULL,
  'http://orange.fr',
  true,
  'small',
  8
),
(
  'LiveJournal',
  'Enter your Livejournal username.',
  'http://{username}.livejournal.com/',
  true,
  'small',
  9
),
(
  'ClaimID',
  'Enter your ClaimID username.',
  'http://claimid.com/{username}',
  true,
  'small',
  10
),
(
  'ClickPass',
  'Enter your ClickPass username.',
  'http://clickpass.com/public/{username}',
  true,
  'small',
  11
),
(
  'Technorati',
  'Enter your Technorati username.',
  'http://technorati.com/people/technorati/{username}/',
  false,
  'small',
  12
),
(
  'Vidoop',
  'Enter your Vidoop username.',
  'http://{username}.myvidoop.com/',
  false,
  'small',
  13
),
(
  'Launchpad',
  'Enter your Launchpad username.',
  'https://launchpad.net/~{username}',
  false,
  'small',
  14
),
(
  'Google Profile',
  'Enter your Google Profile username.',
  'http://www.google.com/profiles/{username}',
  true,
  'small',
  15
);
