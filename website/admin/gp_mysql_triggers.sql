-- --------------------------------------------------------
--
-- Déclencheurs (Triggers)
--
-- -- Affects all triggers
-- -- FALSE value overrides trigger type settings
-- SET @TRIGGER_CHECKS = [TRUE|FALSE];
--
-- -- Trigger type settings
-- SET @TRIGGER_BEFORE_INSERT_CHECKS = [TRUE|FALSE];
-- SET @TRIGGER_AFTER_INSERT_CHECKS = [TRUE|FALSE];
-- SET @TRIGGER_BEFORE_UPDATE_CHECKS = [TRUE|FALSE];
-- SET @TRIGGER_AFTER_UPDATE_CHECKS = [TRUE|FALSE];
-- SET @TRIGGER_BEFORE_DELETE_CHECKS = [TRUE|FALSE];
-- SET @TRIGGER_AFTER_DELETE_CHECKS = [TRUE|FALSE];
--
-- -- Table triggers
--
-- --------------------------------------------------------

DELIMITER //

-- --------------------------------------------------------
-- Homologies
--


CREATE TRIGGER families_sequences_i
    AFTER INSERT ON families_sequences
    FOR EACH ROW
BEGIN
    -- when a sequence is inserted into a family, the family content has changed
    -- so the homology scores related to that family become obsolete
    DELETE FROM homologies WHERE family_id = NEW.family_id;
    
    -- when a sequence is inserted into a family, the family content has changed
    -- so the family statistics and specificity become obsolete
    -- clear family statistics
    CALL clearFamilyStats(NEW.family_id);
    -- clear family specificity
    CALL clearFamilySpecificity(NEW.family_id);
    -- set update_family_stats flag
    INSERT INTO variables VALUES ('update_family_stats', '1'),('update_alignment_stats', '1'),('update_plant_specificity', '1')
      ON DUPLICATE KEY UPDATE value = '1';

END//


CREATE TRIGGER families_sequences_d
    AFTER DELETE ON families_sequences
    FOR EACH ROW
BEGIN
    -- when a sequence is removed from a family, the family content has changed
    -- so the homology scores related to that family become obsolete
    DELETE FROM homologies WHERE family_id = OLD.family_id;

    -- when a sequence is removed from a family, the family content has changed
    -- so the family statistics become obsolete
    -- clear family statistics
    CALL clearFamilyStats(OLD.family_id);
    -- set update_family_stats flag
    INSERT INTO variables VALUES ('update_family_stats', '1'),('update_alignment_stats', '1')
      ON DUPLICATE KEY UPDATE value = '1';

END//


CREATE TRIGGER families_sequences_u
    AFTER UPDATE ON families_sequences
    FOR EACH ROW
BEGIN
    -- when the family of a sequence is changed, the family content has changed
    -- so the homology scores related to that family become obsolete
    IF ((OLD.family_id != NEW.family_id)
        OR (OLD.sequence_id != NEW.sequence_id)) THEN
        DELETE FROM homologies WHERE family_id = OLD.family_id;
        DELETE FROM homologies WHERE family_id = NEW.family_id;
    END IF;
    
    -- when the family of a sequence is changed, the family content has changed
    -- so the family statistics become obsolete
    IF ((OLD.family_id != NEW.family_id)
        OR (OLD.sequence_id != NEW.sequence_id)) THEN
            -- clear family statistics
            CALL clearFamilyStats(NEW.family_id);
            -- set update_family_stats flag
            INSERT INTO variables VALUES ('update_family_stats', '1'),('update_alignment_stats', '1')
              ON DUPLICATE KEY UPDATE value = '1';
    END IF;

END//


CREATE TRIGGER sequences_u
    AFTER UPDATE ON sequences
    FOR EACH ROW
BEGIN
    DECLARE v_family_id INT;

    -- when a sequence is filtered or unfiltered, the family statistics become
    -- obsolete
    IF (OLD.filtered != NEW.filtered) THEN
        SELECT INTO v_family_id    sii.family_id FROM families_sequences sii WHERE sii.sequence_id = OLD.sequence_id
            -- clear family statistics
            CALL clearFamilyStats(v_family_id);
            -- set update_family_stats flag
            INSERT INTO variables VALUES ('update_family_stats', '1'),('update_alignment_stats', '1')
              ON DUPLICATE KEY UPDATE value = '1';
    END IF;
END//


/*
*+FIXME:
* add triggers:
-table species: sequence_count quand on ajoute/supprime des séquences


*/

--
-- Triggers 'custom_families'
--
DROP TRIGGER IF EXISTS custom_families_u;
DELIMITER //
CREATE TRIGGER custom_families_u AFTER UPDATE ON custom_families
 FOR EACH ROW BEGIN

    INSERT INTO custom_families_history
      (
        id,
        name,
        accession,
        level,
        type,
        description,
        inferences,
        user_id,
        access,
        data,
        flags,
        creation_date,
        last_update,
        transaction_date,
        transaction_type,
        transaction_account,
        transaction_user
      )
    VALUES
      (
        OLD.id,
        OLD.name,
        OLD.accession,
        OLD.level,
        OLD.type,
        OLD.description,
        OLD.inferences,
        OLD.user_id,
        OLD.access,
        OLD.data,
        OLD.flags,
        OLD.creation_date,
        OLD.last_update,
        NOW(),
        'update',
        USER(),
        COALESCE(@authenticated_user, '')
      )
    ;
END
//
DELIMITER ;
DROP TRIGGER IF EXISTS custom_families_d;
DELIMITER //
CREATE TRIGGER custom_families_d AFTER DELETE ON custom_families
 FOR EACH ROW BEGIN

    INSERT INTO custom_families_history
      (
        id,
        name,
        accession,
        level,
        type,
        description,
        inferences,
        user_id,
        access,
        data,
        flags,
        creation_date,
        last_update,
        transaction_date,
        transaction_type,
        transaction_account,
        transaction_user
      )
    VALUES
      (
        OLD.id,
        OLD.name,
        OLD.accession,
        OLD.level,
        OLD.type,
        OLD.description,
        OLD.inferences,
        OLD.user_id,
        OLD.access,
        OLD.data,
        OLD.flags,
        OLD.creation_date,
        OLD.last_update,
        NOW(),
        'delete',
        USER(),
        COALESCE(@authenticated_user, '')
      )
    ;
END
//
DELIMITER ;


--
-- Triggers 'custom_families_sequences'
--
DROP TRIGGER IF EXISTS custom_families_sequences_u;
DELIMITER //
CREATE TRIGGER custom_families_sequences_u AFTER UPDATE ON custom_families_sequences
 FOR EACH ROW BEGIN

    INSERT INTO custom_families_sequences_history
      (
        custom_family_id,
        custom_sequence_id,
        transaction_date,
        transaction_type,
        transaction_account,
        transaction_user
      )
    VALUES
      (
        OLD.custom_family_id,
        OLD.custom_sequence_id,
        NOW(),
        'update',
        USER(),
        COALESCE(@authenticated_user, '')
      )
    ;
END
//
DELIMITER ;
DROP TRIGGER IF EXISTS custom_families_sequences_d;
DELIMITER //
CREATE TRIGGER custom_families_sequences_d AFTER DELETE ON custom_families_sequences
 FOR EACH ROW BEGIN

    INSERT INTO custom_families_sequences_history
      (
        custom_family_id,
        custom_sequence_id,
        transaction_date,
        transaction_type,
        transaction_account,
        transaction_user
      )
    VALUES
      (
        OLD.custom_family_id,
        OLD.custom_sequence_id,
        NOW(),
        'delete',
        USER(),
        COALESCE(@authenticated_user, '')
      )
    ;
END
//
DELIMITER ;


--
-- Triggers 'custom_family_relationships'
--
DROP TRIGGER IF EXISTS custom_family_relationships_u;
DELIMITER //
CREATE TRIGGER custom_family_relationships_u AFTER UPDATE ON custom_family_relationships
 FOR EACH ROW BEGIN

    INSERT INTO custom_family_relationships_history
      (
        subject_custom_family_id,
        object_custom_family_id,
        level_delta,
        type,
        transaction_date,
        transaction_type,
        transaction_account,
        transaction_user
      )
    VALUES
      (
        OLD.subject_custom_family_id,
        OLD.object_custom_family_id,
        OLD.level_delta,
        OLD.type,
        NOW(),
        'update',
        USER(),
        COALESCE(@authenticated_user, '')
      )
    ;
END
//
DELIMITER ;
DROP TRIGGER IF EXISTS custom_family_relationships_d;
DELIMITER //
CREATE TRIGGER custom_family_relationships_d AFTER DELETE ON custom_family_relationships
 FOR EACH ROW BEGIN

    INSERT INTO custom_family_relationships_history
      (
        subject_custom_family_id,
        object_custom_family_id,
        level_delta,
        type,
        transaction_date,
        transaction_type,
        transaction_account,
        transaction_user
      )
    VALUES
      (
        OLD.subject_custom_family_id,
        OLD.object_custom_family_id,
        OLD.level_delta,
        OLD.type,
        NOW(),
        'delete',
        USER(),
        COALESCE(@authenticated_user, '')
      )
    ;
END
//
DELIMITER ;



--
-- Triggers 'families'
--
DROP TRIGGER IF EXISTS families_u;
DELIMITER //
CREATE TRIGGER families_u AFTER UPDATE ON families
 FOR EACH ROW BEGIN

    INSERT INTO families_history
      (
        id,
        name,
        accession,
        level,
        type,
        description,
        inferences,
        validated,
        black_listed,
        taxonomy_id,
        plant_specific,
        min_sequence_length,
        max_sequence_length,
        average_sequence_length,
        median_sequence_length,
        alignment_length,
        masked_alignment_length,
        sequence_count,
        alignment_sequence_count,
        masked_alignment_sequence_count,
        transaction_date,
        transaction_type,
        transaction_account,
        transaction_user
      )
    VALUES
      (
        OLD.id,
        OLD.name,
        OLD.accession,
        OLD.level,
        OLD.type,
        OLD.description,
        OLD.inferences,
        OLD.validated,
        OLD.black_listed,
        OLD.taxonomy_id,
        OLD.plant_specific,
        OLD.min_sequence_length,
        OLD.max_sequence_length,
        OLD.average_sequence_length,
        OLD.median_sequence_length,
        OLD.alignment_length,
        OLD.masked_alignment_length,
        OLD.sequence_count,
        OLD.alignment_sequence_count,
        OLD.masked_alignment_sequence_count,
        NOW(),
        'update',
        USER(),
        COALESCE(@authenticated_user, '')
      )
    ;
END
//
DELIMITER ;
DROP TRIGGER IF EXISTS families_d;
DELIMITER //
CREATE TRIGGER families_d AFTER DELETE ON families
 FOR EACH ROW BEGIN

    INSERT INTO families_history
      (
        id,
        name,
        accession,
        level,
        type,
        description,
        inferences,
        validated,
        black_listed,
        taxonomy_id,
        plant_specific,
        min_sequence_length,
        max_sequence_length,
        average_sequence_length,
        median_sequence_length,
        alignment_length,
        masked_alignment_length,
        sequence_count,
        alignment_sequence_count,
        masked_alignment_sequence_count,
        transaction_date,
        transaction_type,
        transaction_account,
        transaction_user
      )
    VALUES
      (
        OLD.id,
        OLD.name,
        OLD.accession,
        OLD.level,
        OLD.type,
        OLD.description,
        OLD.inferences,
        OLD.validated,
        OLD.black_listed,
        OLD.taxonomy_id,
        OLD.plant_specific,
        OLD.min_sequence_length,
        OLD.max_sequence_length,
        OLD.average_sequence_length,
        OLD.median_sequence_length,
        OLD.alignment_length,
        OLD.masked_alignment_length,
        OLD.sequence_count,
        OLD.alignment_sequence_count,
        OLD.masked_alignment_sequence_count,
        NOW(),
        'delete',
        USER(),
        COALESCE(@authenticated_user, '')
      )
    ;
END
//
DELIMITER ;

