#!/usr/bin/perl

use strict;
use warnings;
use lib '/home/rouard/perllib/';
use Parallel::ForkManager;
use Bio::SeqIO;

#multi fasta file
my $file        = shift;
my $output_dir  = shift;
my $in          = new Bio::SeqIO( -format => 'fasta',  -file  => $file );

my $MAX_PROCESSES = 8;
my $pm = new Parallel::ForkManager($MAX_PROCESSES); 

my @data;
while( my $seqobj = $in->next_seq ) {
	push @data , $seqobj;
}


my $cpt = 0;
foreach my $seqobj (@data) 
{
    # Forks and returns the pid for the child:
	$pm->start and next;
	
	my $outfile     = $output_dir . '/' . $seqobj->display_id() . '.xml';
	my $temp_file   = 'temp/' . $seqobj->display_id() . '.faa';
	
	print "Run : ",$seqobj->display_id(),"\n";
	
	my $out = new Bio::SeqIO(-format => 'fasta', -file  => ">$temp_file");
	$out->write_seq($seqobj);
	
	# run interproscan
	my $job_id  = "/usr/local/bioinfo/iprscan/bin/iprscan -cli -i $temp_file  -o $outfile -goterms -iprlookup -seqtype p -format xml 2>/dev/null 1>/dev/null";
	print $job_id;
	#system($job_id) unless -e $outfile;
	
	print "Done : Result in :", $outfile ,"\n";
	
	system("rm $temp_file") if -e $temp_file;
	
	$pm->finish;  # Terminates the child process
}
$pm->wait_all_children;
