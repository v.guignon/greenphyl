#!/usr/bin/perl

# perl merge_clusters.pl <family_id_to_merge> <family_id> <family_id_at_level2> <family_id_at_level3> etc..


use strict;
use warnings;
use Carp qw(cluck confess croak);
use Error qw(:try);
use lib '../../lib';
use lib '../../local_lib';


use Carp qw (cluck confess croak);
use Greenphyl::Config;
use Greenphyl::ConnectMysql;
use DBIx::Simple;

my $mysql        = Greenphyl::ConnectMysql->new();
my $dbh          = $mysql->DBconnect();
my $dbis         = DBIx::Simple->new($dbh); 

# retrieve arguments
my $family_to_move     =  shift;
my $family_to_assigned =  shift;
my @subfamilies_to_assigned =  @ARGV if (@ARGV);

my $sql = "SELECT seq_id FROM seq_is_in WHERE family_id = ?;";
my @seq_ids = $dbis->query( $sql, $family_to_move )->flat;

print scalar (@seq_ids) . " sequences will move in the form cluster $family_to_move to cluster $family_to_assigned\n";
print "In progress...\n";

# start SQL transaction
$dbh->begin_work() or croak $dbh->errstr;

$dbh->parse_trace_flag('SQL');

try
{
    foreach my $seq_id (@seq_ids)
    {
       $dbis->update('seq_is_in', 
                        { 
                            'family_id' => $family_to_assigned,
                        },
                        { 
                            'family_id' => $family_to_move,
                        });              
         
        foreach my $sub_family (@subfamilies_to_assigned)
        { 
            $dbis->insert('seq_is_in', { 
                                        'seq_id'    => $seq_id,
                                        'family_id' => $sub_family,
                        } );              
        }
    }
    print "Done!\n";
    print "Remove $family_to_move...\n";
    $dbh->do("DELETE FROM family WHERE family_id = $family_to_move");
    $dbh->commit() or confess($dbh->errstr);
    print "Done!\n";
    
    #$dbh->do('call countFamilySeqBySpecies();');
}
otherwise
{
    my $error = shift;
    $dbh->rollback();
    confess $error;
};



