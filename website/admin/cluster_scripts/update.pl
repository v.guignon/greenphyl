#!/usr/bin/perl

=pod

=head1 NAME

update.pl - script used to update Greenphyl database

=head1 SYNOPSIS

    update.pl -c 1 -f fasta.fa -sp BRADI

=head1 REQUIRES

Perl5, BioPerl, Greenphyl

=head1 DESCRIPTION

This script can be used to update GreenPhyl database by inserting new species
data or updating data of existing species.

=cut

use strict;
use warnings;
no warnings 'once';

use lib '../../lib';
use lib '../../local_lib';


use Readonly;
use Carp qw(cluck confess croak);
use Error qw(:try);
use Fatal qw(open close);
use Getopt::Long;
use Pod::Usage;
use File::Basename;
use FileHandle;
use DBIx::Simple;
use Bio::SeqIO;
use Bio::SearchIO;
use XML::Simple qw(:strict);

use Greenphyl::Config;
use Greenphyl::Log('nolog' => 1,);
use Greenphyl::Utils;
use Greenphyl::HashInsensitive;
use Greenphyl::Load::ProteinSequence;
use Greenphyl::Load::Family;
use Greenphyl::Run::InterproWS;
use Greenphyl::Run::Statistics;
use Greenphyl::Run::Blast;
use Greenphyl::Load::Pirsf;
use Greenphyl::Load::Kegg;
use Greenphyl::Load::Uniprot;
use Greenphyl::Load::Interproscan;
use Greenphyl::PathManager;
use Greenphyl::QCov 'compare_sequence_qcov';

++$|;


# for regression tests...
mock_update() if $ENV{TESTING} and !$ENV{RUN_REAL_UPDATE};
sub mock_update
{
    exit;
}


# SVN management part
######################
my $REV_STRING = '$id$';


# Script global constants
##########################

=pod

=head1 CONSTANTS

B<$DEBUG>: (boolean)

Set default debug mode value.

B<$UPDATER_VERSION>: (string)

Script version string.

B<$GLOBAL_UPDATE_DIRECTORY>: (string)

Name of the output directory to use when no species name is specified.

B<$EVALUE>: (float)

BLAST e-value to use.

B<$MAX_BEST_HIT>: (integer)

BLAST max best hit value.

B<$SEQUENCE_NAME>: (string)

Hash key name for sequence name.

B<$SEQ_LENGTH>: (string)

Hash key name for sequence length.

B<$SEQUENCE_CONTENT>: (string)

Hash key name for sequence content.

B<$ANNOTATION>: (string)

Hash key name for sequence annotation.

B<$BLAST_PIN_EXT>: (string)

File extension for PIN files (for BLAST bank).

B<$BLAST_PHR_EXT>: (string)

File extension for PHR files (for BLAST bank).

B<$BLAST_PSQ_EXT>: (string)

File extension for PSQ files (for BLAST bank).

B<$FASTA_FILE_EXT>: (string)

File extension for FASTA files.

B<$UNANNOTATED_CLUSTER_NAME>: (string)

Name to use for clusters that have not been annotated.

B<$BAD_HIT_ANNOTATION>: (string)

Annotation word to add to FASTA sequences that had invalid BLAST hit.

B<$NO_HIT_ANNOTATION>: (string)

Annotation word to add to FASTA sequences that had no BLAST hit.

B<$RUN_CODE_*>: (string)

Code string used to select the commands to run.

=cut

my $DEBUG                    = 0;

my $UPDATER_VERSION          = '2.0.' . $REV_STRING;
my $GLOBAL_UPDATE_DIRECTORY  = 'GLOBAL';

my $SPECIES_NAME_REGEXP      = '[A-Z0-9]{3,8}';
my $SPECIES_BLAST_NAME_SEPARATOR = "_vs_";

my $EVALUE                   = 1e-10;
my $MAX_BEST_HIT             = 1; # set to 2?
my $SEQUENCE_NAME            = 'seq_textid';
my $SEQ_LENGTH               = 'seq_length';
my $SEQUENCE_CONTENT         = 'sequence';
my $ANNOTATION               = 'annotation';

my $BLAST_PIN_EXT            = '.pin';
my $BLAST_PHR_EXT            = '.phr';
my $BLAST_PSQ_EXT            = '.psq';
my $FASTA_FILE_EXT           = '.fa';

my $UNANNOTATED_CLUSTER_NAME = 'Unannotated cluster';
my $BAD_HIT_ANNOTATION       = 'bad_hit';
my $NO_HIT_ANNOTATION        = 'no_hit';

my $RUN_CODE_INFO                     = 'info';
my $RUN_INT_CODE_INFO                 = '0';

my $RUN_CODE_SEQUENCE_UPDATE          = 'seq';
my $RUN_INT_CODE_SEQUENCE_UPDATE      = '1';

my $RUN_CODE_FAMILY_CLUSTERING        = 'fam';
my $RUN_INT_CODE_FAMILY_CLUSTERING    = '2';

my $RUN_CODE_EXTERNAL_DATA_UPDATE     = 'ext';
my $RUN_INT_CODE_EXTERNAL_DATA_UPDATE = '3';

my $RUN_CODE_INTERPROSCAN_UPDATE      = 'ipr';
my $RUN_INT_CODE_INTERPROSCAN_UPDATE  = '4';

my $RUN_CODE_REMOVE_SPECIES           = 'remove_species';
my $RUN_INT_CODE_REMOVE_SPECIES       = '5';

my $RUN_CODE_CLEAN_FAMILIES           = 'clean_fam';
my $RUN_INT_CODE_CLEAN_FAMILIES       = '6';

my $RUN_CODE_GENERATE_FAM_FASTA        = 'famfasta';
my $RUN_INT_CODE_GENERATE_FAM_FASTA    = '7';

my $RUN_CODE_MEME_MAST                = 'mm';
my $RUN_INT_CODE_MEME_MAST            = '8';

my $RUN_CODE_GENERATE_SP_FASTA        = 'spfasta';
my $RUN_INT_CODE_GENERATE_SP_FASTA    = '9';

my $RUN_CODE_GENERATE_ORPH_FASTA      = 'orfasta';
my $RUN_INT_CODE_GENERATE_ORPH_FASTA  = '10';

my $RUN_CODE_LOAD_BBMH                = 'bbmh';
my $RUN_INT_CODE_LOAD_BBMH            = '11';

my $RUN_CODE_UPDATE_GO                = 'go';
my $RUN_INT_CODE_UPDATE_GO            = '12';

my $RUN_CODE_UPDATE_TAXONOMY          = 'tax';
my $RUN_INT_CODE_UPDATE_TAXONOMY      = '13';

my $RUN_CODE_LOAD_CLASSIFICATION      = 'class';
my $RUN_INT_CODE_LOAD_CLASSIFICATION  = '14';

my $RUN_CODE_LOAD_ORTHOLOGS           = 'ortho';
my $RUN_INT_CODE_LOAD_ORTHOLOGS       = '15';

my $RUN_CODE_ORPHANS_CLUSTERING       = 'orfam';
my $RUN_INT_CODE_ORPHANS_CLUSTERING   = '16';

my $RUN_MEME_SCRIPT               = 'run_meme.pl';

my $UNIPROT_XML_EXT               = '_uniprot.xml';

my $RAP_ORTHOLOGS_OUTPUT_EXT      = '_rap_stats_tree.txt';

my $HOMOLOGY_TYPE_ORTHOLOGY       = 'orthology';
my $HOMOLOGY_TYPE_SUPER_ORTHOLOGY = 'super-orthology';
my $HOMOLOGY_TYPE_ULTRA_PARALOGY  = 'ultra-paralogy';


# Script global variables
##########################

=pod

=head1 VARIABLES

B<$dbh>: (DBI::db)

A Perl DBI database handler object.

B<$dbis>: (DBIx::Simple)

A "DBI simple" object sharing $dbh.

B<$path_manager>: (Greenphyl::PathManager)

A path manager object.

B<$debug>: (boolean)

Enable or disable debug mode (debug messages).

B<$no_prompt>: (boolean)

disable prompting and use default values.

=cut

our $dbh;
our $dbis;
our $path_manager;
our $debug;
our $no_prompt;



# Script global functions
##########################

=pod

=head1 FUNCTIONS

=head2 initUpdate

B<Description>: Initialize update process. Check BLAST version and set directories.

B<ArgsCount>: 1

=over 4

=item arg: $species_name (string) (R)

5 capital letter code of a species.

=back

B<Return>: (nothing)

=cut

sub initUpdate
{
    my ($species_name) = @_;
    # check Blastall version as results may vary depending the version
    # `$Greenphyl::Config::BLASTALL_COMMAND` =~ /blastall 2.2.16/ or warn "WARNING: Wrong blast version, 2.2.16 is required!\n";

    # database access
    $dbh          ||= connectToDatabase();
    $dbis         ||= DBIx::Simple->new($dbh);

    # path
    $path_manager = Greenphyl::PathManager->new($species_name);
    $path_manager->initUpdateDirectories(0); # ($no_prompt?0:undef);

    print "Log to database\n";
    print "Working on database: " . $Greenphyl::Config::DATABASES->[0]->{'database'} . " (" . $Greenphyl::Config::DATABASES->[0]->{'host'} . ")\n";
    LogInfo("-------------------------------------------------------------------------------");
    LogInfo("GreenPhyl Database Updater v$UPDATER_VERSION\nDate: " . getCurrentDate());
    LogInfo("Database: " . $Greenphyl::Config::DATABASES->[0]->{'name'} . ", mysql://" . $Greenphyl::Config::DATABASES->[0]->{'login'} . ":" . ('*' x length($Greenphyl::Config::DATABASES->[0]->{'password'})) . "@" . $Greenphyl::Config::DATABASES->[0]->{'host'} . "/" . $Greenphyl::Config::DATABASES->[0]->{'database'});
}


=pod

=head2 fetchSpecies

B<Description>: Returns the species code and the database species ID of a given
species.

B<ArgsCount>: 1

=over 4

=item arg: $species_name (string) (R)

5 letter code of the species to fetch.

=back

B<Return>: (list)

First element is the species code as stored in database.
Second element it the database species ID.
If the species has not be found, the species code returned is the same that was
provided and the database species ID returned is 0.

=cut

sub fetchSpecies
{
    my ($species_name) = @_;
    my $species_id = 0;

    if (!$species_name)
    {
        confess "ERROR: no species name was specified!\n";
    }

    my @species_data = @{$dbh->selectall_arrayref("SELECT species_id, species_name FROM species WHERE species_name LIKE ?;", undef, $species_name)};
    # more than 1 match?
    if (1 < @species_data)
    {
        confess "ERROR: specified species '$species_name' found more than 1 time in database!\n" . join(', ', map({$_->[1];} @species_data));
    }

    if (1 == @species_data)
    {
        # get species ID
        $species_id   = $species_data[0]->[0];
        # use the name in database (nb.: we did a case insensitive search)
        $species_name = $species_data[0]->[1];
    }
    return ($species_name, $species_id);
}


=pod

=head2 setTaxonomicOrder

B<Description>: Reorder species in database and place the given species right
after the provided previous species.

B<ArgsCount>: 2

=over 4

=item arg: $species_name (string) (R)

Name of the .

=item arg: $previous_species (string) (R)

D.

=back

B<Return>: (string)

A.

B<Example>:

    my

=cut

sub setTaxonomicOrder
{
    my ($species_name, $previous_species) = @_;

    if ($species_name && $previous_species && ($species_name ne $previous_species))
    {
        my ($taxonomic_position) = ($dbh->selectrow_array("SELECT tax_order FROM species WHERE species_name = ?;", undef, $previous_species));
        if ($taxonomic_position)
        {
            $dbh->do("UPDATE species SET tax_order = tax_order + 1 WHERE tax_order > $taxonomic_position;")
                or confess "ERROR: Failed to update taxonomic order!\n" . $dbh->errstr;
            $dbh->do("UPDATE species SET tax_order = " . ($taxonomic_position+1) . " WHERE species_name = '$species_name';")
                or confess "ERROR: Failed to set taxonomic order!\n" . $dbh->errstr;
        }
        else
        {
            warn "WARNING: unable to set species taxonomic order for species '$species_name' because the specified previous species code '$previous_species' was not found in database!\n";
        }
    }
}


=pod

=head2 updateSpecies

B<Description>: R

B<ArgsCount>: 1

=over 4

=item arg: $d (integer) (R)

D.

=back

B<Return>: (string)

A.

B<Example>:

    my

=cut

sub updateSpecies
{
    my ($species_id, $species_name, $species_taxonomy, $organism, $common_name, $url_institute, $url_fasta, $url_picture, $version, $previous_species) = @_;

    if (!$species_id)
    {
        confess "ERROR: updateSpecies: No species_id provided!\n";
    }

    # update species fields
    my %species_fields;
    if ($species_taxonomy)
    {
        $species_fields{'tax_id'} = $species_taxonomy;
    }
    if ($organism)
    {
        $species_fields{'organism'} = $organism;
    }
    if ($common_name)
    {
        $species_fields{'common_name'} = $common_name;
    }
    if ($url_institute)
    {
        $species_fields{'url_institute'} = $url_institute;
    }
    if ($url_fasta)
    {
        $species_fields{'url_fasta'} = $url_fasta;
    }
    if ($url_picture)
    {
        $species_fields{'url_picture'} = $url_picture;
    }
    if ($version)
    {
        $species_fields{'version'} = $version;
    }
    # update species fields
    if (keys(%species_fields))
    {
        $dbis->update('species', \%species_fields, { 'species_id' => $species_id });
    }

    # set tax_order
    setTaxonomicOrder($species_name, $previous_species);

    return 1;
}


=pod

=head2 insertSpecies

B<Description>: R

B<ArgsCount>: 1

=over 4

=item arg: $d (integer) (R)

D.

=back

B<Return>: (string)

A.

B<Example>:

    my

=cut

sub insertSpecies
{
    my ($species_name, $species_taxonomy, $organism, $common_name, $url_institute, $url_fasta, $url_picture, $version, $previous_species) = @_;
    my $species_id = 0;

    # check required parameters for species insertion
    if (!$species_name)
    {
        confess "ERROR: insertSpecies: no species_name provided!\n";
    }
    if (!$species_taxonomy)
    {
        confess "ERROR: insertSpecies: a taxonomy should be specified to insert a new species but it was not provided!\n";
    }
    if (!$organism)
    {
        confess "ERROR: insertSpecies: an organism should be specified to insert a new species but it was not provided!\n";
    }
    if (!$common_name)
    {
        $common_name = '';
        warn "WARNING: insertSpecies: a common species name should be specified to insert a new species but it was not provided!\n";
    }
    if (!$url_institute)
    {
        $url_institute = '';
        warn "WARNING: insertSpecies: an institute URL should be specified to insert a new species but it was not provided!\n";
    }
    if (!$url_fasta)
    {
        $url_fasta = '';
        warn "WARNING: insertSpecies: a FASTA URL should be specified to insert a new species but it was not provided!\n";
    }
    if (!$url_picture)
    {
        $url_picture = '';
    }
    if (!$version)
    {
        $version = undef;
        warn "WARNING: insertSpecies: a species genome version should be specified to insert a new species but it was not provided!\n";
    }
    # insert new species
    $dbis->insert('species', {
                                'species_name'    => $species_name,
                                'tax_id'          => $species_taxonomy,
                                'organism'        => $organism,
                                'common_name'     => $common_name,
                                'url_institute'   => $url_institute,
                                'url_fasta'       => $url_fasta,
                                'url_picture'     => $url_picture,
                                'version'         => $version,
                                'sequence_number' => 0,
                             } );

    # get new species ID
    my @species_data = @{$dbh->selectall_arrayref("SELECT species_id FROM species WHERE species_name = ?;", undef, $species_name)};
    if (1 != @species_data)
    {
        confess "ERROR: Failed to insert and retrieve the new species!\n";
    }
    $species_id   = $species_data[0]->[0];

    # set tax_order
    setTaxonomicOrder($species_name, $previous_species);

    return $species_id;
}


=pod

=head2 loadFASTAinHashes

B<Description>: R

B<ArgsCount>: 1

=over 4

=item arg: $d (integer) (R)

D.

=back

B<Return>: (string)

A.

B<Example>:

    my

=cut

#+FIXME: replace with Greenphyl::LoadFASTA
sub loadFASTAinHashes
{
    my ($fasta_file_path)   = @_;
    tie my %fasta_sequences, 'Greenphyl::HashInsensitive';
    tie my %fasta_bio_sequences, 'Greenphyl::HashInsensitive';

    my $input_fasta_fh = Bio::SeqIO->newFh(
        '-file'   => $fasta_file_path,
        '-format' => 'Fasta'
    );

    while (<$input_fasta_fh>)
    {
        #+FIXME: check if sequence name has to be filtered (FilterSequence.pm)
        # remove ending '*' from sequence content
        my $seq_content = $_->seq;
        $seq_content =~ s/\*$//;
        # store
        $fasta_sequences{$_->display_id} = {
            $SEQUENCE_CONTENT => $seq_content,
            $SEQ_LENGTH       => $_->length,
            $ANNOTATION       => $_->desc,
        };
        # keep Bio::Seq objects in a second hash for later use
        $fasta_bio_sequences{$_->display_id} = $_;
    }
    return (\%fasta_sequences, \%fasta_bio_sequences);
}


=pod

=head2 createFamilyFastaFile

B<Description>: R

B<ArgsCount>: 1

=over 4

=item arg: $d (integer) (R)

D.

=back

B<Return>: (string)

A.

B<Example>:

    my

=cut

sub createFamilyFastaFile
{
    my ($file_path, $family_id, $species_names, $additional_parameters) = @_;

    # select which sequences/species to export
    my $filter_species = ($species_names && @$species_names && $species_names->[0]) ? ' AND SP.species_name  IN(\'' . join("', '", @$species_names) . '\')' : '';

    my $sql_query = "SELECT concat(S.seq_textid, '_', SP.species_name) AS \"seq_textid\", S.sequence AS \"sequence\"
                         FROM sequences S, species SP, seq_is_in SI
                         WHERE S.species_id = SP.species_id
                               AND SI.seq_id = S.seq_id
                               $filter_species
                               AND SI.family_id = ?";

    createFastaFileFromQuery($file_path, $sql_query, $additional_parameters, $family_id);
}


=pod

=head2 createSpeciesFastaFile

B<Description>: R

B<ArgsCount>: 1

=over 4

=item arg: $d (integer) (R)

D.

=back

B<Return>: (string)

A.

B<Example>:

    my

=cut

sub createSpeciesFastaFile
{
    my ($file_path, $species_name, $additional_parameters) = @_;

    # select which sequences to export
    my $sql_query = "SELECT S.seq_textid AS \"seq_textid\", S.sequence AS \"sequence\"
                         FROM sequences S, species SP
                         WHERE S.species_id = SP.species_id
                               AND SP.species_name = ?";

    createFastaFileFromQuery($file_path, $sql_query, $additional_parameters, $species_name);
}


=pod

=head2 createFastaFileFromQuery

B<Description>: R

B<ArgsCount>: 4

=over 4

=item arg: $fasta_filename (string) (R)

The name of the fasta file to generate.

=item arg: $sql_query (string) (R)

The query must return in first column a seq_textid with label 'seq_textid' and
in second column a sequence (content) with label 'sequence'.

=item arg: $additional_parameters (hash) (U)

additional parameters such as:
-'exclude_sequences': an array of sequences name to exclude from the FASTA
-'remove_splice_forms': boolean set to a true value if splice forms should be
  removed
-'min_seq': minimum number of sequences to create the file
-'max_seq': maximum number of sequences to create the file

=item arg: @binds (scalars) (O)

a list of variables to bind to the SQL query.

=back

B<Return>: (boolean)

True: the file has been created. False: no sequence found, file not created.

B<Example>:

    my

=cut

sub createFastaFileFromQuery
{
    my ($fasta_filename, $sql_query, $additional_parameters, @binds) = @_;

    if (!$fasta_filename)
    {
        confess "ERROR: createFastaFileFromQuery: missing FASTA file name!\n";
    }
    elsif (!$sql_query)
    {
        confess "ERROR: createFastaFileFromQuery: missing SQL query to use!\n";
    }
    if (!$additional_parameters)
    {
        $additional_parameters = {};
    }
    elsif (ref($additional_parameters) ne 'HASH')
    {
        confess "ERROR: createFastaFileFromQuery: invalida additional parameter! Should be a hash ref instead of '$additional_parameters'\n";
    }

    my $seq_data = $dbh->selectall_arrayref($sql_query, {'Slice' => {}}, @binds);

    if (@$seq_data)
    {
        # check for sequences to exclude
        my %sequences_to_exclude;
        if (exists($additional_parameters->{'exclude_sequences'}))
        {
            if (ref($additional_parameters->{'exclude_sequences'}) ne 'ARRAY')
            {
                confess "ERROR: Invalid 'exclude_sequences' parameter! Not an array!\n";
            }
            %sequences_to_exclude = map {$_ => 1} @{$additional_parameters->{'exclude_sequences'}};
        }
        # check for splice forms to remove
        if (exists($additional_parameters->{'remove_splice_forms'}) && $additional_parameters->{'remove_splice_forms'})
        {
            $seq_data = Greenphyl::Utils::removeSpliceForm($seq_data);
        }
        
        if ($additional_parameters->{'min_seq'}
            && ($additional_parameters->{'min_seq'} > @$seq_data)) {
            LogDebug("FASTA file not created: not enough sequences (got " . scalar(@$seq_data) . " sequences and needed " . $additional_parameters->{'min_seq'} . " sequences)!\n");
            return 0;
        }

        if ($additional_parameters->{'max_seq'}
            && ($additional_parameters->{'max_seq'} < @$seq_data)) {
            LogDebug("FASTA file not created: too many sequences (got " . scalar(@$seq_data) . " sequences and expected at most " . $additional_parameters->{'max_seq'} . " sequences)!\n");
            return 0;
        }

        my $fasta_writer = Bio::SeqIO->new( '-file' => ">$fasta_filename", '-format' => 'Fasta' );
        LogDebug("FASTA file: '$fasta_filename'\nSequences: " . @$seq_data);
        for (@$seq_data)
        {
            if (!exists($sequences_to_exclude{$_->{'seq_textid'}}))
            {
                my $seq = Bio::Seq->new(
                    '-display_id' => $_->{'seq_textid'},
                    '-seq'        => $_->{'sequence'},
                );
                $fasta_writer->write_seq($seq);
            }
        }
        # created
        return 1;
    }
    # no sequence, not created
    LogDebug("FASTA file not created: no sequence found!\nQUERY: $sql_query\nBINDS: " .  join(', ', @binds));
    return 0;
}


=pod

=head2 getArgumentFilePath

B<Description>: R

B<ArgsCount>: 1

=over 4

=item arg: $d (integer) (R)

D.

=back

B<Return>: (string)

A.

B<Example>:

    my

=cut

sub getArgumentFilePath
{
    my ($filepath, @path_to_check) = @_;

    if (!$filepath)
    {
        confess "ERROR: getArgumentFilePath: No file name provided!\n";
    }

    # check for absolute/relative path or file in current directory
    if (($filepath !~ m/\//) && (!-e $filepath))
    {
        # not relative nor in current dir, find path
CHECK_PATH_LOOP:
        foreach my $path_to_check (@path_to_check)
        {
            $path_to_check =~ s/\/+$//;
            $path_to_check .= '/' . $filepath;
            if (-e $path_to_check)
            {
                $filepath = $path_to_check;
                last CHECK_PATH_LOOP;
            }
        }
    }

    return $filepath;
}


=pod

=head2 copyFile

B<Description>: R

B<ArgsCount>: 1

=over 4

=item arg: $d (integer) (R)

D.

=back

B<Return>: (string)

A.

B<Example>:

    my

=cut

sub copyFile
{
    my ($src_file_path, $target_file_path, $params) = @_;
    my ($error_message,
        $warn_only) =
        ($params->{'error_message'},
         $params->{'warn_only'});

    # parameters check...
    if (!$src_file_path)
    {
        confess "ERROR: copyFile: no source file specified!\n";
    }
    if (!-e $src_file_path)
    {
        confess "ERROR: copyFile: source file does not exist!\n";
    }
    if (!$target_file_path)
    {
        confess "ERROR: copyFile: no target file specified!\n";
    }
    if (-e $target_file_path)
    {
        warn "WARNING: copyFile: target file already exists and will be overwritten!\n";
    }
    # set default error message if none specified
    $error_message ||= "ERROR: Failed to copy file ('$src_file_path' to '$target_file_path')!\n$!\n";
    # do the copy
    if (system('cp -f ' . $src_file_path . ' ' . $target_file_path))
    {
        if ($warn_only)
        {
            warn $error_message;
        }
        else
        {
            confess $error_message;
        }
    }
}


=pod

=head2 get

B<Description>: R

B<ArgsCount>: 1

=over 4

=item arg: $d (integer) (R)

D.

=back

B<Return>: (string)

A.

B<Example>:

    my

=cut

sub auto_prompt
{
    if ($no_prompt && (2 <= @_) && (exists($_[1]->{'default'})))
    {
        return $_[1]->{'default'};
    }
    else
    {
        prompt(@_);
    }
}


=pod

=head2 get

B<Description>: R

B<ArgsCount>: 1

=over 4

=item arg: $d (integer) (R)

D.

=back

B<Return>: (string)

A.

B<Example>:

    my

=cut

sub loadSeqData
{
    my ($input_object) = @_;
    my $seq_hash = {};

    if (ref($input_object) eq 'Bio::Seq')
    {
        #+FIXME: filter_text_id
        my $query_seq_name = $input_object->display_id || '';
        if (!$input_object->length)
        {
            cluck "WARNING: Length missing for sequence '$query_seq_name'!\n";
        }
        $seq_hash = {
            'textid'    => $query_seq_name,
            #'seq'       => $input_object->seq,
            'id'        => $dbh->selectrow_array("SELECT seq_id FROM sequences WHERE seq_textid = ? LIMIT 1;", undef, $query_seq_name) || undef,
            'length'    => $input_object->length || undef,
            'family_id' => undef,
        };
    }
    elsif (ref($input_object) eq 'Bio::Search::Hit::BlastHit')
    {
        #+FIXME: filter_text_id
        # my $hit_seq_name = filter_text_id($input_object->name);
        my $hit_seq_name = $input_object->name || '';
        if (!$input_object->length)
        {
            cluck "WARNING: Length missing for sequence '$hit_seq_name'!\n";
        }
        $seq_hash = {
            'textid'    => $hit_seq_name,
            'id'        => $dbh->selectrow_array("SELECT seq_id FROM sequences WHERE seq_textid = ?", undef, $hit_seq_name),
            'length'    => $input_object->length || undef,
            'family_id' => undef,
        };
    }
    # only take class 1 families
    ($seq_hash->{'family_id'}) = $dbh->selectrow_array("SELECT s.family_id FROM seq_is_in s JOIN found_in f ON f.class_id = 1 AND f.family_id = s.family_id WHERE s.seq_id = ? LIMIT 1;", undef, $seq_hash->{'id'});

    return $seq_hash;
}


=pod

=head2 get_median_seq_length_for_family

B<Description>: R

B<ArgsCount>: 1

=over 4

=item arg: $d (integer) (R)

D.

=back

B<Return>: (string)

A.

B<Example>:

    my

=cut

sub get_median_seq_length_for_family
{
    my ($family_id) = @_;

    # level 1 only
    my $sql_query = "
        SELECT s.seq_length
        FROM sequences s
            JOIN seq_is_in sii ON s.seq_id = sii.seq_id
        WHERE sii.family_id = ?;";

    my $sequences = $dbh->selectcol_arrayref($sql_query, undef, $family_id);

    # check if we got a result
    if (!@$sequences)
    {
        return 0;
    }

    my $stats = Statistics::Descriptive::Full->new();
    $stats->add_data(@$sequences);

    return $stats->median();
}


=pod

=head2 get

B<Description>: R

B<ArgsCount>: 1

=over 4

=item arg: $d (integer) (R)

D.

=back

B<Return>: (string)

A.

B<Example>:

    my

=cut

sub removeObsoleteFamilies
{
    my ($path_manager) = @_;

    # check parameters
    if (!$path_manager)
    {
        confess "ERROR: removeObsoleteFamilies(): no path manager provided!";
    }

    # remove obsolete families and log...
    print "\nRemove obsolete families...\n";
    LogInfo("Remove obsolete families...");
    my $obsolete_families_count = 0;
    my ($obsolete_family_fh, $annotated_obsolete_family_fh);
    my $obsolete_family_file_path = $path_manager->getOutputDirectory() . '/' . $Greenphyl::Config::OBSOLETE_FAMILY_LIST_FILE;
    my $annotated_obsolete_family_file_path = $path_manager->getOutputDirectory() . '/' . $Greenphyl::Config::ANNOTATED_OBSOLETE_FAMILY_LIST_FILE;
    open($obsolete_family_fh, ">$obsolete_family_file_path") or confess("ERROR: Failed to create log file '$obsolete_family_file_path'!\n$!\n");
    open($annotated_obsolete_family_fh, ">$annotated_obsolete_family_file_path") or confess("ERROR: Failed to create log file '$annotated_obsolete_family_file_path'!\n$!\n");
    print {$obsolete_family_fh} "family_id\tfamily_name\tvalidated\n";
    print {$annotated_obsolete_family_fh} "family_id\tfamily_name\tvalidated\n";
    # fetch obsolete family ID, name and validated level (families with 0 or 1 member)
    foreach my $obsolete_family (@{$dbh->selectall_arrayref("SELECT f.family_id AS family_id, f.family_name AS family_name, f.validated AS validated FROM family f WHERE 1 >= (SELECT count(1) FROM seq_is_in si WHERE si.family_id = f.family_id);", { 'Slice' => {} })})
    {
        # remove obsolete families
        LogInfo('-' . $obsolete_family->{'family_id'});
        $dbis->delete('family', {'family_id' => $obsolete_family->{'family_id'}});
        # log removed family ID and name
        print {$obsolete_family_fh} $obsolete_family->{'family_id'} . "\t" . $obsolete_family->{'family_name'} . "\t" . $obsolete_family->{'validated'} . "\n";
        # log family ID and name if family.validated IN (1, 2)
        if ((1 == $obsolete_family->{'validated'})
            || (2 == $obsolete_family->{'validated'}))
        {
            print {$annotated_obsolete_family_fh} $obsolete_family->{'family_id'} . "\t" . $obsolete_family->{'family_name'} . "\t" . $obsolete_family->{'validated'} . "\n";
        }
        ++$obsolete_families_count;
    }
    close($obsolete_family_fh);
    close($annotated_obsolete_family_fh);
    print "...done!\n";
    LogInfo("...done!");
    print "Obsolete families removed: " . $obsolete_families_count . " (see '$obsolete_family_file_path')\n";

    return $obsolete_families_count;
}


=pod

=head2 processIPRProtein

B<Description>: .

B<ArgsCount>: 2

=over 4

=item arg: $s (string) (R)

Name of the .

=back

B<Return>: (string)

A.

B<Example>:

    my

=cut

sub processIPRProtein
{
    my ($ipr_protein, $ipr_version, $ipr_update_log_fh) = @_;

    my $stats = {
        'unusable_ipr_xml_count' => 0,
        'matched_seq_count'      => 0,
        'unmatched_seq_count'    => 0,
        'inserted_ipr_count'     => 0,
        'updated_ipr_count'      => 0,
        'reused_ipr_count'       => 0,
    };

    my $sql_query;

    if ('HASH' ne ref($ipr_protein))
    {
        confess "ERROR: processIPRProtein: invalid ipr_protein argument!\n";
    }

    # check XML data
    if (!$ipr_protein->{'interpro'})
    {
        LogInfo(" No IPR match (empty interpro)");
        $stats->{'unmatched_seq_count'} = 1;
        return $stats;
    }
    elsif (0 == @{$ipr_protein->{'interpro'}})
    {
        LogInfo(" No IPR match (0 interpro)");
        $stats->{'unmatched_seq_count'} = 1;
        return $stats;
    }

    if (!$ipr_protein->{'id'})
    {
        LogInfo(" Protein with no ID");
        $stats->{'unusable_ipr_xml_count'} = 1;
        return $stats;
    }
    LogInfo(" Sequence: " . $ipr_protein->{'id'});

    # fetch sequence ID in database
    my ($seq_id) = $dbh->selectrow_array("SELECT seq_id FROM sequences WHERE seq_textid LIKE ? LIMIT 1;", undef, $ipr_protein->{'id'});
    if (!$seq_id)
    {
        warn "ERROR: Sequence '" . $ipr_protein->{'id'} . "' not found in database!\n";
        LogInfo(" WARNING: sequence '" . $ipr_protein->{'id'} . "' not found in database!");
        $stats->{'unmatched_seq_count'} = 1;
        return $stats;
    }
    LogInfo(" Sequence ID: $seq_id");

    # we got something
    $stats->{'matched_seq_count'} = 1;

    # store data in database...
IPR_RESULT_LOOP:
    foreach my $ipr_data (@{$ipr_protein->{'interpro'}})
    {
        #print "\r-IPR:  " . $ipr_data->{'id'} . "                                        ";
        LogInfo(" -IPR:  " . $ipr_data->{'id'});
        # check if IPR is valid
        if (!$ipr_data->{'id'})
        {
            warn "WARNING: Unable to get IPR name!\n";
            LogWarning("Unable to get IPR name!");
            next IPR_RESULT_LOOP;
        }
        elsif ($ipr_data->{'id'} !~ m/^IPR\d+/)
        {
            LogInfo("  Not a valid IPR: skipped");
            next IPR_RESULT_LOOP;
        }
        LogDebug("  Desc: " . $ipr_data->{'name'});
        LogDebug("  Type: " . $ipr_data->{'type'});

        #+Val: following lines commented; we do not handle multiple matches at the moment
        #print "  Matches:\n";
        #foreach my $match (@{$ipr_data->{'match'}})
        #{
        #    print "  -Match ID:   " . $match->{'id'} . "\n";
        #    print "   Start: " . $match->{'location'}->[0]->{'start'} . "\n";
        #    print "   End:   " . $match->{'location'}->[0]->{'end'} . "\n";
        #    print "   Score: " . $match->{'location'}->[0]->{'score'} . "\n";
        #}

        my $match = $ipr_data->{'match'}->[0];
        LogDebug("  -Match ID:   " . $match->{'id'});
        LogDebug("   Start: " . $match->{'location'}->[0]->{'start'});
        LogDebug("   End:   " . $match->{'location'}->[0]->{'end'});
        LogDebug("   Score: " . $match->{'location'}->[0]->{'score'});

        # check if IPR already exists
        my $greenphyl_ipr = $dbh->selectrow_hashref("SELECT ipr_id, ipr_code, ipr_longdesc, type, version FROM ipr WHERE ipr_code = ?;", undef, $ipr_data->{'id'});
        $greenphyl_ipr->{'version'} ||= 'NULL';
        if (!$greenphyl_ipr || !$greenphyl_ipr->{'ipr_id'})
        {
            LogInfo("  Not in DB: insert");
            # it does not exist, insert it
            $dbis->insert('ipr', {
                                        'ipr_code'     => $ipr_data->{'id'},
                                        'ipr_longdesc' => $ipr_data->{'name'},
                                        'type'         => $ipr_data->{'type'},
                                        'version'      => $ipr_version,
                                     } );
            # fetch new id
            $greenphyl_ipr = $dbh->selectrow_hashref("SELECT ipr_id, ipr_code, ipr_longdesc, type, version FROM ipr WHERE ipr_code = ?;", undef, $ipr_data->{'id'});
            $greenphyl_ipr->{'version'} ||= 'NULL';
            print {$ipr_update_log_fh} "insert\t" . $greenphyl_ipr->{'ipr_id'} . "\t" . $greenphyl_ipr->{'ipr_code'} . "\t" . $greenphyl_ipr->{'type'} . "\t" . $greenphyl_ipr->{'version'} . "\t" . $greenphyl_ipr->{'ipr_longdesc'} . "\n";
            $stats->{'inserted_ipr_count'}++;
        }
        elsif (($greenphyl_ipr->{'ipr_longdesc'} ne $ipr_data->{'name'})
                || ($greenphyl_ipr->{'type'} ne $ipr_data->{'type'}))
        {
            # it does exist but description or type should be updated
            LogInfo("  In DB but not up-to-date: update");
            # update database
            $dbis->update('ipr', {
                    'ipr_longdesc' => $ipr_data->{'name'},
                    'type'         => $ipr_data->{'type'},
                    'version'      => $ipr_version,
                },
                {
                    'ipr_id' => $greenphyl_ipr->{'ipr_id'},
                },
            );
            # log old values
            print {$ipr_update_log_fh} "update_old\t" . $greenphyl_ipr->{'ipr_id'} . "\t" . $greenphyl_ipr->{'ipr_code'} . "\t" . $greenphyl_ipr->{'type'} . "\t" . $greenphyl_ipr->{'version'} . "\t" . $greenphyl_ipr->{'ipr_longdesc'} . "\n";
            # update data in memory
            $greenphyl_ipr->{'ipr_longdesc'} = $ipr_data->{'name'};
            $greenphyl_ipr->{'type'}         = $ipr_data->{'type'};
            $greenphyl_ipr->{'version'}      = $ipr_version;
            # log new values
            print {$ipr_update_log_fh} "update_new\t" . $greenphyl_ipr->{'ipr_id'} . "\t" . $greenphyl_ipr->{'ipr_code'} . "\t" . $greenphyl_ipr->{'type'} . "\t" . $greenphyl_ipr->{'version'} . "\t" . $greenphyl_ipr->{'ipr_longdesc'} . "\n";
            $stats->{'updated_ipr_count'}++;
        }
        else
        {
            LogInfo("  In DB: yes");
            $stats->{'reused_ipr_count'}++;
        }

        # insert data into seq_has_an
        #$dbis->insert('seq_has_an', {
        #        'seq_id'      => $seq_id,
        #        'ipr_id'      => $greenphyl_ipr->{'ipr_id'},
        #        'ipr_start'   => $match->{'location'}->[0]->{'start'},
        #        'ipr_stop'    => $match->{'location'}->[0]->{'end'},
        #        'ipr_score'   => $match->{'location'}->[0]->{'score'},
        #    }
        #);
        $sql_query = "INSERT IGNORE INTO seq_has_an (seq_id, ipr_id, ipr_start, ipr_stop, ipr_score) VALUES (?, ?, ?, ?, ?);";
        LogDebug("QUERY (insert in seq_has_an): $sql_query");
        $dbh->do($sql_query, undef, $seq_id, $greenphyl_ipr->{'ipr_id'}, $match->{'location'}->[0]->{'start'}, $match->{'location'}->[0]->{'end'}, $match->{'location'}->[0]->{'score'});

    }
    return $stats;
}



=pod

=head2 loadBestHit

B<Description>: R

B<ArgsCount>: 1

=over 4

=item arg: $d (integer) (R)

D.

=back

B<Return>: (string)

A.

B<Example>:

    my

=cut

sub loadBestHit
{
    my ($blast_result_filename) = @_;
    my %species_hits = ();

    # parse BLAST file
    LogInfo("Processing BLAST hits for '$blast_result_filename'...");
    print "Processing BLAST best hits for $blast_result_filename...\n";
    my $in = new Bio::SearchIO('-format' => 'blasttable', '-file' => $blast_result_filename) or confess("ERROR: $!\n");
    while (my $result = $in->next_result())
    {
        print "\r working on " . $result->query_name . "                                        ";
        if (my $hit = $result->next_hit())
        {
            if (my $hsp = $hit->next_hsp())
            {
                #print "DEBUG: got hit " . $hit->name . "\n"; #+debug
                #print "DEBUG: got bits score " . $hsp->bits . "\n"; #+debug
                #print "DEBUG: got e-value " . $hsp->evalue . "\n"; #+debug
                $species_hits{$result->query_name} = {'hit' => $hit->name, 'score' => $hsp->bits, 'eval' => $hsp->evalue};
                if (!$hsp->bits || !$hsp->evalue)
                {
                    confess "ERROR: failed to parse BLAST results (missing a score or an e-value for match between '" . $result->query_name . "' and '" . $hit->name . "')!\n";
                }
            }
            else
            {
                LogWarning("no hsp found!");
                print "WARNING: no hsp found for '" . $result->query_name . "'!\n";
            }
        }
        else
        {
            LogWarning("no hit found for '" . $result->query_name . "'!");
            print "WARNING: no hit found!\n";
        }
    }
    $in->close();
    $in = undef;
    LogInfo("...done.");
    print "\n...done.\n";
    return \%species_hits;
}


=pod

=head2 findAndStoreSpeciesBBMH

B<Description>: R

B<ArgsCount>: 1

=over 4

=item arg: $d (integer) (R)

D.

=back

B<Return>: (string)

A.

B<Example>:

    my

=cut

sub findAndStoreSpeciesBBMH
{
    my ($species_name1, $species_name2) = @_;
    print "BBMH: $species_name1 vs $species_name2\n";

    # init best hit storage
    my ($species1_hits, $species2_hits) = ((), ());

    my $blast_result_filename = $Greenphyl::Config::BBMH_PATH . '/' . $species_name1 . $SPECIES_BLAST_NAME_SEPARATOR . $species_name2;
    my $blast_result_filename2 = $Greenphyl::Config::BBMH_PATH . '/' . $species_name2 . $SPECIES_BLAST_NAME_SEPARATOR . $species_name1;

    #+Mat: make sure we got the corresponding file to avoid error (and rollback)
    if (!-r $blast_result_filename)
    {
        LogWarning("Missing BLAST result file: $blast_result_filename\nSkipping BBMH insertion of BBMH between $species_name1 and $species_name2.");
        return;
    }
    if (!-r $blast_result_filename2)
    {
        LogWarning("Missing BLAST result file: $blast_result_filename2\nSkipping BBMH insertion of BBMH between $species_name1 and $species_name2.");
        return;
    }

    # parse BLAST file for species1 vs species2
    $species1_hits = loadBestHit($blast_result_filename);

    # parse BLAST file for species2 vs species1
    $species2_hits = loadBestHit($blast_result_filename2);

    # prepare SQL queries
    my $sql_query = "SELECT seq_id FROM sequences WHERE seq_textid LIKE ?;";
    my $sth_get_seq_id_from_seq_textid = $dbh->prepare($sql_query);

    $sql_query = "INSERT INTO bbmh (query, hit, score, eval) VALUES(?, ?, ?, ?);";
    my $sth_insert_bbmh = $dbh->prepare($sql_query);

    # for each result of first BLAST check if it correspond to result of second BLAST
    foreach my $seq_textid1 (keys(%$species1_hits))
    {
        # BBMH?
        if (exists($species1_hits->{$seq_textid1})
            && exists($species2_hits->{$species1_hits->{$seq_textid1}->{'hit'}})
            && ($species2_hits->{$species1_hits->{$seq_textid1}->{'hit'}}->{'hit'} eq $seq_textid1))
        {
            # yes, store BBMH...
            LogInfo("-Query: $seq_textid1");
            # fetch query sequence seq_id in database
            $sth_get_seq_id_from_seq_textid->execute($seq_textid1);
            my ($seq_id1) = $sth_get_seq_id_from_seq_textid->fetchrow_array();
            if (!$seq_id1)
            {
                LogInfo(" WARNING: query sequence '$seq_textid1' not found in database!");
                cluck "WARNING: query sequence '$seq_textid1' not found in database!\n";
            }
            else
            {
                LogInfo(" Query seq_id: $seq_id1");
                # fetch matched sequence seq_id in database
                my $seq_textid2 = $species1_hits->{$seq_textid1}->{'hit'};
                $sth_get_seq_id_from_seq_textid->execute($seq_textid2);
                my ($seq_id2) = $sth_get_seq_id_from_seq_textid->fetchrow_array();
                if (!$seq_id2)
                {
                    LogInfo(" WARNING: hit sequence '$seq_textid2' not found in database!");
                    cluck "WARNING: hit sequence '$seq_textid2' not found in database!\n";
                }
                else
                {
                    LogInfo(" Hit: $seq_textid2");
                    LogInfo(" Hit seq_id: $seq_id2");
                    #print "\r-$seq_textid1 & $seq_textid2                                        ";
                    LogDebug("-$seq_textid1 -> $seq_textid2: score=" . $species1_hits->{$seq_textid1}->{'score'} . ", e-value=" . $species1_hits->{$seq_textid1}->{'eval'});
                    LogDebug(" $seq_textid2 -> $seq_textid1: score=" . $species2_hits->{$seq_textid2}->{'score'} . ", e-value=" . $species2_hits->{$seq_textid2}->{'eval'});
                    # store BBMH species -> db_species
                    if (0 == $sth_insert_bbmh->execute($seq_id1, $seq_id2, $species1_hits->{$seq_textid1}->{'score'}, $species1_hits->{$seq_textid1}->{'eval'}))
                    {
                        LogInfo("ERROR: failed to insert BBMH between '$seq_textid1' and '$seq_textid2' in database!");
                        cluck "ERROR: failed to insert BBMH between '$seq_textid1' and '$seq_textid2' in database!\n";
                    }
                    # store BBMH db_species -> species
                    if (0 == $sth_insert_bbmh->execute($seq_id2, $seq_id1, $species2_hits->{$seq_textid2}->{'score'}, $species2_hits->{$seq_textid2}->{'eval'}))
                    {
                        LogInfo("ERROR: failed to insert BBMH between '$seq_textid2' and '$seq_textid1' in database!");
                        cluck "ERROR: failed to insert BBMH between '$seq_textid2' and '$seq_textid1' in database!\n";
                    }
                    LogInfo(" BBMH stored");
                }
            }
        }
        else
        {
            # not a mutual hit
            # nb.: only non-mutual hit from the selected species side are identified here
            #      and not the non-mutual from the db species
        }
    } # end of foreach my $seq_textid1 keys(%$species1_hits)
    print "\n";

    $sth_get_seq_id_from_seq_textid->finish();
    $sth_insert_bbmh->finish();
}


=pod

=head2 showMainMenu

B<Description>: R

B<ArgsCount>: 1

=over 4

=item arg: $d (integer) (R)

D.

=back

B<Return>: (string)

A.

B<Example>:

    my

=cut

sub showMainMenu
{
    my %parameters = (@_);
    print <<"___1006_MAIN_MENU___";

 Main Menu
-----------

    $RUN_INT_CODE_INFO. Information/Configuration
    $RUN_INT_CODE_SEQUENCE_UPDATE. Protein Sequence Insertion/Update
    $RUN_INT_CODE_FAMILY_CLUSTERING. Protein Family Clustering (using blast)
    $RUN_INT_CODE_EXTERNAL_DATA_UPDATE. External Data Loading (UNIPROT)
    $RUN_INT_CODE_INTERPROSCAN_UPDATE. InterProScan Insertion/Update
    $RUN_INT_CODE_REMOVE_SPECIES. Remove a Species
    $RUN_INT_CODE_CLEAN_FAMILIES. Remove Obsolete Families
    $RUN_INT_CODE_GENERATE_FAM_FASTA. Generate family FASTA files
    $RUN_INT_CODE_MEME_MAST. Run MEME/MAST
    $RUN_INT_CODE_GENERATE_SP_FASTA. Generate species FASTA files
    $RUN_INT_CODE_GENERATE_ORPH_FASTA. Generate orphan sequence FASTA file for a species
    $RUN_INT_CODE_LOAD_BBMH. Load BBMH
    $RUN_INT_CODE_UPDATE_GO. Update GO mapping 
    $RUN_INT_CODE_UPDATE_TAXONOMY. Update homepage taxonomy tree
    $RUN_INT_CODE_LOAD_CLASSIFICATION. Load MCL clustering
    $RUN_INT_CODE_LOAD_ORTHOLOGS. Load orthologs
    $RUN_INT_CODE_ORPHANS_CLUSTERING. Protein Family Clustering (using orphans)
    x. Exit

___1006_MAIN_MENU___
    if (exists($parameters{'species'}) && $parameters{'species'})
    {
        print " Current species selected: " . $parameters{'species'} . "\n\n";
    }

    return prompt("Enter your choice: ", { 'constraint' => '[xXqQ0-9]' });
}




# Script options
#################

=pod

=head1 OPTIONS

    update.pl [-help] [-man] [-debug] [-noprompt]
              < -code seq <-f FASTA_FILE> <-sp SPECIES> [-o ORGANISM] [-t TAXONOMIC_ID] [-n COMMON_NAME] [-url_institute URL1] [-url_fasta URL2] [-url_picture URL3] [-v SPECIES_VERSION] [-prevspecies SPECIES2]
              | -code fam <-sp SPECIES> [-update] [-b BLAST_FILE]
              | -code ext <-sp SPECIES> <-interpro FILE | -kegg FILE | -pirsf FILE | -uniprot FILE>
              | -code ipr <-sp SPECIES> [-interpro IPR_XML_PATH] <-iprversion IPR_VERSION>
              | -code remove_species <-sp SPECIES>
              | -code clean_fam
              | -code famfasta
              | -code mm
              | -code spfasta
              | -code orfasta <-sp SPECIES>
              | -code bbmh <-bbmhfile FILE>
              | -code go [-obofile FILE] [-ipr2gofile FILE] [-uniprot2gofile FILE]
              | -code tax [-tree FILE]
              | -code ortho [-pipe_res_path PATH]
              >

=head2 Parameters

=over 4

=item B<-help>:

display this help message.

=item B<-man>:

display user manual.

=item B<-debug>:

set debug mode on (nothing committed to database but files are generated).

=item B<-noprompt>:

Disable prompting user for some choices and use a default behavior (stops on
errors, use default prompt option in other cases).

=item B<-code CODE or -c CODE>:

Specifies the update process to perform. CODE can be one of:
* info: informations about current database and configuration
* seq: species sequences insertion/update;
* fam: protein sequence family insertion/update;
* ipr: interproscan result insertion/update;
* ext: external data insertion/update;
* remove_species: remove the specified species and all its sequences;
* clean_fam: remove obsolete families;
* famfasta: generate family FASTA files;
* mm: run MEME/MAST.
* spfasta: generate species FASTA
* orfasta: generate orphan sequence FASTA
* bbmh: load BBMH data

=item B<-file FASTA_FILE or -f FASTA_FILE>:

Name of the FASTA input file containing the new sequences.

=item B<-species SPECIES or -sp SPECIES>:

Name of the species to work on (usually a 5 capital letters code).

=item B<-organism ORGANISM or -o ORGANISM>:

organism name required when inserting a new species (for code seq).

=item B<-taxonomy TAXONOMIC_ID or -t TAXONOMIC_ID>:

species taxonomy identifier (integer from NCBI) required when inserting a new
species (for code seq).

=item B<-name COMMON_NAME or -n COMMON_NAME>:

species common name (including spaces) required when inserting a new species
(for code seq).

=item B<-url_institute URL1>:

URL of the institute who provided the species sequences wished when inserting a
new species (for code seq).

=item B<-url_fasta URL2>:

URL of the species FASTA file wished when inserting a new species (for code
seq).

=item B<-url_picture URL3>:

URL of the species image (for code seq).

=item B<-version SPECIES_VERSION or -v SPECIES_VERSION>:

Species version required when inserting a new species (for code seq).

=item B<-prevspecies SPECIES2>

Species code (5 caps letters) of the species that should appear before the
processed species on the family composition tree.

=item B<-interpro IPR_XML_PATH or -ipr IPR_XML_PATH>:

Path to the directory including all the Interpro scan XML results.
If not specified, the species update data path is used and XML should
be stored in a 'iprscan' subdirectory.
The path may include a trailing '/' but it's not requiered.

=item B<-iprversion IPR_VERSION or -iprv IPR_VERSION>:

Interpro version required when processing Interproscan results (for code ipr).

=item B<-update>:

Set update mode in case of species update (for code fam).

=item B<-blast BLAST_FILE>:

Input BLAST file to avoid computation (for code fam).

=item B<-interpro FILE>:

Input Interpro file to load (for code ext).

=item B<-kegg FILE>:

Input KEGG file to load (for code ext).

=item B<-pirsf FILE>:

Input PIRSF file to load (for code ext).

=item B<-uniprot FILE>:

Input UniProt file to load (for code ext).

=item B<-bbmhfile FILE>:

Input BBMH file to load (for code bbmh).

=item B<-obofile FILE>:

Input GO ONO file containing GO data including ID with associated namespaces.

=item B<-ipr2gofile FILE>:

IPR to GO mapping file.

=item B<-uniprot2gofile FILE>:

UniProt to GO mapping file.

=item B<-pipe_res_path PATH>:

Path to pipeline outputs.

=back

=cut

# CODE START
#############

print "\nGreenPhyl Database Updater v$UPDATER_VERSION\n\n";

#
my ($code, $input_filename, $help, $man, $species_id, $species_name,
    $species_taxonomy, $organism, $common_name, $kegg_path, $pirsf_path, $uniprot_path,
    $interpro, $email, $url_institute, $url_fasta, $url_picture,
    $version, $source_blast, $update, $interpro_path, $bbmh_filepath,
    $obo_filepath, $ipr2go_filepath, $uniprot2go_filepath,
    $ipr_version, $sql_query, $previous_species, $interactive_mode,
    $taxonomy_tree_filepath, $pipeline_results_path, $orthologs_pairs_export, $ec2go_filepath);

# parse options
Getopt::Long::Configure("pass_through"); # allows unprocessed options to stay in @ARGV
GetOptions(
    'code|c=s'          => \$code,             # action code (string, see $RUN_CODE_* global constants)
    'fasta|f=s'         => \$input_filename,   # input file (string)
    'species|sp=s'      => \$species_name,     # species name code (usually 5 capital letters)
    'organism|o=s'      => \$organism,         # organism name required when inserting a new species (for code seq)
    'taxonomy|t=s'      => \$species_taxonomy, # species taxonomy required when inserting a new species (for code seq)
    'name|n=s'          => \$common_name,      # species common name (including spaces) required when inserting a new species (for code seq)
    'url_institute=s'   => \$url_institute,    # URL of the institute who provided the species sequences required when inserting a new species (for code seq)
    'url_fasta=s'       => \$url_fasta,        # URL of the species FASTA file required when inserting a new species (for code seq)
    'url_picture=s'     => \$url_picture,      # URL of the picture of a species (code seq)
    'version|v=s'       => \$version,          # species version required when inserting a new species (for code seq)
    'prevspecies|ps=s'  => \$previous_species, # species code of the species that should appear before on the family composition chart
    'update'            => \$update,           # set update in case of species update (for code fam)
    'blast|b=s'         => \$source_blast,     # input BLAST file to avoid computation (for code fam)
    'help'              => \$help,             # display command line help
    'man'               => \$man,              # display user manual
    'debug'             => \$debug,            # debug mode
    'noprompt'          => \$no_prompt,        # when set, disable user prompting and use default behavior
    'interpro|ipr=s'    => \$interpro_path,    # path to Interproscan XML files
    'iprversion|iprv=s' => \$ipr_version,      # Interproscan database release
    'kegg=s'            => \$kegg_path,        # chemin du fichier si kegg a ete coche dans le formulaire, autrement undef.
    'pirsf=s'           => \$pirsf_path,       # chemin du fichier si pirsf a ete coche dans le formulaire, autrement undef.
    'uniprot=s'         => \$uniprot_path,     # chemin du fichier si uniprot a ete coche dans le formulaire, autrement undef.
    'bbmhfile=s'        => \$bbmh_filepath,    # BBMH file path
    'obofile=s'         => \$obo_filepath,     # GO OBO file path
    'ipr2gofile=s'      => \$ipr2go_filepath,  # IPR to GO file path
    'ec2gofile=s'      => \$ec2go_filepath,  # EC to GO file path
    'uniprot2gofile=s'  => \$uniprot2go_filepath, # UniProt to GO file path
    'tree=s'            => \$taxonomy_tree_filepath, # taxonomy Nexick tree
    'pipe_res_path=s'   => \$pipeline_results_path, # pipeline results path

    'email=s'         => \$email,               # pour interproscan
    'sid=i'           => \$species_id,          # species id corresponding to the id in the table species (integer)
) or pod2usage(2);

# parameters check
if ($help) {pod2usage(1);}
if ($man) {pod2usage('-verbose' => 2);}

# check if debug mode was specified or forced by config
$debug ||= $DEBUG;

# set interactive mode if needed
if (!$code)
{
    $interactive_mode = 1;
    # a code should be specified
    $code = showMainMenu('species' => $species_name);
}
else
{
    # non-interactive mode, init some variables
    if (!defined($update))
    {
        $update = 0;
    }
}


my $need_commit = 0;
while ($code !~ m/^([xq]|exit|quit)$/i)
{
    # check if update code is species-based
    if ($code =~ m/^(?:
                      $RUN_INT_CODE_SEQUENCE_UPDATE      |$RUN_CODE_SEQUENCE_UPDATE
                     |$RUN_INT_CODE_FAMILY_CLUSTERING    |$RUN_CODE_FAMILY_CLUSTERING
                     |$RUN_INT_CODE_EXTERNAL_DATA_UPDATE |$RUN_CODE_EXTERNAL_DATA_UPDATE
                     |$RUN_INT_CODE_INTERPROSCAN_UPDATE  |$RUN_CODE_INTERPROSCAN_UPDATE
                     |$RUN_INT_CODE_REMOVE_SPECIES       |$RUN_CODE_REMOVE_SPECIES
                     |$RUN_INT_CODE_GENERATE_ORPH_FASTA  |$RUN_CODE_GENERATE_ORPH_FASTA
					 |$RUN_INT_CODE_ORPHANS_CLUSTERING
                    )$/six)
    {
        $species_name ||= auto_prompt("Please enter a species letter code (5 capital letters code): ", { 'default' => $GLOBAL_UPDATE_DIRECTORY, 'constraint' => $SPECIES_NAME_REGEXP });
        initUpdate($species_name);
    }
    else
    {
        # update code does not require a specific species
        initUpdate($GLOBAL_UPDATE_DIRECTORY);
    }

    # start of the procedure...
    my $start_date = time();

    # start SQL transaction
    if ($dbh->{AutoCommit})
    {
        $dbh->begin_work() or croak $dbh->errstr;
    }
    $need_commit = 0;

    try
    {

        if ($code =~ m/^(?:$RUN_INT_CODE_INFO|$RUN_CODE_INFO)$/si) # info
        {
            # get database info
            my $species_info = '';
            foreach my $species_data (@{$dbh->selectall_arrayref("SELECT species_id, species_name, organism FROM species ORDER BY species_id ASC;")})
            {
                # concate species info
                $species_info .= sprintf("   % 3s. %s (%s)\n", $species_data->[0], $species_data->[1], $species_data->[2]);
            }

            # get BLAST info
            my $blast_info = '';
            if ($Greenphyl::Config::CLUSTER_QUEUE)
            {
                $blast_info  = "BLAST mode:        cluster (queue: $Greenphyl::Config::CLUSTER_QUEUE)\n";
                $blast_info .= "BLAST command:     $Greenphyl::Config::CLUSTER_BLASTALL_COMMAND\n";
            }
            else
            {
                $blast_info  = "BLAST mode:        local\n";
                $blast_info .= "BLAST command:     $Greenphyl::Config::BLASTALL_COMMAND\n";
            }
            $blast_info .= "BLAST parameters:  $Greenphyl::Config::BLASTALL_ADDITIONAL_PARAM\n";
            $blast_info .= "                   $Greenphyl::Config::BLASTALL_TABULAR_FORMAT_PARAM";

            # print status
            print <<"___1583_INFO___";
 GreenPhyl Update Info
-----------------------
Debug mode:        $debug

Selected database: $Greenphyl::Config::DATABASES->[0]{name}
                   $Greenphyl::Config::DATABASES->[0]{login}\@$Greenphyl::Config::DATABASES->[0]{host}:$Greenphyl::Config::DATABASES->[0]{database}

Root directory:    $Greenphyl::Config::BASE_PATH
Data path:         $Greenphyl::Config::DATA_PATH
Temporary path:    $Greenphyl::Config::TEMP_OUTPUT_PATH
Logs:              database

$blast_info
BLAST banks:       $Greenphyl::Config::BLAST_BANKS_PATH
All species bank:  $Greenphyl::Config::ALL_BLAST_BANK

Dump path:         $Greenphyl::Config::DUMP_PATH
Dump command:      $Greenphyl::Config::MYSQL_DUMPER

Species info:
$species_info
-----------------------

___1583_INFO___
            auto_prompt("Hit enter to continue...", { 'default' => '' });
        }
        elsif ($code =~ m/^(?:$RUN_INT_CODE_SEQUENCE_UPDATE|$RUN_CODE_SEQUENCE_UPDATE)$/si) # protein sequence insertion
        {
            $need_commit = 1;
            $input_filename ||= auto_prompt("Please enter the name of the FASTA file to process: ", { 'default' => '', 'constraint' => '\w' });

            print "\nProcessing Sequences Update...\n";
            print "Code seq\nInput FASTA file: '$input_filename'\n";
            LogInfo("Code seq\nInput FASTA file: '$input_filename'");

            # hash_refs of keys=sequence_names and values=hash_ref of
            #  keys = $SEQUENCE_NAME and values $SEQ_LENGTH, $SEQUENCE_CONTENT,
            #  $ANNOTATION and associated values.
            my ($old_list, $kept_list, $inserted_list, $reassigned_list, $still_to_process, $new_list) = ({}, {}, {}, {}, {}, {});
            my $new_fasta_sequences = {};
            my $species_update = 0;
            my $updated_sequence = 0;

            # check if species is in database and retrieve ID
            ($species_name, $species_id) = fetchSpecies($species_name);

            if ($species_id)
            {
                $species_update = 1;
                print "Found species '$species_name' (ID $species_id) into database: update mode\n";
                LogInfo("Found species '$species_name' (ID $species_id) into database: update mode");

                my $species_defaults = $dbh->selectrow_hashref("SELECT * FROM species WHERE species_id = ?;", undef, $species_id);
                if (auto_prompt("Would you like to also update '$species_name' species fields? [y/n] ", { 'default' => 'n', 'constraint' => '[yYnN]' }) =~ m/[yY]/)
                {
                    # get current values
                    my ($previous_species_name) = $dbh->selectrow_array("SELECT species_name FROM species WHERE tax_order = ?;", undef, $species_defaults->{'tax_order'} - 1);
                    $previous_species_name ||= '';
                    # get parameters
                    $species_taxonomy ||= auto_prompt("Please enter species taxonomy identifier (NCBI integer): ", { 'default' => $species_defaults->{'tax_id'} });
                    $organism         ||= auto_prompt("Please enter species organism name: ", { 'default' => $species_defaults->{'organism'} });
                    $common_name      ||= auto_prompt("Please enter species common name: ", { 'default' => $species_defaults->{'common_name'} });
                    $url_institute    ||= auto_prompt("Please enter source institute URL: ", { 'default' => $species_defaults->{'url_institute'} });
                    $url_fasta        ||= auto_prompt("Please enter source FASTA URL: ", { 'default' => $species_defaults->{'url_fasta'} });
                    $url_picture      ||= auto_prompt("Please enter species picture URL: ", { 'default' => $species_defaults->{'url_picture'} });
                    $previous_species ||= auto_prompt("Please provide the code of the species that should appear before this species in the family composition chart: ", { 'default' => $previous_species_name });
                }
                $version          ||= auto_prompt("Please enter species version: ", { 'default' => int($species_defaults->{'version'}) + 1 });

                # update species fields
                updateSpecies($species_id, $species_name, $species_taxonomy, $organism, $common_name, $url_institute, $url_fasta, $url_picture, $version, $previous_species);
            }
            else
            {
                $species_update = 0;
                print "Species '$species_name' not found into database: insertion mode\n";
                LogInfo("Species '$species_name' not found into database: insertion mode");

                # get parameters
                $species_taxonomy ||= auto_prompt("Please enter species taxonomy identifier (NCBI integer): ", { 'default' => '', 'constraint' => '^\d+$' });
                $organism         ||= auto_prompt("Please enter species organism name: ", { 'default' => '', 'constraint' => '\w' });
                $common_name      ||= auto_prompt("Please enter species common name: ", { 'default' => '' });
                $url_institute    ||= auto_prompt("Please enter source institute URL: ", { 'default' => '' });
                $url_fasta        ||= auto_prompt("Please enter source FASTA URL: ", { 'default' => '' });
                $url_picture      ||= auto_prompt("Please enter species picture URL: ", { 'default' => '' });
                $version          ||= auto_prompt("Please enter species version: ", { 'default' => '' });
                $previous_species ||= auto_prompt("Please provide the code of the species that should appear before this species in the family composition chart: ", { 'default' => '' });

                # insert new species
                $species_id = insertSpecies($species_name, $species_taxonomy, $organism, $common_name, $url_institute, $url_fasta, $url_picture, $version, $previous_species);
                LogInfo("New species '$species_name' inserted into database as ID $species_id");
            }

            # load fasta file and get new sequences (new_list)
            if (!-e $input_filename)
            {
                confess "ERROR: Input FASTA file '$input_filename' not found!\n";
            }
            elsif (!-r $input_filename)
            {
                confess "ERROR: Unable to read input FASTA file '$input_filename'! Please check file permissions.\n";
            }

            # copy input FASTA file
            my $input_fasta_file_path = $path_manager->getOutputDirectory() . '/'. $Greenphyl::Config::INPUT_FASTA_FILENAME;
            copyFile($input_filename, $input_fasta_file_path, {'error_message' => "ERROR: Failed to copy input FASTA file ('$input_filename' to '$input_fasta_file_path')!\n$!\n"});

            # load FASTA
            ($new_list, $new_fasta_sequences) = loadFASTAinHashes($input_fasta_file_path);

            my $new_sequence_count = scalar(keys(%$new_list));
            print "Loaded $new_sequence_count FASTA sequences\n";
            LogInfo("Loaded $new_sequence_count FASTA sequences");

            # check if update mode
            if ($species_update)
            {
                # get the list of "old" sequences from database (old_list)
                $sql_query = "SELECT seq_textid AS \"$SEQUENCE_NAME\",
                                        sequence AS \"$SEQUENCE_CONTENT\",
                                        seq_length AS \"$SEQ_LENGTH\",
                                        annotation AS \"$ANNOTATION\"
                                    FROM sequences
                                    WHERE species_id = ?;";
                $old_list = $dbh->selectall_hashref($sql_query, $SEQUENCE_NAME, undef, $species_id);

                # here we use a tied hash which is case insensitive for keys but
                # which keeps keys case. Therefore, new names like 'AT4G31240.2'
                # will still match old names like 'At4g31240.2'
                tie my %temp_old_list, 'Greenphyl::HashInsensitive';
                # transfert regular hash data into case insensitive hash
                %temp_old_list = %$old_list;
                $old_list = \%temp_old_list;

                print "Got " . scalar(keys(%$old_list)) . " database sequences\n";
                LogInfo("Got " . scalar(keys(%$old_list)) . " database sequences");

                print "\nProcessing sequences update...\n";
                LogInfo("Processing sequences update...");
                # for each new sequence of new_list
                foreach my $new_sequence (keys(%$new_list))
                {
                    LogInfo('-' . $new_sequence . ': ');
                    # check if the sequence ID is in database
                    if (exists($old_list->{$new_sequence}))
                    {
                        # check if sequence content has been changed
                        if ($old_list->{$new_sequence}->{$SEQUENCE_CONTENT} ne $new_list->{$new_sequence}->{$SEQUENCE_CONTENT})
                        {
                            # remove sequence data in database
                            $dbh->do("DELETE FROM sequences WHERE seq_textid LIKE '$new_sequence';");
                            # insert sequence data in database
                            $dbis->insert('sequences', {
                                                            'seq_textid' => $new_sequence,
                                                            'sequence'   => $new_list->{$new_sequence}->{$SEQUENCE_CONTENT},
                                                            'seq_length' => $new_list->{$new_sequence}->{$SEQ_LENGTH},
                                                            'annotation' => $new_list->{$new_sequence}->{$ANNOTATION},
                                                            'species_id' => $species_id,
                                                       } );
                            # add the sequence to the inserted_list
                            $inserted_list->{$new_sequence} = $new_list->{$new_sequence};
                            # remove from the old list
                            delete($old_list->{$new_sequence});
                            ++$updated_sequence;
                            LogInfo("existing sequence content has changed. Sequence has been removed and re-inserted;");
                        }
                        else
                        {
                            # else if unchanged, remove sequence from the old_list and add it to the kept_list
                            $kept_list->{$new_sequence} =  delete($old_list->{$new_sequence});
                            LogInfo("existing sequence has been conserved;");
                        }
                        # end if
                    }
                    else
                    {
                        # else if not in database, put sequence in the "still_to_process" list
                        $still_to_process->{$new_sequence} =  $new_list->{$new_sequence};
                        LogInfo("new sequence to process;");
                    }
                    # end if
                }
                # end of for each
                print "...done!\n";
                LogInfo("...done!");
                # new_list = still_to_process
                $new_list = $still_to_process; # nb: copy hash_ref and does not clone the hash
            }
            # end if

            print "\nProcessing new sequences...\n";
            LogInfo("Processing new sequences...");
            my @new_sequence_name_list = keys(%$new_list);
            my @old_sequence_name_list = keys(%$old_list);
            my $processed_sequence_count = 0;
            my $computation_start_date = time();
            # for each sequence of new_list list
            foreach my $new_sequence (@new_sequence_name_list)
            {
                LogInfo('-' . $new_sequence . ': ');
                # try to find a (exact) matching sequence for new_sequence in old_list
                my ($sequence_index, $match_found, $old_sequence) = (0, 0, '');
                while (!$match_found && ($sequence_index < @old_sequence_name_list))
                {
                    $old_sequence = $old_sequence_name_list[$sequence_index];
                    if ($old_list->{$old_sequence}->{$SEQUENCE_CONTENT} eq $new_list->{$new_sequence}->{$SEQUENCE_CONTENT})
                    {
                        $match_found = 1;
                    }
                    else
                    {
                        ++$sequence_index;
                    }
                }

                # check if the sequence content matches a sequence content of the old_list
                if ($match_found)
                {
                    # remove sequence from the old_list and add it to the reassigned_list
                    $reassigned_list->{$new_sequence} = delete($old_list->{$old_sequence});
                    # remove the sequence name from the list of old sequence to check
                    splice(@old_sequence_name_list, $sequence_index, 1);
                    # update the sequence identifier in database
                    $dbis->update('sequences', {
                                                    'seq_textid' => $new_sequence,
                                                    'annotation' => $new_list->{$new_sequence}->{$ANNOTATION},
                                               },
                                               {
                                                    'seq_textid' => $old_sequence,
                                                    'species_id' => $species_id,
                                               }
                                           );
                    LogInfo("old sequence '$old_sequence' matched new sequence '$new_sequence'. Annotation data has been transfered;");
                    # store sequence history
                    #+FIXME: code to test!
                    if (!$dbh->do("INSERT INTO sequences_history (seq_id, old_seq_textid, new_seq_textid, transaction_date) SELECT seq_id, ?, ?, NOW() FROM sequences WHERE seq_textid = ? AND species_id = ;", undef, $old_sequence, $new_sequence, $new_sequence, $species_id))
                    {
                        cluck "Failed to save sequence history for sequence $new_sequence!\n";
                        LogWarning("Failed to save sequence history for sequence $new_sequence!");
                    }
                }
                else
                {
                    # else if no match found
                    # insert new sequence in database
                    $dbis->insert('sequences', {
                                                    'seq_textid' => $new_sequence,
                                                    'sequence'   => $new_list->{$new_sequence}->{$SEQUENCE_CONTENT},
                                                    'seq_length' => $new_list->{$new_sequence}->{$SEQ_LENGTH},
                                                    'annotation' => $new_list->{$new_sequence}->{$ANNOTATION},
                                                    'species_id' => $species_id,
                                               } );
                    # add sequences to the inserted_list
                    $inserted_list->{$new_sequence} = $new_list->{$new_sequence};
                    LogInfo("new sequence '$new_sequence' has been inserted;");
                }
                # end if

                # print progress
                my $computation_duration = time() - $computation_start_date;
                my $progress_percent = 100. * $processed_sequence_count++ / scalar(@new_sequence_name_list);
                my $remaining_time = $progress_percent ? '(' . getDuration((100 - $progress_percent) * $computation_duration / $progress_percent) . ' remaining)' : '';
                printf("\rProgress: %.1f%% %s                    ", $progress_percent, $remaining_time);
            }
            # end of for each
            print "\r...done!                    \n";
            LogInfo("...done!");

            print "\nRemoving obsolete sequences...\n";
            LogInfo("Removing obsolete sequences...");
            # for each sequence of old_list list
            foreach my $old_sequence (keys(%$old_list))
            {
                LogInfo("-$old_sequence: ");
                # remove sequence from the database
                $dbis->delete('sequences', {
                                                'seq_textid' => $old_sequence,
                                                'species_id' => $species_id,
                                           } );
                LogInfo("removed;");
            }
            # end of for each
            print "...done!\n";
            LogInfo("...done!");

            # empty new_list
            $new_list = {};

            # here:
            # * old_list contains all the obsolete sequences
            # * kept_list contains the list of sequences that were kept
            # * inserted_list contains the list of sequences inserted or updated (sequence content)
            # * reassigned_list contains the list of old sequence which identifier changed
            # * new_list = still_to_process and should be empty

            # update the number of sequence in species table
            $dbh->do("UPDATE species SET sequence_number = (SELECT count(1) FROM sequences s WHERE s.species_id = $species_id) WHERE species_id = $species_id;");
            my ($db_sequence_count) = $dbh->selectrow_array("SELECT sequence_number FROM species WHERE species_id = ?;", undef, $species_id);

            # check if the count correspond to the count of sequence in FASTA file
            if ($db_sequence_count != $new_sequence_count)
            {
                print "WARNING: the number of sequences from the FASTA file ($new_sequence_count) is different from the number of sequences (for the species '$species_name') in database ($db_sequence_count)!\n";
                LogWarning("the number of sequences from the FASTA file ($new_sequence_count) is different from the number of sequences (for the species '$species_name') in database ($db_sequence_count)!\n");
                cluck "WARNING: the number of sequences from the FASTA file ($new_sequence_count) is different from the number of sequences (for the species '$species_name') in database ($db_sequence_count)!\n";
            }

            # Store lists of updated sequences
            try
            {
                LogInfo("Number of new sequences: " . (scalar(keys(%$inserted_list)) - $updated_sequence));
                # if species update
                if ($species_update)
                {
                    # log the lists...
                    LogInfo("Number of updated sequences: " . $updated_sequence);
                    LogInfo("Number of reassigned sequences: " . scalar(keys(%$reassigned_list)));
                    LogInfo("Number of unchanged sequences: " . scalar(keys(%$kept_list)));
                    LogInfo("Number of obsolete sequences: " . scalar(keys(%$old_list)));

                    # log reassigned_list
                    my $reassigned_seq_fh;
                    my $reassigned_seq_file_path = $path_manager->getOutputDirectory() . '/' . $Greenphyl::Config::REASSIGNED_SEQUENCE_FILE;
                    open($reassigned_seq_fh, ">$reassigned_seq_file_path") or confess("ERROR: Failed to create log file '$reassigned_seq_file_path'!\n$!\n");
                    foreach my $reassigned_sequence (keys(%$reassigned_list))
                    {
                        print {$reassigned_seq_fh} $reassigned_list->{$reassigned_sequence}->{$SEQUENCE_NAME} . "\t$reassigned_sequence\n";
                    }
                    close($reassigned_seq_fh);

                    # log kept_list
                    my $kept_seq_fh;
                    my $kept_seq_file_path = $path_manager->getOutputDirectory() . '/' . $Greenphyl::Config::KEPT_SEQUENCE_FILE;
                    open($kept_seq_fh, ">$kept_seq_file_path") or confess("ERROR: Failed to create log file '$kept_seq_file_path'!\n$!\n");
                    foreach my $kept_sequence (keys(%$kept_list))
                    {
                        print {$kept_seq_fh} "$kept_sequence\n";
                    }
                    close($kept_seq_fh);

                    # log old_list
                    my $old_seq_fh;
                    my $old_seq_file_path = $path_manager->getOutputDirectory() . '/' . $Greenphyl::Config::OBSOLETE_SEQUENCE_FILE;
                    open($old_seq_fh, ">$old_seq_file_path") or confess("ERROR: Failed to create log file '$old_seq_file_path'!\n$!\n");
                    foreach my $old_sequence (keys(%$old_list))
                    {
                        print {$old_seq_fh} "$old_sequence\n";
                    }
                    close($old_seq_fh);

                }
                # end if
            }
            otherwise
            {
                print "ERROR: Failed to save updated sequences lists!\n";
                if (auto_prompt("Commit database changes anyway? [y/n] ", { 'default' => 'n', 'constraint' => '[yYnN]' }) =~ m/[nN]/)
                {
                    confess "Database commit aborted by user!\n";
                }
            };

            # FASTA generation
            try
            {
                print "\nGenerating new FASTA...\n";
                LogInfo("Generating new FASTA...");
                # create a new fasta with the inserted_list and also log inserted_list
                my $inserted_seq_fasta_io = Bio::SeqIO->new(
                    '-file'   => '>' . $path_manager->getOutputDirectory() . '/' . $Greenphyl::Config::FASTA_TO_PROCESS,
                    '-format' => 'Fasta'
                );
                my $new_seq_fh;
                my $new_seq_file_path = $path_manager->getOutputDirectory() . '/' . $Greenphyl::Config::NEW_SEQUENCE_FILE;
                open($new_seq_fh, ">$new_seq_file_path") or confess("ERROR: Failed to create log file '$new_seq_file_path'!\n$!\n");
                foreach my $new_sequence (keys(%$inserted_list))
                {
                    print {$new_seq_fh} "$new_sequence\n";
                    if (!exists($new_fasta_sequences->{$new_sequence}))
                    {
                        warn "WARNING: unable to add sequence '$new_sequence' in new FASTA file!\n";
                    }
                    $inserted_seq_fasta_io->write_seq($new_fasta_sequences->{$new_sequence});
                }
                close($new_seq_fh);
                print "...done!\n";
                LogInfo("...done!");
            }
            otherwise
            {
                print "ERROR: Failed to create FASTA file!\n";
                if (auto_prompt("Commit database changes anyway? [y/n] ", { 'default' => 'n', 'constraint' => '[yYnN]' }) =~ m/[nN]/)
                {
                    confess "Database commit aborted by user!\n";
                }
            };
        }
        elsif ($code =~ m/^(?:$RUN_INT_CODE_FAMILY_CLUSTERING|$RUN_CODE_FAMILY_CLUSTERING)$/si) # protein family clustering
        {
            $need_commit = 1;

            if (!$source_blast && (auto_prompt("Do you already have a BLAST result file? [y/n] ", { 'default' => 'n', 'constraint' => '[yYnN]' }) =~ m/[yY]/))
            {
                $source_blast = prompt("Please enter the name (path) to your BLAST result file: ", { 'constraint' => '\w' });
            }
            elsif (!defined($update) && (auto_prompt("Is it a species update? [y/n] ", { 'default' => 'n', 'constraint' => '[yYnN]' }) =~ m/[yY]/))
            {
                $update = 1;
            }

            print "\nProcessing Family Update...\n";
            print "Code fam\nSpecies: '$species_name'\n";
            LogInfo("Code fam\nSpecies: '$species_name'");

            my $query_sequences = {}; # keys are sequence names, values are Bio::Seq objects
            my $query_sequences_count = 0;
            my @orphans_list = (); # list of Bio::Seq objects
            my @new_families_list = (); # list of name of new families
            my @modified_families_list = ();

            # check if no BLAST file was specified
            my $blast_output_filename = "blast_out$$";
            my $blast_bank_file_path = $path_manager->getBlastDirectory() . '/ALL-' . $species_name;
            my $fasta_filename_to_blast = $input_filename;
            $fasta_filename_to_blast ||= $path_manager->getOutputDirectory() . '/' . $Greenphyl::Config::FASTA_TO_PROCESS;
            if (!$source_blast)
            {
                # backup old blasts if present
                if (-e $path_manager->getBlastDirectory())
                {
                    opendir(BLAST_DIR, $path_manager->getBlastDirectory()) or confess "ERROR: $!";
                    readdir BLAST_DIR; # reads .
                    readdir BLAST_DIR; # reads ..
                    # check if not empty
                    if (readdir(BLAST_DIR))
                    {
                        print "Backup old BLAST directory...";
                        LogInfo("Backup old BLAST directory...");

                        # remove previous backup
                        if ( (-e $path_manager->getOldBlastDirectory())
                            && (system('rm -rf ' . $path_manager->getOldBlastDirectory())) )
                        {
                            confess "ERROR: Failed to remove old BLAST bank directory!\n";
                        }
                        # backup old BLAST
                        if (system('mv ' . $path_manager->getBlastDirectory() . ' ' . $path_manager->getOldBlastDirectory()))
                        {
                            confess "ERROR: Failed to backup BLAST bank directory!\n";
                        }
                        # recreated empty BLAST directory
                        mkdir($path_manager->getBlastDirectory(), 0776) or confess "ERROR: Failed to create output BLAST directory '" . $path_manager->getBlastDirectory() . "'!\n$!\n";
                        print "done!\n";
                        LogInfo("done!");
                    }
                    closedir(BLAST_DIR);
                }

                # if species update
                if ($update)
                {
                    print "Update mode\n";
                    LogInfo("Update mode");
                    # get species list
                    my @species_list = ();
                    my @species_data = @{$dbh->selectall_arrayref('SELECT species_name FROM species ORDER BY species_name ASC', { 'Slice' => {} }) };

                    foreach my $species_db_name (@species_data) # (Greenphyl::Web::Greenphyl::get_all_species_names())
                    {
                        if ($species_db_name->{'species_name'} ne $species_name)
                        {
                            push @species_list, $species_db_name->{'species_name'};
                        }
                    }
                    LogDebug("got species: " . join(', ', @species_list));
                    LogInfo("Species in database: " . join(', ', @species_list));
                    LogInfo("Preparing ALL species - $species_name BLAST bank...");
                    # concate BLAST banks without species sequences into BLAST update directory (/dbupdate/<species>/blast)
                    Greenphyl::Run::Blast::concatBlastBank('output_filename' => $blast_bank_file_path,
                                                            'species_list'   => \@species_list,);
                    LogInfo("done!");
                }
                else
                {
                    print "Insert mode\n";
                    LogInfo("Insert mode");
                    # copy ALL BLAST bank into BLAST update directory
                    my $copy_error_stat = 0;
                    if (!-e $Greenphyl::Config::ALL_BLAST_BANK)
                    {
                        confess "ERROR: Failed to access to BLAST bank '$Greenphyl::Config::ALL_BLAST_BANK'!\n";
                    }
                    if (system('cp -f ' . $Greenphyl::Config::ALL_BLAST_BANK . $Greenphyl::Run::Blast::BLAST_PIN_EXT . ' ' . $blast_bank_file_path . $Greenphyl::Run::Blast::BLAST_PIN_EXT))
                    {
                        ++$copy_error_stat;
                        warn "ERROR: Failed to copy ALL BLAST bank '$Greenphyl::Config::ALL_BLAST_BANK$Greenphyl::Run::Blast::BLAST_PIN_EXT' to BLAST directory as '" . $blast_bank_file_path . $Greenphyl::Run::Blast::BLAST_PIN_EXT . "'!\n";
                    }
                    if (system('cp -f ' . $Greenphyl::Config::ALL_BLAST_BANK . $Greenphyl::Run::Blast::BLAST_PHR_EXT . ' ' . $blast_bank_file_path . $Greenphyl::Run::Blast::BLAST_PHR_EXT))
                    {
                        ++$copy_error_stat;
                        warn "ERROR: Failed to copy ALL BLAST bank '$Greenphyl::Config::ALL_BLAST_BANK$Greenphyl::Run::Blast::BLAST_PHR_EXT' to BLAST directory as '" . $blast_bank_file_path . $Greenphyl::Run::Blast::BLAST_PHR_EXT . "'!\n";
                    }
                    if (system('cp -f ' . $Greenphyl::Config::ALL_BLAST_BANK . $Greenphyl::Run::Blast::BLAST_PSQ_EXT . ' ' . $blast_bank_file_path . $Greenphyl::Run::Blast::BLAST_PSQ_EXT))
                    {
                        ++$copy_error_stat;
                        warn "ERROR: Failed to copy ALL BLAST bank '$Greenphyl::Config::ALL_BLAST_BANK$Greenphyl::Run::Blast::BLAST_PSQ_EXT' to BLAST directory as '" . $blast_bank_file_path . $Greenphyl::Run::Blast::BLAST_PSQ_EXT . "'!\n";
                    }
                    if ($copy_error_stat)
                    {
                        if (system('cp -f ' . $Greenphyl::Config::ALL_BLAST_BANK . ' ' . $blast_bank_file_path))
                        {
                            confess "ERROR: Failed to copy ALL BLAST bank '$Greenphyl::Config::ALL_BLAST_BANK' to BLAST directory as '" . $blast_bank_file_path . "'!\n";
                        }
                        Greenphyl::Run::Blast::formatdb($blast_bank_file_path);
                    }
                }
                # end if

                # BLAST inserted sequences .fa versus created BLAST bank
                print "\nBLAST sequences...";
                LogInfo("BLAST sequences (output: $blast_output_filename)...");
                # check if cluster is available
                if ($Greenphyl::Config::CLUSTER_QUEUE)
                {
                    Greenphyl::Run::Blast::runBlast('-i' => $fasta_filename_to_blast, '-e' => $EVALUE, '-d' => $blast_bank_file_path, 'max_best_hit' => $MAX_BEST_HIT, '-o' => $path_manager->getBlastDirectory() . '/' . $blast_output_filename, ' ' => $Greenphyl::Config::BLASTALL_TABULAR_FORMAT_PARAM, 'cluster_queue' => $Greenphyl::Config::CLUSTER_QUEUE);
                }
                else
                {
                    Greenphyl::Run::Blast::runBlast('-i' => $fasta_filename_to_blast, '-e' => $EVALUE, '-d' => $blast_bank_file_path, 'max_best_hit' => $MAX_BEST_HIT, '-o' => $path_manager->getBlastDirectory() . '/' . $blast_output_filename, ' ' => $Greenphyl::Config::BLASTALL_TABULAR_FORMAT_PARAM);
                }
                # check if a valid file was generated
                if ((!-e $path_manager->getBlastDirectory() . '/' . $blast_output_filename) || (-z $path_manager->getBlastDirectory() . '/' . $blast_output_filename))
                {
                    confess "ERROR: BLAST execution failed! ($blast_output_filename)\n";
                }
                print "done! (see $blast_output_filename)\n";
                LogInfo("done!");
            }
            else
            {
                $blast_output_filename = $source_blast;
                print "\nUsing user-provided BLAST $source_blast\n";
                LogInfo("User-provided BLAST file: '$source_blast'");
            }

            # load query FASTA file
            my $query_sequences_tmp;
            my $fasta_to_process = getArgumentFilePath($fasta_filename_to_blast, $path_manager->getOutputDirectory());
            $fasta_to_process ||= $path_manager->getOutputDirectory() . '/' . $Greenphyl::Config::FASTA_TO_PROCESS;
            ($query_sequences_tmp, $query_sequences) = loadFASTAinHashes($fasta_to_process);
            $query_sequences_count = scalar(keys(%$query_sequences));
            print "Loaded input FASTA ('$fasta_to_process'): $query_sequences_count sequences without families (or to reassign to a family)\n";
            LogInfo("Input FASTA: $fasta_to_process");
            LogInfo("Loaded sequences: $query_sequences_count sequences without families (or to reassign to a family)");

            print "\nProcessing BLAST output...\n";
            LogInfo("Processing BLAST output...");
            # Parse BLAST output
            my $blast_output_file_path = getArgumentFilePath($blast_output_filename, $path_manager->getBlastDirectory());
            print "BLAST file: $blast_output_file_path\n";
            LogInfo("BLAST file: $blast_output_file_path");
            my $blast_result_io = new Bio::SearchIO(
                '-format' => 'blast',
                '-file'   => $blast_output_file_path,
            );

            # foreach blasted sequence
            my $processed_hit_count = 0;
            my $computation_start_date = time();
BLAST_HIT_LOOP:
            while (my $result = $blast_result_io->next_result())
            {
                LogInfo('-' . $result->query_name);
                # check if sequences is also in FASTA (and DB)
                if (!exists $query_sequences->{$result->query_name})
                {
                    print "WARNING: BLAST sequence '" . $result->query_name . "' not found inf FASTA file!\n";
                    LogWarning("BLAST sequence '" . $result->query_name . "' not found inf FASTA file!\n");
                    next BLAST_HIT_LOOP;
                }
                # get first hit
                my $hit = $result->next_hit();
                # $query_seq: Bio::Seq
                my $query_seq = loadSeqData($query_sequences->{$result->query_name});
                # remove previous family associations
                $dbis->delete('seq_is_in', { 'seq_id' => $query_seq->{'id'} } );
                # if hit
                if ($hit)
                {
                    # $hit_seq: Bio::Search::Hit::HitI
                    my $hit_seq = loadSeqData($hit);
                    LogInfo(' Got hit: ' . $hit_seq->{'textid'});

                    $hit_seq->{'hsp_iterator'} = $query_seq->{'hsp_iterator'} = $hit;

                    my $candidate_family_id = $hit_seq->{'family_id'};
                    my $family_median_length = get_median_seq_length_for_family($candidate_family_id);
                    LogDebug("family_median_length = $family_median_length");
                    
                    # if no family, median = 0, set to sequence length
                    $family_median_length = $hit->length;
                    LogDebug("family_median_length = query length ( $family_median_length)");

                    # test first hit (calp + cip + median) (qcov.pm)
                    my $qcov_error = compare_sequence_qcov($query_seq, $hit_seq, $family_median_length);

                    # if test validated and hit sequence has a family
                    if (!$qcov_error)
                    {
                        LogDebug("QCov test passed");
                        LogInfo(" QCov test OK:");
                        if ($candidate_family_id)
                        {
                            LogInfo(" add to existing family");
                            # insert blasted sequence into the same family/class as the hit sequence (seq_is_in)
                            $sql_query = "INSERT INTO seq_is_in (SELECT " . $query_seq->{'id'} . ", f.family_id FROM seq_is_in f WHERE f.seq_id = '" . $hit_seq->{'id'} . "');";
                            LogDebug("QUERY (insert in same family as hit): $sql_query");
                            if (0 == $dbh->do($sql_query))
                            {
                                cluck "WARNING: unable to insert a match! SQL query:\n $sql_query\n";
                            }
                            # keep track of modified families
                            # push(@modified_families_list, @{$dbh->selectall_arrayref("SELECT f.family_id AS family_id, f.family_name AS family_name, f.validated AS validated FROM family f WHERE f.family_id IN (SELECT si.family_id FROM seq_is_in si WHERE si.seq_id = " . $query_seq->{'id'} . ");", { 'Slice' => {} })});
                            push(@modified_families_list, @{$dbh->selectall_arrayref("SELECT f.family_id AS family_id, COALESCE(f.family_name, '') AS family_name, f.validated AS validated FROM family f JOIN seq_is_in si ON si.family_id = f.family_id AND si.seq_id = " . $query_seq->{'id'} . ";", { 'Slice' => {} })});
                        }
                        else
                        {
                            LogInfo(" create new family");
                            # test validated and hit sequence is an orphan (no family)
                            # create new a family, class 1:
                            # -insert into family with name "Unannotated cluster"
                            my $new_family_name = $UNANNOTATED_CLUSTER_NAME;
                            $dbis->insert('family', {
                                                        'family_name' => $new_family_name,
                                                    } );
                            # -fetch id of inserted family (nb. we're in transaction so no concurrent ID chould have been inserted)
                            my $new_family_id = $dbh->last_insert_id(undef, undef, 'family', 'family_id');
                            # -insert class=1 into found_in for the family
                            $dbis->insert('found_in', {
                                                        'family_id' => $new_family_id,
                                                        'class_id'  => 1,
                                                    } );
                            # -insert sequence-family link into seq_is_in
                            $dbis->insert('seq_is_in', {
                                                        'seq_id'    => $query_seq->{'id'},
                                                        'family_id' => $new_family_id,
                                                    } );
                            $dbis->insert('seq_is_in', {
                                                        'seq_id'    => $hit_seq->{'id'},
                                                        'family_id' => $new_family_id,
                                                    } );
                            # keep track of inserted families
                            push(@new_families_list, {'family_id' => $new_family_id, 'family_name' => $new_family_name, 'validated' => 0});
                            LogDebug("NEW FAMILY: $new_family_id");
                        }
                    }
                    else
                    {
                        # test failed
                        LogDebug("QCov test failed: $qcov_error");
                        LogInfo(" QCov test failed: add sequence to orphans list (bad hit)\n  $qcov_error");
                        # add sequence to orphan list with 'bad hit' annotation appended
                        $query_sequences->{$query_seq->{'textid'}}->desc($query_sequences->{$query_seq->{'textid'}}->desc() . ' ' . $BAD_HIT_ANNOTATION);
                        push(@orphans_list, $query_sequences->{$query_seq->{'textid'}});
                    }
                    # end if
                }
                else
                {
                    # no hit
                    LogInfo(" No hit: add sequence to orphans list (no hit)");
                    # add sequence to orphan list with 'no hit' annotation appended
                    $query_sequences->{$query_seq->{'textid'}}->desc($query_sequences->{$query_seq->{'textid'}}->desc() . ' ' . $NO_HIT_ANNOTATION);
                    push(@orphans_list, $query_sequences->{$query_seq->{'textid'}});
                }
                # end if
                # print progress
                my $computation_duration = time() - $computation_start_date;
                my $progress_percent = (100. * $processed_hit_count++ / $query_sequences_count) || 0.01;
                my $remaining_time = $progress_percent ? '(' . getDuration((100 - $progress_percent) * $computation_duration / $progress_percent) . ' remaining)' : '';
                printf("\rProgress: %.1f%% %s                    ", $progress_percent, $remaining_time);
            }
            # end for each
            print "...done!\n";
            LogInfo("...done!");

            # Here:
            # %$query_sequences contains the list (keys) of sequences that were new or modified (loaded from FASTA file)
            # $query_sequences_count is the total number of sequences that were new or modified
            # @orphans_list contains the list (of hash) of orphans sequences (sequences with no family)
            # @new_families_list contains the list of new family created
            # @modified_families_list contains the list of families that received at least one new sequence from the new/updated species
            LogInfo("New families: " . scalar(@new_families_list));
            LogInfo("Modified families: " . scalar(@modified_families_list));
            LogInfo("Orphans sequences: " . scalar(@orphans_list));

            # create orphan FASTA
            try
            {
                print "\nGenerating orphan FASTA...";
                LogInfo("Generating orphan FASTA...");
                my $orphans_fasta_file_path = $path_manager->getOutputDirectory() . '/' . $Greenphyl::Config::ORPHANS_FASTA_FILENAME;
                my $orphans_fasta_io = Bio::SeqIO->new(
                    '-file'   => '>' . $orphans_fasta_file_path,
                    '-format' => 'Fasta',
                );
                foreach my $orphan_sequence (@orphans_list)
                {
                    $orphans_fasta_io->write_seq($orphan_sequence);
                }
                print "done! (see '$orphans_fasta_file_path')\n";
                LogInfo("done! (see '$orphans_fasta_file_path')");
            }
            otherwise
            {
                print "ERROR: Failed to create orphans FASTA file!\n";
                if (auto_prompt("Commit database changes anyway? [y/n] ", { 'default' => 'n', 'constraint' => '[yYnN]' }) =~ m/[nN]/)
                {
                    confess "Database commit aborted by user!\n";
                }
            };


            # remove obsolete families and log
            removeObsoleteFamilies($path_manager);

            # log the list of modified families
            print "Log modified families...";
            LogInfo("Log modified families...");
            my ($modified_family_fh);
            my $modified_family_file_path = $path_manager->getOutputDirectory() . '/' . $Greenphyl::Config::MODIFIED_FAMILY_LIST_FILE;
            open($modified_family_fh, ">$modified_family_file_path") or confess("ERROR: Failed to create log file '$modified_family_file_path'!\n$!\n");
            print {$modified_family_fh} "family_id\tfamily_name\tvalidated\n";
            # fetch obsolete family ID, name and validated level (families with 0 or 1 member)
            foreach my $modified_family (@modified_families_list)
            {
                #+debug
                #LogDebug("modified_family=" . $modified_family . ", " . $modified_family->{'family_id'} . "\t" . $modified_family->{'family_name'} . "\t" . $modified_family->{'validated'});
                print {$modified_family_fh} $modified_family->{'family_id'} . "\t" . $modified_family->{'family_name'} . "\t" . $modified_family->{'validated'} . "\n";
            }
            close($modified_family_fh);
            print "done! (see '$modified_family_file_path')\n";
            LogInfo("done! (see '$modified_family_file_path')");

            # log the list of new families
            print "Log new families...";
            LogInfo("Log new families...");
            my ($new_family_fh);
            my $new_family_file_path = $path_manager->getOutputDirectory() . '/' . $Greenphyl::Config::NEW_FAMILY_LIST_FILE;
            open($new_family_fh, ">$new_family_file_path") or confess("ERROR: Failed to create log file '$new_family_file_path'!\n$!\n");
            print {$new_family_fh} "family_id\tfamily_name\tvalidated\n";
            # fetch obsolete family ID, name and validated level (families with 0 or 1 member)
            foreach my $new_family (@new_families_list)
            {
                print {$new_family_fh} $new_family->{'family_id'} . "\t" . $new_family->{'family_name'} . "\t" . $new_family->{'validated'} . "\n";
            }
            close($new_family_fh);
            print "done! (see '$new_family_file_path')\n";
            LogInfo("done! (see '$new_family_file_path')");

            # prompt to create a FASTA (<family_id>.fa) with all the sequences of the family for each modified or new family
            if (auto_prompt("Generate a new FASTA file for each created/modified family? [y/n] ", { 'default' => 'n', 'constraint' => '[yYnN]' }) =~ m/[yY]/)
            {
                try
                {
                    print "\nGenerating FASTA for each created/modified family...\n";
                    LogInfo("Generating FASTA for each created/modified family...");
                    # note: for each sequence, concatenate seq_text_id + '_' + species_name
                    foreach my $family (@new_families_list, @modified_families_list)
                    {
                        my $family_fasta_file_path = $path_manager->getFamilyFastaDirectory() . '/' . $family->{'family_id'} . $FASTA_FILE_EXT;
                        LogInfo('-' . $family->{'family_id'} . "$FASTA_FILE_EXT");
                        LogDebug("creating FASTA file for family ID $family->{'family_id'}");
                        createFamilyFastaFile($family_fasta_file_path, $family->{'family_id'}, undef);
                    }
                    print "...done!\n";
                    LogInfo("...done!");
                }
                otherwise
                {
                    print "ERROR: Failed to create a family FASTA file!\n";
                    if (auto_prompt("Commit database changes anyway? [y/n] ", { 'default' => 'n', 'constraint' => '[yYnN]' }) =~ m/[nN]/)
                    {
                        confess "Database commit aborted by user!\n";
                    }
                };
            }

            if (auto_prompt("Update sequence count per family? [y/n] ", { 'default' => 'n', 'constraint' => '[yYnN]' }) =~ m/[yY]/)
            {
                print "\nUpdate sequence count per family...";
                LogInfo("Update sequence count per family...");
                $dbh->do('CALL countFamilySeqBySpecies();');
                # $dbh->do('CALL updateFamiliesStats();');
                
                print "done!\n";
                LogInfo("done!");
                print "Clear cache...";
                LogInfo("Clear cache...");
                $dbh->do('TRUNCATE go_family_cache;');
                print "done!\n";
                LogInfo("done!");
            }
            else
            {
                print "\nNo update on sequence count per family\n";
                LogInfo("No update on sequence count per family");
            }

            my $species_fasta_file_path = $path_manager->getOutputDirectory() . '/'. $Greenphyl::Config::INPUT_FASTA_FILENAME;
            # prompt to update species BLAST bank (if update)
            if ($update && (auto_prompt("Update $species_name BLAST bank file? [y/n] ", { 'default' => 'n', 'constraint' => '[yYnN]' }) =~ m/[yY]/))
            {
                print "\nUpdate $species_name BLAST bank file...";
                LogInfo("Update $species_name BLAST bank file...");
                my $species_blast_bank_path = $Greenphyl::Config::BLAST_BANKS_PATH . '/' . $species_name;

                # remove old bank file
                unlink($species_blast_bank_path);

                # copy new FASTA file
                if (system('cp -f ' . $species_fasta_file_path . ' ' . $species_blast_bank_path))
                {
                    confess "ERROR: Failed to copy species FASTA file ('$species_fasta_file_path' to '$species_blast_bank_path')!\n";
                }

                # run formatdb
                Greenphyl::Run::Blast::formatdb($species_blast_bank_path);

                print "done!\n";
                LogInfo("done!");
            }
            else
            {
                print "\nNo update on $species_name BLAST bank file\n";
                LogInfo("No update on $species_name BLAST bank file");
            }

            # prompt to update ALL BLAST bank
            if (auto_prompt("Update ALL species BLAST bank file? [y/n] ", { 'default' => 'n', 'constraint' => '[yYnN]' }) =~ m/[yY]/)
            {
                print "\nUpdate ALL species BLAST bank file...";
                LogInfo("Update ALL species BLAST bank file...");

                # remove old bank file
                unlink($Greenphyl::Config::ALL_BLAST_BANK);

                LogInfo("Concate $species_name and $blast_bank_file_path");
                # concate ALL BLAST banks into BLAST bank directory
                Greenphyl::Run::Blast::concatBlastBank('output_filename'  => $Greenphyl::Config::ALL_BLAST_BANK,
                                                       'species_list'     => [$species_name],
                                                       'input_blast_bank' => $$blast_bank_file_path,);

                print "done!\n";
                LogInfo("done!");
            }
            else
            {
                print "\nNo update on ALL species BLAST bank file\n";
                LogInfo("No update on ALL species BLAST bank file");
            }

            # print "END\n";
        }
		elsif ($code =~ m/^(?:$RUN_INT_CODE_ORPHANS_CLUSTERING|$RUN_CODE_ORPHANS_CLUSTERING)$/si) # protein orphans clustering
        {
			$need_commit = 1;
			
            print "Code fam\nSpecies: '$species_name'\n";
            my $fasta_filename_to_blast = $path_manager->getOutputDirectory() . '/' . $Greenphyl::Config::ORPHANS_FASTA_FILENAME;
            
            if ( -e $fasta_filename_to_blast)
            {
                print $fasta_filename_to_blast ." exists\n";
                
                #checked that count is the same in the db and in the file
                my $count_seq_file = qx/grep '>' $fasta_filename_to_blast | wc -l/;
                print "count sequences in fasta file : $count_seq_file\n";
                my ($species_name_tmp, $species_id) = fetchSpecies($species_name);
                my ($count_seq_db)  = $dbh->selectrow_array("SELECT COUNT(1) FROM sequences s WHERE s.species_id = $species_id AND NOT EXISTS (SELECT TRUE FROM seq_is_in sii WHERE s.seq_id = sii.seq_id LIMIT 1);");
                print "count sequences in db : $count_seq_db\n";
                
                if ( $count_seq_file != $count_seq_db)
                {
                    print "Warning: Counts are not identical. A new orphan file is being generated...";
                    #otherwise generate a new file and overwrite
                    my $orphans_fasta_file_path = $path_manager->getOutputDirectory() . '/' . $Greenphyl::Config::ORPHANS_FASTA_FILENAME;
                    print("Output file: '$orphans_fasta_file_path'");                
                    my $sql_query = "SELECT s.seq_textid AS \"seq_textid\", s.sequence AS \"sequence\" FROM sequences s WHERE s.species_id = $species_id AND NOT EXISTS (SELECT TRUE FROM seq_is_in sii WHERE s.seq_id = sii.seq_id LIMIT 1);";
                    createFastaFileFromQuery($orphans_fasta_file_path, $sql_query);
                    print "Done. (see '$orphans_fasta_file_path')\n";
                }
           
            }
            
            if (!$source_blast && (auto_prompt("Do you already have a BLAST result file? [y/n] ", { 'default' => 'y', 'constraint' => '[yYnN]' }) =~ m/[yY]/))
            {
                my $orphans_blast_fasta_file_path = $path_manager->getOutputDirectory() . '/blast/blast_orphans.out';
                $source_blast = prompt("Please enter the name (path) to your BLAST result file: ", { 'default' => $orphans_blast_fasta_file_path, 'constraint' => '\w' });
            }
            else
            {
                 auto_prompt("You need to generate a blast file. Hit enter to exit...", { 'default' => '' });
            }
            
            my $blast_output_filename;            
            if (-e $source_blast)
            {
                $blast_output_filename = $source_blast;
                print "\nUsing user-provided BLAST $source_blast\n";                 
            }
            else
            {
                confess "Blast result file ($source_blast) not found\n";
            }
            
            # load query FASTA file
            my $query_sequences = {};
            my $query_sequences_tmp;
            my $query_sequences_count = 0;
            my @orphans_list = (); # list of Bio::Seq objects
            my @new_families_list = (); # list of name of new families
            my @modified_families_list = ();
            
            my $fasta_to_process = getArgumentFilePath($fasta_filename_to_blast, $path_manager->getOutputDirectory());
            ($query_sequences_tmp, $query_sequences) = loadFASTAinHashes($fasta_to_process);
            $query_sequences_count = scalar(keys(%$query_sequences));
            print "Loaded input FASTA ('$fasta_to_process'): $query_sequences_count sequences without families (or to reassign to a family)\n";
            
            print "\nProcessing orphans clustering...\n";
            
            print "\nProcessing BLAST output...\n";

            # Parse BLAST output
            my $blast_output_file_path = getArgumentFilePath($blast_output_filename, $path_manager->getBlastDirectory());
            print "BLAST file: $blast_output_file_path\n";
           
            my $blast_result_io = new Bio::SearchIO(
                '-format' => 'blast',
                '-file'   => $blast_output_file_path,
            );
                    
            # foreach blasted sequence
            my $processed_hit_count = 0;
            my $computation_start_date = time();
BLAST_HIT_LOOP:
            while (my $result = $blast_result_io->next_result())
            {
               
                # check if sequences is also in FASTA (and DB)
                if (!exists $query_sequences->{$result->query_name})
                {
                    print "WARNING: BLAST sequence '" . $result->query_name . "' not found inf FASTA file!\n";
                    print("BLAST sequence '" . $result->query_name . "' not found inf FASTA file!\n");
                    next BLAST_HIT_LOOP;
                }
                
           
                # $query_seq: Bio::Seq
                my $query_seq = loadSeqData($query_sequences->{$result->query_name});
                         
                # get hits
                NEXT_HIT: 
                while (my $hit = $result->next_hit())
                {             
                    # if hit
                    if ($hit)
                    {
                        #need second hit because the first hit is supposed to be the same sequence itself (to be checked)
                        print $result->query_name . "\t" . $hit->name . "\n";
                        if ($hit->name eq $result->query_name)
                        {    
                             print 'same sequence ' .  $hit->name . "\t" . $result->query_name . "\n";                           
                             next NEXT_HIT;               
                        }

                        # $hit_seq: Bio::Search::Hit::HitI
                        my $hit_seq = loadSeqData($hit);
                        print ' Got hit: ' . $hit_seq->{'textid'} . "\n";
    
                        $hit_seq->{'hsp_iterator'} = $query_seq->{'hsp_iterator'} = $hit;
    
                        my $candidate_family_id = $hit_seq->{'family_id'};
                        
                        #if orphans already used to create a species specific family AND avoid creation of multiple families with same sequence                     
                        if (exists $query_sequences->{$hit->name})
                        {                          
                            print 'Match with orphan of same species : ' . $hit->name . "\n";                                                                    
                            my ($check_clustering_status) = $dbh->selectrow_array("SELECT sii.family_id FROM seq_is_in sii, sequences s WHERE s.seq_id = sii.seq_id AND s.seq_textid = '" . $hit->name . "' LIMIT 1");                         
                            next NEXT_HIT if ($check_clustering_status != $candidate_family_id);                          
                        }
                                     
                        my $family_median_length = get_median_seq_length_for_family($candidate_family_id);
                        print "family_median_length = $family_median_length" . "\n";
                        
                        # if no family, median = 0, set to sequence length
                        $family_median_length = $hit->length if (!$candidate_family_id);
                        
                        # test first hit (calp + cip + median) (qcov.pm)
                        my $qcov_error = compare_sequence_qcov($query_seq, $hit_seq, $family_median_length);
    
                        # if test validated and hit sequence has a family
                        if (!$qcov_error)
                        {
                            print "QCov test passed" . "\n";
                            if ($candidate_family_id)
                            {
                                print "add to existing family\n";
                                # insert blasted sequence into the same family/class as the hit sequence (seq_is_in)
                                $sql_query = "INSERT INTO seq_is_in (SELECT " . $query_seq->{'id'} . ", f.family_id FROM seq_is_in f WHERE f.seq_id = '" . $hit_seq->{'id'} . "');";
                                print("QUERY (insert in same family as hit): $sql_query");
                                if (0 == $dbh->do($sql_query))
                                {
                                    cluck "WARNING: unable to insert a match! SQL query:\n $sql_query\n";
                                }
                                # keep track of modified families
                                # push(@modified_families_list, @{$dbh->selectall_arrayref("SELECT f.family_id AS family_id, f.family_name AS family_name, f.validated AS validated FROM family f WHERE f.family_id IN (SELECT si.family_id FROM seq_is_in si WHERE si.seq_id = " . $query_seq->{'id'} . ");", { 'Slice' => {} })});
                                push(@modified_families_list, @{$dbh->selectall_arrayref("SELECT f.family_id AS family_id, COALESCE(f.family_name, '') AS family_name, f.validated AS validated FROM family f JOIN seq_is_in si ON si.family_id = f.family_id AND si.seq_id = " . $query_seq->{'id'} . ";", { 'Slice' => {} })});
                            }
                            else
                            {
                                print " create new family" . "\n";
                                # test validated and hit sequence is an orphan (no family)
                                # create new a family, class 1:
                                # -insert into family with name "Unannotated cluster"
                                my $new_family_name = $UNANNOTATED_CLUSTER_NAME;
                                $dbis->insert('family', {
                                                            'family_name' => $new_family_name,
                                                        } );                                                                          
                                                        
                                # -fetch id of inserted family (nb. we're in transaction so no concurrent ID chould have been inserted)
                                my $new_family_id = $dbh->last_insert_id(undef, undef, 'family', 'family_id');
                                # -insert class=1 into found_in for the family
                                $dbis->insert('found_in', {
                                                            'family_id' => $new_family_id,
                                                            'class_id'  => 1,
                                                        } );
                                # -insert sequence-family link into seq_is_in
                                $dbis->insert('seq_is_in', {
                                                            'seq_id'    => $query_seq->{'id'},
                                                            'family_id' => $new_family_id,
                                                        } );
                                $dbis->insert('seq_is_in', {
                                                            'seq_id'    => $hit_seq->{'id'},
                                                            'family_id' => $new_family_id,
                                                        } );
                                # keep track of inserted families
                                push(@new_families_list, {'family_id' => $new_family_id, 'family_name' => $new_family_name, 'validated' => 0});
                                print "NEW FAMILY: $new_family_id" . "\n";
                            }
                        }
                        else
                        {
                            # test failed
                            print "QCov test failed: $qcov_error" . "\n";                    
                            # add sequence to orphan list with 'bad hit' annotation appended
                            # $query_sequences->{$query_seq->{'textid'}}->desc($query_sequences->{$query_seq->{'textid'}}->desc() . ' ' . $BAD_HIT_ANNOTATION);
                            # push(@orphans_list, $query_sequences->{$query_seq->{'textid'}});
                        }
                        # end if
                    }
                    else
                    {
                        # no hit
                        print " No hit: add sequence to orphans list (no hit)";
                        # add sequence to orphan list with 'no hit' annotation appended
                        $query_sequences->{$query_seq->{'textid'}}->desc($query_sequences->{$query_seq->{'textid'}}->desc() . ' ' . $NO_HIT_ANNOTATION);
                        push(@orphans_list, $query_sequences->{$query_seq->{'textid'}});
                    }
                    # end if
                    last NEXT_HIT;
                }
                # print progress
                my $computation_duration = time() - $computation_start_date;
                my $progress_percent = (100. * $processed_hit_count++ / $query_sequences_count) || 0.01;
                my $remaining_time = $progress_percent ? '(' . getDuration((100 - $progress_percent) * $computation_duration / $progress_percent) . ' remaining)' : '';
                printf("\rProgress: %.1f%% %s                    ", $progress_percent, $remaining_time);
            }
            
            # update family accession
            print "\nupdate family accession...";
            $dbh->do("UPDATE family f SET f.accession = CONCAT('GP', RIGHT(CONCAT('000000', f.family_id), 6)) WHERE f.accession IS NULL OR f.accession ='';");
            print "done!\n";
            
            if (auto_prompt("Update sequence count per family? [y/n] ", { 'default' => 'y', 'constraint' => '[yYnN]' }) =~ m/[yY]/)
            {
                print "\nUpdate sequence count per family...";
                $dbh->do('CALL countFamilySeqBySpecies();');
                #$dbh->do('CALL updateFamiliesStats();');
                print "done!\n";
                            
                ##TODO: call update_taxonomy
                #qx/perl update_family_taxonomy.pl/
                                          
                print "Clear cache...";
                $dbh->do('TRUNCATE go_family_cache;');
                print "done!\n";
            }
            else
            {
                print "\nNo update on sequence count per family\n";
            }
            
            
            
            # end for each
            print "...done!\n";
		}
        elsif ($code =~ m/^(?:$RUN_INT_CODE_EXTERNAL_DATA_UPDATE|$RUN_CODE_EXTERNAL_DATA_UPDATE)$/si)    # load external data
        {
            my $choice = '';

            # check if species is in database and retrieve ID
            ($species_name, $species_id) = fetchSpecies($species_name);
            if (!$species_id)
            {
                confess "ERROR: External data importation requires a species ID!\n";
            }

            if ($interactive_mode)
            {
                print <<"___2167_CODE_3_MENU___";

    1. UNIPROT
    2. KEGG
    3. PIRSF
    4. INTERPRO
    c. Cancel

___2167_CODE_3_MENU___
                $choice = prompt("Enter your choice (number or 'c' to go back to main menu): ", { 'constraint' => '[cC1-4]' });

                if ($choice =~ m/1/i)
                {
                    # ask for the XML file
                    if (prompt("Fetch Uniprot XML file from the web? (y/n)", { 'constraint' => '[yYnN]' }) =~ m/y/i)
                    {
                        # my $tax_id = ; wget(http://www.uniprot.org/uniprot/?query=taxonomy%3a$tax_id+AND+reviewed%3ayes&force=yes&format=xml)
                        my ($tax_id) = $dbh->selectrow_array("SELECT tax_id FROM species WHERE species_id = $species_id;");
                        $uniprot_path = $path_manager->getUniprotDirectory() . '/' . $species_name . $UNIPROT_XML_EXT;
                        # check if file already exists
                        if ((!-e $uniprot_path) || (prompt("File '$uniprot_path' already exists! Overwrite? (y/n)", { 'constraint' => '[yYnN]' }) =~ m/y/i))
                        {
                            # fetch file from the web
                            if (system("wget -O '$uniprot_path' '" . GetURL('uniprot') . "?query=((existence%3A\"evidence+at+protein+level\"+or+existence%3A\"evidence+at+transcript+level\"+or+reviewed%3Ayes)taxonomy%3a$tax_id)&force=yes&format=xml'"))
                            {
                                confess "ERROR: Failed to fetch file using wget!\n$?\n";
                            }
                        }
                    }
                    else
                    {
                        $uniprot_path = prompt("Enter the input file path: ", { 'constraint' => '\w+' });
                    }
                }
                elsif ($choice =~ m/2/i)
                {
                    $kegg_path = prompt("Enter the input file path: ", { 'constraint' => '\w+' });
                }
                elsif ($choice =~ m/3/i)
                {
                    $pirsf_path = prompt("Enter the input file path: ", { 'constraint' => '\w+' });
                }
                elsif ($choice =~ m/4/i)
                {
                    $interpro_path = prompt("Enter the input file path: ", { 'constraint' => '\w+' });
                }
            }
            print "\nProcessing External Data Insertion...\n";
            print "Code ext\n";
            LogInfo("Code ext");

            if ($uniprot_path && (-e $uniprot_path))
            {
                $need_commit = 1;
                print "Input File: $uniprot_path\n";
                LogInfo("Input File: $uniprot_path");
                my $obj_uniprot = Greenphyl::Load::Uniprot->new('dbh' => $dbh);
                $obj_uniprot->loadUniprot($uniprot_path, $species_id, $species_name);
                print "done!\n";
                LogInfo("done!");
            }
            elsif ($kegg_path && (-e $kegg_path)) # pas undef donc on a un chemin de fichier et ce fichier existe
            {
                $need_commit = 1;
                # check if species is in database and retrieve ID
                ($species_name, $species_id) = fetchSpecies($species_name);
                if ($species_id)
                {
                    print "Update : KEGG whith   ---  $kegg_path   --- <br />\n";
                    my $obj_kegg = Greenphyl::Load::Kegg->new( 'dbh' => $dbh );
                    $obj_kegg->load_kegg( $kegg_path, $species_id );
                    print "END\n";
                }

                else
                {
                    print "Update KEGG requiere species_id <br />\n";
                }
            }
            elsif ($pirsf_path && (-e $pirsf_path))
            {
                $need_commit = 1;
                print "Update : PIRSF whith   ---  $pirsf_path   --- <br />\n";
                my $obj_pirsf = Greenphyl::Load::Pirsf->new( 'dbh' => $dbh );
                $obj_pirsf->load_pirsf($pirsf_path);
                print "END\n";
            }
            # utilisateur a coche interpro on va lancer les differente methode pour recuperer tout.
            elsif ($interpro && (-e $interpro))
            {
                $need_commit = 1;
                # check if species is in database and retrieve ID
                ($species_name, $species_id) = fetchSpecies($species_name);
                if ($species_name)
                {
                    print "Retreive interproscan results<br />\n";
                    &loadInterpro( $species_name, $interpro, $email );
                    print "end interpro\n";
                    print "END\n";
                }
            }
            elsif ($choice =~ m/c/i)
            {
                # nothing to do as user canceled
            }
            else
            {
                confess "ERROR: missing file or species not specified!\n";
            }
        }
        elsif ($code =~ m/^(?:$RUN_INT_CODE_INTERPROSCAN_UPDATE|$RUN_CODE_INTERPROSCAN_UPDATE)$/si) # interproscan update
        {
            $need_commit = 1;
            if (!$interpro_path)
            {
                if (auto_prompt("Your InterProScan results are stored in several XML files in a directory [d] or in a single XML file [f]? [d/f]", { 'default' => 'd', 'constraint' => '[dDfF]' }) =~ m/[fF]/)
                {
                    # get file
                    $interpro_path = auto_prompt("Enter the InterProScan XML result file: ", { 'constraint' => '\w' });
                }
                else
                {
                    # get directory
                    $interpro_path = auto_prompt("Enter the InterProScan XML result directory: ", { 'constraint' => '\w' });
                }
            }

            if (!-e $interpro_path)
            {
                # directory
                confess "ERROR: file or directory not found!";
            }

            print "\nProcessing Interproscan Update...\n";
            print "Code ipr\nSpecies: '$species_name'\n";
            LogInfo("Code ipr\nSpecies: '$species_name'");

            # make sure an Interproscan version was provided
            $ipr_version ||= auto_prompt("Please provide an InterProScan version: ", { 'default' => '0', 'constraint' => '^\d+$' });

            # check if species is in database and retrieve ID
            ($species_name, $species_id) = fetchSpecies($species_name);

            if (!$species_id)
            {
                confess "ERROR: species '$species_name' not found in database!\n";
            }
            print "Species ID: $species_id\n";
            LogInfo("Species ID: $species_id");

            my @xml_files_list;
            my $interpro_xml_path;
            if (-d $interpro_path)
            {
                # process XML directory and list files
                my $xml_dh = undef;
                $interpro_xml_path = $interpro_path;
                $interpro_xml_path ||= $path_manager->getInterproscanDirectory();
                $interpro_xml_path =~s/\/+$//; # remove trailing '/'
                print "Listing XML ($interpro_xml_path)...";
                LogInfo("Listing XML ($interpro_xml_path)...");
                opendir($xml_dh, $interpro_xml_path);
                while (my $filename = readdir($xml_dh))
                {
                    # only keep xml files
                    if ($filename =~ /\.xml$/)
                    {
                        push(@xml_files_list, $filename);
                    }
                }
                closedir($xml_dh);
                print "done!\n";
                LogInfo("done!");

                # get sequences count for the species in database
                my ($db_sequence_count) = $dbh->selectrow_array("SELECT sequence_number FROM species WHERE species_id = ?;", undef, $species_id);

                # check if file count correspond to sequence found in DB
                LogInfo("Interproscan XML files count: " . scalar(@xml_files_list) . "\nNumber of sequence for '$species_name' in database: $db_sequence_count");
                if ($db_sequence_count != @xml_files_list)
                {
                    # counts are different, prompt to continue
                    if (auto_prompt("WARNING: Interproscan XML files count (" . scalar(@xml_files_list) . ") is not equal to the number of sequence for '$species_name' ($db_sequence_count) in database!\nContinue anyway? [y/n] ", { 'default' => 'n', 'constraint' => '[yYnN]' }) =~ m/[nN]/)
                    {
                        confess "Interproscan parsing process aborted by user!\n";
                    }
                }
            } # end of if (-d $interpro_path)
            else
            {
                # single file
                my $xml_filename;
                ($xml_filename, $interpro_xml_path) = fileparse($interpro_path);
                $interpro_xml_path =~ s/\/+$//; # remove trailing '/'
                if (!$interpro_xml_path && ($interpro_xml_path !~ m/0/))
                {
                    $interpro_xml_path = '.';
                }
                push(@xml_files_list, $xml_filename);
            }

            # prepare Interpro update log
            my $inserted_ipr_count     = 0;
            my $updated_ipr_count      = 0;
            my $reused_ipr_count       = 0;
            my $matched_seq_count      = 0;
            my $unmatched_seq_count    = 0;
            my $unusable_ipr_xml_count = 0;
            my $ipr_update_log_fh;
            my $ipr_update_log_file_path = $path_manager->getOutputDirectory() . '/' . $Greenphyl::Config::INTERPROSCAN_UPDATE_FILE;
            open($ipr_update_log_fh, ">$ipr_update_log_file_path") or confess("ERROR: Failed to create log file '$ipr_update_log_file_path'!\n$!\n");
            print {$ipr_update_log_fh} "OP\tIPR_ID\tIPR_CODE\tTYPE\tVERSION\tIPR_LONGDESC\n";

            # remove IPR data of specified species (seq_has_an and family_has_an)
            print "Removing species IPR from database...";
            LogInfo("Removing species IPR from database...");
            $dbh->do("DELETE FROM seq_has_an USING seq_has_an, sequences WHERE sequences.species_id = $species_id AND sequences.seq_id = seq_has_an.seq_id;");
            $dbis->delete('family_has_an', {'species_id' => $species_id} );

            # reset species sequence scanned field
            $dbis->update('sequences', {'scanned' => 0}, {'species_id' => $species_id} );
            print "done!\n";
            LogInfo("done!");


            # for each XML file, parse XML data
            print "Processing XML...\n";
            LogInfo("Processing XML...");
            my $processed_xml_count = 0;
            my $computation_start_date = time();
XML_TO_PROCESS:
            foreach my $xml_filepath (@xml_files_list)
            {
#                LogInfo("-$xml_filepath");

                #LogInfo(" Processing XML file: $interpro_xml_path/$xml_filepath");
                print STDERR "Processing XML file: $interpro_xml_path/$xml_filepath";
                my $parsed_data;
                try 
                {
                    $parsed_data = XMLin(
                        $interpro_xml_path . '/' . $xml_filepath,
                        'forcearray'   => 1,
                        'keeproot'     => 1,
                        'forcecontent' => 1,
                        'keyattr'      => {},
                    );
                }
                otherwise
                {
                    cluck shift();
                };
                
 
                # check XML data
                if (!$parsed_data)
                {
#                    LogWarning("Failed to parse '$interpro_xml_path/$xml_filepath'!\n");
                    warn "\nWARNING: Invalid Interpro XML result file: $interpro_xml_path/$xml_filepath\n";
                    ++$unusable_ipr_xml_count;
                }
                
                if ($parsed_data->{'EBIInterProScanResults'})
                {
                    $parsed_data = $parsed_data->{'EBIInterProScanResults'}->[0];
                }

                if (!exists($parsed_data->{'interpro_matches'}))
                {
#                    LogWarning("Failed to parse '$interpro_xml_path/$xml_filepath': 'interpro_matches' tag not found!\n");
                    warn "\nWARNING: Failed to parse '$interpro_xml_path/$xml_filepath': 'interpro_matches' tag not found!\n";
                    ++$unusable_ipr_xml_count;
                }
                elsif (!exists($parsed_data->{'interpro_matches'}->[0]->{'protein'}))
                {
#                    LogWarning("Failed to parse '$interpro_xml_path/$xml_filepath': 'protein' tag not found!\n");
                    #warn "\nWARNING: Failed to parse '$interpro_xml_path/$xml_filepath': 'protein' tag not found!\n";
                    ++$unmatched_seq_count;
                }
                else
                {
                    foreach my $ipr_protein (@{ $parsed_data->{'interpro_matches'}->[0]->{'protein'} })
                    {
                        # check if we got a valid protein ID
                        if ((1 < @xml_files_list)
                            && (!$ipr_protein->{'id'} || ('Sequence_1' eq $ipr_protein->{'id'})))
                        {
                            # extract protein from file name
                            ($ipr_protein->{'id'}) = ($xml_filepath =~ m/^(.*)\.xml$/i);
                        }
                        my $stats = processIPRProtein($ipr_protein, $ipr_version, $ipr_update_log_fh);

                        $unusable_ipr_xml_count += $stats->{'unusable_ipr_xml_count'};
                        $matched_seq_count      += $stats->{'matched_seq_count'};
                        $unmatched_seq_count    += $stats->{'unmatched_seq_count'};
                        $inserted_ipr_count     += $stats->{'inserted_ipr_count'};
                        $updated_ipr_count      += $stats->{'updated_ipr_count'};
                        $reused_ipr_count       += $stats->{'reused_ipr_count'};
                    }
                }

                # print progress
                my $computation_duration = time() - $computation_start_date;
                my $progress_percent = (100. * $processed_xml_count++ / @xml_files_list) || 0.01;
                my $remaining_time = $progress_percent ? '(' . getDuration((100 - $progress_percent) * $computation_duration / $progress_percent) . ' remaining)' : '';
                printf("\rProgress: %.1f%% %s                    ", $progress_percent, $remaining_time);
            }
            # end for each

            # update family_has_an data
            # note: nothing will be inserted for sequences without family
            $sql_query = "INSERT IGNORE INTO family_has_an (family_id, ipr_id, species_id)
                            (SELECT sii.family_id AS family_id,
                                    sha.ipr_id    AS ipr_id,
                                    s.species_id  AS species_id
                                FROM sequences s
                                     JOIN seq_is_in sii USING (seq_id)
                                     JOIN seq_has_an sha USING (seq_id)
                                WHERE s.species_id = $species_id
                            );";
            LogDebug("QUERY (insert in family_has_an): $sql_query");
            $dbh->do($sql_query);


            close($ipr_update_log_fh);
            print "\nInserted IPR: $inserted_ipr_count\nUpdated IPR: $updated_ipr_count\nIPR already in DB: $reused_ipr_count\nSequences with at least 1 IPR domain: $matched_seq_count\nSequences with no IPR domain: $unmatched_seq_count\nUnusable XML: $unusable_ipr_xml_count\n";
            LogInfo("Inserted IPR: $inserted_ipr_count\nUpdated IPR: $updated_ipr_count\nIPR already in DB: $reused_ipr_count\nSequences with at least 1 IPR domain: $matched_seq_count\nSequences with no IPR domain: $unmatched_seq_count\nUnusable XML: $unusable_ipr_xml_count");

            print "done!\n";
            LogInfo("done!");

            try
            {
                # prompt for stats
                if (auto_prompt("Compute statistics? [y/n] ", { 'default' => 'n', 'constraint' => '[yYnN]' }) =~ m/[yY]/)
                {
                    # run IPR Stat
                    if (system('perl run_ipr_statistics.pl'))
                    {
                        warn "WARNING: Failed to compute statistics!\n$!\n";
                    }
                }
            }
            otherwise
            {
                warn "WARNING: Failed to compute statistics!\n";
            };
        }
        elsif ($code =~ m/^(?:$RUN_INT_CODE_REMOVE_SPECIES|$RUN_CODE_REMOVE_SPECIES)$/si) # remove specified species
        {
            $need_commit = 1;
            print "\nProcessing Species Deletion...\n";
            print "Code $RUN_CODE_REMOVE_SPECIES\nSpecies: '$species_name'\n";
            LogInfo("Code $RUN_CODE_REMOVE_SPECIES\nSpecies: '$species_name'");

            ($species_name, $species_id) = fetchSpecies($species_name);

            if (!$species_id)
            {
                confess "ERROR: species '$species_name' not found in database!\n";
            }

            # remove sequences from species
            print "Removing species '$species_name' from database...";
            LogInfo("Removing species '$species_name' from database...");
            $dbis->delete('sequences', {'species_id' => $species_id} );
            $dbis->delete('species', {'species_id' => $species_id} );
            print "done!\n";
            LogInfo("done!");

            # remove obsolete families and log
            removeObsoleteFamilies($path_manager);

            print "done!\n";
            LogInfo("done!");
        }
        elsif ($code =~ m/^(?:$RUN_INT_CODE_CLEAN_FAMILIES|$RUN_CODE_CLEAN_FAMILIES)$/si) # remove obsolete families
        {
            $need_commit = 1;
            print "\nProcessing Families Cleaning...\n";
            print "Code $RUN_CODE_CLEAN_FAMILIES\n";
            LogInfo("Code $RUN_CODE_CLEAN_FAMILIES");

            # remove obsolete families and log
            removeObsoleteFamilies($path_manager);

            print "done!\n";
            LogInfo("done!");
        }
        elsif ($code =~ m/^(?:$RUN_INT_CODE_GENERATE_FAM_FASTA|$RUN_CODE_GENERATE_FAM_FASTA)$/si) # generate family FASTA
        {
            print "\nGenerating family FASTA...\n";
            print "Code $RUN_CODE_GENERATE_FAM_FASTA\n";
            LogInfo("Code $RUN_CODE_GENERATE_FAM_FASTA");

            my $family_id_list = auto_prompt("Please enter a list of family ID to process (space-separated) or an empty string to process all families: ", { 'default' => '', 'constraint' => '^(?: *\d+ *)+$' }); # validate: "456 468 789 845"
            $family_id_list =~ s/^ +//;
            $family_id_list =~ s/ +$//;
            $family_id_list = [split(/ +/, $family_id_list)];

            my $class_id_list = auto_prompt("Please enter a list of family class to process (space-separated): ", { 'default' => '1', 'constraint' => '(?: *\d{1,1} *)+' });
            $class_id_list =~ s/^ +//;
            $class_id_list =~ s/ +$//;
            $class_id_list = [split(/ +/, $class_id_list)];

            my $validated_list = auto_prompt("Please enter a list of validated level to process (space-separated): ", { 'default' => '0 1 2 3', 'constraint' => '(?: *\d{1,1} *)+' });
            $validated_list =~ s/^ +//;
            $validated_list =~ s/ +$//;
            $validated_list = [split(/ +/, $validated_list)];

            my $species_list = auto_prompt("Please enter the list of species the families should belong to (space-separated) or an empty string for all: ", { 'default' => '', 'constraint' => "(?: *$SPECIES_NAME_REGEXP *)+" });
            $species_list =~ s/^ +//;
            $species_list =~ s/ +$//;
            $species_list = [split(/ +/, $species_list)];

            my $species_exclusive = 0;
            if ($species_list && @$species_list && (auto_prompt("Only keep sequences from selected species and exclude sequences from other species? ", { 'default' => 'y', 'constraint' => '^[yYnN]$' }) =~ m/y/i))
            {
                $species_exclusive = 1;
            }

            my $remove_splice_forms = (auto_prompt("Only keep one sample of each splice form?: ", { 'default' => 'n', 'constraint' => "[yYnN]" }) =~ m/y/i);

            my $family_min_size = auto_prompt("Please enter the minimum number (inclusive) of member sequences in family to process after applying previous filtration (0: no limit): ", { 'default' => '0', 'constraint' => '^\d+$' });
            my $family_max_size = auto_prompt("Please enter the MAXIMUM number (inclusive) of member sequences in family to process after applying previous filtration (0: no limit): ", { 'default' => '0', 'constraint' => '^\d+$' });

            my $no_phylo_only = auto_prompt("Only keep unprocessed families for phylogeny analyses (y/n)", { 'default' => 'n', 'constraint' => '^[yYnN]$' });

            my $output_directory = auto_prompt("Full path to output directory? ", { 'default' => $path_manager->getTempDirectory() });
            $output_directory =~ s/\/+$//;
            
            my $file_name = (auto_prompt("Would you like to include the gene family annotation in the FASTA file name? (y/n)", { 'default' => 'n', 'constraint' => '^[yYNn]$' }) =~ m/y/i) ;
            
            # select families
            my $sql_query = "
                SELECT f.family_id, f.family_name
                FROM family f
                    JOIN found_in fi USING (family_id)
                WHERE f.validated IN (" . join(', ', @$validated_list) . ") AND fi.class_id IN (" . join(', ', @$class_id_list) . ")";
            if ($no_phylo_only =~ m/y/i)
            {
                # only keep families without phylogeny analyses
                $sql_query .= ' AND NOT EXISTS (SELECT TRUE FROM homologies h WHERE h.family_id = f.family_id LIMIT 1)';
            }
            if (@$family_id_list)
            {
                # only keep specified families
                $sql_query .= ' AND f.family_id IN (' .  join(', ', @$family_id_list) . ')';
            }
            
            # takes 2 columns (as family name was added)
            my %family  = @{ $dbh->selectcol_arrayref($sql_query, {Columns=>[1,2]}) };           
            my @families = keys %family;
            
            # for each family, generate a FASTA
            try
            {
                print "\nGenerating FASTA for each selected family...\n";
                LogInfo("Generating FASTA for each selected family...");
                print "Output directory : " .$output_directory . "\n";
                LogInfo("Output directory : " . $output_directory);

                my $computation_start_date  = time();
                # first query provide families that can be filter later in the loop
                my $processed_family_count  = 0;
                # family count of the remaining GF selected for export in fasta 
                my $selected_family_count   = 0; 
                # note: for each sequence, concatenate seq_text_id + '_' + species_name
                foreach my $family_id (@families)
                {
                    # check count limitations
                    if (@$species_list && $species_exclusive)
                    {
                        $sql_query = "
                            SELECT count(DISTINCT sii.seq_id)
                            FROM seq_is_in sii
                                JOIN sequences s ON s.seq_id = sii.seq_id
                                JOIN species sp ON s.species_id = sp.species_id AND sp.species_name IN ('" . join("', '", @$species_list) . "')
                            WHERE sii.family_id = $family_id;";
                    }
                    else
                    {
                        $sql_query = "
                            SELECT count(DISTINCT sii.seq_id)
                            FROM seq_is_in sii
                            WHERE sii.family_id = $family_id;";
                    }
                    my ($members_count) = $dbh->selectrow_array($sql_query);
                    if ((!$family_min_size || ($family_min_size <= $members_count))
                        && (!$family_max_size || ($family_max_size >= $members_count)))
                    {
                        my $family_fasta_file_path;
                        if (!$file_name)
                        {
                            $family_fasta_file_path = $output_directory . '/' . $family_id . $FASTA_FILE_EXT;
                        }
                        else
                        {
                            $family{$family_id} =~ s/\W+/_/g;
                            $family_fasta_file_path = $output_directory . '/' . $family_id . '-' . $family{$family_id} . $FASTA_FILE_EXT;        
                        }
                        
                        LogInfo('-' . $family_id . "$FASTA_FILE_EXT");
                        LogDebug("creating FASTA file for family ID $family_id");
                        # FASTA constraints
                        my $fasta_parameters = {'remove_splice_forms' => $remove_splice_forms};
                        if ($family_min_size)
                        {
                            $fasta_parameters->{'min_seq'} = $family_min_size;
                        }
                        if ($family_max_size)
                        {
                            $fasta_parameters->{'max_seq'} = $family_max_size;
                        }
                        # check for species
                        if ($species_exclusive)
                        {
                            $selected_family_count += createFamilyFastaFile($family_fasta_file_path, $family_id, $species_list, $fasta_parameters);
                        }
                        else
                        {
                            $selected_family_count += createFamilyFastaFile($family_fasta_file_path, $family_id, undef, $fasta_parameters);
                        }
                    }
                    else
                    {
                        LogDebug("$family_id skipped because of size restrictions; members count: $members_count");
                    }
                    # print progress
                    my $computation_duration = time() - $computation_start_date;
                    my $progress_percent = 100. * ++$processed_family_count / scalar(@families);
                    my $remaining_time = $progress_percent ? '(' . getDuration((100 - $progress_percent) * $computation_duration / $progress_percent) . ' remaining)' : '';
                    printf("\rProgress: %.1f%% %s                    ", $progress_percent, $remaining_time);
                }
                print "...done!\n";
                LogInfo("...done!");
                print $selected_family_count . " FASTA were generated in '" . $output_directory . "'!\n";
                LogInfo("Processed Families: " . $selected_family_count .  "\nOutput Directory: " . $output_directory);
            }
            otherwise
            {
                confess "ERROR: Failed to create a family FASTA file!\n" . shift;
            };
            print "done!\n";
            LogInfo("done!");
        }
        elsif ($code =~ m/^(?:$RUN_INT_CODE_MEME_MAST|$RUN_CODE_MEME_MAST)$/si) # remove obsolete families
        {
            print "\nProcessing MEME/MAST analyses...\n";
            print "Code $RUN_CODE_MEME_MAST\n";
            LogInfo("Code $RUN_CODE_MEME_MAST");

            # get MEME/MAST FASTA directory
            my $family_fasta_directory = $path_manager->getMemeMastDirectory();
            my $meme_log_filename = $path_manager->getOutputDirectory() . '/' . getLogFilename($Greenphyl::Config::MEME_MAST_LOG_FILE);
            my $processor_use ||= auto_prompt("How many cluster node can be used for computation? ", { 'default' => '32', 'constraint' => '^\d+$'});
            if (!$processor_use)
            {
                $processor_use = 1;
            }
            my $limitations ||= auto_prompt("Enter MEME/MAST limitation parameters (ex. 'less=500 time=3600'): ", { 'default' => '' });
            if ($limitations)
            {
                $limitations = '-limitations ' . $limitations;
            }

            my $command = "nohup perl $RUN_MEME_SCRIPT -dir $family_fasta_directory -p $processor_use > $meme_log_filename 2>&1 < /dev/null &";
            my $exit_code = system($command);
            if ($exit_code)
            {
                print "ERROR: $RUN_MEME_SCRIPT execution failed (exit code $exit_code):\n$!\n";
                LogInfo("ERROR: $RUN_MEME_SCRIPT execution failed:\n$!");
            }

            print "Job launched ($meme_log_filename)!\n";
            LogInfo("Launched!\nLog File: $meme_log_filename");
        }
        elsif ($code =~ m/^(?:$RUN_INT_CODE_GENERATE_SP_FASTA|$RUN_CODE_GENERATE_SP_FASTA)$/si) # generate species FASTA
        {
            print "\nGenerating Species FASTA...\n";
            print "Code $RUN_CODE_GENERATE_SP_FASTA\n";
            LogInfo("Code $RUN_CODE_GENERATE_SP_FASTA");

            # select species
            my $sql_query = "SELECT s.species_name FROM species s;";

            my @species = @{$dbh->selectcol_arrayref($sql_query)};
            # for each family, generate a FASTA
            try
            {
                print "\nGenerating FASTA for each species...\n";
                LogInfo("Generating FASTA for species...");

                my $computation_start_date = time();
                my $processed_species_count = 0;
                # note: for each sequence, concatenate seq_text_id + '_' + species_name
                foreach my $species_name (@species)
                {
                    my $species_fasta_file_path = $path_manager->getSpeciesFastaDirectory() . '/' . $species_name . $FASTA_FILE_EXT;
                    LogInfo('-' . $species_name . "$FASTA_FILE_EXT");
                    LogDebug("creating FASTA file for species " . $species_name);
                    createSpeciesFastaFile($species_fasta_file_path, $species_name);
                    # print progress
                    my $computation_duration = time() - $computation_start_date;
                    my $progress_percent = 100. * ++$processed_species_count / scalar(@species);
                    my $remaining_time = $progress_percent ? '(' . getDuration((100 - $progress_percent) * $computation_duration / $progress_percent) . ' remaining)' : '';
                    printf("\rProgress: %.1f%% %s                    ", $progress_percent, $remaining_time);
                }
                print "...done!\n";
                LogInfo("...done!");
                print scalar(@species) . " FASTA were generated in '" . $path_manager->getSpeciesFastaDirectory() . "/'!\n";
                LogInfo("Processed Species: " . scalar(@species) . "\nOutput Directory: " . $path_manager->getSpeciesFastaDirectory() . "/");
            }
            otherwise
            {
                confess "ERROR: Failed to create a species FASTA file!\n" . shift;
            };
            print "done!\n";
            LogInfo("done!");
        }
        elsif ($code =~ m/^(?:$RUN_INT_CODE_GENERATE_ORPH_FASTA|$RUN_CODE_GENERATE_ORPH_FASTA)$/si) # generate orphan sequences FASTA
        {
            print "\nGenerating orphan FASTA for species $species_name...";
            LogInfo("Generating orphan FASTA...");
            LogInfo("Code $RUN_CODE_GENERATE_ORPH_FASTA\nSpecies: '$species_name'");
            # check species
            ($species_name, $species_id) = fetchSpecies($species_name);

            if ($species_id)
            {
                # write sequences to file
                my $orphans_fasta_file_path = $path_manager->getOutputDirectory() . '/' . $Greenphyl::Config::ORPHANS_FASTA_FILENAME;
                LogInfo("Output file: '$orphans_fasta_file_path'");
                my $sql_query = "SELECT s.seq_textid AS \"seq_textid\", s.sequence AS \"sequence\" FROM sequences s WHERE s.species_id = $species_id AND NOT EXISTS (SELECT TRUE FROM seq_is_in sii WHERE s.seq_id = sii.seq_id LIMIT 1);";
                createFastaFileFromQuery($orphans_fasta_file_path, $sql_query);
                print "Done. (see '$orphans_fasta_file_path')\n";
                LogInfo("Done.");
            }
            else
            {
                confess "ERROR: no valid species name provided!\n";
            }
        }
        elsif ($code =~ m/^(?:$RUN_INT_CODE_LOAD_BBMH|$RUN_CODE_LOAD_BBMH)$/si) # Load BBMH
        {
            $need_commit = 1;
            print "\nLoading BBMH...";
            LogInfo("Loading BBMH...");
            LogInfo("Code $RUN_CODE_LOAD_BBMH");

            #+TODO: do what blast_all_vs_all.pl does

            # get all species codes
            $sql_query = "SELECT species_name FROM species ORDER BY species_id ASC;";
            my @db_species_names = @{$dbh->selectcol_arrayref($sql_query)};

            my @involved_blast_files = (); # list of BLAST results that should be re-computed
            # check if a species was specified
            if ($species_name)
            {
                # list all species vs db species combinations
                foreach my $db_species_name (@db_species_names)
                {
                    if ($db_species_name ne $species_name)
                    {
                        push(@involved_blast_files, $species_name . $SPECIES_BLAST_NAME_SEPARATOR . $db_species_name, $db_species_name . $SPECIES_BLAST_NAME_SEPARATOR . $species_name);
                    }
                }
            }
            else
            {
                # no species, confirm it should be run on all the species
                if (auto_prompt("No species was specified. Run BBMH on all species? [y/n] ", { 'default' => 'y', 'constraint' => '[yYnN]' }) =~ m/[yY]/)
                {
                    # all versus all
                    # remove all files from BBMH directory
                    my @db_species_names_temp = @db_species_names;
                    while (my $db_species_name1 = shift @db_species_names_temp)
                    {
                        foreach my $db_species_name2 (@db_species_names_temp)
                        {
                            if ($db_species_name1 ne $db_species_name2)
                            {
                                push(@involved_blast_files, $db_species_name1 . $SPECIES_BLAST_NAME_SEPARATOR . $db_species_name2, $db_species_name2 . $SPECIES_BLAST_NAME_SEPARATOR . $db_species_name1);
                            }
                        }
                    }
                }
                else
                {
                    confess "BBMH computation aborted by user.\n";
                }
            }

            # check for existing BLAST results
            print "BBMH directory: $Greenphyl::Config::BBMH_PATH/\n";
            LogInfo("BBMH directory: $Greenphyl::Config::BBMH_PATH/");
            my %existing_blast_files = ();
            my %missing_blast_files = ();
            foreach my $blast_file_to_check (@involved_blast_files)
            {
                if (-e "$Greenphyl::Config::BBMH_PATH/$blast_file_to_check")
                {
                    $existing_blast_files{$blast_file_to_check} = 1;
                }
                else
                {
                    $missing_blast_files{$blast_file_to_check} = 1;
                }
            }

            print "Existing BLAST results found:\n" . join(', ', keys(%existing_blast_files)) . "\n\n";
            if (auto_prompt("Keep existing BLAST result files? (if no, files will be renamed to 'SPEC1_vs_SPEC2.old') [y/n] ", { 'default' => 'y', 'constraint' => '[yYnN]' }) =~ m/[nN]/)
            {
                # remove existing files in the BBMH directory
                foreach my $blast_file_to_replace (keys(%existing_blast_files))
                {
                    # print "BLAST rename: $blast_file_to_replace\n";
                    rename("$Greenphyl::Config::BBMH_PATH/$blast_file_to_replace", "$Greenphyl::Config::BBMH_PATH/$blast_file_to_replace.old");
                    delete($existing_blast_files{$blast_file_to_replace});
                    $missing_blast_files{$blast_file_to_replace} = 1;
                }
            }

            #+TODO: check BLAST bank directory and run formatdb on unformatted banks

            #+TODO: launch the BLAST to generate missing BBMH files
            foreach my $blast_result_to_generate (@involved_blast_files)
            {
                if (!exists($existing_blast_files{$blast_result_to_generate}))
                {
                    # BBMH file is missing
                    #+TODO: launch the BLAST
                    #delete($missing_blast_files{$blast_result_to_generate});
                }
            }
            if (keys(%missing_blast_files))
            {
                warn "ERROR: some BLAST result files are missing:\n" . join(', ', sort(keys(%missing_blast_files))) . "\n";
                warn " WARNING: These data will not be missing in the db" . "\n";;
                #+debug confess "ERROR: some BLAST result files are missing:\n" . join(', ', sort(keys(%missing_blast_files))) . "\n";
            }

            # check if we work on a single species are all species
            if ($species_name)
            {
                # single species
                ($species_name, $species_id) = fetchSpecies($species_name);
                if (!$species_id)
                {
                    confess "ERROR: species ID not found!\n";
                }
                # remove previous BBMH
                my $deleted_rows = $dbh->do("DELETE FROM bbmh USING bbmh, sequences s WHERE s.species_id = $species_id AND (s.seq_id = bbmh.query OR s.seq_id = bbmh.hit);");
                LogDebug("Removing old BBMH entries: $deleted_rows entrie(s) removed.");
                LogInfo("Old BBMH entries removed: $deleted_rows");
                foreach my $db_species_name (@db_species_names)
                {
                    if ($db_species_name ne $species_name)
                    {
                        # store BBMH
                        findAndStoreSpeciesBBMH($species_name, $db_species_name)
                    }
                } # end of foreach my $db_species_name (@db_species_names)
            } # end of if ($species_name)
            else
            {
                # all species, loop on each species
                # remove previous BBMH: truncate table
                #+TODO: replace truncate by delete (trucate does not support transactions)
                $dbh->do('TRUNCATE bbmh;');
                my @db_species_names_temp = @db_species_names;
                while (my $db_species_name1 = shift @db_species_names_temp)
                {
                    foreach my $db_species_name2 (@db_species_names_temp)
                    {
                        if ($db_species_name1 ne $db_species_name2)
                        {
                            # store BBMH
                            findAndStoreSpeciesBBMH($db_species_name1, $db_species_name2);
                        }
                    }
                }
            } # end of else of if ($species_name)

#            # get BBMH input file
#            $bbmh_filepath ||= auto_prompt("Enter the input BBMH file path: ", { 'default' => 'input.bbmh', 'constraint' => '\w+' });
#
#            # prepare SQL queries
#            my $sql_query = "SELECT seq_id FROM sequences WHERE seq_textid LIKE ?;";
#            my $sth_get_seq_id_from_seq_textid = $dbh->prepare($sql_query);
#
#            $sql_query = "INSERT INTO bbmh (query, hit, score, eval) VALUES(?, ?, ?, ?);";
#            my $sth_insert_bbmh = $dbh->prepare($sql_query);
#
#            my $bbmh_fh;
#            open($bbmh_fh, $bbmh_filepath) or confess "ERROR: unable to open BBMH File '$bbmh_filepath'!\n";
#
#            print "Processing BBMH file '$bbmh_filepath'\n";
#            LogInfo("BBMH file: '$bbmh_filepath'");
#BBMH_LINE:
#            while (my $bbmh_line = <$bbmh_fh>)
#            {
#                # skip commented lines
#                if ((!$bbmh_line) || ($bbmh_line =~ m/^#/))
#                {
#                    next BBMH_LINE;
#                }
#
#                my ($query_seq, @seq_ids) = (split(/\t/, $bbmh_line));
#                if ($query_seq)
#                {
#                    LogInfo("-$query_seq");
#                    print "-$query_seq\n";
#                    if (!@seq_ids)
#                    {
#                        LogInfo(" WARNING: no hit");
#                        cluck "WARNING: no hit for '$query_seq'\n";
#                        next BBMH_LINE;
#                    }
#
#                    # fetch query sequence seq_id in database
#                    $sth_get_seq_id_from_seq_textid->execute($query_seq);
#                    my ($query_seq_id) = $sth_get_seq_id_from_seq_textid->fetchrow_array();
#                    if (!$query_seq_id)
#                    {
#                        LogInfo(" WARNING: query sequence '$query_seq' not found in database!");
#                        cluck "WARNING: query sequence '$query_seq' not found in database!\n";
#                        next BBMH_LINE;
#                    }
#                    LogInfo(" Query seq_id: $query_seq_id");
#
#BBMH_HIT:
#                    foreach my $hit (@seq_ids)
#                    {
#                        # skip no match (empty hit)
#                        if (!$hit || ($hit =~ m/^\s*$/))
#                        {
#                            next BBMH_HIT;
#                        }
#                        
#                        my ($match_seq, $score, $evalue) = ($hit =~ m/(\S+)\((\S+)\s(\S+)\)/);
#                        if (defined($match_seq) && defined($score) && defined($evalue))
#                        {
#                            # fetch matched sequence seq_id in database
#                            $sth_get_seq_id_from_seq_textid->execute($match_seq);
#                            my ($match_seq_id) = $sth_get_seq_id_from_seq_textid->fetchrow_array();
#                            if (!$match_seq_id)
#                            {
#                                LogInfo(" WARNING: matched sequence '$match_seq' not found in database!");
#                                cluck "WARNING: matched sequence '$match_seq' not found in database!\n";
#                                next BBMH_LINE;
#                            }
#
#                            # do not store self-match
#                            if ($match_seq_id != $query_seq_id)
#                            {
#                                LogInfo(" Matched: $match_seq");
#                                LogInfo(" Matched seq_id: $match_seq_id");
#                                print " $match_seq ";
#                                # store BBMH
#                                if (0 == $sth_insert_bbmh->execute($query_seq_id, $match_seq_id, $score, $evalue))
#                                {
#                                    LogInfo("ERROR: failed to insert BBMH between '$query_seq' and '$match_seq' in database!");
#                                    cluck "ERROR: failed to insert BBMH between '$query_seq' and '$match_seq' in database!\n";
#                                }
#                            }
#                        }
#                        else
#                        {
#                            LogInfo(" WARNING: failed to parse hit '$hit'\n Line: $bbmh_line");
#                            cluck "WARNING: failed to parse hit '$hit' in line:\n$bbmh_line\n";
#                        }
#                    }
#                    print "\n";
#                }
#            }
#            $sth_get_seq_id_from_seq_textid->finish();
#            $sth_insert_bbmh->finish();

            LogInfo("Done.");
        }
        elsif ($code =~ m/^(?:$RUN_INT_CODE_UPDATE_GO|$RUN_CODE_UPDATE_GO)$/si) # Load GO
        {
            $need_commit = 1;
            print "\nUpdate GO...\n";
            LogInfo("Update GO...");
            LogInfo("Code $RUN_CODE_UPDATE_GO");

            # load GO data to retrieve namespaces...
            if (!$obo_filepath)
            {
                $obo_filepath = $Greenphyl::Config::GO_OBO_FILE_PATH;
            }

            if (!-e $obo_filepath)
            {
                if (auto_prompt("GO OBO file is missing ('$obo_filepath'). Fetch it from the web? (y/n)", { 'default' => 'y', 'constraint' => '[yYnN]' }) =~ m/y/i)
                {
                    if (system("wget -O '$obo_filepath' '" . GetURL('gene_ontology_obo') . "'"))
                    {
                        confess "ERROR: Failed to fetch file using wget!\n$?\n";
                    }
                }
            }
            else
            {
                if (auto_prompt("Update current GO OBO file ('$obo_filepath') using the web? (y/n)", { 'default' => 'y', 'constraint' => '[yYnN]' }) =~ m/y/i)
                {
                    unlink($obo_filepath);
                    if (system("wget -O '$obo_filepath' '" . GetURL('gene_ontology_obo') . "'"))
                    {
                        confess "ERROR: Failed to fetch file using wget!\n$?\n";
                    }
                }
            }
            
            LogInfo("GO OBO file: '$obo_filepath'");
            # open GO file
            my $go_fh;
            if (!open($go_fh, $obo_filepath))
            {
                confess "ERROR: Failed to open GO OBO file '$obo_filepath' (specified in GreenPhyl config file)!\n$!\n";
            }
            my $obo_file_size = -s $obo_filepath;
            my $progression = 0;
            my $obo_data = {}; # keys: GO ID, values: {namespace => ..., name => ...}
            my $go_id = '';

            # parse file
            print " Loading Gene Ontology data...";
            LogInfo("Loading Gene Ontology data");
            while (my $line = <$go_fh>)
            {
                $progression += length($line);
                printf("\rProgression: %.0i%% " . ($go_id? "($go_id)":'') . '          ', 100.*$progression/$obo_file_size);
                # check if we got a term block
                if ($line =~ m/^\s*\[Term\]/i)
                {
                    # reset GO ID
                    $go_id = '';
                }
                # check if got a new GO ID
                if ($line =~ m/^\s*id:\s*(GO:\d+)/i)
                {
                    $go_id = uc($1);
                    $obo_data->{$go_id} = {};
                }
                # get GO type
                if ($go_id)
                {
                    if ($line =~ m/^\s*namespace:\s*(.*?)\s*$/i)
                    {
                        $obo_data->{$go_id}->{'namespace'} = $1;
                    }
                    elsif ($line =~ m/^\s*name:\s*(.*?)\s*$/i)
                    {
                        $obo_data->{$go_id}->{'name'} = $1;
                    }
                }
            }
            close($go_fh);
            LogInfo("Gene Ontology data loaded.");
            print "\n done!\n";

            # make sure we loaded something
            my $loaded_go_data = scalar(keys(%$obo_data));
            if ($loaded_go_data < 10)
            {
                confess "ERROR: it seems the GO OBO file ('$obo_filepath') was not parsed correctly or did not contain valid data!\n";
            }
            LogInfo("GO entries loaded: $loaded_go_data");
            
            my ($updated_go_count, $inserted_go_count, $ipr_to_go_count, $ipr_to_go_failed_count, $uniprot_to_go_count, $uniprot_to_go_failed_count, $ec_to_go_count, $ec_to_go_failed_count) = (0, 0, 0, 0, 0, 0, 0, 0);

            if (auto_prompt("Update IPR to GO? (y/n)", { 'default' => 'y', 'constraint' => '[yYnN]' }) =~ m/y/i)
            {
                # process IPR to GO update...
                if (!$ipr2go_filepath)
                {
                    $ipr2go_filepath = $path_manager->getOutputDirectory() . '/' . $Greenphyl::Config::IPR_TO_GO_FILENAME;
                    # ask for the IPR to GO mapping file
                    if (auto_prompt("Fetch Interpro to GO mapping file from the web? (y/n)", { 'default' => 'y', 'constraint' => '[yYnN]' }) =~ m/y/i)
                    {
                        # check if file already exists
                        if ((!-e $ipr2go_filepath) || (auto_prompt("File '$ipr2go_filepath' already exists! Overwrite? (y/n)", { 'default' => 'y', 'constraint' => '[yYnN]' }) =~ m/y/i))
                        {
                            # fetch file from the web
                            if (system("wget -O '$ipr2go_filepath' '" . GetURL('ipr_to_gene_ontology') . "'"))
                            {
                                confess "ERROR: Failed to fetch file using wget!\n$?\n";
                            }
                        }
                    }
                    else
                    {
                        $ipr2go_filepath = auto_prompt("Enter the Interpro-to-GO input file path: ", { 'default' => $ipr2go_filepath, 'constraint' => '\w+' });
                    }
                }
                LogInfo("IPR2GO mapping file: '$ipr2go_filepath'");
    
                # open IPR to GO file
                my $ipr_to_go_fh;
                if (!open($ipr_to_go_fh, $ipr2go_filepath))
                {
                    confess "ERROR: Failed to open file '$ipr2go_filepath '!\n$!\n";
                }
                my $ipr_file_size = -s $ipr2go_filepath;
                $progression = 0;
    
                # get release date
                # format: "!version date: 2011/03/05 11:49:19"
                my ($ipr_to_go_version) = (<$ipr_to_go_fh> =~ m/!version date: (\S+ \S+)/i);
                LogInfo("IPR2GO version: $ipr_to_go_version");
                if (!$dbh->do("INSERT INTO variables (name, value) VALUES ('ipr2go_version', ?) ON DUPLICATE KEY UPDATE value = ?;", undef, $ipr_to_go_version, $ipr_to_go_version))
                {
                    cluck "Failed to store IPR2GO version ($ipr_to_go_version)!";
                    LogWarning("Failed to store IPR2GO version ($ipr_to_go_version)!");
                }
    
                # parse each line
                LogInfo("Parsing IPR to GO data");
IPR_TO_GO_LOOP:
                while (my $line = <$ipr_to_go_fh>)
                {
                    $progression += length($line);
                    printf("\rProgression: %.0i%% ", 100.*$progression/$ipr_file_size);
                    # skip comments
                    if ($line =~ m/^!/)
                    {
                        next IPR_TO_GO_LOOP;
                    }
                    # get IPR code
                    if ($line =~ m/^InterPro:(IPR\d{6}) (?:[^;]*?> GO:)?(.*?)\s*; (GO:\d{7})$/i)
                    {
                        my $ipr_code       = $1;
                        my $go_code        = $3;
                        # my $go_description = $2;
                        my $go_description = $obo_data->{$go_code}->{'name'} || $2;
                        print "(IPR: $ipr_code --> $go_code)          ";
                        # check if IPR is in database
                        if (my ($greenphyl_ipr_id) = $dbh->selectrow_array("SELECT ipr_id FROM ipr WHERE ipr_code LIKE ?;", undef, $ipr_code))
                        {
                            # check if GO is in database
                            my $greenphyl_go_id;
                            if (($greenphyl_go_id) = $dbh->selectrow_array("SELECT go_id FROM go WHERE go_code LIKE ?;", undef, $go_code))
                            {
                                # already there, update GO
                                $dbh->do("UPDATE go SET go_desc = ? WHERE go_id = ?;", undef, $go_description, $greenphyl_go_id);
                                ++$updated_go_count;
                            }
                            else
                            {
                                # not there, insert...
                                # get GO type
                                my $go_type = $obo_data->{$go_code}->{'namespace'} || '';
                                if (!$go_type)
                                {
                                    LogWarning("GO term type not found for GO '$go_code'!");
                                }
                                $dbh->do("INSERT INTO go (go_code, go_type, go_desc) VALUES (?, ?, ?);", undef, $go_code, $go_type, $go_description);
                                ++$inserted_go_count;
                                # get go_id
                                ($greenphyl_go_id) = $dbh->selectrow_array("SELECT go_id FROM go WHERE go_code LIKE ?;", undef, $go_code);
                            }
                            # make sure we found a GO ID
                            if (!$greenphyl_go_id)
                            {
                                confess "ERROR: Failed to retrieve GO id for GO '$go_code'!\n";
                            }
                            # link IPR to GO if needed
                            if (!$dbh->do("INSERT IGNORE INTO ipr_to_go (ipr_id, go_id, evidence_code) VALUES (?, ?, 'IC');", undef, $greenphyl_ipr_id, $greenphyl_go_id))
                            {
                                ++$ipr_to_go_failed_count;
                                LogError("Failed to insert a GO to IPR relationship! IPR: '$ipr_code' (id=$greenphyl_ipr_id), GO: '$go_code' (id=$greenphyl_go_id)\n");
                                cluck "ERROR: Failed to insert a GO to IPR relationship! IPR: '$ipr_code' (id=$greenphyl_ipr_id), GO: '$go_code' (id=$greenphyl_go_id)\n";
                            }
                            else
                            {
                                ++$ipr_to_go_count;
                            }
                        }
                    }
                }
                LogInfo("Done parsing IPR to GO data");
                print "\n";
    
                # close file
                close($ipr_to_go_fh);

                LogInfo("IPR to GO relationship set: $ipr_to_go_count\nIPR to GO relationship NOT set: $ipr_to_go_failed_count");
                print "IPR to GO relationship set: $ipr_to_go_count\nIPR to GO relationship NOT set: $ipr_to_go_failed_count\n";
            }
            
            if (auto_prompt("Update Uniprot to GO? (y/n)", { 'default' => 'y', 'constraint' => '[yYnN]' }) =~ m/y/i)
            {
                # process Uniprot to GO update...
                if (!$uniprot2go_filepath)
                {
                    $uniprot2go_filepath = $Greenphyl::Config::UNIPROT_TO_GO_FILE_PATH;
                    # ask for the Uniprot to GO mapping file
                    if (auto_prompt("Fetch UniProt to GO mapping file from the web? (y/n)", { 'default' => 'y', 'constraint' => '[yYnN]' }) =~ m/y/i)
                    {
                        # check if file already exists
                        if (((!-e $uniprot2go_filepath) && (!-e $uniprot2go_filepath . '.gz'))
                            || (auto_prompt("File '$uniprot2go_filepath' already exists! Overwrite? (y/n)", { 'default' => 'y', 'constraint' => '[yYnN]' }) =~ m/y/i))
                        {
                            # fetch file from the web
                            if (system("wget -O '$uniprot2go_filepath' '" . GetURL('uniprot_to_go') . "'"))
                            {
                                confess "ERROR: Failed to fetch file using wget!\n$?\n";
                            }
                        }
                    }
                    else
                    {
                        $uniprot2go_filepath = auto_prompt("Enter the Uniprot-to-GO input file path: ", { 'default' => $uniprot2go_filepath, 'constraint' => '\w+' });
                    }
                }
                LogInfo("UniProt2GO mapping file: '$uniprot2go_filepath'");
    
                # check if it should be unzipped
                if ($uniprot2go_filepath =~ m/\.gz$/)
                {
                    if (system("gunzip $uniprot2go_filepath"))
                    {
                        confess "ERROR: Failed to unzip UniProt to GO file '$uniprot2go_filepath'!\n$?\n";
                    }
                    $uniprot2go_filepath =~ s/\.gz$//;
                }
    
                # parse file
                my $uniprot_to_go_fh;
                if (!open($uniprot_to_go_fh, $uniprot2go_filepath))
                {
                    confess "ERROR: Failed to open file '$uniprot2go_filepath '!\n$!\n";
                }
                my $uniprot_file_size = -s $uniprot2go_filepath;
                $progression = 0;
    
                # get release date
                # format: "!gaf-version: 2.0"
                my ($uniprot_to_go_version) = (<$uniprot_to_go_fh> =~ m/!gaf-version: (\S+)/i);
                LogInfo("UniProt2GO version: $uniprot_to_go_version");
                if (!$dbh->do("INSERT INTO variables (name, value) VALUES ('uniprot2go_version', ?) ON DUPLICATE KEY UPDATE value = ?;", undef, $uniprot_to_go_version, $uniprot_to_go_version))
                {
                    cluck "Failed to store UniProt2GO version ($uniprot_to_go_version)!";
                    LogWarning("Failed to store UniProt2GO version ($uniprot_to_go_version)!");
                }
    
                LogInfo("Parsing Uniprot to GO data");
UNIPROT_TO_GO_LOOP:
                while (my $line = <$uniprot_to_go_fh>)
                {
                    $progression += length($line);
                    printf("\rProgression: %.0i%% ", (100.*$progression/$uniprot_file_size) || 0);
                    # skip comments
                    if ($line =~ m/^!/)
                    {
                        next UNIPROT_TO_GO_LOOP;
                    }
                    
=pod

=head3 UniProt to GO file line format:

1.  DB
Database from which annotated entry has been taken.
For the UniProtKB and UniProtKB Complete Proteomes gene associaton files: UniProtKB
For the PDB association file:  PDB
Example: UniProtKB

2.  DB_Object_ID
A unique identifier in the database for the item being annotated.
Here: an accession number or identifier of the annotated protein
(or PDB entry for the gene_association.goa_pdb file)
For the UniProtKB and UniProtKB Complete Proteomes gene association files: a UniProtKB Accession.
Examples O00165

3.  DB_Object_Symbol
A (unique and valid) symbol (gene name) that corresponds to the DB_Object_ID.
An officially approved gene symbol will be used in this field when available.
Alternatively, other gene symbols or locus names are applied.
If no symbols are available, the identifier applied in column 2 will be used.
Examples: G6PC
CYB561
MGCQ309F3

4.  Qualifier
This column is used for flags that modify the interpretation of an
annotation.
If not null, then values in this field can equal: NOT, colocalizes_with, contributes_to,
NOT | contributes_to, NOT | colocalizes_with
Example: NOT

5.  GO ID
The GO identifier for the term attributed to the DB_Object_ID.
Example: GO:0005634

6.  DB:Reference
A single reference cited to support an annotation.
Where an annotation cannot reference a paper, this field will contain
a GO_REF identifier. See section 8 and
http://www.geneontology.org/doc/GO.references
for an explanation of the reference types used.
Examples: PMID:9058808
DOI:10.1046/j.1469-8137.2001.00150.x
GO_REF:0000002
GO_REF:0000020
GO_REF:0000004
GO_REF:0000003
GO_REF:0000019
GO_REF:0000023
GO_REF:0000024
GO_REF:0000033

7.  Evidence
One of either EXP, IMP, IC, IGI, IPI, ISS, IDA, IEP, IEA, TAS, NAS,
NR, ND or RCA.
Example: TAS

8.  With
An additional identifier to support annotations using certain
evidence codes (including IEA, IPI, IGI, IMP, IC and ISS evidences).
Examples: UniProtKB:O00341
InterPro:IPROO1878
RGD:123456
CHEBI:12345
Ensembl:ENSG00000136141
GO:0000001
EC:3.1.22.1

9.  Aspect
One of the three ontologies, corresponding to the GO identifier applied.
P (biological process), F (molecular function) or C (cellular component).
Example: P

10. DB_Object_Name
Name of protein
The full UniProt protein name will be present here,
if available from UniProtKB. If a name cannot be added, this field
will be left empty.
Examples: Glucose-6-phosphatase
Cellular tumor antigen p53
Coatomer subunit beta

11. Synonym
Gene_symbol [or other text]
Alternative gene symbol(s), IPI identifier(s) and UniProtKB/Swiss-Prot identifiers are
provided pipe-separated, if available from UniProtKB. If none of these identifiers
have been supplied, the field will be left empty.
Example:  RNF20|BRE1A|IPI00690596|BRE1A_BOVIN
IPI00706050
MMP-16|IPI00689864

12. DB_Object_Type
What kind of entity is being annotated.
Here: protein (or protein_structure for the
gene_association.goa_pdb file).
Example: protein

13. Taxon_ID
Identifier for the species being annotated.
Example: taxon:9606

14. Date
The date of last annotation update in the format 'YYYYMMDD'
Example: 20050101

15. Assigned_By
Attribute describing the source of the annotation.  One of
either UniProtKB, AgBase, BHF-UCL, CGD, DictyBase, EcoCyc, EcoWiki, Ensembl,
FlyBase, GDB, GeneDB_Spombe,GeneDB_Pfal, GOC, GR (Gramene), HGNC, Human Protein Atlas,
JCVI, IntAct, InterPro, LIFEdb, PAMGO_GAT, MGI, Reactome, RGD,
Roslin Institute, SGD, TAIR, TIGR, ZFIN, PINC (Proteome Inc.) or WormBase.
Example: UniProtKB

16. Annotation_Extension
Contains cross references to other ontologies/databases that can be used to qualify or
enhance the GO term applied in the annotation.
The cross-reference is prefaced by an appropriate GO relationship; references to multiple ontologies
can be entered.
Example: part_of(CL:0000084)
occurs_in(GO:0009536)
has_input(CHEBI:15422)
has_output(CHEBI:16761)
has_participant(UniProtKB:Q08722)
part_of(CL:0000017)|part_of(MA:0000415)

17. Gene_Product_Form_ID
The unique identifier of a specific spliceform of the protein described in column 2 (DB_Object_ID)
Example:O43526-2

=cut
                    # parse line cf. http://www.geneontology.org/GO.format.gaf-2_0.shtml
                    if ($line =~ m/\s(GO:\d{7})\s.*\s(\w{1,4})\s+UniProtKB:(\S+)\s/)
                    {
                        my $go_code        = $1;
                        my $evidence_code  = $2;
                        my $uniprot_code   = $3;
                        print "(UniProt: $uniprot_code --> $go_code)          ";
                        LogInfo("UniProt: $uniprot_code --> $go_code");
                        # check if Uniprot is in database
                        if (my ($greenphyl_uniprot_id) = $dbh->selectrow_array("SELECT dbxref_id FROM dbxref WHERE accession LIKE ?;", undef, $uniprot_code))
                        {
                            # check if GO is in database
                            my $greenphyl_go_id;
                            my $go_description = $obo_data->{$go_code}->{'name'} || '';
                            if ((($greenphyl_go_id) = $dbh->selectrow_array("SELECT go_id FROM go WHERE go_code LIKE ?;", undef, $go_code))
                                && $go_description)
                            {
                                # already there, update GO
                                $dbh->do("UPDATE go SET go_desc = ? WHERE go_id = ?;", undef, $go_description, $greenphyl_go_id);
                                ++$updated_go_count;
                            }
                            else
                            {
                                # not there, insert...
                                # get GO type
                                my $go_type = $obo_data->{$go_code}->{'namespace'} || '';
                                if (!$go_type)
                                {
                                    LogWarning("GO term type not found for GO '$go_code'!");
                                }
                                if (!$go_description)
                                {
                                    LogWarning("GO term description not found for GO '$go_code'!");
                                }
                                $dbh->do("INSERT INTO go (go_code, go_type, go_desc) VALUES (?, ?, ?);", undef, $go_code, $go_type, $go_description);
                                ++$inserted_go_count;
                                # get go_id
                                ($greenphyl_go_id) = $dbh->selectrow_array("SELECT go_id FROM go WHERE go_code LIKE ?;", undef, $go_code);
                            }
                            # make sure we found a GO ID
                            if (!$greenphyl_go_id)
                            {
                                confess "ERROR: Failed to retrieve GO id for GO '$go_code'!\n";
                            }
                            # link UniProt to GO if needed
                            if (!$dbh->do("INSERT IGNORE INTO uniprot_to_go (dbxref_id, go_id, evidence_code) VALUES (?, ?, ?);", undef, $greenphyl_uniprot_id, $greenphyl_go_id, $evidence_code))
                            {
                                ++$uniprot_to_go_failed_count;
                                LogError("Failed to insert a GO to UniProt relationship! UniProt: '$uniprot_code' (id=$greenphyl_uniprot_id), GO: '$go_code' (id=$greenphyl_go_id), Evidence code: $evidence_code\n");
                                cluck "ERROR: Failed to insert a GO to UniProt relationship! UniProt: '$uniprot_code' (id=$greenphyl_uniprot_id), GO: '$go_code' (id=$greenphyl_go_id), Evidence code: $evidence_code\n";
                            }
                            else
                            {
                                ++$uniprot_to_go_count;
                            }
                        }
                        else
                        {
                            LogDebug("UniProt code not found in database: $uniprot_code");
                        }
                    }
                    else
                    {
                        LogDebug("Ignored line: $line");
                    }
                }
                LogInfo("Done parsing Uniprot to GO data");
                print "\n";
    
                # close file
                close($uniprot_to_go_fh);
                LogInfo("UniProt to GO relationship set: $uniprot_to_go_count\nUniProt to GO relationship NOT set: $uniprot_to_go_failed_count");
                print "UniProt to GO relationship set: $uniprot_to_go_count\nUniProt to GO relationship NOT set: $uniprot_to_go_failed_count\n";
            }
            LogInfo("Updated GO: $updated_go_count\nInserted GO: $inserted_go_count\n");
            print "Updated GO: $updated_go_count\nInserted GO: $inserted_go_count\n\n";
            
            ### Update EC to GO
            if (auto_prompt("Update EC to GO? (y/n)", { 'default' => 'y', 'constraint' => '[yYnN]' }) =~ m/y/i)
            {
                if (!$ec2go_filepath)
                {
                    $ec2go_filepath = $Greenphyl::Config::EC_TO_GO_FILE_PATH;

                     if (auto_prompt("Fetch EC to GO mapping file from the web? (y/n)", { 'default' => 'y', 'constraint' => '[yYnN]' }) =~ m/y/i)
                     {
                            # check if file already exists
                            if ((!-e $ec2go_filepath) 
                                || (auto_prompt("File '$ec2go_filepath' already exists! Overwrite? (y/n)", { 'default' => 'y', 'constraint' => '[yYnN]' }) =~ m/y/i))
                            {
                                # fetch file from the web
                                if (system("wget -O '$ec2go_filepath' '" . GetURL('ec_to_go') . "'"))
                                {
                                    confess "ERROR: Failed to fetch file using wget!\n$?\n";
                                }
                            }
                     }
                }
                else
                {
                    $ec2go_filepath = auto_prompt("Enter the EC-to-GO input file path: ", { 'default' => $uniprot2go_filepath, 'constraint' => '\w+' });
                }
                LogInfo("EC2GO mapping file: '$ec2go_filepath'");

                 # parse file
                my $ec_to_go_fh;
                if (!open($ec_to_go_fh, $ec2go_filepath))
                {
                    confess "ERROR: Failed to open file '$ec2go_filepath '!\n$!\n";
                }
                my $ec_file_size = -s $ec_to_go_fh;
                $progression = 0;
                    
                         
                LogInfo("Parsing EC to GO data");
EC_TO_GO_LOOP:
                while (my $line = <$ec_to_go_fh>)
                {
                    $progression += length($line);
                    printf("\rProgression: %.0i%% ", (100.*$progression/$ec_file_size) || 0);
                    # skip comments
                    if ($line =~ m/^!/)
                    {
                        next EC_TO_GO_LOOP;
                    }
                    # parse line
                    if ($line =~ m/(EC:[\d\.]+)\s.*;\s(GO:\d{7})/)
                    {
                        my $ec_code  = $1;
                        my $go_code  = $2;
                       
                        print "(EC: $ec_code -->  $go_code )";
                        
                        #Insert EC in database
                        $dbh->do("INSERT IGNORE INTO dbxref (dbxref_id, db_id, accession ) VALUES (DEFAULT, ?, ?);",  undef, 4, $ec_code);
                        
                        #select EC in database
                        if (my ($greenphyl_ec_id) = $dbh->selectrow_array("SELECT dbxref_id FROM dbxref WHERE accession LIKE ?;", undef, $ec_code))
                        {
                            # check if GO is in database
                            my $greenphyl_go_id;
                            my $go_description = $obo_data->{$go_code}->{'name'} || '';
                            if ((($greenphyl_go_id) = $dbh->selectrow_array("SELECT go_id FROM go WHERE go_code LIKE ?;", undef, $go_code))
                                && $go_description)
                            {
                                # already there, update GO
                                $dbh->do("UPDATE go SET go_desc = ? WHERE go_id = ?;", undef, $go_description, $greenphyl_go_id);
                                ++$updated_go_count;
                            }
                            else
                            {
                                # not there, insert...
                                # get GO type
                                my $go_type = $obo_data->{$go_code}->{'namespace'} || '';
                                if (!$go_type)
                                {
                                    LogWarning("GO term type not found for GO '$go_code'!");
                                }
                                if (!$go_description)
                                {
                                    LogWarning("GO term description not found for GO '$go_code'!");
                                }
                                $dbh->do("INSERT INTO go (go_code, go_type, go_desc) VALUES (?, ?, ?);", undef, $go_code, $go_type, $go_description);
                                ++$inserted_go_count;
                                # get go_id
                                ($greenphyl_go_id) = $dbh->selectrow_array("SELECT go_id FROM go WHERE go_code LIKE ?;", undef, $go_code);
                            }
                            # make sure we found a GO ID
                            if (!$greenphyl_go_id)
                            {
                                confess "ERROR: Failed to retrieve GO id for GO '$go_code'!\n";
                            }
                            # link EC to GO if needed
                            if (!$dbh->do("INSERT IGNORE INTO ec_to_go (dbxref_id, go_id) VALUES (?, ?);", undef, $greenphyl_ec_id, $greenphyl_go_id))
                            {
                                ++$ec_to_go_failed_count;
                                LogError("Failed to insert a GO to EC relationship! EC: '$ec_code' (id=$greenphyl_ec_id), GO: '$go_code' (id=$greenphyl_go_id)\n");
                                cluck "ERROR: Failed to insert a GO to EC relationship! EC: '$ec_code' (id=$greenphyl_ec_id),  GO: '$go_code' (id=$greenphyl_go_id)\n";
                            }
                            else
                            {
                                ++$ec_to_go_count;
                            }
                        }
                    }
                }
                LogInfo("Done parsing EC to GO data");
                print "\n";
            }
            LogInfo("Updated GO: $updated_go_count\nInserted GO: $inserted_go_count\n EC: $ec_to_go_count\n");
            print "Updated GO: $updated_go_count\nInserted GO: $inserted_go_count\n EC: $ec_to_go_count\n\n";
            
            # clear GO cache
            $dbh->do("TRUNCATE go_family_cache;");

            print "...done!\n";
            LogInfo("Done.");
        }
        elsif ($code =~ m/^(?:$RUN_INT_CODE_UPDATE_TAXONOMY|$RUN_CODE_UPDATE_TAXONOMY)$/si) # Update taxonomy
        {
            $need_commit = 1;
            print "\nUpdate taxonomy...\n";
            LogInfo("Update taxonomy...");
            LogInfo("Code $RUN_CODE_UPDATE_TAXONOMY");

            # check if a species tree has been specified
            my $got_user_tree = 0;
            if ($taxonomy_tree_filepath)
            {
                if (!-r $taxonomy_tree_filepath)
                {
                    LogError("Input tree file '$taxonomy_tree_filepath' not readable!");
                    confess "Input tree file '$taxonomy_tree_filepath' not readable!\n";
                }
                LogInfo("Input tree provided: '$taxonomy_tree_filepath'");
                $got_user_tree = 1;
            }
            else
            {
                # retrieve taxonomy tree from NCBI
                LogInfo("Retrieve taxonomy tree from NCBI");
                # get species tax_id
                my $sql_query = "SELECT tax_id FROM species;";
                my @species_tax_id = @{$dbh->selectcol_arrayref($sql_query)};

                if (!@species_tax_id)
                {
                    confess "Failed to get taxonomy IDs!";
                }

                # fetch taxonomy tree from NCBI
                my $taxonomy_fetch_url = "http://www.ncbi.nlm.nih.gov/Taxonomy/CommonTree/wwwcmt.cgi?cmd=Save%20as&saveas=phylip%20tree&an=" . join('&an=', @species_tax_id);
                my $ncbi_taxonomy_tree_filepath = $path_manager->getTempDirectory() . '/' . 'greenphyl_ncbi_taxonomy.nwk';
                if (system("wget -O '$ncbi_taxonomy_tree_filepath' '$taxonomy_fetch_url'"))
                {
                    confess "ERROR: Failed to fetch file using wget!\n$?\n";
                }
                $taxonomy_tree_filepath = $ncbi_taxonomy_tree_filepath;
            }

            # get newick file content
            my $newick_taxonomy_fh;
            if (!open($newick_taxonomy_fh, $taxonomy_tree_filepath))
            {
                confess "ERROR: Failed to open taxonomy tree file '$taxonomy_tree_filepath'!\n$!\n";
            }
            my $newick_taxonomy_tree = join('', <$newick_taxonomy_fh>);
            close($newick_taxonomy_fh);
            
            if (!$newick_taxonomy_tree)
            {
                confess "ERROR: taxonomy tree file '$taxonomy_tree_filepath' is empty!\n";
            }

            # store newick tree in database
            if (!$dbh->do("INSERT INTO variables (name, value) VALUES ('taxonomy_newick_tree', ?) ON DUPLICATE KEY UPDATE value = ?;", undef, $newick_taxonomy_tree, $newick_taxonomy_tree))
            {
                LogError("Failed to store taxonomy newick tree!");
                confess "Failed to store taxonomy newick tree!";
            }

            # convert into HTML tree
            use Greenphyl::TreeNode;
            my $root_node = Greenphyl::TreeNode->loadNewickTree($newick_taxonomy_tree);

            if (!$got_user_tree)
            {
                # re-order species...
                $sql_query = "SELECT organism AS \"organism\", tax_order AS \"index\" FROM species ORDER BY tax_order ASC;";
                # store species order in a hash{species} = index
                my $species_order = $dbh->selectall_hashref($sql_query, 'organism');

                # validate database species order...
                # start from leaves and go up until all nodes are processed or an error is met
                my @nodes_to_process = $root_node->getLeafNodes();
                # prepare index_range structure
                my $node_range = {};
                foreach my $node_to_process (@nodes_to_process)
                {
                    #+FIXME: create a global constant for name lookup
                    # we use 'Oryza sativa' as name for 'Oryza sativa Japonica Group'
                    if ($node_to_process->getName() eq 'Oryza sativa Japonica Group')
                    {
                         $node_to_process->setName('Oryza sativa');
                    }
                    # we use 'Malus domestica' as name for 'Malus x domestica'
                    if ($node_to_process->getName() eq 'Malus x domestica')
                    {
                         $node_to_process->setName('Malus domestica');
                    }
                    #+ for Matthieu...
                    if ($node_to_process->getName() eq 'Orobanche')
                    {
                         $node_to_process->setName('Orobanche aegyptiaca');
                    }
                    if ($node_to_process->getName() eq 'Striga')
                    {
                         $node_to_process->setName('Striga hermonthica');
                    }
                    if ($node_to_process->getName() eq 'Beta vulgaris subsp. vulgaris')
                    {
                         $node_to_process->setName('Beta vulgaris');
                    }
                    #+...Matthieu

                    if (!exists($species_order->{$node_to_process->getName()}))
                    {
                        confess 'Species not fount in database: ' . $node_to_process->getName() . "\n";
                    }
                    $node_range->{$node_to_process} = [$species_order->{$node_to_process->getName()}->{'index'}, $species_order->{$node_to_process->getName()}->{'index'}];
                }
                # process each level of the tree starting from the leaves up to the root
                try
                {
                    TAXA_TREE_ORDERING:
                    while (@nodes_to_process)
                    {
                        my $next_nodes_to_process = {};
                        foreach my $node_to_process (@nodes_to_process)
                        {
                            # get neighbors
                            my @children = ();
                            foreach my $neighbor_node ($node_to_process->getNeighborNodes())
                            {
                                # check if neighbor has been processed
                                if (exists($node_range->{$neighbor_node}))
                                {
                                    # child has been processed, store it as a child
                                    push(@children, $neighbor_node);
                                }
                                else
                                {
                                    # store the unprocessed neighbor node
                                    $next_nodes_to_process->{$neighbor_node} = $neighbor_node;
                                }
                            }
                            if (@children)
                            {
                                # sort child nodes
                                my $sort_child_nodes = sub
                                {
                                    if ($node_range->{$a}->[0] > $node_range->{$b}->[0])
                                    {
                                        if ($node_to_process->getNeighborNodeIndex($a) < $node_to_process->getNeighborNodeIndex($b))
                                        {
                                            $node_to_process->swapSubtrees($a, $b);
                                        }
                                        return 1;
                                    }
                                    if ($node_to_process->getNeighborNodeIndex($a) > $node_to_process->getNeighborNodeIndex($b))
                                    {
                                        $node_to_process->swapSubtrees($a, $b);
                                    }
                                    return -1;
                                };
                                # order the children
                                @children = sort $sort_child_nodes (@children);
                                # make sure subtrees follow each-other without gaps or overlapping
                                my $previous_child_node = shift(@children);
                                my $lowest_index = $node_range->{$previous_child_node}->[0];
                                while (my $child_node = shift(@children))
                                {
                                    if (($node_range->{$previous_child_node}->[1] + 1) != $node_range->{$child_node}->[0])
                                    {
                                        # sub-trees are not following each other!
                                        # confess "Cannot re-order species tree as wanted in database because it is not possible! Please check species table tax_order column and make sure it is possible to build a tree with the specified order.\n";
                                        cluck "Cannot re-order species tree as wanted in database because it is not possible! Please check species table tax_order column and make sure it is possible to build a tree with the specified order.\n";
                                        last TAXA_TREE_ORDERING;
                                    }
                                    $previous_child_node = $child_node;
                                }
                                my $highest_index = $node_range->{$previous_child_node}->[1];
                                # store range of current node and mark it as processed
                                $node_range->{$node_to_process} = [$lowest_index, $highest_index];
                            }
                            elsif (!$node_to_process->isLeaf())
                            {
                                # confess "ERROR: no children found for an internal node!\n";
                                cluck "ERROR: no children found for an internal node!\n";
                                last TAXA_TREE_ORDERING;
                            }
                        }
                        # check if some nodes should not be processed yet
                        foreach my $unprocessed_node (values(%$next_nodes_to_process))
                        {
                            my $unprocessed_neighbor_count = 0;
                            foreach my $neighbor_node ($unprocessed_node->getNeighborNodes())
                            {
                                if (!exists($node_range->{$neighbor_node}))
                                {
                                    ++$unprocessed_neighbor_count;
                                }
                            }
                            if ((1 < $unprocessed_neighbor_count)
                                || ((0 < $unprocessed_neighbor_count) && ($unprocessed_node == $root_node)))
                            {
                                # more than 1 neighbor unprocessed node or root node with at least 1 unprocessed child
                                # node not ready to be processed
                                delete($next_nodes_to_process->{$unprocessed_node});
                            }
                        }
                        @nodes_to_process = values(%$next_nodes_to_process);
                    }

                    # get ordered newick tree
                    $newick_taxonomy_tree = $root_node->getNewickTree();
                    print "Ordered Newick tree:\n$newick_taxonomy_tree\n\n";
                    # save ordered newick tree to disk
                    my $ordered_newick_taxonomy_filepath = $path_manager->getTempDirectory() . '/' . 'greenphyl_taxonomy.nwk';
                    if (open($newick_taxonomy_fh, ">$ordered_newick_taxonomy_filepath"))
                    {
                        print {$newick_taxonomy_fh} $newick_taxonomy_tree;
                        close($newick_taxonomy_fh);
                    }
                    print "Tree saved to '$ordered_newick_taxonomy_filepath'\n";
                    LogInfo("Newick tree saved to '$ordered_newick_taxonomy_filepath'");
                    
                    # and update ordered newick tree in database
                    if (!$dbh->do("UPDATE variables SET value = ? WHERE name = 'taxonomy_newick_tree';", undef, $newick_taxonomy_tree))
                    {
                        LogError("Failed to store ordered taxonomy newick tree!");
                        confess "Failed to store ordered taxonomy newick tree!";
                    }
                    LogInfo("Newick tree updated in database\n");
                }
                otherwise
                {
                    print "WARNING: Failed to re-order tree leaves: " . shift() . "\n";
                };
            }

            # render tree in HTML
            my $html_taxonomy_tree = "<div id=\"species_tree\">\n" . $root_node->getHTMLTree() . "</div>\n";

            # store HTML tree in database
            if (!$dbh->do("INSERT INTO variables (name, value) VALUES ('taxonomy_html_tree', ?) ON DUPLICATE KEY UPDATE value = ?;", undef, $html_taxonomy_tree, $html_taxonomy_tree))
            {
                LogError("Failed to store taxonomy HTML tree!");
                confess "Failed to store taxonomy HTML tree!";
            }
            
            print "DEBUG:\n\n$html_taxonomy_tree\n\n";

            print "...done!\n";
            LogInfo("Done.");
        }
        elsif ($code =~ m/^(?:$RUN_INT_CODE_LOAD_CLASSIFICATION|$RUN_CODE_LOAD_CLASSIFICATION)$/si) # Load MCL classification
        {
            $need_commit = 0;
            print "\nLoad classification...\n";
            LogInfo("Load classification...");
            LogInfo("Code $RUN_CODE_LOAD_CLASSIFICATION");
            
            my $mcl_filepath = prompt("Please enter the file name (path) to your MCL file: ", { 'constraint' => '\w' });
            if (-r $mcl_filepath && -f $mcl_filepath)
            {
                my $family_level = prompt("Which classification level to process (1, 2, 3 or 4)? ", { 'constraint' => '^[1234]$' });
                if (system("perl load_classification.pl '$mcl_filepath' $family_level"))
                {
                    warn "WARNING: Failed to load classification!\n$!\n";
                }
            }
            else
            {
                confess "File '$mcl_filepath' not found or is not readable!\n";
            }
            print "...done!\n";
            LogInfo("Done.");
        }
        elsif ($code =~ m/^(?:$RUN_INT_CODE_LOAD_ORTHOLOGS|$RUN_CODE_LOAD_ORTHOLOGS)$/si) # Load orthologs
        {
            $need_commit = 1;
            print "\nLoad orthologs...\n";
            LogInfo("Load orthologs...");
            LogInfo("Code $RUN_CODE_LOAD_ORTHOLOGS");
            
            $pipeline_results_path ||= auto_prompt("Please enter the path to your GreenPhyl pipeline results: ", { 'constraint' => '\w', 'default' => '../../../pipeline/temp/' });
            # remove trailing slash '/'
            $pipeline_results_path =~ s/\/+$//;
            
            $orthologs_pairs_export ||= (auto_prompt("Do you want to export orthologs pairs in a text file (NO DB load) [y/n] ", { 'default' => 'n', 'constraint' => '[yYnN]' }) =~ m/[yY]/);
            
            my ($op_output_fh, $ortho_pairs_output_filepath);
            if ($orthologs_pairs_export)
            {
                $ortho_pairs_output_filepath ||= auto_prompt("Please enter the filename to your export file: ", { 'constraint' => '\w', 'default' => '../../../data/ortho_pairs'  . time . '.txt' });
                open($op_output_fh, ">$ortho_pairs_output_filepath");                         
                if (-e $ortho_pairs_output_filepath)
                {
                    print "\nFile created...\n"; 
                } 
                else
                {
                    print "$ortho_pairs_output_filepath failed!";
                }                           
            }
            
            # make sure we can process the directory
            if (-r $pipeline_results_path
                && -x $pipeline_results_path
                && -d $pipeline_results_path)
            {
                # process each subdirectory
                LogDebug("Process pipeline directory: '$pipeline_results_path'\n");
                my $pipeline_results_dh;
                opendir($pipeline_results_dh, $pipeline_results_path)
                    or confess "ERROR: $!";
                readdir($pipeline_results_dh); # reads .
                readdir($pipeline_results_dh); # reads ..
                while (my $fam_results_dir = readdir($pipeline_results_dh))
                {
                    # make sure we process a family (ie. number) directories
                    if (($fam_results_dir =~ m/^\d+$/) && (-d "$pipeline_results_path/$fam_results_dir"))
                    {
                        # check if there is a "*_rap_stats_tree.txt" file and process it
                        my $rap_output_filepath = "$pipeline_results_path/$fam_results_dir/$fam_results_dir" . $RAP_ORTHOLOGS_OUTPUT_EXT;
                        my $rap_output_fh;
                        if (-e $rap_output_filepath)
                        {
                            LogDebug("Processing RAP output '$rap_output_filepath'\n");
                            if (open($rap_output_fh, $rap_output_filepath))
                            {
                                # remove previous homologies of processed family (if export is not asked)
                                if ((!$orthologs_pairs_export) && !$dbh->do("DELETE FROM homologies WHERE family_id = ?;", undef, $fam_results_dir))
                                {
                                    cluck "Failed to remove previous homology data for family '$fam_results_dir'!\n";
                                    LogWarning("Failed to remove previous homology data for family '$fam_results_dir'!");
                                }
                                
                                # skip first line
                                my $line = <$rap_output_fh>;
                                # ex.:
                                # GENE1                   kVALUE  DIST    SPEC    T-DUP   I-DUP   ORTHO   ULTRAP  GENE2
                                # cmh256c_CYAME           131     2.7326  4       0       2       true    false   Phypa_129914_PHYPA
                                # Bradi2g05300.1_BRADI    105     98.1689 2       0       0       true    false   PDK_30s1187421g005_PHODA
                                while ($line = <$rap_output_fh>)
                                {
                                    my ($gene_name,
                                        $species_code,
                                        $kvalue,
                                        $distance,
                                        $speciation,
                                        $tdup,
                                        $idup,
                                        $orthology,
                                        $ultraparalogy,
                                        $homologous_gene,
                                        $homologous_species) = (
                                            $line =~ m/
                                                (\S+)_(\w{3,5})\s+ # gene name and species code
                                                (\d+)\s+         # kvalue
                                                ([\d.\-+eE]+)\s+ # distance
                                                (\d+)\s+         # speciation
                                                (\d+)\s+         # t-dup
                                                (\d+)\s+         # i-dup
                                                (\w+)\s+         # orthology
                                                (\w+)\s+         # paralogy
                                                (\S+)_(\w{3,5})    # homolog gene and species
                                            /x
                                    );
                                    if (defined($homologous_species))
                                    {
                                        if ($orthologs_pairs_export)
                                        {
                                            $need_commit = 0;
                                            print $op_output_fh "$gene_name \t $homologous_gene \n" if ($orthology =~ m/true/i);       
                                        }
                                        else 
                                        {                                       
                                            # print "DEBUG: $gene_name ($species_code) is " . ($orthology =~ m/true/i ? 'orthologous':'ultraparalogous') . " to $homologous_gene ($homologous_species)\n"; #+debug
                                            # insert homology relationship
                                            if (!$dbh->do("INSERT INTO homologies (family_id, type, score, kvalue, distance, speciation, i_duplication, t_duplication) VALUES (?, ?, ?, ?, ?, ?, ?, ?);", undef, $fam_results_dir, ($orthology =~ m/true/i ? $HOMOLOGY_TYPE_ORTHOLOGY : $HOMOLOGY_TYPE_ULTRA_PARALOGY), -1, $kvalue, $distance, $speciation, $idup, $tdup))
                                            {
                                                cluck "Failed to insert homology for $gene_name ($species_code) and $homologous_gene ($homologous_species)!\n";
                                                LogWarning("Failed to insert homology for $gene_name ($species_code) and $homologous_gene ($homologous_species)!");
                                            }
                                            my $homology_id = $dbh->last_insert_id(undef, undef, 'homologies', 'homology_id');
                                            if (!$homology_id || !$dbh->do("INSERT INTO homologs (homology_id, seq_id) SELECT ?, seq_id FROM sequences WHERE seq_textid IN (?, ?);", undef, $homology_id, $gene_name, $homologous_gene))
                                            {
                                                cluck "Failed to insert homology relationship for $gene_name ($species_code) and $homologous_gene ($homologous_species)!\n";
                                                LogWarning("Failed to insert homology relationship for $gene_name ($species_code) and $homologous_gene ($homologous_species)!");
                                            }
                                        }
                                    }
                                    else
                                    {
                                        #cluck "Failed to parse homology line:\n$line\n";
                                        confess "Failed to parse homology line:\n$line\n";
                                        LogWarning("Failed to parse homology line:\n$line");
                                    }
                                }
                            }
                            else
                            {
                                cluck "WARNING: Failed to open RAP output file '$rap_output_filepath'!\n$!\n";
                            }
                        }
                        else
                        {
                            # no RAP output available
                            LogDebug("No RAP output found in '$fam_results_dir'\n");
                        }
                    }
                    else
                    {
                        # skip non-directory
                        LogDebug("Skip non-directory: '$fam_results_dir'\n");
                    }
                }
                closedir($pipeline_results_dh);
            }
            else
            {
                confess "Directory '$pipeline_results_path' not accessible!\n";
            }
            
            close $op_output_fh if ($orthologs_pairs_export);

            print "...done!\n";
            LogInfo("Done.");
        }
        elsif ($code =~ m/^x$/i) # just exit
        {
            $interactive_mode = 0;
        }
        else
        {
            confess "ERROR: unrecognized update code '$code'!\n";
        }


        if ($need_commit)
        {
            if ($debug)
            {
                confess "DEBUG: ROLLBACK!\n";
            }
            # commit SQL transaction
            $dbh->commit() or confess($dbh->errstr);
        }

    }
    otherwise
    {
        my $error = shift;
        print "ERROR: An unexpected error occured!\n$error\n";
        print "Duration: " . getDuration(time() - $start_date) . "\n";

        LogInfo("ERROR: An unexpected error occured!\n$error");
        LogInfo("Duration: " . getDuration(time() - $start_date));
        LogInfo("-------------------------------------------------------------------------------");
        $dbh->rollback() or confess($error . "\nAnother error occured during rollback:\n" . $dbh->errstr);
        confess $error;
    };
    LogInfo("Duration: " . getDuration(time() - $start_date));
    LogInfo("-------------------------------------------------------------------------------");
    print "Duration: " . getDuration(time() - $start_date) . "\n";

    # display menu and restart... or exit
    if ($interactive_mode)
    {
        # reinit variables
        $species_name     = undef;
        $species_id       = undef;
        $input_filename   = undef;
        $species_taxonomy = undef;
        $organism         = undef;
        $common_name      = undef;
        $url_institute    = undef;
        $url_fasta        = undef;
        $url_picture      = undef;
        $version          = undef;
        $previous_species = undef;
        $source_blast     = undef;
        $update           = undef;
        $interpro_path    = undef;
        $ipr_version      = undef;
        $previous_species = undef;

        # show menu
        $code = showMainMenu('species' => $species_name);
    }
    else
    {
        $code = 'x';
    }
}


# CODE END
###########


=pod

=head1 DIAGNOSTICS


=head1 AUTHORS

Valentin GUIGNON (Bioversity-France), v.guignon@cgiar.org
Christelle ALUOME
Mathieu ROUARD (Bioversity-France), m.rouard@cgiar.org
Christian WALDE
Matthieu CONTE

=head1 VERSION

Version 2.0.x

$Id$

=head1 SEE ALSO

GreenPhyl documentation

=cut
