#!/usr/bin/perl

=pod

=head1 NAME

show_logs.pl - Displays Greenphyl logs

=head1 SYNOPSIS

    show_logs.pl -follow

=head1 REQUIRES

Perl5, Greenphyl

=head1 DESCRIPTION

Display logs stored in database.

=cut

use strict;
use Carp qw (cluck confess croak);
use warnings;

use lib '../../lib';
use lib '../../local_lib';


use Getopt::Long;
use Pod::Usage;
use Error qw(:try);

use Greenphyl::Config;
use Greenphyl::Log('nolog' => 1,);




# Script options
#################

=pod

=head1 OPTIONS

perl show_logs.pl [-help | -man] [-debug] [-from_date <DATE>] [-to_date <DATE>] [-pid <PID>] [-search <STRING>] [-last [OFFSET]] [-follow]

=head2 Parameters

=over 4

=item B<-help>:

Prints a brief help message and exits.

=item B<-man>:

Prints the manual page and exits.

=item B<-debug>:

Executes the script in debug mode.

=item B<-from_date DATE> (date):

restrict log message to messages issued after DATE. DATE should be of the form:
"2011-03-01 13:00:00"

=item B<-to_date DATE> (date):

restrict log message to messages issued before DATE. DATE should be of the form:
"2011-03-01 13:00:00"

=item B<-pid PID> (integer):

restrict log message to messages issued by the process of pid PID.

=item B<-search STRING> (string):

restrict log message to messages containing STRING.

=item B<-last> (integer):

only display all the messages from the last pid recorded. If an offset is
provided, it will display all the messages from the "offset"-th pid before last
pid recorded.

=item B<-follow> (flag):

keep displaying the last entries appended to the logs.

=back

=cut


# CODE START
#############

# options processing
my ($man, $help, $debug, $pid, $from_date, $to_date, $search_string, $last, $follow) = (0, 0, 0, 0, 0, 0, '', undef, undef);
# parse options and print usage if there is a syntax error.
GetOptions("help|?"      => \$help,
           "man"         => \$man,
           "debug"       => \$debug,
           "from_date=s" => \$from_date,
           "to_date=s"   => \$to_date,
           "pid=s"       => \$pid,
           "search=s"    => \$search_string,
           "last:i"      => \$last,
           "follow"      => \$follow,
          ) # a string
    or pod2usage(2);
if ($help) {pod2usage(1);}
if ($man) {pod2usage(-verbose => 2);}

my $query_parameters = {};

if (defined($follow))
{
    my $last_log_id = GetLastLogID();
    print GetLogs('from_log_id' => $last_log_id,
                  'limit'       => 1);
    while (1)
    {
        while (GetLastLogID() == $last_log_id)
        {sleep(1);}
        my $next_log_id = GetLastLogID();
        print GetLogs('from_log_id' => $last_log_id + 1,
                      'to_log_id'   => $next_log_id,
                      'limit'       => 0);
        $last_log_id = $next_log_id;
    }
}
elsif (defined($last))
{
    if (0 <= $last)
    {
        print GetLastLogs();
    }
    else
    {
        print GetLastLogs(-1*$last);
    }
}
else
{
    print GetLogs('from_date' => $from_date,
                  'to_date'   => $to_date,
                  'pid'       => $pid,
                  'script'    => 'update.pl',
                  'search'    => $search_string,
                  'limit'     => 0);
}

exit(0);

# CODE END
###########


=pod

=head1 AUTHORS

Mathieu ROUARD (Bioversity-France), m.rouard@cgiar.org

Valentin GUIGNON (Bioversity-France), v.guignon@cgiar.org

Matthieu CONTE

Christian WALDE

=head1 VERSION

Version 1.0.0

Date 01/03/2011

=head1 SEE ALSO

GreenPhyl documentation

=cut
