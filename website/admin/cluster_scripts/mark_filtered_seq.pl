#!/usr/bin/perl

=pod

=head1 NAME

mark_filtered_seq.pl - Mark filtered sequences in database

=head1 SYNOPSIS

    mark_filtered_seq.pl

=head1 REQUIRES

Perl5, mySQL

=head1 DESCRIPTION

Set the 'filtered' field of 'sequences' table to a true value for sequences of
families that have been filtered. The 'filtered' field is treated like an array
of binary flags, each bit position corresponding to the class of the family.

We assume that a sequence can only belong to one family at a given class.

For instance, if a sequence has a filtered value of 6, which is in binary:
0b00000110
it means that the sequence has been filtered in its families of level 1 and 2.
(2**1 + 2**2 = 6)

Note: flag bit 0 is reserved for future/other uses.

=cut

use lib '../../lib';
use lib '../../local_lib';


use strict;
use warnings;
use Carp qw (cluck confess croak);
use Readonly;

use Getopt::Long;
use Pod::Usage;
use Error qw(:try);
use File::Temp qw/ tempfile tempdir /;
 
use Greenphyl::Config;
use Greenphyl::Log('nolog' => 1,);
use Greenphyl::Utils;

++$|;

# Script global constants
##########################

=pod

=head1 CONSTANTS

B<$DEFAULT_PIPELINE_OUTPUT_DIRECTORY>: (string)

local path to GreenPhyl pipeline output without ending slash '/'.

B<$MAX_CLASS_LEVEL>: (integer)

Maximum number of class supported.

B<$FILTRATION_FILE_EXT>: (string)

Extension of the filtration file.

=cut

my $DEFAULT_PIPELINE_OUTPUT_DIRECTORY = '/home/greenphyl/pipeline/output';
my $MAX_CLASS_LEVEL                   = 7;
my $FILTRATION_FILE_EXT               = '_filtered.seq';




# Script global variables
##########################

=pod

=head1 VARIABLES

B<$g_debug>: (boolean)

debug mode flag. Set to 1 when "-debug" command line parameter is used.
Default: false

B<$g_verbose>: (boolean)

verbose mode. Display processed families and files.
Default: false

B<$g_dbh>: (DBI handler)

Global database handler.

=cut

my $g_debug;
my $g_verbose;
my $g_dbh;



=pod

=head1 FUNCTIONS

=head2 processFiltrationFile

B<Description>: Set filtration flag of filtered sequences.

B<ArgsCount>: 2

=over 1

=item $family_id: (intger) (R)

ID of the family the sequences belong to.

=item $filtration_file_path: (string) (R)

Name of the file containing filtered sequences.

=back

B<Return>: (integer)

The number of filtered sequences.

=cut

sub processFiltrationFile
{
    my ($family_id, $filtration_file_path) = @_;
    my $filtered_sequence_count = 0;

    # check parameters
    if (!$family_id)
    {
        confess "ERROR: processFiltrationFile: missing family ID!\n";
    }

    if (!$filtration_file_path)
    {
        confess "ERROR: processFiltrationFile: no filtration file provided!\n";
    }
    elsif (!-e $filtration_file_path)
    {
        confess "ERROR: processFiltrationFile: missing file '$filtration_file_path'!\n";
    }

    # get family level
    my $sql_query = "SELECT fi.class_id FROM found_in fi WHERE fi.family_id = ?;";
    my ($class_id) = ($g_dbh->selectrow_array($sql_query, undef, $family_id));
    if (!$class_id)
    {
        warn "WARNING: failed to retrieve family level for family '$family_id'! Filtration flags not set!\n";
        return 0;
    }
    # check if level is supported
    if (($class_id <= 0) || ($class_id > $MAX_CLASS_LEVEL))
    {
        warn "WARNING: unsupported family level for family '$family_id': $class_id.  Filtration flags not set!\n";
        return 0;
    }

    # compute class flags mask
    my $family_filtration_flag = 2**$class_id;

    # clear filtration file in DB of every sequence of the family
    # ~$family_filtration_flag --> bitwise invertion
    # & --> bitwise logical "and" to only keep other flags if they are set
    $sql_query = "UPDATE sequences s JOIN seq_is_in sii USING (seq_id) SET s.filtered = (s.filtered & ~$family_filtration_flag) WHERE sii.family_id = ?;";
    warn "DEBUG: UPDATE sequences s JOIN seq_is_in sii USING (seq_id) SET s.filtered = (s.filtered & ~$family_filtration_flag) WHERE sii.family_id = $family_id;\n" if ($g_debug); #+debug
    if (!$g_dbh->do($sql_query, undef, $family_id))
    {
        cluck "WARNING: Failed to update sequence filtration flags for family $family_id: " . $g_dbh->errstr;
    }

    # check if file is empty
    if (-z $filtration_file_path)
    {
        warn "NOTE: filtration file is empty for family '$family_id'! No sequence has been filtered!\n";
    }
    else
    {
        warn "NOTE: some sequences of '$family_id' were filtered\n";
        my $filtration_fh;
        if (open($filtration_fh, $filtration_file_path))
        {
            # get the list of sequences
            my @seq_textids = (<$filtration_fh>);
            close($filtration_fh);
            # process each sequence
            foreach my $seq_textid (@seq_textids)
            {
                $seq_textid =~ s/_[A-Z]{3,6}\n*$//;
                warn "DEBUG: filtering sequence '$seq_textid'\n" if ($g_debug); #+debug
                # skip empty lines
                if ($seq_textid)
                {
                    # set filtration flag for the sequence
                    $sql_query = "UPDATE sequences s SET s.filtered = (s.filtered | $family_filtration_flag) WHERE s.seq_textid = ?;";
                    warn "DEBUG: UPDATE sequences s SET s.filtered = (s.filtered | $family_filtration_flag) WHERE s.seq_textid = '$seq_textid';\n" if ($g_debug); #+debug
                    if (!$g_dbh->do($sql_query, undef, $seq_textid))
                    {
                        cluck "WARNING: Failed to update sequence filtration flags for sequence '$seq_textid': " . $g_dbh->errstr;
                    }
                    else
                    {
                        ++$filtered_sequence_count;
                    }
                }
            }
        }
        else
        {
            warn "ERROR: Failed to open filtration file '$filtration_file_path'!\n$!\n";
        }
    }

    return $filtered_sequence_count;
}




# Script options
#################

=pod

=head1 OPTIONS

    trans_to_web.pl [-help | -man]
                    [-pipeline_output_dir <path_to_pipeline_output>]

=over 4

=item B<-help>:

Prints a brief help message and exits.

=item B<-man>:

Prints the manual page and exits.

=item B<-pipeline_output_dir> or B<-p> or B<-o>: (string)

Path to local pipeline outputs (directory containing the family output
directories).
Default: $DEFAULT_PIPELINE_OUTPUT_DIRECTORY

=back

=cut


# CODE START
#############

# options processing
my ($man, $help, $pipeline_output_dir)
 = (   0,     0,                   '');

# parse options and print usage if there is a syntax error.
GetOptions("help|?"             => \$help,
           "man"                => \$man,
           "debug"              => \$g_debug,
           "verbose|v"          => \$g_verbose,
           "o|p|pipeline_output_dir=s" => \$pipeline_output_dir,
) or pod2usage(2);
if ($help) {pod2usage(0);}
if ($man) {pod2usage(-verbose => 2);}

$pipeline_output_dir ||= $DEFAULT_PIPELINE_OUTPUT_DIRECTORY;


# remove trailing slash
$pipeline_output_dir =~ s/\/+$//;

# connect to database
$g_dbh = connectToDatabase();
if ($g_dbh->{AutoCommit})
{
    $g_dbh->begin_work() or croak $g_dbh->errstr;
}

print "Working on database:         '" . $g_dbh->{Name} . "'\n";
print "Working on output directory: '$pipeline_output_dir'\n";

# process each family (not black-listed)
my $sql_query = "SELECT family_id FROM family WHERE black_list = 0;";
my @family_ids = @{$g_dbh->selectcol_arrayref($sql_query)};
my $filtered_sequence_count = 0;
foreach my $family_id (@family_ids)
{
    if ($g_verbose)
    {
        print "Family $family_id:\n";
    }

    # -check if the output directory exists
    my $family_directory = $pipeline_output_dir . '/' . $family_id;
    if (-d $family_directory)
    {
        my $filtration_file_path = "$family_directory/$family_id$FILTRATION_FILE_EXT";
        if (-e $filtration_file_path)
        {
            # process filtration file
            $filtered_sequence_count += processFiltrationFile($family_id, $filtration_file_path);
        }
        else
        {
            # log: family not processed by the pipeline = skipped
            warn "WARNING: family '$family_id' doesn't have a filtration file!\n";
        }
    }
    else
    {
        # log: family not processed by the pipeline = skipped
        warn "WARNING: family '$family_id' not processed and skipped (path '$family_directory' not found)!\n";
    }
}

$g_dbh->commit() or confess($g_dbh->errstr);

print "$filtered_sequence_count sequence(s) have been marked as filtered.\n";

exit(0);

# CODE END
###########


=pod

=head1 AUTHORS

Valentin GUIGNON (CIRAD, Bioversity-France), valentin.guignon@cirad.fr, V.Guignon@cgiar.org
Mathieu ROUARD (Bioversity-France), m.rouard@cgiar.org

=head1 VERSION

Version 1.0.0

Date 13/09/2011

=head1 SEE ALSO

GreenPhyl documentation.

=cut
