#!/usr/bin/perl

=pod

=head1 NAME

run_meme.pl

=head1 SYNOPSIS

    run_meme.pl -d fasta_directory

=head1 REQUIRES

Perl5

=head1 DESCRIPTION

This script will process FASTA files present in the directory provided or the
FASTA file provided and will extract homologous score data from Rio text files,
load into the db (if specified with -l) and will create a phyloxml file with
orthologous scores embedded.

Output files and directories are placed in the same directory as each input
FASTA file as default but another place can be specified.

=cut

use strict;
use warnings;

use lib '../../lib';
use lib '../../local_lib';


use Readonly;
use Carp qw (cluck confess croak);
use Getopt::Long;
use Pod::Usage;
use File::Basename;

use Greenphyl::Config;
use Greenphyl::Utils;
use Greenphyl::SGEUtils;

use LockFile::Simple qw(trylock  unlock);



# SVN management part
######################
my $REV_STRING = '$id$';


# Script global constants
##########################

=pod

=head1 CONSTANTS

B<$DEBUG>: (boolean)

If set to non-zero, debug messages will be displayed.

B<$RUN_MEME_VERSION>: (string)

run_meme.pl version.

B<$GREENPHYL_EMAIL>: (string)

Contact e-mail adresse passed to qsub.

B<$GREENPHYL_PROJECT>: (string)

Project name passed to qsub.

B<$SGE_ARGS_MEME>: (string)

Command line arguments passed to qsub (SGE) when running MEME.
Note: the %d part is used to specify the number of processor to require.
Therefor, this constant should be used this way:
sprintf($SGE_ARGS_MEME, $g_num_processor)

B<$SGE_ARGS_MAST>: (string)

Command line arguments passed to qsub (SGE) when running MAST.

B<$FASTA_FILE_EXT>: (string)

FASTA file extension (including the dot).

B<$HTML_FILE_EXT>: (string)

HTML file extension (including the dot).

B<$MEME_HTML_FILE_PREFIX>: (string)

Name prefix for MEME HTML files.

B<$MAST_HTML_FILE_PREFIX>: (string)

Name prefix for MAST HTML files.

B<$MEME_LOG_EXT>: (string)

Log file extension for MEME logs.

B<$MEME_ERR_LOG_EXT>: (string)

Log file extension for MEME error logs.

B<$MAST_ERR_LOG_EXT>: (string)

Log file extension for MAST error logs.


=cut

Readonly my $DEBUG                 => 0;

Readonly my $RUN_MEME_VERSION      => '2.0.' . $REV_STRING;

Readonly my $GREENPHYL_EMAIL       => 'm.rouard\@cgiar.org'; # note: the @ must be provided to the command line escaped with '\', be aware to also escape the '\' if you use double quotes
Readonly my $GREENPHYL_PROJECT     => 'greenphyl';
Readonly my $SGE_ARGS_MEME         => "-q $Greenphyl::Config::CLUSTER_QUEUE -pe parallel_fill %d -b y -M $GREENPHYL_EMAIL -N " . $GREENPHYL_PROJECT . "m";
Readonly my $SGE_ARGS_MAST         => "-q $Greenphyl::Config::CLUSTER_QUEUE -pe parallel_fill 1 -b y -M $GREENPHYL_EMAIL -N " . $GREENPHYL_PROJECT . "M";
Readonly my $FASTA_FILE_EXT        => '.fa';
Readonly my $HTML_FILE_EXT         => '.html';
Readonly my $MEME_HTML_FILE_PREFIX => 'meme';
Readonly my $MAST_HTML_FILE_PREFIX => 'mast';
Readonly my $MEME_LOG_EXT          => '.meme.log';
Readonly my $MEME_ERR_LOG_EXT      => '.meme.err.log';
Readonly my $MAST_ERR_LOG_EXT      => '.mast.log';

Readonly my $NUM_PROCESSOR_DEFAULT => 16;



# Script global variables
##########################

=pod

=head1 VARIABLES

B<$g_debug>: (boolean)

Debug mode value.

B<$g_log_fh>: (file handle)

Log file handle.

B<$g_start_date>: (integer)

Time (seconds) when the program started.

B<$g_num_processor>: (integer)

Number of processors to use with MEME.

=cut

our $g_debug;
our $g_log_fh;
our $g_start_date;
our $g_num_processor = $NUM_PROCESSOR_DEFAULT;




# Script global functions
##########################

=pod

=head1 FUNCTIONS

=head2 processFastaDirectory

B<Description>: Process FASTA files of the given directory according to the
provided limitations.

B<ArgsCount>: 1-3

=over 4

=item arg: $fasta_directory (string) (R)

Full path to the directory containing FASTA files to process.

=item arg: $parameters (hash ref) (O)

Additional limitations on the files to process can be specified using this hash.
Available restrictions are:
'output_dir' => (string) path of the output directory to use (instead of the
    FASTA directory as default);
'reverse' => (boolean) if set to non-zero, file will be sorted in reverse
    alphabetical order otherwise files will be sorted in alphabetical order.
    This order hash an impact on the 'start' and 'end' limitations since it can
    invert starting and ending file names!
'start' => (string) name of the first file to process;
'end' => (string) name of the last file to process;
'time' => (integer) allowed processing time in minutes. After that delay, no
    more remaining FASTA file will be processed;
'exact' => (integer) exact number of sequence a FASTA should contain in order to
    be processed;
'less' => (integer) only FASTA with less than the specified number of sequence
    will be processed;
'more' => (integer) only FASTA with more than the specified number of sequence
    will be processed;
'no' => (string 'meme' or 'mast') will not execute MEME or MAST process;
'resume' => skip MEME or MAST step when output files are present;

=item arg: $fasta_list (array ref) (O)

list of FASTA file names (path relative to $fasta_directory parameter)

=back

B<Return>: nothing

B<Example>:

    # this will process FASTA files in '/data/fasta/'
    # starting from FASTA file '456.fa' down to '123.fa'
    # and will only process files if they have between 51 and 99 sequences
    # and will stop processing remaining FASTA file if the computation time
    # exceed 5 minutes.
    processFastaDirectory('/data/fasta/',
        {
            'reverse' => 1,
            'start'   => '456.fa',
            'end'     => '123.fa',
            'time'    => 5,
            'less'    => 100,
            'more'    => 50,
        }
    );

=cut

sub processFastaDirectory
{
    my ($fasta_directory, $parameters, $fasta_list) = @_;

    # check arguments
    if (!$fasta_directory)
    {
        confess "ERROR: processFastaDirectory: no directory to process provided!\n";
    }

    if ($parameters && ('HASH' ne ref($parameters)))
    {
        confess "ERROR: processFastaDirectory: invalid 'parameters' argument (not a hash ref)!\n";
    }
    if (!$parameters)
    {
        $parameters = {};
    }
    if ($fasta_list && ('ARRAY' ne ref($fasta_list)))
    {
        confess "ERROR: processFastaDirectory: invalid 'fasta_list' argument (not an array ref)!\n";
    }

    # make sure we got only one trailing slash
    $fasta_directory =~s/\/+$//g;
    $fasta_directory .= '/';

    if (!-e $fasta_directory)
    {
        confess "ERROR: directory '$fasta_directory' not found!\n";
    }
    elsif (!-d $fasta_directory)
    {
        confess "ERROR: '$fasta_directory' is not a directory!\n";
    }
    elsif (!-r $fasta_directory)
    {
        confess "ERROR: directory '$fasta_directory' is not readable!\n";
    }

    my @list;

    if ($fasta_list && @$fasta_list)
    {
        foreach my $fasta_filename (@$fasta_list)
        {
            # remove full path if specified by error
            $fasta_filename =~ s/^\Q$fasta_directory\E//;
            push(@list, $fasta_directory . $fasta_filename);
        }
    }
    else
    {
        @list = glob($fasta_directory . '*' . $FASTA_FILE_EXT);
    }

    # sort list
    if ($parameters->{'reverse'})
    {
        @list = reverse sort @list;
    }
    else
    {
        @list = sort @list;
    }

    # process file name restrictions
    if (defined($parameters->{'start'}))
    {
        # find first file to process and remove previous files
        while (@list && ($list[0] !~ m/$parameters->{'start'}$/))
        {shift(@list);}
    }
    if (defined($parameters->{'end'}))
    {
        # find last file to process and remove following files
        while (@list && ($list[$#list] !~ m/$parameters->{'end'}$/))
        {pop(@list);}
    }

    my $computation_start_date = time();
    my $processed_fasta_count = 0;
    print "FASTA directory: $fasta_directory\nProcessing " . scalar(@list) . " files...\n";
    print {$g_log_fh} "FASTA directory: $fasta_directory\nFiles count: " . scalar(@list) . "\n\nProcessing files:\n";
    print "Progress: 0%";
FASTA_FILES_LOOP:
    foreach my $fasta_file_path (@list)
    {
        processFastaFile($fasta_file_path, $parameters);
        # print progress
        my $computation_duration = time() - $computation_start_date;
        my $progress_percent = 100. * ++$processed_fasta_count / scalar(@list);
        my $remaining_time = $progress_percent ? '(' . $processed_fasta_count . '/' . scalar(@list) .  ' ' . getDuration((100 - $progress_percent) * $computation_duration / $progress_percent) . ' probably remaining)' : '';
        printf("\rProgress: %.1f%% %s                    ", $progress_percent, $remaining_time);
        # check time limitations
        if (defined($parameters->{'time'})
            && (((time() - $g_start_date)/60) >= $parameters->{'time'}))
        {
            print "\nLast processed file: $fasta_file_path";
            last FASTA_FILES_LOOP;
        }
    }
    print "\n...done!\n";

    return;
}


=pod

=head2 processFastaFile

B<Description>: Process the specified FASTA files according to the provided
limitations.

B<ArgsCount>: 1-2

=over 4

=item arg: $fasta_file_path (string) (R)

Full path to the FASTA file to process.

=item arg: $parameters (hash ref) (O)

Additional limitations that can restrict file processing can be specified using
this hash.
Available restrictions are:
'output_dir' => (string) path of the output directory to use (instead of the
    FASTA directory as default);
'exact' => (integer) exact number of sequence the FASTA should contain in order to
    be processed;
'less' => (integer) only FASTA with less than the specified number of sequence
    will be processed;
'more' => (integer) only FASTA with more than the specified number of sequence
    will be processed;
'no' => (string 'meme' or 'mast') will not execute MEME or MAST process;
'resume' => skip MEME or MAST step when output directory or files are present.

=back

B<Return>: nothing

B<Example>:

    # this will only process '234.fa' FASTA file if it has exactly 40 sequences.
    processFastaFile('/data/fasta/234.fa',
        {
            'exact' => 40,
        }
    );

=cut

sub processFastaFile
{
    my ($fasta_file_path, $parameters) = @_;

    # check arguments
    if (!$fasta_file_path)
    {
        confess "ERROR: processFastaFile: no FASTA file path provided!\n";
    }
    elsif (!-e $fasta_file_path)
    {
        confess "ERROR: processFastaFile: FASTA file '$fasta_file_path' not found!\n";
    }
    elsif (!-r $fasta_file_path)
    {
        confess "ERROR: processFastaFile: Unable to read FASTA file '$fasta_file_path'!\n";
    }

    if ($parameters && ('HASH' ne ref($parameters)))
    {
        confess "ERROR: processFastaFile: invalid 'parameters' argument (not a hash ref)!\n";
    }
    if (!$parameters)
    {
        $parameters = {};
    }

    my ($filename, $directory) = fileparse($fasta_file_path);

    print {$g_log_fh} "-$filename\n";

    # check limitations
    if (defined($parameters->{'exact'})
        || defined($parameters->{'less'})
        || defined($parameters->{'more'}))
    {
        my $seq_count = count_sequences_in_fasta($fasta_file_path);
        if (defined($parameters->{'exact'})
            && ($seq_count != $parameters->{'exact'}))
        {
            print {$g_log_fh} "    skipped! ($seq_count not matching 'exact' limitation)\n";
            return;
        }
        elsif ((defined($parameters->{'less'}))
            && ($seq_count >= $parameters->{'less'}))
        {
            print {$g_log_fh} "    skipped! ($seq_count not matching 'less' limitation)\n";
            return;
        }
        elsif ((defined($parameters->{'more'}))
            && ($seq_count <= $parameters->{'more'}))
        {
            print {$g_log_fh} "    skipped! ($seq_count not matching 'more' limitation)\n";
            return;
        }
        print {$g_log_fh} "    $seq_count sequences\n";
    }

    # check/set lock file
    my $lock_base_name = $fasta_file_path;
    $lock_base_name =~ s/$FASTA_FILE_EXT$//;
    # init lock manager:
    # -stale => 1: detect stale locks and break them if necessary
    # -hold  => 0: max life time (sec) of a lock. 0 prevents any forced unlocking.
    my $lockmgr = LockFile::Simple->make('-stale' => 1, '-hold' => 0);
    # try to lock and skip if already locked
    if (!$lockmgr->trylock($lock_base_name))
    {
        # lock file exists
        print {$g_log_fh} "    skipped! (locked by another process)\n";
        return;
    }
    print {$g_log_fh} "    Lock FASTA\n";

    # output directory
    my $output_dir = $directory;
    if ($parameters->{'output_dir'})
    {
        $output_dir = $parameters->{'output_dir'};
        $output_dir =~ s/\/+$//;
        $output_dir .= '/';
        if (!-e $output_dir)
        {
            mkdir($output_dir, 0776);
        }
    }

    my $local_start_date = time();

    my ($job_id, $meme_html_filename, $meme_log_filepath, $success, $meme_html_path);
    $job_id = 'n/a';
    $meme_log_filepath = 'n/a'; # no log
    # compute expected MEME HTML file name
    $meme_html_path = $output_dir . $filename; # use FASTA file name as base
    $meme_html_path =~ s/\Q$FASTA_FILE_EXT\E$//oi; # remove the .fa extension
    $meme_html_filename = $meme_html_path . "/$MEME_HTML_FILE_PREFIX$HTML_FILE_EXT"; # append slash and the HTML file name
    if (defined($parameters->{'no'}) && ('meme' eq $parameters->{'no'}))
    {
        # skip MEME step
        print {$g_log_fh} "    MEME step skipped! (no MEME)\n    MEME HTML: $meme_html_filename\n";
        # wait for Meme job to end
        $success = 1;
    }
    elsif (($parameters->{'resume'})
           && (-d $meme_html_path)
           && (-e $meme_html_filename)
           && (!-z $meme_html_filename))
    {
        # MEME already executed, skip step
        print {$g_log_fh} "    MEME step skipped! (resume)\n    MEME HTML: $meme_html_filename\n";
        # wait for Meme job to end
        $success = 1;
    }
    else
    {
        # run MEME
        ($job_id, $meme_html_filename, $meme_log_filepath) = runMeme($directory, $filename, $output_dir);
        print {$g_log_fh} "    MEME SGE Job ID: $job_id\n    MEME HTML: $meme_html_filename\n    MEME log: $meme_log_filepath\n";
        # wait for Meme job to end
        $success = waitSGEResult($job_id);
    }

    if (0 >= $success)
    {
        warn "ERROR: MEME execution failed for file '$filename'!\nSee '$meme_log_filepath' for details.\n";
        print {$g_log_fh} "    FAILED! (MEME)\n";
    }
    elsif (!-e $meme_html_filename)
    {
        warn "ERROR: MEME output HTML file '$meme_html_filename' not found for file '$filename'!\nSee '$meme_log_filepath' for details.\n";
        print {$g_log_fh} "    FAILED! (no MEME HTML)\n";
    }
    elsif (defined($parameters->{'no'}) && ('mast' eq $parameters->{'no'}))
    {
        # skip MAST step
        print {$g_log_fh} "    MAST step skipped! (limitation)\n";
    }
    else
    {
        if (!defined($parameters->{'no'}) || ('meme' ne $parameters->{'no'}))
        {
            # save MEME duration for current FASTA in log file
            print {$g_log_fh} "    MEME: " . getDuration(time() - $local_start_date) . "\n";
        }

        # check if MAST has already been executed
        my ($basename) = ($filename =~ m/^(.*)\Q$FASTA_FILE_EXT\E$/o);
        my ($mast_job_id, $mast_html_filename, $mast_log_filepath);
        $mast_html_filename = "$output_dir$basename/$MAST_HTML_FILE_PREFIX$HTML_FILE_EXT";
        if (($parameters->{'resume'}) && (-e $mast_html_filename))
        {
            print {$g_log_fh} "    MAST step skipped! (resume)\n    MAST HTML: $mast_html_filename\n";
        }
        else
        {

            # run MAST
            ($mast_job_id, $mast_html_filename, $mast_log_filepath) = runMast($directory, $filename, $meme_html_filename, $output_dir);
            print {$g_log_fh} "    MAST SGE Job ID: $job_id\n    MAST HTML: $mast_html_filename\n    MAST log: $mast_log_filepath\n";

            # wait for MAST job to end
            $success = waitSGEResult($mast_job_id);
            if (0 >= $success)
            {
                warn "ERROR: MAST execution failed for file '$filename'!\nSee '$mast_log_filepath' for details.\n";
                print {$g_log_fh} "    FAILED! (MAST)\n";
            }
            elsif (!-e $mast_html_filename)
            {
                warn "ERROR: MAST output HTML file '$mast_html_filename' not found for file '$filename'!\nSee '$mast_log_filepath' for details.\n";
                print {$g_log_fh} "    FAILED! (no MAST HTML)\n";
            }
            else
            {
                print {$g_log_fh} "    OK.\n";
            }
        }
    }

    # release lock
    $lockmgr->unlock($lock_base_name);
    print {$g_log_fh} "    Lock released\n";

    # save MEME/MAST duration for current FASTA in log file
    print {$g_log_fh} "    MEME+MAST: " . getDuration(time() - $local_start_date) . "\n";

    return;
}


=pod

=head2 runMeme

B<Description>: Launch MEME on the specified FASTA file on SGE.
Note: the function will return before the job has finished. Use
waitSGEResult(job_id) in order to make sure the SGE job has ended.

B<ArgsCount>: 3

=over 4

=item arg: $directory (string) (R)

Full directory path to the FASTA file to process.
It must include the trailing '/'.

=item arg: $filename (string) (R)

File name of the FASTA file to process.

=item arg: $output_dir (string) (R)

Output directory.
It must include the trailing '/'.

=back

B<Return>: (list)

job_id: ID of the SGE job launched;
html_output_filepath: full path to the output HTML file.
err_log_filepath: full path to the error log file.

B<Example>:

    my ($job_id, $html_output_filepath, $err_log_filepath) = runMeme('/data/fasta/', '234.fa', '/data/fasta/');

=cut

sub runMeme
{
    my ($directory, $filename, $output_dir) = @_;

    # remove FASTA extension (\Q--\E pair used to escape chars such as '.' in the regexp)
    my ($basename) = ($filename =~ m/^(.*)\Q$FASTA_FILE_EXT\E$/o);
    if ($g_debug)
    {
        print "DEBUG:\n   directory='$directory'\n   filename='$filename'\n   basename='$basename'\n    output_dir=$output_dir\n";
    }

    if (!-e $directory . $filename)
    {
        confess "ERROR: FASTA file '$directory$filename' does not exist!\n";
    }

    # get file size
    my $maxsize = -s $directory . $filename;
    # set maximum dataset size in characters
    $maxsize += 5000;

    my $log_filepath     = $output_dir . $basename . $MEME_LOG_EXT;
    my $err_log_filepath = $output_dir . $basename . $MEME_ERR_LOG_EXT;
    my $meme_cmd    = "$Greenphyl::Config::MEME_COMMAND $directory$filename -oc $output_dir$basename -mod oops -nmotifs 10 -maxw 50 -maxsize $maxsize -nostatus -evt 0.01 -p $g_num_processor";
    my $sge_cmd     = "$Greenphyl::Config::QSUB_COMMAND " . sprintf($SGE_ARGS_MEME, $g_num_processor) . " -o $log_filepath -e $err_log_filepath $meme_cmd";

    if ($g_debug)
    {
        print "DEBUG: SGE COMMAND (MEME):\n$sge_cmd\n";
    }

    my $sge_output = `$sge_cmd` or confess "ERROR: An error occured during MEME execution:\n$!\n";

    # get job ID
    my ($job_id) = ($sge_output =~ m/(\d+)/);
    if ($g_debug)
    {
        print "DEBUG: Job ID:$job_id\n";
        print "DEBUG: SGE output:\n$sge_output\n\n";
    }

    return ($job_id, "$output_dir$basename/$MEME_HTML_FILE_PREFIX$HTML_FILE_EXT", $log_filepath);
}


=pod

=head2 runMast

B<Description>: Launch MAST on the specified FASTA file on SGE.
Note: the function will return before the job has finished. Use
waitSGEResult(job_id) in order to make sure the SGE job has ended.

B<ArgsCount>: 4

=over 4

=item arg: $directory (string) (R)

Full directory path to the FASTA file to process.
It must include the trailing '/'.

=item arg: $filename (string) (R)

File name of the FASTA file to process.

=item arg: $html_filename (string) (R)

File name of the HTML output file provided by MEME.

=item arg: $output_dir (string) (R)

Output directory.
It must include the trailing '/'.

=back

B<Return>: (list)

job_id: ID of the SGE job launched;
html_output_filepath: relative path (from where the FASTA file is) to the output
    HTML file.
err_log_filepath: full path to the error log file.

B<Example>:

    my ($job_id, $html_output_filepath, $err_log_filepath) = runMast('/data/fasta/', '234.fa', '234/meme.html', '/data/fasta/');

=cut

sub runMast
{
    my ($directory, $filename, $html_filename, $output_dir) = @_;

    # remove FASTA extension (\Q--\E pair used to escape chars such as '.' in the regexp)
    my ($basename) = ($filename =~ m/^(.*)\Q$FASTA_FILE_EXT\E$/o);

    if ($g_debug)
    {
        print "DEBUG:\n   directory='$directory'\n   filename='$filename'\n   basename='$basename'\n   html_filename='$html_filename'\n    output_dir=$output_dir\n";
    }

    # [-d <database>] search sequences in <database> with motifs
    # [-b] print only section II and II
    # [-nostatus] do not print progress report
    # [-stdout] print output to standard output instead of file

    my $output_html_filename = "$output_dir$basename/$MAST_HTML_FILE_PREFIX$HTML_FILE_EXT";
    my $err_log_filepath     = $output_dir . $basename . $MAST_ERR_LOG_EXT;
    if (!-e $output_dir . $basename)
    {
        mkdir($output_dir . $basename, 0776);
    }
    my $mast_cmd = "$Greenphyl::Config::MAST_COMMAND $html_filename -d $directory$filename -nostatus -stdout";
    my $sge_cmd  = "$Greenphyl::Config::QSUB_COMMAND $SGE_ARGS_MAST -o $output_html_filename -e $err_log_filepath $mast_cmd";

    if ($g_debug)
    {
        print "DEBUG: SGE COMMAND (MAST):\n$sge_cmd\n";
    }

    my $sge_output = `$sge_cmd` or confess "ERROR: An error occured during MAST execution:\n$!\n";

    # get job ID
    my ($job_id) = ($sge_output =~ m/(\d+)/);
    if ($g_debug)
    {
        print "DEBUG: Job ID:$job_id\n";
        print "DEBUG: SGE output:\n$sge_output\n\n";
    }

    return ($job_id, $output_html_filename, $err_log_filepath);
}




# Script options
#################

=pod

=head1 OPTIONS

    run_meme.pl [-help] [-man] [-debug] [-output PATH] [-p <NB_PROC>] [-noreverse|-reverse] [-overwrite] [-limitations STRING [STRING] ...] <-dir DIRECTORY | -fid FILENAME>

=head2 Parameters

=over 4

=item B<-help>:

display this help message.

=item B<-man>:

display user manual.

=item B<-debug>:

set debug mode on.

=item B<-dir DIRECTORY>:

DIRECTORY is the full path to the directory containing the FASTA files to
process. (Only files with the $FASTA_FILE_EXT file extension will be processed)

=item B<-fid FILENAME>:

FILENAME is the full path to the FASTA files to process.

=item B<-output PATH>:

Use PATH as output directory for logs and generated files. By default, the FASTA
directory is used for output.

=item B<-p NB_PROC>:

Used to specify the number of processor to use with MEME. Default: 16.

=item B<-noreverse|-reverse>:

By default '-reverse' is on and files are sorted and processed in reverse
alphabetical order. When '-noreverse' is used, files will be sorted and
processed in alphabetical order.

=item B<-overwrite>:

By default, if MEME output files exist for a family, the MEME step for that
family is skipped and if MAST HTML output file is found for a family, the MAST
step for that family is skipped. To force these step execution, use the
"-overwrite" parameter.

=item B<-limitations STRING>:

This parameter can be used several times and the limitations will be cumulated.
STRING is a limitation string of the form:
1) "file1.fa-file2.fa": 'file1.fa' will be the first file processed and 'file2.fa'
    will be the last one;
2) "start=file1.fa": 'file1.fa' will be the first file processed;
3) "end=file2.fa": 'file2.fa' will be the last file processed;
4) "time=12": remaining FASTA files wont be processed after that delay in
    minutes (here 12 minutes);
5) "exact=50": only FASTA files containing 50 sequences will be processed;
6) "less=50": only FASTA files with less than 50 sequences will be processed;
7) "more=50": only FASTA files with more than 50 sequences will be processed;
8) "no=meme": will not execute MEME process;
9) "no=mast": will not execute MAST process;
10) "fasta_file.fa": a name of a FASTA file to process.

=back

=head2 Examples

    # will only process '1234.fa' and only do MAST analysis
    run_meme.pl -fid /data/meme/1234.fa -limitations no=meme

    # start from '1234.fa' and then will process '1235.fa', '1236.fa', ...
    run_meme.pl -dir /data/meme -limitations start=1234.fa -noreverse

    # will process files between '1234.fa' and '5678.fa'
    run_meme.pl -dir /data/meme -limitations start=5678.fa end=1234.fa

    # will process files down to '5678.fa' which have less than 75 sequences
    run_meme.pl -dir /data/meme -limitations end=5678.fa less=75

    # will only process files '2345.fa', '3456.fa' and '4444.fa'
    run_meme.pl -dir /data/meme -limitations 2345.fa 3456.fa 4444.fa

    # will process MEME analyses on files between '1234.fa' and '5678.fa'
    # will only process files containing between 51 and 79 sequences
    # and will not process unprocessed files after 5min
    run_meme.pl -d /data/meme -l no=mast 5678.fa-1234.fa more=50 less=80 time=5

=cut

# CODE START
#############

print "GreenPhyl MEME/MAST Execution v$RUN_MEME_VERSION\n\n";

my ($help, $man, $fasta_directory , $fasta_file_path, @limitations, $overwrite,
    $reverse_order, $log_dir, @fasta_files, $output_dir, $nb_proc);

$reverse_order = 1; # by default: reverse order

GetOptions(
    'help|?'             => \$help,
    'man'                => \$man,
    'debug'              => \$g_debug,
    'dir|d=s'            => \$fasta_directory,
    'fid|f=s'            => \$fasta_file_path,
    'output|o=s'         => \$output_dir,
    'reverse!'           => \$reverse_order,
    'overwrite|w'        => \$overwrite,
    'limitations|l=s{,}' => \@limitations,
    'p=i'                => \$nb_proc,
) or pod2usage(2);
if ($help) { pod2usage(1); }
if ($man) { pod2usage( -verbose => 2 ); }

$g_debug ||= $DEBUG;

# parameters check
if (!$fasta_directory && !$fasta_file_path)
{
    print "ERROR: no file or directory to process!\n\n";
    pod2usage(2);
}

if ($nb_proc)
{
    if (0 < int($nb_proc))
    {
        $g_num_processor = int($nb_proc);
    }
    else
    {
        warn "WARNING: invalid value for \"-p\" parameter! Ignored.\n";
    }
}

my ($single_filename, $directory);
if ($fasta_file_path)
{
    ($single_filename, $directory) = fileparse($fasta_file_path);
}
$output_dir = $output_dir || $fasta_directory || $directory || '';
if ($output_dir && (!-e $output_dir))
{
    mkdir($output_dir, 0776);
}
if ($output_dir)
{
    $output_dir =~ s/\/+$//;
    $output_dir .= '/';
}

$log_dir = $output_dir || '';

my $report_filename = getLogFilename($Greenphyl::Config::MEME_MAST_LOG_FILE);
open($g_log_fh, ">>$log_dir$report_filename") or confess("ERROR: Failed to open log file '$log_dir$report_filename' for writting!\n$!\n");
print {$g_log_fh} "GreenPhyl MEME/MAST Execution v$RUN_MEME_VERSION\nDate: " . getCurrentDate() . "\n\n";

print "Log file: '$log_dir$report_filename'\n";


# process options
my %process_param = ('output_dir' => $output_dir, 'reverse' => ($reverse_order?1:0), 'resume' => ($overwrite?0:1));
print {$g_log_fh} "Reverse order: " . $process_param{'reverse'} . "\n";
foreach my $limitation (@limitations)
{
    print {$g_log_fh} "Limitation: $limitation\n";
    # find limitation type
    if ($limitation =~ m/^([^-]+)-([^-]+)$/)
    {
        # limitation "start_filename"-"end_filename"
        $process_param{'start'} = $1;
        $process_param{'end'}   = $2;
        print "Will process files between (and including) '$1' and '$2'\n";
    }
    elsif ($limitation =~ m/^start=(.*)$/i)
    {
        # limitation start="start_filename"
        $process_param{'start'} = $1;
        print "Will start from file '$1'\n";
    }
    elsif ($limitation =~ m/^end=(.*)$/i)
    {
        # limitation end="end_filename"
        $process_param{'end'} = $1;
        print "Will end at file '$1'\n";
    }
    elsif ($limitation =~ m/^time=(.*)$/i)
    {
        # limitation time="duration"
        $process_param{'time'} = $1;
        print "Will stop processing new FASTA after $1 minutes\n";
    }
    elsif ($limitation =~ m/^exact=(\d+)$/i)
    {
        # limitation exact="num_seq"
        $process_param{'exact'} = $1;
        print "Will only process FASTA files with $1 sequences\n";
    }
    elsif ($limitation =~ m/^less=(\d+)$/i)
    {
        # limitation less="max_num_seq+1"
        $process_param{'less'} = $1;
        print "Will only process FASTA files with less than $1 sequences\n";
    }
    elsif ($limitation =~ m/^more=(\d+)$/i)
    {
        # limitation more="min_num_seq-1"
        $process_param{'more'} = $1;
        print "Will only process FASTA files with more than $1 sequences\n";
    }
    elsif ($limitation =~ m/^no=(meme|mast)$/i)
    {
        # limitation more="min_num_seq-1"
        $process_param{'no'} = lc($1);
        print "Will not execute $1 process\n";
    }
    elsif ($limitation =~ m/^(.*\Q$FASTA_FILE_EXT\E)$/i)
    {
        # limitation fasta_file.fa
        push(@fasta_files, $1);
        print "Will process FASTA file $1\n";
    }
}



$g_start_date = time();

# method selection
if ($fasta_directory)
{
    # all files in 1 directory
    processFastaDirectory($fasta_directory, \%process_param, \@fasta_files);
}
elsif ($fasta_file_path)
{
    # only one file
    print "Processing FASTA file '$fasta_file_path'...\n";
    print {$g_log_fh} "FASTA file: $fasta_file_path\n";
    processFastaFile($fasta_file_path, \%process_param);
    print "...done!\n";
}

print {$g_log_fh} "Done!\n";

print "Duration: " . getDuration(time() - $g_start_date) . "\n";
print {$g_log_fh} "Duration: " . getDuration(time() - $g_start_date) . "\n";

close($g_log_fh);

exit(0);


# CODE END
###########

=pod

=head1 DIAGNOSTICS


=head1 AUTHORS

Mathieu ROUARD (Bioversity-France), m.rouard@cgiar.org

Valentin GUIGNON (Bioversity-France), v.guignon@cgiar.org

Matthieu CONTE

Christian WALDE

Christelle ALUOME

=head1 VERSION

Version 2.0.x

$Id$

=head1 SEE ALSO

GreenPhyl documentation

=cut
