#!/usr/bin/perl

=pod

=head1 NAME

cp_to_web.pl - Copy some GreenPhyl output files to the web directory

=head1 SYNOPSIS

    cp_to_web.pl

=head1 REQUIRES

Perl5, mySQL

=head1 DESCRIPTION

Copy some GreenPhyl output files to the web directory.

=cut

use lib '../../lib';

use strict;
use warnings;
use Carp qw (cluck confess croak);
use Readonly;

use Getopt::Long;
use Pod::Usage;
use Error qw(:try);
use File::Temp qw/ tempfile tempdir /;
 
use Greenphyl::Config;
use Greenphyl::Log;
use Greenphyl::Utils;

++$|;

# Script global constants
##########################

=pod

=head1 CONSTANTS


B<$DEFAULT_WEB_PATH>: (string)

path of the website root without ending slash '/'.

B<$DEFAULT_PIPELINE_OUTPUT_DIRECTORY>: (string)

path to GreenPhyl pipeline output without ending slash '/'.

=cut

my $DEFAULT_PIPELINE_OUTPUT_DIRECTORY = '/userhome/u578998/output';
my $ALIGNMENT_EXTENSION               = '.fltr.fasta.aln';
my $PHYLOGENY_XML_TREE_EXTENSION      = $Greenphyl::Config::PHYLO_XML_TREE_FILE_SUFFIX;
my $PHYLOGENY_NEWICK_TREE_EXTENSION   = $Greenphyl::Config::PHYLO_NEWICK_TREE_FILE_SUFFIX;

my %PIPELINE_FILE_EXT_TO_COPY = (
    $ALIGNMENT_EXTENSION      => $Greenphyl::Config::MULTI_ALIGN_PATH,
    '_filtered.html'          => $Greenphyl::Config::MULTI_ALIGN_PATH,
    '_filtered.seq'           => $Greenphyl::Config::MULTI_ALIGN_PATH,
    '.1_masking.html'         => $Greenphyl::Config::MULTI_ALIGN_PATH,
    '.2_masking.html'         => $Greenphyl::Config::MULTI_ALIGN_PATH,
    '_rap_stats_tree.txt'     => $Greenphyl::Config::TREES_PATH,
    $PHYLOGENY_NEWICK_TREE_EXTENSION => $Greenphyl::Config::TREES_PATH,
    $PHYLOGENY_XML_TREE_EXTENSION    => $Greenphyl::Config::TREES_PATH,
    # MEME_OUTPUT_PATH
    # BBMH_PATH
);

my %MEME_FILE_EXT_TO_COPY = (
    '.html'           => '$Greenphyl::Config::MEME_OUTPUT_PATH',
    # BBMH_PATH
);




# Script global variables
##########################

=pod

=head1 VARIABLES

B<$g_debug>: (boolean)

debug mode flag. Set to 1 when "-debug" command line parameter is used.
Default: false

B<$g_verbose>: (boolean)

verbose mode. Display processed families and files.
Default: false

=cut

my $g_debug;
my $g_verbose;
my (@g_transfered_files,
    @g_not_transfered_files,
    @g_transfered_families,
    @g_partially_g_transfered_families,
    @g_unprocessed_families,
    %g_available_files,
    %g_not_utd_families);



=pod

=head1 FUNCTIONS

=head2 transferFile

B<Description>: transfer a file from pipeline output directory to web directory.

B<ArgsCount>: 4

=over 1

=item $output_filename: (string) (R)

pipeline output file name.

=item $target_path: (string) (R)

Target web directory.

=back

B<Return>: (boolean)
ture value if the file was transfered, false otherwise.

B<Example>:

    transferFile($output_filename, $target_path);

=cut

sub transferFile
{
    my ($output_filename, $target_path) = @_;

    if ((-r $output_filename) && (!-z $output_filename))
    {
        my $cmd = "cp -p $output_filename $target_path";
        if ($g_debug)
        {
            print "DEBUG: COMMAND: $cmd\n";
        }
        if (system($cmd))
        {
            cluck "WARNING: Failed to transfer file '$output_filename':\n$!\n";
            push(@g_not_transfered_files, $output_filename);
            return 0;
        }
        else
        {
            if ($g_verbose)
            {
                print "-'$output_filename' ok\n";
            }
            push(@g_transfered_files, $output_filename);
            return 1;
        }
    }
    elsif (-z $output_filename)
    {
        warn "WARNING: File '$output_filename' is empty and has been skipped!\n";
        push(@g_not_transfered_files, $output_filename);
        return 0;
    }
    else
    {
        push(@g_not_transfered_files, $output_filename);
        return 0;
    }
}


=pod

=head2 transferFamilyFiles

B<Description>: transfer family files from pipeline output directory to web directory.

B<ArgsCount>: 4

=over 1

=item $family_id: (string) (R)

Family name (ID).

=item $family_directory: (string) (R)

Directory path to family results.

=back

B<Return>: nothing

B<Example>:

    transferFamilyFiles($family_id, $family_directory);

=cut

sub transferFamilyFiles
{
    my ($family_id, $family_directory) = @_;

    my $untransfered_file = 0;
    foreach my $file_to_copy (keys(%PIPELINE_FILE_EXT_TO_COPY))
    {
        my $output_filename = "$family_directory/$family_id$file_to_copy";
        my $target_path    = "$PIPELINE_FILE_EXT_TO_COPY{$file_to_copy}/";
        if (!transferFile($output_filename, $target_path))
        {
            ++$untransfered_file;
        }
    }
    if ($untransfered_file)
    {
        push(@g_partially_g_transfered_families, $family_id);
    }
    else
    {
        push(@g_transfered_families, $family_id);
    }

}


=pod

=head2 findAvailableFiles

B<Description>: find files available on web directory and fills
%g_available_files hash.

B<ArgsCount>: 0

B<Return>: nothing

B<Example>:

    findAvailableFiles();

=cut

sub findAvailableFiles
{
    my %processed_directories;
    foreach my $file_to_copy (keys(%PIPELINE_FILE_EXT_TO_COPY))
    {
        if (!exists($processed_directories{$PIPELINE_FILE_EXT_TO_COPY{$file_to_copy}}))
        {
            $processed_directories{$PIPELINE_FILE_EXT_TO_COPY{$file_to_copy}} = 1;
            my $cmd = "ls -plog --time=atime --time-style=long-iso $PIPELINE_FILE_EXT_TO_COPY{$file_to_copy}/"; # note: STDERR is redirected when error checking below
            if ($g_debug)
            {
                print "DEBUG: COMMAND: $cmd\n";
            }
            if (my $files_list = `$cmd`)
            {
                # print "DEBUG files list:\n$files_list\n"; #+debug 
                foreach my $line (split(/\n/, $files_list))
                {
                    # extract info (only files at least r/w by owner)
                    # ex.: "-rw-r--r-- 1 282475 2011-07-07 12:38 musa_all.txt"
                    my ($is_file, $file_size, $file_date, $file_name) = ($line =~ m/^(\-)rw[\w\-]+\s+\d\s+(\d+)\s+(\d\d\d\d-\d\d-\d\d \d\d:\d\d)\s+(.*)$/);
                    if ($is_file)
                    {
                        # keep track of files found
                        $g_available_files{$file_name} = {'name' => $file_name, 'size' => $file_size, 'date' => $file_date, };
                    }
                }
            }
            else
            {
                # redirect error in case of error
                $files_list = `$cmd 2>\&1` || '';
                cluck "WARNING: Failed to list available files in '$PIPELINE_FILE_EXT_TO_COPY{$file_to_copy}/'!\n$!\n$files_list\n";
            }
        }
    }
}


=pod

=head2 analyseTransferedFamily

B<Description>: only gather statistics on transfered family files.

B<ArgsCount>: 4

=over 1

=item $family_id: (string) (R)

Family name (ID).

=item $family_directory: (string) (R)

Directory path to family results.

=back

B<Return>: nothing

B<Example>:

    analyseTransferedFamily($family_id, $family_directory);

=cut

sub analyseTransferedFamily
{
    my ($family_id, $family_directory) = @_;

    my $untransfered_file = 0;
    foreach my $file_to_copy (keys(%PIPELINE_FILE_EXT_TO_COPY))
    {
        my $output_filename = "$family_directory/$family_id$file_to_copy";
        if (exists($g_available_files{$family_id . $file_to_copy}))
        {
            if (-r $output_filename)
            {
                my ($dev, $ino, $mode, $nlink, $uid, $gid, $rdev, $size,
                    $atime, $mtime, $ctime, $blksize, $blocks)
                    = stat($output_filename);
                my ($sec, $min, $hour, $mday, $month, $year, $wday, $yday, $isdst) = localtime($atime);
                my $ldate = sprintf('%04d-%02d-%02d %02d:%02d', 1900+$year, 1+$month, $mday, $hour, $min);
                # check if file needs to be updated
                if (($size != $g_available_files{$family_id . $file_to_copy}->{'size'})
                    || ($ldate ne $g_available_files{$family_id . $file_to_copy}->{'date'}))
                {
                    if ($g_debug)
                    {
                        print "DEBUG: not up-to-date '$family_id$file_to_copy'\n";
                        print "output file size:  $size\n";
                        print "web directory file size: " . $g_available_files{$family_id . $file_to_copy}->{'size'} . "\n";
                        print "output file date:  $ldate\n";
                        print "web directory file date: " . $g_available_files{$family_id . $file_to_copy}->{'date'} . "\n";
                    }
                    $g_not_utd_families{$family_id} = 1;
                }
            }
            else
            {
                warn "WARNING: Missing output file '$output_filename' for family $family_id!\n";
                ++$untransfered_file;
            }
        }
        else
        {
            if ($g_debug)
            {
                print "DEBUG: missing on web directory: '$family_id$file_to_copy'\n"; #+debug
            }
            ++$untransfered_file;
        }
    }
    if ($untransfered_file)
    {
        push(@g_partially_g_transfered_families, $family_id);
    }
    else
    {
        push(@g_transfered_families, $family_id);
    }
}


=pod

=head2 transferMissingFamilyFiles

B<Description>: only transfer family files missing to web directory.

B<ArgsCount>: 4

=over 1

=item $family_id: (string) (R)

Family name (ID).

=item $family_directory: (string) (R)

Directory path to family results.

=back

B<Return>: nothing

B<Example>:

    transferMissingFamilyFiles($family_id, $family_directory);

=cut

sub transferMissingFamilyFiles
{
    my ($family_id, $family_directory) = @_;

    my $untransfered_file = 0;
    foreach my $file_to_copy (keys(%PIPELINE_FILE_EXT_TO_COPY))
    {
        if (!exists($g_available_files{$family_id . $file_to_copy}))
        {
            my $output_filename = "$family_directory/$family_id$file_to_copy";
            if (-r $output_filename)
            {
                my $target_path    = "$PIPELINE_FILE_EXT_TO_COPY{$file_to_copy}/";
                if ($g_debug)
                {
                    print "DEBUG: missing on web directory: '$output_filename'\n"; #+debug
                }
                if (!transferFile($output_filename, $target_path))
                {
                    ++$untransfered_file;
                }
            }
            else
            {
                warn "WARNING: Missing output file '$output_filename' for family $family_id!\n";
                ++$untransfered_file;
            }
        }
    }
    if ($untransfered_file)
    {
        push(@g_partially_g_transfered_families, $family_id);
    }
    else
    {
        push(@g_transfered_families, $family_id);
    }
}


=pod

=head2 transferUpdatedFamilyFiles

B<Description>: only transfer family files existing on web directory that need
to be updated.

B<ArgsCount>: 4

=over 1

=item $family_id: (string) (R)

Family name (ID).

=item $family_directory: (string) (R)

Directory path to family results.

=back

B<Return>: nothing

B<Example>:

    transferUpdatedFamilyFiles($family_id, $family_directory);

=cut

sub transferUpdatedFamilyFiles
{
    my ($family_id, $family_directory) = @_;

    my $untransfered_file = 0;
    foreach my $file_to_copy (keys(%PIPELINE_FILE_EXT_TO_COPY))
    {
        if (exists($g_available_files{$family_id . $file_to_copy}))
        {
            my $output_filename = "$family_directory/$family_id$file_to_copy";
            my $target_path    = "$PIPELINE_FILE_EXT_TO_COPY{$file_to_copy}/";
            my ($dev, $ino, $mode, $nlink, $uid, $gid, $rdev, $size,
                $atime, $mtime, $ctime, $blksize, $blocks)
                = stat($output_filename);
            my ($sec, $min, $hour, $mday, $month, $year, $wday, $yday, $isdst) = localtime($atime);
            my $ldate = sprintf('%04d-%02d-%02d %02d:%02d', 1900+$year, 1+$month, $mday, $hour, $min);
            # check if file needs to be updated
            if (($size != $g_available_files{$family_id . $file_to_copy}->{'size'})
                || ($ldate ne $g_available_files{$family_id . $file_to_copy}->{'date'}))
            {
                if ($g_debug)
                {
                    print "DEBUG: not up-to-date '$output_filename'\n";
                    print "output file size:  $size\n";
                    print "web directory file size: " . $g_available_files{$family_id . $file_to_copy}->{'size'} . "\n";
                    print "output file date:  $ldate\n";
                    print "web directory file date: " . $g_available_files{$family_id . $file_to_copy}->{'date'} . "\n";
                }
                if (!transferFile($output_filename, $target_path))
                {
                    ++$untransfered_file;
                }
                $g_not_utd_families{$family_id} = 1;
            }
        }
    }
    if ($untransfered_file)
    {
        push(@g_partially_g_transfered_families, $family_id);
    }
    else
    {
        push(@g_transfered_families, $family_id);
    }
}




# Script options
#################

=pod

=head1 OPTIONS

    trans_to_web.pl [-help | -man]
                    [-pipeline_output_dir <path_to_pipeline_output>]
                    [-missing_only | -update_only | -stats_only]
                    [-report_success] [-report_partial] [-report_not_utd]
                    [-report_unprocessed] [-report_failures]


=over 4

=item B<-help>:

Prints a brief help message and exits.

=item B<-man>:

Prints the manual page and exits.

=item B<-pipeline_output_dir>: (string)

Path to pipeline outputs (directory containing the family output
directories).
Default: $DEFAULT_PIPELINE_OUTPUT_DIRECTORY

=item B<-missing_only>: (boolean)

Only copy missing files.

=item B<-update_only>: (boolean)

Only copy updated files (different size or date).
Note: it does not copy missing files!

=item B<-stats_only>: (boolean)

Only display stats about transfered families (no transfer).

=back

=cut


# CODE START
#############

# options processing
my ($man, $help,
    $pipeline_output_dir, $missing_only, $update_only, $stats_only, $stats,
    $report_success, $report_partial, $report_not_utd, $report_unprocessed,
    $report_failures)
 = (   0,     0,          '',            '',          '',
                      '',             0,            0,           0,      1,
                  0,               0,               0,                   0,
                   0);

# parse options and print usage if there is a syntax error.
GetOptions("help|?"             => \$help,
           "man"                => \$man,
           "debug"              => \$g_debug,
           "verbose|v"          => \$g_verbose,
           "stats|s!"           => \$stats,
           "pipeline_output_dir=s" => \$pipeline_output_dir,
           "missing_only"       => \$missing_only,
           "update_only"        => \$update_only,
           "stats_only"         => \$stats_only,
           "report_success"     => \$report_success,
           "report_partial"     => \$report_partial,
           "report_not_utd"     => \$report_not_utd,
           "report_unprocessed" => \$report_unprocessed,
           "report_failures"    => \$report_failures,
) or pod2usage(2);
if ($help) {pod2usage(0);}
if ($man) {pod2usage(-verbose => 2);}

$pipeline_output_dir ||= $DEFAULT_PIPELINE_OUTPUT_DIRECTORY;

# remove trailing slash
$pipeline_output_dir =~ s/\/+$//;


# connect to database
my $dbh = connectToDatabase();
if ($dbh->{AutoCommit})
{
    $dbh->begin_work() or croak $dbh->errstr;
}

# get web directory files info if needed
if ($stats_only || $missing_only || $update_only)
{
    findAvailableFiles();
}

# select transfer procedure
my $transfer_procedures = \&transferFamilyFiles; # default
if ($stats_only)
{
    $transfer_procedures = \&analyseTransferedFamily;
}
elsif ($missing_only)
{
    $transfer_procedures = \&transferMissingFamilyFiles;
}
elsif ($update_only)
{
    $transfer_procedures = \&transferUpdatedFamilyFiles;
}


# process each family (not black-listed)
my $sql_query = "SELECT family_id FROM family WHERE black_list = 0;";
my @family_ids = @{$dbh->selectcol_arrayref($sql_query)};
foreach my $family_id (@family_ids)
{
    if ($g_verbose)
    {
        print "Family $family_id:\n";
    }
    # transfer pipeline results
    # -check if the output directory exists
    my $family_directory = $pipeline_output_dir . '/' . $family_id;
    if (-d $family_directory)
    {
        &$transfer_procedures($family_id, $family_directory);
    }
    else
    {
        # log: family not processed by the pipeline = skipped
        warn "WARNING: family '$family_id' not processed and skipped (path '$family_directory' not found)!\n";
        push(@g_unprocessed_families, $family_id);
    }

    #+TODO: meme/mast
}

$dbh->commit() or confess($dbh->errstr);

if ($stats)
{
    print "\n";
    print "Total database families:                " . scalar(@family_ids) . "\n";
    print "Total successfully transfered families: " . scalar(@g_transfered_families) . "\n";
    print "Total partially transfered families:    " . scalar(@g_partially_g_transfered_families) . "\n";
    print "Total not up-to-date families:          " . scalar(keys(%g_not_utd_families)) . "\n";
    print "Total unprocessed families:             " . scalar(@g_unprocessed_families) . "\n";
    print "Total transfered files:                 " . scalar(@g_transfered_files) . "\n";
    print "Total failed transfers:                 " . scalar(@g_not_transfered_files) . "\n";
    print "\n";
}

if ($report_success)
{
    print "Successfully transfered families:\n";
    print join("\n", @g_transfered_families);
    print "\n";
}
if ($report_partial)
{
    print "Partially transfered families:\n";
    print join("\n", @g_partially_g_transfered_families);
    print "\n";
}
if ($report_not_utd)
{
    print "Not up-to-date families:\n";
    print join("\n", keys(%g_not_utd_families));
    print "\n";
}
if ($report_unprocessed)
{
    print "Unprocessed families:\n";
    print join("\n", @g_unprocessed_families);
    print "\n";
}
if ($report_failures)
{
    print "File transfer failures:\n";
    print join("\n", @g_not_transfered_files);
    print "\n";
}

exit(0);

# CODE END
###########


=pod

=head1 AUTHORS

Valentin GUIGNON (Bioversity-France), v.guignon@cgiar.org
Mathieu ROUARD (Bioversity-France), m.rouard@cgiar.org
Matthieu CONTE (Syngenta)

=head1 VERSION

Version 1.0.0

Date 24/04/2012

=head1 SEE ALSO

GreenPhyl documentation.

=cut
