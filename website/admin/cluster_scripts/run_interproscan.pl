#!/usr/bin/perl

#/usr/local/bioinfo/iprscan/bin/iprscan -cli -i Bradi_1.0.pep.fa -o BRADI/ -goterms -iprlookup -seqtype p -format xml
use strict;
use warnings;
use Bio::SeqIO;
use Bio::SearchIO;

Interproscan($ARGV[0], $ARGV[1]);

sub Interproscan
{  
    my ( $file, $output_dir ) = @_;

    my $in = Bio::SeqIO->new(
        -file   => $file,
        -format => 'fasta'
    );

    my $count;
    while ( my $seq = $in->next_seq() ) 
    {
        $count++;
        my $id = $seq->display_id;

        $seq->{primary_id} = $id;
        my $input = "tmp/$id.fa";
        my $output = Bio::SeqIO->new
        (
               -file   => ">$input",
               -format => 'fasta'
        );
        $output->write_seq($seq);
         
       # run interposcan   
       my $cmd = "/usr/local/bioinfo/iprscan/bin/iprscan -cli -i $input  -o $output_dir$id.xml -goterms -iprlookup -seqtype p -format xml";
       print $cmd . "\n";
       if (! -e $output_dir.$id.xml)
       {
            system($cmd) and die "pb running interprscan";
       }
           
       system("rm $input") if -e $input;
    }   
    return;   
}
 