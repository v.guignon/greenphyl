#!/usr/bin/perl

=pod

=head1 NAME

run_ipr_statistics.pl - script used to update Greenphyl InterProScan statistics

=head1 SYNOPSIS

    run_ipr_statistics.pl

=head1 REQUIRES

Perl5, BioPerl, Greenphyl

=head1 DESCRIPTION

This script can be used to update InterProScan statistics into GreenPhyl
database.

=cut

use strict;
use warnings;

use lib '../lib';
use lib '../local_lib';

use lib '../../lib';
use lib '../../local_lib';


use Carp qw(cluck confess croak);
use Error qw(:try);
use LockFile::Simple qw(trylock  unlock);

#+... used by Christian for development...
BEGIN
{
    require Win32::Detached if ($^O eq 'MSWin32');
}
#...+

use Greenphyl::Config;
use Greenphyl::Log('nolog' => 1,);
use Greenphyl::Utils;
use Greenphyl::Run::Statistics;
use Greenphyl::ConnectMysql;


Greenphyl::Log::_ResetConfig();
OpenLog('logfile'         => 'ipr_statitics.log',
        'logpath'         => '.',
        'nodb'            => 1,
        'nofiletimestamp' => 1,
        'append'          => 0,
        'logtimestamp'    => 0,
);

# SVN management part
######################
my $REV_STRING = '$id$';


# Script global constants
##########################

=pod

=head1 CONSTANTS

B<$DEBUG>: (boolean)

If debug is set to a true value, debug messages will be displayed and
modification to database won't be committed.

B<$UPDATER_VERSION>: (string)

Version of the InterProScan Statistics updater.

=cut

my $DEBUG                    = 0;

my $UPDATER_VERSION          = '2.0.' . $REV_STRING;



# CODE START
#############

LogInfo("GreenPhyl InterProScan Statistics Updater v$UPDATER_VERSION");

my $start_date = time();

# check output log directory
if ((!$Greenphyl::Config::IPR_STATS_PATH) || (!-d $Greenphyl::Config::IPR_STATS_PATH) || (!-w $Greenphyl::Config::IPR_STATS_PATH))
{
    confess "ERROR: Missing output directory '$Greenphyl::Config::IPR_STATS_PATH'!\n";
}

my $working_lock_base_path = "$Greenphyl::Config::IPR_STATS_PATH/ipr_running";

my $mysql = Greenphyl::ConnectMysql->new();
my $dbh   = $mysql->DBconnect();

# start transaction
$dbh->begin_work() or croak $dbh->errstr;

# check if the updater is not currently working
my $lockmgr = LockFile::Simple->make( -stale => 1 );
if (!$lockmgr->trylock($working_lock_base_path))
{
    # already in use, abort this instance
    confess "NOTE: An InterProScan Statistics Updater is underway at the moment, no need to start it again!\n";
}

# update stats
LogInfo("Updating InterProScan statistics...");
my $error;
try
{
    my $iprstat = Greenphyl::Run::Statistics->new('dbh' => $dbh, 'mysql' => $mysql);
    $iprstat->iprSpecific();
    if ($DEBUG)
    {
        confess "Database commit aborted due to debug mode!\n";
    }
    $dbh->commit() or confess($dbh->errstr);
    LogInfo("Duration: " . getDuration(time() - $start_date));
}
otherwise
{
    $error = shift;
    LogInfo("Duration: " . getDuration(time() - $start_date));
    $dbh->rollback();
};

# disconnect from database
$mysql->disconnect();

# release lock
$lockmgr->unlock($working_lock_base_path);

# check if an error occured and report it
if ($error)
{
    confess $error;
}

LogInfo("Done!");

exit(0);


# CODE END
###########

=pod

=head1 DIAGNOSTICS

=head1 AUTHORS

Mathieu ROUARD (Bioversity-France), m.rouard@cgiar.org

Valentin GUIGNON (Bioversity-France), v.guignon@cgiar.org

Matthieu CONTE

Christian WALDE

Christelle ALUOME

=head1 VERSION

Version 2.0.x

$Id$

=head1 SEE ALSO

GreenPhyl documentation

=cut
