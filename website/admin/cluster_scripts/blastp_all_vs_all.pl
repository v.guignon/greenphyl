#!/usr/bin/perl -w 

use strict;
use File::Basename;

#my $BLAST_PARAM = " --program blastp -e 10e-5 -q greenphyl --max_target_seq 1 --format 6";
my $BLAST_PARAM = " --program blastp -e 1e-5 -q greenphyl --format 6";



	my ($BlastBankDir, $blastZValue) = (@ARGV);
	
	my $DIR_RESULTS = "$BlastBankDir/bbmh";
	
	if ( !defined($BlastBankDir) )
	{
		print STDERR "\nUSAGE:  $0  <BlastBankDir> [blastZValue]\n\n";
		exit(1);
	}
	
	my (@a_files) = `find $BlastBankDir -maxdepth 1 -type f`;
	chomp(@a_files);
	
	my (@a_fastaFiles) = ();
	foreach my $file (@a_files)
	{
		open(FILE, $file) || die "Cannot open file '$file'\n";
		my $line = <FILE>;
		close(FILE);
		if ( defined($line) && ($line=~/^\s*>/) )
		{	
			push(@a_fastaFiles, $file);   	  
			if ( ! -f "$file.pin" )
			{
				print STDERR "Running formatdb on file '$file' ... \n";
				`formatdb -i $file`;
				print STDERR "  done.\n";						
			}
		}
	}
    (@a_files) = ();




    
   `mkdir $DIR_RESULTS ` if ( ! -d $DIR_RESULTS );
	
	my (@a_blastCommands) = ();
	foreach my $fileA (@a_fastaFiles)
	{
		foreach my $fileB (@a_fastaFiles)
		{	
		    my ( $filenameA, $directoryA ) = fileparse($fileA);
		    my ( $filenameB, $directoryB ) = fileparse($fileB);
		    
		    #if ( $fileA ne $fileB )		    
			#{	    
		        #my ($targetDBName) = ($fileB =~ /\/([^\/]+)$/);
		        #my $blastOutput = "${fileA}_vs_$targetDBName";
		        
		       my $blastOutput = $filenameA . '_vs_' . $filenameB;
		       if ( (!-e $DIR_RESULTS .'/'. $blastOutput) || (-z $DIR_RESULTS .'/'. $blastOutput) )
			   {		   
			        my $cmd = "blast_cluster.pl $BLAST_PARAM --database $fileB --input $fileA -o $blastOutput --directory $DIR_RESULTS";
			        
			        $cmd .= " -z $blastZValue" if ( defined($blastZValue) );
			        push(@a_blastCommands, $cmd);
			    }
			    else
			    {
			        print STDERR "$blastOutput....skipped\n";
			    }			    
			#}
		}
	}
	
	print STDERR "\n" . join("\n", @a_blastCommands) . "\n\n";
	foreach my $cmd (@a_blastCommands)
	{
		`$cmd` or die $!;
	}
	



