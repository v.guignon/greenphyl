#!/usr/bin/perl

=pod

=head1 NAME

load_classification.pl - Loads family clustering and/or tranfer annotations

=head1 SYNOPSIS

    load_classification.pl <options> families.mcl 1
    
=head1 REQUIRES

Perl5

=head1 DESCRIPTION

Loads Markov Clustering of sequences as family into a GreenPhyl database and
transfer old annotations on these families from an older database for each
new family that matches an older family.

Families are matched using the following procedure:
1) only sequences of the new family (MCL) that are found in the old database
   are taken in account (name changes are taken in account);
2) the script finds the old family that contains the maximum of these sequences;
3) if the old family found has more than 60% of the sequences taken in account,
   then the annotation is transfered.

=cut

use strict;
use warnings;
use lib '../../lib';
use lib '../../local_lib';

use Getopt::Long;
use Pod::Usage;

use DBI;
use DBIx::Simple;
use Carp qw (cluck confess croak);
use Greenphyl::Config;
use Greenphyl::ConnectMysql;
use Greenphyl::Utils;
use Greenphyl::Log('nolog' => 1,);

Greenphyl::Log::_ResetConfig();
OpenLog('logfile'         => 'mcl_all.log',
        'logpath'         => '.',
        'nodb'            => 1,
        'nofiletimestamp' => 1,
        'append'          => 0,
        'logtimestamp'    => 0,
);

++$|;


# Script global constants
##########################

=pod

=head1 CONSTANTS

B<$DEBUG>: (boolean)

Default debug mode status (can be changed using command line).

B<$UNANNOTATED_CLUSTER_NAME>: (string)

Name to use for unannotated clusters.

B<$SECONDARY_DATABASE>: (hash)

Connection info of the old database containing annotations to transfer.

B<$MIN_SEQUENCE_PER_FAMILY>: (integer)

Minimal number of sequence a family must contain to be created.

=cut

my $DEBUG = 0;

my $UNANNOTATED_CLUSTER_NAME = 'Unannotated cluster';

# secondary database settings:
my $SECONDARY_DATABASE = [
    {
        name     => 'V2 DB',
        host     => 'localhost',
        login    => 'greenphyl',
        password => 'hades2000',
        database => 'greenphyl_v2',
    },
];

my $MIN_SEQUENCE_PER_FAMILY = 3;
my $TRANSFER_THRESHOLD = 60; # [0-100] value

my %DB_ID_LOOKUP = ();


# Script global variables
##########################

=pod

=head1 VARIABLES

B<$g_debug>: (boolean)

Set debug mode.

B<$error_found>: (integer)

Count the errors met during the load.

B<$sql_query>: (string)

Used to store temporary SQL queries.

B<$mysql>: (Greenphyl::ConnectMysql)

Database connection object.

B<$g_dbh>: (dbi handle)

Handle to the current GreenPhyl database.

B<$g_dbis>: (dbi simple handle)

Handle to the current GreenPhyl database.

B<$g_dbh2>: (dbi handle)

Handle to the old GreenPhyl database containing annotations.

B<$g_dbis>: (dbi simple handle)

Handle to the old GreenPhyl database containing annotations.

=cut

my $g_debug       = $DEBUG;
my $error_found = 0;
my $sql_query;
my $mysql;
my $g_dbh;
my $g_dbis;
my $g_dbh2;
my $g_dbis2;
my $g_created_family_count     = 0;
my $g_reassigned_family_count  = 0;
my $g_annotation_transfer      = 0;
my $g_annotation_transfer_only = 0;
my $g_no_test_upstream         = 0;



# Script global functions
##########################

=pod

=head1 FUNCTIONS

=head2 getOldSeqID

B<Description>: retrieve the seq_id in secondary database of a sequence name
corresponding to a sequence in new database.

B<ArgsCount>: 1

=over 4

=item $seq_textid: (string) (R)

Sequence name (seq_textid).

=back

B<Return>: (integer)
the seq_id in secondary database or undef if no sequence has been found.

=cut

sub getOldSeqID
{
    my ($seq_textid) = @_;

    # check if sequence is present in $SECONDARY_DATABASE
    $sql_query = "SELECT seq_id FROM sequences WHERE seq_textid LIKE '$seq_textid';";
    my ($seq_id) = $g_dbh2->selectrow_array($sql_query);
    # if sequence is not found using seq_textid try using sequences_history table
    if (!$seq_id)
    {
        # get older sequence names from main database
        $sql_query = "SELECT old_seq_textid FROM sequences_history WHERE new_seq_textid LIKE '$seq_textid' ORDER BY transaction_date DESC;";
        my $old_seq_textid;
        # try to find a match in secondary database with each old name
        while (!$seq_id && (($old_seq_textid) = $g_dbh->selectrow_array($sql_query)))
        {
            $sql_query = "SELECT seq_id FROM sequences WHERE seq_textid LIKE '$old_seq_textid';";
            ($seq_id) = $g_dbh2->selectrow_array($sql_query);
            if ($seq_id)
            {
#                    LogInfo("Found a matching renamed sequence '$old_seq_textid' ($seq_id) for sequence $seq_textid");
                #print "  Found a matching renamed sequence '$old_seq_textid' ($seq_id) for sequence $seq_textid\n";
            }
        }
    }
    else
    {
#            LogInfo("Found sequence '$seq_textid' ($seq_id) in secondary database");
        #print "  Found sequence '$seq_textid' ($seq_id) in secondary database\n";
    }
    return $seq_id;
}


=pod

=head2 createAndLoadFamily

B<Description>: creates a family of sequences.

B<ArgsCount>: 3

=over 4

=item $sequence_list: (array of string) (R)

List of sequence names (seq_textid).

=item $family_level: (integer) (R)

Family level (1, 2, 3 or 4).

=item $new_family_name: (string) (R)

Family name.

=back

B<Return>: nothing

=cut

sub createAndLoadFamily
{
    my ($sequence_list, $family_level, $new_family_name) = @_;

    # create new a family
    $g_dbis->insert('family',
        {
            'family_name' => $new_family_name,
        }
    );

    # -fetch id of inserted family (nb. we're in transaction so no concurrent ID chould have been inserted)
    my $new_family_id = $g_dbh->last_insert_id(undef, undef, 'family', 'family_id');

    # -insert class=$family_level into found_in for the new family
    $g_dbis->insert('found_in',
        {
            'family_id' => $new_family_id,
            'class_id'  => $family_level,
        }
    );
	print "Creating a new family (family_id=$new_family_id) with " . scalar(@$sequence_list) . " sequences\n";
	LogInfo("Creating a new family (family_id=$new_family_id) with " . scalar(@$sequence_list) . " sequences");

    foreach my $sequence_name (@$sequence_list)
    {
        #warn "DEBUG: Add sequence '$sequence_name' to family $new_family_id\n";
        # -insert sequence-family link into seq_is_in
        if (!$g_dbh->do("INSERT INTO seq_is_in (seq_id, family_id) SELECT seq_id, $new_family_id FROM sequences WHERE seq_textid = ? LIMIT 1;", undef, $sequence_name))
        {
            ++$error_found;
            warn "WARNING: Sequence '$sequence_name' not found in main database while it is expected to be there!\n";
        }

    }
    warn "DEBUG: NEW FAMILY added: $new_family_id\n" if ($g_debug);
    ++$g_created_family_count;
    
    if ($g_annotation_transfer)
    {
        if (my $best_candidate_family = findMatchingFamilyInOtherDB($sequence_list, $family_level, $new_family_id))
        {
            transferFamilyAnnotations($new_family_id, $best_candidate_family);
        }
    }
}


=pod

=head2 findMatchingFamilyInOtherDB

B<Description>: try to find a matching family in the secondary database.

B<ArgsCount>: 3

=over 4

=item $sequence_list: (array of string) (R)

List of sequence names (seq_textid).

=item $family_level: (integer) (R)

Family level (1, 2, 3 or 4).

=item $new_family_id: (integer) (R)

ID of the new family in main database.

=back

B<Return>: (integer)

The family ID of the best matching candidate family in secondary database or
undef if no valid match was found.

=cut

sub findMatchingFamilyInOtherDB
{
    my ($sequence_list, $family_level, $new_family_id) = @_;

    # search in secondary database a family containing all the sequences of
    # current new family
    warn "DEBUG: Search in secondary database for candidate families...\n" if ($g_debug);
    my @seq_found_also_in_db2 = ();
    my %family_seq_in_db2 = (); # keys are family IDs, values are sequence count
    foreach my $sequence_name (@$sequence_list)
    {
        my $seq_id = getOldSeqID($sequence_name);

        # if seq found in $SECONDARY_DATABASE get family_id
        if ($seq_id)
        {
            push(@seq_found_also_in_db2, $sequence_name);
			##modif : "AND fi.class_id = $family_level" etait apres le 2eme LEFT JOIN. probleme: annotation level 1 se retrouve transf�r� dans nouveaux clusters level2 
            $sql_query = "SELECT si.family_id, tf.new_family_id, tf.transfer_date
                            FROM sequences se
                                 JOIN seq_is_in si ON si.seq_id = se.seq_id
                                 JOIN found_in fi ON fi.family_id = si.family_id 
                                 LEFT JOIN transfered_families tf ON tf.old_family_id = si.family_id
                            WHERE se.seq_id = $seq_id
                                  AND se.filtered = 0
								  AND fi.class_id = $family_level;";

            # get the family the sequence belongs to
            my ($family_id_in_db2, $transfered_new_family_id, $transfer_date) = ($g_dbh2->selectrow_array($sql_query));
            $family_id_in_db2 ||= 'orphan';

            # mark families already transfered
            if ($transfered_new_family_id)
            {
                $family_id_in_db2 .= '_' . $transfered_new_family_id;
            }

            # increase associated counters
            if (exists($family_seq_in_db2{$family_id_in_db2}))
            {
                push(@{$family_seq_in_db2{$family_id_in_db2}}, $sequence_name);
            }
            elsif ($family_id_in_db2 ne 'orphan')
            {
                $family_seq_in_db2{$family_id_in_db2} = [$sequence_name];
            }
        }
    }
    if ($g_debug)
    {
        warn "DEBUG: Done.\nGot an initial set of " . scalar(@$sequence_list). " sequences\nFound a common set of " . scalar(@seq_found_also_in_db2) . " sequences spread in " . scalar(keys(%family_seq_in_db2)) . " families\n";
    }

    my $db2_candidate_family_count = scalar(keys(%family_seq_in_db2));

    my @candidate_families = sort { return scalar(@{$family_seq_in_db2{$b}}) <=> scalar(@{$family_seq_in_db2{$a}}) } (keys(%family_seq_in_db2));
    print "    $db2_candidate_family_count candidate families for annotation transfer in secondary database.\n";
    LogInfo("    $db2_candidate_family_count candidate families for annotation transfer in secondary database.");
    map
    {
        my ($old_family_id, $transfered_family_id, $real_old_family_id) = ($_, 0, $_);
        # check if family has already been transfered
        if ($old_family_id =~ m /^(\d+)_(\d+)$/)
        {
            $real_old_family_id = $1;
            $transfered_family_id = $2;
        }

        my $percent = int(0.5 + 100. * scalar(@{$family_seq_in_db2{$old_family_id}}) / scalar(@seq_found_also_in_db2));

        # only print families with more than 1% correspondance
        if (1 < $percent)
        {
            # store family match stats
            $g_dbh->do("INSERT INTO family_history (old_family_id,
                                                    new_family_id,
                                                    transaction_date,
                                                    transaction_type,
                                                    sequences_in_old_family,
                                                    sequences_in_new_family,
                                                    additional_info)
                        VALUES (?, ?, NOW(), 'none', ?, ?, ?);",
                       undef,
                       $real_old_family_id, $new_family_id, scalar(@{$family_seq_in_db2{$old_family_id}}), scalar(@seq_found_also_in_db2), "sequences counts only reflect sequences present in both db.\nScore: $percent\nFamily sequences in both DB: " . join(' ', @seq_found_also_in_db2) . "\nCommon set: " . join(' ', @{$family_seq_in_db2{$old_family_id}}) . ($transfered_family_id ? '' : "Previously transfered to family: $transfered_family_id\n"));
            if ($transfered_family_id)
            {
                print "    family $old_family_id contains " . $percent . "% sequences but has already been transfered to family $transfered_family_id\n";
#                LogInfo("    family $old_family_id contains " . $percent . "% sequences  but has already been transfered to family $transfered_family_id");
            }
            else
            {
                print "    candidate family $old_family_id contains " . $percent . "% sequences\n";
#                LogInfo("    candidate family $old_family_id contains " . $percent . "% sequences");
            }
        }
    } @candidate_families;
    print "\n";

    # find best candidate (ie. skip families already transfered)
    my $best_candidate_family;
    do
    {
        $best_candidate_family = shift(@candidate_families);
        # check if family id is not valid (already transfered family_id)
        if ($best_candidate_family && ($best_candidate_family =~ m /_/))
        {
            # candidate has already been transfered to another family
            $best_candidate_family = undef;
        }
    } while (@candidate_families && !$best_candidate_family);
    # check if best candidate is good enough for data transfer
    if ($best_candidate_family)
    {
        my $percent = int(0.5 + 100. * @{$family_seq_in_db2{$best_candidate_family}} / scalar(@seq_found_also_in_db2));
        if ($percent > $TRANSFER_THRESHOLD)
        {
            if ($g_debug)
            {
                warn "DEBUG: Got a valid candidate (family_id $best_candidate_family) family for annotation transfering ($percent %).\n";
            }
            # got a valid match
            return $best_candidate_family;
        }
    }
    if ($g_debug)
    {
        warn "DEBUG: No valid candidate family found.\n";
    }

    # no valid match found
    return undef;
}


=pod

=head2 transferFamilyAnnotations

B<Description>: transfer annotation from the secondary database to the main one.

B<ArgsCount>: 2

=over 4

=item $new_family_id: (integer) (R)

ID of the new family in main database.

=item $best_candidate_family: (integer) (R)

ID of the best matching candidate family in the secondary database.

=back

B<Return>: nothing

=cut

sub transferFamilyAnnotations
{
    my ($new_family_id, $best_candidate_family) = @_;
    print "    TRANSFERING annotations from family $best_candidate_family to family $new_family_id\n";
    LogInfo("    TRANSFERING annotations from family $best_candidate_family to family $new_family_id");

    # transfer $SECONDARY_DATABASE annotations to current database if and
    # only if in family table:
    # -validated = 1, 2 or 3
    # -or validated = 0 and family name contains "%family"
    # and then, add family_id to transfered_families table in
    # $SECONDARY_DATABASE to avoid family reallocation to 2 or more
    # families in new classification.
    my ($kept_family_name,
        $kept_type,
        $kept_description,
        $kept_validated,
        $kept_black_list,
        $kept_inference) =
        ($g_dbh2->selectrow_array("SELECT family_name,
                                        type,
                                        description,
                                        validated,
                                        black_list,
                                        inference
                                 FROM family
                                 WHERE family_id = $best_candidate_family;"));
    $kept_family_name ||= '';

    if ((1 == $kept_validated)
        || (2 == $kept_validated)
        || (3 == $kept_validated)
        || ((0 == $kept_validated) && ($kept_family_name =~ m/family$/i)))
    {
        $g_dbis->update('family',
            {
                'family_name' => $kept_family_name,
                'type'        => $kept_type,
                'description' => $kept_description,
                'validated'   => $kept_validated,
                'black_list'  => $kept_black_list,
                'inference'   => $kept_inference,
            },
            {
                'family_id'   => $new_family_id,
            }
        );
        # transfer synonyms
        my $kept_synonyms = $g_dbh2->selectcol_arrayref("SELECT synonym FROM synonym WHERE family_id = $best_candidate_family;");
        foreach my $synonym (@$kept_synonyms)
        {
            # $g_dbis->insert('synonym', {'family_id' => $new_family_id, 'synonym' => $synonym});
            $g_dbh->do("INSERT IGNORE INTO synonym (family_id, synonym) VALUES ('$new_family_id', '$synonym');");
        }
        # transfer dbxref
        my $kept_dbxrefs = $g_dbh2->selectall_arrayref("SELECT dbx.* FROM family_dbxref fd JOIN dbxref dbx USING (dbxref_id) WHERE fd.family_id = $best_candidate_family;", { 'Slice' => {} }
        ) or LogWarning("WARNING: failed to fetch dbxref for family $best_candidate_family!\n");
        if ($kept_dbxrefs)
        {
            foreach my $dbxref (@$kept_dbxrefs)
            {
                my ($dbxref_id) = ($g_dbh->selectrow_array("SELECT dbxref_id FROM dbxref WHERE db_id = ? AND accession LIKE ? AND version = ?;", undef, $DB_ID_LOOKUP{$dbxref->{'db_id'}}, $dbxref->{'accession'}, $dbxref->{'version'}));
                if (!$dbxref_id)
                {
                    # try to insert dbxref in database or do nothing if already there
                    $g_dbh->do("INSERT IGNORE INTO dbxref (db_id, alias, accession, description, version) VALUE (?, ?, ?, ?, ?);", undef, $DB_ID_LOOKUP{$dbxref->{'db_id'}}, $dbxref->{'alias'}, $dbxref->{'accession'}, $dbxref->{'description'}, $dbxref->{'version'});
                }
                # fetch corresponding dbxref_id
                if ($dbxref_id || (($dbxref_id) = ($g_dbh->selectrow_array("SELECT dbxref_id FROM dbxref WHERE db_id = ? AND accession LIKE ? AND version = ?;", undef, $DB_ID_LOOKUP{$dbxref->{'db_id'}}, $dbxref->{'accession'}, $dbxref->{'version'}))))
                {
                    # insert family dbxref relationship using db_id translation table
                    $g_dbh->do("INSERT IGNORE INTO family_dbxref (family_id, dbxref_id) VALUE (?, ?);", undef, $new_family_id, $dbxref_id);
                }
                else
                {
                    LogWarning("WARNING: failed to transfer dbxref for family $best_candidate_family!\n");
                }
            }
        }
        
        # keep track of transfered families
        $g_dbis2->insert('transfered_families', {'old_family_id' => $best_candidate_family, 'new_family_id' => $new_family_id});
        if ($g_debug)
        {warn "DEBUG: Done.\n";}
        # store transfer info in new database
        $g_dbh->do("UPDATE family_history SET transaction_type = 'annotation_transfer' WHERE new_family_id = $new_family_id AND old_family_id = $best_candidate_family;");
        ++$g_reassigned_family_count;
    }
    else
    {
        # log untransfered matching families
        LogInfo("Matching old family $best_candidate_family does not suite validation rules for family $new_family_id.");
    }
}


sub testUpstreamLevel
{
    my ($sequence_list, $family_level, $new_family_name) = @_;
    my %upper_families = ();

	# we test only if all sequence in a cluster from level 2, 3 and 4 belong to the same cluster at level 1 
	my $family_level_test = 1;

    #warn "DEBUG: count upper families...\n";
    foreach my $sequence_name (@$sequence_list)
    {
        my $seq_id = getOldSeqID($sequence_name);

        if ($seq_id)
        {
            # find to wich family above the current sequence blongs to
            my $sql_query = "SELECT si.family_id
                             FROM sequences se
                                  JOIN seq_is_in si ON si.seq_id = se.seq_id
                                  JOIN found_in fi ON fi.family_id = si.family_id
                             WHERE fi.class_id = $family_level_test
                                   AND se.seq_id = $seq_id;";

            my ($family_id) = $g_dbh->selectrow_array($sql_query);
            # keep track of families found
            if ($family_id)
            {
               # $upper_families{$family_id} = 1;
                $upper_families{$family_id}++;
            }
            elsif ($g_debug)
            {warn "DEBUG: no upper family found for '$sequence_name'!\n";}
        }
        else
        {
            if ($g_debug)
            {warn "DEBUG: sequence '$sequence_name' not found in secondary database.\n";}
        }
    }
    my $upper_families_count = scalar(keys(%upper_families));

    print "Belong to $upper_families_count cluster at level $family_level_test\n";
    LogInfo("Belong to $upper_families_count cluster at level $family_level_test");

	if (0 == $upper_families_count)
    {
        warn "WARNING: no sequence classified at upper level!\n";
		
    }

	if (1 == $upper_families_count)
    {
        #warn "DEBUG: Belong to one cluster at upper level\n";
        if ($g_debug)
		{warn "\n";}
		print "    ";
		createAndLoadFamily($sequence_list, $family_level, $new_family_name);
    }
    else
    {
        #warn "WARNING: $upper_families_count families found at level $family_level_test for '" . join('\', \'', @$sequence_list) . "'!\n";
		#warn "WARNING: $upper_families_count families found at level $family_level_test!\n";
		##
		print "    Upper family: ";
		foreach my $upper_family (keys %upper_families)
		{
            print "$upper_family ($upper_families{$upper_family}) ";
            LogInfo("Upper family: $upper_family ($upper_families{$upper_family})");
        }
		print "\n";
		#print "    Family not created\n";
		createAndLoadFamily($sequence_list, $family_level, $new_family_name);
    }
}


# Script options
#################

=pod

=head1 OPTIONS

load_classification.pl [-help | -man] | [-debug] [-rm_splice] [-annotation_transfer] [-check_subfamily] <CLASSIFICATION_FILE> <FAMILY_LEVEL> | [-annotation_transfer_only] <FAMILY_LEVEL>

=over 4

=item B<-help>:

Prints a brief help message and exits.

=item B<-man>:

Prints the manual page and exits.

=item B<-debug> (integer):

Executes the script in debug mode. If an integer value is specified, it will
be the debug level. If "-debug" option was used without specifying a debug
level, level 1 is assumed.
Default: 0 (not in debug mode).

=item B<-rm_splice>:

do not load alternative transcripts.

=item B<-annotation_transfer>:

compare with a secondary database to transfer cluster annotation if 60% of
COMMON sequences are in the same cluster.

=item B<-annotation_transfer_only>:

only transfer annotations and do not load new clustering.

=item B<-check_subfamily>:

log inversed subclassification (clusters a sublevel with more than 1 parent).

=item B<CLASSIFICATION_FILE> (string):

Path to the classification file. Each line of the file is a list of
space-separated sequence name that belong to a same family.

=item B<FAMILY_LEVEL> (integer):

Level (class_id) of the families stored in the classification file.

=back

=cut


# CODE START
#############

print("GreenPhyl Load Classification v0.2.0\n\n");

# options processing
my ($man, $help, $rm_splice) = (0, 0, 0);
# parse options and print usage if there is a syntax error.
GetOptions("help|?"                   => \$help,
           "man"                      => \$man,
           "debug"                    => \$g_debug,
           "rm_splice"                => \$rm_splice,
           "annotation_transfer"      => \$g_annotation_transfer,
           "annotation_transfer_only" => \$g_annotation_transfer_only,
           "check_subfamily"          => \$g_no_test_upstream,
           )
    or pod2usage(2);
if ($help) {pod2usage(1);}
if ($man) {pod2usage(-verbose => 2);}

if ($g_debug)
{warn "Running in DEBUG mode! No database commit will be issued!\n";}

# get script arguments
my $mcl_filename = '';
# check if an MCL filename is needed (in case of annotation transfer only)
if (!$g_annotation_transfer_only)
{
    # out mcl file
    $mcl_filename = shift() or pod2usage(1);
}
else
{
    # $g_annotation_transfer_only set, also set $g_annotation_transfer
    $g_annotation_transfer = 1;
}

# class_id
my $family_level = int(shift) or pod2usage(1);

print "Working database:   " . $Greenphyl::Config::DATABASES->[0]->{'database'} . " (" . $Greenphyl::Config::DATABASES->[0]->{'host'} . ")\n";
LogInfo("Working database:   " . $Greenphyl::Config::DATABASES->[0]->{'database'} . " (" . $Greenphyl::Config::DATABASES->[0]->{'host'} . ")");
print "Secondary database: " . $SECONDARY_DATABASE->[0]->{'database'} . " (" . $SECONDARY_DATABASE->[0]->{'host'} . ")\n";
LogInfo("Secondary database: " . $SECONDARY_DATABASE->[0]->{'database'} . " (" . $SECONDARY_DATABASE->[0]->{'host'} . ")");
print "Note: secondary database is used as source data for family annotations transfering.\n\n";
LogInfo("Note: secondary database is used as source data for family annotations transfering.");

if ($g_debug)
{warn "DEBUG: connecting to databases...\n";}

# init variables
# -first database handler
$mysql        = Greenphyl::ConnectMysql->new();
$g_dbh          = $mysql->DBconnect();
$g_dbis         = DBIx::Simple->new($g_dbh);

# -second database handler
$g_dbh2         = DBI->connect( "dbi:mysql:" . $SECONDARY_DATABASE->[0]->{database} . ":" . $SECONDARY_DATABASE->[0]->{host}, $SECONDARY_DATABASE->[0]->{login}, $SECONDARY_DATABASE->[0]->{password} ) or confess "ERROR: failed to connect to secondary database (" . $SECONDARY_DATABASE->[0]->{database} . "):\n$DBI::errstr\n";
$g_dbis2        = DBIx::Simple->new($g_dbh2);
$g_dbh2->{LongReadLen} = 512 * 1024;
$g_dbh2->{HandleError} = sub { confess(shift) }; # more explicit error reporting

if ($g_debug)
{
    warn "DEBUG: done.\n";
    warn "DEBUG: Begin database transaction...\n";
}
# prepare transaction
$g_dbh->begin_work() or confess $g_dbh->errstr;
$g_dbh2->begin_work() or confess $g_dbh->errstr;

# check if transfered_families table exists
if (!$g_dbh2->selectrow_array("SELECT TRUE FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_SCHEMA = '" . $SECONDARY_DATABASE->[0]->{'database'} . "' AND TABLE_NAME = 'transfered_families';"))
{
    if ($g_debug)
    {warn "DEBUG: Table transfered_families does not exist in old (secondary) database, creating table...\n";}
    # table does not exist, create it
    $g_dbh2->do("CREATE TABLE IF NOT EXISTS transfered_families (
                   old_family_id mediumint(10) unsigned NOT NULL,
                   new_family_id mediumint(10) unsigned NOT NULL,
                   transfer_date TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
                   PRIMARY KEY (old_family_id)
               ) ENGINE=InnoDB  DEFAULT CHARSET=utf8;"
    ) or confess "ERROR: Failed to create transfered_families table:\n" . $g_dbh->errstr;
    print "Note: 'transfered_families' table did not exist in secondary database and was added.\n";
    LogInfo("Note: 'transfered_families' table did not exist in secondary database and was added.");
    if ($g_debug)
    {warn "DEBUG: Done.\n";}
}
else
{
    # clear transfered families info
    print "Cleaning 'transfered_families' table...\n";
    LogInfo("Truncate 'transfered_families' table.");
    $g_dbh2->do("DELETE FROM transfered_families;"
    ) or confess "ERROR: Failed to clean transfered_families table:\n" . $g_dbh->errstr;
    print "Done.\n";
}

# synchronize db sources...
# -get all db_id used by family_dbxref
my $db2_db_all_data = $g_dbh2->selectall_arrayref("SELECT DISTINCT db.* FROM db db JOIN dbxref dx USING (db_id) JOIN family_dbxref USING (dbxref_id);", { 'Slice' => {} }
) or confess "ERROR: Failed to get used db in second database (for dbxref):\n" . $g_dbh->errstr;
# -make sure each db is in current database
foreach my $db2_db_data (@$db2_db_all_data)
{
    # -try to insert db if not already there
    $g_dbh->do("INSERT IGNORE INTO db (name, description, urlprefix, url) VALUE (?, ?, ?, ?);", undef, $db2_db_data->{'name'}, $db2_db_data->{'description'}, $db2_db_data->{'urlprefix'}, $db2_db_data->{'url'});
    if (my ($db1_db_id) = ($g_dbh->selectrow_array("SELECT db_id FROM db WHERE name LIKE ?;", undef, $db2_db_data->{'name'})))
    {
        # -db found in both database, init lookup table
        $DB_ID_LOOKUP{$db2_db_data->{'db_id'}} = $db1_db_id;
    }
    else
    {
        confess "Failed to insert and find database reference '" . $db2_db_data->{'name'} . "' into primary database!\n";
    }
}

# check if only annotation transfer must occure
if ($g_annotation_transfer_only)
{
    # check level
    if ((1 <= $family_level) &&  (4 >= $family_level))
    {
        # get list of family_id for that level
        if (my $family_id_list = $g_dbh->selectcol_arrayref("SELECT family_id FROM found_in WHERE class_id = ?;", undef, $family_level))
        {
            # process each family_id
            foreach my $family_id (@$family_id_list)
            {
                # get list of seq_textid for current family
                my $sequence_list = $g_dbh->selectcol_arrayref("SELECT s.seq_textid FROM sequences s JOIN seq_is_in sii USING (seq_id) WHERE sii.family_id = ?;", undef, $family_id);
                if (my $best_candidate_family = findMatchingFamilyInOtherDB($sequence_list, $family_level, $family_id))
                {
                    transferFamilyAnnotations($family_id, $best_candidate_family);
                }
            }
        }
        else
        {
            confess "ERROR: no family found for level $family_level!\n" . $g_dbh->errstr;
        }
    }
    else
    {
        confess "ERROR: invalid family level ($family_level)!\n";
    }
    print "Families with transfered annotations: $g_reassigned_family_count\n";
    LogInfo("Families with transfered annotations: $g_reassigned_family_count");
}
else
{
    # make sure we index families from the next available family_id
    # because indexing may be altered by previous aborted transactions.
    my ($next_free_family_id) = $g_dbh->selectrow_array("SELECT max(family_id)+1 FROM family;");
    $next_free_family_id = 1000 if (!defined($next_free_family_id) || ("" eq $next_free_family_id));
    $g_dbh->do("ALTER TABLE family AUTO_INCREMENT = $next_free_family_id;");

    if (1 == $family_level)
    {
        # Clear all family table
        print "Removing all families...\n";
        LogInfo("Truncate family table");
        $sql_query = 'DELETE FROM family;';
        $g_dbh->do($sql_query);
        print  "Done.\n*********************************************************\n";
    }
    #else
    #{
        # Remove from family families of the specified level
       # print "Removing all families of level $family_level...\n";
        
        ###temp until DELETE FORM..USING fixed ###
        #my $get_family_id = $g_dbh->selectcol_arrayref("SELECT F.family_id FROM family F, found_in FI WHERE FI.family_id = F.family_id AND FI.class_id = $family_level;");
        #foreach my $fam_id (@$get_family_id)
        
                #{
                    ##print "DELETE FROM family WHERE family_id = '$fam_id';\n";
                    #$sql_query = "DELETE FROM family WHERE family_id = '$fam_id';";
                    #$g_dbh->do($sql_query);
                #}
        ###

      #not functional
        #$sql_query = "DELETE FROM family USING found_in fi WHERE fi.class_id = $family_level AND fi.family_id = family_id;";
        #$g_dbh->do($sql_query);
       # print "Done.\n";
        #print "*********************************************************\n";
    #}
    # parse MCL output file: one line = one cluster
    print "Processing input file '$mcl_filename'...\n";
    LogInfo("Processing input file '$mcl_filename'...");

    if ($g_debug)
    {warn "DEBUG: parsing input file...\n";}
    my $mcl_fh;
    open($mcl_fh, $mcl_filename) or confess "ERROR: failed to open file '$mcl_filename'!\n";
    if ((!$g_no_test_upstream) || (1 == $family_level))
    {
        foreach my $line (<$mcl_fh>)
        {
            if ($g_debug)
            {warn "." ;}
            # trim line
            $line =~ s/^\s+//;
            $line =~ s/\s+$//;
            # explode line in sequence text IDs
            my @sequence_list = map { {'seq_textid' => $_, 'sequence' => ''}; } split(/\s+/, $line);
            if ($rm_splice)
            {
                # remove splice forms
                @sequence_list = @{Greenphyl::Utils::removeSpliceForm(\@sequence_list)};
            }
            @sequence_list = map { $_->{'seq_textid'} } @sequence_list;

            # check number of seq in new cluster
            # and allow to create cluster with a minimum of 3 seq at level 1
            if (@sequence_list >= $MIN_SEQUENCE_PER_FAMILY)
            {
                createAndLoadFamily(\@sequence_list, $family_level, $UNANNOTATED_CLUSTER_NAME);
            }
            elsif ($g_debug)
            {
                warn "DEBUG: family creation skipped because there was not enough members (" . scalar(@sequence_list) . ": " . join(', ', @sequence_list) . ")\n";
            }
        }
        if ($g_debug)
        {warn "\n";}
    }
    # else if level 2, 3 or 4
    elsif ((2 == $family_level)
           || (3 == $family_level)
           || (4 == $family_level))
    {
        foreach my $line (<$mcl_fh>)
        {
            if ($g_debug)
            {warn ".";}
            # trim line
            $line =~ s/^\s+//;
            $line =~ s/\s+$//;
            # explode line in sequence IDs
            my @sequence_list = map { return {'seq_textid' => $_, 'sequence' => ''}; } split(/\s+/, $line);
            if ($rm_splice)
            {
                # remove splice forms
                @sequence_list = @{Greenphyl::Utils::removeSpliceForm(\@sequence_list)};
            }
            @sequence_list = map { $_->{'seq_textid'} } @sequence_list;

            # and allow to create cluster with a minimum of 3 seq
            if (@sequence_list >= $MIN_SEQUENCE_PER_FAMILY)
            {
                print "Test new family at level $family_level:\t";
    #			LogInfo("Test new family at level $family_level:\t");
                testUpstreamLevel(\@sequence_list, $family_level, '');
            }
        }
        if ($g_debug)
        {warn "\n";}
    }
    else
    {
        confess "ERROR: invalid family level ($family_level)!\n";
    }
    close($mcl_fh);

    print "Families with transfered annotations: $g_reassigned_family_count\n";
    print "Brand new families: " . ($g_created_family_count - $g_reassigned_family_count) . "\n";
    print "Total: $g_created_family_count families created\n";
    LogInfo("Families with transfered annotations: $g_reassigned_family_count");
    LogInfo("Brand new families: " . ($g_created_family_count - $g_reassigned_family_count));
    LogInfo("Total: $g_created_family_count families created");
}

if ($g_debug)
{warn "DEBUG: Done.\n";}


#print "Updating family/sequence/species count...\n";
# warn "DEBUG: Count families.\n" if ($g_debug);
#$g_dbh->do('CALL countFamilySeqBySpecies();') if (!$g_debug);
# warn "DEBUG: Done.\n" if ($g_debug);
#print "Done.\n";

if ($g_debug)
{
    # rollback in debug mode
    warn "DEBUG: Rolling back database changes for debug mode!\n";
    $g_dbh->rollback();
    $g_dbh2->rollback();
}
elsif ($error_found)
{
    if (prompt("Some error occured! Commit database changes anyway? [y/n] ", { 'default' => 'n', 'constraint' => '[yYnN]' }) =~ m/[yY]/)
    {
        print "Committing changes...\n";
        $g_dbh->commit();
        $g_dbh2->commit();
    }
    else
    {
        print "Database changes canceled by user. Rolling back...\n";
        $g_dbh->rollback();
        $g_dbh2->rollback();
    }
}
else
{
    print "Committing changes...\n";
    $g_dbh->commit();
    $g_dbh2->commit();
}

print "All done.\n\n";
LogInfo("All done.");

exit(0);
