#!/usr/bin/perl

use strict;
use warnings;

use LWP::UserAgent;
use URI::Escape;
use Data::Dumper;
use Carp qw(cluck confess croak);

use lib '../../lib';
use Greenphyl;
use Greenphyl::Log;

my $uri = URI->new('http://www.ebi.ac.uk/QuickGO/GAnnotation?q=');
my $ua = LWP::UserAgent->new;

my $g_dbh = GetDatabaseHandler();

{
    my %go_type_for = (
        Process   => 'biological_process',
        Component => 'cellular_component',
        Function  => 'molecular_function',
    );
    
    my %id_for_code;
    
    # Select all uniprots
    my $dbxref = $g_dbh->selectall_hashref( 
        qq{
            SELECT id, accession
            FROM dbxref
            WHERE db_id = 1
        }, 'accession');
    my @accessions = keys %$dbxref;
    
    # Prepare the output file. This file will be loaded into mysql to update
    # the uniprot_to_go table
    open( OUTPUT_FILE, ">load_uniprot_to_go.txt" );
        
    # Send a request for 150 uniprots at each loop
    while( my @uniprot_range = splice( @accessions, 0, 150 ) ) {
        $encoded_uri = $uri->query_form({
            'protein' => join ',', @uniprot_range
            '!ref'    => 'GO_REF:0000002', # exclude GO annotations coming from InterPro2GO
            'format'  => 'tsv',            # tabulated text
            'limit'   => -1,               # get all data at once, not by chunks
            'col'     => join ',', qw{proteinID goID goName aspect evidence ref'},
        });
        
        my $response = $ua->get( $encoded_uri );
        
        if ( $response->is_success )
        {
            my @go_for_uniprot = split( /\n/, $response->content );
            shift @go_for_uniprot; # skip header
            
            foreach (@go_for_uniprot) {
                my ($accession,$go_code,$go_desc,$aspect,$evidence,$ref) = split /\t/;
                
                # 1- Get go_id from hash or from DB (insert/update/select)
                $id_for_code{$go_code} ||= insert_in_db( $go_code, $go_desc, $go_type_for{$aspect} );
                my $go_id = $id_for_code{$go_code};
                
                # 2- Append uniprot_to_go data to output file
                print OUTPUT_FILE "$dbxref->{$accession}->{'id'}\t$go_id\t$evidence\n";
            }
        }
        else {
            LogError( "Web Service request failed for $encoded_uri" );
        }
    }
    
    close OUTPUT_FILE;
    
    # my $rows_affected = $g_dbh->do( "LOAD DATA LOCAL INFILE 'load_uniprot_to_go.txt' INTO TABLE uniprot_to_go" );
    # LogInfo( "uniprot_to_go inserts: $rows_affected" );
}


sub insert_in_db
{
    my ($go_code, $go_desc, $go_type) = @_;
    
    my $sql_exists = "SELECT go_id, go_desc FROM go WHERE go_code = ?";
    my $sql_update = "UPDATE go SET go_desc = ? WHERE go_id = ?";
    my $sql_create = "INSERT INTO go (go_code, go_type, go_desc) VALUES (?,?,?)";
    
    my ($go_id,$current_go_desc) = $g_dbh->selectrow_array( $sql_exists, undef, $go_code );
    
    # If GO exists...
    if ( $go_id )
    {
        # ... and go_desc is given -> update GO
        if ( $go_desc && $go_desc ne $current_go_desc ) {
            $g_dbh->do( $sql_update, undef, $go_desc, $go_id );
        }
    }
    # GO does not exist in DB -> INSERT it
    else 
    {
        $g_dbh->do( $sql_create, undef, $go_code, $go_type, $go_desc );
        
        # Get the newly created go_id
        ($go_id,$current_go_desc) = $g_dbh->selectrow_array( $sql_exists, undef, $go_code );
    }
    
    return $go_id;
}
