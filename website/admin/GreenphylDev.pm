package GreenphylDev;

=pod

=head1 NAME

Dev

=head1 SYNOPSIS

    use Dev;
    print "Greenphyl root directory is: " . $Dev::ROOT . "\n";

=head1 REQUIRES

Perl5

=head1 DESCRIPTION

This module contains constants for update_dev.pl, commit_dev.pl and
dev_to_prod.pl.

=cut

use strict;
use warnings;

use lib "../lib";
use lib '../local_lib';
use Carp qw (cluck confess croak);

use Greenphyl;


# Script global constants
##########################

=pod

=head1 CONSTANTS

Note: for each array of path, path starting by a '/' are considered as
      absolute, other path are considered relative to the specified dev
      directory. Path should not include an ending '/'.

B<$BRANCHES>: (string)

Directory containing versioned branch-specific files.
Relative to GP('ROOT_PATH'). No starting or ending slash '/'.

B<$LOGS>: (string)

Directory containing log files.
Relative to GP('ROOT_PATH'). No starting or ending slash '/'.

B<$DEFAULT_DEV>: (string)

Default development directory.
Relative to GP('ROOT_PATH'). No starting or ending slash '/'.

B<$UPDATE_DEV_SCRIPT>: (string)

Path to the script used to update a developement site version.

B<$SVN_UPDATE_COMMAND>: (string)

Command line to use to update using SVN.

B<$SVN_REVERT_COMMAND>: (string)

Command line to use to revert files using SVN.

B<$SVN_COMMIT_COMMAND>: (string)

Command line to use to commit using SVN.

B<$SVN_STATUS_COMMAND>: (string)

Command line to use to get SVN status.

B<$SVN_EXPORT_COMMAND>: (string)

Command line to use to export using SVN.

B<$CONFIG_FILE>: (string)

Relative path (from the website dir) to Greenphyl config file.

B<@REMOVE_FILES>: (array of strings)

Files that should be removed after update.

B<@REMOVE_DIRECTORIES>: (array of strings)

Directory that should be removed after update.

B<@CREATE_DIRECTORIES>: (array of strings)

Empty directory that should be created after update.

B<%COPIES>: (hash of strings)

Files or directories that should be copied after update.
Keys are source files/directories, values are destination files/directories.

B<%LINKS_TO_SET>: (hash of strings)

Links to create if missing.
Keys are link names, values are targets.

B<%BRANCH_COPIES>: (hash of strings)

Files or directories that should be copied after update related to a branch.
Keys are source files/directories, values are destination files/directories.
NOTE: keys relative path are relative to the 'GP('ROOT_PATH')/$BRANCHES/<dev_directory>'
instead of 'GP('ROOT_PATH')/<dev_directory>' dir!

B<%CHMODS>: (hash of strings)

Hash of new access rights to set for files or directories.
Keys are files or directories (wildcard ok) and values are the chmod masks.

B<%CHOWNS>: (hash of strings)

Hash of new owner/group to set for files or directories.
Keys are files or directories (wildcard ok) and values are the 'owner:group' strings.

B<@PROD_FILES_TO_PROCESS>: (array of strings)

List of files to process in order to turn a development site into a production site.

B<%PROD_DIRFILES_TO_ADD>: (hash of strings)

Hash of directories or files to copy to the production directory.
This files can be located in the old prod directory at time of copy process.
Each key is a source directory or file and each value is the target.

B<%STRING_TRANSLATION>: (hash of strings)

Hash to associate a development string to a production string. Each
development string will be replaced by its associated production string.

=cut

our $BRANCHES    = 'branches';

our $DEFAULT_PROD = 'prod';
our $DEFAULT_DEV = 'dev';

our $SVN_EXPORT_COMMAND = "svn export --non-interactive ";

our @REMOVE_DIRECTORIES = (
    'admin',
    't',
);

our @REMOVE_FILES = (
    'lib/Greenphyl/Config_template.pm',
    'lib/Greenphyl/DBObjectTemplate.pmt',
    'lib/Greenphyl/generate_table_properties.pl',
    'templates/cache/*',
    'dev_howto.txt',
);

our @CREATE_DIRECTORIES = (
    'sessions',
    'tmp',
    'tmp/blast',
);

our %LINKS_TO_SET = (
  # 'link name' => 'link target',
    'data/alm'         => GP('ROOT_PATH') . '/data/alm',
    'data/lib'         => GP('ROOT_PATH') . '/data/lib',
    'data/meme'        => GP('ROOT_PATH') . '/data/meme',
    'data/trees'       => GP('ROOT_PATH') . '/data/trees',
    'htdocs/alm'       => GP('ROOT_PATH') . '/data/alm',
    'htdocs/meme'      => GP('ROOT_PATH') . '/data/meme',
#    'htdocs/positions' => GP('ROOT_PATH') . '/dev/data/positions',
#    'htdocs/chromosomes' => 'data/chromosomes',
    'htdocs/trees'     => GP('ROOT_PATH') . '/data/trees',
    'htdocs/tmp'       => 'tmp',
);

our %CHMODS = (
  # 'file/dir'            => 'chmod mask',
    'cgi-bin/*.cgi'       => 'ug+x',
    'cgi-bin/*.pl'        => 'ug+x',
    'admin/*.pl'          => 'ug+x',
#    GP('ROOT_PATH') . '/data/lib'   => 'g+w',
#    GP('ROOT_PATH') . '/data/meme'  => 'g+w',
#    GP('ROOT_PATH') . '/data/trees' => 'g+w',
    'templates/cache'     => 'g+w',
    'tmp'                 => 'g+w',
    'tmp/blast'           => 'g+w',
    'sessions'            => 'g+w',
);

our @PROD_FILES_TO_PROCESS = (
    #+FIXME: check config using check_config.pl
    #'lib/Greenphyl/Config.pm', # not transfered from dev but from previous prod: no need to process config
    'htdocs/css/config.less',
    'htdocs/css/greenphyl.less',
    'htdocs/css/custom.less',
    'htdocs/css/config.css',
    'htdocs/css/greenphyl.css',
    # update google analytics ID
    #'templates/global_header.tt',
    #'templates/global_footer.tt',
);

our %TRANSFER_PROD_FILES = (
    # transfer files from previous prod (nb.: can use absolute path for prod-independent files)
    'local_lib/Greenphyl/Config.pm' => 'local_lib/Greenphyl/Config.pm',
    # 'htdocs/css/config.less'        => 'htdocs/css/config.less',
    # 'htdocs/css/custom.less'        => 'htdocs/css/custom.less',
    # 'htdocs/css/greenphyl.css'      => 'htdocs/css/greenphyl.css',
    'htdocs/css/custom.css'         => 'htdocs/css/custom.css',
    'htdocs/robots.txt'             => 'htdocs/robots.txt',
);

our %STRING_TRANSLATION   = (
    'greenphyl_dev'              => 'greenphyl_prod', # database name
    'greenphyl-dev.cirad.fr'     => 'www.greenphyl.org', # URL
    'greenphyl/dev'              => 'greenphyl/prod', # path
    #'UA-6713098-1'               => 'UA-33499123-1', # google analytics
    #'background-color:\s*#CCFF99;' => 'background-color: #99CCFF;', # background color
);


1;

=pod

=head1 AUTHORS

Valentin GUIGNON (Bioversity), v.guignon@cgiar.org

=head1 VERSION

Version 2.0.0

Date 30/01/2013

=cut