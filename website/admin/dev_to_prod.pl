#!/usr/bin/perl

=pod

=head1 NAME

dev_to_prod.pl - Turn development files into production files.

=head1 SYNOPSIS

    dev_to_prod.pl -dev dev_val -prod prod_1234 -nolink

=head1 REQUIRES

Perl5

=head1 DESCRIPTION

This script edits development files from the specified directory and
turns what is development config into production config.
For instance, the development database name is changed to the
production database name. URL and path are also replaced.
See "%STRING_TRANSLATION" in "GreenphylDev.pm".

=cut

use strict;
use warnings;

use lib "../lib";
use lib '../local_lib';
use Carp qw (cluck confess croak);

use Greenphyl;
use Greenphyl::Log('nolog' => 1,);
use GreenphylDev;

use Getopt::Long;
use Pod::Usage;




# Script global constants
##########################

=pod

=head1 CONSTANTS

B<$DEBUG>: (boolean)

When set to true, it enables debug mode.

B<$CONSTANT_NAME>: ([constant nature]) #+++

[constant description and use]. #+++

=cut

our $DEBUG = 0;




# Script global variables
##########################

=pod

=head1 VARIABLES

B<[g_variable_name]>: ([variable nature]) #+++

[variable description, use and default value]. #+++
Default: [variable default value if one] #+++

#--- Example:
#--- B<$g_output_method>: (integer)
#---
#--- used to store current output method;
#--- 0=raw text (default), 1=image, 2=html.
#---
#---     ...
#---
#--- my $g_output_method = 0;
#---

=cut

#+++ my [$g_variable_name] = ["value"];




# Script global functions
##########################

=pod

=head1 FUNCTIONS

=head2 [SubName] #+++

B<Description>: [function description]. #+++

B<ArgsCount>: [count of arguments] #+++

=over 4

=item $variable_name: ([variable nature]) ([requirements]) #+++ see below

#--- requirement can be:
#--- (R)=required,
#--- (O)=optional
#--- (U)=optional and must be undef if omitted
[variable description]. #+++

=item $variable_name2: ([variable nature]) ([requirements]) #+++

[variable description]. #+++

=back

B<Return>: ([return type]) #+++

[return description]. #+++

B<Example>:

    [example code] #+++

=cut

sub CheckDirectories
{
    my ($dev_to_prod_parameters) = @_;

    # parameters check
    if (1 != @_)
    {
        confess "usage: CheckDirectories(dev_path, prod_path, branch_path);";
    }
    
    my $dev_path    = $dev_to_prod_parameters->{'dev_path'};
    my $prod_path   = $dev_to_prod_parameters->{'prod_path'};
    my $branch_path = $dev_to_prod_parameters->{'branch_path'};
    
    # check given parameters
    if (!$prod_path)
    {
        warn "ERROR: Production path not specified!\n";
        pod2usage(1);
}
    if (!$dev_path)
    {
        warn "ERROR: Development path not specified!\n";
        pod2usage(1);
    }

    # check for relative/absolute path
    if ($dev_path !~ m/^[\/\.]/)
    {
        # relative path
        $dev_path = GP('ROOT_PATH') . '/' . $dev_path;
    }
    if ($prod_path !~ m/^[\/\.]/)
    {
        # relative path
        $prod_path = GP('ROOT_PATH') . '/' . $prod_path;
    }
    if ($branch_path && ($branch_path !~ m/^[\/\.]/))
    {
        # relative path
        $branch_path = GP('ROOT_PATH') . "/$GreenphylDev::BRANCHES/" . $branch_path;
    }


    # check if production path already exists
    if (-e $prod_path)
    {
        confess "given production path already exists ('$prod_path')!\n";
    }

    # check if development path exists
    if (!-d $dev_path)
    {
        confess "given development path does not exist ('$dev_path')!\n";
    }
    elsif (!-r $dev_path)
    {
        confess "given development path is not readable ('$dev_path')!\n";
    }
    
    # check if branch path as been specified
    if (!$branch_path)
    {
        LogDebug('Branch ');
        # guess branch path
        $branch_path = $dev_path;
        $branch_path =~ s/.*\///;
        $branch_path = GP('ROOT_PATH') . "/$GreenphylDev::BRANCHES/$branch_path";
    }
    
    # check branch path
    if (!-d $branch_path)
    {
        confess "given branch path does not exist ('$branch_path')!\n";
    }
    elsif (!-r $branch_path)
    {
        confess "given branch path is not readable ('$branch_path')!\n";
    }

    # check prod link
    if ($dev_to_prod_parameters->{'prod_link'}
        && (-e $dev_to_prod_parameters->{'prod_link'})
        && !(-l $dev_to_prod_parameters->{'prod_link'}))
    {
        confess "given production link ('$dev_to_prod_parameters->{'prod_link'}') is not a link!";
    }

    LogDebug('Dev path: ' . $dev_path);
    LogDebug('Prod path: ' . $prod_path);
    LogDebug('Branch path: ' . $branch_path);
    $dev_to_prod_parameters->{'dev_path'} = $dev_path;
    $dev_to_prod_parameters->{'prod_path'} = $prod_path;
    $dev_to_prod_parameters->{'branch_path'} = $branch_path;
}



=pod

=head2 [SubName] #+++

B<Description>: [function description]. #+++

B<ArgsCount>: [count of arguments] #+++

=over 4

=item $variable_name: ([variable nature]) ([requirements]) #+++ see below

#--- requirement can be:
#--- (R)=required,
#--- (O)=optional
#--- (U)=optional and must be undef if omitted
[variable description]. #+++

=item $variable_name2: ([variable nature]) ([requirements]) #+++

[variable description]. #+++

=back

B<Return>: ([return type]) #+++

[return description]. #+++

B<Example>:

    [example code] #+++

=cut

sub ExportDev
{
    my ($dev_to_prod_parameters) = @_;

    # parameters check
    if (1 != @_)
    {
        confess "usage: ExportDev(dev_to_prod_parameters);";
    }
    
    my $cmd_return = 0;
    my $cmd_output = '';
    my $command;
    
    # export dev to prod
    LogInfo("Export dev directory to production path\n");
    $command = "$GreenphylDev::SVN_EXPORT_COMMAND $dev_to_prod_parameters->{'dev_path'} $dev_to_prod_parameters->{'prod_path'}";
    LogDebug("COMMAND:\n   $command\n");
    $cmd_output = `$command 2>&1`;
    LogInfo($cmd_output);
    if ($?)
    {
        confess "Export failed!\n$!\n";
}
    LogInfo("Done.\n\n");
}


=pod

=head2 [SubName] #+++

B<Description>: [function description]. #+++

B<ArgsCount>: [count of arguments] #+++

=over 4

=item $variable_name: ([variable nature]) ([requirements]) #+++ see below

#--- requirement can be:
#--- (R)=required,
#--- (O)=optional
#--- (U)=optional and must be undef if omitted
[variable description]. #+++

=item $variable_name2: ([variable nature]) ([requirements]) #+++

[variable description]. #+++

=back

B<Return>: ([return type]) #+++

[return description]. #+++

B<Example>:

    [example code] #+++

=cut

sub ExportBranch
{
    my ($dev_to_prod_parameters) = @_;

    # parameters check
    if (1 != @_)
    {
        confess "usage: ExportBranch(dev_to_prod_parameters);";
    }
    
    my $cmd_return = 0;
    my $cmd_output = '';
    my $command;
    
    # export dev branch to prod
    LogInfo("Export branch data to production path\n");
    $command = "$GreenphylDev::SVN_EXPORT_COMMAND --force $dev_to_prod_parameters->{'branch_path'}/website $dev_to_prod_parameters->{'prod_path'}";
    LogDebug("COMMAND:\n   $command\n");
    $cmd_output = `$command 2>&1`;
    LogInfo($cmd_output);
    if ($?)
    {
        confess "Branch export failed!\n$!\n";
}
    LogInfo("Done.\n\n");
}


=pod

=head2 [SubName] #+++

B<Description>: [function description]. #+++

B<ArgsCount>: [count of arguments] #+++

=over 4

=item $variable_name: ([variable nature]) ([requirements]) #+++ see below

#--- requirement can be:
#--- (R)=required,
#--- (O)=optional
#--- (U)=optional and must be undef if omitted
[variable description]. #+++

=item $variable_name2: ([variable nature]) ([requirements]) #+++

[variable description]. #+++

=back

B<Return>: ([return type]) #+++

[return description]. #+++

B<Example>:

    [example code] #+++

=cut

sub RemoveNonProductionDirectories
{
    my ($dev_to_prod_parameters) = @_;

    # parameters check
    if (1 != @_)
    {
        confess "usage: RemoveNonProductionDirectories(dev_to_prod_parameters);";
    }
    
    LogInfo("Remove non-production directories\n");

    # remove non-production directories
    foreach my $dir (@GreenphylDev::REMOVE_DIRECTORIES)
    {
        # check for absolute dir
        if ($dir !~ m/^[\/\.]/)
        {
            # relative dir
            $dir = "$dev_to_prod_parameters->{'prod_path'}/$dir";
        }
        if (-e $dir)
        {
            my $cmd_return = 0;
            my $cmd_output = '';
            my $command;

            LogInfo("   * '$dir'\n");
            $command = "rm -rf $dir";
            LogDebug("COMMAND:\n   $command\n");
            $cmd_return = system($command);
            if ($cmd_return)
            {LogWarning("Failed to remove directory '$dir'!\n");}
        }
    }
    LogInfo("Done.\n\n");
}


=pod

=head2 [SubName] #+++

B<Description>: [function description]. #+++

B<ArgsCount>: [count of arguments] #+++

=over 4

=item $variable_name: ([variable nature]) ([requirements]) #+++ see below

#--- requirement can be:
#--- (R)=required,
#--- (O)=optional
#--- (U)=optional and must be undef if omitted
[variable description]. #+++

=item $variable_name2: ([variable nature]) ([requirements]) #+++

[variable description]. #+++

=back

B<Return>: ([return type]) #+++

[return description]. #+++

B<Example>:

    [example code] #+++

=cut

sub RemoveNonProductionFiles
{
    my ($dev_to_prod_parameters) = @_;

    # parameters check
    if (1 != @_)
    {
        confess "usage: RemoveNonProductionFiles(dev_to_prod_parameters);";
    }
    
    LogInfo("Remove non-production files\n");

    # remove non-production files
    foreach my $file (@GreenphylDev::REMOVE_FILES)
    {
        # check for absolute path
        if ($file !~ m/^[\/\.]/)
        {
            # relative path
            $file = "$dev_to_prod_parameters->{'prod_path'}/$file";
        }
        if (-e $file)
        {
            my $cmd_return = 0;
            my $cmd_output = '';
            my $command;

            LogInfo("   * '$file'\n");
            $command = "rm -f $file";
            LogDebug("COMMAND:\n   $command\n");
            $cmd_return = system($command);
            if ($cmd_return)
            {LogWarning("Failed to remove file '$file'!\n");}
        }
    }
    LogInfo("Done.\n\n");
}


=pod

=head2 [SubName] #+++

B<Description>: [function description]. #+++

B<ArgsCount>: [count of arguments] #+++

=over 4

=item $variable_name: ([variable nature]) ([requirements]) #+++ see below

#--- requirement can be:
#--- (R)=required,
#--- (O)=optional
#--- (U)=optional and must be undef if omitted
[variable description]. #+++

=item $variable_name2: ([variable nature]) ([requirements]) #+++

[variable description]. #+++

=back

B<Return>: ([return type]) #+++

[return description]. #+++

B<Example>:

    [example code] #+++

=cut

sub AddMissingDirectories
{
    my ($dev_to_prod_parameters) = @_;

    # parameters check
    if (1 != @_)
    {
        confess "usage: AddMissingDirectories(dev_to_prod_parameters);";
    }

    LogInfo("Create missing directories\n");

    # create missing directories
    foreach my $dir (@GreenphylDev::CREATE_DIRECTORIES)
    {
        my $cmd_return = 0;
        my $cmd_output = '';
        my $command;

        # check for absolute dir
        if ($dir !~ m/^[\/\.]/)
        {
            # relative dir
            $dir = "$dev_to_prod_parameters->{'prod_path'}/$dir";
        }
        LogInfo("   * create '$dir'\n");
        $command = "mkdir -p $dir";
        LogDebug("COMMAND:\n   $command\n");
        if (!-d $dir)
        {
            $cmd_return = system($command);
            if ($cmd_return)
            {LogWarning("Failed to create directory '$dir'!\n");}
        }
        else
        {
            LogVerboseInfo("   NOTE: already exists, skipping...\n");
        }
    }
    LogInfo("Done.\n\n");
}


=pod

=head2 [SubName] #+++

B<Description>: [function description]. #+++

B<ArgsCount>: [count of arguments] #+++

=over 4

=item $variable_name: ([variable nature]) ([requirements]) #+++ see below

#--- requirement can be:
#--- (R)=required,
#--- (O)=optional
#--- (U)=optional and must be undef if omitted
[variable description]. #+++

=item $variable_name2: ([variable nature]) ([requirements]) #+++

[variable description]. #+++

=back

B<Return>: ([return type]) #+++

[return description]. #+++

B<Example>:

    [example code] #+++

=cut

sub AddMissingLinks
{
    my ($dev_to_prod_parameters) = @_;

    # parameters check
    if (1 != @_)
    {
        confess "usage: AddMissingLinks(dev_to_prod_parameters);";
    }

    LogInfo("Create missing links\n");

    # create missing links
    while (my ($link, $target_dir) = each(%GreenphylDev::LINKS_TO_SET))
    {
        my $cmd_return = 0;
        my $cmd_output = '';
        my $command;

        # check for absolute path
        if ($link !~ m/^[\/\.]/)
        {
            # relative path
            $link = "$dev_to_prod_parameters->{'prod_path'}/$link";
        }
        if ($target_dir !~ m/^[\/\.]/)
        {
            # relative path
            $target_dir = $dev_to_prod_parameters->{'prod_path'} . "/$target_dir";
        }
        LogInfo("   * link '$link'\n      --> '$target_dir'\n");
        $command = "ln -s $target_dir $link";
        LogDebug("COMMAND:\n   $command\n");
        if (!-l $link)
        {
            $cmd_return = system($command);
            if ($cmd_return)
            {LogWarning("WARNING: Failed to create link '$link' --> '$target_dir'!\n");}
        }
        else
        {
            LogVerboseInfo(" link already exists, skipping...\n");
        }
    }
    LogInfo("Done.\n\n");
}


=pod

=head2 [SubName] #+++

B<Description>: [function description]. #+++

B<ArgsCount>: [count of arguments] #+++

=over 4

=item $variable_name: ([variable nature]) ([requirements]) #+++ see below

#--- requirement can be:
#--- (R)=required,
#--- (O)=optional
#--- (U)=optional and must be undef if omitted
[variable description]. #+++

=item $variable_name2: ([variable nature]) ([requirements]) #+++

[variable description]. #+++

=back

B<Return>: ([return type]) #+++

[return description]. #+++

B<Example>:

    [example code] #+++

=cut

sub TransferProductionFiles
{
    my ($dev_to_prod_parameters) = @_;

    # parameters check
    if (1 != @_)
    {
        confess "usage: TransferProductionFiles(dev_to_prod_parameters);";
    }

    LogInfo("Transfer previous production files\n");
    # transfer needed previous prod files and directories
    while (my ($src_file_or_dir, $target_file_or_dir) = each(%GreenphylDev::TRANSFER_PROD_FILES))
    {
        my $cmd_return = 0;
        my $cmd_output = '';
        my $command;

        # check for absolute path
        if ($src_file_or_dir !~ m/^[\/\.]/)
        {
            # relative path
            # set default prod path as default
            my $previous_prod_path = GP('ROOT_PATH') . '/' . $GreenphylDev::DEFAULT_PROD;
            # and if available, use prod link instead
            if ($dev_to_prod_parameters->{'prod_link'}
                && -e $dev_to_prod_parameters->{'prod_link'})
            {
                $previous_prod_path = $dev_to_prod_parameters->{'prod_link'};
            }
            $src_file_or_dir = "$previous_prod_path/$src_file_or_dir";
        }

        if ($target_file_or_dir !~ m/^[\/\.]/)
        {
            # relative path
            $target_file_or_dir = "$dev_to_prod_parameters->{'prod_path'}/$target_file_or_dir";
        }

        LogInfo("   * copy '$src_file_or_dir'\n       to '$target_file_or_dir'\n");
        $command = "cp $src_file_or_dir $target_file_or_dir";
        LogDebug("COMMAND:\n   $command\n");
        $cmd_return = system($command);
        if ($cmd_return)
        {LogWarning("Failed to copy '$src_file_or_dir' to '$target_file_or_dir'!\n");}
    }
    LogInfo("Done.\n\n");
}


=pod

=head2 [SubName] #+++

B<Description>: [function description]. #+++

B<ArgsCount>: [count of arguments] #+++

=over 4

=item $variable_name: ([variable nature]) ([requirements]) #+++ see below

#--- requirement can be:
#--- (R)=required,
#--- (O)=optional
#--- (U)=optional and must be undef if omitted
[variable description]. #+++

=item $variable_name2: ([variable nature]) ([requirements]) #+++

[variable description]. #+++

=back

B<Return>: ([return type]) #+++

[return description]. #+++

B<Example>:

    [example code] #+++

=cut

sub PatchProductionFiles
{
    my ($dev_to_prod_parameters) = @_;

    # parameters check
    if (1 != @_)
    {
        confess "usage: PatchProductionFiles(dev_to_prod_parameters);";
    }

    # patch production file
    LogInfo("Patch production files\n");
    foreach my $prod_file (@GreenphylDev::PROD_FILES_TO_PROCESS)
    {
        my $fh;
        my $prod_file_data = '';
        # check for relative or absolute path to file
        if ($prod_file !~ m/^[\/\.]/)
        {
            $prod_file = "$dev_to_prod_parameters->{'prod_path'}/$prod_file";
        }
        LogInfo("   * $prod_file\n");

        # read current file
        if (open($fh, "<$prod_file"))
        {
            $prod_file_data = join('', <$fh>);
            close($fh);
        }
        else
        {
            LogWarning("Production failed: unable to open '$prod_file'!");
        }

        # replace what's needed
        while (my ($dev_string, $prod_string) = each (%GreenphylDev::STRING_TRANSLATION))
        {
            $prod_file_data =~ s/$dev_string/$prod_string/g;
        }

        # write the new file
        if ($prod_file_data && open($fh, ">$prod_file"))
        {
            print {$fh} $prod_file_data;
            close($fh);
        }
        else
        {
            LogWarning("Production failed: unable to open '$prod_file' for writing!");
        }
    }
    LogInfo("Done.\n\n");
}


=pod

=head2 [SubName] #+++

B<Description>: [function description]. #+++

B<ArgsCount>: [count of arguments] #+++

=over 4

=item $variable_name: ([variable nature]) ([requirements]) #+++ see below

#--- requirement can be:
#--- (R)=required,
#--- (O)=optional
#--- (U)=optional and must be undef if omitted
[variable description]. #+++

=item $variable_name2: ([variable nature]) ([requirements]) #+++

[variable description]. #+++

=back

B<Return>: ([return type]) #+++

[return description]. #+++

B<Example>:

    [example code] #+++

=cut

sub UpdateAccessRights
{
    my ($dev_to_prod_parameters) = @_;

    # parameters check
    if (1 != @_)
    {
        confess "usage: PatchProductionFiles(dev_to_prod_parameters);";
    }
    
    while (my ($file_or_dir, $chmod_mask) = each(%GreenphylDev::CHMODS))
    {
        my $cmd_return = 0;
        my $cmd_output = '';
        my $command;

       # check for absolute path
        if ($file_or_dir !~ m/^[\/\.]/)
        {
            # relative path
            $file_or_dir = "$dev_to_prod_parameters->{'prod_path'}/$file_or_dir";
        }
        LogInfo("   * chmod $chmod_mask $file_or_dir\n");
        $command = "chmod $chmod_mask $file_or_dir";
        LogDebug("COMMAND:\n   $command\n");
        $cmd_return = system($command);
        if ($cmd_return)
        {LogWarning("WARNING: Failed to chmod '$file_or_dir' (mask: $chmod_mask)!\n");}
    }
}


=pod

=head2 [SubName] #+++

B<Description>: [function description]. #+++

B<ArgsCount>: [count of arguments] #+++

=over 4

=item $variable_name: ([variable nature]) ([requirements]) #+++ see below

#--- requirement can be:
#--- (R)=required,
#--- (O)=optional
#--- (U)=optional and must be undef if omitted
[variable description]. #+++

=item $variable_name2: ([variable nature]) ([requirements]) #+++

[variable description]. #+++

=back

B<Return>: ([return type]) #+++

[return description]. #+++

B<Example>:

    [example code] #+++

=cut

sub UpdateProductionLink
{
    my ($dev_to_prod_parameters) = @_;

    # parameters check
    if (1 != @_)
    {
        confess "usage: UpdateProductionLink(dev_to_prod_parameters);";
    }
    
    my $prod_link = $dev_to_prod_parameters->{'prod_link'} || $GreenphylDev::DEFAULT_PROD;
    my $cmd_return = 0;
    my $cmd_output = '';
    my $command;
    
    LogInfo("Update prod link\n");
    $command = "rm $prod_link";
    LogDebug("COMMAND:\n   $command\n");
    $cmd_return = system($command);
    if ($cmd_return)
    {confess "Failed to remove old prod link!\n$!\n";}
    $command = "ln -s $dev_to_prod_parameters->{'prod_path'} $prod_link";
    LogDebug("COMMAND:\n   $command\n");
    $cmd_return = system($command);
    if ($cmd_return)
    {confess "Failed to create new prod link!\n$!\n";}
    LogInfo("Done.\n\n");

}

#+FIXME: SetProdOffline



# Script options
#################

=pod

=head1 OPTIONS

#--- describes parameters given to the script
#+++ command line syntax
#--- requirement of the option and its parameter can be:
#--- required: name or nature inside <>
#--- optional: name or nature inside []
#--- alternative between 2 elements: elements separated by a |

=head2 Parameters

=over 4

#+++=item B<-help>:
#+++
#+++Prints a brief help message and exits.
#+++
#+++=item B<-man>:
#+++
#+++Prints the manual page and exits.
#+++
#+++=item B<-debug> (integer):
#+++
#+++Executes the script in debug mode. The integer value is optional and is used
#+++to set debug level (use 0 to force disable debug mode).
#+++Default: 0 (not in debug mode).
#+++

=item B<[option_name]> ([option nature]): #+++

[option description]. #+++
Default: [option default value if one] #+++

#--- remove if log not used
#+++=item B<-log>:
#+++
#+++Enable logging.
#+++
#+++=item B<-log-*> (any):
#+++
#+++Logging arguments.

=back
#--- Example:
#---
#--- Template.pl [-help | -man]
#---
#--- Template.pl [-debug [debug_level]] [-size <width> [height]]
#---
#--- =over 4
#---
#--- =item B<-help>:
#---
#--- Prints a brief help message and exits.
#---
#--- =item B<-man>:
#---
#--- Prints the manual page and exits.
#---
#--- =item B<-debug> (integer):
#---
#--- Executes the script in debug mode. If an integer value is specified, it will
#--- be the debug level. If "-debug" option was used without specifying a debug
#--- level, level 1 is assumed.
#--- Default: 0 (not in debug mode).
#---
#---=item B<-size> (positive_real) (positive_real):
#---
#--- Set the dimensions of the object that will be drawn. The first value is
#--- the width; the height is the second value if specified, otherwise it will
#--- assume height and width are equal to the first value.
#--- Default: width and height are set to 1.
#---
#---=back

=cut


# CODE START
#############

# options processing
my ($man, $help, $debug) = (0, 0, undef);
my $dev_path = '';
my $prod_path = '';
my $branch_path = '';
my $prod_link = '';
my $no_link_update;

# parse options and print usage if there is a syntax error.
GetOptions('help|?'  => \$help,
           'man'     => \$man,
           'debug:s' => \$debug,
           'dev=s'   => \$dev_path,
           'prod=s'  => \$prod_path,
           'link=s'  => \$prod_link,
           'branch=s' => \$branch_path,
           'nolink|no-link-update' => \$no_link_update,
           @Greenphyl::Log::LOG_GETOPT, # skip log parameters
) or pod2usage(1);
if ($help) {pod2usage('-verbose' => 1, '-exitval' => 0);}
if ($man) {pod2usage('-verbose' => 2, '-exitval' => 0);}

# change debug mode if requested/forced
if (defined($debug))
{
    # just '-debug' ?
    if ($debug eq '')
    {
        $debug = 1;
    }
    $DEBUG = $debug;
}

#--- to log messages:
#--- LogInfo('Message');    # used for regular log messages
#--- LogVerboseInfo('Message'); # used for verbose log messages only when verbose mode is set;
#--- LogDebug('Message');   # should be used to only log debugging messages;
#--- LogWarning('Message'); # should be used to log warning messages;
#--- LogError('Message');   # should be used to log errors.

eval
{
    my $dev_to_prod_parameters = {
        'dev_path'    => $dev_path,
        'prod_path'   => $prod_path,
        'branch_path' => $branch_path,
        'prod_link'   => $prod_link,
    };

    # check dev and prod directories
    CheckDirectories($dev_to_prod_parameters);

    # export trunk into new prod
    ExportDev($dev_to_prod_parameters);

    # export dev-specific files into new prod
    ExportBranch($dev_to_prod_parameters);

    # remove non-production files and directories (tests,cache,...)
    RemoveNonProductionDirectories($dev_to_prod_parameters);
    RemoveNonProductionFiles($dev_to_prod_parameters);
    
    # add missing files/directories/links (tmp, sessions, data, config,...)
    AddMissingDirectories($dev_to_prod_parameters);
    AddMissingLinks($dev_to_prod_parameters);
    TransferProductionFiles($dev_to_prod_parameters);

    # generate/check CSS files
    #+FIXME: todo
    
    # update files content
    PatchProductionFiles($dev_to_prod_parameters);

    # update access right
    UpdateAccessRights($dev_to_prod_parameters);
    
    # update prod link?
    if (!$no_link_update)
    {
        UpdateProductionLink($dev_to_prod_parameters);
    }
};

# catch
HandleErrors();

exit(0);

# CODE END
###########


=pod

=head1 AUTHORS

Mathieu ROUARD (Bioversity), m.rouard@cgiar.org

Valentin GUIGNON (Bioversity), v.guignon@cgiar.org

Matthieu CONTE (Syngenta)

Sebastien BRIOIS (Syngenta)

=head1 VERSION

Version 1.0.0

Date 15/01/2013

=head1 SEE ALSO

GreenPhyl documentation.

=cut
