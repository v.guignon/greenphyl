#!/usr/bin/env perl

=pod

=head1 NAME

transfer_ipr_to_family.pl - Annotate unannotated families with IPR domain.

=head1 SYNOPSIS

    transfer_ipr_to_family.pl

=head1 REQUIRES

Perl5

=head1 DESCRIPTION

-transfer_ipr_to_family

=cut

use strict;
use warnings;

use FindBin;
use lib "$FindBin::Bin/../../lib";
use lib "$FindBin::Bin/../../local_lib";
use Carp qw (cluck confess croak);

use Greenphyl;
use Greenphyl::Log('nolog' => 1,);
use Greenphyl::Family;
use Greenphyl::Tools::Families;

use Getopt::Long;
use Pod::Usage;

++$|; #no buffering




# Script global constants
##########################

=pod

=head1 CONSTANTS

B<$REV_STRING>: (string)

Current revision (auto-set by GIT).

B<$DEBUG>: (boolean)

When set to true, it enables debug mode.

=cut

# SVN management part
my $REV_STRING = '$id$';

our $DEBUG = 0;




# Script global variables
##########################

=pod

=head1 VARIABLES

B<$g_dbh>: (DBI::db)

Global database handler.

=cut

my $g_dbh = GetDatabaseHandler();
my $g_updated_annotations = 0;




# Script global functions
##########################

=pod

=head1 FUNCTIONS

=head2 TryTransferIPRAnnotation

B<Description>: tries to transfer IPR annotation.

B<ArgsCount>: 0

B<Return>: nothing

=cut

sub TryTransferIPRAnnotation
{
    my ($family) = @_;

    if (!$family)
    {
        confess "ERROR: Missing parameters for TryTransferIPRAnnotation!\n";
    }

    LogVerboseInfo("Working on family " . $family->accession  . " (" . $family->id . ")");
    # Check if already annotated.
    if ($family->isAnnotated)
    {
        LogDebug("Family skipped as already annotated (" . $family->name . ").");
        return;
    }
    
    # Get family's main IPR family if one.
    my $ipr_stats = $family->ipr_stats();
    my $sql_query = "
        SELECT
          i.description
        FROM families_ipr_cache fic
            JOIN ipr i ON (i.id = fic.ipr_id)
        WHERE
          fic.family_id = " . $family->id . "
          AND fic.family_specific = 1
          AND fic.percent >= 55
          AND i.type = 'Family'
        ORDER BY fic.percent DESC
        LIMIT 1
    ;";

    my ($ipr_domain) = $g_dbh->selectrow_array($sql_query);

    if ($ipr_domain)
    {
        # We got a domain.
        # Check family level.
        if (1 < $family->level)
        {
            # Check parent families.
            my @parent_families = @{$family->getAncestorFamilies()};
            my $parent_family;
            while ($ipr_domain && ($parent_family = pop(@parent_families)))
            {
                if ($parent_family->name =~ m/\Q$ipr_domain\E/i)
                {
                    LogVerboseInfo('IPR annotation not transfered to family ' . $family->accession . ' because parent family already has the IPR annotation.');
                    $ipr_domain = '';
                }
            }
        }

        # If we still have a domain, transfer annotation.
        if ($ipr_domain)
        {
            $family->name($ipr_domain);
            $family->validated('normal');
            $family->inferences('IEA:InterPro');
            $family->save();
            ++$g_updated_annotations;
            LogInfo("Updated family " . $family->accession . " (" . $family->id . ") annotation with IPR '$ipr_domain'");
        }
    }
}




# Script options
#################

=pod

=head1 OPTIONS

    transfer_ipr_to_family.pl [-help|-man]
    transfer_ipr_to_family.pl [-debug] [family selectors]


=head2 Parameters

=over 4

=item B<-help>:

Prints a brief help message and exits.

=item B<-man>:

Prints the manual page and exits.

=item B<-debug> (integer):

Executes the script in debug mode. The integer value is optional and is used
to set debug level (use 0 to force disable debug mode).
Default: 0 (not in debug mode).

=item B<-log>:

Enable logging.

=item B<-log-*> (any):

Logging arguments.

=back

=cut


# CODE START
#############

# for regression tests
if ($ENV{'TESTING'} && $ENV{'TESTING_NO_RUN'})
{return 1;}

# options processing
my ($man, $help, $debug, $no_prompt) = (0, 0, undef, undef);

GetOptions('help|?'     => \$help,
           'man'        => \$man,
           'debug:s'    => \$debug,
           'q|noprompt' => \$no_prompt,
           @Greenphyl::Log::LOG_GETOPT, # skip log parameters
) or pod2usage(1);
if ($help) {pod2usage('-verbose' => 1, '-exitval' => 0);}
if ($man) {pod2usage('-verbose' => 2, '-exitval' => 0);}

# change debug mode if requested/forced
if (defined($debug))
{
    # just '-debug' ?
    if ($debug eq '')
    {
        $debug = 1;
    }
    $DEBUG = $debug;
}

eval
{
    LogDebug('Running in debug mode...');
    LoopOnFamilyLists(
        sub
        {
            my ($selected_families) = @_;
            
            foreach my $family (@$selected_families)
            {
                TryTransferIPRAnnotation($family);
            }

        }
    );
};
LogInfo("Done. Updated families: $g_updated_annotations");

my $error;

if ($error = Exception::Class->caught())
{
    warn "An error occured!\n" . $error;
}


exit(0);

# CODE END
###########


=pod

=head1 AUTHORS

Mathieu ROUARD (Bioversity), m.rouard@cgiar.org

Valentin GUIGNON (Bioversity), v.guignon@cgiar.org

=head1 VERSION

Version 1.0.0

Date 07/07/2020

=head1 SEE ALSO

GreenPhyl documentation.

=cut
