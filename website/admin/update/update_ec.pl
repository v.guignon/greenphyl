#!/usr/bin/perl

=pod

=head1 NAME

update_ec.pl - update EC data

=head1 SYNOPSIS

    update_ec.pl

=head1 REQUIRES

Perl5, GreenPhyl API v3

=head1 DESCRIPTION

This script updates EC data in the following tables:
-ec_go
-dbxref
-dbxref_sequences

It requiers the following tables to be filled:
-go_sequences_cache


=cut

use strict;
use warnings;

use FindBin;
use lib "$FindBin::Bin/../../lib";
use lib "$FindBin::Bin/../../local_lib";
use Carp qw (cluck confess croak);

use Getopt::Long;
use Pod::Usage;
use Data::Dumper; #+debug

use Greenphyl;
use Greenphyl::Log('nolog' => 1,);
use Greenphyl::SourceDatabase;
use Greenphyl::DBXRef;
use Greenphyl::GO;
use Greenphyl::Tools::GO;

++$|; #no buffering



# Script global constants
##########################

=pod

=head1 CONSTANTS

B<$REV_STRING>: (string)

Current revision (auto-set by GIT).

B<$DEBUG>: (boolean)

When set to true, it enables debug mode.

=cut

# SVN management part
my $REV_STRING = '$id$';

our $DEBUG = 0;
our $EC_DB_NAME        = 'EC';
our $EC_DB_DESCRIPTION = 'KEGG Enzyme';




# Script global variables
##########################

=pod

=head1 VARIABLES

B<$g_dbh>: (DBI::db)

Global database handler.

=cut

my $g_dbh = GetDatabaseHandler();
my $g_no_prompt = undef;




# Script global functions
##########################

=pod

=head1 FUNCTIONS

=head2 GetECDB

B<Description>: Updates and return GreenPhyl source database object for EC. If
it is not found, a new database entry is created.

B<ArgsCount>: 0

B<Return>: (Greenphyl::SourceDatabase)

Source database (from "db" table) object corresponding to EC database.

=cut

sub GetECDB
{
    # insert or update EC DB
    my $sql_query = "
        INSERT INTO db (id, name, description, query_url_prefix, site_url)
        VALUE (DEFAULT, '$EC_DB_NAME', '$EC_DB_DESCRIPTION', ?, ?)
        ON DUPLICATE KEY UPDATE description = '$EC_DB_DESCRIPTION', query_url_prefix = ?, site_url = ?;
    ";
    LogDebug("SQL QUERY: $sql_query\nBindings: " . join(', ', GetURL('fetch_ec'), GetURL('kegg'), GetURL('fetch_ec'), GetURL('kegg')));
    $g_dbh->do($sql_query, undef, GetURL('fetch_ec'), GetURL('kegg'), GetURL('fetch_ec'), GetURL('kegg'))
        or confess LogError("Failed to insert external source database:\n" . $g_dbh->errstr);
    my $ec_db = Greenphyl::SourceDatabase->new($g_dbh, {'selectors' => {'name' => 'EC'}})
        or confess LogError("Failed to load external source database object from database for EC!");

    return $ec_db;
}


=pod

=head2 ClearECData

B<Description>: Removes EC-related data from GreenPhyl database.

B<ArgsCount>: 1

=over 4

=item $ec_db: (Greenphyl::SourceDatabase) (R)

Source database (from "db" table) object corresponding to EC database.

=back


B<Return>: nothing

=cut

sub ClearECData
{
    my ($ec_db) = @_;

    # clear EC DBXRef and also related data at the same time
    my $sql_query = "DELETE FROM dbxref WHERE db_id = " . $ec_db->id . ";";
    $g_dbh->do($sql_query)
        or confess LogError("Failed to clear EC data:\n" . $g_dbh->errstr);
}


=pod

=head2 GetECDefinitions

B<Description>: Returns a hash of EC definitions. Loads definitions from EC data
file and ask to download/update it from the web if needed.

B<ArgsCount>: 0

B<Return>: (hash ref)

A hash of EC definitions: keys are EC numbers (ie. '1.1.1.1') and values are
definitions (string).

=cut

sub GetECDefinitions
{
    my ($ec_file_path) = @_;
    my $ec_definitions = {};
    LogDebug("Loading EC Definitions...");
    eval
    {
        # get file
        $ec_file_path = GetWebFile({
            'file_path' => $ec_file_path,
            'default_file_path' => GP('EC_DEFINITION_FILE_PATH'),
            'source_url' => GetURL('ec_definition'),
            'file_description' => 'EC definition file',
            'no_prompt' => $g_no_prompt,
        });
        
        # parse file
        my $ec_fh;
        if (open($ec_fh, $ec_file_path))
        {
            my ($current_id, $description) = (0, '');
            while (my $line = <$ec_fh>)
            {
                if ($line =~ m/^\/\//)
                {
                    if ($current_id && length($description))
                    {
                        # save current EC
                        $ec_definitions->{$current_id} = $description;
                        LogDebug($current_id . ": " . $description);
                    }
                    ($current_id, $description) = (0, '');
                }
                elsif ($line =~ m/^ID\s+([\d\.]+)/)
                {
                    $current_id = $1;
                }
                elsif ($line =~ m/^DE\s+(.*)/)
                {
                    $description .= $1;
                }
            }

            # save last EC description
            if ($current_id && length($description))
            {
                # save current EC
                $ec_definitions->{$current_id} = $description;
                LogDebug($current_id . ": " . $description);
            }

            close($ec_fh);
        }
    };
    LogDebug("...end loading EC definitions.");

    if ($@)
    {
        LogWarning("An error occurred during EC definition loading:\n$@");
    }

    return $ec_definitions;
}


=pod

=head2 LoadEC

B<Description>: Insert a new EC DBXRef in database if missing and returns it.

B<ArgsCount>: 4

=over 4

=item $ec_xref: (GO::Model::Xref) (R)

the EC cross-reference.

=item $ec_object_hash: (hash ref) (R)

Hash of GreenPhyl::DBXRef object corresponding to EC already loaded.

=item $ec_definitions: (hash) (R)

Hash associating an EC accession to its definition.

=item $ec_db: (Greenphyl::SourceDatabase) (R)

Source database (from "db" table) object corresponding to EC database.

=back

B<Return>: (Greenphyl::DBXRef)

The correspondig existing or inserted GreenPhyl DBXRef object (for EC).

=cut

sub LoadEC
{
    my ($ec_xref, $ec_object_hash, $ec_definitions, $ec_db) = @_;

    # check if EC has already been processed
    if (!exists($ec_object_hash->{$ec_xref->xref_key}))
    {
        LogDebug("Adding missing EC:" . $ec_xref->xref_key . " to database.");
        my $ec_accession = $ec_xref->xref_key;
        my $ec_description = $ec_definitions->{$ec_xref->xref_key} || '';

        my $sql_query = "
            INSERT INTO dbxref (db_id, accession, description)
            VALUE (" . $ec_db->id . ", ?, ?)
            ON DUPLICATE KEY UPDATE description = ?
        ;";
        LogDebug("SQL QUERY: $sql_query\nBindings: " . join(', ', $ec_accession, $ec_description, $ec_description,));
        $g_dbh->do($sql_query, undef, $ec_accession, $ec_description, $ec_description,)
            or confess LogError("Failed to insert EC DBXRef:\n" . $g_dbh->errstr);
        my $ec = Greenphyl::DBXRef->new($g_dbh, {'selectors' => {'db_id' => $ec_db->id, 'accession' => $ec_accession}})
            or confess LogError("Failed to load EC ($ec_accession) DBXRef object from database!");
        $ec_object_hash->{$ec_xref->xref_key} = $ec;
    }

    return $ec_object_hash->{$ec_xref->xref_key};
}


=pod

=head2 GetGO2ECVersion

B<Description>: Returns GO2EC file version.

B<ArgsCount>: 1

=over 4

=item $file_path: (string)

Path to GO2EC file.

=back

B<Return>: (string)

The version.

=cut

sub GetGO2ECVersion
{
    my ($file_path) = @_;
    my ($fh, $version);
    if (open($fh, $file_path))
    {
        # EC format: ! version: $Revision$
        #            ! date: $Date: 2012/06/19 19:38:05 $
        ($version) = (<$fh> =~ m/!\s*version:\s*\$Revision:\s*(\S+)/i);
        close($fh);
    }
    return $version;
}


=pod

=head2 ProcessGO2ECEntries

B<Description>: store relevant EC-GO association in database.

B<ArgsCount>: 3

=over 4

=item $ec_go_graph: (GO::Model::Graph) (R)

GO graph object containg EC associations.

=item $go2ec_file_path: (string) (R)

Path to GO cross-ref file.

=item $ec_definitions: (hash) (R)

Hash associating an EC accession to its definition.

=item $ec_db: (Greenphyl::SourceDatabase) (R)

Source database (from "db" table) object corresponding to EC database.

=back

B<Return>: nothing

=cut

sub ProcessGO2ECEntries
{
    my ($ec_go_graph, $ec_definitions, $ec_db) = @_;
    
    # hash for loaded EC objects
    my $ec_object_hash = {};

    # process each cross-ref entry
    foreach my $ec_go_term (@{$ec_go_graph->get_all_nodes()})
    {
        LogDebug("Working on " . $ec_go_term->acc);
        if (my $go = Greenphyl::GO->new($g_dbh, {'selectors' => {'code' => $ec_go_term->acc,}}))
        {
            # loop on each cross-ref
            if ($ec_go_term->dbxref_list())
            {
                LogDebug("Looping on " . scalar(@{$ec_go_term->dbxref_list()}) . " cross-reference(s) of " . $ec_go_term->acc);
                foreach my $ec_xref (@{$ec_go_term->dbxref_list()})
                {
                    LogVerboseInfo("Working on EC cross-ref " . $ec_xref->xref_key);
                    # store info
                    my $ec = LoadEC($ec_xref, $ec_object_hash, $ec_definitions, $ec_db);

                    my $sql_query = "INSERT INTO ec_go (dbxref_id, go_id) VALUES (?, ?);";
                    LogDebug("SQL QUERY: $sql_query\nBindings: " . join(', ', $ec->id, $go->id));
                    $g_dbh->do($sql_query, undef, $ec->id, $go->id)
                        or LogError("Failed to add EC cross-reference for GO '$go':\n" . $g_dbh->errstr);

                    $sql_query = "UPDATE go_sequences_cache SET ec_id = ? WHERE go_id = ?;";
                    LogDebug("SQL QUERY: $sql_query\nBindings: " . join(', ', $ec->id, $go->id));
                    $g_dbh->do($sql_query, undef, $ec->id, $go->id)
                        or LogError("Failed to add sequence EC cross-reference for GO '$go':\n" . $g_dbh->errstr);
                }
            }
            else
            {
                LogWarning('No cross-references found for ' . $ec_go_term->acc . '!');
            }
        }
        else
        {
            LogDebug('No corresponding GO in database for GO ' . $ec_go_term->acc . '. Skipping.');
        }
    }
}




# Script options
#################

=pod

=head1 OPTIONS

    update_ec.pl [-help|-man]
    update_ec.pl [-debug] [db_index=<db_index>]

=head2 Parameters

=over 4

=item B<-help>:

Prints a brief help message and exits.

=item B<-man>:

Prints the manual page and exits.

=item B<-debug> (integer):

Executes the script in debug mode.

=item B<db_index> (integer):

Index of the database to use (see Greenphyl::Config).
Default: 0

=back

=cut


# CODE START
#############

# for regression tests
if ($ENV{'TESTING'} && $ENV{'TESTING_NO_RUN'})
{return 1;}

# options processing
my ($man, $help, $debug) = (0, 0, undef, undef);
my $obo_file_path;
my $ipr2go_file_path;
my $uniprot2go_file_path;
my $go2ec_file_path;
my ($no_clear_ec, $no_ipr, $no_uniprot, $no_ec);
# parse options and print usage if there is a syntax error.
GetOptions('help|?'           => \$help,
           'man'              => \$man,
           'debug:s'          => \$debug,
           'q|noprompt'       => \$g_no_prompt,
           'go2ecfile=s'      => \$go2ec_file_path,
           'noclear'          => \$no_clear_ec,
           @Greenphyl::Log::LOG_GETOPT, # skip log parameters
) or pod2usage(1);
if ($help) {pod2usage('-verbose' => 1, '-exitval' => 0);}
if ($man) {pod2usage('-verbose' => 2, '-exitval' => 0);}

# change debug mode if requested/forced
if (defined($debug))
{
    # just '-debug' ?
    if ($debug eq '')
    {
        $debug = 1;
    }
    $DEBUG = $debug;
}

eval
{
    LogInfo("Update EC...");

    # start SQL transaction
    if ($g_dbh->{'AutoCommit'})
    {
        $g_dbh->begin_work() or croak $g_dbh->errstr;
    }

    LogDebug("Running in DEBUG mode, no database commit will be issued!");

    # Check if EC source database is available
    my $ec_db = GetECDB();
    
    # clear previous data if needed
    if (!$no_clear_ec
        && Prompt("Clear current EC data? (y/n)", { 'default' => 'y', 'constraint' => '[yYnN]' }, $g_no_prompt) =~ m/y/i)
    {
        ClearECData($ec_db);
    }
    
    # get EC definitions
    my $ec_definitions = GetECDefinitions();
    LogDebug("Loaded " . scalar(keys(%$ec_definitions)) . " EC definitions.");

    # check input file
    $go2ec_file_path = GetWebFile({
        'file_path' => $go2ec_file_path,
        'default_file_path' => GP('EC_TO_GO_FILE_PATH'),
        'source_url' => GetURL('ec_to_go'),
        'file_description' => 'EC to GO cross-reference file',
        'no_prompt' => $g_no_prompt,
    });

    # get version
    my $version = GetGO2ECVersion($go2ec_file_path) || 'unknown';
    LogInfo("GO2EC version: $version");
    my $sql_query = "INSERT INTO variables (name, value) VALUES ('ec_version', ?) ON DUPLICATE KEY UPDATE value = ?;";
    LogDebug("SQL QUERY: $sql_query\nBindings: " . join(', ', $version, $version));
    if (!$g_dbh->do($sql_query, undef, $version, $version))
    {
        LogWarning("Failed to store GO2EC version ($version)!" . $g_dbh->errstr);
    }

    # load GO2EC data
    my $ec_go_graph = ParseOBOToGraph('go_xref', $go2ec_file_path);
    LogDebug("Loadded " . scalar(@{$ec_go_graph->get_all_nodes}) . " GO2EC entries.");

    # process GO2EC
    ProcessGO2ECEntries($ec_go_graph, $ec_definitions, $ec_db);

    if (!$DEBUG)
    {SaveCurrentTransaction($g_dbh);}

    LogInfo("Done.");
};

HandleErrors();

exit(0);

# CODE END
###########


=pod

=head1 AUTHORS

Mathieu ROUARD (Bioversity), m.rouard@cgiar.org

Valentin GUIGNON (Bioversity), v.guignon@cgiar.org

Matthieu CONTE (Syngenta)

Sebastien BRIOIS (Syngenta)

=head1 VERSION

Version 1.0.0

Date 25/02/2013

=head1 SEE ALSO

GreenPhyl documentation.
http://search.cpan.org/~cmungall/go-perl-0.14/
http://search.cpan.org/~cmungall/go-perl-0.14/GO/Parser.pm
http://search.cpan.org/~cmungall/go-perl-0.14/GO/Model/Graph.pm
http://search.cpan.org/~cmungall/go-perl-0.14/GO/Model/Term.pm


=cut
