#!/usr/bin/perl

=pod

=head1 NAME

fix_mcl_pyramids.pl - Diagnose and fix MCL clustering problems (revert-pyramids)

=head1 SYNOPSIS

    fix_mcl.pl 2

=head1 REQUIRES

Perl5

=head1 DESCRIPTION

This script can be used to find and often fix "revert-pyramid" clustering
issues. It can detects families of level 2 or 3 or 4 having more than one
parent family in the level above. It can fix some kinds of bad sequence
clustering by reassigning sequences to the more appropriate families.

This script should be run several times in a given order:
-level 4
-level 3
-level 2
-level 4
-level 3
-level 2
-... until no more revert-pyramids are fixed (no reassigned sequences)
Then
-level 4 -force
-level 3 -force
-level 2 -force
-... until no more revert-pyramids are fixed (no reassigned sequences).

=cut

use strict;
use warnings;

use FindBin;
use lib "$FindBin::Bin/../../lib"; #+++ change path if in sub-directories
use lib "$FindBin::Bin/../../local_lib";
use Carp qw (cluck confess croak);

use Getopt::Long;
use Pod::Usage;

use Greenphyl;
use Greenphyl::Log('nolog' => 1,);
use Greenphyl::Family;
use Greenphyl::AbstractFamily;

++$|;




# Script global constants
##########################

=pod

=head1 CONSTANTS

B<$REV_STRING>: (string)

Current revision (auto-set by GIT).

B<$DEBUG>: (boolean)

When set to true, it enables debug mode.

=cut

# SVN management part
my $REV_STRING = '$id$';

our $DEBUG = 0;




# Script global variables
##########################

=pod

=head1 VARIABLES

B<$g_dbh>: (DBI::db)

Global database handler.

=cut

my $g_dbh = GetDatabaseHandler();




# Script global functions
##########################

=pod

=head1 FUNCTIONS

=head2 FixPyramidFamilies

B<Description>: Tries to resolve inverted pyramids focusing at a given level L
by moving sequences in the appropriate families.

Here is a schema of a common case where:
- "S" is the sequence of interest;
- the stream of "S" is symbolized by a simple line (either "|", "/" or "\");
- the main stream of sequences between families are symbolized by "#";
- "Y" is the family of interest at the specified level L (here L=2);
- "M" is the Main (largest) parent family of "Y";
- "N" is a miNor parent family of "Y" containing "S";
- "C" is the main Child family of "Y";
- "SeqC" (sequence Child family) is the family of S at level L+1;
- "SeqS" is a Sibbling family of "Y" (where "S" should/could be moved);
- "SeqP" (sequence Parent family) is the main parent family of "SeqS";
- "*bis" parent as the largest parent of a family if one, or if there
  is a tie, the parent in which sequences from the family represent the highest
  percent in the parent, or if there is still a tie, the one with the smallest
  ID.

level 1
        Family M    Family N    Family SeqP
           #     S/                #
level 2    #     /                 #
        Family Y          Family SeqS
           #    S\           #
level 3    #      \          #
        Family C    Family SeqC


CASE 0 (No "M"):
----------------
level 1
        Family *    Family N
           |     S/
level 2    |     /
        Family Y


--> If forced, use "Mbis" as "M" and follow other cases.



CASE 1 ("M" and no child family or C == SeqC):
----------------------------------------------
level 1
        Family M    Family N
           #     S/
level 2    #     /
        Family Y
          (#  S|)
(level 3) (#   |)
       (Family C/SeqC)
--> Transfert sequence S from N to M


CASE 2 (S not in "C", "SeqS" main parent is "SeqP" and "N" == "SeqP"):
----------------------------------------------------------------------
level 1
        Family M    Family N/SeqP
           #     S/    #
level 2    #     /     #
        Family Y    Family SeqS
           #    S\     #
level 3    #      \    #
        Family C    Family SeqC
--> Transfert sequence S from Y to SeqS


CASE 3 (S not in "C", "SeqS" main parent is "SeqP" and "N" <> "SeqP"):
----------------------------------------------------------------------
level 1
        Family M   Family N   Family SeqP
           #     S/              #
level 2    #     /               #
        Family Y        Family SeqS
           #    S\         #
level 3    #      \        #
        Family C    Family SeqC
--> Transfert sequence S from Y to SeqS and from N to SeqP


CASE 4 (S not in "C", no "SeqP"):
-------
level 1
        Family M    Family N (no "Family SeqP")
           #     S/
level 2    #     /
        Family Y    Family SeqS
           #    S\     #
level 3    #      \    #
        Family C    Family SeqC
--> If forced, use SeqPbis as SeqP and follow other cases.


CASE 5 (SeqC with no SeqS):
-------
level 1
        Family M    Family N
           #     S/
level 2    #     /
        Family Y    (no "Family SeqS")
           #    S\
level 3    #      \
        Family C    Family SeqC
--> If forced, use SeqSbis as SeqS and follow other cases.


B<ArgsCount>: 1-2

=over 4

=item $family: (Greenphyl::Family) (R)

Pyramid family to resolve.

=item $force: (boolean) (O)

If true, force resolution in ambiguous cases.

=back

B<Return>: (hash ref)

Hash of resolution statistics. Keys are: 'unsolvable_families',
'unresolved_sequences' and 'resolved_sequences'.

=cut

sub FixPyramidFamilies
{
    my ($level, $force) = @_;

    my %statistics = (
        'resolved_sequences'   => [],
        'unresolved_sequences' => [],
        'unsolvable_families'  => [],
        'family_changes'       => {},
    );

    # parameters check
    if (!$level)
    {
        confess LogError("No level to process!");
    }

    # select all the families at level n with more than 1 group at level n-1
    my $sql_query = qq{
        SELECT child_f.id AS "id"
        FROM families child_f
            JOIN family_relationships_cache frc ON (frc.subject_family_id = child_f.id)
        WHERE
            child_f.level = $level
            AND frc.level_delta = -1
            AND frc.type = '$Greenphyl::AbstractFamily::RELATIONSHIP_INHERITANCE'
        GROUP BY child_f.id
        HAVING COUNT(DISTINCT frc.object_family_id) > 1
        ORDER BY child_f.id
    };

    my $pyramid_families = $g_dbh->selectcol_arrayref($sql_query);

    LogInfo('Processing ' . scalar(@$pyramid_families) . " revert-pyramid families (level $level) (ie. with more than one parent)...");
    # loop on each family with more than one parent family
    foreach my $pyramid_family_id (@$pyramid_families)
    {
        # load "revert-pyramid" family of interest "Y"
        my $family = Greenphyl::Family->new(
            $g_dbh,
            {
                'selectors' => {
                    'id' => $pyramid_family_id,
                },
            }
        );
        LogVerboseInfo("Working on family $family");

        # Get main parent "M"
        my $main_parent = GetMainParent($family, $force);

        # check if we really got a main parent (containg more than 50% of current family)
        if ($main_parent)
        {
            # Get all parents sorted by the maximum number of common sequences,
            # excluding main parent.
            my @parents = sort
                {$family->getCommonSequenceCount($b) <=> $family->getCommonSequenceCount($a)}
                (grep
                    {$_->id != $main_parent->id}
                    @{$family->parents()})
            ;
            # here, @parents contains only minor parents "N"

            LogDebug("Got main parent: $main_parent");
            # get child families "C" for which current family is the "main" parent
            my @real_children = grep
                {
                    ($_->countSequences()/2.) < $family->getCommonSequenceCount($_)
                }
                @{$family->children()}
            ;

            # loop on minor parents N
            foreach my $minor_parent (@parents)
            {
                # loop on common sequences "Seq" (nb.: counts are automatically stored in cache)
                foreach my $sequence (@{$family->getCommonSequences($minor_parent)})
                {
                    # check if a child family "SeqC" of Y also contains Seq
                    my ($seq_child_family, @rest_fam) = @{$sequence->fetchFamilies({'selectors' => {'level' => $level + 1}})};
                    if (@rest_fam)
                    {
                        confess LogError("Unexpected: sequence '$sequence' in more than one family at level " . ($level + 1) . "! " . join(', ', $seq_child_family, @rest_fam));
                    }
                    # nb. if there is a child family at level "$level + 1", it must be, by construction, a child of Y
                    if ($seq_child_family)
                    {
                        my ($matching_child_family) = grep
                            {$_->id == $seq_child_family->id}
                            @{$family->children()}
                        ;
                        if (!$matching_child_family)
                        {
                            LogWarning("Unexpected: sub-family $seq_child_family was not a child of family $family while they both share sequence '$sequence'!");
                        }
                    }
                    # if Seq does not belong to any child family (of Y)
                    # or if Seq belongs to C
                    if (!$seq_child_family
                        || (@real_children
                            && (grep {$seq_child_family->id == $_->id} @real_children)
                           )
                       )
                    {
                        # transfer Seq from N to M
                        TransferSequence($sequence, $minor_parent, $main_parent, $statistics{'family_changes'});
                        LogVerboseInfo("CASE 1: Transfering sequence '$sequence' from minor parent '$minor_parent' to major parent '$main_parent'");
                        push(@{$statistics{'resolved_sequences'}}, $sequence);
                    }
                    else
                    {
                        # SeqC is not in C
                        # Seq might have to be transfered from Y to another sibling family

                        # find main parent "SeqS" (sibling of Y) of SeqC
                        my $seq_sibling_family = GetMainParent($seq_child_family, $force);

                        # if there is a SeqS
                        if ($seq_sibling_family)
                        {
                            # get SeqS main parent "SeqP"
                            my $seq_parent_family = GetMainParent($seq_sibling_family, $force);

                            if ($seq_parent_family)
                            {
                                # check if N is the main parent of SeqS
                                if ($seq_parent_family->id == $minor_parent->id)
                                {
                                    # transfer Seq from Y to SeqS
                                    TransferSequence($sequence, $family, $seq_sibling_family, $statistics{'family_changes'});
                                    LogVerboseInfo("CASE 2: Transfering sequence '$sequence' from '$family' to sibling family '$seq_sibling_family'");
                                    push(@{$statistics{'resolved_sequences'}}, $sequence);
                                }
                                else
                                {
                                    # SeqS is not a (real) child of N
                                    # transfer Seq from Y to SeqS and from N to SeqP
                                    TransferSequence($sequence, $family, $seq_sibling_family, $statistics{'family_changes'});
                                    TransferSequence($sequence, $minor_parent, $seq_parent_family, $statistics{'family_changes'});
                                    LogVerboseInfo("CASE 3: Transfering sequence '$sequence' to family '$seq_sibling_family' and parent family '$seq_parent_family'");
                                    push(@{$statistics{'resolved_sequences'}}, $sequence);
                                }
                            }
                            else
                            {
                                LogVerboseInfo("CASE 4: Unsolved: sequence '$sequence' can not be related to other families at that stage");
                                push(@{$statistics{'unresolved_sequences'}}, $sequence);
                            }
                        }
                        else
                        {
                            LogVerboseInfo("CASE 5: Unsolved: child family '$seq_child_family' with current sequence '$sequence' has no major parent");
                            push(@{$statistics{'unresolved_sequences'}}, $sequence);
                        }
                    }
                }
            }
        }
        else
        {
            LogVerboseInfo("CASE 0: Unsolved: no main parent");
            push(@{$statistics{'unsolvable_families'}}, $family);
        }

        # clear cache as we might have changed family content
        Greenphyl::Family->ClearCache();
    }
    LogInfo("...done processing revert-pyramid families.");

    return \%statistics;
}


=pod

=head2 GetMainParent

B<Description>: Returns the parent that as the largest number of common
  sequence with the given child family or nothing if there is a tie and $forced
  is FALSE. If there is a tie and $foreced is TRUE, returns the tying parent in
  which sequences from the family represent the highest percent in the parent
  (ie. the smallest parent), or if there is still a tie, the one with the
  smallest ID.

B<ArgsCount>: [count of arguments] #+++

=over 4

=item $variable_name: ([variable nature]) ([requirements]) #+++ see below

#--- requirement can be:
#--- (R)=required,
#--- (O)=optional
#--- (U)=optional and must be undef if omitted
[variable description]. #+++

=item $variable_name2: ([variable nature]) ([requirements]) #+++

[variable description]. #+++

=back

B<Return>: ([return type]) #+++

[return description]. #+++

B<Example>:

    [example code] #+++

=cut

sub GetMainParent
{
    my ($family, $force) = @_;

    my $main_parent_family;

    # Sort parents by largest number of common sequences.
    my @parents = sort {$b->getCommonSequenceCount($family) <=> $a->getCommonSequenceCount($family)} @{$family->parents()};
    if (@parents)
    {
        # More than one parent?
        if (1 < @parents)
        {
            if ($force)
            {
                # Tie?
                my $tie_score = $parents[0]->getCommonSequenceCount($family);
                if ($tie_score == $parents[1]->getCommonSequenceCount($family))
                {
                    # Tie, get all parents in tie.
                    @parents = grep {$tie_score <= $_->getCommonSequenceCount($family)} @parents;
                    # Sort parents by percent of common sequences (smallest parent first).
                    @parents = sort {$a->getSeqCount() <=> $b->getSeqCount()} @parents;
                    $tie_score = $parents[0]->getSeqCount();
                    # Still tie?
                    if ($tie_score == $parents[1]->getSeqCount())
                    {
                        # Tie, get all parents in tie.
                        @parents = grep {$tie_score >= $_->getSeqCount()} @parents;
                        # Sort parents by ID.
                        @parents = sort {$a->id <=> $b->id} @parents;
                        $main_parent_family = $parents[0];
                    }
                    else
                    {
                        # No tie.
                        $main_parent_family = $parents[0];
                    }
                }
                else
                {
                    # No tie.
                    $main_parent_family = $parents[0];
                }
            }
        }
        else
        {
            # One parent.
            $main_parent_family = $parents[0];
        }
    }

    return $main_parent_family;
}


=pod

=head2 TransferSequence

B<Description>: [function description]. #+++

B<ArgsCount>: [count of arguments] #+++

=over 4

=item $variable_name: ([variable nature]) ([requirements]) #+++ see below

#--- requirement can be:
#--- (R)=required,
#--- (O)=optional
#--- (U)=optional and must be undef if omitted
[variable description]. #+++

=item $variable_name2: ([variable nature]) ([requirements]) #+++

[variable description]. #+++

=back

B<Return>: ([return type]) #+++

[return description]. #+++

B<Example>:

    [example code] #+++

=cut

sub TransferSequence
{
    my ($sequence, $from_family, $to_family, $statistics) = @_;
    my $sql_query = "DELETE FROM families_sequences WHERE sequence_id = ? AND family_id = ?;";
    LogDebug("SQL Query: $sql_query\nBindings: " . $sequence->id . ", " . $from_family->id);
    $g_dbh->do($sql_query, undef, $sequence->id, $from_family->id)
        or confess LogError("Failed to remove sequence '$sequence' from family '$from_family':\n" . $DBI::errstr);

    $statistics->{$from_family->id} ||= 0;
    $statistics->{$from_family->id}--;

    $sql_query = "INSERT INTO families_sequences (sequence_id, family_id) VALUES (?, ?);";
    LogDebug("SQL Query: $sql_query\nBindings: " . $sequence->id . ", " . $to_family->id);
    $g_dbh->do($sql_query, undef, $sequence->id, $to_family->id)
        or confess LogError("Failed to add sequence '$sequence' to family '$to_family':\n" . $DBI::errstr);

    $statistics->{$to_family->id} ||= 0;
    $statistics->{$to_family->id}++;
}

=pod

=head2 CleanupFamilies

B<Description>: [function description]. #+++

B<ArgsCount>: [count of arguments] #+++

=over 4

=item $variable_name: ([variable nature]) ([requirements]) #+++ see below

#--- requirement can be:
#--- (R)=required,
#--- (O)=optional
#--- (U)=optional and must be undef if omitted
[variable description]. #+++

=item $variable_name2: ([variable nature]) ([requirements]) #+++

[variable description]. #+++

=back

B<Return>: ([return type]) #+++

[return description]. #+++

B<Example>:

    [example code] #+++

=cut

sub CleanupFamilies
{
    # Remove empty families.
    my $sql_query = "DELETE FROM families WHERE NOT EXISTS (SELECT TRUE FROM families_sequences fs WHERE fs.family_id = id);";
    my $removed_families = $g_dbh->do($sql_query)
        or confess LogError("Failed to cleanup empty families:\n" . $DBI::errstr);
    $sql_query = "CALL updateFamiliesStats();";
    $g_dbh->do($sql_query);
    $sql_query = "CALL countSequencesByFamiliesSpecies();";
    $g_dbh->do($sql_query);
    $sql_query = "CALL countSequencesByFamiliesGenomes();";
    $g_dbh->do($sql_query);
    # Remove families with only 1 or 2 sequences (min-seq = 3).
    # $sql_query = "DELETE FROM families WHERE level IN (1,2,3,4) AND sequence_count IN (1, 2);";
    # my $removed_small_families = $g_dbh->do($sql_query)
    #     or confess LogError("Failed to cleanup families with less than 3 sequences:\n" . $DBI::errstr);
    # $removed_families += $removed_small_families;

    return $removed_families;
}


=pod

=head2 PrintStatistics

B<Description>: [function description]. #+++

B<ArgsCount>: [count of arguments] #+++

=over 4

=item $variable_name: ([variable nature]) ([requirements]) #+++ see below

#--- requirement can be:
#--- (R)=required,
#--- (O)=optional
#--- (U)=optional and must be undef if omitted
[variable description]. #+++

=item $variable_name2: ([variable nature]) ([requirements]) #+++

[variable description]. #+++

=back

B<Return>: ([return type]) #+++

[return description]. #+++

B<Example>:

    [example code] #+++

=cut

sub PrintStatistics
{
    my ($level, $statistics) = @_;

    # select all the families at level n with more than 1 group at level n-1
    my $sql_query = qq{
        SELECT COUNT(child_f.id) AS "count"
        FROM families child_f
            JOIN family_relationships_cache frc ON (frc.subject_family_id = child_f.id)
        WHERE
            child_f.level = $level
            AND frc.level_delta = -1
            AND frc.type = '$Greenphyl::AbstractFamily::RELATIONSHIP_INHERITANCE'
        GROUP BY child_f.id
        HAVING COUNT(DISTINCT frc.object_family_id) > 1
    };

    my ($pyramid_families_count) = $g_dbh->selectrow_array($sql_query);
    $pyramid_families_count ||= 0;

    LogInfo("Revert-pyramid families at level $level: $pyramid_families_count\n");
    if ($statistics)
    {
        LogInfo("Transfered sequences: " . scalar(@{$statistics->{'resolved_sequences'}}) . "\n");
        LogVerboseInfo("    " . join(', ', sort {$a->accession cmp $b->accession} @{$statistics->{'resolved_sequences'}}) . "\n\n");
        LogInfo("Unresolved sequences: " . scalar(@{$statistics->{'unresolved_sequences'}}) . "\n");
        LogVerboseInfo("    " . join(', ', sort {$a->accession cmp $b->accession} @{$statistics->{'unresolved_sequences'}}) . "\n\n");
        LogInfo("Unsolvable families:  " . scalar(@{$statistics->{'unsolvable_families'}}) . "\n");
        LogVerboseInfo("    " . join(', ', sort {$a->accession cmp $b->accession} @{$statistics->{'unsolvable_families'}}) . "\n\n");
        LogInfo("Removed families:  " . $statistics->{'removed_families'} . "\n");
        if ($statistics->{'family_changes'} && %{$statistics->{'family_changes'}})
        {
            LogVerboseInfo("Family sequence changes:\n");
            foreach my $family_id (sort {$a <=> $b} keys(%{$statistics->{'family_changes'}}))
            {
                LogVerboseInfo(sprintf("    % 8s: " . $statistics->{'family_changes'}->{$family_id} . "\n", $family_id));
            }
            LogVerboseInfo("\n");
        }

        # species stats
        my %species_stats;
        foreach my $sequence (@{$statistics->{'resolved_sequences'}}, @{$statistics->{'unresolved_sequences'}})
        {
            $species_stats{$sequence->species->code} ||= 0;
            $species_stats{$sequence->species->code}++;
        }

        if (%species_stats)
        {
            LogVerboseInfo("Species statistics:\n");
            foreach my $species_code (sort keys(%species_stats))
            {
                LogVerboseInfo("$species_code: $species_stats{$species_code}\n");
            }
            LogVerboseInfo("\n");
        }
    }
}




# Script options
#################

=pod

=head1 OPTIONS

    fix_mcl.pl [-help | -man | -debug] | <[-level] level> [-diagnose] [-nocleanup]


=head2 Parameters

=over 4

=item B<-help>:

Prints a brief help message and exits.

=item B<-man>:

Prints the manual page and exits.

=item B<-debug> (integer):

debug mode: display debugging messages and rollback database changes.

=item B<-level> (integer):

family level to process for revert-pyramids.

=item B<-diagnose>:

Do not attempt to modify database and just report statistics.

=item B<-nocleanup>:

Do not remove empty families or update counts.

=item B<-forcefix>:

Force non-trivial cases to be solved by trusting lower level clustering.

=item B<-log>:

Enable logging.

=item B<-log-*> (any):

Logging arguments.

=back

=cut


# CODE START
#############


# for regression tests
if ($ENV{'TESTING'} && $ENV{'TESTING_NO_RUN'})
{return 1;}

# options processing
my ($man, $help, $debug, $level, $diagnose, $nocleanup, $force) = (0, 0, 0, 0, 0, 0);
# parse options and print usage if there is a syntax error.
GetOptions("help|?"    => \$help,
           "man"       => \$man,
           "debug"     => \$debug,
           "diagnose"  => \$diagnose,
           "level=i"   => \$level,
           "nocleanup" => \$nocleanup,
           "f|force|forcefix" => \$force,
           @Greenphyl::Log::LOG_GETOPT, # skip log parameters
) or pod2usage(1);
if ($help) {pod2usage('-verbose' => 1, '-exitval' => 0);}
if ($man) {pod2usage('-verbose' => 2, '-exitval' => 0);}

# change debug mode if requested/forced
if (defined($debug))
{
    # just '-debug' ?
    if ($debug eq '')
    {
        $debug = 1;
    }
    $DEBUG = $debug;
}


$level ||= shift() || GetParameterValues('level');;
if (!$level)
{
    LogError("No family level specified!");
    pod2usage(1);
}

if ($level !~ m/^[234]$/)
{
    confess LogError("Please specify a valid clustering level (2, 3 or 4)! (got '$level')");
}


eval
{
    # start SQL transaction
    if ($g_dbh->{'AutoCommit'})
    {
        $g_dbh->begin_work() or croak $DBI::errstr;
    }
    # set warning state (not fatal)
    $g_dbh->{'HandleError'} = undef;

    # check if family_relationships_cache table is up-to-date
    my $sql_query = "SELECT COUNT(DISTINCT subject_family_id) FROM family_relationships_cache;";
    my ($relationships_count) = $g_dbh->selectrow_array($sql_query);
    if (!$relationships_count)
    {
        LogError("family_relationships_cache table appears to be empty!");
    }
    # $sql_query = "SELECT COUNT(id) FROM families;";
    # my ($family_count) = $g_dbh->selectrow_array($sql_query);
    # if ($family_count != $relationships_count)
    # {
    #     LogWarning("family_relationships_cache table does not appear to be up-to-date! ($relationships_count families with relationships against a total of $family_count families)");
    # }

    my $statistics = FixPyramidFamilies($level, $force);

	if (!$DEBUG && !$diagnose)
    {
		SaveCurrentTransaction($g_dbh, 0);
    my $dbindex = SelectActiveDatabase();
    my $dbindex_parameter = '';
    if ($dbindex)
    {
        $dbindex_parameter = ' dbindex=' . $dbindex;
    }
		# update relationships
		if (system('perl update_family_relationships.pl ' . $dbindex_parameter))
		{
			LogWarning("Failed to update family relationships!\n$!\nPlease run the script 'update_family_relationships.pl' manually.");
		}
		# start SQL transaction
		if ($g_dbh->{'AutoCommit'})
		{
			$g_dbh->begin_work() or croak $DBI::errstr;
		}
	}
    else
    {
        $g_dbh->rollback();
        LogInfo('No change made to database!');
    }


    # remove empty families and update counts
    if (!$nocleanup)
    {
        $statistics->{'removed_families'} = CleanupFamilies();
    }
    else
    {
        $statistics->{'removed_families'} = 0;
    }

    PrintStatistics($level, $statistics);

    if (!$diagnose)
    {SaveCurrentTransaction($g_dbh);}
    else
    {
        $g_dbh->rollback();
        LogInfo('No change made to database!');
    }

};

# catch
HandleErrors();

exit(0);

# CODE END
###########

=pod

=head1 AUTHORS

Mathieu ROUARD (Bioversity), m.rouard@cgiar.org

Valentin GUIGNON (Bioversity), v.guignon@cgiar.org

Matthieu CONTE (Syngenta)

Sebastien BRIOIS (Syngenta)

=head1 VERSION

Version 1.2.0

Date 02/03/2020

=head1 SEE ALSO

GreenPhyl documentation, OrthoMCL, Clustering

=cut
