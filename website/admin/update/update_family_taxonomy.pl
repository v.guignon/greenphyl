#!/usr/bin/perl

=pod

=head1 NAME

update_family_taxonomy.pl - Find family specificities

=head1 SYNOPSIS

    update_family_taxonomy.pl

=head1 REQUIRES

Perl5

=head1 DESCRIPTION

Work on each sequence family in a Greenphyl database and detects their
specificities: species, phylum,...

=cut

use strict;
use warnings;

use FindBin;
use lib "$FindBin::Bin/../../lib";
use lib "$FindBin::Bin/../../local_lib";
use Carp qw (cluck confess croak);

use Getopt::Long;
use Pod::Usage;

use Greenphyl;
use Greenphyl::Log('nolog' => 1,);
use Greenphyl::Family;
use Greenphyl::Tools::Families;
use Greenphyl::Load::Taxonomy;

use LWP::UserAgent;
use HTML::Entities;
use XML::Simple qw(:strict);




# Script global constants
##########################

=pod

=head1 CONSTANTS


=cut

our $DEBUG = 0;




# Script global functions
##########################

=pod

=head1 FUNCTIONS

=head2 ProcessFamilySpecificity

B<Description>: Parses taxonomy data from the given XML content.

B<ArgsCount>: 1

=over 4

=item $: (string) (R)

le.

=back

B<Return>: (hash ref)

An XML structure returned by XMLin() reflecting the content of the XML file.

=cut

sub ProcessFamilySpecificity
{
    my ($family, $species_to_lineage) = @_;
    
    # process family
    my %lineage_counters = ();
    #$sql_query = "SELECT sp.taxonomy_id FROM seq_is_in sii JOIN sequences s JOIN species sp USING (species_id) USING (seq_id) WHERE sii.family_id = ? GROUP BY sp.taxonomy_id;";
    my $sql_query = "
        SELECT sp.taxonomy_id
        FROM sequence_count_by_families_species_cache scfs
            JOIN species sp ON sp.id = scfs.species_id
        WHERE scfs.family_id = ?
        GROUP BY sp.taxonomy_id;";
    # for each family species add weight to associated lineages
    my $family_species = GetDatabaseHandler()->selectcol_arrayref($sql_query, undef, $family->id)
        or confess LogError("Failed to fetch family species for family " . $family->id . "!");
    if (!@$family_species)
    {
        LogWarning("Failed to fetch family species for family " . $family->id . ": no associated species found! If that family contains sequences, maybe you should call SQL procedure 'CALL countFamilySeqBySpecies();'?");
    }
    else
    {
        foreach my $tax_id (@$family_species)
        {
            # species
            $lineage_counters{$tax_id} = 1;
            # loop on associated lineages
            foreach my $lineage (@{$species_to_lineage->{$tax_id}})
            {
                $lineage_counters{$lineage} ||= 0;
                $lineage_counters{$lineage}++;
            }
        }
        # find root node of the smallest common subtree using for instance the
        # lineage of the first species of the family
        my @lineage_to_follow = @{$species_to_lineage->{$family_species->[0]}};
        push(@lineage_to_follow, $family_species->[0]);
        my $family_root;
        while (($family_root = pop(@lineage_to_follow))
               && ($lineage_counters{$family_root} < scalar(@$family_species)))
        {}
        # set family tax_id
        if ($family_root)
        {
            $sql_query = "UPDATE families SET taxonomy_id = $family_root WHERE id = ?;";
            GetDatabaseHandler()->do($sql_query, undef, $family->id);
            LogDebug("Family taxonomy root ID is $family_root");
        }
        else
        {
            LogWarning("Failed to find family taxonomy root for family " . $family->id . "!");
        }
    }

    return 1;
}




# Script options
#################

=pod

=head1 OPTIONS

update_family_taxonomy.pl [-help | -man]
update_family_taxonomy.pl [-debug] [-taxonomy <TAXONOMY_FILE>]

=head2 Parameters

=over 4

=item B<-help>:

Prints a brief help message and exits.

=item B<-man>:

Prints the manual page and exits.

=item B<-debug> (integer):

Executes the script in debug mode. If an integer value is specified, it will
be the debug level. If "-debug" option was used without specifying a debug
level, level 1 is assumed.
Default: 0 (not in debug mode).

=item B<-taxonomy> (string):

Specify the file that contains species lineage taxonomy information in XML.
Default: fetched from the web services.

=back

=cut


# CODE START
#############

# options processing
my ($man, $help, $debug, $ncbi_taxonomy_filename) = (0, 0, 0, '');
# parse options and print usage if there is a syntax error.
GetOptions("help|?"   => \$help,
           "man"      => \$man,
           "debug"    => \$debug,
           "taxonomy=s" => \$ncbi_taxonomy_filename,
           @Greenphyl::Log::LOG_GETOPT, # skip log parameters
) or pod2usage(2);
if ($help) {pod2usage(1);}
if ($man) {pod2usage(-verbose => 2);}

eval
{
    LogDebug('Running in debug mode...');
    LogInfo('Update family taxonomy data...');

    my $dbh = GetDatabaseHandler();
    # start SQL transaction
    if ($dbh->{'AutoCommit'})
    {
        $dbh->begin_work() or croak $dbh->errstr;
    }
    # set warning state (not fatal)
    $dbh->{'HandleError'} = undef;

    # get species list with taxonomy_id from database
    my $sql_query = "SELECT taxonomy_id FROM species WHERE display_order > 0;";
    my $species_tax_ids = $dbh->selectcol_arrayref($sql_query);

    # build taxonomy hash: associate to a given species taxonomy_id an array of
    # taxonomy_id corresponding to parents in the taxonomy lineage ordered by
    # the lowest level first.
    my $species_to_lineage = {};
    foreach my $species_taxonomy_id (@$species_tax_ids)
    {
        $sql_query = "SELECT l.lineage_taxonomy_id FROM lineage l JOIN taxonomy t ON (t.id = l.lineage_taxonomy_id) WHERE l.taxonomy_id = $species_taxonomy_id ORDER BY t.level ASC;";
        $species_to_lineage->{$species_taxonomy_id} = $dbh->selectcol_arrayref($sql_query);
        if (!$species_to_lineage->{$species_taxonomy_id} || !@{$species_to_lineage->{$species_taxonomy_id}})
        {
            confess "Species lineage not available for species taxonomy_id '$species_taxonomy_id'!";
        }
    }

    my $processed_family_count = 0;

    LoopOnFamilies(
        sub
        {
            my $family = shift();
            LogDebug("Processing family $family...");
            # update taxonomy data
            $processed_family_count += ProcessFamilySpecificity($family, $species_to_lineage);
            LogDebug("...done with family $family.");
        }
    );
    
    LogInfo("Processed $processed_family_count families.");

    if (!$DEBUG)
    {SaveCurrentTransaction(GetDatabaseHandler());}

    LogInfo("Done.");
};

HandleErrors();

exit(0);

# CODE END
###########


=pod

=head1 AUTHORS

Mathieu ROUARD (Bioversity), m.rouard@cgiar.org

Valentin GUIGNON (Bioversity), v.guignon@cgiar.org

Matthieu CONTE (Syngenta)

Sebastien BRIOIS (Syngenta)

=head1 VERSION

Version 1.0.0

Date 28/11/2012

=head1 SEE ALSO

GreenPhyl documentation.

=cut
