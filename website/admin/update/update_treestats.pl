#!/usr/bin/env perl

=pod

=head1 NAME

update_treestats.pl - Loads family phylogeny tree statistics in database

=head1 SYNOPSIS

    update_treestats.pl

=head1 REQUIRES

Perl5, treestats

=head1 DESCRIPTION

Loads in database family phylogeny tree statistics computed from trees in a given
directory.

=cut

use strict;
use warnings;

use FindBin;
use lib "$FindBin::Bin/../../lib";
use lib "$FindBin::Bin/../../local_lib";
use Carp qw (cluck confess croak);

use Greenphyl;
use Greenphyl::Log('nolog' => 1,);
use Greenphyl::AbstractFamily;

use Getopt::Long;
use Pod::Usage;

++$|; #no buffering




# Script global constants
##########################

=pod

=head1 CONSTANTS

B<$REV_STRING>: (string)

Current revision (auto-set by GIT).

B<$DEBUG>: (boolean)

When set to true, it enables debug mode.

=cut

# SVN management part
my $REV_STRING = '$id$';

our $DEBUG = 0;




# Script global variables
##########################

=pod

=head1 VARIABLES

B<$g_dbh>: (DBI::db)

Global database handler.

=cut

my $g_dbh;




# Script global functions
##########################

=pod

=head1 FUNCTIONS

=head2 RunTreeStats

B<Description>: Run treestats command on the given tree directory.

B<ArgsCount>: 0-2

=over 4

=item $tree_path: (string) (U)

Path where the newick tree files to parse are stored. Tree files must begin
with the family accession (8 characters) and contain '_rap_gene_tree.nwk' (see
$Greenphyl::Config::PHYLO_NEWICK_TREE_FILE_SUFFIX).
Default: $Greenphyl::Config::TREES_PATH

=item $output_csv_path: (string) (O)

Path to the output CSV file to use.
Default: $Greenphyl::Config::DATA_PATH/treestats.csv

=back

B<Return>: (string)

Path to the output CSV file used.

=cut

sub RunTreeStats
{
    my ($tree_path, $output_csv_path) = @_;

    $tree_path ||= GP('TREES_PATH');
    $tree_path =~ s~/*$~/~; # only one trailing slash

    $output_csv_path ||= GP('DATA_PATH') . '/treestats.csv';

    # run TreeStats
    LogInfo("Running treestats on '$tree_path'...");
    my $command = GP('TREESTATS_COMMAND') . " -input '$tree_path' -output '$output_csv_path' -depths -key " . GP('PHYLO_NEWICK_TREE_FILE_SUFFIX');
    LogDebug("COMMAND: $command");
    if (system($command))
    {
        confess LogError("An error occured while running treestats:\n$!");
    }
    LogInfo("...treestats done!\nOutput file: '$output_csv_path'");

    return $output_csv_path;
}


=pod

=head2 ProcessTreeStatResults

B<Description>: Parse treestats CSV output file and store results in database.

B<ArgsCount>: 1

=over 4

=item $treestats_csv_path: (string) (R)

Path to the treestats CSV file to parse.

=back

B<Return>: nothing

=cut

sub ProcessTreeStatResults
{
    my ($treestats_csv_path) = @_;

    if (!-e $treestats_csv_path)
    {
        confess LogError("TreeStats output file '$treestats_csv_path' not found!");
    }

    # open and parse file
    my $treestats_csv_fh;
    if (open($treestats_csv_fh, $treestats_csv_path))
    {
        # CSV file content exemple:
        # FILE    NBSEQ   AVERAGE MEDIAN  DEVIATION
        # GP000413_rap_gene_tree.nwk      301     3.0971227498922422      3.0184871312449997      1.7471908745489937
        # GP105124_rap_gene_tree.nwk      25      4.512059999999999E-7    4.441499999999999E-7    1.9180223999999993E-7

        my $columns = <$treestats_csv_fh>;
        if ($columns !~ m/^FILE\s+NBSEQ\s+AVERAGE\s+MEDIAN\s+DEVIATION\s*$/)
        {
            confess LogError("Unsupported CSV file! Got columns:\n$columns");
        }
        
        LogInfo("Processing family tree stats...");
        my $line_count = 0;
        my $processed_families = 0;
        while (my $line = <$treestats_csv_fh>)
        {
            my ($family_id, $leaves_count, $average, $median, $deviation) =
#                 ($line =~
# #       971552.enc_rap_gene_tree.nwk     11      0.963774        0.905426        0.1189265454545455
#                     m/^
#                         (\d+)\S*\s+ # tree file with family accession
#                         (\d+)\s+            # leaves count
#                         ([\d\.\-eE]+)\s+    # average branch length
#                         ([\d\.\-eE]+)\s+    # median branch length
#                         ([\d\.\-eE]+)\s*    # standard deviation
#                     $/x);
                 ($line =~
                     m/^
                         ($Greenphyl::AbstractFamily::FAMILY_REGEXP)\S*\s+ # tree file with family accession
                         (\d+)\s+            # leaves count
                         ([\d\.\-eE]+)\s+    # average branch length
                         ([\d\.\-eE]+)\s+    # median branch length
                         ([\d\.\-eE]+)\s*    # standard deviation
                     $/x);

            if (!defined($deviation))
            {
                confess LogError("Failed to parse line $line_count:\n$line");
            }

            my $family = Greenphyl::Family->new($g_dbh, {'selectors' => {'id' => $family_id}});
            if ($family)
            {
                $family->branch_average($average);
                $family->branch_median($median);
                $family->branch_standard_deviation($deviation);
                $family->save();
                ++$processed_families;
            }
            else
            {
                LogWarning("Failed to find family '$family_id'! Skipping line:\n$line");
            }

            ++$line_count;
        }

        LogInfo("..Done processing stats!\nUpdated $processed_families families (over $line_count lines)");
        close($treestats_csv_fh);
    }
    else
    {
        confess LogError("Failed to open TreeStats output CSV file '$treestats_csv_path': $!");
    }
}




# Script options
#################

=pod

=head1 OPTIONS

    update_treestats.pl [-help | -man]
    update_treestats.pl [-debug] [-trees <TREE_PATH>] [-csv <CSV_PATH>]

=head2 Parameters

=over 4

=item B<-help>:

Prints a brief help message and exits.

=item B<-man>:

Prints the manual page and exits.

=item B<-debug> (integer):

Executes the script in debug mode. The integer value is optional and is used
to set debug level (use 0 to force disable debug mode).
Default: 0 (not in debug mode).

=item B<-trees> (string):

Path to the directory containing '*_rap_gene_tree.nwk' newick tree files. The
file names must begin with the family accession.
Default: $Greenphyl::Config::TREES_PATH

=item B<-csv> (string):

Path to the treestats CSV (output) file. If this parameter is set but not
-trees, the program will assume the CSV file is supposed to already exist and
just need to be parsed and stored in database (treestats won't be run).
Default: $Greenphyl::Config::DATA_PATH/treestats.csv

=item B<-log>:

Enable logging.

=item B<-log-*> (any):

Logging arguments.

=back

=cut


# CODE START
#############

# options processing
my ($man, $help, $debug, $no_prompt) = (0, 0, undef, undef);
my ($tree_path, $output_csv_path);
# parse options and print usage if there is a syntax error.
GetOptions('help|?'         => \$help,
           'man'            => \$man,
           'debug:s'        => \$debug,
           'q|noprompt'     => \$no_prompt,
           'i|tree|trees=s' => \$tree_path,
           'o|csv|output=s' => \$output_csv_path,
           @Greenphyl::Log::LOG_GETOPT, # skip log parameters
) or pod2usage(1);
if ($help) {pod2usage('-verbose' => 1, '-exitval' => 0);}
if ($man) {pod2usage('-verbose' => 2, '-exitval' => 0);}

# change debug mode if requested/forced
if (defined($debug))
{
    # just '-debug' ?
    if ($debug eq '')
    {
        $debug = 1;
    }
    $DEBUG = $debug;
}

eval
{
    LogDebug('Running in debug mode...');
    LogInfo('Update family tree statistics data...');
    $g_dbh = GetDatabaseHandler();

    # # start SQL transaction
    # if (GetDatabaseHandler()->{'AutoCommit'})
    # {
    #     GetDatabaseHandler()->begin_work() or croak GetDatabaseHandler()->errstr;
    # }

    # set warning state (not fatal)
    GetDatabaseHandler()->{'HandleError'} = undef;

    if ($tree_path || !$output_csv_path)
    {
        # run treestats
        my $treestats_csv_path = RunTreeStats($tree_path, $output_csv_path);

        # parse the result and store in DB
        ProcessTreeStatResults($treestats_csv_path);
    }
    else
    {
        ProcessTreeStatResults($output_csv_path);
    }

    # if (!$DEBUG)
    # {SaveCurrentTransaction(GetDatabaseHandler());}

    LogInfo("Done.");
};

# catch
HandleErrors();

exit(0);

# CODE END
###########


=pod

=head1 AUTHORS

Mathieu ROUARD (Bioversity), m.rouard@cgiar.org

Valentin GUIGNON (Bioversity), v.guignon@cgiar.org

Matthieu CONTE (Syngenta)

Sebastien BRIOIS (Syngenta)

=head1 VERSION

Version 1.0.0

Date 31/01/2014

=head1 SEE ALSO

GreenPhyl documentation.

=cut
