#!/usr/bin/env perl

=pod

=head1 NAME

update_sequences_cache.pl - Update sequence-related cached data in database.

=head1 SYNOPSIS

    update_sequences_cache.pl

=head1 REQUIRES

Perl5

=head1 DESCRIPTION

-sequences_cache

=cut

use strict;
use warnings;

use FindBin;
use lib "$FindBin::Bin/../../lib";
use lib "$FindBin::Bin/../../local_lib";
use Carp qw (cluck confess croak);

use threads;
use threads::shared;

use Greenphyl;
use Greenphyl::Log('nolog' => 1,);
use Greenphyl::CachedSequence;
use Greenphyl::Tools::Sequences;

use Getopt::Long;
use Pod::Usage;

++$|; #no buffering




# Script global constants
##########################

=pod

=head1 CONSTANTS

B<$REV_STRING>: (string)

Current revision (auto-set by GIT).

B<$DEBUG>: (boolean)

When set to true, it enables debug mode.

=cut

# SVN management part
my $REV_STRING = '$id$';

our $DEBUG = 0;




# Script global variables
##########################

=pod

=head1 VARIABLES

B<$g_continue>: (boolean)

Tells if there are still sequences to process.

=cut

my $g_continue :shared;
my $g_offset :shared;
my $g_sequence_progress :shared;




# Script global functions
##########################

=pod

=head1 FUNCTIONS

=head2 LoadSequenceCache

B<Description>: updates sequences_cache table.

B<ArgsCount>: 0

B<Return>: nothing

=cut

sub process_sequences_thread
{
    # since this procedure is run from threads, modules should be reloaded.
    use strict;
    use Carp qw (cluck confess croak);
    use warnings;
    use Readonly;
    use Error qw(:try);

    use Greenphyl;
    use Greenphyl::Log('nolog' => 1,);
    use Greenphyl::CachedDBObject;
    use Greenphyl::CachedSequence;
    use Greenphyl::Tools::Sequences;
    
    # Cleanup connection stuff.
    ResetDatabaseHandlers();

    my ($parameters) = @_;
    
    # We want to load complete sequence and we there's no parameter to do
    # that with LoadSequences() so we modify class properties localy.
    $Greenphyl::Sequence::OBJECT_PROPERTIES->{'load'} = $parameters->{'load_all'};
    my $sequences = LoadSequences($parameters->{'limit'}, $g_offset);
    # Put back default load for next operations.
    $Greenphyl::Sequence::OBJECT_PROPERTIES->{'load'} = $parameters->{'default_load'};
    # Prepare next offset.

    if (@$sequences)
    {
        LogInfo("Working on " . scalar(@$sequences) . " sequence(s) (offset $g_offset).");
    }
    $g_offset += $parameters->{'limit'};
    foreach my $sequence (@$sequences)
    {
        print GetProgress({'label' => 'Processing sequences:', 'current' => $g_sequence_progress++, 'total' => $parameters->{'total_sequences'},}, 80, 5);
        LoadSequenceCache($sequence, $parameters->{'resume'});
    }

    Greenphyl::CachedDBObject->ClearCache();

    $g_continue = scalar(@$sequences);
}


=pod
 
=head2 LoadSequenceCache

B<Description>: updates sequences_cache table.

B<ArgsCount>: 0

B<Return>: nothing

=cut

sub LoadSequenceCache
{
    my ($sequence, $resume) = @_;
    
    if (!$sequence)
    {
        confess "ERROR: Missing parameters for LoadSequenceCache!\n";
    }
    
    my $cached_sequence = Greenphyl::CachedSequence->new(
            GetDatabaseHandler(),
            {
                'selectors' => { 'id' => $sequence->id, },
            },
        );
    if ($resume && $cached_sequence->id)
    {
      LogVerboseInfo("\nSkipping already cached sequence " . $cached_sequence->id . " (resume mode)");
      return;
    }

    # Clone sequence.
    $cached_sequence = Greenphyl::CachedSequence->new(
            GetDatabaseHandler(),
            {
                'load' => 'none',
                'members' => $sequence->getHashValue(1),
            },
        );

    # Add members to serialize.

    # BBMH
    my $bbmh = {
        'bbmh' => $sequence->bbmh() || undef,
        'bbmh_sequences' => $sequence->bbmh_sequences() || undef,
    };
    $cached_sequence->setBbmhCache($bbmh);

    # Homologs
    my $homolgy_relationships = {'homologies' => {}, 'homologs' => {}, };
    foreach my $homology (@{$sequence->homologies()})
    {
        $homolgy_relationships->{'homologies_to_homologs'}->{$homology->id} = 
            $homology->object_sequence_id();
        $homolgy_relationships->{'homologs_to_homologies'}->{$homology->object_sequence_id()} = 
            $homology->id;
    }
    my $homologs = {
        'homologies' => $sequence->homologies() || undef,
        'homologs' => $sequence->homologs() || undef,
        'homolgy_relationships' => $homolgy_relationships || undef,
    };
    $cached_sequence->setHomologsCache($homologs);

    # IPR
    my $ipr = {
        'ipr' => $sequence->ipr() || undef,
        'ipr_details' => $sequence->ipr_details() || undef,
    };
    $cached_sequence->setIprCache($ipr);

    # level_1_family_id
    my $level_1_family = $sequence->families({'selectors' => {'level' => 1}});
    $cached_sequence->set_level_1_family_id($level_1_family && @$level_1_family && $level_1_family->[0] ? $level_1_family->[0]->id : undef);

    # level_2_family_id
    my $level_2_family = $sequence->families({'selectors' => {'level' => 2}});
    $cached_sequence->set_level_2_family_id($level_2_family && @$level_2_family && $level_2_family->[0] ? $level_2_family->[0]->id : undef);

    # level_3_family_id
    my $level_3_family = $sequence->families({'selectors' => {'level' => 3}});
    $cached_sequence->set_level_3_family_id($level_3_family && @$level_3_family && $level_3_family->[0] ? $level_3_family->[0]->id : undef);

    # level_4_family_id
    my $level_4_family = $sequence->families({'selectors' => {'level' => 4}});
    $cached_sequence->set_level_4_family_id($level_4_family && @$level_4_family && $level_4_family->[0] ? $level_4_family->[0]->id : undef);

    # pangene_family_id
    my $pangene_family = $sequence->getPangeneFamily();
    $cached_sequence->set_pangene_family_id($pangene_family ? $pangene_family->id : undef);

    $cached_sequence->save(1);

    LogVerboseInfo("\nUpdated cached sequence " . $cached_sequence->id);
}




# Script options
#################

=pod

=head1 OPTIONS

    update_sequences_cache.pl [-help|-man]
    update_sequences_cache.pl [-debug] [sequence selectors]


=head2 Parameters

=over 4

=item B<-help>:

Prints a brief help message and exits.

=item B<-man>:

Prints the manual page and exits.

=item B<-debug> (integer):

Executes the script in debug mode. The integer value is optional and is used
to set debug level (use 0 to force disable debug mode).
Default: 0 (not in debug mode).

=item B<-log>:

Enable logging.

=item B<-log-*> (any):

Logging arguments.

=back

=cut


# CODE START
#############

# for regression tests
if ($ENV{'TESTING'} && $ENV{'TESTING_NO_RUN'})
{return 1;}

# options processing
my ($man, $help, $debug, $no_prompt, $resume) = (0, 0, undef, undef, 0);

GetOptions('help|?'     => \$help,
           'man'        => \$man,
           'debug:s'    => \$debug,
           'q|noprompt' => \$no_prompt,
           'r|resume'   => \$resume,
           @Greenphyl::Log::LOG_GETOPT, # skip log parameters
) or pod2usage(1);
if ($help) {pod2usage('-verbose' => 1, '-exitval' => 0);}
if ($man) {pod2usage('-verbose' => 2, '-exitval' => 0);}

# change debug mode if requested/forced
if (defined($debug))
{
    # just '-debug' ?
    if ($debug eq '')
    {
        $debug = 1;
    }
    $DEBUG = $debug;
}

eval
{
    LogDebug('Running in debug mode...');

    my $parameters = {
      'limit' => 1000,
      'default_load' => $Greenphyl::Sequence::OBJECT_PROPERTIES->{'load'},
      'load_all' => [keys(%{$Greenphyl::Sequence::OBJECT_PROPERTIES->{'base'}})],
      'resume' => $resume,
    };
     $g_offset = 0;
     $g_sequence_progress = 0;

    my ($table_clause, $where_clause, $bind_values) = Greenphyl::Tools::Sequences::GetSequenceLoadingParameters();
    my $sql_query = "SELECT COUNT(1) AS \"cnt\" FROM\n  $table_clause\n"
        . (@$where_clause ? "WHERE\n  " . join("\n  AND ", @$where_clause) : '')
        . ";";
    PrintDebug("Sequence count query: $sql_query\nBindings: " . join(', ', @$bind_values));
    my $dbh = GetDatabaseHandler();
    my ($total_sequences) = $dbh->selectrow_array($sql_query, { 'Slice' => {} }, @$bind_values)
        or confess $dbh->errstr;

    $parameters->{'total_sequences'} = $total_sequences;
    LogInfo("Total number of sequence to process: $total_sequences\n" );
    do
    {
        my $thread = threads->create(\&process_sequences_thread, $parameters);
        $thread->join();

        my $ps_output = `/bin/ps -p $$ -o %mem,vsz`;
        $ps_output =~ m/(\d+\.\d*)\s+(\d+)/;
        LogInfo("\nMemory usage: %MEM=$1, VSZ=$2");
    } while($g_continue);

    LogInfo("Done!");
};

my $error;

if ($error = Exception::Class->caught())
{
    warn "An error occured!\n" . $error;
}


exit(0);

# CODE END
###########


=pod

=head1 AUTHORS

Mathieu ROUARD (Bioversity), m.rouard@cgiar.org

Valentin GUIGNON (Bioversity), v.guignon@cgiar.org

=head1 VERSION

Version 1.0.0

Date 08/06/2020

=head1 SEE ALSO

GreenPhyl documentation.

=cut
