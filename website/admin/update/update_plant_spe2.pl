#!/usr/bin/env perl

=pod

=head1 NAME

update_plant_spe.pl - Update family plant-specific status.

=head1 SYNOPSIS

    update_plant_spe.pl level=1 min_sequences=5 -log-screenonly

    qsub -b y -cwd -N plantspe -sync no -V -q greenphyl.q perl /home/greenphyl/dbupdate/admin/utils/process_families.pl -run "'/home/greenphyl/dbupdate/admin/update/update_plant_spe.pl family_id={family_id} family_set_size=1 -q -complete -no-update-refseq -log-nolog'" level=1 min_sequences=5 reverse=1 -log-screenonly
    
=head1 REQUIRES

Perl5, BioPerl

=head1 DESCRIPTION

Compute and update plant-specific value for the selected families in database.

=cut

use strict;
use warnings;


use FindBin;
use lib "$FindBin::Bin/../../lib";
use lib "$FindBin::Bin/../../local_lib";

use Carp qw (cluck confess croak);

use Greenphyl;
use Greenphyl::Log('nolog' => 1,);
use Greenphyl::Tools::Families;
use Greenphyl::Run::Blast;

use Getopt::Long;
use Pod::Usage;

use Fatal qw(:void open close);
use Bio::SearchIO; 




# Script global constants
##########################

=pod

=head1 CONSTANTS

B<$DEBUG>: (boolean)

When set to true, it enables debug mode.

B<$FASTA_FILE_EXT>: (string)

File extension for FASTA files.

B<$BLAST_EXTENSION>: (string)

File extension for BLAST output files.

=cut

our $DEBUG = 0;
our $FASTA_FILE_EXT = '.fa';
our $BLAST_EXTENSION = '.blast';
our $BLAST_MAX_HIT   = 10;
our $BLAST_EVALUE    = '1e-5';
our $MIN_COVERAGE    = 25; # 0-100
our $CLUSTER_QUEUE = 'greenphyl';



# Script global functions
##########################

=pod

=head1 FUNCTIONS

=head2 GetFamilySelectionArguments

B<Description>: Extract family selection arguments from the given command line
arguments.

B<ArgsCount>: 2

=over 4

=item $arguments: (array ref) (R)

Command line arguments (@ARGV).

=item $family_selection_hash: (hash ref) (R)

Hash of parameters used to selecte families.

=back

B<Return>: (string)

Arguments used to select families in command line.

B<Example>:

    my $family_selection_hash = GetFamilyLoadingParameters();
    my $family_selection_arguments = GetFamilySelectionArguments(\@ARGV, $family_selection_hash);
    my $other_command_line = 'script.pl some args ' . $family_selection_arguments;
    system($other_command_line);

=cut

sub GetFamilySelectionArguments
{
    my ($arguments, $family_selection_hash) = @_;
    my @family_selection_arguments;
    
    foreach my $argument (@$arguments)
    {
        my ($parameter_name) = ($argument =~ m/^\s*(\w+)=/);
        if (exists($family_selection_hash->{$parameter_name})
            || ($parameter_name =~ m/^(?:family_set_size|family_page|unsorted|reverse|sort_by_size|reverse_size)$/))
        {
            push(@family_selection_arguments, $argument);
        }
    }
    my $family_selection_arguments = join(' ', @family_selection_arguments);
    LogDebug("Found family arguments: $family_selection_arguments");

    return $family_selection_arguments;
}


=pod

=head2 GenerateCDHIT

B<Description>: generate CDHIT FASTA for the given family using auto_cd_hit.pl
external script.

B<ArgsCount>: 4

=over 4

=item $family: (Greenphyl::Family) (R)

Family to process.

=item $input_path: (string) (R)

Family input FASTA file path.

=item $output_path: (string) (R)

Path of CDHIT output FASTA file to generate.

=item $complete: (boolean) (R)

If set to true and the output file specified by $output_path exists, CDHIT will
not be executed.

=back

B<Return>: nothing

=cut

sub GenerateCDHIT
{
    my ($family, $input_path, $output_path, $complete) = @_;

    # check for missing output directory
    my $family_output_path = $output_path . '/' . $family->getSubPath();
    if (!-e $family_output_path && system("mkdir -p $family_output_path"))
    {
        LogError("ERROR: Failed to create CDHIT output path '$family_output_path'!\n$!");
    }
    
    my $input_fasta = $input_path . '/' . $family->getSubPath() . '/' . $family->accession . $FASTA_FILE_EXT;
    my $output_fasta = $family_output_path . '/' . $family->accession . $FASTA_FILE_EXT;
    
    if (!$complete || (!-e $output_fasta))
    {
        # update FASTA files
        my $cdhit_command =
            'perl ' . GP('UPDATE_PATH') . '/auto_cd_hit.pl '
            . ' -i ' . $input_fasta
            . ' -o ' . $output_fasta
            . ' -r ' # overwrite existing FASTA
            . ' -length=1 ' # no minimal sequence length
        ;
        LogDebug("Generating CD HIT FASTA file for family $family using command:\n$cdhit_command");
        if (system($cdhit_command))
        {
            cluck "WARNING: Failed to generate CD HIT FASTA for family $family!\n$!\nSee logs for details.";
        }
    }
    else
    {
        LogDebug("Output FASTA file ('$output_fasta') already exists, skipping.");
    }
}


=pod

=head2 UpdateRefSeq

B<Description>: Update RefSeq BLAST banks from the web using download_refseq.pl
external script.

B<ArgsCount>: 0

B<Return>: nothing

=cut

sub UpdateRefSeq
{
    # update FASTA files
    my $update_refseq_command =
        GP('UPDATE_PATH') . '/download_refseq.pl '
        . ' --output ' . GP('REFSEQ_PATH')
        . ' --force ' # overwrite existing FASTA
        . ' ' . join(' ', @{GP('REFSEQ_BANKS')}). ' '
    ;
    LogDebug("Updating RefSeq FASTA files using command:\n$update_refseq_command");
    if (system($update_refseq_command))
    {
        confess "ERROR: Failed to update RefSeq FASTA!\n$!\nSee logs for details.";
    }

    # update BLAST bank
    $update_refseq_command =
        GP('UPDATE_PATH') . '/formatdb_refseq.pl '
        . ' -i ' . GP('REFSEQ_PATH')
        . ' -o ' . GP('REFSEQ_PATH')
        . ' ' . GetLogCommandLineArguments()
    ;
    LogDebug("Updating RefSeq banks using command:\n$update_refseq_command");
    if (system($update_refseq_command))
    {
        confess "ERROR: Failed to update RefSeq bank!\n$!\nSee logs for details.";
    }
}


=pod

=head2 DiamondFamily

B<Description>: Run BLAST on the given family against non-plant RefSeq BLAST
bank or against the given BLAST bank.

B<ArgsCount>: 4-5

=over 4

=item $family: (Greenphyl::Family) (R)

Family to process.

=item $fasta_path: (string) (R)

Path to family FASTA file.

=item $output_path: (string) (R)

Path to BLAST output file to generate.

=item $complete: (boolean) (R)

if true and output BLAST file exists, no BLAST will be run. Otherwise, BLAST
will be run.

=item $bank: (string) (O)

Path to a BLAST bank file.

=back

B<Return>: (string)

Diamond output file path.

=cut

sub DiamondFamily
{
    my ($family, $fasta_path, $output_path, $complete, $bank) = @_;

    $bank ||= '';
    my $bank_file_path = GP('REFSEQ_PATH') . '/' . $bank;
    if (!$bank || !-s ($bank_file_path . '.dmnd'))
    {
        LogError("ERROR: Empty/missing bank '$bank_file_path.dmnd'\n");
    }

    my $family_output_path = $output_path . '/' . $family->getSubPath();
    if (!-e $family_output_path && system("mkdir -p $family_output_path"))
    {
        LogError("ERROR: Failed to create Diamond output path '$family_output_path'!\n$!");
    }

    my $input_fasta = $fasta_path . '/' . $family->getSubPath() . '/' . $family->accession . $FASTA_FILE_EXT;
    if ( not -e $input_fasta ) {
        warn "WARNING: Can't find $input_fasta\n";
        return;
    }
    
    my $output_blast = $family_output_path . '/' . $family->accession . '_' . $bank . $BLAST_EXTENSION;

    if (!$complete || (!-e $output_blast) || (-z $output_blast))
    {
        # WARNING: -b 12 means 12*6Gb memory usage, so program must be run with
        # 72Gb memory reservation!
        #
        # Diamond doc:
        # --block-size/-b
        #  Block size in billions of sequence letters to be processed at a time.
        #  This is the main pa-rameter for controlling the program’s memory usage.
        #  Bigger numbers will increase the useof memory and temporary disk space,
        #  but also improve performance.  The program can beexpected to use roughly
        #  six times this number of memory (in GB). So for the default value of
        #  -b2.0, the memory usage will be about 12 GB.
        # --tmpdir/-t <directory>
        #  Directory to be used for temporary storage.  This is set to the
        #  output directory by default.The amount of disk space that will be
        #  used depends on the program’s settings and yourdata.  As a general
        #  rule you should ensure that 100 GB of disk space are available here.
        #  If you run the program in a cluster environment,  and disk space is
        #  only available over aslow network based file system, you may want to
        #  set the --tmpdiroption to /dev/shm. This will keep temporary
        #  information in memory and thus increase the program’s memory
        #  usagesubstantially.
        # --index-chunks/-c
        #  The number of chunks for processing the seed index (default=4).
        #  This option can be addi-tionally used to tune the performance. It is
        #  recommended to set this to 1 on a high memoryserver, which will
        #  increase performance and memory usage, but not the usage of
        #  temporarydisk space.
        my $diamond_cmd =
            GP('DIAMOND_COMMAND')
            . " blastp -d "
            . $bank_file_path
            . " -e $BLAST_EVALUE -q $input_fasta -o $output_blast -k $BLAST_MAX_HIT -f 0 -b 12 -t " . GP('TEMP_OUTPUT_PATH') .  " -c 1";
        LogDebug("COMMAND: $diamond_cmd");
        LogInfo("Running Diamond...");
        if (0 != system($diamond_cmd))
        {
            if (not $!)
            {
                cluck LogError("Failed to run Diamond!");
            }
            cluck LogWarning("WARNING: Diamond execution failed for family $family!\n");
            $output_blast = 0;
        }
        elsif (-z $output_blast)
        {
            LogInfo("Empty Diamond result.");
            # Add a space to the file so it won't appear as empty and it won't
            # be re-computed in case of resume ("-z $filename" will return false).
            if (open(my $fh, '>' . $output_blast))
            {
                print {$fh} ' ';
                close($fh);
            }
        }
    }
    else
    {
        LogDebug("Output Diamond file ('$output_blast') already exists, skipping.");
    }

    return $output_blast;
}


=pod

=head2 HasRefSeqHit

B<Description>: Tells if a RefSeq hit has been found.

B<ArgsCount>: 2

=over 4

=item $family: (Greenphyl::Family) (R)

Family to process.

=item $input_blast: (string) (R)

Output BLAST file that contains match results.

=back

B<Return>: nothing

B<Example>:

    HasRefSeqHit($family, $input_blast_file_path);

=cut

sub HasRefSeqHit
{
    my ($family, $input_blast) = @_;

    if (!$family || !$input_blast)
    {
        confess "ERROR: Missing parameters for HasRefSeqHit!\n";
    }
    
    if (!-e $input_blast)
    {
        LogError("ERROR: BLAST file '$input_blast' is missing!");
        return;
    }
    if (1 >= -s $input_blast)
    {
        LogInfo("Empty BLAST file '$input_blast'. Skipping.");
        return;
    }
    

    LogDebug("Parsing BLAST result file '$input_blast' for family $family");

    # open BLAST file
    my $blast_sio = new Bio::SearchIO('-format' => 'blast', '-file' => $input_blast);
    if (!$blast_sio)
    {
        LogError("ERROR: Failed to parse BLAST file '$input_blast'!");
        return;
    }

    my $family_hits = 0;
    my $result;
    RESULT:
    while (!$family_hits && ($result = $blast_sio->next_result))
    {
        my $hit;
        HIT:
        while (!$family_hits && ($hit = $result->next_hit))
        {
            my $hsp;
            while (!$family_hits && ($hsp = $hit->next_hsp))
            {
                # check minimal coverage
                my $query_coverage = 100*$hsp->length('query')/$result->query_length;
                my $hit_coverage   = 100*$hsp->length('hit')/$hit->length;
                if (($MIN_COVERAGE < $query_coverage) && ($MIN_COVERAGE < $hit_coverage))
                {
                    ++$family_hits;
                }
            }
        }
    }

    return $family_hits;
}


=pod

=head2 UpdatePlantSpecificFamily

B<Description>: Update plant-specific value of the given family in database
according to the Diamond scans.

B<ArgsCount>: 2

=over 4

=item $family: (Greenphyl::Family) (R)

Family to process.

=item $blast_output_path: (string) (R)

Diamond output directory.

=back

B<Return>: nothing

B<Example>:

    UpdatePlantSpecificFamily($family, $blast_output_path);

=cut

sub UpdatePlantSpecificFamily
{
    my ($family, $blast_output_path) = @_;

    if (!$family)
    {
        confess "ERROR: Missing parameters for UpdatePlantSpecificFamily!\n";
    }

    # Loop on all refseq banks unless a hit is found.
    my $hit = 0;

    foreach my $bank_name (@{GP('REFSEQ_BANKS')})
    {
        my $diamond_blast_result_file = $blast_output_path . '/' . $family->getSubPath() . '/' . $family->accession . '_' . $bank_name . $BLAST_EXTENSION;

        if (!-e $diamond_blast_result_file)
        {
            LogError("ERROR: BLAST file '$diamond_blast_result_file' is missing!");
            return;
        }
        if (1 >= -s $diamond_blast_result_file)
        {
            LogInfo("Empty BLAST file '$diamond_blast_result_file'. Skipping.");
            return;
        }
        

        LogDebug("Parsing BLAST result file '$diamond_blast_result_file' for family $family");

        $hit = HasRefSeqHit($family, $diamond_blast_result_file);
        # Stops at first bank hit.
        last if $hit;
    }

    # Update family.
    my $sql_query = 'UPDATE families SET plant_specific = ? WHERE id = ?;';
    GetDatabaseHandler()->do($sql_query, undef, ($hit ? 0 : 100), $family->id)
        or confess GetDatabaseHandler()->errstr;
    # And its parents if not specific.
    if ($hit)
    {
        $sql_query = "
            UPDATE families
            SET plant_specific = 0
            WHERE
                id IN (
                    SELECT fr.object_family_id
                    FROM family_relationships_cache fr
                    WHERE
                        fr.subject_family_id = ?
                        AND fr.level_delta < 0
                        AND fr.type= 'inheritance'
                );";
        GetDatabaseHandler()->do($sql_query, undef, $family->id)
            or confess GetDatabaseHandler()->errstr;
    }
}




# Script options
#################

=pod

=head1 OPTIONS

update_plant_spe.pl [-help|-man]
update_plant_spe.pl [-debug] [-f FASTA_OUT] [-c CDHIT_OUT] [-b BLAST_OUT]
               [-q|-noprompt] [-complete]
               [-init]
               [-[no-]update-refseq]
               [-[no-]generate-family-fasta]
               [-[no-]generate-cdhit-fasta]
               [-[no-]blast]
               [-[no-]update-db]

=head2 Parameters

=over 4

=item B<-help>:

Prints a brief help message and exits.

=item B<-man>:

Prints the manual page and exits.

=item B<-debug> (integer):

Executes the script in debug mode. If an integer value is specified, it will
be the debug level. If "-debug" option was used without specifying a debug
level, level 1 is assumed.
Default: 0 (not in debug mode).

=item B<-f> (string):

Path where family FASTA files should be put or found.

=item B<-c> (string):

Path where family CD HIT FASTA files should be put or found.

=item B<-b> (string):

Path where BLAST output files should be put or found.

=item B<-init>:

Create missing output directories before starting.

=item B<-q or -noprompt>:

If set, no user prompt will be displayed and default settings will be used.

=item B<-complete>:

If set, existing family FASTA files will not be replaced.

=item B<-bank> (string):

Select the BLAST bank to blast on. Default: refseq no plants.

=item B<-generate-family-fasta or -no-generate-family-fasta>:

Set the default setting for family FASTA generation.

=item B<-generate-cdhit-fasta or -no-generate-cdhit-fasta>:

Set the default setting for family CD HIT FASTA generation.

=item B<-update-refseq or -no-update-refseq>:

Set the default setting for RefSeq BLAST banks update.

=item B<-blast or -no-blast>:

Set the default setting for BLAST execution on CD HIT Fasta. If disabled
(-no-blast), the script will assume BLAST output are already available.

=item B<-update-db or -no-update-db>:

Enable or disable database update. (Default: enabled)

=back

=cut


# CODE START
#############

# options processing
my ($man, $help, $debug) = (0, 0, 0);
my $family_output_path   = GP('TEMP_OUTPUT_PATH') . '/family_fasta';
my $cdhit_output_path    = GP('TEMP_OUTPUT_PATH') . '/cdhit_fasta';
my $blast_output_path    = GP('TEMP_OUTPUT_PATH') . '/plant_spe_blast';
my $no_prompt = 0;
my $complete = 0;
my $generate_family_fasta = 1;
my $generate_cdhit_fasta  = 1;
my $update_refseq         = 0;
my $run_blast             = 1;
my $db_update             = 1;
my $blast_bank            = '';
my $init_directories      = 0;

# parse options and print usage if there is a syntax error.
GetOptions('help|?'     => \$help,
           'man'        => \$man,
           'debug:s'    => \$debug,
           'f=s'        => \$family_output_path,
           'c=s'        => \$cdhit_output_path,
           'b=s'        => \$blast_output_path,
           'q|noprompt' => \$no_prompt,
           'complete'   => \$complete,
           'generate-family-fasta!' => \$generate_family_fasta,
           'generate-cdhit-fasta!'  => \$generate_cdhit_fasta,
           'update-refseq!'         => \$update_refseq,
           'blast!'                 => \$run_blast,
           'update-database|update-db!' => \$db_update,
           'init'       => \$init_directories,
           @Greenphyl::Log::LOG_GETOPT, # skip log parameters
) or pod2usage(1);
if ($help) {pod2usage('-verbose' => 1, '-exitval' => 0);}
if ($man) {pod2usage('-verbose' => 2,  '-exitval' => 0);}

$DEBUG ||= $debug;

if (!$blast_output_path)
{
    warn "ERROR: Missing parameter!\n";
    pod2usage('-verbose' => 2, '-exitval' => 0);
}

# Parameters check...
# -family FASTA  directory
if (!$family_output_path)
{
    confess "ERROR: no family FASTA output directory specified!\n";
}
elsif (!-e $family_output_path)
{
    if (!$init_directories || system("mkdir -p $family_output_path"))
    {
        confess "ERROR: family FASTA output directory ('$family_output_path') does not exist!\n";
    }
}

if (!$cdhit_output_path)
{
    confess "ERROR: no CD HIT output directory specified!\n";
}
elsif (!-e $cdhit_output_path)
{
    if (!$init_directories || system("mkdir -p $cdhit_output_path"))
    {
        confess "ERROR: CD HIT output directory ('$cdhit_output_path') does not exist!\n";
    }
}

if (!$blast_output_path)
{
    confess "ERROR: no BLAST output directory specified!\n";
}
elsif (!-e $blast_output_path)
{
    if (!$init_directories || system("mkdir -p $blast_output_path"))
    {
        confess "ERROR: BLAST output directory ('$blast_output_path') does not exist!\n";
    }
}

eval
{
    LogDebug('Running in debug mode...');
    my $answer;
    
    # Update RefSeq?
    $answer = Prompt("Update local RefSeq banks (in '" . GP('REFSEQ_PATH') . "' to generate '" . GP('REFSEQ_NOPLANT_BANK') . "')? [Y/N]", { 'default' => ($update_refseq?'Y':'N'), 'constraint' => '^[yYnN]', }, $no_prompt);
    if ($answer =~ m/Y/i)
    {
        $update_refseq = 1;
        UpdateRefSeq();
    }
    else
    {
        $update_refseq = 0;
    }

    LoopOnFamilyLists(
        sub
        {
            my ($selected_families, $family_selection_hash) = @_;
            # FASTA generation
            $answer = Prompt("Generate family FASTA (in '$family_output_path')? [Y/N]", { 'default' => ($generate_family_fasta?'Y':'N'), 'constraint' => '^[yYnN]', }, $no_prompt);
            if ($answer =~ m/Y/i)
            {
                $generate_family_fasta = 1;
                my $family_selection_arguments = GetFamilySelectionArguments(\@ARGV, $family_selection_hash);
                my $fasta_command =
                    GP('UTILS_PATH') . '/process_families.pl dbindex=' . SelectActiveDatabase() . ' '
                    . ($complete ? '-complete' : '-generate')
                    . ' -fasta -without-splice -f ' . $family_output_path
                    . ' ' . $family_selection_arguments
                    . ' ' . GetLogCommandLineArguments()
                ;
                LogDebug("Generating family FASTA using command:\n$fasta_command\n");
                if (system($fasta_command))
                {
                    confess "ERROR: Failed to generate family FASTA files!\n$!\nSee logs for details.";
                }
            }
            else
            {
                $generate_family_fasta = 0;
            }

            # CD HIT
            $answer = Prompt("Generate CD HIT FASTA files (in '$cdhit_output_path')? [Y/N]", { 'default' => ($generate_cdhit_fasta?'Y':'N'), 'constraint' => '^[yYnN]', }, $no_prompt);
            if ($answer =~ m/Y/i)
            {
                $generate_cdhit_fasta = 1;
                foreach my $family (@$selected_families)
                {
                    GenerateCDHIT($family, $family_output_path, $cdhit_output_path, $complete);
                }
            }
            else
            {
                $generate_cdhit_fasta = 0;
            }

            # Run Diamond?
            $answer = Prompt("Run Diamond (on FASTA in '$cdhit_output_path' and output into '$blast_output_path')? [Y/N]", { 'default' => ($run_blast?'Y':'N'), 'constraint' => '^[yYnN]', }, $no_prompt);
            if ($answer =~ m/Y/i)
            {
                $run_blast = 1;
                foreach my $family (@$selected_families)
                {
                    # Loop on all refseq banks unless a hit is found.
                    my $hit = 0;
                    REFSEQ_BANK:
                    foreach my $bank_name (@{GP('REFSEQ_BANKS')})
                    {
                        my $output_blast = DiamondFamily($family, $cdhit_output_path, $blast_output_path, $complete, $bank_name);
                        if (!$output_blast)
                        {
                          cluck LogWarning("RefSeq bank $bank_name could not be used on $family. Stopping this family scan.");
                          last REFSEQ_BANK;
                        }
                        $hit = HasRefSeqHit($family, $output_blast);
                        # Stops at first bank hit.
                        last if $hit;
                    }
                }
            }
            else
            {
                $run_blast = 0;
            }

            # Update database
            if ($db_update)
            {
                LogDebug('Updating ' . scalar(@$selected_families) . ' families in database...');
                # start SQL transaction
                # if (GetDatabaseHandler()->{'AutoCommit'})
                # {
                #     GetDatabaseHandler()->begin_work() or croak GetDatabaseHandler()->errstr;
                # }

                # Parse results
                foreach my $family (@$selected_families)
                {
                    LogDebug('Updating family ' . $family->accession . ' plant-specificity status');
                    UpdatePlantSpecificFamily($family, $blast_output_path);
                }

                # if (!$debug)
                # {
                #     # commit SQL transaction
                #     GetDatabaseHandler()->commit() or confess(GetDatabaseHandler()->errstr);
                # }
                # else
                # {
                #     # rollback transaction
                #     GetDatabaseHandler()->rollback() or confess(GetDatabaseHandler()->errstr);
                # }
                LogDebug('Family update done.');
            }
            else
            {
                LogDebug('Skipping database update');
            }

            # we don't want to prompt the user again for the same questions for next families
            $no_prompt = 1;
        }
    );
};


my $error;

if ($error = Exception::Class->caught())
{
    warn "An error occured!\n" . $error;
}


exit(0);

# CODE END
###########


=pod

=head1 AUTHORS

Mathieu ROUARD (Bioversity), m.rouard@cgiar.org

Valentin GUIGNON (Bioversity), v.guignon@cgiar.org

Matthieu CONTE (Syngenta)

=head1 VERSION

Version 2.0.0

Date 20/08/2020

=head1 SEE ALSO

GreenPhyl documentation.

=cut
