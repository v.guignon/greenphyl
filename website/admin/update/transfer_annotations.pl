#!/usr/bin/env perl

=pod

=head1 NAME

transfer_annotations.pl - Transfer family annotations from custom families

=head1 SYNOPSIS

    transfer_annotations.pl

=head1 REQUIRES

Perl5

=head1 DESCRIPTION

Transfer family annotations from a given set of custom families to the best
matching GreenPhyl families.

=cut

use strict;
use warnings;

use FindBin;
use lib "$FindBin::Bin/../../lib";
use lib "$FindBin::Bin/../../local_lib";
use Carp qw (cluck confess croak);

use Greenphyl;
use Greenphyl::Log('nolog' => 1,);
use Greenphyl::User;
use Greenphyl::CustomFamily;
use Greenphyl::CustomSequence;
use Greenphyl::Sequence;
use Greenphyl::SourceDatabase;
use Greenphyl::AbstractFamily;
use Greenphyl::DBXRef;

use Getopt::Long;
use Pod::Usage;

++$|; #no buffering




# Script global constants
##########################

=pod

=head1 CONSTANTS

B<$REV_STRING>: (string)

Current revision (auto-set by GIT).

B<$DEBUG>: (boolean)

When set to true, it enables debug mode.

B<$CONSTANT_NAME>: ([constant nature]) #+++

[constant description and use]. #+++

=cut

# SVN management part
my $REV_STRING = '$id$';

our $DEBUG = 0;

our $CUSTOM_FAMILY_BATCH_SIZE = 1000;
our $DEFAULT_COMMON_SEQUENCE_THRESHOLD = 60; # percent [0-100]
our %VALIDATION_CONVERSION_HASH = (
    '0' => 'N/A',
    '1' => 'high',
    '2' => 'normal',
    '3' => 'unknown',
    '4' => 'suspicious',
    '5' => 'clustering error',
);

our $FAMILY_PREFIX = 'GP';




# Script global variables
##########################

=pod

=head1 VARIABLES

B<[g_variable_name]>: ([variable nature]) #+++

[variable description, use and default value]. #+++
Default: [variable default value if one] #+++

#--- Example:
#--- B<$g_output_method>: (integer)
#---
#--- used to store current output method;
#--- 0=raw text (default), 1=image, 2=html.
#---
#---     ...
#---
#--- my $g_output_method = 0;
#---

=cut

our $g_dbh = GetDatabaseHandler();




# Script global functions
##########################

=pod

=head1 FUNCTIONS

=head2 GetCandidates

B<Description>: [function description]. #+++

B<ArgsCount>: [count of arguments] #+++

=over 4

=item $variable_name: ([variable nature]) ([requirements]) #+++ see below

#--- requirement can be:
#--- (R)=required,
#--- (O)=optional
#--- (U)=optional and must be undef if omitted
[variable description]. #+++

=item $variable_name2: ([variable nature]) ([requirements]) #+++

[variable description]. #+++

=back

B<Return>: ([return type]) #+++

[return description]. #+++

B<Example>:

    [example code] #+++

=cut

sub GetCandidates
{
    my ($custom_family) = @_;
    my %candidate_families;
    my $total_common_sequences = 0;

    LogVerboseInfo("Searching candidate families for '$custom_family'...");
    if ($custom_family->level)
    {
        LogVerboseInfo("Limit candidates to level " . $custom_family->level);
    }

    # loop on custom family sequences that have an associated non-custom sequence
    foreach my $custom_sequence (@{$custom_family->sequences()})
    {
        # get the associated sequence (there should only be one or none)
        my ($associated_sequence) = @{$custom_sequence->getAssociatedSequences($Greenphyl::CustomSequence::SEQUENCE_RELATIONSHIP_ASSOCIATED)};
        if ($associated_sequence)
        {
            ++$total_common_sequences;
            my $candidate_family;
            # check if we work on a specific level
            if ($custom_family->level)
            {
                # get family of that level
                ($candidate_family) = @{$associated_sequence->fetchFamilies({'selectors' => {'level' => $custom_family->level}})};
            }
            else
            {
                ($candidate_family) = @{$associated_sequence->families()};
            }
            
            if ($candidate_family)
            {
                # add candidate family if needed
                $candidate_families{$candidate_family->id} ||= [$candidate_family, 0];
                # update common sequence counter
                $candidate_families{$candidate_family->id}->[1]++;
            }
        }
    }
    
    LogVerboseInfo("...found  " . scalar(keys(%candidate_families)) . " candidate family/ies for '$custom_family' ($total_common_sequences common sequences)");
    
    return ([values(%candidate_families)], $total_common_sequences);
}


=pod

=head2 FindBestCandidate

B<Description>: [function description]. #+++

B<ArgsCount>: [count of arguments] #+++

=over 4

=item $variable_name: ([variable nature]) ([requirements]) #+++ see below

#--- requirement can be:
#--- (R)=required,
#--- (O)=optional
#--- (U)=optional and must be undef if omitted
[variable description]. #+++

=item $variable_name2: ([variable nature]) ([requirements]) #+++

[variable description]. #+++

=back

B<Return>: ([return type]) #+++

[return description]. #+++

B<Example>:

    [example code] #+++

=cut

sub FindBestCandidate
{
    my ($custom_family, $candidate_families, $total_common_sequences, $min_threshold) = @_;
    
    if (!$total_common_sequences)
    {
        LogVerboseInfo("No best candidate found for '$custom_family': no common sequence!");
        return undef;
    }
    
    my $best_candidate = [undef, 0];
    # find candidate with the highest number of common sequences
    foreach my $candidate (@$candidate_families)
    {
        if ($candidate->[1] > $best_candidate->[1])
        {
            $best_candidate = $candidate;
        }
    }

    # check if it represent at least 60% of the $total_common_sequences
    if ($min_threshold <= ($best_candidate->[1] / $total_common_sequences))
    {
        LogVerboseInfo("Got a best candidate found for '$custom_family': '" . $best_candidate->[0] . "' ($best_candidate->[1] common sequences / $total_common_sequences, " . sprintf("%i%%", 100 * $best_candidate->[1] / $total_common_sequences) . ", total sequences in '$custom_family': " . $custom_family->countSequences() . ", total sequences in '" . $best_candidate->[0] . "': " . $best_candidate->[0]->countSequences() . ")");
        return {
            'family' => $best_candidate->[0],
            'common_sequence_count' => $best_candidate->[1],
            'total_common_sequences' => $total_common_sequences,
        };
    }
    else
    {
        LogVerboseInfo("No best candidate found for '$custom_family': not enough common sequences");
        return undef;
    }
}


=pod

=head2 TransferDBXRef

B<Description>: [function description]. #+++

B<ArgsCount>: [count of arguments] #+++

=over 4

=item $variable_name: ([variable nature]) ([requirements]) #+++ see below

#--- requirement can be:
#--- (R)=required,
#--- (O)=optional
#--- (U)=optional and must be undef if omitted
[variable description]. #+++

=item $variable_name2: ([variable nature]) ([requirements]) #+++

[variable description]. #+++

=back

B<Return>: ([return type]) #+++

[return description]. #+++

B<Example>:

    [example code] #+++

=cut

sub TransferDBXRef
{
    my ($custom_family, $selected_family) = @_;

    # transfer dbxref
    if (@{$custom_family->data->{'dbxref'}})
    {
        foreach my $dbxref_data (@{$custom_family->data->{'dbxref'}})
        {
            # get corresponding database
            my $source_db = Greenphyl::SourceDatabase->new(
                $g_dbh,
                {
                    'selectors' => {
                        'name' => $dbxref_data->{'db'},
                    },
                },
            );
            # if not available, error
            if (!$source_db)
            {
                confess LogError("Failed to load source database '$dbxref_data->{db}'!");
            }
            # try to insert dbxref if missing
            my $sql_query = "
                INSERT IGNORE INTO dbxref
                    (db_id, accession, description, version)
                VALUE (?, ?, ?, ?);
            ";
            $g_dbh->do($sql_query, undef,
                    $source_db->id,
                    $dbxref_data->{'accession'},
                    $dbxref_data->{'description'},
                    $dbxref_data->{'version'},
                )
                or LogError("Failed to insert dbxref '$dbxref_data->{'accession'}' for family '$selected_family'!" . $g_dbh->errstr);
            # load dbxref
            my $dbxref = Greenphyl::DBXRef->new(
                $g_dbh,
                {
                    'selectors' => {
                        'accession' => $dbxref_data->{'accession'},
                        'db_id' => $source_db->id,
                    },
                },
            );

            if (!$dbxref)
            {
                confess LogError("Unable to retrieve dbxref '$dbxref_data->{'accession'}' for family '$selected_family'");
            }

            # try to add synonyms if missing
            $sql_query = "
                INSERT IGNORE INTO dbxref_synonyms
                    (dbxref_id, synonym)
                VALUE (?, ?);
            ";
            $g_dbh->do($sql_query, undef,
                    $dbxref->id,
                    $dbxref_data->{'alias'},
                )
                or LogWarning("Failed to insert synonym '$dbxref_data->{'alias'}' for dbxref '$dbxref_data->{'accession'}' for family '$selected_family'!" . $g_dbh->errstr);
            # add family-dbxref association
            $sql_query = "
                INSERT IGNORE INTO dbxref_families
                    (family_id, dbxref_id)
                VALUE (?, ?);
            ";
            $g_dbh->do($sql_query, undef,
                    $selected_family->id,
                    $dbxref->id,
                )
                or LogError("Failed to insert dbxref_families relationship for dbxref '$dbxref' and family '$selected_family'!" . $g_dbh->errstr);
        }
    }
}


=pod

=head2 TransferAnnotation

B<Description>: [function description]. #+++

B<ArgsCount>: [count of arguments] #+++

=over 4

=item $variable_name: ([variable nature]) ([requirements]) #+++ see below

#--- requirement can be:
#--- (R)=required,
#--- (O)=optional
#--- (U)=optional and must be undef if omitted
[variable description]. #+++

=item $variable_name2: ([variable nature]) ([requirements]) #+++

[variable description]. #+++

=back

B<Return>: ([return type]) #+++

[return description]. #+++

B<Example>:

    [example code] #+++

=cut

sub TransferAnnotation
{
    my ($custom_family, $selected_family) = @_;

    # check if custom_family is annotated
    if ($custom_family->isAnnotated())
    {
        LogInfo("Transfering annotation from '$custom_family' to '$selected_family'");
        # transfer name
        $selected_family->name($custom_family->name);
        # transfer description
        $selected_family->description($custom_family->description);
        # transfer type
        $selected_family->type($custom_family->type);
        # transfer validation status
        $selected_family->validated($VALIDATION_CONVERSION_HASH{$custom_family->data->{'family_v3'}->validated});
        # transfer inferences
        $selected_family->inferences($custom_family->inferences);
        
        $selected_family->save();

    }
    else
    {
        LogInfo("Limited transfer from unannotated family '$custom_family' to '$selected_family'");
        # only transfer what has been left empty
        if (!$selected_family->name() || ($selected_family->name() eq $Greenphyl::AbstractFamily::UNANNOTATED_CLUSTER_NAME))
        {
            # transfer name
            $selected_family->name($custom_family->name);
        }
        if (!$selected_family->description() && $custom_family->description())
        {
            # transfer description
            $selected_family->description($custom_family->description);
        }

        if ($selected_family->type() eq 'N/A')
        {
            # transfer type
            $selected_family->type($custom_family->type);
        }
        
        if ($selected_family->validated() eq 'N/A')
        {
            # transfer validation status
            $selected_family->validated($VALIDATION_CONVERSION_HASH{$custom_family->data->{'family_v3'}->validated});
        }

        if (!$selected_family->inferences())
        {
            # transfer inferences
            $selected_family->inferences($custom_family->inferences);
        }
        
        $selected_family->save();
    }

    # transfer synonyms
    if (@{$custom_family->data->{'synonyms'}})
    {
        my $sql_query = "INSERT IGNORE INTO family_synonyms (family_id, synonym) VALUES ";
        $sql_query .= ('(' . $selected_family->id . ', ?), ') x scalar(@{$custom_family->data->{'synonyms'}});
        $sql_query =~ s/, $/;/;
        $g_dbh->do($sql_query, undef, @{$custom_family->data->{'synonyms'}})
            or LogError("Failed to transfer synonyms for family '$selected_family' (" . $selected_family->id . "):\n" . $g_dbh->errstr);
    }

    # transfer dbxref
    TransferDBXRef($custom_family, $selected_family);

    # track annotation transfer
    my $sql_query = "
        INSERT IGNORE INTO family_transfer_history
            (old_family_id, new_family_id, transaction_date, transaction_type, sequences_in_old_family, sequences_in_new_family, additional_info)
        VALUE
            (?, ?, NOW(), 'annotation_transfer', ?, ?, ?)
        ;
    ";
    my $additional_info = '';
    $g_dbh->do($sql_query, undef, $custom_family->data->{'family_v3'}->id, $selected_family->id, $custom_family->data->{'family_v3'}->countSequences(), $selected_family->countSequences(), $additional_info);

    # store custom_family-family relationship
    $sql_query = "
        INSERT IGNORE INTO custom_families_families_relationships
            (custom_family_id, family_id, type)
        VALUE (?, ?, '$Greenphyl::AbstractFamily::RELATIONSHIP_REPLACED_BY')
    ";
    $g_dbh->do($sql_query, undef, $custom_family->id, $selected_family->id);
}


=pod

=head2 TransferAnnotations

B<Description>: [function description]. #+++

B<ArgsCount>: [count of arguments] #+++

=over 4

=item $variable_name: ([variable nature]) ([requirements]) #+++ see below

#--- requirement can be:
#--- (R)=required,
#--- (O)=optional
#--- (U)=optional and must be undef if omitted
[variable description]. #+++

=item $variable_name2: ([variable nature]) ([requirements]) #+++

[variable description]. #+++

=back

B<Return>: ([return type]) #+++

[return description]. #+++

B<Example>:

    [example code] #+++

=cut

sub TransferAnnotations
{
    my ($source_user, $common_sequence_threshold, $report_filename, $report_only) = @_;
    
    if ((0 > $common_sequence_threshold) || (100 < $common_sequence_threshold))
    {
        confess LogError("Invalid threshold!");
    }
    if ((0 < $common_sequence_threshold) && (1 > $common_sequence_threshold))
    {
        LogWarning("Threshold between 0 and 1! Please make sure you really didn't want to specify a value between 0 and 100 instead of '$common_sequence_threshold%'!");
    }
    # convert percent into [0.-1.] float value
    my $min_threshold = $common_sequence_threshold / 100.;
    
    LogInfo("Processing custom families of " . $source_user->login . "...");

    my $report_fh;
    if ($report_filename && !open($report_fh, ">$report_filename"))
    {
        confess LogError("Unable to open report file: $!");
    }
    if ($report_fh)
    {
        print {$report_fh} "CFID\tCAccession\tCustom Family\tSequence Count\tGreenPhyl Sequence Count\tGFID\tAccession\tGreenPhyl Family\tSequence Count\tCommon Sequence Count\tOther Candidates\n";
    }

    # get custom family count
    my $sql_query = "SELECT COUNT(1) FROM custom_families WHERE user_id = " . $source_user->id . ";";
    my ($total_family_count) = $g_dbh->selectrow_array($sql_query);

    # loop on source families
    my $annotated_family_hash = {};
    my $offset = 0;
    my $custom_families = [Greenphyl::CustomFamily->new($g_dbh, {'selectors' => {'user_id' => $source_user->id}, 'sql' => {'OFFSET' => $offset, 'LIMIT' => $CUSTOM_FAMILY_BATCH_SIZE}})];

    if (!@$custom_families)
    {
        LogError("No custom family found for user " . $source_user->login . "!");
        return;
    }

    while (@$custom_families)
    {
        LogVerboseInfo("Working on custom families [$offset, " . ($offset + $#$custom_families) . "]...");
        ANNOTATION_TRANSFER_LOOP:
        foreach my $custom_family (@$custom_families)
        {
            LogVerboseInfo("-working on custom family '" . $custom_family->accession . "'");

            # get candidates
            my ($candidate_families, $total_common_sequences) = GetCandidates($custom_family);

            # only keep families not already associated
            my $valid_candidates = [grep {!exists($annotated_family_hash->{$_->[0]->id})} @$candidate_families];
            if (scalar(@$candidate_families) != scalar(@$valid_candidates))
            {
                LogVerboseInfo("Removed " . (@$candidate_families - @$valid_candidates) . " already associated candidate(s).");
            }
            
            # find best matching family
            my $best_candidate = FindBestCandidate($custom_family, $valid_candidates, $total_common_sequences, $min_threshold);
            
            if ($best_candidate)
            {
                if (!$report_only)
                {
                    TransferAnnotation($custom_family, $best_candidate->{'family'});
                }

                if ($report_fh)
                {
                    print {$report_fh}
                        $custom_family->id()
                        . "\t"
                        . $custom_family->accession()
                        . "\t"
                        . $custom_family->name()
                        . "\t"
                        . $custom_family->countSequences()
                        . "\t"
                        . $best_candidate->{'total_common_sequences'}
                        . "\t"
                        . $best_candidate->{'family'}->id()
                        . "\t"
                        . $best_candidate->{'family'}->accession()
                        . "\t"
                        . $best_candidate->{'family'}->name()
                        . "\t"
                        . $best_candidate->{'family'}->countSequences()
                        . "\t"
                        . $best_candidate->{'common_sequence_count'}
                        . "\t"
                        . join(';', map { $_->[0]->accession . ' (' . $_->[0]->id . '): ' . $_->[0]->name . ' (' . $_->[0]->countSequences() . ' sequences, ' . $_->[1] . ' common)'} @$candidate_families)
                        . "\n";
                }

                $annotated_family_hash->{$best_candidate->{'family'}->id} = $custom_family->id;
            }
            elsif ($report_fh)
            {
                print {$report_fh}
                        $custom_family->id()
                        . "\t"
                        . $custom_family->accession()
                        . "\t"
                        . $custom_family->name()
                        . "\t"
                        . $custom_family->countSequences()
                        . "\t\t\t\t\t\t\t\n";
            }

        } # end of foreach

        LogVerboseInfo("...done with current batch of custom families.");
        print GetProgress(
            {'label' => 'Processed families:', 'current' => $offset + scalar(@$custom_families), 'total' => $total_family_count,},
            80,
            5, # seconds
        );
        
        # get next batch of custom families
        $offset += $CUSTOM_FAMILY_BATCH_SIZE;
        $custom_families = [Greenphyl::CustomFamily->new($g_dbh, {'selectors' => {'user_id' => $source_user->id}, 'sql' => {'OFFSET' => $offset, 'LIMIT' => $CUSTOM_FAMILY_BATCH_SIZE}})];
    } # end of while
    if ($total_family_count)
    {
        # print 100%
        print GetProgress(
            {'label' => 'Processed families:', 'current' => $total_family_count, 'total' => $total_family_count,},
            80,
            0, # seconds
        ) . "\n";
    }
    LogInfo("...done processing custom families.");
    if ($report_fh)
    {
        close($report_fh);
    }

}


=pod

=head2 TransferAccessions

B<Description>: [function description]. #+++

B<ArgsCount>: [count of arguments] #+++

=over 4

=item $variable_name: ([variable nature]) ([requirements]) #+++ see below

#--- requirement can be:
#--- (R)=required,
#--- (O)=optional
#--- (U)=optional and must be undef if omitted
[variable description]. #+++

=item $variable_name2: ([variable nature]) ([requirements]) #+++

[variable description]. #+++

=back

B<Return>: ([return type]) #+++

[return description]. #+++

B<Example>:

    [example code] #+++

=cut

sub TransferAccessions
{
    my ($source_user) = @_;

    LogInfo("Transfering accessions from " . $source_user->login . " families...");
    
    # get custom family count
    my $sql_query = "SELECT COUNT(1) FROM custom_families WHERE user_id = " . $source_user->id . ";";
    my ($total_family_count) = $g_dbh->selectrow_array($sql_query);

    # loop on source families
    my $offset = 0;
    my $custom_families = [Greenphyl::CustomFamily->new($g_dbh, {'selectors' => {'user_id' => $source_user->id}, 'sql' => {'OFFSET' => $offset, 'LIMIT' => $CUSTOM_FAMILY_BATCH_SIZE}})];

    if (!@$custom_families)
    {
        LogError("No custom family found for user " . $source_user->login . "!");
        return;
    }

    while (@$custom_families)
    {
        LogVerboseInfo("Working on custom families [$offset, " . ($offset + $#$custom_families) . "]...");
        ACCESSION_TRANSFER_LOOP:
        foreach my $custom_family (@$custom_families)
        {
            LogVerboseInfo("-working on custom family '" . $custom_family->accession . "'");
            my $families = $custom_family->getAssociatedFamilies($Greenphyl::AbstractFamily::RELATIONSHIP_REPLACED_BY);
            if (@$families)
            {
                if (1 < @$families)
                {
                    LogWarning("More than one family associated to custom family '$custom_family'! Accession not transfered.");
                }
                else
                {
                    my $new_accession = $FAMILY_PREFIX . substr($custom_family->accession, 2); # replace 'V3' or 'CF' with 'GP'
                    LogWarning("Transfer accession from '$custom_family' to '$families->[0]' (becomes '$new_accession')");
                    $families->[0]->accession($new_accession);
                    $families->[0]->save();
                }
            }
            else
            {
                LogVerboseInfo("No family associated to custom family '$custom_family'");
            }
        } # end of foreach

        LogVerboseInfo("...done with current batch of custom families.");
        print GetProgress(
            {'label' => 'Processed families:', 'current' => $offset + scalar(@$custom_families), 'total' => $total_family_count,},
            80,
            5, # seconds
        );
        
        # get next batch of custom families
        $offset += $CUSTOM_FAMILY_BATCH_SIZE;
        $custom_families = [Greenphyl::CustomFamily->new($g_dbh, {'selectors' => {'user_id' => $source_user->id}, 'sql' => {'OFFSET' => $offset, 'LIMIT' => $CUSTOM_FAMILY_BATCH_SIZE}})];
    } # end of while
    if ($total_family_count)
    {
        # print 100%
        print GetProgress(
            {'label' => 'Processed families:', 'current' => $total_family_count, 'total' => $total_family_count,},
            80,
            0, # seconds
        ) . "\n";
    }
    LogInfo("...done transfering accssions.");

}


=pod

=head2 GenerateNewAccessions

B<Description>: [function description]. #+++

B<ArgsCount>: [count of arguments] #+++

=over 4

=item $variable_name: ([variable nature]) ([requirements]) #+++ see below

#--- requirement can be:
#--- (R)=required,
#--- (O)=optional
#--- (U)=optional and must be undef if omitted
[variable description]. #+++

=item $variable_name2: ([variable nature]) ([requirements]) #+++

[variable description]. #+++

=back

B<Return>: ([return type]) #+++

[return description]. #+++

B<Example>:

    [example code] #+++

=cut

sub GenerateNewAccessions
{
    my ($start_accession) = @_;
    my $families = [Greenphyl::Family->new($g_dbh, {'selectors' => {'accession' => ['LIKE', 'LF%']}})];
    my $sql_query = "SELECT MAX(accession) FROM families;";
    if (!$start_accession)
    {
        my ($max_accession) = $g_dbh->selectrow_array($sql_query);
        $max_accession ||= 'GP000001';
        $start_accession = $max_accession;
        LogWarning("No new accession start has been specified, using default ('$start_accession')!");
    }
    # extract prefix and index
    my ($prefix, $index) = ($start_accession =~ m/([A-Z]+)(\d+)/i);
    $prefix ||= $FAMILY_PREFIX;
    $index ||= 1;
    LogVerboseInfo("Reassign accessions starting from '" . sprintf("%2s%06s", $prefix, $index) . "'...");
    foreach my $family (@$families)
    {
        my $new_accession = sprintf("%2s%06s", $prefix, $index++);
        LogVerboseInfo("Reassign accession: '$family' --> '$new_accession'");
        $family->accession($new_accession);
        $family->save();
    }
    LogVerboseInfo("...done reassign accessions!");
}




# Script options
#################

=pod

=head1 OPTIONS

#--- describes parameters given to the script
#+++ command line syntax
#--- requirement of the option and its parameter can be:
#--- required: name or nature inside <>
#--- optional: name or nature inside []
#--- alternative between 2 elements: elements separated by a |

=head2 Parameters

=over 4

#+++=item B<-help>:
#+++
#+++Prints a brief help message and exits.
#+++
#+++=item B<-man>:
#+++
#+++Prints the manual page and exits.
#+++
#+++=item B<-debug> (integer):
#+++
#+++Executes the script in debug mode. The integer value is optional and is used
#+++to set debug level (use 0 to force disable debug mode).
#+++Default: 0 (not in debug mode).
#+++

=item B<[option_name]> ([option nature]): #+++

[option description]. #+++
Default: [option default value if one] #+++

#--- remove if log not used
#+++=item B<-log>:
#+++
#+++Enable logging.
#+++
#+++=item B<-log-*> (any):
#+++
#+++Logging arguments.

=back
#--- Example:
#---
#--- Template.pl [-help | -man]
#---
#--- Template.pl [-debug [debug_level]] [-size <width> [height]]
#---
#--- =over 4
#---
#--- =item B<-help>:
#---
#--- Prints a brief help message and exits.
#---
#--- =item B<-man>:
#---
#--- Prints the manual page and exits.
#---
#--- =item B<-debug> (integer):
#---
#--- Executes the script in debug mode. If an integer value is specified, it will
#--- be the debug level. If "-debug" option was used without specifying a debug
#--- level, level 1 is assumed.
#--- Default: 0 (not in debug mode).
#---
#---=item B<-size> (positive_real) (positive_real):
#---
#--- Set the dimensions of the object that will be drawn. The first value is
#--- the width; the height is the second value if specified, otherwise it will
#--- assume height and width are equal to the first value.
#--- Default: width and height are set to 1.
#---
#---=back

=cut


# CODE START
#############

# for regression tests
if ($ENV{'TESTING'} && $ENV{'TESTING_NO_RUN'})
{return 1;}

# options processing
my ($man, $help, $debug, $no_prompt) = (0, 0, undef, undef);

my ($source_name, $transfer_accessions, $start_accession, $report_filename, $report_only);
my $common_sequence_threshold = $DEFAULT_COMMON_SEQUENCE_THRESHOLD;

# parse options and print usage if there is a syntax error.
GetOptions('help|?'            => \$help,
           'man'               => \$man,
           'debug:s'           => \$debug,
           'q|noprompt'        => \$no_prompt,
           'source=s'          => \$source_name,
           'transfer-accessions' => \$transfer_accessions,
           'start-accession=s'  => \$start_accession,
           'report=s'          => \$report_filename,
           'report-only'       => \$report_only,
           'threshold=f'       => \$common_sequence_threshold,
           @Greenphyl::Log::LOG_GETOPT, # skip log parameters
) or pod2usage(1);
if ($help) {pod2usage('-verbose' => 1, '-exitval' => 0);}
if ($man) {pod2usage('-verbose' => 2, '-exitval' => 0);}

# change debug mode if requested/forced
if (defined($debug))
{
    # just '-debug' ?
    if ($debug eq '')
    {
        $debug = 1;
    }
    $DEBUG = $debug;
}

if (!$source_name)
{
    warn "Missing custom family source name to use for annotation transfer!\n";
    pod2usage(1);
}

if ($report_only && !$report_filename)
{
    warn "Missing report file name!\n";
    pod2usage(1);
}

eval
{
    my $source_user = Greenphyl::User->new($g_dbh, {'selectors' => {'login' => $source_name}});
    if (!$source_user)
    {
        confess LogError("Source not found: '$source_name'");
    }

    if ($report_filename
        && (-e $report_filename)
        && (Prompt("'$report_filename' already exists! Replace it? [Y/N]", { 'default' => 'Y', 'constraint' => '^[ynYN]$', }, $no_prompt) =~ m/N/i))
    {
        confess LogError("Report file '$report_filename' already exists. Transfer aborted by user.");
    }

    # start SQL transaction
    if ($g_dbh->{'AutoCommit'})
    {
        $g_dbh->begin_work() or croak $g_dbh->errstr;
    }
    # set warning state (not fatal)
    $g_dbh->{'HandleError'} = undef;

    TransferAnnotations($source_user, $common_sequence_threshold, $report_filename, $report_only);

    if (!$report_only && $transfer_accessions)
    {
        # update accessions: process each v3 family and see if it is associated with a v4 family
        TransferAccessions($source_user);
        # once all families have been processed, reassign new accessions using $start_accession
        GenerateNewAccessions($start_accession);
    }

	SaveCurrentTransaction($g_dbh);

};

# catch
HandleErrors();

exit(0);

# CODE END
###########


=pod

=head1 AUTHORS

Mathieu ROUARD (Bioversity), m.rouard@cgiar.org

Valentin GUIGNON (Bioversity), v.guignon@cgiar.org

Matthieu CONTE (Syngenta)

Sebastien BRIOIS (Syngenta)

=head1 VERSION

Version 1.0.0

Date 02/12/2013

=head1 SEE ALSO

GreenPhyl documentation.

=cut
