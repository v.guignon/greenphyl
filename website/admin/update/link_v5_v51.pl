#!/usr/bin/env perl

=pod

=head1 NAME

transfer_annotations.pl - Transfer family annotations from custom families

=head1 SYNOPSIS

    transfer_annotations.pl

=head1 REQUIRES

Perl5

=head1 DESCRIPTION

Transfer family annotations from a given set of custom families to the best
matching GreenPhyl families.

=cut

use strict;
use warnings;

use FindBin;
use lib "$FindBin::Bin/../../lib";
use lib "$FindBin::Bin/../../local_lib";
use Carp qw (cluck confess croak);

use Greenphyl;
use Greenphyl::Log('nolog' => 1,);
use Greenphyl::User;
use Greenphyl::CustomFamily;
use Greenphyl::CustomSequence;
use Greenphyl::Sequence;
use Greenphyl::SourceDatabase;
use Greenphyl::AbstractFamily;
use Greenphyl::DBXRef;

use Getopt::Long;
use Pod::Usage;

++$|; #no buffering




# Script global constants
##########################

=pod

=head1 CONSTANTS

B<$REV_STRING>: (string)

Current revision (auto-set by GIT).

B<$DEBUG>: (boolean)

When set to true, it enables debug mode.

B<$CONSTANT_NAME>: ([constant nature]) #+++

[constant description and use]. #+++

=cut

# SVN management part
my $REV_STRING = '$id$';

our $DEBUG = 0;

our $CUSTOM_FAMILY_BATCH_SIZE = 1000;
our $DEFAULT_COMMON_SEQUENCE_THRESHOLD = 60; # percent [0-100]
our %VALIDATION_CONVERSION_HASH = (
    '0' => 'N/A',
    '1' => 'high',
    '2' => 'normal',
    '3' => 'unknown',
    '4' => 'suspicious',
    '5' => 'clustering error',
    'N/A' => 'N/A',
    'high' => 'high',
    'normal' => 'normal',
    'unknown' => 'unknown',
    'suspicious' => 'suspicious',
    'clustering error' => 'clustering error',
);

our $FAMILY_PREFIX = 'GP';




# Script global variables
##########################

=pod

=head1 VARIABLES

B<[g_variable_name]>: ([variable nature]) #+++

[variable description, use and default value]. #+++
Default: [variable default value if one] #+++

#--- Example:
#--- B<$g_output_method>: (integer)
#---
#--- used to store current output method;
#--- 0=raw text (default), 1=image, 2=html.
#---
#---     ...
#---
#--- my $g_output_method = 0;
#---

=cut

our $g_dbh = GetDatabaseHandler();




# Script global functions
##########################

=pod

=head1 FUNCTIONS

=head2 GetCandidates

B<Description>: [function description]. #+++

B<ArgsCount>: [count of arguments] #+++

=over 4

=item $variable_name: ([variable nature]) ([requirements]) #+++ see below

#--- requirement can be:
#--- (R)=required,
#--- (O)=optional
#--- (U)=optional and must be undef if omitted
[variable description]. #+++

=item $variable_name2: ([variable nature]) ([requirements]) #+++

[variable description]. #+++

=back

B<Return>: ([return type]) #+++

[return description]. #+++

B<Example>:

    [example code] #+++

=cut

sub GetCandidates
{
    my ($custom_family) = @_;
    my %candidate_families;
    my $total_common_sequences = 0;

    LogVerboseInfo("Searching candidate families for '$custom_family'...");
    if ($custom_family->level)
    {
        LogVerboseInfo("Limit candidates to level " . $custom_family->level);
    }

    # loop on custom family sequences that have an associated non-custom sequence
    foreach my $custom_sequence (@{$custom_family->sequences()})
    {
        # get the associated sequence (there should only be one or none)
        my ($associated_sequence, @rest_seq) = @{$custom_sequence->getAssociatedSequences($Greenphyl::CustomSequence::SEQUENCE_RELATIONSHIP_ASSOCIATED)};
        if (@rest_seq)
        {
            LogWarning("More than one sequence associated with " . $custom_sequence . " (" . $custom_sequence->id . "): " . join(', ', ($associated_sequence, @rest_seq)));
        }
        if ($associated_sequence)
        {
            # We got an associated sequence.
            # Make sure we are working on a representative sequence.
            $associated_sequence = $associated_sequence->getRepresentativeSequence();
            
            ++$total_common_sequences;
            my $candidate_family;
            # check if we work on a specific level
            if ($custom_family->level)
            {
                # get family of that level if one.
                my $candidate_families = $associated_sequence->fetchFamilies({'selectors' => {'level' => $custom_family->level}});
                # Make sure it's not an orphan.
                if ($candidate_families && @$candidate_families)
                {
                    if (1 < @$candidate_families)
                    {
                        LogWarning("More than one candidate family for $custom_family (" . $custom_family->id . ") for sequence $custom_sequence. Keeping juste the first one.");
                    }
                    ($candidate_family) = (@$candidate_families);
                }
            }
            else
            {
                LogWarning("No level for custom family " . $custom_family . "(" . $custom_family->id . "). Will try to match any cluster regardless the level.");
                my $candidate_families = $associated_sequence->families();
                # Make sure it's not an orphan.
                if ($candidate_families && @$candidate_families)
                {
                    if (1 < @$candidate_families)
                    {
                        LogWarning("More than one candidate family for $custom_family (" . $custom_family->id . ") for sequence $custom_sequence. Keeping juste the first one.");
                    }
                    ($candidate_family) = (@$candidate_families);
                }
            }
            
            if ($candidate_family)
            {
                # add candidate family if needed
                $candidate_families{$candidate_family->id} ||= [$candidate_family, 0];
                # update common sequence counter
                $candidate_families{$candidate_family->id}->[1]++;
            }
        }
    }
    LogVerboseInfo("...found  " . scalar(keys(%candidate_families)) . " candidate family/ies for '$custom_family' ($total_common_sequences common sequences)");
    
    return ([values(%candidate_families)], $total_common_sequences);
}


=pod

=head2 FindBestCandidate

B<Description>: [function description]. #+++

B<ArgsCount>: [count of arguments] #+++

=over 4

=item $variable_name: ([variable nature]) ([requirements]) #+++ see below

#--- requirement can be:
#--- (R)=required,
#--- (O)=optional
#--- (U)=optional and must be undef if omitted
[variable description]. #+++

=item $variable_name2: ([variable nature]) ([requirements]) #+++

[variable description]. #+++

=back

B<Return>: ([return type]) #+++

[return description]. #+++

B<Example>:

    [example code] #+++

=cut

sub FindBestCandidate
{
    my ($custom_family, $candidate_families, $total_common_sequences, $min_threshold) = @_;
    
    if (!$total_common_sequences)
    {
        LogVerboseInfo("No best candidate found for '$custom_family': no common sequence!");
        return undef;
    }
    
    my $best_candidate = [undef, 0];
    # find candidate with the highest number of common sequences
    foreach my $candidate (@$candidate_families)
    {
        if ($candidate->[1] > $best_candidate->[1])
        {
            $best_candidate = $candidate;
        }
    }

    # check if it represent at least 60% of the $total_common_sequences
    if ($min_threshold <= ($best_candidate->[1] / $total_common_sequences))
    {
        LogVerboseInfo("Got a best candidate found for '$custom_family': '" . $best_candidate->[0] . "' ($best_candidate->[1] common sequences / $total_common_sequences, " . sprintf("%i%%", 100 * $best_candidate->[1] / $total_common_sequences) . ", total sequences in '$custom_family': " . $custom_family->countSequences() . ", total sequences in '" . $best_candidate->[0] . "': " . $best_candidate->[0]->countSequences() . ")");
        return {
            'family' => $best_candidate->[0],
            'common_sequence_count' => $best_candidate->[1],
            'total_common_sequences' => $total_common_sequences,
        };
    }
    else
    {
        LogVerboseInfo("No best candidate found for '$custom_family': not enough common sequences (max of " . $best_candidate->[1] . " sequences in a family over " . $total_common_sequences . " total sequences having a new version)");
        return undef;
    }
}


=pod

=head2 LinkFamilies

B<Description>: [function description]. #+++

B<ArgsCount>: [count of arguments] #+++

=over 4

=item $variable_name: ([variable nature]) ([requirements]) #+++ see below

#--- requirement can be:
#--- (R)=required,
#--- (O)=optional
#--- (U)=optional and must be undef if omitted
[variable description]. #+++

=item $variable_name2: ([variable nature]) ([requirements]) #+++

[variable description]. #+++

=back

B<Return>: ([return type]) #+++

[return description]. #+++

B<Example>:

    [example code] #+++

=cut

sub LinkFamilies
{
    my ($custom_family, $selected_family) = @_;

    # store custom_family-family relationship
    my $sql_query = "
        INSERT IGNORE INTO custom_families_families_relationships
            (custom_family_id, family_id, type)
        VALUE (?, ?, '$Greenphyl::AbstractFamily::RELATIONSHIP_REPLACED_BY')
    ";
    $g_dbh->do($sql_query, undef, $custom_family->id, $selected_family->id);
}


=pod

=head2 FindAndStoreFamilyLinks

B<Description>: [function description]. #+++

B<ArgsCount>: [count of arguments] #+++

=over 4

=item $variable_name: ([variable nature]) ([requirements]) #+++ see below

#--- requirement can be:
#--- (R)=required,
#--- (O)=optional
#--- (U)=optional and must be undef if omitted
[variable description]. #+++

=item $variable_name2: ([variable nature]) ([requirements]) #+++

[variable description]. #+++

=back

B<Return>: ([return type]) #+++

[return description]. #+++

B<Example>:

    [example code] #+++

=cut

sub FindAndStoreFamilyLinks
{
    my ($source_user, $common_sequence_threshold, $report_filename, $report_only) = @_;
    
    if ((0 > $common_sequence_threshold) || (100 < $common_sequence_threshold))
    {
        confess LogError("Invalid threshold!");
    }
    if ((0 < $common_sequence_threshold) && (1 > $common_sequence_threshold))
    {
        LogWarning("Threshold between 0 and 1! Please make sure you really didn't want to specify a value between 0 and 100 instead of '$common_sequence_threshold%'!");
    }
    # convert percent into [0.-1.] float value
    my $min_threshold = $common_sequence_threshold / 100.;
    
    LogInfo("Processing custom families of " . $source_user->login . "...");

    my $report_fh;
    if ($report_filename && !open($report_fh, ">$report_filename"))
    {
        confess LogError("Unable to open report file: $!");
    }
    if ($report_fh)
    {
        print {$report_fh} "CFID\tCAccession\tCustom Family\tSequence Count\tGreenPhyl Sequence Count\tGFID\tAccession\tGreenPhyl Family\tSequence Count\tCommon Sequence Count\tOther Candidates\n";
    }

    # get custom family count
    my $sql_query = "SELECT COUNT(1) FROM custom_families WHERE user_id = " . $source_user->id . ";";
    my ($total_family_count) = $g_dbh->selectrow_array($sql_query);

    # loop on source families
    my $annotated_family_hash = {};
    my $offset = 0;
    my $custom_families = [Greenphyl::CustomFamily->new($g_dbh, {'selectors' => {'user_id' => $source_user->id}, 'sql' => {'OFFSET' => $offset, 'LIMIT' => $CUSTOM_FAMILY_BATCH_SIZE}})];

    if (!@$custom_families)
    {
        LogError("No custom family found for user " . $source_user->login . "!");
        return;
    }

    while (@$custom_families)
    {
        LogVerboseInfo("Working on custom families [$offset, " . ($offset + $#$custom_families) . "]...");
        ANNOTATION_TRANSFER_LOOP:
        foreach my $custom_family (@$custom_families)
        {
            LogVerboseInfo("-working on custom family '" . $custom_family->accession . "'");

            # get candidates
            my ($candidate_families, $total_common_sequences) = GetCandidates($custom_family);

            # only keep families not already associated
            my $valid_candidates = [grep {!exists($annotated_family_hash->{$_->[0]->id})} @$candidate_families];
            if (scalar(@$candidate_families) != scalar(@$valid_candidates))
            {
                LogVerboseInfo("Removed " . (@$candidate_families - @$valid_candidates) . " already associated candidate(s).");
            }
            
            # find best matching family
            my $best_candidate = FindBestCandidate($custom_family, $valid_candidates, $total_common_sequences, $min_threshold);
            
            if ($best_candidate)
            {
                if (!$report_only)
                {
                    LinkFamilies($custom_family, $best_candidate->{'family'});
                }

                if ($report_fh)
                {
                    print {$report_fh}
                        $custom_family->id()
                        . "\t"
                        . $custom_family->accession()
                        . "\t"
                        . $custom_family->name()
                        . "\t"
                        . $custom_family->countSequences()
                        . "\t"
                        . $best_candidate->{'total_common_sequences'}
                        . "\t"
                        . $best_candidate->{'family'}->id()
                        . "\t"
                        . $best_candidate->{'family'}->accession()
                        . "\t"
                        . $best_candidate->{'family'}->name()
                        . "\t"
                        . $best_candidate->{'family'}->countSequences()
                        . "\t"
                        . $best_candidate->{'common_sequence_count'}
                        . "\t"
                        . join(';', map { $_->[0]->accession . ' (' . $_->[0]->id . '): ' . $_->[0]->name . ' (' . $_->[0]->countSequences() . ' sequences, ' . $_->[1] . ' common)'} @$candidate_families)
                        . "\n";
                }

                $annotated_family_hash->{$best_candidate->{'family'}->id} = $custom_family->id;
            }
            elsif ($report_fh)
            {
                print {$report_fh}
                        $custom_family->id()
                        . "\t"
                        . $custom_family->accession()
                        . "\t"
                        . $custom_family->name()
                        . "\t"
                        . $custom_family->countSequences()
                        . "\t\t\t\t\t\t\t\n";
            }

        } # end of foreach

        LogVerboseInfo("...done with current batch of custom families.");
        print GetProgress(
            {'label' => 'Processed families:', 'current' => $offset + scalar(@$custom_families), 'total' => $total_family_count,},
            80,
            5, # seconds
        );
        
        # get next batch of custom families
        $offset += $CUSTOM_FAMILY_BATCH_SIZE;
        $custom_families = [Greenphyl::CustomFamily->new($g_dbh, {'selectors' => {'user_id' => $source_user->id}, 'sql' => {'OFFSET' => $offset, 'LIMIT' => $CUSTOM_FAMILY_BATCH_SIZE}})];
    } # end of while
    if ($total_family_count)
    {
        # print 100%
        print GetProgress(
            {'label' => 'Processed families:', 'current' => $total_family_count, 'total' => $total_family_count,},
            80,
            0, # seconds
        ) . "\n";
    }
    LogInfo("...done processing custom families.");
    if ($report_fh)
    {
        close($report_fh);
    }

}


# Script options
#################

=pod

=head1 OPTIONS

#--- describes parameters given to the script
#+++ command line syntax
#--- requirement of the option and its parameter can be:
#--- required: name or nature inside <>
#--- optional: name or nature inside []
#--- alternative between 2 elements: elements separated by a |

=head2 Parameters

=over 4

#+++=item B<-help>:
#+++
#+++Prints a brief help message and exits.
#+++
#+++=item B<-man>:
#+++
#+++Prints the manual page and exits.
#+++
#+++=item B<-debug> (integer):
#+++
#+++Executes the script in debug mode. The integer value is optional and is used
#+++to set debug level (use 0 to force disable debug mode).
#+++Default: 0 (not in debug mode).
#+++

=item B<[option_name]> ([option nature]): #+++

[option description]. #+++
Default: [option default value if one] #+++

#--- remove if log not used
#+++=item B<-log>:
#+++
#+++Enable logging.
#+++
#+++=item B<-log-*> (any):
#+++
#+++Logging arguments.

=back
#--- Example:
#---
#--- Template.pl [-help | -man]
#---
#--- Template.pl [-debug [debug_level]] [-size <width> [height]]
#---
#--- =over 4
#---
#--- =item B<-help>:
#---
#--- Prints a brief help message and exits.
#---
#--- =item B<-man>:
#---
#--- Prints the manual page and exits.
#---
#--- =item B<-debug> (integer):
#---
#--- Executes the script in debug mode. If an integer value is specified, it will
#--- be the debug level. If "-debug" option was used without specifying a debug
#--- level, level 1 is assumed.
#--- Default: 0 (not in debug mode).
#---
#---=item B<-size> (positive_real) (positive_real):
#---
#--- Set the dimensions of the object that will be drawn. The first value is
#--- the width; the height is the second value if specified, otherwise it will
#--- assume height and width are equal to the first value.
#--- Default: width and height are set to 1.
#---
#---=back

=cut


# CODE START
#############

# for regression tests
if ($ENV{'TESTING'} && $ENV{'TESTING_NO_RUN'})
{return 1;}

# options processing
my ($man, $help, $debug, $no_prompt) = (0, 0, undef, undef);

my ($report_filename, $report_only);
my $common_sequence_threshold = $DEFAULT_COMMON_SEQUENCE_THRESHOLD;

# parse options and print usage if there is a syntax error.
GetOptions('help|?'            => \$help,
           'man'               => \$man,
           'debug:s'           => \$debug,
           'q|noprompt'        => \$no_prompt,
           'report=s'          => \$report_filename,
           'report-only'       => \$report_only,
           'threshold=f'       => \$common_sequence_threshold,
           @Greenphyl::Log::LOG_GETOPT, # skip log parameters
) or pod2usage(1);
if ($help) {pod2usage('-verbose' => 1, '-exitval' => 0);}
if ($man) {pod2usage('-verbose' => 2, '-exitval' => 0);}

# change debug mode if requested/forced
if (defined($debug))
{
    # just '-debug' ?
    if ($debug eq '')
    {
        $debug = 1;
    }
    $DEBUG = $debug;
}

if ($report_only && !$report_filename)
{
    warn "Missing report file name!\n";
    pod2usage(1);
}

eval
{
    my $source_user = Greenphyl::User->new($g_dbh, {'selectors' => {'login' => 'greenphyl_v5'}});
    if (!$source_user)
    {
        confess LogError("Source not found: 'greenphyl_v5'");
    }

    if ($report_filename
        && (-e $report_filename)
        && (Prompt("'$report_filename' already exists! Replace it? [Y/N]", { 'default' => 'Y', 'constraint' => '^[ynYN]$', }, $no_prompt) =~ m/N/i))
    {
        confess LogError("Report file '$report_filename' already exists. Transfer aborted by user.");
    }

    # start SQL transaction
    if ($g_dbh->{'AutoCommit'})
    {
        $g_dbh->begin_work() or croak $g_dbh->errstr;
    }
    # set warning state (not fatal)
    $g_dbh->{'HandleError'} = undef;

    FindAndStoreFamilyLinks($source_user, $common_sequence_threshold, $report_filename, $report_only);

    SaveCurrentTransaction($g_dbh);

};

# catch
HandleErrors();

exit(0);

# CODE END
###########


=pod

=head1 AUTHORS

Mathieu ROUARD (Bioversity), m.rouard@cgiar.org

Valentin GUIGNON (Bioversity), v.guignon@cgiar.org

=head1 VERSION

Version 2.0.0

Date 15/01/2020

=head1 SEE ALSO

GreenPhyl documentation.

=cut
