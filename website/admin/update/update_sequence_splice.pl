#!/usr/bin/perl

=pod

=head1 NAME

update_sequence_splice.pl - update sequences splice form data

=head1 SYNOPSIS

    update_sequence_splice.pl

=head1 REQUIRES

Perl5, GreenPhyl API v3

=head1 DESCRIPTION

This script updates sequence splice form data. It updates:
-sequences.locus
-sequences.splice
-sequences.splice_priority
-sequence_count_by_families_species_cache
-sequence_no_splice_by_families_species_cache

Requiered tables to be filled:
-families_sequences

=cut

use strict;
use warnings;

use FindBin;
use lib "$FindBin::Bin/../../lib";
use lib "$FindBin::Bin/../../local_lib";
use Carp qw (cluck confess croak);

use Greenphyl;
use Greenphyl::Species;
use Greenphyl::Genome;
use Greenphyl::Tools::Sequences;
use Greenphyl::Log('nolog' => 1,);

use Getopt::Long;
use Pod::Usage;

++$|;



# Script global constants
##########################

=pod

=head1 CONSTANTS

B<$DEBUG>: (boolean)
When set to true, it enables debug mode.

=cut

our $DEBUG = 0;




# Script global variables
##########################

=pod

=head1 VARIABLES

B<$g_dbh>: (DBI::db)

Global database handler.

=cut

my $g_dbh;




# Script global functions
##########################

=pod

=head1 FUNCTIONS

=head2 ProcessLocusHash

B<Description>: Process a hash of array of sequences where keys are locus names.
It updates the sequences table data fields:
-locus: contains the locus name (which may include capital letters);
-splice: contains the splice form index (integer) or NULL if no splice-form;
-splice_priority: splice priority, highest is 0. NULL if no splice-form;

B<ArgsCount>: 1

=over 4

=item $locus_hash: (hash ref) (R)

A hash ref: keys are lower-case locus names and values are arrays of splice-form
data. A splice-form data is a hash ref with 3 keys:
-'splice_form': the splice-form index (integer);
-'locus': the original sequence locus name (may have capital letters);
-'sequence': the Greenphyl::Sequence object.

=back

B<Return>: nothing

=cut

sub ProcessLocusHash
{
    my ($locus_hash) = @_;

    # parameters check
    if (1 != @_)
    {
        confess "usage: ProcessLocusHash(locus_hash_ref);";
    }

    my $sql_query;

    # update sequence splice data
    foreach my $lc_locus (keys(%$locus_hash))
    {
        # check if there are splice forms
        if (1 < @{$locus_hash->{$lc_locus}})
        {
            LogVerboseInfo("Locus '$lc_locus' has " . scalar(@{$locus_hash->{$lc_locus}}) . " splice forms");

            # sort splices
            my @sorted_splices = sort
                {
                    return
                        ($b->{'sequence'}->length <=> $a->{'sequence'}->length) # longest sequence first
                        || (($a->{'splice_form'} || 0) <=> ($b->{'splice_form'} || 0)) # if equal length, lowest splice form first
                }
                @{$locus_hash->{$lc_locus}}
            ;
            
            my $filtered_sequences = 0;
            # update sequences
            my $splice_priority = 0;
            foreach my $splice (@sorted_splices)
            {
                # filter status
                $filtered_sequences += ($splice->{'sequence'}->filtered ? 1:0);
                # check for TINYINT limits
                if ($splice_priority > 255)
                {
                    LogWarning("WARNING: More splice forms than database column can support! Truncating splice indices!");
                    $splice_priority = 255;
                    $splice->{'splice_form'} = 255;
                }
                $sql_query = "
                    UPDATE sequences
                    SET locus = ?,
                        splice = ?,
                        splice_priority = ?
                    WHERE id = ?;";
                $g_dbh->do(
                    $sql_query,
                    undef,
                    $splice->{'locus'},
                    $splice->{'splice_form'},
                    $splice_priority,
                    $splice->{'sequence'}->id
                ) or confess $g_dbh->errstr;
                
                # update object
                $splice->{'sequence'}->locus($splice->{'locus'});
                $splice->{'sequence'}->splice($splice->{'splice_form'});
                $splice->{'sequence'}->splice_priority($splice_priority);

                $splice_priority++;
            }

            # warn if there is a filtration issue
            if ($filtered_sequences && ($filtered_sequences != scalar(@sorted_splices)))
            {
                LogWarning("WARNING: Locus: '$lc_locus', mixed filtered and non-filtered splice-forms! You should have a look to your sequences.");
            }
        }
        else
        {
            LogVerboseInfo("Locus '$lc_locus' doesn't have splice forms");
            # no more than 1 sequence: no splice
            $sql_query = "
                UPDATE sequences
                SET locus = ?,
                    splice = NULL,
                    splice_priority = NULL
                WHERE id = ?;";
            $g_dbh->do($sql_query, undef, $locus_hash->{$lc_locus}->[0]->{'locus'}, $locus_hash->{$lc_locus}->[0]->{'sequence'}->id)
                or confess $g_dbh->errstr;
        }
    }

}


=head2 ProcessGenomeSequences

B<Description>: Process sequences of a given genome.

B<ArgsCount>: 1

=over 4

=item $genome: (Greenphyl::Genome) (R)

Species object.

=back

B<Return>: nothing

=cut

sub ProcessGenomeSequences
{
    my ($genome) = @_;

    # parameters check
    if (1 != @_)
    {
        confess "usage: ProcessGenomeSequences(genome);";
    }

    # sort sequences by locus
    my %locus_hash;
    # Work by genomes.
    foreach my $sequence (@{$genome->sequences()})
    {
        my ($locus, $splice_form) = ExtractLocusAndSpliceForm($sequence->accession, $sequence->species->code);
        my $lc_locus = lc($locus); # consistency: remove capital letter for some genes like Arath...
    
        $locus_hash{$lc_locus} ||= [];
        push(
            @{$locus_hash{$lc_locus}},
            {
                'splice_form' => $splice_form,
                'locus'       => $locus,
                'sequence'    => $sequence,
            }
        );
    }

    # update sequence splice data
    ProcessLocusHash(\%locus_hash);

}




# Script options
#################

=pod

=head1 OPTIONS

update_sequence_splice.pl [-help|-man] [-debug]

=head2 Parameters

=over 4

=item B<-help>:

Prints a brief help message and exits.

=item B<-man>:

Prints the manual page and exits.

=item B<-debug> (integer):

Executes the script in debug mode.

=back

=cut


# CODE START
#############

# options processing
my ($man, $help, $db_index) = (0, 0, 0);
# parse options and print usage if there is a syntax error.
GetOptions("help|?"        => \$help,
           "man"           => \$man,
           "debug:i"       => \$DEBUG,
           @Greenphyl::Log::LOG_GETOPT, # skip log parameters
) or pod2usage(2);
if ($help) {pod2usage(0);}
if ($man) {pod2usage(-verbose => 2);}


eval
{

    $g_dbh = GetDatabaseHandler();

    # start SQL transaction
    if ($g_dbh->{'AutoCommit'})
    {
        $g_dbh->begin_work() or croak $g_dbh->errstr;
    }

    PrintDebug("Running in DEBUG mode, no database commit will be issued!");

    # clear related caches
    my $sql_query = "TRUNCATE sequence_count_by_families_species_cache;";
    $g_dbh->do($sql_query);

    # Loop on all genomes.
    my @genome_list = new Greenphyl::Genome($g_dbh, {'selectors' => { 'representative' => 1, }, 'sql' => {'ORDER BY' => 'id'}});
    foreach my $genome (@genome_list)
    {
        LogInfo("Working on genome " . $genome->accession . "...");
        ProcessGenomeSequences($genome);

        LogInfo("Update cache.");
        # update species counts
        $sql_query = qq{
            INSERT INTO sequence_count_by_families_species_cache
            (family_id, species_id, sequence_count, sequence_count_without_splice) 
            SELECT 
                f.id, s.species_id, COUNT(s.id), COUNT(DISTINCT s.locus) 
            FROM families f 
                JOIN families_sequences fs ON (f.id = fs.family_id)
                JOIN sequences s ON (s.id = fs.sequence_id)
            WHERE 
                s.genome_id = ?
            GROUP BY f.id, s.species_id
        };
        $g_dbh->do($sql_query, undef, $genome->id);
        LogInfo("...done with genome " . $genome->accession . ".");
    }

    if ($DEBUG)
    {
        # rollback
        print "DEBUG: ROLLBACK!\n";
        $g_dbh->rollback() or confess($g_dbh->errstr);
    }
    else
    {
        # commit SQL transaction
        LogInfo("Done.");
        $g_dbh->commit() or confess($g_dbh->errstr);
    }
};

HandleErrors();

exit(0);

# CODE END
###########


=pod

=head1 AUTHORS

Mathieu ROUARD (Bioversity), m.rouard@cgiar.org

Valentin GUIGNON (Bioversity), v.guignon@cgiar.org

Matthieu CONTE (Syngenta)

Sebastien BRIOIS (Syngenta)

=head1 VERSION

Version 1.0.0

Date 24/09/2012

=head1 SEE ALSO

GreenPhyl documentation.

=cut
