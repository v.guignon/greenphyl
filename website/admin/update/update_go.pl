#!/usr/bin/perl

=pod

=head1 NAME

update_go.pl - update GO data

=head1 SYNOPSIS

    update_go.pl

=head1 REQUIRES

Perl5, GreenPhyl API v3

=head1 DESCRIPTION

This script updates GO data in the following tables:
-go
-go_alternates
-go_synonyms
-go_relationships
-go_lineage
-go_ipr
-go_uniprot
-ec_go (clear)
-families_go_cache (clear)
-go_sequences_cache (clear)

=cut

use strict;
use warnings;

use FindBin;
use lib "$FindBin::Bin/../../lib";
use lib "$FindBin::Bin/../../local_lib";
use Carp qw (cluck confess croak);

use Getopt::Long;
use Pod::Usage;

use Greenphyl;
use Greenphyl::Log('nolog' => 1,);
use Greenphyl::GO;
use Greenphyl::IPR;
use Greenphyl::DBXRef;
use Greenphyl::Tools::GO;

++$|; #no buffering




# Script global constants
##########################

=pod

=head1 CONSTANTS

B<$REV_STRING>: (string)

Current revision (auto-set by GIT).

B<$DEBUG>: (boolean)

When set to true, it enables debug mode.

=cut

# SVN management part
my $REV_STRING = '$id$';

our $DEBUG = 0;

our $DATA_SOURCE =
    {
        'IPR2GO' =>
            {
                'path'         => GP('IPR_TO_GO_FILE_PATH'),
                'url'          => GetURL('ipr_to_gene_ontology'),
                'format'       => 'go_xref',
                'type'         => 'Greenphyl::IPR',
                'table'        => 'go_ipr',
                'id'           => 'ipr_id',
                'version_var'  => 'ipr2go_version',
                'version_func' => \&GetIPR2GOVersion,
                'load_objects' => \&LoadGreenPhylXRefObjects,
                'parse_func'   => \&ProcessXRefFile,
                'store_func'   => \&StoreCrossRef,
            },
        'UniProt2GO' =>
            {
                'path'         => GP('UNIPROT_TO_GO_FILE_PATH'),
                'url'          => GetURL('uniprot_to_go'),
                'format'       => 'go_assoc',
                'type'         => 'Greenphyl::DBXRef',
                'table'        => 'go_uniprot',
                'id'           => 'dbxref_id',
                'version_var'  => 'uniprot2go_version',
                'version_func' => \&GetUniProt2GOVersion,
                'load_objects' => \&LoadGreenPhylXRefObjects,
                'parse_func'   => \&ProcessUniprotXRefFile,
                'store_func'   => \&StoreUniProtCrossRef,
            },
    };
our $SPLIT_SIZE = 134217728; # 128Mb


# Script global variables
##########################

=pod

=head1 VARIABLES

B<$g_dbh>: (DBI::db)

Global database handler.

B<$g_no_prompt>: (boolean)

Disables prompting when set to a true value.

B<$g_need_commit>: (boolean)

Set to true when a the database has been changed by a query inside this script
inside an uncommitted transaction.

=cut

my $g_dbh         = GetDatabaseHandler();
my $g_no_prompt   = undef;
my $g_need_commit = 0;
my %g_go_linage;



# Script global functions
##########################

=pod

=head1 FUNCTIONS

=head2 LoadGOTerm

B<Description>: Insert a new GO term in database if missing and returns it.

B<ArgsCount>: 2

=over 4

=item $go_term: (GO::Model::Term or string) (R)

the GO term or GO code.

=item $go_graph: (GO::Model::Graph) (R)

the GO graph.

=back

B<Return>: (Greenphyl::GO)

The correspondig existing or inserted GreenPhyl GO object.

=cut

sub LoadGOTerm
{
    my ($go_term, $go_graph) = @_;

    if (!$go_term)
    {
        confess LogError("Failed to load GO term: no GO term provided!");
    }

    # check if a GO code has been provided
    if (!ref($go_term))
    {
        LogDebug("Loading GO object for GO term '$go_term'");
        $go_term = $go_graph->get_term($go_term);
    }
    
    if (!$go_term)
    {
        confess LogError("Failed to load GO term: GO term not found!");
    }

    # store GO term if missing
    LogVerboseInfo("Trying to add GO " . $go_term->acc);
    my $sql_query = 'INSERT IGNORE INTO go(id, code, namespace, name) VALUES(DEFAULT, ?, ?, ?);';
    LogDebug("SQL QUERY: $sql_query\nBindings: " . join(', ', $go_term->acc, $go_term->namespace, $go_term->name));
    $g_dbh->do($sql_query, undef, $go_term->acc, $go_term->namespace, $go_term->name)
        or confess LogError('Failed to insert GO ' . $go_term->acc . ":\n" . $g_dbh->errstr);

    # get GO
    my $go = Greenphyl::GO->new($g_dbh, {'selectors' => {'code' => $go_term->acc}});
    if ($go)
    {
        # store GO alternatives
        foreach my $alternate (@{$go_term->alt_id_list})
        {
            $sql_query = 'INSERT IGNORE INTO go_alternates(go_id, alternate_code) VALUES(?, ?);';
            LogDebug("SQL QUERY: $sql_query\nBindings: " . join(', ', $go->id, $alternate));
            $g_dbh->do($sql_query, undef, $go->id, $alternate)
                or confess LogError('Failed to insert GO alternate ' . $alternate . " for GO $go:\n" . $g_dbh->errstr);
        }

        # store GO synonyms
        foreach my $synonym (@{$go_term->synonym_list})
        {
            $sql_query = 'INSERT IGNORE INTO go_synonyms(go_id, synonym) VALUES(?, ?);';
            LogDebug("SQL QUERY: $sql_query\nBindings: " . join(', ', $go->id, $synonym));
            $g_dbh->do($sql_query, undef, $go->id, $synonym)
                or confess LogError('Failed to insert GO synonym ' . $synonym . " for GO $go:\n" . $g_dbh->errstr);
        }
        # store GO relationships
        foreach my $parent_relationship (@{$go_graph->get_parent_relationships($go_term->acc)})
        {
            my $parent_go_term = $go_graph->get_term($parent_relationship->object_acc);
            if ($parent_go_term)
            {
                # check if parent is already in database
                my $parent_go = Greenphyl::GO->new($g_dbh, {'selectors' => {'code' => $parent_go_term->acc}});
                # do not insert term again if already
                $parent_go ||= LoadGOTerm($parent_go_term, $go_graph);
                # store relationship info
                $sql_query = 'INSERT IGNORE INTO go_relationships(subject_go_id, object_go_id, relationship) VALUES(?, ?, ?);';
                LogDebug("SQL QUERY: $sql_query\nBindings: " . join(', ', $go->id, $parent_go->id, $parent_relationship->type));
                $g_dbh->do($sql_query, undef, $go->id, $parent_go->id, $parent_relationship->type)
                    or confess LogError("Failed to insert GO relationship '" . $parent_relationship->type . "' between GO $go and $parent_go:\n" . $g_dbh->errstr);
            }
            else
            {
                confess LogError('Failed to insert parent term ' . $parent_relationship->object_acc . ': parent not present in graph!');
            }
        }
    }
    else
    {
        LogWarning("GO " . $go_term->acc . " not found in database! Insertion failed!");
    }

    return $go;
}


=pod

=head2 GetGOOBOVersion

B<Description>: Returns GO OBO file version.

B<ArgsCount>: 1

=over 4

=item $file_path: (string)

Path to GO OBO file.

=back

B<Return>: (string)

The version.

=cut

sub GetGOOBOVersion
{
    my ($file_path) = @_;
    my ($fh, $version);
    if (open($fh, $file_path))
    {
        # GO OBO format: "format-version: 1.2"
        ($version) = (<$fh> =~ m/format-version:\s*(\S+)/i);
        close($fh);
    }
    return $version;
}

=pod

=head2 GetIPR2GOVersion

B<Description>: Returns IPR2GO file version.

B<ArgsCount>: 1

=over 4

=item $file_path: (string)

Path to IPR2GO file.

=back

B<Return>: (string)

The version.

=cut

sub GetIPR2GOVersion
{
    my ($file_path) = @_;
    my ($fh, $version);
    if (open($fh, $file_path))
    {
        # IPR format: "!version date: 2011/03/05 11:49:19"
        ($version) = (<$fh> =~ m/!\s*version\s*date:\s*(\S+\s*\S+)/i);
        close($fh);
    }
    return $version;
}


=pod

=head2 GetUniProt2GOVersion

B<Description>: Returns UniProt2GO file version.

B<ArgsCount>: 1

=over 4

=item $file_path: (string)

Path to UniProt2GO file.

=back

B<Return>: (string)

The version.

=cut

sub GetUniProt2GOVersion
{
    my ($file_path) = @_;
    my ($fh, $version);
    if (open($fh, $file_path))
    {
        # UniProt format: "!gaf-version: 2.0"
        ($version) = (<$fh> =~ m/!\s*gaf-version:\s*(\S+)/i);
        close($fh);
    }
    return $version;
}


=pod

=head2 StoreCrossRef

B<Description>: Store cross-reference in database if needed.

B<ArgsCount>: 5

=over 4

=item $go_graph: (GO::Model::Graph) (R)

GO graph object.

=item $go_term: (GO::Model::Term) (R)

GO term object.

=item $xref_go_term: (GO::Model::Term) (R)

GO cross-reference term object.

=item $xref_object_hash: (hash) (R)

Hash of available GreenPhyl objects corresponding to cross-reference objects.

=item $xref_name: (string) (R)

Name of the cross-reference to load. It must be a key of $DATA_SOURCE.

=back

B<Return>: nothing

=cut

sub StoreCrossRef
{
    my ($go_graph, $go_term, $xref_go_term, $xref_object_hash, $xref_name) = @_;
    my ($xref_table, $xref_id_column) = ($DATA_SOURCE->{$xref_name}->{'table'}, $DATA_SOURCE->{$xref_name}->{'id'});

    # loop on each cross-ref
    if ($xref_go_term->dbxref_list())
    {
        LogDebug("Looping on " . scalar(@{$xref_go_term->dbxref_list()}) . " cross-reference(s) of " . $xref_go_term->acc);
        foreach my $go_xref (@{$xref_go_term->dbxref_list()})
        {
            LogVerboseInfo("Working on cross-ref " . $go_xref->xref_key);
            if (exists($xref_object_hash->{$go_xref->xref_key}))
            {
                my $xref = $xref_object_hash->{$go_xref->xref_key};
                
                LogDebug("Cross-ref $xref found in database.");
                # store info...
                if (my $go = LoadGOTerm($go_term, $go_graph))
                {
                    # store XRef info
                    my $sql_query = "INSERT INTO $xref_table ($xref_id_column, go_id, evidence_code) VALUES (?, ?, 'IEA')";
                    LogDebug("SQL QUERY: $sql_query\nBindings: " . join(', ', $xref->id, $go->id));
                    $g_dbh->do($sql_query, undef, $xref->id, $go->id)
                        or confess LogError("Failed to insert GO cross-ref relationship ($xref_table) for XRef '$xref' and GO '$go':\n" . $g_dbh->errstr);
                }
            }
            else
            {
                LogDebug("Cross-ref " . $go_xref->xref_key . " not in database. Skipping.");
            }

        }
    }
    else
    {
        LogWarning("No cross-references found for " . $xref_go_term->acc);
    }
    
    return;
}


=pod

=head2 StoreUniProtCrossRef

B<Description>: Store UniProt cross-reference in database if needed.

B<ArgsCount>: 5

=over 4

=item $go_graph: (GO::Model::Graph) (R)

GO graph object.

=item $go_term: (GO::Model::Term) (R)

GO term object.

=item $go_xref: (GO::Model::Xref) (R)

GO cross-reference object.

=item $xref_object_hash: (hash) (R)

Hash of available GreenPhyl objects corresponding to cross-reference objects.

=item $xref_name: (string) (R)

Name of the cross-reference to load. It must be a key of $DATA_SOURCE.

=back

B<Return>: nothing

=cut

sub StoreUniProtCrossRef
{
    my ($go_graph, $go_term, $xref_go_term, $xref_object_hash, $xref_name) = @_;
    my ($xref_table, $xref_id_column) = ($DATA_SOURCE->{$xref_name}->{'table'}, $DATA_SOURCE->{$xref_name}->{'id'});

    if ($xref_go_term->association_list())
    {
        LogDebug("Looping on " . scalar(@{$xref_go_term->association_list()}) . " association(s) of " . $xref_go_term->acc);
        foreach my $association (@{$xref_go_term->association_list})
        {
            # check if association is from SwissProt
            LogDebug("Working on association...");
            foreach my $evidence (@{$association->evidence_list})
            {
                LogDebug("Working on evidence " . $evidence->code . "...");
                foreach my $evidence_xref (@{$evidence->xref_list})
                {
                    LogVerboseInfo("Working on association " . $evidence_xref->xref_dbname . "...");
                    if ($evidence_xref->xref_dbname =~ m/^UniProtKB$/i)
                    {
                        LogDebug("Got UniProt evidence.");
                        if (exists($xref_object_hash->{$evidence_xref->xref_key}))
                        {
                            my $xref = $xref_object_hash->{$evidence_xref->xref_key};
                            LogDebug("Got corresponding GreenPhyl object $xref.");
                            # store info...
                            if (my $go = LoadGOTerm($go_term, $go_graph))
                            {
                                # store XRef info
                                my $sql_query = "INSERT IGNORE INTO $xref_table ($xref_id_column, go_id, evidence_code) VALUES (?, ?, ?)";
                                LogDebug("SQL QUERY: $sql_query\nBindings: " . join(', ', $xref->id, $go->id, $evidence->code));
                                $g_dbh->do($sql_query, undef, $xref->id, $go->id, $evidence->code)
                                    or confess LogError("Failed to insert GO cross-ref relationship ($xref_table) for XRef '$xref' and GO '$go':\n" . $g_dbh->errstr);
                            }
                            else
                            {
                                LogWarning("GO object not found for GO '" . $go_term->acc . "'!");
                            }
                        }
                        else
                        {
                            LogDebug("Corresponding GreenPhyl object for '" . $evidence_xref->xref_key . "' not found in database. Skipping.");
                        }
                    }
                    else
                    {
                        LogDebug("Not UniProt (" . $evidence_xref->xref_dbname . "). Skipping.");
                    }
                }
                LogDebug("...done with evidence.");
            }
            LogDebug("...done with association.");
        }
    }
    else
    {
        LogWarning("No association found for " . $xref_go_term->acc);
    }

    return;
}


=pod

=head2 ProcessXRefFile

B<Description>: Process a cross-reference GO file and store data into database.
It can process large files by splitting them into parts.
This function relies on GO::Parser.

B<ArgsCount>: 4

=over 4

=item $go_graph: (GO::Model::Graph) (R)

GO graph object.

=item $xref_file_path: (string) (R)

Path to GO cross-ref file.

=item $xref_object_hash: (hash ref) (R)

Hash containing data source identifiers as key and corresponding GreenPhyl
objects as values.

=item $xref_name: (string) (R)

Name of the cross-reference to load. It must be a key of $DATA_SOURCE.

=back

B<Return>: nothing

=cut

sub ProcessXRefFile
{
    my ($go_graph, $xref_file_path, $xref_object_hash, $xref_name) = @_;
    
    my ($xref_table, $xref_id_column) = ($DATA_SOURCE->{$xref_name}->{'table'}, $DATA_SOURCE->{$xref_name}->{'id'});
    LogVerboseInfo("Starting to store cross-ref data for $xref_name...");
    LogDebug("File: $xref_file_path\nLoaded GreenPhyl objects: " . scalar(keys(%$xref_object_hash)) . "\nTable (id): $xref_table ($xref_id_column)\n");

    # get version
    my $version = $DATA_SOURCE->{$xref_name}->{'version_func'}->($xref_file_path) || 'unknown';
    LogInfo("$xref_name version: $version");
    my $sql_query = "INSERT INTO variables (name, value) VALUES ('$DATA_SOURCE->{$xref_name}->{'version_var'}', ?) ON DUPLICATE KEY UPDATE value = ?;";
    LogDebug("SQL QUERY: $sql_query\nBindings: " . join(', ', $version, $version));
    if (!$g_dbh->do($sql_query, undef, $version, $version))
    {
        LogWarning("Failed to store $xref_name version ($version)!");
    }

    # check if file is large
    my $xref_file_size = -s $xref_file_path;
    if (($SPLIT_SIZE < $xref_file_size)
        && ($DATA_SOURCE->{$xref_name}->{'format'} =~ m/obo_text|go_ont|go_xref|go_assoc/))
    {
        LogInfo("Cross-reference OBO file is large ($xref_file_size > $SPLIT_SIZE bytes) and will be split in sub-parts.");
        # split too large files first and process each part separately...
        my $total_read_size = 0;
        my $part_size = 0;
        my $obo_header = '';
        my $current_content_part = '';
        my $xref_fh;
        if (open($xref_fh, $xref_file_path))
        {
            # extract original file name
            my ($obo_file_name) = ($xref_file_path =~ m/([^\/]+)$/);

            my $line;
            # get file header
            while (defined($line = <$xref_fh>) && ($line =~ m/^!.*/))
            {
                $obo_header .= $line;
            }
            $total_read_size = length($obo_header);

            # loop on content lines
            while (defined($line))
            {
                # extract a part of the file (which should be a little bit more than $SPLIT_SIZE)
                $part_size = 0;
                $current_content_part = $line;
                while (defined($line = <$xref_fh>) && ($part_size < $SPLIT_SIZE))
                {
                    $current_content_part .= $line;
                    $part_size += length($line);
                }
                
                $total_read_size += length($current_content_part);
                # add header
                $current_content_part = $obo_header . $current_content_part;
                my $temp_file_path = WriteTemporaryFile($current_content_part, $obo_file_name);

                # process part
                StoreXRefToGO($go_graph, $temp_file_path, $xref_object_hash, $xref_name);

                # remove temporary file
                unlink($temp_file_path);

                # progression
                print GetProgress({'label' => 'cross-reference:', 'current' => $total_read_size, 'total' => $xref_file_size,}, 80, 5);
            }
            # check everything has been processed
            if ($total_read_size != $xref_file_size)
            {
                LogWarning("Processed data size ($total_read_size) does not correspond to original file size ($xref_file_size)!");
            }
            close($xref_fh);
        }
    }
    else
    {
        # no so big or couldn't be split: process file directly
        return StoreXRefToGO($go_graph, $xref_file_path, $xref_object_hash, $xref_name);
    }
}


=pod

=head2 ProcessUniprotXRefFile

B<Description>: Process a UniProt to GO file and store data into database.
As this kind of file is very large (more than 20Gb), parsing is done with regexp
rather than with GO::Parser (wich would take ages).

B<ArgsCount>: 4

=over 4

=item $go_graph: (GO::Model::Graph) (R)

GO graph object.

=item $xref_file_path: (string) (R)

Path to GO cross-ref file.

=item $xref_object_hash: (hash ref) (R)

Hash containing data source identifiers as key and corresponding GreenPhyl
objects as values.

=item $xref_name: (string) (R)

Name of the cross-reference to load. It must be a key of $DATA_SOURCE.

=back

B<Return>: nothing

=head3 UniProt to GO file line format:

1.  DB
Database from which annotated entry has been taken.
For the UniProtKB and UniProtKB Complete Proteomes gene associaton files: UniProtKB
For the PDB association file:  PDB
Example: UniProtKB

2.  DB_Object_ID
A unique identifier in the database for the item being annotated.
Here: an accession number or identifier of the annotated protein
(or PDB entry for the gene_association.goa_pdb file)
For the UniProtKB and UniProtKB Complete Proteomes gene association files: a UniProtKB Accession.
Examples O00165

3.  DB_Object_Symbol
A (unique and valid) symbol (gene name) that corresponds to the DB_Object_ID.
An officially approved gene symbol will be used in this field when available.
Alternatively, other gene symbols or locus names are applied.
If no symbols are available, the identifier applied in column 2 will be used.
Examples: G6PC
CYB561
MGCQ309F3

4.  Qualifier
This column is used for flags that modify the interpretation of an
annotation.
If not null, then values in this field can equal: NOT, colocalizes_with, contributes_to,
NOT | contributes_to, NOT | colocalizes_with
Example: NOT

5.  GO ID
The GO identifier for the term attributed to the DB_Object_ID.
Example: GO:0005634

6.  DB:Reference
A single reference cited to support an annotation.
Where an annotation cannot reference a paper, this field will contain
a GO_REF identifier. See section 8 and
http://www.geneontology.org/doc/GO.references
for an explanation of the reference types used.
Examples: PMID:9058808
DOI:10.1046/j.1469-8137.2001.00150.x
GO_REF:0000002
GO_REF:0000020
GO_REF:0000004
GO_REF:0000003
GO_REF:0000019
GO_REF:0000023
GO_REF:0000024
GO_REF:0000033

7.  Evidence
One of either EXP, IMP, IC, IGI, IPI, ISS, IDA, IEP, IEA, TAS, NAS,
NR, ND or RCA.
Example: TAS

8.  With
An additional identifier to support annotations using certain
evidence codes (including IEA, IPI, IGI, IMP, IC and ISS evidences).
Examples: UniProtKB:O00341
InterPro:IPROO1878
RGD:123456
CHEBI:12345
Ensembl:ENSG00000136141
GO:0000001
EC:3.1.22.1

9.  Aspect
One of the three ontologies, corresponding to the GO identifier applied.
P (biological process), F (molecular function) or C (cellular component).
Example: P

10. DB_Object_Name
Name of protein
The full UniProt protein name will be present here,
if available from UniProtKB. If a name cannot be added, this field
will be left empty.
Examples: Glucose-6-phosphatase
Cellular tumor antigen p53
Coatomer subunit beta

11. Synonym
Gene_symbol [or other text]
Alternative gene symbol(s), IPI identifier(s) and UniProtKB/Swiss-Prot identifiers are
provided pipe-separated, if available from UniProtKB. If none of these identifiers
have been supplied, the field will be left empty.
Example:  RNF20|BRE1A|IPI00690596|BRE1A_BOVIN
IPI00706050
MMP-16|IPI00689864

12. DB_Object_Type
What kind of entity is being annotated.
Here: protein (or protein_structure for the
gene_association.goa_pdb file).
Example: protein

13. Taxon_ID
Identifier for the species being annotated.
Example: taxon:9606

14. Date
The date of last annotation update in the format 'YYYYMMDD'
Example: 20050101

15. Assigned_By
Attribute describing the source of the annotation.  One of
either UniProtKB, AgBase, BHF-UCL, CGD, DictyBase, EcoCyc, EcoWiki, Ensembl,
FlyBase, GDB, GeneDB_Spombe,GeneDB_Pfal, GOC, GR (Gramene), HGNC, Human Protein Atlas,
JCVI, IntAct, InterPro, LIFEdb, PAMGO_GAT, MGI, Reactome, RGD,
Roslin Institute, SGD, TAIR, TIGR, ZFIN, PINC (Proteome Inc.) or WormBase.
Example: UniProtKB

16. Annotation_Extension
Contains cross references to other ontologies/databases that can be used to qualify or
enhance the GO term applied in the annotation.
The cross-reference is prefaced by an appropriate GO relationship; references to multiple ontologies
can be entered.
Example: part_of(CL:0000084)
occurs_in(GO:0009536)
has_input(CHEBI:15422)
has_output(CHEBI:16761)
has_participant(UniProtKB:Q08722)
part_of(CL:0000017)|part_of(MA:0000415)

17. Gene_Product_Form_ID
The unique identifier of a specific spliceform of the protein described in column 2 (DB_Object_ID)
Example:O43526-2

=cut

sub ProcessUniprotXRefFile
{
    my ($go_graph, $xref_file_path, $xref_object_hash, $xref_name) = @_;
    
    my ($xref_table, $xref_id_column) = ($DATA_SOURCE->{$xref_name}->{'table'}, $DATA_SOURCE->{$xref_name}->{'id'});
    LogVerboseInfo("Starting to store cross-ref data for $xref_name...");
    LogDebug("File: $xref_file_path\nLoaded GreenPhyl objects: " . scalar(keys(%$xref_object_hash)) . "\nTable (id): $xref_table ($xref_id_column)\n");

    # get version
    my $version = $DATA_SOURCE->{$xref_name}->{'version_func'}->($xref_file_path) || 'unknown';
    LogInfo("$xref_name version: $version");
    my $sql_query = "INSERT INTO variables (name, value) VALUES ('$DATA_SOURCE->{$xref_name}->{'version_var'}', ?) ON DUPLICATE KEY UPDATE value = ?;";
    LogDebug("SQL QUERY: $sql_query\nBindings: " . join(', ', $version, $version));
    if (!$g_dbh->do($sql_query, undef, $version, $version))
    {
        LogWarning("Failed to store $xref_name version ($version)!");
    }

    
    # we don't use ProcessXRefFile here because GO::Parser is too slow to parse
    # a >22Gb file.
    my $progression = 0;
    my $uniprot_file_size = -s $xref_file_path;
    my $uniprot2go_fh;
    if (open($uniprot2go_fh, $xref_file_path))
    {
UNIPROT_TO_GO_LOOP:
        while (my $line = <$uniprot2go_fh>)
        {
            $progression += length($line);
            print GetProgress({'label' => 'UniProt:', 'current' => $progression, 'total' => $uniprot_file_size,}, 80, 5);
            # skip comments
            if ($line =~ m/^!/)
            {
                next UNIPROT_TO_GO_LOOP;
            }

            # parse line cf. http://www.geneontology.org/GO.format.gaf-2_0.shtml
            if ($line =~ m/
                    \t(GO:\d{7}) # GO ID
                    \t[^\t]+     # DB:Reference
                    \t(\w{1,4})  # Evidence
                    \t([^\t]+)\t # With
                /x)
            {
                my $go_code       = $1;
                my $evidence_code = $2;
                my $assigned_by   = $3;
                # nb.: in some cases, several assignations are concatenated using pipes
                foreach my $assignation_source (split(/\|/, $assigned_by))
                {
                    # only take in account "UniProtKB" sources and extract the
                    # UniProt accession without taking in account accession
                    # variants (ie. "P36873-2").
                    if ($assignation_source =~ m/^UniProtKB:(\w{6})((?:-\d)?)/i)
                    {
                        # get accession
                        my $uniprot_accession = $1;
                        my $uniprot_version = $2;
                        LogDebug("UniProt: $uniprot_accession --> $go_code");
                        # check if Uniprot is in database (w/ or w/o version number)
                        my $dbxref;
                        if ($uniprot_version && exists($xref_object_hash->{$uniprot_accession . $uniprot_version}))
                        {
                            $dbxref = $xref_object_hash->{$uniprot_accession . $uniprot_version};
                        }
                        elsif (exists($xref_object_hash->{$uniprot_accession}))
                        {
                            # not found with version, try without
                            $dbxref = $xref_object_hash->{$uniprot_accession};
                        }

                        if ($dbxref)
                        {
                            # get GO object
                            if (my $go = LoadGOTerm($go_code, $go_graph))
                            {
                                # link UniProt to GO if needed
                                $sql_query = "
                                    INSERT IGNORE INTO " . $DATA_SOURCE->{$xref_name}->{'table'} . " (dbxref_id, go_id, evidence_code)
                                    VALUES (?, ?, ?);
                                ";
                                LogDebug("SQL QUERY: $sql_query\nBindings: " . join(', ', $dbxref->id, $go->id, $evidence_code));
                                if (!$g_dbh->do($sql_query, undef, $dbxref->id, $go->id, $evidence_code))
                                {
                                    confess LogError("Failed to insert a GO to UniProt relationship! UniProt: '$dbxref', GO: '$go', Evidence code: '$evidence_code'\n" . $g_dbh->errstr);
                                }
                            }
                        }
                        else
                        {
                            LogDebug("UniProt accession not found in database: '$uniprot_accession'");
                        }
                    }
                }
            }
            else
            {
                LogDebug("Ignored line: '$line'");
            }
        }

        close($uniprot2go_fh);
    }
    else
    {
        confess LogError("Failed to open file '$xref_file_path'!\n$!")
    }
}


=pod

=head2 StoreXRefToGO

B<Description>: store relevant GO association in database.

B<ArgsCount>: 4

=over 4

=item $go_graph: (GO::Model::Graph) (R)

GO graph object.

=item $xref_file_path: (string) (R)

Path to GO cross-ref file.

=item $xref_object_hash: (hash ref) (R)

Hash containing data source identifiers as key and corresponding GreenPhyl
objects as values.

=item $xref_name: (string) (R)

Name of the cross-reference to load. It must be a key of $DATA_SOURCE.

=back

B<Return>: nothing

=cut

sub StoreXRefToGO
{
    my ($go_graph, $xref_file_path, $xref_object_hash, $xref_name) = @_;
    
    # parse cross-ref data
    my $xref_go_graph = ParseOBOToGraph($DATA_SOURCE->{$xref_name}->{'format'}, $xref_file_path);
    LogDebug("Loadded " . scalar(@{$xref_go_graph->get_all_nodes}) . " corss-ref GO entries.");

    # process each cross-ref entry
    foreach my $xref_go_term (@{$xref_go_graph->get_all_nodes()})
    {
        LogDebug("Working on " . $xref_go_term->acc);
        # compare with GO term from $go_graph
        my $go_term = $go_graph->get_term($xref_go_term->acc);
        if (!$go_term)
        {
            LogWarning("Cross-ref GO term '" . $xref_go_term->acc . "' not found in loaded GO OBO!");
            next;
        }

        $DATA_SOURCE->{$xref_name}->{'store_func'}->($go_graph, $go_term, $xref_go_term, $xref_object_hash, $xref_name);
    }
}


=pod

=head2 LoadGreenPhylXRefObjects

B<Description>: Load cross-reference GreenPhyl objects.

B<ArgsCount>: 1

=over 4

=item $xref_name: (string) (R)

Name of the cross-reference to load. It must be a key of $DATA_SOURCE.

=back

B<Return>: (hash ref)

Hash containing data source identifiers as key and corresponding GreenPhyl
objects as values.

B<Example>:

    ProcessXRef();

=cut

sub LoadGreenPhylXRefObjects
{
    my ($xref_name) = @_;

    # cache all database objects
    LogDebug("Loading corresponding database objects ($DATA_SOURCE->{$xref_name}->{'type'})...");

    my @gp_xref;
    eval('@gp_xref = (' . $DATA_SOURCE->{$xref_name}->{'type'} . '->new($g_dbh));');
    if ($@)
    {
        confess LogError("Failed to load database cross-ref objects!\n$@");
    }
    my %gp_xref = map { ("$_" => $_); } (@gp_xref);
    LogDebug("Loaded dbxref: " . join(', ', keys(%gp_xref)) . "\n");
    LogDebug("...done. Loaded " . scalar(keys(%gp_xref)) . " objects.");

    return \%gp_xref;
}


=pod

=head2 ProcessXRef

B<Description>: Load relevant GO cross-reference data into database.

B<ArgsCount>: 2-3

=over 4

=item $go_graph: (GO::Model::Graph) (R)

GO graph object.

=item $xref_name: (string) (R)

Name of the cross-reference to load. It must be a key of $DATA_SOURCE.

=item $xref_file_path: (string) (O)

Path of the GO cross-reference file. If not provided, corresponding default path
from $DATA_SOURCE hash will be used.

=back

B<Return>: nothing

B<Example>:

    ProcessXRef();

=cut

sub ProcessXRef
{
    my ($go_graph, $xref_name, $xref_file_path) = @_;

    if (Prompt("Load $xref_name relationships? (y/n)", { 'default' => 'y', 'constraint' => '[yYnN]' }, $g_no_prompt) =~ m/y/i)
    {
        LogDebug("Loading $xref_name relationships...");

        # make sure output path exists
        my $xref_directory = $xref_file_path || $DATA_SOURCE->{$xref_name}->{'path'};
        # extract directory part
        $xref_directory =~ s/\/[^\/]+$//;
        if (system("mkdir -p $xref_directory"))
        {
            confess LogError("Failed to create XRef directory ('$xref_directory', error code: $?): $!");
        }

        # prepare file
        $xref_file_path = GetWebFile({
            'file_path' => $xref_file_path,
            'default_file_path' => $DATA_SOURCE->{$xref_name}->{'path'},
            'source_url' => $DATA_SOURCE->{$xref_name}->{'url'},
            'file_description' => $xref_name,
            'no_prompt' => $g_no_prompt,
        });

        
        # clear relation table
        my $sql_query = 'DELETE FROM ' . $DATA_SOURCE->{$xref_name}->{'table'} . ';';
        LogDebug("SQL QUERY: $sql_query");
        if (!$g_dbh->do($sql_query, undef))
        {
            LogWarning("Failed to clear cross-reference table " . $DATA_SOURCE->{$xref_name}->{'table'} . "!\n" . $g_dbh->errstr);
        }

        # get corresponding database objects
        my $gp_xref = $DATA_SOURCE->{$xref_name}->{'load_objects'}->($xref_name);

        my $result = $DATA_SOURCE->{$xref_name}->{'parse_func'}->($go_graph, $xref_file_path, $gp_xref, $xref_name);
        
        # get inserted relations
        $sql_query = 'SELECT COUNT(1) FROM ' . $DATA_SOURCE->{$xref_name}->{'table'} . ';';
        LogDebug("SQL QUERY: $sql_query");
        my ($inserted_count) = ($g_dbh->selectrow_array($sql_query, undef));
        LogInfo("$inserted_count cross-references for $xref_name were inserted.");

        LogDebug("...done loading $xref_name relationships.");        
        return $result;
    }
    else
    {
        return;
    }
}


=pod

=head2 AddOverlieRelationshipsToGO

B<Description>: Adds 'overlies' relationships between GO term. It is not a
standard GO relationship but it is usefull for GreenPhyl database.

B<ArgsCount>: 1-2

=over 4

=item $go: (Greenphyl::GO) (R)

GO object.

=item $parents: (array ref) (U)

Array of Greenphyl::GO parent (overlying) objects.

=item $already_processed_go: (hash ref) (O)

Hash of Greenphyl::GO (keys = GO id) that have been already processed by this
function.

=back

B<Return>: nothing

B<Example>:

    AddOverlieRelationshipsToGO($go);

=cut

sub AddOverlieRelationshipsToGO
{
    my ($go, $parents) = @_;
    $parents ||= [];

    LogVerboseInfo("Adding lineage for GO $go");
    # make sure current GO is not its own parent
    if (grep {$_->id eq $go->id} @$parents)
    {
        LogDebug("Found a loop in GO tree for $go!");
        return;
    }

    # store overlie relationship for current GO
    my $parent_codes = '';
    foreach my $parent (@$parents)
    {
        $g_go_linage{$go->id} ||= {};
        # check if parent has already been added
        if (!$g_go_linage{$go->id}->{$parent->id})
        {
            $g_go_linage{$go->id}->{$parent->id} = scalar(keys(%{$g_go_linage{$go->id}})) + 1;
            $parent_codes .= ',' . $parent->code;
        }
    }

    if ($parent_codes)
    {
        # remove leading coma
        $parent_codes = substr($parent_codes, 1);
        my $sql_query = "
            INSERT INTO go_lineage(go_id, lineage_go_codes)
            VALUE (?, ?)
            ON DUPLICATE KEY UPDATE lineage_go_codes = CONCAT_WS(',', lineage_go_codes, ?);";
        LogDebug("SQL QUERY: $sql_query\nBindings: " . join(', ', $go->id, "'$parent_codes'", "'$parent_codes'"));
        $g_dbh->do($sql_query, undef, $go->id, $parent_codes, $parent_codes)
            or confess LogError("Failed to insert GO lineage for GO $go having " . scalar(@$parents) . " parents ($parent_codes):\n" . $g_dbh->errstr);
#        $g_need_commit = 1;
    }

    # process children
    my @child_parents = ($go, @$parents);
    foreach my $child_go (@{$go->children()})
    {
        AddOverlieRelationshipsToGO($child_go, \@child_parents);
    }

}


=pod

=head2 AddOverlieRelationships

B<Description>: Adds 'overlies' relationships between GO term. It is not a
standard GO relationship but it is usefull for GreenPhyl database.

B<ArgsCount>: 0

B<Return>: nothing

B<Example>:

    AddOverlieRelationships();

=cut

sub AddOverlieRelationships
{

    # remove previous overlie relationships
    my $sql_query = "DELETE FROM go_lineage;";
    LogDebug("SQL QUERY: $sql_query");
    $g_dbh->do($sql_query)
        or confess LogError("Failed to remove GO overlie relationships:\n" . $g_dbh->errstr);
#    $g_need_commit = 1;

    # starts from root GO terms:
    # GO:0003674    molecular_function
    # GO:0008150    biological_process
    # GO:0005575    cellular_component
    my @root_gos = (Greenphyl::GO->new(
        GetDatabaseHandler(),
        {
            # 'selectors' => {'code' => ['IN', 'GO:0003674', 'GO:0008150', 'GO:0005575']},
            'selectors' => {'EXISTS' => 'SELECT TRUE FROM go g2 WHERE go.name = g2.namespace LIMIT 1'},
        },
    ));

    my $start_time = time();
    foreach my $go (@root_gos)
    {
        AddOverlieRelationshipsToGO($go);
    }
    LogDebug("Lineage insertion duration: " . GetDuration(time() - $start_time));
}




# Script options
#################

=pod

=head1 OPTIONS

update_go.pl [-help|-man]

update_go.pl [-debug] [db_index=<db_index>] [-q|-noprompt]
             [-obofile=<OBO_FILE>]
             [-ipr2gofile=<IPR2GO_FILE>]
             [-uniprot2gofile=<UNIPROT2GO_FILE>]
             [-noclear]
             [-noipr]
             [-nouniprot]

=head2 Parameters

=over 4

=item B<-help>:

Prints a brief help message and exits.

=item B<-man>:

Prints the manual page and exits.

=item B<-debug> (integer):

Executes the script in debug mode.

=item B<db_index> (integer):

Index of the database to use (see Greenphyl::Config).
Default: 0

=item B<-noprompt> or B<-q> (flag):

Disable user prompting and always use default values/behavior.
Default: query user

=item B<-obofile> (string):

Path to GO OBO file.
Default: see GreenPhyl config

=item B<-ipr2gofile> (string):

Path to IPR to GO OBO file.
Default: see GreenPhyl config

=item B<-uniprot2gofile> (string):

Path to UniProt to GO OBO file.
Default: see GreenPhyl config

=item B<-noclear> (flag):

Do not clear GO table and existing relationships. This can be usefull when you
want to load IPR relationships first and later UniProt relationships and
vice-versa. The first time, you clear everything and the next time, you don't
and just add new data.
Default: clear

=item B<-noipr> (flag):

Disable IPR relationships loading (usually used in combination with -q).
Default: IPR relationships are loaded.

=item B<-nouniprot> (flag):

Disable UniProt relationships loading (usually used in combination with -q).
Default: UniProt relationships are loaded.

=back

=cut


# CODE START
#############

# for regression tests
if ($ENV{'TESTING'} && $ENV{'TESTING_NO_RUN'})
{return 1;}

# options processing
my ($man, $help, $debug) = (0, 0, undef);
my $obo_file_path;
my $ipr2go_file_path;
my $uniprot2go_file_path;
my ($no_clear_go, $no_ipr, $no_uniprot);
# parse options and print usage if there is a syntax error.
GetOptions('help|?'           => \$help,
           'man'              => \$man,
           'debug:s'          => \$debug,
           'q|noprompt'       => \$g_no_prompt,
           'obofile=s'        => \$obo_file_path, # GO OBO file path
           'ipr2gofile=s'     => \$ipr2go_file_path,
           'uniprot2gofile=s' => \$uniprot2go_file_path,
           'noclear'          => \$no_clear_go,
           'noipr'            => \$no_ipr,
           'nouniprot'        => \$no_uniprot,
           @Greenphyl::Log::LOG_GETOPT, # skip log parameters
) or pod2usage(1);
if ($help) {pod2usage('-verbose' => 1, '-exitval' => 0);}
if ($man) {pod2usage('-verbose' => 2, '-exitval' => 0);}

# change debug mode if requested/forced
if (defined($debug))
{
    # just '-debug' ?
    if ($debug eq '')
    {
        $debug = 1;
    }
    $DEBUG = $debug;
}

eval
{

    LogInfo("Update GO...");
    my $sql_query;

#    # start SQL transaction
#    if ($g_dbh->{'AutoCommit'})
#    {
#        $g_dbh->begin_work() or croak $g_dbh->errstr;
#    }
    # set warning state (not fatal)
    $g_dbh->{'HandleError'} = undef;

    LogDebug("Running in DEBUG mode, no database commit will be issued!");

    # make sure output path exists
    my $obo_directory = GP('GO_OBO_FILE_PATH');
    if (!$obo_directory)
    {
        confess LogError("Missing config parameter GO_OBO_FILE_PATH!");
    }
    # extract directory part
    $obo_directory =~ s/\/[^\/]+$//;
    if (system("mkdir -p $obo_directory"))
    {
        confess LogError("Failed to create OBO directory ('$obo_directory', error code: $?): $!");
    }
    
    $obo_file_path = GetWebFile({
        'file_path'         => $obo_file_path,
        'default_file_path' => GP('GO_OBO_FILE_PATH'),
        'source_url'        => GetURL('gene_ontology_obo'),
        'file_description'  => 'GO OBO file',
        'no_prompt'         => $g_no_prompt,
    });

    if (!$no_clear_go
        && Prompt("Clear GO table (also clears GO relations and cache)? (y/n)", { 'default' => 'y', 'constraint' => '[yYnN]' }, $g_no_prompt) =~ m/y/i)
    {
        # clear go and also related caches at the same time
        $sql_query = "DELETE FROM go;";
        $g_dbh->do($sql_query)
            or confess LogError("Failed to clear GO table:\n" . $g_dbh->errstr);
#        $g_need_commit = 1;
    }

    # make sure we have something to do
    if (!$no_ipr || !$no_uniprot)
    {
        # get version
        my $version = GetGOOBOVersion($obo_file_path) || 'unknown';
        LogInfo("GO OBO version: $version");
        $sql_query = "INSERT INTO variables (name, value) VALUES ('go_version', ?) ON DUPLICATE KEY UPDATE value = ?;";
        LogDebug("SQL QUERY: $sql_query\nBindings: " . join(', ', $version, $version));
        if (!$g_dbh->do($sql_query, undef, $version, $version))
        {
            LogWarning("Failed to store GO OBO version ($version)!" . $g_dbh->errstr);
        }
#        $g_need_commit = 1;

        # load OBO data
        my $go_graph = ParseOBOToGraph('obo_text', $obo_file_path);
        LogDebug("Loadded " . scalar(@{$go_graph->get_all_nodes}) . " GO entries.");

        if (!$no_ipr)
        {
            # process IPR2GO
            ProcessXRef($go_graph, 'IPR2GO', $ipr2go_file_path);
#            if (!$DEBUG)
#            {
#                SaveCurrentTransaction($g_dbh);
#                $g_need_commit = 0;
#            }
        }

        if (!$no_uniprot)
        {
            # process UniProt2GO
            ProcessXRef($go_graph, 'UniProt2GO', $uniprot2go_file_path);
#            if (!$DEBUG)
#            {
#                SaveCurrentTransaction($g_dbh);
#                $g_need_commit = 0;
#            }
        }
    }

    AddOverlieRelationships();

#    if (!$DEBUG && $g_need_commit)
#    {SaveCurrentTransaction($g_dbh);}

    LogInfo("Done.");
};

HandleErrors();

exit(0);

# CODE END
###########


=pod

=head1 AUTHORS

Mathieu ROUARD (Bioversity), m.rouard@cgiar.org

Valentin GUIGNON (Bioversity), v.guignon@cgiar.org

Matthieu CONTE (Syngenta)

Sebastien BRIOIS (Syngenta)

=head1 VERSION

Version 1.0.0

Date 21/02/2013

=head1 SEE ALSO

GreenPhyl documentation.
http://search.cpan.org/~cmungall/go-perl-0.14/
http://search.cpan.org/~cmungall/go-perl-0.14/GO/Parser.pm
http://search.cpan.org/~cmungall/go-perl-0.14/GO/Model/Graph.pm
http://search.cpan.org/~cmungall/go-perl-0.14/GO/Model/Term.pm


=cut
