#!/usr/bin/perl

=pod

=head1 NAME

formatdb_refseq.pl - Run formatdb on RefSeq FASTA to generate BLAST banks

=head1 SYNOPSIS

    formatdb_refseq.pl

=head1 REQUIRES

Perl5, BLAST formatdb

=head1 DESCRIPTION

Run formatdb on RefSeq FASTA to generate BLAST banks.

=cut

use strict;
use warnings;

use FindBin;
use lib "$FindBin::Bin/../../lib";
use lib "$FindBin::Bin/../../local_lib";
use Carp qw (cluck confess croak);

use Greenphyl;
use Greenphyl::Log('nolog' => 1,);

use Greenphyl::Run::Blast;

use Getopt::Long;
use Pod::Usage;




# Script global constants
##########################

=pod

=head1 CONSTANTS

B<$DEBUG>: (boolean)

When set to true, it enables debug mode.

B<$REFSEQ_BANKS>: (array ref)

Array of RefSeq protein bank names.

=cut

our $DEBUG = 0;

our $REFSEQ_BANKS = GP('REFSEQ_BANKS');

my $REFSEQ_FASTA_EXTENTION = '.faa';

our $REFSEQ_BANK_NAME = ' refseq-noplant';



# Script options
#################

=pod

=head1 OPTIONS

formatdb_refseq.pl [-help | -man] [-debug] [-output <OUTPUT_PATH>]

=head2 Parameters

=over 4

=item B<-help>:

Prints a brief help message and exits.

=item B<-man>:

Prints the manual page and exits.

=item B<-debug>:

Executes the script in debug mode.
Default: not in debug mode.

=item B<-input> (string):

Path where the input FASTA file should be found. You don't need to include the
terminal slash.
Default: looks in current directory.

=item B<-output> (string):

Path where the output bank should be put. You don't need to include the terminal
slash.
Default: output to current directory.

=back

=cut


# CODE START
#############

# options processing
my ($man, $help, $debug, $output_directory, $input_directory) = (0, 0, 0, '', '');
# parse options and print usage if there is a syntax error.

GetOptions('help|?'     => \$help,
           'man'        => \$man,
           'debug:s'    => \$debug,
           'i|input=s'  => \$input_directory,
           'o|output=s' => \$output_directory,
           @Greenphyl::Log::LOG_GETOPT, # skip log parameters
) or pod2usage(1);
if ($help) {pod2usage('-verbose' => 1, '-exitval' => 0);}
if ($man) {pod2usage('-verbose' => 2, '-exitval' => 0);}

$DEBUG ||= $debug;

if ($output_directory)
{
    # replace multiple-trailing slash with only one
    $output_directory =~ s/\/+$//;
    $output_directory .= '/';
}
$output_directory ||= '';

if ($input_directory)
{
    # replace multiple-trailing slash with only one
    $input_directory =~ s/\/+$//;
    $input_directory .= '/';
}
$input_directory  ||= '';

#+diamond update # system("module load bioinfo/blast/marmadais") == 0
#+diamond update #     or confess "ERROR: Failed to load formatdb module!\n";

# for each input FASTA file run formatdb
#+diamond update # foreach my $bank_name (@$REFSEQ_BANKS)
#+diamond update # {
#+diamond update #     my $fasta_file = $input_directory . $bank_name . $REFSEQ_FASTA_EXTENTION;
#+diamond update #     Greenphyl::Run::Blast::FormatDB($fasta_file, $output_directory . $bank_name, {'name' => 'RefSeq database without plants', 'gi' => 1, });
#+diamond update # }

# generate alias
# my $db_alias_command = GP('BLAST_PLUS_PATH') . "/blastdb_aliastool -dblist '" . join(' ', map { $output_directory . $_ } @$REFSEQ_BANKS) . "' -dbtype prot -out " . GP('REFSEQ_NOPLANT_BANK') . " -title 'RefSeq Database without plants'";
# Load cluster CD-HIT.
#+diamond update # system("module load bioinfo/ncbi-blast/2.9.0") == 0
#+diamond update #     or confess "ERROR: Failed to load BLAST module!\n";

#+diamond update # my $db_alias_command = "blastdb_aliastool -dblist '" . join(' ', map { $output_directory . $_ } @$REFSEQ_BANKS) . "' -dbtype prot -out " . GP('REFSEQ_NOPLANT_BANK') . " -title 'RefSeq Database without plants'";
#+diamond update # PrintDebug("Generating RefSeq alias using command line:\n$db_alias_command");
#+diamond update # if (system($db_alias_command))
#+diamond update # {
#+diamond update #     confess "ERROR: Failed to generate RefSeq alias database!\n" . ((0 > $?) ? $! . "\n" : '');
#+diamond update # }

# Make a bank for each dataset.
foreach my $bank_name (@$REFSEQ_BANKS)
{
    my $fasta_file = $input_directory . $bank_name . $REFSEQ_FASTA_EXTENTION;
    my $cmd = GP('DIAMOND_COMMAND') . " makedb --in $fasta_file -d $input_directory$bank_name";
    LogDebug("COMMAND: $cmd");
    LogInfo("Running Diamond makedb for $bank_name...");
    if (0 != system($cmd))
    {
        if (not $!)
        {
            cluck LogError("Failed to run Diamond!");
        }
        cluck LogWarning("WARNING: Diamond execution failed to generate RefSeq bank for $bank_name!");
    }

}


# Make a global bank
my $fasta_files = join(' ', map { $input_directory . $_ . $REFSEQ_FASTA_EXTENTION; } @$REFSEQ_BANKS);

print "$fasta_files\n";

my $cmd = "/bin/cat "
    . $fasta_files
    . " | "
    . GP('DIAMOND_COMMAND')
    . " makedb -d "
    . GP('REFSEQ_NOPLANT_BANK');
LogDebug("COMMAND: $cmd");
LogInfo("Running Diamond makedb...");
if (0 != system($cmd))
{
    if (not $!)
    {
        cluck LogError("Failed to run Diamond!");
    }
    cluck LogWarning("WARNING: Diamond execution failed to generate RefSeq no-plant bank!");
}


exit(0);

# CODE END
###########


=pod

=head1 AUTHORS

Mathieu ROUARD (Bioversity), m.rouard@cgiar.org

Valentin GUIGNON (Bioversity), v.guignon@cgiar.org

Matthieu CONTE (Syngenta)

Sebastien BRIOIS (Syngenta)

=head1 VERSION

Version 1.0.0

Date 22/11/2012

=head1 SEE ALSO

GreenPhyl documentation, BLAST, NCBI RefSeq.

=cut
