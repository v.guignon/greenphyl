#!/usr/bin/env perl

=pod

=head1 NAME

update_homepage_tree.pl - Updates homepage tree

=head1 SYNOPSIS

    update_homepage_tree.pl

=head1 REQUIRES

Perl5, NCBI

=head1 DESCRIPTION

Creates or updates homepage tree from taxonomy ids found in species table.
Output file is HTML and will be put in 'templates/cache/homepage_tree.html' by
default.

=cut

use strict;
use warnings;

use FindBin;
use lib "$FindBin::Bin/../../lib";
use lib "$FindBin::Bin/../../local_lib";
use Carp qw (cluck confess croak);

use Greenphyl;
use Greenphyl::Log('nolog' => 1,);
use Greenphyl::Web::Templates;
use Greenphyl::TreeNode;
use Greenphyl::Species;

use IO::String;
use Bio::TreeIO;

use Getopt::Long;
use Pod::Usage;

use LWP::UserAgent;

++$|; #no buffering




# Script global constants
##########################

=pod

=head1 CONSTANTS

B<$REV_STRING>: (string)

Current revision (auto-set by GIT).

B<$DEBUG>: (boolean)

When set to true, it enables debug mode.

B<$TREE_TEMPLATE>: (string)

relative path (from template directory) to tree template toolkit file.

B<$HTML_TREE_FILE_PATH>: (string)

relative path to the cached HTML version of the tree.

B<%RENAME_SETTINGS>: (hash of strings)

Hash associating a species name from NCBI to the corresponding species name in
GreenPhyl species table.

=cut

# SVN management part
my $REV_STRING = '$id$';

our $DEBUG = 1;

our $TREE_TEMPLATE = 'miscelaneous/homepage_tree.tt';
our $TREE_JSON_TEMPLATE = 'miscelaneous/homepage_tree_json.tt';
our $HTML_TREE_FILE_PATH = GP('TEMPLATES_PATH') . "/cache/homepage_tree.html";
our $JSON_TREE_FILE_PATH = GP('TEMPLATES_PATH') . "/cache/homepage_tree.json";

# Define here how you want to rename some leaves
our %RENAME_SETTINGS = (
    'Oryza sativa Japonica Group'   => 'Oryza sativa',
    'Malus x domestica'             => 'Malus domestica',
    'Orobanche'                     => 'Orobanche aegyptiaca',
    'Striga'                        => 'Striga hermonthica',
    'Beta vulgaris subsp. vulgaris' => 'Beta vulgaris',
);




# Script global functions
##########################

=pod

=head1 FUNCTIONS

=head2 GetTaxonomyTreeFromNCBI

B<Description>:

1- Read taxonomy ids from species table
2- Send a GET request to NCBI service that returns a tree in the newick format
3- Read newick file with Bio::TreeIO
4- Returns a Bio::Tree::Tree object


B<ArgsCount>: 0


B<Return>: (string)

A newick tree in string format.

=cut

sub GetTaxonomyTreeFromNCBI
{
    # Prepare GET request to NCBI service
    my $dbh = GetDatabaseHandler();

    my $tax_ids = $dbh->selectcol_arrayref(
        qq{
            SELECT taxonomy_id 
            FROM species 
            WHERE display_order > 0
            ORDER BY id
        }
    );

    my $taxonomy_id_query = '&an=' . join('&an=', @$tax_ids);
    my $base_url = GetURL('ncbi_get_tree', undef, {'append' => $taxonomy_id_query});
    LogDebug("Tree URL: " . $base_url);
    
    # Download NCBI tree
    my $ua = new LWP::UserAgent;
    $ua->timeout(120); # seconds
    $ua->env_proxy();
    my $response = $ua->get($base_url);
    my $newick;
    if ($response->is_success())
    {
        $newick = $response->content;
    }
    else
    {
        confess LogError("Failed to fetch Newick tree from NCBI!\n" . $response->status_line);
    }

    # Rename leaves
    map { $newick =~ s/$_/$RENAME_SETTINGS{$_}/g } keys %RENAME_SETTINGS;

    # load BioPerl tree to validate output and reorder branches by length by default
    my $treeio = Bio::TreeIO->new(
        '-format'   => 'newick', 
        '-fh'       => IO::String->new($newick),
        '-order_by' => Bio::Tree::Node::branch_length,
    );
    
    if (!$treeio)
    {
        confess LogError("Failed to load Newick tree! Newick string:\n$newick");
    }
    
    # update tree
    $newick = $treeio->next_tree->as_text('newick');


    # generate species code tree
    my $species_code_newick = $newick;
    my $organism_codes = $dbh->selectall_arrayref(
        qq{
            SELECT organism, code 
            FROM species 
            WHERE display_order > 0
            ORDER BY id
        },
        { 'Slice' => {} },
    );
    foreach my $organism_code (@$organism_codes)
    {
        $species_code_newick =~ s/$organism_code->{'organism'}/$organism_code->{'code'}/i;
    }
    # update newick taxonomy code tree in database
    if (!$dbh->do("INSERT INTO variables (name, value) VALUES ('taxonomy_code_newick_tree', ?) ON DUPLICATE KEY UPDATE value = ?;", undef, $species_code_newick, $species_code_newick))
    {
        confess LogError("Failed to store taxonomy code newick tree!");
    }
    my $tree_of_life_fh;
    if (open($tree_of_life_fh, ">" . GP('TREE_OF_LIFE')))
    {
        print {$tree_of_life_fh} $species_code_newick;
        close($tree_of_life_fh);
    }
    else
    {
        confess LogError("Failed to generate tree of life (" . GP('TREE_OF_LIFE') . ")!\n$!\n");
    }


    # now load tree as TreeNode to reorder leaves
    my $root_node = Greenphyl::TreeNode->loadNewickTree($newick);

    # get order wished
    my $sql_query = "SELECT organism AS \"organism\", display_order AS \"index\" FROM species WHERE display_order > 0 ORDER BY display_order ASC;";
    my $species_order = $dbh->selectall_hashref($sql_query, 'organism');
    
    # validate database species order...
    my $max_order = 1;
    # start from leaves and go up until all nodes are processed or an error is met
    my @nodes_to_process = $root_node->getLeafNodes();
    # prepare index_range structure
    my $node_range = {};

    foreach my $node_to_process (@nodes_to_process)
    {
        if (!exists($species_order->{$node_to_process->getName()}))
        {
            confess 'Species not fount in database: ' . $node_to_process->getName() . "\n";
        }
        if ($max_order < $species_order->{$node_to_process->getName()}->{'index'})
        {
            $max_order = $species_order->{$node_to_process->getName()}->{'index'};
        }
        $node_range->{$node_to_process} = [
            $species_order->{$node_to_process->getName()}->{'index'},
            $species_order->{$node_to_process->getName()}->{'index'}
        ];
    }

    # process each level of the tree starting from the leaves up to the root
    eval
    {
        TAXA_TREE_ORDERING:
        while (@nodes_to_process)
        {
            my $next_nodes_to_process = {};
            foreach my $node_to_process (@nodes_to_process)
            {
                # get neighbors
                my @children = ();
                foreach my $neighbor_node ($node_to_process->getNeighborNodes())
                {
                    # check if neighbor has been processed
                    if (exists($node_range->{$neighbor_node}))
                    {
                        # child has been processed, store it as a child
                        push(@children, $neighbor_node);
                    }
                    else
                    {
                        # store the unprocessed neighbor node
                        $next_nodes_to_process->{$neighbor_node} = $neighbor_node;
                    }
                }
                if (@children)
                {
                    # sort child nodes
                    my $sort_child_nodes = sub
                    {
                        if ($node_range->{$a}->[0] > $node_range->{$b}->[0])
                        {
                            if ($node_to_process->getNeighborNodeIndex($a) < $node_to_process->getNeighborNodeIndex($b))
                            {
                                $node_to_process->swapSubtrees($a, $b);
                            }
                            return 1;
                        }
                        if ($node_to_process->getNeighborNodeIndex($a) > $node_to_process->getNeighborNodeIndex($b))
                        {
                            $node_to_process->swapSubtrees($a, $b);
                        }
                        return -1;
                    };
                    # order the children
                    @children = sort $sort_child_nodes (@children);
                    # make sure subtrees follow each-other without gaps or overlapping
                    my $previous_child_node = shift(@children);
                    my $lowest_index = $node_range->{$previous_child_node}->[0];
                    while (my $child_node = shift(@children))
                    {
                        if (($node_range->{$previous_child_node}->[1] + 1) != $node_range->{$child_node}->[0])
                        {
                            # sub-trees are not following each other!
                            # confess "Cannot re-order species tree as wanted in database because it is not possible! Please check species table tax_order column and make sure it is possible to build a tree with the specified order.\n";
                            warn "Cannot re-order species tree as wanted in database because it is not possible! Please check species table tax_order column and make sure it is possible to build a tree with the specified order.\n";
                            last TAXA_TREE_ORDERING;
                        }
                        $previous_child_node = $child_node;
                    }
                    my $highest_index = $node_range->{$previous_child_node}->[1];
                    # store range of current node and mark it as processed
                    $node_range->{$node_to_process} = [$lowest_index, $highest_index];
                }
                elsif (!$node_to_process->isLeaf())
                {
                    # confess "ERROR: no children found for an internal node!\n";
                    confess "ERROR: no children found for an internal node!\n";
                }
            }
            # check if some nodes should not be processed yet
            foreach my $unprocessed_node (values(%$next_nodes_to_process))
            {
                my $unprocessed_neighbor_count = 0;
                foreach my $neighbor_node ($unprocessed_node->getNeighborNodes())
                {
                    if (!exists($node_range->{$neighbor_node}))
                    {
                        ++$unprocessed_neighbor_count;
                    }
                }
                if ((1 < $unprocessed_neighbor_count)
                    || ((0 < $unprocessed_neighbor_count) && ($unprocessed_node == $root_node)))
                {
                    # more than 1 neighbor unprocessed node or root node with at least 1 unprocessed child
                    # node not ready to be processed
                    delete($next_nodes_to_process->{$unprocessed_node});
                }
            }
            @nodes_to_process = values(%$next_nodes_to_process);
        }



### ----- +DEBUG -----
#+FIXME: second pass to fix some ordering issue. It works but I don't know why!

        @nodes_to_process = $root_node->getLeafNodes();
        # prepare index_range structure
        $node_range = {};
        foreach my $node_to_process (@nodes_to_process)
        {
            if (!exists($species_order->{$node_to_process->getName()}))
            {
                confess 'Species not fount in database: ' . $node_to_process->getName() . "\n";
            }
            $node_range->{$node_to_process} = [
                $species_order->{$node_to_process->getName()}->{'index'},
                $species_order->{$node_to_process->getName()}->{'index'}
            ];
        }

        # process each level of the tree starting from the leaves up to the root
        if (1 < $max_order)
        {
            TAXA_TREE_ORDERING2:
            while (@nodes_to_process)
            {
                my $next_nodes_to_process = {};
                foreach my $node_to_process (@nodes_to_process)
                {
                    # get neighbors
                    my @children = ();
                    foreach my $neighbor_node ($node_to_process->getNeighborNodes())
                    {
                        # check if neighbor has been processed
                        if (exists($node_range->{$neighbor_node}))
                        {
                            # child has been processed, store it as a child
                            push(@children, $neighbor_node);
                        }
                        else
                        {
                            # store the unprocessed neighbor node
                            $next_nodes_to_process->{$neighbor_node} = $neighbor_node;
                        }
                    }
                    if (@children)
                    {
                        # sort child nodes
                        my $sort_child_nodes = sub
                        {
                            if ($node_range->{$a}->[1] > $node_range->{$b}->[1])
                            {
                                if ($node_to_process->getNeighborNodeIndex($a) < $node_to_process->getNeighborNodeIndex($b))
                                {
                                    $node_to_process->swapSubtrees($a, $b);
                                }
                                return 1;
                            }
                            if ($node_to_process->getNeighborNodeIndex($a) > $node_to_process->getNeighborNodeIndex($b))
                            {
                                $node_to_process->swapSubtrees($a, $b);
                            }
                            return -1;
                        };
                        # order the children
                        @children = sort $sort_child_nodes (@children);
                        # make sure subtrees follow each-other without gaps or overlapping
                        my $previous_child_node = shift(@children);
                        my $lowest_index = $node_range->{$previous_child_node}->[0];
                        while (my $child_node = shift(@children))
                        {
                            if (($node_range->{$previous_child_node}->[1] + 1) != $node_range->{$child_node}->[0])
                            {
                                # sub-trees are not following each other!
                                # confess "Cannot re-order species tree as wanted in database because it is not possible! Please check species table tax_order column and make sure it is possible to build a tree with the specified order.\n";
                                warn "Cannot re-order species tree as wanted in database because it is not possible! Please check species table tax_order column and make sure it is possible to build a tree with the specified order.\n";
                                last TAXA_TREE_ORDERING2;
                            }
                            $previous_child_node = $child_node;
                        }
                        my $highest_index = $node_range->{$previous_child_node}->[1];
                        # store range of current node and mark it as processed
                        $node_range->{$node_to_process} = [$lowest_index, $highest_index];
                    }
                    elsif (!$node_to_process->isLeaf())
                    {
                        # confess "ERROR: no children found for an internal node!\n";
                        confess "ERROR: no children found for an internal node!\n";
                    }
                }
                # check if some nodes should not be processed yet
                foreach my $unprocessed_node (values(%$next_nodes_to_process))
                {
                    my $unprocessed_neighbor_count = 0;
                    foreach my $neighbor_node ($unprocessed_node->getNeighborNodes())
                    {
                        if (!exists($node_range->{$neighbor_node}))
                        {
                            ++$unprocessed_neighbor_count;
                        }
                    }
                    if ((1 < $unprocessed_neighbor_count)
                        || ((0 < $unprocessed_neighbor_count) && ($unprocessed_node == $root_node)))
                    {
                        # more than 1 neighbor unprocessed node or root node with at least 1 unprocessed child
                        # node not ready to be processed
                        delete($next_nodes_to_process->{$unprocessed_node});
                    }
                }
                @nodes_to_process = values(%$next_nodes_to_process);
            }
### ----- +DEBUG -----
        }


    
        # display indices if debug on
        if ($DEBUG)
        {
            foreach my $node_to_process ($root_node->getLeafNodes())
            {
                # $node_to_process->setName(sprintf('#%02i', $species_order->{$node_to_process->getName()}->{'index'}) . ' ' . $node_to_process->getName());
                $node_to_process->setName($node_to_process->getName());
            }
        }
        
        # get ordered newick tree
        $newick = $root_node->getNewickTree();
        print "Ordered Newick tree:\n$newick\n\n";

    };

    # and update ordered newick tree in database
    if (!$dbh->do("INSERT INTO variables (name, value) VALUES ('taxonomy_newick_tree', ?) ON DUPLICATE KEY UPDATE value = ?;", undef, $newick, $newick))
    {
        confess LogError("Failed to store ordered taxonomy newick tree!");
    }
    LogInfo("Newick tree updated in database\n");

    return $newick;
}


=pod

=head2 SaveAsHTML

B<Description>:

Takes a Bio::Tree::Tree object
Process Tree object and convert it into HTML format using TT
Returns nothing - HTML file shall be found in $output

B<ArgsCount>: [count of arguments] #+++

=over 4

=item $variable_name: ([variable nature]) ([requirements]) #+++ see below

#--- requirement can be:
#--- (R)=required,
#--- (O)=optional
#--- (U)=optional and must be undef if omitted
[variable description]. #+++

=item $variable_name2: ([variable nature]) ([requirements]) #+++

[variable description]. #+++

=back

B<Return>: ([return type]) #+++

[return description]. #+++

B<Example>:

    [example code] #+++

=cut

sub SaveAsHTML
{
    my ($newick, $output) = @_;
    
    my $treeio = Bio::TreeIO->new(
        '-format'   => 'newick', 
        '-fh'       => IO::String->new($newick),
        '-order_by' => Bio::Tree::Node::branch_length,
    );
    
    if (!$treeio)
    {
        confess LogError("Failed to load Newick tree! Newick string:\n$newick");
    }

    # update tree
    my $tree = $treeio->next_tree;
    my $species_data = { map {$_->organism => $_} (new Greenphyl::Species(GetDatabaseHandler(), {'selectors' => { 'display_order' => ['>', 0], }, })) };

    my $html_taxonomy_tree = ProcessTemplate($TREE_TEMPLATE, {'bioperl_tree' => $tree, 'species_data' => $species_data,} );

    my $tree_fh;
    open($tree_fh, ">$output")
        or confess LogError("Failed to open '$output' for writting!\n$!");

    print {$tree_fh} $html_taxonomy_tree;

    close($tree_fh); 
    
    # store HTML tree in database
    if (!GetDatabaseHandler()->do("INSERT INTO variables (name, value) VALUES ('taxonomy_html_tree', ?) ON DUPLICATE KEY UPDATE value = ?;", undef, $html_taxonomy_tree, $html_taxonomy_tree))
    {
        LogError("Failed to store taxonomy HTML tree!");
        confess "Failed to store taxonomy HTML tree!";
    }

}


=pod

=head2 SaveAsJSON

B<Description>:

Takes a Bio::Tree::Tree object
Process Tree object and convert it into JSON format using TT
Returns nothing - JSON file shall be found in $output

B<ArgsCount>: [count of arguments] #+++

=over 4

=item $variable_name: ([variable nature]) ([requirements]) #+++ see below

#--- requirement can be:
#--- (R)=required,
#--- (O)=optional
#--- (U)=optional and must be undef if omitted
[variable description]. #+++

=item $variable_name2: ([variable nature]) ([requirements]) #+++

[variable description]. #+++

=back

B<Return>: ([return type]) #+++

[return description]. #+++

B<Example>:

    [example code] #+++

=cut

sub SaveAsJSON
{
    my ($newick, $output) = @_;
    
    my $treeio = Bio::TreeIO->new(
        '-format'   => 'newick', 
        '-fh'       => IO::String->new($newick),
        '-order_by' => Bio::Tree::Node::branch_length,
    );
    
    if (!$treeio)
    {
        confess LogError("Failed to load Newick tree! Newick string:\n$newick");
    }

    # update tree
    my $tree = $treeio->next_tree;
    my $species_data = {
        map
        {$_->organism => $_}
        (new Greenphyl::Species(
            GetDatabaseHandler(),
            {'selectors' => { 'display_order' => ['>', 0], }, }
        ))
    };

    my $json_taxonomy_tree = ProcessTemplate($TREE_JSON_TEMPLATE, {'bioperl_tree' => $tree, 'species_data' => $species_data,} );

    my $tree_fh;
    open($tree_fh, ">$output")
        or confess LogError("Failed to open '$output' for writting!\n$!");

    print {$tree_fh} $json_taxonomy_tree;

    close($tree_fh); 
    
    # store JSON tree in database
    if (!GetDatabaseHandler()->do("INSERT INTO variables (name, value) VALUES ('taxonomy_json_tree', ?) ON DUPLICATE KEY UPDATE value = ?;", undef, $json_taxonomy_tree, $json_taxonomy_tree))
    {
        confess LogError("Failed to store taxonomy JSON tree!");
    }

}




# Script options
#################

=pod

=head1 OPTIONS

#--- describes parameters given to the script
#+++ command line syntax
#--- requirement of the option and its parameter can be:
#--- required: name or nature inside <>
#--- optional: name or nature inside []
#--- alternative between 2 elements: elements separated by a |

=head2 Parameters

=over 4

=item B<-help>:

Prints a brief help message and exits.

=item B<-man>:

Prints the manual page and exits.

=item B<-debug> (integer):

Executes the script in debug mode. The integer value is optional and is used
to set debug level (use 0 to force disable debug mode).
Default: 0 (not in debug mode).

=item B<--output> (string):

Output file path. Default: templates/cache/homepage_tree.html

=item B<-log>:

Enable logging.

=item B<-log-*> (any):

Logging arguments.

=back
#--- Example:
#---
#--- Template.pl [-help | -man]
#---
#--- Template.pl [-debug [debug_level]] [-size <width> [height]]
#---
#--- =over 4
#---
#--- =item B<-help>:
#---
#--- Prints a brief help message and exits.
#---
#--- =item B<-man>:
#---
#--- Prints the manual page and exits.
#---
#--- =item B<-debug> (integer):
#---
#--- Executes the script in debug mode. If an integer value is specified, it will
#--- be the debug level. If "-debug" option was used without specifying a debug
#--- level, level 1 is assumed.
#--- Default: 0 (not in debug mode).
#---
#---=item B<-size> (positive_real) (positive_real):
#---
#--- Set the dimensions of the object that will be drawn. The first value is
#--- the width; the height is the second value if specified, otherwise it will
#--- assume height and width are equal to the first value.
#--- Default: width and height are set to 1.
#---
#---=back

=cut


# CODE START
#############

# for regression tests
if ($ENV{'TESTING'} && $ENV{'TESTING_NO_RUN'})
{return 1;}

# options processing
my ($man, $help, $debug, $no_prompt) = (0, 0, undef, undef);

my $output = $JSON_TREE_FILE_PATH;

# parse options and print usage if there is a syntax error.
GetOptions('help|?'     => \$help,
           'man'        => \$man,
           'debug:s'    => \$debug,
           'q|noprompt' => \$no_prompt,
           'output=s'   => \$output,
           @Greenphyl::Log::LOG_GETOPT, # skip log parameters
) or pod2usage(1);
if ($help) {pod2usage('-verbose' => 1, '-exitval' => 0);}
if ($man) {pod2usage('-verbose' => 2, '-exitval' => 0);}

# change debug mode if requested/forced
if (defined($debug))
{
    # just '-debug' ?
    if ($debug eq '')
    {
        $debug = 1;
    }
    $DEBUG = $debug;
}

eval
{
    # Get newick data from NCBI
    my $newick_tree = GetTaxonomyTreeFromNCBI();
    
    # # Save tree as HTML
    SaveAsHTML($newick_tree, $output);

    # Save tree as JSON
    SaveAsJSON($newick_tree, $output);
};

# catch
HandleErrors();

exit(0);

# CODE END
###########


=pod

=head1 AUTHORS

Mathieu ROUARD (Bioversity), m.rouard@cgiar.org

Valentin GUIGNON (Bioversity), v.guignon@cgiar.org

Matthieu CONTE (Syngenta)

Sebastien BRIOIS (Syngenta)

=head1 VERSION

Version 2.0.0

Date 27/01/2020

=head1 SEE ALSO

GreenPhyl documentation.

=cut
