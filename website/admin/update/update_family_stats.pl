#!/usr/bin/perl

=pod

=head1 NAME

update_family_stats.pl - Update statistics on families (including phylogeny results)

=head1 SYNOPSIS

    update_family_stats.pl

=head1 REQUIRES

Perl5, mySQL

=head1 DESCRIPTION



=cut

use strict;
use warnings;

use FindBin;
use lib "$FindBin::Bin/../../lib";
use lib "$FindBin::Bin/../../local_lib";
use Carp qw (cluck confess croak);

use Getopt::Long;
use Pod::Usage;

use File::Temp qw/ tempfile tempdir /;
use Bio::SeqIO;
use Bio::AlignIO;
 
use Greenphyl;
use Greenphyl::Log('nolog' => 1,);
use Greenphyl::Tools::Families;

++$|; #no buffering




# Script global constants
##########################

=pod

=head1 CONSTANTS


=cut

our $DEBUG = 1;
our $INITIAL_ALIGNMENT_SUFFIX   = '.mafft';
our $MASKED_ALIGNMENT_SUFFIX    = '.phy';
our $FAMILY_FASTA_SUFFIX        = '.src';




=pod

=head1 FUNCTIONS

=head2 ProcessFamilyAlignmentFiles

B<Description>: process initial and masked family alignments and update
statistics in database.

B<ArgsCount>: 2

=over 1

=item $family: (Greenphyl::Family) (R)

GreenPhyl family object.

=item $pipeline_output_path: (string) (R)

Path where family output directories should be located.

=back

B<Return>: (boolean)

A true value if the family was updated, false otherwise.

=cut

sub ProcessFamilyAlignmentFiles
{
    my ($family, $pipeline_output_path) = @_;
    
    if (!$family)
    {
        LogError("Invalid argument for ProcessFamilyAlignmentFiles(family, pipeline_output_path)!");
    }
    
    my $updated_family = 0;

    # check if the family output directory exists
    my $family_directory = FindFamilyOutputDirectory($family, $pipeline_output_path);

    if (!$family_directory)
    {
        LogWarning "Family $family output directory not found! Family skipped!\n";
        return 0;
    }

    my $sequence_file_path = FindFamilyFilePath($family, $family_directory, $FAMILY_FASTA_SUFFIX);

    # make sure family has the same sequences than the one that have been processed
    if (!$sequence_file_path)
    {
        LogWarning("Unable to check database family versus pipeline result consistency! Family FASTA file not found in '$pipeline_output_path'!");
    }
    else
    {
        # check FASTA content
        LogDebug("Checking family content consistency using $sequence_file_path file.");
        my $input_fasta = Bio::SeqIO->new(
                                    '-file'   => $sequence_file_path,
                                    '-format' => 'fasta',
        );
        # get FASTA sequence names
        my @sequence_names;
        while (my $bio_seq = $input_fasta->next_seq)
        {
            push(@sequence_names, $bio_seq->display_id);
        }
        if ($family->countSequences() < scalar(@sequence_names))
        {
            # we shouldn't have more sequence in the filtered alignement than in the initial set of family sequences!
            LogWarning("Database family $family (" . $family->countSequences() . " sequences) does not match analysed family (" . scalar(@sequence_names) . " sequences)! Skipping family.");
            return 0;
        }
        #+FIXME: Check sequences names (takes species code in account).
        #        Just a random couple of sequences?
    }

    my $initial_alignment_file_path = FindFamilyFilePath($family, $family_directory, $INITIAL_ALIGNMENT_SUFFIX);
    my $masked_alignment_file_path  = FindFamilyFilePath($family, $family_directory, $MASKED_ALIGNMENT_SUFFIX);
    my ($initial_alignment_sequence_count,
        $initial_alignment_length,
        $masked_alignment_sequence_count,
        $masked_alignment_length);

    # process initial alignment (FASTA)
    if (!$initial_alignment_file_path)
    {
        LogWarning("Initial alignment file not found in '$pipeline_output_path'! Family $family skipped!");
        return 0;
    }
    LogDebug("Processing $initial_alignment_file_path initital alignment file.");
    my $input_align = Bio::AlignIO->new(
        '-file'   => $initial_alignment_file_path,
        '-format' => 'fasta',
    );
    # $initial_alignment_sequence_count = $input_align->width();
    # $initial_alignment_length = $input_align->next_aln()->length();

    $initial_alignment_sequence_count = 0;
    if (my $bio_initial_alignment = $input_align->next_aln())
    {
        $initial_alignment_length = $bio_initial_alignment->length();
        $initial_alignment_sequence_count = $bio_initial_alignment->num_sequences();
    }

    $input_align = undef;

    # process masked alignment (PHYLIP)
    if (!$masked_alignment_file_path)
    {
        LogWarning("Masked alignment file not found in '$pipeline_output_path'! Family $family skipped!");
        return 0;
    }
    LogDebug("Processing $masked_alignment_file_path masked alignment file.");
    my $align_fh;
    if (open($align_fh, $masked_alignment_file_path))
    {
        my $phylip_header = <$align_fh>;
        ($masked_alignment_sequence_count, $masked_alignment_length) = ($phylip_header =~ m/\s*(\d+)\s+(\d+)/);
        close($align_fh);
        if (!$masked_alignment_sequence_count && !$masked_alignment_length)
        {
            # Not Phylip format.
            my $masked_align = Bio::AlignIO->new(
                '-file'   => $masked_alignment_file_path,
                '-format' => 'fasta',
            );
            if (my $bio_initial_alignment = $masked_align->next_aln())
            {
                $masked_alignment_length = $bio_initial_alignment->length();
                $masked_alignment_sequence_count = $bio_initial_alignment->num_sequences();
            }
        }
    }

    # check if we got everything and update database
    if ($initial_alignment_sequence_count
        && $initial_alignment_length
        && $masked_alignment_sequence_count
        && $masked_alignment_length)
    {
        $family->alignment_length($initial_alignment_length);
        $family->masked_alignment_length($masked_alignment_length);
        $family->alignment_sequence_count($initial_alignment_sequence_count);
        $family->masked_alignment_sequence_count($masked_alignment_sequence_count);
        $family->save();
        ++$updated_family;
    }
    else
    {
        LogWarning("Alingment statistics don't appear to be valid. Family $family not updated!");
    }

    return $updated_family;
}




# Script options
#################

=pod

=head1 OPTIONS

    update_family_stats.pl [-help | -man]
    update_family_stats.pl [-debug]
        [-pipeline_output_dir <path_to_pipeline_output>]

=over 4

=item B<-help>:

Prints a brief help message and exits.

=item B<-man>:

Prints the manual page and exits.

=item B<-debug> (integer):

Executes the script in debug mode. If an integer value is specified, it will
be the debug level. If "-debug" option was used without specifying a debug
level, level 1 is assumed.
Default: 0 (not in debug mode).

=item B<-pipeline_output_dir>: (string)

Path to local pipeline outputs (directory containing the family output
directories).
Default: GP('TEMP_OUTPUT_PATH')

=back

=cut


# CODE START
#############

# options processing
my ($man, $help, $debug, $pipeline_output_dir)
 = (   0,     0,  undef,                   '');

# parse options and print usage if there is a syntax error.
GetOptions('help|?'                => \$help,
           'man'                   => \$man,
           'debug:s'               => \$debug,
           'pipeline_output_dir=s' => \$pipeline_output_dir,
           @Greenphyl::Log::LOG_GETOPT, # skip log parameters
) or pod2usage(2);
if ($help) {pod2usage(0);}
if ($man) {pod2usage(-verbose => 2);}

# change debug mode if requested/forced
if (defined($debug))
{
    # just '-debug' ?
    if ($debug eq '')
    {
        $debug = 1;
    }
    $DEBUG = $debug;
}

# check output directory
$pipeline_output_dir ||= GP('TEMP_OUTPUT_PATH');
# remove trailing slash
$pipeline_output_dir =~ s/\/+$//;

# DBD::mysql version check
# too old versions produce the error:
# "... PROCEDURE ... can't return a result set in the given context at ..."
use DBD::mysql;
if ($DBD::mysql::VERSION < 4.006)
{
    confess LogError("ERROR: too old version of DBD::mysql for stored procedure call. Please upgrade your Perl DBD::mysql library (to > 4.006).");
}

eval
{
    LogDebug('Running in debug mode...');
    LogInfo('Update family statistics...');

    # start SQL transaction
    if (GetDatabaseHandler()->{'AutoCommit'})
    {
        GetDatabaseHandler()->begin_work() or croak GetDatabaseHandler()->errstr;
    }
    # set warning state (not fatal)
    GetDatabaseHandler()->{'HandleError'} = undef;

    # process each family (not black-listed)
    my $updated_family_count = 0;

    LoopOnFamilies(
        sub
        {
                my $family = shift();
                LogDebug("Processing family $family...");
                # update all stats (excluding alignment data)
                my $prepared_statement = GetDatabaseHandler()->prepare('CALL updateFamilyStats(?);');
                $prepared_statement->execute($family->id)
                    or LogWarning("Failed to update sequence statistics for family $family!\n" . GetDatabaseHandler()->errstr);
                # update alignment data
                $updated_family_count += ProcessFamilyAlignmentFiles($family, $pipeline_output_dir);
                LogDebug("...done with family $family.");
        }
    );

    LogInfo("Updated $updated_family_count families.");
    if (!$DEBUG)
    {SaveCurrentTransaction(GetDatabaseHandler());}

    LogInfo("Done.");

};

HandleErrors();

exit(0);

# CODE END
###########


=pod

=head1 AUTHORS

Valentin GUIGNON (CIRAD, Bioversity-France), valentin.guignon@cirad.fr, V.Guignon@cgiar.org
Mathieu ROUARD (Bioversity-France), m.rouard@cgiar.org

=head1 VERSION

Version 1.0.0

Date 03/04/2013

=head1 SEE ALSO

GreenPhyl documentation.

=cut
