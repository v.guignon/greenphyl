#!/usr/bin/perl

=pod

=head1 NAME

update_family_uniprot.pl - Update dbxref_families table


=head1 SYNOPSIS

    update_family_uniprot.pl

=head1 REQUIRES

Perl5

=head1 DESCRIPTION

Remove UniProt relationship in dbxref_families table and add new relationship
from family sequences. Table dbxref_sequences must be loaded with UniProt
cross-references.

=cut

use strict;
use warnings;

use lib "../../lib";
use lib '../../local_lib';
use Carp qw (cluck confess croak);

use Getopt::Long;
use Pod::Usage;

use Greenphyl;
use Greenphyl::Log('nolog' => 1,);
use Greenphyl::SourceDatabase;




# Script global constants
##########################

=pod

=head1 CONSTANTS

B<$DEBUG>: (boolean)

When set to true, it enables debug mode.

B<$CLEAR_FAMILY_UNIPROT_QUERY>: (string)

SQL query used to remove previous family-UniProt cross-refercences.

B<$INSERT_FAMILY_UNIPROT_QUERY>: (string)

SQL query used to insert family-UniProt cross-refercences.

=cut

our $DEBUG = 0;
our $CLEAR_FAMILY_UNIPROT_QUERY = <<"___73_END_CLEAR_FAMILY_UNIPROT_QUERY___";
    DELETE
    FROM df
    USING dbxref_families df
      JOIN dbxref d ON (d.id = df.dbxref_id)
      JOIN db db ON (db.id = d.db_id)
    WHERE
      db.name = '$Greenphyl::SourceDatabase::SOURCE_DATABASE_UNIPROT'
    ;
___73_END_CLEAR_FAMILY_UNIPROT_QUERY___

our $INSERT_FAMILY_UNIPROT_QUERY = <<"___84_END_INSERT_FAMILY_UNIPROT_QUERY___";
    INSERT INTO dbxref_families(family_id, dbxref_id)
    SELECT fs.family_id, ds.dbxref_id
    FROM dbxref_sequences ds
        JOIN dbxref d ON (d.id = ds.dbxref_id)
        JOIN db db ON (db.id = d.db_id)
        JOIN families_sequences fs ON (fs.sequence_id = ds.sequence_id)
    WHERE
      db.name = '$Greenphyl::SourceDatabase::SOURCE_DATABASE_UNIPROT'
    ;
___84_END_INSERT_FAMILY_UNIPROT_QUERY___




# Script options
#################

=pod

=head1 OPTIONS

update_family_uniprot.pl [-help | -man]

update_family_uniprot.pl [-debug [debug_level]] [-log]

=head2 Parameters

=over 4

=item B<-help>:

Prints a brief help message and exits.

=item B<-man>:

Prints the manual page and exits.

=item B<-debug> (integer):

Executes the script in debug mode. The integer value is optional and is used
to set debug level (use 0 to force disable debug mode).
Default: 0 (not in debug mode).

=item B<-log>:

Enable logging.

=item B<-log-*> (any):

Logging arguments.

=back

=cut


# CODE START
#############

# for regression tests
if ($ENV{'TESTING'} && $ENV{'TESTING_NO_RUN'})
{return 1;}

# options processing
my ($man, $help, $debug) = (0, 0, undef);
# parse options and print usage if there is a syntax error.
GetOptions('help|?'     => \$help,
           'man'        => \$man,
           'debug:s'    => \$debug,
           @Greenphyl::Log::LOG_GETOPT, # skip log parameters
) or pod2usage(1);
if ($help) {pod2usage('-verbose' => 1, '-exitval' => 0);}
if ($man) {pod2usage('-verbose' => 2, '-exitval' => 0);}

# change debug mode if requested/forced
if (defined($debug))
{
    # just '-debug' ?
    if ($debug eq '')
    {
        $debug = 1;
    }
    $DEBUG = $debug;
}

# connect to database
my $dbh = GetDatabaseHandler();

eval
{
    if ($dbh->{'AutoCommit'})
    {
        $dbh->begin_work() or croak $dbh->errstr;
    }

    # remove previous UniProt cross-refercences
    LogInfo('Clearing previous family <--> UniProt cross-refercences');
    if (!$dbh->do($CLEAR_FAMILY_UNIPROT_QUERY))
    {
        confess "ERROR: Failed to remove previous family <==> UniProt cross-refercences!\n" . $dbh->errstr;
    }
    LogInfo('...clearing done.');

    # insert new UniProt cross-refercences
    LogInfo('Inserting family <--> UniProt cross-refercences...');
    if (!$dbh->do($INSERT_FAMILY_UNIPROT_QUERY))
    {
        confess "ERROR: Failed to insert family <--> UniProt cross-references!\n" . $dbh->errstr;
    }
    LogInfo('...insertion done.');

    # commit SQL transaction
    $dbh->commit() or confess($dbh->errstr);
};

# catch
HandleErrors();

exit(0);

# CODE END
###########


=pod

=head1 AUTHORS

Mathieu ROUARD (Bioversity), m.rouard@cgiar.org

Valentin GUIGNON (Bioversity), v.guignon@cgiar.org

Matthieu CONTE (Syngenta)

Sebastien BRIOIS (Syngenta)

=head1 VERSION

Version 1.0.0

Date 03/05/2013

=head1 SEE ALSO

GreenPhyl documentation.

=cut
