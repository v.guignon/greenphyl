#!/usr/bin/perl

=pod

=head1 NAME

update_homologies.pl - Update homology data on families

=head1 SYNOPSIS

    update_homologies.pl

=head1 REQUIRES

Perl5, mySQL

=head1 DESCRIPTION

Insert homologies relationship into database. When a family output is processed,
previous homologies from that family are removed from database first.

update tables:
-homologies
-homologs

=cut

use strict;
use warnings;

use FindBin;
use lib "$FindBin::Bin/../../lib";
use lib "$FindBin::Bin/../../local_lib";
use Carp qw (cluck confess croak);

use Getopt::Long;
use Pod::Usage;

use File::Temp qw/ tempfile tempdir /;
 
use Greenphyl;
use Greenphyl::Log('nolog' => 1,);
use Greenphyl::Sequence;
use Greenphyl::Tools::Families;

++$|; #no buffering




# Script global constants
##########################

=pod

=head1 CONSTANTS


=cut

our $DEBUG = 0;
our $RAP_HOMOLOGY_SUFFIX           = '_rap_stats_tree.txt';
our $HOMOLOGY_TYPE_ORTHOLOGY       = 'orthology';
our $HOMOLOGY_TYPE_SUPER_ORTHOLOGY = 'super-orthology';
our $HOMOLOGY_TYPE_ULTRA_PARALOGY  = 'ultra-paralogy';




=pod

=head1 FUNCTIONS

=head2 ProcessFamilyPhylogenyFiles

B<Description>: process phylogeny files and update statistics in database.

B<ArgsCount>: 2

=over 1

=item $family: (Greenphyl::Family) (R)

GreenPhyl family object.

=item $pipeline_output_path: (string) (R)

Path where family output directories should be located.

=back

B<Return>: (boolean)

A true value if the family was updated, false otherwise.

=cut

sub ProcessFamilyPhylogenyFiles
{
    my ($family, $pipeline_output_path) = @_;
    
    if (!$family)
    {
        LogError("Invalid argument for ProcessFamilyPhylogenyFiles(family, pipeline_output_path)!");
    }
    
    my $updated_family = 0;

    # check if the family output directory exists
    my $family_directory = FindFamilyOutputDirectory($family, $pipeline_output_path);

    if (!$family_directory)
    {
        LogWarning "Family $family output directory not found! Family skipped!\n";
        return 0;
    }

    my $homology_file_path = FindFamilyFilePath($family, $family_directory, $RAP_HOMOLOGY_SUFFIX);
    
    # check if we got homology results
    if (!$homology_file_path)
    {
        LogWarning("Unable to process homology results for family $family: homology statistics file not found in '$pipeline_output_path'!");
    }
    else
    {
        my $homology_fh;
        if (open($homology_fh, $homology_file_path))
        {
            my $dbh = GetDatabaseHandler();
            # remove previous homologies of processed family
            my $sql_query = "DELETE FROM homologies WHERE family_id = ?;";
            LogDebug("SQL QUERY: $sql_query\nBindings: " . $family->id);
            if (!$dbh->do($sql_query, undef, $family->id))
            {
                LogWarning("Failed to remove previous homology data for family '$family'!\n" . $dbh->errstr);
            }

            # skip first line
            my $line = <$homology_fh>;
            # ex.:
            # GENE1                   kVALUE  DIST    SPEC    T-DUP   I-DUP   U-DUP   ORTHO   ULTRAP  GENE2                   FSCORE
            # Glyma13g02700.1_GLYMA   300     1.0     1       0       0       0       true    false   EG4P293_ELAGV           0.102
            # MA_60583g0010_PICAB     2       0.0171  0       0       0       2       false   true    MA_10434711g0020_PICAB  0.965

            # New format:
            # GENE1                                           kVALUE  DIST    SPEC    T-DUP   I-DUP   ORTHO   ULTRAP  GENE2
            # evm_27.model.AmTr_v1.0_scaffold00002.587_AMBTC  60      1.2306  4       1       0       true    false   Dr16474_DIORT

            my $line_count = 1;
            while ($line = <$homology_fh>)
            {
                ++$line_count;
                # my ($udup, $fscore) = (0, 0);
                my ($gene_name,
                    $species_code,
                    $kvalue,
                    $distance,
                    $speciation,
                    $tdup,
                    $idup,
                    $udup,
                    $orthology,
                    $ultraparalogy,
                    $homologous_gene,
                    $homologous_species,
                    $fscore
                    ) = (
                        $line =~ m/^
                            (\S+)_(\w{3,5})\s+   # gene name and species code
                            ([\d.\-+eE]+)\s+     # kvalue
                            ([\d.\-+eE]+)\s+     # distance
                            ([\d.\-+eE]+)\s+     # speciation
                            ([\d.\-+eE]+)\s+     # t-dup
                            ([\d.\-+eE]+)\s+     # i-dup
                            ([\d.\-+eE]+)\s+     # u-dup
                            (true|false)\s+      # orthology
                            (true|false)\s+      # paralogy
                            (\S+)_(\w{3,5})\s+   # homolog gene and species
                            ([\d.\-+eE]+)        # f-score
                            $
                        /xi
                        # $line =~ m/
                        #     (\S+)_(\w{3,5})\s+   # gene name and species code
                        #     (\d+)\s+             # kvalue
                        #     ([\d.\-+eE]+)\s+     # distance
                        #     (\d+)\s+             # speciation
                        #     (\d+)\s+             # t-dup
                        #     (\d+)\s+             # i-dup
                        #     (\d+)\s+             # u-dup
                        #     (\w+)\s+             # orthology
                        #     (\w+)\s+             # paralogy
                        #     (\S+)_(\w{3,5})   # homolog gene and species
                        # /x
                );
                if (defined($homologous_species))
                {
                    LogDebug("$gene_name ($species_code) is " . ($orthology =~ m/true/i ? 'orthologous':'ultraparalogous') . " to $homologous_gene ($homologous_species)\n");
                    # insert homology relationship
                    $sql_query = "
                        INSERT INTO homologies
                            (family_id,
                            type,
                            score,
                            kvalue,
                            distance,
                            speciation,
                            i_duplication,
                            t_duplication,
                            u_duplication)
                        VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?);"
                    ;
                    my @bindings = (
                        $family->id,
                        ($orthology =~ m/true/i ? $HOMOLOGY_TYPE_ORTHOLOGY : $HOMOLOGY_TYPE_ULTRA_PARALOGY),
                        $fscore,
                        $kvalue,
                        $distance,
                        $speciation,
                        $idup,
                        $tdup,
                        $udup,
                    );
                    LogDebug("SQL QUERY: $sql_query\nBindings: " . join(', ', @bindings));
                    if (!$dbh->do($sql_query, undef, @bindings))
                    {
                        LogWarning("Failed to insert homology for $gene_name ($species_code) and $homologous_gene ($homologous_species)!\n" . $dbh->errstr);
                    }
                    else
                    {
                        my $homology_id = $dbh->last_insert_id(undef, undef, 'homologies', 'id');
                        if (!$homology_id)
                        {
                            confess LogError("Failed to retrieve inserted homology identifierfor for $gene_name ($species_code) and $homologous_gene ($homologous_species)!");
                        }

                        # find corresponding sequences
                        my $subject_sequence = Greenphyl::Sequence->new($dbh, {'selectors' => {'accession' => $gene_name}});
                        my $object_sequence = Greenphyl::Sequence->new($dbh, {'selectors' => {'accession' => $homologous_gene}});
                        if (!$subject_sequence)
                        {
                            confess LogError("Failed to retrieve a sequence in database: $gene_name");
                        }
                        elsif (!$subject_sequence->isInFamily($family))
                        {
                            confess LogError("Sequence $subject_sequence is not a member of family $family while the homology file says so!");
                        }
                        elsif (!$object_sequence)
                        {
                            confess LogError("Failed to retrieve a sequence in database: $homologous_gene");
                        }
                        elsif (!$object_sequence->isInFamily($family))
                        {
                            confess LogError("Sequence $object_sequence is not a member of family $family while the homology file says so!");
                        }

                        # store relationship
                        if (!$dbh->do("INSERT INTO homologs (homology_id, subject_sequence_id, object_sequence_id) VALUES (?, ?, ?);", undef, $homology_id, $subject_sequence->id, $object_sequence->id))
                        {
                            LogWarning("Failed to insert homology relationship for $gene_name ($species_code) and $homologous_gene ($homologous_species)!\n" . ($homology_id ? $dbh->errstr : ''));
                        }
                        else
                        {
                            ++$updated_family;
                        }
                    }
                }
                else
                {
                    confess LogError("Failed to parse homology line ($homology_file_path:$line_count):\n$line");
                }
            }
            LogInfo("$family info inserted!\n");
        }
        else
        {
            LogWarning("Failed to open RAP output file '$homology_file_path' (family $family)!\n$!\n");
        }
    }

    return $updated_family;
}




# Script options
#################

=pod

=head1 OPTIONS

    update_homologies.pl [-help | -man]
    update_homologies.pl [-debug]
        [-pipeline_output_dir <path_to_pipeline_output>]

=over 4

=item B<-help>:

Prints a brief help message and exits.

=item B<-man>:

Prints the manual page and exits.

=item B<-debug> (integer):

Executes the script in debug mode. If an integer value is specified, it will
be the debug level. If "-debug" option was used without specifying a debug
level, level 1 is assumed.
Default: 0 (not in debug mode).

=item B<-pipeline_output_dir>: (string)

Path to local pipeline outputs (directory containing the family output
directories).
Default: GP('TEMP_OUTPUT_PATH')

=back

=cut


# CODE START
#############

# options processing
my ($man, $help, $debug, $pipeline_output_dir)
 = (   0,     0,  undef,                   '');

# parse options and print usage if there is a syntax error.
GetOptions('help|?'                => \$help,
           'man'                   => \$man,
           'debug:s'               => \$debug,
           'pipeline_output_dir=s' => \$pipeline_output_dir,
           @Greenphyl::Log::LOG_GETOPT, # skip log parameters
) or pod2usage(2);
if ($help) {pod2usage(0);}
if ($man) {pod2usage(-verbose => 2);}

# change debug mode if requested/forced
if (defined($debug))
{
    # just '-debug' ?
    if ($debug eq '')
    {
        $debug = 1;
    }
    $DEBUG = $debug;
}

# check output directory
$pipeline_output_dir ||= GP('TEMP_OUTPUT_PATH');
# remove trailing slash
$pipeline_output_dir =~ s/\/+$//;

eval
{
    LogDebug('Running in debug mode...');
    LogInfo('Update family homology data...');

    # # start SQL transaction
    # if (GetDatabaseHandler()->{'AutoCommit'})
    # {
    #     GetDatabaseHandler()->begin_work() or croak GetDatabaseHandler()->errstr;
    # }
    # set warning state (not fatal)
    GetDatabaseHandler()->{'HandleError'} = undef;

    # process each family (not black-listed)
    my $updated_family_count = 0;

    LoopOnFamilies(
        sub
        {
            my $family = shift();
            LogDebug("Processing family $family...");
            # update homology data
            $updated_family_count += ProcessFamilyPhylogenyFiles($family, $pipeline_output_dir);
            LogDebug("...done with family $family.");
        }
    );

    LogInfo("Updated $updated_family_count families.");
    # if (!$DEBUG)
    # {SaveCurrentTransaction(GetDatabaseHandler());}

    LogInfo("Done.");

};

HandleErrors();

exit(0);

# CODE END
###########


=pod

=head1 AUTHORS

Valentin GUIGNON (CIRAD, Bioversity-France), valentin.guignon@cirad.fr, V.Guignon@cgiar.org
Mathieu ROUARD (Bioversity-France), m.rouard@cgiar.org

=head1 VERSION

Version 1.0.0

Date 03/04/2013

=head1 SEE ALSO

GreenPhyl documentation.

=cut
