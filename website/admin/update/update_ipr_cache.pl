#!/usr/bin/env perl

=pod

=head1 NAME

update_ipr_cache.pl - Update IPR-related cached data in database.

=head1 SYNOPSIS

    update_ipr_cache.pl

=head1 REQUIRES

Perl5

=head1 DESCRIPTION

-families_ipr_species_cache
-families_ipr_cache

=cut

use strict;
use warnings;

use FindBin;
use lib "$FindBin::Bin/../../lib";
use lib "$FindBin::Bin/../../local_lib";
use Carp qw (cluck confess croak);

use Greenphyl;
use Greenphyl::Log('nolog' => 1,);

use Getopt::Long;
use Pod::Usage;

++$|; #no buffering




# Script global constants
##########################

=pod

=head1 CONSTANTS

B<$REV_STRING>: (string)

Current revision (auto-set by GIT).

B<$DEBUG>: (boolean)

When set to true, it enables debug mode.

=cut

# SVN management part
my $REV_STRING = '$id$';

our $DEBUG = 0;




# Script global variables
##########################

=pod

=head1 VARIABLES

B<$g_dbh>: (DBI::db)

Global database handler.

=cut

my $g_dbh = GetDatabaseHandler();




# Script global functions
##########################

=pod

=head1 FUNCTIONS

=head2 LoadFamiliesIPRSpeciesCache

B<Description>: fills families_ipr_species_cache table.

B<ArgsCount>: 0

B<Return>: nothing

=cut

sub LoadFamiliesIPRSpeciesCache
{
    # clear previous cache data
    $g_dbh->do(qq{DELETE FROM families_ipr_species_cache;})
        or confess LogError("Failed to clear families_ipr_species_cache table:\n" . $g_dbh->errstr);

    # fill table
    my $inserted_row_count = $g_dbh->do(qq{
        INSERT IGNORE INTO families_ipr_species_cache (family_id, ipr_id, species_id)
        (
            SELECT family_id, ipr_id, species_id
            FROM sequences
                JOIN families_sequences
                    ON families_sequences.sequence_id = sequences.id
                JOIN ipr_sequences
                    ON ipr_sequences.sequence_id = sequences.id
         );
    });

    LogInfo("$inserted_row_count rows inserted into family_ipr_cache");
}


=pod

=head2 LoadFamiliesIPRCache

B<Description>: fills families_ipr_cache table. Process families, level by
level, starting at level 1.

B<ArgsCount>: 0

B<Return>: nothing

=cut

sub LoadFamiliesIPRCache
{
    # clear previous cache data
    $g_dbh->do(qq{DELETE FROM families_ipr_cache;})
        or confess LogError("Failed to clear families_ipr_cache table:\n" . $g_dbh->errstr);

    # Allow longer GROUP_CONCAT.
    $g_dbh->do(qq{SET SESSION group_concat_max_len = 1000000;})
        or confess LogError("Failed to change GROUP_CONCAT limit:\n" . $g_dbh->errstr);

    # Query to get all families of a given level, grouped by ipr
    my $families_by_level_and_ipr_query = $g_dbh->prepare(qq{
        SELECT
            ipr_id AS "ipr_id",
            GROUP_CONCAT(DISTINCT families.id) AS "family_ids"
        FROM
            families
                JOIN families_sequences ON family_id = families.id
                JOIN ipr_sequences USING (sequence_id)
        WHERE
            level = ?
        GROUP BY (ipr_id);
    });

    my $insert_query = qq{
        INSERT IGNORE INTO families_ipr_cache (
            family_id,
            ipr_id,
            sequence_count,
            percent,
            family_specific
        )
        VALUES (?, ?, ?, ?, ?);
    };

    my $inserted_rows = 0;

    foreach my $level (1..4)
    {
        LogInfo("Processing level $level families...");
        $families_by_level_and_ipr_query->execute($level);
        my $families_by_ipr = $families_by_level_and_ipr_query->fetchall_arrayref({});

        foreach my $ipr_families_data (@$families_by_ipr)
        {
            my @family_ids = split(',', $ipr_families_data->{'family_ids'});
            my $is_specific = (scalar(@family_ids) == 1 ? 1 : 0);
            LogVerboseInfo("Working on IPR ID " . $ipr_families_data->{'ipr_id'} . " (" . scalar(@family_ids) . " famil" . ($is_specific ? 'y' : 'ies') . ")");

            foreach my $family_id (@family_ids)
            {
                my $counts = $g_dbh->selectall_arrayref(
                    qq{
                        SELECT
                            COUNT(DISTINCT sequence_id) AS "sequence_count",
                            ROUND(100.0 * (COUNT(DISTINCT sequence_id) / sequence_count), 2) AS "percent"
                        FROM
                            families
                                JOIN families_sequences ON family_id = families.id
                                JOIN ipr_sequences USING( sequence_id )
                        WHERE
                            family_id = ?
                            AND ipr_id = ?
                    },
                    { 'Slice' => {} },
                    $family_id,
                    $ipr_families_data->{'ipr_id'},
                );

                $inserted_rows += $g_dbh->do(
                    $insert_query,
                    undef,
                    $family_id,
                    $ipr_families_data->{'ipr_id'},
                    $counts->[0]->{'sequence_count'},
                    $counts->[0]->{'percent'},
                    $is_specific,
                );
            }
        }
        LogInfo("...done processing level $level families.");
    }

    LogInfo("$inserted_rows rows inserted into families_ipr_cache");
}




# Script options
#################

=pod

=head1 OPTIONS

    update_ipr_cache.pl [-help|-man]
    update_ipr_cache.pl [-debug] [-skip-family_ipr_cache] [-skip-family_ipr_species_cache]


=head2 Parameters

=over 4

=item B<-help>:

Prints a brief help message and exits.

=item B<-man>:

Prints the manual page and exits.

=item B<-debug> (integer):

Executes the script in debug mode. The integer value is optional and is used
to set debug level (use 0 to force disable debug mode).
Default: 0 (not in debug mode).

=item B<skip-family_ipr_cache> (flag):

If set, family_ipr_cache table will not be updated.

=item B<skip-family_ipr_species_cache> (flag):

If set, family_ipr_species_cache table will not be updated.

=item B<-log>:

Enable logging.

=item B<-log-*> (any):

Logging arguments.

=back

=cut


# CODE START
#############

# for regression tests
if ($ENV{'TESTING'} && $ENV{'TESTING_NO_RUN'})
{return 1;}

# options processing
my ($man, $help, $debug, $no_prompt) = (0, 0, undef, undef);
# parse options and print usage if there is a syntax error.
my ($skip_family_ipr_cache, $skip_family_ipr_species_cache);

GetOptions('help|?'     => \$help,
           'man'        => \$man,
           'debug:s'    => \$debug,
           'q|noprompt' => \$no_prompt,
           'skip-family_ipr_cache'         => \$skip_family_ipr_cache,
           'skip-family_ipr_species_cache' => \$skip_family_ipr_species_cache,
           @Greenphyl::Log::LOG_GETOPT, # skip log parameters
) or pod2usage(1);
if ($help) {pod2usage('-verbose' => 1, '-exitval' => 0);}
if ($man) {pod2usage('-verbose' => 2, '-exitval' => 0);}

# change debug mode if requested/forced
if (defined($debug))
{
    # just '-debug' ?
    if ($debug eq '')
    {
        $debug = 1;
    }
    $DEBUG = $debug;
}

eval
{
    if (!$skip_family_ipr_cache)
    {
        LogInfo("Loading families_ipr_cache...");
        LoadFamiliesIPRCache();
        LogInfo("...done loading families_ipr_cache.");
    }

    if (!$skip_family_ipr_species_cache)
    {
        LogInfo("Loading families_ipr_species_cache...");
        LoadFamiliesIPRSpeciesCache();
        LogInfo("...done loading families_ipr_species_cache.");
    }

};

# catch
HandleErrors();

exit(0);

# CODE END
###########


=pod

=head1 AUTHORS

Mathieu ROUARD (Bioversity), m.rouard@cgiar.org

Valentin GUIGNON (Bioversity), v.guignon@cgiar.org

Matthieu CONTE (Syngenta)

Sebastien BRIOIS (Syngenta)

=head1 VERSION

Version 1.0.0

Date 19/11/2013

=head1 SEE ALSO

GreenPhyl documentation.

=cut
