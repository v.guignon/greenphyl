#!/usr/bin/env perl

=pod

=head1 NAME

update_families_cache.pl - Update family-related cached data in database.

=head1 SYNOPSIS

    update_families_cache.pl

=head1 REQUIRES

Perl5

=head1 DESCRIPTION

-families_cache

=cut

use strict;
use warnings;

use FindBin;
use lib "$FindBin::Bin/../../lib";
use lib "$FindBin::Bin/../../local_lib";
use Carp qw (cluck confess croak);

use Greenphyl;
use Greenphyl::Log('nolog' => 1,);
use Greenphyl::CachedFamily;
use Greenphyl::Tools::Families;

use Getopt::Long;
use Pod::Usage;

++$|; #no buffering




# Script global constants
##########################

=pod

=head1 CONSTANTS

B<$REV_STRING>: (string)

Current revision (auto-set by GIT).

B<$DEBUG>: (boolean)

When set to true, it enables debug mode.

=cut

# SVN management part
my $REV_STRING = '$id$';

our $DEBUG = 1;




# Script global variables
##########################

=pod

=head1 VARIABLES

B<$g_dbh>: (DBI::db)

Global database handler.

=cut

my $g_dbh = GetDatabaseHandler();




# Script global functions
##########################

=pod

=head1 FUNCTIONS

=head2 LoadFamilyCache

B<Description>: updates families_cache table.

B<ArgsCount>: 0

B<Return>: nothing

=cut

sub LoadFamilyCache
{
    my ($family) = @_;
    
    if (!$family)
    {
        confess "ERROR: Missing parameters for LoadFamilyCache!\n";
    }
    
    #+debug
    confess 'DEBUG STOP' if ($family->accession ne 'GP000046');
    
    # Clone family.
    my $cached_family = Greenphyl::CachedFamily->new(
            GetDatabaseHandler(),
            {
                'load' => 'none',
                'members' => $family->getHashValue(1),
            },
        );
    # Add members to serialize.
    # use Data::Dumper;
    # print Dumper([$family->ipr_species]);
    my $ipr_cache = {
      'families_ipr_cache' => [],
      'families_ipr_species_cache' => [],
      'ipr' => $family->ipr(),
    };
    
    # my $iprs = ;
    
    # my @ipr = $family->ipr_species->[0]->getHashValue()
    
    $cached_family->setIprCache($ipr_cache);
    #+++

$Greenphyl::DBObject::DEBUG = 1;
    $cached_family->save(1);

    LogInfo("Updated cached family " . $cached_family->id);
}




# Script options
#################

=pod

=head1 OPTIONS

    update_families_cache.pl [-help|-man]
    update_families_cache.pl [-debug] [family selectors]


=head2 Parameters

=over 4

=item B<-help>:

Prints a brief help message and exits.

=item B<-man>:

Prints the manual page and exits.

=item B<-debug> (integer):

Executes the script in debug mode. The integer value is optional and is used
to set debug level (use 0 to force disable debug mode).
Default: 0 (not in debug mode).

=item B<-log>:

Enable logging.

=item B<-log-*> (any):

Logging arguments.

=back

=cut


# CODE START
#############

# for regression tests
if ($ENV{'TESTING'} && $ENV{'TESTING_NO_RUN'})
{return 1;}

# options processing
my ($man, $help, $debug, $no_prompt) = (0, 0, undef, undef);

GetOptions('help|?'     => \$help,
           'man'        => \$man,
           'debug:s'    => \$debug,
           'q|noprompt' => \$no_prompt,
           @Greenphyl::Log::LOG_GETOPT, # skip log parameters
) or pod2usage(1);
if ($help) {pod2usage('-verbose' => 1, '-exitval' => 0);}
if ($man) {pod2usage('-verbose' => 2, '-exitval' => 0);}

# change debug mode if requested/forced
if (defined($debug))
{
    # just '-debug' ?
    if ($debug eq '')
    {
        $debug = 1;
    }
    $DEBUG = $debug;
}

eval
{
    LogDebug('Running in debug mode...');
    LoopOnFamilyLists(
        sub
        {
            my ($selected_families) = @_;
            
            foreach my $family (@$selected_families)
            {
                LoadFamilyCache($family);
            }

        }
    );
};


my $error;

if ($error = Exception::Class->caught())
{
    warn "An error occured!\n" . $error;
}


exit(0);

# CODE END
###########


=pod

=head1 AUTHORS

Mathieu ROUARD (Bioversity), m.rouard@cgiar.org

Valentin GUIGNON (Bioversity), v.guignon@cgiar.org

=head1 VERSION

Version 1.0.0

Date 13/05/2020

=head1 SEE ALSO

GreenPhyl documentation.

=cut
