#!/usr/bin/env perl

=pod

=head1 NAME

manage_pipeline_outputs.pl - Manages GreenPhyl output files (web transfer)

=head1 SYNOPSIS

    manage_pipeline_outputs.pl

=head1 REQUIRES

Perl5, scp, cp, ln, rsync

=head1 DESCRIPTION

Transfer some GreenPhyl output files to the web server which can be either on
the same server used for computation or on another server.
When the computation server differs from the web server, before running this
script, an SSH-RSA key system should be used in order to log into the web
server without requesting a password.

To generate RSA keys:
on the web server (ie. gohelle):
$ mkdir ~/.ssh
$ chmod 700 ~/.ssh

on the computation server (ie. marmadais):
$ ssh-keygen -t rsa -f /home/<login>/.ssh/greenphyl_rsa
$ cat /home/<login>/.ssh/greenphyl_rsa.pub | ssh -l <login> <web_server> 'cd .ssh; cat >> authorized_keys; chmod 600 authorized_keys'

Check automatic access:
$ ssh -l <login> -i /home/<login>/.ssh/greenphyl_rsa <web_server>

Example of pipeline output file names:
<FAMILY_ACCESSION>/<FAMILY_ID>.1.bsp
<FAMILY_ACCESSION>/<FAMILY_ID>.1.mask
<FAMILY_ACCESSION>/<FAMILY_ID>.1_masking.html
<FAMILY_ACCESSION>/<FAMILY_ID>.2.bsp
<FAMILY_ACCESSION>/<FAMILY_ID>.2.mask
<FAMILY_ACCESSION>/<FAMILY_ID>.2_masking.html
<FAMILY_ACCESSION>/<FAMILY_ID>.3.mask
<FAMILY_ACCESSION>/<FAMILY_ID>.dic
<FAMILY_ACCESSION>/<FAMILY_ID>.enc_rap_gene_tree.nwk
<FAMILY_ACCESSION>/<FAMILY_ID>.enc_rap_reconcilied_gene_tree.nwk
<FAMILY_ACCESSION>/<FAMILY_ID>.enc_rap_stats_tree.txt
<FAMILY_ACCESSION>/<FAMILY_ID>.enc_rap_tree.xml
<FAMILY_ACCESSION>/<FAMILY_ID>_filtered.html
<FAMILY_ACCESSION>/<FAMILY_ID>_filtered.seq
<FAMILY_ACCESSION>/<FAMILY_ID>.fltr.fasta
<FAMILY_ACCESSION>/<FAMILY_ID>.fltr.fasta.aln
<FAMILY_ACCESSION>/<FAMILY_ID>.hmm
<FAMILY_ACCESSION>/<FAMILY_ID>_hmmbuild_err.log
<FAMILY_ACCESSION>/<FAMILY_ID>.mafft
<FAMILY_ACCESSION>/<FAMILY_ID>_mafft_err.log
<FAMILY_ACCESSION>/<FAMILY_ID>.phy
<FAMILY_ACCESSION>/<FAMILY_ID>_phylogeny_tree.nwk
<FAMILY_ACCESSION>/<FAMILY_ID>_phylogeny_tree.nwk.out
<FAMILY_ACCESSION>/<FAMILY_ID>_phylogeny_tree.nwk.out.out
<FAMILY_ACCESSION>/<FAMILY_ID>_phyml_err.log
<FAMILY_ACCESSION>/<FAMILY_ID>_phyml.log
<FAMILY_ACCESSION>/<FAMILY_ID>.phy_phyml_dist.txt
<FAMILY_ACCESSION>/<FAMILY_ID>.phy_phyml_pos.txt
<FAMILY_ACCESSION>/<FAMILY_ID>.phy_phyml_stats.txt
<FAMILY_ACCESSION>/<FAMILY_ID>_rap.dic
<FAMILY_ACCESSION>/<FAMILY_ID>_rap_err.log
<FAMILY_ACCESSION>/<FAMILY_ID>_rap_gene_tree.nwk
<FAMILY_ACCESSION>/<FAMILY_ID>_rap.log
<FAMILY_ACCESSION>/<FAMILY_ID>_rap_reconcilied_gene_tree.nwk
<FAMILY_ACCESSION>/<FAMILY_ID>_rap_stats_tree.txt
<FAMILY_ACCESSION>/<FAMILY_ID>_rap_tree.xml
<FAMILY_ACCESSION>/<FAMILY_ID>.src
<FAMILY_ACCESSION>/<FAMILY_ID>.src.out
<FAMILY_ACCESSION>/<FAMILY_ID>_trimal.log

=cut

use strict;
use warnings;

use FindBin;
use lib "$FindBin::Bin/../../lib";
use lib "$FindBin::Bin/../../local_lib";
use Carp qw (cluck confess croak);

use Getopt::Long;
use Pod::Usage;
use File::Temp qw/ tempfile tempdir /;
 
use Greenphyl;
use Greenphyl::Log('nolog' => 1,);
use Greenphyl::Family;
use Greenphyl::Tools::Families;
use Greenphyl::Species;
use Greenphyl::Sequence;

++$|; #no buffering




# Script global constants
##########################

=pod

=head1 CONSTANTS

B<$DEBUG>: (boolean)

Enables debug mode.

B<$WEB_CONFIG_NAMESPACE>: (string)

PERL namespace for remote config to use to replace the original one that would
conflict with current config 'Greenphyl::Config'.

B<$*_EXTENSION> and B<$*_SUFFIX>: (string)

File extensions/suffices.

B<$IDENTITY_PARAMETER>: (string)

Parameter used to authenticate SSH sessions on remote web using a certificate
file.

=cut

our $DEBUG                             = 0;
our $WEB_CONFIG_NAMESPACE              = 'Greenphyl::WebConfig';

our $INPUT_FILE_EXTENSION              = '.src';
our $OUTPUT_FILE_EXTENSION             = '.out';
our $DICTIONARY_FILE_EXTENSION         = '.dic';
our $FASTA_FILE_EXTENSION              = '.fasta';
our $MAFFT_FILE_EXTENSION              = '.mafft';
our $HMM_FILE_EXTENSION                = '.hmm';
our $MASKING_FILE_EXTENSION            = '.mask';
our $FILTERING_FILE_EXTENSION          = '.fltr';
our $BSP_MASKING_FILE_EXTENSION        = '.bsp';
our $MASKING_HTML_FILE_EXTENSION       = "_masking.html";
our $FILTERED_HTML_FILE_EXTENSION      = "_filtered.html";
our $FILTERED_SEQ_FILE_EXTENSION       = "_filtered.seq";
our $PHYLIP_FILE_EXTENSION             = '.phy';
our $NEWICK_FILE_EXTENSION             = '.nwk';
our $PHYLOXML_FILE_EXTENSION           = '.xml';
our $PHYML_DISTANCE_MATRIX_FILE_SUFFIX = '_phyml_dist.txt';
our $PHYLOGENY_FILE_SUFFIX             = '_phylogeny_tree' . $NEWICK_FILE_EXTENSION;
our $ROOTED_TREE_FILE_SUFFIX           = '_rap_gene_tree' . $NEWICK_FILE_EXTENSION;
our $RECONCILIED_TREE_FILE_SUFFIX      = '_rap_reconcilied_gene_tree' . $NEWICK_FILE_EXTENSION;
our $STATS_TREE_FILE_SUFFIX            = '_rap_stats_tree.txt';
our $XML_TREE_FILE_SUFFIX              = '_rap_tree.xml';
our $RIO_DATA_OUTPUT_FILE_EXTENSION    = '.txt';
our $RIO_TREE_OUTPUT_FILE_EXTENSION    = '.xml';
our $LOG_EXTENSION                      = '.log';

my $IDENTITY_PARAMETER                 = "-i $ENV{HOME}/.ssh/greenphyl_rsa";




# Script global variables
##########################

=pod

=head1 VARIABLES

B<%g_pipeline_suffix_to_transfer>: (hash)

Keys are pipeline output file suffices that have to be transfered to the web
site and values are configuration hashes with the following keys:
-'type': (string) category of the file (alignment or tree)
-'local': (string) local path where the corresponding files should be put
-'remote': (string) remote path where the corresponding files should be copied
-'species_filtering_func': (code reference) reference to a function that can be
  used to filter species from the corresponding files if needed. If not defined,
  the file won't be filtered. The function will recieve 2 arguments: 1) the
  original file content in a string and 2) the array ref of species codes
  (5-letter strings) to filter. The function should return the filtered content.

=cut

# values must not be interpolated right now, that's why we don't use double quotes.
my %g_pipeline_suffix_to_transfer = (
    '.fltr.fasta.aln'=>
        {
            'type' => 'alignment',
            'local' => GP('MULTI_ALIGN_PATH'),
            'remote' => 'GPWeb("MULTI_ALIGN_PATH")',
            'species_filtering_func' => \&FilterSpeciesInFASTA,
        },
    '_filtered.html' =>
        {
            'type' => 'alignment',
            'local' => GP('MULTI_ALIGN_PATH'),
            'remote' => 'GPWeb("MULTI_ALIGN_PATH")',
            'species_filtering_func' => \&FilterSpeciesInTrimalHTML,
        },
    '_filtered.seq' =>
        {
            'type' => 'alignment',
            'local' => GP('MULTI_ALIGN_PATH'),
            'remote' => 'GPWeb("MULTI_ALIGN_PATH")',
        },
    '.1_masking.html' =>
        {
            'type' => 'alignment',
            'local' => GP('MULTI_ALIGN_PATH'),
            'remote' => 'GPWeb("MULTI_ALIGN_PATH")',
            'species_filtering_func' => \&FilterSpeciesInTrimalHTML,
        },
    '.2_masking.html' =>
        {
            'type' => 'alignment',
            'local' => GP('MULTI_ALIGN_PATH'),
            'remote' => 'GPWeb("MULTI_ALIGN_PATH")',
            'species_filtering_func' => \&FilterSpeciesInTrimalHTML,
        },
    '_rap_stats_tree.txt' =>
        {
            'type' => 'tree',
            'local' => GP('TREES_PATH'),
            'remote' => 'GPWeb("TREES_PATH")',
        },
    GP('PHYLO_NEWICK_TREE_FILE_SUFFIX') =>
        {
            'type' => 'tree',
            'local' => GP('TREES_PATH'),
            'remote' => 'GPWeb("TREES_PATH")',
        },
    GP('PHYLO_XML_TREE_FILE_SUFFIX') =>
        {
            'type' => 'tree',
            'local' => GP('TREES_PATH'),
            'remote' => 'GPWeb("TREES_PATH")',
        },
    # HMM_PATH
    '.hmm' =>
        {
            'type' => 'hmm',
            'local' => GP('HMM_PATH'),
            'remote' => 'GPWeb("HMM_PATH")',
        },
    # MEME_OUTPUT_PATH
);

my %g_meme_file_ext_to_transfer = (
    '.html'           => 'GPWeb("MEME_OUTPUT_PATH")',
);

my (@g_transfered_files,
    @g_not_transfered_files,
    @g_missing_files,
    @g_transfered_families,
    @g_partially_transfered_families,
    @g_unprocessed_families,
    %g_not_utd_families,
    @g_families_ok_list,
    @g_families_error_list,
    %g_family_errors,
);



=pod

=head1 FUNCTIONS

=head2 GPWeb

B<Description>: Returns the value of a GreenPhyl Web configuration constant.
If the conctant is not defined, it raise an error.

B<ArgsCount>: 1

=over 4

=item $config_constant_name: (string) (R)

Name of the constant in GreenPhyl config file without namespace or type.

=back

B<Return>: (any type of scalar)

the configuration constant value.

B<Caller>: general

B<Example>:

    my $first_database_name = GPWeb('DATABASES')->[0]{'name'};

=cut

sub GPWeb
{
    my ($config_constant_name) = @_;
    no strict 'refs';
    my $constant_ref = \${$WEB_CONFIG_NAMESPACE . "::$config_constant_name"};
    # make sure the constant exists
    if (!defined($$constant_ref))
    {
        confess "ERROR: use of uninitialized web config constant: '$config_constant_name'\n       Either it's a mistyping or the constant should be defined in lib/Greenphyl/Config.pm file.\n       Please check your code!\n";
    }
    return $$constant_ref;
}


=pod

=head2 LoadWebServerConfig

B<Description>: loads the config parameters of web server. Config can be loaded
either from a remote host (requires parameters host, login and config path) or
a local config or current config.

B<ArgsCount>: 1

=over 1

=item $parameters: (hash) (R)

Config loading parameter hash with the following options:
-'config': absolute path to config file (O) (R if host specified);
-'host':   name of the remote host (O);
-'login':  login to use on the remote host (O) (R if host specified);

=back

B<Return>: nothing

B<Example>:

    LoadWebServerConfig($parameters);

=cut

sub LoadWebServerConfig
{
    my ($parameters) = @_;

    # check if it's a remote file
    if ($parameters->{'host'})
    {
        # load remote config in Greenphyl::WebConfig
        LogDebug("Try to get remote config " . $parameters->{'login'} . "\@" . $parameters->{'host'} . ":" . $parameters->{'config'});
        # -get remote file
        my ($config_fh, $config_filename) = tempfile();
        my $command = "scp $IDENTITY_PARAMETER -p -q " . $parameters->{'login'} . "\@" . $parameters->{'host'} . ":" . $parameters->{'config'} . " $config_filename";
        LogDebug("COMMAND: $command");
        if (system($command))
        {
            confess LogError("Failed to get web config file: $!");
        }
        # -read content
        my $config_data = join('', <$config_fh>);
        LogDebug("Remote config data ('$config_filename'):\n----------\n$config_data\n----------\n\n");
        close($config_fh);
        # -remove local copy
        unlink($config_filename);

        # -replace workspace to avoid conflicts with local config workspace
        $config_data =~ s/package\s+Greenphyl::Config/package Greenphyl::WebConfig/;
        # -load config
        eval $config_data;
        if ($@)
        {
            confess LogError("Failed to load remote (web) configuration!\n" . $@);
        }
    }
    elsif ($parameters->{'config'})
    {
        # local config in another directory
        # load local config in Greenphyl::WebConfig
        my $config_fh;
        if (!open($config_fh, $parameters->{'config'}))
        {
            confess LogError("Failed to open local web config file '" . $parameters->{'config'} . "': $!");
        }
        # -read content
        my $config_data = join('', <$config_fh>);
        LogDebug("Remote config data ('$parameters->{'config'}'):\n----------\n$config_data\n----------\n\n");
        close($config_fh);

        # -replace workspace to avoid conflicts with local config workspace
        $config_data =~ s/package\s+Greenphyl::Config/package Greenphyl::WebConfig/;
        # -load config
        eval $config_data;
        if ($@)
        {
            confess LogError("Failed to load local web configuration!\n" . $@);
        }
    }
    else
    {
        # use local config
        $WEB_CONFIG_NAMESPACE = 'Greenphyl::Config';
        #+FIXME: or use below instead?
        # *Greenphyl::WebConfig:: = *Greenphyl::Config::;
        
    }
}


=pod

=head2 ExpandRemoteTransferPath

B<Description>: Interpolate target remote path for each file suffix to transfer.

B<ArgsCount>: 1

=over 1

=item $parameters: (hash) (O)

unused at the moment.

=back

B<Return>: nothing

=cut

sub ExpandRemoteTransferPath
{
    my ($parameters) = @_;

    my $file_ext;
    foreach $file_ext (keys(%g_pipeline_suffix_to_transfer))
    {
        eval "\$g_pipeline_suffix_to_transfer{'$file_ext'}->{'remote'} = " . $g_pipeline_suffix_to_transfer{$file_ext}->{'remote'} . ";";
    }
    foreach $file_ext (keys(%g_meme_file_ext_to_transfer))
    {
        eval "\$g_meme_file_ext_to_transfer{'$file_ext'} = " . $g_meme_file_ext_to_transfer{$file_ext} . ";";
    }
}


=pod

=head2 InitLocalTargetPath

B<Description>: Check and set local target directories. It updates 'local'
fields of global hash %g_pipeline_suffix_to_transfer.

B<ArgsCount>: 1

=over 1

=item $parameters: (hash) (O)

Parameter hash. Used keys are:
-'align_path': path to use for alignment-related files
-'tree_path':  path to use for tree-related files

=back

B<Return>: nothing

=cut

sub InitLocalTargetPath
{
    my ($parameters) = @_;

    if ($parameters->{'align_path'}
        && ((!-r $parameters->{'align_path'}) || (!-d $parameters->{'align_path'})))
    {
        confess LogError("Invalid alignment path ('$parameters->{'align_path'}')! Make sure it exists as a directory and it has read access!");
    }

    if ($parameters->{'tree_path'}
        && ((!-r $parameters->{'tree_path'}) || (!-d $parameters->{'tree_path'})))
    {
        confess LogError("Invalid tree path ('tree_path')! Make sure it exists as a directory and it has read access!");
    }

    if ($parameters->{'align_path'})
    {
        foreach my $suffix (keys(%g_pipeline_suffix_to_transfer))
        {
            if ('alignment' eq $g_pipeline_suffix_to_transfer{$suffix}->{'type'})
            {
                $g_pipeline_suffix_to_transfer{$suffix}->{'local'} = $parameters->{'align_path'};
            }
        }
    }

    if ($parameters->{'tree_path'})
    {
        foreach my $suffix (keys(%g_pipeline_suffix_to_transfer))
        {
            if ('tree' eq $g_pipeline_suffix_to_transfer{$suffix}->{'type'})
            {
                $g_pipeline_suffix_to_transfer{$suffix}->{'local'} = $parameters->{'tree_path'};
            }
        }
    }

}


=pod

=head2 PrintConfig

B<Description>: Display configuration.

B<ArgsCount>: 1

=over 1

=item $parameters: (hash ref) (R)

Hash of parameters.

=back

B<Return>: nothing

B<Example>:

    PrintConfig($parameters);

=cut

sub PrintConfig
{
    my ($parameters) = @_;

    print <<"___501_LOCAL_TRANSFER_CONFIG___";
Configuration:
----------------------------------------

Pipeline output directory:  '$parameters->{'output'}'
Target alignment directory: '$parameters->{'align_path'}'
Target tree directory:      '$parameters->{'tree_path'}'

___501_LOCAL_TRANSFER_CONFIG___

    # check if we use a remote server
    if ($parameters->{'host'})
    {
        # remote
        print <<"___481_WEB_TRANSFER_CONFIG___";
Transfer to a remote web server:
HOST:   $parameters->{'host'}
LOGIN:  $parameters->{'login'}
CONFIG: $parameters->{'config'}
___481_WEB_TRANSFER_CONFIG___
    }

    print "\nSuffix transfer:\n";
    foreach my $suffix (sort(keys(%g_pipeline_suffix_to_transfer)))
    {
        printf("%- 24s: %s\n", $suffix, $g_pipeline_suffix_to_transfer{$suffix}->{'remote'});
    }

    print "\n";
    print "Options:\n";
    if ($parameters->{'stats_only'})
    {
        print "Only display statistics\n";
    }
    if ($parameters->{'complete_only'})
    {
        print "Only complete missing files\n";
    }
    if ($parameters->{'update_only'})
    {
        print "Only update files\n";
    }
    if ($parameters->{'filter_species'})
    {
        print "Filter species: " . join(', ', @{$parameters->{'filter_species'}}) . "\n";
    }

    print "\n----------------------------------------\n";
}


=pod

=head2 FilterSpeciesInFASTA

B<Description>: Filter sequence data of the given species from the given input
and return filtered result.

B<ArgsCount>: 2

=over 1

=item $input: (string) (R)

The original full content.

=item $species_list: (array ref) (R)

Array of species codes (5-letter strings) that should be filtered.

=back

B<Return>: (string)

Filtered content.

=cut

sub FilterSpeciesInFASTA
{
    my ($input, $species_list) = @_;

    my $species_regexp = '_(?:' . join('|', @$species_list) . ')';
    my $output = '';
    my $skip_sequence = 0;

    my @lines = split('\n', $input);
    
    LogDebug("Filtering FASTA...");
    do
    {
        my $line = shift(@lines);
        # new sequence line?
        if ('>' eq substr($line, 0, 1))
        {
            if ($line =~ m/$species_regexp/)
            {
                # skip until next sequence
                $skip_sequence = 1;
                $output .= "$line\n";
                LogDebug("-skipping sequence $line");
                $line = shift(@lines)
            }
            else
            {
                $skip_sequence = 0;
            }
        }

        if ($skip_sequence)
        {
            $output .= 'X' x length($line) . "\n";
        }
        else
        {
            $output .= "$line\n";
        }
    } while (@lines);
    LogDebug("...done filtering FASTA.");

    return $output;
}


=pod

=head2 FilterSpeciesInTrimalHTML

B<Description>: Filter sequence data of the given species from the given input
and return filtered result.

B<ArgsCount>: 2

=over 1

=item $input: (string) (R)

The original full content.

=item $species_list: (array ref) (R)

Array of species codes (5-letter strings) that should be filtered.

=back

B<Return>: (string)

Filtered content.

=cut

sub FilterSpeciesInTrimalHTML
{
    my ($input, $species_list) = @_;

    my $species_regexp = join('|', @$species_list);
    my $output = '';

    my @lines = split('\n', $input);
    
    LogDebug("Filtering Trimal HTML...");

    # get sequences used
    my %sequences;
    foreach my $line (@lines)
    {
        if ($line =~ m/<span class=sel>([^><]{20,32})<\/span>/)
        {
            my $html_accession = $1;
            my $accession = $1;
            # remove trailing spaces
            $accession =~ s/\s*$//;
            # remove species code
            $accession =~ s/_[A-Z]{0,5}$//;

            # make sure we got a valid accession
            if ($accession
                && ($accession =~ m/[\w.\-]{4}/)
                && !$sequences{$html_accession})
            {
                my $sequence = Greenphyl::Sequence->new(
                    GetDatabaseHandler(),
                    {'selectors' => {'accession' => ['LIKE', $accession . (length($accession) >= 13 ? '%' : '')]} },
                );

                $sequences{$html_accession} = $sequence;
            }
        }
    }
    
    # get sequences to filter
    my %filtered_sequences;
    while (my ($accession, $sequence) = each(%sequences))
    {
        if ($sequence && $sequence->species->code =~ m/$species_regexp/)
        {
            LogVerboseInfo("Will filter accession '$accession'");
            $filtered_sequences{$accession} = 1;
        }
    }
    my $accession_regexp = join('|', keys(%filtered_sequences));
    $accession_regexp =~ s/\./\\./;

    foreach my $line (@lines)
    {
        if ($line =~ m/>($accession_regexp)\s*<\/span>/)
        {
            LogDebug("  -filtering accession line '$1'");
            $line =~ s/(>(?:$accession_regexp)\s*<\/span>)(.*)/$1/;
            # replace residues
            my $residues = $2;
            $residues =~ s/<[^>]+>//g;
            $residues =~ s/\S/X/g;
            $line .= $residues;
        }

        $output .= "$line\n";
    }
    LogDebug("...done filtering Trimal HTML.");

    return $output;
}


=pod

=head2 TransferFileToLocalRepository

B<Description>: transfer a file from local server to a local repository.
"local repository" is just a local directory that will gather files spread
arround several other directories.

B<ArgsCount>: 4

=over 1

=item $parameters: (hash ref) (R)

Hash of parameters. Used keys:
-'stats_only': do not transfer/copy file.
-'copy': copy file instead of linking (default behaviour) unmodified files.
-'filter_species': array of species code to filter. Can be an empty array.

=item $local_file_path: (string) (R)

Path to the phylogeny pipeline output file.

=item $local_target_path: (string) (R)

Path to the local target file.

=item $species_filtering_func: (code ref) (R)

Reference to a function that can be used to filter species from file content.
Species code to filter should be provided through
$parameters->{'filter_species'}.

=back

B<Return>: (boolean)

True value (1) if the file was transfered, false (0) otherwise.

=cut

sub TransferFileToLocalRepository
{
    my ($parameters, $local_file_path, $local_target_path, $species_filtering_func) = @_;

    if (!$local_file_path)
    {
        confess LogError("Missing file name to transfert!");
    }
    
    if ($parameters->{'stats_only'})
    {
        push(@g_not_transfered_files, $local_file_path);
        return 0;
    }

    if (-z $local_file_path)
    {
        LogWarning("File '$local_file_path' is empty and has been skipped!");
        push(@g_not_transfered_files, $local_file_path);
        return 0;
    }
    elsif (-r $local_file_path)
    {
        # check if species should be filtered
        if (@{$parameters->{'filter_species'}} && defined($species_filtering_func))
        {
            my ($ifh, $ofh);
            if (!open($ifh, "<$local_file_path"))
            {
                LogWarning("Failed to open local file '$local_file_path' for copy:\n$!");
                push(@g_not_transfered_files, $local_file_path);
                return 0;
            }

            # remove existing file or link
            if (-e $local_target_path)
            {
                if (!unlink($local_target_path))
                {
                    confess LogError("Failed to remove file '$local_target_path':\n$!");
                }
            }

            if (!open($ofh, ">$local_target_path"))
            {
                LogWarning("Failed to open target local file '$local_target_path' for copy:\n$!");
                push(@g_not_transfered_files, $local_file_path);
                return 0;
            }

            my $input = join('', <$ifh>);

            my $output = $species_filtering_func->($input, $parameters->{'filter_species'});

            print {$ofh} $output;

            close($ifh);
            close($ofh);
            
            LogVerboseInfo("-'$local_file_path' copy ok");
        }
        else
        {
            my $command;

            # link file
            if ($parameters->{'copy'})
            {
                $command = "cp -fp $local_file_path $local_target_path";
            }
            else
            {
                $command = "ln -fs $local_file_path $local_target_path";
            }

            LogDebug("COMMAND: $command");
            if (system($command))
            {
                LogWarning("Failed to link file '$local_file_path' to '$local_target_path':\n$!");
                push(@g_not_transfered_files, $local_file_path);
                return 0;
            }
            else
            {
                LogVerboseInfo("-'$local_file_path' link ok");
                push(@g_transfered_files, $local_file_path);
                return 1;
            }
        }
    }
    else
    {
        LogWarning("File '$local_file_path' not found and skipped!");
        push(@g_not_transfered_files, $local_file_path);
        return 0;
    }
}


=pod

=head2 SyncLocalRepositoryAndWeb

B<Description>: synchronizes local repository and remote directories.

B<ArgsCount>: 1

=over 1

=item $parameters: (hash ref) (R)

Hash of parameters. Used keys:
-'stats_only': no synchronization will occure
-'rsync_parameters': parameters to use instead of default rsync parameters
  used: '-vzrLtO' (verbose, compress transfer, recursive, dereference, preserve
  times, omit directory times)

=back

B<Return>: (boolean)
True value if the remote server has been synchronized, false otherwise.

=cut

sub SyncLocalRepositoryAndWeb
{
    my ($parameters) = @_;

    if ($parameters->{'stats_only'})
    {
        return 0;
    }

    # prepare rsync parameters
    my $rsync_parameters = $parameters->{'rsync_parameters'} || '-vzrLtO ';

    # sync local directories and the remote ones
    my %synchronized_directories;
    foreach my $suffix_data (values(%g_pipeline_suffix_to_transfer))
    {
        if (!exists($synchronized_directories{$suffix_data->{'remote'}}))
        {
            $synchronized_directories{$suffix_data->{'remote'}} = 1;
            my $command = 'rsync ' . $rsync_parameters . ' --rsh="ssh -l ' . $parameters->{'login'} . ' ' . $IDENTITY_PARAMETER . '" ' . $suffix_data->{'local'} . '/ ' . $parameters->{'host'} . ':' . $suffix_data->{'remote'} . '/';
            LogDebug("COMMAND: $command");
            if (system($command))
            {
                confess LogError("Failed to synchronize web server!\n$!");
            }
        }
    }

    return 1;
}


=pod

=head2 ProcessFamilyTransfer

B<Description>: only gather statistics on transfered family files.

B<ArgsCount>: 2

=over 1

=item $parameters: (hash ref) (R)

Parameters.

=item $family: (Greenphyl::Family) (R)

Family object.

=back

B<Return>: (boolean)

True if the family has been processed successfully.

=cut

sub ProcessFamilyTransfer
{
    my ($parameters, $family) = @_;

    LogDebug("Transfering files for family $family");

    # make sure the family has been processed by the phylogeny pipeline
    my $family_output_directory = FindFamilyOutputDirectory($family, $parameters->{'output'});

    if (!$family_output_directory)
    {
        # not processed
        LogWarning("Output path not found for family $family! Skipped.");
        return 0;
    }

    # process each family file to transfer
    my $untransfered_file = 0;
    foreach my $suffix_to_transfer (keys(%g_pipeline_suffix_to_transfer))
    {
        my $local_file_path   = FindFamilyFilePath($family, $family_output_directory, $suffix_to_transfer);
        my $target_filename   = $family->accession . $suffix_to_transfer;
        my $local_target_path = "$g_pipeline_suffix_to_transfer{$suffix_to_transfer}->{'local'}/$target_filename";
        my $web_target_path   = "$g_pipeline_suffix_to_transfer{$suffix_to_transfer}->{'remote'}/$target_filename";
        my $species_filtering_func = $g_pipeline_suffix_to_transfer{$suffix_to_transfer}->{'species_filtering_func'};

        if ($local_file_path)
        {
            # check mode
            if ($parameters->{'transfer_only'})
            {
                # just transfer file
                if (!TransferFileToLocalRepository($parameters, $local_file_path, $local_target_path, $species_filtering_func))
                {
                    ++$untransfered_file;
                }
            }
            else
            {
                # stats, update or complete missing...

                # check if the file is already available
                if (-e $local_target_path)
                {
                    # the file is already available
                    if ($parameters->{'complete_only'})
                    {
                        LogDebug("File already present and skipped: '$local_target_path'");
                    }
                    else
                    {
                        # stats or update
                        # phylogeny pipeline file exists?
                        if ($local_file_path && -r $local_file_path)
                        {
                            my ($ldev, $lino, $lmode, $lnlink, $luid, $lgid, $lrdev, $lsize,
                                $latime, $lmtime, $lctime, $lblksize, $lblocks)
                                = stat($local_file_path);

                            my ($rdev, $rino, $rmode, $rnlink, $ruid, $rgid, $rrdev, $rsize,
                                $ratime, $rmtime, $rctime, $rblksize, $rblocks)
                                = stat($local_target_path);

                            # check if file needs to be updated
                            if (($lino != $rino)
                                && (($lsize != $rsize) || ($latime != $ratime)))
                            {
                                LogVerboseInfo("Not up-to-date: '$local_target_path'");

                                # update?
                                if ($parameters->{'update_only'}
                                    && (!TransferFileToLocalRepository($parameters, $local_file_path, $local_target_path)))
                                {
                                    ++$untransfered_file;
                                }
                                $g_not_utd_families{$family} = 1;
                            }
                            else
                            {
                                LogVerboseInfo("Up-to-date: '$local_target_path'");
                            }
                        }
                        else
                        {
                            # phylogeny pipeline file does not exist!
                            if ($local_file_path)
                            {
                                LogWarning("Local file '$local_file_path' for family $family can not be read!");
                            }
                            else
                            {
                                LogWarning("Missing local file with suffix '$suffix_to_transfer' for family $family!");
                            }
                            ++$untransfered_file;
                        }
                    }
                }
                else
                {
                    # the file does not exist
                    LogDebug("Missing on remote server: '$family$suffix_to_transfer'");
                    if ($parameters->{'stats_only'})
                    {
                        # stats only: keep track of the missing file
                        ++$untransfered_file;
                    }
                    elsif ($parameters->{'complete_only'})
                    {
                        # complete missing files: add missing file
                        if (!TransferFileToLocalRepository($parameters, $local_file_path, $local_target_path))
                        {
                            ++$untransfered_file;
                        }
                    }
                    # update: file does not exist, nothing to do, just skip
                }
            }
        }
        else
        {
            # file not available
            push(@g_missing_files, {'family' => $family, 'suffix' => $suffix_to_transfer,});
        }
    }

    if ($untransfered_file)
    {
        push(@g_partially_transfered_families, $family);
    }
    else
    {
        push(@g_transfered_families, $family);
    }

    return 1;
}




=pod

=head2 DumpFile

B<Description>: Dump the content of a file.

B<ArgsCount>: 1

=over 4

=item arg: $file_path (string) (R)

Full path to the file to dump.

=back

B<Return>: (string)

File content.

B<Example>:

    print DumpFile('/data/pipeline/output/1234/mafft_err.log');

=cut

sub DumpFile
{
    my ($file_path) = @_;

    my $content = '';

    # check arguments
    if (!$file_path)
    {
        confess "ERROR: DumpFile: no file specified!\n";
    }

    # display log
    my $fh;
    if (open($fh, $file_path))
    {
        my $filename = $file_path;
        $filename =~ s/.*\///;
        $content .= "$filename content:\n/---------\n|  ";
        $content .= join('|  ', <$fh>);
        $content .= "|\n\\---------\n\n";
    }
    else
    {
        $content .= "Failed to open log file '$file_path'!\n$!\n";
    }

    return $content;
}


=pod

=head2 CheckOutputDirectory

B<Description>: check the output directory.

B<ArgsCount>: 1

=over 4

=item arg: $output_directory (string) (R)

Full path to the output directory.

=back

B<Return>: (string)

Checked output directory with one terminating slash '/'.

=cut

sub CheckOutputDirectory
{
    my ($output_directory) = @_;

    # check arguments
    if (!$output_directory)
    {
        confess LogError("No directory to process provided!");
    }

    # make sure we got only one trailing slash
    $output_directory =~ s/\/+$/\//;

    if (!-e $output_directory)
    {
        confess LogError("Directory '$output_directory' not found!");
    }
    elsif (!-d $output_directory)
    {
        confess LogError("'$output_directory' is not a directory!");
    }
    elsif (!-r $output_directory)
    {
        confess LogError("Directory '$output_directory' is not readable!");
    }
    elsif (!-w $output_directory)
    {
        LogWarning("Directory '$output_directory' is not writeable!");
    }

    return $output_directory;
}


=pod

=head2 CheckMAFFTOutputs

B<Description>: check the outputs of MAFFT.

B<ArgsCount>: 2

=over 4

=item arg: $output_directory (string) (R)

Full path to the output directory.

=item arg: $family (Greenphyl::Family) (R)

Family.

=back

B<Return>: nothing

=cut

sub CheckMAFFTOutputs
{
    my ($output_directory, $family) = @_;

    # check MAFFT outputs
    if (!-e $output_directory . $family->accession . $MAFFT_FILE_EXTENSION)
    {
        confess("MAFFT output missing");
    }
    elsif (!-s $output_directory . $family->accession . $MAFFT_FILE_EXTENSION)
    {
        confess("MAFFT output empty");
    }
}


=pod

=head2 CheckMAFFTOutputErrors

B<Description>: check the outputs of MAFFT.

B<ArgsCount>: 2

=over 4

=item arg: $output_directory (string) (R)

Full path to the output directory.

=item arg: $family (Greenphyl::Family) (R)

Family.

=back

B<Return>: (string)

Check result message.

=cut

sub CheckMAFFTOutputErrors
{
    my ($output_directory, $family) = @_;
    my $error_tips = '';

    # check MAFFT outputs
    eval
    {
        CheckMAFFTOutputs($output_directory, $family);
    };
    
    if ($@)
    {
        # check if alignment is available
        if (!-s $output_directory . $family->accession . $INPUT_FILE_EXTENSION)
        {
            $error_tips .= "Input alignment not available!\n";
        }
        if (-e $output_directory . "mafft_err.log")
        {
            $error_tips .= "mafft_err.log:\n" . DumpFile($output_directory . "mafft_err.log");
        }
        else
        {
            $error_tips .= "mafft_err.log not available\n";
        }
        $error_tips .= "\nSequence(s) to align: " . `cat $output_directory$family$INPUT_FILE_EXTENSION |grep '>' |wc -l` . "\n\n";
        $error_tips .= "\nAvailable files:\n----------\n" . `ls -al $output_directory 2>&1` . "----------\n\n";
        confess $@;
    };

    return $error_tips;
}


=pod

=head2 CheckTrimalOutputs

B<Description>: check the outputs of Trimal.

B<ArgsCount>: 2

=over 4

=item arg: $output_directory (string) (R)

Full path to the output directory.

=item arg: $family (Greenphyl::Family) (R)

Family.

=back

B<Return>: nothing

=cut

sub CheckTrimalOutputs
{
    my ($output_directory, $family) = @_;

    # check Trimal outputs
    if (!-e $output_directory . $family->accession . $FILTERING_FILE_EXTENSION . $FASTA_FILE_EXTENSION)
    {
        confess("Trimal output missing");
    }
    elsif (!-s $output_directory . $family->accession . $FILTERING_FILE_EXTENSION . $FASTA_FILE_EXTENSION)
    {
        confess("Trimal output empty");
    }
}


=pod

=head2 CheckTrimalOutputErrors

B<Description>: check the outputs of Trimal.

B<ArgsCount>: 2

=over 4

=item arg: $output_directory (string) (R)

Full path to the output directory.

=item arg: $family (Greenphyl::Family) (R)

Family.

=back

B<Return>: (string)

Check result message.

=cut

sub CheckTrimalOutputErrors
{
    my ($output_directory, $family) = @_;
    my $error_tips = '';

    CheckTrimalOutputs($output_directory, $family);

    return $error_tips;
}


=pod

=head2 CheckPhyMLOutputs

B<Description>: check the outputs of PhyML.

B<ArgsCount>: 2

=over 4

=item arg: $output_directory (string) (R)

Full path to the output directory.

=item arg: $family (Greenphyl::Family) (R)

Family.

=back

B<Return>: nothing

=cut

sub CheckPhyMLOutputs
{
    my ($output_directory, $family) = @_;

    # check PhyML outputs
    if (!-e $output_directory . $family->accession . $PHYLOGENY_FILE_SUFFIX)
    {
        confess("PhyML output missing");
    }
    elsif (!-s $output_directory . $family->accession . $PHYLOGENY_FILE_SUFFIX)
    {
        confess("PhyML output empty");
    }
}


=pod

=head2 CheckPhyMLOutputErrors

B<Description>: check the outputs of PhyML.

B<ArgsCount>: 2

=over 4

=item arg: $output_directory (string) (R)

Full path to the output directory.

=item arg: $family (Greenphyl::Family) (R)

Family.

=back

B<Return>: (string)

Check result message.

=cut

sub CheckPhyMLOutputErrors
{
    my ($output_directory, $family) = @_;
    my $error_tips = '';

    # check PhyML outputs
    eval
    {
        CheckPhyMLOutputs($output_directory, $family);
    };
    
    if ($@)
    {
        # get SGE job ID
        if (-e $output_directory . "phyml_err.log")
        {
            $error_tips .= "PhyML SGE job ID:\n" . DumpFile($output_directory . "phyml_err.log");
        }
        else
        {
            $error_tips .= "PhyML SGE job ID (phyml_err.log) not available\n";
        }
        # check logs
        if (-e $output_directory . "phyml.log")
        {
            $error_tips .= "phyml.log:\n" . DumpFile($output_directory . "phyml.log");
        }
        else
        {
            $error_tips .= "phyml.log not available\n";
        }
        
        confess $@;
    };

    return $error_tips;
}


=pod

=head2 CheckRAPOutputs

B<Description>: check the outputs of RAPGreen.

B<ArgsCount>: 2

=over 4

=item arg: $output_directory (string) (R)

Full path to the output directory.

=item arg: $family (Greenphyl::Family) (R)

Family.

=back

B<Return>: nothing

=cut

sub CheckRAPOutputs
{
    my ($output_directory, $family) = @_;

    # check RAP outputs
    if (!-e $output_directory . $family->accession . $ROOTED_TREE_FILE_SUFFIX)
    {
        confess("RAP output missing");
    }
    elsif (!-s $output_directory . $family->accession . $ROOTED_TREE_FILE_SUFFIX)
    {
        confess("RAP output empty");
    }

}


=pod

=head2 CheckRAPOutputErrors

B<Description>: check the outputs of RAPGreen.

B<ArgsCount>: 2

=over 4

=item arg: $output_directory (string) (R)

Full path to the output directory.

=item arg: $family (Greenphyl::Family) (R)

Family.

=back

B<Return>: (string)

Check result message.

=cut

sub CheckRAPOutputErrors
{
    my ($output_directory, $family) = @_;
    my $error_tips = '';

    CheckRAPOutputs($output_directory, $family);

    return $error_tips;
}


=pod

=head2 DisplayFamilyErrorDetails

B<Description>: Process the directory containing all the output files of a
family processed by the pipeline and give details about the error that occured.

B<ArgsCount>: 2

=over 4

=item arg: $output_directory (string) (R)

Full path to the output directory.

=item arg: $family (Greenphyl::Family) (R)

Family.

=back

B<Return>: nothing

B<Example>:

    DisplayFamilyErrorDetails('/data/pipeline/output', '1234');

=cut

sub DisplayFamilyErrorDetails
{
    my ($parameters, $family) = @_;

    # make sure the family has been processed by the phylogeny pipeline
    my $family_output_directory = FindFamilyOutputDirectory($family, $parameters->{'output'}) . '/';

    print "Error details for family $family:\n";

    my $error_tips = '';
    
    eval
    {
        $error_tips .= CheckMAFFTOutputErrors($family_output_directory, $family);
        $error_tips .= CheckTrimalOutputErrors($family_output_directory, $family);
        $error_tips .= CheckPhyMLOutputErrors($family_output_directory, $family);
        $error_tips .= CheckRAPOutputErrors($family_output_directory, $family);

        #+FIXME: check HMM and SDI-RIO

        # add family to the list of OK families
        print "No error found!\n";
    };
    
    if ($@)
    {
        my $error = $@;
        # remove debug info from error message
        $error =~ s/ at .* line \d+.*$//s;
        # display error found
        print "-$error\n\n";

        # display log
        my $family_log_filepath = $parameters->{'family_log'} . '/' . $family->accession . $LOG_EXTENSION;
        if ((-d $parameters->{'family_log'}) && (-e $family_log_filepath))
        {
            print DumpFile($family_log_filepath);
        }
        else
        {
            print "WARNING: Family log file '$family_log_filepath' not available!\n";
        }

        if ($error_tips)
        {
            print "Other info:\n$error_tips\n";
        }
    };

    return;
}



=pod

=head2 ProcessFamilyOutput

B<Description>: Process the directory containing all the output files of a
family processed by the pipeline.

B<ArgsCount>: 1

=over 4

=item arg: $output_directory (string) (R)

Full path to the family output directory.

=back

B<Return>: nothing

B<Example>:

    ProcessFamilyOutput({}, $family);

=cut

sub ProcessFamilyOutput
{
    my ($parameters, $family) = @_;

    # make sure the family has been processed by the phylogeny pipeline
    my $family_output_directory = (FindFamilyOutputDirectory($family, $parameters->{'output'}) || '.') . '/';
    LogDebug("Family ($family) output directory: $family_output_directory");

    eval
    {
        if (!$family_output_directory)
        {
            # not processed
            confess "Output path not found for family $family!";
        }
        elsif (-e $family_output_directory || !$parameters->{'ignore_missing'})
        {
            $family_output_directory = CheckOutputDirectory($family_output_directory);
        }
    };
    
    if (!-e $family_output_directory && $parameters->{'ignore_missing'})
    {
        LogVerboseInfo("Family output directory '$family_output_directory' missing and ignored");
        return;
    }
    
    if ($@)
    {
        cluck "WARNING: ProcessFamilyOutput: unable to process directory" . ($family_output_directory ? " '$family_output_directory'" : '') . "!\n" . $@ . "\n";
        return;
    }

    if ($family->accession)
    {
        LogInfo("\rProcessing family $family: ");

        eval
        {
            CheckMAFFTOutputs($family_output_directory, $family);
            CheckTrimalOutputs($family_output_directory, $family);
            CheckPhyMLOutputs($family_output_directory, $family);
            CheckRAPOutputs($family_output_directory, $family);

            #+FIXME: check HMM and SDI-RIO

            # add family to the list of OK families
            push(@g_families_ok_list, $family);
            LogInfo("OK        ");
        };

        if ($@)
        {
            my $error = $@;
            # remove debug info from error message
            $error =~ s/ at .* line \d+.*$//s;

            # add family to the list of families with the same error type
            if (exists($g_family_errors{$error}))
            {
                push(@{$g_family_errors{$error}}, $family);
            }
            else
            {
                $g_family_errors{$error} = [$family];
            }
            # add family to the list of family with and error
            push(@g_families_error_list, $family);
            LogInfo("Error     ");
        }
    }
    else
    {
        LogError("ProcessFamilyOutput: family ID not found in given path ('$family_output_directory')");
    }

    return;
}




# Script options
#################

=pod

=head1 OPTIONS

    manage_pipeline_outputs.pl [-help | -man]
    manage_pipeline_outputs.pl [-show-config[-only]]
                    [<-remote-config <CONFIG_PATH> [-host <HOST> -login <LOGIN>]>
                     | <-align-path <ALIGN_PATH>> <-tree-path <TREE_PATH>>
                    ]
                    [-phylogeny-output-path <OUTPUT_PATH>]
                    [-complete-only | -update-only | -stats-only]
                    [-sync-only]
                    [-transfer]
                    [-copy]
                    [-filter-species=<CODE> -filter-species=<CODE> ...]
                    [-rsync <"-parameters">]
                    [-report-not-utd][-report-success]
                    [-report-partial][-report-unprocessed][-report-failures]
                    [-restrict-to <EXTENSION>]

ex.:
    manage_pipeline_outputs.pl -host gohelle.cirad.fr -login greenphyl \
                    -remote-config /apps/www/greenphyl/dev/local_lib/Greenphyl/Config.pm \
                    -phylogeny-output-path /work/guignon/pipeline \
                    -transfer
                    level=1 min_sequences=5 max_sequences=500 \
                    -stats-only -log -log-screenonly

=over 4

=item B<-help>:

Prints a brief help message and exits.

=item B<-show-config>:

Display file transfer configuration.

=item B<-show-config-only>:

Display file transfer configuration and do not process families.

=item B<-man>:

Prints the manual page and exits.

=item B<-host>: (string)

name of the web (host) server.
Default: use current local server

=item B<-login>: (string)

remote login to use on web server.
Default: empty

=item B<-remote-config>: (string)

path to the web config file (local_lib/Greenphyl/Config.pm).
Default: current config

=item B<-pipeline-output-dir>: (string)

Path to local pipeline outputs (directory containing the family output
directories).
Default: $Greenphyl::Config::TEMP_OUTPUT_PATH

=item B<-transfer>: (boolean)

Process to output file transfer for the web.

=item B<-copy>: (boolean)

Copy files instead of creating symbolic links.

=item B<-filter-species>: (string)

Specify the species code that should be filtered during transfer.
Several species can be specified by repeating the parameter.

=item B<-complete-only>: (boolean)

Only copy missing files.

=item B<-update-only>: (boolean)

Only copy updated files (different size or date).
Note: it does not copy missing files!

=item B<-stats-only>: (boolean)

Only display stats about transfered families (no transfer).

=item B<-sync-only>: (boolean)

Only synchronize remote server with local repository.

=item B<-rsync>: (string)

A quoted string that contains parameters to give to the rsync command instead
of the one used by default which are: '-vzrLtO' (verbose, compress transfer,
recursive, dereference, preserve times, omit directory times).
Ex.: -rsync '-vzrLtO --I --size-only --delete'
     -rsync '-vzrLtO --ignore-existing --log-file=gp_rsync.log'
     -rsync '-vzrLtO --I --size-only --list-only --log-file=gp_rsync.log'

=item B<-report-not-utd>: (boolean)

Report the list of families that are not up-to-date.

=item B<-report-success>: (boolean)

Report the list of families that are up-to-date.

=item B<-report-partial>: (boolean)

Report the list of families that have not all of their files being transfered.

=item B<-report-unprocessed>: (boolean)

Report the list of families that have been skipped.

=item B<-report-failures>: (boolean)

Report the list of files that couldn't be transfered.

=item B<-restrict-to>: (string)

List of file extension to work on (others are ignored).

=back

=cut


# CODE START
#############

# options processing
my ($man, $help, $debug) = (0, 0, undef);
my $remote_server         = '';
my $remote_login          = '';
my $remote_config         = '';
my $phylogeny_output_path = '';
my $transfer_files      = 0;
my $copy_files          = 0;
my $complete_only       = 0;
my $update_only         = 0;
my $sync_only           = 0;
my $stats_only          = 0;
my $report_success      = 0;
my $report_partial      = 0;
my $report_not_utd      = 0;
my $report_unprocessed  = 0;
my $report_failures     = 0;
my $show_config         = 0;
my $show_config_only    = 0;
my $align_path          = '';
my $tree_path           = '';
my $rsync_parameters;
my @filter_species      = ();
my @restrictions        = ();
my @list_errors         = ();
my $all_errors          = 0;
my $list_error_families = 0;
my @remove_errors       = ();
my @move_errors         = ();
my $new_dir             = '';
my $details             = 0;
my $detail_all          = 0;
my $family_log_path     = '';
my $ignore_missing      = 0;

# parse options and print usage if there is a syntax error.
GetOptions("help|?"             => \$help,
           "man"                => \$man,
           "debug:s"            => \$debug,
           "v|verbose"          => \$Greenphyl::Config::VERBOSE_MODE,
           "show-config"        => \$show_config,
           "show-config-only"   => \$show_config_only,
           "host=s"             => \$remote_server,
           "login=s"            => \$remote_login,
           "remote-config=s"    => \$remote_config,
           "o|phylogeny-output-path=s" => \$phylogeny_output_path,
           "transfer"           => \$transfer_files,
           "copy"               => \$copy_files,
           "complete-only"      => \$complete_only,
           "update-only"        => \$update_only,
           "sync-only"          => \$sync_only,
           "stats-only"         => \$stats_only,
           "report-success"     => \$report_success,
           "report-partial"     => \$report_partial,
           "report-not-utd"     => \$report_not_utd,
           "report-unprocessed" => \$report_unprocessed,
           "report-failures"    => \$report_failures,
           "filter-species=s"   => \@filter_species,
           "align-path=s"       => \$align_path,
           "tree-path=s"        => \$tree_path,
           "rsync=s"            => \$rsync_parameters,
           "restrict-to=s"      => \@restrictions,

           'list'               => \$list_error_families,
           'list-error=s'       => \@list_errors,
           'remove=s'           => \@remove_errors,
           'move=s'             => \@move_errors,
           'to=s'               => \$new_dir,
           'all-errors'         => \$all_errors,
           'details'            => \$details,
           'family-log=s'       => \$family_log_path,
           'ignore-missing'     => \$ignore_missing,

           @Greenphyl::Log::LOG_GETOPT, # skip log parameters
) or pod2usage(1);
if ($help) {pod2usage('-verbose' => 1, '-exitval' => 0);}
if ($man) {pod2usage('-verbose' => 2, '-exitval' => 0);}

# change debug mode if requested/forced
if (defined($debug))
{
    # just '-debug' ?
    if ($debug eq '')
    {
        $debug = 1;
    }
    $DEBUG = $debug;
}

# parameters check...
if (1 < ($stats_only + $complete_only + $update_only))
{
    cluck 'Invalid argument combination! You can only specify one of -stats-only, -complete-only or -update-only!';
    pod2usage(1);
}


$phylogeny_output_path ||= GP('TEMP_OUTPUT_PATH');
# remove trailing slash
$phylogeny_output_path =~ s/\/+$//;
CheckOutputDirectory($phylogeny_output_path);
LogDebug("Processing phylogeny pipeline output path '$phylogeny_output_path'");
$family_log_path ||= $phylogeny_output_path;
$family_log_path =~ s/\/+$//;

if ($remote_server)
{
    if (!$remote_login)
    {
        confess LogError("Missing login parameter for specified remote host!");
    }
    elsif (!$remote_config)
    {
        confess LogError("Missing config path parameter for specified remote host!");
    }
}

# check move option
if (@move_errors)
{
    if (!$new_dir)
    {
        # no target directory specified
        pod2usage(1);
    }
    # remove trailing slash
    $new_dir =~ s/\/+$//;
    
    if ((!-e $new_dir) && (!mkdir($new_dir, 0775)))
    {
        # target directory missing and unable to create it
        confess "ERROR: Failed to create target directory '$new_dir'!\n$!\n";
    }
    # check dir
    if (!-d $new_dir)
    {
        confess "ERROR: Specified target directory '$new_dir' is not a directory!\n";
    }
    elsif (!-x $new_dir)
    {
        confess "ERROR: Specified target directory '$new_dir' can not be opened!\n";
    }
    elsif (!-w $new_dir)
    {
        confess "ERROR: Specified target directory '$new_dir' is write-protected!\n";
    }
}


$align_path ||= GP('MULTI_ALIGN_PATH');
$tree_path  ||= GP('TREES_PATH');

# check given species codes
my @invalid_species_codes;
foreach my $species_code (@filter_species)
{
    if (!Greenphyl::Species->new(
            GetDatabaseHandler(),
            {'selectors' => {'code' => $species_code} },
        )
    )
    {
        push(@invalid_species_codes, $species_code);
    }
}

if (@invalid_species_codes)
{
    confess LogError("Invalid species codes: " . join(', ', @invalid_species_codes));
}


if (@restrictions)
{
    # restrict to specified extensions/suffices
    my @ok_restriction;
    my %restrictions = map {$_ => 1,} @restrictions;
    my $suffix_to_transfer;
    foreach $suffix_to_transfer (keys(%g_pipeline_suffix_to_transfer))
    {
        if (!exists($restrictions{$suffix_to_transfer}))
        {
            delete($g_pipeline_suffix_to_transfer{$suffix_to_transfer});
        }
        if (exists($g_pipeline_suffix_to_transfer{$suffix_to_transfer}))
        {
            push(@ok_restriction, $suffix_to_transfer);
        }
    }
    foreach $suffix_to_transfer (keys(%g_meme_file_ext_to_transfer))
    {
        if (!exists($restrictions{$suffix_to_transfer}))
        {
            delete($g_meme_file_ext_to_transfer{$suffix_to_transfer});
        }
        if (exists($g_meme_file_ext_to_transfer{$suffix_to_transfer}))
        {
            push(@ok_restriction, $suffix_to_transfer);
        }
    }
    if (!@ok_restriction)
    {
        confess LogError("Failed to restrict to transfer to the specified extensions: " . join(', ', @restrictions));
    }
    LogInfo("Transfers will be restricted to: " . join(', ', @ok_restriction));
}


my $parameters = {
    'config'         => $remote_config,
    'host'           => $remote_server,
    'login'          => $remote_login,
    'align_path'     => $align_path,
    'tree_path'      => $tree_path,
    'output'         => $phylogeny_output_path,
    'copy'           => $copy_files,
    'stats_only'     => $stats_only,
    'complete_only'  => $complete_only,
    'update_only'    => $update_only,
    'rsync_parameters' => $rsync_parameters,
    'filter_species' => \@filter_species,
    'family_log'     => $family_log_path,
    'ignore_missing' => $ignore_missing,
};

eval
{
    # load web server config if available
    LoadWebServerConfig($parameters);

    # resolve remote path
    ExpandRemoteTransferPath($parameters);

    # update alignment and tree path if specified
    InitLocalTargetPath($parameters);
    
    # do we have extra-process?
    if (!$stats_only && !$complete_only && !$update_only)
    {
        $parameters->{'transfer_only'} = 1;
    }

    if ($show_config || $show_config_only)
    {
        PrintConfig($parameters);
    }
    
    my $processed_family_count = 0;
    if (!$show_config_only)
    {
        if (!$sync_only)
        {
            $processed_family_count = LoopOnFamilies(
                sub
                {
                    my $family = shift();
                    LogVerboseInfo("Working on family $family");
                    ProcessFamilyOutput($parameters, $family);
                    if ($details)
                    {
                        DisplayFamilyErrorDetails($parameters, $family);
                    }
                    # transfer pipeline results
                    if ($transfer_files)
                    {
                        if (!ProcessFamilyTransfer($parameters, $family))
                        {
                            # family not processed by the pipeline = skipped
                            LogWarning("Family '$family' not processed and skipped!");
                            push(@g_unprocessed_families, $family);
                        }
                    }
                }
            );
        }
        
        # sync web server?
        if ($parameters->{'host'})
        {
            SyncLocalRepositoryAndWeb($parameters);
        }
    }

    LogInfo("\n"
        . "Total processed families:                 " . scalar($processed_family_count) . "\n"
        . "Total up-to-date families:                " . (scalar($processed_family_count) - scalar(keys(%g_not_utd_families))) . "\n"
        . "Total not up-to-date families:            " . scalar(keys(%g_not_utd_families)) . "\n"
        . "Total successfully transferable families: " . scalar(@g_transfered_families) . "\n"
        . "Total partially transferable families:    " . scalar(@g_partially_transfered_families) . "\n"
        . "Total unprocessed families:               " . scalar(@g_unprocessed_families) . "\n"
        . "Total transfered files:                   " . scalar(@g_transfered_files) . "\n"
        . "Total failed transfers:                   " . scalar(@g_not_transfered_files) . "\n"
        . "Total missing files:                      " . scalar(@g_missing_files) . "\n"
        . "\n"
    );

    if ($report_not_utd)
    {
        LogInfo("Not up-to-date families:\n"
            . join("\n", keys(%g_not_utd_families))
            . "\n"
        );
    }

    if ($report_success)
    {
        LogInfo("Successfully transfered families:\n"
            . join("\n", @g_transfered_families)
            ."\n"
        );
    }

    if ($report_partial)
    {
        LogInfo("Partially transfered families:\n"
            . join("\n", @g_partially_transfered_families)
            . "\n"
        );
    }

    if ($report_unprocessed)
    {
        LogInfo("Unprocessed families:\n"
            . join("\n", @g_unprocessed_families)
            . "\n"
        );
    }

    if ($report_failures)
    {
        LogInfo("File transfer failures:\n"
            . join("\n", @g_not_transfered_files)
            . "\n"
        );
    }
    
    # Pipeline output stats
    # print summary
    my @family_error_list = sort {$a->accession cmp $b->accession} @g_families_error_list;
    LogInfo("\n\n"
            . "Pipeline output summary:\n"
            . "Processed families: $processed_family_count\n"
            . "Families OK: " . scalar(@g_families_ok_list) . "\n"
            . "Families with error: " . scalar(@family_error_list) . "\n"
    );

    if ($processed_family_count)
    {
        LogInfo("Processed families OK ratio: " . int(0.5 + 100.*scalar(@g_families_ok_list)/$processed_family_count) . "%");
    }
    if ($processed_family_count != (scalar(@family_error_list) + scalar(@g_families_ok_list)))
    {
        LogWarning("Check family counts! Some families were not checked correctly!");
    }
    LogInfo("\n");

    LogInfo("List of errors:");
    foreach my $error_type (keys(%g_family_errors))
    {
        LogInfo("-'$error_type': " . scalar(@{$g_family_errors{$error_type}}) . " family/-ies");
    }
    LogInfo("\n");

    # list families with error if requested
    if ($list_error_families)
    {
        LogInfo("List of families with error: " . join(', ', @family_error_list));
        LogInfo("\n");
    }

    # display lists of families with error grouped by error type
    if ($all_errors)
    {
        @list_errors = keys(%g_family_errors);
    }

    foreach my $list_error (@list_errors)
    {
        if ($list_error
            && $g_family_errors{$list_error}
            && @{$g_family_errors{$list_error}})
        {
            LogInfo("List of families with error '$list_error': " . join(', ', @{$g_family_errors{$list_error}}));
        }
    }

    # move families
    foreach my $move_error (@move_errors)
    {
        if ($move_error
            && $g_family_errors{$move_error}
            && @{$g_family_errors{$move_error}})
        {
            foreach my $family (@{$g_family_errors{$move_error}})
            {
                my $family_output_directory;
                if ($family_output_directory = FindFamilyOutputDirectory($family, $parameters->{'output'}))
                {
                    # move family directory
                    if (system("mv $family_output_directory $new_dir/" . $family->accession))
                    {
                        LogWarning("WARNING: Failed to move family $family to '$new_dir/" . $family->accession . "'!\n");
                    }
                    # move log
                    if (system("mv $family_log_path/" . $family->accession . "$LOG_EXTENSION $new_dir/"))
                    {
                        LogWarning("WARNING: Failed to move family $family log to '$new_dir'!\n");
                    }
                }
            }
        }
    }

    # remove selected family results
    foreach my $remove_error (@remove_errors)
    {
        if ($remove_error
            && $g_family_errors{$remove_error}
            && @{$g_family_errors{$remove_error}})
        {
            foreach my $family (@{$g_family_errors{$remove_error}})
            {
                my $family_output_directory;
                if ($family_output_directory = FindFamilyOutputDirectory($family, $parameters->{'output'}))
                {
                    # remove family directory
                    if (system("rm -rf $family_output_directory"))
                    {
                        LogWarning("WARNING: Failed to remove family $family results!\n");
                    }
                    # remove log
                    if (system("rm -f $family_log_path/" . $family->accession . "$LOG_EXTENSION"))
                    {
                        LogWarning("WARNING: Failed to remove family $family log!\n");
                    }
                }
            }
        }
    }

    
};

HandleErrors();

exit(0);

# CODE END
###########


=pod

=head1 AUTHORS

Valentin GUIGNON (Bioversity-France), v.guignon@cgiar.org

Mathieu ROUARD (Bioversity-France), m.rouard@cgiar.org

Matthieu CONTE (Syngenta)

Sebastien BRIOIS (Syngenta)

=head1 VERSION

Version 3.0.0

Date 08/01/2014

=head1 SEE ALSO

GreenPhyl documentation.

=cut
