#!/usr/bin/perl

=pod

=head1 NAME

update_go_cache.pl - Updates GreenPhyl GO cache tables.

=head1 SYNOPSIS

    update_go_cache.pl

=head1 REQUIRES

Perl5

=head1 DESCRIPTION

Clears and updates GreenPhyl GO cache tables:
-go_sequences_cache
-families_go_cache

=cut

use strict;
use warnings;

use FindBin;
use lib "$FindBin::Bin/../../lib";
use lib "$FindBin::Bin/../../local_lib";
use Carp qw (cluck confess croak);


use Getopt::Long;
use Pod::Usage;

use Greenphyl;
use Greenphyl::Log('nolog' => 1,);




# Script global constants
##########################

=pod

=head1 CONSTANTS

B<$REV_STRING>: (string)

Current revision (auto-set by GIT).

B<$DEBUG>: (boolean)

When set to true, it enables debug mode.

=cut

# SVN management part
my $REV_STRING = '$id$';

our $DEBUG = 0;




# Script global variables
##########################

=pod

=head1 VARIABLES

B<$g_dbh>: (DBI::db)

Global database handler.

=cut

my $g_dbh = GetDatabaseHandler();




# Script global functions
##########################

=pod

=head1 FUNCTIONS

=head2 ClearGOCache

B<Description>: Clears GO cache tables for sequences and families
(families_go_cache and go_sequences_cache).

B<ArgsCount>: 0

B<Return>: nothing

=cut

sub ClearGOCache
{
    LogInfo("Clear cache tables...");

    # clear go family cache
    my $sql_query = "DELETE FROM families_go_cache;";
    $g_dbh->do($sql_query)
        or confess LogError("Failed to clear families_go_cache table:\n" . $g_dbh->errstr);

    # clear go sequence cache
    $sql_query = "DELETE FROM go_sequences_cache;";
    $g_dbh->do($sql_query)
        or confess LogError("Failed to clear go_sequences_cache table:\n" . $g_dbh->errstr);
}


=pod

=head2 UpdateGOSequencesCache

B<Description>: update GO-Sequences cache table (go_sequences_cache).

B<ArgsCount>: 0

B<Return>: nothing

=cut

sub UpdateGOSequencesCache
{
    LogInfo("Updating go_sequences_cache...");

    LogVerboseInfo("-IPR2GO");
    my $sql_query = '
        INSERT INTO go_sequences_cache (go_id, sequence_id, ipr_id)
            SELECT gi.go_id, isq.sequence_id, isq.ipr_id
            FROM ipr_sequences isq
                JOIN go_ipr gi USING (ipr_id)
        ON DUPLICATE KEY UPDATE ipr_id = isq.ipr_id;
    ';
    $g_dbh->do($sql_query)
        or confess LogError("Failed to update go_sequences_cache table for IPR2GO:\n" . $g_dbh->errstr);

    LogVerboseInfo("-UniProt2GO");
    $sql_query = '
        INSERT INTO go_sequences_cache (go_id, sequence_id, uniprot_id)
        SELECT gu.go_id, ds.sequence_id, ds.dbxref_id
        FROM dbxref_sequences ds
            JOIN go_uniprot gu USING (dbxref_id)
        ON DUPLICATE KEY UPDATE uniprot_id = ds.dbxref_id;
    ';
    $g_dbh->do($sql_query)
        or confess LogError("Failed to update go_sequences_cache table for UniProt2GO:\n" . $g_dbh->errstr);

    LogVerboseInfo("-GO2EC");
    $sql_query = '
        UPDATE go_sequences_cache gsc, ec_go eg
        SET gsc.ec_id = eg.dbxref_id WHERE eg.go_id = gsc.go_id;
    ';
    $g_dbh->do($sql_query)
        or confess LogError("Failed to update go_sequences_cache table for GO2EC:\n" . $g_dbh->errstr);

    if (!$DEBUG)
    {SaveCurrentTransaction($g_dbh);}

    LogInfo("... done updating go_sequences_cache.");
}


=head2 UpdateFamiliesGOCache

B<Description>: update GO-Families cache table (families_go_cache).

B<ArgsCount>: 0

B<Return>: nothing

=cut

sub UpdateFamiliesGOCache
{
    LogInfo("Updating families_go_cache...");

    my $sql_query = '
        INSERT INTO families_go_cache (go_id, family_id, percent)
        SELECT gsc.go_id, f.id, ROUND(100*COUNT(gsc.sequence_id)/f.sequence_count)
        FROM go_sequences_cache gsc
            JOIN families_sequences fs USING (sequence_id)
            JOIN families f ON (f.id = fs.family_id)
        GROUP BY gsc.go_id, f.id;
    ';
    $g_dbh->do($sql_query)
        or confess LogError("Failed to update families_go_cache table for IPR2GO:\n" . $g_dbh->errstr);

    if (!$DEBUG)
    {SaveCurrentTransaction($g_dbh);}

    LogInfo("... done updating families_go_cache.");
}




# Script options
#################

=pod

=head1 OPTIONS

    update_go_cache.pl [-help | -man]
    update_go_cache.pl [-debug]

=over 4

=item B<-help>:

Prints a brief help message and exits.

=item B<-man>:

Prints the manual page and exits.

=item B<-debug> (integer):

Executes the script in debug mode. If an integer value is specified, it will
be the debug level. If "-debug" option was used without specifying a debug
level, level 1 is assumed.
Default: 0 (not in debug mode).

=back

=cut


# CODE START
#############

# for regression tests
if ($ENV{'TESTING'} && $ENV{'TESTING_NO_RUN'})
{return 1;}

# options processing
my ($man, $help, $debug) = (0, 0, undef);
my $clear_cache_only;
# parse options and print usage if there is a syntax error.
GetOptions('help|?'  => \$help,
           'man'     => \$man,
           'debug:s' => \$debug,
           'clear'   => \$clear_cache_only,
           @Greenphyl::Log::LOG_GETOPT, # skip log parameters
) or pod2usage(1);
if ($help) {pod2usage('-verbose' => 1, '-exitval' => 0);}
if ($man) {pod2usage('-verbose' => 2, '-exitval' => 0);}

# change debug mode if requested/forced
if (defined($debug))
{
    # just '-debug' ?
    if ($debug eq '')
    {
        $debug = 1;
    }
    $DEBUG = $debug;
}

eval
{
    LogInfo("Update GO...");

    # start SQL transaction
    if ($g_dbh->{'AutoCommit'})
    {
        $g_dbh->begin_work() or croak $g_dbh->errstr;
    }
    LogDebug("Running in DEBUG mode, no database commit will be issued!");
    
    ClearGOCache();

    if (!$clear_cache_only)
    {
        UpdateGOSequencesCache();
        UpdateFamiliesGOCache();
    }

};

# catch
HandleErrors();

exit(0);

# CODE END
###########


=pod

=head1 DIAGNOSTICS

=head1 AUTHORS

Mathieu ROUARD (Bioversity), m.rouard@cgiar.org

Valentin GUIGNON (Bioversity), v.guignon@cgiar.org

Matthieu CONTE (Syngenta)

Sebastien BRIOIS (Syngenta)

=head1 VERSION

Version 1.0.0

Date 25/02/2013

=head1 SEE ALSO

GreenPhyl documentation.

=cut
