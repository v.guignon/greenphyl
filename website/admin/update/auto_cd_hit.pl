#!/usr/bin/perl

=pod

=head1 NAME

auto_cd_hit.pl - Run CD-HIT and automatically find the best 10 sequences

=head1 SYNOPSIS

    auto_cd_hit.pl -i 500.fa -o 500r.fa

=head1 REQUIRES

Perl5, cd-hit program

=head1 DESCRIPTION

This script runs CD HIT as many time as necessary using various parameters to
automatically find the best 10 (or whatever "-size" parameter has been set to)
candidate sequences that can represent the input dataset. In some cases, there
might be less than 10 sequences but never more than 10.

If the input dataset has less than 10 sequences, it will be copied to the output
dataset.

If the input dataset has more than 10 sequences but less than or exactly 3 times
10 sequences, cluster found by CD HIT are considered valid if they hold more
than 2 sequences or if they hold a sequence from a "trusted" species.

If the input dataset has more than 3 times 10 sequences, cluster found by CD HIT
are considered valid if they hold more than 3 sequences or if they hold at least
a sequence from a "trusted" species and any other sequence.

Selected clusters are taken from clusters considered as valid using the rules
above. If there are not enought valid cluster to have 10 representative
sequences, invalid cluster will be also considered.

The algorithm sort selected clusters by number of sequences plus the number of
sequences that belong to "trusted" species in descending order. Only one
representative sequence given by CD HIT from each one of the 10 best selected
clusters will be kept for the output dataset.

Note: CD HIT output files will not be removed automatically (cdhitXXX.fa,
 cdhitXXX.clstr, cdhitXXX.bak.clstr).

=cut

use strict;
use warnings;

use FindBin;
use lib "$FindBin::Bin/../../lib";
use lib "$FindBin::Bin/../../local_lib";

use Carp qw (cluck confess croak);

use Getopt::Long;
use Pod::Usage;

use Greenphyl;
use Greenphyl::Tools::Sequences;


# Script global constants
##########################

=pod

=head1 CONSTANTS

B<$DEBUG>: (boolean)

When set to true, it enables debug mode.

B<$OUTPUT_FILE_PREFIX>: (string)

Default output file prefix used if no output file name is given.

B<$CDHIT_STATS_OUTPUT_FILE_SUFFIX>: (string)

Suffix used by CD HIT for statistics file.

B<$DEFAULT_OUTPUT_SUFFIX>: (string)

Suffix for default output file names.

B<$EXPECTED_DATASET_SIZE>: (integer)

Number of wanted sequences in output dataset.

B<$DATASET_MIN_LENGTH>: (integer)

Minimal number of sequences in output dataset.

B<@TRUSTED_SPECIES_CODES>: (array of string)

Array of trusted species code. Sequences from these species will have a higher
weight (count as 2).

=cut

our $DEBUG = 0;

my $OUTPUT_FILE_PREFIX = 'cdhit';
my $CDHIT_STATS_OUTPUT_FILE_SUFFIX = '.clstr';
my $DEFAULT_OUTPUT_SUFFIX = '_representative.fa';
my $EXPECTED_DATASET_SIZE = 10;
my $DATASET_MIN_LENGTH = 3;
my @TRUSTED_SPECIES_CODES = ('ARATH', 'ORYSA', 'VITVI', 'MUSAC');




# Script global variables
##########################

=pod

=head1 VARIABLES

B<$g_expected_dataset_size>: (integer)

Expected output dataset number of sequences.
Default: $EXPECTED_DATASET_SIZE

B<$g_dataset_min_length>: (integer)

Minimum  number of sequences for a dataset to be valid.
Default: $DATASET_MIN_LENGTH

=cut

my $g_expected_dataset_size = $EXPECTED_DATASET_SIZE;
my $g_dataset_min_length    = $DATASET_MIN_LENGTH;




# Script global functions
##########################

=pod

=head1 FUNCTIONS

=head2 GetClusters

B<Description>: Returns a hash of cluster found by CD HIT.

B<ArgsCount>: 2

=over 4

=item $stats_output_filename: (string) (R)

name of CD HIT statistics output file.

=item $trusted_sequences: (hash ref) (R)

a hash of trusted sequence name (as hash keys with a non-false value).

=back

B<Return>: (hash ref)

Keys are cluster names. Each value is a hash containing:
-'name': cluster name;
-'sequences': array of sequence names;
-'trusted_count': number of sequence belonging to trusted species;
-'representative': name of the sequence selected for the cluster.

=cut

sub GetClusters
{
    my ($stats_output_filename, $trusted_sequences) = @_;

    my ($output_fh, %clusters);
    my $current_cluster = '';

    open($output_fh, $stats_output_filename)
        or confess Throw('error' => "Failed to open CD HIT statistics output file '$stats_output_filename'! $!");
    while (my $line = <$output_fh>)
    {
        if ($line =~ m/^>(.*)/)
        {
            # got a new cluster definition
            $current_cluster = $1;
        }
        elsif ($line =~ m/\d+\s+\d+aa,\s+>(.*)\.\.\.\s+(.)/)
        {
            # got a sequence, store it in cluster structure
            $clusters{$current_cluster} ||=
            {
                'name' => $current_cluster,
                'sequences' => [],
                'trusted_count' => 0,
                'representative' => '',
            };
            
            push(@{$clusters{$current_cluster}->{'sequences'}}, $1);
            # keep track of trusted sequences
            if ($trusted_sequences->{$1})
            {
                $clusters{$current_cluster}->{'trusted_count'}++;
            }
            # keep track of selected sequences
            if ($2 eq '*')
            {
                $clusters{$current_cluster}->{'representative'} = $1;
            }
        }
        elsif ($line)
        {
            cluck "WARNING: failed to parse line:\n$line\n";
        }
    }
    close($output_fh);
    PrintDebug('Found ' . scalar(keys(%clusters)) . ' cluster(s)');
    
    return \%clusters;
}


=pod

=head2 GetValidClusters

B<Description>: Returns a hash of valid clusters.

B<ArgsCount>: 2

=over 4

=item $clusters: (hash ref) (R)

a hash of the same type than the one returned by GetClusters.

=item $total_sequence_count: (integer) (R)

Total number of sequences.

=back

B<Return>: (hash ref, hash ref)

First hash correspond to valid clusters, second hash to dropped clusters that
do not meet requirements.
Keys are cluster names. Each value is a hash containing:
-'name': cluster name;
-'sequences': array of sequence names;
-'trusted_count': number of sequence belonging to trusted species;
-'representative': name of the sequence selected for the cluster.

=cut

sub GetValidClusters
{
    my ($clusters, $total_sequence_count) = @_;
    my $valid_clusters = {};
    my $dropped_clusters = {};

    # minimum of 2 sequences (nb.: trusted sequences count as double)
    my $minimal_sequence_count = 2;
    # check initial number of sequences
    # be more stringent on large dataset: 3 sequences at least to have a valid cluster
    if ($total_sequence_count > 3*$g_expected_dataset_size)
    {
        # large initial datasets: minimum of 3 sequences
        $minimal_sequence_count = 3;
    }

    # loop on clusters
    foreach my $cluster (values(%$clusters))
    {
        # check minimum number of sequences (nb. trusted sequences count qs double)
        if ($minimal_sequence_count <= scalar(@{$cluster->{'sequences'}}) + $cluster->{'trusted_count'})
        {
            # PrintDebug("Valid cluster: " . $cluster->{'name'} . " (" . scalar(@{$cluster->{'sequences'}}) . " sequence(s) + " . $cluster->{'trusted_count'} . " bonus vs $minimal_sequence_count)");
            $valid_clusters->{$cluster->{'name'}} = $cluster;
        }
        else
        {
            $dropped_clusters->{$cluster->{'name'}} = $cluster;
        }
    }
    PrintDebug('Found ' . scalar(keys(%$valid_clusters)) . ' valid cluster(s)');

    return ($valid_clusters, $dropped_clusters);
}


=pod

=head2 RunCDHIT

B<Description>: Run CD HIT with the given parameters.

B<ArgsCount>: 1

=over 4

=item $parameters: (hash ref) (R)

Parameters to use.

=back

B<Return>: nothing

=cut

sub RunCDHIT
{
    my ($parameters) = @_;

    my @parameters;
    push(@parameters, map {($_, $parameters->{$_})} keys(%$parameters));

    # my $cdhit_command = 'cd-hit ';
    my $cdhit_command = "/gs7k1/projects/greenphyl/dev/pipeline/dependencies/sge/cc2_module_load.sh   cdhit ";
    $cdhit_command .= join(' ', @parameters);
    PrintDebug("COMMAND: $cdhit_command");
    `$cdhit_command`;
    
#    my $program_sh;
#    open($program_sh, "-|")
#        or exec({$cdhit_command}
#            $cdhit_command, @parameters
#        )
#        or confess "ERROR: unable to launch CD HIT! $?, $!\n";
#    # PrintDebug("CD HIT output:\n" . join('', <$program_sh>));
#    # PrintDebug("===============================================================================\n\n");
#    close($program_sh);            
}


=pod

=head2 RunOptimalCDHIT

B<Description>: Find the best parameters for CD HIT to find the wanted clusters
by launching as many time as necessary CD HIT with various parameters.

B<ArgsCount>: 4

=over 4

=item $input_filename: (string) (R)

Input file path.

=item $output_filename: (string) (R)

Output file path (existing file will be overwritten).

=item $trusted_sequences: (hash ref) (R)

a hash of trusted sequence name (as hash keys with a non-false value).

=item $total_sequence_count: (integer) (R)

Total number of sequences.

=back

B<Return>: (list)

The list of kept sequences.

=cut

sub RunOptimalCDHIT
{
    my ($input_filename, $output_filename, $trusted_sequences, $total_sequence_count) = @_;

    my @IDENTITY_THRESHOLD_AND_WORD_LENGTH_SETS = (
        [1.0, 5],
        [0.9, 5],
        [0.8, 5],
        [0.7, 5],
        [0.7, 4],
        [0.6, 4],
        [0.6, 3],
        [0.5, 3],
        [0.5, 2],
        [0.4, 2],
    );

    # compute CD HIT output file names
    my $cdhit_fasta_filename = $input_filename;
    
    # print STDERR $input_filename . "\n";
    # add prefix and take in account optional path info
    $cdhit_fasta_filename =~ s/^((?:.*\/)?)(.*)/$1$OUTPUT_FILE_PREFIX$2/;
    
    # print STDERR $cdhit_fasta_filename . "\n";
    my $cdhit_cluster_filename = $cdhit_fasta_filename . $CDHIT_STATS_OUTPUT_FILE_SUFFIX;

    my $current_parameter_set = $#IDENTITY_THRESHOLD_AND_WORD_LENGTH_SETS;
    my $clusters = {};
    my $previous_valid_clusters = {};
    my $valid_clusters = {};
    my $previous_dropped_clusters = {};
    my $dropped_clusters = {};

    # loop while we didn't try all the parameters
    # and we still don't have enought valid clusters
    # and we don't have less valid clusters than the previous try
    while (($current_parameter_set >= 0)
           && (keys(%$valid_clusters) < $g_expected_dataset_size)
           && (keys(%$previous_valid_clusters) <= keys(%$valid_clusters)))
    {
        PrintDebug("Trying CD HIT with c=" . $IDENTITY_THRESHOLD_AND_WORD_LENGTH_SETS[$current_parameter_set]->[0] . " and n=" . $IDENTITY_THRESHOLD_AND_WORD_LENGTH_SETS[$current_parameter_set]->[1]);
        RunCDHIT(
        {
            '-i' => $input_filename,
            '-o' => $cdhit_fasta_filename,
            '-c' => $IDENTITY_THRESHOLD_AND_WORD_LENGTH_SETS[$current_parameter_set]->[0],
            '-n' => $IDENTITY_THRESHOLD_AND_WORD_LENGTH_SETS[$current_parameter_set]->[1],
            '-g' => 1, # more accurate and not really slow anyway
            '-d' => 0, # full sequence name in statistics output
        });

        # parse statistics output and get clusters
        $clusters = GetClusters($cdhit_fasta_filename . $CDHIT_STATS_OUTPUT_FILE_SUFFIX, $trusted_sequences);

        $previous_valid_clusters = $valid_clusters;
        $previous_dropped_clusters = $dropped_clusters;

        # select valid clusters
        ($valid_clusters, $dropped_clusters) = GetValidClusters($clusters, $total_sequence_count);

        --$current_parameter_set;
    }
    
    # make sure we got the best amount of cluster we can get, even with the most
    # stringent parameters
    if (keys(%$previous_valid_clusters) > keys(%$valid_clusters))
    {
        PrintDebug('Note: Stopping parameter adjustment as previous results had more valid clusters! Using previous results.');
        $valid_clusters = $previous_valid_clusters;
        $dropped_clusters = $previous_dropped_clusters;
    }
    
    
    # sort best valid clusters
    my @valid_clusters = sort
        {
            (scalar(@{$b->{'sequences'}}) + $b->{'trusted_count'}) <=> (scalar(@{$a->{'sequences'}}) + $a->{'trusted_count'})
        }
        (values(%$valid_clusters))
    ;
    # make sure we got the expected number of clusters
    if (@valid_clusters < $g_expected_dataset_size)
    {
        # not enought, add dropped clusters
        push(@valid_clusters, sort
            {
                (scalar(@{$b->{'sequences'}}) + $b->{'trusted_count'}) <=> (scalar(@{$a->{'sequences'}}) + $a->{'trusted_count'})
            }
            (values(%$dropped_clusters))
        );
    }
    
    # only keep the $g_expected_dataset_size best clusters
    if (@valid_clusters > $g_expected_dataset_size)
    {
        splice(@valid_clusters, $g_expected_dataset_size);
    }
    PrintDebug("Valid cluster kept:\n" . join("\n", map { $_->{'name'} . ' (' . scalar(@{$_->{'sequences'}}) . ' sequence(s) = ' . sprintf('%.1f%%', (100 * scalar(@{$_->{'sequences'}}) / $total_sequence_count)) . ', ' . $_->{'trusted_count'} . ' trusted sequence(s), ' . $_->{'representative'} . ')' } @valid_clusters) . "\n\n");

    # get representative sequences
    my %kept_sequences;
    foreach my $valid_cluster (@valid_clusters)
    {
        $kept_sequences{$valid_cluster->{'representative'}} = 1;
    }
    
    print "Final dataset size: " . scalar(keys(%kept_sequences)) . " sequence(s)\n";
    print "Kept sequences: " . join(', ', keys(%kept_sequences)) . "\n";
    CreateFASTASubset($cdhit_fasta_filename, $output_filename, \%kept_sequences);
    
    return keys(%kept_sequences);
}



# Script options
#################

=pod

=head1 OPTIONS

    auto_cd_hit.pl [-help | -man]
    auto_cd_hit.pl -i <INPUT_FASTA> [-o <OUTPUT_FASTA>] [-r] [-debug]
        [-size <EXPECTED_DATASET_SIZE>]

=head2 Parameters

=over 4

=item B<-help>:

Prints a brief help message and exits.

=item B<-man>:

Prints the manual page and exits.

=item B<-debug>:

Executes the script in debug mode.
Default: 0 (not in debug mode).

=item B<-size> (integer):

Set the expected size of the output dataset. Default: 10.

=item B<-r>:

Force existing output file to be replaced.

=back

=cut


# CODE START
#############

# options processing
my ($man, $help, $debug, $input_filename, $output_filename, $force_remove,
    $dataset_size, $dataset_min_length, ) =
    (0, 0, 0, '', '', 0, 0, 0, );

# parse options and print usage if there is a syntax error.
GetOptions("help|?"   => \$help,
           "man"      => \$man,
           "debug"    => \$debug,
           "i=s"      => \$input_filename,
           "o=s"      => \$output_filename,
           "r"        => \$force_remove,
           "size=i"   => \$dataset_size,
           "length=i" => \$dataset_min_length,
           #@Greenphyl::Log::LOG_GETOPT,
) or pod2usage(1);
if ($help) {pod2usage('-verbose' => 1, '-exitval' => 0);}
if ($man) {pod2usage('-verbose' => 2, '-exitval' => 0);}

$DEBUG ||= $debug;

# dataset size
if ($dataset_size)
{
    $g_expected_dataset_size = $dataset_size;
}

# dataset minimum length
if ($dataset_min_length)
{
    $g_dataset_min_length = $dataset_min_length;
}

# make sure intput file exists...
if (!$input_filename)
{
    warn "ERROR: No input file specified!\n";
    pod2usage(1);
}
elsif (!-e $input_filename)
{
    confess "ERROR: input file '$input_filename' not found!\n";
}
elsif (!-r $input_filename)
{
    confess "ERROR: unable to read input file '$input_filename'!\n";
}
PrintDebug("Input file name: '$input_filename'");


# make sure output file does not exist...
if (!$output_filename)
{
    $output_filename = $input_filename;
    # generate default output name from input name
    $output_filename =~ s/(.*?)((?:\.\w{1,5})?)$/$1$DEFAULT_OUTPUT_SUFFIX/;
}

if (-e $output_filename)
{
    if ($force_remove)
    {
        unlink($output_filename);
        warn "WARNING: existing file '$output_filename' has been removed!\n";
    }
    else
    {
        confess "ERROR: output file '$output_filename' already exists! Use '-r' option to force overwrite (replace).\n";
    }
}
PrintDebug("Output file name: '$output_filename'");

eval
{
    # check input file content...
    print "Working on FASTA '$input_filename'\n";
    print "Output dataset into '$output_filename'\n";
    # open input file and read sequences
    my ($sequences) = LoadFASTA($input_filename);

    PrintDebug("Loaded " . scalar(keys(%$sequences)) . " sequences from input file");

    if ($g_dataset_min_length >= scalar(keys(%$sequences)))
    {
        Throw('error' => "Not enought sequences in input FASTA file '$input_filename'!");
    }
    elsif ($EXPECTED_DATASET_SIZE >= scalar(keys(%$sequences)))
    {
        # less than $EXPECTED_DATASET_SIZE sequences output FASTA
        PrintDebug("Less than $EXPECTED_DATASET_SIZE sequences: copy input file into output file.");
        if (system("cp $input_filename $output_filename"))
        {
            Throw('error' => "Failed to copy FASTA file: $?");
        }
    }
    else
    {
        # otherwise use  CD HIT
        PrintDebug("More than $EXPECTED_DATASET_SIZE sequences: use CD HIT");

        # find trusted sequences
        my %trusted_sequences;
        # regular expression used to match trusted species code
        # nb.: the final regexp looks like that: m/(?:ARATH|ORYSA|ZEAMA)/
        my $species_match_regexp = '(?:' . join('|', @TRUSTED_SPECIES_CODES) . ')';
        foreach my $sequence (values(%$sequences))
        {
            if ($sequence->name =~ m/$species_match_regexp/o)
            {
                # PrintDebug($sequence->name . " added to trusted sequences");
                $trusted_sequences{$sequence->name} = 1;
            }
        }
        PrintDebug("Got " . scalar(keys(%trusted_sequences)) . " trusted sequences in input file");

        RunOptimalCDHIT($input_filename, $output_filename, \%trusted_sequences, scalar(keys(%$sequences)));

    }
};
#+FIXME: remove CD HIT temporary files


# catch
HandleErrors();

exit(0);

# CODE END
###########


=pod

=head1 AUTHORS

Mathieu ROUARD (Bioversity), m.rouard@cgiar.org

Valentin GUIGNON (Bioversity), v.guignon@cgiar.org

Matthieu CONTE (Syngenta)

Sebastien BRIOIS (Syngenta)

=head1 VERSION

Version 1.0.0

Date 05/11/2012

=head1 SEE ALSO

GreenPhyl documentation,
CD-HIT documentation:
http://weizhong-lab.ucsd.edu/cd-hit/wiki/doku.php?id=cd-hit_user_guide

=cut
