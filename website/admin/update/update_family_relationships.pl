#!/usr/bin/perl

=pod

=head1 NAME

update_family_relationships.pl - Update family_relationships_cache table


=head1 SYNOPSIS

    update_family_relationships.pl

=head1 REQUIRES

Perl5

=head1 DESCRIPTION

Clear and update family_relationships_cache table by infering relationships
using common sequences between families of different levels.

=cut

use strict;
use warnings;

use lib "../../lib";
use lib '../../local_lib';
use Carp qw (cluck confess croak);

use Getopt::Long;
use Pod::Usage;

use Greenphyl;
use Greenphyl::Log('nolog' => 1,);
use Greenphyl::AbstractFamily;




# Script global constants
##########################

=pod

=head1 CONSTANTS

B<$DEBUG>: (boolean)

When set to true, it enables debug mode.

B<$CHILD_RELATIONSHIP>: (string)

Database value (enum) used for children relationship.

B<$PARENT_RELATIONSHIP>: (string)

Database value (enum) used for parent relationship.

B<$ANCESTOR_RELATIONSHIP>: (string)

Database value (enum) used for ancestor relationship.

B<$DESCENDANT_RELATIONSHIP>: (string)

Database value (enum) used for descendant relationship.

=cut

our $DEBUG = 0;
our $RELATIONSHIP_ENUM = "'" . join("', '", @Greenphyl::AbstractFamily::RELATIONSHIPS) . "'";
our $FAMILY_RELATIONSHIPS_CREATION_QUERY = <<"___253_END_CREATE_FAMILY_RELATIONSHIPS___";
    CREATE TABLE IF NOT EXISTS family_relationships_cache (
      subject_family_id MEDIUMINT(8) UNSIGNED NOT NULL,
      object_family_id MEDIUMINT(8) UNSIGNED NOT NULL,
      level_delta TINYINT(3) NOT NULL COMMENT 'Difference between object_family.level and subject_family.level; level_delta=-1 means subject_family is a direct child of object_family; all children of a subject_family will be selected using level_delta>0 and all ancestors using level_delta<0',
      type 	ENUM($RELATIONSHIP_ENUM) NOT NULL COMMENT 'Type of relationship',
      PRIMARY KEY (subject_family_id, object_family_id),
      KEY (subject_family_id),
      KEY (object_family_id)
    ) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Cache storing the relationship between families.';
___253_END_CREATE_FAMILY_RELATIONSHIPS___

our $FAMILY_RELATIONSHIPS_CONSTRAINTS_QUERY = <<"___276_END_FAMILY_RELATIONSHIPS_CONSTRAINTS___";
    ALTER TABLE family_relationships_cache
        ADD FOREIGN KEY (subject_family_id) REFERENCES families (id) ON DELETE CASCADE ON UPDATE CASCADE,
        ADD FOREIGN KEY (object_family_id) REFERENCES families (id) ON DELETE CASCADE ON UPDATE CASCADE
    ;
___276_END_FAMILY_RELATIONSHIPS_CONSTRAINTS___

our $FAMILY_RELATIONSHIPS_TRUNCATE_QUERY = 'DELETE FROM family_relationships_cache;';

our $FAMILY_RELATIONSHIPS_INSERT_QUERY = <<"___298_END_INSERT_RELATIONSHIP___";
    INSERT IGNORE INTO
        family_relationships_cache(subject_family_id, object_family_id, level_delta, type)
    SELECT    
        subject.id,
        object.id,
        CAST(object.level - subject.level AS SIGNED),
        '$Greenphyl::AbstractFamily::RELATIONSHIP_INHERITANCE'
    FROM
        families subject
        JOIN families_sequences fss ON (subject.id = fss.family_id),
        families object
        JOIN families_sequences fso ON (object.id = fso.family_id)
    WHERE
        subject.id != object.id
        AND fso.sequence_id = fss.sequence_id
        AND subject.level IS NOT NULL
        AND object.level IS NOT NULL
    ;
___298_END_INSERT_RELATIONSHIP___




# Script options
#################

=pod

=head1 OPTIONS

update_family_relationships.pl [-help | -man]

update_family_relationships.pl [-debug [debug_level]] [-log]

=head2 Parameters

=over 4

=item B<-help>:

Prints a brief help message and exits.

=item B<-man>:

Prints the manual page and exits.

=item B<-debug> (integer):

Executes the script in debug mode. The integer value is optional and is used
to set debug level (use 0 to force disable debug mode).
Default: 0 (not in debug mode).

=item B<-log>:

Enable logging.

=item B<-log-*> (any):

Logging arguments.

=back

=cut


# CODE START
#############

# for regression tests
if ($ENV{'TESTING'} && $ENV{'TESTING_NO_RUN'})
{return 1;}

# options processing
my ($man, $help, $debug) = (0, 0, undef);
# parse options and print usage if there is a syntax error.
GetOptions('help|?'     => \$help,
           'man'        => \$man,
           'debug:s'    => \$debug,
           @Greenphyl::Log::LOG_GETOPT, # skip log parameters
) or pod2usage(1);
if ($help) {pod2usage('-verbose' => 1, '-exitval' => 0);}
if ($man) {pod2usage('-verbose' => 2, '-exitval' => 0);}

# change debug mode if requested/forced
if (defined($debug))
{
    # just '-debug' ?
    if ($debug eq '')
    {
        $debug = 1;
    }
    $DEBUG = $debug;
}

# connect to database
my $dbh = GetDatabaseHandler();

eval
{
    if ($dbh->{'AutoCommit'})
    {
        $dbh->begin_work() or croak $dbh->errstr;
    }

    # check if the family_relationships_cache table exists and if not, create it
    if (!TestTableExists('family_relationships_cache'))
    {
        LogInfo('Create family_relationships_cache table');
        if (!$dbh->do($FAMILY_RELATIONSHIPS_CREATION_QUERY))
        {
            confess "ERROR: Failed to create family relationships table!\n" . $dbh->errstr;
        }

        LogInfo('Add family_relationships_cache table contraints');
        if (!$dbh->do($FAMILY_RELATIONSHIPS_CONSTRAINTS_QUERY))
        {
            confess "ERROR: Failed to add family relationships table constraints!\n" . $dbh->errstr;
        }
    }

    # truncate table
    LogInfo('Truncating family relationships table');
    if (!$dbh->do($FAMILY_RELATIONSHIPS_TRUNCATE_QUERY))
    {
        confess "ERROR: Failed to empty family relationships table!\n" . $dbh->errstr;
    }
    LogInfo('...truncating done.');

    # insert relationships
    LogInfo('Filling family relationships table...');
    if (!$dbh->do($FAMILY_RELATIONSHIPS_INSERT_QUERY))
    {
        confess "ERROR: Failed to insert family relationships!\n" . $dbh->errstr;
    }
    LogInfo('...filling done.');

    # commit SQL transaction
    $dbh->commit() or confess($dbh->errstr);
};

# catch
HandleErrors();

exit(0);

# CODE END
###########


=pod

=head1 AUTHORS

Mathieu ROUARD (Bioversity), m.rouard@cgiar.org

Valentin GUIGNON (Bioversity), v.guignon@cgiar.org

Matthieu CONTE (Syngenta)

Sebastien BRIOIS (Syngenta)

=head1 VERSION

Version 1.0.2

Date 26/11/2013

=head1 SEE ALSO

GreenPhyl documentation.

=cut
