/*
-- -----------------------------------------------------------------------------
-- Script SQL de mise à jour du schéma GreenPhyl v4 vers v5.
--
*/

CREATE TABLE IF NOT EXISTS genomes (
  id smallint(5) unsigned NOT NULL AUTO_INCREMENT,
  accession varchar(127) NOT NULL COMMENT 'Genome name',
  species_id smallint(4) unsigned NOT NULL,
  representative tinyint(1) NOT NULL DEFAULT 0 COMMENT 'Species representative genome',
  sequence_count mediumint(10) unsigned DEFAULT NULL,
  chromosome_count tinyint(3) unsigned DEFAULT NULL COMMENT 'Number of chromosome of the genome (ploidy * n)',
  ploidy tinyint(3) unsigned DEFAULT NULL COMMENT 'Ploidy of the genome (diploid=2, triploid=3, etc.)',
  genome_size float DEFAULT NULL COMMENT 'in Mbp',
  complete_busco float DEFAULT NULL COMMENT 'percent',
  fragment_busco float DEFAULT NULL COMMENT 'percent',
  missing_busco float DEFAULT NULL COMMENT 'missing',
  url_institute varchar(127) CHARACTER SET latin1 NOT NULL DEFAULT '',
  url_fasta varchar(1023) CHARACTER SET latin1 NOT NULL DEFAULT '',
  version varchar(31) NOT NULL DEFAULT '',
  version_notes text NOT NULL DEFAULT '' COMMENT 'complementary notes about the version',
  PRIMARY KEY (id),
  UNIQUE KEY accession (accession)
);

ALTER TABLE evidence_codes
  CHANGE `code` `code` varchar(3) NOT NULL COMMENT 'Evidence code (usually 2 or 3 characters)';

ALTER TABLE ipr
  CHANGE `type` `type` varchar(63) NOT NULL COMMENT 'IPR pattern might be family, domain etc.';

ALTER TABLE sequence_transfer_history
  CHANGE `old_accession` `old_accession` varchar(127) NOT NULL,
  CHANGE `new_accession` `new_accession` varchar(127) NOT NULL;

ALTER TABLE taxonomy
  CHANGE `scientific_name` `scientific_name` varchar(63) NOT NULL DEFAULT '',
  CHANGE `rank` `rank` varchar(63) NOT NULL DEFAULT '',
  CHANGE `division` `division` varchar(63) NOT NULL DEFAULT '';

ALTER TABLE user_openids
  ADD KEY `user_id_ipk` (`user_id`);

DELETE FROM custom_sequences WHERE NOT EXISTS (SELECT TRUE FROM users WHERE id = user_id LIMIT 1);

ALTER TABLE custom_sequences
  ADD CONSTRAINT custom_sequences_ibfk_2 FOREIGN KEY (user_id) REFERENCES users (id) ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE files
  ADD CONSTRAINT files_ibfk_1 FOREIGN KEY (user_id) REFERENCES `users` (id) ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE families
  CHANGE `accession` `accession` char(12) NOT NULL DEFAULT '' COMMENT 'Family accession (cross-release identifier); GreenPhyl family accession must be upper case and begin with GP',
  CHANGE `type` `type` enum('N/A','superfamily','family','subfamily','group','core','soft-core','dispensable','specific') NOT NULL COMMENT 'Family cluster type or pangenome type. Core=100%, soft-core=95-99%, dispensable=up-to-94%, specific=single genome.';

ALTER TABLE families
  ADD CONSTRAINT families_ibfk_2 UNIQUE KEY (accession);

ALTER TABLE families_sequences
   ADD COLUMN `type` enum('participating','representative','consensus') NOT NULL DEFAULT 'participating' COMMENT 'Sequence role';

ALTER TABLE sequences
  CHANGE `accession` `accession` varchar(127) NOT NULL DEFAULT '',
  CHANGE `locus` `locus` varchar(127) DEFAULT NULL,
  ADD COLUMN `type` enum('protein','dna','pan protein','pan dna') NOT NULL COMMENT 'Type of sequence',
  ADD COLUMN `genome_id` smallint(4) unsigned NOT NULL DEFAULT '0' AFTER species_id,
  ADD COLUMN `representative` tinyint(1) NOT NULL DEFAULT '0' COMMENT 'species representative sequence' AFTER polypeptide,
  ADD KEY `genome_id` (`genome_id`);

ALTER TABLE species
  CHANGE `code` `code` varchar(7) NOT NULL COMMENT 'Species code in capital letters',
  CHANGE `organism` `organism` varchar(63) DEFAULT NULL COMMENT 'species full name',
  CHANGE `common_name` `common_name` varchar(63) NOT NULL DEFAULT '',
  CHANGE `url_picture` `url_picture` varchar(255) NOT NULL,
  DROP COLUMN `sequence_count`,
  DROP COLUMN `chromosome_count`,
  DROP COLUMN `url_institute`,
  DROP COLUMN `url_fasta`,
  DROP COLUMN `version`,
  DROP COLUMN `genome_size`;

ALTER TABLE sequences
  ADD CONSTRAINT sequences_ibfk_2 FOREIGN KEY (genome_id) REFERENCES genomes (id) ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE genomes
  ADD CONSTRAINT genomes_ibfk_1 FOREIGN KEY (species_id) REFERENCES species (id) ON DELETE CASCADE ON UPDATE CASCADE;

CREATE TABLE IF NOT EXISTS sequence_count_by_families_genomes_cache (
  family_id mediumint(8) unsigned NOT NULL,
  genome_id smallint(5) unsigned NOT NULL COMMENT 'genome id (same as in table genomes)',
  sequence_count mediumint(10) unsigned NOT NULL COMMENT 'Number of sequence by family by genome',
  sequence_count_without_splice mediumint(8) unsigned NOT NULL,
  PRIMARY KEY (family_id,genome_id),
  KEY genomes (genome_id)
);

ALTER TABLE sequence_count_by_families_genomes_cache
  ADD CONSTRAINT sequence_count_by_families_genomes_cache_ibfk_3 FOREIGN KEY (family_id) REFERENCES families (id) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT sequence_count_by_families_genomes_cache_ibfk_4 FOREIGN KEY (genome_id) REFERENCES genomes (id) ON DELETE CASCADE ON UPDATE CASCADE;


DELIMITER $$

DROP FUNCTION IF EXISTS IdToChar;
CREATE FUNCTION IdToChar(
  	id INT UNSIGNED
  ) 
  RETURNS CHAR(4)
  DETERMINISTIC
BEGIN
  IF (id IS NULL) THEN
    RETURN NULL;
  END IF;

  RETURN CONCAT(CHAR((id & 0x0FF000000) / 0x01000000), CHAR((id & 0x0FF0000) / 0x010000), CHAR((id & 0x0FF00) / 0x0100), CHAR(id & 0x0FF));
END$$

DROP FUNCTION IF EXISTS IdIn;
CREATE FUNCTION IdIn(
  	id INT UNSIGNED,
    serialized_ids LONGBLOB
  ) 
  RETURNS INT UNSIGNED
  DETERMINISTIC
BEGIN
  DECLARE position INT UNSIGNED;
  DECLARE char_id BINARY(4);

  IF (id IS NULL) OR (serialized_ids IS NULL) THEN
    RETURN NULL;
  END IF;

  SET char_id = IdToChar(id);
  SELECT LOCATE(char_id, serialized_ids) INTO position;
  IF (position != 1) THEN
    WHILE (position != 0) AND (((position - 1) MOD 4) != 0) DO
      SELECT LOCATE(char_id, serialized_ids, position + (4 - ((position - 1) MOD 4))) INTO position;
    END WHILE;
  END IF;

	RETURN (position);
END$$

DELIMITER ;

-- -----------------------------------------------------------------------------
