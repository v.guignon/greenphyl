#!/usr/bin/perl

=pod

=head1 NAME

config_check.pl - validates a config file

=head1 SYNOPSIS

    perl config_check.pl -t Greenphyl/config_template.pm

=head1 REQUIRES

Perl5.

=head1 DESCRIPTION

This script checks if constants defined in a specified template config file or
another config file are also defined in current config file. It also checks if
all dependencies required by GreenPhyl are available and if report.cgi can
be run.

=cut

use strict;
use Carp qw (cluck confess croak);
use warnings;
use Readonly;
use Getopt::Long;
use Pod::Usage;


# Script global constants
##########################

=pod

=head1 CONSTANTS

B<$CONFIG_FILE_PATH>: (string)

Default path to current config file.

B<$LIB_PATH>: (string)

Default path to extra-libraries.

B<@PERL_LIB_TO_CHECK>: (array of strings)

List of global dependencies to check.

B<@PERL_WEBSITE_LIB_TO_CHECK>: (array of strings)

List of dependencies to check.

B<@PERL_ADMIN_LIB_TO_CHECK>: (array of strings)

List of dependencies to check.

=cut

Readonly my $CONFIG_FILE_PATH => '../lib/Greenphyl/Config.pm';
Readonly my $LIB_PATH         => '../lib';
Readonly my @PERL_LIB_TO_CHECK => qw(
    DBI DBIx::Simple
    CGI CGI::Session CGI::Carp CGI::Pretty
    GD Template File::Temp Tie::IxHash Statistics::Descriptive
    Proc::Background
    LWP::Simple URI::Escape List::Compare
    Data::Dumper Data::SpreadPagination Data::Tabular::Dumper
    XML::Simple XML::LibXML::Reader XML::LibXML XML::DOM
    File::Slurp Text::CSV_XS
    Bio::Seq Bio::SeqIO
    POSIX
);
Readonly my @PERL_WEBSITE_LIB_TO_CHECK => qw(
    JSON
);
Readonly my @PERL_ADMIN_LIB_TO_CHECK => qw(
    File::Slurp LockFile::Simple
    Data::SpreadPagination
);




# Script global variables
##########################

=pod

=head1 VARIABLES

B<$g_verbose_mode>: (interger)

Set verbose mode level.

=cut

my $g_verbose_mode = 0;




# Script global functions
##########################

=pod

=head1 FUNCTIONS

=head2 parseConfig

B<Description>      : Pase a config file and returns its content in a hash.

B<ArgsCount>        : 1

=over 4

=item $config_file_path: (string) (R)

the path to the config file to parse.

=back

B<Return>           : (hash ref)

A hash containing the config variable name as keys and their values.

=cut

sub parseConfig
{
    my ($config_file_path) = @_;
    # parameters check
    if (1 != @_)
    {
        confess "usage: parseConfig(config_file);";
    }

    # check config file
    my $config_fh;
    open($config_fh, "<$config_file_path") or confess "ERROR: failed to open config file '$config_file_path'!\n$!\n";
    my ($line, $parameter, %config_parameters);
    my ($in_comment, $in_variable) = (0, 0);
    my $line_num = 0;
    while ($line = <$config_fh>)
    {
        ++$line_num;
        # check if we're inside a comment block
        if ($in_comment)
        {
            if ($line =~ m/^=cut/)
            {
                $in_comment = 0;
            }
        }
        elsif (($line =~ m/^=/i) && ($line !~ m/^=cut/))
        {
            $in_comment = 1;
            $in_variable = 0;
        }
        else
        {
            # check if we got a parameter
            if ($line =~ m/^(?:[ \t]*Readonly)?[ \t]*our[ \t]/s)
            {
                $parameter = $line;
                # check if the parameter is in one line only (the ending ';' is on the same line)
                if ($line =~ m/;[ \t]*(?:#.*)?[\r\n]*$/s)
                {
                    my ($param_name, $param_value) = ($parameter =~ m/^(?:[ \t]*Readonly)?[ \t]*our[ \t]+([^ \t=]+)[ \t]*=>?[ \t]*("(?:\\?.)*"[ \t]*|'(?:\\?.)*'[ \t]*|[^;]+);[ \t]*((?:#.*)?)[\r\n]*$/s);
                    my ($param_name_indent, $param_comments) = ($parameter =~ m/^([^=]+=)[ \t]*(?:"(?:\\?.)*?"|'(?:\\?.)*?'|[^;]+)[ \t]*;([ \t]*#?[^\r\n]*)[\r\n]*$/s);
                    if (defined $param_name && defined $param_name_indent)
                    {
                        $config_parameters{$param_name} = [$param_value, $param_name_indent . ' ', $param_comments];
                    }
                    $in_variable = 0;
                }
                else
                {
                    $in_variable = 1;
                }
            }
            elsif ($in_variable)
            {
                $parameter .= $line;
                if ($line =~ m/;[ \t]*(?:#.*)?[\r\n]*$/s)
                {
                    # last line of a parameter
                    my ($param_name, $param_value) = ($parameter =~ m/^(?:[ \t]*Readonly)?[ \t]*our[ \t]+([^ \t=]+)[ \t]*=>?[ \t]*("(?:\\?.)*"[ \t]*|'(?:\\?.)*'[ \t]*|[^;]+);[ \t]*((?:#.*)?)[\r\n]*$/s);
                    my ($param_name_indent, $param_comments) = ($parameter =~ m/^([^=]+=)[ \t]*(?:"(?:\\?.)*?"|'(?:\\?.)*?'|[^;]+)[ \t]*;([ \t]*#?[^\r\n]*)[\r\n]*$/s);
                    if (defined $param_name && defined $param_name_indent)
                    {
                        $config_parameters{$param_name} = [$param_value, $param_name_indent . ' ', $param_comments];
                    }
                    $in_variable = 0;
                }
            }
        }
    }
    close($config_fh);

    return \%config_parameters;
}


=pod

=head2 checkConfig

B<Description>      : Compare current config and the given one.

B<ArgsCount>        : 1-2

=over 4

=item $template_file_path: (string) (R)

the path to the config file to use as template.

=item $config_file_path: (string) (O)

the path to the config file currently used. If omitted, $CONFIG_FILE_PATH is
used as default.

=back

B<Return>           : nothing

=cut

sub checkConfig
{
    my ($template_file_path, $config_file_path) = @_;
    # parameters check
    if ((1 != @_) && (2 != @_))
    {
        confess "usage: checkConfig(template_config_file, config_file);";
    }

    $config_file_path ||= $CONFIG_FILE_PATH;

    print "Checking GreenPhyl config ('$config_file_path' against '$template_file_path')...\n";

    my $no_difference = 1;
    # check config file
    my $template_parameters = parseConfig($template_file_path);
    my $current_config_parameters = parseConfig($config_file_path);

    print "Missing parameters:\n";
    foreach my $parameter (keys(%$template_parameters))
    {
        if (!exists($current_config_parameters->{$parameter}))
        {
            print "-$parameter\n";
            $no_difference = 0;
        }
    }
    if ($no_difference)
    {
        print " No missing parameter found.\n";
    }
    $no_difference = 1;
    print "\n";
    print "Unused parameters:\n";
    foreach my $parameter (keys(%$current_config_parameters))
    {
        if (!exists($template_parameters->{$parameter}))
        {
            print "-$parameter\n";
            $no_difference = 0;
        }
    }
    if ($no_difference)
    {
        print " No unused parameter found.\n";
    }
    $no_difference = 1;
    print "\nDone.\n";
}


=pod

=head2 checkDependencies

B<Description>: Check if GreenPhyl dependencies are available in the
specified lib directory or in default lib path.

B<ArgsCount>: 0-2

=over 4

=item $lib_path: (string) (U)

the path where the Perl libraries are supposed to be.

=item $parameters: (hash) (O)

Hash containing additional parameters.

=back

B<Return>: nothing

=cut

sub checkDependencies
{
    my ($lib_path, $parameters) = @_;
    # parameters check
    if (2 < @_)
    {
        confess "usage: checkDependencies(lib_path);";
    }

    $lib_path ||= $LIB_PATH;
    $parameters ||= {};
    if (ref($parameters) ne 'HASH')
    {
        confess "usage: checkDependencies(lib_path, parameters_hashref);";
    }

    print "Checking GreenPhyl dependencies (using extra-library path '$lib_path')...\n";

    my @perl_lib_to_check = @PERL_LIB_TO_CHECK;
    if ($parameters->{'admin'})
    {
        push @perl_lib_to_check, @PERL_ADMIN_LIB_TO_CHECK;
    }
    if ($parameters->{'website'})
    {
        push @perl_lib_to_check, @PERL_WEBSITE_LIB_TO_CHECK;
    }
    # special check for MySQL lib version: version >= 4.006 required for
    # procedure calls. Typical related error message:
    # "... PROCEDURE ... can't return a result set in the given context at ..."
    use DBD::mysql;
    if ($DBD::mysql::VERSION < 4.006)
    {
        print STDERR "WARNING: too old version on DBD::mysql. You should update it or stored MySQL procedure calls won't work!\n";
    }
    
    foreach my $lib_to_check (@perl_lib_to_check)
    {
        eval("use lib '$lib_path'; require $lib_to_check;");
        if ($@)
        {
            print "-$lib_to_check: ERROR!\n";
            if ($g_verbose_mode)
            {
                print STDERR $@ . "\n";
            }
        }
        else
        {
            print "-$lib_to_check: ok\n";
        }
    }

    print "Done.\n";
}




# Script options
#################

=pod

=head1 OPTIONS

perl config_check.pl < -t <template_config> > [-c config_file] [-l lib_path] [-configonly | -libonly] [-context all|admin|website] [-verbose]

=head2 Parameters

=over 4

=item B<-t template_config> (string):

Path to the config template (or another config file).

=item B<-c config_file> (string):

Path to the config file to check. If not set, default config path will be used.

=item B<-l lib_path> (string):

Path to the extra-library to be used by GreenPhyl.

=item B<-configonly> (flag):

Only check config.

=item B<-libonly> (flag):

Only check dependencies.

=item B<-context all|admin|website> (string):

If set to "all", both dependencies for admin interface and website will be
checked. If set to "admin", only admin dependencies will be checked. If set to
"website", only website dependencies will be checked.

=item B<-verbose> (flag):

Set verbose mode on.

=back

=cut


# CODE START
#############

# options processing
my ($man, $help, $debug, $template_path, $config_path, $config_only, $lib_path, $lib_only, $context) =
   (   0,     0,      0,             '',           '',            0,        '',         0,       '');
# parse options and print usage if there is a syntax error.
GetOptions("help|?"      => \$help,
           "man"         => \$man,
           "debug"       => \$debug,
           "t=s"         => \$template_path,
           "c=s"         => \$config_path,
           "l=s"         => \$lib_path,
           "configonly"  => \$config_only,
           "libonly"     => \$lib_only,
           "verbose|v+"  => \$g_verbose_mode,
           "context|x=s" => \$context,)
    or pod2usage(2);
if ($help) {pod2usage(1);}
if ($man) {pod2usage(-verbose => 2);}

if ((not $template_path) || ($config_only && $lib_only))
{
    print STDERR "ERROR: Syntax error!\n\n";
    pod2usage(2);
    exit(1);
}

my $parameters = {'admin' => 1, 'website' => 1,};
if ($context)
{
    if ($context =~ m/all/)
    {
        # already set to all
    }
    elsif ($context =~ m/admin/)
    {
        $parameters = {'admin' => 1};
    }
    elsif ($context =~ m/website/i)
    {
        $parameters = {'website' => 1};
    }
    else
    {
        print STDERR "ERROR: Syntax error: unknown context '$context'!\n\n";
        pod2usage(2);
        exit(1);
    }
}

if (!$lib_only)
{
    checkConfig($template_path, $config_path);
}
print "\n";
if (!$config_only)
{
    checkDependencies($lib_path, $parameters);
}

print "\nAll done!\n";

# CODE END
###########


=pod

=head1 AUTHORS

Valentin GUIGNON (Bioversity-France), v.guignon@cgiar.org

=head1 VERSION

Version 0.1.0

Date 06/12/2010

=cut
