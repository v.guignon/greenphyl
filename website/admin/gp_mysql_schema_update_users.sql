DELIMITER $$

DROP PROCEDURE IF EXISTS updateUsersSchemaTemp$$
CREATE PROCEDURE updateUsersSchemaTemp()
  BEGIN
    CREATE TABLE IF NOT EXISTS users (
      id SMALLINT(5) UNSIGNED NOT NULL AUTO_INCREMENT,
      login varchar(255) NOT NULL,
      display_name varchar(255) NOT NULL,
      email VARCHAR(255) NULL DEFAULT NULL,
      password_hash CHAR(40) NOT NULL,
      salt CHAR(40) NOT NULL,
      flags SET('administrator', 'annotator', 'registered user', 'disabled') NOT NULL DEFAULT '',
      comments TEXT CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
      PRIMARY KEY (id),
      UNIQUE KEY login (login)
    ) ENGINE=InnoDB DEFAULT CHARSET=utf8;

    -- upgrade user accounts
    BEGIN
      DECLARE l_password VARCHAR(255);
      DECLARE l_email VARCHAR(255);
      DECLARE l_group INT(10);
      DECLARE l_username VARCHAR(255);
      DECLARE done INT DEFAULT 0;
      DECLARE user_cursor CURSOR FOR SELECT passwd, email, group_ann, username FROM annot_user;
      DECLARE CONTINUE HANDLER FOR NOT FOUND SET done := 1;

      OPEN user_cursor;
      user_cursor_loop: LOOP
        FETCH user_cursor INTO l_password, l_email, l_group, l_username;
        IF done THEN
          LEAVE user_cursor_loop;
        END IF;

        -- add user
        SELECT CONCAT('Upgrading user "', l_username, '"') AS 'Message';
        IF (1 = l_group) THEN
          -- admin
          CALL addGreenPhylUser(l_username, l_username, l_email, l_password, 'administrator,annotator,registered user', '');
        ELSE
          -- regular user
          CALL addGreenPhylUser(l_username, l_username, l_email, l_password, 'annotator,registered user', '');
        END IF;
      END LOOP;

      CLOSE user_cursor;
    END;
  END;
$$

        
DROP PROCEDURE IF EXISTS addGreenPhylUser$$
CREATE PROCEDURE addGreenPhylUser(
    IN gp_user_login VARCHAR(255),
    IN gp_user_display_name VARCHAR(255),
    IN gp_user_email VARCHAR(255),
    IN gp_user_password VARCHAR(255),
    IN gp_user_flags VARCHAR(255),
    IN gp_comments VARCHAR(255)
  )
  BEGIN
  
    -- generate random salt on 40 characters (using MD5 to get a hexadecimal string value)
    SET @l_user_salt := MD5(RAND());
    -- generate password hash
    SET @l_user_password_hash := hashGreenPhylPassword(gp_user_password, @l_user_salt);
    -- store new user
    INSERT INTO users (login, display_name, email, password_hash, salt, flags, comments) VALUES (
      gp_user_login,
      gp_user_display_name,
      gp_user_email,
      @l_user_password_hash,
      @l_user_salt,
      gp_user_flags,
      gp_comments
    );
  END
$$


DROP FUNCTION IF EXISTS hashGreenPhylPassword $$
CREATE FUNCTION hashGreenPhylPassword(gp_user_password VARCHAR(255), gp_user_salt CHAR(40)) RETURNS CHAR(40)
  BEGIN
    -- use AES to encrypt password using given salt (enforce security against hash-dictionnary brute-force attacks)
    -- and generate a SHA1 hash
    RETURN SHA1(AES_ENCRYPT(gp_user_password, gp_user_salt));
  END
$$


DROP FUNCTION IF EXISTS authenticateGreenPhylUser $$
CREATE FUNCTION authenticateGreenPhylUser(gp_user_login VARCHAR(255), gp_user_password VARCHAR(255)) RETURNS BOOL
  BEGIN
    DECLARE l_authenticated BOOL DEFAULT FALSE;
    
    IF (EXISTS (SELECT TRUE FROM users u WHERE u.login = gp_user_login AND u.flags NOT LIKE '%disabled%' AND u.password_hash = hashGreenPhylPassword(gp_user_password, u.salt) LIMIT 1)) THEN
      SET l_authenticated := TRUE;
    END IF;

    RETURN l_authenticated;
  END
$$

DELIMITER ;

CALL updateUsersSchemaTemp();
DROP FUNCTION updateUsersSchemaTemp;
