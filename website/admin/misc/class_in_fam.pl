#!/usr/bin/perl

use strict;
use warnings;

use lib '../../lib';
use lib '../../local_lib';
use Carp;
use Greenphyl::ConnectMysql;
use Statistics::Descriptive;
use Greenphyl::SQL;
use Data::Dumper;
use Bio::Seq;
use Bio::SearchIO;
use Bio::SeqIO;
use Greenphyl::Config;                # contains configuation path for the server

my $file_temp = "/apps/GreenPhyl/v2/htdocs/tmp/blast/tmp_in$$";

my $dbh = Greenphyl::ConnectMysql->new->DBconnect;

my $file = shift;
my $in   = Bio::SeqIO->newFh(
    -file   => $file,
    -format => 'fasta'
);

while (<$in>)
{
    my $id     = $_->display_id;    #query
    my $seq    = $_->seq;
    my $length = $_->length;
    my $fasta  = $_;

    print "$id\t";
    $file_temp = clipboard( $id, $seq, $file_temp );
    system("chmod 777 $file_temp");
    blast( $file_temp, $id, $length );

}

sub blast
{
    my $file_temp  = shift;
    my $id         = shift;
    my $length     = shift;
    my $evalue     = "1e-10";
    my $out_number = "2";
    my $cpt_hit    = 0;

    #my $database   = "/apps/GreenPhyl/v2/lib/blast/ALL";
    my $database  = "/apps/GreenPhyl/v2/lib/blast/ARATH_ORYZA";
    my $file_out  = "/apps/GreenPhyl/v2/htdocs/tmp/blast/tmp_out$$";
    my $hit_while = 0;
    my %hash;

    `$Greenphyl::Config::BLASTALL_COMMAND -i $file_temp -e $evalue -p blastp -F none -d $database -a 2 -K $out_number -v $out_number -b $out_number -o $file_out`;

#print "$Greenphyl::Config::BLASTALL_COMMAND -i $file_temp -e $evalue -p blastx -F none -d $database -a 2 -K $out_number -v $out_number -b $out_number -o $file_out\n";
# for family _id controle
    my $toto;
    my $toto_cpt = 0;

    #for control lenght
    my $tito = 0;
    my $family_id;
    my $name;
    my $ref_seq;
    my $found = 0;
    if ( -e $file_out )    #si on a des sorties de blast, elles sont stockees ici
    {
        my $in = new Bio::SearchIO( -format => 'blast', -file => "$file_out" );
        system("chmod 777 $file_out");

        while ( my $result = $in->next_result and $hit_while == 0 )
        {
            while ( my $hit = $result->next_hit and $hit_while == 0 )    #sortir ici
            {

                while ( my $hsp = $hit->next_hsp and $hit_while == 0 )
                {
                    my $out = $hit->name;
                    $cpt_hit++;

                    #print "$out\t";
                    if ( $cpt_hit == 1 ) { $hash{$id} = $out; }
                    if ( $cpt_hit == 2 ) { $hit_while = 1; }
                    $found++;
                    my @seq_ids = split( /\s+/, $out );

                    ##va recuperer classification du hit found $seq_id
                    for my $seq_id (@seq_ids)
                    {
                        $seq_id  = ucfirst( lc($seq_id) );
                        $ref_seq = $seq_id;
                        my $sql = "SELECT S.seq_textid, F.family_name, F.family_id, C.class_id
                                                FROM sequences S, seq_is_in SI, family F, classifications C, found_in FO  
                                                WHERE S.seq_textid =\"$seq_id\" 
                                                    and S.seq_id     = SI.seq_id 
                                                    and SI.family_id = F.family_id 
                                                    and F.family_id  = FO.family_id 
                                                    and FO.class_id  = C.class_id";
                        my @rep7 = @{ $dbh->selectall_arrayref($sql) };
                        my $count_verif;    #pour verifier la taille de la sequence

                        # hit found est classifie dans une famille
                        if (@rep7)
                        {
                            $family_id = $rep7[0][2];    #recupere la famille pour le niveau 1
                            $name      = $rep7[0][1];

                            if ($toto)
                            {
                                if ( $toto == $family_id )
                                {                        #print "***same fam****\t";
                                }
                                else
                                {

                                    #print "****diff fam****\t";
                                    $toto_cpt++;
                                    $hit_while = 1;
                                }
                            }
                            $toto = $family_id;
                            $count_verif = &count( $family_id, $id, $length );    #verifier si ce sont les bons parametres

                            if   ( $count_verif == 1 ) { }
                            else                       { $tito++; $hit_while = 1; }
                        }
                    }
                }
            }    # end boucle sur list HIT
        }
    }

    if ( $tito == 0 && $toto_cpt == 0 && $cpt_hit > 0 )
    {

        #print"$family_id\t$name\t$ref_seq\n";
        my @req2 = @{
            $dbh->selectall_arrayref(
                "SELECT  F.family_id, F.family_name, FO.class_id  
                FROM seq_is_in SI, family F, found_in FO, sequences S 
                WHERE S.seq_textid =\"$ref_seq\" 
                AND S.seq_id     = SI.seq_id
                AND SI.family_id = F.family_id 
                AND FO.family_id = F.family_id"
            )
            };
        foreach (@req2)
        {
            my $fam_id   = $$_[0];
            my $name     = $$_[1];
            my $class_id = $$_[2];
            print "$fam_id\t$name\t";
        }
        print "\n";
    }
    else { print "ORPHAN\n"; }
}

#fonction pour verifier avec un critere de taille : si sequence a une longueur OK : renvoie 1,  sinon 0
sub count
{
    my $family_id    = shift;
    my $seq_textid   = shift;
    my $seq_length   = shift;
    my $cpt          = 0;
    my $somme_length = 0;
    my @length_seq;
    my $dbh = Greenphyl::ConnectMysql->new->DBconnect;
    my %result;

    #select les sequences qui appartiennent a la meme famille que la seq que l'on veut etudier (pour le niveau 1 uniquement)
    my $sql = "SELECT S.seq_textid, S.seq_length FROM SEQUENCES S, SEQ_IS_IN SI WHERE S.seq_id = SI.seq_id and SI.family_id = $family_id";
    my @rep = @{ $dbh->selectall_arrayref($sql) };

    if (@rep)    #si trouve des sequences (si pas trouve alors que toutes les sequences de ALL sont dans la base = probleme !!!!!!
    {
        for ( my $i = 0; $i <= $#rep; $i++ )
        {
            $cpt++;
            $result{$cpt}{'length'} = $rep[$i][1];    #recupere pour chaque sequence de la famille dans une hashatable, la taille de chaque sequence
        }

        foreach my $test ( keys %result )
        {
            $somme_length += $result{$test}{'length'};    #fait la somme des longueurs des sequences pour la famille
            push @length_seq, $seq_length;
        }

        my $moy_length = $somme_length / $cpt;   #fait la moyenne
                                                 #print "Your sequence is $seq_textid : Length  $seq_length   and Average lenght in this group: $moy_length \n";
        my $stat2 = Statistics::Descriptive::Full->new();
        $stat2->add_data(@length_seq);
        my $mean         = $stat2->mean();
        my $median       = $stat2->median();
        my $soixante     = ( $median * 10 ) / 100;
        my $centquarante = ( $median * 190 ) / 100;

        if ( $seq_length < $soixante )
        {

            #print "Too small sequence $seq_textid !!!  $seq_length < $soixante \n";
            return 0;                            #mauvais c'est un orphan, il est trop petit, on integre pas cette sequence.
        }
        if ( $seq_length > $centquarante )
        {

            # print "Too large sequence $seq_textid !!!  $seq_length > $centquarante \n";
            return 0;                            #mauvais c'est un orphan, il est trop  grand, on integre pas cette sequence.
        }
        else
        {

            #print "OK min $soixante max $centquarante  seq_length: $seq_length\n";
            return 1;                            #bon, on integre cette sequence.
        }
    }
    else
    {

        #print "ERROR the sequences not in DATABASE ! \n";
        exit;

    }

}

sub clipboard
{
    my $name      = shift;
    my $seq       = shift;
    my $file_temp = shift;

    my $sql          = "SELECT SP.species_name FROM sequences S, species SP WHERE S.species_id=SP.species_id AND S.seq_textid = \"$name\"";
    my $species_name = $dbh->selectrow_array($sql);

    open( OUT, ">$file_temp" ) or die "cannot open file : $!";
    print OUT ">" . $name . "_" . $species_name . "\n" . $seq . "\n";
    $name = "";
    $seq  = "";
    close OUT;
    return $file_temp;
}

