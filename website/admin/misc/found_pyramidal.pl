#!/usr/bin/perl

use strict;
use warnings;

use lib '../../../lib';
use lib '../../../local_lib';
use Carp;
use Greenphyl::ConnectMysqlTest;
use Statistics::Descriptive;
use Greenphyl::SQL;
use Data::Dumper;

my $dbh = Greenphyl::ConnectMysqlTest->new->DBconnect;

#my $family = shift;
my @req1 = @{ $dbh->selectall_arrayref("SELECT distinct(family_id) FROM found_in WHERE class_id = 2") };
foreach (@req1)
{
    my $family = $_;
    if ( $family == 42252 )
    {
        print "$family\t";
        test($family);
    }
}

sub test()
{
    my $fam  = shift;
    my @req2 = @{
        $dbh->selectall_arrayref(
            "SELECT seq_id
                                FROM seq_is_in 
                                WHERE family_id = $fam"
        )
        };

    #foreach seq
    my $control;
    my $cpt = 0;

    foreach (@req2)
    {
        my $seq_id = $_;

        # print "*$seq_id*";
        my $sql = "SELECT F.family_id 
                   FROM seq_is_in SI, family F, found_in FI
                   WHERE SI.seq_id = $seq_id AND FI.class_id = 1 AND SI.family_id = F.family_id AND F.family_id = FI.family_id";
        my $fam_id = $dbh->selectrow_array($sql);

        #print "Family level 1:$fam_id\t";
        if ($control)
        {
            if   ( $control == $fam_id ) { }
            else                         { $cpt++; next; }
        }
        $control = $fam_id;

    }
    if ( $cpt > 0 )
    {
        print "WARNING\n";
    }
    else { print "\n"; }
}

