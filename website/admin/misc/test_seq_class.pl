#!/usr/bin/perl
#create by Matthieu
# allow to control classficiation of a sequence
# use:  perl test_seq_class.pl At1g14920.1

use strict;
use warnings;

use lib '../../lib';
use lib '../../local_lib';
use Greenphyl::ConnectMysql;
use Greenphyl::SQL;
use Data::Dumper;
use Bio::Seq;
use Bio::SearchIO;
use Bio::SeqIO;
use Greenphyl::Config;                # contains configuation path for the server

my $url_tmp   = $Greenphyl::Config::TEMP_OUTPUT_PATH;
my $file_out  = "$url_tmp/tmp_out$$";
my $file_temp = "$url_tmp/tmp_in$$";

my $dbh = Greenphyl::ConnectMysql->new->DBconnect;

my $seq_textid = shift();
my $family_o;    #original family (tested)
my $family_n;    #new family
my $hit_found;

print "$seq_textid\t";

( $family_n, $hit_found ) = &blastall($seq_textid);

#if match control lenght
if ($family_n)
{
    my $count_verif = &count( $family_n, $seq_textid );
    if ( $count_verif == 1 )    #length ok
    {
        print "=> Lenght  ok\n";
    }
    else                        #length NOT ok
    {
        print "=> Lenght  NOT ok\n";
    }
}
print "\n";

#}#comment for rapid test

#blast query and return family_id level1 if found
sub blastall
{
    my $seq_textid = shift;
    my $evalue     = "1e-10";
    my $out_number = "10";

    my @req = @{
        $dbh->selectall_arrayref(
            "SELECT sequence, seq_id  
                                FROM sequences
                                WHERE seq_textid = \"$seq_textid\""
        )
        };

    my $seq       = $req[0][0];
    my $seq_id    = $req[0][1];
    my $database  = "/apps/GreenPhyl/v2/lib/blast/ALL";
    my $file_temp = clipboard( $seq_textid, $seq, $file_temp );
    system("chmod 777 $file_temp");

    `$Greenphyl::Config::BLASTALL_COMMAND -i $file_temp -e $evalue -p blastp -F none -d $database -a 5 -K $out_number -v $out_number -b $out_number -o $file_out`;

#print "$Greenphyl::Config::BLASTALL_COMMAND -i $file_temp -e $evalue -p blastp -F none -d $database -a 5 -K $out_number -v $out_number -b $out_number -o $file_out\n";
    if ( -e $file_out )
    {
        my $in = new Bio::SearchIO( -format => 'blast', -file => "$file_out" );
        system("chmod 777 $file_out");
        while ( my $result = $in->next_result )
        {
            while ( my $hit = $result->next_hit )
            {
                while ( my $hsp = $hit->next_hsp )
                {
                    my $out   = $hit->name;
                    my $score = $hsp->evalue;
                    my $hit   = $hsp->length('total');
                    my $sql   = "SELECT F.family_id
                           FROM sequences S, seq_is_in SI, family F, classifications C, found_in FO  
                           WHERE S.seq_textid =\"$out\" and S.seq_id = SI.seq_id and SI.family_id = F.family_id and F.family_id= FO.family_id and FO.class_id = C.class_id and C.class_id =1";
                    my $family_id = $dbh->selectrow_array($sql);
                    print "classified into: $family_id ";
                    return ( $family_id, $out );
                }
            }
        }
        print "\n";
    }
    return;
}

sub clipboard
{
    my $name      = shift;
    my $seq       = shift;
    my $file_temp = shift;
    open( OUT, ">$file_temp" ) or die "cannot open file : $!";
    print OUT ">" . $name . "\n" . $seq . "\n";
    $name = "";
    $seq  = "";
    close OUT;
    return $file_temp;
}

#(christelle) fonction pour verifier avec un critere de taille : si sequence a une longueur OK : renvoie 1,  sinon 0
sub count
{
    my $family_id    = shift;
    my $seq_textid   = shift;
    my $cpt          = 0;
    my $somme_length = 0;
    my $dbh          = Greenphyl::ConnectMysql->new->DBconnect;
    my %result;

    #print "1./ Sequence lenght control for $seq_textid ---$family_id\n";
    my $sql = "SELECT seq_length FROM  sequences  WHERE seq_textid = \"$seq_textid\"";    #recupere taille de la sequence
    my @rep = @{ $dbh->selectall_arrayref($sql) };

    if (@rep)
    {
        my $seq_length = $rep[0];

        #select les sequences qui appartiennent a la meme famille que la seq que l'on veut etudier (pour le niveau 1 uniquement)
        my $sql = "SELECT S.seq_textid, S.seq_length FROM SEQUENCES S, SEQ_IS_IN SI WHERE S.seq_id = SI.seq_id and SI.family_id = $family_id";
        my @rep = @{ $dbh->selectall_arrayref($sql) };

        if (@rep)    #si trouve des sequences (si pas trouve alors que toutes les sequences de ALL sont dans la base = probleme !!!!!!
        {
            for ( my $i = 0; $i <= $#rep; $i++ )
            {
                $cpt++;
                $result{$cpt}{'length'} = $rep[$i][1];    #recupere pour chaque sequence de la famille dans une hashatable, la taille de chaque sequence
            }

            foreach my $test ( keys %result )
            {
                $somme_length += $result{$test}{'length'};    #fait la somme des longueurs des sequences pour la famille
            }

            my $moy_length = $somme_length / $cpt;            #fait la moyenne
                 #print "Your sequence is $seq_textid : Length  $seq_length   and Average lenght in this group ($family_id): $moy_length \n";
            my $soixante     = ( $moy_length * 60 ) / 100;
            my $centquarante = ( $moy_length * 140 ) / 100;

            if ( $seq_length < $soixante )
            {

                #print "Too small sequence $seq_textid !!!  $seq_length < $soixante \n";
                return 0;    #mauvais c'est un orphan, il est trop petit, on integre pas cette sequence.
            }
            if ( $seq_length > $centquarante )
            {

                #print "Too large sequence $seq_textid !!!  $seq_length > $centquarante \n";
                return 0;    #mauvais c'est un orphan, il est trop  grand, on integre pas cette sequence.
            }
            else
            {

                #print "OK min $soixante max $centquarante  for $seq_length\n";
                return 1;    #bon, on integre cette sequence.
            }
        }
        else
        {
            print "ERROR the sequences not in DATABASE ! \n";
            exit;
        }
    }
    else
    {
        print "cannot find length for $seq_textid , mayby the sequence is not in the database\n";
        exit;
    }
}

