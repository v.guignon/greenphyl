#!/usr/bin/perl
#create by Matthieu
# allow to test each group specie-spe and control reability.
# use:  perl clean_specie_spe.pl 15 ARATH

use strict;
use warnings;

use lib '../../lib';
use lib '../../local_lib';
use Carp;
use Greenphyl::ConnectMysqlTest;
use Statistics::Descriptive;
use Greenphyl::SQL;
use Data::Dumper;
use Bio::Seq;
use Bio::SearchIO;
use Bio::SeqIO;
use Greenphyl::Config;                # contains configuation path for the server

my $url_tmp   = $Greenphyl::Config::TEMP_OUTPUT_PATH;
my $file_out  = "$url_tmp/tmp_out$$";
my $file_temp = "$url_tmp/tmp_in$$";

my $dbh = Greenphyl::ConnectMysqlTest->new->DBconnect;

my $spe_id    = shift();
my $reference = shift();    #(blastlib: correspond to species code)
my $family_o;               #original family (tested)
my $family_n;               #new family
my $hit_found;

#get all specie-specific family for a specie
my @req1 = @{ $dbh->selectall_arrayref("SELECT distinct(family_id) FROM family WHERE species_id = $spe_id") };

foreach (@req1)
{
    $family_o = $_;

    #$family_o  =56483;#for rapid test
    if ( $family_o > 42244 )
    {

        #to count good match
        my %hash;
        my $cpt_ok = 0;

        #get number of seq in this group to stat
        my $sql   = "SELECT number FROM count_family_seq WHERE family_id = $family_o";
        my $count = $dbh->selectrow_array($sql);

        print "Control FID:$family_o--Nb of seq:$count\n";

        #get all seq from this group
        my @req2 = @{
            $dbh->selectall_arrayref(
                "SELECT SE.seq_textid, SE.sequence,SE.seq_id
                                FROM seq_is_in SI, sequences SE 
                                WHERE SI.seq_id = SE.seq_id AND SI.family_id = $family_o"
            )
            };

        #foreach seq make blast
        foreach (@req2)
        {
            my $seq_textid = $$_[0];
            my $seq        = $$_[1];
            my $seq_id     = $$_[2];
            my $species_id = $$_[3];

            #blast each seq from this family
            ( $family_n, $hit_found ) = &blastall( $seq_textid, $seq, $spe_id, $reference );

            #print "hash\n$seq_id=>$hit_found\n";
            $hash{$seq_id} = $hit_found;

            #if match control lenght
            if ($family_n)
            {
                my $count_verif = &count( $family_n, $seq_textid );
                if ( $count_verif == 1 )    #length ok
                {
                    print "\tLenght  ok\n";
                    $cpt_ok++;              #for stat
                }
                else                        #length NOT ok
                {
                    print "\tLenght  NOT ok\n";
                }
            }
        }

        #test if average seq form family could be transfered
        my $control = $count / 2;
        if ( $cpt_ok >= $control )
        {
            print "TRANSFERT $family_o to $family_n ($count---$cpt_ok)\n";
            move( $family_o, \%hash );
        }
        else
        {
            print "Conserve $family_o ($count---$cpt_ok)\n";
        }
        print "\n";
    }    #end if
}    #comment for rapid test

#blast query and return family_id level1 if found
sub blastall
{
    my $seq_textid = shift;
    my $seq        = shift;
    my $spe_id     = shift;
    my $reference  = shift;
    my $evalue     = "1e-10";
    my $out_number = "1";
    my $database   = "/apps/GreenPhyl/v2/lib/blast/$reference";
    my $file_temp  = clipboard( $seq_textid, $seq, $file_temp );
    system("chmod 777 $file_temp");

    `$Greenphyl::Config::BLASTALL_COMMAND -i $file_temp -e $evalue -p blastp -F none -d $database -a 5 -K $out_number -v $out_number -b $out_number -o $file_out`;

#print "$Greenphyl::Config::BLASTALL_COMMAND -i $file_temp -e $evalue -p blastp -F none -d $database -a 5 -K $out_number -v $out_number -b $out_number -o $file_out\n";
    if ( -e $file_out )
    {
        my $in = new Bio::SearchIO( -format => 'blast', -file => "$file_out" );
        system("chmod 777 $file_out");
        while ( my $result = $in->next_result )
        {
            while ( my $hit = $result->next_hit )
            {
                while ( my $hsp = $hit->next_hsp )
                {
                    my $out   = $hit->name;
                    my $score = $hsp->evalue;
                    my $hit   = $hsp->length('total');
                    my $sql   = "SELECT FO.family_id
                            FROM found_in FO
                            LEFT JOIN seq_is_in SI ON SI.family_id = FO.family_id
                            LEFT JOIN  sequences S ON S.seq_id = SI.seq_id
                            WHERE S.seq_textid =\"$out\" and FO.class_id =1";
                    my $family_id = $dbh->selectrow_array($sql);
                    print "$seq_textid => HIT $out ($hit $score) from family:$family_id ";
                    if ($family_id)
                    {
                        return ( $family_id, $out );
                    }
                }
            }
        }

        #print "\n";
    }
    return;
}

sub clipboard
{
    my $name      = shift;
    my $seq       = shift;
    my $file_temp = shift;

    my $sql          = "SELECT SP.species_name FROM sequences S, species SP WHERE S.species_id=SP.species_id AND S.seq_textid = \"$name\"";
    my $species_name = $dbh->selectrow_array($sql);

    open( OUT, ">$file_temp" ) or die "cannot open file : $!";
    print OUT ">" . $name . "_" . $species_name . "\n" . $seq . "\n";
    $name = "";
    $seq  = "";
    close OUT;
    return $file_temp;
}

#(christelle) fonction pour verifier avec un critere de taille : si sequence a une longueur OK : renvoie 1,  sinon 0
sub count
{
    my $family_id    = shift;
    my $seq_textid   = shift;
    my $cpt          = 0;
    my $somme_length = 0;
    my @length_seq;
    my $dbh = Greenphyl::ConnectMysql->new->DBconnect;
    my %result;

    #print "1./ Sequence lenght control for $seq_textid ---$family_id\n";
    my $sql = "SELECT seq_length FROM  sequences  WHERE seq_textid = \"$seq_textid\"";    #recupere taille de la sequence
    my @rep = @{ $dbh->selectall_arrayref($sql) };

    if (@rep)
    {
        my $seq_length = $rep[0];

        #select les sequences qui appartiennent a la meme famille que la seq que l'on veut etudier (pour le niveau 1 uniquement)
        my $sql = "select s.seq_textid, s.seq_length 
               FROM sequences s LEFT JOIN seq_is_in si ON s.seq_id = si.seq_id  
               WHERE si.family_id = $family_id";
        my @rep = @{ $dbh->selectall_arrayref($sql) };

        if (@rep)    #si trouve des sequences (si pas trouve alors que toutes les sequences de ALL sont dans la base = probleme !!!!!!
        {
            for ( my $i = 0; $i <= $#rep; $i++ )
            {
                $cpt++;
                $result{$cpt}{'length'} = $rep[$i][1];    #recupere pour chaque sequence de la famille dans une hashatable, la taille de chaque sequence
            }

            foreach my $test ( keys %result )
            {
                $somme_length += $result{$test}{'length'};    #fait la somme des longueurs des sequences pour la famille
                push @length_seq, $result{$test}{'length'};
            }

            #my $moy_length  = $somme_length / $cpt; #fait la moyenne
            my $stat2 = Statistics::Descriptive::Full->new();
            $stat2->add_data(@length_seq);
            my $median = $stat2->median();

            #print "Your sequence is $seq_textid : Length  $seq_length   and Average lenght in this group ($family_id): $moy_length \n";
            my $soixante     = ( $median * 60 ) / 100;
            my $centquarante = ( $median * 140 ) / 100;

            if ( $seq_length < $soixante )
            {

                #print "Too small sequence $seq_textid !!!  $seq_length < $soixante \n";
                return 0;    #mauvais c'est un orphan, il est trop petit, on integre pas cette sequence.
            }
            if ( $seq_length > $centquarante )
            {

                #print "Too large sequence $seq_textid !!!  $seq_length > $centquarante \n";
                return 0;    #mauvais c'est un orphan, il est trop  grand, on integre pas cette sequence.
            }
            else
            {

                #print "OK min $soixante max $centquarante  for $seq_length\n";
                return 1;    #bon, on integre cette sequence.
            }
        }
        else
        {
            print "ERROR the sequences not in DATABASE ! \n";
            exit;
        }
    }
    else
    {
        print "cannot find length for $seq_textid , mayby the sequence is not in the database\n";
        exit;
    }
}

#transfer classification
sub move
{
    my $family_o = $_[0];
    my %hash     = %{ $_[1] };

    my $query;
    my $hit;

    #print "DELETE from seq_is_in\n";
    my $sql = "DELETE from seq_is_in WHERE family_id = \"$family_o\"";
    #################################
    #$dbh->do($sql);
    while ( ( $query, $hit ) = each %hash )
    {

        #get seq_id of hit found
        my $sql    = "SELECT seq_id FROM sequences WHERE seq_textid =\"$hit\"";
        my $seq_id = $dbh->selectrow_array($sql);

        #print "\nID query $query => $hit($seq_id)\n";
        #get classification of hit found
        my $sql = "SELECT family_id FROM  seq_is_in  WHERE seq_id = \"$seq_id\"";
        my @rep = @{ $dbh->selectall_arrayref($sql) };

        #if classification
        if (@rep)
        {

            #transfer this classification to query
            foreach (@rep)
            {
                my $family_id = $_;
                my $sql       = "INSERT INTO `seq_is_in` (`seq_id`, `family_id`) VALUES ('$query', '$family_id')";
                ################################
                #$dbh->do($sql);
                #test if this family submit to phylo
                if ( -e "/data2/GreenPhyl/pipe/IN/$family_id.fa" )
                {
                    print "\t\t$family_id submit to PHYLO! transfer $query\n";
                    ##############################
                    #concat($family_id,$query);
                }
            }
        }
    }

    #delete origaninal family - seq without match will become orphans
    ###############################
    #delete_fam($family_o);
}

#delete all family structure
sub delete_fam
{
    my $family = shift;
    print "\n";
    eval {

        #print "DELETE from family\n";
        my $sql = "DELETE from family WHERE family_id = $family LIMIT 1";
        $dbh->do($sql);

        #print "DELETE from annot_family\n";
        my $sql = "DELETE from annot_family WHERE family_id = $family";
        $dbh->do($sql);

        #print "DELETE from count_family_seq\n";
        my $sql = "DELETE from count_family_seq WHERE family_id = $family";
        $dbh->do($sql);

        #print "DELETE from family_dbxref\n";
        my $sql = "DELETE from family_dbxref WHERE family_id = $family";
        $dbh->do($sql);

        #print "DELETE from family_has_an\n";
        my $sql = "DELETE from family_has_an WHERE family_id = $family";
        $dbh->do($sql);

        #print "DELETE from found_in\n";
        my $sql = "DELETE from found_in WHERE family_id = $family LIMIT 1";
        $dbh->do($sql);

        #print "DELETE from ipr_stat\n";
        my $sql = "DELETE from ipr_stat WHERE family_id = $family";
        $dbh->do($sql);

        #print "DELETE from synonym\n";
        my $sql = "DELETE from synonym WHERE family_id = $family";
        $dbh->do($sql);

        #print "DELETE from seq_is_in\n";
        my $sql = "DELETE from seq_is_in WHERE family_id = $family";
        $dbh->do($sql);
    };
    if ($@)    #probleme whith process
    {
        confess "Unable to delete family: $@\n";
        $dbh->rollback();
    }

}

#concat fasta file
sub concat
{
    my $family     = shift;
    my $seq_id     = shift;
    my $sql        = "SELECT sequence, seq_textid FROM sequences WHERE seq_id = \"$seq_id\"";
    my @rep        = @{ $dbh->selectall_arrayref($sql) };
    my $seq        = $rep[0][0];
    my $seq_textid = $rep[0][1];

    my $file = "/data2/GreenPhyl/pipe/IN/$family.fa";
    my $file_temp = clipboard( $seq_textid, $seq, $file_temp );
    print "cat $seq_textid\n";
    `cat  $file_temp >> $file`;

}
