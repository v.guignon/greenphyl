#!/usr/bin/perl
#create by Matthieu
# allow to delete family a specified family

# use:  perl delete_fam.pl family_id

use strict;
use warnings;

use lib '../../../lib';
use lib '../../../local_lib';
use Carp;
use Greenphyl::ConnectMysql;
use Greenphyl::SQL;

my $dbh = Greenphyl::ConnectMysql->new->DBconnect;

my $seq = shift;
my $sql = "SELECT seq_id FROM sequences WHERE seq_textid = ?";

my $seq_id = $dbh->selectrow_array( $sql, undef, $seq );

eval {
    print "DELETE $seq ($seq_id)\n";
    my $sql = "DELETE from seq_is_in WHERE seq_id = $seq_id";

    #print "$sql\n";
    $dbh->do($sql);

};
if ($@)    #probleme whith process
{
    confess "Unable to delete family: $@\n";
    $dbh->rollback();
}

