#!/usr/bin/perl
#create by Matthieu
# allow to delete family a specified family

# use:  perl delete_fam.pl family_id

use strict;
use warnings;

use lib '../../../lib';
use lib '../../../local_lib';
use Carp;
use Greenphyl::ConnectMysql;
use Greenphyl::SQL;
use Data::Dumper;

my $dbh = Greenphyl::ConnectMysql->new->DBconnect;
my $in  = shift;
my ( $seq, $old_fam, $new_fam ) = ( split( /\-/, $in ) );

#my $seq = shift;
#my $old_fam = shift;
#my $new_fam = shift
print "$seq---$old_fam--- $new_fam\n";
my $sql = "SELECT seq_id FROM sequences WHERE seq_textid = '$seq'";
print "$sql\n";
my $seq_id = $dbh->selectrow_array($sql);

eval {
    print "TRANSFERT  $seq ($seq_id) from $old_fam to $new_fam\n";
    my $sql = "UPDATE `seq_is_in` SET `family_id` = $new_fam WHERE `seq_id` = $seq_id AND `family_id` =$old_fam LIMIT 1 ;";
    print "$sql\n";
    $dbh->do($sql);

};
if ($@)    #probleme whith process
{
    confess "Unable to delete family: $@\n";
    $dbh->rollback();
}

