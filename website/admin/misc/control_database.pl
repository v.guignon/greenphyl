#!/usr/bin/perl
#create by Matthieu
#liste de commandes permettant de controler la base et de suivre cl� dans diff table

use strict;
use warnings;

use lib '../../../lib';
use lib '../../../local_lib';
use Greenphyl::ConnectMysql;
use Greenphyl::SQL;
use Data::Dumper;

my $file = shift();
my $dbh  = Greenphyl::ConnectMysql->new->DBconnect;

#detecte family dans table xxxx mais pas dans  family
my @req1 = @{ $dbh->selectall_arrayref("SELECT distinct(family_id) FROM annot_family") };
my $cpt  = 0;
foreach (@req1)
{
    my $family = $_;

    #print "Test $family\n";
    my $sql    = "SELECT family_number FROM family WHERE family_id = $family";
    my $fam_id = $dbh->selectrow_array($sql);
    if ( !$fam_id )
    {
        print "$family de annot_family plus dans family\n";

        #vire dans la table count_family_seq
        my $sql2 = "DELETE from annot_family WHERE family_id = $family ";

        #$dbh->do($sql2);
        $cpt++;
    }
}
print "COUNT $cpt\n";
###################################

=comm
#fait via phpmyadmin pour la version dev pour virer glyma SOYBN
  #vire dans SEQ_IS_IN
    my $sql = "DELETE FROM seq_is_in WHERE seq_id IN (select seq_id FROM sequences WHERE species_id = 10)";
    print "$sql\n";
    #$dbh->do($sql); 

#vire dans SEQUENCE
    my $sql = "DELETE FROM sequences WHERE species_id = 10";
    print "$sql\n";
    #$dbh->do($sql); 

#vire dans FAMILY_HAS_AN
    my $sql = "DELETE FROM family_has_an WHERE species_id = 10";
    print "$sql\n";
    #$dbh->do($sql);   
    
#vire dans SPECIES
    my $sql = "DELETE FROM species WHERE species_id = 10";
    print "$sql\n";
    #$dbh->do($sql);
    
###################################  

=comm

#recup family a level 1 avec moins de 2 sequences puis vire la famille dans SEQ_IS_IN et dans FAMILY
my @req1 = @{ $dbh->selectall_arrayref("SELECT  F.family_id, COUNT(SI.seq_id) 
           FROM seq_is_in SI, family F, found_in FO 
           WHERE SI.family_id = F.family_id AND F.family_id = FO.family_id AND FO.class_id = 1 group by F.family_id")
           };
       
 foreach (@req1)  {  
       my $family = $$_[0];
       my $count = $$_[1];

        if ($count)
        {
            if ($count < 2){
                #vire dans la table SEQ_IS_IN
                my $sql1 = "DELETE from seq_is_in WHERE family_id = $family ";
                print "$sql1\n";
                #$dbh->do($sql1);
                
                #vire dans la table FAMILY
                my $sql2 = "DELETE from family WHERE family_id = $family ";
                print "vire $family car $count\n";
                #$dbh->do($sql2);
            }
         }
}
 


