#!/usr/bin/perl

use strict;
use warnings;

use lib '../../lib';
use lib '../../local_lib';
use Greenphyl::Web::Greenphyl;

my $gp  = Greenphyl::Web::Greenphyl->new;
my $dbh = $gp->{dbh};

my $output_by_species = '';
$output_by_species .= "DESCRIPTION\tID\tCYAME\tCHLRE\tOSTTA\tPHYPA\tSELMO\tORYSA\tBRADI\tZEAMA\tSORBI\tVITVI\tARATH\tCARPA\tPOPTR\tRICCO\tGLYMA\tMEDTR\n";

my $output_by_phylum = '';
$output_by_phylum .= "ID\talgae\tPHYPA\tSELMO\tmonocots\tdicots\n";

my  %phylum = (
    'CYAME' => 1,
    'OSTTA' => 1,
    'CHLRE' => 1,
    'PHYPA' => 2,
    'SELMO' => 3,  
    'SORBI' => 4,
    'ZEAMA' => 4,  
    'ORYSA' => 4,
    'BRADI' => 4,
    'ARATH' => 5,
    'CARPA' => 5,
    'VITVI' => 5,
    'GLYMA' => 5,
    'RICCO' => 5,
    'POPTR' => 5,
    'MEDTR' => 5,
);

#open( F, "monom_list.txt" ) or die "cannot open file : $!";
#
#my %monome;
#foreach my $fid (<F>)
#{
#    chomp($fid);
#
#    $monome{$fid}++;
#}

# list of families with more than 5 sequences
my $sql_families =
    "SELECT a.family_id, f.family_name FROM seq_is_in a, found_in c, family f WHERE c.class_id = 1 AND a.family_id = c.family_id AND a.family_id = f.family_id GROUP BY a.family_id HAVING COUNT(a.family_id) >= 5";


# list of families with more than 5 sequences and plant specific
#my $sql_families = "SELECT a.family_id, f.family_name FROM seq_is_in a, found_in c, family f WHERE f.plant_spe = 1 AND c.class_id = 1 AND a.family_id = c.family_id AND a.family_id = f.family_id GROUP BY a.family_id HAVING COUNT(a.family_id) >= 5";

my @families = @{ $dbh->selectall_arrayref( $sql_families, { Slice => {} } ) };

my $sql1 = "SELECT species_name FROM species ORDER BY tax_order asc";

my @species_codes = @{ $dbh->selectall_arrayref( $sql1, { Slice => {} } ) };

@species_codes = map { $_->{species_name} } @species_codes;

my $species_sql = "
    SELECT  S.species_name, C.number
    FROM    count_family_seq C
        LEFT JOIN species S ON C.species_id = S.species_id
    WHERE   C.family_id = ?
";


foreach (@families)
{

    #next unless ( $monome{ $_->{family_id} } );

    $_->{family_name} ||= "Unknown family name";
    $_->{family_name} =~ s/^\s+//;    # because some family names start with a white space

    $output_by_species .= "$_->{family_name}\t$_->{family_id}";
    $output_by_phylum  .= "$_->{family_id}";

    my %result = %{ $dbh->selectall_hashref( $species_sql, 'species_name', undef, $_->{family_id} ) };
    my %count_phylum;
    for (@species_codes)
    {
        my $species = $result{$_};
        $species ||= { species_name => $_, number => 0 };
 
        $output_by_species .= "\t" . $species->{number};
        
        if (exists($count_phylum{$phylum{ $species->{species_name} } }) )
        {
            $count_phylum{ $phylum{ $species->{species_name} } } += $species->{number};
        }
        else
        {
            $count_phylum{ $phylum{ $species->{species_name} } } = $species->{number};
        }
    }
    my $max = scalar(keys %count_phylum);
    for (1..$max)
    {
        $output_by_phylum .= "\t" . $count_phylum{$_}
    }
    
    $output_by_species .= "\n";
    $output_by_phylum .= "\n";  
}

#print $output_by_species;
print $output_by_phylum;

=pod

=head1 AUTHORS

Mathieu ROUARD, m.rouard@cgiar.org

=cut

