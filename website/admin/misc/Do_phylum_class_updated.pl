#!/usr/bin/perl
#create by Matthieu
# allow to load species_id and node_id in family table to create species specific and phylum specific family list

use strict;
use lib '../../lib';
use lib '../../local_lib';
use Greenphyl::ConnectMysql;
use Greenphyl::SQL;
use Data::Dumper;

my $mysql = new Greenphyl::ConnectMysql();
my $dbh   = $mysql->DBconnect();

#get family_id - specie_id
my @species = $mysql->requete(
    "SELECT distinct FI.family_id, S.species_id
                    FROM `sequences` S, seq_is_in SI, found_in FI
                    WHERE class_id=1
                    AND SI.seq_id = S.seq_id 
                    and SI.family_id=FI.family_id"
);
my %family;

for ( my $i = 0; $i < @species; $i = $i + 2 )
{
    if ( exists $family{ $species[$i] } )
    {
        $family{ $species[$i] } .= " $species[$i+1]";
    }
    else
    {
        $family{ $species[$i] } = $species[ $i + 1 ];
    }
}

my %result;
foreach my $k ( keys %family )
{
    my ( $monocot, $paccad, $bep );
    my ( $dicot, $eurosidI, $eurosidII, $coreeurosid, $fabales, $malpighiales );
    my ( $algue, $tracheo, $embryo, $cyame, $greenalgue );
    my @spec = split( / /, $family{$k} );

    # if only one species found
    if ( @spec == 1 )
    {
        my $sql = "UPDATE `family` SET  `species_id` = $spec[0] WHERE `family_id` = $k";

        #print "$sql\n";
        $mysql->insert($sql);
    }

    #if several species
    else
    {
        foreach (@spec)
        {

            #create groups
            if ( $_ == 1 || $_ == 5 || $_ == 14 || $_ == 17 )
            {
                $monocot++;
                if    ( $_ == 5 || $_ == 17 ) { $paccad++; }
                elsif ( $_ == 1 || $_ == 14 ) { $bep++; }
            }
            elsif ( $_ == 2 || $_ == 3 || $_ == 6 || $_ == 7 || $_ == 10 || $_ == 15 || $_ == 16 )
            {
                $dicot++;
                if ( $_ == 3 || $_ == 6 || $_ == 10 || $_ == 16 )
                {
                    $eurosidI++;
                    if    ( $_ == 6 || $_ == 16 ) { $malpighiales++; }
                    elsif ( $_ == 3 || $_ == 10 ) { $fabales++; }
                }
                elsif ( $_ == 2 || $_ == 15 ) { $eurosidII++; }
                elsif ( $_ == 7 ) { $coreeurosid++; }
            }
            elsif ( $_ == 9 ) { $tracheo++; }
            elsif ( $_ == 8 ) { $embryo++; }

            elsif ( $_ == 11 || $_ == 12 || $_ == 13 )
            {
                $algue++;
                if ( $_ == 13 ) { $cyame++; }
                elsif ( $_ == 11 || $_ == 12 ) { $greenalgue++; }
            }
        }
        ###Monocot
        if (   $monocot
            && !$dicot
            && !$algue
            && !$tracheo
            && !$embryo )
        {
            my $sql = "UPDATE `family` SET  `node_id` = 6 WHERE `family_id` = $k";
            print "$sql\n";
            $mysql->insert($sql);
        }

        #paccad clade
        if (   $paccad
            && !$bep
            && !$dicot
            && !$algue
            && !$tracheo
            && !$embryo )
        {
            my $sql = "UPDATE `family` SET  `node_id` = 11 WHERE `family_id` = $k";
            print "$sql\n";
            $mysql->insert($sql);
        }

        #bep clade
        if (   $bep
            && !$paccad
            && !$dicot
            && !$algue
            && !$tracheo
            && !$embryo )
        {
            my $sql = "UPDATE `family` SET  `node_id` = 10 WHERE `family_id` = $k";
            print "$sql\n";
            $mysql->insert($sql);
        }
        ###Dicot
        if (   $dicot
            && !$monocot
            && !$algue
            && !$tracheo
            && !$embryo )
        {
            my $sql = "UPDATE `family` SET  `node_id` = 7 WHERE `family_id` = $k";
            print "$sql\n";
            $mysql->insert($sql);
        }

        #eurosid1
        if (   $eurosidI
            && !$eurosidII
            && !$coreeurosid
            && !$monocot
            && !$algue
            && !$tracheo
            && !$embryo )
        {
            my $sql = "UPDATE `family` SET  `node_id` = 8 WHERE `family_id` = $k";
            print "$sql\n";
            $mysql->insert($sql);
        }

        #malpigghiales
        if (   $malpighiales
            && !$fabales
            && !$eurosidII
            && !$coreeurosid
            && !$monocot
            && !$algue
            && !$tracheo
            && !$embryo )
        {
            my $sql = "UPDATE `family` SET  `node_id` = 12 WHERE `family_id` = $k";
            print "$sql\n";
            $mysql->insert($sql);
        }

        #fabales
        if (   $fabales
            && !$malpighiales
            && !$eurosidII
            && !$coreeurosid
            && !$monocot
            && !$algue
            && !$tracheo
            && !$embryo )
        {
            my $sql = "UPDATE `family` SET  `node_id` = 13 WHERE `family_id` = $k";
            print "$sql\n";
            $mysql->insert($sql);
        }

        #eurosid2
        if (   $eurosidII
            && !$eurosidI
            && !$coreeurosid
            && !$monocot
            && !$algue
            && !$tracheo
            && !$embryo )
        {
            my $sql = "UPDATE `family` SET  `node_id` = 9 WHERE `family_id` = $k";
            print "$sql\n";
            $mysql->insert($sql);
        }

        #Angios
        if (   $dicot
            && $monocot
            && !$algue
            && !$tracheo
            && !$embryo )
        {
            my $sql = "UPDATE `family` SET  `node_id` = 5 WHERE `family_id` = $k";
            print "$sql\n";
            $mysql->insert($sql);
        }

        #Tracheo
        if (   $tracheo
            && ( $dicot || $monocot )
            && !$algue
            && !$embryo )
        {
            my $sql = "UPDATE `family` SET  `node_id` = 4 WHERE `family_id` = $k";
            print "$sql\n";
            $mysql->insert($sql);
        }

        #Embryo
        if (   $embryo
            && ( $dicot || $monocot || $tracheo )
            && !$algue )
        {
            my $sql = "UPDATE `family` SET  `node_id` = 3 WHERE `family_id` = $k";
            print "$sql\n";
            $mysql->insert($sql);
        }

        #Viridiplante
        if (   $greenalgue
            && ( $dicot || $monocot || $tracheo || $embryo )
            && !$cyame )
        {
            my $sql = "UPDATE `family` SET  `node_id` = 2 WHERE `family_id` = $k";
            print "$sql\n";
            $mysql->insert($sql);
        }

        #algue
        if (   $algue
            && !$dicot
            && !$monocot
            && !$tracheo
            && !$embryo )
        {
            my $sql = "UPDATE `family` SET  `node_id` = 1 WHERE `family_id` = $k";
            print "Algue $sql\n";
            $mysql->insert($sql);
        }

    }
}

