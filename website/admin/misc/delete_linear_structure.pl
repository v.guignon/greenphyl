#!/usr/bin/perl
#create by Matthieu
#script permettant virer les structures totalement lineaires sur les 4niveaux: pas encore appliqu�...
use strict;
use lib '../../lib';
use lib '../../local_lib';
use Greenphyl::ConnectMysql;
use Greenphyl::SQL;
use Data::Dumper;

my $mysql = new Greenphyl::ConnectMysql();
my $dbh   = $mysql->DBconnect();

#recup family a level 1

my @req1 = (
    $mysql->select(
        "SELECT  F.family_id, SI.seq_id, COUNT(SI.seq_id) 
           FROM seq_is_in SI, family F, found_in FO 
           WHERE SI.family_id = F.family_id AND F.family_id = FO.family_id AND FO.class_id = 1 group by F.family_id"
    )
);
my $toto = 0;

foreach (@req1)
{
    my $family = $$_[0];
    my $seq_id = $$_[1];
    my $level1 = $$_[2];

    #recup family a level 4 pour ce seq_id
    my $req4 = (
        $mysql->select(
            "SELECT F.family_id
      FROM seq_is_in SI, family F, found_in FO 
      WHERE SI.family_id = F.family_id AND F.family_id = FO.family_id AND FO.class_id = 4 AND SI.seq_id = $seq_id"
        )
    )[0];

    if ( $req4 && $req4 != 41815 && $req4 != 39011 && $req4 != 37336 )
    {

        #print "FAM level 4 $req4\n";
        my $level4 = ( $mysql->select("SELECT COUNT(seq_id) FROM seq_is_in  WHERE family_id = $req4 ") )[0];

        #si meme nombre de seq entre level 1 et level4
        if ( $level1 == $level4 )
        {

            #print "level 1: $family ->$level1\tlevel 4: $req4 ->$level4\n";
            #recup family a level 2 pour ce seq_id
            my $req2 = (
                $mysql->select(
                    "SELECT F.family_id
              FROM seq_is_in SI, family F, found_in FO 
              WHERE SI.family_id = F.family_id AND F.family_id = FO.family_id AND FO.class_id = 2 AND SI.seq_id = $seq_id"
                )
            )[0];
            my $level2 = ( $mysql->select("SELECT COUNT(seq_id) FROM seq_is_in  WHERE family_id = $req2 ") )[0];

            #recup family a level 3 pour ce seq_id

            my $req3 = (
                $mysql->select(
                    "SELECT F.family_id
              FROM seq_is_in SI, family F, found_in FO 
              WHERE SI.family_id = F.family_id AND F.family_id = FO.family_id AND FO.class_id = 3 AND SI.seq_id = $seq_id"
                )
            )[0];

            my $level3 = ( $mysql->select("SELECT COUNT(seq_id) FROM seq_is_in  WHERE family_id = $req3") )[0];

            ##si tout le monde est egal
            if ( $level1 == $level2 && $level1 == $level3 && $level1 == $level4 )
            {
                print "level 1: $family ->$level1\tlevel 2: $req2 ->$level2\tlevel 3: $req3 ->$level3\tlevel 4: $req4 ->$level4\n";
                deletefam($req2);
                deletefam($req3);
                deletefam($req4);
                my $total = $level1 * 3;
                my $tmp   = $toto + $total;
                $toto = $tmp;
                print "Total delete in SEQ_IS_IN:$toto\n";
            }
        }
    }
}

sub deletefam
{
    my $fam = shift;

    #vire dans la table SEQ_IS_IN
    my $sql1 = "DELETE from seq_is_in WHERE family_id = $fam ";

    #print "$sql1\n";
    $mysql->insert($sql1);

    #vire dans la table FAMILY
    my $sql2 = "DELETE from family WHERE family_id = $fam LIMIT 1";

    #print "$sql2\n";
    $mysql->insert($sql2);

    #vire dans la table FOUND_IN
    my $sql2 = "DELETE from found_in WHERE family_id = $fam LIMIT 1";

    #print "$sql2\n";
    $mysql->insert($sql2);

    #vire dans la table IPR_STAT
    my $sql2 = "DELETE from ipr_stat WHERE family_id = $fam";

    #print "$sql2\n";
    $mysql->insert($sql2);

    #vire dans la table count_family_seq
    my $sql2 = "DELETE from count_family_seq WHERE family_id = $fam";

    #print "$sql2\n";
    $mysql->insert($sql2);
    return;
}
