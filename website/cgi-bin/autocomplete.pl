#!/usr/bin/perl

=pod

=head1 NAME

autocomplete.pl - Returns a JSON list of words for auto-completion

=head1 SYNOPSIS

    autocomplete.pl?q=plat&limit=10&timestamp=1338367549155&search_type=any

Returns:
  [{"value":"PLATZ transcription factor family","label":"PLATZ transcription factor family"},{"value":"TPLATE homologs","label":"TPLATE homologs"},{"value":"platelet-activating factor acetyltransferase activ","label":"platelet-activating factor acetyltransferase activ"}]

=head1 REQUIRES

Perl5

=head1 DESCRIPTION

This script is used for auto-completion in the Quick-search input field.
Depending the field of search selected, it returns the top-10 of corresponding
words. If all the fields of search are used, it returns the top 2 for each
field.

=cut

use strict;
use warnings;

use lib '../lib';
use lib '../local_lib';
use Carp qw (cluck confess croak);

use CGI qw( header );
use Greenphyl;
use Greenphyl::Web;
use Greenphyl::SourceDatabase;

use JSON;
use HTML::Entities;




# Script global constants
##########################

=pod

=head1 CONSTANTS

B<$DEBUG>: (boolean)

enables debug mode.

B<$MAX_SUGGESTIONS>: (integer)

the maximum number of line of suggestions returned.

B<%AUTOCOMPLETE_HANDLERS>: (hash)

hash associating a field of search to a function that handle the
auto-completion for that field of search.

=cut

our $DEBUG = 0;

our $MAX_SUGGESTIONS = 10;


my %AUTOCOMPLETE_HANDLERS = (
    ''             => \&GetQuickList,
    'any'          => \&GetAnyElementList,
    'alias'        => \&GetAliasList,
#    'annotation'   => \&GetAnnotationList,
    'annotation'   => \&GetNoCompletion,
    'ec'           => \&GetECList,
#    'family_id'    => \&GetFamilyIDList,
    'family_id'    => \&GetNoCompletion,
    'family_name'  => \&GetFamilyNameList,
    'go_fam'       => \&GetGOList,
    'go_seq'       => \&GetGOList,
#    'interpro_fam' => \&GetInterproList,
    'interpro_fam' => \&GetNoCompletion,
    'interpro_seq' => \&GetInterproList,
    'kegg'         => \&GetKEGGList,
    'quick'        => \&GetQuickList,
    'sequence_id'  => \&GetSequenceIDList,
    'uniprot'      => \&GetUniprotList,
);




# Script global variables
##########################

=pod

=head1 VARIABLES

B<$g_dbh>: (DBI::db)

Global database handler.

=cut

my $g_dbh = GetDatabaseHandler();




# Script global functions
##########################

=pod

=head1 FUNCTIONS

=head2 GetSuggestionList

B<Description>: Perform a query on database to fetch entries that match the
given parameters and returns a corresponding array of strings.

B<ArgsCount>: 1

=over 4

=item $search_field: (array ref or hash ref) (R)

If it's a hash ref, one query will be performed using the given parameters in
the hash.
If an array ref is given, then several queries will be performed using each hash
of the array as hash of search.

Hash content:
'column': the name of the column to return;
'extra': some extra WHERE-CLAUSE conditions ending with an ' AND ' string;
'max_suggestions': maximum number of suggestion string returned;
'query': the user input text to search in database;
'table': the name of the table to search into.

=back

B<Return>: (array ref)

An array of string, each string being a suggestion to the given input query.

=cut

sub GetSuggestionList
{
    my ($search_field) = @_;

    my $suggestions = {};

    # check if we have several queries to handle or just one
    if ('ARRAY' eq ref($search_field))
    {
        my $multi_parameters = $search_field;
        foreach (@{$multi_parameters})
        {
            my $suggestion_array = GetSuggestionList($_);
            # we use a hash to avoid doubles
            map { $suggestions->{$_} = 1 if ($_); } @$suggestion_array;
        }
    }
    else
    {
        my $max_suggestions = $search_field->{'max_suggestions'} || $MAX_SUGGESTIONS;
        my $extra_condition = $search_field->{'extra'} || '';
        my $sql_query       = "SELECT DISTINCT $search_field->{'column'} FROM $search_field->{'table'} WHERE $extra_condition $search_field->{'column'} LIKE ? ORDER BY $search_field->{'column'} ASC LIMIT $max_suggestions;";
        # add jocker characters arround user query
        my $user_filter     = "%$search_field->{'query'}%";
        PrintDebug("SQL QUERY: $sql_query\nBindings: $user_filter");
        # user input query string is escaped using Perl DBI function
        my $suggestion_array = $g_dbh->selectcol_arrayref($sql_query, undef, $user_filter);
        # we use a hash to avoid doubles
        map { $suggestions->{$_} = 1 if ($_); } @$suggestion_array;
    }

    return [sort(keys(%$suggestions))];
}


=pod

=head2 GetJsonList

B<Description>: Returns a JSON list from the given array.

B<ArgsCount>: 1

=over 4

=item $input_array_ref: (array ref) (R)

An array of suggestion string (or words).

=back

B<Return>: (string)

A JSON string representing the sugggestion list.

=cut

sub GetJsonList
{
    my ($input_array_ref) = @_;
    return to_json([ map { {'value' => $_, 'label' => encode_entities($_)}; } @$input_array_ref ]);
}


=pod

=head2 GetNoCompletion

B<Description>: Returns an empty array.

B<ArgsCount>: 0

B<Return>: (array ref)

An empty array.

=cut

sub GetNoCompletion
{
    return [];
}


=pod

=head2 GetFamilyNameList

B<Description>: Returns the array of suggestion strings for family names.

B<ArgsCount>: 2

=over 4

=item $user_input: (string) (R)

User query string.

=item $max_suggestions: (integer) (O)

Maximum number of strings to return.

=back

B<Return>: (array ref)

An array of string containing suggestion strings.

=cut

sub GetFamilyNameList
{
    my ($user_input, $max_suggestions) = @_;
    $max_suggestions ||= $MAX_SUGGESTIONS;
    return GetSuggestionList(
        [
            { 'query' => $user_input, 'column' => 'name', 'table' => 'families', 'max_suggestions' => $max_suggestions },
            { 'query' => $user_input, 'column' => 'synonym', 'table' => 'family_synonyms', 'max_suggestions' => $max_suggestions },
        ]
    );
}


=pod

=head2 GetSequenceIDList

B<Description>: Returns the array of suggestion strings for sequence ID.

B<ArgsCount>: 2

=over 4

=item $user_input: (string) (R)

User query string.

=item $max_suggestions: (integer) (O)

Maximum number of strings to return.

=back

B<Return>: (array ref)

An array of string containing suggestion strings.

=cut

sub GetSequenceIDList
{
    my ($user_input, $max_suggestions) = @_;
    $max_suggestions ||= $MAX_SUGGESTIONS;
    return GetSuggestionList({'query' => $user_input, 'column' => 'accession', 'table' => 'sequences', 'max_suggestions' => $max_suggestions });
}


=pod

=head2 GetFamilyIDList

B<Description>: Returns the array of suggestion strings for family ID.

B<ArgsCount>: 2

=over 4

=item $user_input: (string) (R)

User query string.

=item $max_suggestions: (integer) (O)

Maximum number of strings to return.

=back

B<Return>: (array ref)

An array of string containing suggestion strings.

=cut

sub GetFamilyIDList
{
    my ($user_input, $max_suggestions) = @_;
    $max_suggestions ||= $MAX_SUGGESTIONS;
    return GetSuggestionList({'query' => $user_input, 'column' => 'id', 'table' => 'families', 'max_suggestions' => $max_suggestions });
}


=pod

=head2 GetAliasList

B<Description>: Returns the array of suggestion strings for aliases.

B<ArgsCount>: 2

=over 4

=item $user_input: (string) (R)

User query string.

=item $max_suggestions: (integer) (O)

Maximum number of strings to return.

=back

B<Return>: (array ref)

An array of string containing suggestion strings.

=cut

sub GetAliasList
{
    my ($user_input, $max_suggestions) = @_;
    $max_suggestions ||= $MAX_SUGGESTIONS;
    return GetSuggestionList({'query' => $user_input, 'extra' => "EXISTS (SELECT TRUE FROM dbxref dx JOIN db db ON (db.id = dx.db_id) WHERE dx.id = dbxref_synonyms.dbxref_id AND db.name = '$Greenphyl::SourceDatabase::SOURCE_DATABASE_UNIPROT') AND ", 'column' => 'synonym', 'table' => 'dbxref_synonyms', 'max_suggestions' => $max_suggestions });
}


=pod

=head2 GetAnnotationList

B<Description>: Returns the array of suggestion strings for family annotations.

B<ArgsCount>: 2

=over 4

=item $user_input: (string) (R)

User query string.

=item $max_suggestions: (integer) (O)

Maximum number of strings to return.

=back

B<Return>: (array ref)

An array of string containing suggestion strings.

=cut

sub GetAnnotationList
{
    my ($user_input, $max_suggestions) = @_;
    $max_suggestions ||= $MAX_SUGGESTIONS;
    return GetSuggestionList({'query' => $user_input, 'column' => 'annotation', 'table' => 'sequences', 'max_suggestions' => $max_suggestions });
}


=pod

=head2 GetInterproList

B<Description>: Returns the array of suggestion strings for InterPro.

B<ArgsCount>: 2

=over 4

=item $user_input: (string) (R)

User query string.

=item $max_suggestions: (integer) (O)

Maximum number of strings to return.

=back

B<Return>: (array ref)

An array of string containing suggestion strings.

=cut

sub GetInterproList
{
    my ($user_input, $max_suggestions) = @_;
    $max_suggestions ||= $MAX_SUGGESTIONS;
    return GetSuggestionList({'query' => $user_input, 'column' => 'code', 'table' => 'ipr', 'max_suggestions' => $max_suggestions });
}


=pod

=head2 GetUniprotList

B<Description>: Returns the array of suggestion strings for UniProt.

B<ArgsCount>: 2

=over 4

=item $user_input: (string) (R)

User query string.

=item $max_suggestions: (integer) (O)

Maximum number of strings to return.

=back

B<Return>: (array ref)

An array of string containing suggestion strings.

=cut

sub GetUniprotList
{
    my ($user_input, $max_suggestions) = @_;
    $max_suggestions ||= $MAX_SUGGESTIONS;

    my $db = Greenphyl::SourceDatabase->new($g_dbh, {'selectors' => {'name' => $Greenphyl::SourceDatabase::SOURCE_DATABASE_UNIPROT}});
    if (!$db)
    {
        Throw('error' => 'UniProt source database not available for auto-completion!');
    }

    return GetSuggestionList({'query' => $user_input, 'extra' => 'db_id = ' . $db->id . ' AND ', 'column' => 'accession', 'table' => 'dbxref', 'max_suggestions' => $max_suggestions });
    # return GetSuggestionList({'query' => $user_input, 'extra' => 'db_id = (SELECT db.id FROM db db WHERE db.name = \'UNIPROT\') AND ', 'column' => 'accession', 'table' => 'dbxref', 'max_suggestions' => $max_suggestions });
}


=pod

=head2 GetKEGGList

B<Description>: Returns the array of suggestion strings for KEGG.

B<ArgsCount>: 2

=over 4

=item $user_input: (string) (R)

User query string.

=item $max_suggestions: (integer) (O)

Maximum number of strings to return.

=back

B<Return>: (array ref)

An array of string containing suggestion strings.

=cut

sub GetKEGGList
{
    my ($user_input, $max_suggestions) = @_;
    $max_suggestions ||= $MAX_SUGGESTIONS;

    my $db = Greenphyl::SourceDatabase->new($g_dbh, {'selectors' => {'name' => $Greenphyl::SourceDatabase::SOURCE_DATABASE_KEGG}});
    if (!$db)
    {
        Throw('error' => 'Kegg source database not available for auto-completion!');
    }

    return GetSuggestionList({'query' => $user_input, 'extra' => 'db_id = ' . $db->id . ' AND ', 'column' => 'accession', 'table' => 'dbxref', 'max_suggestions' => $max_suggestions });
    # return GetSuggestionList({'query' => $user_input, 'extra' => 'db_id = (SELECT db.id FROM db db WHERE db.name = \'Kegg\') AND ', 'column' => 'accession', 'table' => 'dbxref', 'max_suggestions' => $max_suggestions });
}


=pod

=head2 GetECList

B<Description>: Returns the array of suggestion strings for EC.

B<ArgsCount>: 2

=over 4

=item $user_input: (string) (R)

User query string.

=item $max_suggestions: (integer) (O)

Maximum number of strings to return.

=back

B<Return>: (array ref)

An array of string containing suggestion strings.

=cut

sub GetECList
{
    my ($user_input, $max_suggestions) = @_;
    $max_suggestions ||= $MAX_SUGGESTIONS;
    
    my $db = Greenphyl::SourceDatabase->new($g_dbh, {'selectors' => {'name' => $Greenphyl::SourceDatabase::SOURCE_DATABASE_EC}});
    if (!$db)
    {
        Throw('error' => 'EC source database not available for auto-completion!');
    }

    return GetSuggestionList({'query' => $user_input, 'extra' => 'db_id = ' . $db->id . ' AND ', 'column' => 'accession', 'table' => 'dbxref', 'max_suggestions' => $max_suggestions });
    # return GetSuggestionList({'query' => $user_input, 'extra' => 'db_id = (SELECT db.id FROM db db WHERE db.name = \'EC\') AND ', 'column' => 'accession', 'table' => 'dbxref', 'max_suggestions' => $max_suggestions });
}


=pod

=head2 GetGOList

B<Description>: Returns the array of suggestion strings for GO.

B<ArgsCount>: 2

=over 4

=item $user_input: (string) (R)

User query string.

=item $max_suggestions: (integer) (O)

Maximum number of strings to return.

=back

B<Return>: (array ref)

An array of string containing suggestion strings.

=cut

sub GetGOList
{
    my ($user_input, $max_suggestions) = @_;
    $max_suggestions ||= $MAX_SUGGESTIONS;
    return GetSuggestionList(
        [
            {'query' => $user_input, 'column' => 'name', 'table' => 'go', 'max_suggestions' => $max_suggestions },
            {'query' => $user_input, 'column' => 'code', 'table' => 'go', 'max_suggestions' => $max_suggestions },
        ]
    );
}


=pod

=head2 GetAnyElementList

B<Description>: Returns the array of suggestion strings for all field of search.

B<ArgsCount>: 2

=over 4

=item $user_input: (string) (R)

User query string.

=item $max_suggestions: (integer) (O)

Maximum number of strings to return.

=back

B<Return>: (array ref)

An array of string containing suggestion strings.

=cut

sub GetAnyElementList
{
    my ($user_input, $max_suggestions) = @_;
    $max_suggestions ||= $MAX_SUGGESTIONS;

    my $suggestions = {};
    foreach my $autocomplete_type (keys(%AUTOCOMPLETE_HANDLERS))
    {
        # make sure we won't go in an infinite loop (deep recursion)
        next if (\&GetAnyElementList == $AUTOCOMPLETE_HANDLERS{$autocomplete_type});

        my $suggestion_array = $AUTOCOMPLETE_HANDLERS{$autocomplete_type}->($user_input, $max_suggestions);
        # warn "DEBUG: " . ref($suggestion_array) . " for $autocomplete_type\n";
        # we use a hash to avoid doubles
        foreach (@$suggestion_array)
        {
            if (defined($_) && ($_ ne ''))
            {
                $suggestions->{$_} = $autocomplete_type;
            }
        }
    }

    # alternate suggestion of each type
    my %suggestion_by_type;
    map { $suggestion_by_type{$suggestions->{$_}} ||= []; push(@{$suggestion_by_type{$suggestions->{$_}}}, $_)} sort(keys(%$suggestions));
    my @any_list = ();
    # add 1 result from each remaining suggestion type and loop while we got
    # suggestions and we did not reach the maximum number of suggestions
    while ((scalar(@any_list) < $max_suggestions)
           && (keys(%suggestion_by_type)))
    {
        foreach my $autocomplete_type (keys(%suggestion_by_type))
        {
            push(@any_list, pop(@{$suggestion_by_type{$autocomplete_type}}));
            # remove empty list
            if (!scalar(@{$suggestion_by_type{$autocomplete_type}}))
            {
                delete($suggestion_by_type{$autocomplete_type});
            }
        }
    }

    return [sort(@any_list)];
}



=pod

=head2 GetQuickList

B<Description>: Returns the array of suggestion strings.

B<ArgsCount>: 2

=over 4

=item $user_input: (string) (R)

User query string.

=item $max_suggestions: (integer) (O)

Maximum number of strings to return.

=back

B<Return>: (array ref)

An array of string containing suggestion strings.

=cut

sub GetQuickList
{
    my ($user_input, $max_suggestions) = @_;
    $max_suggestions ||= $MAX_SUGGESTIONS;

    my $suggestions = {};
    # foreach my $autocomplete_type ('family_name', 'go_seq', 'interpro_seq', 'alias')
    foreach my $autocomplete_type ('family_name')
    {
        # make sure we won't go in an infinite loop (deep recursion)
        next if (\&GetAnyElementList == $AUTOCOMPLETE_HANDLERS{$autocomplete_type});

        my $suggestion_array = $AUTOCOMPLETE_HANDLERS{$autocomplete_type}->($user_input, $max_suggestions);
        # warn "DEBUG: " . ref($suggestion_array) . " for $autocomplete_type\n";
        # we use a hash to avoid doubles
        foreach (@$suggestion_array)
        {
            if (defined($_) && ($_ ne ''))
            {
                $suggestions->{$_} = $autocomplete_type;
            }
        }
    }

    # alternate suggestion of each type
    my %suggestion_by_type;
    map { $suggestion_by_type{$suggestions->{$_}} ||= []; push(@{$suggestion_by_type{$suggestions->{$_}}}, $_)} sort(keys(%$suggestions));
    my @any_list = ();
    # add 1 result from each remaining suggestion type and loop while we got
    # suggestions and we did not reach the maximum number of suggestions
    while ((scalar(@any_list) < $max_suggestions)
           && (keys(%suggestion_by_type)))
    {
        foreach my $autocomplete_type (keys(%suggestion_by_type))
        {
            push(@any_list, pop(@{$suggestion_by_type{$autocomplete_type}}));
            # remove empty list
            if (!scalar(@{$suggestion_by_type{$autocomplete_type}}))
            {
                delete($suggestion_by_type{$autocomplete_type});
            }
        }
    }

    return [sort(@any_list)];
}



=pod

=head2 GetAutocompleteContent

B<Description>: Returns the JSON suggestion page content into a string.

B<ArgsCount>: 3

=over 4

=item $autocomplete_type: (string) (U)

Name of the field of search.

=item $user_input: (string) (R)

User query string.

=item $max_suggestions: (integer) (O)

Maximum number of strings to return.

=back

B<Return>: (string)

The JSON suggestion page content including HTML header.

=cut

sub GetAutocompleteContent
{
    my ($autocomplete_type, $user_input, $max_suggestions) = @_;

    $autocomplete_type ||= '';
    $autocomplete_type = lc($autocomplete_type);

    $max_suggestions ||= $MAX_SUGGESTIONS;

    # fall back to the default handler if there's none
    if (!exists($AUTOCOMPLETE_HANDLERS{$autocomplete_type}))
    {
        cluck "Attempt to call unregistered task handler '$autocomplete_type'!";
        $autocomplete_type = 'any';
    }
    
    # generate page and return it
    my $page = header(
        -type    => $Greenphyl::Web::STREAM_TYPES{'json'},
        -expires => "-1d",
    );
    $page .= GetJsonList($AUTOCOMPLETE_HANDLERS{$autocomplete_type}->($user_input, $max_suggestions));

    return $page;
}


# Script options
#################

=pod

=head1 OPTIONS

    autocomplete.pl <q=<query_string>> <search_type=<search_type>> <limit=<max_suggestions>> <timestamp=<timestamp>>

=head2 Parameters

=over 4

=item B<q> (string):

the string to search in database.

=item B<search_type> (string):

The field of search. Can be one of %AUTOCOMPLETE_HANDLERS keys.

=item B<limit> (integer):

The maximum number of suggestions to return.

=item B<timestamp> (integer):

Date (timestamp) of the query. (unused)

=back

=cut


# CODE START
#############

my $search_type = GetParameterValues('search_type');
my $user_input = GetParameterValues('term');
my $max_suggestions = GetParameterValues('limit');

if (!$max_suggestions || ($max_suggestions !~ m/^\d+$/))
{
    $max_suggestions = $MAX_SUGGESTIONS;
}

# replace unwanted characters with jocker '_'
$user_input =~ s/[^\w\-\. ]/_/g;
PrintDebug("Search type: $search_type\nUser input: '$user_input'");

eval
{
  print GetAutocompleteContent($search_type, $user_input, $max_suggestions);
};

my $error;
if ($error = Exception::Class->caught())
{
    print header(
        -type    => $Greenphyl::Web::STREAM_TYPES{'json'},
        -expires => "-1d",
    );
    print to_json(['an error occured']); # if changed, remember to also update greenphyl.js and quick_search.tt
    PrintDebug("An error occured in auto-completion:\n$error");
}

exit(0);

# CODE END
###########


=pod

=head1 AUTHORS

Mathieu ROUARD (Bioversity), m.rouard@cgiar.org

Valentin GUIGNON (Bioversity), v.guignon@cgiar.org

Matthieu CONTE (Syngenta)

=head1 VERSION

Version 1.0.0

Date 30/05/2012

=head1 SEE ALSO

GreenPhyl documentation.

=cut
