#!/usr/bin/perl

=pod

=head1 NAME

family.cgi - display list of gene family and details of each family

=head1 SYNOPSIS

    /family.cgi?id=20827

=head1 REQUIRES

Perl5

=head1 DESCRIPTION

This script runs the a cgi page for family stored in the database.

=cut

use strict;
use warnings;

use lib '../lib';
use lib '../local_lib';
use Carp qw (cluck confess croak);

use Bio::SeqIO;
use Bio::SearchIO;
use Bio::TreeIO;
use URI::Escape;
use Digest::MD5 qw(md5_hex);

use Greenphyl;
use Greenphyl::Web;
use Greenphyl::Web::Access;
use Greenphyl::Tools::MyList;
use Greenphyl::AbstractFamily;
use Greenphyl::Family;
use Greenphyl::CustomFamily;
use Greenphyl::Sequence;
use Greenphyl::CustomSequence;
use Greenphyl::Tools::Sequences;
use Greenphyl::Species;
use Greenphyl::Tools::Families;
use Greenphyl::Tools::Users; # password hash
use Greenphyl::File;
use Greenphyl::TreeNode;




# Script global constants
##########################

=pod

=head1 CONSTANTS

B<$DEBUG>: (boolean)

Enables debug mode.

=cut

our $DEBUG = 0;
our $MAX_SEQUENCE_SEARCH = 3;




# Script global variables
##########################

=pod

=head1 VARIABLES

B<$g_dbh>: (DBI::db)

Current database handler.

=cut

my $g_dbh = GetDatabaseHandler();




# Script global functions
##########################



=pod

=head1 FUNCTIONS

=head2 PrepareBLASTParameters

B<Description>: Prepare BLAST parameters using CGI parameters.

B<ArgsCount>: 1-2

B<Return>: (list)

the array (ref) of BLAST parameters followed by the used input temporary file
name and the used output temporary file name.

=cut

sub PrepareBLASTParameters
{
    my ($input_sequences, $species_code) = @_;
    
    $species_code ||= 'All';

    # get and check BLAST parameters
    my @blast_parameters;

    # check FASTA contains only printable characters
    my $sequence_regexp = '^(?:>[\x20-\x7E]+[\s\r\n]+[a-zA-Z\*\s\r\n]+)+$';
    if ($input_sequences !~ m/$sequence_regexp/)
    {
        Throw('error' => "Invalid BLAST sequence! You may have invalid characters in your sequence or did not include the species code. Please make sure you entered FASTA sequences with no accents or other special characters in sequence names or contents and each sequence name has a species code in capital letters (prefixed with an underscore '_') appended to the original sequence name.", 'log' => "Sequence given:\n$input_sequences\n");
    }

    my $evalue   = '1e-5';
    push(@blast_parameters, '-e', $evalue);
    
    my $database = $species_code;
    push(@blast_parameters, '-d ', GP('BLAST_BANKS_PATH') . '/' . $database); # BLAST file must have same name as the species name

    my $blast_program = 'blastp';
    push(@blast_parameters, '-p', lc($blast_program));

    my $out_number = 3; # 3 best hits
    push(@blast_parameters, '-K', $out_number);
    push(@blast_parameters, '-v', $out_number);
    push(@blast_parameters, '-b', $out_number);

    my $input_temp_file = GP('TEMP_OUTPUT_PATH') . "/tmp_in$$";
    push(@blast_parameters, '-i', $input_temp_file);
    push(@blast_parameters, '-F', 'none', '-a', '2');

    my $output_temp_file = GP('TEMP_OUTPUT_PATH') . "/tmp_out$$";
    push(@blast_parameters, '-o', $output_temp_file);

    $input_temp_file = WriteTemporaryFile($input_sequences, $input_temp_file);
    
    return (\@blast_parameters, $input_temp_file, $output_temp_file);
}


=pod

=head2 LaunchBLAST

B<Description>: Render BLAST form into an HTML string.

B<ArgsCount>: 1

=over 4

=item $blast_parameters: (array ref) (R)

array of BLAST command line parameters.

=back

B<Return>: (float)

the computation time.

=cut

sub LaunchBLAST
{
    my ($blast_parameters) = @_;

    PrintDebug("BLAST CMD: " . join(' ', GP('BLASTALL_COMMAND'), @$blast_parameters));

    # secure (indirect) BLAST call
    my $program_sh;
    open($program_sh, "-|")
        or exec({GP('BLASTALL_COMMAND')}
            GP('BLASTALL_COMMAND'),
            @$blast_parameters
        )
        or confess "ERROR: unable to launch BLAST! $?, $!\n";
}


=pod

=head2 AutosetLevels

B<Description>: auto-sets custom family levels when missing.

B<ArgsCount>: 3

=over 4

=item $subject: (Greenphyl::CustomFamily) (R)

Subject family.

=item $object: (Greenphyl::CustomFamily) (R)

Object family.

=item $relationship: (string) (R)

Relationship. Must contain one of 'parent' or 'child' (case insensitive).
  
=back

B<Return>: (nothing)

=cut

sub AutosetLevels
{
    my ($subject, $object, $relationship) = @_;
    if (!$subject->level && !$object->level)
    {
        if ($relationship =~ m/parent/i)
        {
            $subject->level(1);
            $object->level(2);
            $subject->save();
            $object->save();
        }
        elsif ($relationship =~ m/child/i)
        {
            $subject->level(2);
            $object->level(1);
            $subject->save();
            $object->save();
        }
    }
    elsif (!$subject->level)
    {
        if ($relationship =~ m/parent/i)
        {
            $subject->level($object->level - 1);
            $subject->save();
        }
        elsif ($relationship =~ m/child/i)
        {
            $subject->level($object->level + 1);
            $subject->save();
        }
    }
    elsif (!$object->level)
    {
        if ($relationship =~ m/parent/i)
        {
            $object->level($subject->level + 1);
            $object->save();
        }
        elsif ($relationship =~ m/child/i)
        {
            $object->level($subject->level - 1);
            $object->save();
        }
    }
}


=pod

=head2 GetPreviousRelationship

B<Description>: .

B<ArgsCount>: 1

=over 4

=item $: () (R)

.

=back

B<Return>: ()

.

=cut

sub GetPreviousRelationship
{
    my ($subject_id, $object_id) = @_;
    my $sql_query = '
        SELECT level_delta
        FROM custom_family_relationships
        WHERE
            subject_custom_family_id = ' . $subject_id . '
            AND object_custom_family_id = ' . $object_id . "
            AND type = '$Greenphyl::AbstractFamily::RELATIONSHIP_INHERITANCE'
        ;";
    PrintDebug("SQL Query: $sql_query");
    my ($previous_relationship_delta) = $g_dbh->selectrow_array($sql_query);
    return $previous_relationship_delta;
}


=pod

=head2 RemovePreviousRelationships

B<Description>: .

B<ArgsCount>: 1

=over 4

=item $: () (R)

.

=back

B<Return>: ()

.

=cut

sub RemovePreviousRelationships
{
    my ($parent_id, $child_id) = @_;

    # remove previous relationships
    PrintDebug("Remove previous relationship between the 2 elements");
    my $sql_query = '
        DELETE FROM custom_family_relationships
        WHERE
            (
                (subject_custom_family_id = ' . $parent_id . '
                AND object_custom_family_id = ' . $child_id . ')
            OR
                (object_custom_family_id = ' . $parent_id . '
                AND subject_custom_family_id = ' . $child_id . ")
            )
            AND type = '$Greenphyl::AbstractFamily::RELATIONSHIP_INHERITANCE'
        ;";
    PrintDebug("SQL Query: $sql_query");
    $g_dbh->do($sql_query)
        or confess "ERROR: Failed to remove previous relationships! parent: $parent_id\nchild: $child_id";

    # get parents level
    $sql_query = "
        SELECT
            level
        FROM custom_families
        WHERE
            id = " . $parent_id . "
        ;";
    PrintDebug("SQL Query: $sql_query");
    my ($parent_level) = $g_dbh->selectrow_array($sql_query)
        or confess 'ERROR: Failed to get parent level: ' . $g_dbh->errstr;

    # remove related relationships
    PrintDebug("Get subsets...");
    # get grand-parents set
    $sql_query = "
        SELECT
            object_custom_family_id
        FROM custom_family_relationships
        WHERE
            subject_custom_family_id = " . $parent_id . "
            AND type = '$Greenphyl::AbstractFamily::RELATIONSHIP_INHERITANCE'
            AND 0 > level_delta
        ;";
    PrintDebug("SQL Query: $sql_query");
    my $grand_parent_ids = $g_dbh->selectcol_arrayref($sql_query);
    #    or confess 'ERROR: Failed to fetch grand-parent families: ' . $g_dbh->errstr;

    # get grand-children set
    $sql_query = "
        SELECT
            object_custom_family_id
        FROM custom_family_relationships
        WHERE
            subject_custom_family_id = " . $child_id . "
            AND type = '$Greenphyl::AbstractFamily::RELATIONSHIP_INHERITANCE'
            AND 0 < level_delta
        ;";
    PrintDebug("SQL Query: $sql_query");
    my $grand_child_ids = $g_dbh->selectrow_arrayref($sql_query);
    #    or confess 'ERROR: Failed to fetch grand-parent families: ' . $g_dbh->errstr;

    PrintDebug("Remove related relationship");
    my $child_level = $parent_level + 1;
    if (!$grand_child_ids || @$grand_child_ids)
    {
        PrintDebug("Remove grand-parent relationships");
        # loop on parents
        foreach my $grand_parent_id (@$grand_parent_ids)
        {
            $sql_query = '
                DELETE FROM custom_family_relationships
                WHERE
                    (
                        (subject_custom_family_id = ' . $grand_parent_id . '
                        AND object_custom_family_id = ' . $child_id . ')
                    OR
                        (object_custom_family_id = ' . $grand_parent_id . '
                        AND subject_custom_family_id = ' . $child_id . ")
                    )
                    AND type = '$Greenphyl::AbstractFamily::RELATIONSHIP_INHERITANCE'
                ;";
            PrintDebug("SQL Query: $sql_query");
            $g_dbh->do($sql_query)
                or confess "ERROR: Failed to remove previous relationships! parent: $grand_parent_id\nchild: $child_id";
        }
    }
    elsif (!$grand_parent_ids || @$grand_parent_ids)
    {
        PrintDebug("Remove grand-child relationships");
        # loop on children
        foreach my $grand_child_id (@$grand_child_ids)
        {
            $sql_query = '
                DELETE FROM custom_family_relationships
                WHERE
                    (
                        (subject_custom_family_id = ' . $parent_id . '
                        AND object_custom_family_id = ' . $grand_child_id . ')
                    OR
                        (object_custom_family_id = ' . $parent_id . '
                        AND subject_custom_family_id = ' . $grand_child_id . ")
                    )
                    AND type = '$Greenphyl::AbstractFamily::RELATIONSHIP_INHERITANCE'
                ;";
            PrintDebug("SQL Query: $sql_query");
            $g_dbh->do($sql_query)
                or confess "ERROR: Failed to remove previous relationships! parent: $parent_id\nchild: $grand_child_id";
        }
    }    
    else
    {
        PrintDebug("Remove inter-subset relationships");
        # loop on parents
        foreach my $grand_parent_id (@$grand_parent_ids)
        {
            PrintDebug("Checking relationships with grand-parent $grand_parent_id");
            # loop on children
            foreach my $grand_child_id (@$grand_child_ids)
            {
                PrintDebug("Checking relationships with grand-child $grand_child_id");
                # check if there should still be a link between grand-parent and grand-child
                $sql_query = "
                    SELECT
                        pcfr.object_custom_family_id
                    FROM
                        custom_family_relationships pcfr
                            JOIN custom_families pcf ON (pcf.id = pcfr.object_custom_family_id)
                            JOIN custom_family_relationships ccfr ON (ccfr.object_custom_family_id = pcfr.object_custom_family_id)
                    WHERE
                        pcfr.subject_custom_family_id = " . $grand_parent_id . "
                        AND pcfr.type = '$Greenphyl::AbstractFamily::RELATIONSHIP_INHERITANCE'
                        AND 0 < pcfr.level_delta
                        AND pcf.level = $parent_level
                        AND ccfr.subject_custom_family_id = " . $grand_child_id . "
                        AND 0 > ccfr.level_delta
                        AND pcfr.object_custom_family_id != $parent_id
                    UNION
                    SELECT
                        ccfr.object_custom_family_id
                    FROM
                        custom_family_relationships ccfr
                            JOIN custom_families ccf ON (ccf.id = ccfr.object_custom_family_id)
                            JOIN custom_family_relationships pcfr ON (pcfr.object_custom_family_id = ccfr.object_custom_family_id)
                    WHERE
                        ccfr.subject_custom_family_id = " . $grand_child_id . "
                        AND ccfr.type = '$Greenphyl::AbstractFamily::RELATIONSHIP_INHERITANCE'
                        AND 0 > ccfr.level_delta
                        AND ccf.level = $child_level
                        AND pcfr.subject_custom_family_id = " . $grand_parent_id . "
                        AND 0 < pcfr.level_delta
                        AND ccfr.object_custom_family_id != $child_id
                    ;";
                PrintDebug("SQL Query: $sql_query");
                my ($still_has_link) = $g_dbh->selectrow_array($sql_query);
                if (!$still_has_link)
                {
                    # remove links
                    PrintDebug("No more link between $grand_parent_id and $grand_child_id");
                    $sql_query = '
                        DELETE FROM custom_family_relationships
                        WHERE
                            (
                                (subject_custom_family_id = ' . $grand_parent_id . '
                                AND object_custom_family_id = ' . $grand_child_id . ')
                            OR
                                (object_custom_family_id = ' . $grand_parent_id . '
                                AND subject_custom_family_id = ' . $grand_child_id . ")
                            )
                            AND type = '$Greenphyl::AbstractFamily::RELATIONSHIP_INHERITANCE'
                    ;";
                    PrintDebug("SQL Query: $sql_query");
                    $g_dbh->do($sql_query)
                        or confess "ERROR: Failed to remove relative relationships: " . $g_dbh->errstr;
                }
                else
                {
                    PrintDebug("There is still a relationships between grand-parent $grand_parent_id and grand-child $grand_child_id");
                }
            }
        }
    }
}



=pod

=head2 SetNewRelationship

B<Description>: .

B<ArgsCount>: 1

=over 4

=item $: () (R)

.

=back

B<Return>: ()

.

=cut

sub SetNewRelationships
{
    my ($parent_id, $child_id) = @_;

    # insert new relationships
    my $sql_query = "
        INSERT IGNORE INTO custom_family_relationships (
            subject_custom_family_id,
            object_custom_family_id,
            level_delta,
            type
        )
        -- child/parent
        SELECT
            " . $child_id . ",
            " . $parent_id . ",
            -1,
            '$Greenphyl::AbstractFamily::RELATIONSHIP_INHERITANCE'
        UNION
        -- parent/child
        SELECT
            " . $parent_id . ",
            " . $child_id . ",
            1,
            '$Greenphyl::AbstractFamily::RELATIONSHIP_INHERITANCE'
        UNION
        -- grand-parent/child
        SELECT
            " . $child_id . ",
            object_custom_family_id,
            level_delta - 1,
            '$Greenphyl::AbstractFamily::RELATIONSHIP_INHERITANCE'
        FROM custom_family_relationships
        WHERE
            subject_custom_family_id = " . $parent_id . "
            AND type = '$Greenphyl::AbstractFamily::RELATIONSHIP_INHERITANCE'
            AND 0 > level_delta
        UNION
        -- child/grand-parent
        SELECT
            object_custom_family_id,
            " . $child_id . ",
            1 - level_delta,
            '$Greenphyl::AbstractFamily::RELATIONSHIP_INHERITANCE'
        FROM custom_family_relationships
        WHERE
            subject_custom_family_id = " . $parent_id . "
            AND type = '$Greenphyl::AbstractFamily::RELATIONSHIP_INHERITANCE'
            AND 0 > level_delta
        UNION
        -- parent/grand-child
        SELECT
            " . $parent_id . ",
            object_custom_family_id,
            1 - level_delta,
            '$Greenphyl::AbstractFamily::RELATIONSHIP_INHERITANCE'
        FROM custom_family_relationships
        WHERE
            subject_custom_family_id = " . $child_id . "
            AND type = '$Greenphyl::AbstractFamily::RELATIONSHIP_INHERITANCE'
            AND 0 < level_delta
        UNION
        -- grand-child/parent
        SELECT
            object_custom_family_id,
            " . $parent_id . ",
            level_delta - 1,
            '$Greenphyl::AbstractFamily::RELATIONSHIP_INHERITANCE'
        FROM custom_family_relationships
        WHERE
            subject_custom_family_id = " . $child_id . "
            AND type = '$Greenphyl::AbstractFamily::RELATIONSHIP_INHERITANCE'
            AND 0 < level_delta
        ;";
    PrintDebug("SQL Query: $sql_query");
    $g_dbh->do($sql_query)
        or Throw('error' => 'Failed to insert new relationships!');
}


=pod

=head2 RenderUsePassword

B<Description>: Renders a single family identification card with family details.

B<ArgsCount>: 0

B<Return>: (string)

Returns an HTML string containing the family identification card.

=cut

sub RenderUsePassword
{
    my $custom_family = LoadCustomFamily();

    # make sure the family is in database
    if (!$custom_family)
    {
        Throw('error' => "Custom family not found in database!");
    }

    my $password = GetParameterValues('cfp');
    my $password_hash = GetParameterValues('cfph');
    if (defined($password) && ($password ne ''))
    {
        ($password_hash) = HashPassword($password, $custom_family->salt);
    }

    if ($password_hash && ($password_hash =~ m/\w{40}/))
    {
        my $session = Greenphyl::Web::GetSession();
        my $custom_family_access = $session->param('custom_family_access');
        $custom_family_access ||= {};
        $custom_family_access->{$custom_family->id} = lc($password_hash);
        $session->param('custom_family_access', $custom_family_access);
    }

    return RenderCustomFamilyIdCard($custom_family);
}


=pod

=head2 RenderCustomFamilyIdCard

B<Description>: Renders a single family identification card with family details.

B<ArgsCount>: 0

B<Return>: (string)

Returns an HTML string containing the family identification card.

=cut

sub RenderCustomFamilyIdCard
{
    # remove "page" parameter when called from ProceedPage
    if (@_ && ('HASH' eq ref($_[0])))
    {
        shift(@_);
    }
    my ($custom_family) = @_;

    InitAccess();

    if (!$custom_family)
    {
        $custom_family = LoadCustomFamily();
    }

    # make sure the family is in database
    if (!$custom_family)
    {
        Throw('error' => "Custom family not found in database!");
    }

    # check if current user can view the custom family
    if (!$custom_family->hasReadAccess() && !IsAdmin()) 
    {
        return RenderHTMLFullPage(
            {
                'title'         => 'Family ' . $custom_family->accession,
                'content'       => 'families/custom_family_id_card.tt',
                'family'        => $custom_family,
            },
        );
    }
    
    $custom_family->setHighlight(1);

    # my @species = sort { $a->name cmp $b->name } Greenphyl::Species->new($g_dbh, {'selectors' => { 'display_order' => ['>', 0], },});
    my @species = sort { $a->name cmp $b->name } @{$custom_family->species};

    my $family_composition_data  = GenerateCustomFamilyCompositionData($custom_family);
    # my $family_protein_domains   = GenerateFamilyProteinDomainData($custom_family);
    my $family_protein_list      = GenerateFamilyProteinListData($custom_family);
    my $phylogenomic_analysis    = GenerateCustomPhylogenomicAnalysisData($custom_family);
    
    # custom family types
    my $cluster_types = [ GetTableColumnValues('custom_families', 'type') ];
    my $inferences    = [ GetTableColumnValues('custom_families', 'inferences') ];
    # my $access        = [ GetTableColumnValues('custom_families', 'access') ];
    # disable group restrictions
    my $access        = [ grep { $_ !~ /group/i } GetTableColumnValues('custom_families', 'access') ];

    my $tabs = [
        {
            'id'      => 'tab-famcomp',
            'title'   => 'Family Composition',
            'content' => 'family_tools/family_composition.tt',
        },
        {
            'id'      => 'tab-famstruct',
            'title'   => 'Family Structure',
            'content' => 'families/custom_family_relationship.tt',
        },
        # {
        #     'id'      => 'tab-protdom',
        #     'title'   => 'Protein Domains',
        #     'content' => 'family_tools/protein_domains.tt',
        # },
        {
            'id'      => 'tab-protlist',
            'title'   => 'Protein List',
            'content' => 'family_tools/protein_list.tt',
        },
        # {
        #     'id'      => 'tab-homopred',
        #     'title'   => 'Homolog Predictions',
        #     'content' => 'family_tools/homolog_predictions.tt',
        # },
    ];

    my @related_custom_families;
    my %common_sequence_counts;
    if ($custom_family->hasWriteAccess() || IsAdmin())
    {
        # add relationship management data
        # find sequence-related families
        my $sql_query = '
            SELECT
                DISTINCT ' . join(', ', Greenphyl::CustomFamily->GetDefaultMembers('cf')) . ',
                COUNT(cfs2.custom_sequence_id) AS "common_count"
            FROM
                custom_families_sequences cfs
                JOIN custom_families_sequences cfs2 ON (cfs2.custom_family_id != cfs.custom_family_id AND cfs2.custom_sequence_id = cfs.custom_sequence_id)
                JOIN custom_families cf ON (cf.id = cfs2.custom_family_id)
            WHERE
                cfs.custom_family_id = ' . $custom_family->id . '
                AND cf.user_id = ' . $custom_family->user_id . '
            GROUP BY cfs2.custom_family_id ASC
            ;
        ';
        my $sql_custom_families = $g_dbh->selectall_arrayref($sql_query, { 'Slice' => {} })
            or confess 'ERROR: Failed to fetch related custom families with common sequences: ' . $g_dbh->errstr;
        foreach my $sql_custom_family (@$sql_custom_families)
        {
            my $related_custom_family = Greenphyl::CustomFamily->new($g_dbh, {'load' => 'none', 'members' => $sql_custom_family});
            push(@related_custom_families, $related_custom_family);
            $common_sequence_counts{$related_custom_family->id} = $sql_custom_family->{'common_count'} || 0;
        }
        @related_custom_families = sort { $common_sequence_counts{$b->id} <=> $common_sequence_counts{$a->id} } @related_custom_families;

        push(
            @$tabs,
            {
                'id'      => 'tab-relman',
                'title'   => 'Family Relationship',
                'content' => 'family_tools/custom_family_relationship_management.tt',
            },
        );

        # add sequence association management data
        push(
            @$tabs,
            {
                'id'      => 'tab-seqman',
                'title'   => 'Sequence Management',
                'content' => 'family_tools/custom_family_sequence_management.tt',
            },
        );
    }

    # phylogeny analyses
    push(@$tabs,
        {
            'id'      => 'tab-phyloana',
            'title'   => 'Phylogenomic Analysis',
            'content' => 'family_tools/phylogenomic_analysis.tt',
        },
    );

    return RenderHTMLFullPage(
        {
            'title'         => 'Family ' . $custom_family->accession,
            'content'       => 'family_tools/family_details.tt',
            'javascript'    => ['d3-sankey.min', 'raphael', 'raphael-zpd', 'g.raphael', 'g.bar', 'chart', 'ipr', 'relationship_graph', 'families',],
            'family'        => $custom_family,
            'species_list'  => \@species,
            'cluster_types' => $cluster_types,
            'inferences'    => $inferences,
            'access_levels' => $access,
            'related_custom_families' => \@related_custom_families,
            'common_sequence_counts'  => \%common_sequence_counts,

            # tab-specific data
            # family composition
            'family_composition'     => $family_composition_data,
            # protein domains
            # 'family_protein_domains' => $family_protein_domains,
            # protein list
            'family_protein_list'    => $family_protein_list,
            # phylogenomic analysis
            'phylogenomic_analysis'  => $phylogenomic_analysis,

            # tabs definition
            'contents' => $tabs,
        },
    );
}


=pod

=head2 SaveCustomFamily

B<Description>: Saves modifications made to a custom family.

B<ArgsCount>: 0

B<Return>: (string)

.

=cut

sub SaveCustomFamily
{
    InitAccess();

    my $requirements = {
        'require_access' => [GP('ACCESS_KEY_REGISTERED_USER'),],
        'access_needs'   => 'any',
    };

    my $custom_family = LoadCustomFamily();
    
    my $current_user = GetCurrentUser();

    # check if current family is editable by current user
    if (!$custom_family->hasWriteAccess($current_user) && !IsAdmin()) 
    {
        ThrowAccessDenied('error' => 'You are not allowed to edit this family!');
    }
    
    # update fields
    # -custom_family_name
    my $custom_family_name = GetParameterValues('custom_family_name');
    if ($custom_family_name)
    {
        $custom_family->name($custom_family_name);
    }

    # flags management
    my $flags = $custom_family->flags();
    $flags =~ s/,?$Greenphyl::CustomFamily::CUSTOM_FAMILY_FLAG_LISTED//gi;
    $flags =~ s/,?$Greenphyl::CustomFamily::CUSTOM_FAMILY_FLAG_OBSOLETE//gi;
    $flags =~ s/^,//gi;
    my @flags = split(/,/, $flags);
    
    # -custom_family_listed
    my $custom_family_listed = GetParameterValues('custom_family_listed');
    if ($custom_family_listed)
    {
        push(@flags, $Greenphyl::CustomFamily::CUSTOM_FAMILY_FLAG_LISTED);
    }
    # -custom_family_obsolete
    my $custom_family_obsolete = GetParameterValues('custom_family_obsolete');
    if ($custom_family_obsolete)
    {
        push(@flags, $Greenphyl::CustomFamily::CUSTOM_FAMILY_FLAG_OBSOLETE);
    }

    $custom_family->flags(join(',', @flags));

    # -custom_family_level
    my $custom_family_level = GetParameterValues('custom_family_level');
    if ($custom_family_level)
    {
        ChangeLevel($custom_family, $custom_family_level);
    }

    # -custom_family_type
    my $custom_family_type = GetParameterValues('custom_family_type');
    if ($custom_family_type)
    {
        $custom_family->type($custom_family_type);
    }

    # -custom_family_inferences
    my @custom_family_inferences = GetParameterValues('custom_family_inferences');
    if (@custom_family_inferences)
    {
        $custom_family->inferences(join(',', @custom_family_inferences));
    }

    # -custom_family_password
    my $custom_family_password = GetParameterValues('custom_family_password');
    if (defined($custom_family_password))
    {
        $custom_family->password($custom_family_password);
    }

    # -custom_family_access
    my $custom_family_access = GetParameterValues('custom_family_access');
    if ($custom_family_access)
    {
        $custom_family->access($custom_family_access);
        # check if no password was set
        if (($custom_family_access =~ m/password/i)
            && (!defined($custom_family->password) || ($custom_family->password eq '')))
        {
            # set a random password
            $custom_family_password = '';
            for (1..16)
            {
                $custom_family_password .= chr(32+int(rand(95)));
            }
            $custom_family->password($custom_family_password);
        }
    }

    # -custom_family_description
    my $custom_family_description = GetParameterValues('custom_family_description');
    if ($custom_family_description)
    {
        $custom_family->description($custom_family_description);
    }

    # sequence list
    my @sequence_ids = GetParameterValues('cseq_id', undef, '[,;\s]+');
    if (@sequence_ids)
    {
        PrintDebug('Update sequence list');
        my $sql_query;
        # get mode
        if (GetParameterValues('add_seq'))
        {
            my $class = GetParameterValues('class') || '';
            if ($class =~ m/custom_sequence/)
            {
                while (my @seq_ids_to_process = splice(@sequence_ids, 0, 250))
                {
                    $sql_query = "
                        INSERT IGNORE INTO custom_families_sequences (
                                custom_family_id,
                                custom_sequence_id
                            )
                        VALUES
                        (" . $custom_family->id . ', ?' . ")
                        " . (", (" . $custom_family->id . ', ?' . ")" x $#seq_ids_to_process) . "
                        ;
                    ";
                    PrintDebug("SQL Query: $sql_query\nBindings: " . join(', ', @seq_ids_to_process));
                    $g_dbh->do($sql_query, undef, @seq_ids_to_process)
                        or Throw('error' => 'Failed to add custom family/sequence links!');
                }
            }
            else
            {
                #+FIXME: clone sequence and add custom copy
            }
        }
        elsif (GetParameterValues('remove_seq'))
        {
            PrintDebug('Remove sequences');
            while (my @seq_ids_to_process = splice(@sequence_ids, 0, 250))
            {
                $sql_query = "
                    DELETE
                    FROM custom_families_sequences
                    WHERE custom_family_id = " . $custom_family->id . "
                        AND custom_sequence_id IN (?" . (', ?' x $#seq_ids_to_process) . ")
                    ;
                ";
                PrintDebug("SQL Query: $sql_query\nBindings: " . join(', ', @seq_ids_to_process));
                $g_dbh->do($sql_query, undef, @seq_ids_to_process)
                    or Throw('error' => 'Failed to remove custom family/sequence links!');
            }

            #+FIXME: remove family if empty?
            #+FIMXE: remove orphan sequences?
        }
    }
    else
    {
        PrintDebug('No sequence selection found');
    }

    # update custom family
    $custom_family->save();

    return Redirect(GetURL('fetch_custom_family', {'accession' => $custom_family->accession}));
}


=pod

=head2 SaveMyListToCustomFamily

B<Description>: Saves MyList sequences and families into a new custom family.

B<ArgsCount>: 0

B<Return>: (string)

.

=cut

sub SaveMyListToCustomFamily
{
    InitAccess();

    my $requirements = {
        'require_access' => [GP('ACCESS_KEY_REGISTERED_USER'),],
        'access_needs'   => 'any',
    };

    my $current_user = GetCurrentUser();

    if (!CheckAccess($requirements, $current_user))
    {
        ThrowAccessDenied('error' => 'Only registered users are allowed to create custom families! Please apply for an account or login if you already have one.');
    }

    # get sequences
    my $sequences = GetMyListAllSequences();

    if (!@$sequences)
    {
        Throw('error' => 'You must have at least one sequence selected to create a new custom family!');
    }

    # get a new temporary accession
    my $temp_accession = sprintf("CT%06d", $$);

    # save unamed family to database
    my $sql_query = "
        INSERT INTO custom_families (
            id,
            user_id,
            access,
            name,
            accession,
            type,
            description,
            inferences,
            data,
            creation_date,
            last_update
        )
        VALUES(
            DEFAULT,
            " . $current_user->id . ",
            '$Greenphyl::CustomFamily::CUSTOM_FAMILY_ACCESS_PRIVATE',
            '$Greenphyl::AbstractFamily::UNANNOTATED_CLUSTER_NAME',
            '$temp_accession',
            'N/A',
            '',
            '',
            NULL,
            NOW(),
            NOW()
        );
    ";
    PrintDebug("SQL Query: $sql_query");
    $g_dbh->do($sql_query)
        or Throw('error' => 'Failed to insert custom family into database!');
    # load family from database
    my $custom_family = Greenphyl::CustomFamily->new(
        $g_dbh,
        {
            'selectors' =>
                {
                    'accession' => $temp_accession,
                    'user_id'   => $current_user->id,
                },
        }
    );

    if (!$custom_family)
    {
        Throw('error' => 'Failed to retrieve inserted custom family from database!');
    }

    # update accession: generate a real one and replace temporary accession
    $custom_family->accession(sprintf('CF%06d', $custom_family->id));
    $custom_family->save();

    # add sequences
    AddSequencesToCustomFamily($custom_family, $sequences);

    return Redirect(GetURL('fetch_custom_family', {'accession' => $custom_family->accession}));
}


=pod

=head2 AddMyListToCustomFamily

B<Description>: Adds MyList sequences and families to current custom family.

B<ArgsCount>: 0

B<Return>: (string)

.

=cut

sub AddMyListToCustomFamily
{
    InitAccess();

    my $custom_family = LoadCustomFamily();

    # make sure the family is in database
    if (!$custom_family)
    {
        Throw('error' => "Custom family not found in database!");
    }

    # check if current user can edit the custom family
    if (!$custom_family->hasWriteAccess() && !IsAdmin()) 
    {
        ThrowAccessDenied('error' => 'You are not allowed to edit this family!');
    }

    my $requirements = {
        'require_access' => [GP('ACCESS_KEY_REGISTERED_USER'),],
        'access_needs'   => 'any',
    };

    my $current_user = GetCurrentUser();

    if (!CheckAccess($requirements, $current_user))
    {
        ThrowAccessDenied('error' => 'Only registered user are allowed to add sequence to custom families! Please apply for an account or login if you already have one.');
    }

    # get sequences
    my $sequences = GetMyListAllSequences();

    if (!@$sequences)
    {
        Throw('error' => 'You must have at least one sequence selected to create a new custom family!');
    }

    # add sequences
    AddSequencesToCustomFamily($custom_family, $sequences);

    return Redirect(GetURL('fetch_custom_family', {'accession' => $custom_family->accession}));
}


=pod

=head2 AddSequencesPhase1

B<Description>: Adds given sequences to current custom family.

B<ArgsCount>: 0

B<Return>: (string)

.

=cut

sub AddSequencesPhase1
{
    InitAccess();

    my $custom_family = LoadCustomFamily();

    # make sure the family is in database
    if (!$custom_family)
    {
        Throw('error' => "Custom family not found in database!");
    }

    # check if current user can edit the custom family
    if (!$custom_family->hasWriteAccess() && !IsAdmin()) 
    {
        ThrowAccessDenied('error' => 'You are not allowed to edit this family!');
    }

    my $requirements = {
        'require_access' => [GP('ACCESS_KEY_REGISTERED_USER'),],
        'access_needs'   => 'any',
    };

    my $current_user = GetCurrentUser();

    if (!CheckAccess($requirements, $current_user))
    {
        ThrowAccessDenied('error' => 'Only registered user are allowed to add sequence to custom families! Please apply for an account or login if you already have one.');
    }

    my $species_sequences = CheckUserProvidedSequences();

    return RenderHTMLFullPage(
        {
            'title'         => 'Add custom sequences to family',
            'content'       => 'family_tools/custom_family_add_sequences_form2.tt',
            'species_sequences' => $species_sequences,
            'family'        => $custom_family,
            'form_data' =>
            {
                'identifier' => 'family_sequence_add_form',
                'action'     => GetURL('add_custom_sequences_to_family', {'phase2' => 1,}),
                'submit'     => 'Add sequences',
                'noclear'    => 1,
            },
        },
    );
}


=pod

=head2 AddSequencesPhase2

B<Description>: Adds given sequences to current custom family.

B<ArgsCount>: 0

B<Return>: (string)

.

=cut

sub AddSequencesPhase2
{
    InitAccess();

    my $custom_family = LoadCustomFamily();

    # make sure the family is in database
    if (!$custom_family)
    {
        Throw('error' => "Custom family not found in database!");
    }

    # check if current user can edit the custom family
    if (!$custom_family->hasWriteAccess() && !IsAdmin()) 
    {
        ThrowAccessDenied('error' => 'You are not allowed to edit this family!');
    }

    my $requirements = {
        'require_access' => [GP('ACCESS_KEY_REGISTERED_USER'),],
        'access_needs'   => 'any',
    };

    my $current_user = GetCurrentUser();

    if (!CheckAccess($requirements, $current_user))
    {
        ThrowAccessDenied('error' => 'Only registered user are allowed to add sequence to custom families! Please apply for an account or login if you already have one.');
    }

    # get sequences
    my ($custom_sequences, $sequence_relationships) = GetValidatedUserProvidedSequences();

    if (!@$custom_sequences)
    {
        Throw('error' => 'You must have at least one sequence selected to add to your custom family!');
    }

    # add sequences
    AddSequencesToCustomFamily($custom_family, $custom_sequences, $sequence_relationships);

    return Redirect(GetURL('fetch_custom_family', {'accession' => $custom_family->accession}));
}


=pod

=head2 CreateCustomFamilyForm

B<Description>: Saves MyList sequences and families into a new custom family.

B<ArgsCount>: 0

B<Return>: (string)

.

=cut

sub CreateCustomFamilyForm
{
    InitAccess();

    my $requirements = {
        'require_access' => [GP('ACCESS_KEY_REGISTERED_USER'),],
        'access_needs'   => 'any',
    };

    my $current_user = GetCurrentUser();

    if (!CheckAccess($requirements, $current_user))
    {
        ThrowAccessDenied('error' => 'Only registered user are allowed to create custom families! Please apply for an account or login if you already have one.', 'access' => $requirements);
    }

    return RenderHTMLFullPage(
        {
            'title'         => 'Create new custom family',
            'content'       => 'family_tools/custom_family_create_form.tt',
        },
    );

}


=pod

=head2 CreateCustomFamilyForm2

B<Description>: Saves MyList sequences and families into a new custom family.

B<ArgsCount>: 0

B<Return>: (string)

.

=cut

sub CheckUserProvidedSequences
{
    # hash of sequence data associated to species
    # first-level keys are species codes
    # second-level keys are sequence accessions
    # values are polypeptide sequences
    # note: there is the empty species code '' for missing species
    my %species_sequences;

    # get list of set of sequences
    my @set_list = GetParameterValues('set_list', undef, '[,;\s]+');

    # process each set
    foreach my $set_index (@set_list)
    {
        # get specified species
        my $current_species = GetParameterValues('species_code_' . $set_index);
        # get associated sequences
        my $fasta_sequences = GetParameterValues('sequences_' . $set_index);
        my $fasta_file_sequences = GetFileContentFromCGI('sequence_file_' . $set_index);
        if ($fasta_file_sequences)
        {
            $fasta_sequences .= "\n$fasta_file_sequences";
        }
        my $user_bio_sequences = {};

        my ($istringfh);
        open($istringfh, "<", \$fasta_sequences)
            or confess "Could not open string for reading: $!";   # Use this for Perl AFTER 5.8.0 (inclusive)

        my $seqio = Bio::SeqIO->new(
                '-fh'     => $istringfh,
                '-format' => 'fasta',
            );

        while (my $bio_seq = $seqio->next_seq)
        {
            # check type
            if ('protein' ne $bio_seq->alphabet)
            {
                # invalid sequence type
                Throw('error' => "The sequence '" . $bio_seq->id . "' doesn't appear to be a protein sequence! Only protein sequences are allowed!", 'log' => "Detected sequence type: " . $bio_seq->alphabet);
            }

            $user_bio_sequences->{$bio_seq->id} = $bio_seq;
            PrintDebug("Got user sequence: " . $bio_seq->id . "\n" . $bio_seq->seq());
        }

        if ($current_species && ($current_species =~ m/^$Greenphyl::Species::SPECIES_CODE_REGEXP$/))
        {
            foreach my $bio_sequence (values(%$user_bio_sequences))
            {
                # species code given in accession
                $species_sequences{$current_species} ||= {};
                $species_sequences{$current_species}->{$bio_sequence->id} = $bio_sequence->seq();
            }            
        }
        else
        {
            # species not specified or invalid, check all sequence names
            foreach my $bio_sequence (values(%$user_bio_sequences))
            {
                # check if accession includes species codes
                my $sequence_name_regexp = '^([\x20-\x7E]+)_(' . $Greenphyl::Species::SPECIES_CODE_REGEXP . ')\s*$';
                if ($bio_sequence->id =~ m/$sequence_name_regexp/)
                {
                    # species code given in accession
                    my ($accession, $species_code) = ($1, $2);
                    $species_sequences{$species_code} ||= {};
                    $species_sequences{$species_code}->{$accession} = $bio_sequence->seq();
                }
                else
                {
                    # no species code
                    $species_sequences{''} ||= {};
                    $species_sequences{''}->{$bio_sequence->id} = {
                        'polypeptide' => $bio_sequence->seq(),
                        'organism'    => $current_species,
                    };
                }
            }
        }
    }

    # check given species codes
    foreach my $species_code (keys(%species_sequences))
    {
        if ($species_code ne '')
        {
            my $species = Greenphyl::Species->new(
                $g_dbh,
                {
                    'selectors' => {'code' => $species_code,},
                }
            );

            if (!$species)
            {
                # species not found!
                PrintDebug("WARNING: Species code '$species_code' not found in database!");
                # transfer sequences to the unknown species set
                $species_sequences{''} ||= {};
                foreach my $accession (keys(%{$species_sequences{$species_code}}))
                {
                    if (!exists($species_sequences{''}->{$accession}))
                    {
                        $species_sequences{''}->{$accession} = {
                            'polypeptide' => $species_sequences{$species_code}->{$accession},
                            'organism'    => $species_code,
                        };
                    }
                }
                delete($species_sequences{$species_code});
            }
        }
    }

    return \%species_sequences;
}


=pod

=head2 CreateCustomFamilyForm2

B<Description>: Saves MyList sequences and families into a new custom family.

B<ArgsCount>: 0

B<Return>: (string)

.

=cut

sub CreateCustomFamilyForm2
{
    InitAccess();

    my $requirements = {
        'require_access' => [GP('ACCESS_KEY_REGISTERED_USER'),],
        'access_needs'   => 'any',
    };

    my $current_user = GetCurrentUser();

    if (!CheckAccess($requirements, $current_user))
    {
        ThrowAccessDenied('error' => 'Only registered user are allowed to create custom families! Please apply for an account or login if you already have one.');
    }

    my $species_sequences = CheckUserProvidedSequences();

    return RenderHTMLFullPage(
        {
            'title'         => 'Create new custom family',
            'content'       => 'family_tools/custom_family_create_form2.tt',
            'species_sequences' => $species_sequences,
#            'exact_matches' => \%exact_matches,
#            'blast_matches' => \%blast_matches,
        },
    );

}


=pod

=head2 GetValidatedUserProvidedSequences

B<Description>: .

B<ArgsCount>: 0

B<Return>: (string)

.

=cut

sub GetValidatedUserProvidedSequences
{
    # get current user
    my $current_user = GetCurrentUser();

    # get number of sequence to process
    my $sequence_count = GetParameterValues('sequence_count');

    if (!$sequence_count)
    {
        Throw('error' => 'You must have at least one sequence for your custom family!');
    }

    my @custom_sequences;
    my %sequence_relationships;
    for (my $sequence_index = 0; $sequence_index < $sequence_count; ++$sequence_index)
    {
        # get accession
        my $accession = uri_unescape(GetParameterValues("sequence_accession_$sequence_index"));

        # get polypeptide
        my $polypeptide = uri_unescape(GetParameterValues("sequence_polypeptide_$sequence_index"));

        # get species code
        my $species_code = uri_unescape(GetParameterValues("species_code_$sequence_index"));
        my ($species, $organism);
        if ($species_code)
        {
            $species = Greenphyl::Species->new(
                $g_dbh,
                {
                    'selectors' => {
                        'code' => $species_code,
                    },
                }
            );
        }

        # if no valid species has been specified, use organism field
        if (!$species)
        {
            $species = Greenphyl::Species->new(
                $g_dbh,
                {
                    'selectors' => {
                        'code' => GP('NONUNIPROT_SPECIES_CODE'),
                    },
                }
            );
            $organism = $species_code;
            # clean user input
            $organism =~ s/[^\w\s\-+=\\\/\.,;:!?'"#~{}\[\]\(\)]//g;
        }

        # get relationship
        my $relationship = GetParameterValues("sequence_relation_$sequence_index");

        if ($accession && $polypeptide && $species)
        {
            my ($locus, $splice_form) = ExtractLocusAndSpliceForm($accession, $species->code);
            my $custom_sequence = Greenphyl::CustomSequence->new(
                $g_dbh,
                {
                    'load' => 'none',
                    'members' => {
                        'accession'       => $accession,
                        'locus'           => $locus,
                        'splice'          => $splice_form,
                        'splice_priority' => (defined($splice_form)?0:undef),
                        'length'          => length($polypeptide),
                        'species_id'      => $species->id,
                        'organism'        => $organism,
                        'annotation'      => '',
                        'polypeptide'     => $polypeptide,
                        'user_id'         => $current_user->id,
                    },
                },
            );

            # add sequence to the list
            push(@custom_sequences, $custom_sequence);
            # save its relationship
            if ($relationship)
            {
                $sequence_relationships{$custom_sequence->accession} = $relationship;
            }
        }
        else
        {
            PrintDebug('Incomplete sequence ' . ($accession ? "($accession)" : '') . ' skipped!');
        }
    }

    return (\@custom_sequences, \%sequence_relationships);
}


=pod

=head2 CreateCustomFamily

B<Description>: Saves MyList sequences and families into a new custom family.

B<ArgsCount>: 0

B<Return>: (string)

.

=cut

sub CreateCustomFamily
{
    InitAccess();

    my $requirements = {
        'require_access' => [GP('ACCESS_KEY_REGISTERED_USER'),],
        'access_needs'   => 'any',
    };

    my $current_user = GetCurrentUser();

    if (!CheckAccess($requirements, $current_user))
    {
        ThrowAccessDenied('error' => 'Only registered user are allowed to create custom families! Please apply for an account or login if you already have one.');
    }

    my ($custom_sequences, $sequence_relationships) = GetValidatedUserProvidedSequences();

    if (!@$custom_sequences)
    {
        Throw('error' => 'You must have at least one valid sequence to create a new custom family!');
    }

    # get new family name if one
    my $family_name = GetParameterValues("family_name") || $Greenphyl::AbstractFamily::UNANNOTATED_CLUSTER_NAME;

    # get a new temporary accession
    my $temp_accession = sprintf("CT%06d", $$);

    # save unamed family to database
    my $sql_query = "
        INSERT INTO custom_families (
            id,
            user_id,
            access,
            name,
            accession,
            type,
            description,
            inferences,
            data,
            creation_date,
            last_update
        )
        VALUES(
            DEFAULT,
            " . $current_user->id . ",
            '$Greenphyl::CustomFamily::CUSTOM_FAMILY_ACCESS_PRIVATE',
            '$family_name',
            ?,
            'N/A',
            '',
            '',
            NULL,
            NOW(),
            NOW()
        );
    ";

    PrintDebug("SQL Query: $sql_query\nBindings: '$temp_accession'");
    $g_dbh->do($sql_query, undef, $temp_accession)
        or Throw('error' => 'Failed to insert custom family into database!');
    # load family from database
    my $custom_family = Greenphyl::CustomFamily->new(
        $g_dbh,
        {
            'selectors' =>
                {
                    'accession' => $temp_accession,
                    'user_id'   => $current_user->id,
                },
        }
    );

    if (!$custom_family)
    {
        Throw('error' => 'Failed to retrieve inserted custom family from database!');
    }

    # update accession: generate a real one and replace temporary accession
    $custom_family->accession(sprintf('CF%06d', $custom_family->id));
    $custom_family->save();

    # add sequences
    AddSequencesToCustomFamily($custom_family, $custom_sequences, $sequence_relationships);

    return Redirect(GetURL('fetch_custom_family', {'accession' => $custom_family->accession}));
}



=pod

=head2 ChangeLevel

B<Description>: change a custom family level and update all related families.

B<ArgsCount>: 2

=over 4

=item $custom_family: (Greenphyl::CustomFamily) (R)

Subject family.

=item $level: (integer) (R)

new level.

=back

B<Return>: (nothing)

=cut

sub ChangeLevel
{
    my ($custom_family, $level) = @_;
    
    # check if family could have related families thats should be also updated
    if ($custom_family->level && $level)
    {
        # get all related families
        my %related_families_hash = ($custom_family->id => $custom_family);
        my $families_to_check = $custom_family->getRelatedFamilies();
        while (@$families_to_check)
        {
            my $related_family = pop(@$families_to_check);
            if (!exists($related_families_hash{$related_family->id}))
            {
                $related_families_hash{$related_family->id} = $related_family;
                push(@$families_to_check, @{$custom_family->getRelatedFamilies()});
            }
        }

        # check for each related family if the level change is ok (for its own relationships)
        my ($min_level, $max_level) = ($custom_family->level, $custom_family->level);
        foreach my $related_family (values(%related_families_hash))
        {
            if ($related_family->level < $min_level)
            {
                $min_level = $related_family->level;
            }

            if ($related_family->level > $max_level)
            {
                $max_level = $related_family->level;
            }
        }

        # make sure range is ok
        my $delta = $level - $custom_family->level;

        if ((0 < ($min_level + $delta))
            && (255 > ($max_level + $delta)))
        {
            # ok
            # change all related families
            foreach my $related_family (values(%related_families_hash))
            {
                $related_family->level($related_family->level() + $delta);
                $related_family->save();
            }
        }
        else
        {
            PrintDebug("WARNING: unable to change family level because of related families!");
        }
    }
    else
    {
        $custom_family->level($level);
        $custom_family->save();
    }
}


=pod

=head2 SaveCustomFamilyRelationships

B<Description>: .

B<ArgsCount>: 0

B<Return>: (string)

.

=cut

sub SaveCustomFamilyRelationships
{
    InitAccess();

    my $requirements = {
        'require_access' => [GP('ACCESS_KEY_REGISTERED_USER'),],
        'access_needs'   => 'any',
    };

    my $custom_family_id = GetParameterValues('current_custom_family_id');
    my $custom_family;
    if ($custom_family_id)
    {
        $custom_family = Greenphyl::CustomFamily->new(
            $g_dbh,
            {
                'selectors' => {'id' => $custom_family_id,},
            }
        );
    }

    if (!$custom_family)
    {
        Throw('error' => 'Custom family not found!');
    }
    
    my $current_user = GetCurrentUser();

    # check if current family is editable by current user
    if (!$custom_family->hasWriteAccess($current_user) && !IsAdmin()) 
    {
        ThrowAccessDenied('error' => 'You are not allowed to edit this family!');
    }

    # update relationships...
    # loop on subject families
    my @subject_custom_family_ids = GetParameterValues('custom_family_id', undef, '[,;\s]+');
    foreach my $subject_custom_family_id (@subject_custom_family_ids)
    {
    
        my $subject_custom_family = Greenphyl::CustomFamily->new(
            $g_dbh,
            {
                'selectors' => {'id' => $subject_custom_family_id,},
            }
        );

        # make sure we treat user custom families only
        if (!$subject_custom_family
            || ($subject_custom_family->user_id != $custom_family->user_id))
        {
            next;
        }

        # get new relationship requested
        my $new_relationship = GetParameterValues('relationship_' . $subject_custom_family_id);
        if ($new_relationship)
        {
            # check if new relation is allowed
            if (($subject_custom_family->level && $custom_family->level
                 && (   (($new_relationship =~ m/parent/i) && ($subject_custom_family->level - $custom_family->level != -1))
                     || (($new_relationship =~ m/child/i)  && ($subject_custom_family->level - $custom_family->level !=  1))
                    )
                )
                || ((255 == $subject_custom_family->level) && ($new_relationship =~ m/parent/i))
                || ((1 == $subject_custom_family->level) && ($new_relationship =~ m/child/i))
                || ((1 == $custom_family->level) && ($new_relationship =~ m/parent/i))
                || ((255 == $custom_family->level) && ($new_relationship =~ m/child/i))
               )
            {
                PrintDebug("WARNING: Tried to set a relationship that is not allowed: check family levels for $custom_family (level " . $custom_family->level . ") and $subject_custom_family (level " . $subject_custom_family->level . ")!");
                next;
            }

            # autoset levels if needed
            AutosetLevels($subject_custom_family, $custom_family, $new_relationship);

            # get previous relationship if one
            my $previous_relationship_delta = GetPreviousRelationship($subject_custom_family_id, $custom_family->id);
            
            # check if there's a change
            if (($previous_relationship_delta
                    && ((($new_relationship =~ m/parent/i) && ($previous_relationship_delta == 1))
                        || (($new_relationship =~ m/child/i) && ($previous_relationship_delta == -1)))
                )
                || (!$previous_relationship_delta && ($new_relationship !~ m/parent|child/i))
               )
            {
                # no change
                PrintDebug("Skipped: no relation change between $custom_family and $subject_custom_family");
                next;
            }
            
            # remove previous relationships
            if ($previous_relationship_delta)
            {
                if ($previous_relationship_delta == 1)
                {
                    # current subject family is parent of the custom family
                    RemovePreviousRelationships($subject_custom_family_id, $custom_family->id);
                }
                else
                {
                    RemovePreviousRelationships($custom_family->id, $subject_custom_family_id);
                }
            }
            
            if ($new_relationship =~ m/parent/i)
            {
                # add subject as parent
                SetNewRelationships($subject_custom_family_id, $custom_family->id);
            }
            elsif ($new_relationship =~ m/child/i)
            {
                # add subject as child
                SetNewRelationships($custom_family->id, $subject_custom_family_id);
            }
        }
        else
        {
            # PrintDebug('No new relationship value found between ' . $subject_custom_family . ' and ' . $custom_family . '!');
        }
    }

    return Redirect(GetURL('fetch_custom_family', {'accession' => $custom_family->accession}));
}


=pod

=head2 AddPhylogenyFiles

B<Description>: .

B<ArgsCount>: 0

B<Return>: (string)

.

=cut

sub AddPhylogenyFiles
{
    InitAccess();

    my $requirements = {
        'require_access' => [GP('ACCESS_KEY_REGISTERED_USER'),],
        'access_needs'   => 'any',
    };

    my $custom_family_id = GetParameterValues('custom_family_id');
    my $custom_family;
    if ($custom_family_id)
    {
        $custom_family = Greenphyl::CustomFamily->new(
            $g_dbh,
            {
                'selectors' => {'id' => $custom_family_id,},
            }
        );
    }

    if (!$custom_family)
    {
        Throw('error' => 'Custom family not found!');
    }
    
    my $current_user = GetCurrentUser();

    # check if current family is editable by current user
    if (!$custom_family->hasWriteAccess($current_user) && !IsAdmin()) 
    {
        ThrowAccessDenied('error' => 'You are not allowed to edit this family!');
    }

    # update phylogeny files...
    # get files
    my $alignment_fasta  = GetParameterValues('alignment_fasta', undef, undef, 1);
    my $newick_tree_data = GetParameterValues('tree_newick', undef, undef, 1);
    my $xml_tree_data    = GetParameterValues('tree_xml', undef, undef, 1);
    my $family_data = $custom_family->data || {};

    my %sequence_hash = map {lc($_->accession) => 1} @{$custom_family->sequences};
    my $warning_message = '';


    # check files content...

    # check alignment content
    if ($alignment_fasta)
    {
        my $seqio;
        eval
        {
            # process FASTA content
            my ($istringfh);
            open($istringfh, "<", \$alignment_fasta)
                or confess "Could not open string for reading: $!";   # Use this for Perl AFTER 5.8.0 (inclusive)

            $seqio = Bio::SeqIO->new(
                '-fh'     => $istringfh,
                '-format' => 'fasta',
            );
        };

        if ($@ || !$seqio)
        {
            Throw('error' => "Failed to load the given FASTA file! Please make sure you provided a valid FASTA file.", 'log' => $@);
        }

        my ($fasta_sequence_count, $matched_sequence_count) = (0, 0);
        my @bio_sequences;
        eval
        {
            while (my $bio_seq = $seqio->next_seq)
            {
                # # check type
                # if ('protein' ne $bio_seq->alphabet)
                # {
                #     # invalid sequence type
                #     Throw('error' => "The sequence '" . $bio_seq->id . "' doesn't appear to be a protein sequence! Only protein sequences are allowed!", 'log' => "Detected sequence type: " . $bio_seq->alphabet);
                # }

                ++$fasta_sequence_count;
                my $bio_accession = lc($bio_seq->id);
                my $bio_accession_no_species = $bio_accession;
                $bio_accession_no_species =~ s/_$Greenphyl::Species::SPECIES_CODE_REGEXP$//i;
                if (!exists($sequence_hash{$bio_accession}) && !exists($sequence_hash{$bio_accession_no_species}))
                {
                    $warning_message .= "WARNING: FASTA sequence '" . $bio_seq->id . "' not found in current family!\n";
                }
                else
                {
                    ++$matched_sequence_count;
                }
                PrintDebug("Got user sequence: " . $bio_seq->id . "\n" . $bio_seq->seq());
                push(@bio_sequences, $bio_seq);
            }
        };

        if ($@)
        {
            Throw('error' => "Failed to load the given FASTA file! Please make sure you provided a valid FASTA file.", 'log' => $@);
        }

        if ($matched_sequence_count < $custom_family->countSequences())
        {
            if ($fasta_sequence_count == $custom_family->countSequences())
            {
                $warning_message .= "WARNING: not all sequences of current family have been matched with the given FASTA file! You may check the sequence accession in your FASTA and update the file.\n";
            }
            else
            {
                $warning_message .= "WARNING: some sequences of current family where not found in the given FASTA file!\n";
            }
        }
        elsif ($fasta_sequence_count > $custom_family->countSequences())
        {
            $warning_message .= "WARNING: the given FASTA file contains more sequences than current family!\n";
        }
        
        # generate a new name: we need a random part to protect files of private families
        
        my $saved_alignment_filename = $custom_family->accession . '_' . md5_hex(rand()) . '.fasta';

        # save to file
        my $output_seqio = Bio::SeqIO->new(
            '-file'   => '>' . GP('USER_FILES_PATH') . '/' . $saved_alignment_filename,
            '-format' => 'fasta',
        );

        # write each entry in the input file to the output file
        foreach my $bio_seq (@bio_sequences)
        {
            $output_seqio->write_seq($bio_seq);
        }

        # get old file
        my $old_align_file;
        if (exists($family_data->{'alignment'}) && $family_data->{'alignment'})
        {
            $old_align_file = $family_data->{'alignment'};
        }

        # add alignment file to family
        my $db_file = Greenphyl::File->new(
            GetDatabaseHandler(),
            {
                'load' => 'none',
                'members' => {
                    'id'      => 0,
                    'name'    => $saved_alignment_filename,
                    'path'    => GP('USER_FILES_PATH'),
                    'url'     => GetURL('user_files') . '/' . $saved_alignment_filename,
                    'type'    => 'text/fasta-alignment',
                    'flags'   => '',
                    'size'    => (-s GP('USER_FILES_PATH') . '/' . $saved_alignment_filename),
                    'user_id' => $custom_family->user_id,
                },
            },
        );
        $db_file->save;
        $family_data->{'alignment'} = {
            'type' => 'Greenphyl::File',
            'selectors' => {'id' => $db_file->id},
        };

        # remove unused previous file if one
        if ($old_align_file)
        {
            # remove from disk
            my $file_path = $old_align_file->path . '/' . $old_align_file->name;
            PrintDebug("Removing file $file_path from disk and database (id=" . $old_align_file->id . ")");
            unlink($file_path);
            # remove from database
            $old_align_file->remove();
        }

    }


    # check Newick tree data
    if ($newick_tree_data)
    {
        my $tree_node;
        eval
        {
            # process Newick content
            $tree_node = Greenphyl::TreeNode->loadNewickTree($newick_tree_data);
        };

        if ($@ || !$tree_node)
        {
            Throw('error' => "Failed to load the given Newick tree file! Please make sure you provided a valid Newick file.", 'log' => "ERROR: $@\nTreeNode: $tree_node");
        }

        # check taxa vs family sequences
        my ($tree_sequence_count, $matched_sequence_count) = (0, 0);
        foreach my $leaf ($tree_node->getLeafNodes())
        {
            ++$tree_sequence_count;
            my $leaf_accession = lc($leaf->getName);
            my $leaf_accession_no_species = $leaf_accession;
            $leaf_accession_no_species =~ s/_$Greenphyl::Species::SPECIES_CODE_REGEXP$//i;
            if (!exists($sequence_hash{$leaf_accession}) && !exists($sequence_hash{$leaf_accession_no_species}))
            {
                $warning_message .= "WARNING: Newick taxon '" . $leaf->getName . "' not found in current family!\n";
            }
            else
            {
                ++$matched_sequence_count;
            }
            PrintDebug("Got user sequence: " . $leaf->getName);
        }

        if ($matched_sequence_count < $custom_family->countSequences())
        {
            if ($tree_sequence_count == $custom_family->countSequences())
            {
                $warning_message .= "WARNING: not all sequences of current family have been matched with the given Newick file! You may check the sequence accession in your Newick and update the file.\n";
            }
            else
            {
                $warning_message .= "WARNING: some sequences of current family where not found in the given Newick file!\n";
            }
        }
        elsif ($tree_sequence_count > $custom_family->countSequences())
        {
            $warning_message .= "WARNING: the given Newick file contains more sequences than current family!\n";
        }
        
        # generate a new name: we need a random part to protect files of private families
        my $saved_newick_filename = $custom_family->accession . '_' . md5_hex(rand()) . '.nwk';

        # save to file
        my $output_newick_fh;
        if (open($output_newick_fh, '>' . GP('USER_FILES_PATH') . '/' . $saved_newick_filename))
        {
            print {$output_newick_fh} $newick_tree_data; # $tree_node->getNHXTree();
            close($output_newick_fh);
        }

        # get old newick file
        my $old_align_tree_file;
        if (exists($family_data->{'alignment_tree'}) && $family_data->{'alignment_tree'})
        {
            $old_align_tree_file = $family_data->{'alignment_tree'};
        }

        # add Newick tree file to family
        my $db_file = Greenphyl::File->new(
            GetDatabaseHandler(),
            {
                'load' => 'none',
                'members' => {
                    'id'      => 0,
                    'name'    => $saved_newick_filename,
                    'path'    => GP('USER_FILES_PATH'),
                    'url'     => GetURL('user_files') . '/' . $saved_newick_filename,
                    'type'    => 'text/newick',
                    'flags'   => '',
                    'size'    => (-s GP('USER_FILES_PATH') . '/' . $saved_newick_filename),
                    'user_id' => $custom_family->user_id,
                },
            },
        );

        $db_file->save;
        $family_data->{'alignment_tree'} = {
            'type' => 'Greenphyl::File',
            'selectors' => {'id' => $db_file->id},
        };

        # remove unused previous file if one
        if ($old_align_tree_file)
        {
            # remove from disk
            my $file_path = $old_align_tree_file->path . '/' . $old_align_tree_file->name;
            PrintDebug("Removing file $file_path from disk and database (id=" . $old_align_tree_file->id . ")");
            unlink($file_path);
            # remove from database
            $old_align_tree_file->remove();
        }
    }


    # check XML tree data
    if ($xml_tree_data)
    {
        my $treeio;
        eval
        {
            # process XML tree content
            my ($istringfh);
            open($istringfh, "<", \$xml_tree_data)
                or confess "Could not open string for reading: $!";   # Use this for Perl AFTER 5.8.0 (inclusive)

            $treeio = Bio::TreeIO->new(
                '-fh'     => $istringfh,
                '-format' => 'phyloxml',
            );
        };

        if ($@ || !$treeio)
        {
            Throw('error' => "Failed to load the given PhyloXML tree file! Please make sure you provided a valid PhyloXML file.", 'log' => $@);
        }

        my $tree = $treeio->next_tree;
        if (!$tree)
        {
            Throw('error' => "Failed to find a valid tree in the given PhyloXML tree file! Please make sure you provided a valid PhyloXML file.");
        }

        #+FIXME: unable to retrieve the taxon label from Bio::Tree!
        #my ($phyloxml_taxa_count, $matched_sequence_count) = (0, 0);
        #
        # foreach my $bio_node ($tree->get_leaf_nodes)
        # {
        #     ++$phyloxml_taxa_count;
        #     my $bio_accession = lc($bio_node->id);
        #     my $bio_accession_no_species = $bio_accession;
        #     $bio_accession_no_species =~ s/_$Greenphyl::Species::SPECIES_CODE_REGEXP$//i;
        #     if (!exists($sequence_hash{$bio_accession}) && !exists($sequence_hash{$bio_accession_no_species}))
        #     {
        #         $warning_message .= "WARNING: PhyloXML sequence '" . $bio_node->id . "' not found in current family!\n";
        #     }
        #     else
        #     {
        #         ++$matched_sequence_count;
        #     }
        #     PrintDebug("Got user taxon: " . $bio_node->id);
        # }
        # 
        # if ($matched_sequence_count < $custom_family->countSequences())
        # {
        #     if ($phyloxml_taxa_count == $custom_family->countSequences())
        #     {
        #         $warning_message .= "WARNING: not all sequences of current family have been matched with the given PhyloXML file! You may check the sequence accession in your PhyloXML and update the file.\n";
        #     }
        #     else
        #     {
        #         $warning_message .= "WARNING: some sequences of current family where not found in the given PhyloXML file!\n";
        #     }
        # }
        # elsif ($phyloxml_taxa_count > $custom_family->countSequences())
        # {
        #     $warning_message .= "WARNING: the given PhyloXML file contains more taxa than current family!\n";
        # }
        
        # generate a new name: we need a random part to protect files of private families
        
        my $saved_tree_filename = $custom_family->accession . '_' . md5_hex(rand()) . '.xml';

        # save to file
        my $output_treeio = Bio::TreeIO->new(
            '-file'   => '>' . GP('USER_FILES_PATH') . '/' . $saved_tree_filename,
            '-format' => 'phyloxml',
        );

        # write each entry in the input file to the output file
        $output_treeio->write_tree($tree);

        # get old file
        my $old_xml_tree_file;
        if (exists($family_data->{'tree'}) && $family_data->{'tree'})
        {
            $old_xml_tree_file = $family_data->{'tree'};
        }

        # add XML tree file to family
        my $db_file = Greenphyl::File->new(
            GetDatabaseHandler(),
            {
                'load' => 'none',
                'members' => {
                    'id'      => 0,
                    'name'    => $saved_tree_filename,
                    'path'    => GP('USER_FILES_PATH'),
                    'url'     => GetURL('user_files') . '/' . $saved_tree_filename,
                    'type'    => 'application/xml',
                    'flags'   => '',
                    'size'    => (-s GP('USER_FILES_PATH') . '/' . $saved_tree_filename),
                    'user_id' => $custom_family->user_id,
                },
            },
        );
        $db_file->save;
        $family_data->{'tree'} = {
            'type' => 'Greenphyl::File',
            'selectors' => {'id' => $db_file->id},
        };

        # remove unused previous file if one
        if ($old_xml_tree_file)
        {
            # remove from disk
            my $file_path = $old_xml_tree_file->path . '/' . $old_xml_tree_file->name;
            PrintDebug("Removing file $file_path from disk and database (id=" . $old_xml_tree_file->id . ")");
            unlink($file_path);
            # remove from database
            $old_xml_tree_file->remove();
        }
    }
    
    if ($alignment_fasta || $newick_tree_data || $xml_tree_data)
    {
        $custom_family->setData($family_data);
        $custom_family->save();
        $custom_family->reload(['data']);
    }

    #+FIXME: use a warning system
    if ($warning_message)
    {
        PrintDebug($warning_message);
    }

    return RenderCustomFamilyIdCard($custom_family);
}


=pod

=head2 RenderCustomSequenceAssociations

B<Description>: .

B<ArgsCount>: 0

B<Return>: (string)

.

=cut

sub RenderCustomSequenceAssociations
{
    InitAccess();

    my $requirements = {
        'require_access' => [GP('ACCESS_KEY_REGISTERED_USER'),],
        'access_needs'   => 'any',
    };

    my $current_user = GetCurrentUser();
    my $custom_sequences = [];
    
    # get custom sequences from specified id
    my @custom_sequence_ids = GetParameterValues('cseq_id');
    foreach my $custom_sequence_id (@custom_sequence_ids)
    {
        my $custom_sequence = Greenphyl::CustomSequence->new(
            $g_dbh,
            {
                'selectors' => {'id' => $custom_sequence_id,},
            }
        );
        if ($custom_sequence)
        {
            if ($custom_sequence->isOwner($current_user) || IsAdmin()) 
            {
                push(@$custom_sequences, $custom_sequence);
            }
        }
    }

    my $custom_family;

    # get custom sequences from family
    my $custom_family_id = GetParameterValues('custom_family_id');
    if ($custom_family_id)
    {
        $custom_family = Greenphyl::CustomFamily->new(
            $g_dbh,
            {
                'selectors' => {'id' => $custom_family_id,},
            }
        );
        
        if ($custom_family)
        {
            # check if current family is editable by current user
            if ($custom_family->hasWriteAccess($current_user) || IsAdmin()) 
            {
                push(@$custom_sequences, $custom_family->sequences);
            }
        }
    }

    if (!@$custom_sequences)
    {
        Throw('error' => 'No corresponding custom sequence found!');
    }

    # sort
    $custom_sequences = [
        sort
        {
            if (@{$a->sequences} && !@{$b->sequences})
            {
                return 1;
            }
            elsif (!@{$a->sequences} && @{$b->sequences})
            {
                return -1;
            }
            elsif (@{$a->sequences} && @{$b->sequences})
            {
                return @{$b->sequences} <=> @{$a->sequences};
            }
            else
            {
                return $a->accession cmp $b->accession;
            }
        }
        @$custom_sequences
    ];
    
    # show associations...
    # keys are custom sequences and values are [array of Greenphyl::Sequence]
    my %exact_matches;
    # keys are custom sequences and values are [array of {'sequence' => Greenphyl::Sequence, 'score' => score, 'evalue' => e-value,}]
    my %blast_matches;

    # find exact matches in database
    my $searched_sequences = 0;
    foreach my $custom_sequence (@$custom_sequences)
    {
        if ($MAX_SEQUENCE_SEARCH <= $searched_sequences++)
        {
            last;
        }
        # check if some associations are not already set
        if (!@{$custom_sequence->sequences})
        {
            # no associations set, try to find suggestions...
            # -try to match polypeptide
            my $sequences = [Greenphyl::Sequence->new(
                $g_dbh,
                {
                    'selectors' => {'polypeptide' => ['LIKE', $custom_sequence->polypeptide],},
                }
            )];

            if (@$sequences && $sequences->[0])
            {
                $exact_matches{$custom_sequence} = $sequences;
            }
            # else
            # {
            #     # find approximate matches with BLAST
            #     my ($blast_parameters, $input_temp_file, $output_temp_file) = PrepareBLASTParameters(">$custom_sequence\n" . $custom_sequence->polypeptide, $custom_sequence->species->code);
            #     LaunchBLAST($blast_parameters);
            # 
            #     # get BLAST output
            #     my $family_hits = {}; # hash of family accessions as keys
            #     if (-e $output_temp_file)
            #     {
            #         my $output_fh;
            #         if (open($output_fh, $output_temp_file ))
            #         {
            #             my $in = new Bio::SearchIO('-format' => 'blast', '-file' => $output_temp_file);
            #             while (my $result = $in->next_result())
            #             {
            #                 while (my $hit = $result->next_hit())
            #                 {
            #                     while (my $hsp = $hit->next_hsp())
            #                     {
            #                         my @seq_ids = split(/\s+/, $hit->name);
            #     
            #                         for my $seq_id (@seq_ids)
            #                         {
            #                             my $sequence = Greenphyl::Sequence->new(
            #                                 $g_dbh,
            #                                 {
            #                                     'selectors' => {'accession' => ['LIKE', $seq_id],},
            #                                 },
            #                             );
            #                             if ($sequence)
            #                             {
            #                                 $blast_matches{$custom_sequence} ||= [];
            #                                 push(@{$blast_matches{$custom_sequence}},
            #                                     {
            #                                         'sequence' => $sequence,
            #                                         'score' => $hsp->score,
            #                                         'evalue' => $hsp->evalue,
            #                                     },
            #                                 );
            #                             }
            #                         }
            #                     }
            #                 }
            #             }
            #             close($output_fh);
            #         }
            #         else
            #         {
            #             confess "ERROR: failed to open BLAST output '$output_temp_file': $!\n"
            #         }
            #     }
            #     else
            #     {
            #         confess "ERROR: failed to get BLAST output file '$output_temp_file'!\n"
            #     }
            #     #+FIXME: do all BLAST at once?
            # }
        }
    }

    return RenderHTMLFullPage(
        {
            'title'            => 'Custom sequence associations',
            'content'          => 'family_tools/custom_sequence_associations.tt',
            'family'           => $custom_family,
            'custom_sequences' => $custom_sequences,
            'exact_matches'    => \%exact_matches,
            'blast_matches'    => \%blast_matches,
            'max_search_count' => $MAX_SEQUENCE_SEARCH,
        },
    );
}


=pod

=head2 RenderSaveCustomSequenceAssociations

B<Description>: .

B<ArgsCount>: 0

B<Return>: (string)

.

=cut

sub RenderSaveCustomSequenceAssociations
{
    InitAccess();

    my $requirements = {
        'require_access' => [GP('ACCESS_KEY_REGISTERED_USER'),],
        'access_needs'   => 'any',
    };

    my $current_user = GetCurrentUser();
    my $custom_sequences = [];
    
    # get custom sequences from specified id
    my @custom_sequence_ids = GetParameterValues('cseq_id');
    PrintDebug("Got " . scalar(@custom_sequence_ids) . " sequences to process: " . join(', ', @custom_sequence_ids));
    foreach my $custom_sequence_id (@custom_sequence_ids)
    {
        my $custom_sequence = Greenphyl::CustomSequence->new(
            $g_dbh,
            {
                'selectors' => {'id' => $custom_sequence_id,},
            }
        );
        if ($custom_sequence)
        {
            if ($custom_sequence->isOwner($current_user) || IsAdmin()) 
            {
                push(@$custom_sequences, $custom_sequence);
            }
        }
    }

    if (!@$custom_sequences)
    {
        Throw('error' => 'No corresponding custom sequence found!');
    }
    PrintDebug("Loaded (kept) " . scalar(@$custom_sequences) . " corresponding custom sequences");
    

    # update associations...
    foreach my $custom_sequence (@$custom_sequences)
    {
        PrintDebug("Working on custom sequence '$custom_sequence' (" . $custom_sequence->id . ")");
        # get new associations
        my @associated_sequence_ids = GetParameterValues('sequence_relation_' . $custom_sequence->id);
        PrintDebug("Found " . scalar(@associated_sequence_ids) . " associated sequences: " . join(', ', @associated_sequence_ids));

        my $sequence_accession = GetParameterValues('sequence_relation_accession_' . $custom_sequence->id);
        if ($sequence_accession)
        {
            PrintDebug("An accession has been specified: $sequence_accession");
            my $sequence = Greenphyl::Sequence->new(
                $g_dbh,
                {
                    'selectors' => {'accession' => $sequence_accession,},
                }
            );
            if ($sequence)
            {
                PrintDebug("The associated sequence has been found: " . $sequence->id);
                push(@associated_sequence_ids, $sequence->id);
            }
            else
            {
                PrintDebug("The associated sequence has NOT been found!");
            }
        }

        # remove previous associations
        my $sql_query = "DELETE FROM custom_sequences_sequences_relationships WHERE custom_sequence_id = ? AND type = 'associated';";
        PrintDebug("SQL Query: $sql_query");
        $g_dbh->do($sql_query, undef, $custom_sequence->id);

        # add new ones
        my @valid_sequence_ids = grep {m/^\d+$/} @associated_sequence_ids;
        if (@valid_sequence_ids)
        {
            $sql_query = "
                INSERT
                  INTO custom_sequences_sequences_relationships (custom_sequence_id, sequence_id, type)
                  VALUES " . join(",\n", map { '(' . $custom_sequence->id . ', ' . $_ .  ", 'associated')" } @valid_sequence_ids) . "
                ;";
            PrintDebug("SQL Query: $sql_query");
            $g_dbh->do($sql_query);
        }
    }

    my $custom_family_id = GetParameterValues('custom_family_id');

    if ($custom_family_id)
    {
        return Redirect(GetURL('fetch_custom_family', {'custom_family_id' => $custom_family_id}));
    }
    else
    {
        return Redirect(GetURL('fetch_custom_family_list', {'user_id' => $current_user->id}));
    }
}


=pod

=head2 DeleteCustomFamily

B<Description>: .

B<ArgsCount>: 0

B<Return>: (string)

.

=cut

sub DeleteCustomFamily
{
    InitAccess();

    my $requirements = {
        'require_access' => [GP('ACCESS_KEY_REGISTERED_USER'),],
        'access_needs'   => 'any',
    };

    my $custom_family = LoadCustomFamily(); # throws an error if no family found

    my $current_user = GetCurrentUser();

    # check if current family is editable by current user
    if (!$custom_family->isOwner($current_user) && !IsAdmin()) 
    {
        ThrowAccessDenied('error' => 'You are not allowed to delete this family!');
    }

    my $sql_query = 'DELETE FROM custom_families WHERE id = ' . $custom_family->id . ';';
    PrintDebug("SQL Query: $sql_query");
    $g_dbh->do($sql_query)
        or Throw('error' => 'Failed to delete custom family ' . $custom_family . ' (' . $custom_family->id . ')!');

    return Redirect(GetURL('fetch_custom_family_list', {'user_id' => $current_user->id}));
}



# Script options
#################

=pod

=head1 OPTIONS

    custom_family.cgi ?p=id &custom_family_id=<FAMILY_ID>
    custom_family.cgi ?p=id &accession=<FAMILY_ACCESSION>
    custom_family.cgi ?p=save &accession=<FAMILY_ACCESSION> | &custom_family_id=<FAMILY_ID>

=head2 Parameters

=over 4

=item B<custom_family_id> FAMILY_ID (integer):

Family identifier in database.

=item B<accession> FAMILY_ACCESSION (string):

A family accession.

=back

=cut


# CODE START
#############

# hash for function selection.
my $actions =
    {
        'id' =>
        {
            'custom_family_id' => \&RenderCustomFamilyIdCard,
            'accession'        => \&RenderCustomFamilyIdCard,
            'password'         => \&RenderUsePassword,
            'cfph'             => \&RenderUsePassword,
        },
        'save'                 => \&SaveCustomFamily,
        'mylist_to_family'     => \&SaveMyListToCustomFamily,
        'add_mylist_to_family' => \&AddMyListToCustomFamily,
        'add_sequences_to_family' => {
            ''       => \&AddSequencesPhase1,
            'phase2' => \&AddSequencesPhase2,
        },
        'create' =>
        {
            '' => \&CreateCustomFamilyForm,
            'input_sequences' => \&CreateCustomFamilyForm2,
            'create_custom_family' => \&CreateCustomFamily,
        },
        'save_relationships' => \&SaveCustomFamilyRelationships,
        'update_files'       => \&AddPhylogenyFiles,
        'seqasso'            => \&RenderCustomSequenceAssociations,
        'save_seqasso'       => \&RenderSaveCustomSequenceAssociations,
        'delete' =>
        {
            '' => \&DeleteCustomFamily,
        },
    };

# try block
eval
{
    # display page
    print ProceedPage($actions);
};

HandleErrors();

exit(0);

# CODE END
###########


=pod

=head1 AUTHORS

Mathieu ROUARD (Bioversity), m.rouard@cgiar.org

Valentin GUIGNON (Bioversity), v.guignon@cgiar.org

Matthieu CONTE (Syngenta)

=head1 VERSION

Version 1.0.0

Date 12/06/2012

=head1 SEE ALSO

GreenPhyl documentation.

=cut
