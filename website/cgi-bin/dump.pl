#!/usr/bin/perl

=pod

=head1 NAME

dump.pl - Dumps GreenPhyl data

=head1 SYNOPSIS

    dump.pl?format=fasta&object_type=sequences&fields=seq_id&file_name=sequences.fasta&seq_id=1234&seq_id=5678

=head1 REQUIRES

Perl5

=head1 DESCRIPTION

Dumps given GreenPhyl objects in a given format. If a file name is specified,
outputs content as an attachement in case of CGI or to the given file otherwise.

=cut

use strict;
use warnings;

use lib '../lib';
use lib '../local_lib';
use Carp qw (cluck confess croak);

use Greenphyl;
use Greenphyl::Web;
use Greenphyl::Dumper;




# Script options
#################

=pod

=head1 OPTIONS

    dump.pl format=<output_format> object_type=<object_type> [file_name=<file_name>] [params=<parameters>] fields=<field_name> (<field_name>=<value> ...)

=head2 Parameters

=over 4

=item B<output_format> (string):

output file format.

=item B<object_type> (string):

type of object to dump. Supported types are defined in
%Greenphyl::Dumper::OBJECTS_TYPES.

=item B<file_name> (string):

name of the output file. If no name is specified, output to screen.

=item B<parameters> (string):

a set of additional parameters such as object fields to dump stored into a Perl
serialized data using 'Storable' package. The content of 'parameters' depends
on the type of object to dump. See object "Dump" (or "dump") method
documentation for details.

=item B<field_name> (string):

name of the parameter that will contains the required data to fetch objects to
dump.

=item B<value> (string):

data that can identify objects to dump.

=back

=cut


# CODE START
#############

my ($dump_format, $file_name, $objects, $parameters);

# try block
eval
{
    print DumpPageData();
};

HandleErrors();

exit(0);

# CODE END
###########


=pod

=head1 AUTHORS

Mathieu ROUARD (Bioversity), m.rouard@cgiar.org

Valentin GUIGNON (Bioversity), v.guignon@cgiar.org

Matthieu CONTE (Syngenta)

=head1 VERSION

Version 1.0.0

Date 30/04/2012

=head1 SEE ALSO

GreenPhyl documentation, template dumps.tt documentation.

=cut
