#!/usr/bin/perl

=pod

=head1 NAME

register.cgi - Account request page

=head1 SYNOPSIS

    register.cgi

=head1 REQUIRES

Perl5

=head1 DESCRIPTION

Display the registration form for a GreenPhyl account.

=cut

use strict;
use warnings;

use FindBin;
use lib "$FindBin::Bin/../lib";
use lib "$FindBin::Bin/../local_lib";
use Carp qw (cluck confess croak);

use Greenphyl;
use Greenphyl::Web;
use Greenphyl::Species;
use Greenphyl::User;
use Greenphyl::Tools::Users;
use Greenphyl::Web::Access;
use Greenphyl::OpenID;

binmode STDOUT, ":utf8";
use utf8;




# Script global constants
##########################

=pod

=head1 CONSTANTS

B<$DEBUG>: (boolean)

When set to true, it enables debug mode.

=cut

our $DEBUG = 0;




# Script global functions
##########################

=pod

=head1 FUNCTIONS

=head2 RenderRegistrationForm

B<Description>: Returns the registration form.

B<ArgsCount>: 0-1

=over 4

=item $parameters: (hash ref) (O)

Hash containing default values for some fields:
-'login': (string) default user login;
-'dissplay_name': (string) default user display name;
-'email': (string) default e-mail address;
-'agreement': (boolean) default setting for the agreement checkbox.

=back

B<Return>: (string)

Returns an HTML string containing the GreenPhyl form for registration.

=cut

sub RenderRegistrationForm
{
    InitAccess();
    my $current_user = GetCurrentUser();
    if ($current_user)
    {
        Throw("You already logged in! Please logout first.");
    }
    
    my ($parameters) = @_;

    # parameters can contain page parameters like default values for some fields
    if (!$parameters || (ref($parameters) ne 'HASH'))
    {
        $parameters = {};
    }

    # anti-spam check
    my ($code, $label) = InitAntiSpam();

    my $page_parameters = GetOpenIDPageParameters(
        {
            'title'   => 'Create your GreenPhyl account',
            'content' => 'users/register_form.tt',
            'form_data'          =>
                {
                    'identifier' => 'registration_form',
                    'action'     => '?p=register',
                    'submit'     => 'Register',                
                },
            'antispam' => $label,
            %$parameters,
        }
    );

    return RenderHTMLFullPage($page_parameters);
}


=pod

=head2 RenderRegistrationSubmission

B<Description>: [function description]. #+++

B<ArgsCount>: 0

B<Return>: (string)

Returns an HTML string containing the GreenPhyl page for ... #+++

=cut

sub RenderRegistrationSubmission
{
    # get CGI parameters
    my $agreement = GetParameterValues('agreement');
    my $openid_url = GetParameterValues('openid_identity');
    
    my $user_data = GetUserDataFromForm();

    # check user input an capture errors that can be reported to the user
    eval
    {
        # check agreements
        if (!$agreement)
        {
            Throw('error' => "You must agree to GreenPhyl agreements in order to have an account!");
        }

        CheckNewUserData($user_data);

        # authentication method check
        if (!$user_data->{'password'} && !$openid_url)
        {
            Throw('error' => "You must either enter a password to create a local account or provide an external account (OpenID URL)!");
        }

        # check for robots
        CheckAntiSpam();

        if (GP('ENABLE_OPENID') && $openid_url)
        {
            # try OpenID identification...
            my $session   = GetSession();
            $user_data->{'openid_url'} = $openid_url;
            $session->param('openid_user_data', $user_data);
            OpenIDAuthenticatePhase1($openid_url, GetURL('account_request', {'p' => 'validate_openid',}));
        }
    };

    # check for errors
    my $error;
    if ($error = Exception::Class->caught('Greenphyl::Redirection'))
    {
        return $error->redirect();
    }
    elsif ($error = Exception::Class->caught('Greenphyl::Error'))
    {
        # an error occured, stop registration and go back
        return RenderRegistrationForm({
            'access_message' => $error,
            'agreement'      => $agreement,
            %$user_data,
        });
    }
    elsif ($error = Exception::Class->caught())
    {
        # unhandled type of error: re-throw
        confess $error;
    }
    elsif ($@)
    {
        # eval error
        confess $@;
    }

    return RenderRegistrationValidation({
        %$user_data,
        'openid_url' => undef,
        'status'     => 'ok',
    });
}


=pod

=head2 RenderOpenIDValidation

B<Description>: Renders OpenID authentification validation.

B<ArgsCount>: 0

B<Return>: (string)

Returns an HTML string containing the GreenPhyl page for OpenID authentication
validation.

=cut

sub RenderOpenIDValidation
{
    if (!GP('ENABLE_OPENID'))
    {
        confess "Unexpected call to RenderOpenIDValidation!";
    }

    my $session = GetSession();
    # retrieve user data
    my $user_data = $session->param('openid_user_data');

    if (!$user_data || ('HASH' ne ref($user_data)))
    {
        Throw('error' => "Your current user session have not been found! You need either to enable cookies in order to get through the registration process or just retry.");
    }

    $user_data->{'status'} = '';
    $user_data->{'openid_identity'} = undef;

    my ($status, $openid_data) = OpenIDAuthenticatePhase2();

    $user_data->{'status'} = $status;
    $user_data->{'status'} ||= '';

    if ('verified' eq $user_data->{'status'})
    {
        $user_data->{'openid_identity'} = $openid_data;
    }
    else
    {
        $user_data->{'openid_data'} = $openid_data;
    }

    return RenderRegistrationValidation({%$user_data,});
}


=pod

=head2 RenderRegistrationValidation

B<Description>: If registration succeeded, add the new user to the database and
display a welcome message otherwise, display the error status.

B<ArgsCount>: 0-1

=over 4

=item $parameters: (hash ref) (O)

Hash containing:
-'login':
-'display_name':
-'email':
-'password':
-'openid_url':
-'openid_identity':
-'openid_data':
-'status':

=back

B<Return>: (string)

Returns an HTML string containing the GreenPhyl status of the registration.

=cut

sub RenderRegistrationValidation
{
    my ($parameters) = @_;
    my $dbh = GetDatabaseHandler();

    if (!$parameters
        || ('HASH' ne ref($parameters)))
    {
        $parameters = {};
    }

    CheckNewUserData($parameters);

    # create new user
    my $user_data = {
        'login'        => $parameters->{'login'},
        'display_name' => $parameters->{'display_name'},
        'email'        => $parameters->{'email'},
        'password'     => $parameters->{'password'},
        'description'  => 'Online registration',
        'flags'        => $Greenphyl::User::USER_FLAG_REGISTERED,
    };

    CreateUser($user_data)
        or Throw('error' => "Failed to create new user!", 'log' => "name: '$parameters->{'login'}'");

    my $new_user = Greenphyl::User->new(
       $dbh,
        {
            #'selectors' => {'login' => $parameters->{'login'}, 'id' => $user_id, },
            'selectors' => {'login' => $parameters->{'login'}, },
        },
    );
    
    if (!$new_user)
    {
        Throw('error' => "Failed to retrieve newly created user!", 'log' => "login: '$parameters->{'login'}'");
    }
    
    # check for openid data to store
    if (GP('ENABLE_OPENID')
        && $parameters->{'openid_url'}
        && $parameters->{'openid_identity'})
    {
        my $sql_query = "INSERT INTO user_openids (user_id, openid_url, openid_identity, priority) VALUES (?, ?, ?, 0);";
        # if we cannot store user OpenID, he/she won't be able to authenticate
        # then we must remove the new account in order to let her/him try again to register with the same name/email.
        if ((!$dbh->do($sql_query, undef, $new_user->id, $parameters->{'openid_url'}, $parameters->{'openid_identity'}))
            && !$parameters->{'password'})
        {
            # delete inserted user
            $sql_query = "DELETE FROM users WHERE id = ?;";
            $dbh->do($sql_query, undef, $new_user->id);
            PrintDebug("Removed user " . $new_user->login . " (" . $new_user->id . ") because of OpenID link insertion failure ('" . $parameters->{'openid_url'}. "', '" . $parameters->{'openid_identity'} . "')!");
            cluck "Warning: removed user " . $new_user->login . " (" . $new_user->id . ") because of OpenID link insertion failure ('" . $parameters->{'openid_url'}. "', '" . $parameters->{'openid_identity'} . "')!";
            Throw('error' => "Failed to store OpenID identity!");
        }
    }

    # clear session data
    my $session = GetSession();
    $session->clear('consumer_secret', 'openid_user_data');

    # send email with name and email if provided  
    if (GP('CONTACTS_EMAIL'))
    {
        eval('use MIME::Lite;');
        if (!$@)
        {
            PrintDebug("email send to " . GP('CONTACTS_EMAIL') . " for new user '$new_user'\n");
    
            my $openid_info = '';
            if (GP('ENABLE_OPENID')
                && $parameters->{'openid_url'}
                && $parameters->{'openid_identity'})
            {
                $openid_info = 'OpenID URL: ' . $parameters->{'openid_url'} . "\nOpenID identity: " . $parameters->{'openid_identity'} . "\n";
            }

            my $msg =  MIME::Lite->new(
                From     => 'greenphyldb@greenphyl.org',
                To       => GP('CONTACTS_EMAIL'),
                Subject  => "[GreenPhyl] New account: " . $new_user->login,
                Data     => "User: '" . $new_user->login . "'\ne-mail: '" . ($new_user->email || '') . "'\n$openid_info\n",
            );
            $msg->send;
        }
        else 
        {
            cluck "Warning: unable to send a mail for registration request. MIME::Lite may not be installed on the server!\n$@\n";
        }
    }
    
    # log user in
    $session->param('user', $new_user->getHashValue());

    return RenderHTMLFullPage(
        {
            'title'   => 'GreenPhyl User Registration',
            'content' => 'users/register_status.tt',
            %$parameters,
        },
    );
}




# Script options
#################

=pod

=head1 OPTIONS

    register.cgi ?p=[register|validate_openid]

=head2 Parameters

=over 4

=item B<p> (string):

-default: display registration page.

-'register': display registration result

-'validate_openid': run OpenID validation and display 'register' page

=back

=cut


# CODE START
#############

# hash for function selection.
my $actions =
    {
        '' => \&RenderRegistrationForm,
        'register' => \&RenderRegistrationSubmission,
        'validate_openid' => \&RenderOpenIDValidation,
    };

# try block
eval
{
    # display page
    print ProceedPage($actions);
};

HandleErrors();

exit(0);

# CODE END
###########


=pod

=head1 AUTHORS

Mathieu ROUARD (Bioversity), m.rouard@cgiar.org

Valentin GUIGNON (Bioversity), v.guignon@cgiar.org

Matthieu CONTE (Syngenta)

Sebastien BRIOIS (Syngenta)

=head1 VERSION

Version 1.0.0

Date 07/11/2013

=head1 SEE ALSO

GreenPhyl documentation, TreePattern documentation.

=cut
