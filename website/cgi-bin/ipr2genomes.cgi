#!/usr/bin/perl

=pod

=head1 NAME

ipr2genomes.cgi - IPR domains to genomes

=head1 SYNOPSIS

    http://www.greenphyl.fr/cgi-bin/ipr2genomes.cgi

=head1 REQUIRES

Perl5

=head1 DESCRIPTION

Enables search of combination of IPR domaines in GreenPhyl database.

IPR expression syntax:

IPR_Query = IPR_Element | IPR_Query op IPR_Query | (IPR_Query op IPR_Query)

IPR_Element = IPR | [IPR_Set]

IPR_Set = IPR | IPR + IPR_Set

op = '+' | '-' | '*' | ',' | '~'

Query is processed from left to right in the order operators apear. Parenthesis
can be used to set priorities.

ex.:
IPR000001+IPR000002,IPR000001*IPR000002,(IPR000001+IPR000002*IPR000003)
(IPR000001,[IPR000001+IPR000002])
(IPR000001*(IPR000002+IPR000003)),((IPR000001*IPR000002)+IPR000003)

=cut

use strict;
use warnings;

use lib '../lib';
use lib '../local_lib';
use Carp qw (cluck confess croak);

use CGI qw(:standard);
# use CGI::Carp qw(fatalsToBrowser); # useful for debugging
# use Data::Dumper; # useful for debugging

use Greenphyl;
use Greenphyl::Web;
use Greenphyl::Dumper;
use Greenphyl::IPR;
use Greenphyl::Species;
use Greenphyl::Genome;
use Greenphyl::Family;
use Greenphyl::Sequence;
use Greenphyl::Tools::Sequences;

# Script global constants
##########################

=pod

=head1 CONSTANTS

B<$DEBUG>: (boolean)

When set to true, it enables debug mode.

=cut

our $DEBUG = 0;

my %OPERATIONS =
    (
        '+' => \&ProcessAndQuery,
        '~' => \&ProcessOrQuery,
        '-' => \&ProcessNotQuery,
        '*' => \&ProcessCombinationQuery,
    );




# Script global variables
##########################

=pod

=head1 VARIABLES

B<$g_dbh>: (DBI::db)

Current database handler.

=cut

my $g_dbh = GetDatabaseHandler();
my $g_ipr_sequence = {};


# Script global functions
##########################

=pod

=head1 FUNCTIONS

=cut

sub ValidateIPRExpression
{
    my $ipr_expression = shift();
    
    return 1;
}


sub GetIPRSequences
{
    my ($ipr_code) = (@_);
    
    if (!exists($g_ipr_sequence->{$ipr_code}))
    {
        my $ipr = Greenphyl::IPR->new($g_dbh, {'selectors' => {'code' => $ipr_code}});
        if (!$ipr)
        {
            Throw('error' => "Invalid IPR code or IPR code not present in database: '$ipr_code'");
        }
        #check if splice form should be kept
        my $with_splice_form = GetParameterValues('with_splice');
        if ($with_splice_form)
        {
            # keep all
            $g_ipr_sequence->{$ipr_code} = $ipr->fetchSequences({'selectors' => {'representative' => 1,}});
        }
        else
        {
            # remove splice forms
            my $ipr_sequences = $ipr->fetchSequences({'selectors' => {'representative' => 1,}});
            $g_ipr_sequence->{$ipr_code} = FilterSpliceForm($ipr_sequences);
        }
    }

    return $g_ipr_sequence->{$ipr_code};
}


sub PreprocessIPRQuery
{
    my ($ipr_query) = @_;

    PrintDebug("Preprocessing IPR query $ipr_query\n");
    
    if (!$ipr_query)
    {
        Throw('error' => 'No IPR query provided!');
    }

    # remove spaces
    $ipr_query =~ s/\s*//g;
    # all upper case
    $ipr_query = uc($ipr_query);
    # remove empty parentheses
    while ($ipr_query =~ m/\(\)/)
    {
        $ipr_query =~ s/\(\)//g;
    }

    # make sure we only got valid content
    if ($ipr_query !~ m/^(?:$Greenphyl::IPR::IPR_REGEXP|[+~\-*,\[\]\(\)])+$/o)
    {
        Throw('error' => "Syntax error: invalid character(s) detected in the IPR query (or empty string)!", 'log' => "Invalid IPR query: $ipr_query");
    }

    # remove leading and trailing comas (may be lead or followed by parentheses)
    $ipr_query =~ s/^([()]*),+/$1/;
    $ipr_query =~ s/,+([()]*)$/$1/;
    # separate "or" blocks
    $ipr_query =~ s/,+/),(/g;
    $ipr_query = "($ipr_query)"; # and add terminal parentheses

    # quote each IPR and operator except parentheses
    $ipr_query =~ s/($Greenphyl::IPR::IPR_REGEXP|[+~\-*,\[\]])/'$1',/go;

    # add missing quotes
    $ipr_query =~ s/'\(/',(/g;
    $ipr_query =~ s/\)'/),'/g;

    # turn parenthesis into braquets to use Perl parsing
    $ipr_query =~ tr/()/[]/;

    # now let Perl parse the expression!
    my $ipr_preprocessed_query;
    PrintDebug("IPR query:\n$ipr_query");

    eval "\$ipr_preprocessed_query = [$ipr_query];";
    if ($@)
    {
        Throw('error' => "Syntax error: either some parentheses do not match or missing or invalid coma position!", 'log' => "Syntax error: $@");
    }

    return $ipr_preprocessed_query;
}

sub MergeIPRQueryResults
{
    my ($first_set, $second_set) = @_;
    map { $first_set->{$_} ||= $second_set->{$_}; } keys(%$second_set);
    return $first_set;
}

sub ProcessIPRQuery
{
    return ProcessPrerocessedIPRQuery(PreprocessIPRQuery(@_));
}

sub ProcessPrerocessedIPRQuery
{
    my ($ipr_preprocessed_query) = @_;

    #+FIXME: check if operation is in cache and return if so

    # remove unecessary parentheses (get inner element)
    while (ref($ipr_preprocessed_query) && (1 == @$ipr_preprocessed_query))
    {
        $ipr_preprocessed_query = $ipr_preprocessed_query->[0];
    }

    # check if we got a single IPR or a set of operations
    if (ref($ipr_preprocessed_query))
    {
        # process operations
        my $query_results = {}; # final results
        my $current_result_set; # intermediate results
        my $current_operator = '';
        # get first operand
        while (my $query_element = shift(@$ipr_preprocessed_query))
        {
            # check for an operator
            if (!ref($query_element) && ($query_element =~ m/^[+~\-*,]$/))
            {
                # we got an operator
                # make sure we had a previous result set
                if (!$current_result_set)
                {
                    Throw('error' => "Syntax error near operator '$query_element': no previous operand!");
                }
                # make sure we did not have a previous operator
                if ($current_operator)
                {
                    Throw('error' => "Syntax error: two operators are following without operand between them! ('$current_operator' and '$query_element')");
                }
                # save as current operator
                $current_operator = $query_element;
            }
            else
            {
                # we got an operand
                my $new_result_set;
                if (ref($query_element))
                {
                    # process sub-query
                    $new_result_set = ProcessPrerocessedIPRQuery($query_element);
                    # # wrap results inside parentheses
                    # map { $new_result_set->{"($_)"} = delete($new_result_set->{$_}); } keys(%$new_result_set);
                }
                elsif ($query_element =~ m/^$Greenphyl::IPR::IPR_REGEXP$/o)
                {
                    # process single IPR code
                    $new_result_set = ProcessPrerocessedIPRQuery($query_element);
                }
                elsif ($query_element eq '[')
                {
                    # got exclusive IPR set
                    # get first IPR
                    $query_element = shift(@$ipr_preprocessed_query)
                        or Throw('error' => "Syntax error: invalid exclusive specification! Incomplete expression.");
                    if (ref($query_element) || ($query_element !~ m/^$Greenphyl::IPR::IPR_REGEXP$/o))
                    {
                        # next element is not an IPR!
                        Throw('error' => "Syntax error: invalid exclusive specification! Expecting an IPR code while found '$query_element'.");
                    }
                    my @ipr_set = ($query_element);
                    my $operator = shift(@$ipr_preprocessed_query);
                    # loop until we reach the end of the exclusive set
                    while (($operator eq '+')
                           && (my $next_element = shift(@$ipr_preprocessed_query)))
                    {
                        # make sure we only have IPR
                        if (ref($next_element) || ($next_element !~ m/^$Greenphyl::IPR::IPR_REGEXP$/o))
                        {
                            # next element is not an IPR!
                            Throw('error' => "Syntax error: invalid exclusive specification! Expecting an IPR code while found '$next_element'.");
                        }
                        push(@ipr_set, $next_element);
                        $operator = shift(@$ipr_preprocessed_query);
                    }
                    # make sure we only got 'and' operators
                    if ($operator ne ']')
                    {
                        # either we got an invalid operator
                        # or an operator was missing
                        # or we reached the end of the array without finding the closing ']'
                        Throw('error' => "Syntax error: invalid exclusive specification!");
                    }
                    # get exclusive IPR set
                    $new_result_set = {'[' . join('+', @ipr_set) . ']' => GetExclusiveIPRSetResult(@ipr_set)};
                }

                # if we had previous results
                if (defined($current_result_set))
                {
                    # check if we have an operator
                    if ($current_operator && ($current_operator ne ','))
                    {
                        # perform the operation
                        $current_result_set = $OPERATIONS{$current_operator}->($current_result_set, $new_result_set);
                    }
                    else
                    {
                        # otherwise assume we got a (possibly implicit) "or"
                        # and save current results in $query_results hash
                        MergeIPRQueryResults($query_results, $current_result_set);
                        # replace current result set with the new one
                        $current_result_set = $new_result_set;
                    }
                    $current_operator = undef;
                }
                else
                {
                    # no previous results
                    # make sure we didn't met an operator before (it should have already been checked when the operator has been met)
                    if ($current_operator)
                    {
                        Throw('error' => "Syntax error: got an operator '$current_operator' without left operand!");
                    }
                    # save as current result
                    $current_result_set = $new_result_set;
                }
            }
        }
        # save current results
        MergeIPRQueryResults($query_results, $current_result_set);
        # done
        return $query_results;
    }
    elsif ($ipr_preprocessed_query =~ m/^$Greenphyl::IPR::IPR_REGEXP$/o)
    {
        # get results for requested IPR
        return {$ipr_preprocessed_query => GetIPRSequences($ipr_preprocessed_query)};
    }
    else
    {
        # syntax error
        Throw('error' => "Syntax error: unexpected operator: '$ipr_preprocessed_query'");
    }

    # cache results
#    return {$ipr_rephrased_query => {species => count}, ...};
}

# return sequences with only the specified IPR and no others
sub GetExclusiveIPRSetResult
{
    my @ipr_list = @_;
    my $ipr_count = scalar(@ipr_list);
    # print "DEBUG: processing [" . join('+', @ipr_list) . "]\n"; #+debug
    my $current_ipr = shift(@ipr_list);
    my $matching_set = GetIPRSequences($current_ipr);

    # get the "and" list
    while ($current_ipr = shift(@ipr_list))
    {
        my %in_set = map { $_ => 1 } @{GetIPRSequences($current_ipr)};
        $matching_set = [grep {exists($in_set{$_});} @$matching_set];
    }
    
    # here $matching_set contains all the sequences having all the requested IPR and maybe more
    # remove sequences with additional IPR
    $matching_set = [grep {$ipr_count == scalar(@{$_->ipr_details})} @$matching_set];
    
    return $matching_set;
}

sub ProcessAndQuery
{
    my ($first_set, $second_set) = @_;
    my %new_result_set;
    foreach my $first_query (keys(%$first_set))
    {
        foreach my $second_query (keys(%$second_set))
        {
            # print "DEBUG: processing $first_query + $second_query\n"; #+debug
            my $matching_set = $first_set->{$first_query};
            # get the "and" list
            my %in_set = map { $_ => 1 } @{$second_set->{$second_query}};
            $matching_set = [grep {exists($in_set{$_});} @$matching_set];
            $new_result_set{$first_query . '+' . $second_query} = $matching_set;
        }
    }
    return \%new_result_set;    
}

sub ProcessOrQuery
{
    my ($first_set, $second_set) = @_;
    my %new_result_set;
    foreach my $first_query (keys(%$first_set))
    {
        foreach my $second_query (keys(%$second_set))
        {
            my %in_set;
            foreach my $matching_element (@{$first_set->{$first_query}}, @{$second_set->{$second_query}})
            {
                $in_set{$matching_element} = $matching_element;
            }
            $new_result_set{$first_query . '~' . $second_query} = [values(%in_set)];
        }
    }
    return \%new_result_set;    
}

sub ProcessNotQuery
{
    my ($first_set, $second_set) = @_;
    my %new_result_set;
    foreach my $first_query (keys(%$first_set))
    {
        foreach my $second_query (keys(%$second_set))
        {
            # print "DEBUG: processing $first_query - $second_query\n"; #+debug
            my $matching_set = $first_set->{$first_query};
            # get the "and" list
            my %in_set = map { $_ => 1 } @{$second_set->{$second_query}};
            $matching_set = [grep {!exists($in_set{$_});} @$matching_set];
            
            if ($second_query =~ m/^\[|^$Greenphyl::IPR::IPR_REGEXP$/o)
            {
                $new_result_set{$first_query . '-' . $second_query} = $matching_set;
            }
            else
            {
                $new_result_set{$first_query . '-' . "($second_query)"} = $matching_set;
            }
        }
    }
    return \%new_result_set;    
}

sub ProcessCombinationQuery
{
    my ($first_set, $second_set) = @_;
    my $new_result_set = {};
    foreach my $first_query (keys(%$first_set))
    {
        foreach my $second_query (keys(%$second_set))
        {
            # print "DEBUG: processing $first_query * $second_query\n"; #+debug
            MergeIPRQueryResults($new_result_set, ProcessNotQuery($first_set, $second_set));
            MergeIPRQueryResults($new_result_set, ProcessNotQuery($second_set, $first_set));
            MergeIPRQueryResults($new_result_set, ProcessAndQuery($first_set, $second_set));
        }
    }
    return $new_result_set;
}


=pod

=head2 RenderForm

B<Description>: Render IPR 2 Genomes form into an HTML string.

B<ArgsCount>: 0

B<Return>: (string)

Returns an HTML string containing the full GreenPhyl page with the form.

=cut

sub RenderForm
{
    return RenderHTMLFullPage(
        {
            'title'             => 'InterPro Domain Distribution',
            'content'           => 'tools/ipr2genomes_form.tt',
            'form_data'         =>
                {
                    'identifier' => 'ipr',
                    'action'     => url() . '?p=results',
                    'submit'     => 'Get IPR distribution',
                },
        },
    );
}


=pod

=head2 RenderResults

B<Description>: Render table of matching sequences by species.

B<ArgsCount>: 0

B<Return>: (string)

HTML string.

=cut

sub RenderResults
{
    my $ipr_expression = param('ipr_expression');
    #if ($ipr_expression !~ m/^
    #            (?:
    #                \(* # matches any number of opening parenthesis
    #                (?:
    #                    \s*$Greenphyl::IPR::IPR_REGEXP               # matches IPRxxxxxx
    #                    | \[                         # or matches a restrictive group [...]
    #                        \s*$Greenphyl::IPR::IPR_REGEXP           #   restrictive group must contain 1
    #                        (?:\s*\+\s*$Greenphyl::IPR::IPR_REGEXP)* #   or more IPR separated by +
    #                      \s*\]
    #                )                                # matches and IPRxxxxxx or a restrictive group
    #                (?:
    #                    \s*[+~\-\*,]                      # matches an operator
    #                    (?:
    #                        \s*$Greenphyl::IPR::IPR_REGEXP               # matches IPRxxxxxx
    #                        | \[                         # or matches a restrictive group [...]
    #                            \s*$Greenphyl::IPR::IPR_REGEXP           #   restrictive group must contain 1
    #                            (?:\s*\+\s*$Greenphyl::IPR::IPR_REGEXP)* #   or more IPR separated by +
    #                          \s*\]
    #                    )                                # matches and IPRxxxxxx or a restrictive group
    #                )*                               # matches additional IPR or groups with operators
    #                \)* # matches any number of closing parenthesis
    #            )+ # matches a list of IPR with operators and parentheses
    #        $/ix)
    #{
    #    Throw('error' => "ERROR: Invalid IPR selection expression '$ipr_expression'!");
    #}


    # get species list and order them like on home page
    my $species_array = [sort {return $a->display_order <=> $b->display_order;} Greenphyl::Species->new($g_dbh, {'selectors' => { 'display_order' => ['>', 0], }, 'load' => {'display_order' => 1} })];
    # process user query
    my $iprquery_to_sequences = ProcessIPRQuery($ipr_expression);
    my $iprquery_to_species = {};

    # fill $iprquery_to_species hash
    # -loop on ipr queries to get all matching sequences
    foreach my $iprquery (keys(%$iprquery_to_sequences))
    {
        # -init species counters
        $iprquery_to_species->{$iprquery} = {};
        foreach my $species (@$species_array)
        {
            $iprquery_to_species->{$iprquery}->{$species->code} = 0;
        }

        # -loop on sequences to see the species they belong to
        #  and increase species counter
        foreach my $sequence (@{$iprquery_to_sequences->{$iprquery}})
        {
            $iprquery_to_species->{$iprquery}->{$sequence->species->code}++;
        }
    }
    
    # create IPR list
    my %ipr_in_expression;
    foreach my $ipr_code ($ipr_expression =~ m/$Greenphyl::IPR::IPR_REGEXP/go)
    {
        $ipr_in_expression{$ipr_code} = Greenphyl::IPR->new($g_dbh, {'selectors' => {'code' => $ipr_code}});
    }

    my $with_splice_form = GetParameterValues('with_splice');
    return RenderHTMLFullPage(
        {
            'title'            => "InterPro Domain Distribution Results",
            'content'          => 'tools/ipr2genomes_results.tt',
            'ipr_expression'   => $ipr_expression,
            'species'          => $species_array,
            'ipr_query_to_species_sequence_count' => $iprquery_to_species,
            'iprs'             => [values(%ipr_in_expression)],
            'with_splice'      => $with_splice_form,
        },
    );
}


=pod

=head2 RenderClassificationComparison

B<Description>: Render .

B<ArgsCount>: 0

B<Return>: (string)


=cut

sub RenderClassificationComparison
{
    my $ipr_expression = param('ipr_expression');

    # check parameters
    if (!ValidateIPRExpression($ipr_expression)
        || ($ipr_expression =~ m/\*/))
    {
        Throw('error' => "Invalid IPR expression!", 'log' => "IPR expression: '$ipr_expression'");
    }

    # get families with associated species
    my @classification_selection = GetParameterValues('family_id');
    my @compared_families;
    my $max_sequence_length = 0;
    foreach my $family_id (@classification_selection)
    {
        my $family;
        # only instanciate valide family ID from database
        if ($family_id =~ m/^\d+$/)
        {
            $family = Greenphyl::Family->new($g_dbh, {'selectors' => {'id' => $family_id,}});
        }
        else
        {
            $family = Greenphyl::Family->new(undef, {'members' => {'id' => $family_id, 'accession' => 'Unclassified', 'family_name' => 'Unclassified (orphans)'}});
        }

        # get sequences associated to the family using POST data
        my $selected_sequences = $family->getCGIMembers()->{'ipr_seq_id'};
        PrintDebug("ASSOCIATED SEQUENCES: @$selected_sequences: " . join(', ', @$selected_sequences));
        my $sequences = [];
        foreach my $selected_sequence (@$selected_sequences)
        {
            my $sequence = Greenphyl::Sequence->new($g_dbh, {'selectors' => { 'id' => $selected_sequence,},});
            if ($sequence)
            {
                if ($max_sequence_length < $sequence->length)
                {
                    $max_sequence_length = $sequence->length;
                }
                push(@$sequences, $sequence);
            }
        }
        
        push(
            @compared_families,
            {
                'family'    => $family,
                'sequences' => $sequences,
            }
        );
    }

    my $with_splice_form = GetParameterValues('with_splice');

    return RenderHTMLFullPage(
        {
            'title'             => "InterPro Classification Comparison",
            'javascript'        => ['raphael', 'ipr'],
            'content'           => 'tools/ipr2genomes_comparison.tt',
            'ipr_expression'    => $ipr_expression,
            'with_splice'       => $with_splice_form,
            'compared_families' => \@compared_families,
            'max_length'        => $max_sequence_length,
        },
    );
}


=pod

=head2 RenderDetails

B<Description>: Render BLAST results into an HTML string.



B<ArgsCount>: 0

B<Return>: (string)

Returns an HTML string containing the full GreenPhyl page with the BLAST
results if no error occured.

=cut

sub RenderDetails
{
    my $species_code   = param('species') || '';
    my $ipr_expression = param('ipr') || '';
    my $species;

    # check parameters
    $species_code = uc($species_code);
    if (($species_code !~ m/^[A-Z]{3,8}$/)
        || (!($species = Greenphyl::Species->new($g_dbh, {'selectors' => {'code' => $species_code}}))))
    {
        Throw('error' => "Invalid species code specified!", 'log' => "Invalid species code: $species_code");
    }

    if (!ValidateIPRExpression($ipr_expression)
        || ($ipr_expression =~ m/\*/))
    {
        Throw('error' => "Invalid IPR expression!", 'log' => "IPR expression: '$ipr_expression'");
    }

    # process user query
    my $iprquery_to_sequences = ProcessIPRQuery($ipr_expression);
    
    if (1 != keys(%$iprquery_to_sequences))
    {
        Throw('error' => "IPR expression does not correspond to a single query!", 'log' => "IPR expression: '$ipr_expression'");
    }

    # get only sequences from specified species
    ($ipr_expression) = keys(%$iprquery_to_sequences);
    my @sequences = ();
    my $max_sequence_length = 0;
    foreach my $sequence (@{$iprquery_to_sequences->{$ipr_expression}})
    {
        if ($sequence->species->code eq $species->code)
        {
            push(@sequences, $sequence);
            if ($sequence->length() > $max_sequence_length)
            {
                $max_sequence_length = $sequence->length();
            }
        }
    }
    
    my $with_splice_form = GetParameterValues('with_splice');

    return RenderHTMLFullPage(
        {
            'title'            => "InterPro Domain Details",
            'javascript'       => ['raphael', 'ipr'],
            'content'          => 'tools/ipr2genomes_details.tt',
            'form_data'        =>
                {
                    'identifier'  => 'ipr_details',
                    'action'      => url() . '?p=more_details',
                    'nosubmit'    => 1,
                    'customstyle' => 'ipr-details-form',
                },
            'ipr_expression'   => $ipr_expression,
            'species'          => $species,
            'sequences'        => [sort { $a->name cmp $b->name } @sequences],
            'max_length'       => $max_sequence_length,
            'with_splice'      => $with_splice_form,
        },
    );

}


=pod

=head2 DumpIPRSequencesToFasta

B<Description>: dumps sequences in a FASTA file.


B<ArgsCount>: 0

B<Return>: (string)

Returns an HTML string containing the full GreenPhyl page with the BLAST
results if no error occured.

=cut

sub DumpIPRSequencesToFasta
{
    my $objects      = LoadObjectsToDump('fasta');
    my $dump_content = DumpObjects($objects, 'fasta');
    # get header after content in case we got an error during content generation
    # so we can still generate a clean header
    my $header = RenderHTTPFileHeader({'file_name' => 'ipr_sequences.fasta', 'format' => 'fasta'});
    # to not ouput as attachement:
    # my $header = RenderHTTPFileHeader({'mime' => 'TEXT', 'format' => 'fasta'}); #+debug 
    return $header . $dump_content;
}


=pod

=head2 RenderClassification

B<Description>: Render .

B<ArgsCount>: 0

B<Return>: (string)

Returns an HTML string containing the full GreenPhyl page with the BLAST
results if no error occured.

=cut

sub RenderClassification
{
    my @sequences = param('seq_id');
    my $ipr_expression = param('ipr') || '';

    if (!ValidateIPRExpression($ipr_expression)
        || ($ipr_expression =~ m/\*/))
    {
        Throw('error' => "Invalid IPR expression!", 'log' => "IPR expression: '$ipr_expression'");
    }

    my $families = {};
    my $matching_sequences = {};

    # create a family from scratch (not database-related) for orphans
    my $orphans = Greenphyl::Family->new(undef, {'members' => {'id' => -1, 'accession' => '-', 'family_name' => 'Unclassified (orphans)'}});

    # get level-1 families for each sequence
    foreach my $seq_id (@sequences)
    {
        # load sequences if valid
        if (($seq_id =~ m/^\d+$/)
            && (my $sequence = Greenphyl::Sequence->new($g_dbh, {'selectors' => {'id' => $seq_id}})))
        {
            # got a sequence, check family
            my ($sequence_family) = @{$sequence->getFamilies()};
            $sequence_family ||= $orphans; # unclassified
            $matching_sequences->{$sequence_family} ||= [];
            push(@{$matching_sequences->{$sequence_family}}, $sequence);
            $families->{$sequence_family} = $sequence_family;
        }
        else
        {
            Throw('error' => "Invalid sequence identifier provided!", 'log' => "Sequence ID: '$seq_id'");
        }
    }

    my $with_splice_form = GetParameterValues('with_splice');

    return RenderHTMLFullPage(
        {
            'title'              => "InterPro Domain Classification",
            'content'            => 'tools/ipr2genomes_classification.tt',
            'families'           => [map { $families->{$_} } sort({int($a) <=> int($b)} keys(%$families))],
            'matching_sequences' => $matching_sequences,
            'ipr_expression'     => $ipr_expression,
            'with_splice'        => $with_splice_form,
            'form_data'        =>
                {
                    'identifier'  => 'ipr_comparison',
                    'action'      => url() . '?p=compare',
                    'nosubmit'    => 1,
                },
        },
    );

}   




# Script options
#################

=pod

=head1 OPTIONS

ipr2genomes.cgi?p=form

=head2 Parameters

=over 4

=item B<p> (string):

Select the page to display:
'form': displays IPR selection form;
'results': displays matching results.

Default: 'form'

=back

=cut


# CODE START
#############

# hash for function selection.
my $actions =
{
    ''             => \&RenderForm,
    'form'         => \&RenderForm,
    'results'      => \&RenderResults,
    'details'      => \&RenderDetails,
    'more_details' =>
        {
            ''               => \&RenderForm,
            'classification' => \&RenderClassification,
            'fasta'          => \&DumpIPRSequencesToFasta,
        },
    'compare' => \&RenderClassificationComparison,
};

# try block
eval
{
    # display page
    print ProceedPage($actions);
};

HandleErrors();

exit(0);


# CODE END
###########


=pod

=head1 AUTHORS

Mathieu ROUARD (Bioversity), m.rouard@cgiar.org

Valentin GUIGNON (Bioversity), v.guignon@cgiar.org

Matthieu CONTE (Syngenta)

=head1 VERSION

Version 1.0.0

Date 20/04/2012

=head1 SEE ALSO

GreenPhyl documentation.

=cut
