#!/usr/bin/perl

=pod

=head1 NAME

tos.cgi - Displays GreenPhyl terms of service

=head1 SYNOPSIS

    tos.cgi

=head1 REQUIRES

Perl5

=head1 DESCRIPTION

Displays GreenPhyl terms of service.

=cut

use strict;
use warnings;

use FindBin;
use lib "$FindBin::Bin/../lib";
use lib "$FindBin::Bin/../local_lib";
use Carp qw (cluck confess croak);

use Greenphyl;
use Greenphyl::Web;




# Script global functions
##########################

=pod

=head1 FUNCTIONS

=head2 RenderTermsOfService

B<Description>: Render GreenPhyl terms of service page.

B<ArgsCount>: 0

B<Return>: (string)

Returns an HTML string containing the GreenPhyl terms of service.

=cut

sub RenderTermsOfService
{
    return RenderHTMLFullPage(
        {
            'title'   => 'Terms of Service',
            'content' => 'miscelaneous/terms_of_service.tt',
        },
    );
}




# Script options
#################

=pod

=head1 OPTIONS

    tos.cgi

=cut


# CODE START
#############

# hash for function selection.
my $actions =
    {
        '' => \&RenderTermsOfService,
    };

# try block
eval
{
    # display page
    print ProceedPage($actions);
};

HandleErrors();

exit(0);

__END__
# CODE END
###########

=pod

=head1 AUTHORS

Mathieu ROUARD (Bioversity), m.rouard@cgiar.org

Valentin GUIGNON (Bioversity), v.guignon@cgiar.org

Matthieu CONTE (Syngenta)

Sebastien BRIOIS (Syngenta)

=head1 VERSION

Version 1.0.0

Date 13/02/2014

=head1 SEE ALSO

GreenPhyl documentation.

=cut
