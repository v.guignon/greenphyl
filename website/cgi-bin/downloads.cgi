#!/usr/bin/perl

=pod

=head1 NAME

downloads.cgi - display Greenphyl downloads

=head1 SYNOPSIS

    downloads.cgi

=head1 REQUIRES

Perl5

=head1 DESCRIPTION

This script provides links to Greenphyl data files.

=cut

use strict;
use warnings;

use lib '../lib';
use lib '../local_lib';
use Carp qw (cluck confess croak);

use Greenphyl;
use Greenphyl::Web;
use Greenphyl::Species;
use Greenphyl::Variables;


# Script global constants
##########################

=pod

=head1 CONSTANTS

B<$DEBUG>: (boolean)

Enables debug mode.

=cut

our $DEBUG = 0;
our $DOWNLOAD_FILES = [
  {
    'title' => 'Species',
    'description' => 'species and pangenome species proteins as FASTA files',
    'path' => 'species',
    'files' => {
        'AMBTC_v1.0.faa'          => '',
        'ARATH_Araport11.faa'     => '',
        'BETVU_RefBeet_v1.2.faa'  => '',
        'BRADI_pan.faa'           => '',
        'BRANA_pan.faa'           => '',
        'BRAOL_pan.faa'           => '',
        'BRARR_pan.faa'           => '',
        'CAJCA_Asha_v1.0.faa'     => '',
        'CAPAN_pan.faa'           => '',
        'CHEQI_JGI_v1.0.faa'      => '',
        'CICAR_pan.faa'           => '',
        'CITMA_v1.0.faa'          => '',
        'CITME_v1.0.faa'          => '',
        'CITSI_v2.0.faa'          => '',
        'COCNU_pan.faa'           => '',
        'COFAR_RB_v1.0.faa'       => '',
        'COFCA_v1.0.faa'          => '',
        'CUCME_DHL92_v3.6.1.faa'  => '',
        'CUCSA_pan.faa'           => '',
        'DAUCA_JGI_v2.0.faa'      => '',
        'DIORT_TDr96_F1_v1.0.faa' => '',
        'ELAGV_RefSeq_v1.0.faa'   => '',
        'FRAVE_v4.0a1.faa'        => '',
        'HELAN_XRQ_v1.2.faa'      => '',
        'HORVU_IBSC_v2.0.faa'     => '',
        'IPOTF_pan.faa'           => '',
        'IPOTR_NSP323.faa'        => '',
        'MAIZE_pan.faa'           => '',
        'MALDO_pan.faa'           => '',
        'MANES_AM560_2_v6.1.faa'  => '',
        'MEDTR_pan.faa'           => '',
        'MUSAC_pan.faa'           => '',
        'MUSBA_Mba1.1.faa'        => '',
        'OLEEU_v1.0.faa'          => '',
        'ORYGL_v1.0.faa'          => '',
        'ORYSA_pan.faa'           => '',
        'PHAVU_v2.0.faa'          => '',
        'PHODC_v3.0.faa'          => '',
        'SACSP_AP85_441_v1.0.faa' => '',
        'SOLLC_ITAG3.2.faa'       => '',
        'SOLTU_PGSC_v4.03.faa'    => '',
        'SORBI_pan.faa'           => '',
        'SOYBN_pan.faa'           => '',
        'THECC_pan.faa'           => '',
        'TRITU_pan.faa'           => '',
        'VITVI_pan.faa'           => '',
    },
  },
  {
    'title' => 'MCL',
    'description' => 'MCL computation files',
    'path' => 'mcl',
    'files' => {
        # Lost during cc2 cluster crash.
        # 'matrix_I12_level1.txt' => 'MCL matrix for level 1 clusters.',
        # 'matrix_I20_level2.txt' => 'MCL matrix for level 2 clusters.',
        # 'matrix_I30_level3.txt' => 'MCL matrix for level 3 clusters.',
        # 'matrix_I50_level4.txt' => 'MCL matrix for level 4 clusters.',
        'mcl_level1.txt'        => 'MCL clusters for level 1.',
        'mcl_level2.txt'        => 'MCL clusters for level 2.',
        'mcl_level3.txt'        => 'MCL clusters for level 3.',
        'mcl_level4.txt'        => 'MCL clusters for level 4.',
    },
  },
  {
    'title' => 'Pangenomes',
    'description' => 'pangenome computation files',
    'path' => 'pangenomes',
    'files' => {
        # Mise en commentaire des "representative" et "seq_map" car on n'a pas
        # les bons fichiers à cause du crash de CC2. Les autres devraient etre
        # les memes.
        'BRADI/BRADI_clusters.txt' => 'List of genome sequences behind each pan-sequence.',
        'BRADI/BRADI_orphans_real.faa' => 'Genome unmatched sequences.',
        # 'BRADI/BRADI_representative.txt' => 'List of genome sequences selected as representative for other associated sequences in the same genome.',
        # 'BRADI/BRADI_seq_map.txt' => 'Genome sequence mapping sequence -> [representative ->] consensus pan-sequence.',
        'BRANA/BRANA_clusters.txt' => 'List of genome sequences behind each pan-sequence.',
        'BRANA/BRANA_orphans_real.faa' => 'Genome unmatched sequences.',
        # 'BRANA/BRANA_representative.txt' => 'List of genome sequences selected as representative for other associated sequences in the same genome.',
        # 'BRANA/BRANA_seq_map.txt' => 'Genome sequence mapping sequence -> [representative ->] consensus pan-sequence.',
        'BRAOL/BRAOL_clusters.txt' => 'List of genome sequences behind each pan-sequence.',
        'BRAOL/BRAOL_orphans_real.faa' => 'Genome unmatched sequences.',
        # 'BRAOL/BRAOL_representative.txt' => 'List of genome sequences selected as representative for other associated sequences in the same genome.',
        # 'BRAOL/BRAOL_seq_map.txt' => 'Genome sequence mapping sequence -> [representative ->] consensus pan-sequence.',
        'BRARR/BRARR_clusters.txt' => 'List of genome sequences behind each pan-sequence.',
        'BRARR/BRARR_orphans_real.faa' => 'Genome unmatched sequences.',
        # 'BRARR/BRARR_representative.txt' => 'List of genome sequences selected as representative for other associated sequences in the same genome.',
        # 'BRARR/BRARR_seq_map.txt' => 'Genome sequence mapping sequence -> [representative ->] consensus pan-sequence.',
        'CAPAN/CAPAN_clusters.txt' => 'List of genome sequences behind each pan-sequence.',
        'CAPAN/CAPAN_orphans_real.faa' => 'Genome unmatched sequences.',
        # 'CAPAN/CAPAN_representative.txt' => 'List of genome sequences selected as representative for other associated sequences in the same genome.',
        # 'CAPAN/CAPAN_seq_map.txt' => 'Genome sequence mapping sequence -> [representative ->] consensus pan-sequence.',
        'CICAR/CICAR_clusters.txt' => 'List of genome sequences behind each pan-sequence.',
        'CICAR/CICAR_orphans_real.faa' => 'Genome unmatched sequences.',
        # 'CICAR/CICAR_representative.txt' => 'List of genome sequences selected as representative for other associated sequences in the same genome.',
        # 'CICAR/CICAR_seq_map.txt' => 'Genome sequence mapping sequence -> [representative ->] consensus pan-sequence.',
        'COCNU/COCNU_clusters.txt' => 'List of genome sequences behind each pan-sequence.',
        'COCNU/COCNU_orphans_real.faa' => 'Genome unmatched sequences.',
        # 'COCNU/COCNU_representative.txt' => 'List of genome sequences selected as representative for other associated sequences in the same genome.',
        # 'COCNU/COCNU_seq_map.txt' => 'Genome sequence mapping sequence -> [representative ->] consensus pan-sequence.',
        'CUCSA/CUCSA_clusters.txt' => 'List of genome sequences behind each pan-sequence.',
        'CUCSA/CUCSA_orphans_real.faa' => 'Genome unmatched sequences.',
        # 'CUCSA/CUCSA_representative.txt' => 'List of genome sequences selected as representative for other associated sequences in the same genome.',
        # 'CUCSA/CUCSA_seq_map.txt' => 'Genome sequence mapping sequence -> [representative ->] consensus pan-sequence.',
        'IPOTF/IPOTF_clusters.txt' => 'List of genome sequences behind each pan-sequence.',
        'IPOTF/IPOTF_orphans_real.faa' => 'Genome unmatched sequences.',
        # 'IPOTF/IPOTF_representative.txt' => 'List of genome sequences selected as representative for other associated sequences in the same genome.',
        # 'IPOTF/IPOTF_seq_map.txt' => 'Genome sequence mapping sequence -> [representative ->] consensus pan-sequence.',
        'MAIZE/MAIZE_clusters.txt' => 'List of genome sequences behind each pan-sequence.',
        'MAIZE/MAIZE_orphans_real.faa' => 'Genome unmatched sequences.',
        # 'MAIZE/MAIZE_representative.txt' => 'List of genome sequences selected as representative for other associated sequences in the same genome.',
        # 'MAIZE/MAIZE_seq_map.txt' => 'Genome sequence mapping sequence -> [representative ->] consensus pan-sequence.',
        'MALDO/MALDO_clusters.txt' => 'List of genome sequences behind each pan-sequence.',
        'MALDO/MALDO_orphans_real.faa' => 'Genome unmatched sequences.',
        # 'MALDO/MALDO_representative.txt' => 'List of genome sequences selected as representative for other associated sequences in the same genome.',
        # 'MALDO/MALDO_seq_map.txt' => 'Genome sequence mapping sequence -> [representative ->] consensus pan-sequence.',
        'MEDTR/MEDTR_clusters.txt' => 'List of genome sequences behind each pan-sequence.',
        'MEDTR/MEDTR_orphans_real.faa' => 'Genome unmatched sequences.',
        # 'MEDTR/MEDTR_representative.txt' => 'List of genome sequences selected as representative for other associated sequences in the same genome.',
        # 'MEDTR/MEDTR_seq_map.txt' => 'Genome sequence mapping sequence -> [representative ->] consensus pan-sequence.',
        'MUSAC/MUSAC_clusters.txt' => 'List of genome sequences behind each pan-sequence.',
        'MUSAC/MUSAC_orphans_real.faa' => 'Genome unmatched sequences.',
        # 'MUSAC/MUSAC_representative.txt' => 'List of genome sequences selected as representative for other associated sequences in the same genome.',
        # 'MUSAC/MUSAC_seq_map.txt' => 'Genome sequence mapping sequence -> [representative ->] consensus pan-sequence.',
        'ORYSA/ORYSA_clusters.txt' => 'List of genome sequences behind each pan-sequence.',
        'ORYSA/ORYSA_orphans_real.faa' => 'Genome unmatched sequences.',
        # 'ORYSA/ORYSA_representative.txt' => 'List of genome sequences selected as representative for other associated sequences in the same genome.',
        # 'ORYSA/ORYSA_seq_map.txt' => 'Genome sequence mapping sequence -> [representative ->] consensus pan-sequence.',
        'SORBI/SORBI_clusters.txt' => 'List of genome sequences behind each pan-sequence.',
        'SORBI/SORBI_orphans_real.faa' => 'Genome unmatched sequences.',
        # 'SORBI/SORBI_representative.txt' => 'List of genome sequences selected as representative for other associated sequences in the same genome.',
        # 'SORBI/SORBI_seq_map.txt' => 'Genome sequence mapping sequence -> [representative ->] consensus pan-sequence.',
        'SOYBN/SOYBN_clusters.txt' => 'List of genome sequences behind each pan-sequence.',
        'SOYBN/SOYBN_orphans_real.faa' => 'Genome unmatched sequences.',
        # 'SOYBN/SOYBN_representative.txt' => 'List of genome sequences selected as representative for other associated sequences in the same genome.',
        # 'SOYBN/SOYBN_seq_map.txt' => 'Genome sequence mapping sequence -> [representative ->] consensus pan-sequence.',
        'THECC/THECC_clusters.txt' => 'List of genome sequences behind each pan-sequence.',
        'THECC/THECC_orphans_real.faa' => 'Genome unmatched sequences.',
        # 'THECC/THECC_representative.txt' => 'List of genome sequences selected as representative for other associated sequences in the same genome.',
        # 'THECC/THECC_seq_map.txt' => 'Genome sequence mapping sequence -> [representative ->] consensus pan-sequence.',
        'TRITU/TRITU_clusters.txt' => 'List of genome sequences behind each pan-sequence.',
        'TRITU/TRITU_orphans_real.faa' => 'Genome unmatched sequences.',
        # 'TRITU/TRITU_representative.txt' => 'List of genome sequences selected as representative for other associated sequences in the same genome.',
        # 'TRITU/TRITU_seq_map.txt' => 'Genome sequence mapping sequence -> [representative ->] consensus pan-sequence.',
        'VITVI/VITVI_clusters.txt' => 'List of genome sequences behind each pan-sequence.',
        'VITVI/VITVI_orphans_real.faa' => 'Genome unmatched sequences.',
        # 'VITVI/VITVI_representative.txt' => 'List of genome sequences selected as representative for other associated sequences in the same genome.',
        # 'VITVI/VITVI_seq_map.txt' => 'Genome sequence mapping sequence -> [representative ->] consensus pan-sequence.',
    },
  },
  # Lost during cc2 cluster crash.
  # {
  #   'title' => 'Get_homologues-est',
  #   'description' => 'get_homologues-est computation files archives',
  #   'path' => 'get_homologues-est',
  #   'files' => {
  #       'BRADI_est_homologues.tbz' => 'Get_homologues-EST computation files.',
  #       'BRANA_est_homologues.tbz' => 'Get_homologues-EST computation files.',
  #       'BRAOL_est_homologues.tbz' => 'Get_homologues-EST computation files.',
  #       'BRARR_est_homologues.tbz' => 'Get_homologues-EST computation files.',
  #       'CAPAN_est_homologues.tbz' => 'Get_homologues-EST computation files.',
  #       'CICAR_est_homologues.tbz' => 'Get_homologues-EST computation files.',
  #       'COCNU_est_homologues.tbz' => 'Get_homologues-EST computation files.',
  #       'CUCSA_est_homologues.tbz' => 'Get_homologues-EST computation files.',
  #       'IPOTF_est_homologues.tbz' => 'Get_homologues-EST computation files.',
  #       'MAIZE_est_homologues.tbz' => 'Get_homologues-EST computation files.',
  #       'MALDO_est_homologues.tbz' => 'Get_homologues-EST computation files.',
  #       'MEDTR_est_homologues.tbz' => 'Get_homologues-EST computation files.',
  #       'MUSAC_est_homologues.tbz' => 'Get_homologues-EST computation files.',
  #       'ORYSA_est_homologues.tbz' => 'Get_homologues-EST computation files.',
  #       'SORBI_est_homologues.tbz' => 'Get_homologues-EST computation files.',
  #       'SOYBN_est_homologues.tbz' => 'Get_homologues-EST computation files.',
  #       'THECC_est_homologues.tbz' => 'Get_homologues-EST computation files.',
  #       'TRITU_est_homologues.tbz' => 'Get_homologues-EST computation files.',
  #       'VITVI_est_homologues.tbz' => 'Get_homologues-EST computation files.',
  #   },
  # },
];



# Script global functions
##########################

=pod

=head1 FUNCTIONS

=head2 RenderDownloads

B<Description>: Renders requested downloads page.

B<ArgsCount>: 0

B<Return>: (string)

Returns an HTML string containing the requested GreenPhyl downloads page.

=cut

sub RenderDownloads
{
    my $page_name = GetParameterValues('page') || GetParameterValues('p');
    my %allowed_pages = (
        'species'            => 1,
        'pangenomes'         => 1,
        'mcl'                => 1,
        'get_homologues-est' => 1,
    );

    my $page_parameters = {
            'title'        => 'Download GreenPhyl v5 datasets',
            'content'      => 'downloads/overview.tt',
            'downloads'    => $DOWNLOAD_FILES,
        };

    my $page_index = 0;
    my %download_files_hash = map {$_->{'key'} => $page_index++} @$DOWNLOAD_FILES;
    if (exists($allowed_pages{$page_name}) && $allowed_pages{$page_name}
        && exists($download_files_hash{$page_name}))
    {
        $page_parameters->{'content'} = 'downloads/downloads.tt';
        $page_parameters->{'download'}    = $DOWNLOAD_FILES->[$download_files_hash{$page_name}];
    }
    # change appearance when in popup mode
    my $mode = GetPageMode();
    if ($Greenphyl::Web::PAGE_MODE_FRAME eq $mode)
    {
        $page_parameters->{'title'}   = 'Downloads';
        $page_parameters->{'body_id'} = 'downloads_body';
    }

    return RenderHTMLFullPage($page_parameters);
}


# Script options
#################

=pod

=head1 OPTIONS

downloads.cgi p=<downloads_page>

=head2 Parameters

=over 4

=item B<p> (string):

name of the requested downloads page.
Can be one of:
-'datasource'
-'faq'
-'methodology'
-'overview'
-'references'
-'team'
-'usecases'
Default: datasource

=back

=cut


# CODE START
#############

# hash for function selection.
my $actions =
    {
        #+storage issue '' => \&RenderIssue,
        '' => \&RenderDownloads,
    };

# try block
eval
{
    # display page
    print ProceedPage($actions);
};

HandleErrors();

exit(0);

# CODE END
###########


=pod

=head1 DIAGNOSTICS

=head1 AUTHORS

Mathieu ROUARD (Bioversity), m.rouard@cgiar.org

Valentin GUIGNON (Bioversity), v.guignon@cgiar.org

=head1 VERSION

Version 1.0.0

Date 24/04/2020

=head1 SEE ALSO

GreenPhyl downloads.

=cut
