#!/usr/bin/perl

=pod

=head1 NAME

svg.pl - Generate SVG downloadable data

=head1 SYNOPSIS

    svg.pl?file_name=image.svg&content=<svg ...</svg>

=head1 REQUIRES

Perl5

=head1 DESCRIPTION

This script can be used in combination with AJAX call to generate the
appropriate header and content for SVG download.

jQuery can be used to gather SVG content and use an AJAX POST call to this
script to generate the SVG file ready for user download.

Submitted SVG content is checked for security purpose. A file name can be
optionaly specified.

=cut

use strict;
use warnings;

use lib '../lib';
use lib '../local_lib';
use Carp qw (cluck confess croak);

use utf8;
use URI::Escape;
 
use Greenphyl;
use Greenphyl::Web;
use Greenphyl::Web::Templates;



# Script options
#################

=pod

=head1 OPTIONS

    svg.pl content=<SVG_CONTENT> [file_name=<file_name>]

=head2 Parameters

=over 4

=item B<file_name> (string):

name of the output file. If no name is specified, output to screen.

=item B<SVG_CONTENT> (string):

The SVG content encoded in URI format.

=back

=cut


# CODE START
#############

my $svg_args = {
    'mime' => 'SVG',
};

# try block
eval
{
    my $file_name = GetParameterValues('file_name');
    if ($file_name)
    {
        $svg_args->{'mime'} = 'DISK';
        $svg_args->{'file_name'} = $file_name;
    }

    # get and check SVG content
    my $content = GetParameterValues('content');
    
    if ($content)
    {
        # decode content
        $content = uri_unescape($content);
        utf8::decode($content); # decode special UTF8 characters
    }
    else
    {
        Throw('error' => 'No SVG content!');
    }
     # accent characters allowed (ë for Raphaël)
    if ($content !~ m/^\s*<svg\s+[\s\x21-\x7eÀÂÇÉÊÈËÏÔÛÙàâçéêèëïôûù]+<\/svg\s*>\s*$/s)
    {
        my $unmatched_char = $content;
        $unmatched_char =~ s/[\s\x21-\x7eÀÂÇÉÊÈËÏÔÛÙàâçéêèëïôûù]+//g;
        Throw('error' => 'Invalid SVG content!', 'log' => "content:\n$content\n\nUnmatched characters:\n----------\n$unmatched_char\n----------\n");
    }
    utf8::encode($content); # reencode
    $svg_args->{'content'} = $content;

    my $svg_output = RenderHTTPFileHeader($svg_args);
    $svg_output   .= ProcessTemplate('svg.tt', $svg_args);
    print $svg_output;
};

HandleErrors();

exit(0);

# CODE END
###########


=pod

=head1 AUTHORS

Mathieu ROUARD (Bioversity), m.rouard@cgiar.org

Valentin GUIGNON (Bioversity), v.guignon@cgiar.org

Matthieu CONTE (Syngenta)

=head1 VERSION

Version 1.0.0

Date 30/04/2012

=head1 SEE ALSO

GreenPhyl documentation, template dumps.tt documentation.

=cut
