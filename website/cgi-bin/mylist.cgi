#!/usr/bin/perl

=pod

=head1 NAME

mylist.cgi - render GreenPhyl "My List" page

=head1 SYNOPSIS

    mylist.cgi
    mylist.cgi?p=show_list
    mylist.cgi?p=add_entry&type=sequence&id=At2g21770.1
    mylist.cgi?p=add_entry&type=sequence&id=At2g21770.1

=head1 DESCRIPTION

Performs optional actions on "My List" and displays the
"My List" page.

=cut

use strict;
use warnings;

use lib '../lib';
use lib '../local_lib';
use Carp qw (cluck confess croak);

use Greenphyl;
use Greenphyl::Web;
use Greenphyl::Web::Access;
use Greenphyl::Tools::MyList;
use Greenphyl::MyListItem;
use Greenphyl::MyList;
use Greenphyl::Family;
use Greenphyl::CustomFamily;
use Greenphyl::Sequence;
use Greenphyl::CustomSequence;
use Greenphyl::CompositeObject;
use Greenphyl::Dumper;




# Script global constants
##########################

=pod

=head1 CONSTANTS

B<$DEBUG>: (boolean)

Enables debug mode.

=cut

our $DEBUG = 0;



# Script global variables
##########################

=pod

=head1 VARIABLES

B<$g_dbh>: (DBI::db)

Current database handler.

=cut

my $g_dbh = GetDatabaseHandler();




# Script global functions
##########################

=pod

=head1 FUNCTIONS

=head2 DumpMyList

B<Description>: dump current list of sequences and families to the web client.

B<ArgsCount>: 0

B<Return>: (string)

The dump content.

=cut

sub DumpMyList
{
    my $mylist;

    # load My List from database if an ID has been specified
    my $mylist_id = GetParameterValues('list_id');
    if ($mylist_id && ($mylist_id =~ m/^\d+$/))
    {
        # only dump My List of current user
        my $current_user = GetCurrentUser();
        $mylist = Greenphyl::MyList->new(
                GetDatabaseHandler(),
                {
                    'selectors' => {'id' => $mylist_id, 'user_id' => $current_user->id,},
                },
            );

    }
    else
    {
        # no ID provided, use current My List
        $mylist = GetCurrentMyList();
    }
    
    if (!$mylist)
    {
        Throw('error' => 'No My List to dump!', 'log' => ($mylist_id ? "list_id: $mylist_id" : 'no list_id'));
    }

    # get dump type
    my $dump_type = GetParameterValues('type') || '';

    # get dump format
    my $dump_format = GetDumpFormat();
    
    # get file name to display
    my $file_name = GetDumpFileName();

    # get objects
    my $dumpable_objects = [];
    # check if we dump list or sequences
    if ($dump_type =~ m/sequences/i)
    {
        # dump sequences
        $dumpable_objects = GetMyListAllSequences($mylist);
    }
    else
    {
        # dump list
        my $objects = GetSortedMyListObjects($mylist);
        foreach my $type (keys(%$objects))
        {
            foreach my $object (@{$objects->{$type}})
            {
                my $composite = Greenphyl::CompositeObject->new(
                    $object,
                    {
                        'type' => $type,
                        'id'   =>  '' . $object,
                    },
                );
                push(
                    @$dumpable_objects,
                    $composite,
                );
            }
        }
    }
    
    if (!@$dumpable_objects)
    {
        Throw('error' => 'No element to dump!', 'log' => ($mylist_id ? "list_id: $mylist_id" : 'no list_id'));
    }

    # get dump parameters
    my $parameters      = GetDumpParameters();
    
    # dump data
    my $dump_content = DumpObjects($dumpable_objects, $dump_format, $parameters);

    # get header after content in case we got an error during content generation
    # so we can still generate a clean header

    my $header = '';
    if ($file_name)
    {
        # to file
        $header = RenderHTTPFileHeader({'file_name' => $file_name, 'format' => $dump_format});
    }
    else
    {
        # to screen
        $header = RenderHTTPFileHeader({'mime' => 'TEXT', 'format' => $dump_format});
    }

    return $header . $dump_content;
}


=pod

=head2 RenderRemoveEntry

B<Description>: remove specified entry and display "My List" page.

B<ArgsCount>: 0

B<Return>: (string)

The page content.

=cut

sub RenderRemoveEntry
{
    my ($type, $id) = GetEntryTypeAndID();

    # get current list
    my $mylist_hash = FetchCurrentMyListHash();

    # remove entry according to its type and ID
    delete($mylist_hash->{$type}{$id});

    # if there's not more entry of this type, remove the type
    if (!keys(%{$mylist_hash->{$type}}))
    {
        delete($mylist_hash->{$type});
    }

    return RenderList();
}


=pod

=head2 RenderAddEntry

B<Description>: add specified entry and display "My List" page.

B<ArgsCount>: 0

B<Return>: (string)

The page content.

=cut

sub RenderAddEntry
{
    my ($type, $id) = GetEntryTypeAndID();

    if ($type && exists($Greenphyl::MyListItem::ITEM_TYPES_TO_CLASS->{$type}))
    {
        my $mylist_hash = FetchCurrentMyListHash();
        $mylist_hash->{$type}->{$id} = 1;
        SetCurrentMyList($mylist_hash);
    }

    return RenderList();
}


=pod

=head2 RenderReplaceEntries

B<Description>: replace current my list entries by the specified ones and
display "My List" page.

B<ArgsCount>: 0

B<Return>: (string)

The page content.

=cut

sub RenderReplaceEntries
{
    my $mapping = GetMultipleEntryTypeAndID();

    if (%$mapping)
    {
        my $mylist_hash = ClearCurrentMyList();

        my ($type, $ids);
        while (($type, $ids) = each %$mapping)
        {
            if ($type && exists($Greenphyl::MyListItem::ITEM_TYPES_TO_CLASS->{$type}))
            {
                foreach my $id (@$ids)
                {
                    $mylist_hash->{$type}->{$id} = 1;
                }
            }
        }

        SetCurrentMyList($mylist_hash);
    }

    return RenderList();
}


=pod

=head2 RenderAddEntries

B<Description>: add specified entries and display "My List" page.

B<ArgsCount>: 0

B<Return>: (string)

The page content.

=cut

sub RenderAddEntries
{
    my $mapping = GetMultipleEntryTypeAndID();

    if (%$mapping)
    {
        my $mylist_hash = FetchCurrentMyListHash();

        my ($type, $ids);
        while (($type, $ids) = each %$mapping)
        {
            if ($type && exists($Greenphyl::MyListItem::ITEM_TYPES_TO_CLASS->{$type}))
            {
                foreach my $id (@$ids)
                {
                    $mylist_hash->{$type}->{$id} = 1;
                }
            }
        }

        SetCurrentMyList($mylist_hash);
    }

    return RenderList();
}


=pod

=head2 RenderIntersectEntries

B<Description>: add specified entries and display "My List" page.

B<ArgsCount>: 0

B<Return>: (string)

The page content.

=cut

sub RenderIntersectEntries
{
    my $mapping = GetMultipleEntryTypeAndID();
    
    if (%$mapping)
    {
        my $mylist_hash = FetchCurrentMyListHash();
        my $new_mylist_hash = {};

        my ($type, $ids);
        while (($type, $ids) = each %$mapping)
        {
            if ($type && exists($Greenphyl::MyListItem::ITEM_TYPES_TO_CLASS->{$type}))
            {
                foreach my $id (@$ids)
                {
                    if (exists($mylist_hash->{$type}->{$id}))
                    {
                        $new_mylist_hash->{$type}->{$id} = 1;
                    }
                }
            }
        }
        SetCurrentMyList($new_mylist_hash);
    }

    return RenderList();
}


=pod

=head2 RenderSubstractEntries

B<Description>: add specified entries and display "My List" page.

B<ArgsCount>: 0

B<Return>: (string)

The page content.

=cut

sub RenderSubstractEntries
{
    my $mapping = GetMultipleEntryTypeAndID();

    if (%$mapping)
    {
        my $mylist_hash = FetchCurrentMyListHash();

        my ($type, $ids);
        while (($type, $ids) = each %$mapping)
        {
            if ($type && exists($Greenphyl::MyListItem::ITEM_TYPES_TO_CLASS->{$type}))
            {
                foreach my $id (@$ids)
                {
                    delete($mylist_hash->{$type}->{$id});
                    if (!%{$mylist_hash->{$type}})
                    {
                        delete($mylist_hash->{$type});
                    }
                }
            }
        }

        SetCurrentMyList($mylist_hash);
    }

    return RenderList();
}


=pod

=head2 RenderClearList

B<Description>: clears "My List".

B<ArgsCount>: 0

B<Return>: (string)

The page content.

=cut

sub RenderClearList
{
    # clear list
    ClearCurrentMyList();

    return RenderList(GetCurrentMyList());
}


=pod

=head2 RenderSaveList

B<Description>: display "My List" page.

B<ArgsCount>: 0

B<Return>: (string)

The page content.

=cut

sub RenderSaveList
{
    # check if current user is allowed to save lists
    InitAccess();

    my $requirements = {
        'require_access' => [GP('ACCESS_KEY_REGISTERED_USER'),],
        'access_needs'   => 'any',
    };

    my $current_user = GetCurrentUser();

    # check if current family is editable by current user
    if (!CheckAccess($requirements, $current_user))
    {
        ThrowAccessDenied('error' => 'You are not allowed to save list of objects! You need a user account.');
    }

    # get list name
    my $list_name = GetParameterValues('list_name') || 'No name';

    # get list description
    my $list_description = GetParameterValues('list_description') || '';

    # get list elements
    my $mylist_hash  = FetchCurrentMyListHash();
    
    if (!$mylist_hash || !%$mylist_hash)
    {
        Throw('error' => 'Empty list! Nothing to save.');
    }

    my $sql_query = "
        INSERT INTO my_lists (name, description, user_id)
        VALUES (?, ?, ?);
    ";
    PrintDebug("SQL Query: $sql_query\nBindings: $list_name, $list_description, " . $current_user->id);
    $g_dbh->do($sql_query, undef, $list_name, $list_description, $current_user->id);
    
    # get list ID
    my $new_list_id = $g_dbh->last_insert_id(undef, undef, 'my_lists', 'id')
        or confess "ERROR: Failed to retrieve new list ID!\n";

    my @bindings;
    my @space_holders;
    foreach my $object_type (keys(%$mylist_hash))
    {
        foreach my $object_id (keys(%{$mylist_hash->{$object_type}}))
        {
            push(@bindings, $new_list_id, $object_id, $object_type);
            push(@space_holders, "(?, ?, ?)");
        }
    }

    $sql_query = "
        INSERT INTO my_list_items (my_list_id, foreign_id, type)
        VALUES " . join(",\n  ", @space_holders) . ";
    ";
    PrintDebug("SQL Query: $sql_query\nBindings: " . join(', ', @bindings));
    $g_dbh->do($sql_query, undef, @bindings);

    return RenderLists();
}


=pod

=head2 RenderSaveListForm

B<Description>: .

B<ArgsCount>: 0

B<Return>: (string)

The page content.

=cut

sub RenderSaveListForm
{
    return RenderHTMLFullPage(
        {
            'header_args' => ['-expires' => '-1d'],
            'title'       => 'Save List',
            'content'     => 'mylists/mylist_save_form.tt',
            'body_id'     => 'mylist_body',
        },
    );
}


=pod

=head2 RenderLoadSavedList

B<Description>: .

B<ArgsCount>: 0

B<Return>: (string)

The page content.

=cut

sub RenderLoadSavedList
{
    my $list_id = GetParameterValues('list_id');
    
    if ($list_id)
    {
        # check if current user is allowed to have lists
        InitAccess();

        my $requirements = {
            'require_access' => [GP('ACCESS_KEY_REGISTERED_USER'),],
            'access_needs'   => 'any',
        };

        my $current_user = GetCurrentUser();

        # check if current family is editable by current user
        if (!CheckAccess($requirements, $current_user))
        {
            ThrowAccessDenied('error' => 'You are not allowed to load list of objects! You need a user account.');
        }
        
        my $mylist = Greenphyl::MyList->new(
            $g_dbh,
            {
                'selectors' =>
                {
                    'id' => $list_id,
                    'user_id' => $current_user->id,
                },
            },
        );
        
        if (!$mylist)
        {
            Throw('error' => 'List not found!', 'log' => "User: " . $current_user->id . "\nList: $list_id");
        }

        SetCurrentMyList($mylist);
    }
    else
    {
        PrintDebug('No given list to load!');
    }

    return RenderList(GetCurrentMyList());
}


=pod

=head2 RenderImportMyList

B<Description>: .

B<ArgsCount>: 0

B<Return>: (string)

The page content.

=cut

sub RenderImportMyList
{
    # get from either text field or file upload
    my $list_content = GetParameterValues('list', undef, undef, 1);
    
    if ($list_content)
    {
        # process importation
        my $current_user = GetCurrentUser();

        my $mylist_hash = {};
        my @lines = split(/[\n\r]+/, $list_content);
        # skip first line
        if (@lines && ($lines[0] =~ m/1\.\sType/))
        {
            shift(@lines);
        }

        foreach my $line (@lines)
        {
            # parse line
            my ($type, $accession) = ($line =~ m/^['"]?(\w+)\s*['"]?\s*[,\t]\s*['"]?([a-zA-Z0-9_.]+)/);
            # check if type is valid
            if ($type && exists($Greenphyl::MyListItem::ITEM_TYPES_TO_CLASS->{$type}))
            {
                my $object_module = $Greenphyl::MyListItem::ITEM_TYPES_TO_CLASS->{$type};
                my $object_properties;
                my $object_table;
                eval "use $object_module; \$object_properties = \$" . $object_module . "::OBJECT_PROPERTIES;";
                if ($@)
                {
                    confess $@;
                }
                
                if (!$object_properties || !($object_table = $object_properties->{'table'}))
                {
                    confess "ERROR: failed to get item table!";
                }

                my $user_clause = '';
                if ($type =~ m/Custom/i)
                {
                    $user_clause = 'AND o.user_id = ' . $current_user->id;
                }

                my $sql_query = "
                    SELECT o.id
                    FROM $object_table o
                    WHERE o.accession = ?
                        $user_clause
                    ;
                ";
                PrintDebug("SQL Query: $sql_query\nBindings: $accession");
                my ($object_id) = $g_dbh->selectrow_array($sql_query, undef, $accession)
                    or confess 'ERROR: Failed to fetch object: ' . $g_dbh->errstr;
                $mylist_hash->{$type} ||= {};
                $mylist_hash->{$type}->{$object_id} = 1;
            }
        }

        SetCurrentMyList($mylist_hash);
    }
    else
    {
        # display importation form
        return RenderHTMLFullPage(
            {
                'title'       => 'Import My List',
                'content'     => 'mylists/import_form.tt',
            },
        );
    }

    return RenderList(GetCurrentMyList());
}


=pod

=head2 RenderDeleteList

B<Description>: .

B<ArgsCount>: 0

B<Return>: (string)

The page content.

=cut

sub RenderDeleteList
{
    my $list_id = GetParameterValues('list_id');
    
    if ($list_id)
    {
        # check if current user is allowed to have lists
        InitAccess();

        my $requirements = {
            'require_access' => [GP('ACCESS_KEY_REGISTERED_USER'),],
            'access_needs'   => 'any',
        };

        my $current_user = GetCurrentUser();

        # check if current family is editable by current user
        if (!CheckAccess($requirements, $current_user))
        {
            ThrowAccessDenied('error' => 'You are not allowed to delete lists! You need a user account.');
        }

        my $sql_query = 'DELETE FROM my_lists WHERE id = ? AND user_id = ?;';
        PrintDebug("SQL Query: $sql_query\nBindings: $list_id, " . $current_user->id);
        my $affected_rows = $g_dbh->do($sql_query, undef, $list_id, $current_user->id);
        if (!$affected_rows || (0 == $affected_rows))
        {
            Throw('error' => 'Failed to remove selected list!', 'log' => $g_dbh->errstr);
        }
    }
    else
    {
        PrintDebug('No given list to load!');
    }

    return RenderLists();
}


=pod

=head2 RenderMergeLists

B<Description>: .

B<ArgsCount>: 0

B<Return>: (string)

The page content.

=cut

sub RenderMergeLists
{
    my $list_id = GetParameterValues('list_id');
    
    if ($list_id)
    {
        # check if current user is allowed to have lists
        InitAccess();

        my $requirements = {
            'require_access' => [GP('ACCESS_KEY_REGISTERED_USER'),],
            'access_needs'   => 'any',
        };

        my $current_user = GetCurrentUser();

        # check if current family is editable by current user
        if (!CheckAccess($requirements, $current_user))
        {
            ThrowAccessDenied('error' => 'You are not allowed to merge list of objects! You need a user account.');
        }

        # get user mylist
        my $db_mylist = Greenphyl::MyList->new(
            $g_dbh,
            {
                'selectors' =>
                {
                    'id' => $list_id,
                    'user_id' => $current_user->id,
                },
            },
        );
        
        if (!$db_mylist)
        {
            Throw('error' => 'List not found!', 'log' => "User: " . $current_user->id . "\nList: $list_id");
        }

        # get current mylist
        my $current_mylist = FetchCurrentMyListHash();

        # merge lists
        my $db_mylist_hash = $db_mylist->getAsMyListHash();
        foreach my $item_type (keys(%$db_mylist_hash))
        {
            foreach my $item_id (keys(%{$db_mylist_hash->{$item_type}}))
            {
                PrintDebug("Merging $item_id (" . $item_type . ")");
                $current_mylist->{$item_type}->{$item_id} = 1;
            }
        }

        SetCurrentMyList($current_mylist);
    }
    else
    {
        PrintDebug('No given list to load!');
    }

    return RenderLists();
}


=pod

=head2 RenderIntersectLists

B<Description>: .

B<ArgsCount>: 0

B<Return>: (string)

The page content.

=cut

sub RenderIntersectLists
{
    my $list_id = GetParameterValues('list_id');
    
    if ($list_id)
    {
        # check if current user is allowed to have lists
        InitAccess();

        my $requirements = {
            'require_access' => [GP('ACCESS_KEY_REGISTERED_USER'),],
            'access_needs'   => 'any',
        };

        my $current_user = GetCurrentUser();

        # check if current family is editable by current user
        if (!CheckAccess($requirements, $current_user))
        {
            ThrowAccessDenied('error' => 'You are not allowed to merge list of objects! You need a user account.');
        }

        # get user mylist
        my $db_mylist = Greenphyl::MyList->new(
            $g_dbh,
            {
                'selectors' =>
                {
                    'id' => $list_id,
                    'user_id' => $current_user->id,
                },
            },
        );
        
        if (!$db_mylist)
        {
            Throw('error' => 'List not found!', 'log' => "User: " . $current_user->id . "\nList: $list_id");
        }

        # get current mylist
        my $current_mylist = FetchCurrentMyListHash();

        # merge lists
        my $db_mylist_hash = $db_mylist->getAsMyListHash();
        my $new_mylist_hash = {};
        foreach my $item_type (keys(%$db_mylist_hash))
        {
            foreach my $item_id (keys(%{$db_mylist_hash->{$item_type}}))
            {
                if (exists($current_mylist->{$item_type}->{$item_id}))
                {
                    PrintDebug("Intersection on $item_id (" . $item_type . ")");
                    $new_mylist_hash->{$item_type} ||= {};
                    $new_mylist_hash->{$item_type}->{$item_id} = 1;
                }
            }
        }

        SetCurrentMyList($new_mylist_hash);
    }
    else
    {
        PrintDebug('No given list to load!');
    }

    return RenderLists();
}


=pod

=head2 RenderSubstractLists

B<Description>: .

B<ArgsCount>: 0

B<Return>: (string)

The page content.

=cut

sub RenderSubstractLists
{
    my $list_id = GetParameterValues('list_id');
    
    if ($list_id)
    {
        # check if current user is allowed to have lists
        InitAccess();

        my $requirements = {
            'require_access' => [GP('ACCESS_KEY_REGISTERED_USER'),],
            'access_needs'   => 'any',
        };

        my $current_user = GetCurrentUser();

        # check if current family is editable by current user
        if (!CheckAccess($requirements, $current_user))
        {
            ThrowAccessDenied('error' => 'You are not allowed to merge list of objects! You need a user account.');
        }

        # get user mylist
        my $db_mylist = Greenphyl::MyList->new(
            $g_dbh,
            {
                'selectors' =>
                {
                    'id' => $list_id,
                    'user_id' => $current_user->id,
                },
            },
        );
        
        if (!$db_mylist)
        {
            Throw('error' => 'List not found!', 'log' => "User: " . $current_user->id . "\nList: $list_id");
        }

        # get current mylist
        my $current_mylist_hash = FetchCurrentMyListHash();

        # merge lists
        my $db_mylist_hash = $db_mylist->getAsMyListHash();
        foreach my $item_type (keys(%$db_mylist_hash))
        {
            foreach my $item_id (keys(%{$db_mylist_hash->{$item_type}}))
            {
                if (exists($current_mylist_hash->{$item_type}->{$item_id}))
                {
                    PrintDebug("Substract $item_id (" . $item_type . ")");
                    delete($current_mylist_hash->{$item_type}->{$item_id});
                    if (!%{$current_mylist_hash->{$item_type}})
                    {
                        delete($current_mylist_hash->{$item_type});
                    }
                }
            }
        }

        SetCurrentMyList($current_mylist_hash);
    }
    else
    {
        PrintDebug('No given list to load!');
    }

    return RenderLists();
}


=pod

=head2 RenderConvertToSequence

B<Description>: convert families into list of sequences and display "My List"
page.

B<ArgsCount>: 0

B<Return>: (string)

The page content.

=cut

sub RenderConvertToSequence
{
    my $mylist;
    my $list_id = GetParameterValues('list_id');

    InitAccess();
    my $current_user = GetCurrentUser();

    if ($list_id)
    {
        # check if current user is allowed to have lists
        my $requirements = {
            'require_access' => [GP('ACCESS_KEY_REGISTERED_USER'),],
            'access_needs'   => 'any',
        };

        # check if current list is visible by current user
        if (!CheckAccess($requirements, $current_user))
        {
            ThrowAccessDenied('error' => 'You are not allowed to use saved list of objects! You need a user account.');
        }

        # get user mylist
        $mylist = Greenphyl::MyList->new(
            $g_dbh,
            {
                'selectors' =>
                {
                    'id' => $list_id,
                    'user_id' => $current_user->id,
                },
            },
        );
        
        if (!$mylist)
        {
            Throw('error' => 'List not found!', 'log' => "User: " . $current_user->id . "\nList: $list_id");
        }
    }

    # get current mylist
    my $current_mylist_hash = FetchCurrentMyListHash();

    if ($current_mylist_hash && %$current_mylist_hash)
    {
        # convert families into sequences
        if ($current_mylist_hash->{'Family'} && %{$current_mylist_hash->{'Family'}})
        {
            $current_mylist_hash->{'Sequence'} ||= {};
            foreach my $family_id (keys(%{$current_mylist_hash->{'Family'}}))
            {
                PrintDebug("Converting family $family_id into sequences");
                # load family
                my $family = Greenphyl::Family->new(
                    $g_dbh,
                    {
                        'selectors' =>
                        {
                            'id' => $family_id,
                        },
                    },
                );
                if ($family)
                {
                    # add each sequence
                    foreach my $sequence ($family->sequences())
                    {
                        $current_mylist_hash->{'Sequence'}->{$sequence->id} = 1;
                    }
                }
            }
            delete($current_mylist_hash->{'Family'});
        }

        # convert custom families into custom sequences
        if ($current_mylist_hash->{'CustomFamily'} && %{$current_mylist_hash->{'CustomFamily'}})
        {
            $current_mylist_hash->{'CustomSequence'} ||= {};
            foreach my $custom_family_id (keys(%{$current_mylist_hash->{'CustomFamily'}}))
            {
                PrintDebug("Converting custom family $custom_family_id into sequences");
                # load custom family
                my $custom_family = Greenphyl::CustomFamily->new(
                    $g_dbh,
                    {
                        'selectors' =>
                        {
                            'id' => $custom_family_id,
                            'user_id' => $current_user->id,
                        },
                    },
                );
                if ($custom_family)
                {
                    # add each sequence
                    foreach my $custom_sequence ($custom_family->sequences())
                    {
                        $current_mylist_hash->{'CustomSequence'}->{$custom_sequence->id} = 1;
                    }
                }
            }
            delete($current_mylist_hash->{'CustomFamily'});
        }

        SetCurrentMyList($current_mylist_hash);
    }


    return RenderList();
}


=pod

=head2 RenderList

B<Description>: display "My List" page.

B<ArgsCount>: 0

B<Return>: (string)

The page content.

=cut

sub RenderList
{
    my ($mylist, $list_id);
    
    # remove "page" parameter when called from ProceedPage
    if (@_ && ('HASH' eq ref($_[0])))
    {
        shift(@_);
    }

    # check if a "mylist" has been specified
    ($mylist) = @_;
    if (!$mylist || (ref($mylist) ne 'Greenphyl::MyList'))
    {
        $list_id = GetParameterValues('list_id');
    }
    
    if ($list_id)
    {
        # check if current user is allowed to have lists
        InitAccess();

        my $requirements = {
            'require_access' => [GP('ACCESS_KEY_REGISTERED_USER'),],
            'access_needs'   => 'any',
        };

        my $current_user = GetCurrentUser();

        # check if current list is visible by current user
        if (!CheckAccess($requirements, $current_user))
        {
            ThrowAccessDenied('error' => 'You are not allowed to have list of objects! You need a user account.');
        }

        # get user mylist
        $mylist = Greenphyl::MyList->new(
            $g_dbh,
            {
                'selectors' =>
                {
                    'id' => $list_id,
                    'user_id' => $current_user->id,
                },
            },
        );
        
        if (!$mylist)
        {
            Throw('error' => 'List not found!', 'log' => "User: " . $current_user->id . "\nList: $list_id");
        }
    }

    # default will get current my list if none specified
    my $objects = GetSortedMyListObjects($mylist);

    return RenderHTMLFullPage(
        {
            'header_args' => ['-expires' => '-1d'],
            'title'       => 'My List',
            'content'     => 'mylists/mylist_page.tt',
            'body_id'     => 'mylist_body',
            'types'       =>
            {
                'Family' =>
                {
                    'label'    => 'Family',
                    'template' => 'families/family_id.tt',
                    'object_alias' => 'family',
                    'icon'     => 'cluster',
                },
                'CustomFamily' =>
                {
                    'label'    => 'Custom Family',
                    'template' => 'families/family_id.tt',
                    'object_alias' => 'family',
                    'icon'     => 'custom_cluster',
                },
                'Sequence' =>
                {
                    'label'    => 'Sequence',
                    'template' => 'sequences/sequence_id.tt',
                    'object_alias' => 'sequence',
                    'icon'     => 'sequence',
                },
                'CustomSequence' =>
                {
                    'label'    => 'Custom Sequence',
                    'template' => 'sequences/sequence_id.tt',
                    'object_alias' => 'sequence',
                    'icon'     => 'custom_sequence',
                },
            },
            'mylist'  => $mylist,
            'objects' => $objects,
        },
    );
}


=pod

=head2 RenderLists

B<Description>: display the list of My List owned by current user.

B<ArgsCount>: 0

B<Return>: (string)

The page content.

=cut

sub RenderLists
{

    my $current_user = GetCurrentUser();

    if (!$current_user)
    {
        Throw('error' => 'Only registered users are allowed to manage several "My List"!');
    }

    my $mylists  = [Greenphyl::MyList->new(
        $g_dbh,
        {
            'selectors' =>
            {
                'user_id' => $current_user->id,
            },
        },
    )];

    my $mylist = GetCurrentMyList();
    
    return RenderHTMLFullPage(
        {
            'title'       => 'My Lists',
            'content'     => 'mylists/mylist_list.tt',
            'body_id'     => 'mylist_body',
            'mylists'     => $mylists,
            'current_mylist' => $mylist,
        },
    );
}




# Script options
#################

=pod

=head1 OPTIONS

Takes optional GET parameters.

=head2 Parameters

=over 4

=item B<p> (string):

The action to perform. It can be one of: show_list, add_entry, remove_entry,
dump.
show_list: just display the table;
add_entry: add specified entry (using type and id parameters) and display the
           table;
remove_entry: remove specified entry (using type and id parameters) and display
              the table;
dump: dump current list.
Default: show_list

=item B<type> (string):

Type of the object specified by the identifier. Can be one of: sequence, family.

=item B<id> (string):

Identifier of the object to perform a action on.

=back

=cut

# CODE START
#############

# hash for function selection.
my $actions =
    {
        ''             => \&RenderList,
        'show_list'    => \&RenderList,
        'list'         => \&RenderLists,
        'clear'        => \&RenderClearList,
        'save_form'    => \&RenderSaveListForm,
        'save'         => \&RenderSaveList,
        'load'         => \&RenderLoadSavedList,
        'delete'       => \&RenderDeleteList,
        'merge'        => \&RenderMergeLists,
        'intersect'    => \&RenderIntersectLists,
        'substract'    => \&RenderSubstractLists,
        'add_entry'    => \&RenderAddEntry,
        'remove_entry' => \&RenderRemoveEntry,
        'replace_entries'   => \&RenderReplaceEntries,
        'add_entries'       => \&RenderAddEntries,
        'intersect_entries' => \&RenderIntersectEntries,
        'substract_entries' => \&RenderSubstractEntries,
        'import'       => \&RenderImportMyList,
        'to_sequences' => \&RenderConvertToSequence,
        'dump'         => \&DumpMyList,
    };

# try block
eval
{
    # display page
    print ProceedPage($actions);
};

HandleErrors();

GetSession()->flush;

exit(0);

# CODE END
###########


=pod

=head1 AUTHORS

Mathieu ROUARD (Bioversity), m.rouard@cgiar.org

Valentin GUIGNON (Bioversity), v.guignon@cgiar.org

Matthieu CONTE (Syngenta)

=head1 VERSION

Version 1.0.0

Date 12/07/2012

=head1 SEE ALSO

GreenPhyl documentation.

=cut
