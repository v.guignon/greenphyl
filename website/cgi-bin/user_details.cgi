#!/usr/bin/perl

=pod

=head1 NAME

user_details.cgi - User account detail page

=head1 SYNOPSIS

    user_details.cgi

=head1 REQUIRES

Perl5

=head1 DESCRIPTION

Display user account details. If no user is specified, it displays current user
details.

=cut

use strict;
use warnings;

use FindBin;
use lib "$FindBin::Bin/../lib";
use lib "$FindBin::Bin/../local_lib";
use Carp qw (cluck confess croak);

use Greenphyl;
use Greenphyl::Web;
use Greenphyl::Web::Access;
use Greenphyl::Tools::Users;
use Greenphyl::OpenID;





# Script global constants
##########################

=pod

=head1 CONSTANTS

B<$DEBUG>: (boolean)

When set to true, it enables debug mode.

=cut

our $DEBUG = 0;



# Script global functions
##########################

=pod

=head1 FUNCTIONS

=head2 RenderUserOverview

B<Description>: Display user details. If no user ID is provided, current user ID
is used. If the given user ID differs from current user ID, current user is
requested to have administrator access level to view user details.

B<ArgsCount>: 0

B<Return>: (string)

Returns an HTML string containing the GreenPhyl page for user details.

=cut

sub RenderUserOverview
{
    # remove "page" parameter when called from ProceedPage
    if (@_ && ('HASH' eq ref($_[0])))
    {
        shift(@_);
    }
    my ($user, $confirmation_message) = @_;

    my $user_id ||= GetParameterValues('user_id');
    my $required_access = [GP('ACCESS_KEY_REGISTERED_USER')];
    my $access_message = 'You need to log in first!';

    if (!$user)
    {
        if ($user_id && (GetCurrentUser()->id != $user_id))
        {
            $required_access = [GP('ACCESS_KEY_ADMINISTRATOR')];
            if (IsAdmin())
            {
                $user = Greenphyl::User->new(
                    GetDatabaseHandler(),
                    {
                        'selectors' => {'id' => $user_id},
                    },
                ) || $Greenphyl::User::ANONYMOUS;
            }
            else
            {
                $user = $Greenphyl::User::ANONYMOUS;
                $access_message = 'You are not allowed to access to this page!';
            }
        }
        else
        {
            $user = GetCurrentUser();
        }
    }

    my $flags_def = {GetTableInfo('users')}->{'flags'}->{'type'};
    $flags_def =~ s/^set\('//;
    $flags_def =~ s/'\)$//;
    my $flags = [split(/','/, $flags_def)];

    my $page_parameters = GetOpenIDPageParameters(
        {
            'title'          => 'User Details',
            'content'        => 'users/user_overview.tt',
            'confirmation_message' => $confirmation_message,
            'access_message' => $access_message,
            'require_access' => $required_access,
            'access_needs'   => 'all',
            'user'           => $user,
            'flags'          => $flags,
        },
    );
    
    return RenderHTMLFullPage($page_parameters);
}


=pod

=head2 RenderUpdateUser

B<Description>: Save user data into database.

B<ArgsCount>: 0-1

B<Return>: (string)

Returns an HTML string containing a confirmation message.

=cut

sub RenderUpdateUser
{
    my $user_id = GetParameterValues('user_id') || 0;

    my $dbh = GetDatabaseHandler();
    my $user = Greenphyl::User->new(
            $dbh,
            {'selectors' => {'id' => $user_id,},}
        )
    ;

    my $confirmation_message = "User not saved!";
    if ($user)
    {
        # check access (admin or current user update)
        if (IsAdmin() || (GetCurrentUser()->id == $user_id))
        {
            PrintDebug("Processing user update");

            eval
            {
                my $user_data = GetUserDataFromForm($user);

                # remove user?
                if ($user_data->{'remove_user'})
                {
                    PrintDebug("Removing user '$user'");
                    DeleteUser($user);
                    # logout
                    if (GetCurrentUser()->id == $user_id)
                    {
                        my $session = GetSession();
                        $session->clear('user');
                        ThrowRedirection(GetURL('account_request'));
                    }
                    elsif (IsAdmin())
                    {
                        ThrowRedirection(GetURL('manage_users'));
                    }
                    else
                    {
                        ThrowRedirection(GetURL('user_details'));
                    }
                }
                
                # update user
                UpdateUser($user, $user_data);
                $confirmation_message = "User successfully updated!";

                # handle OpenID...
                # -remove unchecked OpenIDs
                my @user_openids = GetParameterValues('user_openids');
                my %keep_openids = map {$_ => 1,} GetParameterValues('keep_openids');
                
                foreach my $remove_openid (@user_openids)
                {
                    if (!exists($keep_openids{$remove_openid}))
                    {
                        PrintDebug("Removing OpenID '$remove_openid'");
                        DeleteUserOpenID($user, $remove_openid);
                    }
                    else
                    {
                        PrintDebug("Keeping OpenID '$remove_openid'");
                    }
                }
                # -add new OpenID
                my $target_url = GetURL('user_details', {'p' => 'add_openid', 'user_id' => $user->id});
                UpdateUserOpenID($user, $target_url);
            };

            my $error;
            if ($error = Exception::Class->caught('Greenphyl::Redirection'))
            {
                return $error->redirect();
            }
            elsif ($error = Exception::Class->caught('Greenphyl::Error'))
            {
                $confirmation_message = "Failed to update user: $error";
            }
            elsif ($error = Exception::Class->caught())
            {
                if (IsAdmin())
                {
                    $confirmation_message = "Failed to update user! An unexpected error occurred: $error";
                }
                else
                {
                    $confirmation_message = "Failed to update user!";
                }
            }
            elsif ($@)
            {
                # eval error
                confess $@;
            }
        }
        else
        {
            $confirmation_message = "You are not allowed to edit this user!";
        }
    }
    elsif ($user_id && !$user)
    {
        # unexisting user!
        Throw('error' => "Trying to update an unexisting user!", 'log' => "user_id: " . $user_id);
    }
    
    return RenderUserOverview($user, $confirmation_message);
}


=pod

=head2 RenderAddUserOpenID

B<Description>: Save user data into database.

B<ArgsCount>: 0-1

B<Return>: (string)

Returns an HTML string containing a confirmation message.

=cut

sub RenderAddUserOpenID
{

    if (!GP('ENABLE_OPENID'))
    {
        confess "Unexpected call to RenderOpenIDValidation!";
    }

    my $user_id = GetParameterValues('user_id') || 0;
    my $confirmation_message;

    my $dbh = GetDatabaseHandler();
    my $user = Greenphyl::User->new(
            $dbh,
            {'selectors' => {'id' => $user_id,},}
        )
    ;

    if ($user)
    {
        my ($status, $openid_data) = OpenIDAuthenticatePhase2();

        if ('verified' eq $status)
        {
            # get next priority
            my $sql_query = "SELECT CONCAT_WS(',',priority) FROM user_openids WHERE user_id = ?;";
            my %openid_priorities = map {$_ => 1} split(',', $dbh->selectrow_array($sql_query, undef, $user->id));
            my $priority = 0;
            while (exists($openid_priorities{$priority}))
            {++$priority;}
            
            $sql_query = "INSERT INTO user_openids (user_id, openid_url, openid_identity, priority) VALUES (?, ?, ?, ?);";
            if (!$dbh->do($sql_query, undef, $user->id, $openid_data, $openid_data, $priority))
            {
                Throw('error' => "Failed to add OpenID identity!");
            }
            $confirmation_message = 'User OpenID updated.';
        }
        else
        {
            $confirmation_message = $openid_data;
        }
    }

    return RenderUserOverview($user, $confirmation_message);
}




# Script options
#################

=pod

=head1 OPTIONS

user_details.cgi? [user_id=<user_id>]

=head2 Parameters

=over 4

=item B<user_id> (integer):

A user identifier.
Default: current user ID.

=back

=cut


# CODE START
#############

# hash for function selection.
my $actions =
    {
        '' => \&RenderUserOverview,
        'update' => \&RenderUpdateUser,
        'add_openid' => \&RenderAddUserOpenID,
    };

# try block
eval
{
    # display page
    print ProceedPage($actions);
};

HandleErrors();

exit(0);

# CODE END
###########


=pod

=head1 AUTHORS

Mathieu ROUARD (Bioversity), m.rouard@cgiar.org

Valentin GUIGNON (Bioversity), v.guignon@cgiar.org

Matthieu CONTE (Syngenta)

Sebastien BRIOIS (Syngenta)

=head1 VERSION

Version 1.0.0

Date 28/029/2012

=head1 SEE ALSO

GreenPhyl documentation.

=cut
