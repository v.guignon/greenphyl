#!/usr/bin/perl

=pod

=head1 NAME

issue.cgi - display Greenphyl issue page.

=head1 SYNOPSIS

    issue.cgi

=head1 REQUIRES

Perl5

=head1 DESCRIPTION

This script provides links to Greenphyl data files.

=cut

use strict;
use warnings;

use lib '../lib';
use lib '../local_lib';
use Carp qw (cluck confess croak);

use Greenphyl;
use Greenphyl::Web;
use Greenphyl::Species;
use Greenphyl::Variables;


# Script global constants
##########################

=pod

=head1 CONSTANTS

B<$DEBUG>: (boolean)

Enables debug mode.

=cut

our $DEBUG = 0;




# Script global functions
##########################

=pod

=head1 FUNCTIONS

=head2 RenderIssue

B<Description>: Renders issue page.

B<ArgsCount>: 0

B<Return>: (string)

Returns an HTML string containing the issue page.

=cut

sub RenderIssue
{
    my $page_parameters = {
            'title'   => 'Issue',
            'content' => <<"___77_ISSUE_MESSAGE___",
<p>
11/05/2021<br/>
We are currently experiencing hardware issues on our storage server which
impacts several GreenPhyl features. We are sorry for the inconvenience and hope
to find a solution as soon as possible.
</p>
___77_ISSUE_MESSAGE___
        };

    # change appearance when in popup mode
    my $mode = GetPageMode();
    if ($Greenphyl::Web::PAGE_MODE_FRAME eq $mode)
    {
        $page_parameters->{'title'}   = 'Issue';
        $page_parameters->{'body_id'} = '';
    }

    return RenderHTMLFullPage($page_parameters);
}


# Script options
#################

=pod

=head1 OPTIONS

issue.cgi

=cut


# CODE START
#############

# hash for function selection.
my $actions =
    {
        '' => \&RenderIssue,
    };

# try block
eval
{
    # display page
    print ProceedPage($actions);
};

HandleErrors();

exit(0);

# CODE END
###########


=pod

=head1 DIAGNOSTICS

=head1 AUTHORS

Mathieu ROUARD (Bioversity), m.rouard@cgiar.org

Valentin GUIGNON (Bioversity), v.guignon@cgiar.org

=head1 VERSION

Version 1.0.0

Date 11/05/2021

=head1 SEE ALSO

GreenPhyl downloads.

=cut
