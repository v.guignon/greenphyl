#!/usr/bin/perl

=pod

=head1 NAME

annotate_family.cgi - Family annotation interface

=head1 SYNOPSIS

    annotate_family.cgi?family_id=42

=head1 REQUIRES

Perl5

=head1 DESCRIPTION

Displays GreenPhyl family annotation interface. Access to the interface requires
annotator access.

=cut

use strict;
use warnings;

use lib '../lib';
use lib '../local_lib';
use Carp qw (cluck confess croak);

use Greenphyl;
use Greenphyl::Web;
use Greenphyl::Web::Access;
use Greenphyl::Tools::Families;

use Greenphyl::Family;
use Greenphyl::FamilyAnnotation;
use Greenphyl::SourceDatabase;
use Greenphyl::IPR;
use Greenphyl::DBXRef;

use Bio::Biblio;




# Script global constants
##########################

=pod

=head1 CONSTANTS

B<$CONSTANT_NAME>: ([constant nature]) #+++

[constant description and use]. #+++

#--- Example:
#--- B<$DEBUG>: (boolean)
#--- When set to true, it enables debug mode.
#--- ...
#--- our $DEBUG = 0;
#---
=cut

our $DEBUG = 0;





# Script global variables
##########################

=pod

=head1 VARIABLES

B<[g_variable_name]>: ([variable nature]) #+++

[variable description, use and default value]. #+++
Default: [variable default value if one] #+++

#--- Example:
#--- B<$g_output_method>: (integer)
#---
#--- used to store current output method;
#--- 0=raw text (default), 1=image, 2=html.
#---
#---     ...
#---
#--- my $g_output_method = 0;
#---

=cut

our $g_dbh = GetDatabaseHandler();




# Script global functions
##########################

=pod

=head1 FUNCTIONS

=head2 [RenderFunctionToCall] #+++

B<Description>: [function description]. #+++

B<ArgsCount>: 0

B<Return>: (string)

Returns an HTML string containing the GreenPhyl page for ... #+++

=cut

sub GetFamilySources
{
    my ($family) = @_;

    my $sql_query = "
        SELECT
          sii.sequence_id AS 'sequence_id',
          GROUP_CONCAT(sha.ipr_id SEPARATOR ',') AS 'ipr_ids',
          GROUP_CONCAT(sdx.dbxref_id SEPARATOR ',') AS 'dbxref_ids'
        FROM
          families f
          JOIN families_sequences sii ON (f.id = sii.family_id)
          LEFT JOIN ipr_sequences sha ON (sii.sequence_id = sha.sequence_id)
          LEFT JOIN dbxref_sequences sdx ON (sii.sequence_id = sdx.sequence_id)
        WHERE f.id = ?
        GROUP BY sii.sequence_id;";
    my $family_sources = $g_dbh->selectall_arrayref($sql_query, { 'Slice' => {} }, $family->id)
        or confess $g_dbh->errstr;

    my ($ipr_sources, $dbxref_sources) = ({}, {});

    foreach my $family_source (@$family_sources)
    {
        # IPR
        if ($family_source->{'ipr_ids'})
        {
            foreach my $ipr_id (split(/,/, $family_source->{'ipr_ids'}))
            {
                $ipr_sources->{$ipr_id} ||= {};
                $ipr_sources->{$ipr_id}->{'sequences'} ||= [];
                push(@{$ipr_sources->{$ipr_id}->{'sequences'}}, $family_source->{'sequence_id'});
            }
        }

        # DBXRef
        if ($family_source->{'dbxref_ids'})
        {
            foreach my $dbxref_id (split(/,/, $family_source->{'dbxref_ids'}))
            {
                $dbxref_sources->{$dbxref_id} ||= {};
                $dbxref_sources->{$dbxref_id}->{'sequences'} ||= [];
                push(@{$dbxref_sources->{$dbxref_id}->{'sequences'}}, $family_source->{'sequence_id'});
            }
        }
    }
    
    return ($ipr_sources, $dbxref_sources);
}

=pod

=head2 [RenderFunctionToCall] #+++

B<Description>: [function description]. #+++

B<ArgsCount>: 0

B<Return>: (string)

Returns an HTML string containing the GreenPhyl page for ... #+++

=cut

sub OverloadIPRObjects
{
    my ($ipr_sources, $family) = @_;

    # fetch all IPR from database at once (into cache)
    my $ipr_list = Greenphyl::IPR->new(
        $g_dbh,
        {'selectors' => { 'id' => ['IN', keys(%$ipr_sources)], } },
    );

    my $ipr_id_sources = $ipr_sources; # keep a reference to hash using ID as keys
    $ipr_sources = {}; # reset hash
    $ipr_sources->{'specific_family_type_ipr'} = [];
    $ipr_sources->{'ipr'} = [];
    $ipr_sources->{'pirsf'} = [];
    foreach my $ipr_id (keys(%$ipr_id_sources))
    {
        # get corresponding IPR object (from cache)
        my $ipr = Greenphyl::IPR->new(
                $g_dbh,
                {'selectors' => { 'id' => $ipr_id, } },
            );
        # if IPR loaded ok
        if ($ipr)
        {
            # use IPR code as hash key
            $ipr_sources->{$ipr} = $ipr_id_sources->{$ipr_id};
            # add IPR object
            $ipr_sources->{$ipr}->{'ipr'} = $ipr;
            # add percent
            my $ipr_stats = $ipr->family_stats({'selectors' => {'family_id' => $family->id,},});
            if (@$ipr_stats)
            {$ipr_stats = $ipr_stats->[0];}
            else
            {$ipr_stats = undef;}
            my $coverage_percent = ($ipr_stats ? $ipr_stats->percent : 0);
            $ipr_sources->{$ipr}->{'percent'} = (0 < $coverage_percent ? $coverage_percent . '%' : '<1%');
            # add specificity
            $ipr_sources->{$ipr}->{'specificity'} = ($ipr_stats && $ipr_stats->specificity ? 'Specific' : '');

            push(@{$ipr_sources->{'ipr'}}, $ipr);

            # Family and specific
            if ($ipr_sources->{$ipr}->{'specificity'}
                && ('Family' eq $ipr_sources->{$ipr}->{'ipr'}->type))
            {
                push(@{$ipr_sources->{'specific_family_type_ipr'}}, $ipr);
            }

            # PIRSF
            if ($ipr_sources->{$ipr}->{'ipr'}->pirsf_code)
            {
                push(@{$ipr_sources->{'pirsf'}}, $ipr);
            }
        }
    }
    
    # Sort lists
    $ipr_sources->{'specific_family_type_ipr'} = [
        sort
            {
                # sort by coverage and then by code
                return scalar(@{$ipr_sources->{$b}->{'sequences'}}) <=> scalar(@{$ipr_sources->{$a}->{'sequences'}})
                    || ($a->code cmp $b->code);
            }
            @{$ipr_sources->{'specific_family_type_ipr'}}
    ];
    $ipr_sources->{'ipr'} = [
        sort
            {
                # sort by coverage and then by code
                return scalar(@{$ipr_sources->{$b}->{'sequences'}}) <=> scalar(@{$ipr_sources->{$a}->{'sequences'}})
                    || ($a->code cmp $b->code);
            }
            @{$ipr_sources->{'ipr'}}
    ];
    $ipr_sources->{'pirsf'} = [
        sort
            {
                # sort by coverage and then by code
                return scalar(@{$ipr_sources->{$b}->{'sequences'}}) <=> scalar(@{$ipr_sources->{$a}->{'sequences'}})
                    || ($a->code cmp $b->code);
            }
            @{$ipr_sources->{'pirsf'}}
    ];

    return $ipr_sources;
}


=pod

=head2 [RenderFunctionToCall] #+++

B<Description>: [function description]. #+++

B<ArgsCount>: 0

B<Return>: (string)

Returns an HTML string containing the GreenPhyl page for ... #+++

=cut

sub OverloadDBXRefObjects
{
    my ($dbxref_sources, $family) = @_;

    # fetch all DBXRef from database at once (into cache)
    my $dbxref_selectors = {};
    if (%$dbxref_sources)
    {
        $dbxref_selectors = { 'id' => ['IN', keys(%$dbxref_sources)], };
    }
    my $dbxref_list = Greenphyl::DBXRef->new(
        $g_dbh,
        {'selectors' =>  $dbxref_selectors},
    );

    my $db_sources = {};
    foreach my $dbxref_id (keys(%$dbxref_sources))
    {
        # get corresponding DBXRef object (from cache)
        my $dbxref = Greenphyl::DBXRef->new(
                $g_dbh,
                {'selectors' => { 'id' => $dbxref_id, } },
            );
        # if DBXRef loaded ok
        if ($dbxref)
        {
            $db_sources->{$dbxref->db->name} ||= {};
            my $source_hash = $db_sources->{$dbxref->db->name};
            # use DBXRef accession as hash key
            $source_hash->{$dbxref} = $dbxref_sources->{$dbxref_id};
            # add DBXRef object
            $source_hash->{$dbxref}->{'dbxref'} = $dbxref;
            # add percent
            my $coverage_percent = int(100 * scalar(@{$source_hash->{$dbxref}->{'sequences'}}) / $family->sequence_count);
            $source_hash->{$dbxref}->{'percent'} = (0 < $coverage_percent ? "$coverage_percent%" : '<1%');
        }
    }
    
    # generate lists
    foreach my $db_name (keys(%$db_sources))
    {
        my @dbxref_list = map { $_->{'dbxref'} } values(%{$db_sources->{$db_name}});
        $db_sources->{$db_name}->{'dbxref'} = [
            sort
            {
                # sort by coverage and then by accession
                return scalar(@{$db_sources->{$db_name}->{$b}->{'sequences'}}) <=> scalar(@{$db_sources->{$db_name}->{$a}->{'sequences'}})
                    || ($a->accession cmp $b->accession);
            }
            @dbxref_list
        ];
    }

    return $db_sources;
}


=pod

=head2 [RenderFunctionToCall] #+++

B<Description>: [function description]. #+++

B<ArgsCount>: 0

B<Return>: (string)

Returns an HTML string containing the GreenPhyl page for ... #+++

=cut

sub OverloadDBXRefSpecificity
{
    my ($dbxref_sources, $family) = @_;

    # -compute KEGG specificity
    foreach my $dbxref (@{$dbxref_sources->{'dbxref'}})
    {
        my $family_count = scalar(@{$dbxref_sources->{$dbxref}->{'dbxref'}->families});
        if (!$family_count)
        {
            $dbxref_sources->{$dbxref}->{'specificity'} = 'N/A';
        }
        elsif (1 == $family_count)
        {
            $dbxref_sources->{$dbxref}->{'specificity'} = 'Specific';
        }
        else
        {
            $dbxref_sources->{$dbxref}->{'specificity'} = 'No (' . $family_count . ' families)';
        }
    }
    
    return $dbxref_sources;
}


=pod

=head2 [RenderFunctionToCall] #+++

B<Description>: [function description]. #+++

B<ArgsCount>: 0

B<Return>: (string)

Returns an HTML string containing the GreenPhyl page for ... #+++

=cut

sub GetGeneAnnotations
{
    my ($family) = @_;

    my $gene_annotations = {};

    foreach my $sequence ($family->sequences({'selectors' => {'annotation' => ['IS NOT NULL'],},}))
    {
        if ($sequence->annotation)
        {
            $gene_annotations->{$sequence->annotation} ||= 0;
            $gene_annotations->{$sequence->annotation}++;
        }
    }

    my $gene_annotations_stats = [
        sort
            {
                return $b->{'occurence'} <=> $a->{'occurence'}
                    || $a->{'annotation'} cmp $b->{'annotation'};
            }
            (map
                {
                    my $percent = int(100 * $gene_annotations->{$_} / $family->sequence_count);
                    {
                        'annotation' => $_,
                        'occurence'  => $gene_annotations->{$_},
                        'percent'    => (0 < $percent ? "$percent%" : '<1%'),
                    }
                }
                keys(%$gene_annotations)
            )
    ];
    return $gene_annotations_stats;
}


=pod

=head2 [RenderFunctionToCall] #+++

B<Description>: [function description]. #+++

B<ArgsCount>: 0

B<Return>: (string)

Returns an HTML string containing the GreenPhyl page for ... #+++

=cut

sub RenderFamilyAnnotation
{
    # get selected family
    my $family_id = GetParameterValues('family_id');
    if (!$family_id || ($family_id !~ m/^\d+$/))
    {
        Throw('error' => 'No family identifier provided!');
    }

    my $family = Greenphyl::Family->new(
        $g_dbh,
        {'selectors' => { 'id' => $family_id, } }
    );

    # my $go_data                  = GenerateGOData($family);
    # my $sorted_go                = [sort {$a->code cmp $b->code} @$go_data];
    # my $go_groups                = Greenphyl::GO::GroupGOByType($sorted_go);
    my $family_composition_data  = GenerateFamilyCompositionData($family);


    my $cluster_types = [ GetTableColumnValues('family_annotations', 'type') ];
    my $inferences    = [ GetTableColumnValues('family_annotations', 'inferences') ];
    my $validations   = [ GetTableColumnValues('family_annotations', 'confidence_level') ];

    # get sources...
    # -get source database objects (load cache)
    my $source_dbs = Greenphyl::SourceDatabase->new($g_dbh);

    # -prepare hashes that will contain annotation sources
    #  each hash contains hashes with 2 keys: 'ipr'/'dbxref' for the source
    #  object and 'sequences' which is an array containing all the associated
    #  sequences.
    #  Hash are first initialized using source object identifiers as keys but
    #  then, identifiers are replaced by corresponding codes or accessions.

    # -get sources
    my ($ipr_sources, $all_dbxref_sources) = GetFamilySources($family);
    $ipr_sources = OverloadIPRObjects($ipr_sources, $family);

    my $db_sources = OverloadDBXRefObjects($all_dbxref_sources, $family);
    $db_sources->{'Kegg'} = OverloadDBXRefSpecificity($db_sources->{'Kegg'}, $family);

    my $gene_annotations = GetGeneAnnotations($family);
    # get user annotation (check if a specific annotation has been specified)
    my $user_annotation = [];
    if (IsAdmin() && (my $annotation_id = GetParameterValues('annotation_id')))
    {
        $user_annotation = [Greenphyl::FamilyAnnotation->new($g_dbh, {'selectors' => {'id' => $annotation_id,}})];
    }
    elsif (IsAdmin() && (my $user_id = GetParameterValues('user_id')))
    {
        $user_annotation = $family->annotations({'selectors' => {'user_id' => $user_id, 'accession' => $family->accession, }});
    }
    elsif (my $user = GetCurrentUser())
    {
        $user_annotation = $family->annotations({'selectors' => {'user_id' => $user->id, 'accession' => $family->accession, }});
    }

    # check if an annotation has already been created
    if (@$user_annotation)
    {
        # yes, use it as basis
        PrintDebug('Using annotation found in database');
        $user_annotation = $user_annotation->[0];
    }
    else
    {
        PrintDebug('Creating a blank annotation');
        # no, create a blank one
        my $INT_TYPE_TO_TEXT =
            {
                '0' => 'N/A',
                '1' => 'superfamily',
                '2' => 'family',
                '3' => 'subfamily',
                '4' => 'group',
            };
        my $synonyms = join("\n", @{$family->synonyms?$family->synonyms:[]});
        my $inferences = $family->inferences;
        my $cross_references = join(
            ';',
            (map {$_->accession} @{$family->getPubMed}),
            # (map {$_->code} @{$family->go}),
        );
        
        # create a family annotation object corresponding to current family and
        # current user if one
        my $user = GetCurrentUser();
        $user_annotation = Greenphyl::FamilyAnnotation->new(
            undef,
            {
                'load' => 'none',
                'members' =>
                    {
                        'id'               => 0,
                        'user_id'          => $user ? $user->id : 0,
                        'user_name'        => $user ? 'your' : 0,
                        'accession'        => $family->accession,
                        'name'             => $family->name,
                        'type'             => $INT_TYPE_TO_TEXT->{$family->type || 0},
                        'synonyms'         => $synonyms,
                        'status'           => 'in progress',
                        'date'             => 0,
                        'confidence_level' => 'N/A',
                        'inferences'       => $inferences,
                        'dbxref'           => $cross_references,
                        'description'      => $family->description,
                        'comments'         => '',
                    },
            }
        );
    }

    return RenderHTMLFullPage(
        {
            'title'      => 'Family Annotation',
            'content'    => 'family_tools/annotation_page.tt',
            'javascript' => ['raphael', 'raphael-zpd', 'g.raphael', 'g.bar', 'chart', 'ipr', 'relationship_graph', 'families', ],
            # access restriction
            'access_message' => 'You need annotator priviledges to access to this page!',
            'require_access' => [GP('ACCESS_KEY_ADMINISTRATOR'), GP('ACCESS_KEY_ANNOTATOR')],
            'access_needs'   => 'any',
            # page data
            'family'              => $family,
            'user_annotation'     => $user_annotation,
            # family composition
            'family_composition'  => $family_composition_data,
            # annotation static data
            'cluster_types'       => $cluster_types,
            'inferences'          => $inferences,
            'validations'         => $validations,
            'ipr_sources'         => $ipr_sources,
            'db_sources'          => $db_sources,
            'gene_annotations'    => $gene_annotations,
            # tabs definition
            'contents' =>
            [
                {
                    'id'      => 'tab-primary-sources',
                    'title'   => 'Primary Sources',
                    'content' => 'family_tools/primary_sources.tt',
                },
                {
                    'id'      => 'tab-secondary-sources',
                    'title'   => 'Secondary Sources',
                    'content' => 'family_tools/secondary_sources.tt',
                },
            ],
        },
    );
}


=pod

=head2 [RenderFunctionToCall] #+++

B<Description>: [function description]. #+++

B<ArgsCount>: 0

B<Return>: (string)

Returns an HTML string containing the GreenPhyl page for ... #+++

=cut

sub InsertMissingPMID
{
    my ($pmid_string) = @_;
    
    # load PubMed DB object
    my $pubmed_db = Greenphyl::SourceDatabase->new($g_dbh, {'selectors' => {'name' => 'PubMed'}})
        or Throw('error' => 'PubMed source database reference not found in database!');

    # get publication name using pubmed id proposed by annotator
    my $biblio = Bio::Biblio->new(-access => 'biofetch')
        or Throw('error' => 'Unable to instanciate Bio::Biblio object!');
    
    #+FIXME: refactoring: add this function to Greenphyl::Tools::DBXRefs
    foreach my $pmid (split(/[\s,;]+/i, $pmid_string))
    {
        # make sure we got a valid PMID
        $pmid =~ s/PMID://;
        if ($pmid)
        {
            if ($pmid =~ m/^(\d{1,10})$/)
            {
                # check if PMID is already in database
                my $dbxref = Greenphyl::DBXRef->new($g_dbh, {'selectors' => {'accession' => $pmid, 'db_id' => $pubmed_db->id,}});
                if (!$dbxref)
                {
                    PrintDebug("Adding DBXRef for PMID $pmid");
                    # not in database, add it!
                    my $pubmed_xml = $biblio->get_by_id($pmid);
                    my $sql_query = "INSERT INTO dbxref(db_id, accession, description) VALUE (?, ?, ?);";
                    $g_dbh->do($sql_query, undef, $pubmed_db->id, $pmid, $pubmed_xml->title)
                        or confess $g_dbh->errstr;
                }
            }
            elsif ($pmid)
            {
                PrintDebug("WARNING: Invalid PMID found: '$pmid'");
            }
        }
    }

}


=pod

=head2 [RenderFunctionToCall] #+++

B<Description>: [function description]. #+++

B<ArgsCount>: 0

B<Return>: (string)

Returns an HTML string containing the GreenPhyl page for ... #+++

=cut

sub RenderUpdateFamilyAnnotation
{
    InitAccess();

    my $requirements = {
        'require_access' => [GP('ACCESS_KEY_ADMINISTRATOR'), GP('ACCESS_KEY_ANNOTATOR'),],
        'access_needs'   => 'any',
    };

    my $user = GetCurrentUser();
    if (!$user || !CheckAccess($requirements, $user))
    {
        # access denied
        ThrowAccessDenied('error' => 'You are not allowed to save family annotations!');
    }

    # get selected family
    my $family_id = GetParameterValues('family_id');
    if (!$family_id || ($family_id !~ m/^\d+$/))
    {
        Throw('error' => 'No family identifier provided!');
    }

    my $family = Greenphyl::Family->new(
        $g_dbh,
        {'selectors' => { 'id' => $family_id, } }
    );

    # get annotation
    my $family_type = GetParameterValues('family_type');
    my $family_name = GetParameterValues('family_name');
    my $family_synonyms = GetParameterValues('family_synonyms');
    my $family_inferences = GetParameterValues('family_inferences');
    my $family_description = GetParameterValues('family_description');
    my $family_dbxref = GetParameterValues('family_dbxref');
    my $family_validated = GetParameterValues('family_validated');
    my $annotation_status = GetParameterValues('annotation_status');
    my $comments = GetParameterValues('comments') || '';
    
    if (!$annotation_status || ($annotation_status !~ m/^(?:in progress|submitted)$/))
    {
        $annotation_status = 'in progress';
    }

    my @values = ($user->id, $family->accession, $family_name, $family_type,
        $family_synonyms, $family_dbxref, $family_inferences, $family_validated,
        $annotation_status, $family_description, $comments);

    # update family annotation
    my $sql_query = "INSERT INTO family_annotations
            (user_id,
             accession,
             name,
             type,
             synonyms,
             dbxref,
             inferences,
             confidence_level,
             status,
             description,
             comments)
        VALUES
            (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)
        ON DUPLICATE KEY UPDATE
            user_id = ?,
            accession = ?,
            name = ?,
            type = ?,
            synonyms = ?,
            dbxref = ?,
            inferences = ?,
            confidence_level = ?,
            status = ?,
            description = ?,
            comments = ?
        ;";
    PrintDebug("SQL QUERY: $sql_query\nBinding: " . join(', ', @values) . ', ' . join(', ', @values));
    $g_dbh->do($sql_query, undef, @values, @values)
        or confess $g_dbh->errstr;

    # add missing PMID
    if ($family_dbxref)
    {
        InsertMissingPMID($family_dbxref);
    }

    return RenderFamilyAnnotation();
}




# Script options
#################

=pod

=head1 OPTIONS

#--- describes GET/POST parameters given to the CGI script

=head2 Parameters

=over 4

=item B<[option_name]> ([option nature]): #+++

[option description]. #+++
Default: [option default value if one] #+++

=back
#--- Example:
#---
#--- No parameters requiered or parameters given using POST method.
#---
#--- =over 4
#---
#--- =item B<login> (string):
#---
#--- The user login.
#---
#--- =item B<tracker> (integer):
#---
#--- A tracker used to retrive the data element to display.
#---
#---=back

=cut


# CODE START
#############

# hash for function selection.
my $actions =
    {
        ''       => \&RenderFamilyAnnotation,
        'update' => \&RenderUpdateFamilyAnnotation,
    };

# try block
eval
{
    # display page
    print ProceedPage($actions);
};

HandleErrors();

exit(0);

# CODE END
###########


=pod

=head1 DIAGNOSTICS

=head1 AUTHORS

Mathieu ROUARD (Bioversity), m.rouard@cgiar.org

Valentin GUIGNON (Bioversity), v.guignon@cgiar.org

Matthieu CONTE (Syngenta)

Sebastien BRIOIS (Syngenta)

=head1 VERSION

Version 1.0.0

Date 03/10/2012

=head1 SEE ALSO

GreenPhyl documentation.

=cut
