#!/usr/bin/perl

=pod

=head1 NAME

statistics.cgi - Display database statistics

=head1 SYNOPSIS

    statistics.cgi

=head1 REQUIRES

Perl5

=head1 DESCRIPTION

Display GreenPhyl database statistics on families and species.

=cut

use strict;
use warnings;

use lib "../lib";
use lib '../local_lib';
use Carp qw (cluck confess croak);

use Benchmark qw( :hireswallclock timeit timestr );

use Greenphyl;
use Greenphyl::Web;
use Greenphyl::Web::Templates; # ProcessPage, ProcessTemplate
use Greenphyl::Species;
use Greenphyl::AbstractFamily;




# Script global constants
##########################

=pod

=head1 CONSTANTS

B<$DEBUG>: (boolean)

When set to true, it enables debug mode.

B<$STATISTICS_CACHE_TEMPLATE>: (string)

Relative (to templates path) path to statistics static template file for cache.

=cut

our $DEBUG = 0;
our $STATISTICS_CACHE_TEMPLATE = 'cache/statistics.tt';




# Script global functions
##########################

=pod

=head1 FUNCTIONS

=head2 RenderStatistics

B<Description>: renders statistics page.

B<ArgsCount>: 0

B<Return>: (string)

Returns an HTML string containing the GreenPhyl statistics page.

=cut

sub RenderStatistics
{
    my $stats_template_file = GP('TEMPLATES_PATH') . '/' . $STATISTICS_CACHE_TEMPLATE;

    # check if statistics have to be generated
    if ((!-e $stats_template_file)
        || GetParameterValues('clearcache')
        || GetParameterValues('nocache'))
    {
        PrintDebug("Regenerating statistics...");
        my $level1_clusters_count = 0;
        my $annotated_clusters_count = 0;
        my $phylogeny_clusters_count = 0;
        my @species_list = ();
        my $statistics_content = '';

        my $computation_duration = timestr(
            timeit(
                1,
                sub
                {
                    my $dbh = GetDatabaseHandler();
                    my $sql_query = '
                        SELECT count(f.id)
                        FROM families f
                        WHERE
                            f.sequence_count >= 5
                            AND f.level = 1
                    ';

                    PrintDebug("SQL QUERY: $sql_query");
                    ($level1_clusters_count) = $dbh->selectrow_array($sql_query);

                    # f.validated IN ('high','normal','unknown') AND 
                    $sql_query = "
                        SELECT count(f.id)
                        FROM families f
                        WHERE
                            f.level IN (1, 2, 3, 4)
                            AND f.name != ''
                            AND f.name NOT LIKE '$Greenphyl::AbstractFamily::UNANNOTATED_CLUSTER_NAME'
                    ";
                    PrintDebug("SQL QUERY: $sql_query");
                    ($annotated_clusters_count) = $dbh->selectrow_array($sql_query);

                    $sql_query = "
                        SELECT count(DISTINCT h.family_id)
                        FROM homologies h
                    ";
                    PrintDebug("SQL QUERY: $sql_query");
                    ($phylogeny_clusters_count) = $dbh->selectrow_array($sql_query);

                    @species_list = sort {return $a->name cmp $b->name;} (Greenphyl::Species->new($dbh, {'selectors' => { 'display_order' => ['>', 0], },}));

                    my $sequences_with_ipr_by_species = {};
                    foreach my $species (@species_list)
                    {
                        $sql_query = '
                            SELECT count(DISTINCT s.id)
                            FROM sequences s
                                JOIN ipr_sequences i ON i.sequence_id = s.id
                            WHERE s.species_id = ?;';
                        PrintDebug("SQL QUERY: $sql_query\nBindings: " . $species->id);
                        ($sequences_with_ipr_by_species->{$species}) = $dbh->selectrow_array($sql_query, undef, $species->id);
                    }
                    
                    
                    # generate statistics
                    $statistics_content = ProcessPage(
                        {
                            'content' => 'miscelaneous/statistics.tt',
                            'level1_clusters_count'    => $level1_clusters_count,
                            'annotated_clusters_count' => $annotated_clusters_count,
                            'phylogeny_clusters_count' => $phylogeny_clusters_count,
                            'species_list'             => \@species_list,
                            'sequences_with_ipr_by_species' => $sequences_with_ipr_by_species,
                        },
                    );
                }
            )
        );

        # add computation time
        $statistics_content .= ProcessTemplate(
            'duration.tt',
            {
                'duration_label' => 'Statistics Collection Time',
                'duration' => $computation_duration,
            }
        );

        # if nocache and cache file exists, just return what has just been computed
        if (GetParameterValues('nocache') && (-e $stats_template_file))
        {
            return RenderHTMLFullPage(
                {
                    'title'   => 'Statistics',
                    'content' => $statistics_content,
                    # 'after_content' => 'duration.tt',
                    # 'duration_label' => 'Statistics Collection Time',
                    # 'duration' => $computation_duration,
                },
            );
        }

        PrintDebug("Storing statistics cache ($stats_template_file)");
        # Store static statistics page (cache)
        # To update statistics, just delete cache file ($STATISTICS_CACHE_TEMPLATE)
        my $stat_fh;
        if (open($stat_fh, ">$stats_template_file" ))
        {
            # chmod(0666, $stats_template_file) or confess $!;
            print {$stat_fh} $statistics_content;
            close($stat_fh);
        }
        else
        {
            Throw('error' => 'Unable to generate statistics! An unexpected error occurred!', 'log' => "Error generating statistics cache file: $!");
        }
    }

    return RenderHTMLFullPage(
        {
            'title'   => 'Statistics',
            'content' => $STATISTICS_CACHE_TEMPLATE,
        },
    );
}




# Script options
#################

=pod

=head1 OPTIONS

    statistics.cgi? <nocache=1> & <clearcache=1>

=head2 Parameters

=over 4

=item B<nocache> (boolean):

Do not use cache data. Recompute statistics and update cache only if missing.
Default: false

=item B<clearcache> (boolean):

Recompute statistics and replace cache data.
Default: false

=back

=cut


# CODE START
#############

# hash for function selection.
my $actions =
    {
        '' => \&RenderStatistics,
    };

# try block
eval
{
    # display page
    print ProceedPage($actions);
};

HandleErrors();

exit(0);

# CODE END
###########


=pod

=head1 AUTHORS

Mathieu ROUARD (Bioversity), m.rouard@cgiar.org

Valentin GUIGNON (Bioversity), v.guignon@cgiar.org

Matthieu CONTE (Syngenta)

Sebastien BRIOIS (Syngenta)

=head1 VERSION

Version 1.0.0

Date 20/02/2013

=head1 SEE ALSO

GreenPhyl documentation.

=cut
