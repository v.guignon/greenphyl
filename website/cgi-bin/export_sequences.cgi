#!/usr/bin/perl

=pod

=head1 NAME

export_sequences.cgi - Export sequences

=head1 SYNOPSIS

    export_sequences.cgi

=head1 REQUIRES

Perl5

=head1 DESCRIPTION

Export the list of user-provided sequences into the selected format.

=cut

use strict;
use warnings;

use lib '../lib';
use lib '../local_lib';
use Carp qw (cluck confess croak);

use Greenphyl;
use Greenphyl::Web;
use Greenphyl::Dumper;
use Greenphyl::Sequence;




# Script global functions
##########################

=pod

=head1 FUNCTIONS

=head2 RenderExportSequencesForm

B<Description>: Renders the export Form.

B<ArgsCount>: 0

B<Return>: (string)

Returns an HTML string containing the GreenPhyl form for sequence export.

=cut

sub RenderExportSequencesForm
{
    return RenderHTMLFullPage(
        {
            'title'               => 'Export sequences',
            'content'             => 'tools/export_sequences_form.tt',
            'form_data'           =>
                {
                    'action'     => 'dump.pl',
                    'identifier' => 'screen_fasta',
                    'submit'     => 'View FASTA',
                },
            'dump_data' =>
            {
                'links' => [
                    {
                        'label'       => 'Fasta',
                        'format'      => 'fasta',
                        'file_name'   => 'sequences.fasta',
                        'namespace'   => 'fasta',
                    },
                    {
                        # parameters used by the form button "View"
                        'format'      => 'fasta',
                        'namespace'   => 'screen_fasta',
                    },
                ],
                'object_type' => 'sequences',
                'fields'      => 'seq_textids',
                'mapping'     => {'seq_textids'=>'accession'},
                'separator'   => 'eol',
                'mode'        => 'embeded',
                'header'      => 'Export to:',
            },
        },
    );
}




# Script options
#################

=pod

=head1 OPTIONS

    export_sequences.cgi

=head2 Parameters

no parameters.

=cut


# CODE START
#############

# hash for function selection.
my $actions =
    {
        ''             => \&RenderExportSequencesForm,
        'form'         => \&RenderExportSequencesForm,
    };

   
# try block
eval
{
    # display page
    print ProceedPage($actions);
};

HandleErrors();

exit(0);

# CODE END
###########


=pod

=head1 AUTHORS

Mathieu ROUARD (Bioversity), m.rouard@cgiar.org

Valentin GUIGNON (Bioversity), v.guignon@cgiar.org

Matthieu CONTE (Syngenta)

=head1 VERSION

Version 1.0.0

Date 25/05/2012

=head1 SEE ALSO

GreenPhyl documentation.

=cut
