#!/usr/bin/perl

=pod

=head1 NAME

index.cgi - GreenPhyl home page

=head1 SYNOPSIS

    index.cgi

=head1 REQUIRES

Perl5

=head1 DESCRIPTION

Display GreenPhyl home page with species tree and news.

=cut

use strict;
use warnings;

use lib '../lib';
use lib '../local_lib';
use Carp qw (cluck confess croak);

use Greenphyl; # includes Config and Exceptions
use Greenphyl::Web; # includes CGI:standard and Templates
use Greenphyl::Tools::News;



# Script global functions
##########################

=pod

=head1 FUNCTIONS

=head2 RenderHomePage

B<Description>: Renders GreenPhyl home page.

B<ArgsCount>: 0

B<Return>: (string)

Returns an HTML string containing the GreenPhyl home page.

=cut

sub RenderHomePage
{
    # get pre-computed tree from database
    my $species_html_tree = '';
    eval
    {
        my $sql_query = "
                SELECT value
                FROM variables
                WHERE name = 'taxonomy_html_tree';";
        ($species_html_tree) = GetDatabaseHandler()->selectrow_array($sql_query);
    };

    if (my $error = Exception::Class->caught())
    {
        $species_html_tree = "<p>Unable to retrieve taxonomy tree from database!</p>\n";
        PrintDebug($error);
        cluck($error);
    }
    
    # News
    my $news_parameters = {
        'offset' => 0,
        'limit'  => 3,
    };
    my $news_list = ListNews($news_parameters);

    return RenderHTMLFullPage(
        {
            'title'             => '',
            'content'           => 'miscelaneous/homepage.tt',
            'css'               => ['tree'],
            'species_html_tree' => $species_html_tree,
            'news_list'         => $news_list,
        },
    );
}




# Script options
#################

=pod

=head1 OPTIONS

index.cgi

=head2 Parameters

none

=cut


# CODE START
#############

# hash for function selection.
my $actions =
    {
        '' => \&RenderHomePage,
    };

# try block
eval
{
    # display page
    print ProceedPage($actions);
};

HandleErrors();

exit(0);

# CODE END
###########


=pod

=head1 AUTHORS

Mathieu ROUARD (Bioversity), m.rouard@cgiar.org

Valentin GUIGNON (Bioversity), v.guignon@cgiar.org

Matthieu CONTE (Syngenta)

=head1 VERSION

Version 1.0.0

Date 27/06/2012

=head1 SEE ALSO

GreenPhyl documentation.

=cut
