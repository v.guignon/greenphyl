#!/usr/bin/perl

=pod

=head1 NAME

get_homologs.cgi - Returns a set of homolog sequences

=head1 SYNOPSIS

    http://www.greenphyl.fr/cgi-bin/get_homologs.cgi

=head1 REQUIRES

Perl5

=head1 DESCRIPTION

Given a set of sequence (accession) and a target species, returns homolog
sequences for that species.

=cut

use strict;
use warnings;

use lib '../lib';
use lib '../local_lib';
use Carp qw (cluck confess croak);

use CGI qw(:standard);

use Greenphyl;
use Greenphyl::Web;
use Greenphyl::Sequence;
use Greenphyl::CachedSequence;
use Greenphyl::Tools::Sequences;
use Greenphyl::Species;
use Greenphyl::CompositeObject;
use Greenphyl::Dumper;




# Script global variables
##########################

=pod

=head1 VARIABLES

B<$g_dbh>: (DBI::db)

Current database handler.

=cut

my $g_dbh = GetDatabaseHandler();




# Script global functions
##########################

=pod

=head1 FUNCTIONS

=head2 GenerateSpeciesOptions

B<Description>: Generate the option structure for species selection.

B<ArgsCount>: 0

B<Return>: (array ref)

Returns an array of hash of options or option groups that will be processed
by the get_homologs_form.tt template.

=cut

sub GenerateSpeciesOptions
{
    # get available species
    my $sql_query = 'SELECT taxonomy_id AS "value", organism AS "label" FROM species WHERE display_order > 0 ORDER BY organism asc;';
    my $db_species = $g_dbh->selectall_arrayref($sql_query, { 'Slice' => {} });

    # get taxonomy info for 'All', 'Dicotyledons' and 'Monocotyledons'
    my $eukaryota      = new Greenphyl::Taxonomy($g_dbh, {'selectors' => {'scientific_name' => 'Eukaryota'}});
    my $eudicotyledons = new Greenphyl::Taxonomy($g_dbh, {'selectors' => {'scientific_name' => 'eudicotyledons'}});
    my $liliopsida     = new Greenphyl::Taxonomy($g_dbh, {'selectors' => {'scientific_name' => 'Liliopsida'}});

    # make sure we got required phylonodes
    if (!$eukaryota)
    {
        Throw('error' => 'This tool is currently not available due to missing data in database. Please report this error to the site administrator.', 'log' => '"Eukaryota" phylonode not found in database! Maybe you should run admin/cluster_scripts/update_family_taxonomy.pl');
    }
    if (!$eudicotyledons)
    {
        Throw('error' => 'This tool is currently not available due to missing data in database. Please report this error to the site administrator.', 'log' => '"eudicotyledons" phylonode not found in database! Maybe you should run admin/cluster_scripts/update_family_taxonomy.pl');
    }
    if (!$liliopsida)
    {
        Throw('error' => 'This tool is currently not available due to missing data in database. Please report this error to the site administrator.', 'log' => '"Liliopsida" phylonode not found in database! Maybe you should run admin/cluster_scripts/update_family_taxonomy.pl');
    }

    # database options dropdown
    my $species_options = [
        {
            'label' => 'All',
            'value' => $eukaryota->id,
        },
        {
            'label'   => 'Phylum',
            'options' => [
                {
                    'label' => 'Dicotyledons',
                    'value' => $eudicotyledons->id,
                },
                {
                    'label' => 'Monocotyledons',
                    'value' => $liliopsida->id,
                },
            ],
        },
        {
            'label'   => 'species',
            'options' => $db_species,
        }
    ];

    return $species_options;
}


=head2 RenderForm

B<Description>: Render homology form into an HTML string.

B<ArgsCount>: 0

B<Return>: (string)

Returns an HTML string containing the full GreenPhyl page with the form.

=cut

sub RenderForm
{
    # database options dropdown
    my $species_options = GenerateSpeciesOptions();

    return RenderHTMLFullPage(
        {
            'title'             => 'Get homologs and/or similar sequences',
            'content'           => 'tools/get_homologs_form.tt',
            'form_data'         =>
                {
                    'identifier' => 'homologs',
                    # 'action'     => url() . '?p=results&class=cached_sequence',
                    'action'     => url() . '?p=results',
                    'submit'     => 'Search',
                },
            'dump_data'           =>
                {
                   'links' => [
                     {
                       'label'       => 'FASTA',
                       'format'      => 'fasta',
                       'file_name'   => 'homologs.fasta',
                       'namespace'   => 'fasta',
                     },
                     {
                       'label'       => 'Excel',
                       'format'      => 'excel',
                       'file_name'   => 'homologs.xls',
                       'namespace'   => 'excel',
                     },
                     {
                       'label'       => 'CSV',
                       'format'      => 'csv',
                       'file_name'   => 'homologs.csv',
                       'namespace'   => 'csv',
                     },
                     {
                       'label'       => 'XML',
                       'format'      => 'xml',
                       'file_name'   => 'homologs.xml',
                       'namespace'   => 'xml',
                     },
                   ],
                   'object_type' => 'sequences',
                   'fields'      => 'seq_textids',
                   'mapping'     => {'seq_textids' => 'accession'},
                   'separator'   => 'eol',
                   'mode'        => 'embeded',
                },
            'species_options' => $species_options,
        }
    );
}


=pod

=head2 GetOutputColumnsCode

B<Description>: Returns the selection code of the main table columns to output.

B<ArgsCount>: 0

B<Return>: (string)

The output columns code.

=cut

sub GetOutputColumnsCode
{
    # get output columns
    my $output_columns = GetParameterValues('output_selection');
    if ($output_columns !~ m/all|homologs|bbmh/i)
    {
        $output_columns = 'all';
    }
    return $output_columns;
}


=pod

=head2 GetSpeciesFilter

B<Description>: Returns the selected species filter.

B<ArgsCount>: 0

B<Return>: (hash ref)

Hash which keys are species ID and values are species objects.

=cut

sub GetSpeciesFilter
{
    my $species_array = [];
    my $species_filter = {};
    my @species_selection = GetParameterValues('target_species');
    foreach my $species_selection (@species_selection)
    {
        if ($species_selection !~ m/^\d+$/)
        {
            Throw('error' => 'Invalid species selection!', 'log' => "Given species: $species_selection");
        }

        $species_array = [Greenphyl::Species::GetSpeciesByTaxonomy($g_dbh, {'taxonomy_id' => $species_selection})];
        foreach my $species (@$species_array)
        {
            $species_filter->{$species->id} = $species;
        }
    }

    return $species_filter;
}


=pod

=head2 ComputeHomologs

B<Description>: Compute and return homology results.

B<ArgsCount>: 2

=over 4

=item $output_columns: (string) (R)

String containing the kind of homologies/similarities to find. Allowed values
are 'homologs', 'bbmh' and 'all'.

=item $species_filter: (hash) (R)

The hash containing only requested species. Keys are species ID.

=back

B<Return>: (array ref)

An array containing homologies for each query ('seq_textids' field) sequence.
An homology record consists of a hash with 2 keys: 'sequence' and 'homologs'.
The 'sequence' value is the query sequence and the 'homologs' values contains
an array of hit sequence objects.

=cut

sub ComputeHomologs
{
    my ($output_columns, $species_filter) = @_;

    # get sequences
    my @sequence_textids;
    foreach my $seq_textid (split(/\s*[,;\n\r\s\t]+\s*/, GetParameterValues('seq_textids')))
    {
        # only keep valid sequence names
        if (ValidateSequenceName($seq_textid))
        {
            push(@sequence_textids, $seq_textid);
        }
    }

    @sequence_textids = @{GetAccessionsFromSynonyms(\@sequence_textids, \@sequence_textids)};

    my $class = GetParameterValues('class') || '';
    my @query_sequences;
    if ($class =~ m/cached_sequence/i)
    {
        @query_sequences = Greenphyl::CachedSequence->new(
            $g_dbh,
            {
                'selectors' =>
                {
                    'accession' => ['REGEXP', join('|', @sequence_textids)],
                },
            },
        );
    }
    else
    {
        @query_sequences = Greenphyl::Sequence->new(
            $g_dbh,
            {
                'selectors' =>
                {
                    'accession' => ['REGEXP', join('|', @sequence_textids)],
                },
            },
        );
    }

    if (!@query_sequences || !$query_sequences[0])
    {
        Throw('error' => 'No corresponding sequence found in database!<br/>Go <a href="' . url() . '">back</a>');
    }

    # sorting sequence result functions
    my $sort_by_homology_and_bbmh = sub
        {
            my ($query_sequence) = @_;
            if ($query_sequence->getHomologyWith($a) && $query_sequence->getHomologyWith($b))
            {
                return $query_sequence->getHomologyWith($a)->distance cmp $query_sequence->getHomologyWith($b)->distance;
            }
            elsif ($query_sequence->getHomologyWith($a))
            {
                return -1;
            }
            elsif ($query_sequence->getHomologyWith($b))
            {
                return 1;
            }
            elsif ($query_sequence->getBBMHWith($a) && $query_sequence->getBBMHWith($b))
            {
                return $query_sequence->getBBMHWith($a)->score cmp $query_sequence->getBBMHWith($b)->score;
            }
            elsif ($query_sequence->getBBMHWith($a))
            {
                return -1;
            }
            elsif ($query_sequence->getBBMHWith($b))
            {
                return 1;
            }
            else
            {
                return $a->species->name cmp $b->species->name;
            }
        }
    ;
    my $sort_by_homology = sub
        {
            my ($query_sequence) = @_;
            if ($query_sequence->getHomologyWith($a) && $query_sequence->getHomologyWith($b))
            {
                return $query_sequence->getHomologyWith($a)->distance cmp $query_sequence->getHomologyWith($b)->distance;
            }
            elsif ($query_sequence->getHomologyWith($a))
            {
                return -1;
            }
            elsif ($query_sequence->getHomologyWith($b))
            {
                return 1;
            }
            else
            {
                return $a->species->name cmp $b->species->name;
            }
        }
    ;
    my $sort_by_bbmh = sub
        {
            my ($query_sequence) = @_;
            if ($query_sequence->getBBMHWith($a) && $query_sequence->getBBMHWith($b))
            {
                return $query_sequence->getBBMHWith($a)->score cmp $query_sequence->getBBMHWith($b)->score;
            }
            elsif ($query_sequence->getBBMHWith($a))
            {
                return -1;
            }
            elsif ($query_sequence->getBBMHWith($b))
            {
                return 1;
            }
            else
            {
                return $a->species->name cmp $b->species->name;
            }
        }
    ;

    # process each sequence
    my $homology_data = [];
    foreach my $query_sequence (@query_sequences)
    {
        my %hit_sequences;
        my $sort_function;

        # get homologs
        if ($output_columns =~ m/all|homologs/i)
        {
            foreach (@{$query_sequence->homologs})
            {
                $hit_sequences{$_} ||= $_;
            }
            $sort_function = $sort_by_homology;
        }

        # get BBMH
        if ($output_columns =~ m/all|bbmh/i)
        {
            foreach (@{$query_sequence->getBBMHSequences()})
            {
                $hit_sequences{$_} ||= $_;
            }
            $sort_function = $sort_by_bbmh;
        }

        if ($output_columns =~ m/all/i || !$sort_function)
        {
            $sort_function = $sort_by_homology_and_bbmh;
        }

        # remove unwanted species
        my (@hit_sequences, @grouped_hit_sequences, %loci_met);
        foreach my $hit_sequence (values(%hit_sequences))
        {
            if (exists($species_filter->{$hit_sequence->species_id}))
            {
                $loci_met{$hit_sequence->locus} ||= 0;
                $loci_met{$hit_sequence->locus}++;
                push(@hit_sequences, $hit_sequence);
            }
        }

        # sort sequences by:
        # 1) evolutive distance (closest first)
        # 2) or by bbmh distance
        # 3) or by species
        @hit_sequences = sort
            {
                return int($sort_function->($query_sequence));
            }
            @hit_sequences
        ;
        
        # regroup splice-forms
        my $current_splice_index = 0;
        foreach my $hit_sequence (@hit_sequences)
        {
            ++$current_splice_index;
            # check if the sequence has not already been processed as a splice-form
            if ($loci_met{$hit_sequence->locus})
            {
                push(@grouped_hit_sequences, $hit_sequence);
                $loci_met{$hit_sequence->locus}--;
                # get all splice-forms of current hit sequence
                # process the rest of the array to find splice-forms
                my $splice_index = $current_splice_index;
                while (($splice_index < @hit_sequences)
                    && $loci_met{$hit_sequence->locus})
                {
                    if ($hit_sequences[$splice_index]->locus eq $hit_sequence->locus)
                    {
                        # a splice-form has been found, add it and count it
                        push(@grouped_hit_sequences, $hit_sequences[$splice_index]);
                        $loci_met{$hit_sequence->locus}--;
                    }
                    ++$splice_index;
                }
            }
            # else: splice-form already added
        }


        # store matches
        push(
            @$homology_data,
            {
                'sequence' => $query_sequence,
                'homologs' => \@grouped_hit_sequences,
            },
        );
    }
    return $homology_data;
}


=pod

=head2 PrepareCompositeDumpColumns

B<Description>: Prepare the parameter hash for columns that should be dumped.

B<ArgsCount>: 1

=over 4

=item $output_columns: (string) (R)

String containing the kind of homologies/similarities to find. Allowed values
are 'homologs', 'bbmh' and 'all'.

=back

B<Return>: (hash)

column dump parameter hash. Keys are column labels and values are corresponding
object member path.

=cut

sub PrepareCompositeDumpColumns
{
    my ($output_columns) = @_;

    my $column_index = 'a';
    my $columns =
        {
            $column_index++ . '. Query ID'        => 'sequence.accession',
            $column_index++ . '. Query Species'   => 'sequence.species.name',
            $column_index++ . '. Homolog ID'      => 'homolog.name',
            $column_index++ . '. Homolog Species' => 'homolog.species.name',
        };

    if ($output_columns =~ m/all|homologs/i)
    {
        $columns->{$column_index++ . '. Homology Type'}         = 'homology.type';
        $columns->{$column_index++ . '. Evolutionary distance'} = 'homology.distance';
        $columns->{$column_index++ . '. Node distance'}         = 'homology.speciation';
    }

    if ($output_columns =~ m/all|bbmh/i)
    {
        $columns->{$column_index++ . '. RBH score'}            = 'bbmh.score';
        $columns->{$column_index++ . '. RBH e-value'}          = 'bbmh.e_value';
    }

    return $columns;
}


=pod

=head2 PrepareCGIDumpColumns

B<Description>: Prepare the parameter hash for columns that should be dumped on
the HTML result page.

B<ArgsCount>: 1

=over 4

=item $output_columns: (string) (R)

String containing the kind of homologies/similarities to find. Allowed values
are 'homologs', 'bbmh' and 'all'.

=back

B<Return>: (hash)

column dump parameter hash. Keys are column labels and values are corresponding
object member path.

=cut

sub PrepareCGIDumpColumns
{
    my ($output_columns) = @_;

    my $column_index = 'a';
    my $columns =
        {
            $column_index++ . '. Query ID'           => 'getCGIMember.query',
            $column_index++ . '. Query Species'      => 'getCGIMember.query_species',
            $column_index++ . '. Homolog ID'         => 'getCGIMember.homolog',
            $column_index++ . '. Homolog Species'    => 'getCGIMember.homolog_species',
        };

    if ($output_columns =~ m/all|homologs/i)
    {
        $columns->{$column_index++ . '. Homology Type'}         = 'getCGIMember.homology_type';
        $columns->{$column_index++ . '. Evolutionary distance'} = 'getCGIMember.homology_distance';
        $columns->{$column_index++ . '. Node distance'}         = 'getCGIMember.homology_speciation';
    }

    if ($output_columns =~ m/all|bbmh/i)
    {
        $columns->{$column_index++ . '. RBH score'}            = 'getCGIMember.bbmh_score';
        $columns->{$column_index++ . '. RBH e-value'}          = 'getCGIMember.bbmh_evalue';
    }

    return $columns;
}


=pod

=head2 RenderHomologs

B<Description>: Render homology results into an HTML string.

B<ArgsCount>: 0

B<Return>: (string)

Returns an HTML string containing the GreenPhyl page for homology results.

=cut

sub RenderHomologs
{
    # get output columns
    my $output_columns = GetOutputColumnsCode();

    # get species
    my $species_filter = GetSpeciesFilter();
    my $species_array = [sort {$a->name cmp $b->name} values(%$species_filter)];

    # get homolog sequences
    my $homology_data = ComputeHomologs($output_columns, $species_filter);

    # prepare output columns for dumps
    my $columns = PrepareCGIDumpColumns($output_columns);

    # render result page
    return RenderHTMLFullPage(
        {
            'title'               => 'Homologs and/or similar sequences results',
            'dump_data'           =>
                {
                   'links' => [
                     {
                       'label'       => 'FASTA',
                       'format'      => 'fasta',
                       'file_name'   => 'homologs.fasta',
                       'namespace'   => 'fasta',
                     },
                     {
                       'label'       => 'Excel',
                       'format'      => 'excel',
                       'file_name'   => 'homologs.xls',
                       'namespace'   => 'excel',
                     },
                     {
                       'label'       => 'CSV',
                       'format'      => 'csv',
                       'file_name'   => 'homologs.csv',
                       'namespace'   => 'csv',
                     },
                     {
                       'label'       => 'XML',
                       'format'      => 'xml',
                       'file_name'   => 'homologs.xml',
                       'namespace'   => 'xml',
                     },
                   ],
                   'parameters'  => {'columns' => EncodeDumpParameterValue($columns),},
                   'object_type' => 'composite',
                   'fields'      => 'composite_id',
                   'mode'        => 'form',
                },
            'species_list'        => $species_array,
            'output_homologs'     => ($output_columns =~ m/all|homologs/i)?1:0,
            'output_bbmh'         => ($output_columns =~ m/all|bbmh/i)?1:0,
            'homology_data'       => $homology_data,
            'content'             => 'tools/get_homologs_results.tt',
        },
    );
}


=pod

=head2 DumpHomologs

B<Description>: Dumps all user sequence homologs/similar sequences.

B<ArgsCount>: 0

B<Return>: (string)

Returns the dump in the specified format.

=cut

sub DumpHomologs
{
    # get output columns
    my $output_columns = GetOutputColumnsCode();

    # get species
    my $species_filter = GetSpeciesFilter();

    # get homolog sequences
    my $homology_data = ComputeHomologs($output_columns, $species_filter);

    # get dump parameters
    my @namespaces   = GetParameterValues('namespace');
    my $namespace    = GetPageAction(\@namespaces);
    my $dump_format  = GetDumpFormat($namespace);
    my $file_name    = GetDumpFileName($namespace);

    # prepare output columns for dumps
    my $columns = PrepareCompositeDumpColumns($output_columns);
    my $parameters = {'columns' => $columns};

    # create table to dump
    my $composite_objects = [];
    foreach my $sequence_homology (@$homology_data)
    {
        my $sequence = $sequence_homology->{'sequence'};
        foreach my $homolog (@{$sequence_homology->{'homologs'}})
        {
            my $row = {};
            $row->{'sequence'} = $sequence;
            $row->{'homolog'}  = $homolog;
            if ($output_columns =~ m/all|homologs/i)
            {
                $row->{'homology'} = $sequence->getHomologyWith($homolog);
            }
            if ($output_columns =~ m/all|bbmh/i)
            {
                $row->{'bbmh'} = $sequence->getBBMHWith($homolog);
            }
            push(@$composite_objects, Greenphyl::CompositeObject->new($row));
        }
    }

    if (!@$composite_objects)
    {
        Throw('error' => 'Sorry, given sequence(s) gave no match!<br/>Go <a href="' . url() . '">back</a>');
    }
    
    my $dump_content = DumpObjects($composite_objects, $dump_format, $parameters);

    # get header after content in case we got an error during content generation
    # so we can still generate a clean header
    my $header = RenderHTTPFileHeader({'file_name' => $file_name, 'format' => $dump_format});
    # to not ouput as attachement:
    # my $header = RenderHTTPFileHeader({'mime' => 'TEXT', 'format' => 'fasta'}); #+debug 
    return $header . $dump_content;
}


=pod

=head2 DumpHomologsToFASTA

B<Description>: Dumps all user sequence homologs/similar sequences.

B<ArgsCount>: 0

B<Return>: (string)

Returns the dump in the specified format.

=cut

sub DumpHomologsToFASTA
{
    # get output columns
    my $output_columns = GetOutputColumnsCode();

    # get species
    my $species_filter = GetSpeciesFilter();

    # get homolog sequences
    my $homology_data = ComputeHomologs($output_columns, $species_filter);

    # get dump parameters
    my $dump_format  = 'fasta';
    my $file_name    = 'homologs.fasta';

    # group homologs all together without regarding query sequences
    my $homolog_sequences;
    foreach my $sequence_homology (@$homology_data)
    {
        push(@$homolog_sequences, @{$sequence_homology->{'homologs'}});
    }
    
    if (!@$homolog_sequences)
    {
        Throw('error' => 'Sorry, given sequence(s) gave no match!<br/>Go <a href="' . url() . '">back</a>');
    }
    
    my $dump_content = DumpObjects($homolog_sequences, $dump_format);

    # get header after content in case we got an error during content generation
    # so we can still generate a clean header
    my $header = RenderHTTPFileHeader({'file_name' => $file_name, 'format' => $dump_format});
    # to not ouput as attachement:
    # my $header = RenderHTTPFileHeader({'mime' => 'TEXT', 'format' => 'fasta'}); #+debug 
    return $header . $dump_content;
}




# Script options
#################

=pod

=head1 OPTIONS

    get_homologs.cgi p=from
    get_homologs.cgi p=results seq_textids=<seq_textids> target_species=<tax_id> output_selection=<homologs>

=head2 Parameters

=over 4

=item B<seq_textids> (list of seq_textids):

a list of new-line separated sequence text IDs.

=back

=cut


# CODE START
#############

# hash for function selection.
my $actions =
    {
        ''        => \&RenderForm,
        'form'    => \&RenderForm,
        'results' => {
            ''      => \&RenderHomologs,
            'fasta' => \&DumpHomologsToFASTA,
            'excel' => \&DumpHomologs,
            'csv'   => \&DumpHomologs,
            'xml'   => \&DumpHomologs,
        },
    };

# try block
eval
{
    # display page
    print ProceedPage($actions);
};

HandleErrors();

exit(0);

# CODE END
###########


=pod

=head1 AUTHORS

Mathieu ROUARD (Bioversity), m.rouard@cgiar.org

Valentin GUIGNON (Bioversity), v.guignon@cgiar.org

Matthieu CONTE (Syngenta)

=head1 VERSION

Version 1.0.0

Date 24/05/2012

=head1 SEE ALSO

GreenPhyl documentation.

=cut
