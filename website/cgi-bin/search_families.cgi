#!/usr/bin/perl

=pod

=head1 NAME

search_families.cgi - Family search interface

=head1 SYNOPSIS

    search_families.cgi

=head1 REQUIRES

Perl5

=head1 DESCRIPTION

Displays GreenPhyl family search interface.

=cut

use strict;
use warnings;

use lib '../lib';
use lib '../local_lib';
use Carp qw (cluck confess croak);

use Greenphyl;
use Greenphyl::Web;
use Greenphyl::Species;
use Greenphyl::Taxonomy;

use Greenphyl::Tools::Taxonomy;




# Script global functions
##########################

=pod

=head1 FUNCTIONS

=head2 RenderFamilySearchInterface

B<Description>: [function description]. #+++

B<ArgsCount>: 0

B<Return>: (string)

Returns an HTML string containing the GreenPhyl page for ... #+++

=cut

sub RenderFamilySearchInterface
{

    my $taxonomy_html_tree = GetHTMLTaxonomySelectionTree();
 
    return RenderHTMLFullPage(
        {
            'title'              => 'Family Advanced Search',
            'content'            => 'family_tools/search_form.tt',
            'taxonomy_html_tree' => $taxonomy_html_tree,
        },
    );
}




# Script options
#################

=pod

=head1 OPTIONS

    search_families.cgi

=head2 Parameters

none

=cut


# CODE START
#############

# hash for function selection.
my $actions =
    {
        ''       => \&RenderFamilySearchInterface,
    };

# try block
eval
{
    # display page
    print ProceedPage($actions);
};

HandleErrors();

exit(0);

# CODE END
###########


=pod

=head1 DIAGNOSTICS

=head1 AUTHORS

Mathieu ROUARD (Bioversity), m.rouard@cgiar.org

Valentin GUIGNON (Bioversity), v.guignon@cgiar.org

Matthieu CONTE (Syngenta)

Sebastien BRIOIS (Syngenta)

=head1 VERSION

Version 1.0.0

Date 03/10/2012

=head1 SEE ALSO

GreenPhyl documentation.

=cut
