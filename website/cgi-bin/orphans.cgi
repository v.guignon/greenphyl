#!/usr/bin/perl

=pod

=head1 NAME

orphans.cgi - Display GreenPhyl orphan gene sequences

=head1 SYNOPSIS

    orphans.cgi

=head1 REQUIRES

Perl5

=head1 DESCRIPTION

Display GreenPhyl orphan gene sequences (ie. sequences that do not belong to
any family).

=cut

use strict;
use warnings;

use lib '../lib';
use lib '../local_lib';
use Carp qw (cluck confess croak);

use Greenphyl;
use Greenphyl::Web;
use Greenphyl::Sequence;
use Greenphyl::Species;
use Greenphyl::Dumper;
use Greenphyl::Tools::Families;




# Script global functions
##########################

=pod

=head1 FUNCTIONS

=head2 GetSelectedSpecies

B<Description>: Returns the list of species specified by the user.

B<ArgsCount>: 0

B<Return>: (list of integer)

an array of species ID.

=cut

sub GetSelectedSpecies
{
    my @species_ids;

    my @species_filter = GetParameterValues('species_id');
    if (@species_filter)
    {
        foreach (@species_filter)
        {
            foreach my $species_id (split(',', $_))
            {
                # only keep valid species ID
                if ($species_id =~ m/^\d+$/)
                {
                    push(@species_ids, $species_id);
                }
            }
        }
    }

    return @species_ids;
}


=pod

=head2 RenderOrphans

B<Description>: Render a list of orphan gene sequences.

B<ArgsCount>: 0

B<Return>: (string)

Returns an HTML string containing the GreenPhyl page for orphan gene sequences.

=cut

sub RenderOrphans
{
    my $title = '';
    my $query_parameters = GetOrphanQueryParameters();
    my @species = sort {$a->name cmp $b->name} Greenphyl::Species->new(GetDatabaseHandler(), {'selectors' => { 'display_order' => ['>', 0], },});
    unshift(@species, Greenphyl::Species->new(undef, {'members' => { 'organism' => 'All', 'code' => 'ALL', 'id' => '-1' } }));
    my $no_species = 0;

    my @selected_species;
    if (my @selected_species_id = GetSelectedSpecies())
    {
        @selected_species = sort {$a->name cmp $b->name} Greenphyl::Species->new(GetDatabaseHandler(), { 'selectors' => {'id' => ['IN', @selected_species_id]} });
    }
    if (!defined(GetParameterValues('species_id')))
    {
        # tip when no species is selected. select fake species to return nothing
        $query_parameters->{'selectors'}->{'species_id'} = 0;
        $no_species = 1;
    }

    my ($sequences, $pager_data) = GetPagedObjects('Greenphyl::Sequence', $query_parameters);
    $pager_data->{'entry_ranges_label'} = 'Sequences per page:';
    my $total_sequence_count = $pager_data->{'pager'}->total_entries;

    my $dumper_script = CGI::url('-relative' => 1, '-query_string' => 1);
    $dumper_script = RemovePagerParameters($dumper_script);
    $dumper_script =~ s/([?&;]p=)\w+/$1dump/;
    $dumper_script ||= 'orphans.cgi?';
    $dumper_script =~ s/([^?&;])$/$1&amp;/; # append a '&' to the URL if missing

    return RenderHTMLFullPage(
        {
            'title'               => $title || 'Orphan Gene Sequences',
            'before_content'      => 'family_tools/orphans.tt',
            'content'             => 'sequences/sequence_list.tt',
            'sequence_count'      => $total_sequence_count,
            'sequences'           => $sequences,
            'columns'             => ['annotation'],
            'additional'          => {
                'columns' => [
                    {
                        'label'  => 'Sequence length',
                        'member' => 'current_object.length',
                    },
                ],
            },
            'not_sortable'        => 1,
            'pager_data'          => $pager_data,
            'species_list'        => \@species,
            'selected_species_list' => \@selected_species,
            'dump_data' =>
            {
                'links' => [
                    {
                        'label'       => 'Fasta',
                        'format'      => 'fasta',
                        'file_name'   => 'orphans.fasta',
                        'namespace'   => 'fasta',
                    },
                ],
                'object_type' => 'sequences',
                'mode'        => 'link',
                'script'      => $dumper_script,
            },
            'no_species' => $no_species,
        },
    );
}


=pod

=head2 DumpOrphanList

B<Description>: Dump a list of orphan sequences.

B<ArgsCount>: 0

B<Return>: (string)

Returns a FASTA file of the orphan sequences.

=cut

sub DumpOrphanList
{
    my $query_parameters = GetOrphanQueryParameters();

    my $sequences = [Greenphyl::Sequence->new(GetDatabaseHandler(), $query_parameters)];
    if (!@$sequences)
    {
        Throw('error' => 'No sequence to dump!');
    }

    my $dump_format  = GetDumpFormat();
    my $file_name    = GetDumpFileName();
    my $dump_content = DumpObjects($sequences, $dump_format);

    # get header after content in case we got an error during content generation
    # so we can still generate a clean header
    my $header = RenderHTTPFileHeader({'file_name' => $file_name, 'format' => $dump_format});
    return $header . $dump_content;

}




# Script options
#################

=pod

=head1 OPTIONS

orphans.cgi? p=<list|dump> [&species=<ID>] [&level=<1-4>] [&has_level=<1-4>]

=head2 Parameters

=over 4

=item B<p> (string):

list: lists orphan sequences
dump: dump sequences into FASTA
Default: list

=item B<species> (string):

List of species for sequence filtering. Only sequences that belong to the
specified species identifier are returned. Several identifiers can be separated
using coma. Species identifier correspond to the database field
'species.species_id'.
Default: all species (no filtering)

=back

=cut


# CODE START
#############

# hash for function selection.
my $actions =
    {
        ''     => \&RenderOrphans,
        'list' => \&RenderOrphans,
        'dump' => \&DumpOrphanList,
    };

# try block
eval
{
    # display page
    print ProceedPage($actions);
};

HandleErrors();

exit(0);

# CODE END
###########


=pod

=head1 AUTHORS

Mathieu ROUARD (Bioversity), m.rouard@cgiar.org

Valentin GUIGNON (Bioversity), v.guignon@cgiar.org

Matthieu CONTE (Syngenta)

=head1 VERSION

Version 1.0.0

Date 25/06/2012

=head1 SEE ALSO

GreenPhyl documentation.

=cut
