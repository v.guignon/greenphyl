#!/usr/bin/perl

=pod

=head1 NAME

family.cgi - display list of gene family and details of each family

=head1 SYNOPSIS

    /family.cgi?id=20827

=head1 REQUIRES

Perl5

=head1 DESCRIPTION

This script runs the a cgi page for family stored in the database.

=cut

use strict;
use warnings;

use lib '../lib';
use lib '../local_lib';
use Carp qw (cluck confess croak);

use Greenphyl;
use Greenphyl::Web;
use Greenphyl::Family;
use Greenphyl::Tools::Families;
use Greenphyl::Dumper;
use Greenphyl::Species;
use Greenphyl::Taxonomy;
use Greenphyl::CompositeObject;
use Greenphyl::Sequence;
use Greenphyl::SequenceCount;
use Greenphyl::CustomFamily;
use Greenphyl::User;




# Script global constants
##########################

=pod

=head1 CONSTANTS

B<$DEBUG>: (boolean)

Enables debug mode.

=cut

our $DEBUG = 0;




# Script global variables
##########################

=pod

=head1 VARIABLES

B<$g_dbh>: (DBI::db)

Current database handler.

=cut

my $g_dbh = GetDatabaseHandler();




# Script global functions
##########################

=pod

=head1 FUNCTIONS

=head2 RenderFamilyIdCard

B<Description>: Renders a single family identification card with family details.

B<ArgsCount>: 0

B<Return>: (string)

Returns an HTML string containing the family identification card.

=cut

sub RenderFamilyIdCard
{
    my $selectors = { 'selectors' => {} };
    if (my $family_id = GetParameterValues('family_id'))
    {
        $selectors->{'selectors'}->{'id'} = $family_id;
    }
    elsif (my $accession = GetParameterValues('accession'))
    {
        $selectors->{'selectors'}->{'accession'} = $accession;
    }
    else
    {
        Throw('error' => 'No family identifier provided!');
    }

    my $family = Greenphyl::Family->new($g_dbh, $selectors);

    # make sure the family is in database
    if (!$family)
    {
        # if no family has been found but we got an accession, check v3
        if ($selectors->{'selectors'}->{'accession'})
        {
            # Get v3 user
            my $v3_user = Greenphyl::User->new($g_dbh, {'selectors' => {'login' => 'greenphyl_v3'}});
            if ($v3_user)
            {
                $selectors->{'selectors'}->{'user_id'} = $v3_user->id;
                $selectors->{'selectors'}->{'accession'} = 'V3' . substr($selectors->{'selectors'}->{'accession'}, 2);
                my $custom_family = Greenphyl::CustomFamily->new($g_dbh, $selectors);
                if ($custom_family)
                {
                    return RenderHTMLFullPage(
                        {
                            'title'        => 'Old Family Link',
                            'content'      => 'families/family_removed.tt',
                            'family'       => $custom_family,
                        }
                    );
                }
            }
        }

        Throw('error' => "Family not found in database!");
    }

    # highlight family (for cluster structure rendering)
    $family->setHighlight(1);

    # my @species = sort { $a->name cmp $b->name } Greenphyl::Species->new($g_dbh, {'selectors' => { 'display_order' => ['>', 0], },});
    my @species = sort { $a->name cmp $b->name } @{$family->species};

    my $family_composition_data  = GenerateFamilyCompositionData($family);
    my $family_protein_domains   = GenerateFamilyProteinDomainData($family);
    my $family_protein_list      = GenerateFamilyProteinListData($family);
    my $phylogenomic_analysis    = GeneratePhylogenomicAnalysisData($family);

    my $tabs = [
        {
            'id'      => 'tab-famcomp',
            'title'   => 'Family Composition',
            'content' => 'family_tools/family_composition.tt',
        },
        {
            'id'      => 'tab-famstruct',
            'title'   => 'Family Structure',
            'content' => 'families/family_relationship.tt',
        },
        {
            'id'      => 'tab-protdom',
            'title'   => 'Protein Domains',
            'content' => 'family_tools/protein_domains.tt',
        },
    ];

    if (grep {$_} values(%$phylogenomic_analysis))
    {
        push(@$tabs,
            {
                'id'      => 'tab-phyloana',
                'title'   => 'Phylogenomic Analysis',
                'content' => 'family_tools/phylogenomic_analysis.tt',
            },
        );
    }

    if (@{$family->homologies})
    {
        push(@$tabs,
            {
                'id'      => 'tab-homopred',
                'title'   => 'Homolog Predictions',
                'content' => 'family_tools/homolog_predictions.tt',
            },
        );
    }

    return RenderHTMLFullPage(
        {
            'title'        => 'Family ' . $family->accession,
            'content'      => 'family_tools/family_details.tt',
            'javascript'   => ['d3-sankey.min', 'raphael', 'raphael-zpd', 'g.raphael', 'g.bar', 'chart', 'ipr', 'relationship_graph', 'families',],
            'family'       => $family,
            'species_list' => \@species,

            # tab-specific data
            # family composition
            'family_composition'     => $family_composition_data,
            # protein domains
            'family_protein_domains' => $family_protein_domains,
            # protein list
            'family_protein_list'    => $family_protein_list,
            # phylogenomic analysis
            'phylogenomic_analysis'  => $phylogenomic_analysis,

            # tabs definition
            'contents' => $tabs,
        },
    );
}




# Script options
#################

=pod

=head1 OPTIONS

    family.cgi ?p=id &family_id=<FAMILY_ID>
    family.cgi ?p=id &accession=<FAMILY_ACCESSION>

=head2 Parameters

=over 4

=item B<family_id> FAMILY_ID (integer):

Family identifier in database.

=item B<accession> FAMILY_ACCESSION (string):

A family accession.

=back

=cut


# CODE START
#############

# hash for function selection.
my $actions =
    {
        'id'        =>
        {
            'family_id' => \&RenderFamilyIdCard,
            'accession' => \&RenderFamilyIdCard,
        },
    };

# try block
eval
{
    # display page
    print ProceedPage($actions);
};

HandleErrors();

exit(0);

# CODE END
###########


=pod

=head1 AUTHORS

Mathieu ROUARD (Bioversity), m.rouard@cgiar.org

Valentin GUIGNON (Bioversity), v.guignon@cgiar.org

Matthieu CONTE (Syngenta)

=head1 VERSION

Version 1.0.0

Date 12/06/2012

=head1 SEE ALSO

GreenPhyl documentation.

=cut
