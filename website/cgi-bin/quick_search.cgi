#!/usr/bin/perl

=pod

=head1 NAME

quick_search.cgi - Search on several fields at the same time

=head1 SYNOPSIS

    quick_search.cgi?qsearch_input=hkt&qsearch_type=any&qsearch_submit=Search

=head1 REQUIRES

Perl5

=head1 DESCRIPTION

Displays the Quick Search form. Perfoms search through 'quick_search.pl'.

=head1 TRICKS

* my %SEARCH_TYPES = map {$_ => 1} ('any', (map { map {$_->{'type'}} @{$_->{'search_types'}} } @SEARCH_GROUPS));

    ('any', (<second map expression>))
generates a list containing 'any' and the content returned by the second map
expression. $_ correspond to each type name including the 'any' type.

    map {$_ => 1} ('any', (<second map expression>));
the first map generates a list of key => value pairs for the hash. Each key is a
search type, each value is set to '1' (true boolean value) to enable the search
type.

    map { <third map expression> } @SEARCH_GROUPS
this is the "second map expression". It processes each first-level element of
the array @SEARCH_GROUPS which means each group hash. The third map expression
then processes each group hash.

    @{$_->{'search_types'}}
this is the list provided to the third map expression. It contains all the
search type hash of each group hash (ie. the content of the array associated
to the key 'search_types' of each group hash).
 
    map {$_->{'type'}} @{$_->{'search_types'}}
process each search type hash and returns its 'type' value.
Note: the first $_ correspond to each element of "@{$_->{'search_types'}}" while
the second $_ (in "@{$_->{'search_types'}}") correspond to each element of
@SEARCH_GROUPS as specified in the first map.

=cut

use strict;
use warnings;

use lib '../lib';
use lib '../local_lib';

use Greenphyl;
use Greenphyl::Web;


# Script global constants
##########################

=pod

=head1 CONSTANTS

B<$DEFAULT_SEARCH_TEXT>: (string)

Default text displayed in the input box.

B<@SEARCH_GROUPS>: (array)

Array containing search form settings.
First-level hash are groups of search.
Each hash contains a label and an array of search type.
Each search type as a type name and a label.

This array is used to both display the result table and know what kind of
search to perform. See also %SEARCH_TYPES hash which uses @SEARCH_GROUPS for
its initialization.

B<%SEARCH_TYPES>: (hash)

Contains available search types. Keys are search types and values are just
boolean values set to 1. Type list (keys) is build using @SEARCH_GROUPS and
contains all the 'type' types of each 'search_types' plus the type 'any'.

=cut

our $DEBUG = 0;

my $DEFAULT_SEARCH_TEXT = 'Enter your text to search here';

my @SEARCH_GROUPS =
    (
        {
            'label' => 'Families',
            'short' => 'F.',
            'search_types' =>
            [
                {'type' => 'family_id',    'label' => 'Family ID', 'short' => 'id', },
                {'type' => 'family_name',  'label' => 'Family Name', 'short' => 'name', },
                {'type' => 'interpro_fam', 'label' => 'Interpro', 'short' => 'IPR', },
                {'type' => 'go_fam',       'label' => 'GO', 'short' => 'GO', },
            ],
        },
        {
            'label' => 'Sequences',
            'short' => 'S.',
            'search_types' =>
            [
                {'type' => 'sequence_id',    'label' => 'Sequence ID', 'short' => 'id', },
                {'type' => 'uniprot_seq',    'label' => 'UniProt', 'short' => 'UP', },
                {'type' => 'annotation_seq', 'label' => 'Annotation', 'short' => 'ann.', },
#                {'type' => 'kegg_seq',       'label' => 'KEGG', 'short' => 'KEGG', },
#                {'type' => 'ec_seq',         'label' => 'EC', 'short' => 'EC', },
#                {'type' => 'interpro_seq',   'label' => 'Interpro', 'short' => 'IPR', },
                {'type' => 'go_seq',         'label' => 'GO', 'short' => 'GO', },
#                {'type' => 'pubmed_seq',     'label' => 'PubMed References', 'short' => 'PM', },
            ],
        }
    );

# gather all the 'type' values of each 'search_types' from @SEARCH_GROUPS and
# add type 'any' to that list. These list items are mapped as keys of the hash
# and each value is set to '1' (search type enabled).
# For details see TRICKS section.
my %SEARCH_TYPES = map {$_ => 1} ('any', (map { map {$_->{'type'}} @{$_->{'search_types'}} } @SEARCH_GROUPS));




# Script global functions
##########################

=pod

=head1 FUNCTIONS

=head2 RenderQuickSearchForm

B<Description>: Render the Quick Search form into a string.

B<ArgsCount>: 0

B<Return>: (string)

Returns an HTML string containing the GreenPhyl page for Quick Search.

=cut

sub RenderQuickSearchForm
{
    my $output;

    my $qsearch_input = GetParameterValues('qsearch_input') || '';
    my $qsearch_type  = GetParameterValues('qsearch_type')  || '';
    my $qdeep_search  = GetParameterValues('qdeep_search')  || '';
    my $using_ajax    = GetParameterValues('using_ajax')    || '';
    my $master_mode   = GetParameterValues('mmode')         || '';

    # contains search results for non-javascript submissions
    # keys are search types and values are HTML content
    my $results = {};
    
    if (!$using_ajax
        && $qsearch_input
        && $qsearch_type
        && exists($SEARCH_TYPES{$qsearch_type}))
    {
        my @searches = ();
        if ('any' eq $qsearch_type)
        {
            # remove 'any' element
            delete($SEARCH_TYPES{'any'});
            @searches = (keys(%SEARCH_TYPES));
        }
        else
        {
            @searches = ($qsearch_type);
        }

        foreach my $search (@searches)
        {
            my $search_results_sh;
            $qsearch_input =~ s/[^\w\-\.\+\ ]//g;
            $qsearch_type  =~ s/[^\w\-\.\+\ ]//g;
            $qdeep_search  =~ s/[^\w\-\.\+\ ]//g;
            my $command_line = GP('CGI_PATH') . "/quick_search.pl mode=inner mmode=$master_mode qsearch_input='$qsearch_input' qsearch_type='$search' qdeep_search='$qdeep_search' 2>\&1";
            PrintDebug("COMMAND LINE: $command_line");
            open($search_results_sh, "-|")
#                or exec({GP('CGI_PATH') . "/quick_search.pl"}
#                    GP('CGI_PATH') . "/quick_search.pl",
#                    'mode=inner',
#                    "qsearch_input=$qsearch_input",
#                    "qsearch_type=$search",
#                );
                or exec($command_line);
           $results->{$search} = join('', <$search_results_sh>);
       }
    }

    $output .= RenderHTMLFullPage(
        {
            'title'         => 'Quick Search',
            'content'       => 'qsearch/quick_search.tt',
            'search_groups' => \@SEARCH_GROUPS,
            'max_results'   => GP('SEARCH_MAX_RESULTS'),
            'search_query'  =>
                {
                    'input'       => $qsearch_input,
                    'type'        => $qsearch_type,
                    'deep_search' => $qdeep_search,
                },
            'results'       => $results,
            'mmode'         => $master_mode,
        }
    );

    return $output;
}




# Script options
#################

=pod

=head1 OPTIONS

quick_search.cgi [qsearch_input=<input> qsearch_type=<search_type>]

=head2 Parameters

=over 4

=item B<qsearch_input> (string):

contains the input text to search in database.

=item B<qsearch_type> (string):

select the search type. Must be one of 'any' or any of the 'type' values of the
@SEARCH_GROUPS array.

=back

=cut


# CODE START
#############

# hash for function selection.
my $actions =
    {
        ''             => \&RenderQuickSearchForm,
    };

# try block
eval
{
    # display page
    print ProceedPage($actions);
};

HandleErrors();

exit(0);

# CODE END
###########


=pod

=head1 AUTHORS

Mathieu ROUARD (Bioversity), m.rouard@cgiar.org

Valentin GUIGNON (Bioversity), v.guignon@cgiar.org

Matthieu CONTE (Syngenta)

=head1 VERSION

Version 1.0.0

Date 07/05/2012

=head1 SEE ALSO

GreenPhyl documentation.

=cut
