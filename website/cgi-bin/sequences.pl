#!/usr/bin/perl

=pod

=head1 NAME

sequences.pl - Performs sequences services

=head1 SYNOPSIS

    sequences.pl?check_textid=At1g14920.1

=head1 REQUIRES

Perl5

=head1 DESCRIPTION

Performs several sequences services.

=cut

use strict;
use warnings;

use lib '../lib';
use lib '../local_lib';
use Carp qw (cluck confess croak);

use Greenphyl;
use Greenphyl::Web;
use Greenphyl::Web::PseudoService;
use Greenphyl::Tools::Sequences;




# Script global constants
##########################

=pod

=head1 CONSTANTS

B<$SEQUENCE_SERVICES>: (hash ref)

Contains the sequence service handlers.

=cut

our $DEBUG = 0;
our $SEQUENCE_SERVICES =
    {
        'check_seq_textids' => \&Greenphyl::Tools::Sequences::CheckSeqTextID,
        'get_sequence_data' => \&Greenphyl::Tools::Sequences::GetSequenceList,
        'get_homologs_data' => \&Greenphyl::Tools::Sequences::GetHomologList,
    };




# Script options
#################

=pod

=head1 OPTIONS

    sequences.pl <service=<service_name>> ...<service parameters>

=head2 Parameters

=over 4

=item B<service> (string):

name of the service.

=back

=cut


# CODE START
#############

HandleService($SEQUENCE_SERVICES);

exit(0);

# CODE END
###########


=pod

=head1 AUTHORS

Mathieu ROUARD (Bioversity), m.rouard@cgiar.org

Valentin GUIGNON (Bioversity), v.guignon@cgiar.org

Matthieu CONTE (Syngenta)

=head1 VERSION

Version 1.0.0

Date 30/05/2012

=head1 SEE ALSO

GreenPhyl documentation.

=cut
