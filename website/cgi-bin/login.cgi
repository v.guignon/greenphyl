#!/usr/bin/perl

=pod

=head1 NAME

login.cgi - Login page

=head1 SYNOPSIS

    login.cgi

=head1 REQUIRES

Perl5

=head1 DESCRIPTION

Displays login form.

=cut

use strict;
use warnings;

use lib '../lib';
use lib '../local_lib';
use Carp qw (cluck confess croak);

use Greenphyl; # includes Config and Exceptions
use Greenphyl::Web; # includes CGI:standard and Templates
use Greenphyl::OpenID;
use Greenphyl::Web::Access;



# Script global functions
##########################

=pod

=head1 FUNCTIONS

=head2 RenderLoginForm

B<Description>: Renders login form.

B<ArgsCount>: 0

B<Return>: (string)

Returns an HTML string containing the GreenPhyl page for the login page.

=cut

sub RenderLoginForm
{
    my $page_parameters = GetOpenIDPageParameters(
        {
            'title'     => 'Login',
            'content'   => 'users/login.tt',
            'form_data' =>
                {
                    'identifier'  => 'login_page_form',
                    'submit'      => 'Login',
                    'customstyle' => 'center-block',
                },
            'access_message' => GetAccessErrorMessage(),
        }
    );

    return RenderHTMLFullPage($page_parameters);
}




# Script options
#################

=pod

=head1 OPTIONS

Displays login screen.

=cut


# CODE START
#############

# hash for function selection.
my $actions =
    {
        '' => \&RenderLoginForm,
    };

# try block
eval
{
    # display page
    print ProceedPage($actions);
};

HandleErrors();

exit(0);

# CODE END
###########


=pod

=head1 DIAGNOSTICS

=head1 AUTHORS

Mathieu ROUARD (Bioversity), m.rouard@cgiar.org

Valentin GUIGNON (Bioversity), v.guignon@cgiar.org

Matthieu CONTE (Syngenta)

Sebastien BRIOIS (Syngenta)

=head1 VERSION

Version 1.0.0

Date 03/10/2012

=head1 SEE ALSO

GreenPhyl documentation.

=cut
