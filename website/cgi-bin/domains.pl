#!/usr/bin/perl

=pod

=head1 NAME

domains.pl - Display family sequence protein domains.

=head1 SYNOPSIS

    domains.pl?service=get_family_domains&format=html&family_id=42&iprdomains=1&level1=1&level2=1&level3=0&level4=0

=head1 REQUIRES

Perl5

=head1 DESCRIPTION

Renders IPR and meme domains for sequences of a given family.

=cut

use strict;
use warnings;

use FindBin;
use lib "$FindBin::Bin/../lib";
use lib "$FindBin::Bin/../local_lib";
use Carp qw (cluck confess croak);

use Greenphyl;
use Greenphyl::Web;
use Greenphyl::Web::PseudoService;
use Greenphyl::Tools::Domains;
use Greenphyl::Tools::Families;




# Script global constants
##########################

=pod

=head1 CONSTANTS

B<$DOMAINS_SERVICES>: (hash ref)

Contains the family service handlers.

=cut

our $DEBUG = 0;
our $DOMAINS_SERVICES =
    {
        'get_family_domains' => \&Greenphyl::Tools::Domains::GetFamilyDomains,
    };


# Script options
#################

=pod

=head1 OPTIONS

    domains.pl <service=<service_name>> ...<service parameters>

=head2 Parameters

=over 4

=item B<service> (string):

name of the service.

=back

=cut


# CODE START
#############

HandleService($DOMAINS_SERVICES);

exit(0);

# CODE END
###########


=pod

=head1 AUTHORS

Mathieu ROUARD (Bioversity), m.rouard@cgiar.org

Valentin GUIGNON (Bioversity), v.guignon@cgiar.org

Matthieu CONTE (Syngenta)

=head1 VERSION

Version 1.0.0

Date 15/02/2013

=head1 SEE ALSO

GreenPhyl documentation.

=cut
