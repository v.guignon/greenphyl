#!/usr/bin/perl

=pod

=head1 NAME

species.pl - Performs species services

=head1 SYNOPSIS

    species.pl?complete=ART

=head1 REQUIRES

Perl5

=head1 DESCRIPTION

Performs several species services.

=cut

use strict;
use warnings;

use lib '../lib';
use lib '../local_lib';
use Carp qw (cluck confess croak);

use Greenphyl;
use Greenphyl::Web;
use Greenphyl::Web::PseudoService;
use Greenphyl::Tools::Taxonomy;




# Script global constants
##########################

=pod

=head1 CONSTANTS

B<$SPECIES_SERVICES>: (hash ref)

Contains the species service handlers.

=cut

our $DEBUG = 0;
our $SPECIES_SERVICES =
    {
        'complete'      => \&Greenphyl::Tools::Taxonomy::CompleteSpeciesCode,
        'complete_name' => \&Greenphyl::Tools::Taxonomy::CompleteSpeciesName,
    };




# Script options
#################

=pod

=head1 OPTIONS

    species.pl <service=<service_name>> ...<service parameters>

=head2 Parameters

=over 4

=item B<service> (string):

name of the service.

=back

=cut


# CODE START
#############

HandleService($SPECIES_SERVICES);

exit(0);

# CODE END
###########


=pod

=head1 AUTHORS

Mathieu ROUARD (Bioversity), m.rouard@cgiar.org

Valentin GUIGNON (Bioversity), v.guignon@cgiar.org

Matthieu CONTE (Syngenta)

=head1 VERSION

Version 1.0.0

Date 27/08/2013

=head1 SEE ALSO

GreenPhyl documentation.

=cut
