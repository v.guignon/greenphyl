#!/usr/bin/perl

=pod

=head1 NAME

go_browser.cgi - Display GO Browser interface

=head1 SYNOPSIS

    go_browser.cgi

=head1 REQUIRES

Perl5

=head1 DESCRIPTION

Displays GO Browser interface.

=cut

use strict;
use warnings;

use lib "../lib";
use lib '../local_lib';
use Carp qw (cluck confess croak);

use Greenphyl;
use Greenphyl::Web;
use Greenphyl::Tools::GO;




# Script global constants
##########################

=pod

=head1 CONSTANTS

B<$DEBUG>: (boolean)

When set to true, it enables debug mode.

=cut

our $DEBUG = 0;




# Script global functions
##########################

=pod

=head1 FUNCTIONS

=head2 RenderGOBrowser

B<Description>: Display GO Browser.

B<ArgsCount>: 0

B<Return>: (string)

Returns an HTML string containing the GO Browser.

=cut

sub RenderGOBrowser
{
    if (DoGOBrowserTemplateNeedsUpdate())
    {
        GenerateGOBrowserTemplate();
    }
    my $max_depth = GetParameterValues('maxdepth');

    return RenderHTMLFullPage(
        {
            'title'      => 'GO Browser',
            'javascript' => ['jquery.treeview.edit', 'jquery.treeview.async', ],
            'content'    => $Greenphyl::Tools::GO::GO_BROWSER_TEMPLATE,
            'max_depth'  => $max_depth,
        },
    );
}




# Script options
#################

=pod

=head1 OPTIONS

go_browse.cgi? [nocache=<0,1>] [maxdepth=<level>]

=head2 Parameters

=over 4

=item B<nocache> (boolean):

Disables cache version and recompute the browser tree. It also updates cache
version.

=item B<max_depth> (integer):

Max depth of loaded trees/subtrees. When the user tries to access to deeper
branches, the subtrees are automatically loaded.
Default: 2 (set in the templates)

=back

=cut


# CODE START
#############

# hash for function selection.
my $actions =
    {
        '' => \&RenderGOBrowser,
    };

# try block
eval
{
    # display page
    print ProceedPage($actions);
};

HandleErrors();

exit(0);

# CODE END
###########


=pod

=head1 AUTHORS

Mathieu ROUARD (Bioversity), m.rouard@cgiar.org

Valentin GUIGNON (Bioversity), v.guignon@cgiar.org

Matthieu CONTE (Syngenta)

Sebastien BRIOIS (Syngenta)

=head1 VERSION

Version 1.0.0

Date 20/02/2013

=head1 SEE ALSO

GreenPhyl documentation.

=cut
