#!/usr/bin/perl

=pod

=head1 NAME

annotations.pl - Performs annotation services

=head1 SYNOPSIS

    annotations.pl?

=head1 REQUIRES

Perl5

=head1 DESCRIPTION

Performs several annotation services.

=cut

use strict;
use warnings;

use lib '../lib';
use lib '../local_lib';
use Carp qw (cluck confess croak);

use Greenphyl;
use Greenphyl::Web;
use Greenphyl::Web::PseudoService;
use Greenphyl::Tools::Annotations;




# Script global constants
##########################

=pod

=head1 CONSTANTS

B<$ANNOTATION_SERVICES>: (hash ref)

Contains the annotation service handlers.

=cut

our $DEBUG = 0;
our $ANNOTATION_SERVICES =
    {
        'get_annotations' => \&Greenphyl::Tools::Annotations::GetAnnotationList,
    };




# Script options
#################

=pod

=head1 OPTIONS

    annotations.pl <service=<service_name>> ...<service parameters>

=head2 Parameters

=over 4

=item B<service> (string):

name of the service.

=back

=cut


# CODE START
#############

HandleService($ANNOTATION_SERVICES);

exit(0);

# CODE END
###########


=pod

=head1 AUTHORS

Mathieu ROUARD (Bioversity), m.rouard@cgiar.org

Valentin GUIGNON (Bioversity), v.guignon@cgiar.org

Matthieu CONTE (Syngenta)

=head1 VERSION

Version 1.0.0

Date 30/05/2012

=head1 SEE ALSO

GreenPhyl documentation.

=cut
