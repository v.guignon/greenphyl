#!/usr/bin/perl
=pod

=head1 NAME

quick_search.pl - Perform quick search

=head1 SYNOPSIS

    quick_search.pl

=head1 REQUIRES

Perl5

=head1 DESCRIPTION

Quick Search engine.

=cut

use strict;
use warnings;

use lib '../lib';
use lib '../local_lib';
use Carp qw (cluck confess croak);


use CGI qw/:standard url/;
use URI::Escape;
# use CGI::Carp qw(fatalsToBrowser);    # useful to debug

use Greenphyl;
use Greenphyl::Web;
use Greenphyl::Web::PseudoService;
use Greenphyl::Web::Templates;
use Greenphyl::IPR;
use Greenphyl::Sequence;
use Greenphyl::Tools::Sequences;
use Greenphyl::CustomSequence;
use Greenphyl::Family;
use Greenphyl::CustomFamily;
use Greenphyl::SourceDatabase;
use Greenphyl::DBXRef;
use Greenphyl::CompositeObject;




# Script global constants
##########################

=pod

=head1 CONSTANTS

B<$DEBUG>: (boolean)

Enables debug mode.


B<$QUICK_SEARCH_SERVICES>: (hash ref)

Contains the quick search service handlers.

B<$IPR_MIN_THRESHOLD>: (integer)

Requiered percent (0-100) of sequence of a family that must have an IPR domain
in order to display that family in the result list.

=cut

our $DEBUG = 0;
our $QUICK_SEARCH_SERVICES =
    {
        'annotation_seq' => \&searchAnnotation,
        'family_id'      => \&searchFamilyAccession,
        'family_name'    => \&searchFamilyName,
        'go_fam'         => \&searchFamilyGO,
        'go_seq'         => \&searchSequenceGO,
        'interpro_fam'   => \&searchFamilyInterpro,
        'interpro_seq'   => \&searchSequenceInterpro,
        'kegg_seq'       => \&searchKEGG,
        'ec_seq'         => \&searchEC,
        'sequence_id'    => \&searchSequenceID,
        'uniprot_seq'    => \&searchUniProt,
        'pubmed_seq'     => \&searchPubMed,
    };

my %SEARCH_TYPES = (
    'annotation_seq' => {
        'label'    => 'Annotation',
        'function' => 'Annotation',
        'template' => 'annotation.tt',
    },
    'family_id'    => {
        'label'    => 'Family ID',
        'function' => 'FamilyAccession',
        'template' => 'family_id.tt',
    },
    'family_name'  => {
        'label'    => 'Family Name',
        'function' => 'FamilyName',
        'template' => 'family_name.tt',
    },
    'go_fam'       => {
        'label'    => 'Family GO',
        'function' => 'FamilyGO',
        'template' => 'go_family.tt',
    },
    'go_seq'       => {
        'label'    => 'Sequence GO',
        'function' => 'SequenceGO',
        'template' => 'go_sequences.tt',
    },
    'interpro_fam' => {
        'label'    => 'Family InterPro Domains',
        'function' => 'FamilyInterpro',
        'template' => 'interpro_fam.tt',
    },
    'interpro_seq' => {
        'label'    => 'Sequence InterPro Domains',
        'function' => 'SequenceInterpro',
        'template' => 'interpro_seq.tt',
    },
    'kegg_seq'     => {
        'label'    => 'KEGG',
        'function' => 'KEGG',
        'template' => 'kegg.tt',
    },
    'ec_seq'       => {
        'label'    => 'EC',
        'function' => 'EC',
        'template' => 'ec.tt',
    },
    'sequence_id'  => {
        'label'    => 'Sequence ID',
        'function' => 'SequenceID',
        'template' => 'sequence_id.tt',
    },
    'uniprot_seq'  => {
        'label'    => 'UniProt',
        'function' => 'UniProt',
        'template' => 'uniprot.tt',
    },
    'pubmed_seq'   => {
        'label'    => 'PubMed References',
        'function' => 'PubMed',
        'template' => 'pubmed.tt',
    },
);

my $IPR_MIN_THRESHOLD = 20;
our $SEARCH_MAX_SUBQUERY_RESULTS = 50;
our $IPR_SOURCE     = 'ipr';
our $UNIPROT_SOURCE = 'uniprot';



# Script global variables
##########################

=pod

=head1 VARIABLES

B<$g_dbh>: (DBI::db)

Global database handler.

=cut

my $g_dbh = GetDatabaseHandler();




# Script global functions
##########################

=pod

=head1 FUNCTIONS

=head2 getSearchParameters

B<Description>: Returns a hash of search parameters.

B<ArgsCount>: 0

B<Return>: (hash ref)

Hash of search parameters with the following keys:
-search_type: area (type) of search
-search_text: text to search
-deep_search: tells if deppe search is enabled
-mode: page mode
-mmode: main page mode

=cut

sub getSearchParameters
{
    # check mode
    my $mode = GetPageMode();

    # check for exec/command-line call
    if (@ARGV)
    {
        # set inner mode
        $mode = $Greenphyl::Web::PAGE_MODE_INNER;
        PrintDebug('Quick Search command line mode');
    }

    my $search_type   = GetParameterValues('service')  || '';
    my $qsearch_input = GetParameterValues('qsearch_input') || '';
    my $qdeep_search  = GetParameterValues('qdeep_search')  || '';
    my $mmode         = GetParameterValues('mmode')  || '';

    PrintDebug("Search type: $search_type\nInput: $qsearch_input\nDeep search: $qdeep_search");

    my $parameters =
        {
            'search_type' => $search_type,
            'search_text' => $qsearch_input,
            'deep_search' => $qdeep_search,
            'mode'        => $mode,
            'mmode'       => $mmode,
        };

    return $parameters;
}


=pod

=head2 preRenderFamily

B<Description>: pre-render a family.

B<ArgsCount>: 1

=over 4

=item $family: (Greenphyl::Family)

A family object.

=back

=cut

sub preRenderFamily
{
    my ($family) = @_;

    my $family_label = ProcessTemplate(
        'families/family_id.tt',
        {
            'family' => $family,
        }
    );
    # my $analyzes = ProcessTemplate(
    #     'families/family_analyzes.tt',
    #     {
    #         'family' => $family,
    #     }
    # );

    return {
        'label' => $family_label,
        # 'analyzes' => $analyzes,
    };
}


=pod

=head2 preRenderDBXRef

B<Description>: pre-render a dbxref.

B<ArgsCount>: 1

=over 4

=item $dbxref: (Greenphyl::DBXRef)

A dbxref object.

=back

=cut

sub preRenderDBXRef
{
    my ($dbxref) = @_;

    my $dbxref_label = ProcessTemplate(
        'dbxref/dbxref_accession.tt',
        {
            'dbxref' => $dbxref,
        }
    );

    return {
        'label' => $dbxref_label,
    };
}


=pod

=head2 preRenderIPR

B<Description>: pre-render an IPR code.

B<ArgsCount>: 1

=over 4

=item $ipr: (Greenphyl::IPR)

A IPR object.

=back

=cut

sub preRenderIPR
{
    my ($ipr) = @_;

    my $ipr_label = ProcessTemplate(
        'ipr/ipr_code.tt',
        {
            'ipr' => $ipr,
        }
    );

    return {
        'label' => $ipr_label,
    };
}


=pod

=head2 getMatchingIPR

B<Description>: returns an array of IPR objects corresponding to the search.

B<ArgsCount>: 1-2

=over 4

=item $search_text: (string)

string to search.

=item $deep_search: (boolean)

enables/disables deep search.

=back

B<Return>: (array ref)

an array containing matching IPR found (Greenphyl::IPR).

=cut

sub getMatchingIPR
{
    my $parameters = getSearchParameters();
    my $search_text = $parameters->{'search_text'};
    my $deep_search = $parameters->{'deep_search'};

    my $selectors = {'code' => ['LIKE', "$search_text%"]};
    # check if search should also be performed on IPR description
    if ($deep_search)
    {
        $selectors = [$selectors, 'OR', {'description' => ['REGEXP', "(^|.*[^[:alnum:]])" . EscapeSQLRegExp($search_text) . "([^[:alnum:]].*|\$)"]}];
    }

    # get related IPRs
    my @iprs = Greenphyl::IPR->new($g_dbh, {'selectors' => $selectors, 'sql' => {'LIMIT' => GP('SEARCH_MAX_RESULTS'), 'ORDER BY' => 'code'}});

    return \@iprs;
}


=pod

=head2 getMatchingGO

B<Description>: returns an array of GO objects corresponding to the search.

B<ArgsCount>: 1-2

=over 4

=item $search_text: (string)

string to search.

=item $deep_search: (boolean)

enables/disables deep search.

=back

B<Return>: (array ref)

an array containing matching GO found (Greenphyl::GO).

=cut

sub getMatchingGO
{
    my $parameters = getSearchParameters();
    my $search_text = $parameters->{'search_text'};
    my $deep_search = $parameters->{'deep_search'};

    my $selectors = {'code' => ['LIKE', "$search_text%"]};
    if ($deep_search)
    {
        $selectors = [$selectors, 'OR', {'name' => ['REGEXP', "(^|.*[^[:alnum:]])" . EscapeSQLRegExp($search_text) . "([^[:alnum:]].*|\$)"]}];
    }

    # get related GOs
    my $gos = [Greenphyl::GO->new($g_dbh, {'selectors' => $selectors, 'sql' => {'LIMIT' => GP('SEARCH_MAX_RESULTS'), 'ORDER BY' => 'code'}})];

    #+FIXME: take in account alternates and synonyms

    return $gos;
}


=pod

=head2 extractFamilyGOSources

B<Description>: returns a hash containg family sources.

B<ArgsCount>: 1

=over 4

=item $family_data: (hash ref)

family data:
$IPR_SOURCE (string): a coma-separated list of source IPR IDs
$UNIPROT_SOURCE (string): a coma-separated list of source UniProt (DBXRef) IDs
'sequence_count' (integer): number of sequences matching the GO
'family' (Greenphyl::Family): family object

=back

B<Return>: (hash ref)

$IPR_SOURCE (array ref): a list of Greenphyl::IPR
$UNIPROT_SOURCE (array ref): a list of Greenphyl::DBXRef
'sequence_count' (integer): number of sequences matching the GO
'sequence_percent' (integer): percent (0-100) of sequences matching the GO

=cut

sub extractFamilyGOSources
{
    my ($family_data) = @_;

    # get sources
    my $family_stats = {$IPR_SOURCE => [], $UNIPROT_SOURCE => [], };
    if ($family_data->{$IPR_SOURCE})
    {
        push(
            @{$family_stats->{$IPR_SOURCE}},
            Greenphyl::IPR->new($g_dbh, {'selectors' => { 'id' => ['IN', split(',', $family_data->{$IPR_SOURCE})], }})
        );
    }
    if ($family_data->{$UNIPROT_SOURCE})
    {
        push(
            @{$family_stats->{$UNIPROT_SOURCE}},
            Greenphyl::DBXRef->new($g_dbh, {'selectors' => { 'id' => ['IN', split(',', $family_data->{$UNIPROT_SOURCE})], }})
        );
    }
    $family_stats->{'sequence_count'} = $family_data->{'sequence_count'};
    $family_stats->{'sequence_percent'} = int(0.5 + 100 * $family_data->{'sequence_count'} / $family_data->{'family'}->sequence_count);

    return $family_stats;
}


=pod

=head2 searchDBXRef

B<Description>: Returns the results for a search related to sequence DBXRef.

B<ArgsCount>: 2

=over 4

=item $parameters: (hash ref)

Hash of search parameters.

=item $db_name: (string)

Name of the source database (DBXRef database).

=back

B<Return>: (hash ref)

A hash containing the results.

=cut

sub searchDBXRef
{
    my ($parameters, $db_name) = @_;

    my $search_text = $parameters->{'search_text'};
    my $deep_search = $parameters->{'deep_search'};

    # get database source
    my $db = Greenphyl::SourceDatabase->new($g_dbh, {'selectors' => {'name' => $db_name}});

    my $composite;
    my $max_results_reached = 0;
    my %families;

    # make sure we found it
    if (!$db)
    {
        # not found, stop here
        PrintDebug("WARNING: '$db_name' database source not found in database!");
    }
    else
    {
        # default search field
        my $conditions = 'd.accession REGEXP ?';
        my @bindings   = ("(^|.*[^[:alnum:]])" . EscapeSQLRegExp($search_text) . "([^[:alnum:]].*|\$)");

        # if deep search, increase search field
        if ($deep_search)
        {
            $conditions = '(ds.synonym REGEXP ? OR d.accession REGEXP ? OR d.description REGEXP ?)';
            @bindings   = ("(^|.*[^[:alnum:]])" . EscapeSQLRegExp($search_text) . "([^[:alnum:]].*|\$)", "(^|.*[^[:alnum:]])" . EscapeSQLRegExp($search_text) . "([^[:alnum:]].*|\$)", "(^|.*[^[:alnum:]])" . EscapeSQLRegExp($search_text) . "([^[:alnum:]].*|\$)");
        }

        my $sql_query = "
            SELECT " . join(', ', Greenphyl::Family->GetDefaultMembers('f', 'family_')) . ",
                " . join(', ', Greenphyl::DBXRef->GetDefaultMembers('d', 'dbxref_')) . ",
                COUNT(DISTINCT fs.sequence_id) AS \"seq_count\"
            FROM dbxref d
                LEFT JOIN dbxref_synonyms ds ON (ds.dbxref_id = d.id)
                JOIN dbxref_sequences sd ON (sd.dbxref_id = d.id)
                JOIN families_sequences fs ON (fs.sequence_id = sd.sequence_id)
                JOIN families f ON (f.id = fs.family_id)
            WHERE d.db_id = " . $db->id . "
                  AND f.level = 1
                  AND $conditions
            GROUP BY f.id, d.id
            LIMIT " . GP('SEARCH_MAX_RESULTS') . ';';
        PrintDebug("SQL QUERY: $sql_query\nBindings: " . join(', ', @bindings));
        my $families_and_dbxref = $g_dbh->selectall_arrayref($sql_query, { 'Slice' => {} }, @bindings);

        # check if we got too many sequences
        if (@$families_and_dbxref >= GP('SEARCH_MAX_RESULTS'))
        {
            ++$max_results_reached;
        }

        # my %dbxrefs;
        foreach my $family_and_dbxref_hash (@$families_and_dbxref)
        {
            my $dbxref_hash = Greenphyl::DBXRef->ExtractMembersFromHash($family_and_dbxref_hash, 'dbxref_');
            my $dbxref      = Greenphyl::DBXRef->new($g_dbh, {'load' => 'none', 'members' => $dbxref_hash});

            my $family_hash = Greenphyl::Family->ExtractMembersFromHash($family_and_dbxref_hash, 'family_');
            my $family = Greenphyl::Family->new($g_dbh, {'load' => 'none', 'members' => $family_hash});

            if (exists($families{$family->id}))
            {
                my $dbxref_label = preRenderDBXRef($dbxref)->{'label'};
                $dbxref_label =~ s/[\r\n]+/ /g;
                $families{$family->id}->{'match_' . $parameters->{'search_type'}} .=
                    "\n" . $dbxref_label . ' (' . $family_and_dbxref_hash->{'seq_count'} . ' seq.)';
            }
            else
            {
                my $dbxref_label = preRenderDBXRef($dbxref)->{'label'};
                $dbxref_label =~ s/[\r\n]+/ /g;
                my $family_export_hash = preRenderFamily($family);
                $family_export_hash->{'match'}      = 'match_' . $parameters->{'search_type'};
                $family_export_hash->{'match_' . $parameters->{'search_type'}} =
                    $dbxref_label . ' (' . $family_and_dbxref_hash->{'seq_count'} . ' seq.)';
#                    $family_and_dbxref_hash->{'dbxref'} . ' (' . $family_and_dbxref_hash->{'seq_count'} . ' seq.)';
                $family_export_hash->{'class'} = $family->getObjectType();
                $family_export_hash->{'class'} =~ s/^Cached//;

                $families{$family->id} = Greenphyl::CompositeObject->new(
                    $family_export_hash,
                    $family->getHashValue()
                );
            }

            # my $dbxref_hash = Greenphyl::DBXRef->ExtractMembersFromHash($family_and_dbxref_hash, 'dbxref_');
            # my $dbxref    = Greenphyl::DBXRef->new($g_dbh, {'load' => 'none', 'members' => $dbxref_hash});
            # $dbxrefs{''.$sequence} = {'sequence' => $sequence, 'dbxref' => $dbxref};
        }
        # $composite = Greenphyl::CompositeObject->new(
        #     {
        #         'sequences' => \@sequences,
        #         'dbxrefs'   => \%dbxrefs,
        #     }
        # );
    }

    my @families = sort
        {
            ($b->{'class'} cmp $a->{'class'})
            || ($a->{'id'} cmp $b->{'id'})
        }
        values(%families)
    ;

    my $status_object = Greenphyl::CompositeObject->new(
        {
            'id' => -1,
            'class' => 'status',
            'search_text' => $search_text,
            'too_many_results' => $max_results_reached,
        }
    );
    unshift(@families, $status_object);
    return (\@families, {'columns' => { '1.' => 'accession', '2.' => 'name', '3.' => 'sequence_count', '4.' => 'validated', '5.' => 'hasAnalyzes', '6.' => 'match_' . $parameters->{'search_type'}, }});
    # return { 'results' => $composite, 'search_text' => $search_text, 'too_many_results' => $max_results_reached };
}


=pod

=head2 search*

B<Description>: search a given string in specific area of the database depending
on the function name.

B<ArgsCount>: 2

=over 4

=item $search_text: (string)

string to search.

=item $deep_search: (boolean)

Enables/disables deep search.

=back

B<Return>: (hash ref)

a hash containing search function results.

=cut

sub searchSequenceInterpro
{
    my $parameters = getSearchParameters();
    my $search_text = $parameters->{'search_text'};
    my $deep_search = $parameters->{'deep_search'};

    my $max_results_reached = 0;

    my $iprs = getMatchingIPR($search_text, $deep_search);

    # check if we got too many IPR
    if (@$iprs >= GP('SEARCH_MAX_RESULTS'))
    {
        $max_results_reached = 1;
    }

    my %families;
    foreach my $ipr (@$iprs)
    {
        # Commented because too slow: COUNT(fs.sequence_id) AS \"seq_count\"
        my $sql_query = "
            SELECT DISTINCT " . join(', ', Greenphyl::Family->GetDefaultMembers('f')) . "
            FROM
                families f
                JOIN families_sequences fs ON (fs.family_id = f.id)
                JOIN ipr_sequences sha ON (sha.sequence_id = fs.sequence_id)
            WHERE
                f.level = 1
                AND sha.ipr_id = ?
            GROUP BY f.id
            LIMIT " . $SEARCH_MAX_SUBQUERY_RESULTS . ";";
        PrintDebug("SQL QUERY: $sql_query\nBindings: " . $ipr->id);
        my $families = $g_dbh->selectall_arrayref($sql_query, { 'Slice' => {} }, $ipr->id);

        # check if we got too many families for a single IPR
        if (@$families >= $SEARCH_MAX_SUBQUERY_RESULTS)
        {
            ++$max_results_reached;
        }

        foreach my $family_hash (@$families)
        {
            my $family = Greenphyl::Family->new($g_dbh, {'load' => 'none', 'members' => $family_hash});
            my $ipr_label = preRenderIPR($ipr)->{'label'};
            $ipr_label =~ s/[\r\n]+/ /g;
            # $ipr_label .= ' (' . $family_hash->{'seq_count'} . ' seq.)';
            if (exists($families{$family->id}))
            {
                $families{$family->id}->{'match_interpro_seq'} .= "\n$ipr_label";
            }
            else
            {
                my $family_export_hash = preRenderFamily($family);
                $family_export_hash->{'match'}         = 'match_interpro_seq';
                $family_export_hash->{'match_interpro_seq'} = $ipr_label;
                $family_export_hash->{'class'} = $family->getObjectType();
                $family_export_hash->{'class'} =~ s/^Cached//;;
                $families{$family->id} = Greenphyl::CompositeObject->new(
                    $family_export_hash,
                    $family->getHashValue()
                );
            }
        }
    }

    my @families = sort
        {
            ($b->{'class'} cmp $a->{'class'})
            || ($a->{'id'} cmp $b->{'id'})
        }
        values(%families)
    ;

    my $status_object = Greenphyl::CompositeObject->new(
        {
            'id' => -1,
            'class' => 'status',
            'search_text' => $parameters->{'search_text'},
            'too_many_results' => $max_results_reached,
        }
    );
    unshift(@families, $status_object);
    return (\@families, {'columns' => { '1.' => 'accession', '2.' => 'name', '3.' => 'sequence_count', '4.' => 'validated', '5.' => 'hasAnalyzes', '6.' => 'match_interpro_seq', }});
}

sub searchFamilyInterpro
{
    my $parameters = getSearchParameters();
    my $search_text = $parameters->{'search_text'};
    my $deep_search = $parameters->{'deep_search'};

    my $max_results_reached = 0;

    my $iprs = getMatchingIPR($search_text, $deep_search);

    # check if we got too many IPR
    if (@$iprs >= GP('SEARCH_MAX_RESULTS'))
    {
        ++$max_results_reached;
    }

    my %families;
    foreach my $ipr (@$iprs)
    {
        my $sql_query = "
            SELECT DISTINCT " . join(', ', Greenphyl::Family->GetDefaultMembers('f')) . ",
                   st.percent AS \"percent\"
            FROM
                families f
                JOIN families_ipr_cache st ON (f.id = st.family_id)
            WHERE
                f.level = 1
                AND st.ipr_id = ?
                AND st.percent >= $IPR_MIN_THRESHOLD
            LIMIT " . GP('SEARCH_MAX_RESULTS') . ";";
        PrintDebug("SQL QUERY: $sql_query\nBindings: " . $ipr->id);
        my $families = $g_dbh->selectall_arrayref($sql_query, { Slice => {} }, $ipr->id);

        # check if we got too many families for a single IPR
        if (@$families >= GP('SEARCH_MAX_RESULTS'))
        {
            ++$max_results_reached;
        }

        foreach my $family_hash (@$families)
        {
            my $family = Greenphyl::Family->new($g_dbh, {'load' => 'none', 'members' => $family_hash});
            my $ipr_label = preRenderIPR($ipr)->{'label'};
            $ipr_label =~ s/[\r\n]+/ /g;
            $ipr_label .= ' (' . $family_hash->{'percent'} . '%)';
            if (exists($families{$family->id}))
            {
                $families{$family->id}->{'match_interpro_fam'} .= "\n$ipr_label";
            }
            else
            {
                my $family_export_hash = preRenderFamily($family);
                $family_export_hash->{'match'}         = 'match_interpro_fam';
                $family_export_hash->{'match_interpro_fam'} = $ipr_label;
                $family_export_hash->{'class'} = $family->getObjectType();
                $family_export_hash->{'class'} =~ s/^Cached//;
                $families{$family->id} = Greenphyl::CompositeObject->new(
                    $family_export_hash,
                    $family->getHashValue()
                );
            }
        }
    }

    my @families = sort
        {
            ($b->{'class'} cmp $a->{'class'})
            || ($a->{'id'} cmp $b->{'id'})
        }
        values(%families)
    ;

    my $status_object = Greenphyl::CompositeObject->new(
        {
            'id' => -1,
            'class' => 'status',
            'search_text' => $parameters->{'search_text'},
            'too_many_results' => $max_results_reached,
        }
    );
    unshift(@families, $status_object);
    return (\@families, {'columns' => { '1.' => 'accession', '2.' => 'name', '3.' => 'sequence_count', '4.' => 'validated', '5.' => 'hasAnalyzes', '6.' => 'match_interpro_fam', }});
}


sub searchUniProt
{
    my $parameters = getSearchParameters();

    return searchDBXRef($parameters, $Greenphyl::SourceDatabase::SOURCE_DATABASE_UNIPROT);
}


sub searchPubMed
{
    my $parameters = getSearchParameters();
    # remove PMID: for accession search
    $parameters->{'search_text'} =~ s/^\s*PMID://i;
    $parameters->{'deep_search'} = 1;

    # always use deep search for PubMed title
    return searchDBXRef($parameters, $Greenphyl::SourceDatabase::SOURCE_DATABASE_PUBMED);
}


sub searchKEGG
{
    my $parameters = getSearchParameters();

    return searchDBXRef($parameters, $Greenphyl::SourceDatabase::SOURCE_DATABASE_KEGG);
}


sub searchEC
{
    my $parameters = getSearchParameters();
    my $search_text = $parameters->{'search_text'};
    my $deep_search = $parameters->{'deep_search'};

    # get database source
    my $db = Greenphyl::SourceDatabase->new($g_dbh, {'selectors' => {'name' => $Greenphyl::SourceDatabase::SOURCE_DATABASE_EC}});

    my $composite;
    my $max_results_reached = 0;

    # make sure we found it
    if (!$db)
    {
        # not found, stop here
        PrintDebug("WARNING: 'EC' database source not found in database!");
    }
    else
    {
        # default search field
        my $conditions = 'd.accession REGEXP ?';
        my @bindings   = ("(^|.*[^[:alnum:]])" . EscapeSQLRegExp($search_text) . "([^[:alnum:]].*|\$)");

        # if deep search, increase search field
        if ($deep_search)
        {
            $conditions = '(ds.synonym REGEXP ? OR d.accession REGEXP ? OR d.description REGEXP ?)';
            @bindings   = ("(^|.*[^[:alnum:]])" . EscapeSQLRegExp($search_text) . "([^[:alnum:]].*|\$)", "(^|.*[^[:alnum:]])" . EscapeSQLRegExp($search_text) . "([^[:alnum:]].*|\$)", "(^|.*[^[:alnum:]])" . EscapeSQLRegExp($search_text) . "([^[:alnum:]].*|\$)");
        }

        # we use an SQL query as we do not have EC-sequence links into dbxref_sequences table
        my $sql_query = "
            SELECT DISTINCT " . join(', ', Greenphyl::Sequence->GetDefaultMembers('s', 'sequence_')) . ",
                            s.annotation AS \"sequence_annotation\",
                            " . join(', ', Greenphyl::DBXRef->GetDefaultMembers('d', 'dbxref_')) . "
            FROM dbxref d
                LEFT JOIN dbxref_synonyms ds ON (ds.dbxref_id = d.id)
                JOIN go_sequences_cache gsc ON (gsc.ec_id = d.id)
                JOIN sequences s ON (s.id = gsc.sequence_id)
            WHERE d.db_id = " . $db->id . "
                  AND $conditions
            LIMIT " . GP('SEARCH_MAX_RESULTS') . ';';
        PrintDebug("SQL QUERY: $sql_query\nBindings: " . join(', ', @bindings));
        my $sequences_and_dbxref = $g_dbh->selectall_arrayref($sql_query, { 'Slice' => {} }, @bindings);

        # check if we got too many sequences
        if (@$sequences_and_dbxref >= GP('SEARCH_MAX_RESULTS'))
        {
            $max_results_reached = 1;
        }

        my @sequences = ();
        my %dbxrefs = ();
        foreach my $sequence_and_dbxref_hash (@$sequences_and_dbxref)
        {
            my $sequence_hash = Greenphyl::Sequence->ExtractMembersFromHash($sequence_and_dbxref_hash, 'sequence_');
            my $sequence = Greenphyl::Sequence->new($g_dbh, {'load' => 'none', 'members' => $sequence_hash});
            push(@sequences, $sequence);
            my $dbxref_hash = Greenphyl::DBXRef->ExtractMembersFromHash($sequence_and_dbxref_hash, 'dbxref_');
            my $dbxref = Greenphyl::DBXRef->new($g_dbh, {'load' => 'none', 'members' => $dbxref_hash});
            $dbxrefs{''.$sequence} = {'sequence' => $sequence, 'dbxref' => $dbxref};
        }
        $composite = Greenphyl::CompositeObject->new(
            {
                'sequences' => \@sequences,
                'dbxrefs'   => \%dbxrefs,
            }
        );
    }

    return { 'results' => $composite, 'search_text' => $search_text, 'too_many_results' => $max_results_reached };
}


# takes a GO ID as input
sub searchFamilyGO
{
    my $parameters = getSearchParameters();
    my $search_text = $parameters->{'search_text'};
    my $deep_search = $parameters->{'deep_search'};

    my $max_results_reached = 0;

    my $gos = getMatchingGO($search_text, $deep_search);

    # check if we got too many GO
    if (@$gos >= GP('SEARCH_MAX_RESULTS'))
    {
        ++$max_results_reached;
    }

    my @composites;
    foreach my $go (@$gos)
    {
        # get related GO (from lineage)
        my $related_gos = $go->descendants();

        my $sql_query = "
            SELECT
                " . join(', ', Greenphyl::Family->GetDefaultMembers('f', 'family_')) . ",
                GROUP_CONCAT(gsc.ipr_id) AS \"$IPR_SOURCE\",
                GROUP_CONCAT(gsc.uniprot_id) AS \"$UNIPROT_SOURCE\",
                COUNT(gsc.sequence_id) AS \"sequence_count\"
            FROM
                families f
                  JOIN families_sequences fs ON (f.id = fs.family_id)
                  JOIN go_sequences_cache gsc ON (fs.sequence_id = gsc.sequence_id)
            WHERE
                f.level = 1
                AND gsc.go_id IN (?" . (', ?' x scalar(@$related_gos)) . ")
            GROUP BY f.id
            LIMIT " . GP('SEARCH_MAX_RESULTS') . ';';
        PrintDebug("SQL QUERY: $sql_query\nBindings: " . $go->id . ', ' . join(',', @$related_gos));
        my $family_sql_stats = $g_dbh->selectall_arrayref($sql_query, { Slice => {} }, $go->id, @$related_gos);

        # check if we got too many families for a single GO
        if (@$family_sql_stats >= GP('SEARCH_MAX_RESULTS'))
        {
            ++$max_results_reached;
        }

        my (@families, %family_stats);
        foreach my $family_data (@$family_sql_stats)
        {
            # get family data
            my $family_hash = Greenphyl::Family->ExtractMembersFromHash($family_data, 'family_');
            my $family = Greenphyl::Family->new($g_dbh, {'load' => 'none', 'members' => $family_hash});
            push(@families, $family);
            # get sources
            $family_data->{'family'} = $family;
            $family_stats{$family->id} = extractFamilyGOSources($family_data);
        }

        # sort by matches
        @families = sort { $family_stats{$b->id}->{'sequence_count'} <=> $family_stats{$a->id}->{'sequence_count'} } @families;

        my $composite = Greenphyl::CompositeObject->new(
            {
                'go'       => $go,
                'families' => \@families,
                'sources'  => \%family_stats,
            }
        );
        push(@composites, $composite);
    }

    my %families;
    foreach my $composite (@composites)
    {
        foreach my $family (@{$composite->{'families'}})
        {
            if (exists($families{$family->id}))
            {
                $families{$family->id}->{'match_go_fam'} .=
                    "\n"
                    . $composite->{'go'}->code
                    . ': '
                    . $composite->{'go'}->name
                    . '('
                    . $composite->{'sources'}->{$family->id}->{'sequence_count'}
                    . ' seq.)'
                ;
            }
            else
            {
                my $family_export_hash = preRenderFamily($family);
                # $family_export_hash->{'hasAnalyzes'}     = $family->hasAnalyzes;
                $family_export_hash->{'match'}           = 'match_go_fam';
                $family_export_hash->{'match_go_fam'} =
                    $composite->{'go'}->code
                    . ': '
                    . $composite->{'go'}->name
                    . '('
                    . $composite->{'sources'}->{$family->id}->{'sequence_count'}
                    . ' seq.)'
                ;
                $family_export_hash->{'class'} = $family->getObjectType();
                $family_export_hash->{'class'} =~ s/^Cached//;

                $families{$family->id} = Greenphyl::CompositeObject->new(
                    $family_export_hash,
                    $family->getHashValue()
                );
            }
        }
    }

    my @families = sort
        {
            ($b->{'class'} cmp $a->{'class'})
            || ($a->{'id'} cmp $b->{'id'})
        }
        values(%families)
    ;

    my $status_object = Greenphyl::CompositeObject->new(
        {
            'id' => -1,
            'class' => 'status',
            'search_text' => $parameters->{'search_text'},
            'too_many_results' => $max_results_reached,
        }
    );
    unshift(@families, $status_object);
    return (\@families, {'columns' => { '1.' => 'accession', '2.' => 'name', '3.' => 'sequence_count', '4.' => 'validated', '5.' => 'hasAnalyzes', '6.' => 'match_go_fam', }});
}

sub searchSequenceGO
{
    my $parameters = getSearchParameters();
    my $search_text = $parameters->{'search_text'};
    my $deep_search = $parameters->{'deep_search'};

    my $max_results_reached = 0;
    my $result_count = 0;

    my $gos = getMatchingGO($search_text, $deep_search);

    # check if we got too many GO
    if (@$gos >= GP('SEARCH_MAX_RESULTS'))
    {
        ++$max_results_reached;
    }

    my $IPR_SOURCE     = 'ipr';
    my $UNIPROT_SOURCE = 'uniprot';

    my $composites;
GO_SEQ_LOOP:
    foreach my $go (@$gos)
    {
        my $sequences = {};
        my $sequence_stats = {};

        #+FIXME: take in account descendant GOs
        eval
        {
            # get IPR sources
            my $ipr_sources = $go->fetchIPR({'sql' => {'LIMIT' => $SEARCH_MAX_SUBQUERY_RESULTS}});
            foreach my $ipr (@$ipr_sources)
            {
                # get associated sequences
                my $ipr_sequences = $ipr->fetchSequences({'sql' => {'LIMIT' => $SEARCH_MAX_SUBQUERY_RESULTS}});
                if (@$ipr_sequences >= $SEARCH_MAX_SUBQUERY_RESULTS)
                {
                    ++$max_results_reached;
                }
                foreach my $sequence (@$ipr_sequences)
                {
                    # initialize sequence stats hash if not initialized for this sequence
                    $sequence_stats->{$sequence->id} ||= {$IPR_SOURCE => [], $UNIPROT_SOURCE => [],};
                    my $current_sequence_stats = $sequence_stats->{$sequence->id};
                    # store sequence
                    $sequences->{$sequence->id} = $sequence;
                    # store stats
                    push(@{$current_sequence_stats->{$IPR_SOURCE}}, $ipr);
                }
                if ($SEARCH_MAX_SUBQUERY_RESULTS <= @$ipr_sources)
                {
                    ++$max_results_reached;
                    # Exit eval.
                    die;
                }
            }

            # get UniProt sources
            my $uniprot_sources = $go->fetchUniprot({'sql' => {'LIMIT' => $SEARCH_MAX_SUBQUERY_RESULTS}});
            foreach my $uniprot (@$uniprot_sources)
            {
                # get associated sequences
                my $uniprot_sequences = $uniprot->fetchSequences({'sql' => {'LIMIT' => $SEARCH_MAX_SUBQUERY_RESULTS}});
                if (@$uniprot_sequences >= $SEARCH_MAX_SUBQUERY_RESULTS)
                {
                    ++$max_results_reached;
                }
                foreach my $sequence (@$uniprot_sequences)
                {
                    # initialize sequence stats hash if not initialized for this sequence
                    $sequence_stats->{$sequence->id} ||= {$IPR_SOURCE => [], $UNIPROT_SOURCE => [],};
                    my $current_sequence_stats = $sequence_stats->{$sequence->id};
                    # store sequence
                    $sequences->{$sequence->id} = $sequence;
                    # store stats
                    push(@{$current_sequence_stats->{$UNIPROT_SOURCE}}, $uniprot);
                }
                if ($SEARCH_MAX_SUBQUERY_RESULTS <= @$uniprot_sources)
                {
                    ++$max_results_reached;
                    # Exit eval.
                    die;
                }
            }

        };

        my $composite = Greenphyl::CompositeObject->new(
            {
                'go'        => $go,
                'sequences' => [sort {$a->accession cmp $b->accession} values(%$sequences)],
                'sources'   => $sequence_stats,
            }
        );
        push(@$composites, $composite);
        $result_count += scalar(keys(%$sequences));

        if (GP('SEARCH_MAX_RESULTS') <= $result_count)
        {
            $max_results_reached = 1;
            last GO_SEQ_LOOP;
        }
    }

    # if at least one GO had matching sequences, only keep GO with matching sequences
    if ($result_count)
    {
        my @non_empty_composites;
        foreach my $composite (@$composites)
        {
            if ($composite->{'sequences'} && @{$composite->{'sequences'}})
            {
                push(@non_empty_composites, $composite);
            }
        }
        $composites = \@non_empty_composites;
    }

    my %families;
    foreach my $composite (@$composites)
    {
        foreach my $sequence (@{$composite->{'sequences'}})
        {
            my $families = $sequence->fetchFamilies({'selectors' => {'level' => 1}});
            if ($families && ('ARRAY' eq ref($families)) && (0 < scalar(@$families)))
            {
                foreach my $family (@$families)
                {
                    if (exists($families{$family->id}))
                    {
                        $families{$family->id}->{'match_go_seq_hash'}->{$composite->{'go'}->code . ': ' . $composite->{'go'}->name} ||= [];
                        push(@{$families{$family->id}->{'match_go_seq_hash'}->{
                            $composite->{'go'}->code . ': ' . $composite->{'go'}->name}},
                            $sequence->accession);
                    }
                    else
                    {
                        my $family_export_hash = preRenderFamily($family);
                        # $family_export_hash->{'hasAnalyzes'}     = $family->hasAnalyzes;
                        $family_export_hash->{'match'}           = 'match_go_seq';
                        $family_export_hash->{'match_go_seq'}    = '';
                        $family_export_hash->{'match_go_seq_hash'} = {
                            $composite->{'go'}->code . ': ' . $composite->{'go'}->name =>
                            [$sequence->accession]
                        };
                        $family_export_hash->{'class'} = $family->getObjectType();
                        $family_export_hash->{'class'} =~ s/^Cached//;

                        $families{$family->id} = Greenphyl::CompositeObject->new(
                            $family_export_hash,
                            $family->getHashValue()
                        );
                    }
                }
            }
        }
    }

    my @families = sort
        {
            ($b->{'class'} cmp $a->{'class'})
            || ($a->{'id'} cmp $b->{'id'})
        }
        values(%families)
    ;

    foreach my $family (@families)
    {
        foreach my $go_term (keys(%{$family->{'match_go_seq_hash'}}))
        {
            my $seq_count = scalar(@{$family->{'match_go_seq_hash'}->{$go_term}});
            $family->{'match_go_seq'} .=
              $go_term
              . '('
              . ($seq_count >= $SEARCH_MAX_SUBQUERY_RESULTS ? '>' . $seq_count : $seq_count)
              . " seq.)\n";
        }
    }

    my $status_object = Greenphyl::CompositeObject->new(
        {
            'id' => -1,
            'class' => 'status',
            'search_text' => $parameters->{'search_text'},
            'too_many_results' => $max_results_reached,
        }
    );
    unshift(@families, $status_object);
    return (\@families, {'columns' => { '1.' => 'accession', '2.' => 'name', '3.' => 'sequence_count', '4.' => 'validated', '5.' => 'hasAnalyzes', '6.' => 'match_go_seq', }});
    # return { 'results' => $composites, 'search_text' => $search_text, 'too_many_results' => $max_results_reached };
}

sub searchSequenceID
{
    my $parameters = getSearchParameters();
    my $search_text = $parameters->{'search_text'};
    my $deep_search = $parameters->{'deep_search'};
    my $max_results_reached = 0;

    # get related sequences
    my @accession_regexp = @{GetAccessionsFromSynonyms([$search_text], undef, 1)};
    push(@accession_regexp, '(^|.*[^[:alnum:]])' . EscapeSQLRegExp($search_text) . '([^[:alnum:]].*|$)');

    my @sequences = Greenphyl::Sequence->new($g_dbh, {'selectors' => {'accession' => ['REGEXP', join('|', @accession_regexp)]}, 'sql' => {'LIMIT' => GP('SEARCH_MAX_RESULTS'), 'DISTINCT' => 1,}});
    PrintDebug("GOT: " . @sequences . ", " . join(', ', @sequences));
    if (GP('SEARCH_MAX_RESULTS') <= scalar(@sequences))
    {
        ++$max_results_reached;
    }

    # # deep search? include custom sequences
    # if ($deep_search)
    # {
    #     my @custom_sequences = (Greenphyl::CustomSequence->new($g_dbh, {'selectors' => {'accession' => ['REGEXP', '.*' . EscapeSQLRegExp($search_text) . '.*']}, 'sql' => {'LIMIT' => $SEARCH_MAX_SUBQUERY_RESULTS, 'DISTINCT' => 1,}}));
    #     push(@sequences, @custom_sequences);
    #     if ($SEARCH_MAX_SUBQUERY_RESULTS <= scalar(@custom_sequences))
    #     {
    #         ++$max_results_reached;
    #     }
    # }

    my %families;
    foreach my $sequence (@sequences)
    {
        my $families = $sequence->fetchFamilies({'selectors' => {'level' => 1}});
        if ($families && ('ARRAY' eq ref($families)) && (0 < scalar(@$families)))
        {
            foreach my $family (@$families)
            {
                if (exists($families{$family->id}))
                {
                    $families{$family->id}->{'match_sequence_id'} .= ", " . $sequence->accession;
                }
                else
                {
                    my $family_export_hash = preRenderFamily($family);
                    # $family_export_hash->{'hasAnalyzes'}     = $family->hasAnalyzes;
                    $family_export_hash->{'match'}           = 'match_sequence_id';
                    my $sequence_label = ProcessTemplate(
                        'sequences/sequence_id.tt',
                        {
                            'sequence' => $sequence,
                        }
                    );
                    #$family_export_hash->{'match_sequence_id'} = $sequence->accession;
                    $family_export_hash->{'match_sequence_id'} = $sequence_label;
                    $family_export_hash->{'class'} = $family->getObjectType();
                    $family_export_hash->{'class'} =~ s/^Cached//;

                    $families{$family->id} = Greenphyl::CompositeObject->new(
                        $family_export_hash,
                        $family->getHashValue()
                    );
                }
            }
        }
    }

    my @families = sort
        {
            ($b->{'class'} cmp $a->{'class'})
            || ($a->{'id'} cmp $b->{'id'})
        }
        values(%families)
    ;

    my $status_object = Greenphyl::CompositeObject->new(
        {
            'id' => -1,
            'class' => 'status',
            'search_text' => $parameters->{'search_text'},
            'too_many_results' => $max_results_reached,
        }
    );
    unshift(@families, $status_object);
    return (\@families, {'columns' => { '1.' => 'accession', '2.' => 'name', '3.' => 'sequence_count', '4.' => 'validated', '5.' => 'hasAnalyzes', '6.' => 'match_sequence_id', }});
}

sub searchFamilyName
{
    my $parameters = getSearchParameters();
    my $search_text = $parameters->{'search_text'};
    my $deep_search = $parameters->{'deep_search'};
    my $max_results_reached = 0;

    # get related families
    my $sql_query = "
        SELECT DISTINCT " . join(', ', Greenphyl::Family->GetDefaultMembers('f')) . ",
                        s.synonym AS \"synonym\"
        FROM families f
            LEFT JOIN family_synonyms s ON (s.family_id = f.id)
        WHERE f.level = 1
          AND (f.name REGEXP ?
              OR s.synonym REGEXP ?
              " . ($deep_search ? 'OR f.description REGEXP ?' : '') . ")
        LIMIT " . GP('SEARCH_MAX_RESULTS') . ';';
    my @bindings = ("(^|.*[^[:alnum:]])" . EscapeSQLRegExp($search_text) . "([^[:alnum:]].*|\$)", "(^|.*[^[:alnum:]])" . EscapeSQLRegExp($search_text) . "([^[:alnum:]].*|\$)");
    if ($deep_search)
    {
        push(@bindings, "(^|.*[^[:alnum:]])" . EscapeSQLRegExp($search_text) . "([^[:alnum:]].*|\$)");
    }
    PrintDebug("SQL QUERY: $sql_query\nBindings: '" . join("', '", @bindings) . "'");
    my $family_hashes = $g_dbh->selectall_arrayref($sql_query, { 'Slice' => {} }, @bindings);
    if (GP('SEARCH_MAX_RESULTS') <= scalar(@$family_hashes))
    {
        ++$max_results_reached;
    }

    my %families;
    foreach my $family_hash (@$family_hashes)
    {
        $families{$family_hash->{'id'}} ||= Greenphyl::Family->new($g_dbh, {'load' => 'none', 'members' => $family_hash});
    }

    # # search custom families?
    # if ($deep_search)
    # {
    #     $sql_query = "
    #         SELECT " . join(', ', Greenphyl::CustomFamily->GetDefaultMembers('cf')) . "
    #         FROM custom_families cf
    #         WHERE cf.name REGEXP ?
    #               OR cf.description REGEXP ?
    #         LIMIT " . GP('SEARCH_MAX_RESULTS') . ';';
    #     @bindings = ("(^|.*[^[:alnum:]])" . EscapeSQLRegExp($search_text) . "([^[:alnum:]].*|\$)", "(^|.*[^[:alnum:]])" . EscapeSQLRegExp($search_text) . "([^[:alnum:]].*|\$)");
    #     PrintDebug("SQL QUERY: $sql_query\nBindings: '" . join("', '", @bindings) . "'");
    #     my $custom_family_hashes = $g_dbh->selectall_arrayref($sql_query, { 'Slice' => {} }, @bindings);
    #     if (GP('SEARCH_MAX_RESULTS') <= scalar(@$custom_family_hashes))
    #     {
    #         ++$max_results_reached;
    #     }
    # 
    #     foreach my $custom_family_hash (@$custom_family_hashes)
    #     {
    #         $families{'cf' . $custom_family_hash->{'id'}} ||= Greenphyl::CustomFamily->new($g_dbh, {'load' => 'none', 'members' => $custom_family_hash});
    #     }
    # }

    my @families = sort
        {
            ($b->getObjectType cmp $a->getObjectType)
            || ($a->id cmp $b->id)
        }
        values(%families)
    ;
    @families = map
        {
            Greenphyl::CompositeObject->new(
                {
                    'match' => 'match_family_name',
                    'match_family_name' =>
                        $_->name
                        . ($_->description ? "\n" . $_->description : ''),
                },
                preRenderFamily($_),
                $_->getHashValue()
            )
        }
        @families
    ;

    my $status_object = Greenphyl::CompositeObject->new(
        {
            'id' => -1,
            'class' => 'status',
            'search_text' => $parameters->{'search_text'},
            'too_many_results' => $max_results_reached,
        }
    );
    unshift(@families, $status_object);

    return (
        \@families,
        {'columns' => {'columns' => { '1.' => 'accession', '2.' => 'name', '3.' => 'sequence_count', '4.' => 'validated', '5.' => 'hasAnalyzes', '6.' => 'match_family_name', }}}
    );

}

sub searchFamilyAccession
{
    my $parameters = getSearchParameters();
    my $max_results_reached = 0;

    # get related families
    my $search_text = $parameters->{'search_text'};
    my $accession_search_string;
    # check if the user entered a valid family accession
    if ($search_text =~ m/^GP\d{6}$/i)
    {
        # valid accession
        $accession_search_string = uc($search_text); # upper case it
    }
    elsif ($search_text =~ m/^\d{1,6}$/i)
    {
        # digits only, complete accession
        $accession_search_string = sprintf('GP%06s', $search_text);
    }
    elsif ($parameters->{'deep_search'} && ($search_text =~ m/^GP(\d{1,5})$/i))
    {
        # incomplete accession
        $accession_search_string = "GP%$1%";
    }
    else
    {
        # invalid accession, return an empty result
        # return { 'results' => [], 'search_text' => $search_text, 'too_many_results' => 0 };
        return ([], {'columns' => {} });
    }

    # my @families = Greenphyl::Family->new($g_dbh, {'selectors' => {'accession' => ['LIKE', $accession_search_string],}, 'sql' => {'LIMIT' => GP('SEARCH_MAX_RESULTS')}});
    my @families;
    foreach my $family (Greenphyl::Family->new($g_dbh, {'selectors' => {'accession' => ['LIKE', $accession_search_string],}, 'sql' => {'LIMIT' => GP('SEARCH_MAX_RESULTS')}}))
    {
        my $family_export_hash = preRenderFamily($family);
        # $family_export_hash->{'hasAnalyzes'}     = $family->hasAnalyzes;
        $family_export_hash->{'match'}           = 'match_family_id';
        $family_export_hash->{'match_family_id'} = $family->accession;
        push(
            @families,
            Greenphyl::CompositeObject->new(
                $family_export_hash,
                $family->getHashValue()
            )
        );
    }

    if (GP('SEARCH_MAX_RESULTS') <= scalar(@families))
    {
        ++$max_results_reached;
    }

    my $status_object = Greenphyl::CompositeObject->new(
        {
            'id' => -1,
            'class' => 'status',
            'search_text' => $parameters->{'search_text'},
            'too_many_results' => $max_results_reached,
        }
    );
    unshift(@families, $status_object);
    return (\@families, {'columns' => { '1.' => 'accession', '2.' => 'name', '3.' => 'sequence_count', '4.' => 'validated', '5.' => 'hasAnalyzes', '6.' => 'match_family_id', }});
}

sub searchAnnotation
{
    my $parameters = getSearchParameters();
    my $max_results_reached = 0;

    my @families;
    my $sql_query = '
        SELECT
            DISTINCT ' . join(', ', Greenphyl::Family->GetDefaultMembers('f')) . ',
            GROUP_CONCAT(s.accession, \': \', s.annotation SEPARATOR \'\\n\') AS "match_annotation_seq",
            \'match_annotation_seq\' AS "match"
        FROM
            sequences s
            JOIN families_sequences fs ON (fs.sequence_id = s.id)
            JOIN families f ON (f.id = fs.family_id)
        WHERE
            f.level = 1
            AND s.annotation REGEXP ?
        GROUP BY f.id
        LIMIT ' . GP('SEARCH_MAX_RESULTS') . '
        ;
    ';
    my $sql_families = $g_dbh->selectall_arrayref($sql_query, { 'Slice' => {} }, "(^|.*[^[:alnum:]])" . $parameters->{'search_text'} . "([^[:alnum:]].*|\$)")
        or confess 'ERROR: Failed to search annotations: ' . $g_dbh->errstr;
    foreach my $sql_family (@$sql_families)
    {
        my $family = Greenphyl::Family->new($g_dbh, {'load' => 'none', 'members' => $sql_family});
        my $family_export_hash = preRenderFamily($family);
        my $familycomp = Greenphyl::CompositeObject->new($sql_family, $family_export_hash, $family->getHashValue());
        push(@families, $familycomp);
    }

    if (GP('SEARCH_MAX_RESULTS') <= scalar(@families))
    {
        ++$max_results_reached;
    }

    my $status_object = Greenphyl::CompositeObject->new(
        {
            'id' => -1,
            'class' => 'status',
            'search_text' => $parameters->{'search_text'},
            'too_many_results' => $max_results_reached,
        }
    );
    unshift(@families, $status_object);
    return (\@families, {'columns' => { '1.' => 'accession', '2.' => 'name', '3.' => 'sequence_count', '4.' => 'validated', '5.' => 'hasAnalyzes', '6.' => 'match_annotation_seq', }});
}




# Script options
#################

=pod

=head1 OPTIONS

    quick_search.pl [mode=<inner|frame|full>]
        <service=QSEARCH_TYPE>
        <qsearch_input=QSEARCH_INPUT>

=head2 Parameters

=over 4

=item B<inner> (boolean):

Run Quick Search in "inner" mode which means no header nor footer will be
rendered (only inner HTML for Ajax calls).
Default: false

=item B<standalone> (boolean):

Run Quick Search in "standalone" mode which means full Greenphyl header
and footer will be rendered (with menu, logos and all that stuff).
Default: false

=item B<service> (string):

Type of search to perform. Must one of the keys of the %SEARCH_TYPES hash.

=item B<qsearch_input> (string):

Text to search in database.

=back

=cut


# CODE START
#############

HandleService($QUICK_SEARCH_SERVICES);

my $parameters = getSearchParameters();
HandleErrors($parameters);

exit(0);

# CODE END
###########


=pod

=head1 DIAGNOSTICS

=head1 AUTHORS

Mathieu ROUARD (Bioversity), m.rouard@cgiar.org

Valentin GUIGNON (Bioversity), v.guignon@cgiar.org

Matthieu CONTE (Syngenta)

=head1 VERSION

Version 2.0.0

Date 06/02/2020

=head1 SEE ALSO

GreenPhyl documentation.

=cut
