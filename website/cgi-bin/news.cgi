#!/usr/bin/perl

=pod

=head1 NAME

news.cgi - Display GreenPhyl news

=head1 SYNOPSIS

    news.cgi

=head1 REQUIRES

Perl5

=head1 DESCRIPTION

Display GreenPhyl news.

=cut

use strict;
use warnings;

use FindBin;
use lib "$FindBin::Bin/../lib";
use lib "$FindBin::Bin/../local_lib";
use Carp qw (cluck confess croak);

use Greenphyl;
use Greenphyl::Web;
use Greenphyl::Tools::News;




# Script global constants
##########################

=pod

=head1 CONSTANTS

B<$DEBUG>: (boolean)

When set to true, it enables debug mode.

=cut

our $DEBUG = 0;




# Script global functions
##########################

=pod

=head1 FUNCTIONS

=head2 RenderNews

B<Description>: Render GreenPhyl news.

B<ArgsCount>: 0

B<Return>: (string)

Returns an HTML string containing the GreenPhyl page for the news.

=cut

sub RenderNews
{
    # get news list from news directory
    my $news_parameters = GetNewsFilters();
    my $news_list = ListNews($news_parameters);

    return RenderHTMLFullPage(
        {
            'title'   => 'GreenPhyl Information',
            'content' => 'miscelaneous/news.tt',
            'news_list' => $news_list,
        },
    );
}




# Script options
#################

=pod

=head1 OPTIONS

    news.cgi

=head2 Parameters

=over 4

=item B<[option_name]> ([option nature]): #+++

[option description]. #+++
Default: [option default value if one] #+++

=back

=cut


# CODE START
#############

# hash for function selection.
my $actions =
    {
        '' => \&RenderNews,
    };

# try block
eval
{
    # display page
    print ProceedPage($actions);
};

HandleErrors();

exit(0);

__END__
# CODE END
###########

=pod

=head1 AUTHORS

Mathieu ROUARD (Bioversity), m.rouard@cgiar.org

Valentin GUIGNON (Bioversity), v.guignon@cgiar.org

Matthieu CONTE (Syngenta)

Sebastien BRIOIS (Syngenta)

=head1 VERSION

Version 1.0.0

Date 20/02/2014

=head1 SEE ALSO

GreenPhyl documentation.

=cut
