#!/usr/bin/perl

=pod

=head1 NAME

rdf.cgi - Export GreenPhyl data into RDF format

=head1 SYNOPSIS

    rdf.cgi?p=species&code=ARATH,ORYSA
    
    rdf.cgi?p=families&accession=GP104698&with_sequences=1

    rdf.cgi?p=sequences&accession=Sb04g011100.1,Os02g18410.1
    
    rdf.cgi?p=sequences&accession=AT4G20260.5&nocache=1

=head1 REQUIRES

Perl5

=head1 DESCRIPTION

Export selected GreenPhyl data into RDF format. For species, export both
sequences and families related to the selected species.

Exported data is cached. To update/replace cache, use &nocache=1 parameter.

=cut

use strict;
use warnings;

use FindBin;
use lib "$FindBin::Bin/../lib";
use lib "$FindBin::Bin/../local_lib";
use Carp qw (cluck confess croak);

use File::Path qw(make_path);
 
use Greenphyl;
use Greenphyl::Web;
use Greenphyl::Web::Access;
use Greenphyl::Web::Templates;

use Greenphyl::Species;
use Greenphyl::Family;
use Greenphyl::Sequence;
use Greenphyl::CustomFamily;
use Greenphyl::CustomSequence;
use Greenphyl::Tools::Families;
use Greenphyl::Tools::Sequences;


# Script global constants
##########################

=pod

=head1 CONSTANTS

B<$DEBUG>: (boolean)

Enables debug mode.

=cut

our $DEBUG = 1;
our @RDF_CACHE_PATH = (
    GP('TEMPLATES_PATH') . '/' . 'cache/rdf/species',
    GP('TEMPLATES_PATH') . '/' . 'cache/rdf/families',
    GP('TEMPLATES_PATH') . '/' . 'cache/rdf/sequences',
    GP('TEMPLATES_PATH') . '/' . 'cache/rdf/custom_families',
    GP('TEMPLATES_PATH') . '/' . 'cache/rdf/custom_sequences',
);
our $RDF_SPECIES_CACHE_PREFIX         = 'cache/rdf/species/species_';
our $RDF_FAMILY_CACHE_PREFIX          = 'cache/rdf/families/family_';
our $RDF_SEQUENCE_CACHE_PREFIX        = 'cache/rdf/sequences/sequence_';
our $RDF_CUSTOM_FAMILY_CACHE_PREFIX   = 'cache/rdf/custom_families/cfam_';
our $RDF_CUSTOM_SEQUENCE_CACHE_PREFIX = 'cache/rdf/custom_sequences/cseq_';
our %RDF_PREFIXES                     = (
    'greenphyl_sequence'   => 'greenphyl_sequence:<' . GetURL('rdf_sequence') . '>',
    'greenphyl_annotation' => 'greenphyl_annotation:<' . GetURL('rdf_annotation') . '>',
    'greenphyl_family'     => 'greenphyl_family:<' . GetURL('rdf_family') . '>',
    'interpro'             => 'interpro:<http://identifiers.org/interpro/>',
    'ncbi_taxon'           => 'ncbi_taxon:<http://purl.obolibrary.org/obo/NCBITaxon_>',
    'obo'	               => 'obo:<http://purl.obolibrary.org/obo/>',
    'owl'	               => 'owl:<http://www.w3.org/2002/07/owl#>',
    'pubmed'               => 'pubmed:<http://identifiers.org/pubmed/>',
    'tairlocus'            => 'tairlocus:<http://identifiers.org/tair.locus/>',
    'tigrlocus'            => 'tigrlocus:<http://www.southgreen.fr/agrold/tigr.locus/>',
    'uniprot'              => 'uniprot:<http://purl.uniprot.org/uniprot/>',
    'ensembl_plant'        => 'ensembl_plant:<http://identifiers.org/ensembl.plant/>',
    'rapdb'                => 'rapdb:<http://www.southgreen.fr/agrold/rapdb/>',
);




# Script global variables
##########################

=pod

=head1 VARIABLES

B<$g_dbh>: (DBI::db)

Current database handler.

=cut

my $g_dbh = GetDatabaseHandler();




# Script global functions
##########################

=pod

=head1 FUNCTIONS

=head2 InitCachePath

B<Description>: Make sure cache directories exist.

B<ArgsCount>: 0

B<Return>: nothing

=cut

sub InitCachePath
{
    foreach my $cache_path (@RDF_CACHE_PATH)
    {
        if (!-d $cache_path)
        {
            if (!make_path($cache_path))
            {
                PrintDebug("Failed to create missing directory: '$cache_path'");
            }
        }
    }
}


=pod

=head2 GetFamilyRDFIdentifier

B<Description>: Generate a family RDF identifier with a prefix and the family
accession.

B<ArgsCount>: 2

=over 4

=item $family: (Greenphyl::Family) (R)

The family object.

=item $prefixes: (hash ref) (R)

A reference to the RDF prefix hash that will be used for rendering. It may be
updated by this function.

=back

B<Return>: (string)

The prefixed RDF identifier for the family. Required prefix definition is set
in $prefixes.

=cut

sub GetFamilyRDFIdentifier
{
    my ($family, $prefixes) = @_;

    my $family_identifier;

    $prefixes->{$RDF_PREFIXES{'greenphyl_family'}} = 1;
    $family_identifier = 'greenphyl_family:' . $family->accession;

    return $family_identifier;
}


=pod

=head2 GetSequenceRDFIdentifier

B<Description>: Generate a sequence RDF identifier with a prefix and the
sequence accession. It will select the appropriate prefixe according to the
sequence species.

B<ArgsCount>: 2

=over 4

=item $sequence: (Greenphyl::Sequence) (R)

The sequence object.

=item $prefixes: (hash ref) (R)

A reference to the RDF prefix hash that will be used for rendering. It may be
updated by this function.

=back

B<Return>: (string)

The prefixed RDF identifier for the sequence. Required prefix definition is set
in $prefixes.

=cut

sub GetSequenceRDFIdentifier
{
    my ($sequence, $prefixes) = @_;

    my $sequence_identifier;
    
#    if ('ARATH' eq $sequence->species->code)
#    {
#        $sequence_identifier = 'tairlocus:' . $sequence->locus;
#        if (!exists($prefixes->{$RDF_PREFIXES{'tairlocus'}}))
#        {
#            $prefixes->{$RDF_PREFIXES{'tairlocus'}} = 1;
#        }
#    }
#    elsif ('ORYSA' eq $sequence->species->code)
#    {
#        $sequence_identifier = 'tigrlocus:' . $sequence->locus;
#        if (!exists($prefixes->{$RDF_PREFIXES{'tigrlocus'}}))
#        {
#            $prefixes->{$RDF_PREFIXES{'tigrlocus'}} = 1;
#        }
#    }
#    else
#    {
        $prefixes->{$RDF_PREFIXES{'greenphyl_sequence'}} = 1;
        $sequence_identifier = 'greenphyl_sequence:' . $sequence->accession;
#    }
    
    
    return $sequence_identifier;
}


=pod

=head2 RenderRDFDoc

B<Description>: Renders default RDF (documentation) page.

B<ArgsCount>: 0

B<Return>: (string)

Returns an HTML string containing the GreenPhyl RDF documentation.

=cut

sub RenderRDFDoc
{
   
    return RenderHTMLFullPage(
        {
            'title'             => 'GreenPhyl RDF Export',
            'content'           => 'rdf/main.tt',
        },
    );
}


=pod

=head2 RedirectEntity

B<Description>: Redirects to the page related to the given entity.

B<ArgsCount>: 0

B<Return>: (string)

Redirects to the appropriate entity page.

=cut

sub RedirectEntity
{
    # get entity type
    my $entity_type  = GetParameterValues('type');
    # get entity identifier
    my $entity_id    = GetParameterValues('identifier');

    # default redirect
    my $target_url = GetURL('base');
    
    if ($entity_type && defined($entity_id))
    {
        # get associated URL
        if ($entity_type eq 'sequence')
        {
            $target_url = GetURL('fetch_sequence', {'sequence_accession' => $entity_id});
        }
        elsif ($entity_type eq 'family')
        {
            $target_url = GetURL('fetch_family', {'accession' => $entity_id});
        }
        elsif ($entity_type eq 'annotation')
        {
        }
    }
    
    return Redirect($target_url);
}


=pod

=head2 RenderRDFSpecies

B<Description>: Export species sequences and families into RDF format.

B<ArgsCount>: 0

B<Return>: (string)

Returns an HTML string containing the RDF output.

=cut

sub RenderRDFSpecies
{
    my @species_codes  = GetParameterValues('code', undef, '[,;\s]+');
    my $selectors = {'selectors' => {'code' => ['IN', @species_codes],}};
    my $species_list = [Greenphyl::Species->new($g_dbh, $selectors)];
    
    my $args = {
        'mime' => 'TEXT',
    };

    my $html_output = RenderHTTPResponse($args);

    if (!@$species_list)
    {
        return $html_output;
    }

    my $prefixes = {};
    my $entity_output = '';
    # render RDF species data
    foreach my $species (@$species_list)
    {
        $entity_output .= PreRenderRDFSpecies($species, $prefixes);
    }

    # render RDF prefixes
    $html_output .= ProcessTemplate(
        'rdf/prefixes.tt',
        {
            'prefixes' => $prefixes,
        }
    );
    $html_output .= $entity_output;

    return $html_output;
}


=pod

=head2 RenderRDFFamilies

B<Description>: Export families into RDF format. Family sequences are not
part of the output unless the "with_sequences" parameter is used.

B<ArgsCount>: 0

B<Return>: (string)

Returns an HTML string containing the RDF output.

=cut

sub RenderRDFFamilies
{
    my @family_accessions = GetParameterValues('accession', undef, '[,;\s]+');
    my $selectors = {'selectors' => {'accession' => ['IN', @family_accessions],}};
    my $families = [Greenphyl::Family->new($g_dbh, $selectors)];
    
    my $args = {
        'mime' => 'TEXT',
    };

    my $html_output = RenderHTTPResponse($args);
    if (!@$families)
    {
        return $html_output;
    }

    my $prefixes = {};
    my $entity_output = '';
    # render RDF family data
    foreach my $family (@$families)
    {
        $entity_output .= PreRenderRDFFamily($family, $prefixes);
        # export sequences if requested
        if (GetParameterValues('with_sequences'))
        {
            foreach my $sequence (@{$family->sequences})
            {
                $entity_output .= PreRenderRDFSequence($sequence, $prefixes);
            }
        }
    }

    # render RDF prefixes
    $html_output .= ProcessTemplate(
        'rdf/prefixes.tt',
        {
            'prefixes' => $prefixes,
        }
    );
    $html_output .= $entity_output;

    return $html_output;
}


=pod

=head2 RenderRDFSequences

B<Description>: Export sequences into RDF format.

B<ArgsCount>: 0

B<Return>: (string)

Returns an HTML string containing the RDF output.

=cut

sub RenderRDFSequences
{
    my @sequence_accessions  = GetParameterValues('accession', undef, '[,;\s]+');
    my $selectors = {'selectors' => {'accession' => ['IN', @sequence_accessions],}};
    my $sequences = [Greenphyl::Sequence->new($g_dbh, $selectors)];
    
    my $args = {
        'mime' => 'TEXT',
    };

    my $html_output = RenderHTTPResponse($args);
    if (!@$sequences)
    {
        return $html_output;
    }

    my $prefixes = {};
    my $entity_output = '';
    # render RDF sequence data
    foreach my $sequence (@$sequences)
    {
        $entity_output .= PreRenderRDFSequence($sequence, $prefixes);
    }

    # render RDF prefixes
    $html_output .= ProcessTemplate(
        'rdf/prefixes.tt',
        {
            'prefixes' => $prefixes,
        }
    );
    $html_output .= $entity_output;

    return $html_output;
}


=pod

=head2 PreRenderRDFSpecies

B<Description>: [function description]. #+++

B<ArgsCount>: 0

B<Return>: (string)

Returns an HTML string containing the GreenPhyl page for ... #+++

=cut

sub PreRenderRDFSpecies
{
    my ($species, $prefixes) = @_;

    if (!$species)
    {
        my $species_code = GetParameterValues('species');
        my $selectors = {'selectors' => {'code' => $species_code,}};
        $species_code = Greenphyl::Species->new($g_dbh, $selectors);
    }

    my $species_rdf = '';
    if (!$species)
    {
        return $species_rdf;
    }

    # $prefixes->{$RDF_PREFIXES{'gpspecies'}} = 1;

    my $species_template_fh;
    my $species_template_cache_file_name = $RDF_SPECIES_CACHE_PREFIX . $species->code . '.tt';
    my $species_template_cache_file_path = GP('TEMPLATES_PATH') . '/' . $species_template_cache_file_name;
    # check if we got the species in cache
    if ((!-e $species_template_cache_file_path) || GetParameterValues('nocache'))
    {
        # not in cache, generate cache file
        
        # # get RDF species identifier
        # my $species_identifier = GetSpeciesRDFIdentifier($species, $prefixes);
        # # default properties
        # my @species_properties = (
        #     ['rdf:type',        'owl:Class'],
        #     ['rdfs:subClassOf', 'obo:SO_0000'],
        #     ['rdfs:label',      '"' . $species->organism . '"^^xsd:string'],
        # );
        # # array of entitites
        # my @entitites = ({'id' => $species_identifier, 'properties' => \@species_properties});
        # $species_rdf .= ProcessTemplate(
        #     'rdf/entities.tt',
        #     {
        #         'entities' => \@entitites,
        #     }
        # );

        # -add sequence data
        foreach my $sequence (@{$species->sequences})
        {
            $species_rdf .= PreRenderRDFSequence($sequence, $prefixes);
        }

        # -add family data
        foreach my $family (@{$species->families})
        {
            $species_rdf .= PreRenderRDFFamily($family, $prefixes);
        }

        # create file if not empty
        if ($species_rdf)
        {
            if (open($species_template_fh, ">$species_template_cache_file_path"))
            {
                # chmod(0666, $species_template_cache_file_path) or confess $!;
                print {$species_template_fh} $species_rdf;
                close($species_template_fh);
            }
            else
            {
                Throw('error' => "Unable to generate species ($species) RDF template! An unexpected error occurred!", 'log' => "Error generating species ($species) RDF template file: $!\nPath: $species_template_cache_file_path");
            }
        }
    }
    else
    {
        # read cache
        if (open($species_template_fh, $species_template_cache_file_path))
        {
            #+FIXME: get only required prefixes
            map { $prefixes->{$_} = 1; } values(%RDF_PREFIXES);
            $species_rdf = join('', <$species_template_fh>);
            close($species_template_fh);
        }
        else
        {
            Throw('error' => "Unable to read species ($species) RDF template! An unexpected error occurred!", 'log' => "Error reading species ($species) RDF template file: $!\nPath: $species_template_cache_file_path");
        }        
    }

    return $species_rdf;
}


=pod

=head2 PreRenderRDFFamily

B<Description>: [function description]. #+++

B<ArgsCount>: 0

B<Return>: (string)

Returns an HTML string containing the GreenPhyl page for ... #+++

=cut

sub PreRenderRDFFamily
{
    my ($family, $prefixes) = @_;

    $family ||= LoadFamily();

    my $family_rdf = '';
    if (!$family)
    {
        return $family_rdf;
    }

    $prefixes->{$RDF_PREFIXES{'greenphyl_family'}} = 1;

    my $family_template_fh;
    my $family_template_cache_file_name = $RDF_FAMILY_CACHE_PREFIX . $family->accession . '.tt';
    my $family_template_cache_file_path = GP('TEMPLATES_PATH') . '/' . $family_template_cache_file_name;
    # check if we got the family in cache
    if ((!-e $family_template_cache_file_path) || GetParameterValues('nocache'))
    {
        # not in cache, generate cache file
        
        # get RDF family identifier
        my $family_identifier = GetFamilyRDFIdentifier($family, $prefixes);
        my $family_label = $family->accession;
        if ($family->isAnnotated())
        {
            $family_label = $family->name;
        }
        
        # default properties
        my @family_properties = (
            ['rdf:type',        'owl:Class'],
            ['rdfs:subClassOf', 'obo:OBI_0000251'],
            ['rdfs:label',      '"' . $family_label . '"^^xsd:string'],
        );
        $prefixes->{$RDF_PREFIXES{'owl'}} = 1;
        # array of entitites
        my @entitites = ({'id' => $family_identifier, 'properties' => \@family_properties});

        # get family properties...
        # -annotation
        my $description = $family->description;
        if ($description =~ m/\w/)
        {
            $description =~ s/"/''/g;
            push(@family_properties, ['agrold_vocabulary:description', '"' . $family->description . '"^^xsd:string']);
        }
        # -taxonomy
        if ($family->taxonomy_id)
        {
            push(@family_properties, ['agrold_vocabulary:taxon', 'ncbi_taxon:' . $family->taxonomy_id]);
            $prefixes->{$RDF_PREFIXES{'ncbi_taxon'}} = 1;
        }

        # # -plant-specific
        # if (defined($family->plant_specific))
        # {
        #     push(@family_properties, ['agrold_vocabulary:plant_specific', '"' . $family->plant_specific .'"^^xsd:integer' ]);
        # }

        # -black_listed
        if ($family->black_listed)
        {
            push(@family_properties, ['agrold_vocabulary:black_listed', '"true"^^xsd:boolean' ]);
        }

        # -go terms
        my $family_go_data = Greenphyl::Tools::Families::GenerateGOData($family);
        foreach my $go_data (@$family_go_data)
        {
            my $go_code = $go_data->code;
            $go_code =~ tr/:/_/;
            push(@family_properties, ['agrold_vocabulary:has_go_identifier', 'obo:' . $go_code]);
            $prefixes->{$RDF_PREFIXES{'obo'}} = 1;
        }

        # -evidence
        if ($family->inferences)
        {
            push(@family_properties, ['agrold_vocabulary:evidence', '"' . $family->inferences . '"^^xsd:string']);
        }

        # -sequence count
        push(@family_properties, ['agrold_vocabulary:number_of_sequences', '"' . $family->sequence_count . '"^^xsd:integer']);

        # -clustering level
        push(@family_properties, ['agrold_vocabulary:has_thresold', '"' . $family->level . '"^^xsd:integer']);

        # -curation status
        if ($family->validated && ($family->validated ne 'N/A'))
        {
            push(@family_properties, ['agrold_vocabulary:curation_status', '"' . $family->validated . '"^^xsd:string']);
        }

        # -pubmed
        foreach my $pubmed (@{$family->pubmed})
        {
            push(@family_properties, ['agrold_vocabulary:has_dbxref', 'pubmed:' . $pubmed->accession]);
            $prefixes->{$RDF_PREFIXES{'pubmed'}} = 1;
        }

        $family_rdf = ProcessTemplate(
            'rdf/entities.tt',
            {
                'entities' => \@entitites,
            }
        );

        # create file if not empty
        if (@entitites && $family_rdf)
        {
            if (open($family_template_fh, ">$family_template_cache_file_path"))
            {
                # chmod(0666, $family_template_cache_file_path) or confess $!;
                print {$family_template_fh} $family_rdf;
                close($family_template_fh);
            }
            else
            {
                Throw('error' => "Unable to generate family ($family) RDF template! An unexpected error occurred!", 'log' => "Error generating family ($family) RDF template file: $!\nPath: $family_template_cache_file_path");
            }
        }
    }
    else
    {
        # read cache
        if (open($family_template_fh, $family_template_cache_file_path))
        {
            #+FIXME: get only required prefixes
            map { $prefixes->{$_} = 1; } values(%RDF_PREFIXES);
            $family_rdf = join('', <$family_template_fh>);
            close($family_template_fh);
        }
        else
        {
            Throw('error' => "Unable to read family ($family) RDF template! An unexpected error occurred!", 'log' => "Error reading family ($family) RDF template file: $!\nPath: $family_template_cache_file_path");
        }        
    }

    return $family_rdf;
}


=pod

=head2 PreRenderRDFSequence

B<Description>: [function description]. #+++

B<ArgsCount>: 0

B<Return>: (string)

Returns an HTML string containing the GreenPhyl page for ... #+++

=cut

sub PreRenderRDFSequence
{
    my ($sequence, $prefixes) = @_;

    $sequence ||= LoadSequence();

    my $sequence_rdf = '';
    if (!$sequence)
    {
        return $sequence_rdf;
    }

#    # not found, try custom sequence
#    if (!$sequence)
#    {
#        InitAccess();
#        $sequence = LoadCustomSequence();
#        if (!$custom_family->hasReadAccess() && !IsAdmin()) 
#        {
#        }
#    }

    my $sequence_template_fh;
    my $sequence_template_cache_file_name = $RDF_SEQUENCE_CACHE_PREFIX . $sequence->accession . '.tt';
    my $sequence_template_cache_file_path = GP('TEMPLATES_PATH') . '/' . $sequence_template_cache_file_name;
    # check if we got the sequence in cache
    if ((!-e $sequence_template_cache_file_path) || GetParameterValues('nocache'))
    {
        # not in cache, generate cache file
        
        # get RDF sequence identifier
        my $sequence_identifier = GetSequenceRDFIdentifier($sequence, $prefixes);
        # default properties
        my @sequence_properties = (
            ['rdf:type',        'owl:Class'],
            ['rdfs:subClassOf', 'obo:SO_0000104'],
            ['rdfs:label',      '"' . $sequence->accession . '"^^xsd:string'],
        );
        $prefixes->{$RDF_PREFIXES{'owl'}} = 1;
        # array of entitites
        my @entitites = ({'id' => $sequence_identifier, 'properties' => \@sequence_properties});

        # get sequence properties...
        # -annotation
        my $description = $sequence->annotation || '';
        if ($description =~ m/\w/)
        {
            $description =~ s/"/''/g;
            push(@sequence_properties, ['agrold_vocabulary:description', '"' . $description . '"^^xsd:string']);
        }

        # -taxonomy
        push(@sequence_properties, ['agrold_vocabulary:taxon', 'ncbi_taxon:' . $sequence->species->taxonomy_id]);
        $prefixes->{$RDF_PREFIXES{'ncbi_taxon'}} = 1;

        # -go terms
        foreach my $go (@{$sequence->go})
        {
            my $go_code = $go->code;
            $go_code =~ tr/:/_/;
            push(@sequence_properties, ['agrold_vocabulary:has_go_identifier', 'obo:' . $go_code]);
            # if ('biological_process' eq $go->type)
            # {
            #     push(@sequence_properties, ['obo:BFO_0000056', 'obo:' . $go->code]);
            # }
            # elsif ('molecular_function' eq $go->type)
            # {
            #     push(@sequence_properties, ['obo:BFO_0000085', 'obo:' . $go->code]);
            # }
            # elsif ('cellular_component' eq $go->type)
            # {
            #     push(@sequence_properties, ['obo:BFO_0000082', 'obo:' . $go->code]);
            # }
            $prefixes->{$RDF_PREFIXES{'obo'}} = 1;
        }
        # -IPR domains
        foreach my $ipr (@{$sequence->ipr})
        {
            push(@sequence_properties, ['agrold_vocabulary:contains', 'interpro:' . $ipr->code]);
            $prefixes->{$RDF_PREFIXES{'interpro'}} = 1;
        }
        # -families
        foreach my $family (@{$sequence->families})
        {
            push(@sequence_properties, ['agrold_vocabulary:is_member_of', 'greenphyl_family:' . $family->accession]);
        }
        # -homologs
        my @homology_pairs;
        foreach my $homolog (@{$sequence->homologs})
        {
            my $homology = $sequence->getHomologyWith($homolog);
            if ('ultra-paralogy' eq $homology->type)
            {
                push(@sequence_properties, ['agrold_vocabulary:is_paralogous_to', GetSequenceRDFIdentifier($homolog, $prefixes)]);
            }
            elsif ('orthology' eq $homology->type)
            {
                push(@sequence_properties, ['agrold_vocabulary:is_orthologous_to', GetSequenceRDFIdentifier($homolog, $prefixes)]);
            }
            push(@sequence_properties, ['agrold_vocabulary:has_annotation', 'greenphyl_annotation:' . $sequence->accession . '_' . $homolog->accession]);
            $prefixes->{$RDF_PREFIXES{'greenphyl_annotation'}} = 1;

            push(@homology_pairs, {
                'id'       => $sequence->accession . '_' . $homolog->accession,
                'subject'  => $sequence,
                'object'   => $homolog,
                'homology' => $homology,
            });
        }
        # -uniprot
        if (('ARRAY' eq ref($sequence->uniprot)) && @{$sequence->uniprot})
        {
            foreach my $uniprot (@{$sequence->uniprot})
            {
                push(@sequence_properties, ['agrold_vocabulary:has_dbxref', 'uniprot:' . $uniprot->accession]);
            }
            $prefixes->{$RDF_PREFIXES{'uniprot'}} = 1;
        }
        # -synonyms
        if (('ARRAY' eq ref($sequence->synonyms)) && @{$sequence->synonyms})
        {
            foreach my $synonym (@{$sequence->synonyms})
            {
                push(@sequence_properties, ['agrold_vocabulary:has_synonym', "\"$synonym\""]);
            }
            $prefixes->{$RDF_PREFIXES{'uniprot'}} = 1;
        }
        # -pubmed
        foreach my $pubmed (@{$sequence->pubmed})
        {
            push(@sequence_properties, ['agrold_vocabulary:has_dbxref', 'pubmed:' . $pubmed->accession]);
            $prefixes->{$RDF_PREFIXES{'pubmed'}} = 1;
        }
        # -other cross-refs
        if (('ARATH' eq $sequence->species->code)
            || ('SORBI' eq $sequence->species->code)
            || ('ZEAMA' eq $sequence->species->code))
        {
            push(@sequence_properties, ['agrold_vocabulary:derives_from', 'ensembl_plant:' . $sequence->locus]);
            $prefixes->{$RDF_PREFIXES{'ensembl_plant'}} = 1;
        }
        elsif ('ORYSA' eq $sequence->species->code)
        {
            push(@sequence_properties, ['agrold_vocabulary:derives_from', 'rapdb:' . $sequence->locus]);
            $prefixes->{$RDF_PREFIXES{'rapdb'}} = 1;
        }
#        if ('ARATH' eq $sequence->species->code)
#        {
#            push(@sequence_properties, ['agrold_vocabulary:xRef', 'tairlocus:' . $sequence->locus]);
#            $prefixes->{$RDF_PREFIXES{'tairlocus'}} = 1;
#        }
#        elsif ('ORYSA' eq $sequence->species->code)
#        {
#            push(@sequence_properties, ['agrold_vocabulary:xRef', 'tigrlocus:' . $sequence->locus]);
#            $prefixes->{$RDF_PREFIXES{'tigrlocus'}} = 1;
#        }

        # -add homology data
        foreach my $homology_data (@homology_pairs)
        {
            my $homology_predicate = '';
            if ('ultra-paralogy' eq $homology_data->{'homology'}->type)
            {
                $homology_predicate = 'is_paralogous_to';
            }
            elsif ('orthology' eq $homology_data->{'homology'}->type)
            {
                $homology_predicate = 'is_orthologous_to';
            }

            push(@entitites, 
                {
                    'id' => 'greenphyl_annotation:' . $homology_data->{'id'},
                    'properties' =>
                        [
                            ['rdf:type',               'rdf:Statement'],
                            ['rdf:subject',            GetSequenceRDFIdentifier($homology_data->{'subject'})],
                            ['rdf:predicate',          'agrold_vocabulary:' . $homology_predicate],
                            ['rdf:object',             GetSequenceRDFIdentifier($homology_data->{'object'})],
                            ['agrold_vocabulary:has_score', '"' . $homology_data->{'homology'}->score . '"^^xsd:float'],
                            ['agrold_vocabulary:assigned_by', '"GreenPhyl"^^xsd:string'],
                        ],
                },
            );
        }

        $sequence_rdf = ProcessTemplate(
            'rdf/entities.tt',
            {
                'entities' => \@entitites,
            }
        );

        # create file if not empty
        if (@entitites && $sequence_rdf)
        {
            if (open($sequence_template_fh, ">$sequence_template_cache_file_path"))
            {
                # chmod(0666, $sequence_template_cache_file_path) or confess $!;
                print {$sequence_template_fh} $sequence_rdf;
                close($sequence_template_fh);
            }
            else
            {
                Throw('error' => "Unable to generate sequence ($sequence) RDF template! An unexpected error occurred!", 'log' => "Error generating sequence ($sequence) RDF template file: $!\nPath: $sequence_template_cache_file_path");
            }
        }
    }
    else
    {
        # read cache
        if (open($sequence_template_fh, $sequence_template_cache_file_path))
        {
            #+FIXME: get only required prefixes
            map { $prefixes->{$_} = 1; } values(%RDF_PREFIXES);
            $sequence_rdf = join('', <$sequence_template_fh>);
            close($sequence_template_fh);
        }
        else
        {
            Throw('error' => "Unable to read sequence ($sequence) RDF template! An unexpected error occurred!", 'log' => "Error reading sequence ($sequence) RDF template file: $!\nPath: $sequence_template_cache_file_path");
        }        
    }

    return $sequence_rdf;
}




# Script options
#################

=pod

=head1 OPTIONS

#--- describes GET/POST parameters given to the CGI script

=head2 Parameters

=over 4

=item B<[option_name]> ([option nature]): #+++

[option description]. #+++
Default: [option default value if one] #+++

=back
#--- Example:
#---
#--- No parameters requiered or parameters given using POST method.
#---
#--- =over 4
#---
#--- =item B<login> (string):
#---
#--- The user login.
#---
#--- =item B<tracker> (integer):
#---
#--- A tracker used to retrive the data element to display.
#---
#---=back

=cut


# CODE START
#############

# hash for function selection.
my $actions =
    {
        ''          => \&RenderRDFDoc,
        'entity'    => \&RedirectEntity,
        'species'   => \&RenderRDFSpecies,
        'families'  => \&RenderRDFFamilies,
        'sequences' => \&RenderRDFSequences,
    };

# try block
eval
{
    # make sure cache path exist
    InitCachePath();
    # display page
    print ProceedPage($actions);
};

HandleErrors();

exit(0);

__END__
# CODE END
###########

=pod

=head1 DIAGNOSTICS

#--- Give and explain here every error message the the script may generate.
#--- Use: (W)=warning (optional), (D)=deprecation (optional),
#---      (S)=severe warning (mandatory), (F)=fatal error (trappable),
#---      (X)=very fatal error (non-trappable).
#--- example:
#---
#--- =over 4
#---
#--- =item *
#---
#--- Negative radius;
#--- (F) Can not draw a circle with a negative radius.
#---
#---=back #+++

=head1 AUTHORS

Mathieu ROUARD (Bioversity), m.rouard@cgiar.org

Valentin GUIGNON (Bioversity), v.guignon@cgiar.org

Matthieu CONTE (Syngenta)

Sebastien BRIOIS (Syngenta)

=head1 VERSION

Version [version.subversion.build] #+++

Date [DD/MM/YYY] #+++

=head1 SEE ALSO

#--- other documentation or objects related to this package
GreenPhyl documentation.

=cut
