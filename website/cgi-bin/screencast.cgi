#!/usr/bin/perl

=pod

=head1 NAME

screencast.cgi - Display screencast interface

=head1 SYNOPSIS

    screencast.cgi?video=test

=head1 REQUIRES

Perl5

=head1 DESCRIPTION

Display screencast interface.

=cut

use strict;
use warnings;

use lib '../lib';
use lib '../local_lib';
use Carp qw (cluck confess croak);

use Greenphyl;
use Greenphyl::Web;




# Script global constants
##########################

=pod

=head1 CONSTANTS

B<$AVAILABLE_VIDEOS>: (hash)

Hash of available videos. Each key is a video identifier (used in URL) and
values are hash.

=cut

our $AVAILABLE_VIDEOS = {
    'test' => {
        'title'             => 'Screencast Test',
        'file_name'         => 'test.mp4',
        'preview_file_name' => 'test.jpg',
        'description'       => 'This is GreenPhyl test screencast.',
        'long_description'  => qq|
            This <i>screencast</i> is used for test purpose in order to check if
            the screencast subsystem works properly.
        |,
    },
};




# Script global functions
##########################

=pod

=head1 FUNCTIONS

=head2 RenderScreencast

B<Description>: Returns the HTML page of a given screencast.

B<ArgsCount>: 0

B<Return>: (string)

Returns an HTML string containing the GreenPhyl page for a screencast or an
error page if screencast is not available.

=cut

sub RenderScreencast
{
    my $error_details = undef;

    # get video identifier
    my $video_id = GetParameterValues('video');
    my ($screencast_data, $video_size, $preview_size);

    # make sure the video is available
    if (!$video_id || !exists($AVAILABLE_VIDEOS->{$video_id}))
    {
        # video not available!
        $error_details = 'The requested video is not available!';

        # check if a video identifier has been specified
        if (!$video_id)
        {
            $error_details = 'No video identifier provided! Nothing to display!';
        }

        # check if the video identifier can be displayed to the user
        if ($video_id && ($video_id =~ m/^\w+$/))
        {
            $error_details = "The requested video '$video_id' is not available!
                              If you used a link from this website, please
                              report this error to the site administrator.";
        }
    }
    else
    {
        # we got a video
        $screencast_data = $AVAILABLE_VIDEOS->{$video_id};
        # get video file size
        $video_size = -s GP('VIDEOS_PATH') .'/' . $screencast_data->{'file_name'};
        PrintDebug('File: ' . GP('VIDEOS_PATH') .'/' . $screencast_data->{'file_name'});
        PrintDebug('Size: ' . $video_size);
        if (!$video_size)
        {
            $error_details = 'The requested video is missing!';
        }
        $preview_size = -s GP('VIDEOS_PATH') .'/' . $screencast_data->{'preview_file_name'};
    }

    if ($error_details)
    {
        # render an error message
        return RenderHTMLFullPage(
            {
                'title'   => 'Screencast Error',
                'content' => 'error.tt',
                'message' => $error_details,
            },
        );
    }
    else
    {
        return RenderHTMLFullPage(
            {
                'title'      => $screencast_data->{'title'},
                'content'    => 'screencast.tt',
                'css'        => ['shockplayer'],
                'javascript' => ['jquery.osmplayer.compressed', 'jquery.media.template.shockplayer'],
                'screencast' =>
                {
                    'title'            => $screencast_data->{'title'},
                    'file'             => $screencast_data->{'file_name'},
                    'size'             => $video_size,
                    'preview'          => $screencast_data->{'preview_file_name'},
                    'preview_size'     => $preview_size,
                    'description'      => $screencast_data->{'description'},
                    'long_description' => $screencast_data->{'long_description'},
                },
            },
        );
    }
}




# Script options
#################

=pod

=head1 OPTIONS

    screencast.cgi?video=<VIDEO_ID>

=head2 Parameters

=over 4

=item B<video> (string):

Video identifier of the screencast to display.

=back

=cut


# CODE START
#############

# hash for function selection.
my $actions =
    {
        ''             => \&RenderScreencast,
        'video'        => \&RenderScreencast,
        #'playlist'     => \&RenderScreencastPlayList,
    };

# try block
eval
{
    # display page
    print ProceedPage($actions);
};

HandleErrors();

exit(0);

# CODE END
###########


=pod

=head1 AUTHORS

Mathieu ROUARD (Bioversity), m.rouard@cgiar.org

Valentin GUIGNON (Bioversity), v.guignon@cgiar.org

Matthieu CONTE (Syngenta)

=head1 VERSION

Version 1.0.0

Date 22/06/2012

=head1 SEE ALSO

GreenPhyl documentation.

=cut
