#!/usr/bin/perl

=pod

=head1 NAME

tree.pl - Performs tree services

=head1 SYNOPSIS

    tree.pl?service=get_tree&format=xml&family_accession=GP000215&remove=POPTR,MANES

=head1 REQUIRES

Perl5

=head1 DESCRIPTION

Performs several tree services.

=cut

use strict;
use warnings;

use lib '../lib';
use lib '../local_lib';
use Carp qw (cluck confess croak);

use Greenphyl;
use Greenphyl::Web;
use Greenphyl::Web::PseudoService;
use Greenphyl::Tools::Tree;




# Script global constants
##########################

=pod

=head1 CONSTANTS

B<$TREE_SERVICES>: (hash ref)

Contains the tree service handlers.

=cut

our $DEBUG = 0;
our $TREE_SERVICES =
    {
        'get_tree' => \&Greenphyl::Tools::Tree::GetTree,
    };




# Script options
#################

=pod

=head1 OPTIONS

    tree.pl <service=<service_name> > ...<service parameters>

=head2 Parameters

=over 4

=item B<service> (string):

name of the service.

=back

=cut


# CODE START
#############

HandleService($TREE_SERVICES);

exit(0);

# CODE END
###########


=pod

=head1 AUTHORS

Mathieu ROUARD (Bioversity), m.rouard@cgiar.org

Valentin GUIGNON (Bioversity), v.guignon@cgiar.org

Matthieu CONTE (Syngenta)

=head1 VERSION

Version 1.0.0

Date 21/03/2013

=head1 SEE ALSO

GreenPhyl documentation.

=cut
