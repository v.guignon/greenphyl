#!/usr/bin/perl

=pod

=head1 NAME

seq2families.cgi - Retrieve families associated with sequences

=head1 SYNOPSIS

    seq2families.cgi?p=classification&seq_textids=At1g14920%0D%0AOs03g49990%0D%0AAt1g14920.1%0D%0AGRMZM2G024973_P01

=head1 REQUIRES

Perl5

=head1 DESCRIPTION

The user provides a list of sequence names and seq2families finds the
corresponding families classification.

=cut

use strict;
use warnings;

use lib '../lib';
use lib '../local_lib';
use Carp qw (cluck confess croak);

use Greenphyl;
use Greenphyl::Web;
use Greenphyl::Dumper;
use Greenphyl::Sequence;
use Greenphyl::CompositeObject;




# Script global functions
##########################

=pod

=head1 FUNCTIONS

=head2 PrepareDumpColumns

B<Description>: Prepare the parameter hash for columns that should be dumped.

B<ArgsCount>: 0

B<Return>: (hash)

column dump parameter hash. Keys are column labels and values are corresponding
object member path.

=cut

sub PrepareDumpColumns
{
    my $column_index = 'a';
    my $dump_columns =
        {
            $column_index++ . '. sequence_id'     => 'accession',
            $column_index++ . '. lev_1_fam_id'    => 'families.0.accession',
            $column_index++ . '. lev_1_fam_name'  => 'families.0.name',
            $column_index++ . '. lev_1_validated' => 'families.0.validated',
            $column_index++ . '. lev_2_fam_id'    => 'families.1.accession',
            $column_index++ . '. lev_2_fam_name'  => 'families.1.name',
            $column_index++ . '. lev_2_validated' => 'families.1.validated',
            $column_index++ . '. lev_3_fam_id'    => 'families.2.accession',
            $column_index++ . '. lev_3_fam_name'  => 'families.2.name',
            $column_index++ . '. lev_3_validated' => 'families.2.validated',
            $column_index++ . '. lev_4_fam_id'    => 'families.3.accession',
            $column_index++ . '. lev_4_fam_name'  => 'families.3.name',
            $column_index++ . '. lev_4_validated' => 'families.3.validated',
        };

    return $dump_columns;
}


=pod

=head2 RenderSeq2FamiliesForm

B<Description>: Renders sequence to families web form.

B<ArgsCount>: 0

B<Return>: (string)

Returns an HTML string containing the GreenPhyl form for sequence to families
tool.

=cut

sub RenderSeq2FamiliesForm
{
    my $dump_columns = PrepareDumpColumns();

    return RenderHTMLFullPage(
        {
            'title'               => 'Get family classification of your sequences',
            'content'             => 'tools/seq2families_form.tt',
            'form_data'           =>
                {
                    'action'      => '?p=classification',
                    'identifier'  => 'seq2families',
                    'submit'      => 'Get classification',
                },
            'dump_data' =>
            {
                'links' => [
                    {
                        'label'       => 'Excel',
                        'format'      => 'excel',
                        'file_name'   => 'classification.xls',
                        'namespace'   => 'excel',
                    },
                    {
                        'label'       => 'CSV',
                        'format'      => 'csv',
                        'file_name'   => 'classification.csv',
                        'namespace'   => 'csv',
                    },
                    {
                        'label'       => 'XML',
                        'format'      => 'xml',
                        'file_name'   => 'classification.xml',
                        'namespace'   => 'xml',
                    },
                ],
                'parameters'  => {'columns' => EncodeDumpParameterValue($dump_columns),},
                'object_type' => 'sequences',
                'fields'      => 'seq_textids',
                'mapping'     => {'seq_textids'=>'accession'},
                'separator'   => 'eol',
                'mode'        => 'embeded',
            },
        },
    );
}


=pod

=head2 FetchSequences

B<Description>: Fetch sequences in database from user input.

B<ArgsCount>: 0

B<Return>: (list)

 The first element is an array of sequence objects found in database. The
 second element is an array of name (string) of sequences not found in database.

=cut

sub FetchSequences
{
    my $dbh = GetDatabaseHandler();

    my $seq_textids = GetParameterValues('seq_textids');
    my $sequences = [];
    my $seq_textids_not_found = [];

    foreach my $seq_textid (split(/\s+/, $seq_textids))
    {
        if ($seq_textid && ValidateSequenceName($seq_textid))
        {
            if (my $sequence = Greenphyl::Sequence->new($dbh, {'selectors' => {'accession' => $seq_textid}}))
            {
                push(@$sequences, $sequence);
            }
            elsif ($sequence = Greenphyl::Sequence->new($dbh, {'selectors' => {'accession' => ['LIKE', "%$seq_textid%"]}}))
            {
                push(@$sequences, $sequence);
            }
            else
            {
                push(@$seq_textids_not_found, $seq_textid);
            }
        }
        elsif ($seq_textid)
        {
            push(@$seq_textids_not_found, $seq_textid);
        }
    }
    
    return ($sequences, $seq_textids_not_found);
}


=pod

=head2 GetMatchingFamilies

B<Description>: Find families matching the given sequences.

B<ArgsCount>: 1

=over 4

=item $sequences: (ref on an array of sequences) (R)

Array containing the list of sequences to use to find corresponding families.

=back

B<Return>: (list)

The first element is an array of matching families sorted by family level and
then by family ID.
The second element is an array of sequence objects that are orphans.

=cut

sub GetMatchingFamilies
{
    my ($sequences) = @_;
    # keys are family ID
    # values are hash:
    #   {'family'=> family object, 'sequences' => array of sequences}
    my $matching_families = {};
    my $unmatched_sequences = [];

    my $family_level = GetParameterValues('classification_level');
    my %LEVELS =
    (
        'level_1' => 1,
        'level_2' => 2,
        'level_3' => 3,
        'level_4' => 4,
    );

    foreach my $sequence (@$sequences)
    {
        my $matched = 0;
        next if (!$sequence || !$sequence->families || !@{$sequence->families});
        foreach my $family (@{$sequence->families})
        {
            # check if a classification level has been specified and corresponds
            if (!exists($LEVELS{$family_level})
                || ($LEVELS{$family_level} == $family->level))
            {
                $matching_families->{$family->id} ||= {};
                $matching_families->{$family->id}->{'family'} ||= $family;
                $matching_families->{$family->id}->{'sequences'} ||= [];
                push(@{$matching_families->{$family->id}->{'sequences'}}, $sequence);
                $matched = 1;
            }
        }
        if (!$matched)
        {
            push(@$unmatched_sequences, $sequence);
        }
    }

    my @matching_families;
    # create a composite array
    foreach my $family_match (values(%$matching_families))
    {
        push(@matching_families, Greenphyl::CompositeObject->new($family_match))
    }
    
    # sort matched families by level and then by ID
    my $sorted_matching_families =
        [
            sort
            {
                if ($a->family->level == $b->family->level)
                {
                    return $a->family->id <=> $b->family->id;
                }
                else
                {
                    return $a->family->level <=> $b->family->level;
                }
            }
            @matching_families
        ];
        
    return ($sorted_matching_families, $unmatched_sequences);
}


=pod

=head2 RenderClassification

B<Description>: Renders the result page of sequence classification.

B<ArgsCount>: 0

B<Return>: (string)

Returns an HTML string containing the GreenPhyl page for sequence
classification.

=cut

sub RenderClassification
{
    my $dump_columns = PrepareDumpColumns();

    my ($sequences, $seq_textids_not_found) = FetchSequences();

    my ($family_matches, $unmatched_sequences) = GetMatchingFamilies($sequences);

    return RenderHTMLFullPage(
        {
            'title'               => 'Sequence classification',
            'content'             => 'tools/seq2families_classification.tt',
            'dump_data' =>
            {
                'links' => [
                    {
                        'label'       => 'Excel',
                        'format'      => 'excel',
                        'file_name'   => 'classification.xls',
                        'namespace'   => 'excel',
                    },
                    {
                        'label'       => 'CSV',
                        'format'      => 'csv',
                        'file_name'   => 'classification.csv',
                        'namespace'   => 'csv',
                    },
                    {
                        'label'       => 'XML',
                        'format'      => 'xml',
                        'file_name'   => 'classification.xml',
                        'namespace'   => 'xml',
                    },
                ],
                'parameters'  => {'columns' => EncodeDumpParameterValue($dump_columns),},
                'object_type' => 'sequence',
                'fields'      => 'seq_id',
                'mapping'     => {'seq_id' => 'id'},
                'mode'        => 'form',
            },
            'family_matches'        => $family_matches,
            'unmatched_sequences'   => $unmatched_sequences,
            'unavailable_sequences' => $seq_textids_not_found,
        },
    );

}




# Script options
#################

=pod

=head1 OPTIONS

  seq2families.cgi [p=classification <seq_textids=<lis_of_seq_ids> >]

=head2 Parameters

=over 4

=item B<p> (string):

Page selection.
Default: 'form'

=item B<seq_textids> (string):

A list of end-of-line separated sequence text identifier.

=back

=cut


# CODE START
#############

# hash for function selection.
my $actions =
    {
        ''             => \&RenderSeq2FamiliesForm,
        'form'         => \&RenderSeq2FamiliesForm,
        'classification' => {
            ''           => \&RenderClassification,
            'excel' => \&DumpPageData,
            'csv'   => \&DumpPageData,
            'xml'   => \&DumpPageData,
        },
    };

   
# try block
eval
{
    # display page
    print ProceedPage($actions);
};

HandleErrors();

exit(0);

# CODE END
###########


=pod

=head1 AUTHORS

Mathieu ROUARD (Bioversity), m.rouard@cgiar.org

Valentin GUIGNON (Bioversity), v.guignon@cgiar.org

Matthieu CONTE (Syngenta)

=head1 VERSION

Version 1.0.0

Date 25/05/2012

=head1 SEE ALSO

GreenPhyl documentation.

=cut
