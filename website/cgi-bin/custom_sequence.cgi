#!/usr/bin/perl

=pod

=head1 NAME

custom_sequence.cgi - Display custom sequence identification card

=head1 SYNOPSIS

    custom_sequence.cgi?p=id&cseq_id=42

=head1 REQUIRES

Perl5

=head1 DESCRIPTION

This script displays custom sequence identification card with sequence details.

=cut

use strict;
use warnings;

use lib '../lib';
use lib '../local_lib';
use Carp qw (cluck confess croak);

use Greenphyl;
use Greenphyl::Web;

use Greenphyl::CustomSequence;
use Greenphyl::Tools::Sequences;
use Greenphyl::Tools::GO;




# Script global constants
##########################

=pod

=head1 CONSTANTS

B<$DEBUG>: (boolean)

When set to true, it enables debug mode.



=cut

our $DEBUG = 0;




# Script global variables
##########################

=pod

=head1 VARIABLES

B<$g_dbh>: (DBI::db)

Current database handler.

=cut

my $g_dbh = GetDatabaseHandler();




# Script global functions
##########################

=pod

=head1 FUNCTIONS

=head2 RenderCustomSequenceIdCard

B<Description>: Renders a single sequence identification card with sequence
details.

B<ArgsCount>: 0

B<Return>: (string)

Returns an HTML string containing the sequence identification card.

=cut

sub RenderCustomSequenceIdCard
{
    my $custom_sequence =  LoadCustomSequence();

    my $sorted_go            = [];
    if ($custom_sequence->go)
    {
        $sorted_go            = [sort {$a->code cmp $b->code} @{$custom_sequence->go}];
    }
    my $go_groups            = Greenphyl::Tools::GO::GroupGOByType($sorted_go);

    my $html_parameters =
        {
            'title'                => 'Gene ' . $custom_sequence,
            'sequence'             => $custom_sequence,
            'content'              => 'sequence_tools/sequence_details.tt',
            'go_groups'            => $go_groups,

            # tabs definition
            'contents' =>
            [
                {
                    'id'      => 'tab-gene-classification',
                    'title'   => 'Gene Classification',
                    'content' => 'sequence_tools/clustering.tt',
                },
                {
                    'id'      => 'tab-gene-sequence',
                    'title'   => 'Gene Sequence',
                    'content' => 'sequence_tools/gene_sequence.tt',
                },
            ],
        }
    ;

    return RenderHTMLFullPage($html_parameters);
}




# Script options
#################

=pod

=head1 OPTIONS

    custom_sequence.cgi ?p=id &cseq_id=<SEQUENCE_ID>

=head2 Parameters

=over 4

=item B<cseq_id> SEQUENCE_ID (integer):

Custom sequence identifier in database (custom_sequences.id).

=back

=cut


# CODE START
#############

# hash for function selection.
my $actions =
    {
        ''   => \&RenderCustomSequenceIdCard,
        'id' => \&RenderCustomSequenceIdCard,
    };

# try block
eval
{
    # display page
    print ProceedPage($actions);
};

HandleErrors();

exit(0);

# CODE END
###########


=pod

=head1 AUTHORS

Mathieu ROUARD (Bioversity), m.rouard@cgiar.org

Valentin GUIGNON (Bioversity), v.guignon@cgiar.org

Matthieu CONTE (Syngenta)

=head1 VERSION

Version 1.0.0

Date 07/03/2014

=head1 SEE ALSO

GreenPhyl documentation.

=cut
