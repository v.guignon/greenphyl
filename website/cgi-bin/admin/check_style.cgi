#!/usr/bin/perl

=pod

=head1 NAME

check_style.cgi - Display available styles

=head1 SYNOPSIS

    check_style.cgi

=head1 REQUIRES

Perl5

=head1 DESCRIPTION

Display available styles and allows to check all styles displays correctly.

=cut

use strict;
use warnings;

use lib '../../lib';
use lib '../../local_lib';
use Carp qw (cluck confess croak);

use Greenphyl;
use Greenphyl::Web;
use Greenphyl::Web::Access;
use Greenphyl::Species;




# Script global functions
##########################

=pod

=head1 FUNCTIONS

=head2 RenderStylePage

B<Description>: Renders page of GreenPhyl available styles

B<ArgsCount>: 0

B<Return>: (string)

Returns an HTML string containing the GreenPhyl page for styles.

=cut

sub RenderStylePage
{
    my $species_list = [Greenphyl::Species->new(GetDatabaseHandler(), {'selectors' => {'display_order' => ['>', 0]} })];
    return RenderHTMLFullPage(
        {
            'title'          => 'GreenPhyl Styles',
            'content'        => 'admin_tools/check_style.tt',
            'species_list'   => $species_list,            
            'access_message' => 'Style page is only available to administrators.',
            'require_access' => [GP('ACCESS_KEY_ADMINISTRATOR')],
            'access_needs'   => 'all',
        },
    );
}




# Script options
#################

=pod

=head1 OPTIONS

    check_style.cgi

=cut


# CODE START
#############

# hash for function selection.
my $actions =
    {
        '' => \&RenderStylePage,
    };

# try block
eval
{
    # display page
    print ProceedPage($actions);
};

HandleErrors();

exit(0);

# CODE END
###########


=pod

=head1 AUTHORS

Mathieu ROUARD (Bioversity), m.rouard@cgiar.org

Valentin GUIGNON (Bioversity), v.guignon@cgiar.org

Matthieu CONTE (Syngenta)

Sebastien BRIOIS (Syngenta)

=head1 VERSION

Version 1.0.0

Date 05/10/2012

=head1 SEE ALSO

GreenPhyl documentation.

=cut
