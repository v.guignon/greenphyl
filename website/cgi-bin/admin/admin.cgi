#!/usr/bin/perl

=pod

=head1 NAME

admin.cgi - Administration Interface Main Page

=head1 SYNOPSIS

    http://www.greenphyl.org/admin/admin.cgi

=head1 REQUIRES

Perl5

=head1 DESCRIPTION

Displays GreenPhyl administration Interface.

=cut

use strict;
use warnings;

use FindBin;
use lib "$FindBin::Bin/../../lib";
use lib "$FindBin::Bin/../../local_lib";
use Carp qw (cluck confess croak);

use Greenphyl;
use Greenphyl::Web;




# Script global functions
##########################

=pod

=head1 FUNCTIONS

=head2 RenderAdminPage

B<Description>: Renders main administration page.

B<ArgsCount>: 0

B<Return>: (string)

Returns an HTML string containing the GreenPhyl page for administration
interface.

=cut

sub RenderAdminPage
{
    return RenderHTMLFullPage(
        {
            'title'          => 'GreenPhyl Administration',
            'content'        => 'admin_tools/main.tt',
            'access_message' => 'You need administrator priviledges to access to this page!',
            'require_access' => [GP('ACCESS_KEY_ADMINISTRATOR')],
            'access_needs'   => 'any',
        },
    );
}




# Script options
#################

=pod

=head1 OPTIONS

    admin/admin.cgi

=cut


# CODE START
#############

# hash for function selection.
my $actions =
    {
        '' => \&RenderAdminPage,
    };

# try block
eval
{
    # display page
    print ProceedPage($actions);
};

HandleErrors();

exit(0);

# CODE END
###########


=pod

=head1 AUTHORS

Mathieu ROUARD (Bioversity), m.rouard@cgiar.org

Valentin GUIGNON (Bioversity), v.guignon@cgiar.org

Matthieu CONTE (Syngenta)

Sebastien BRIOIS (Syngenta)

=head1 VERSION

Version 1.0.0

Date 10/05/2013

=head1 SEE ALSO

GreenPhyl documentation.

=cut
