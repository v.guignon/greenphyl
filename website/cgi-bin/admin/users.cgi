#!/usr/bin/perl

=pod

=head1 NAME

users.cgi - User management page

=head1 SYNOPSIS

    users.cgi

=head1 REQUIRES

Perl5

=head1 DESCRIPTION

Display an interface to manage users.

=cut

use strict;
use warnings;

use FindBin;
use lib "$FindBin::Bin/../../lib";
use lib "$FindBin::Bin/../../local_lib";
use Carp qw (cluck confess croak);

use Greenphyl;
use Greenphyl::Web;
use Greenphyl::Web::Access;
use Greenphyl::User;
use Greenphyl::Tools::Users;




# Script global constants
##########################

=pod

=head1 CONSTANTS

B<$DEBUG>: (boolean)

When set to true, it enables debug mode.

=cut

our $DEBUG = 0;




# Script global functions
##########################

=pod

=head1 FUNCTIONS

=head2 RenderUserList

B<Description>: Display the list of users in database.

B<ArgsCount>: 0

B<Return>: (string)

Returns an HTML string containing the GreenPhyl page for user list.

=cut

sub RenderUserList
{
    # remove "page" parameter when called from ProceedPage
    if (@_ && ('HASH' eq ref($_[0])))
    {
        shift(@_);
    }
    my ($confirmation_message) = @_;
    my $dbh = GetDatabaseHandler();
    my $users = [Greenphyl::User->new(
            $dbh,
            { 'sql' => {'ORDER BY' => 'id ASC'}, },
        )
    ];

    return RenderHTMLFullPage(
        {
            'title'          => 'User List',
            'before_content' => $confirmation_message,
            'content'        => 'admin_tools/user_list.tt',
            'users'          => $users,
            'access_message' => 'You need administrator priviledges to access to this page!',
            'require_access' => [GP('ACCESS_KEY_ADMINISTRATOR')],
            'access_needs'   => 'any',
        },
    );
}


=pod

=head2 RenderAddUser

B<Description>: Display user creation page.

B<ArgsCount>: 0

B<Return>: (string)

Returns an HTML string containing the user creation page.

=cut

sub RenderAddUser
{
    my $flags_def = {GetTableInfo('users')}->{'flags'}->{'type'};
    $flags_def =~ s/^set\('//;
    $flags_def =~ s/'\)$//;
    my $flags = [split(/','/, $flags_def)];

    return RenderHTMLFullPage(
        {
            'title'          => 'Create New User',
            'content'        => 'admin_tools/user_add.tt',
            'flags'          => $flags,
            'access_message' => 'You need administrator priviledges to access to this page!',
            'require_access' => [GP('ACCESS_KEY_ADMINISTRATOR')],
            'access_needs'   => 'any',
        },
    );
}


=pod

=head2 RenderCreateUser

B<Description>: Save user data into database.

B<ArgsCount>: 0-1

B<Return>: (string)

Returns an HTML string containing a confirmation message.

=cut

sub RenderCreateUser
{
    my $user_id ||= GetParameterValues('user_id') || 0;

    my $dbh = GetDatabaseHandler();
    my $user = Greenphyl::User->new(
            $dbh,
            {'selectors' => {'id' => $user_id,},}
        )
    ;

    my $confirmation_message = "User not saved!";
    if ((!$user) && IsAdmin())
    {
        PrintDebug("Processing user creation");
        # has admin access, get user parameters
        eval
        {
            my $user_data = GetUserDataFromForm($user);
            CheckNewUserData($user_data);

            CreateUser({
                'login'        => $user_data->{'login'},
                'display_name' => $user_data->{'display_name'},
                'email'        => $user_data->{'email'},
                'password'     => $user_data->{'password'},
                'description'  => $user_data->{'description'},
                'flags'        => $user_data->{'flags'},
            });
            $confirmation_message = "User $user_data->{'login'} successfully created!";
        };
        my $error;
        if ($error = Exception::Class->caught())
        {
            $confirmation_message = "Failed to create user: $error";
        }
    }
    
    return RenderUserList($confirmation_message);
}




# Script options
#################

=pod

=head1 OPTIONS

    admin/users.cgi [?p=list]
    admin/users.cgi?p=details&user_id=<ID>
    admin/users.cgi?p=add
    admin/users.cgi?p=save[&user_id=<ID>&...]

=head2 Parameters

=over 4

=item B<user_id> (integer):

User identifier (database identifier).

=back

=cut


# CODE START
#############

# hash for function selection.
my $actions =
    {
        ''               => \&RenderUserList,
        'list'           => \&RenderUserList,
        'add'            => \&RenderAddUser,
        'create'         => \&RenderCreateUser,
    };

# try block
eval
{
    # display page
    print ProceedPage($actions);
};

HandleErrors();

exit(0);

# CODE END
###########


=pod

=head1 DIAGNOSTICS

=head1 AUTHORS

Mathieu ROUARD (Bioversity), m.rouard@cgiar.org

Valentin GUIGNON (Bioversity), v.guignon@cgiar.org

Matthieu CONTE (Syngenta)

Sebastien BRIOIS (Syngenta)

=head1 VERSION

Version 1.0.0

Date 29/08/2013

=head1 SEE ALSO

GreenPhyl documentation.

=cut
