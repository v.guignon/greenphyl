#!/usr/bin/perl

=pod

=head1 NAME

annotations.cgi - Family annotation interface

=head1 SYNOPSIS

    annotations.cgi?family_id=42

=head1 REQUIRES

Perl5

=head1 DESCRIPTION

Displays GreenPhyl family annotation interface. Access to the interface requires
annotator access.

=cut

use strict;
use warnings;

use lib '../../lib';
use lib '../../local_lib';
use Carp qw (cluck confess croak);

use Greenphyl;
use Greenphyl::Web;
use Greenphyl::Web::Access;

use Greenphyl::FamilyAnnotation;
use Greenphyl::Tools::Families;
use Greenphyl::Tools::Taxonomy;
use Greenphyl::Tools::Annotations;



# Script global constants
##########################

=pod

=head1 CONSTANTS

B<$DEBUG>: (boolean)

When set to true, it enables debug mode.

=cut

our $DEBUG = 0;




# Script global variables
##########################

=pod

=head1 VARIABLES

B<$g_dbh>: (DBI::db)

Global database handler.

=cut

my $g_dbh = GetDatabaseHandler();




# Script global functions
##########################

=pod

=head1 FUNCTIONS

=head2 RenderAnnotationInterface

B<Description>: [function description]. #+++

B<ArgsCount>: 0

B<Return>: (string)

Returns an HTML string containing the GreenPhyl page for ... #+++

=cut

sub RenderAnnotationInterface
{
    my ($admin_message) = @_;

    my ($annotations, $parameters) = Greenphyl::Tools::Annotations::GetAnnotationList('html', {'status' => 'submitted',});
    
    # set page parameters
    $parameters->{'title'}    = 'Annotation Management Interface';
    $parameters->{'content'}  = 'admin_tools/annotation_overview.tt';
    $parameters->{'contents'} =
        [
            {
                'id'      => 'tab-submissions',
                'title'   => 'Annotation Submissions',
                'content' => 'admin_tools/annotation_validation.tt',
            },
            {
                'id'      => 'tab-stats',
                'title'   => 'Stats',
                'content' => 'admin_tools/annotation_statistics.tt',
            },
        ];
    $parameters->{'taxonomy_html_tree'} = Greenphyl::Tools::Taxonomy::GetHTMLTaxonomySelectionTree();
    $parameters->{'admin_message'}      = $admin_message;
    $parameters->{'no_filter'}          = (GetParameterValues('no_filter') ? 1 : 0);

    return RenderHTMLFullPage($parameters);

}




=pod

=head2 ProcessAnnotation

B<Description>: [function description]. #+++

B<ArgsCount>: 0

B<Return>: (string)

Returns an HTML string containing the GreenPhyl page for ... #+++

=cut

sub ProcessAnnotation
{
    my $annotation_id = GetParameterValues('annotation_id');
    my $admin_message = {};
    
    # make sure we only work if current user is admin
    if (IsAdmin() && ($annotation_id =~ m/^\d+$/))
    {
        my $annotation = Greenphyl::FamilyAnnotation->new($g_dbh, { 'selectors' => {'id' => $annotation_id,} });

        if (!$annotation)
        {
            $admin_message =
            {
                'type' => 'error',
                'message' => 'Invalid annotation identifier! Unable to process annotation!',
            };
        }
        if (!$annotation->family)
        {
            $admin_message =
            {
                'type' => 'error',
                'message' => 'Invalid annotation: no corresponding family! Unable to process annotation!',
            };
        }
        else
        {
            if (GetParameterValues('accept'))
            {
                # transfer annotation to families table
                if ($annotation->transferAnnotation())
                {
                    # set annotation as validated
                    $annotation->setStatus('validated');
                    if ($annotation->save())
                    {
                        $admin_message =
                        {
                            'type' => 'info',
                            'message' => 'Annotation successfully validated.',
                        };
                    }
                    else
                    {
                        $admin_message =
                        {
                            'type' => 'warning',
                            'message' => 'Unable to validate annotation for ' . $annotation->accession,
                        };
                    }
                }
                else
                {
                    # annotation not transfered
                    $admin_message =
                    {
                        'type' => 'warning',
                        'message' => 'Annotation for ' . $annotation->accession . ' has not been transfered!',
                    };
                }
            }
            elsif (GetParameterValues('deny'))
            {
                $annotation->setStatus('denied');
                if ($annotation->save())
                {
                    $admin_message =
                    {
                        'type' => 'info',
                        'message' => 'Annotation successfully denied.',
                    };
                }
                else
                {
                    $admin_message =
                    {
                        'type' => 'warning',
                        'message' => 'Unable to deny annotation for ' . $annotation->accession,
                    };
                }
            }
            else
            {
                $admin_message =
                {
                    'type' => 'error',
                    'message' => 'Unrecognized annotation processing action!',
                };
            }
        }
    }

    return RenderAnnotationInterface($admin_message);
}




# Script options
#################

=pod

=head1 OPTIONS

#--- describes GET/POST parameters given to the CGI script

=head2 Parameters

=over 4

=item B<[option_name]> ([option nature]): #+++

[option description]. #+++
Default: [option default value if one] #+++

=back
#--- Example:
#---
#--- No parameters requiered or parameters given using POST method.
#---
#--- =over 4
#---
#--- =item B<login> (string):
#---
#--- The user login.
#---
#--- =item B<tracker> (integer):
#---
#--- A tracker used to retrive the data element to display.
#---
#---=back

=cut


# CODE START
#############

# hash for function selection.
my $actions =
    {
        ''       => \&RenderAnnotationInterface,
        'process_annotation' => \&ProcessAnnotation,
    };

# try block
eval
{
    # display page
    print ProceedPage($actions);
};

HandleErrors();

exit(0);

# CODE END
###########


=pod

=head1 DIAGNOSTICS

=head1 AUTHORS

Mathieu ROUARD (Bioversity), m.rouard@cgiar.org

Valentin GUIGNON (Bioversity), v.guignon@cgiar.org

Matthieu CONTE (Syngenta)

Sebastien BRIOIS (Syngenta)

=head1 VERSION

Version 1.0.0

Date 03/10/2012

=head1 SEE ALSO

GreenPhyl documentation.

=cut
