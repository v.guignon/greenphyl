#!/usr/bin/perl

=pod

=head1 NAME

processes.cgi - Manages processes

=head1 SYNOPSIS

    processes.cgi

=head1 REQUIRES

Perl5

=head1 DESCRIPTION

Display an interface to monitor family processing.

=cut

use strict;
use warnings;

use FindBin;
use lib "$FindBin::Bin/../../lib";
use lib "$FindBin::Bin/../../local_lib";
use Carp qw (cluck confess croak);

use Greenphyl;
use Greenphyl::Web;
use Greenphyl::Web::Access;
use Greenphyl::AnalysisProcess;
use Greenphyl::Tools::AnalysisProcesses('optional');
use Greenphyl::Tools::Families;
use Greenphyl::Tools::Taxonomy;




# Script global constants
##########################

=pod

=head1 CONSTANTS

B<$DEBUG>: (boolean)

When set to true, it enables debug mode.

=cut

our $DEBUG = 0;




# Script global functions
##########################

=pod

=head1 FUNCTIONS

=head2 RenderProcessList

B<Description>: Display the list of processes in database.

B<ArgsCount>: 0

B<Return>: (string)

Returns an HTML string containing the GreenPhyl page for process list.

=cut

sub RenderProcessList
{
    my $dbh = GetDatabaseHandler();
    my $processes = Greenphyl::AnalysisProcess->new(
            $dbh,
        );

    return RenderHTMLFullPage(
        {
            'title'          => 'Processes List',
            'content'        => 'admin_tools/process_list.tt',
            'processes'      => $processes,
            'access_message' => 'You need administrator priviledges to access to this page!',
            'require_access' => [GP('ACCESS_KEY_ADMINISTRATOR')],
            'access_needs'   => 'any',
        },
    );
}


=pod

=head2 RenderProcessDetails

B<Description>: Display details and update form for specified process.

B<ArgsCount>: 0

B<Return>: (string)

Returns an HTML string containing the GreenPhyl page for the process details.

=cut

sub RenderProcessDetails
{
    # remove "page" parameter when called from ProceedPage
    if (@_ && ('HASH' eq ref($_[0])))
    {
        shift(@_);
    }

    my ($process_id) = @_;
    $process_id ||= GetParameterValues('process_id') || 0;
    
    my $dbh = GetDatabaseHandler();

    my $process = Greenphyl::AnalysisProcess->new(
            $dbh,
            {
                'selectors' =>
                {
                    'id' => $process_id,
                },
            },
        );

    if (!$process)
    {
        Throw('error' => "Invalid or missing process identifier!", 'log' => "process_id: " . ($process_id?$process_id:''));
    }
    
    
    my ($min_level, $max_level, $min_sequences, $max_sequences);
    foreach my $family (@{$process->families})
    {
        if (!$min_level)
        {
            $min_level = $max_level = $family->level;
            $min_sequences = $max_sequences = $family->sequence_count;
        }
        if ($family->level < $min_level)
        {
            $min_level = $family->level;
        }
        if ($family->level > $max_level)
        {
            $max_level = $family->level;
        }
        if ($family->sequence_count < $min_sequences)
        {
            $min_sequences = $family->sequence_count;
        }
        if ($family->sequence_count > $max_sequences)
        {
            $max_sequences = $family->sequence_count;
        }
    }
    my $level_range = '[?..?]';
    my $sequence_range = '[?..?]';
    if (@{$process->families})
    {
        $level_range    = {'min' => $min_level, 'max' => $max_level,};
        $sequence_range = {'min' => $min_sequences, 'max' => $max_sequences,};
    }

    my $remaining_steps = {};
    my $performed_steps = {};
    # nb.: we processed families before: more efficient as we use caching
    foreach my $processing (@{$process->processing})
    {
        my $step;
        foreach $step (split(/,/, $processing->steps_performed))
        {
            $performed_steps->{$step} ||= 0;
            $performed_steps->{$step}++;
        }
        foreach $step (split(/,/, $processing->steps_to_perform))
        {
            $remaining_steps->{$step} ||= 0;
            $remaining_steps->{$step}++;
        }
    }
    $remaining_steps = [keys(%$remaining_steps)];
    $performed_steps = [keys(%$performed_steps)];


    my $taxonomy_html_tree = GetHTMLTaxonomySelectionTree();

    return RenderHTMLFullPage(
        {
            'title'           => 'Process Details',
            'content'         => 'admin_tools/process_details.tt',
            'process'         => $process,
            'level_range'     => $level_range,
            'sequence_range'  => $sequence_range,
            'performed_steps' => $performed_steps,
            'remaining_steps' => $remaining_steps,
            'taxonomy_html_tree' => $taxonomy_html_tree,
            'process_status' => \%Greenphyl::Tools::AnalysisProcesses::PROCESS_STATUS,
            'family_steps'   => \%Greenphyl::Tools::AnalysisProcesses::FAMILY_STEPS,
            'access_message' => 'You need administrator priviledges to access to this page!',
            'require_access' => [GP('ACCESS_KEY_ADMINISTRATOR')],
            'access_needs'   => 'any',
        },
    );
}


=pod

=head2 RenderNewProcess

B<Description>: Display the process creation form.

B<ArgsCount>: 0

B<Return>: (string)

Returns an HTML string containing the GreenPhyl page for process creation.

=cut

sub RenderNewProcess
{
    my $taxonomy_html_tree = GetHTMLTaxonomySelectionTree();

    return RenderHTMLFullPage(
        {
            'title'          => 'Create New Process',
            'content'        => 'admin_tools/process_new_form.tt',
            'taxonomy_html_tree' => $taxonomy_html_tree,
            'family_steps'   => \%Greenphyl::Tools::AnalysisProcesses::FAMILY_STEPS,
            'access_message' => 'You need administrator priviledges to access to this page!',
            'require_access' => [GP('ACCESS_KEY_ADMINISTRATOR')],
            'access_needs'   => 'any',
        },
    );
}




=pod

=head2 GetSelectedProcessFamiliesStepsAndStatus

B<Description>: .

B<ArgsCount>: 0

B<Return>: (list)

An array of 3 elements:
-a process object (Greenphyl::AnalysisProcess) or an empty object;
-an array (ref) of selected family objects (Greenphyl::Family) or undef;
-an array (ref) of selected steps (string) as in database or undef.

=cut

sub GetSelectedProcessFamiliesStepsAndStatus
{
    my $dbh = GetDatabaseHandler();

    # get parameters
    my $parameter_hash = GetFamilyLoadingParameters();
    my $families;
    if (HaveFamiliesBeenFiltered($parameter_hash))
    {
        $families = LoadFamilies($parameter_hash);
    }

    my $family_status    = GetParameterValues('family_status');

    my @requested_steps_to_perform = GetParameterValues('family_step');

    my $steps_to_perform;
    if (@requested_steps_to_perform)
    {
        $steps_to_perform = [];
        foreach my $step (@requested_steps_to_perform)
        {
            if (exists($Greenphyl::Tools::AnalysisProcesses::FAMILY_STEP_FIELD_VALUE_TO_DB_VALUE{$step}))
            {
                push(@$steps_to_perform, $Greenphyl::Tools::AnalysisProcesses::FAMILY_STEP_FIELD_VALUE_TO_DB_VALUE{$step});
            }
        }
    }

    my $process_id = GetParameterValues('process_id') || 0;
    my $process = Greenphyl::AnalysisProcess->new(
            $dbh,
            {
                'selectors' =>
                {
                    'id' => $process_id,
                },
            },
        );

    return ($process, $families, $steps_to_perform, $family_status);
}


=pod

=head2 RenderSaveProcess

B<Description>: Save modifications on specified process or create a new one and
display process details.

B<ArgsCount>: 0

B<Return>: (string)

Returns an HTML string containing the GreenPhyl page for the process details.

=cut

sub RenderSaveProcess
{
    # make sure we only work if current user is admin
    if (!IsAdmin())
    {
        Throw('error' => "You need administrator priviledges to access to this page!");
    }

    my ($process, $families, $steps_to_perform, $family_status) = GetSelectedProcessFamiliesStepsAndStatus();

    my $process_id = GetParameterValues('process_id') || 0;
    my $process_name     = GetParameterValues('process_name');
    my $process_comments = GetParameterValues('process_comments');
    my $process_status   = GetParameterValues('process_status');
    if ($process_status && exists($Greenphyl::Tools::AnalysisProcesses::PROCESS_STATUS_FIELD_VALUE_TO_DB_VALUE{$process_status}))
    {
        $process_status = $Greenphyl::Tools::AnalysisProcesses::PROCESS_STATUS_FIELD_VALUE_TO_DB_VALUE{$process_status};
    }
    elsif (!$process_status || !exists($Greenphyl::Tools::AnalysisProcesses::PROCESS_STATUS{$process_status}))
    {
        $process_status = undef;
    }

    # if not loaded, create a new one
    if (!$process)
    {
        PrintDebug("Creating a new process");
        $process_name     ||= 'Unspecified process';
        $process_comments ||= '';
        $process_status   ||= $Greenphyl::Tools::AnalysisProcesses::PROCESS_STATUS_TO_PERFORM;
        my $process_data =
            {
                'name'     => $process_name,
                'comments' => $process_comments,
                'status'   => $process_status,
            };
        $process = CreateAnalysisProcess($process_data, $families, $steps_to_perform);
        if (!$process)
        {
            Throw('error' => "Failed to create new process!");
        }
        $process_id = $process->id;
    }
    else
    {
        # update existing process
        if ($process_name)
        {
            $process->name($process_name);
        }
        if ($process_comments)
        {
            $process->comments($process_comments);
        }
        if ($process_status)
        {
            $process->status($process_status);
        }
        if ($families && @$families)
        {
            # add selected families to existing process
            AddFamiliesToProcess($families, $process, $steps_to_perform, $family_status);
        }
        $process->save();
    }

    return RenderProcessDetails($process_id);
}


=pod

=head2 RenderAddFamilies

B<Description>: Save modifications on specified process or create a new one and
display process details.

B<ArgsCount>: 0

B<Return>: (string)

Returns an HTML string containing the GreenPhyl page for the process details.

=cut

sub RenderAddFamilies
{
    # make sure we only work if current user is admin
    if (!IsAdmin())
    {
        Throw('error' => "You need administrator priviledges to access to this page!");
    }

    my ($process, $families, $steps_to_perform, $family_status) = GetSelectedProcessFamiliesStepsAndStatus();

    $family_status ||= $Greenphyl::Tools::AnalysisProcesses::FAMILY_STATUS_PENDING;

    # check if we got something
    if ($process && $families && @$families)
    {
        # add selected families to existing process
        AddFamiliesToProcess($families, $process, $steps_to_perform, $family_status);
    }

    my $process_id = ($process ? $process->id : undef);
    return RenderProcessDetails($process_id);
}


=pod

=head2 RenderCancelFamilies

B<Description>: Save modifications on specified process or create a new one and
display process details.

B<ArgsCount>: 0

B<Return>: (string)

Returns an HTML string containing the GreenPhyl page for the process details.

=cut

sub RenderCancelFamilies
{
    # make sure we only work if current user is admin
    if (!IsAdmin())
    {
        Throw('error' => "You need administrator priviledges to access to this page!");
    }

    my ($process, $families, $steps_to_perform, $family_status) = GetSelectedProcessFamiliesStepsAndStatus();

    # check if we got something
    if ($process)
    {
        if ($families && @$families)
        {
            # just for selected families
            foreach my $family (@$families)
            {
                SetFamilyStatusForProcess($family, $Greenphyl::Tools::AnalysisProcesses::FAMILY_STATUS_CANCELED, $process);
            }
        }
        else
        {
            # on all process families
            SetFamiliesStatusForProcess($process, $Greenphyl::Tools::AnalysisProcesses::FAMILY_STATUS_CANCELED);
        }
    }

    my $process_id = ($process ? $process->id : undef);
    return RenderProcessDetails($process_id);
}


=pod

=head2 RenderAddSteps

B<Description>: Save modifications on specified process or create a new one and
display process details.

B<ArgsCount>: 0

B<Return>: (string)

Returns an HTML string containing the GreenPhyl page for the process details.

=cut

sub RenderAddSteps
{
    # make sure we only work if current user is admin
    if (!IsAdmin())
    {
        Throw('error' => "You need administrator priviledges to access to this page!");
    }

    my ($process, $families, $steps_to_perform, $family_status) = GetSelectedProcessFamiliesStepsAndStatus();

    # check if we got something
    if ($process)
    {
        if ($families && @$families)
        {
            # just for selected families
            foreach my $family (@$families)
            {
                AddFamilyStepsForProcess($family, $steps_to_perform, $process);
            }
        }
        else
        {
            # on all process families
            AddFamiliesStepsForProcess($steps_to_perform, $process);
        }
    }

    my $process_id = ($process ? $process->id : undef);
    return RenderProcessDetails($process_id);
}


=pod

=head2 RenderCancelSteps

B<Description>: Save modifications on specified process or create a new one and
display process details.

B<ArgsCount>: 0

B<Return>: (string)

Returns an HTML string containing the GreenPhyl page for the process details.

=cut

sub RenderCancelSteps
{
    # make sure we only work if current user is admin
    if (!IsAdmin())
    {
        Throw('error' => "You need administrator priviledges to access to this page!");
    }

    my ($process, $families, $steps_to_perform, $family_status) = GetSelectedProcessFamiliesStepsAndStatus();

    # check if we got something
    if ($process)
    {
        if ($families && @$families)
        {
            # just for selected families
            foreach my $family (@$families)
            {
                RemoveFamilyStepsForProcess($family, $steps_to_perform, $process);
            }
        }
        else
        {
            # on all process families
            RemoveFamiliesStepsForProcess($steps_to_perform, $process);
        }
        #+FIXME: mark family done if no more steps?
    }

    my $process_id = ($process ? $process->id : undef);
    return RenderProcessDetails($process_id);
}


=pod

=head2 RenderDoneSteps

B<Description>: Save modifications on specified process or create a new one and
display process details.

B<ArgsCount>: 0

B<Return>: (string)

Returns an HTML string containing the GreenPhyl page for the process details.

=cut

sub RenderDoneSteps
{
    # make sure we only work if current user is admin
    if (!IsAdmin())
    {
        Throw('error' => "You need administrator priviledges to access to this page!");
    }

    my ($process, $families, $steps_to_perform, $family_status) = GetSelectedProcessFamiliesStepsAndStatus();

    # check if we got something
    if ($process)
    {
        if ($families && @$families)
        {
            # just for selected families
            foreach my $family (@$families)
            {
                SetFamilyStepsDoneForProcess($family, $steps_to_perform, $process);
            }
        }
        else
        {
            # on all process families
            SetFamiliesStepsDoneForProcess($steps_to_perform, $process);
        }
        #+FIXME: mark family done if no more steps?
    }

    my $process_id = ($process ? $process->id : undef);
    return RenderProcessDetails($process_id);
}




# Script options
#################

=pod

=head1 OPTIONS

    admin/processes.cgi [?p=list]
    admin/processes.cgi?p=details&process_id=<ID>
    admin/processes.cgi?p=new
    admin/processes.cgi?p=save[&process_id=<ID>&...]

=head2 Parameters

=over 4

=item B<process_id> (integer):

Process identifier (database identifier).

=back

=cut


# CODE START
#############

# hash for function selection.
my $actions =
    {
        ''        => \&RenderProcessList,
        'list'    => \&RenderProcessList,
        'details' => \&RenderProcessDetails,
        'new'     => \&RenderNewProcess,
        'save'    => \&RenderSaveProcess,
        'update'  =>
            {
                ''                => \&RenderProcessDetails,
                'add_families'    => \&RenderAddFamilies,
                'cancel_families' => \&RenderCancelFamilies,
                'add_steps'       => \&RenderAddSteps,
                'cancel_steps'    => \&RenderCancelSteps,
                'done_steps'      => \&RenderDoneSteps,
            },
    };

# try block
eval
{
    if ($@)
    {
        confess $@;
    }
    # display page
    print ProceedPage($actions);
};

HandleErrors();

exit(0);

# CODE END
###########


=pod

=head1 AUTHORS

Mathieu ROUARD (Bioversity), m.rouard@cgiar.org

Valentin GUIGNON (Bioversity), v.guignon@cgiar.org

Matthieu CONTE (Syngenta)

Sebastien BRIOIS (Syngenta)

=head1 VERSION

Version 1.0.0

Date 21/05/2013

=head1 SEE ALSO

GreenPhyl documentation.

=cut
