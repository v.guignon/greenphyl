#!/usr/bin/perl

=pod

=head1 NAME

report.cgi - Displays site status

=head1 SYNOPSIS

    http://www.greenphyl.org/cgi-bin/admin/report.cgi

=head1 REQUIRES

Perl5

=head1 DESCRIPTION

Display site status.

=cut

use strict;
use warnings;

use FindBin;
use lib "$FindBin::Bin/../../lib";
use lib "$FindBin::Bin/../../local_lib";
use Carp qw (cluck confess croak);

use Greenphyl;
use Greenphyl::Web;


# Script global constants
##########################

=pod

=head1 CONSTANTS

B<$DEBUG>: (boolean)

Debug status.

B<%PATH_ACCESS_CHECK>: (hash)

Hash of path (keys) access (values) to check.

B<%PROGRAM_ACCESS_CHECK>: (hash)

Hash of program path (keys) access (values) to check.

B<@PERL_LIB_TO_CHECK>: (array)

Array of Perl libraries to check.

B<@OTHER_CONSTANTS>: (array)

Array of other constants to check.

=cut

our $DEBUG = 0;

# path, see CheckFilePathAccessFailed for values
our %PATH_ACCESS_CHECK = (
    'ROOT_PATH'               => '+r-w+x',
    'BASE_PATH'               => '+r-w+x',
    'CGI_PATH'                => '+r-w+x',
    'IMAGES_PATH'             => '+r-w+x',
    'MEDIA_PATH'              => '~+r~-w~+x',
    'VIDEOS_PATH'             => '~+r~-w~+x',
    'APPLETS_PATH'            => '+r-w+x',
    'TEMPLATES_PATH'          => '+r-w+x',
    'TEMP_OUTPUT_PATH'        => '+r+w+x',
    'SESSIONS_PATH'           => '+r+w+x',
    'ADMIN_SCRIPTS_PATH'      => '~+r~-w~+x',
#    'TEST_PATH'               => '+r-w+x',

    'DATA_PATH'               => '+r-w+x',
    'LOG_PATH'                => '+r+w+x',
    'BLAST_BANKS_PATH'        => '', # remote clustrer path, no local check # '+r-w+x',
    'BBMH_PATH'               => '~+r~-w~+x',
    'HMM_PATH'                => '~+r~-w~+x',
    'MULTI_ALIGN_PATH'        => '~+r~-w~+x',
    'TREES_PATH'              => '~+r~-w~+x',
    'MEME_OUTPUT_PATH'        => '~+r~-w~+x',
    'IPR_STATS_PATH'          => '', # '+r-w+x',
    'DUMP_PATH'               => '~+r~+w~+x',
    'GO_PATH'                 => '~+r~-w~+x',

    'GO_OBO_FILE_PATH'        => '~+r~-w~-x',
    'IPR_TO_GO_FILE_PATH'     => '~+r~-w~-x',
    'UNIPROT_TO_GO_FILE_PATH' => '~+r~-w~-x',
    'GO_PLANT_SLIM_FILE_PATH' => '~+r~-w~-x',
    'ALL_BLAST_BANK'          => '', # remote clustrer path, no local check # '~+r~-w~-x',
    'EC_TO_GO_FILE_PATH'      => '', # '~+r~-w~-x',
    'SPECIES_CODE_PATH'       => '~+r~-w~-x',
);

# programs, see CheckFilePathAccessFailed for values
our %PROGRAM_ACCESS_CHECK = (
    'BLASTALL_COMMAND'         => '-w+x',
    'CLUSTER_BLASTALL_COMMAND' => '-w+x',
#    'HMM_COMMAND'              => '-w+x',
#    'GRAPHIVZ_COMMAND'         => '-w+x',
    'FORMATDB_COMMAND'         => '', # '-w+x',
    'TRANSEQ_COMMAND'          => '', # '-w+x',
    'BACK_TRANSEQ_COMMAND'     => '', # '-w+x',
    'MYSQL_DUMPER'             => '~-w~+x',
    'MEME_COMMAND'             => '', # '-w+x',
    'MAST_COMMAND'             => '', # '-w+x',
    'QSUB_COMMAND'             => '', # '-w+x',
    'QDEL_COMMAND'             => '', # '-w+x',
    'SENDMAIL_PATH'            => '-w+x',
    'JAVA_COMMAND'             => '-w+x',
);

# requiered Perl libraries to check
our @PERL_LIB_TO_CHECK = qw(
    Exception::Class Fatal Getopt::Long Pod::Usage Scalar::Util List::MoreUtils
    IO::String File::Basename Digest::MD5 Template XML::Simple JSON Benchmark
    DBI DBIx::Simple
    CGI CGI::Session HTML::Entities
    LWP::Simple LWP::UserAgent URI::Escape
    Data::Dumper Data::SpreadPagination Data::Tabular::Dumper
    Data::Tabular::Dumper::CSV Data::Tabular::Dumper::Excel
    Data::Tabular::Dumper::XML
    Bio::Seq Bio::SeqIO Bio::Biblio Bio::SearchIO GO::Parser
);

our @OTHER_CONSTANTS = qw(
  DEBUG_MODE VERBOSE_MODE PROXY GO_COVERAGE_LIMIT
  SGE_ROOT SGE_ID_PATH CLUSTER_QUEUE
  SEARCH_MAX_RESULTS GOOGLE_ANALYTICS CONTACTS_EMAIL
  BLASTALL_TABULAR_FORMAT_PARAM BLASTALL_ADDITIONAL_PARAM
  DB_PROFILING DB_PROFILING_CUTOFF
  
  RAW_ALIGNMENT_FILE_SUFFIX MASKING_FILE_SUFFIX FILTERED_FILE_SUFFIX
  PHYLO_XML_TREE_FILE_SUFFIX PHYLO_NEWICK_TREE_FILE_SUFFIX
);

=pod

=head2 CheckFilePathAccessFailed

B<Description>: Check selected access to the given file or path.

B<ArgsCount>: 2

=over 4

=item $file_path: (string) (R)

File path to check.

=item $access: (string) (R)

A concatenation of access to check in a string. Access rights can be:
e: for "exists"
r: for "read";
w: for "write";
x: for "executable";
d: for "directory";

Each access must be prefixed either by a + if the access is required or by a -
if the access must not be available. A ~ can also be used before those prefices
in order to issue a warning rather than an error in case of failure.

For instance, to check if a path is executable, optionaly readable and not
writeable:
'+x~+r-w'

=back


B<Return>: (array ref)

Array of failed status. Status codes are the same as access codes (ie. if '-r'
is tested and failed, '-r' is returned in the list).

=cut

sub CheckFilePathAccessFailed
{
    my ($file_path, $access) = @_;
    
    my @status = ();

    my @access_check = ($access =~ m/(~?[+\-][erwxd])/g);

    foreach my $access_check (@access_check)
    {
        # PrintDebug("Checked '$access_check' for $file_path");
        my ($warn, $sign, $right) = ($access_check =~ m/(~?)([+\-])([erwxd])/);
        my $status;
        eval "\$status = " . ('-' eq $sign ? '!' : '') . "-$right \$file_path;";
        if ($@)
        {
            confess "ERROR: access check failed! $@\n";
        }
        if (!$status)
        {
            push(@status, $access_check);
        }
    }

    return \@status;
}


=pod

=head2 CheckPerlLibraryFailed

B<Description>: Check if the given Perl library is available or not.

B<ArgsCount>: 2

=over 4

=item $library_name: (string) (R)

Name of the library to check.

=back

B<Return>: (boolean)

A true value if the library couldn't be loaded. The value contains the error
message.

=cut

sub CheckPerlLibraryFailed
{
    my ($library_name) = @_;
    eval("require $library_name;");
    return $@;
}


=pod

=head2 RenderStatusReport

B<Description>: Renders status report page.

B<ArgsCount>: 0

B<Return>: (string)

Returns an HTML string containing the GreenPhyl page for status report.

=cut

sub RenderStatusReport
{
    my $active_database = SelectActiveDatabase();

    return RenderHTMLFullPage(
        {
            'title'           => 'Status Report',
            'content'         => 'admin_tools/report.tt',
            'IsReadable'      => sub { return -r shift(); },
            'CheckFilePathAccessFailed' => \&CheckFilePathAccessFailed,
            'CheckPerlLibraryFailed'    => \&CheckPerlLibraryFailed,
            'active_database' => $active_database,
            'applets'         => ['ATVE_APPLET', 'JALVIEW_APPLET', 'TREEFILTER_APPLET'],
            'path_list'       => \%PATH_ACCESS_CHECK,
            'program_list'    => \%PROGRAM_ACCESS_CHECK,
            'library_list'    => \@PERL_LIB_TO_CHECK,
            'other_list'      => \@OTHER_CONSTANTS,
            'access_message'  => 'You need administrator priviledges to access to this page!',
            'require_access'  => [GP('ACCESS_KEY_ADMINISTRATOR')],
            'access_needs'    => 'any',
        },
    );
}




# Script options
#################

=pod

=head1 OPTIONS

    admin/report.cgi

=cut


# CODE START
#############

# hash for function selection.
my $actions =
    {
        ''        => \&RenderStatusReport,
    };

# try block
eval
{
    # display page
    print ProceedPage($actions);
};

HandleErrors();

exit(0);

# CODE END
###########


=pod

=head1 AUTHORS

Mathieu ROUARD (Bioversity), m.rouard@cgiar.org

Valentin GUIGNON (Bioversity), v.guignon@cgiar.org

Matthieu CONTE (Syngenta)

Sebastien BRIOIS (Syngenta)

=head1 VERSION

Version 1.0.0

Date 10/05/2013

=head1 SEE ALSO

GreenPhyl documentation.

=cut
