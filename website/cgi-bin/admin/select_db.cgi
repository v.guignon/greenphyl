#!/usr/bin/perl

=pod

=head1 NAME

select_db.cgi - Select active database

=head1 SYNOPSIS

    select_db.cgi

=head1 REQUIRES

Perl5

=head1 DESCRIPTION

This script can be used to view or change active database during a web session.

=cut

use strict;
use warnings;

use lib "../../lib";
use lib '../../local_lib';
use Carp qw (cluck confess croak);

use Greenphyl;
use Greenphyl::Web;




# Script global constants
##########################

=pod

=head1 CONSTANTS

B<$DEBUG>: (boolean)

When set to true, it enables debug mode.

=cut

our $DEBUG = 0;




# Script global functions
##########################

=pod

=head1 FUNCTIONS

=head2 RenderDBSelectionForm

B<Description>: Renders database selection from with info on active database.

B<ArgsCount>: 0

B<Return>: (string)

Returns an HTML string containing the GreenPhyl page for active database info
and selection.

=cut

sub RenderDBSelectionForm
{

    return RenderHTMLFullPage(
        {
            'title'     => 'Active Database Selection',
            'content'   => 'admin_tools/db_form.tt',
            'databases' => GP('DATABASES'),
            'active_database_index' => SelectActiveDatabase(),
            'before_form_content' => 'admin_tools/db_info.tt',
            'form_data'          =>
                {
                    'identifier' => 'select_db_form',
                    'submit'     => 'Select',
                    # 'method'     => 'GET',
                    'nosubmit'   => 0,
                    'noclear'    => 1,
                },
            'access_message' => 'You need administrator priviledges to access to this page!',
            'require_access' => [GP('ACCESS_KEY_ADMINISTRATOR')],
            'access_needs'   => 'any',
        },
    );
}




# Script options
#################

=pod

=head1 OPTIONS

select_db.cgi?db_index=<db_index>

=head2 Parameters

=over 4

=item B<db_index> (integer) (O):

index of the database to select.

=back

=cut


# CODE START
#############

# hash for function selection.
my $actions =
    {
        '' => \&RenderDBSelectionForm,
    };

# try block
eval
{
    # display page
    print ProceedPage($actions);
};

HandleErrors();

exit(0);

# CODE END
###########


=pod

=head1 DIAGNOSTICS

=head1 AUTHORS

Mathieu ROUARD (Bioversity), m.rouard@cgiar.org

Valentin GUIGNON (Bioversity), v.guignon@cgiar.org

Matthieu CONTE (Syngenta)

Sebastien BRIOIS (Syngenta)

=head1 VERSION

Version 1.0.0

Date 16/01/2013

=head1 SEE ALSO

GreenPhyl documentation.

=cut
