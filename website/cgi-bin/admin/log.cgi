#!/usr/bin/perl

=pod

=head1 NAME

log.cgi

=head1 REQUIRES

Perl5

=head1 DESCRIPTION

Handles Greenphyl logs (filtering and deleting).

GET    cgi-bin/admin/log.cgi -> show logs and filter form. Empty form will not display all logs.
DELETE cgi-bin/admin/log.cgi -> delete selected logs
POST   NOT HANDLED
PUT    NOT HANDLED

=cut

use strict;
use warnings;

use lib '../../lib';
use lib '../../local_lib';
use Greenphyl;
use Greenphyl::Web;
use Greenphyl::Web::Access;
use Greenphyl::Log('nolog' => 1,);
# use Data::Dumper;

my $g_dbh = GetDatabaseHandler();

# REST interface
# 'POST' (create) and 'PUT' (update) not handled
my %handler_for = (
    'GET'    => \&get_logs,
    'DELETE' => \&delete_logs, # can directly be called when using webservice
);
my $request_method = $ENV{'REQUEST_METHOD'};

# When using a web browser: send method=DELETE to simulate a DELETE request
# When using webservice: send a DELETE request directly
if( GetCGI()->param('method') ) {
    $request_method = GetCGI()->param('method');
}

# Get the handler for this request
my $handler = $handler_for{ $request_method } || $handler_for{GET};

# try block
eval
{
    my $response = $handler->();
    print $response;
};
HandleErrors();


# PUBLIC METHODS
#################

sub get_logs
{
    my $form_fields;
    if (GetCurrentUser()->hasAccess(GP('ACCESS_KEY_ADMINISTRATOR')))
    {
        my $from    = GetParameterValues('from');
        my $to      = GetParameterValues('to');
        my $script  = GetParameterValues('script');
        my @pids    = GetParameterValues('pid');
        my @types   = GetParameterValues('types');
        
        my @selectors;
        push( @selectors, { 'pid'    => ['IN', @pids] } )   if @pids;
        push( @selectors, { 'script' => $script } )         unless '' eq $script;
        push( @selectors, { 'type'   => ['IN', @types] } )  if scalar @types > 0;
        push( @selectors, { 'time'   => ['>=', $from] } )   unless '' eq $from;
        push( @selectors, { 'time'   => ['<=', $to] } )     unless '' eq $to;
        
        # Ugly: selectors should be joined with AND by default
        splice( @selectors, 1, 0, 'AND' ) if( scalar @selectors > 1 );

        $form_fields = _prepare_form_fields();
        
        # Show logs statisfying sent parameters
        if( scalar @selectors > 0 )
        {
            my ($logs, $pager_data) = GetPagedObjects( 'Greenphyl::Log',
                { 'selectors' => \@selectors }
            );
            
            return RenderHTMLFullPage({
                'content'           => 'admin_tools/log_view.tt',
                'before_content'    => 'admin_tools/log_form.tt',
                'form_fields'       => $form_fields,
                'logs'              => $logs,
                'pager_data'        => $pager_data,
            });
        }
    }

    return RenderHTMLFullPage({
        'content'        => 'admin_tools/log_view.tt',
        'before_content' => 'admin_tools/log_form.tt',
        'access_message' => 'You need administrator priviledges to access logs!',
        'require_access' => [GP('ACCESS_KEY_ADMINISTRATOR')],
        'access_needs'   => 'any',
        'form_fields'    => $form_fields,
    });
}

sub delete_logs
{
    if (GetCurrentUser()->hasAccess(GP('ACCESS_KEY_ADMINISTRATOR')))
    {
        my @selected_logs = GetParameterValues('selected_logs');
        
        my $sql = "DELETE FROM greenphyl_logs WHERE id IN (" . join( ", ", @selected_logs ) . ")" ;
        $g_dbh->do( $sql );
    }

    # Redirect to log page
    get_logs();
}

# PRIVATE METHODS
##################

sub _prepare_form_fields
{
    my $query = "SELECT DISTINCT pid, script FROM greenphyl_logs";
    my $pid_list = $g_dbh->selectall_arrayref( $query, { Slice => {} } );
    
    return { 'pid_list' => $pid_list }
}