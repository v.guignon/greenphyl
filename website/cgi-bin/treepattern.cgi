#!/usr/bin/perl

=pod

=head1 NAME

treepattenr.cgi - Tree Pattern tool page

=head1 SYNOPSIS

    treepattenr.cgi

=head1 REQUIRES

Perl5,TreePattern website

=head1 DESCRIPTION

Display Tree Pattern interface inside GreenPhyl.

=cut

use strict;
use warnings;

use FindBin;
use lib "$FindBin::Bin/../lib";
use lib "$FindBin::Bin/../local_lib";
use Carp qw (cluck confess croak);

use LWP::Simple;

use Greenphyl;
use Greenphyl::Web;




# Script global constants
##########################

=pod

=head1 CONSTANTS

B<$CONSTANT_NAME>: ([constant nature]) #+++

[constant description and use]. #+++

#--- Example:
#--- B<$DEBUG>: (boolean)
#--- When set to true, it enables debug mode.
#--- ...
#--- our $DEBUG = 0;
#---
=cut

#+++ our [$CONSTANT_NAME] = ["value"];




# Script global variables
##########################

=pod

=head1 VARIABLES

B<[g_variable_name]>: ([variable nature]) #+++

[variable description, use and default value]. #+++
Default: [variable default value if one] #+++

#--- Example:
#--- B<$g_output_method>: (integer)
#---
#--- used to store current output method;
#--- 0=raw text (default), 1=image, 2=html.
#---
#---     ...
#---
#--- my $g_output_method = 0;
#---

=cut

#+++ my [$g_variable_name] = ["value"];




# Script global functions
##########################

=pod

=head1 FUNCTIONS

=head2 [SubName] #+++

B<Description>: [function description]. #+++

B<ArgsCount>: [count of arguments] #+++

=over 4

=item $variable_name: ([variable nature]) ([requirements]) #+++ see below

#--- requirement can be:
#--- (R)=required,
#--- (O)=optional
#--- (U)=optional and must be undef if omitted (position of arguments maters)
[variable description]. #+++

=item $variable_name2: ([variable nature]) ([requirements]) #+++

[variable description]. #+++

=back

B<Return>: ([return type]) #+++

[return description]. #+++

B<Example>:

    [example code] #+++

=cut

#+++sub [SubName] #+++
#+++{
#+++    my ([...]) = @_; #+++ add missing arguments
#--- if needed, parameters check:
#+++    # parameters check
#+++    if (0 != @_)
#+++    {
#+++        confess "usage: SubName();"; #+++
#+++    }
#+++}

=pod

=head2 [DefaultRenderFunctionToCall] #+++

B<Description>: [function description]. #+++

B<ArgsCount>: 0

B<Return>: (string)

Returns an HTML string containing the GreenPhyl page for ... #+++

=cut

#+++sub [DefaultRenderFunctionToCall]
#+++{
#+++    #+++ do some stuff...
#+++    my $example_param  = GetParameterValues('example');
#+++    my @example_params = GetParameterValues('examples', $namespace, '[,;\s]+', $allow_file);
#+++    if (...) {Throw('error' => 'I got this error message!', 'log' => 'and that is for log only');}
#+++    # example of page rendering (see also minimal exemple in
#+++    # RenderFunctionToCall):
#+++    return RenderHTMLFullPage(
#+++        {
#+++            'mime'                => 'HTML',
#+++            'header_args'         => ['-cookie' => $cookie],
#+++            'css'                 => ['custom.autocomplete'],
#+++            'remove_css'          => ['jquery.autocomplete'],
#+++            'less'                => ['greenphyl3'],
#+++            'remove_less'         => ['greenphyl'],
#+++            'javascript'          => ['ipr?no_autoload=1'],
#+++            'remove_javascript'   => ['swfobject'],
#+++            'title'               => 'Hello World',
#+++            'before_content'      => '<img class="centered" src="http://www.greenphyl.fr/images/header_line.png"/>',
#+++            'dump_data'           =>
#+++                {
#+++                   'links' => [
#+++                     {
#+++                       'label'       => 'FASTA',
#+++                       'format'      => 'fasta',
#+++                       'object_type' => 'sequences',
#+++                       'file_name'   => 'sequences.fasta',
#+++                       'fields'      => 'seq_id',
#+++                     },
#+++                     {
#+++                       'label'       => 'Excel',
#+++                       'format'      => 'excel',
#+++                       'object_type' => 'sequences',
#+++                       'file_name'   => 'sequences.xls',
#+++                       'fields'      => 'seq_id',
#+++                     },
#+++                     {
#+++                       'label'       => 'CSV',
#+++                       'format'      => 'csv',
#+++                       'object_type' => 'sequences',
#+++                       'file_name'   => 'sequences.csv',
#+++                       'fields'      => 'seq_id',
#+++                     },
#+++                   ],
#+++                   'mode' => 'form',
#+++                   'parameters' => { 'seq_id' => 'At1g14920.1', },
#+++                 },
#+++            'pager_data'          =>
#+++                GeneratePagerData(
#+++                    {
#+++                        'total_entries' => $total_entry_count,
#+++                        'entry_ranges_label' => 'Items per page:',
#+++                    },
#+++                ),
#+++            'before_form_content' => 'pre_form.tt',
#+++            'content'             => 'full_template.tt',
#+++            'form_data'          =>
#+++                {
#+++                    'identifier' => 'my_form',
#+++                    'action'     => 'submitter.cgi',
#+++                    'method'     => 'POST',
#+++                    'enctype'    => 'application/x-www-form-urlencoded',
#+++                    'submit'     => 'Go go go!', #+++ note: submit name will be "my_form_submit"
#+++                    'nosubmit'   => 0,
#+++                    'noclear'    => 1,
#+++                },
#+++            'after_form_content'  => '<div id="from_ajax_result_container"></div>',
#+++            'after_content'       => 'pre_footer.tt',
#+++            'param1'             => 'val1',
#+++            'param2'             => 'val2',
#+++            'param3'             => 'val3',
#+++        },
#+++    );
#+++
#+++}


=pod

=head2 [RenderFunctionToCall] #+++

B<Description>: [function description]. #+++

B<ArgsCount>: 0

B<Return>: (string)

Returns an HTML string containing the GreenPhyl page for ... #+++

=cut

sub RenderTreePattern
{
#    #+++ do some stuff...
#    my $treepattern_page = get(GetURL('treepattern'));
#    if (!$treepattern_page
#        || ($treepattern_page =~ /Apache\/2\.2/))
#    {
#        $treepattern_page = '';
#    }
#    $treepattern_page =~ s/^.*<body>//i;
#    $treepattern_page =~ s/<\/body>.*$//i;
#    $treepattern_page =~ s/<SCRIPT[^>]+raphael\.js[^>]*>[^<]*<\/SCRIPT[^>]*>//i;
#    $treepattern_page =~ s/(["(])img\//$1http:\/\/gohelle.cirad.fr\/phylogeny\/treepattern_beta\/img\//gi;
#    $treepattern_page =~ s/href="index/href="http:\/\/gohelle.cirad.fr\/phylogeny\/treepattern_beta\/index/gi;

    return RenderHTMLFullPage(
        {
            'title'       => 'TreePattern',
            'content'     => 'tools/treepattern_iframe.tt',
#            'javascript'  => ['raphael', 'treetools'],
#            'css'         => ['treetools'],
#            'treepattern' => $treepattern_page,
        },
    );
}




# Script options
#################

=pod

=head1 OPTIONS

#--- describes GET/POST parameters given to the CGI script

=head2 Parameters

=over 4

=item B<[option_name]> ([option nature]): #+++

[option description]. #+++
Default: [option default value if one] #+++

=back
#--- Example:
#---
#--- No parameters requiered or parameters given using POST method.
#---
#--- =over 4
#---
#--- =item B<login> (string):
#---
#--- The user login.
#---
#--- =item B<tracker> (integer):
#---
#--- A tracker used to retrive the data element to display.
#---
#---=back

=cut


# CODE START
#############

# hash for function selection.
my $actions =
    {
        '' => \&RenderTreePattern,
    };

# try block
eval
{
    # display page
    print ProceedPage($actions);
};

HandleErrors();

exit(0);

# CODE END
###########


=pod

=head1 AUTHORS

Mathieu ROUARD (Bioversity), m.rouard@cgiar.org

Valentin GUIGNON (Bioversity), v.guignon@cgiar.org

Matthieu CONTE (Syngenta)

Sebastien BRIOIS (Syngenta)

=head1 VERSION

Version 1.0.0

Date 07/11/2013

=head1 SEE ALSO

GreenPhyl documentation, TreePattern documentation.

=cut
