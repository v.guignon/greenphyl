#!/usr/bin/env perl

use strict;
use warnings;
use Data::Dumper;

use Galaxy;

my $fasta_input        = '~/workspace/input/test.fasta';
my $species_tree_input = '~/workspace/input/viridiplantae_private.xml';

# Get galaxy instance
my $galaxy = Galaxy->new( verbose => 1 );

# Create new history for this run
my $new_history = $galaxy->create_history( 'custom phylogeny' );

# Start the actual upload and keep the returned history dataset association ID (hda)
my $hda_fasta_input        = $new_history->upload_file( $fasta_input );
my $hda_species_tree_input = $new_history->upload_file( $species_tree_input );

# Run workflow
my $workflow    = $galaxy->get_workflow( 'name' => 'Greenphyl Phylogeny' );
my $input_step1 = $workflow->get_step( 'label' => 'Proteic Sequences' );
my $input_step2 = $workflow->get_step( 'label' => 'Species Tree' );

my $details = $workflow->run( $new_history, { 
    $input_step1->{'id'} => $hda_fasta_input->{'id'}, 
    $input_step2->{'id'} => $hda_species_tree_input->{'id'},
});

print Dumper $details;