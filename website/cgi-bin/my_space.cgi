#!/usr/bin/env perl

=pod

=head1 NAME

my_space.cgi - User space page

=head1 SYNOPSIS

    my_space.cgi

=head1 REQUIRES

Perl5

=head1 DESCRIPTION

Display user space page.

=cut

use strict;
use warnings;

use FindBin;
use lib "$FindBin::Bin/../lib";
use lib "$FindBin::Bin/../local_lib";
use Carp qw (cluck confess croak);

use Greenphyl;
use Greenphyl::Web;



# Script global functions
##########################

=pod

=head1 FUNCTIONS

=head2 RenderMySpacePage

B<Description>: Display user space page.

B<ArgsCount>: 0

B<Return>: (string)

Returns an HTML string containing the GreenPhyl user space page.

=cut

sub RenderMySpacePage
{
    return RenderHTMLFullPage(
        {
            'title'               => 'My Space',
            'content'             => 'miscelaneous/my_space.tt',
        },
    );

}




# Script options
#################

=pod

=head1 OPTIONS

    my_space.cgi

=cut


# CODE START
#############

# hash for function selection.
my $actions =
    {
        '' => \&RenderMySpacePage,
    };

# try block
eval
{
    # display page
    print ProceedPage($actions);
};

HandleErrors();

exit(0);

# CODE END
###########


=pod

=head1 AUTHORS

Mathieu ROUARD (Bioversity), m.rouard@cgiar.org

Valentin GUIGNON (Bioversity), v.guignon@cgiar.org

Matthieu CONTE (Syngenta)

Sebastien BRIOIS (Syngenta)

=head1 VERSION

Version 1.0.0

Date 14/01/2014

=head1 SEE ALSO

GreenPhyl documentation.

=cut
