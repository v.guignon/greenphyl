#!/usr/bin/perl

=pod

=head1 NAME

diamond.cgi - Handles Diamond search tool

=head1 SYNOPSIS

    http://www.greenphyl.fr/cgi-bin/diamond.cgi

=head1 REQUIRES

Perl5

=head1 DESCRIPTION

Handles the form for Diamond search, launch Diamond and display the results.

=cut

use strict;
use warnings;

use lib '../lib';
use lib '../local_lib';
use Carp qw (cluck confess croak);

use Benchmark qw( :hireswallclock timeit timestr );
use Bio::SearchIO;

use Greenphyl;
use Greenphyl::Web;
use Greenphyl::Sequence;




# Script global constants
##########################

=pod

=head1 CONSTANTS

B<$BEST_HIT_LIMIT>: (integer)

Maximum number of best Diamond hit to search.

=cut
my $BEST_HIT_LIMIT = 5;
our $DEBUG         = 0;




# Script global variables
##########################

=pod

=head1 VARIABLES

B<$g_dbh>: (DBI::db)

Current database handler.

=cut

my $g_dbh = GetDatabaseHandler();




# Script global functions
##########################

=pod

=head1 FUNCTIONS

=head2 RenderForm

B<Description>: Render Diamond form into an HTML string.

B<ArgsCount>: 0

B<Return>: (string)

Returns an HTML string containing the full GreenPhyl page with the form.

=cut

sub RenderForm
{
    # get available species
    my $sql_query = 'SELECT code AS "value", organism AS "label" FROM species WHERE display_order > 0 ORDER BY code asc;';
    my $db_species = $g_dbh->selectall_arrayref($sql_query, { Slice => {} });

    # database options dropdown
    my $db_options = [
        {
            'label' => 'All',
            'value' => 'ALL_SPECIES',
        },
        {
            'label'   => 'Phylum',
            'options' => [
                {
                    'label' => 'Dicotyledons',
                    'value' => 'Dicotyledons',
                },
                {
                    'label' => 'Monocotyledons',
                    'value' => 'Monocotyledons',
                },
            ],
        },
        {
            'label'   => 'species',
            'options' => $db_species,
        }
    ];

    return RenderHTMLFullPage(
        {
            'title'             => 'Diamond Search',
            'content'           => 'tools/diamond_form.tt',
            'form_data'         =>
                {
                    'identifier' => 'diamond',
                    'action'     => GetURL('current', {'p' => 'results'}),
                    'submit'     => 'Launch Diamond',
                },
            'db_options' => $db_options,
        }
    );
}


=pod

=head2 PrepareDiamondParameters

B<Description>: Prepare Diamond parameters using CGI parameters.

B<ArgsCount>: 0

B<Return>: (list)

the array (ref) of Diamond parameters followed by the used input temporary file
name and the used output temporary file name.

=cut

sub PrepareDiamondParameters
{
    # get and check Diamond parameters
    my @diamond_parameters;

    my $sequence = GetParameterValues('sequence');
    # check FASTA contains only printable characters and line breaks
    if ($sequence !~ m/^[\x20-\x7E\r\n\t]+$/)
    {
        ThrowGreenphylError('error' => "Invalid Diamond sequence! You may have invalid characters in your sequence. Please make sure you entered FASTA sequences with no accents or other special characters in sequence names or contents.", 'log' => "Sequence given:\n$sequence\n");
    }
    elsif ($sequence !~ m/^>/)
    {
        $sequence = ">Unknown sequence\n" . $sequence;
    }

    my $diamond_command = GetParameterValues('command');
    if ($diamond_command !~ m/^(?:blastp|blastx)$/i)
    {
        $diamond_command = 'blastp';
    }
    # Validate sequence content and blast parameter consistency.
    # if (($diamond_command =~ m/^blastp$/i)
    #     && ($sequence =~ /^(?:>[\x20-\x7E\t]*[\r\n]+)?([ACGTURYSWKMBDHVN\-\.\r\n]+)$/i))
    # {
    #     ThrowGreenphylError('error' => "You selected blastp command while you provided DNA sequence. Use blastx instead.\n");
    # }
    # elsif (($diamond_command =~ m/^blastx$/i)
    #     && ($sequence =~ /^(?:>[\x20-\x7E\t]*[\r\n]+)?([ABCDEFGHIKLMNPQRSTVWXYZ\*\-\.\r\n]+)$/i))
    # {
    #     ThrowGreenphylError('error' => "You selected blastx command while you provided protein sequence. Use blastp instead.\n");
    # }
    push(@diamond_parameters, lc($diamond_command));

    my $evalue   = GetParameterValues('evalue');
    if ($evalue !~ m/^[0-9e\-\+\.]{1,8}$/)
    {
        ThrowGreenphylError('error' => "Invalid Diamond e-value.", 'log' => "e-value given:\n$evalue\n");
    }
    push(@diamond_parameters, '-e', $evalue);
    
    # my $database = $url_lib . '/' . GetParameterValues('database');    # Diamond file must have same name as the species name
    my $database = GetParameterValues('database');    # Diamond file must have same name as the species name
    if ($database !~ m/^[\w]{3,16}$/)
    {
        ThrowGreenphylError('error' => "Invalid Diamond bank name.", 'log' => "Bank name given:\n$database\n");
    }
    push(@diamond_parameters, '-d ', GP('BLAST_BANKS_PATH') . '/' . $database); # Diamond file must have same name as the species name

    my $out_number = $BEST_HIT_LIMIT;
    push(@diamond_parameters, '-k', $out_number);

    my $input_temp_file = GP('TEMP_OUTPUT_PATH') . "/tmp_in$$";
    push(@diamond_parameters, '-q', $input_temp_file);

    my $output_temp_file = GP('TEMP_OUTPUT_PATH') . "/tmp_out$$";
    push(@diamond_parameters, '-o', $output_temp_file);

    # Output format to BLAST pairwise.
    push(@diamond_parameters, '-f', '5');
    
    # Specify temp directory.
    push(@diamond_parameters, '-t', GP('TEMP_OUTPUT_PATH'));

    $input_temp_file = WriteTemporaryFile($sequence, $input_temp_file);
    
    return (\@diamond_parameters, $input_temp_file, $output_temp_file);
}


=pod

=head2 LaunchDiamond

B<Description>: Render Diamond form into an HTML string.

B<ArgsCount>: 1

=over 4

=item $diamond_parameters: (array ref) (R)

array of Diamond command line parameters.

=back

B<Return>: (float)

the computation time.

=cut

sub LaunchDiamond
{
    my ($diamond_parameters) = @_;

    PrintDebug("Diamond CMD: " . join(' ', GP('DIAMOND_COMMAND'), @$diamond_parameters));

    # secure (indirect) Diamond call
    my $computation_time = timestr(
        timeit(
            1,
            sub
            {
                my $program_sh;
                # open($program_sh, "-|")
                #     or exec({GP('DIAMOND_COMMAND')}
                #         # GP('DIAMOND_COMMAND'),
                #         @$diamond_parameters
                #     )
                #         or confess "ERROR: unable to launch Diamond! $?, $!\n";
                # if (open($program_sh, "-|")) {
                    if (system(GP('DIAMOND_COMMAND') . ' ' . join(' ', @$diamond_parameters)))
                    {
                        ThrowGreenphylError('error' => "Failed to run Diamond!", 'log' => "$?, $!");
                    }
                # }
                # else
                # {
                #     confess "ERROR: unable to open Diamond! $?, $!\n";
                # }
            }
        )
    );
    return ($computation_time);
}


=pod

=head2 RenderResults

B<Description>: Render Diamond results into an HTML string.

B<ArgsCount>: 0

B<Return>: (string)

Returns an HTML string containing the full GreenPhyl page with the Diamond
results if no error occured.

=cut

sub RenderResults
{
    my ($diamond_parameters, $input_temp_file, $output_temp_file) = PrepareDiamondParameters();
    my ($computation_time) = LaunchDiamond($diamond_parameters);

    # get Diamond output
    my $diamond_hits = [];
    my $raw_diamond_output = '';

    if (-e $output_temp_file)
    {
        my $output_fh;
        if (open($output_fh, $output_temp_file ))
        {
            my $in = new Bio::SearchIO('-format' => 'blastxml', '-file' => $output_temp_file);
            while (my $result = $in->next_result())
            {
                while (my $hit = $result->next_hit())
                {
                    while (my $hsp = $hit->next_hsp())
                    {
                        my @seq_ids = split(/\s+/, $hit->name);

                        for my $seq_id (@seq_ids)
                        {
                            my $sequence = Greenphyl::Sequence->new(
                                GetDatabaseHandler(),
                                {
                                    'selectors' => {'accession' => ['LIKE', $seq_id],},
                                },
                            );
                            # check if in database
                            if (!$sequence)
                            {
                                # not in DB, create a new empty sequence
                                Greenphyl::Sequence->new(
                                    undef,
                                    {
                                        'members' => {'accession' => $seq_id},
                                    },
                                );
                            }
                            push(@$diamond_hits, $sequence);
                        }
                    }
                }
            }
            $raw_diamond_output = join('', <$output_fh>);
            close($output_fh);
        }
        else
        {
            confess "ERROR: failed to open Diamond output '$output_temp_file': $!\n"
        }
    }
    else
    {
        confess "ERROR: failed to get Diamond output file '$output_temp_file'!\n"
    }

    return RenderHTMLFullPage(
        {
            'title'            => "Diamond Results (first $BEST_HIT_LIMIT best hits)",
            'content'          => 'tools/diamond_results.tt',
            'diamond_hits'       => $diamond_hits,
            'computation_time' => $computation_time,
            'raw_diamond_output' => $raw_diamond_output,
        }
    );
}



# Script options
#################

=pod

=head1 OPTIONS

diamond.cgi?p=form

=head2 Parameters

=over 4

=item B<p> (string):

Select the page to display:
'form': displays Diamond form;
'results': displays Diamond results.

Default: 'form'

=back

=cut


# CODE START
#############

# hash for function selection.
my $actions = {
    #+storage issue ''        => \&RenderIssue, #+storage issue
    ''        => \&RenderForm,
    'form'    => \&RenderForm,
    'results' => \&RenderResults,
};

# try block
eval
{
    # display page
    print ProceedPage($actions);
};

HandleErrors();

exit(0);

# CODE END
###########


=pod

=head1 AUTHORS

Mathieu ROUARD (Bioversity), m.rouard@cgiar.org

Valentin GUIGNON (Bioversity), v.guignon@cgiar.org

Matthieu CONTE (Syngenta)

=head1 VERSION

Version 1.0.0

Date 20/04/2012

=head1 SEE ALSO

GreenPhyl documentation.

=cut
