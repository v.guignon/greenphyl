#!/usr/bin/perl

=pod

=head1 NAME

go.pl - Performs GO services

=head1 SYNOPSIS

    go.pl?service=get_go_families&format=html

=head1 REQUIRES

Perl5

=head1 DESCRIPTION

Performs several GO services.

=cut

use strict;
use warnings;

use lib '../lib';
use lib '../local_lib';
use Carp qw (cluck confess croak);

use Greenphyl;
use Greenphyl::Web;
use Greenphyl::Web::PseudoService;
use Greenphyl::Tools::GO;




# Script global constants
##########################

=pod

=head1 CONSTANTS

B<$GO_SERVICES>: (hash ref)

Contains the GO service handlers.

=cut

our $DEBUG = 0;
our $GO_SERVICES =
    {
        'get_go_families'            => \&Greenphyl::Tools::GO::GetGOFamilies,
        'get_go_subtree'             => \&Greenphyl::Tools::GO::GetGOJSONSubtree,
    };




# Script options
#################

=pod

=head1 OPTIONS

    go.pl <service=<service_name>> ...<service parameters>

=head2 Parameters

=over 4

=item B<service> (string):

name of the service.

=back

=cut


# CODE START
#############

HandleService($GO_SERVICES);

exit(0);

# CODE END
###########


=pod

=head1 AUTHORS

Mathieu ROUARD (Bioversity), m.rouard@cgiar.org

Valentin GUIGNON (Bioversity), v.guignon@cgiar.org

Matthieu CONTE (Syngenta)

=head1 VERSION

Version 1.0.0

Date 20/02/2013

=head1 SEE ALSO

GreenPhyl documentation.

=cut
