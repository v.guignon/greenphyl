#!/usr/bin/perl

=pod

=head1 NAME

blast.cgi - Handles BLAST search tool

=head1 SYNOPSIS

    http://www.greenphyl.fr/cgi-bin/blast.cgi

=head1 REQUIRES

Perl5

=head1 DESCRIPTION

Handles the form for BLAST search, launch BLAST and display the results.

=cut

use strict;
use warnings;

use lib '../lib';
use lib '../local_lib';
use Carp qw (cluck confess croak);

use Benchmark qw( :hireswallclock timeit timestr );
use Bio::SearchIO;

use Greenphyl;
use Greenphyl::Web;
use Greenphyl::Sequence;




# Script global constants
##########################

=pod

=head1 CONSTANTS

B<$BEST_HIT_LIMIT>: (integer)

Maximum number of best BLAST hit to search.

=cut
my $BEST_HIT_LIMIT = 5;
our $DEBUG         = 0;




# Script global variables
##########################

=pod

=head1 VARIABLES

B<$g_dbh>: (DBI::db)

Current database handler.

=cut

my $g_dbh = GetDatabaseHandler();




# Script global functions
##########################

=pod

=head1 FUNCTIONS

=head2 RenderForm

B<Description>: Render BLAST form into an HTML string.

B<ArgsCount>: 0

B<Return>: (string)

Returns an HTML string containing the full GreenPhyl page with the form.

=cut

sub RenderForm
{
    # get available species
    my $sql_query = 'SELECT code AS "value", organism AS "label" FROM species WHERE display_order > 0 ORDER BY code asc;';
    my $db_species = $g_dbh->selectall_arrayref($sql_query, { Slice => {} });

    # database options dropdown
    my $db_options = [
        {
            'label' => 'All',
            'value' => 'ALL_SPECIES',
        },
        {
            'label'   => 'Phylum',
            'options' => [
                {
                    'label' => 'Dicotyledons',
                    'value' => 'Dicotyledons',
                },
                {
                    'label' => 'Monocotyledons',
                    'value' => 'Monocotyledons',
                },
            ],
        },
        {
            'label'   => 'species',
            'options' => $db_species,
        }
    ];

    return RenderHTMLFullPage(
        {
            'title'             => 'BLAST Search',
            'content'           => 'tools/blast_form.tt',
            'form_data'         =>
                {
                    'identifier' => 'blast',
                    'action'     => GetURL('current', {'p' => 'results'}),
                    'submit'     => 'Launch BLAST',
                },
            'db_options' => $db_options,
        }
    );
}


=pod

=head2 PrepareBLASTParameters

B<Description>: Prepare BLAST parameters using CGI parameters.

B<ArgsCount>: 0

B<Return>: (list)

the array (ref) of BLAST parameters followed by the used input temporary file
name and the used output temporary file name.

=cut

sub PrepareBLASTParameters
{
    # get and check BLAST parameters
    my @blast_parameters;

    my $sequence = GetParameterValues('sequence');
    # check FASTA contains only printable characters and line breaks
    if ($sequence !~ m/^[\x20-\x7E\r\n\t]+$/)
    {
        ThrowGreenphylError('error' => "Invalid BLAST sequence! You may have invalid characters in your sequence. Please make sure you entered FASTA sequences with no accents or other special characters in sequence names or contents.", 'log' => "Sequence given:\n$sequence\n");
    }

    my $evalue   = GetParameterValues('evalue');
    if ($evalue !~ m/^[0-9e\-\+\.]{1,8}$/)
    {
        ThrowGreenphylError('error' => "Invalid BLAST e-value.", 'log' => "e-value given:\n$evalue\n");
    }
    push(@blast_parameters, '-e', $evalue);
    
    # my $database = $url_lib . '/' . GetParameterValues('database');    # BLAST file must have same name as the species name
    my $database = GetParameterValues('database');    # BLAST file must have same name as the species name
    if ($database !~ m/^[\w]{3,16}$/)
    {
        ThrowGreenphylError('error' => "Invalid BLAST bank name.", 'log' => "Bank name given:\n$database\n");
    }
    push(@blast_parameters, '-d ', GP('BLAST_BANKS_PATH') . '/' . $database); # BLAST file must have same name as the species name

    my $blast_program = GetParameterValues('program');
    if ($blast_program !~ m/^(?:blastp|blastx)$/i)
    {
        $blast_program = 'blastp';
    }
    push(@blast_parameters, '-p', lc($blast_program));

    my $out_number = $BEST_HIT_LIMIT;
    push(@blast_parameters, '-K', $out_number);
    push(@blast_parameters, '-v', $out_number);
    push(@blast_parameters, '-b', $out_number);

    my $input_temp_file = GP('TEMP_OUTPUT_PATH') . "/tmp_in$$";
    push(@blast_parameters, '-i', $input_temp_file);
    push(@blast_parameters, '-F', 'none', '-a', '2');

    my $output_temp_file = GP('TEMP_OUTPUT_PATH') . "/tmp_out$$";
    push(@blast_parameters, '-o', $output_temp_file);

    $input_temp_file = WriteTemporaryFile($sequence, $input_temp_file);
    
    return (\@blast_parameters, $input_temp_file, $output_temp_file);
}


=pod

=head2 LaunchBLAST

B<Description>: Render BLAST form into an HTML string.

B<ArgsCount>: 1

=over 4

=item $blast_parameters: (array ref) (R)

array of BLAST command line parameters.

=back

B<Return>: (float)

the computation time.

=cut

sub LaunchBLAST
{
    my ($blast_parameters) = @_;

    PrintDebug("BLAST CMD: " . join(' ', GP('BLASTALL_COMMAND'), @$blast_parameters));

    # secure (indirect) BLAST call
    my $computation_time = timestr(
        timeit(
            1,
            sub
            {
                my $program_sh;
                open($program_sh, "-|")
                    or exec({GP('BLASTALL_COMMAND')}
                        GP('BLASTALL_COMMAND'),
                        @$blast_parameters
                    )
                        or confess "ERROR: unable to launch BLAST! $?, $!\n";
            }
        )
    );
    return ($computation_time);
}


=pod

=head2 RenderResults

B<Description>: Render BLAST results into an HTML string.

B<ArgsCount>: 0

B<Return>: (string)

Returns an HTML string containing the full GreenPhyl page with the BLAST
results if no error occured.

=cut

sub RenderResults
{
    my ($blast_parameters, $input_temp_file, $output_temp_file) = PrepareBLASTParameters();
    my ($computation_time) = LaunchBLAST($blast_parameters);

    # get BLAST output
    my $blast_hits = [];
    my $raw_blast_output = '';
    if (-e $output_temp_file)
    {
        my $output_fh;
        if (open($output_fh, $output_temp_file ))
        {
            my $in = new Bio::SearchIO('-format' => 'blast', '-file' => $output_temp_file);
            while (my $result = $in->next_result())
            {
                while (my $hit = $result->next_hit())
                {
                    while (my $hsp = $hit->next_hsp())
                    {
                        my @seq_ids = split(/\s+/, $hit->name);

                        for my $seq_id (@seq_ids)
                        {
                            my $sequence = Greenphyl::Sequence->new(
                                GetDatabaseHandler(),
                                {
                                    'selectors' => {'accession' => ['LIKE', $seq_id],},
                                },
                            );
                            # check if in database
                            if (!$sequence)
                            {
                                # not in DB, create a new empty sequence
                                Greenphyl::Sequence->new(
                                    undef,
                                    {
                                        'members' => {'accession' => $seq_id},
                                    },
                                );
                            }
                            push(@$blast_hits, $sequence);
                        }
                    }
                }
            }
            $raw_blast_output = join('', <$output_fh>);
            close($output_fh);
        }
        else
        {
            confess "ERROR: failed to open BLAST output '$output_temp_file': $!\n"
        }
    }
    else
    {
        confess "ERROR: failed to get BLAST output file '$output_temp_file'!\n"
    }

    return RenderHTMLFullPage(
        {
            'title'            => "BLAST Results (first $BEST_HIT_LIMIT best hits)",
            'content'          => 'tools/blast_results.tt',
            'blast_hits'       => $blast_hits,
            'computation_time' => $computation_time,
            'raw_blast_output' => $raw_blast_output,
        }
    );
}



# Script options
#################

=pod

=head1 OPTIONS

blast.cgi?p=form

=head2 Parameters

=over 4

=item B<p> (string):

Select the page to display:
'form': displays BLAST form;
'results': displays BLAST results.

Default: 'form'

=back

=cut


# CODE START
#############

# hash for function selection.
my $actions = {
    ''        => \&RenderForm,
    'form'    => \&RenderForm,
    'results' => \&RenderResults,
};

# try block
eval
{
    # display page
    print ProceedPage($actions);
};

HandleErrors();

exit(0);

# CODE END
###########


=pod

=head1 AUTHORS

Mathieu ROUARD (Bioversity), m.rouard@cgiar.org

Valentin GUIGNON (Bioversity), v.guignon@cgiar.org

Matthieu CONTE (Syngenta)

=head1 VERSION

Version 1.0.0

Date 20/04/2012

=head1 SEE ALSO

GreenPhyl documentation.

=cut
