#!/usr/bin/env perl

=pod

=head1 NAME

search.cgi - Search page

=head1 SYNOPSIS

    search.cgi

=head1 REQUIRES

Perl5

=head1 DESCRIPTION

Display search page.

=cut

use strict;
use warnings;

use FindBin;
use lib "$FindBin::Bin/../lib";
use lib "$FindBin::Bin/../local_lib";
use Carp qw (cluck confess croak);

use Greenphyl;
use Greenphyl::Web;



# Script global functions
##########################

=pod

=head1 FUNCTIONS

=head2 RenderSearchPage

B<Description>: Display search page with search tool icons.

B<ArgsCount>: 0

B<Return>: (string)

Returns an HTML string containing the GreenPhyl search page.

=cut

sub RenderSearchPage
{
    return RenderHTMLFullPage(
        {
            'title'               => 'GreenPhyl Search Tools',
            'content'             => 'miscelaneous/search_page.tt',
        },
    );

}




# Script options
#################

=pod

=head1 OPTIONS

    search.cgi

=cut


# CODE START
#############

# hash for function selection.
my $actions =
    {
        '' => \&RenderSearchPage,
    };

# try block
eval
{
    # display page
    print ProceedPage($actions);
};

HandleErrors();

exit(0);

# CODE END
###########


=pod

=head1 AUTHORS

Mathieu ROUARD (Bioversity), m.rouard@cgiar.org

Valentin GUIGNON (Bioversity), v.guignon@cgiar.org

Matthieu CONTE (Syngenta)

Sebastien BRIOIS (Syngenta)

=head1 VERSION

Version 1.0.0

Date 14/01/2014

=head1 SEE ALSO

GreenPhyl documentation.

=cut
