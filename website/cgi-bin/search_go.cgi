#!/usr/bin/perl

=pod

=head1 NAME

search_go.cgi - Family search using GO

=head1 SYNOPSIS

    search_go.cgi

=head1 REQUIRES

Perl5

=head1 DESCRIPTION

Displays GO search interface for GreenPhyl families.

=cut

use strict;
use warnings;

use lib '../lib';
use lib '../local_lib';
use Carp qw (cluck confess croak);

use Greenphyl;
use Greenphyl::Web;
use Greenphyl::Species;
use Greenphyl::Tools::Families;
use Greenphyl::Dumper;




# Script global variables
##########################

=pod

=head1 VARIABLES

B<$g_dbh>: (DBI::db)

Current database handler.

=cut

my $g_dbh = GetDatabaseHandler();




# Script global functions
##########################

=pod

=head1 FUNCTIONS

=head2 RenderGOSearchInterface

B<Description>: [function description]. #+++

B<ArgsCount>: 0

B<Return>: (string)

Returns an HTML string containing the GreenPhyl page for ... #+++

=cut

sub RenderGOSearchInterface
{
    my @species = sort { $a->name cmp $b->name } Greenphyl::Species->new($g_dbh, {'selectors' => { 'display_order' => ['>', 0], },});
    return RenderHTMLFullPage(
        {
            'title'        => 'Family Search using Gene Ontology',
            'content'      => 'family_tools/go_search_form.tt',
            'species_list' => \@species,
        },
    );
}


=pod

=head2 DumpGOFamilySequencesToFasta

B<Description>: dumps sequences in a FASTA file.

B<ArgsCount>: 0

B<Return>: (string)

Returns the FASTA content.

=cut

sub DumpGOFamilySequencesToFasta
{
    # get selected species
    my @species_ids = GetParameterValues('species_id');
    if (!@species_ids)
    {
        Throw('error' => "No species selected. Nothing to dump!");
    }

    # get families
    if (!GetParameterValues('family_id'))
    {
        Throw('error' => "No family selected. Nothing to dump!");
    }
    my $families = LoadFamilies();

    # get sequences
    my @sequences;
    foreach my $family (@$families)
    {
        push(@sequences, $family->sequences({'selectors' => {'species_id' => ['IN', @species_ids]}}));
    }

    # dump
    my $dump_content = DumpObjects(\@sequences, 'fasta');
    # get header after content in case we got an error during content generation
    # so we can still generate a clean header
    my $header = RenderHTTPFileHeader({'file_name' => 'go_sequences.fasta', 'format' => 'fasta'});
    # to not ouput as attachement:
    # my $header = RenderHTTPFileHeader({'mime' => 'TEXT', 'format' => 'fasta'}); #+debug 
    return $header . $dump_content;
}




# Script options
#################

=pod

=head1 OPTIONS

    search_go.cgi

=head2 Parameters

none

=cut


# CODE START
#############

# hash for function selection.
my $actions =
    {
        '' => \&RenderGOSearchInterface,
        'dump' => \&DumpGOFamilySequencesToFasta,
    };

# try block
eval
{
    # display page
    print ProceedPage($actions);
};

HandleErrors();

exit(0);

# CODE END
###########


=pod

=head1 DIAGNOSTICS

=head1 AUTHORS

Mathieu ROUARD (Bioversity), m.rouard@cgiar.org

Valentin GUIGNON (Bioversity), v.guignon@cgiar.org

Matthieu CONTE (Syngenta)

Sebastien BRIOIS (Syngenta)

=head1 VERSION

Version 1.0.0

Date 06/05/2013

=head1 SEE ALSO

GreenPhyl documentation.

=cut
