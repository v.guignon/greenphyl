#!/usr/bin/perl

=pod

=head1 NAME

sequence.cgi - Display sequence identification card

=head1 SYNOPSIS

    sequence.cgi?p=id&seq_id=42

=head1 REQUIRES

Perl5

=head1 DESCRIPTION

This script displays sequence identification card with sequence details.

=cut

use strict;
use warnings;

use lib '../lib';
use lib '../local_lib';
use Carp qw (cluck confess croak);

use LWP::Simple qw(getstore is_success);

use Greenphyl;
use Greenphyl::Web;

use Greenphyl::Sequence;
use Greenphyl::CachedSequence;
use Greenphyl::Tools::Sequences;
use Greenphyl::Tools::GO;



# Script global constants
##########################

=pod

=head1 CONSTANTS

B<$DEBUG>: (boolean)

When set to true, it enables debug mode.



=cut

our $DEBUG = 0;




# Script global variables
##########################

=pod

=head1 VARIABLES

B<$g_dbh>: (DBI::db)

Current database handler.

=cut

my $g_dbh = GetDatabaseHandler();






# Script global functions
##########################

=pod

=head1 FUNCTIONS

=head2 GetGeneModelLinkData

B<Description>: Returns GBrowse image (gene location on chromosome) and the
GBrowse URL corresponding to the given gene.

B<ArgsCount>: 1

=over 4

=item $sequence: (Greenphyl::Sequence) (R)

Current sequence object.

=back

B<Return>: (list)

If the image is not available, undef is returned, otherwise, 2 strings are
returned: the first one correspond to the image name (stored in
GP('TEMP_OUTPUT_PATH')) and the second one is the GBrowse URL to the gene.

=cut

sub GetGeneModelLinkData
{
    my ($sequence) = @_;

    # check for image update request
    my $update_model = GetParameterValues('update_model');

    # generate a file name
    my $image_filename    = $sequence->locus . '.tmp';
    my $species_tmp_path = GP('TEMP_OUTPUT_PATH') . '/' . $sequence->species->code;
    if (!-e $species_tmp_path)
    {
        mkdir($species_tmp_path);
    }
    my $image_file_path   = $species_tmp_path . '/' . $image_filename;
    my $gbrowse_link_data = $sequence->getGBrowseLink();
    my ($gbrowse_url, $gbrowse_image_url) = ($gbrowse_link_data->{'url'}, $gbrowse_link_data->{'image_url'});

    # check if the image should be fetched again
    if ($update_model || !-r $image_file_path)
    {
        PrintDebug("Using GBrowse image URL: $gbrowse_image_url");
        # get image from GBrowse and store it on the server
        my $http_response_code = getstore($gbrowse_image_url, $image_file_path);
        if (!is_success($http_response_code))
        {
            cluck "ERROR: Failed to fetch GBrowse image from URL '$gbrowse_image_url' (code $http_response_code)!\n";
        }
    }

    # if the server answered correctly but was not able to return an image,
    # clear filename
    if (!-r $image_file_path)
    {
        $image_filename = '';
    }
    else
    {
        # guess file type
        my $image_fh;
        if (open($image_fh, $image_file_path))
        {
            binmode($image_fh);
            my $header;
            read($image_fh, $header, 4);
            close($image_fh);
   
            my $new_name;
            if ($header =~ m/^GIF/i) # GIF
            {
                $new_name = $sequence->locus . '.gif';
            }
            elsif ($header =~ m/PNG/i) # PNG
            {
                $new_name = $sequence->locus . '.png';
            }
            elsif ($header =~ m/^\xff\xd8/) # JPEG
            {
                $new_name = $sequence->locus . '.jpg';
            }
            else
            {
                PrintDebug(sprintf("Unrecognized image file format '\\x%x\\x%x\\x%x\\x%x'!", split(//, $header)));
            }

            if ($new_name)
            {
                # generate a hard link if needed
                if ((-r "$species_tmp_path/$new_name")
                    || !system("ln $image_file_path $species_tmp_path/$new_name"))
                {
                    # everything seems ok
                    $image_filename = $new_name;
                }
            }
        }
    }

    my $label = $gbrowse_link_data->{'label'} || 'Genome Browser';
    return ($image_filename, $gbrowse_url, $label);
}


=pod

=head2 GetGeneModelData

B<Description>: Returns gene model data.

B<ArgsCount>: 1

=over 4

=item $sequence: (Greenphyl::Sequence) (R)

Current sequence object.

=back

B<Return>: (hash ref)

Gene model data.

=cut

sub GetGeneModelData
{
    my ($sequence) = @_;

    my ($image_filename, $gbrowse_url, $gbrowse_label) = GetGeneModelLinkData($sequence);

    my $gene_model_data = {};
    
    $gene_model_data->{'url'}     = $gbrowse_url;
    $gene_model_data->{'label'}   = $gbrowse_label;
    if ($image_filename)
    {
        $gene_model_data->{'image'}   = GetURL('temp') . '/' . $sequence->species->code . '/' . $image_filename;
    }

    return $gene_model_data;
}


=pod

=head2 GetHomologyPredictionsData

B<Description>: Returns gene (predicted) homology data.

B<ArgsCount>: 1

=over 4

=item $sequence: (Greenphyl::Sequence) (R)

Current sequence object.

=back

B<Return>: (hash ref)

Gene homology preditction.

=cut

sub GetHomologyPredictionsData
{
    my ($sequence) = @_;

    my $homology_predictions = {};

    $homology_predictions->{'homology_trees'} = [];
    foreach my $family (@{$sequence->families()})
    {
        if (my $family_tree = GetPhylogenyAnalyzesURL($family, 'xml'))
        {
            push(@{$homology_predictions->{'homology_trees'}}, $family_tree);
        }
    }
    
    # get homologs by homology type
    my %already_homolog; # avoid duplicates (when database has duplicates)
    $homology_predictions->{'homologies'} = {};
    foreach my $homology (@{$sequence->homologies()})
    {
        my ($homolog) = @{$homology->getHomologs($sequence)};
        $homology_predictions->{'homologies'}->{$homology->type} ||= [];
        if (exists($already_homolog{$homolog->id}))
        {
            # duplicate in database: warn admin homology data should be cleaned
            PrintDebug('Warning: duplicate homology (' . $already_homolog{$homolog->id} . ' vs ' . $homology->id . ') for ' . $sequence->name . ' and ' . $homolog->name);
        }
        else
        {
            # keep track of homologs already recorded
            $already_homolog{$homolog->id} = $homology->id;
            #add to homology list
            push(@{$homology_predictions->{'homologies'}->{$homology->type}},
                {
                    'homolog' => $homolog,
                    'homology' => $homology,
                }
            );
        }
    }
    
    # sort homology results, for each type
    foreach my $homology_type (keys(%{$homology_predictions->{'homologies'}}))
    {
        # sort by homology distance
        $homology_predictions->{'homologies'}->{$homology_type} = [sort { $a->{'homology'}->distance <=> $b->{'homology'}->distance } @{$homology_predictions->{'homologies'}->{$homology_type}}];
    }

    return $homology_predictions;
}


=pod

=head2 RenderSequenceIdCard

B<Description>: Renders a single sequence identification card with sequence
details.

B<ArgsCount>: 0

B<Return>: (string)

Returns an HTML string containing the sequence identification card.

=cut

sub RenderSequenceIdCard
{
    my $sequence =  LoadSequence();
    # my $cached_sequence =  Greenphyl::CachedSequence->new(
    #     $g_dbh,
    #     {
    #         'selectors' => { 'id' => $sequence->id, 'genome_id' => $sequence->genome_id, },
    #     },
    # );

    my $sorted_go            = [sort {$a->code cmp $b->code} @{$sequence->go}];
    my $go_groups            = Greenphyl::Tools::GO::GroupGOByType($sorted_go);
    # my $gene_model_data      = GetGeneModelData($sequence);

    my $html_parameters =
        {
            'title'                => 'Gene ' . $sequence,
            'javascript'           => ['raphael', 'ipr'],
            'sequence'             => $sequence,
            'content'              => 'sequence_tools/sequence_details.tt',
            'go_groups'            => $go_groups,

            # tab-specific data
            # 'gene_model'           => $gene_model_data,

            # tabs definition
            'contents' =>
            [
                {
                    'id'      => 'tab-gene-sequence',
                    'title'   => 'Gene Sequence',
                    'content' => 'sequence_tools/gene_sequence.tt',
                },
                {
                    'id'      => 'tab-gene-classification',
                    'title'   => 'Gene Classification',
                    'content' => 'sequence_tools/clustering.tt',
                },
                # {
                #     'id'      => 'tab-gene-model',
                #     'title'   => 'Gene Model',
                #     'content' => 'sequence_tools/gene_model.tt',
                # },
                {
                    'id'      => 'tab-domain-pattern',
                    'title'   => 'Domain Pattern',
                    'content' => 'sequence_tools/domain_pattern.tt',
                },
            ],
        }
    ;

    # Pangene structure.
    if (!$sequence->representative || (@{$sequence->getRepresentedSequences()}))
    {
        push(@{$html_parameters->{'contents'}},
            {
                'id'      => 'tab-pangene',
                'title'   => 'Pangene Composition',
                'content' => 'sequence_tools/pangene_composition.tt',
            },
        );
    }

    # homologies
    my ($composite_sequences, $homo_parameters) = GetHomologList([$sequence]);
    if (@{$homo_parameters->{'similarity_data'}})
    {
        $html_parameters->{'similarity_data'} = $homo_parameters->{'similarity_data'};
        $html_parameters->{'similarity_single_sequence'} = 1;
        $html_parameters->{'similarity_with_species'} = 0;
        push(@{$html_parameters->{'contents'}},
            {
                'id'      => 'tab-homologs',
                'title'   => 'Homolog Predictions',
                'content' => 'sequence_tools/similarity.tt',
            },
        );
    }

    return RenderHTMLFullPage($html_parameters);
}




# Script options
#################

=pod

=head1 OPTIONS

    sequence.cgi ?p=id &seq_id=<SEQUENCE_ID>

=head2 Parameters

=over 4

=item B<seq_id> SEQUENCE_ID (integer):

Sequence identifier in database (sequences.sequence_id).

=back

=cut


# CODE START
#############

# hash for function selection.
my $actions =
    {
        ''   => \&RenderSequenceIdCard,
        'id' => \&RenderSequenceIdCard,
    };

# try block
eval
{
    # display page
    print ProceedPage($actions);
};

HandleErrors();

exit(0);

# CODE END
###########


=pod

=head1 AUTHORS

Mathieu ROUARD (Bioversity), m.rouard@cgiar.org

Valentin GUIGNON (Bioversity), v.guignon@cgiar.org

Matthieu CONTE (Syngenta)

=head1 VERSION

Version 2.0.0

Date 21/09/2012

=head1 SEE ALSO

GreenPhyl documentation.

=cut
