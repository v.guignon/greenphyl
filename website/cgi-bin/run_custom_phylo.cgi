#!/usr/bin/perl

=pod

=head1 NAME

run_custom_phylo.cgi - Adds custom sequences to a family and run analysis

=head1 SYNOPSIS

    run_custom_phylo.cgi

=head1 REQUIRES

Perl5

=head1 DESCRIPTION

+FIXME

=cut

use strict;
use warnings;

use FindBin;
use lib "$FindBin::Bin/../lib";
use lib "$FindBin::Bin/../local_lib";
use Carp qw (cluck confess croak);

use Bio::SeqIO;
use Bio::SearchIO;

use Greenphyl;
use Greenphyl::Web;
use Greenphyl::Species;
use Greenphyl::Tools::Families;
use Greenphyl::Tools::Sequences;
use Greenphyl::Tools::Taxonomy;




# Script global constants
##########################

=pod

=head1 CONSTANTS

B<$DEBUG>: (boolean)
When set to true, it enables debug mode.

=cut

our $DEBUG = 0;
our $MAX_SEQUENCE_COUNT = 100;
our $MAX_PHYLO_SEQUENCE_COUNT = 100;
our $MAX_SEQUENCE_LENGTH = 5000; # aa




# Script global variables
##########################

=pod

=head1 VARIABLES

B<[g_variable_name]>: ([variable nature]) #+++

[variable description, use and default value]. #+++
Default: [variable default value if one] #+++

#--- Example:
#--- B<$g_output_method>: (integer)
#---
#--- used to store current output method;
#--- 0=raw text (default), 1=image, 2=html.
#---
#---     ...
#---
#--- my $g_output_method = 0;
#---

=cut

#+++ my [$g_variable_name] = ["value"];




# Script global functions
##########################

=pod

=head1 FUNCTIONS

=head2 RenderSequenceInputForm

B<Description>: Renders sequence input form.

B<ArgsCount>: 0

B<Return>: (string)

Returns an HTML string containing the user sequence input form.

=cut

sub RenderSequenceInputForm
{
    return RenderHTMLFullPage(
        {
            'title'             => 'Custom Phylogeny',
            'content'           => 'tools/custom_phylo_form.tt',
            'form_data'         =>
                {
                    'identifier' => 'custom_phylo',
                    'action'     => GetURL('current', {'p' => 'results'}),
                    'nosubmit'   => 1,
                    'inline'     => 1,
                },
            'MAX_SEQUENCE_COUNT' => $MAX_SEQUENCE_COUNT,
            'MAX_PHYLO_SEQUENCE_COUNT' => $MAX_PHYLO_SEQUENCE_COUNT,
        },
    );
}


=pod

=head2 CheckAndGetUserSequences

B<Description>: Get user input sequence FASTA and make sure species codes are
there and valid, otherwise throw an error.

B<ArgsCount>: 0

B<Return>: (list)

The first element is the cleaned sequences (with no duplicates) in FASTA format
and the second element is a hash ref on a hash of sequence found.

=cut

sub CheckAndGetUserSequences
{
    # get user sequences
    my $user_sequences_text = GetParameterValues('sequences') ||'';
    my $clean_fasta_text = '';
    my $sequence_count = 0;

    my ($istringfh, $ostringfh);
    open($istringfh, "<", \$user_sequences_text)
        or confess "Could not open string for reading: $!";   # Use this for Perl AFTER 5.8.0 (inclusive)

    open($ostringfh, ">", \$clean_fasta_text)
        or confess "Could not open string for writting: $!";
    
    my $seqio = Bio::SeqIO->new(
            '-fh'     => $istringfh,
            '-format' => 'fasta',
        );
    my $fasta_output = Bio::SeqIO->new(
            '-fh'     => $ostringfh,
            '-format' => 'fasta',
        );
    
    my $user_sequences = {};
    while (my $bio_seq = $seqio->next_seq)
    {
        if (!exists($user_sequences->{$bio_seq->id}))
        {
            $fasta_output->write_seq($bio_seq);
        }
        else
        {
            PrintDebug("Warning: duplicate sequence found: " . $bio_seq->id);
        }

        # check length
        if (length($bio_seq->seq()) > $MAX_SEQUENCE_LENGTH)
        {
            # sequence too long
            Throw('error' => "The sequence '" . $bio_seq->id . "' is too long (" . length($bio_seq->seq()) . "aa) for phylogeny processing (limit: " . $MAX_SEQUENCE_LENGTH . "aa)!");
        }
        
        # check type
        if ('protein' ne $bio_seq->alphabet)
        {
            # invalid sequence type
            Throw('error' => "The sequence '" . $bio_seq->id . "' doesn't appear to be a protein sequence! Only protein sequences are allowed!", 'log' => "Detected sequence type: " . $bio_seq->alphabet);
        }

        $user_sequences->{$bio_seq->id} = $bio_seq->seq();
    }
    
    $sequence_count = keys(%$user_sequences);
    if ($sequence_count > $MAX_SEQUENCE_COUNT)
    {
        # too many sequences
        Throw('error' => "Too many sequences! You provided $sequence_count sequences while the maximum number of sequence allowed is $MAX_SEQUENCE_COUNT.");
    }


    # check FASTA contains only printable characters and line breaks
    # and must include species codes
    my $sequence_name_regexp = '^[\x20-\x7E]+_(' . $Greenphyl::Species::SPECIES_CODE_REGEXP . ')\s*$';
    my %species_codes;
    foreach my $sequence_name (keys(%$user_sequences))
    {
        if ($sequence_name !~ m/$sequence_name_regexp/)
        {
            Throw('error' => "Invalid sequence! You may have invalid characters in your sequence name or did not include the species code. Please make sure you entered FASTA sequences with no accents or other special characters in sequence names or contents and each sequence name has a species code in capital letters (prefixed with an underscore '_') appended to the original sequence name.", 'log' => "Sequence:\n$sequence_name\n");
        }
        PrintDebug("Species to check: $1");
        $species_codes{$1} ||= [];
        push(@{$species_codes{$1}}, $sequence_name);
    }

    # check species
    foreach my $species_code (keys(%species_codes))
    {
        my $species = new Greenphyl::Species(GetDatabaseHandler(), {'selectors' => { 'code' => $species_code, },});
        if (!$species)
        {
            Throw('error' => qq|Invalid species code '$species_code'! Please check on UniProt website <a href="http://www.uniprot.org/docs/speclist">http://www.uniprot.org/docs/speclist</a>.|);
        }
    }

    PrintDebug("Clean input FASTA:\n$clean_fasta_text");

    return ($clean_fasta_text, $user_sequences);
}


=pod

=head2 PrepareBLASTParameters

B<Description>: Prepare BLAST parameters using CGI parameters.

B<ArgsCount>: 1

B<Return>: (list)

the array (ref) of BLAST parameters followed by the used input temporary file
name and the used output temporary file name.

=cut

sub PrepareBLASTParameters
{
    my ($input_sequences) = @_;

    # get and check BLAST parameters
    my @blast_parameters;

    # check FASTA contains only printable characters and line breaks
    # and must include species codes
    my $sequence_name_regexp = '^(?:>[\x20-\x7E]+_' . $Greenphyl::Species::SPECIES_CODE_REGEXP . '[\s\r\n]+[a-zA-Z\*\s\r\n]+)+$';
    if ($input_sequences !~ m/$sequence_name_regexp/)
    {
        Throw('error' => "Invalid BLAST sequence! You may have invalid characters in your sequence or did not include the species code. Please make sure you entered FASTA sequences with no accents or other special characters in sequence names or contents and each sequence name has a species code in capital letters (prefixed with an underscore '_') appended to the original sequence name.", 'log' => "Sequence given:\n$input_sequences\n");
    }

    my $evalue   = '1e-5';
    push(@blast_parameters, '-e', $evalue);
    
    my $database = 'All';
    push(@blast_parameters, '-d ', GP('BLAST_BANKS_PATH') . '/' . $database); # BLAST file must have same name as the species name

    my $blast_program = 'blastp';
    push(@blast_parameters, '-p', lc($blast_program));

    my $out_number = 1; # best hit only
    push(@blast_parameters, '-K', $out_number);
    push(@blast_parameters, '-v', $out_number);
    push(@blast_parameters, '-b', $out_number);

    my $input_temp_file = GP('TEMP_OUTPUT_PATH') . "/tmp_in$$";
    push(@blast_parameters, '-i', $input_temp_file);
    push(@blast_parameters, '-F', 'none', '-a', '2');

    my $output_temp_file = GP('TEMP_OUTPUT_PATH') . "/tmp_out$$";
    push(@blast_parameters, '-o', $output_temp_file);

    $input_temp_file = WriteTemporaryFile($input_sequences, $input_temp_file);
    
    return (\@blast_parameters, $input_temp_file, $output_temp_file);
}


=pod

=head2 LaunchBLAST

B<Description>: Render BLAST form into an HTML string.

B<ArgsCount>: 1

=over 4

=item $blast_parameters: (array ref) (R)

array of BLAST command line parameters.

=back

B<Return>: (float)

the computation time.

=cut

sub LaunchBLAST
{
    my ($blast_parameters) = @_;

    PrintDebug("BLAST CMD: " . join(' ', GP('BLASTALL_COMMAND'), @$blast_parameters));

    # secure (indirect) BLAST call
    my $program_sh;
    open($program_sh, "-|")
        or exec({GP('BLASTALL_COMMAND')}
            GP('BLASTALL_COMMAND'),
            @$blast_parameters
        )
        or confess "ERROR: unable to launch BLAST! $?, $!\n";
}


=pod

=head2 RenderBlastResults

B<Description>: Render BLAST results into an HTML string.

B<ArgsCount>: 0

B<Return>: (string)

Returns an HTML string containing the full GreenPhyl page with the BLAST
results if no error occured.

=cut

sub RenderBlastResults
{
    # get user sequences
    my ($input_sequences, $sequence_hash) = CheckAndGetUserSequences();

    # launch BLAST
    my ($blast_parameters, $input_temp_file, $output_temp_file) = PrepareBLASTParameters($input_sequences);
    LaunchBLAST($blast_parameters);

    # get BLAST output
    my $family_hits = {}; # hash of family accessions as keys
    if (-e $output_temp_file)
    {
        my $output_fh;
        if (open($output_fh, $output_temp_file ))
        {
            my $in = new Bio::SearchIO('-format' => 'blast', '-file' => $output_temp_file);
            while (my $result = $in->next_result())
            {
                while (my $hit = $result->next_hit())
                {
                    while (my $hsp = $hit->next_hsp())
                    {
                        my @seq_ids = split(/\s+/, $hit->name);

                        for my $seq_id (@seq_ids)
                        {
                            my $sequence = Greenphyl::Sequence->new(
                                GetDatabaseHandler(),
                                {
                                    'selectors' => {'accession' => ['LIKE', $seq_id],},
                                },
                            );
                            if ($sequence)
                            {
                                # push(@$family_hits, $sequence->families({'selectors' => {'level' => 1}}));
                                my $root_family = ($sequence->families)[0];
                                # PrintDebug('TEST: ' . $sequence->families . ', ' . $root_family);
                                $root_family = $root_family->[0];
                                $family_hits->{$root_family->accession} ||= {
                                    'family' => $root_family,
                                    'matches' => {},
                                    'fasta' => '',
                                };
                                $family_hits->{$root_family->accession}->{'matches'}->{$result->query_name} = $sequence_hash->{$result->query_name};
                            }
                        }
                    }
                }
            }
            close($output_fh);
        }
        else
        {
            confess "ERROR: failed to open BLAST output '$output_temp_file': $!\n"
        }
    }
    else
    {
        confess "ERROR: failed to get BLAST output file '$output_temp_file'!\n"
    }

    # sort families: most sequence matches first
    my @sorted_family_hits = sort {keys(%{$b->{'matches'}}) <=> keys(%{$a->{'matches'}})} (values(%$family_hits));

    my $user_email = GetParameterValues('user_email');
    return RenderHTMLFullPage(
        {
            'title'            => "Family Search Results",
            'content'          => 'tools/custom_phylo_family.tt',
            'family_hits'      => \@sorted_family_hits,
            'user_email'       => $user_email,
            'form_data'        =>
                {
                    'identifier' => 'to_sequence_selection',
                    'action'     => GetURL('current', {'p' => 'results'}),
                    'submit'     => 'Next',
                    'noclear'    => 1,
                },
        }
    );
}



=pod

=head2 GenerateFASTAFromSequences

B<Description>: Returns a FASTA string from a given hash of sequences.

B<ArgsCount>: 1

=over 4

=item $sequences: (hash ref) (R)

hash of sequences: keys are sequence identifier/accession and values are
sequence content (polypeptide).

=back

B<Return>: (string)

A FASTA containing the sequences.

=cut

sub GenerateFASTAFromSequences
{
    my ($sequences) = @_;

    my $selected_sequences = [];
    
    foreach my $sequence_id (keys(%$sequences))
    {
        my $sequence_param_hash = {
            'load' => 'none',
            'members' => {
                'id' => -1,
                'accession' => $sequence_id,
                'polypeptide' => $sequences->{$sequence_id},
            },
        };
        push(@$selected_sequences, new Greenphyl::Sequence(undef, $sequence_param_hash));
    }

    # generate FASTA: don't try to dump species code as its already in the identifier!
    my $fasta_sequences = Greenphyl::Sequence::DumpFasta($selected_sequences, {'without_species_code' => 1,});

    return $fasta_sequences;
}


=pod

=head2 RenderSequenceSelectionForm

B<Description>: [function description]. #+++

B<ArgsCount>: 0

B<Return>: (string)

Returns an HTML string containing the GreenPhyl page for ... #+++

=cut

sub RenderSequenceSelectionForm
{
    my $family = LoadFamily();
    my $family_composition_data  = GenerateFamilyCompositionData($family);
    
    my $species_tree_details = {};
    foreach my $sequence_count (values(%{$family_composition_data->{'sequence_counts'}}))
    {
        $species_tree_details->{$sequence_count->species_name} = ' (<span class="seq_count">' . $sequence_count->sequence_count . '</span> sequence' . ($sequence_count->sequence_count > 1 ? 's' : '') . ')';
    }
    
    my $taxonomy_html_tree = Greenphyl::Tools::Taxonomy::GetHTMLTaxonomySelectionTree([$family->species], undef, $species_tree_details);

    # prepare sequence selection table
    my $columns = [
        'alias',
        'uniprot',
        #'kegg',
        'ipr',
        # 'annotation',
        'go',
        #'ec',
        'pubmed',
    ];
    
    
    # get matching sequence names
    my @selected_sequence_id = GetParameterValues('match_id', 'f' . $family->id);
    
    # get sequence content
    my $selected_sequences = {};
    foreach my $seq_id (@selected_sequence_id)
    {
        $selected_sequences->{$seq_id} = GetParameterValues($seq_id, 'f' . $family->id);
    }
    
    my $sequence_fasta = GenerateFASTAFromSequences($selected_sequences);

    my $user_email = GetParameterValues('user_email');

    return RenderHTMLFullPage(
        {
            'title'             => 'Family Sequence Selection',
            'content'           => 'tools/custom_phylo_species.tt',
            'javascript'        => ['raphael', 'raphael-zpd', 'g.raphael', 'g.bar', 'chart', 'ipr', 'relationship_graph', 'families', ],
            'family'            => $family,
            # family composition
            'family_composition' => $family_composition_data,
            # species tree for selection
            'taxonomy_html_tree' => $taxonomy_html_tree,
            # exhaustive sequence selection
            'columns'            => $columns,
            'fasta'              => $sequence_fasta,
            'user_email'         => $user_email,
            'selected_sequence_count'  => scalar(@selected_sequence_id),
            'MAX_PHYLO_SEQUENCE_COUNT' => $MAX_PHYLO_SEQUENCE_COUNT,
        },
    );
}

 
=pod

=head2 RenderPhylogeny

B<Description>: [function description]. #+++

B<ArgsCount>: 0

B<Return>: (string)

Returns an HTML string containing the GreenPhyl page for ... #+++

=cut

sub RenderPhylogeny
{

    # get user sequences
    my ($fasta_sequences, $sequence_hash) = CheckAndGetUserSequences();
    my $sequence_count = keys(%$sequence_hash);

    # check for GreenPhyl sequences
    my $selected_sequences;
    eval
    {
        # if no family has been specified, it throws an error that is silented
        # by the eval.
        my $family = LoadFamily();
        my @selected_species_id = GetParameterValues('species_id');
        
        # check if species were selected
        if (@selected_species_id)
        {
            $selected_sequences = [$family->sequences({'selectors' => {'species_id' => ['IN', @selected_species_id]} })];
        }
        else
        {
            # no selection, use all
            $selected_sequences = [$family->sequences()];
        }
    };
    
    # nothing from family, try from sequences
    if (!$selected_sequences)
    {
        eval
        {
            my @seq_ids = GetParameterValues('seq_id');
            if (@seq_ids)
            {
                $selected_sequences = LoadSequences();
            }
        };
    }
    
    # check if we got additional sequences
    if ($selected_sequences)
    {
        $sequence_count += @$selected_sequences;
        PrintDebug("Adding " . scalar(@$selected_sequences) . " sequences to FASTA.");
        $fasta_sequences .= "\n" . Greenphyl::Sequence::DumpFasta($selected_sequences);
        
        # check sequence count for family or sequences
        if (@$selected_sequences > $MAX_PHYLO_SEQUENCE_COUNT)
        {
            # too many sequences
            Throw('error' => qq|Too many sequences for phylogeny processing! You have selected | . scalar(@$selected_sequences) . qq| sequences while the maximum number of sequence allowed is $MAX_PHYLO_SEQUENCE_COUNT. Please select less species in the family you selected.|);
        }
    }


    # check sequence count
    if ($sequence_count > $MAX_PHYLO_SEQUENCE_COUNT)
    {
        # too many sequences
        Throw('error' => qq|Too many sequences for phylogeny processing! You have selected $sequence_count sequences while the maximum number of sequence allowed is $MAX_PHYLO_SEQUENCE_COUNT. Try to filter species or provide less sequence.|);
    }


    my $user_email = GetParameterValues('user_email');

    #+DEBUG
    $fasta_sequences =~ s/>/&gt;/g;
    my $content = qq|TODO: &eacute;crire la partie qui va recevoir le contenu
        ci-dessous pour le transmettre &agrave; Galaxy, obtiendra un identifiant
        de suivi et rafra&icirc;chira automatiquement cette page en attendant les
        r&eacute;sultats.<br/>
        <textarea name="sequences">$fasta_sequences\n</textarea>
        <input type="text" name="user_email" value="$user_email"/>\n|;

    return RenderHTMLFullPage(
        {
            'title'             => 'Phylogeny Results',
            'content'           => $content, # 'tools/custom_phylo_species.tt',
        },
    );
}





# Script options
#################

=pod

=head1 OPTIONS

#--- describes GET/POST parameters given to the CGI script

=head2 Parameters

=over 4

=item B<[option_name]> ([option nature]): #+++

[option description]. #+++
Default: [option default value if one] #+++

=back
#--- Example:
#---
#--- No parameters requiered or parameters given using POST method.
#---
#--- =over 4
#---
#--- =item B<login> (string):
#---
#--- The user login.
#---
#--- =item B<tracker> (integer):
#---
#--- A tracker used to retrive the data element to display.
#---
#---=back

=cut


# CODE START
#############

# hash for function selection.
my $actions = {
    ''        => \&RenderSequenceInputForm,
    'form'    => \&RenderSequenceInputForm,
    'results' => {
        ''             => \&RenderBlastResults,
        'custom_phylo' => \&RenderBlastResults,
        'to_sequence_selection' => \&RenderSequenceSelectionForm,
        'galaxy'       => \&RenderPhylogeny,
    },
};

# try block
eval
{
    # display page
    print ProceedPage($actions);
};

HandleErrors();

exit(0);

# CODE END
###########


=pod

=head1 DIAGNOSTICS

#--- Give and explain here every error message the the script may generate.
#--- Use: (W)=warning (optional), (D)=deprecation (optional),
#---      (S)=severe warning (mandatory), (F)=fatal error (trappable),
#---      (X)=very fatal error (non-trappable).
#--- example:
#---
#--- =over 4
#---
#--- =item *
#---
#--- Negative radius;
#--- (F) Can not draw a circle with a negative radius.
#---
#---=back #+++

=head1 AUTHORS

Mathieu ROUARD (Bioversity), m.rouard@cgiar.org

Valentin GUIGNON (Bioversity), v.guignon@cgiar.org

Matthieu CONTE (Syngenta)

Sebastien BRIOIS (Syngenta)

=head1 VERSION

Version [version.subversion.build] #+++

Date [DD/MM/YYY] #+++

=head1 SEE ALSO

#--- other documentation or objects related to this package
GreenPhyl documentation.

=cut
