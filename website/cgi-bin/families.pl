#!/usr/bin/perl

=pod

=head1 NAME

families.pl - Performs families services

=head1 SYNOPSIS

    families.pl?service=get_family_data;format=html;family_id=42

=head1 REQUIRES

Perl5

=head1 DESCRIPTION

Performs several families services.

=cut

use strict;
use warnings;

use lib '../lib';
use lib '../local_lib';
use Carp qw (cluck confess croak);

use Greenphyl;
use Greenphyl::Web;
use Greenphyl::Web::PseudoService;
use Greenphyl::Tools::Families;




# Script global constants
##########################

=pod

=head1 CONSTANTS

B<$FAMILY_SERVICES>: (hash ref)

Contains the family service handlers.

=cut

our $DEBUG = 0;
our $FAMILY_SERVICES =
    {
        'get_family_data' => \&Greenphyl::Tools::Families::GetFamilyList,
        'get_family_relationship' => \&Greenphyl::Tools::Families::GetFamilyRelationship,
        'get_custom_family_relationship' => \&Greenphyl::Tools::Families::GetCustomFamilyRelationship,
        'get_ipr_relationship' => \&Greenphyl::Tools::Families::GetIPRFamilyRelationship,
        'get_ipr_specificity' => \&Greenphyl::Tools::Families::GetIPRFamilySpecificity,
        'get_go_distribution' => \&Greenphyl::Tools::Families::GetGOFamilyDistribution,
    };


# Script options
#################

=pod

=head1 OPTIONS

    families.pl <service=<service_name>> ...<service parameters>

=head2 Parameters

=over 4

=item B<service> (string):

name of the service.

=back

=cut


# CODE START
#############

HandleService($FAMILY_SERVICES);

exit(0);

# CODE END
###########


=pod

=head1 AUTHORS

Mathieu ROUARD (Bioversity), m.rouard@cgiar.org

Valentin GUIGNON (Bioversity), v.guignon@cgiar.org

Matthieu CONTE (Syngenta)

=head1 VERSION

Version 1.0.0

Date 30/05/2012

=head1 SEE ALSO

GreenPhyl documentation.

=cut
