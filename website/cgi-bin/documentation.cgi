#!/usr/bin/perl

=pod

=head1 NAME

documentation.cgi - display greenphyl database documentation

=head1 SYNOPSIS

    documentation.cgi?p=overview

=head1 REQUIRES

Perl5

=head1 DESCRIPTION

This script present description and methodology that allow use to construct
Greenphyl database.

=cut

use strict;
use warnings;

use lib '../lib';
use lib '../local_lib';
use Carp qw (cluck confess croak);

use Greenphyl;
use Greenphyl::Web;
use Greenphyl::Species;
use Greenphyl::Variables;


# Script global functions
##########################

=pod

=head1 FUNCTIONS

=head2 RenderDocumentation

B<Description>: Renders requested documentation page.

B<ArgsCount>: 0

B<Return>: (string)

Returns an HTML string containing the requested GreenPhyl documentation page.

=cut

sub RenderDocumentation
{
    my $page_name = GetParameterValues('page') || GetParameterValues('p');
    my %allowed_pages = (
        'datasource'  => 1,
        'faq'         => 1,
        'methodology' => 1,
        'overview'    => 1,
        'references'  => 1,
        'team'        => 1,
        'usecases'    => 1,
    );

    my ($species_data, $variables_data);

    if (!$page_name || ($page_name eq 'datasource'))
    {
        $species_data = [Greenphyl::Species->new(GetDatabaseHandler(), {'selectors' => { 'display_order' => ['>', 0], },})];
        $variables_data = {
            'ipr2go_version' => Greenphyl::Variables->new(
                GetDatabaseHandler(),
                {'selectors' => {'name' => 'ipr2go_version'}},
            ),
        };
    }

    if (!exists($allowed_pages{$page_name})
        || !$allowed_pages{$page_name})
    {
        $page_name = 'overview';
    }
    my $page_parameters = {
            'title'        => 'Documentation',
            'content'      => "documentation/$page_name.tt",
            'species_data' => $species_data,
            'variables'    => $variables_data,
        };

    # change appearance when in popup mode
    my $mode = GetPageMode();
    if ($Greenphyl::Web::PAGE_MODE_FRAME eq $mode)
    {
        $page_parameters->{'title'}   = 'Help';
        $page_parameters->{'body_id'} = 'help_body';
    }

    return RenderHTMLFullPage($page_parameters);
}


# Script options
#################

=pod

=head1 OPTIONS

documentation.cgi p=<documentation_page>

=head2 Parameters

=over 4

=item B<p> (string):

name of the requested documentation page.
Can be one of:
-'datasource'
-'faq'
-'methodology'
-'overview'
-'references'
-'team'
-'usecases'
Default: datasource

=back

=cut


# CODE START
#############

# hash for function selection.
my $actions =
    {
        '' => \&RenderDocumentation,
    };

# try block
eval
{
    # display page
    print ProceedPage($actions);
};

HandleErrors();

exit(0);

# CODE END
###########


=pod

=head1 DIAGNOSTICS

=head1 AUTHORS

Mathieu ROUARD (Bioversity), m.rouard@cgiar.org

Valentin GUIGNON (Bioversity), v.guignon@cgiar.org

Matthieu CONTE (Syngenta)

Sebastien BRIOIS (Syngenta)

=head1 VERSION

Version 1.0.0

Date 13/02/2013

=head1 SEE ALSO

GreenPhyl documentation.

=cut
