#!/usr/bin/perl

=pod

=head1 NAME

get_species_data.cgi - Returns a species ID card (or picture)

=head1 SYNOPSIS

    get_species_data.cgi?mode=inner&species=Ricinus%20communis&picture=only

=head1 REQUIRES

Perl5

=head1 DESCRIPTION

Returns a species identification card with database statistics and species
picture. Data can be returned using arguments.

=cut

use strict;
use warnings;

use lib '../lib';
use lib '../local_lib';
use Carp qw(cluck confess croak);

use Greenphyl;
use Greenphyl::Web;
use Greenphyl::Species;




# Script global functions
##########################

=pod

=head1 FUNCTIONS

=head2 RenderSpeciesIDCard

B<Description>: [function description]. #+++

B<ArgsCount>: 0

B<Return>: (string)

Returns an HTML string containing the GreenPhyl page for ... #+++

=cut

sub RenderSpeciesIDCard
{

    my $organism = GetParameterValues('species');
    my $species = Greenphyl::Species->new(
                        GetDatabaseHandler(),
                        {'selectors' => {'organism' => $organism, }},)
    ;
    my $species_picture = GetParameterValues('picture');

    if (!$species)
    {
        Throw('error' => 'Species not found!', 'log' => "Organism: " . (defined($organism)?$organism:'undef'));
    }

    return RenderHTMLFullPage(
        {
            # 'title'           => 'Species Details',
            'content'         => 'species/species_id_card.tt',
            'content_id'      => 'species_id_card',
            'species'         => $species,
            'species_picture' => $species_picture,
            'parent_mode'     => (GetParameterValues('parent_mode') || ''),
        },
    );

}


# CODE START
#############

# hash for function selection.
my $actions =
    {
        '' => \&RenderSpeciesIDCard,
    };

# try block
eval
{
    # display page
    print ProceedPage($actions);
};

HandleErrors();

exit(0);

# CODE END
###########


=pod

=head1 AUTHORS

Mathieu ROUARD (Bioversity), m.rouard@cgiar.org

Valentin GUIGNON (Bioversity), v.guignon@cgiar.org

Matthieu CONTE (Syngenta)

Sebastien BRIOIS (Syngenta)

=head1 VERSION

Version 1.0.0

Date 31/01/2031

=head1 SEE ALSO

GreenPhyl documentation.

=cut
