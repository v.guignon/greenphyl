#!/usr/bin/perl

=pod

=head1 NAME

treepattern.pl - Performs TreePattern services

=head1 SYNOPSIS

    treepattern.pl?

=head1 REQUIRES

Perl5

=head1 DESCRIPTION

Performs several TreePattern services.

=cut

use strict;
use warnings;

use lib '../lib';
use lib '../local_lib';
use Carp qw (cluck confess croak);

use Greenphyl;
use Greenphyl::Web;
use Greenphyl::Web::PseudoService;
use Greenphyl::Family;
use TreePattern;

use JSON;




# Script global constants
##########################

=pod

=head1 CONSTANTS

B<$SEQUENCE_SERVICES>: (hash ref)

Contains the sequence service handlers.

=cut

our $DEBUG = 0;
our $SEQUENCE_SERVICES =
    {
        'submit_job'      => \&SubmitJob,
        'check_job'       => \&CheckJob,
        'get_job_results' => \&GetJobResults,
        'annotate'        => \&AnnotateTree,
    };





# Script global functions
##########################

=pod

=head1 FUNCTIONS

=head2 SubmitJob

B<Description>: Submit a TreePattern job and return its ID in JSON.
HTML (POST/GET) parameters:
pattern: tree pattern to search.

B<ArgsCount>: 0

B<Return>: (string)

A job identifier in a JSON hash using the key 'jobid'.

=cut

sub SubmitJob
{
    my $pattern = GetParameterValues('pattern');
    if (!$pattern)
    {
        return to_json({'error' => 'Pattern missing (argument pattern)!',});
    }
    
    if ($pattern !~ m/;\s*$/)
    {
        $pattern .= ';';
    }
    
    my $parameters = {
        'database' => $TreePattern::DATABASE_GREENPHYL_V5,
        'pattern'  => $pattern,
    };

    my $job_id = TreePattern::SubmitTreePatternJob($parameters);
    my $json = to_json({'jobid' => $job_id,});

    return $json;
}


=pod

=head2 CheckJob

B<Description>: Returns the status of TreePattern job in JSON.
HTML (POST/GET) parameters:
jobid: tree pattern job identifier.

B<ArgsCount>: 0

B<Return>: (string)

An integer in a JSON hash using the key 'status'.
Status codes are the same as the one returned by
TreePattern::CheckTreePatternJob.

=cut

sub CheckJob
{
    my $job_id = GetParameterValues('jobid');
    if (!$job_id)
    {
        return to_json({'error' => 'No job identifier provided (argument jobid)!',});
    }

    my ($status, $real_job_id) = (TreePattern::CheckTreePatternJob($job_id));
    my $json = to_json({'status' => $status, 'tpjobid' => $real_job_id,});

    return $json;
}


=pod

=head2 GetJobResults

B<Description>: Returns the status of TreePattern job in JSON.
HTML (POST/GET) parameters:
jobid: tree pattern job identifier.

B<ArgsCount>: 0

B<Return>: (string)

An integer in a JSON hash using the key 'status'.
Status codes are the same as the one returned by
TreePattern::CheckTreePatternJob.

=cut

sub GetJobResults
{
    my ($output_format) = @_;
    my $job_id = GetParameterValues('jobid');
    
    if (!$job_id)
    {
        if ('html' eq $output_format)
        {
            Throw('error' => 'No job identifier provided (argument jobid)!');
        }
        else
        {
            return to_json({'error' => 'No job identifier provided (argument jobid)!',});
        }
    }

    my ($status, $real_job_id, $tp_database, $pattern) = (TreePattern::CheckTreePatternJob($job_id));
    if (!defined($status) || (0 > $status))
    {
        if ('html' eq $output_format)
        {
            Throw('error' => "An error occurred while running TreePattern (status '$status')!");
        }
        else
        {
            return to_json({'error' => "An error occurred while running TreePattern (status '$status')!",});
        }
    }

    my %accession_to_file;
    my $result = TreePattern::GetTreePatternJobResults({'job' => $job_id});
    $result = [sort map {
            my $filename = $_;
            $_ =~ s/_rap_tree\.xml//;
            if ($_ =~ m/^\d+$/)
            {
                my $accession = sprintf('GP%06d', $_);
                $accession_to_file{$accession} = $filename;
                $_ = $accession;
            }
            else
            {
                $accession_to_file{$_} = $filename;
            }
            $_;
        }
        @$result
    ];

    if ('html' eq $output_format)
    {
        my @families;
        foreach my $accession (@$result)
        {
            my $family = Greenphyl::Family->new(
                GetDatabaseHandler(),
                {'selectors' => {'accession' => $accession,},}
            );
            if ($family)
            {
                push(@families, $family);
            }
            else
            {
                PrintDebug("Warning: family not found: '$accession'");
            }
        }
        return {
            'title'   => '',
            'content' => 'tools/treepattern_matches.tt',
            'matches' => \@families,
            'accession_to_file' => \%accession_to_file,
            'pattern' => $pattern,
        };
    }
    else
    {
        # default to 'json'
        my $json = to_json({'result' => $result,});

        return $json;
    }
}


=pod

=head2 AnnotateTree

B<Description>: .

B<ArgsCount>: 0

B<Return>: (string)

.

=cut

sub AnnotateTree
{
    my ($output_format) = @_;

    my $pattern = GetParameterValues('pattern');
    my $filename = GetParameterValues('file');

    if (!$pattern)
    {
        return to_json({'error' => 'Pattern missing (argument pattern)!',});
    }
    
    if ($pattern !~ m/;\s*$/)
    {
        $pattern .= ';';
    }
    
    my $parameters = {
        'database' => $TreePattern::DATABASE_GREENPHYL_V5,
        'pattern'  => $pattern,
        'file'     => $filename,
    };

    return TreePattern::AnnotateTreeWithPattern($parameters);
}




# Script options
#################

=pod

=head1 OPTIONS

    treepattern.pl <service=<service_name>> ...<service parameters>

=head2 Parameters

=over 4

=item B<service> (string):

name of the service.

=back

=cut


# CODE START
#############

HandleService($SEQUENCE_SERVICES);

exit(0);

# CODE END
###########


=pod

=head1 AUTHORS

Mathieu ROUARD (Bioversity), m.rouard@cgiar.org

Valentin GUIGNON (Bioversity), v.guignon@cgiar.org

Matthieu CONTE (Syngenta)

=head1 VERSION

Version 1.0.0

Date 21/11/2013

=head1 SEE ALSO

GreenPhyl documentation.

=cut
