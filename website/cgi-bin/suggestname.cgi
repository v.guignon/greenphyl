#!/usr/bin/perl

=pod

=head1 NAME

suggestname.cgi - render GreenPhyl "suggest a name" popup and add record in the db

=head1 SYNOPSIS

    suggestname.cgi?p=add_entry&type=family&id=8

=head1 DESCRIPTION


=cut

use strict;
use warnings;

use lib '../lib';
use lib '../local_lib';
use Carp qw (cluck confess croak);

 
use Greenphyl;
use Greenphyl::Web;
use Greenphyl::Family;
use Greenphyl::Tools::Families;


our $DEBUG = 0;

# Script global functions
##########################



=pod

=head2 ShowForm

B<Description>: display "My List" page.

B<ArgsCount>: 0

B<Return>: (string)

The page content.

=cut

sub ShowForm
{
    
    my $family_id = GetParameterValues('family_id');
    my $family = Greenphyl::Family->new(
                GetDatabaseHandler(),
                {
                    'selectors' => {'id' => $family_id,}
                },
            )
        ;
    my ($code, $label) = InitAntiSpam();

    
    return RenderHTMLFullPage(
        {
            'mime'        => 'HTML',
            'less'        => ['iframe'],
            'content'     => 'miscelaneous/suggest_name_form.tt',
            'body_id'     => 'suggest_name_body',
            'form_data'          =>
                {
                    'identifier' => 'suggest_form',
                    'action'     => '?p=bye&amp;mode=frame',                      
                    'submit'     => 'Send',                
                },
            'family' => $family,
            'antispam' => $label,
        },
    );
}





=pod

=head2 

B<Description>: 

B<ArgsCount>: n

=over 4

=item @family_ids: (list of scalars) (O)


=back

B<Return>: (array ref)

An array of family objects.

=cut

sub SaveAnnotation 
{
    # check for robot
    CheckAntiSpam();

    my $family      = LoadFamily();
    my $family_name = GetParameterValues('gfname');
    my $comments    = GetParameterValues('comments');
    
    my $user        = GetParameterValues('username');
    $user           ||= '';
    $user .= ' ('
        . ($ENV{'HTTP_X_FORWARDED_FOR'}
            || $ENV{'HTTP_CLIENT_IP'}
            || $ENV{'REMOTE_ADDR'}
            || 'IP unavailable')
        . ')';
    my $email       = GetParameterValues('email');
    
    PrintDebug("Family: $family\n");
    
    if ($family && $family_name)
    { 
        #init db connexion
        my $dbh = GetDatabaseHandler();
        
        # insert in annot_family
        my $sql = "INSERT INTO family_annotations (user_name, accession, name, comments, status)
                        VALUES (?, ?, ?, ?, ?)";
        
        PrintDebug("QUERY: $sql");
        # need to specify  status_ann to be seen by the admin in the annotation administration page            
        $dbh->do($sql, undef, $user . ($email ? " ($email)" : ''), $family->accession, $family_name, $comments, 'submitted')
            or Throw('error' => 'Failed to save suggested name!', 'log' => 'SQL Error: ' . $dbh->errstr);
        
        #send email with name and email if provided  
        if (GP('CONTACTS_EMAIL'))
        {    
            eval('use MIME::Lite;');
            if (!$@)
            {
                PrintDebug("email send to " . GP('CONTACTS_EMAIL') . " for '$user'" . ($email ? " ($email)" : '') . "\n");
       
                my $msg =  MIME::Lite->new(
                    From     => 'greenphyldb@greenphyl.org',
                    To       => GP('CONTACTS_EMAIL'),
                    Subject  => "Gene family name suggested for $family",
                    Data     => "from user : $user" . ($email ? " ($email)" : '') . "\n $family_name \n $comments \n",
                );
          
                $msg->send;
            }
            else 
            {
                cluck "Warning: unable to send a mail through 'suggest a name' function. MIME::Lite may not be installed on your server\n";
            }
        }     
    }
    
    return  RenderHTMLFullPage(
        {
            'mime'        => 'HTML',
            'less'        => ['iframe'],
            'content'     => 'miscelaneous/suggest_name_form.tt',
            'body_id'     => 'suggest_name_body',
            'thanks'      => 1,
        },
    );   
}




# Script options
#################

=pod

=head1 OPTIONS

Takes optional GET parameters.

=head2 Parameters

=over 4

=item B<p> (string):

The action to perform. It can be one of: show_list, add_entry, remove_entry,

add_entry: add specified entry (using type and id parameters) and 

=item B<type> (string):

Type of the object specified by the identifier. Can be one of:family.

=item B<id> (string):

Identifier of the object to perform a action on.

=back

=cut

# CODE START
#############

# hash for function selection.
my $actions =
    {
        ''      => \&ShowForm,
        'form'  => \&ShowForm,
        'bye'   => \&SaveAnnotation,
    };

# try block
eval
{
    # display page
    print ProceedPage($actions);
};

HandleErrors();

GetSession()->flush;

exit(0);

# CODE END
###########


=pod

=head1 AUTHORS

Mathieu ROUARD (Bioversity), m.rouard@cgiar.org

Valentin GUIGNON (Bioversity), v.guignon@cgiar.org

Matthieu CONTE (Syngenta)

=head1 VERSION

Version 1.0.0

Date 12/07/2012

=head1 SEE ALSO

GreenPhyl documentation.

=cut
