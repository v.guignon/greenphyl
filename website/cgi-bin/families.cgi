#!/usr/bin/perl

=pod

=head1 NAME

families.cgi - display list of gene family

=head1 SYNOPSIS

    /families.cgi?p=list&type=validated

=head1 REQUIRES

Perl5

=head1 DESCRIPTION

This script displays list of families using user-specified filters.

=cut

use strict;
use warnings;

use lib '../lib';
use lib '../local_lib';
use Carp qw (cluck confess croak);
use Greenphyl;
use Greenphyl::Web;
use Greenphyl::Web::Access;
use Greenphyl::Family;
use Greenphyl::AbstractFamily;
use Greenphyl::Dumper;
use Greenphyl::Species;
use Greenphyl::Taxonomy;
use Greenphyl::CompositeObject;

use Greenphyl::Tools::Taxonomy;

# use Data::Dumper;


# Script global constants
##########################

=pod

=head1 CONSTANTS

B<$DEBUG>: (boolean)

Enables debug mode.

=cut

our $DEBUG = 0;
our $FAMILY_PER_PAGE_DEFAULT = 50;
our $LIST_TYPES =
    {
        'all' =>
            {
                'title' => 'All Families',
                'selectors' => sub
                    {
                        return {};
                    },
            },
        'all_families' =>
            {
                'title' => 'All Families',
                'selectors' => sub
                    {
                        return {'level' => ['IS NOT NULL']};
                    },
            },
        'allergen' =>
            {
                'title' => 'Allergen Families',
                'selectors' => sub
                    {
                        return {'description' => ['LIKE', '%allergen protein%'],};
                        # return {'description' => ['LIKE', '%This group may contain allergen protein. Control your sequence on http://www.allergenonline.org/%'],};
                    },
            },
        'enzyme'  =>
            {
                'title' => 'Enzyme Families',
                'selectors' => sub
                    {
                        return [
                            {'name' => ['LIKE', '%ase%'],},
                            'OR',
                            {'description' => ['LIKE', '%KEGG%'],},
                        ];
                    },
            },
        'ipr'  =>
            {
                'title' => 'IPR Related Families',
                'selectors' => sub
                    {
                        return {'description' => ['LIKE', '%IPR%'],};
                    },
            },
        'kegg' =>
            {
                'title' => 'KEGG Related Families',
                'selectors' => sub
                    {
                        return {'EXISTS' => 
                                    "SELECT TRUE
                                     FROM dbxref_family df
                                        JOIN dbxref dx ON (dx.id = df.dbxref_id)
                                     WHERE df.family_id = families.id
                                        AND df.accession LIKE '%ko:K%'
                                     LIMIT 1"};
                    },
            },
        'lead'  =>
            {
                'title' => 'LEAD Families',
                'selectors' => sub
                {
                    return
                    {
                        'level' => 1,
                        'EXISTS' =>
                            'SELECT TRUE
                             FROM sylk sy
                                  JOIN family_sequence sii ON sii.sequence_id = sy.seq_id
                             WHERE sii.family_id = families.id
                             LIMIT 1',
                    };
                },
                'load' => sub
                {
                    return {'level' => 1,};
                }
            },
        'phylum_exclusive'  =>
            {
                'title' => 'Phylum-specific Families <small>(excluding species-specific)</small>',
                'selectors' => \&GetTaxIDSelectors,
            },
        'phylum_inclusive'  =>
            {
                'title' => 'Phylum-specific Families <small>(including species-specific)</small>',
                'selectors' => \&GetInclusiveTaxIDSelectors,
            },
        'plant_specific'  =>
            {
                'title' => 'Plant-specific Families',
                'selectors' => sub
                    {
                        return {'level' => ['IS NOT NULL'],'plant_specific' => 100,};
                    },
            },
        'species'  =>
            {
                'title' => 'Species-specific Families',
                'selectors' => \&GetTaxIDSelectors,
            },
        'tair' =>
            {
                'title' => 'TAIR Families',
                'selectors' => sub
                    {
                        return {'description' => ['LIKE', '%TAIR%'],};
                    },
            },
        'transcription_factor' =>
            {
                'title' => 'Transcription Factor Families',
                'selectors' => sub
                    {
                        return [
                            {'description' => ['LIKE', '%DATF%'],},
                            'OR',
                            {'description' => ['LIKE', '%DAFT%'],},
                            {'description' => ['LIKE', '%DRTF%'],},
                            {'description' => ['LIKE', '%DRFT%'],},
                            {'inferences' => ['LIKE', '%IEA:TF%'],},
                        ];
                    },
            },
        'validated' =>
            {
                'title' => 'Validated Families',
                'selectors' => sub
                    {
                        return {'level' => ['IS NOT NULL'], 'validated' => ['IN', 'high','normal','unknown'],};
                    },
            },
        'annotated' =>
            {
                'title' => 'Annotated Families',
                'selectors' => sub
                    {
                        return {'level' => ['IS NOT NULL'], 'name' => ['NOT IN','',$Greenphyl::AbstractFamily::UNANNOTATED_CLUSTER_NAME],};
                    },
            },
        'processed' =>
            {
                'title' => 'Processed Families',
                'selectors' => \&GetProcessesSelectors,
            },
    };


# Script global variables
##########################

=pod

=head1 VARIABLES

B<$g_dbh>: (DBI::db)

Current database handler.

=cut

my $g_dbh = GetDatabaseHandler();
my $g_current_list_type = undef;




# Script global functions
##########################

=pod

=head1 FUNCTIONS

=head2 PrepareDumpColumns

B<Description>: Prepare the parameter hash for columns that should be dumped.

B<ArgsCount>: 0

B<Return>: (hash ref)

column dump parameter hash. Keys are column labels and values are corresponding
object member path.

=cut

sub PrepareDumpColumns
{
    my $column_index = 'a';
    my $dump_columns =
        {
            $column_index++ . '. ID'          => 'accession',
            $column_index++ . '. Name'        => 'name',
            $column_index++ . '. Description' => 'description',
            $column_index++ . '. Sequences'   => 'sequence_count',
            $column_index++ . '. Annotation Submitted' => 'hasAnnotationSubmitted',
            $column_index++ . '. Validated'   => 'validated',
            $column_index++ . '. Analyzes'    => 'hasAnalyzes',
        };

    return $dump_columns;
}


=pod

=head2 GetListType

B<Description>: Returns the list type structure.

B<ArgsCount>: 0

B<Return>: (hash ref)

List type configuration hash. See $LIST_TYPES values for details.

=cut

sub GetListType
{
    # already computed?
    if ($g_current_list_type)
    {return $g_current_list_type;}

    # get list type and init associated parameters
    my $list_type = GetParameterValues('type');

    if (!$list_type || !exists($LIST_TYPES->{lc($list_type)}))
    {
        $list_type = 'validated';
    }

    return $g_current_list_type = $LIST_TYPES->{lc($list_type)};
}


=pod

=head2 GetTaxIDSelectors

B<Description>: Returns selectors based on given taxonomy ID to select families
specific to that taxonomy ID.

B<ArgsCount>: 0

B<Return>: (hash ref)

A hash of selectors based on taxonomy_id.

=cut

sub GetTaxIDSelectors
{
    # get list of valid taxonomy_id
    my %valid_tax_ids = map {$_ => 1} @{$g_dbh->selectcol_arrayref('SELECT id FROM taxonomy;')};

    # get list type and init associated parameters
    my @tax_ids_param = GetParameterValues('tax_id');
    my %tax_ids;
    foreach my $param_values (@tax_ids_param)
    {
        foreach my $value (split('\D+', $param_values))
        {
            if (exists($valid_tax_ids{$value}))
            {
                $tax_ids{$value} = $value;
            }
            elsif ($value)
            {
                Throw('error' => "Invalid taxonomy identifier '$value'!");
            }
        }
    }
    return { 'taxonomy_id' => ['IN', keys(%tax_ids)], };
}


=pod

=head2 GetInclusiveTaxIDSelectors

B<Description>: Returns selectors based on given taxonomy ID that will
select families specific to the given taxonomy ID or one of the underlying
taxonomy ID.

B<ArgsCount>: 0

B<Return>: (hash ref)

A hash of selectors based on taxonomy_id.

=cut

sub GetInclusiveTaxIDSelectors
{
    # get list of valid taxonomy_id
    my %valid_tax_ids = map {$_ => 1} @{$g_dbh->selectcol_arrayref('SELECT id FROM taxonomy;')};

    # get list type and init associated parameters
    my @tax_ids_param = GetParameterValues('inc_tax_id');
    my %tax_ids;
    foreach my $param_values (@tax_ids_param)
    {
        foreach my $value (split('\D+', $param_values))
        {
            if (exists($valid_tax_ids{$value}))
            {
                $tax_ids{$value} = $value;
            }
            elsif ($value)
            {
                Throw('error' => "Invalid taxonomy identifier '$value'!");
            }
        }
    }
    return [
        {'taxonomy_id' => ['IN', keys(%tax_ids)]},
        'OR',
        {'taxonomy_id' => ['IN', 'SELECT taxonomy_id FROM lineage WHERE lineage_taxonomy_id IN (' . join(', ', keys(%tax_ids)) . ')']},
    ];
}


=pod

=head2 GetProcessesSelectors

B<Description>: Returns selectors for requested processed families.

B<ArgsCount>: 0

B<Return>: (hash ref)

A hash of selectors for processed families.

=cut

sub GetProcessesSelectors
{
    my $process_id    = GetParameterValues('process_id');
    my @family_status = GetParameterValues('status');

    my $and_where_clause = '';
    
    if ($process_id && ($process_id =~ m/^\d{1,5}$/))
    {
        $and_where_clause .= " AND fp.process_id = $process_id";
    }

    my %valid_status;
    foreach my $status (@family_status)
    {
        if ($status =~ m/^(?:pending|in progress|done|canceled|error|warning)$/)
        {
            $valid_status{$status} = 1;
        }
    }
    if (keys(%valid_status))
    {
        $and_where_clause .= " AND fp.status IN ('" . join("', '", keys(%valid_status)) . "')";
    }

    return
    {
        'EXISTS' =>
            "SELECT TRUE
             FROM family_processing fp
             WHERE fp.family_id = families.id
             $and_where_clause
             LIMIT 1",
    };
}


=pod

=head2 MergeSelectors

B<Description>: Merges second selector to the first one.

B<ArgsCount>: 2

=over 4

=item $first_selector: (array/hash ref) (R)

the first selector that will integrate the second one.

=item $second_selector: (array/hash ref) (R)

the second selector.

=back

B<Return>: (selector)

Returns a selector.

=cut

sub MergeSelectors
{
    my ($first_selector, $second_selector) = @_;
    my $new_selector;
    if (('HASH' eq ref($first_selector)) && ('HASH' eq ref($second_selector)))
    {
        # 2 hashes
        # copy first hash
        $new_selector = {map { $_ => $first_selector->{$_} } keys(%$first_selector)};
        # override with keys-values from second hash
        foreach (keys(%$second_selector))
        {
            $new_selector->{$_} = $second_selector->{$_};
        }
    }
    elsif (('ARRAY' eq ref($first_selector)) && ('ARRAY' eq ref($second_selector)))
    {
        # check if we can merge the array using 'AND' or if a 'OR' is used and
        # and the 2 array can't be merged but wrapped into a global array
        my $uses_or = 0;
OR_CHECK_LOOP:
        foreach (@$first_selector, @$second_selector)
        {
            if ($_ =~ m/^OR$/i)
            {
                $uses_or = 1;
            }
            last OR_CHECK_LOOP;
        }

        if ($uses_or)
        {
            # wrapped
            $new_selector = [$first_selector, 'AND', $second_selector];
        }
        else
        {
            # merged
            $new_selector = [@$first_selector, 'AND', @$second_selector];
        }
    }
    else
    {
        if (('ARRAY' eq ref($first_selector) && !@$first_selector)
             || ('HASH' eq ref($first_selector) && !%$first_selector))
        {
            # first selector is empty
            $new_selector = $second_selector;
        }
        elsif (('ARRAY' eq ref($second_selector) && !@$second_selector)
             || ('HASH' eq ref($second_selector) && !%$second_selector))
        {
            # second selector is empty
            $new_selector = $first_selector;
        }
        else
        {
            # join selectors in an array using 'AND'
            $new_selector = [$first_selector, 'AND', $second_selector];
        }
    }
    return $new_selector;
}


=pod

=head2 CheckLevelFilter

B<Description>: Check if a classification filter has been set and activate the
filter if so.

B<ArgsCount>: 2

=over 4

=item $selectors: (array/hash ref) (R)

the selector that will be used to fetch families.

=item $load: (hash ref) (R)

the load parameter that will be used to fetch families.

=back

B<Return>: (list)

Returns a selector and a load hash ref.

=cut

sub CheckLevelFilter
{
    my ($selectors, $load) = @_;
    my @level_filter = GetParameterValues('level');
    if (@level_filter)
    {
        my @levels;
        foreach (@level_filter)
        {
            foreach my $level (split(',', $_))
            {
                # only keep valid levels
                if ($level =~ m/^[1-4]$/)
                {
                    push(@levels, $level);
                }
            }
        }

        $load->{'level'} = 1; # add level to join found_in table
        if (1 == @levels)
        {
            $selectors = MergeSelectors($selectors, {'level' => $levels[0]});
        }
        elsif (@levels)
        {
            $selectors = MergeSelectors($selectors, {'level' => ['IN', @levels]});
        }
    }

    return ($selectors, $load);
}


=pod

=head2 CheckValidationFilter

B<Description>: Check if a validation filter has been set and activate
the filter if so.

B<ArgsCount>: 2

=over 4

=item $selectors: (array/hash ref) (R)

the selector that will be used to fetch families.

=item $load: (hash ref) (R)

the load parameter that will be used to fetch families.

=back

B<Return>: (list)

Returns a selector and a load hash ref.

=cut

sub CheckValidationFilter
{
    my ($selectors, $load) = @_;
    my $validation_filter = join('|', values(%{GP('VALIDATION_STATUS')}), keys(%{GP('VALIDATION_STATUS')}));
    my @validated_filter = GetParameterValues('validated');
    if (@validated_filter)
    {
        my @validated;
        foreach (@validated_filter)
        {
            foreach my $validated_status (split(',', $_))
            {
                # only keep valid levels
                if ($validated_status =~ m/^(?:$validation_filter)$/o)
                {
                    push(@validated, $validated_status);
                }
            }
        }

        if (1 == @validated)
        {
            $selectors = MergeSelectors($selectors, {'validated' => $validated[0]});
        }
        elsif (@validated)
        {
            $selectors = MergeSelectors($selectors, {'validated' => ['IN', @validated]});
        }
    }

    return ($selectors, $load);
}


=pod

=head2 CheckBlackListFilter

B<Description>: Check if a black-list filter has been set and activate
the filter if so.

B<ArgsCount>: 2

=over 4

=item $selectors: (array/hash ref) (R)

the selector that will be used to fetch families.

=item $load: (hash ref) (R)

the load parameter that will be used to fetch families.

=back

B<Return>: (list)

Returns a selector and a load hash ref.

=cut

sub CheckBlackListFilter
{
    my ($selectors, $load) = @_;

    # default: filter black-listed families
    my $black_list_selector = {'black_listed' => 0,};
    # get black-list filter value...
    my $black_listed = GetParameterValues('black_listed');
    if ($black_listed)
    {
        if ($black_listed eq 'yes')
        {
            delete($black_list_selector->{'black_listed'});
        }
        elsif ($black_listed eq 'only')
        {
            $black_list_selector->{'black_listed'} = 1;
        }
    }

    # remove black-list parameter if one is found at first level
    if ('ARRAY' eq ref($selectors))
    {
        foreach my $sub_selector (@$selectors)
        {
            if ('HASH' eq ref($sub_selector))
            {
                delete($sub_selector->{'black_listed'});
            }
        }
    }

    $selectors = MergeSelectors($selectors, $black_list_selector);

    return ($selectors, $load);
}


=pod

=head2 GetQueryParameters

B<Description>: Returns a hash ref containing the query parameters to use to
fetch the families.

B<ArgsCount>: 0

B<Return>: (hash ref)

See Greenphyl::DBObject CONSTRUCTOR documentation ("$parameters" parameter).

=cut

sub GetQueryParameters
{
    # get list type, selectors and fields to load
    my $selectors = {};
    if (exists(GetListType()->{'selectors'}))
    {
        $selectors = GetListType()->{'selectors'}->();
    }
    
    my $load = {};
    if (exists(GetListType()->{'load'}))
    {
        $load = GetListType()->{'load'}->();
    }
    
    # PrintDebug("Family selectors before filtering:\n" . Dumper([$selectors]));

    # check filters...
    # -class filter
    ($selectors, $load) = CheckLevelFilter($selectors, $load);

    # -validated filter
    ($selectors, $load) = CheckValidationFilter($selectors, $load);

    # -black-listed filter
    ($selectors, $load) = CheckBlackListFilter($selectors, $load);
    
    # PrintDebug("Family selectors after filtering:\n" . Dumper([$selectors]));

    my $query_parameters =
        {
            'selectors' => $selectors,
            'load'      => $load,
            'sql' =>
                {
                    'ORDER BY' =>
                        [
                            'CASE
                                WHEN families.validated = 1
                                THEN 9999
                                ELSE 0+families.validated
                                END
                             ASC',
                            'families.id ASC',
                        ],
                },
        };

    return $query_parameters;
}


=pod

=head2 LoadProcessingData

B<Description>: Returns a hash ref containing the query parameters to use to
fetch the families.

B<ArgsCount>: 0

B<Return>: (hash ref)

See Greenphyl::DBObject CONSTRUCTOR documentation ("$parameters" parameter).

=cut

sub LoadProcessingData
{
    my ($families, $process_id, $additional) = @_;
    $additional->{'columns'} ||= [];
    push(@{$additional->{'columns'}},
        {
            'label'  => 'Steps to perform',
            'member' => '0.steps_to_perform',
        },
        {
            'label'  => 'Performed steps',
            'member' => '0.steps_performed',
        },
        {
            'label'  => 'Processing status',
            'member' => '0.status',
        },
    );

    $additional->{'values'} ||= {};
    foreach my $family (@$families)
    {
        $additional->{'values'}->{$family->id} = $family->processing({'selectors' => {'process_id' => $process_id}});
    }
}


=pod

=head2 RenderFamilyList

B<Description>: Renders a list of families.

B<ArgsCount>: 0

B<Return>: (string)

Returns an HTML string containing the Family listing.

=cut

sub RenderFamilyList
{
    my $title = GetListType()->{'title'};
    my $query_parameters = GetQueryParameters();

    my ($families, $pager_data) = GetPagedObjects('Greenphyl::Family', $query_parameters);

    $pager_data->{'entry_ranges_label'} = 'Families per page:';
    my $total_family_count = $pager_data->{'pager'}->total_entries;

    my $dump_columns = PrepareDumpColumns();

    my $dumper_script = CGI::url('-relative' => 1, '-query_string' => 1);
    $dumper_script = RemovePagerParameters($dumper_script);
    $dumper_script =~ s/([?&;]p=)\w+/$1dump/;
    $dumper_script ||= 'families.cgi?';
    $dumper_script =~ s/([^?&;])$/$1&amp;/; # append a '&' to the URL if missing

    my $additional = {};

    # family processing
    my $process_id = GetParameterValues('process_id');
    if ($process_id && IsAdmin())
    {
        LoadProcessingData($families, $process_id, $additional);
    }

    return RenderHTMLFullPage(
        {
            'title'          => $title || 'Families',
            #'before_content' => 'families/cluster_info.tt',
            'content'        => 'families/family_list.tt',
            'family_count'   => $total_family_count,
            'families'       => $families,
            'not_sortable'   => 1,
            'pager_data'     => $pager_data,
            'additional'     => $additional,
            'dump_data' =>
            {
                'links' => [
                    {
                        'label'       => 'Excel',
                        'format'      => 'excel',
                        'file_name'   => 'families.xls',
                        'namespace'   => 'excel',
                    },
                    {
                        'label'       => 'CSV',
                        'format'      => 'csv',
                        'file_name'   => 'families.csv',
                        'namespace'   => 'csv',
                    },
                    {
                        'label'       => 'XML',
                        'format'      => 'xml',
                        'file_name'   => 'families.xml',
                        'namespace'   => 'xml',
                    },
                ],
                'parameters'  => {'columns' => EncodeDumpParameterValue($dump_columns),},
                'object_type' => 'families',
                'mode'        => 'link',
                'script'      => $dumper_script,
            },
        },
    );
}


=pod

=head2 DumpFamilyList

B<Description>: Dump a list of families.

B<ArgsCount>: 0

B<Return>: (string)

Returns a table dump of the families in the specified format.

=cut

sub DumpFamilyList
{
    my $query_parameters = GetQueryParameters();

    my $families = [Greenphyl::Family->new($g_dbh, $query_parameters)];
    if (!@$families)
    {
        Throw('error' => 'No family to dump!');
    }

    my $dump_format  = GetDumpFormat();
    my $file_name    = GetDumpFileName();
    my $parameters   = GetDumpParameters();
    my $dump_content = DumpObjects($families, $dump_format, $parameters);

    my %dump_file_names =
        (
            'excel' => 'families.xls',
            'csv'   => 'families.csv',
            'xml'   => 'families.xml',
        );

    # get header after content in case we got an error during content generation
    # so we can still generate a clean header
    my $header = RenderHTTPFileHeader({'file_name' => $file_name, 'format' => $dump_format});
    return $header . $dump_content;

}


=pod

=head2 RenderSpecificListForm

B<Description>: Renders species/phylum specific families selection form.

B<ArgsCount>: 0

B<Return>: (string)

Returns an HTML string containing the form.

=cut

sub RenderSpecificListForm
{
    my $species_list = GetSpeciesList();
    my $phylum_list = GetPhylumListForFamilies();
    
    return RenderHTMLFullPage(
        {
            'title'        => 'Display specific lists of gene families',
            'content'      => 'family_tools/specific_form.tt',
            'species_list' => $species_list,
            'phylum_list'  => $phylum_list,
        },
    );
}




# Script options
#################

=pod

=head1 OPTIONS

    families.cgi ?p=list&type=<LIST_TYPE>
               ?p=dump&type=<LIST_TYPE>
               
               [level=<1-4>[,<1-4>...] ]
               [validated=<0-9>[,<0-9>...] ]
               [black_listed=<no|yes|only>]
               [tax_id=<TAX_ID[,TAX_ID...]>]
               

=head2 Parameters

=over 4

=item B<list> or B<dump> LIST_TYPE (string):

Can be one of $LIST_TYPES keys: 
'allergen', 'enzyme', 'ipr', 'kegg', 'lead', 'transcription_factor',
'phylum_exclusive', 'phylum_inclusive', 'plant_specific', 'species', 'tair',
'transcription_factor' or 'validated'.
Default: 'validated'

=item B<level> (integer):

Family classification filter. Only family corresponding to the specified
level(s) are returned. Several levels can be provided using coma.
Default: none (any level)

=item B<validated> (integer):

Family validation filter. Only family corresponding to the specified
validation status are returned. Several status can be provided using coma.
Default: none (any validation status)

=item B<black_listed> (string):

Family black-list filter. Only families with the specified black-list status are
returned.
no: only non-black-listed families are returned;
yes: both non-black-listed and black-listed families are returned;
only: only black-listed families are returned.
Default: no (black-listed families are not returned)

=item B<tax_id> (integer):
For 'phylum_exclusive', 'phylum_inclusive' and 'species' list types, this
parameter selects which phylum-specific families should be displayed according
to the list of tax_id provided. Several tax_id can be specified using coma.

=back

=cut


# CODE START
#############

# hash for function selection.
my $actions =
    {
        ''     => \&RenderFamilyList,
        'list' => \&RenderFamilyList,
        'dump' => \&DumpFamilyList,
        'specific' => \&RenderSpecificListForm,
    };

# try block
eval
{
    # display page
    print ProceedPage($actions);
};

HandleErrors();

exit(0);

# CODE END
###########


=pod

=head1 AUTHORS

Mathieu ROUARD (Bioversity), m.rouard@cgiar.org

Valentin GUIGNON (Bioversity), v.guignon@cgiar.org

Matthieu CONTE (Syngenta)

=head1 VERSION

Version 1.0.0

Date 12/06/2012

=head1 SEE ALSO

GreenPhyl documentation.

=cut
