#!/usr/bin/perl

=pod

=head1 NAME

custom_families.cgi - display list of custom family

=head1 SYNOPSIS

    /custom_families.cgi?p=list

=head1 REQUIRES

Perl5

=head1 DESCRIPTION

This script displays list of custom families.

=cut

use strict;
use warnings;

use lib '../lib';
use lib '../local_lib';
use Carp qw (cluck confess croak);
use Greenphyl;
use Greenphyl::Web;
use Greenphyl::Web::Access;
use Greenphyl::CustomFamily;
use Greenphyl::Dumper;
use Greenphyl::Species;
use Greenphyl::Taxonomy;
use Greenphyl::CompositeObject;
use Greenphyl::Tools::Taxonomy;
use Greenphyl::User;
use Greenphyl::Tools::Users;

# use Data::Dumper;


# Script global constants
##########################

=pod

=head1 CONSTANTS

B<$DEBUG>: (boolean)

Enables debug mode.

=cut

our $DEBUG = 0;
our $FAMILY_PER_PAGE_DEFAULT = 50;


# Script global variables
##########################

=pod

=head1 VARIABLES

B<$g_dbh>: (DBI::db)

Current database handler.

=cut

my $g_dbh = GetDatabaseHandler();
my $g_current_list_type = undef;




# Script global functions
##########################

=pod

=head1 FUNCTIONS

=head2 PrepareDumpColumns

B<Description>: Prepare the parameter hash for columns that should be dumped.

B<ArgsCount>: 0

B<Return>: (hash ref)

column dump parameter hash. Keys are column labels and values are corresponding
object member path.

=cut

sub PrepareDumpColumns
{
    my $column_index = 'a';
    my $dump_columns =
        {
            $column_index++ . '. ID'          => 'id',
            $column_index++ . '. Name'        => 'name',
            $column_index++ . '. Description' => 'description',
            $column_index++ . '. Sequences'   => 'count_sequences',
        };

    return $dump_columns;
}


=pod

=head2 GetQueryParameters

B<Description>: Returns a hash ref containing the query parameters to use to
fetch the families that are visible to current user.

B<ArgsCount>: 0

B<Return>: (hash ref)

See Greenphyl::DBObject CONSTRUCTOR documentation ("$parameters" parameter).

=cut

sub GetQueryParameters
{
    my $current_user = GetCurrentUser();

    # get list type, selectors and fields to load
    my $user_selector = [
        {'user_id' => $current_user->id,}, # owner
        'OR',
        {
            # or public access
            'access' => [
                    'IN',
                    $Greenphyl::CustomFamily::CUSTOM_FAMILY_ACCESS_PUBLIC_WRITE,
                    $Greenphyl::CustomFamily::CUSTOM_FAMILY_ACCESS_PUBLIC_READ
                ],
        },
    ];

    # default for admins: all
    if (IsAdmin())
    {
        $user_selector = undef;
    }

    # remove GreenPhyl families from list?
    if (GetParameterValues('no_gp'))
    {
        # Get old GreenPhyl version users
        my $old_gp_users = [Greenphyl::User->new($g_dbh, {'selectors' => {'login' => ['IN', 'greenphyl_v1', 'greenphyl_v2', 'greenphyl_v3', 'greenphyl_v4', 'greenphyl_v5','greenphyl_panmusa',]}})];

        if (@$old_gp_users)
        {
            if ($user_selector)
            {
                $user_selector = [$user_selector, 'AND', {'user_id' => ['NOT IN', map {$_->id} @$old_gp_users]}];
            }
            else
            {
                $user_selector = {'user_id' => ['NOT IN', map {$_->id} @$old_gp_users]};
            }
        }
    }

    my @selected_owners = GetParameterValues('user_id', undef, '[,;\s]+');
    if (1 == @selected_owners)
    {
        $user_selector = {'user_id' => $selected_owners[0],}; # owner
    }
    elsif (@selected_owners)
    {
        $user_selector = {'user_id' => ['IN', @selected_owners],}; # owners
    }

    my $selectors = [];
    if ($user_selector)
    {
        push(@$selectors, $user_selector);
    }

    # select by level
    if (my @levels = GetParameterValues('level', undef, ','))
    {
        push(@$selectors, 'AND', {'level' => ['IN', @levels]});
    }

    # select listed
    if (my $listed = GetParameterValues('listed'))
    {
        # push(@$selectors, 'AND', {'flags' => ['LIKE', $Greenphyl::CustomFamily::CUSTOM_FAMILY_FLAG_LISTED]});
        push(@$selectors, 'AND', {'flags' => ['&', $Greenphyl::CustomFamily::CUSTOM_FAMILY_FLAG_LISTED_VALUE]});
    }

    # order
    # default order by name and accession
    my $sql_param = {
        'ORDER BY' =>
            [
                'custom_families.name ASC',
                'custom_families.accession ASC',
            ],
    };

    my $order_param = GetParameterValues('order') || '';
    if (($order_param eq 'date') || ($order_param eq 'dateasc'))
    {
        unshift(@{$sql_param->{'ORDER BY'}}, 'custom_families.creation_date ASC');
    }
    elsif ($order_param eq 'datedesc')
    {
        unshift(@{$sql_param->{'ORDER BY'}}, 'custom_families.creation_date DESC');
    }
    elsif (($order_param eq 'update') || ($order_param eq 'updateasc'))
    {
        unshift(@{$sql_param->{'ORDER BY'}}, 'custom_families.last_update ASC');
    }
    elsif ($order_param eq 'updatedesc')
    {
        unshift(@{$sql_param->{'ORDER BY'}}, 'custom_families.last_update DESC');
    }
    elsif (($order_param eq 'level') || ($order_param eq 'levelasc'))
    {
        unshift(@{$sql_param->{'ORDER BY'}}, 'custom_families.level ASC');
    }
    elsif ($order_param eq 'leveldesc')
    {
        unshift(@{$sql_param->{'ORDER BY'}}, 'custom_families.level DESC');
    }

    
    my $query_parameters =
        {
            'selectors' => $selectors,
            'sql' => $sql_param,
        };

    return $query_parameters;
}


=pod

=head2 RenderCustomFamilyList

B<Description>: Renders a list of families.

B<ArgsCount>: 0

B<Return>: (string)

Returns an HTML string containing the Family listing.

=cut

sub RenderCustomFamilyList
{
    InitAccess();

    my $title = 'Custom Families';
    my $query_parameters = GetQueryParameters();
    if ($query_parameters->{'selectors'} && ('ARRAY' eq ref($query_parameters->{'selectors'})))
    {
        my ($user_selector) = grep {('HASH' eq ref($_)) && exists($_->{'user_id'})} @{$query_parameters->{'selectors'}};
        if ($user_selector->{'user_id'}
            && !ref($user_selector->{'user_id'})
            && ($user_selector->{'user_id'} =~ m/^\d+$/))
        {
            my $user = Greenphyl::User->new($g_dbh, {'selectors' => {'id' => $user_selector->{'user_id'}}});
            if ($user)
            {
                my $user_name = $user->display_name;
                $user_name =~ s/_/ /g;
                $user_name =~ s/[^$Greenphyl::Tools::Users::LOGIN_ALLOWED_CHARACTERS]//g;
                $user_name =~ s/greenphyl/GreenPhyl/gi;
                $user_name = join(' ', map {ucfirst;} split(' ', $user_name));
                $title = $user_name . ' Families';
            }
        }
    }


    my ($families, $pager_data) = GetPagedObjects('Greenphyl::CustomFamily', $query_parameters);

    $pager_data->{'entry_ranges_label'} = 'Families per page:';
    my $total_family_count = $pager_data->{'pager'}->total_entries;

    my $dump_columns = PrepareDumpColumns();

    my $dumper_script = CGI::url('-relative' => 1, '-query_string' => 1);
    $dumper_script = RemovePagerParameters($dumper_script);
    $dumper_script =~ s/([?&;]p=)\w+/$1dump/;
    $dumper_script ||= 'custom_families.cgi?';
    $dumper_script =~ s/([^?&;])$/$1&amp;/; # append a '&' to the URL if missing

    my $additional = {
        # 'columns' => [
        #         {
        #             'label'  => 'Owner',
        #             'member' => 'current_object.user',
        #         },
        #     ],
    };

    return RenderHTMLFullPage(
        {
            'title'           => $title,
            'before_content'  => 'families/list_filter.tt',
            'content'         => 'families/family_list.tt',
            'family_count'    => $total_family_count,
            'families'        => $families,
            'not_sortable'    => 1,
            'pager_data'      => $pager_data,
            'additional'      => $additional,
            'dump_data' =>
            {
                'links' => [
                    {
                        'label'       => 'Excel',
                        'format'      => 'excel',
                        'file_name'   => 'families.xls',
                        'namespace'   => 'excel',
                    },
                    {
                        'label'       => 'CSV',
                        'format'      => 'csv',
                        'file_name'   => 'families.csv',
                        'namespace'   => 'csv',
                    },
                    {
                        'label'       => 'XML',
                        'format'      => 'xml',
                        'file_name'   => 'families.xml',
                        'namespace'   => 'xml',
                    },
                ],
                'parameters'  => {'columns' => EncodeDumpParameterValue($dump_columns),},
                'object_type' => 'custom_families',
                'mode'        => 'link',
                'script'      => $dumper_script,
            },
        },
    );
}


=pod

=head2 DumpCustomFamilyList

B<Description>: Dump a list of families.

B<ArgsCount>: 0

B<Return>: (string)

Returns a table dump of the families in the specified format.

=cut

sub DumpCustomFamilyList
{
    my $query_parameters = GetQueryParameters();

    my $families = [Greenphyl::CustomFamily->new($g_dbh, $query_parameters)];
    if (!@$families)
    {
        Throw('error' => 'No family to dump!');
    }

    my $dump_format  = GetDumpFormat();
    my $file_name    = GetDumpFileName();
    my $parameters   = GetDumpParameters();
    my $dump_content = DumpObjects($families, $dump_format, $parameters);

    my %dump_file_names =
        (
            'excel' => 'custom_families.xls',
            'csv'   => 'custom_families.csv',
            'xml'   => 'custom_families.xml',
        );

    # get header after content in case we got an error during content generation
    # so we can still generate a clean header
    my $header = RenderHTTPFileHeader({'file_name' => $file_name, 'format' => $dump_format});
    return $header . $dump_content;

}




# Script options
#################

=pod

=head1 OPTIONS

    families.cgi ?p=list&type=<LIST_TYPE>
               ?p=dump&type=<LIST_TYPE>
               
               [level=<1-4>[,<1-4>...] ]
               [validated=<0-9>[,<0-9>...] ]
               [black_listed=<no|yes|only>]
               [tax_id=<TAX_ID[,TAX_ID...]>]
               

=head2 Parameters

=over 4

=item B<list> or B<dump> LIST_TYPE (string):

Can be one of $LIST_TYPES keys: 
'allergen', 'enzyme', 'ipr', 'kegg', 'lead', 'transcription_factor',
'phylum_exclusive', 'phylum_inclusive', 'plant_specific', 'species', 'tair',
'transcription_factor' or 'validated'.
Default: 'validated'

=item B<level> (integer):

Family classification filter. Only family corresponding to the specified
level(s) are returned. Several levels can be provided using coma.
Default: none (any level)

=item B<validated> (integer):

Family validation filter. Only family corresponding to the specified
validation status are returned. Several status can be provided using coma.
Default: none (any validation status)

=item B<black_listed> (string):

Family black-list filter. Only families with the specified black-list status are
returned.
no: only non-black-listed families are returned;
yes: both non-black-listed and black-listed families are returned;
only: only black-listed families are returned.
Default: no (black-listed families are not returned)

=item B<tax_id> (integer):
For 'phylum_exclusive', 'phylum_inclusive' and 'species' list types, this
parameter selects which phylum-specific families should be displayed according
to the list of tax_id provided. Several tax_id can be specified using coma.

=back

=cut


# CODE START
#############

# hash for function selection.
my $actions =
    {
        ''     => \&RenderCustomFamilyList,
        'list' => \&RenderCustomFamilyList,
        'dump' => \&DumpCustomFamilyList,
    };

# try block
eval
{
    # display page
    print ProceedPage($actions);
};

HandleErrors();

exit(0);

# CODE END
###########


=pod

=head1 AUTHORS

Mathieu ROUARD (Bioversity), m.rouard@cgiar.org

Valentin GUIGNON (Bioversity), v.guignon@cgiar.org

Matthieu CONTE (Syngenta)

=head1 VERSION

Version 1.0.0

Date 12/06/2012

=head1 SEE ALSO

GreenPhyl documentation.

=cut
