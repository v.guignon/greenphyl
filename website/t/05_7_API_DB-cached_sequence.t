#!/usr/bin/perl

use strict;
use warnings;

use lib '../lib';
use lib '../local_lib';


use Test::Most;
# use Test::Regression;
use Carp qw (cluck confess croak);

use List::Util 'shuffle'; # randomize list order

BEGIN {
    use_ok('Greenphyl' );
    use_ok('Greenphyl::Sequence');
    use_ok('Greenphyl::CachedSequence');
    use_ok('Greenphyl::Tools::Sequences');
}

# Cached sequence
my $cached_sequence = Greenphyl::CachedSequence->new(
    GetDatabaseHandler(),
    {
        'selectors' => {'accession' => 'brana_pan_p012620',},
    },
);
ok($cached_sequence, 'Cached sequence object creation from database');
ok('HASH' eq ref($cached_sequence->bbmh_cache), 'Data unserialized.');

our $DEBUG = 1;
use Data::Dumper;
# my $sequence = Greenphyl::Sequence->new(
#     GetDatabaseHandler(),
#     {
#         # 'selectors' => {'accession' => 'Maban_Contig173_t000240',},
#         'selectors' => {'accession' => 'AT1G01020.1',},
#     },
# );
# print Dumper([$sequence->getIPRGO]);

$cached_sequence = Greenphyl::CachedSequence->new(
    GetDatabaseHandler(),
    {
        # 'selectors' => {'accession' => 'Maban_Contig173_t000240',},
        'selectors' => {'accession' => 'AT1G01020.1',},
    },
);



done_testing();
