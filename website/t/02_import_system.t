#!/usr/bin/perl

use strict;
use warnings;
no warnings 'once';

BEGIN {
    chdir 'cgi-bin';
    use lib '../lib';
use lib '../local_lib';

}

use Test::Most qw( defer_plan );
use Test::Regression;
use Capture::Tiny 'capture';
use File::Slurp;
use autodie;
use Benchmark ':hireswallclock';

use Greenphyl::Config;

my %config       = %{ $Greenphyl::Config::DATABASES->[2] };
my $test_log_dir = "../t/logs";
my $testing_data_dir = "../t/data";
my $process_dir = $Greenphyl::Config::DATA_PATH . '/process';
die "directory '$process_dir' missing" if !-e $process_dir;

my @times;
my $profile_id  = -1;
my $do_cover    = 1 if $ARGV[0] and $ARGV[0] eq 'cov';
my $print_times = 0;

# if the parameter 'gen' is first on the command line, the regression files are
# not checked against output, but instead generated from output
$ENV{TEST_REGRESSION_GEN} = 1 if $ARGV[0] and $ARGV[0] eq 'gen';

setup_testing_database();

run_tests();

check_database_result();

all_done();

exit;

################################################################################

sub check_database_result
{

    my $dump   = sub { prep_dump( get_database_dump() ) };

    ok_regression( $dump, "$testing_data_dir/sql/expected.sql", "database contents match expected content" );

    unlink "$testing_data_dir/sql/dump.sql" if -e "$testing_data_dir/sql/dump.sql";

    return;
}

sub prep_dump
{
    my (@dump) = @_;

    $_ =~ s/[\n\r]//g for @dump;

    @dump = grep { !/^--/ and !/\/\*\!/ and $_ } @dump;

    my $dump = join "\n", @dump;

    return $dump;
}

sub get_database_dump
{
    my $dump = `$GreenPhyl::Config::MYSQL_DUMPER -u $config{login} -p$config{password} $config{database} --skip-extended-insert`;

    $dump =~ s/AUTO_INCREMENT=\d+ //g;

    write_file( "$testing_data_dir/sql/dump.sql", $dump );

    my @dump = split /\n/, $dump;

    return @dump;
}

sub run_tests
{
    system("rm -rf ../cover_db/*") if $do_cover;
    system("rm -rf $process_dir/*");

    test_update_pipeline();

    system("cover") if $do_cover;
    sleep 1;
    rename( "cover_db", "../cover_db" ) if $do_cover;

    print_times();
}

sub print_times
{
    return if !$print_times;

    my $times;
    $times .= "$_->{id} : " . timestr( $_->{time} ) . " : $_->{name}\n" for @times;
    diag $times;

    return;
}

sub test_update_pipeline
{

    my @outputs;
    my @tests = get_tests();

    $ENV{PERL5OPT} = '-MDevel::Cover' if $do_cover;
    $ENV{TESTING} = 1;

    run_single_test( $_, \@outputs ) for @tests;

    delete $ENV{TESTING};
    delete $ENV{PERL5OPT} if $do_cover;

    my @logs = grep { !/\.svn/ } read_dir($test_log_dir);
    unlink "$test_log_dir/$_" for @logs;
    print_test_output( $outputs[$_], $_ ) for ( 0 .. $#outputs );

    return;
}

sub run_single_test
{
    my ( $test, $outputs ) = @_;

    delete $ENV{$_} for qw( RUN_REAL_UPDATE ); # reset env to default
    $ENV{$_} = $test->{env}{$_} for keys %{ $test->{env} }; # set env variables associated with test so scripts know whether to use special behavior
    $ENV{PERL5OPT} = '-d:NYTProf' if $test->{id} == $profile_id; # activate profiling if it was requested for this test id

    my $cmd = "perl $test->{script} $test->{param}";

    subtest "ID: $test->{id} - '$cmd'" => sub {

        my $res;
        my $t;
        my ( $stdout, $stderr ) = capture
        {
            $t = timeit( 1, sub { $res = system($cmd ); } );
        };

        push @times, { id => $test->{id}, name => $cmd, time => $t };

        is_deeply( [$res,$?], [0,0], "no fatal errors" );
        is( $stderr, '', "no errors or warnings" );
        ok_regression( pre_filter( $stdout, $test->{id} ), "$testing_data_dir/regression/$test->{id}", 'regression test' );

        #$DB::single = 1 if $test->{id} == 23;

        $test->{stdout} = $stdout;
        $test->{stderr} = $stderr;

        if ( $test->{sge_out} )
        {
            sleep 1;    # this might help with the files still being locked sometimes
            write_file( "$process_dir/sge_stdout_$test->{sge_out}.txt", $test->{stdout} );
            write_file( "$process_dir/sge_stderr_$test->{sge_out}.txt", $test->{stderr} );
        }

        push @{$outputs}, $test;

        done_testing();
    };

    delete $ENV{PERL5OPT} if $test->{id} == $profile_id;    # disable any profiling

    return;
}

sub pre_filter {
    my ( $string, $id ) = @_;

    my %filters = (
        short_session_cookie => sub { $_[0] =~ s/(Set-Cookie: CGISESSID=)\w+(; path=\/)\n/$1$2\n/ },
        session_cookie => sub { $_[0] =~ s/(Set-Cookie: CGISESSID=)\w+(; path=\/; expires=).+GMT\n/$1$2\n/ },
        cookie_date => sub { $_[0] =~ s/(Date: ).* GMT\n/$1\n/ },
        prot_seq_date => sub { $_[0] =~ s/(Date: ).* \d\d\d\d\n/$1\n/ },
        dump_file_name => sub { $_[0] =~ s/(dump_greenphyl)\d+(\.sql)/$1$2/ },
        interpro_scan_id => sub { $_[0] =~ s/(file_tmp_interproscan)\d+\n/$1\n/ },

        fix_url => sub { $_[0] =~ s@greenphyl\.cirad\.fr.\w+/\w+/@greenphyl.cirad.fr.test/clean/@g },
        fix_cgi_path => sub { $_[0] =~ s@/\w+/cgi-bin/@/clean/cgi-bin/@g },
        fix_lib_path => sub { $_[0] =~ s@/\w+/lib/@/clean/lib/@g },
        fix_data_process_path => sub { $_[0] =~ s@/\w+/GreenPhyl/process/@/data_test/GreenPhyl/process/@g },
        fix_data_dir_path => sub { $_[0] =~ s@/\w+/GreenPhyl/dir@/data_test/GreenPhyl/dir@g },
    );

    my %id_filters;

    push @{$id_filters{$_}}, 'dump_file_name' for qw( 2 2 );
    push @{$id_filters{$_}}, 'session_cookie' for qw( 0 1 2 3 4 6 10 15 20 23 );
    push @{$id_filters{$_}}, 'short_session_cookie' for qw( 12 13 17 18 );
    push @{$id_filters{$_}}, 'cookie_date' for qw( 0 1 2 3 4 6 10 12 13 15 17 18 20 23 );
    push @{$id_filters{$_}}, 'prot_seq_date' for qw( 5 5 7 7 8 8 9 9 11 11 16 16 12 12 17 17 );
    push @{$id_filters{$_}}, 'interpro_scan_id' for qw( 21 21 21 21 );

    my @path_fix_list = qw( fix_url fix_cgi_path fix_lib_path fix_data_process_path fix_data_dir_path );
    push @{$id_filters{$_}}, @path_fix_list for 0 .. 23;

    $filters{$_}->( $string ) for @{$id_filters{$id}};

    return sub { $string };
}

sub print_test_output
{
    my ( $output, $id ) = @_;

    for (qw( stdout stderr ))
    {
        next if !$output->{$_};
        my $file_name = sprintf( "%s_%02d_%s.log", $_, $id, $output->{script} );
        my $content = "$output->{script} $output->{param}\n\n$output->{$_}";
        write_file( "$test_log_dir/$file_name", $content );
    }

    return;
}

sub pipeline_nologin
{
    my ( $param ) = @_;

    my %test = (
        script => "update_pipeline.cgi",
        param  => $param,
    );

    return \%test;
}

sub pipeline
{
    my ( $param ) = @_;

    my $test = pipeline_nologin( $param );
    $test->{param} .= " login_override=1";

    return $test;
}

sub update
{
    my ( $param, $sge_out ) = @_;

    my %test = (
        script  => "update.pl",
        param   => $param,
        env     => { RUN_REAL_UPDATE => 1 },
        sge_out => $sge_out,
    );

    return \%test;
}

sub job_process
{
    my ( $param ) = @_;

    my %test = (
        script => "job_process.cgi",
        param  => "$param login_override=1",
    );

    return \%test;
}

sub get_tests
{
    my @tests = (

        #  0 - basic actions
        pipeline_nologin( ".state=Home" ),
        pipeline( "" ),
        pipeline( "button_dump=Launch" ),
        pipeline( "button_dump=Launch name_dump=test" ),
        pipeline( "path=$testing_data_dir/input/Chlre4_best_proteins.fasta species_name=CHLRE orga=Chlamydomonas+reinhardtii url=3055 release=http%3A%2F%2Fgenome.jgi-psf.org%2FChlre4%2FChlre4.home.html url_fasta=http%3A%2F%2Fgenome.jgi-psf.org%2FChlre4%2Fdownload%2Fannotation%2FChlre4_best_proteins.fasta.gz version=4 button_new=Launch species=Null" ),

        #  5 - actual fasta import
        update(
            " -c 1 -f $testing_data_dir/input/Chlre4_best_proteins.fasta  -sid 12 -sp CHLRE -o Chlamydomonas_reinhardtii -taxonomy 3055 -v 4 -path_I http%3A%2F%2Fgenome.jgi-psf.org%2FChlre4%2FChlre4.home.html -path_F http%3A%2F%2Fgenome.jgi-psf.org%2FChlre4%2Fdownload%2Fannotation%2FChlre4_best_proteins.fasta.gz ",
            "1_CHLRE"
        ),
        pipeline( "path=$testing_data_dir/input/Chlre4_best_proteins.fasta button_update=Launch species=12" ),
        update( " -c 1 -f $testing_data_dir/input/Chlre4_best_proteins.fasta -sid 12 -sp CHLRE ",   "1_CHLRE" ),
        update( " -c 1 -f $testing_data_dir/input/Chlre4_best_proteins_2.fasta -sid 12 -sp CHLRE ", "1_CHLRE" ),
        update( " -c 1 -f $testing_data_dir/input/Chlre4_best_proteins_3.fasta -sid 12 -sp CHLRE ", "1_CHLRE" ),

        # 10
        # scan for new families
        pipeline( "path=$testing_data_dir/input/Chlre4_best_proteins_4.fasta button_update=Launch species=12" ),
        update( " -c 1 -f $testing_data_dir/input/Chlre4_best_proteins_4.fasta -sid 12 -sp CHLRE ", "1_CHLRE" ),    # interpro-active fasta data
        job_process( " tracking_id=9999 " ),
        job_process( " species_name_N=CHLRE sge_id_N=9999 " ),
        update( " -db_index=2 -c 2 -sp CHLRE ", "2_CHLRE" ),

        # 15
        # and a second time with additional data to check that modifying families works
        pipeline( "path=$testing_data_dir/input/Chlre4_best_proteins_5.fasta button_update=Launch species=12" ),
        update( " -c 1 -f $testing_data_dir/input/Chlre4_best_proteins_5.fasta -sid 12 -sp CHLRE ", "1_CHLRE" ),
        job_process( " tracking_id=9999 " ),
        job_process( " species_name_N=CHLRE sge_id_N=9999 " ),
        update( " -db_index=2 -c 2 -sp CHLRE ", "2_CHLRE" ),

        # 20
        # scans a given fasta file against the interpro database and imports interpro entries
        pipeline( "interpro_path=$testing_data_dir/input/Chlre4_best_proteins_5.fasta button_interproscan=Launch speciesData=12 interproscan=C" ),
        update( " -c 3 -sp CHLRE -inter $testing_data_dir/input/Chlre4_best_proteins_5.fasta ", "3I_CHLRE" ),
        update( " -c 4 -sp CHLRE -stat 1 ",                                "4I_CHLRE" ),
        # uniprot tasks
        pipeline( "uniprot_path=$testing_data_dir/input/uniprot-taxonomy-3055-AND-reviewed-yes.xml uniprot_path2=$testing_data_dir/input/uniprot-taxonomy-3055-AND-reviewed-no.xml button_uniprot=Launch speciesData=12" ),

    );

    $tests[$_]->{id} = $_ for 0 .. $#tests;
    $_->{param} = " -db_index=2 $_->{param} " for @tests;

    return @tests;
}

sub setup_testing_database
{

    destroy_testing_database();

    my $file = "$testing_data_dir/sql/greenphyl_test.sql";
    run_sql_batch( $file, "main testing database was set up" );

    $file = "$testing_data_dir/sql/inserts.sql";
    run_sql_batch( $file, "main testing database was initialized" );

    return;
}

sub destroy_testing_database
{

    my $file = "$testing_data_dir/sql/drop_tables.sql";
    run_sql_batch( $file, "testing database was destroyed" );

    return;
}

sub run_sql_batch
{
    my ( $file, $action ) = @_;

    my $cmd = "mysql -D $config{database} -u $config{login} --password=$config{password} -h localhost < $file";
    my $res = system($cmd);
    fail $action if $res;
    pass $action if !$res;

    return;
}
