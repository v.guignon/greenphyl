#!/usr/bin/perl

use strict;
use warnings;

use lib '../lib';
use lib '../local_lib';


use Test::Most;
use Test::Regression;
use Carp qw (cluck confess croak);

BEGIN {
    use_ok( 'Greenphyl' );
    use_ok( 'Greenphyl::Exceptions' );
}

# if the parameter 'gen' is first on the command line, the regression files are
# not checked against output, but instead generated from output
if ($ARGV[0] && ($ARGV[0] eq 'gen'))
{
    $ENV{TEST_REGRESSION_GEN} = 1;
}

$ENV{'TESTING'} = 1;

my $TEST_REFERENCE_FILES_PATH =  GP('TEST_PATH') . '/api';
if (!-e $TEST_REFERENCE_FILES_PATH)
{
    mkdir($TEST_REFERENCE_FILES_PATH) or confess "ERROR: unable to create missing regression directory '$TEST_REFERENCE_FILES_PATH'!\n$@\n";
}
elsif (!-w $TEST_REFERENCE_FILES_PATH)
{
    cluck "WARNING: regression directory '$TEST_REFERENCE_FILES_PATH' is not writeable!\n";
}
my $TEST_REFERENCE_BASE_FILENAME = $TEST_REFERENCE_FILES_PATH . '/test_';

my $regexp = '^' . GP('TEMP_OUTPUT_PATH');
my ($temp_file_name, $test_fh) = WriteTemporaryFile("Test 1 2 3...\n\n- Some content -\n");
print "    Temporary file: '$temp_file_name'\n";
ok(($temp_file_name =~ m/$regexp/), 'Temporary file name');
ok((-e $temp_file_name), 'Temporary file creation');
print {$test_fh} "Some more content.\n";
close($test_fh);

open($test_fh, $temp_file_name) or confess "ERROR: unable to open temporary file '$temp_file_name'!\n";
ok_regression(sub { return join('', <$test_fh>); }, $TEST_REFERENCE_BASE_FILENAME . 'temp_file_1', 'Temporary file content');
close($test_fh);
unlink($temp_file_name) or confess "Unable to remove temporary file '$temp_file_name'.\n";

print "    Checking error handling...\n";
eval
{
    ThrowGreenphylError('error' => 'Error message');
};

my $error;
if ($error = Exception::Class->caught('Greenphyl::Error'))
{
    pass('GreenPhyl error catched.');

    # check message content according to DEBUG mode
    my $PUBLIC_MESSAGE = 'Public error message';
    my $PRIVATE_MESSAGE = 'Private error message';
    
    my $debug_mode = $Greenphyl::Config::DEBUG_MODE;
    # disable debug mode
    $Greenphyl::Config::DEBUG_MODE = 0;
    eval
    {
        ThrowGreenphylError('error' => $PUBLIC_MESSAGE, 'log' => $PRIVATE_MESSAGE);
    };
    if ($error = Exception::Class->caught('Greenphyl::Error'))
    {
        ok((($error->message() =~ m/$PUBLIC_MESSAGE/) && ($error->message() !~ m/$PRIVATE_MESSAGE/)), 'Public error message does not contain log info by default');
        ok((($error =~ m/$PUBLIC_MESSAGE/) && ($error =~ m/$PRIVATE_MESSAGE/)), 'Log error message contains all info');
    }

    # force debug mode
    $Greenphyl::Config::DEBUG_MODE = 1;
    eval
    {
        ThrowGreenphylError('error' => $PUBLIC_MESSAGE, 'log' => $PRIVATE_MESSAGE);
    };
    if ($error = Exception::Class->caught('Greenphyl::Error'))
    {
        ok((($error->message() =~ m/$PUBLIC_MESSAGE/) && ($error->message() =~ m/$PRIVATE_MESSAGE/)), 'Public error message contains log info in debug mode');
        ok((($error =~ m/$PUBLIC_MESSAGE/) && ($error =~ m/$PRIVATE_MESSAGE/)), 'Log error message contains all info');
    }
    # restore debug mode
    $Greenphyl::Config::DEBUG_MODE = $debug_mode;
}
elsif ($error = Exception::Class->caught())
{
    fail('GreenPhyl error not catched!');
    confess 'An other error occured: ' . shift();
}
else
{
    # no exception thrown
    fail('GreenPhyl error not catched!');
}

my $REDIRECTION_URL = 'http://www.greenphyl.org';
my $REDIRECTION_URL2 = 'http://greenphyl-dev.cirad.fr';
eval
{
    ThrowRedirection($REDIRECTION_URL);
};

if ($error = Exception::Class->caught('Greenphyl::Redirection'))
{
    pass('GreenPhyl redirection ok.');
    ok($error->message() =~ m/$REDIRECTION_URL/, 'Implicit redirection');

    eval
    {
        ThrowRedirection('url' => $REDIRECTION_URL2);
    };
    if ($error = Exception::Class->caught('Greenphyl::Redirection'))
    {
        ok($error->message() =~ m/$REDIRECTION_URL2/, 'Explicit redirection');
    }

    my $old_cgi_mode = $Greenphyl::IS_CGI;
    eval("use Greenphyl::Web;");
    $Greenphyl::IS_CGI = 1;
    eval
    {
        ThrowRedirection('url' => $REDIRECTION_URL);
    };

    if ($error = Exception::Class->caught('Greenphyl::Redirection'))
    {
        my $EXPECTED_REDIRECTION_STRING = "Status:302FoundLocation:http://www.greenphyl.org";
        my $no_output;
        my $redirection_string = '';
        my $string_handle;
        open($string_handle, '>', \$redirection_string);
        select $string_handle;
        eval
        {
            $no_output = $error->message();
        };
        select STDOUT;
        close($string_handle);
        
        $redirection_string =~ s/\s+//g;

        ok(!$no_output, 'Web throwable redirection 1');
        ok($EXPECTED_REDIRECTION_STRING eq $redirection_string, 'Web throwable redirection 2');
    }
    
    $Greenphyl::IS_CGI = $old_cgi_mode;
    
}
elsif ($error = Exception::Class->caught())
{
    fail('GreenPhyl redirection not handled!');
    confess 'An other error occured: ' . shift();
}
else
{
    # no exception thrown
    fail('GreenPhyl redirection not handled!');
}


# Parameters check
my $parameter_file = "./api/parameter_file";
# save @ARGV
my @old_argv = @ARGV;
my ($value, @values, $expected_values);

# single values
@ARGV = ();
ok(!defined(GetParameterValues('paramtest')), 'Unspecified single parameter');

@ARGV = ('paramtest=0');
$value = GetParameterValues('paramtest');
ok(defined($value) && ('0' eq $value), 'Specified single parameter set to a false-value');

@ARGV = ('paramtest=1');
$value = GetParameterValues('paramtest');
ok(defined($value) && ('1' eq $value), 'Specified single parameter set to a true-value');

@ARGV = ('paramtest=806', 'paramtest=33');
$value = GetParameterValues('paramtest');
ok(defined($value) && ('806' eq $value), 'Specified single parameter using multiple values');

@ARGV = ('space.paramtest=42');
$value = GetParameterValues('paramtest', 'space');
ok(defined($value) && ('42' eq $value), 'Specified single parameter using namespace');

@ARGV = ('paramtest=42');
$value = GetParameterValues('paramtest', 'space');
ok(defined($value) && ('42' eq $value), 'Specified single parameter using namespace with global namespace fallback when namespace missing');

@ARGV = ('noname.paramtest=33');
$value = GetParameterValues('paramtest', 'space');
ok(!defined($value), 'Unspecified single parameter using namespace with same parameter specified in another namespace');

@ARGV = ('paramtest=33', 'space.paramtest=806', 'paramtest=33');
$value = GetParameterValues('paramtest', 'space');
ok(defined($value) && ('806' eq $value), 'Specified single parameter using namespace');


# multiple values
@ARGV = ();
@values = GetParameterValues('paramtest');
ok(!@values, 'Unspecified multiple parameters');

@ARGV = ('paramtest=0');
@values = GetParameterValues('paramtest');
ok((1 == @values) && ('0' eq $values[0]), 'Specified multiple parameters with a single value set to a false-value');

@ARGV = ('paramtest=1');
@values = GetParameterValues('paramtest');
ok((1 == @values) && ('1' eq $values[0]), 'Specified multiple parameters with a single value set to a true-value');

@ARGV = ('paramtest=0', 'paramtest=1');
@values = GetParameterValues('paramtest');
ok((2 == @values) && ('0' eq $values[0]) && ('1' eq $values[1]), 'Specified multiple parameters with 2 values');

@ARGV = ('paramtest=0,2', 'paramtest=4');
@values = GetParameterValues('paramtest');
ok((2 == @values) && ('0,2' eq $values[0]) && ('4' eq $values[1]), 'Specified multiple parameters with 2 values with one unsplitted');

@ARGV = ('paramtest=0,2', 'paramtest=4');
@values = GetParameterValues('paramtest', undef, ',');
ok((3 == @values) && ('0' eq $values[0]) && ('2' eq $values[1]) && ('4' eq $values[2]), 'Specified multiple parameters with 3 values using grouped values');

@ARGV = ('paramtest=0,2, 6    7', 'paramtest=4');
$expected_values = ['0', '2', '6', '7', '4'];
@values = GetParameterValues('paramtest', undef, '[, ]+');
is_deeply(\@values, $expected_values, 'Specified multiple parameters with 5 values using regexp');

@ARGV = ('space.paramtest=0,2', 'paramtest=4');
@values = GetParameterValues('paramtest', 'space', ',');
ok((2 == @values) && ('0' eq $values[0]) && ('2' eq $values[1]), 'Specified multiple parameters with 2 values using grouped values in namespace');

@ARGV = ('paramtest=0,2', 'space.paramtest=4', 'noname.paramtest=33');
@values = GetParameterValues('paramtest', 'space', ',');
ok((1 == @values) && ('4' eq $values[0]), 'Specified multiple parameters with 1 values using namespaces');

@ARGV = ('paramtest=0,2', 'paramtest=4', "paramtest=file:$parameter_file");
@values = GetParameterValues('paramtest', undef, '[,\n]+', 1);
$expected_values = ['0', '2', '4', '0', 'value 2', 'line 4'];
is_deeply(\@values, $expected_values, 'Specified multiple parameters with 6 values including file values');

@values = GetParameterValues('paramtest', undef, undef, 1);
$expected_values = ['0,2', '4', "file:$parameter_file"];
is_deeply(\@values, $expected_values, 'Specified multiple parameters with incorrect use of file parameter');

# restore @ARGV
@ARGV = @old_argv;

done_testing();
