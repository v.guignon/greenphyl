#!/usr/bin/perl

use strict;
use warnings;

use lib '../lib';
use lib '../local_lib';


use Carp qw (cluck confess croak);
use Test::Harness;

runtests(
    '08_1_logs_db.t',
    '08_2_logs_file.t',
    '08_3_logs_misc.t',
    '08_4_logs_file_advanced.t',
    '08_5_logs_db_multithreads.t',
    '08_6_logs_file_multithreads.t',
);
