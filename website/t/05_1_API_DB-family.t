#!/usr/bin/perl

use strict;
use warnings;

use lib '../lib';
use lib '../local_lib';


use Test::Most;
# use Test::Regression;
use Carp qw (cluck confess croak);

BEGIN {
    use_ok( 'Greenphyl' );
    use_ok( 'Greenphyl::Family' );
}

my $family = Greenphyl::Family->new(
    GetDatabaseHandler(),
    {
        'selectors' => {'id' => 205},
        'load'      => {'sequence_count' => 1},
    },
);

ok($family, 'Family object creation');
# explain('Family instance: ', $family);
FAMILY_INSTANCE:
{
    skip 'Family not instantiated, skipping tests.' unless $family;
    ok(205 == $family, 'Family in scalar context');

    my $family_id = scalar($family);
    # Autoload
    print "Checking AUTOLOAD\n";

    # -base members
    #  getters
    ok($family_id == $family->family_id, 'Base member 1 (family_id)');
    ok($family_id == $family->familyId, 'Base member 2 (familyId)');
    ok($family_id == $family->FamilyId, 'Base member 3 (FamilyId)');
    ok($family_id == $family->FamilyID, 'Base member 4 (FamilyID)');
    ok($family_id == $family->get_family_id(), 'Base member getter 1 (get_family_id)');
    ok($family_id == $family->get_familyId(), 'Base member getter 2 (get_familyId)');
    ok($family_id == $family->getFamilyId(), 'Base member getter 3 (getFamilyId)');
    ok($family_id == $family->getFamilyID(), 'Base member getter 4 (getFamilyID)');
    ok($family->getFamilyName(), 'Base member getter 5 (getFamilyName)');
    #  alias
    ok($family_id == $family->id, 'Alias 1 (id)');
    ok($family_id == $family->Id, 'Alias 2 (Id)');
    # ok($family_id == $family->ID, 'Alias 3 (ID)'); # full caps methods not allowed
    ok($family_id == $family->getid(), 'Base member getter alias 1 (getid)');
    ok($family_id == $family->getId(), 'Base member getter alias 2 (getId)');
    ok($family_id == $family->getID(), 'Base member getter alias 3 (getID)');
    ok($family_id == $family->get_id(), 'Base member getter alias 4 (get_id)');
    ok($family_id == $family->get_Id(), 'Base member getter alias 5 (get_Id)');
    ok($family_id == $family->get_ID(), 'Base member getter alias 6 (get_ID)');
    #  setters
    ok(!($family_id != $family->family_id), 'Base member 1 (family_id)');
    # getter on non-default field that should have been loaded at  instanciation
    my $first_count = $family->getSeqCount();
    ok(defined($first_count), 'Family sequence count');
    # getter on non-default field that shouldn't have been loaded yet
    ok(defined($family->getAlignmentSeqCount()), 'Family alignment sequence count');
    # getter on special column
    my $second_count = $family->sequence_count_by_families_and_species_sum;
    ok(defined($second_count), 'Family sequence count using count_family_seq');
    # explain('Family instance: ', $family);
    is($first_count, $second_count, 'Family sequence count are ok (may fail if database data is not up-to-date)');

    # -secondary members
    # --from a module
    ok(defined($family->getCountFamilySeq()), 'Family alignment sequence count object');
    # --multiple from a module
    my @ipr_stats = $family->getIPRStats();
    ok(@ipr_stats, 'Family IPR Statistic objects');
    # --from a table field
    ok(defined($family->level), 'Family cluster level');

    # check SQL options # BINARY --> case sensitive for ORDER BY
    my $sql_query_set = $family->fetchSequences({'sql' => {'ORDER BY' => 'BINARY accession DESC', 'OFFSET' => 13, 'LIMIT' => 5, 'DISTINCT' => 1}});
    # get all sequences and sort them (cmp is case sensistive)
    my $all_sequences_set = [sort { $b->accession cmp $a->accession } ($family->sequences)];
    # limit results range
    my $manual_selection_set = [splice(@$all_sequences_set, 13, 5)];
    my $retrieved_set = $family->fetchSequences();
    my $computed_from_all_sequences_set = $family->fetchSequences({'sql' => {'ORDER BY' => 'BINARY accession DESC', 'OFFSET' => 13, 'LIMIT' => 5, 'DISTINCT' => 1}});

    is_deeply($sql_query_set, $manual_selection_set, 'SQL Options Fetch: initial fetch');
    is_deeply($retrieved_set, $manual_selection_set, 'SQL Options Fetch: retrieved');
    is_deeply($computed_from_all_sequences_set, $manual_selection_set, 'SQL Options Fetch: computed from all');
    
    my @families = Greenphyl::Family->new(
        GetDatabaseHandler(),
        {
            'selectors' => {'id' => ['IN', 200, 203, 205]},
        },
    );
    
    ok(@families == 3, 'Multiple family objects creation');
    # explain('Family instances: ', @families);
    my $i = 0;
    foreach (@families)
    {
        ok($_->id, "family number ++$i checked");
    }

    # compare family created from SQL query or from constructor
    my $object_family = Greenphyl::Family->new(
        GetDatabaseHandler(),
        { 'selectors' => {'id' => 210}, },
    );
    my $sql_query = '
        SELECT ' . join(', ', Greenphyl::Family->GetDefaultMembers('f')) . '
        FROM
            families f
        WHERE
            f.id = 210;';
    my $sql_family = GetDatabaseHandler()->selectrow_hashref($sql_query);
    $sql_family = Greenphyl::Family->new(GetDatabaseHandler(), {'load' => 'none', 'members' => $sql_family});
    my %object_family_hash = map {$_ => $object_family->{$_}} keys(%$object_family);
    my %sql_family_hash = map {$_ => $sql_family->{$_}} keys(%$sql_family);
    $sql_family_hash{'_modified'} = {}; # we don't take care of modified values
    is_deeply(\%sql_family_hash, \%object_family_hash, 'custom-SQL-query loaded family');


    # count families using complex query
    my $count = Greenphyl::Family->Count(
        GetDatabaseHandler(),
        {
            'selectors' =>
            [
                {
                    'EXISTS' =>
                    [
                        'SELECT TRUE
                         FROM families f2
                         WHERE f2.id = families.id
                               AND f2.level = 1',
                        'SELECT TRUE
                         FROM sequences s
                              JOIN families_sequences sii ON sii.sequence_id = s.id
                         WHERE sii.family_id = families.id
                               AND s.species_id = 2
                         LIMIT 1',
                    ],
                },
                'OR',
                [
                    # note: when several conditions on a same column, 2
                    # possibilites:
                    # 1) separate conditions in 2 hashes + 'AND' or...
                    {
                        'name' => ['NOT LIKE', '%unannotated%'],
                        # 2) group conditions in an array (better)
                        'id'   => ['>', 200, '<', 202],
                    },
                    'AND',
                    {
                        'name' => ['IS NOT NULL'],
                    },
                ],
            ],
            'sql'      =>
            {
                'DISTINCT' => 1,
                'ORDER BY' => ['name ASC', 'id DESC'],
                'OFFSET'   => 5, # should be auto-removed and issue a warning
                # LIMIT won't affect the number of families returned but the
                #  number of "COUNT"! Just here for query testing and it should
                # issue a warning.
                'LIMIT'    => 10,
            },
        },
    );
    ok(10 < $count, 'Families count using complex query selection');
    # load families with specific attributes
    @families = Greenphyl::Family->new(
        GetDatabaseHandler(),
        {
            'selectors' =>
            [
                # note: when several conditions on a same column, 2
                # possibilites:
                # 1) separate conditions in 2 hashes + 'AND' or...
                {
                    'name' => ['NOT LIKE', '%unannotated%'],
                    # 2) group conditions in an array (better)
                    'id'   => ['>', 10, '<', 1000],
                },
                'AND',
                {
                    'name' => ['IS NOT NULL'],
                },
                # 'AND' should be implicit
                {
                    'level' => 1,
                },
            ],
            'load' =>
            {
                'level' => 1, # here it's just a "true" value to say we want the field loaded, not a level value!
                'level2' => 1,
            },
            'sql'      =>
            {
                'DISTINCT' => 1,
                'ORDER BY' => ['families.name ASC', 'families.id DESC'],
                'OFFSET'   => 5,
                'LIMIT'    => 10,
            },
        },
    );
    ok(10 == scalar(@families), 'Families loaded using complex query selection');

}

# family from scratch
my $orphans = Greenphyl::Family->new(undef, {'members' => {'id' => -1, 'name' => 'Unclassified (orphans)'}});
ok($orphans, "family creation from scratch");
ok($orphans->id, "scratch family ID");

done_testing();
