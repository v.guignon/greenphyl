#!/usr/bin/perl

use strict;
use warnings;
use FindBin;
use lib "$FindBin::Bin/../lib";
use lib "$FindBin::Bin/../local_lib";


use Test::Most;
# use Test::Regression;
use Carp qw (cluck confess croak);

BEGIN {
    use_ok( 'Greenphyl' );
}
my $sequence_content = ">Os01g01050.1
MINFAVQSRCAPTNSLCSCVALCERNFKLEQVEELASLIKDNLYSKHLVLSTEETLVGIL
QNQYHNSDDDEDEDDIVAAYRGTNRNILELQPASSYQRLLLHRLADIYGFVHESVGEGED
RHLVLQRCPETAIPPVLVSDVLWEYDNKDTSTSVVVKRKDTDLEEAWKEDAQENISAEIS
HLKNDADLKALQKSVAPPAPSLKEREAAYRAARERIFSAHDAKGNGTAVAKPRHVPAVAQ
RMIAHALGKKVESPTETAAVKNGKGKEPAESSRNKLNPRTAGGKEDSRYVENGRMRLHTG
NPCKQSWRTSNSRAASSVSPDELKREQVGAAKRMFVHALRLPGVEGSDGPVRKGK*";

my $BLAST_QUERY_URL = GetURL('blast', { 'p' => 'results', 'sequence' => $sequence_content, 'database' => 'All', 'program' => 'blastp', 'evalue' => '1e-10', 'blast_submit' => 'Launch BLAST',});
my $REFERENCE_BLAST_RESULT_FILENAME = 'data/ref_test.blast';
my $BLAST_RESULT_FILENAME = 'test.blast';


# if the parameter 'gen' is first on the command line, the regression files are
# not checked against output, but instead generated from output
if ($ARGV[0] && ($ARGV[0] eq 'gen'))
{
    $ENV{TEST_REGRESSION_GEN} = 1;
}

# check config
print "Config Check:\n-------------\n";
ok(defined($Greenphyl::Config::BLAST_BANKS_PATH),         'BLAST bank path defined');
ok($Greenphyl::Config::BLAST_BANKS_PATH,         'BLAST bank path set');
ok(defined($Greenphyl::Config::ALL_BLAST_BANK),           'ALL BLAST bank file path defined');
ok($Greenphyl::Config::ALL_BLAST_BANK,           'ALL BLAST bank file path set');
ok(defined($Greenphyl::Config::BLASTALL_COMMAND),         'BLAST command defined');
ok($Greenphyl::Config::BLASTALL_COMMAND,         'BLAST command set');
ok(defined($Greenphyl::Config::CLUSTER_BLASTALL_COMMAND), 'Cluster BLAST command defined');
ok($Greenphyl::Config::CLUSTER_BLASTALL_COMMAND, 'Cluster BLAST command set');

print <<"___34_CONFIG___";

Config content:
---------------
BLAST bank path:          $Greenphyl::Config::BLAST_BANKS_PATH
ALL BLAST bank file path: $Greenphyl::Config::ALL_BLAST_BANK
BLAST command:            $Greenphyl::Config::BLASTALL_COMMAND
Cluster BLAST command:    $Greenphyl::Config::CLUSTER_BLASTALL_COMMAND

___34_CONFIG___


print "BLAST Execution Check:\n----------------------\n";
# launch web BLAST
# options wget: --user=... --password=...
my @wget_parameters = ("--output-document=$BLAST_RESULT_FILENAME", "$BLAST_QUERY_URL");

# dev http password protected?
if ($BLAST_QUERY_URL =~ m~greenphyl-dev\.cirad\.fr~)
{
    push(@wget_parameters, "--user=dev", "--password=Pa5dGr3En");
}

if (system('wget', @wget_parameters))
{
    confess "ERROR: Failed to execute command:\n    wget " . join(' ', @wget_parameters) . "\n\n$!\n";
}

my $blast_result_fh;
open($blast_result_fh, $BLAST_RESULT_FILENAME) or confess "Failed to open BLAST result file!\n$!\n";

#ok_regression(
#    sub
#    {
#        my $blast_data = join('', <$blast_result_fh>);
#        $blast_data =~ s/.*<!-- END global_header\.tt -->//s;
#        $blast_data =~ s/<!-- BEGIN global_footer\.tt -->.*//s;
#        return $blast_data;
#    },
#    $REFERENCE_BLAST_RESULT_FILENAME,
#    'BLAST result content ok');

my $blast_data = join('', <$blast_result_fh>);
$blast_data =~ s/.*<!-- END global_header\.tt -->//s;
$blast_data =~ s/<!-- BEGIN global_footer\.tt -->.*//s;

print <<"___78_HTML_RESULT___";

HTML Output:
--------------------------------------------------------------------------------
$blast_data

--------------------------------------------------------------------------------
___78_HTML_RESULT___

ok($blast_data =~ m/Score\s*=\s*\d+\s*bits/, "BLAST result contains Score");
ok($blast_data =~ m/Expect\s*=\s*\d/, "BLAST result contains Expect");
ok($blast_data =~ m/Method:\s+/, "BLAST result contains Method");
ok($blast_data =~ m/Identities\s*=\s*\d+\/\d+/, "BLAST result contains Identities");
ok($blast_data =~ m/Positives\s*=\s*\d+\/\d+/, "BLAST result contains Positives");
ok($blast_data =~ m/Query:\s*\d+/, "BLAST result contains Query");
ok($blast_data =~ m/Sbjct:\s*\d+/, "BLAST result contains Sbjct");

close($blast_result_fh);

# remove test file
unlink($BLAST_RESULT_FILENAME);

=pod

#+TODO
tester la partie web:
-cas d'une sequence sans match
-cas d'une sequence avec match sur une sequence hors base
-cas d'une sequence avec match sur une sequence sans famille
-cas d'une sequence avec match sur une sequence normale

=cut

done_testing();
