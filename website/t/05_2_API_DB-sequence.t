#!/usr/bin/perl

use strict;
use warnings;

use lib '../lib';
use lib '../local_lib';


use Test::Most;
# use Test::Regression;
use Carp qw (cluck confess croak);

use List::Util 'shuffle'; # randomize list order

BEGIN {
    use_ok('Greenphyl' );
    use_ok('Greenphyl::Sequence');
    use_ok('Greenphyl::Tools::Sequences');
}

ok(ValidateSequenceName('Cre01_g005900-t1.1'), 'Sequence name validation for good names');
ok(!ValidateSequenceName('Cre01%g005900.t1.1'), 'Sequence name validation for bad names');
# sequence
my $sequence = Greenphyl::Sequence->new(
    GetDatabaseHandler(),
    {
        'selectors' => {'accession' => ['LIKE', 'Os01g01050%'],},
    },
);
ok($sequence, 'Sequence object creation from database');
SEQUENCE_INSTANCE:
{
    skip 'Sequence not instantiated, skipping tests.' unless $sequence;
    # explain('Sequence instance: ', $sequence);
    ok(@{$sequence->families()}, 'Load sequence families');
    # explain('Sequence instance: ', $sequence);

    # create a new sequence from scratch
    $sequence = Greenphyl::Sequence->new(
        undef,
        {
            'members' => {'accession' => 'test_sequence', 'id' => -1, },
        },
    );
    ok($sequence, 'Sequence object creation from scratch');
    # explain('Sequence instance: ', $sequence);
    
    my @sequences = Greenphyl::Sequence->new(
        GetDatabaseHandler(),
        {
            'selectors' => [{'accession' => ['LIKE', 'Os01g01050%'],}, 'OR', [{'accession' => ['LIKE', 'At1%'],}, 'AND', {'species_id' => 2,}]], # 2 --> ARATH
        },
    );
    ok(@sequences > 2, 'Sequences loading using multi-like with OR');
    # print "Loaded sequences: " . scalar(@sequences) . "\n";
    # explain('Loaded sequences: ', splice(@sequences, 0, 10));
    # print "...\n";
    
    # Count
    my $parameters =
        {
            'selectors' => {'species_id' => 2,},
        },
    ;
    my $sequence_count = Greenphyl::Sequence->Count(
        GetDatabaseHandler(),
        $parameters,
    );
    @sequences = Greenphyl::Sequence->new(
        GetDatabaseHandler(),
        $parameters,
    );
    # print "Got count: $sequence_count for " . scalar(@sequences) . " sequences\n";
    ok($sequence_count == scalar(@sequences), 'Sequences Count');
    
    # try sorting...
    # reload sequence from cache
    $sequence = Greenphyl::Sequence->new(
        GetDatabaseHandler(),
        {
            'selectors' => {'accession' => 'Os01g01030.1',},
        },
    );
    # by default, families are sorted by level
    my @families = sort {$a->id <=> $b->id} @{$sequence->families};
    is_deeply(\@families, [$sequence->sortFamilies([])], 'Default sort');
    # revert
    @families = sort {$b->id <=> $a->id} @families;
    is_deeply(\@families, [$sequence->sortFamilies(['id', 'ANDESC'])], 'Revert sort');
    # sub-sort
    is_deeply(\@families, [$sequence->sortFamilies(['black_listed', 'ASC', 'id', 'ANDESC'])], 'Sub-revert sort');
    # non-object sort
    my @ipr_go_id = sort {$b <=> $a} @{$sequence->ipr_go_id};
    is_deeply(\@ipr_go_id, [$sequence->sortIprGoID(['NDESC'])], 'Non-object sort');

    #disabled: we don't have sequence synonyms at the moment...
    # load from synonym
    $sequence = Greenphyl::Sequence->new(
        GetDatabaseHandler(),
        {
            'load'      => { 'synonyms' => 1,},
            'selectors' => {'sequence_synonyms.synonym' => 'GSMUA_Achr10T23190_001',},
        },
    );
    ok($sequence && ('GSMUA_Achr10P23190_001' eq $sequence->accession), 'Sequence loaded using synonym');

    
    # splice test
    my %ACCESSION_SPLICE_TEST = (
        'evm_27.model.AmTr_v1.0_scaffold00001.24' =>
            {
                'species_code' => 'AMBTC',
                'locus'        => 'evm_27.model.AmTr_v1.0_scaffold00001.24',
                'splice'       => undef,
                'locus_wosc'   => 'evm_27.model.AmTr_v1.0_scaffold00001',
                'splice_wosc'  => 24,
            },
        'evm_27.model.AmTr_v1.0_scaffold00001.1' =>
            {
                'species_code' => 'AMBTC',
                'locus'        => 'evm_27.model.AmTr_v1.0_scaffold00001.1',
                'splice'       => undef,
                'locus_wosc'   => 'evm_27.model.AmTr_v1.0_scaffold00001',
                'splice_wosc'  => 1,
            },
        'AT1G65960.1' =>
            {
                'species_code' => 'ARATH',
                'locus'        => 'AT1G65960',
                'splice'       => 1,
                'locus_wosc'   => 'AT1G65960',
                'splice_wosc'  => 1,
            },
        'Bradi0038s00200.2' =>
            {
                'species_code' => 'BRADI',
                'locus'        => 'Bradi0038s00200',
                'splice'       => 2,
                'locus_wosc'   => 'Bradi0038s00200',
                'splice_wosc'  => 2,
            },
        'Bradi2g26090.3' =>
            {
                'species_code' => 'BRADI',
                'locus'        => 'Bradi2g26090',
                'splice'       => 3,
                'locus_wosc'   => 'Bradi2g26090',
                'splice_wosc'  => 3,
            },
        'supercontig_0.10' =>
            {
                'species_code' => 'CARPA',
                'locus'        => 'supercontig_0.10',
                'splice'       => undef,
                'locus_wosc'   => 'supercontig_0',
                'splice_wosc'  => 10,
            },
        'evm.TU.contig_24415.10' =>
            {
                'species_code' => 'CARPA',
                'locus'        => 'evm.TU.contig_24415',
                'splice'       => 10,
                'locus_wosc'   => 'evm.TU.contig_24415',
                'splice_wosc'  => 10,
            },
        'g18373.t1' =>
            {
                'species_code' => 'CHLRE',
                'locus'        => 'g18373',
                'splice'       => 1,
                'locus_wosc'   => 'g18373.t1',
                'splice_wosc'  => undef,
            },
        'Cre03.g167650.t1.3' =>
            {
                'species_code' => 'CHLRE',
                'locus'        => 'Cre03.g167650',
                'splice'       => 13,
                'locus_wosc'   => 'Cre03.g167650.t1',
                'splice_wosc'  => 3,
            },
        'Cucsa.000210.2' =>
            {
                'species_code' => 'CUCSA',
                'locus'        => 'Cucsa.000210',
                'splice'       => 2,
                'locus_wosc'   => 'Cucsa.000210',
                'splice_wosc'  => 2,
            },
        'Glyma0120s50.1' =>
            {
                'species_code' => 'GLYMA',
                'locus'        => 'Glyma0120s50',
                'splice'       => 1,
                'locus_wosc'   => 'Glyma0120s50',
                'splice_wosc'  => 1,
            },
        'Glyma0023s00515.1' =>
            {
                'species_code' => 'GLYMA',
                'locus'        => 'Glyma0023s00515',
                'splice'       => 1,
                'locus_wosc'   => 'Glyma0023s00515',
                'splice_wosc'  => 1,
            },
        'Gorai.010G215500.4' =>
            {
                'species_code' => 'GOSRA',
                'locus'        => 'Gorai.010G215500',
                'splice'       => 4,
                'locus_wosc'   => 'Gorai.010G215500',
                'splice_wosc'  => 4,
            },
        'chr1.CM0001.20.r2.a' =>
            {
                'species_code' => 'LOTJA',
                'locus'        => 'chr1.CM0001.20.r2.a',
                'splice'       => undef,
                'locus_wosc'   => 'chr1.CM0001.20.r2.a',
                'splice_wosc'  => undef,
            },
        'LjSGA_000040.2.1' =>
            {
                'species_code' => 'LOTJA',
                'locus'        => 'LjSGA_000040.2.1',
                'splice'       => undef,
                'locus_wosc'   => 'LjSGA_000040.2',
                'splice_wosc'  => 1,
            },
        'LjSGA_000021.1' =>
            {
                'species_code' => 'LOTJA',
                'locus'        => 'LjSGA_000021.1',
                'splice'       => undef,
                'locus_wosc'   => 'LjSGA_000021',
                'splice_wosc'  => 1,
            },
        'contig_52881_3.1' =>
            {
                'species_code' => 'MEDTR',
                'locus'        => 'contig_52881_3',
                'splice'       => 1,
                'locus_wosc'   => 'contig_52881_3',
                'splice_wosc'  => 1,
            },
        'AC146630_4.2' =>
            {
                'species_code' => 'MEDTR',
                'locus'        => 'AC146630_4',
                'splice'       => 2,
                'locus_wosc'   => 'AC146630_4',
                'splice_wosc'  => 2,
            },
        'AC235753_1023.1' =>
            {
                'species_code' => 'MEDTR',
                'locus'        => 'AC235753_1023',
                'splice'       => 1,
                'locus_wosc'   => 'AC235753_1023',
                'splice_wosc'  => 1,
            },
        'ITC1587_Bchr10_T28356' =>
            {
                'species_code' => 'MUSBA',
                'locus'        => 'ITC1587_Bchr10_T28356',
                'splice'       => undef,
                'locus_wosc'   => 'ITC1587_Bchr10_T28356',
                'splice_wosc'  => undef,
            },
        'ITC1587_Bchr5_P12023' =>
            {
                'species_code' => 'MUSBA',
                'locus'        => 'ITC1587_Bchr5_P12023',
                'splice'       => undef,
                'locus_wosc'   => 'ITC1587_Bchr5_P12023',
                'splice_wosc'  => undef,
            },
        'Os01g01040.4' =>
            {
                'species_code' => 'ORYSA',
                'locus'        => 'Os01g01040',
                'splice'       => 4,
                'locus_wosc'   => 'Os01g01040',
                'splice_wosc'  => 4,
            },
        'Phvul.010G028000.2' =>
            {
                'species_code' => 'PHAVU',
                'locus'        => 'Phvul.010G028000',
                'splice'       => 2,
                'locus_wosc'   => 'Phvul.010G028000',
                'splice_wosc'  => 2,
            },
        'Pp1s1_4V6.1' =>
            {
                'species_code' => 'PHYPA',
                'locus'        => 'Pp1s1_4V6',
                'splice'       => 1,
                'locus_wosc'   => 'Pp1s1_4V6',
                'splice_wosc'  => 1,
            },
        'Pp1s6122_1V6.2' =>
            {
                'species_code' => 'PHYPA',
                'locus'        => 'Pp1s6122_1V6',
                'splice'       => 2,
                'locus_wosc'   => 'Pp1s6122_1V6',
                'splice_wosc'  => 2,
            },
        'Potri.T155200.1' =>
            {
                'species_code' => 'POPTR',
                'locus'        => 'Potri.T155200',
                'splice'       => 1,
                'locus_wosc'   => 'Potri.T155200',
                'splice_wosc'  => 1,
            },
        'Potri.010G069000.2' =>
            {
                'species_code' => 'POPTR',
                'locus'        => 'Potri.010G069000',
                'splice'       => 2,
                'locus_wosc'   => 'Potri.010G069000',
                'splice_wosc'  => 2,
            },
        'Sb0011s011820.1' =>
            {
                'species_code' => 'SORBI',
                'locus'        => 'Sb0011s011820',
                'splice'       => 1,
                'locus_wosc'   => 'Sb0011s011820',
                'splice_wosc'  => 1,
            },
        'GRMZM2G055768_P02' =>
            {
                'species_code' => 'ZEAMA',
                'locus'        => 'GRMZM2G055768',
                'splice'       => 2,
                'locus_wosc'   => 'GRMZM2G055768_P02',
                'splice_wosc'  => undef,
            },
        'AC190570.3_FGP003' =>
            {
                'species_code' => 'ZEAMA',
                'locus'        => 'AC190570.3_FGP003',
                'splice'       => undef,
                'locus_wosc'   => 'AC190570.3_FGP003',
                'splice_wosc'  => undef,
            },
    );
    
    while (my ($accession, $splice_data) = each(%ACCESSION_SPLICE_TEST))
    {
        # with species code
        my ($locus, $splice) = ExtractLocusAndSpliceForm($accession, $splice_data->{'species_code'});
        ok(($locus eq $splice_data->{'locus'}),   "Locus for '$accession' ($splice_data->{'species_code'}) with species code");
        if (defined($splice_data->{'splice'}))
        {
            ok(($splice == $splice_data->{'splice'}), "Splice for '$accession' ($splice_data->{'species_code'}) with species code");
        }
        else
        {
            ok(!defined($splice), "Splice for '$accession' ($splice_data->{'species_code'}) with species code");
        }

        # with species code to guess
        ($locus, $splice) = ExtractLocusAndSpliceForm($accession . '_' . $splice_data->{'species_code'});
        ok(($locus eq $splice_data->{'locus'}),   "Locus for '" . $accession . "_" . $splice_data->{'species_code'} . "'");
        if (defined($splice_data->{'splice'}))
        {
            ok(($splice == $splice_data->{'splice'}), "Splice for '" . $accession . "_" . $splice_data->{'species_code'} . "'");
        }
        else
        {
            ok(!defined($splice), "Splice for '" . $accession . "_" . $splice_data->{'species_code'} . "'");
        }

        # without species code
        ($locus, $splice) = ExtractLocusAndSpliceForm($accession);
        ok(($locus eq $splice_data->{'locus_wosc'}),   "Locus for '$accession' ($splice_data->{'species_code'}) without species code");
        if (defined($splice_data->{'splice_wosc'}))
        {
            ok(($splice == $splice_data->{'splice_wosc'}), "Splice for '$accession' ($splice_data->{'species_code'}) without species code");
        }
        else
        {
            ok(!defined($splice), "Splice for '$accession' ($splice_data->{'species_code'}) without species code");
        }
    }
    
    # splice form filtering tests
    #
    # 
    my %splice_seq_textids = (
        'Solyc06g075110.1.2' => 'SOLLY',
        'Solyc06g075110.2.1' => 'SOLLY',
        'Solyc06g075110.2.2' => 'SOLLY',
        'Solyc06g075110.2.3' => 'SOLLY',
        'Sb01g011030.1'      => 'SORBI',
        'Sb01g011030.2'      => 'SORBI',
        'Os04g58410.1'       => 'ORYSA',
        'Os04g58410.2'       => 'ORYSA',
        'Os04g58410.3'       => 'ORYSA',
        'Bradi1g15260.1'     => 'BRADI',
        'Bradi1g15260.2'     => 'BRADI',
        'GRMZM2G024690_P01'  => 'ZEAMA',
        'GRMZM2G024690_P02'  => 'ZEAMA',
        'GRMZM2G024690_P03'  => 'ZEAMA',
        'GRMZM2G060349_P03'  => 'ZEAMA',
        'GRMZM2G060349_P04'  => 'ZEAMA',
        'GRMZM2G060349_P05'  => 'ZEAMA',
        'POPTR_0015s03100.1' => 'POPTR',
        'POPTR_0015s03100.2' => 'POPTR',
        'POPTR_0015s03100.3' => 'POPTR',
        'supercontig_12.181' => 'CARPA',
        'supercontig_29.179' => 'CARPA',
        'supercontig_29.181' => 'CARPA',
        'Glyma17g13080.1'    => 'GLYMA',
        'Glyma17g13080.2'    => 'GLYMA',
        'Cucsa.107670.1'     => 'CUCSA',
        'Cucsa.107670.2'     => 'CUCSA',
        'Cucsa.107670.3'     => 'CUCSA',
        'Cucsa.107670.4'     => 'CUCSA',
        'Medtr4g124880.2'    => 'MEDTR',
        'At1g64810.1'        => 'ARATH',
        'AT1G64810.2'        => 'ARATH',
        'at1g64810.3'        => 'ARATH',
    );
    my %SEQUENCE_LENGTH = (
        'Solyc06g075110.1.2' => 10, #
        'Solyc06g075110.2.1' => 12,
        'Solyc06g075110.2.2' => 14, #
        'Solyc06g075110.2.3' =>  9,
        'Sb01g011030.1'      => 11, #
        'Sb01g011030.2'      => 10,
        'Os04g58410.1'       => 10,
        'Os04g58410.2'       => 10,
        'Os04g58410.3'       => 11, #
        'Bradi1g15260.1'     => 10, #
        'Bradi1g15260.2'     => 10,
        'GRMZM2G024690_P01'  =>  9,
        'GRMZM2G024690_P02'  => 10, #
        'GRMZM2G024690_P03'  => 10,
        'GRMZM2G060349_P03'  => 10, #
        'GRMZM2G060349_P04'  => 10,
        'GRMZM2G060349_P05'  => 10,
        'POPTR_0015s03100.1' => 11, #
        'POPTR_0015s03100.2' => 10,
        'POPTR_0015s03100.3' => 10,
        'supercontig_12.181' => 10, #
        'supercontig_29.179' => 11, #
        'supercontig_29.181' => 10, #
        'Glyma17g13080.1'    => 10, #
        'Glyma17g13080.2'    => 10,
        'Cucsa.107670.1'     => 10,
        'Cucsa.107670.2'     => 10,
        'Cucsa.107670.3'     => 11, #
        'Cucsa.107670.4'     => 10,
        'Medtr4g124880.1'    => 10,
        'Medtr4g124880.2'    => 11, #
        'At1g64810.1'        => 10, #
        'AT1G64810.2'        => 10,
        'at1g64810.3'        => 10,
    );
    my %SEQUENCE_SPLICE_PRIORITY = (
        'Solyc06g075110.1.2' => 0, #
        'Solyc06g075110.2.1' => 0,
        'Solyc06g075110.2.2' => 0, #
        'Solyc06g075110.2.3' => 0,
        'Sb01g011030.1'      => 0, #
        'Sb01g011030.2'      => 1,
        'Os04g58410.1'       => 1,
        'Os04g58410.2'       => 2,
        'Os04g58410.3'       => 0, #
        'Bradi1g15260.1'     => 0, #
        'Bradi1g15260.2'     => 1,
        'GRMZM2G024690_P01'  => 2,
        'GRMZM2G024690_P02'  => 0, #
        'GRMZM2G024690_P03'  => 1,
        'GRMZM2G060349_P03'  => 0, #
        'GRMZM2G060349_P04'  => 1,
        'GRMZM2G060349_P05'  => 2,
        'POPTR_0015s03100.1' => 0, #
        'POPTR_0015s03100.2' => 1,
        'POPTR_0015s03100.3' => 2,
        'supercontig_12.181' => 0, #
        'supercontig_29.179' => 0, #
        'supercontig_29.181' => 0, #
        'Glyma17g13080.1'    => 0, #
        'Glyma17g13080.2'    => 1,
        'Cucsa.107670.1'     => 1,
        'Cucsa.107670.2'     => 2,
        'Cucsa.107670.3'     => 0, #
        'Cucsa.107670.4'     => 3,
        'Medtr4g124880.1'    => 1,
        'Medtr4g124880.2'    => 0, #
        'At1g64810.1'        => 0, #
        'AT1G64810.2'        => 1,
        'at1g64810.3'        => 2,
    );
    my @expected_filtered_list = (
        'Solyc06g075110.1.2',
        'Solyc06g075110.2.1',
        'Solyc06g075110.2.2',
        'Solyc06g075110.2.3',
        'Sb01g011030.1',
        'Os04g58410.3',
        'Bradi1g15260.1',
        'GRMZM2G024690_P02',
        'GRMZM2G060349_P03',
        'POPTR_0015s03100.1',
        'supercontig_12.181',
        'supercontig_29.179',
        'supercontig_29.181',
        'Glyma17g13080.1',
        'Cucsa.107670.3',
        'Medtr4g124880.2',
        'At1g64810.1',
    );
    
    # shuffle list to make sure order does not matter
    my @splice_seq_textids = shuffle(keys(%splice_seq_textids));
    #instanciate sequences
    my $sequences = [];
    foreach my $seq_textid (@splice_seq_textids)
    {
        push(
            @$sequences,
            Greenphyl::Sequence->new(
                undef,
                {
                    'members' => {
                        'id'     => -1,
                        'accession' => $seq_textid,
                        'length' => $SEQUENCE_LENGTH{$seq_textid},
                        'splice_priority' => $SEQUENCE_SPLICE_PRIORITY{$seq_textid},
                        'species' => Greenphyl::Species->new(
                            undef,
                            {
                                'members' => {
                                    'id'   => -1,
                                    'code' => $splice_seq_textids{$seq_textid},
                                },
                            },
                        ),
                    },
                },
            )
        );
    }
    # filter
    my $filtered_sequences = FilterSpliceForm($sequences);
    # print "DEBUG: filtered_list: " . scalar(@$filtered_sequences) ." from " . scalar(@$sequences) . "\n";
    # get labels
    my @filtered_list = map {$_->accession} @$filtered_sequences;
    # sort result
    @filtered_list = sort(@filtered_list);
    @expected_filtered_list = sort(@expected_filtered_list);
    # compare with expected
    is_deeply(\@filtered_list, \@expected_filtered_list, 'Splice form filtering');

}

done_testing();
