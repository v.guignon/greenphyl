#!/usr/bin/perl

use strict;
use warnings;

use lib '../lib';
use lib '../local_lib';

use Carp qw (cluck confess croak);
use Test::Harness;

use Greenphyl;
use Greenphyl::Family;
use Greenphyl::Tools::AnalysisProcesses('o');


$ENV{'TESTING'} = 1;

#
# First, create a process in database and then run other tests.
# Last test (09_999_fpcleanup.t) cleans up database.
# To run test one by one by hand, add the parameter 'skip':
#   % perl 09_family_processing.t skip
#     Test process ID: 42
#   % export TESTING=1
#   % export TEST_ANALYSIS_PROCESS_ID=42
#   % perl 09_1_fpwarn.t
#   % ...
#   % perl 09_999_fpcleanup.t

# note: any change here should be reported in 09_999_fpcleanup.t
my $TEST_PROCESS_NAME = 'RegressionTests';

# create a new process ind DB
my $process = CreateAnalysisProcess(
    {
        'name'=> $TEST_PROCESS_NAME,
        'comments' => 'Regression tests - can be removed manually if not done by the tests.',
        'status' => $Greenphyl::Tools::AnalysisProcesses::PROCESS_STATUS_TO_PERFORM,
    }
);
if (!$process)
{
    confess "ERROR: unable to create process! Tests aborted!\n";
}
# add family 2 to the process
my $family_2 = Greenphyl::Family->new(
    GetDatabaseHandler(),
    {
        'selectors' => {'id' => 2},
    },
);
AddFamiliesToProcess([$family_2], $process);


# set environment variable
$ENV{'TEST_ANALYSIS_PROCESS_ID'} = $process->id;
warn "Test process ID: " . $process->id . "\n";

# check if test should be skipped and test process left in database to
# run other tests by hand
if ($ARGV[0] && ($ARGV[0] eq 'skip'))
{
    exit(0);
}

runtests(
    '09_1_fpbase.t',
    '09_2_fpwarn.t',
    '09_3_fprequired.t',
    '09_4_fpauto.t',
    '09_5_fpoptional.t',
    # last: cleanup
    '09_999_fpcleanup.t'
);
