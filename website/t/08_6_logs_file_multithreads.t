#!/usr/bin/perl

use strict;
use warnings;

use lib '../lib';
use lib '../local_lib';


use threads;
use threads::shared;

use Time::HiRes qw(sleep);

use Test::Most;
use Test::Regression;
use Carp qw (cluck confess croak);

my $REFERENCE_LOG_FILENAME = 'logs/ref_test_threads' . $Greenphyl::Log::LOG_FILE_EXTENSION;
my $TEST_LOG_NAME; # set in BEGIN part
my $log_file_path;

BEGIN
{
    $TEST_LOG_NAME = 'test_threads';
    use_ok('Greenphyl');
    use_ok('Greenphyl::Config');
    use_ok('Greenphyl::Log', $TEST_LOG_NAME);
    # -generate filename timestamp
    my ($second, $minute, $hour, $day_of_month, $month, $year_offset, $day_of_week, $day_of_year, $daylight_savings) = localtime();
    ++$month;
    my $year = 1900 + $year_offset;
    $log_file_path = sprintf("%s_%04i%02i%02i%02i%02i%02i%s", $TEST_LOG_NAME, $year, $month, $day_of_month, $hour, $minute, $second, $Greenphyl::Log::LOG_FILE_EXTENSION);
}

my $THREAD_COUNT = 8;
my $LOG_MESSAGE_PER_THREAD_COUNT = 16;

++$|; # autoflush

# if the parameter 'gen' is first on the command line, the regression files are
# not checked against output, but instead generated from output
if ($ARGV[0] && ($ARGV[0] eq 'gen'))
{
    $ENV{'TEST_REGRESSION_GEN'} = 1;
}
$ENV{'TESTING'} = 1;

$log_file_path = GP('LOG_PATH') . '/' . $log_file_path;
# check file creation
ok($Greenphyl::Log::g_log_fh, 'Log file opened');
ok(-e $log_file_path, 'log file has expected name');

sub RunTestThread
{
    # since this procedure is run from threads, modules should be reloaded.
    use strict;
    use warnings;

    use lib '../lib';
    use lib '../local_lib';
    use Carp qw (cluck confess croak);

    my $test_message = 'test db message for thread ' . threads->tid() . ": ";
    for (1..int($LOG_MESSAGE_PER_THREAD_COUNT/2))
    {
        LogInfo($test_message . $_);
    }
    sleep(0.2+rand());
    for (1..int($LOG_MESSAGE_PER_THREAD_COUNT/2+0.5))
    {
        LogWarning($test_message . $_);
    }
}

my @threads_list;
for (my $slot_number = 0; $slot_number < $THREAD_COUNT; ++$slot_number)
{
    push(@threads_list, threads->create(\&RunTestThread));
}

foreach my $thread (@threads_list)
{
    $thread->join();
}
sleep(1);

# check if ref file must be updated
my $log_fh;
open($log_fh, $log_file_path) or confess "Failed to open generated log ($log_file_path)!\n$!\n";
my $log_data = join('', <$log_fh>);
$log_data =~ s/^GREENPHYL LOG[^\n]+/GREENPHYL LOG/m;

if ($ENV{'TEST_REGRESSION_GEN'})
{
    my $ref_fh;
    open($ref_fh, ">$REFERENCE_LOG_FILENAME") or confess "Failed to generate reference log file ($REFERENCE_LOG_FILENAME)!\n$!\n";
    print {$ref_fh} $log_data;
    close($ref_fh);
}
$ENV{'TESTING'} = 1;

close($log_fh);

# ok_regression(sub { my $log_data = join('', <$log_fh>); $log_data =~ s/^GREENPHYL LOG[^\n]+/GREENPHYL LOG/m; return $log_data;}, $REFERENCE_LOG_FILENAME, 'Log file content ok');
my $output_log_size = (-s $REFERENCE_LOG_FILENAME) || 0;
print "$REFERENCE_LOG_FILENAME\n";
ok(length($log_data) == $output_log_size, 'Log files share the same size');

my $ref_fh;
open($ref_fh, $REFERENCE_LOG_FILENAME) or confess "Failed to open reference log file ($REFERENCE_LOG_FILENAME)!\n$!\n";
# check content
my $missing_lines = 0;
while (my $line = <$ref_fh>)
{
    if ($log_data !~ m/$line/)
    {
        ++$missing_lines;
    }
}
close($ref_fh);
ok(0 == $missing_lines, 'Log content');

# remove test file
unlink($log_file_path);

done_testing();
