#!/usr/bin/perl

use strict;
use warnings;

use lib '../lib';
use lib '../local_lib';


use Test::Most;
use Test::Regression;
use Carp qw (cluck confess croak);

my $REFERENCE_LOG_FILENAME = 'logs/ref_test' . $Greenphyl::Log::LOG_FILE_EXTENSION;
our $TEST_LOG_NAME; # set in BEGIN part

BEGIN {
    $TEST_LOG_NAME = 'test';
    use_ok('Greenphyl::Log',
        'logfile'         => $TEST_LOG_NAME,
        'logpath'         => '.',
        'nodb'            => 1,
        'nofiletimestamp' => 1,
        'append'          => 0,
        'logtimestamp'    => 0,
    );
}

# if the parameter 'gen' is first on the command line, the regression files are
# not checked against output, but instead generated from output
if ($ARGV[0] && ($ARGV[0] eq 'gen'))
{
    $ENV{'TEST_REGRESSION_GEN'} = 1;
}
$ENV{'TESTING'} = 1;

# check config
ok(defined($Greenphyl::Log::LOG_FILE_NAME_DEFAULT), 'Default log file name defined');
ok(defined($Greenphyl::Log::LOG_FILE_EXTENSION), 'Log file extension defined');

# check file creation
ok($Greenphyl::Log::g_log_fh, 'Log file opened');
ok(-e "$TEST_LOG_NAME$Greenphyl::Log::LOG_FILE_EXTENSION", 'log file has expected name');

# test loggers
LogInfo('test message 1');
LogDebug('test message 2');
LogWarning('test message 3');
LogError('test message 4');
LogInfo("test message 5\non\r\nseveral\rlines.\n");
LogWarning("test another message 6\nalso on\r\nseveral nice\rshort lines!\n");

# ends (closes) log
Greenphyl::Log::_ResetConfig();

my $log_fh;
open($log_fh, "$TEST_LOG_NAME$Greenphyl::Log::LOG_FILE_EXTENSION") or confess "Failed to open generated log!\n$!\n";
ok_regression(sub { my $log_data = join('', <$log_fh>); $log_data =~ s/^GREENPHYL LOG[^\n]+/GREENPHYL LOG/m; return $log_data;}, $REFERENCE_LOG_FILENAME, 'Log file content ok');
close($log_fh);

# remove test file
unlink("$TEST_LOG_NAME$Greenphyl::Log::LOG_FILE_EXTENSION");

done_testing();
