#!/usr/bin/perl

use strict;
use warnings;

use lib '../lib';
use lib '../local_lib';


use Test::Most;
use Test::Regression;
use Carp qw (cluck confess croak);

use Greenphyl;
use Greenphyl::AnalysisProcess;

BEGIN {
    use_ok("Greenphyl::Tools::AnalysisProcesses", 'optional');
}

$ENV{'TESTING'} = 1;

my $process_id = $ENV{'TEST_ANALYSIS_PROCESS_ID'};
my $process    = GetCurrentProcess();

if (!$process_id)
{
    confess "ERROR: no process ID provided! Either use the parameters process_id=<ID> or the environment variable 'export TEST_ANALYSIS_PROCESS_ID=<ID>'\n";
}

# check argument transfer
ok(GetProcessParameter() eq "process_id=$process_id process_mode=o", 'Command line argument transfer');

#+FIXME: check lib loads ok if process ID missing
#+FIXME: test with a process ID:
#+FIXME: families in list: no warning
#+FIXME: families auto-added:
# -make sure no warning is issued
# -make sure the family is added

done_testing();
