#!/usr/bin/perl

use strict;
use warnings;

use lib '../lib';
use lib '../local_lib';


use Test::Most;
use Test::Regression;
use Carp qw (cluck confess croak);

use Greenphyl;
use Greenphyl::AnalysisProcess;
use Greenphyl::Tools::AnalysisProcesses('o');

# note: any change here should be reported in 09_family_processing.t
my $TEST_PROCESS_NAME = 'RegressionTests';

# if the parameter 'gen' is first on the command line, the regression files are
# not checked against output, but instead generated from output
if ($ARGV[0] && ($ARGV[0] eq 'gen'))
{
    $ENV{'TEST_REGRESSION_GEN'} = 1;
}
$ENV{'TESTING'} = 1;

my ($sql_query, $query_ok);
my $process_id = $ENV{'TEST_ANALYSIS_PROCESS_ID'};
if ($process_id)
{
    $sql_query = "
        DELETE
        FROM processes
        WHERE id = " . $process_id . ";
    ";
    ($query_ok) = GetDatabaseHandler()->do($sql_query);
    ok($query_ok, 'Test process removal query executed');

    # clear cache
    Greenphyl::AnalysisProcess->ClearCache();
    my $process = Greenphyl::AnalysisProcess->new(
        GetDatabaseHandler(),
        {'selectors' => {'id' => $process_id,} }
    );
    ok(!$process, 'Test process removed');
    
    delete($ENV{'TEST_ANALYSIS_PROCESS_ID'});
}
else
{
    warn "No test process set (TEST_ANALYSIS_PROCESS_ID environment variable)!\n";
    # in case environment variable not set, try to remove regression test porcesses
    $sql_query = "
        DELETE
        FROM processes
        WHERE name = '$TEST_PROCESS_NAME'
    ";
    PrintDebug("SQL QUERY: $sql_query");
    ($query_ok) = GetDatabaseHandler()->do($sql_query);
    if (!$query_ok)
    {
        warn "Warning: unable to remove test process. Maybe it has not been created.\n";
    }
}


done_testing();
