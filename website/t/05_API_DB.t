#!/usr/bin/perl

use strict;
use warnings;

use lib '../lib';
use lib '../local_lib';


use Carp qw (cluck confess croak);
use Test::Harness;

runtests(
    '05_1_API_DB-family.t',
    '05_2_API_DB-sequence.t',
    '05_3_API_DB-ipr.t',
    '05_4_API_DB-composite.t',
    '05_5_API_DB-go.t',
    '05_6_API_DB-cache.t',
);
