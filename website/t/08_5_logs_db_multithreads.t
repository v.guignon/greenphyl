#!/usr/bin/perl

use strict;
use warnings;

use lib '../lib';
use lib '../local_lib';


use threads;
use threads::shared;

use Time::HiRes qw(sleep);

use Test::Most;
use Test::Regression;
use Carp qw (cluck confess croak);

BEGIN {
    use_ok('Greenphyl::Log');
}

my $THREAD_COUNT = 8;
my $LOG_MESSAGE_PER_THREAD_COUNT = 16;

# if the parameter 'gen' is first on the command line, the regression files are
# not checked against output, but instead generated from output
if ($ARGV[0] && ($ARGV[0] eq 'gen'))
{
    $ENV{'TEST_REGRESSION_GEN'} = 1;
}
$ENV{'TESTING'} = 1;

# check auto-init
ok($Greenphyl::Log::g_log_dbh, 'Database opened');

# check database logging
my $test_message = 'test db message';
LogInfo($test_message);
ok(1 == @{$Greenphyl::Log::g_log_dbh->selectall_arrayref("SELECT time, script, pid, type, message FROM greenphyl_logs WHERE script = ? AND pid = ? AND type = ? AND message = ?;", undef, $0, $$, $Greenphyl::Log::LOG_TYPE_INFO, $test_message)}, 'Log messages can be stored in DB');
ok($Greenphyl::Log::g_log_dbh->do("DELETE FROM greenphyl_logs WHERE script = ? AND pid = ? AND type = ? AND message = ?;", undef, $0, $$, $Greenphyl::Log::LOG_TYPE_INFO, $test_message), 'Log messages can be manually removed from DB');


sub RunTestThread
{
    # since this procedure is run from threads, modules should be reloaded.
    use strict;
    use warnings;

    use lib '../lib';
    use lib '../local_lib';
    use Carp qw (cluck confess croak);

    my $test_message = 'test db message for thread ' . threads->tid() . ": ";
    for (1..int($LOG_MESSAGE_PER_THREAD_COUNT/2))
    {
        LogInfo($test_message . $_);
    }
    sleep(0.2+rand());
    for (1..int($LOG_MESSAGE_PER_THREAD_COUNT/2+0.5))
    {
        LogInfo($test_message . $_);
    }
}

my @threads_list;
for (my $slot_number = 0; $slot_number < $THREAD_COUNT; ++$slot_number)
{
    push(@threads_list, threads->create(\&RunTestThread));
}

foreach my $thread (@threads_list)
{
    $thread->join();
}
sleep(1);
# 1st line comes from "PROCESS START: ...", others are just thread lines
ok((1 + $LOG_MESSAGE_PER_THREAD_COUNT*$THREAD_COUNT) == @{$Greenphyl::Log::g_log_dbh->selectall_arrayref("SELECT time, script, pid, type, message FROM greenphyl_logs WHERE script = ? AND pid = ? AND type = ?;", undef, $0, $$, $Greenphyl::Log::LOG_TYPE_INFO)}, 'Log messages can be stored in DB');
ok($Greenphyl::Log::g_log_dbh->do("DELETE FROM greenphyl_logs WHERE script = ? AND pid = ?;", undef, $0, $$), 'Log messages have been removed from DB');

done_testing();
