#!/usr/bin/perl

use strict;
use warnings;

use lib '../lib';
use lib '../local_lib';


use Test::Most;
use Test::Regression;
use Carp qw (cluck confess croak);

BEGIN {
    use_ok('TreePattern');
}

# if the parameter 'gen' is first on the command line, the regression files are
# not checked against output, but instead generated from output
if ($ARGV[0] && ($ARGV[0] eq 'gen'))
{
    $ENV{'TEST_REGRESSION_GEN'} = 1;
}
$ENV{'TESTING'} = 1;

my $DATABASE = 'GreenPhyl';
my $PATTERN = 'Viridiplantae:-1[<R>Viridiplantae</R><S>1</S>];';
my $MATCH_COUNT = 9894;
my $MATCHING_FILE = '4990_rap_tree.xml';
my $ANNOTATED_NEWICK = '(((COLORED_Cre17.g711350.t1.1_CHLRE:5.2E-8,(COLORED_Cre01.g061400.t1.1_CHLRE:1.0E-10,(COLORED_Cre17.g698300.t1.1_CHLRE:1.0E-10,COLORED_Cre17.g738650.t1.1_CHLRE:1.323E-7)D_0.0:9.9E-8)D_0.0:5.2E-8)D_1.0:0.9443198643,((COLORED_Cre10.g453200.t1.1_CHLRE:1.0E-10,COLORED_Cre13.g599950.t1.1_CHLRE:1.0E-10)D_0.999:0.1034750272,(COLORED_Cre08.g379150.t1.1_CHLRE:0.0107210338,(COLORED_Cre16.g672700.t1.1_CHLRE:4.13E-8,(COLORED_Cre12.g490950.t1.1_CHLRE:1.0E-10,(COLORED_Cre17.g730900.t1.1_CHLRE:1.0E-10,(COLORED_Cre06.g255850.t1.1_CHLRE:1.0E-10,((COLORED_Cre09.g400900.t1.1_CHLRE:1.0E-10,COLORED_Cre11.g469550.t1.1_CHLRE:1.0E-10)D_0.0:6.69E-8,(COLORED_Cre17.g730850.t1.1_CHLRE:1.0E-10,(COLORED_Cre17.g697850.t1.1_CHLRE:1.0E-10,COLORED_Cre50.g789950.t1.1_CHLRE:0.0101523981)D_0.0:1.16E-7)D_0.0:9.16E-8)D_0.0:6.46E-8)D_0.0:7.58E-8)D_0.0:8.01E-8)D_0.0:4.13E-8)D_0.747:0.0023731993)D_0.0:5.009E-7)D_0.994:0.6852984076)D_1.0:0.08783334434999995,(COLORED_Selmo_415133_SELMO:9.278E-7,(COLORED_Selmo_415128_SELMO:0.008258749,COLORED_Selmo_427615_SELMO:0.1071852729)D_0.397:0.0073176677)D_1.0:0.9176505513500002)S_1.0:0.0;';

# SubmitTreePatternJob
my $job_id = SubmitTreePatternJob({'database' => $DATABASE, 'pattern' => $PATTERN});
my $id_ok = (defined($job_id) && ($job_id =~ m/^\w+$/));
ok($id_ok, 'Job identifier');

SKIP:
{
    skip 'No job ID!', 0 unless $id_ok;

    # CheckTreePatternJob
    my $status = CheckTreePatternJob($job_id);

    my $max_tries = 120;
    while ($max_tries && (!defined($status) || ('' eq $status)))
    {
        sleep(1);
        --$max_tries;
        $status = CheckTreePatternJob($job_id);
    }
    ok($max_tries, 'No timeout');
    ok(0 < $status, 'Job status');
    skip 'Job failed!', 0 unless (0 < $status);
    ok($MATCH_COUNT == $status, "Result count $MATCH_COUNT == $status");

    # GetTreePatternJobResults
    my $matches = GetTreePatternJobResults({'job' => $job_id, 'offset' => 0, 'length' => $status,});
    ok($matches && (@$matches == $status), 'Match count');
    my @match = grep {$_ eq $MATCHING_FILE} @$matches;
    ok(@match, 'Expected match');

    # AnnotateTreeWithPattern
    my $newick = AnnotateTreeWithPattern({'database' => $DATABASE, 'pattern' => $PATTERN, 'file' => $MATCHING_FILE});
    ok($newick eq $ANNOTATED_NEWICK, 'Tree pattern annotation');
}

done_testing();
