#!/usr/bin/perl

use strict;
use warnings;

use lib '../lib';
use lib '../local_lib';


use Test::Most;
# use Test::Regression;
use Carp qw (cluck confess croak);

BEGIN {
    use_ok( 'Greenphyl' );
    use_ok( 'Greenphyl::CompositeObject');
}
use Greenphyl::Sequence;

# Composite Object
my $sequence = Greenphyl::Sequence->new(
    GetDatabaseHandler(),
    {
        'selectors' => {'accession' => ['LIKE', 'Os01g01050%'],},
    },
);
my $composite = Greenphyl::CompositeObject->new(
    $sequence,
    {'test' => 42,},
    ['abc', 'def']
);

ok($composite, 'Composite object created');
COMPOSITE_INSTANCE:
{
    skip 'Composite object not instantiated, skipping tests.' unless $composite;
    # explain('Composite object instance: ', $composite);

    # array part
    ok(($composite->[1] eq 'def'), 'Get array value 1 from array member');
    ok((2 == @{$composite}), 'Array element count');
    my $composite2 = Greenphyl::CompositeObject->new(
        ['a', 'b', 'c'],
        ['g', 'h'],
        ['d', 'e', 'f'],
    );
    ok(($composite2->[1] eq 'b'), 'Array merge 1');
    ok(($composite2->[4] eq 'h'), 'Array merge 2');
    ok(($composite2->[5] eq 'd'), 'Array merge 3');
    my $composite2b = Greenphyl::CompositeObject->new(
        ['x', 'y', 'w'],
    );
    ok((($composite2b->[1] eq 'y') && ($composite2->[1] eq  'b')), 'Separate instances 1');
    
    # hash part
    ok(($composite->{'test'} == 42), 'Get test from hash member');
    ok(($composite->test == 42), 'Get test from hash member (through method)');
    ok((%$composite), 'Test hash member');
    my $composite_hash_name = 'hash test';
    my $composite3 = Greenphyl::CompositeObject->new(
        {'a' =>  1, 'b' =>   2, 'c' =>  3, 'd' => 13,},
        {           'b' => 806, 'c' =>  7,            'e' => 17,},
        {'a' => 42,             'c' => 33,                       'f' => 19,},
        {'composite_id' => $composite_hash_name,}
    );
    ok(( 42 == $composite3->{'a'}), 'Hash merge 1');
    ok((806 == $composite3->{'b'}), 'Hash merge 2');
    ok(( 33 == $composite3->{'c'}), 'Hash merge 3');
    ok(( 13 == $composite3->{'d'}), 'Hash merge 4');
    ok(( 17 == $composite3->{'e'}), 'Hash merge 5');
    ok(( 19 == $composite3->{'f'}), 'Hash merge 6');
    my $composite3b = Greenphyl::CompositeObject->new(
        {'a' =>  1, 'b' =>   2, 'c' =>  3, 'd' => 13,},
    );
    ok(((42 == $composite3->{'a'}) && (1 == $composite3b->{'a'})), 'Separate instances 2');
    

    # object part
    ok($composite->name, 'Get sequence name');
    ok(1 < @{$composite->families()}, 'Get families from sequence member');
    ok(1 == @{$composite->families({'sql' => {'LIMIT' => 1}})}, 'Get first family from sequence member using parameters');

    # name
    my $composite_array_name = 'array test';
    my $composite2c = Greenphyl::CompositeObject->new([$composite_array_name]);
    ok('' . $composite eq '' . $sequence, 'Composite auto-name from object');
    ok('' . $composite3 eq $composite_hash_name, 'Composite auto-name from hash');
    ok('' . $composite2c eq $composite_array_name, 'Composite auto-name from array');

    # priorities...
    # create a temporary test package
    package CompositeTest;

    sub new
    {
        my ($proto) = @_;
        my $class = ref($proto) || $proto;
        my $self = {};
        bless($self, $class);
    }

    sub answer
    {
        my ($self, $question) = @_;
        if (33 == $question)
        {
            return 2012;
        }
        else
        {
            return 42;
        }
    }
    # end of package CompositeTest
    package main;

    my $test = CompositeTest->new();
    my $hash = {'answer' => 806,};

    my $composite5 = Greenphyl::CompositeObject->new(
        $hash,
        $test,
    );
    ok((806 == $composite5->answer(33)), 'Priority to hash')
        or print "Expected '806' , got '" . $composite5->answer(33) . "'\n";

    my $composite6 = Greenphyl::CompositeObject->new(
        [], {'no_answer' => 1}, $test,
    );
    ok((2012 == $composite6->answer(33)), 'Nothing in hash, fall back to object')
        or print "Expected '2012' , got '" . $composite6->answer(33) . "'\n";

}

done_testing();
