#!/usr/bin/perl

use strict;
use warnings;
use FindBin;
use lib "$FindBin::Bin/../lib";
use lib "$FindBin::Bin/../local_lib";


use Test::Most;
# use Test::Regression;
use Carp qw (cluck confess croak);

BEGIN {
    use_ok( 'Greenphyl' );
}
my $sequence_content = ">Os01g01050.1
MINFAVQSRCAPTNSLCSCVALCERNFKLEQVEELASLIKDNLYSKHLVLSTEETLVGIL
QNQYHNSDDDEDEDDIVAAYRGTNRNILELQPASSYQRLLLHRLADIYGFVHESVGEGED
RHLVLQRCPETAIPPVLVSDVLWEYDNKDTSTSVVVKRKDTDLEEAWKEDAQENISAEIS
HLKNDADLKALQKSVAPPAPSLKEREAAYRAARERIFSAHDAKGNGTAVAKPRHVPAVAQ
RMIAHALGKKVESPTETAAVKNGKGKEPAESSRNKLNPRTAGGKEDSRYVENGRMRLHTG
NPCKQSWRTSNSRAASSVSPDELKREQVGAAKRMFVHALRLPGVEGSDGPVRKGK*";

my $DIAMOND_QUERY_URL = GetURL('diamond', { 'p' => 'results', 'sequence' => $sequence_content, 'database' => 'Monocotyledons', 'command' => 'blastp', 'evalue' => '1e-10', 'diamond_submit' => 'Launch Diamond',});
my $REFERENCE_DIAMOND_RESULT_FILENAME = 'data/ref_test.blast';
my $DIAMOND_RESULT_FILENAME = 'test.blast';


# if the parameter 'gen' is first on the command line, the regression files are
# not checked against output, but instead generated from output
if ($ARGV[0] && ($ARGV[0] eq 'gen'))
{
    $ENV{TEST_REGRESSION_GEN} = 1;
}

# check config
print "Config Check:\n-------------\n";
ok(defined($Greenphyl::Config::BLAST_BANKS_PATH),         'BLAST bank path defined');
ok($Greenphyl::Config::BLAST_BANKS_PATH,         'BLAST bank path set');
ok(defined($Greenphyl::Config::ALL_BLAST_BANK),           'ALL BLAST bank file path defined');
ok($Greenphyl::Config::ALL_BLAST_BANK,           'ALL BLAST bank file path set');
ok(defined($Greenphyl::Config::DIAMOND_COMMAND),         'DIAMOND command defined');
ok($Greenphyl::Config::DIAMOND_COMMAND,         'DIAMOND command set');

print <<"___34_CONFIG___";

Config content:
---------------
DIAMOND bank path:          $Greenphyl::Config::BLAST_BANKS_PATH
ALL DIAMOND bank file path: $Greenphyl::Config::ALL_BLAST_BANK
DIAMOND command:            $Greenphyl::Config::DIAMOND_COMMAND

___34_CONFIG___


print "DIAMOND Execution Check:\n----------------------\n";
# launch web DIAMOND
# options wget: --user=... --password=...
my $diamond_query_url = $DIAMOND_QUERY_URL;
$diamond_query_url =~ s#^//#https://#;
my @wget_parameters = ("--output-document=$DIAMOND_RESULT_FILENAME", "$diamond_query_url");

# dev http password protected?
if ($DIAMOND_QUERY_URL =~ m~greenphyl-dev\.cirad\.fr~)
{
    push(@wget_parameters, "--user=dev", "--password=Pa5dGr3En");
}

if (system('wget', @wget_parameters))
{
    confess "ERROR: Failed to execute command:\n    wget " . join(' ', @wget_parameters) . "\n\n$!\n";
}

my $diamond_result_fh;
open($diamond_result_fh, $DIAMOND_RESULT_FILENAME) or confess "Failed to open DIAMOND result file!\n$!\n";

#ok_regression(
#    sub
#    {
#        my $blast_data = join('', <$blast_result_fh>);
#        $blast_data =~ s/.*<!-- END global_header\.tt -->//s;
#        $blast_data =~ s/<!-- BEGIN global_footer\.tt -->.*//s;
#        return $blast_data;
#    },
#    $REFERENCE_BLAST_RESULT_FILENAME,
#    'BLAST result content ok');

my $diamond_data = join('', <$diamond_result_fh>);
$diamond_data =~ s/.*<!-- END global_header\.tt -->//s;
$diamond_data =~ s/<!-- BEGIN global_footer\.tt -->.*//s;

print <<"___78_HTML_RESULT___";

HTML Output:
--------------------------------------------------------------------------------
$diamond_data

--------------------------------------------------------------------------------
___78_HTML_RESULT___

ok($diamond_data =~ m/BLASTP\s+\d+\.\d+\.\d+/, "Diamond result contains BLASTP version");
ok($diamond_data =~ m/Score\s*=\s*\d+(?:\.\d*)?\s*bits/, "Diamond result contains Score");
ok($diamond_data =~ m/Expect\s*=\s*\d/, "Diamond result contains Expect");
ok($diamond_data =~ m/Identities\s*=\s*\d+\/\d+/, "Diamond result contains Identities");
ok($diamond_data =~ m/Positives\s*=\s*\d+\/\d+/, "Diamond result contains Positives");
ok($diamond_data =~ m/Query\s+\d+/, "Diamond result contains Query");
ok($diamond_data =~ m/Sbjct\s+\d+/, "Diamond result contains Sbjct");

close($diamond_result_fh);

# remove test file
unlink($DIAMOND_RESULT_FILENAME);

=pod

#+TODO
tester la partie web:
-cas d'une sequence sans match
-cas d'une sequence avec match sur une sequence hors base
-cas d'une sequence avec match sur une sequence sans famille
-cas d'une sequence avec match sur une sequence normale

=cut

done_testing();
