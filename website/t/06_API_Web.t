#!/usr/bin/perl

use strict;
use warnings;

use lib '../lib';
use lib '../local_lib';


use Test::Most;
use Test::Regression;
use Carp qw (cluck confess croak);

BEGIN {
    # force CGI mode
    $ENV{'GATEWAY_INTERFACE'} = 'test';
    use_ok( 'Greenphyl' );
    use_ok( 'Greenphyl::Web' );
}

# if the parameter 'gen' is first on the command line, the regression files are
# not checked against output, but instead generated from output
if ($ARGV[0] && ($ARGV[0] eq 'gen'))
{
    $ENV{TEST_REGRESSION_GEN} = 1;
}

my $TEST_REFERENCE_FILES_PATH =  GP('TEST_PATH') . '/web';
if (!-e $TEST_REFERENCE_FILES_PATH)
{
    mkdir($TEST_REFERENCE_FILES_PATH) or confess "ERROR: unable to create missing regression directory '$TEST_REFERENCE_FILES_PATH'!\n$@\n";
}
elsif (!-w $TEST_REFERENCE_FILES_PATH)
{
    cluck "WARNING: regression directory '$TEST_REFERENCE_FILES_PATH' is not writeable!\n";
}
my $TEST_REFERENCE_BASE_FILENAME = $TEST_REFERENCE_FILES_PATH . '/test_';


#+FIXME: to test:
# -GetCGI
# -GetSession
# -IsAuthenticated
# -IsAdmin
# -GetAlignmentURL
# -GetPhylogenyAnalyzesURL

# remove variables parts from HTML code that shouldn't be tested
sub CleanHTML
{
    my $html_content = shift();
    $html_content =~ s/(Set-Cookie: ).+/$1/i;
    $html_content =~ s/(Expires: )[^\n]+/$1/;
    $html_content =~ s/(Date: )[^\n]+/$1/;
    $html_content =~ s~(<title>GreenPhyl[^<]*)( - Debug Mode)?(</title>)~$1$3~;
    $html_content =~ s~http://[^/]*greenphyl[^/]*/(?:dev[^/]*/)?~http://www.greenphyl.fr/~i;
    return $html_content;
}

ok_regression(sub { return join(', ', map { $_->{'name'} } @{GenerateCSSList()})}, $TEST_REFERENCE_BASE_FILENAME . 'css_list', 'Default CSS list');
ok_regression(sub { return join(', ', map { $_->{'name'} } @{GenerateLESSList()})}, $TEST_REFERENCE_BASE_FILENAME . 'less_list', 'Default LESS list');
ok_regression(sub { return join(', ', map { $_->{'name'} } @{GenerateJavascriptList()})}, $TEST_REFERENCE_BASE_FILENAME . 'javascript_list', 'Default Javascript list');
ok_regression(
    sub {return CleanHTML(RenderHTMLMinimalHeader());},
    $TEST_REFERENCE_BASE_FILENAME . 'html_minimal_header',
    'Default HTML minimal header',
);
ok_regression(
    sub {return CleanHTML(RenderHTMLStandardHeader());},
    $TEST_REFERENCE_BASE_FILENAME . 'html_standard_header',
    'Default HTML standard header',
);
ok_regression(
    sub
    {
        return CleanHTML(
            RenderHTMLStandardHeader(
                {
                    'mime'                => 'HTML',
                    'header_args'         => ['text/html'],
                    'css'                 => ['custom.autocomplete'],
                    'remove_css'          => ['jquery.autocomplete'],
                    'less'                => ['greenphyl3'],
                    'remove_less'         => ['greenphyl'],
                    'javascript'          => ['ipr?no_autoload=1'],
                    'remove_javascript'   => ['swfobject'],
                    'title'               => 'Auto-Test Page',
                },
            )
        );
    },
    $TEST_REFERENCE_BASE_FILENAME . 'html_standard_header_with_parameters',
    'Default HTML standard header with parameters',
);
ok_regression(sub { return RenderHTMLMinimalFooter(); }, $TEST_REFERENCE_BASE_FILENAME . 'html_minimal_footer', 'Default HTML minimal footer');
ok_regression(sub { return RenderHTMLStandardFooter(); }, $TEST_REFERENCE_BASE_FILENAME . 'html_standard_footer', 'Default HTML standard footer');
ok(!CheckHTMLIdentifierOk(''), 'missing HTML identifier');
ok(CheckHTMLIdentifierOk('abc')
   && CheckHTMLIdentifierOk('ABC')
   && CheckHTMLIdentifierOk('A_123-bc'), 'valid HTML identifier');
ok(!CheckHTMLIdentifierOk('1bc')
   && !CheckHTMLIdentifierOk('_bc')
   && !CheckHTMLIdentifierOk('-bc'), 'invalid HTML identifier');
ok_regression(sub { return RenderMessage({'message' => 'Regression test message.'}) }, $TEST_REFERENCE_BASE_FILENAME . 'message', 'Default message');
ok_regression(sub { return RenderMessage({'message' => 'Regression test error message.', 'tpye' => 'error'}) }, $TEST_REFERENCE_BASE_FILENAME . 'message_error', 'Error message');
ok_regression(sub { return RenderMessage({'message' => 'Regression test warning message with details.', 'tpye' => 'error', 'with_details' => 1}) }, $TEST_REFERENCE_BASE_FILENAME . 'message_warning_details', 'Warning message with details');

ok_regression(
    sub
    {
        return CleanHTML(
            RenderHTMLFullPage(
                {
                    'mime'                => 'HTML',
                    'header_args'         => ['text/html'],
                    'css'                 => ['custom.autocomplete'],
                    'remove_css'          => ['jquery.autocomplete'],
                    'less'                => ['greenphyl3'],
                    'remove_less'         => ['greenphyl'],
                    'javascript'          => ['ipr?no_autoload=1'],
                    'remove_javascript'   => ['swfobject'],
                    'title'               => 'Hello World',
                    'before_content'      => '<b>Before content</b>',
                    'dump_data'           =>
                        {
                           'links' => [
                             {
                               'label'       => 'FASTA',
                               'format'      => 'fasta',
                               'object_type' => 'sequences',
                               'file_name'   => 'sequences.fasta',
                               'fields'      => 'seq_id',
                             },
                             {
                               'label'       => 'Excel',
                               'format'      => 'excel',
                               'object_type' => 'sequences',
                               'file_name'   => 'sequences.xls',
                               'fields'      => 'seq_id',
                             },
                             {
                               'label'       => 'CVS',
                               'format'      => 'cvs',
                               'object_type' => 'sequences',
                               'file_name'   => 'sequences.cvs',
                               'fields'      => 'seq_id',
                             },
                           ],
                           'mode' => 'form',
                           'parameters' => { 'seq_id' => 'At1g14920.1', },
                         },
                    'before_form_content' => '<p>Before form content</p>',
                    'content'             => 'message.tt',
                    'form_data'          =>
                        {
                            'identifier' => 'my_form',
                            'action'     => 'submitter.cgi',
                            'method'     => 'POST',
                            'enctype'    => 'application/x-www-form-urlencoded',
                            'submit'     => 'Go go go!',
                            'nosubmit'   => 0,
                            'noclear'    => 1,
                        },
                    'after_form_content'  => '<p>After form content</p>',
                    'after_content'       => '<b>After content</b>',
                    'message_type'        => 'info',
                    'message'             => "Beautiful day today. Mary is a girl and John is a boy.",
                },
            ),
        );
    },
    $TEST_REFERENCE_BASE_FILENAME . 'html_full_page',
    'Full HTML page with parameters',
);

# test error handler
ok_regression(
    sub
    {
        $| = 1;
        my ($stdout_sh, $stdout_stream) = (undef, '');
        open($stdout_sh, '>', \$stdout_stream);
        my $old_stdout = select($stdout_sh);
        # exception handler
        eval
        {
            Throw('error' => 'GreenPhyl Error Test', 'log' => 'With log message');
        };
        HandleErrors();
        select($old_stdout);
        return CleanHTML($stdout_stream);
    },
    $TEST_REFERENCE_BASE_FILENAME . 'error_handler', 'Error Handler'
);

done_testing();
