#!/usr/bin/perl

use strict;
use warnings;

use lib '../lib';
use lib '../local_lib';


use Test::Most;
# use Test::Regression;
use Carp qw (cluck confess croak);

BEGIN {
    use_ok( 'Greenphyl' );
    use_ok( 'Greenphyl::GO' );
}

my $go = Greenphyl::GO->new(
    GetDatabaseHandler(),
    {
        'selectors' => {'code' => 'GO:0008773'},
    },
);

ok($go, 'GO object creation');
# explain('Family instance: ', $family);
GO_INSTANCE:
{
    skip 'GO not instantiated, skipping tests.' unless $go;
    ok('GO:0008773' eq "$go", 'GO in scalar context');
    ok('[protein-PII] uridylyltransferase activity' eq $go->name, 'GO name');
    # ok(0 == $go->countFamilies({'selectors' => {'families.level' => 1}, }), 'GO related families (level 1)');
    my %root_gos = map {$_->name => $_} (Greenphyl::GO->new(
        GetDatabaseHandler(),
        {
            'selectors' => {'EXISTS' => 'SELECT TRUE FROM go g2 WHERE go.name = g2.namespace LIMIT 1'},
        },
    ));
    # GO:0003674    molecular_function
    # GO:0008150    biological_process
    # GO:0005575    cellular_component
    ok(3 == scalar(keys(%root_gos)),'root GO terms found');
    ok(exists($root_gos{'molecular_function'}),'molecular_function found');
    ok(exists($root_gos{'biological_process'}),'biological_process found');
    ok(exists($root_gos{'cellular_component'}),'cellular_component found');

}

done_testing();
