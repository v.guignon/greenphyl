#!/usr/bin/perl

use strict;
use warnings;

use lib '../lib';
use lib '../local_lib';


use Test::Most;
# use Test::Regression;
use Carp qw (cluck confess croak);

BEGIN {
    use_ok('Greenphyl');
    use_ok('Greenphyl::CachedDBObject');
}

my @tests = (
    ['abc', 'abc', 1],        # simple
    ['abc', 'ABC', 1],        # case insensitive
    ['abc', 'AbC', 1],        # case insensitive
    ['abc', 'ab', 0],         # simple
    ['abc', 'bc', 0],         # simple
    ['abc', 'a b c', 0],      # simple
    ['abc', 'abcdef', 0],     # simple
    ['abc', 'xyzabc', 0],     # simple
    ['abc', 'xyzabcdef', 0],  # simple

    ['_abc', 'XABC', 1],       # with single jocker character before
    ['_abc', 'ABC', 0],        # with single jocker character before
    ['_abc', 'XYABC', 0],      # with single jocker character before

    ['a_c', 'ABC', 1],        # with single jocker character inside
    ['a_c', 'AC', 0],         # with single jocker character inside
    ['a_c', 'ABXC', 0],       # with single jocker character inside

    ['abc_', 'ABCD', 1],       # with single jocker character after
    ['abc_', 'ABC', 0],        # with single jocker character after
    ['abc_', 'ABCDE', 0],      # with single jocker character after

    ['%abc', 'ABC', 1],        # starting wildcard 
    ['%abc', 'XABC', 1],       # starting wildcard 
    ['%abc', 'XYZABC', 1],     # starting wildcard 
    ['%abc', 'BC', 0],         # starting wildcard 
    ['%abc', 'ABCD', 0],       # starting wildcard 

    ['abc%', 'ABC', 1],        # ending wildcard 
    ['abc%', 'ABCD', 1],       # ending wildcard 
    ['abc%', 'ABCDEF', 1],     # ending wildcard 
    ['abc%', 'AB', 0],         # ending wildcard 
    ['abc%', 'XABC', 0],       # ending wildcard 

    ['%abc%', 'ABC', 1],       # starting and ending wildcard 
    ['%abc%', 'XYZABC', 1],    # starting and ending wildcard 
    ['%abc%', 'ABCDEF', 1],    # starting and ending wildcard 
    ['%abc%', 'XABCD', 1],     # starting and ending wildcard 
    ['%abc%', 'XYZABCDEF', 1], # starting and ending wildcard 
    ['%abc%', 'XYZBCDEF', 0],  # starting and ending wildcard 
    ['%abc%', 'XYZABDEF', 0],  # starting and ending wildcard 
    ['%abc%', 'AXBCD', 0],     # starting and ending wildcard 
    ['%abc%', 'ABDCE', 0],     # starting and ending wildcard 

    ['%a.*c%', 'A.*C', 1],     # special regexp chars
    ['%a.*c%', 'ABC', 0],      # special regexp chars
    ['%a\.*c%', 'A.*C', 1],    # special regexp chars
    ['%a\.*c%', 'ABC', 0],     # special regexp chars

    ['a_+c', 'AB+C', 1],       # special regexp chars
    ['a_+c', 'ABDC', 0],       # special regexp chars
    ['a_\+c', 'AB+C', 1],      # special regexp chars
    ['a_\+c', 'ABDC', 0],      # special regexp chars
    
    ['a"c', 'A"C', 1],         # special LIKE chars
    ['a""c', 'A"C', 1],        # special LIKE chars
    ['a\"c', 'A"C', 1],        # special LIKE chars

);

while (@tests)
{
    my ($like_exp, $string, $result) = @{shift(@tests)};
    ok($result == Greenphyl::CachedDBObject::_MatchUsingMySQLLike($string, $like_exp), "$like_exp " . ($result ? 'matches' : 'does not match') . " $string");
}


done_testing();
