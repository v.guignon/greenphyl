#!/usr/bin/perl

use strict;
use warnings;

use lib '../lib';
use lib '../local_lib';


use Test::Most;
use Test::Regression;
use Carp qw (cluck confess croak);

my $REFERENCE_LOG_FILENAME = 'logs/ref_test' . $Greenphyl::Log::LOG_FILE_EXTENSION;
my $TEST_LOG_NAME; # set in BEGIN part
my $log_file_path;

BEGIN
{
    $TEST_LOG_NAME = 'test';
    use_ok('Greenphyl');
    use_ok('Greenphyl::Config');
    use_ok('Greenphyl::Log', $TEST_LOG_NAME);
    # -generate filename timestamp
    my ($second, $minute, $hour, $day_of_month, $month, $year_offset, $day_of_week, $day_of_year, $daylight_savings) = localtime();
    ++$month;
    my $year = 1900 + $year_offset;
    $log_file_path = sprintf("%s_%04i%02i%02i%02i%02i%02i%s", $TEST_LOG_NAME, $year, $month, $day_of_month, $hour, $minute, $second, $Greenphyl::Log::LOG_FILE_EXTENSION);
}

++$|; # autoflush

# if the parameter 'gen' is first on the command line, the regression files are
# not checked against output, but instead generated from output
if ($ARGV[0] && ($ARGV[0] eq 'gen'))
{
    $ENV{'TEST_REGRESSION_GEN'} = 1;
}
$ENV{'TESTING'} = 1;

# check config
ok(defined($Greenphyl::Config::LOG_PATH), 'Log directory defined');
ok((-w $Greenphyl::Config::LOG_PATH), 'Log directory writeable');
ok(defined($Greenphyl::Log::LOG_FILE_NAME_DEFAULT), 'Default log file name defined');
ok(defined($Greenphyl::Log::LOG_FILE_EXTENSION), 'Log file extension defined');

my $config_ok = (
                    defined($Greenphyl::Config::LOG_PATH)
                    && (-w $Greenphyl::Config::LOG_PATH)
                    && defined($Greenphyl::Log::LOG_FILE_NAME_DEFAULT)
                    && defined($Greenphyl::Log::LOG_FILE_EXTENSION)
                );

CONFIG_OK:
{
  skip 'Invalid log config!' unless $config_ok;

  $log_file_path = GP('LOG_PATH') . '/' . $log_file_path;
  # check file creation
  ok($Greenphyl::Log::g_log_fh, 'Log file opened');
  ok(-e $log_file_path, 'log file has expected name');
  
  # test loggers
  LogInfo('test message 1');
  LogDebug('test message 2');
  LogWarning('test message 3');
  LogError('test message 4');
  LogInfo("test message 5\non\r\nseveral\rlines.\n");
  LogWarning("test another message 6\nalso on\r\nseveral nice\rshort lines!\n");
  
  # ends (closes) log
  Greenphyl::Log::_ResetConfig();
  
  my $log_fh;
  open($log_fh, $log_file_path) or confess "Failed to open generated log ($log_file_path)!\n$!\n";
  ok_regression(sub { my $log_data = join('', <$log_fh>); $log_data =~ s/^GREENPHYL LOG[^\n]+/GREENPHYL LOG/m; return $log_data;}, $REFERENCE_LOG_FILENAME, 'Log file content ok');
  close($log_fh);
  
  # remove test file
  unlink($log_file_path);
}

done_testing();
