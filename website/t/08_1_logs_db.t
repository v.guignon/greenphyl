#!/usr/bin/perl

use strict;
use warnings;

use lib '../lib';
use lib '../local_lib';


use Test::Most;
use Test::Regression;
use Carp qw (cluck confess croak);

BEGIN {
    use_ok('Greenphyl::Log');
}

# if the parameter 'gen' is first on the command line, the regression files are
# not checked against output, but instead generated from output
if ($ARGV[0] && ($ARGV[0] eq 'gen'))
{
    $ENV{'TEST_REGRESSION_GEN'} = 1;
}
$ENV{'TESTING'} = 1;

# check auto-init
ok($Greenphyl::Log::g_log_dbh, 'Database opened');

# check if log table is available
my $log_table_test = $Greenphyl::Log::g_log_dbh->selectrow_arrayref("SELECT time, script, pid, type, message FROM greenphyl_logs LIMIT 1;");
ok(!$Greenphyl::Log::g_log_dbh->err, 'Log table exists and appears to be good');

# check database logging
my $test_message = 'test db message';
LogInfo($test_message);
ok(1 == @{$Greenphyl::Log::g_log_dbh->selectall_arrayref("SELECT time, script, pid, type, message FROM greenphyl_logs WHERE script = ? AND pid = ? AND type = ? AND message = ?;", undef, $0, $$, $Greenphyl::Log::LOG_TYPE_INFO, $test_message)}, 'Log messages can be stored in DB');
ok($Greenphyl::Log::g_log_dbh->do("DELETE FROM greenphyl_logs WHERE script = ? AND pid = ? AND type = ? AND message = ?;", undef, $0, $$, $Greenphyl::Log::LOG_TYPE_INFO, $test_message), 'Log messages can be manually removed from DB');

done_testing();
