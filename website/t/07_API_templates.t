#!/usr/bin/perl

use strict;
use warnings;

use lib '../lib';
use lib '../local_lib';


use Test::Most;
use Test::Regression;
use Carp qw (cluck confess croak);

BEGIN {
    use_ok( 'Greenphyl' );
    use_ok( 'Greenphyl::Web' );
    use_ok( 'Greenphyl::Web::Templates' );
}

# if the parameter 'gen' is first on the command line, the regression files are
# not checked against output, but instead generated from output
if ($ARGV[0] && ($ARGV[0] eq 'gen'))
{
    $ENV{TEST_REGRESSION_GEN} = 1;
}

my $TEST_REFERENCE_FILES_PATH =  GP('TEST_PATH') . '/templates';
if (!-e $TEST_REFERENCE_FILES_PATH)
{
    mkdir($TEST_REFERENCE_FILES_PATH) or confess "ERROR: unable to create missing regression directory '$TEST_REFERENCE_FILES_PATH'!\n$@\n";
}
elsif (!-w $TEST_REFERENCE_FILES_PATH)
{
    cluck "WARNING: regression directory '$TEST_REFERENCE_FILES_PATH' is not writeable!\n";
}
my $TEST_REFERENCE_BASE_FILENAME = $TEST_REFERENCE_FILES_PATH . '/test_';

# remove variables parts from HTML code that shouldn't be tested
sub CleanHTML
{
    my $html_content = shift();
    $html_content =~ s~(<title>GreenPhyl[^<]*)( - Debug Mode)?(</title>)~$1$3~;
    $html_content =~ s~http://[^/]*greenphyl[^/]*/(?:dev[^/]*/)?~http://www.greenphyl.fr/~i;
    return $html_content;
}

#------------
# message.tt
#------------
my @messages =
    (
        {
            'message' => 'This is a test message.',
            'type'    => 'debug',
            'details' => 0,
            'file_name' => 'debug',
        },
        {
            'message' => 'This is a test message with details.',
            'type'    => 'debug',
            'details' => 1,
            'file_name' => 'debug_2',
        },
        {
            'message' => 'This is another test message.',
            'type'    => 'DEBUG',
            'details' => 0,
            'file_name' => 'debug_3',
        },
        {
            'message' => 'This is yet another test message.',
            'type'    => 'info',
            'details' => 0,
            'file_name' => 'info',
        },
        {
            'message' => 'Is this a question?',
            'type'    => 'question',
            'details' => 0,
            'file_name' => 'question',
        },
        {
            'message' => 'This is a warning message.',
            'type'    => 'warning',
            'details' => 0,
            'file_name' => 'warning',
        },
        {
            'message' => 'This is an error message.',
            'type'    => 'error',
            'details' => 0,
            'file_name' => 'error',
        },
        {
            'message' => 'This is not a message.',
            'type'    => 'bad_type',
            'details' => 0,
            'file_name' => 'bad_type',
        },
    );

foreach my $message (@messages)
{
    ok_regression(sub
        {
            return ProcessTemplate(
               'message.tt',
               {
                   'message'      => $message->{'message'},
                   'message_type' => $message->{'type'},
                   'details'      => $message->{'details'},
               });
        },
        $TEST_REFERENCE_BASE_FILENAME . 'message_' . $message->{'file_name'} . '.html', 'message.tt - ' . $message->{'type'} . ($message->{'details'}?' + details':'') . ' (' . $message->{'file_name'} . ')');
}


#---------------
# dump_links.tt
#---------------

my @dump_links_configs =
(
    {
        'config_name'         => 'dump links test 1a - link',
        'file_name'           => 'dump_links_test1',
        'dump_data'           =>
        {
            'links' => [
                {
                    'label'       => 'FASTA',
                    'format'      => 'fasta',
                    'file_name'   => 'sequences.fasta',
                },
            ],
            'object_type' => 'sequences',
            'fields'      => 'seq_textid',
            'values' => { 'seq_textid' => ['At1g14920.1', 'Glyma10g37480.1', 'blabla'] },
            'mode' => 'link',
        },
    },
    {
        'config_name'         => 'dump links test 1b - link',
        'file_name'           => 'dump_links_test1',
        'dump_data'           =>
        {
            'links' => [
                {
                    'label'       => 'FASTA',
                    'format'      => 'fasta',
                    'file_name'   => 'sequences.fasta',
                    'object_type' => 'sequences',
                    'fields'      => 'seq_textid',
                    'values' => { 'seq_textid' => ['At1g14920.1', 'Glyma10g37480.1', 'blabla'] },
                },
            ],
            'mode' => 'link',
        },
    },
    {
        'config_name'         => 'dump links test 2a - form',
        'file_name'           => 'dump_links_test2a',
        'dump_data'           =>
        {
            'links' => [
                {
                    'label'       => 'FASTA',
                    'format'      => 'fasta',
                    'file_name'   => 'sequences.fasta',
                },
            ],
            'object_type' => 'sequences',
            'fields'      => 'seq_textid',
            'values' => { 'seq_textid' => ['At1g14920.1', 'Glyma10g37480.1', 'blabla'] },
            'mode' => 'form',
        },
    },
    {
        'config_name'         => 'dump links test 2b - form',
        'file_name'           => 'dump_links_test2b',
        'dump_data'           =>
        {
            'links' => [
                {
                    'label'       => 'FASTA',
                    'format'      => 'fasta',
                    'file_name'   => 'sequences.fasta',
                    'object_type' => 'sequences',
                    'fields'      => 'seq_textid',
                    'values' => { 'seq_textid' => ['At1g14920.1', 'Glyma10g37480.1', 'blabla'] },
                },
            ],
            'mode' => 'form',
        },
    },
    {
        'config_name'         => 'dump links test 3 - embeded',
        'file_name'           => 'dump_links_test3',
        'dump_data'           =>
        {
            'links' => [
                {
                    'label'       => 'FASTA',
                    'format'      => 'fasta',
                    'file_name'   => 'sequences.fasta',
                    'object_type' => 'sequences',
                    'fields'      => 'seq_textid',
                    'values' => { 'seq_textid' => ['At1g14920.1', 'Glyma10g37480.1', 'blabla'] },
                },
            ],
            'mode' => 'embeded',
        },
    },
);

foreach my $dump_data (@dump_links_configs)
{
    ok_regression(sub
        {
            return CleanHTML(
                ProcessTemplate(
                   'dump_links.tt',
                   {
                       'dump_data'      => $dump_data->{'dump_data'},
                   },
                )
            );
        },
        $TEST_REFERENCE_BASE_FILENAME . $dump_data->{'file_name'} . '.html', 'dump_links.tt - ' . $dump_data->{'config_name'},
    );
}

done_testing();
