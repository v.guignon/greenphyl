#!/usr/bin/perl
use strict;
use warnings;
use FindBin;
use lib "$FindBin::Bin/../lib";
use lib "$FindBin::Bin/../local_lib";

use threads;
use threads::shared;

use Carp qw(cluck confess croak);

use Greenphyl;

# parameters "-re" can be used as 1st parameter in order to only report errors

my $report_error_only = 0;
if (@ARGV && ('-re' eq $ARGV[0]))
{
    print "Note: Only report errors\n";
    $report_error_only = 1;
}

my $sendmail = "/usr/sbin/sendmail -t";
my $reply_to = "Reply-to: v.guignon\@cgiar.org\n";
my $subject = "Subject: [GreenPhyl] BLAST test\n";
my $send_to = "To: m.rouard\@cgiar.org, v.guignon\@cgiar.org;\n";
my $content = "GreenPhyl BLAST auto-check\n--------------------------\n\n";

my $blast_sh;
open($blast_sh, "-|")
    or (exec('/usr/bin/perl ' . GP('TEST_PATH') . '/03_blast.t 2>&1') and print "ERROR: execution failed!\n$!\n");
my $blast_check_output = join('', <$blast_sh>);

if ($blast_check_output =~ m/failed|error|warning/i)
{
    # an error occured
    $subject = "Subject: [GreenPhyl] ERROR: BLAST test FAILED\n";
    $content .= "WARNING: BLAST execution failed!\n\n";
    print "WARNING: BLAST execution failed!\n\n";
}
else
{
    # everything seems ok
    if ($report_error_only)
    {
        # only report errors? stop here.
        exit(0);
    }
    $subject = "Subject: [GreenPhyl] BLAST test OK\n";
    $content .= "Note: BLAST execution ok.\n\n";
    print "Note: BLAST execution ok.\n\n";
}
$content .= "$blast_check_output\n\n";
print "$blast_check_output\n";

open(SENDMAIL, "|$sendmail") or confess "Cannot open $sendmail: $!\n";
print SENDMAIL $reply_to;
print SENDMAIL $subject;
print SENDMAIL $send_to;
print SENDMAIL "Content-type: text/plain\n\n";
print SENDMAIL $content;
close(SENDMAIL);

exit(0);
