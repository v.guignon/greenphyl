#!/usr/bin/perl

use strict;
use warnings;

use lib '../lib';
use lib '../local_lib';


use Test::Most;
use Test::Regression;
use Carp qw (cluck confess croak);

use Greenphyl;
use Greenphyl::AnalysisProcess;

BEGIN {
    use_ok("Greenphyl::Tools::AnalysisProcesses", 'required');
}

$ENV{'TESTING'} = 1;

my $process_id = $ENV{'TEST_ANALYSIS_PROCESS_ID'};
my $process    = GetCurrentProcess();

if (!$process_id)
{
    confess "ERROR: no process ID provided! Either use the parameters process_id=<ID> or the environment variable 'export TEST_ANALYSIS_PROCESS_ID=<ID>'\n";
}

# check argument transfer
ok(GetProcessParameter() eq "process_id=$process_id process_mode=r", 'Command line argument transfer');

#+FIXME: check lib not loaded if process ID missing
#+FIXME: families in list: OK
#+FIXME: families not in list:
# -make sure the execution is stopped

done_testing();
