#!/usr/bin/perl

use strict;
use warnings;

use lib '../lib';
use lib '../local_lib';


use Test::Most;
use Test::Regression;
use Carp qw (cluck confess croak);

use Greenphyl;
use Greenphyl::AnalysisProcess;
use Greenphyl::Family;

BEGIN
{
    use_ok("Greenphyl::Tools::AnalysisProcesses");
}

$ENV{'TESTING'} = 1;

my $process_id = $ENV{'TEST_ANALYSIS_PROCESS_ID'};
my $process    = GetCurrentProcess();

if (!$process_id)
{
    confess "ERROR: no process ID provided! Either use the parameters process_id=<ID> or the environment variable 'export TEST_ANALYSIS_PROCESS_ID=<ID>'\n";
}

# check argument transfer
ok(GetProcessParameter() eq "process_id=$process_id process_mode=w", 'Command line argument transfer');

my $initial_duration = $process->duration;
AddProcessDuration(42);
$process->ClearCache();
ok($process->duration == $initial_duration + 42, 'Duration update');

my $family_1 = Greenphyl::Family->new(
    GetDatabaseHandler(),
    {
        'selectors' => {'id' => 1},
    },
);
my $family_2 = Greenphyl::Family->new(
    GetDatabaseHandler(),
    {
        'selectors' => {'id' => 2},
    },
);
# add family 3 to the process
my $families = [Greenphyl::Family->new(
    GetDatabaseHandler(),
    {
        'selectors' => {'id' => ['IN', 3, 4]},
    },
)];
if (!$family_2 || (2 != scalar(@$families)))
{
    confess "ERROR: Failed to load families for testing.\n";
}
AddFamiliesToProcess([$family_2]);
AddFamiliesToProcess($families);
# nb.: family 2 is supposed to already be there before we added 3 and 4
ok(3 == scalar(@{$process->families}), 'Add families');

# default status
my $status = GetFamilyStatusForProcess($family_2);
ok(defined($status) && ($Greenphyl::Tools::AnalysisProcesses::FAMILY_STATUS_PENDING eq $status), 'Default status');


# status change
SetFamilyStatusForProcess($family_2, $Greenphyl::Tools::AnalysisProcesses::FAMILY_STATUS_IN_PROGRESS);
$status = GetFamilyStatusForProcess($family_2);
ok(defined($status) && ('in progress' eq $status), 'Status change');
SetFamilyStatusForProcess($family_2, $Greenphyl::Tools::AnalysisProcesses::FAMILY_STATUS_PENDING);

# auto-add family
SetFamilyStatusForProcess($family_1, $Greenphyl::Tools::AnalysisProcesses::FAMILY_STATUS_PENDING);
$status = GetFamilyStatusForProcess($family_1);
ok(defined($status) && ('pending' eq $status), 'Status change on not-listed family');

ok(4 == scalar(@{$process->families}), 'Auto-added family');

# remove added family
my $sql_query = "
    DELETE
    FROM family_processing
    WHERE family_id IN (1, 3, 4)
        AND process_id = " . $process->id . "
";
GetDatabaseHandler()->do($sql_query);


done_testing();
