#!/usr/bin/perl

use strict;
use warnings;

use lib '../lib';
use lib '../local_lib';


use Test::Most;
use Test::Regression;
use Carp qw (cluck confess croak);

BEGIN {
    use_ok('Greenphyl::Log');
}

my $REFERENCE_LAST_LOG_FILENAME = 'logs/ref_test_last' . $Greenphyl::Log::LOG_FILE_EXTENSION;

# if the parameter 'gen' is first on the command line, the regression files are
# not checked against output, but instead generated from output
if ($ARGV[0] && ($ARGV[0] eq 'gen'))
{
    $ENV{'TEST_REGRESSION_GEN'} = 1;
}
$ENV{'TESTING'} = 1;

my $test_message = 'Yet another test db message';
my $test_debug_message = 'A test debug db message';
my $test_warning_message = 'A test warning';
my $test_error_message = 'A test ERROR';

LogInfo($test_message);
my $last_id = GetLastLogID();

LogDebug($test_debug_message);
ok(GetLastLogID() == $last_id+1, 'Log entry identifier - test 1');

LogWarning($test_warning_message);
ok(GetLastLogID() == $last_id+2, 'Log entry identifier - test 2');

LogError($test_error_message);
ok(GetLastLogID() == $last_id+3, 'Log entry identifier - test 3');

my $last_pid = GetLastLogPID();
my $good_pid = ($last_pid == $$);
ok($good_pid, 'Last log PID is current PID');

ok_regression(sub
    {
        my $log_message = GetLastLogs();
        $log_message =~ s/PROCESS BEGIN: 08_3_logs_misc\.t.*/PROCESS BEGIN: 08_3_logs_misc\.t/;
        return $log_message;
    },
    $REFERENCE_LAST_LOG_FILENAME,
    'Last DB log content ok'
);

CLEAR_LOGS:
{
    skip 'Unable to clear logs as PID might not be the good one!' unless $good_pid;
    ok(5 == RemoveLogs('pid' => $$), 'Removed test log messages');
}

done_testing();
