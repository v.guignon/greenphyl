#!/usr/bin/perl

use strict;
use warnings;

use lib '../lib';
use lib '../local_lib';


use Test::Most;
# use Test::Regression;
use Carp qw (cluck confess croak);

BEGIN {
    use_ok( 'Greenphyl' );
    use_ok( 'Greenphyl::IPR' );
}

# ipr
my $ipr = Greenphyl::IPR->new(
    GetDatabaseHandler(),
    {
        'selectors' => {'code' => ['LIKE', 'IPR0005%'],},
    },
);
ok($ipr, 'IPR object creation from database');
IPR_INSTANCE:
{
    skip 'IPR not instantiated, skipping tests.' unless $ipr;
    # explain('IPR instance: ', $ipr);
    ok(@{$ipr->sequences()}, 'Load ipr sequences');
    # explain('IPR instance: ', $ipr);
}

done_testing();
