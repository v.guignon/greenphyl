#!/usr/bin/perl

use strict;
use warnings;
use English;

#use Test::Most skip_all => 'old tests that are not very important right now';
#use Test::Most 'defer_plan';
use Test::More; # we don't have Test::Most on CIRAD servers

use lib '../lib';
use lib '../local_lib';


BEGIN {
    use_ok( 'LWP::UserAgent' );
    use_ok( 'Greenphyl' );
}

our $debug = 0;
our $base_url = shift();
if (!$base_url || ($base_url !~ m/^https?:\/\//)) {
    $base_url = GetURL('cgi');
}

run();

sub run {

    my @scripts = (
        {
            path => '/index.cgi', requests => { name => 'Home Page' }
        },
        {
            path => '/family.cgi',
            requests => [
                { name => 'Annotated gene family list', params => { type => 'VAL' } },
                { name => 'Transcription Factor list', params => { type => 'FT' } },
                { name => 'Enzymatic list', params => { type => 'KEGG' } },
                { name => 'Plant-Specific List', params => { type => 'PSP' } },
                { name => 'Family Info 21076', params => { id => '21076' } },
                { name => 'Gene ID search', params => { '.state' => 'Cluster' } },
                { name => 'Download sequences', params => { '.state' => 'Fasta' } },
                
                {
                    name => 'Family classification of sequences - HTML',
                    params => { '.state' => 'Get classification', format => 'fasta', seq_id => 'Os03g49990', dump => 'html' }
                },
                {
                    name => 'Family classification of sequences - Excel',
                    params => { '.state' => 'Get classification', format => 'fasta', seq_id => 'Os03g49990', dump => 'excel' }
                },
                {
                    name => 'Family classification of sequences - XML',
                    params => { '.state' => 'Get classification', format => 'fasta', seq_id => 'Os03g49990', dump => 'xml' }
                },
            ]
        },
        {
            path => '/plantslim.cgi', requests => { name => 'GO Tree' }
        },
        {
            path => '/specific_list.cgi', requests => { name => 'Phylum specific list' }
        },
        {
            path => '/blast_search.cgi', requests => { name => 'BLAST search' }
        },
        {
            path => '/meme_blast_search.cgi', requests => { name => 'Domain architecture search' }
        },
        {
            path => '/transeq.cgi', requests => { name => 'Translate sequence' }
        },
        {
            path => '/login.cgi', requests => { name => 'Annotator access', params => { '.state' => 'Home' } }
        },
        {
            path => '/documentation.cgi', requests => { name => 'Docs: Overview', params => { page => 'overview' } }
        },
        {
            path => '/credits.cgi', requests => { name => 'Credits' }
        },
    );
    
    map confirm_script(), @scripts;
    
    done_testing();
    
    return;
}

sub confirm_script {
    my $path = $_->{path};
    
    $_->{requests} = [ $_->{requests} ] if ref( $_->{requests} ) eq 'HASH';
    my @requests = @{ $_->{requests} };
    
    map confirm_request( $path, $_), @requests;
    
    return;
}

sub confirm_request {
    my ( $path, $request ) = @_;
    warn "Debug: confirm_request: path=" . $path . "\n" if $debug;

    my $response = get_url( $path, $request->{params} );
    
    ok( $response, "Got response." );
    
    check_response_for_errors ( $request, $response );
    check_response_for_warnings ( $request, $response );
    
    return;
}

sub check_response_for_errors {
    my ( $request, $response ) = @_;
    
    my $test_name = "Request '$request->{name}' completed without errors.";
    
    diag( "Got error: " . extract_error($response) ) if $response->is_error;
    ok( $response->is_success, $test_name );
    
    return;
}

sub check_response_for_warnings {
    my ( $request, $response ) = @_;
    
    my $test_name = "Request '$request->{name}' completed without warnings.";
    
    my @warnings = extract_warnings($response);
    
    map diag($_), @warnings;
    ok( !@warnings, $test_name );
    
    return;
}

sub extract_warnings {
    my ( $response ) = @_;
    
    my $text = $response->content;
    
    my @warnings = ( $text =~ /<!-- warning:\ +(.*) -->/g );
    @warnings = map { "$_\n" } @warnings;
    
    if ( @warnings > 5 ) {
        @warnings = splice( @warnings, 0, 5 );
        push @warnings, "... More warnings were emitted, but truncated ...";
    }
    
    return @warnings;
}

sub extract_error {
    my ( $response ) = @_;
    
    my $text = $response->content;
    
    my $error = "Couldn't find error text in html response.";
    
    if ( $text =~ m@<p(?:re)?>(.*?)</p(?:re)?>@s ) {
        $error = $1;
    }
    else {
        warn "Debug: extract_error: " . $text . "\n\n" if $debug;
    }
    
    return $error;
}

sub get_url {
    my ( $path, $params ) = @_;
    
    my $ua = new LWP::UserAgent;
    $ua->timeout(1800);
    
    my $full_path = $base_url . $path;
    warn "Debug: get_url: full_path=" . $full_path . "\n" if $debug;
    
    my $response = $ua->post( $full_path, $params );
    
    return $response;
}