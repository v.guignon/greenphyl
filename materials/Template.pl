#!/usr/bin/env perl
#------------------------------------------------------------------------------
#--- Perl script Template                                                   ---
#------------------------------------------------------------------------------
#--- Description:
#--- This is the template file for Perl scripts of GreenPhyl project that
#--- should be used as starting point for new scripts.
#--- This template was inspired from the work of Steven McDougall:
#---  http://world.std.com/~swmcd/steven/perl/module_pod.html
#--- and the work done for CIRAD (ID Team) and Phylogeny.fr.
#---
#--- Lignes containing "#+++" have to be modified or removed.
#--- Lignes containing "#---" have to be removed.
#--- Any content inside square brackets has to be modified, the rest can be
#--- left as is. Then, square brackets has to be removed.
#--- Once modifications have been done, "#+++" comments should be removed.
#---
#--- Authors:
#---  Valentin Guignon
#---
#--- Template version:
#---  2.0.0
#---
#--- Date:
#---  02/05/2012
#------------------------------------------------------------------------------

=pod

=head1 NAME

[Script name - short description] #+++
#--- example: welcome.pl - Welcome Message Script

=head1 SYNOPSIS

    #+++ Code of how to call this script

=head1 REQUIRES

#--- Module requierments
#--- example:
[Perl5.004, POSIX, Win32] #+++

=head1 DESCRIPTION

#--- What this script can do, how to use it, ...
[Script description]. #+++

=cut

use strict;
use warnings;

use FindBin;
use lib "$FindBin::Bin/../lib"; #+++ change path if in sub-directories
use lib "$FindBin::Bin/../local_lib";
use Carp qw (cluck confess croak);

#--- when using multi-thread processes
#+++ use threads;
#+++ use threads::shared;

use Greenphyl;
#--- if log not used, also remove @Greenphyl::Log::LOG_GETOPT from GetOptions()
#+++ use Greenphyl::Log('nolog' => 1,); #--- disable log by default (use -log in command line to enable)
#+++ use Greenphyl::Log; #--- enable database log by default

#--- in case of a command-line script
#+++ use Getopt::Long;
#--- in case of a command-line script
#+++ use Pod::Usage;

#--- when working with files
#+++ use Fatal qw(:void open close);

++$|; #no buffering




# Script global constants
##########################

=pod

=head1 CONSTANTS

B<$REV_STRING>: (integer)

Current revision (auto-set by SVN when the appropriate property has been set).

B<$DEBUG>: (boolean)

When set to true, it enables debug mode.

B<$CONSTANT_NAME>: ([constant nature]) #+++

[constant description and use]. #+++

=cut

# SVN management part
my ($REV_STRING) = ('$Revision: 1234 $' =~ m/(\d+)/);

our $DEBUG = 0;
#+++ our [$CONSTANT_NAME] = ["value"];




# Script global variables
##########################

=pod

=head1 VARIABLES

B<[g_variable_name]>: ([variable nature]) #+++

[variable description, use and default value]. #+++
Default: [variable default value if one] #+++

#--- Example:
#--- B<$g_output_method>: (integer)
#---
#--- used to store current output method;
#--- 0=raw text (default), 1=image, 2=html.
#---
#---     ...
#---
#--- my $g_output_method = 0;
#---

=cut

#+++ my [$g_variable_name] = ["value"];




# Script global functions
##########################

=pod

=head1 FUNCTIONS

=head2 [SubName] #+++

B<Description>: [function description]. #+++

B<ArgsCount>: [count of arguments] #+++

=over 4

=item $variable_name: ([variable nature]) ([requirements]) #+++ see below

#--- requirement can be:
#--- (R)=required,
#--- (O)=optional
#--- (U)=optional and must be undef if omitted
[variable description]. #+++

=item $variable_name2: ([variable nature]) ([requirements]) #+++

[variable description]. #+++

=back

B<Return>: ([return type]) #+++

[return description]. #+++

B<Example>:

    [example code] #+++

=cut

#+++sub [SubName] #+++
#+++{
#+++    my ([...]) = @_; #+++ add missing arguments
#--- if needed, parameters check:
#+++    # parameters check
#+++    if (0 != @_)
#+++    {
#+++        confess "usage: SubName();"; #+++
#+++    }
#+++}




# Script options
#################

=pod

=head1 OPTIONS

#--- describes parameters given to the script
#+++ command line syntax
#--- requirement of the option and its parameter can be:
#--- required: name or nature inside <>
#--- optional: name or nature inside []
#--- alternative between 2 elements: elements separated by a |

=head2 Parameters

=over 4

#+++=item B<-help>:
#+++
#+++Prints a brief help message and exits.
#+++
#+++=item B<-man>:
#+++
#+++Prints the manual page and exits.
#+++
#+++=item B<-debug> (integer):
#+++
#+++Executes the script in debug mode. The integer value is optional and is used
#+++to set debug level (use 0 to force disable debug mode).
#+++Default: 0 (not in debug mode).
#+++

=item B<[option_name]> ([option nature]): #+++

[option description]. #+++
Default: [option default value if one] #+++

#--- remove if log not used
#+++=item B<-log>:
#+++
#+++Enable logging.
#+++
#+++=item B<-log-*> (any):
#+++
#+++Logging arguments.

=back
#--- Example:
#---
#--- Template.pl [-help | -man]
#---
#--- Template.pl [-debug [debug_level]] [-size <width> [height]]
#---
#--- =over 4
#---
#--- =item B<-help>:
#---
#--- Prints a brief help message and exits.
#---
#--- =item B<-man>:
#---
#--- Prints the manual page and exits.
#---
#--- =item B<-debug> (integer):
#---
#--- Executes the script in debug mode. If an integer value is specified, it will
#--- be the debug level. If "-debug" option was used without specifying a debug
#--- level, level 1 is assumed.
#--- Default: 0 (not in debug mode).
#---
#---=item B<-size> (positive_real) (positive_real):
#---
#--- Set the dimensions of the object that will be drawn. The first value is
#--- the width; the height is the second value if specified, otherwise it will
#--- assume height and width are equal to the first value.
#--- Default: width and height are set to 1.
#---
#---=back

=cut


# CODE START
#############

#--- Used for regression test if script functions should be tested without
#--- running the actual script.
#+++ # for regression tests
#+++ if ($ENV{'TESTING'} && $ENV{'TESTING_NO_RUN'})
#+++ {return 1;}

#--- uncomment the following lines in case of a command-line-like script
#+++# options processing
#+++my ($man, $help, $debug, $no_prompt) = (0, 0, undef, undef);
#+++# parse options and print usage if there is a syntax error.
#--- see doc at http://perldoc.perl.org/Getopt/Long.html
#+++GetOptions('help|?'     => \$help,
#+++           'man'        => \$man,
#+++           'debug:s'    => \$debug,
#+++           'q|noprompt' => \$no_prompt,
#+++           'length|l=i' => \$length, # numeric specified either with "-length 42" or "-l 42"
#+++           'file=s'     => \$data, # a string
#+++           'files=s{,}' => \@files, # a set of strings specified either with "-files toto tutu" or "-files toto -files tutu"
#+++           'flag!'      => \$flag, # a flag wich can set or unset with "--flag" (set) or "--noflag" (unset) or "--no-flag" (unset)
#+++           @Greenphyl::Log::LOG_GETOPT, # skip log parameters
#+++) or pod2usage(1);
#+++if ($help) {pod2usage('-verbose' => 1, '-exitval' => 0);}
#+++if ($man) {pod2usage('-verbose' => 2, '-exitval' => 0);}
#--- end of lines for a command-line-like script

#+++# change debug mode if requested/forced
#+++if (defined($debug))
#+++{
#+++    # just '-debug' ?
#+++    if ($debug eq '')
#+++    {
#+++        $debug = 1;
#+++    }
#+++    $DEBUG = $debug;
#+++}

#--- to get a database handler: my $dbh = GetDatabaseHandler();
#--- note: do NOT use global database handlers to support multi-thread

#--- Note: prefer "PrintDebug" for scripts that may be called through the web
#---       and "LogDebug" for scripts that are only used in command line.

#--- to display debug messages (either web or console):
#--- PrintDebug("Debug message without a terminal line ending.");

#--- to log messages:
#--- LogInfo('Message');    # used for regular log messages
#--- LogVerboseInfo('Message'); # used for verbose log messages only when verbose mode is set;
#--- LogDebug('Message');   # should be used to only log debugging messages;
#--- LogWarning('Message'); # should be used to log warning messages;
#--- LogError('Message');   # should be used to log errors.

#--- to process parameters without GetOptions
#--- my $example_param  = GetParameterValues('example');
#--- my @example_params = GetParameterValues('examples', $namespace, '[,;\s]+', $allow_file);

#--- Query user:
#--- my $answer = Prompt("Would you like to do something? [Y/N]", { 'default' => 'Y', 'constraint' => '^[yYnN]', }, $no_prompt);
#--- if ($answer =~ m/Y/i)
#--- {...}


#--- opening a file when not using 'Fatal' lib:
#+++ my $file_handle;
#+++ if (open($file_handle, '>file.txt'))
#+++ {
#+++     ...
#+++     print {$file_handle} 'some text';
#+++     close($file_handle);
#+++ }
#+++ else
#+++ {
#+++     confess "ERROR: failed to open file:\n$!\n";
#+++ }
#--- if not using 'Fatal':
#+++ open($file_handle, '>file.txt');
#+++ ...
#+++ print {$file_handle} 'some text';
#+++ close($file_handle);

#--- "try-catch" statement:
#--- # try:
#--- eval
#--- {
#---     ...
#---     if (...)
#---     {
#---         Throw('error' => 'I got this error message!', 'log' => 'and that is for log only');
#---     }
#--- };
#---
#--- # catch
#--- HandleErrors();
#--- Other possibility for specific catch needs:
#--- my $error;
#--- if ($error = Exception::Class->caught('Greenphyl::Error'))
#--- {...GreenPhyl excpetion...}
#--- elsif ($error = Exception::Class->caught())
#--- {...otherwise...}
#--- else
#--- {...no exception...}

#... #+++

exit(0);

__END__
# CODE END
###########

=pod

=head1 DIAGNOSTICS

#--- Give and explain here every error message the the script may generate.
#--- Use: (W)=warning (optional), (D)=deprecation (optional),
#---      (S)=severe warning (mandatory), (F)=fatal error (trappable),
#---      (X)=very fatal error (non-trappable).
#--- example:
#---
#--- =over 4
#---
#--- =item *
#---
#--- Negative radius;
#--- (F) Can not draw a circle with a negative radius.
#---
#---=back #+++

=head1 AUTHORS

Mathieu ROUARD (Bioversity), m.rouard@cgiar.org

Valentin GUIGNON (Bioversity), v.guignon@cgiar.org

Matthieu CONTE (Syngenta)

Sebastien BRIOIS (Syngenta)

=head1 VERSION

Version [version.subversion.build] #+++

Date [DD/MM/YYY] #+++

=head1 SEE ALSO

#--- other documentation or objects related to this package
GreenPhyl documentation.

=cut
