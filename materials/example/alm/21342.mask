>SEQ0000047/1-235
METLMDAFELEHKWKELEEHFHGLERFELEALEQAALCEQMDSEGLHKFIEEILALKAAA
NPAQFVLNSLEDFYRTCIMLMECSILLMDVKDQAKIAEWKPLDDANNGEAHAFLQLLATF
GIADFDEELSLIRQAELCLGLSEKMPGVIEVLVNGQIDAVNLAFAFDLTEQFSPVTLLKS
YLKEARKPRELAALKAVIKCIEEHKLEEQYPGPLQKRLLQLEKKKPKRR-YPQ-------
-----GYH
>SEQ0000046/1-175
-ESELKSFEIHQQWKELQTHFDSTFCAELHEEEKERHLLTMDGKALQIFLNEVTALGLSS
DPAKLVLDAMEGFYKSCNLLLEQM-----VREAARLADWRTD------------------
---------------------------------------------LELVKEFRPVPLLKY
YLHDSKIVKRAADLRAVIKCIEDHKLEPELSPFLQEQIAYLEMKKKSTTSSPT-TAAATT
TSATT---
>SEQ0000035/1-234
METLMEAFELEQKWKQLEDHFHGLEKFDLKVLEQAALCEEMNVNGLHKFIEEIVALRGAT
DPYGLVLASLEDFYRTCLMLMESAQLQTTIKERAKIAEWKSLDDASNGEAHAFLQLLATF
GIFEFADELCLLRQTELCLGLSQNMPGVIGVLVEGTIDAINLAYAFELTNQFEPVELLKA
YLQEVKSLRELSALKAAIKCIEEHKLDEKYPILLQKRVIQLEKKRSKRR-H-S-------
-----GYI
>SEQ0000077/1-247
MAAAAGGFELEQLYQQLSDHFASLERSAIRSLEDAGLCARMDSAAFFGFVSEMPALKCCV
DPAKFVMDAVADVFWACVLILEAVPALADARERARMAEWKEKGGVEGADAHAFLQLVVTF
AVARADLLYRIVRQMRLAVGLDEDMPDIIEELIARQLDAVNFAYEAGLQEKFPPVPLLKS
YLEDSKKGKEQSALRAVIKCIEDRKLESEFPPDLQKQLEDLEKKKNKRRATSYPFGPSPT
PYGYY-YY
>SEQ0000060/1-236
MAAAAAGFELEQLYQQLADHFGSLERSQLRSLDHEALCLRMDSSAFLGYVSEMAALKLCV
DPAKFVMDAVADVFWACVLILEAVPALADARERARMAEWKEKGGVEGADAHAFLQHVATF
AVAREDPLYRIVRQMRLALGLEEEMADIIEELIARQLDAVNFAYEAGLQEKFPPVPLLKS
YLEDSKKGKEQSALRAVIKCVEDHNLEAEFSLDLRKQLEELEKKKTKRRA---P--PVPI
P-------
>SEQ0000075/1-244
MAAAAAGFELQQLYQQLTEHFATLERSSLRSISDEGLCASMDSAAFLGFVAQLGALKFCV
DPARFVMDAVSEVFWACVLILEAVPALADARERARMAEWKEKGGVEGADAHAFLQHVATF
AVAKEDGLYRIVRQMRLALGLEDEMDDIIEELVTGQLDAVNFASESGLLEKFSPVPLLKS
YLEDSKKGKEQSALRAVIKCVEDHKLEPDFPLDLRKRLEELEMKKNKRRA---LLAPSRT
VYGYY-YY
>SEQ0000109/1-102
M-----------------------------------SSRAFGGTSLQLGTNGIVNLQESA
------------------------------------------------------------
--------------------------DFVEHLINNTVAAVKFSFAYDLDDKDHLVDMLRK
YVKNAKLIEEIASLGTVLQCISDSNLESTGLLDIEYRILELKA-----------------
--------
>SEQ0000113/1-93
------------------------------------------------------------
------------------------------------------------------------
-------------------------------MVNNQLEAVKFIQALNIAHKYALLPIMRS
YIDHAAV-KERTLIGTLQKFIKEQKLEELPIFEANKRMAHLDQRKRSEVQVAGPYRPGAN
QASRD---
>SEQ0000092/1-230
M-----FFELEKLFTTLSTHFSSLQKSSLDSAAEAALWRKMDAPALLRFVAEIAAMAEAV
DPARLVVEAVEEFLWACGLVIQAMVS---IAERAVVVMWKEE------EVVMFLQMVVCF
GLRRFDEYLRLVRDMKLALQFGDKIIDIIDELIKGEIEAVYFASESGLTERFPPIDLLKS
YHRNYKKRSELNSIKAIIKCVEDHKLESEFNLNLRKWATLLEKKKNKRSHVPFDPA----
TQQYLGYR
>SEQ0000102/1-247
MAAAAAGFELEQLYQQLSDHFGSLERSALRSLDHSALCARMDSAAFLGFVAEMPALKLCV
DPAKFVMDAVADVFWACVLILEAVPALADARDRARMADWKEKGGVEGADAHAFLQLVATF
AVAREDPLYRIVRQMRLALGLEEQMADIIEELIARQLDAVNFAYEAGLQEKFPPVPLLKS
YLEDSKKGKEQSALRAVIKCVEDRKLEAEFPLGLRKQLEELEKKKTKRRAASYPVGSTPS
PYGYY-YY
>SEQ0000074/1-229
MAAAIASFRLVSNWDDLDAHISSLQRFQLQDEEIDQASVNMDTSTMAEILRHFPALRGCP
EPHAFVVGAIRDFLENCSWLLCCR-----TLEHAYLADWKEG-P-ESCAIFGLFGFLVSY
NIAEFDSEIIHVRKCELCLGLIHKMTDSINHLIEGEPDALRLACVLNLTDKYPPLYIMNE
YVDKAKKPKQVNALISSWRAVDEYNID-----SIKAEITQLLHRRKFQKE----SFNAGH
SQQFA---
>SEQ0000059/1-221
M-----AISLQYAWRAVETHASSLDRLGY-------ICSAMDGAGLLAYLRALAALQVAP
DPGLLVLSAAATFCASCRLLMALD-----ARDEARVADWKRG------ETFAFLHLVGVF
GLVDVGGEVLLVERAEAFLDLDQHMPILIQKMVNSQLEAVKFIQALNIVHKYPLLPIMRS
YIDHAAV-KERTLLGTLQKFIKEQKLEELPIFEANKRMAHLDQRKRPEVHVAGPYRPGAN
QASRD---
>SEQ0000050/1-120
M--------------TITVY----------------------------------------
------------------------------------------------------------
--------------MVLSESSNKCLWDIIDELVKGEIEAVYFASESGLTKIFSPVSLLKS
YLKNSKKALELNSIKAIIKCVEDHKLESEFSLSLRKRASLLEKRKNKRHFGPFEPSPYSS
PQQYMAYT
>SEQ0000062/1-235
MEALMDAFELEQKWKQLEDHFRGLEKFELKALEQAALCEEMNVKGLHKFIEEISALKKTS
DPYGLVLDSLEDFYRTCLMLMESGQLQTIIVERAKIAEWKSLDDASNGEAHAFLQLLATF
GISEYNDDLCLLRQTELCLGLSQKMPGVIEVLVEGPIDAINLAYVFELTEQFEPVQLLKA
YLRDVKKPRELSALKSVIKCIEEHKLEEQYPVPLQKRVLQLEKKRSKRR-HPS-------
-----GYI
>SEQ0000001/1-235
-ESELKSLEIHQQWKNIETHFESTFSAALREEEKERFSVKMDGKALQILLNEVIALGLSS
DPAKLVLDAMEGFYRSCNLLLEQT-----VRKEATLALWMTD------DVMGFFYLLAAY
GLAAFDDELIRLRQIEFLLELGDKIPGFIENLIVKQMEAIRFICAFEMVNRFPPGPILRD
YLSGSKIIRRVADLMVVLKCVEDYKLETVFSPTLKQQIKDVERSEKAASCAPM-AAVVIT
TAAAAAGG
>SEQ0000008/1-243
ME----SFEFQQLWKELSEHFTSMEKSALRTEGEAALCLKMDARGFWGFVSQIVALVDCV
DPPKLVLEAVSEVFWACVVILESIPVMVDVKEKAKIATWKARGGIENVDVHTFLQHLVTF
GIVKDDALYRLVKQMKLAVGLGDQMPDMIEELIIGQLDAVHFTFEVGLVHLFPPVPLLKA
YLRDAKKGKEQSALRAVLKCIEEYKLEEEFPPNLKKRLDQLEKKRNKRRASPQPVHQTPS
AYGYA-YY
>SEQ0000103/1-154
M-----SITLAYQWNSVILDVASIGLFGPG------RCKQMNCGGVRRFVQVVGALRRAP
DPAALVFHAVGRYYAACALLLELV--RAPLRQEARAATWRSV-S-GAVGARGLALFMAAF
GVPEFPQELCLVACVVLKKLFAKKMRG---------------------------------
---------------------EQNQVGSPLFFTFFLSLSGLQM-----------------
--------
>SEQ0000029/1-225
MGAAIEAFCLVCRWDDIDAHVSSIQRFQFQD---GMACASMDSSSLARILRHFPALLAAA
EPAALVVRAVRDLLESCVELLSCP-----TMEQANLADWKEG-T-ESCAVWGLLNFLVSY
NIVEFDEEIIFFKKCSLCLGLIDKMADSVGHLIEGQLVAIRLACTLNLTDKYTPLSIMED
YIQNAKE-KQVNALILSWRVVGECNID-----SIKAEITQLLHRHKHREE----SFYVEH
SQHFT---
>SEQ0000069/1-225
MESALNAFNLQKSWLQLDSHFTSLQRFFLQE-----LCEKIDGIGLRNYIAELEEFRSAP
DAGEMVLQSLEGFYRICVMLLKVS-----AREKALLADWKVD------GALGFLYLVYGF
GIVEFRYVLVFAGEFQLCIGFTDKVPEIVQKLVEGHVLAVKFVFEFSLADKIPPVPILKA
AVDESRK-RELRVLKRVIEIIEIHKLESEYPRSLEQRIEQLKGMKNKVR----SSGPTIS
STGYMH--
>SEQ0000093/1-186
MG----SFEFQQLWKELSDHFSSLEKSALKSLRDAAFCLRMDAFGFFAFVAEMVALAECV
DPAKFVLEAISEVFWACVLVLESIPVVVDVKEHATIATWKSRGGVENLDVHTFLQHVVTF
GIVNDDDLYRLVKQMKLALGLAQQMPDMIEELISGQLDAVHFTYEVGLVEKFPPVPLLKS
FLKDAKKG----------------------------------------------------
------FL
>SEQ0000041/1-240
MG----SFEFQQLWKELSDHFTSLEKSALKTLEELALCLTMESRDFWNFVKQILALAECV
DPAKFVIEAISEVFWACVLILESIPVVVDVKERAKIATWKKRGGIENVDVHTFLQHLVTF
GIVKDDDLYRLVKQMKLALGLGDKMPDMIEELISGQLDAVHFTYEVGLVDKFPPVPLLKA
FLKDAKKGKEQSALRAVIKCVEDYKLESRFPPNLKKRLEQLEKKKNKRRATQYPIAPVPN
AYGY----
>SEQ0000006/1-243
MG----SFEFQQLWQELSDHFTSLEKSALKTAGGAALCLKMDARGFWNFVSKIAALVDCV
DPAMLVLEAISEVFWACVVILESTPVIVDVKEKAKIATWKKRGRIENVDVHTFLQHLVTF
GIVSEDALYRLVKQMKLAVGLGDQMPDMIEELISGQLDAVHFTYEVGLVDKFPPVPLLKA
YLRDAKKGKEQSALKAVLKCIEEYKLEEEFPPNLKKRLDQLEKKRNKRRASPQPIPQGPG
AYNYA-YY
>SEQ0000105/1-68
M-----RHSCAARGNTTKSLLDAQDKFCLSE---------------------------VK
E----------------------------LREQVQAGEVNE-------------------
--------------------------QGADGVVQMMPTDINSVLSYRLYIQYR-------
------------------------------------------------------------
--------
>SEQ0000026/1-224
M-----LLHLDHAWSRALAHFASLDRSSLDAEASEAMCGRMDAPALWRFMKEVPAVAASV
DPPRLVLDALADFLWVLGILLRSF-----LVERAAVANWSEKTTEKKEEVQIFLQMVAAF
GLKRYDDFLRLLRELRIALGFEDSLRDVIEEFIKGEIEAIHIAHEAGLLERFPPVPLLKS
YIKRITNSSECNAYKSIIRCVETCQLTSAFNLGIRKKVARMEKRRNKRRSAPG-------
-----G--
>SEQ0000086/1-234
MDTLIDAFELEYKWKELEEHFHGLERFELEALQQAALCKEMDAAGLHKFIDEINALRAAP
NAACLVLDSLEGFYRTCIMLMECCDFLS-IKDRAKVAEWKPLDDASNGEAHAFLQLLASF
GIAGFDEELSLIRQTDLCLGLSEKMPGVIEVLVNGQIDAVNLAFAFDLTEQFCPVSLLKS
YLKDARKPRELVALKAVIKCIEEHKLDEQYPLPLQKRLVQLEKKKPKRR-YPQ-------
-----GYL
>SEQ0000058/1-231
MAAAIAAFRLACAWEDLDGHISSLQRFQLRNEEAKAACANMDAQRLLKLISEFAALRRAP
DAAALALHVVELFLAGCVGLIQMP-----TIEQAKVADWKED-P-ESCASWGLLNFLISY
NIVEFDKEIFLFKKSVLLLGLGNRIPELMDYLIRGQIDVIWLAHVLNMVDKYPPLHLLKG
YIEKAKQHKELDNLRRAHILAKKGITN-----SIREEINILIGKR--QGPKQEPDLAAGH
AQPWS---
>SEQ0000036/1-234
METLMEAFELEQKWKQLEDHFHGLEKFDLKVLEQAALCEEMNVNGLHKFIEEIVALRGAT
DPYGLVLASLEDFYRTCLMLMESAQLQTTIKERAKIAEWKSLDDASNGEAHAFLQLLATF
GIFEFADELCLLRQTELCLGLSQNMPGVIGVLVEGTIDAINLAYAFELTNQFEPVELLKA
YLQEVKSLRELSALKAAIKCIEEHKLDEKYPILLQKRVIQLEKKRSKRR-H-S-------
-----GNR
>SEQ0000040/1-82
--PPVVSIDLTLRYDELQNHLNFIDRSELEAETA--LCQMMCGRGLRKYIEEVAALKCAP
KPAKLVLDCIGRFYEASILVLEFL------------------------------------
----------------LI------------------------------------------
------------------------------------------------------------
--------
>SEQ0000021/1-225
MKAALKAYDLQHSWSHLDAHFTSLHRFLLQP-----LCNNMDGKGLRDYVDTLSALKSAS
DAASMLLDSLDGVVRTCSFLFKQR-----EKLRANLCDWKRD------GAMAFLHFVAAY
GLLELTHEILFSDELELYAGLTDKAPCLVQKLIDSHILAVKFVFEFNLAHKIPPVPILEA
HVNESQK-REIHALKSAIKVIESHNLQSEYPPSLQQRIEQLMKVKIKRR----SYGPSIP
STGYMY--
>SEQ0000007/1-132
MRPKIVSIELAFQFDDLQKHIESIEKLSNGS-----ICELMCSKGLRKYIEEISALKLAK
EPAKFVLDCIGKFYQVSLLILESL----MIKDEAEAAAWRKE----GGDARGLLLLVACF
GVPNFRTDLLLIEIAALK--------------------------------QFQPYSV---
------------------------------------------------------------
--------
>SEQ0000084/1-236
MTVQNDKFELERQWMDIEAHFRDLEKYKLEALDQAAFCEQMDAKGLLNFTEELVALKSAT
DPARLVLDSLEGFYKSCVVFMEAAALMADTKQQAKIAEWKPGGDAANGEAEAFLQLLATF
RIAEFDEELCLVRQAELCLGLTHKMPGVIEALIKGQVDAVHFIQAFQLSESFPPVPLLKT
YLKDLRRPQELAALKAVIKCVGEYSLEADYPLSLQRRVAQLEKRKQKKR-YPHL------
-----GYM
>SEQ0000042/1-236
MKAALKAYDLQLSWADLDSHFSSVQRFLLQSLGDRSLCERMDVKGLRKYMDRLGAMSVAP
DPGSFVLDAMEGFYRTCLDLLEAA-----VNERAKLAEWKRN------EALGFLHLLVAY
NLEEFDGELVYFRQAVLCVDLGEKTADLIQKLIDGQFLAVKFIFEFGLVDKFQPVPLLKA
HLKESKRIREVKTLKSALVLIDEYKLGSEYPRDLKKRIEMLEKAASKRR-IPPAYGSPAL
PAGYTKYY
>SEQ0000066/1-194
----------------------------------------MDGDGLRRFIHELGALKCAV
DPARMVVVALEAY-RACILLLECQVVLAVVKEVAKMAKWRSQKDAAGGDAQAFLQLLATF
GISEYDEELCLIKKTALCIGLSARIPAIVDKLVEGPIEALSMAKEFGIMDRIQPVSLLKN
YLKDARRAKELSATRSVLKCIEEYNLEADFPSPLHKRIFQLEKKKSKRR-VSP-------
-----SFM
>SEQ0000107/1-65
------------------------------------HLLTMDGKALQIFLNEVTALGLSS
DPAKLVLDAMEGFYKSCNLLLEQM-----VREAARLADWRTD------------------
---------------------------------------------LEVL-----------
------------------------------------------------------------
--------
>SEQ0000094/1-243
MG----SFEFQQLWKELSDHFSSLEKSALKSLRDAAFCLRMDAFGFFAFVAEMVALAECV
DPAKFVLEAISEVFWACVLVLESIPVVVDVKEQATIATWKTRGGVENVDVHTFLQHVVTF
GIVNEDDLYRLVKQMKLALGLAQQMPDMIEELISGQLDAVHFTYEVGLVEKFPPVPLLKS
FLKDAKKGKEQSALRAVIKCIEEYKLEDEFPPNLKKRLDQLEKKRNKRRAGQYPIAPAPN
AYGYT-YY
>SEQ0000010/1-235
MRSLMDAFELEQKWKELEEHFHGLERFELEALEQAALCGDMDSTGLHKFVEEIMAFRAAA
NPASLVLDSLEGFYRTCIMLMECSILLSDVKHRAKIAGWNPLDDACNGEAHAFLQLLATF
AIVDFKDELLLIRQAELCLGLAEKMPGVIEVLVNGQIDAVNLAFAFELTEQFSPVSLLKS
YLIEARRPRELIGLKTVIKCIEEHSLEEQYPVPLHKRILQLEKKKPKRR-YPQ-------
-----CYF
>SEQ0000104/1-206
M-----SITLAYQWNSVILDVASIGLFGPG------RCKQMNCGGVRRFVQVVGALRRAP
DPAALVFHAVGRYYAACALLLELV--RAPLRQEARAATWRSV-S-GAVGARGLALFMAAF
GVPEFPQELCLVACVVLKKLFAKKMRDVVIEMINAYLQAMRIILVFEFQEAFPLAPTLAL
IIEKLEH-DDLALLSSISKCMEDHKLSSE---TFAAKIALLEERR---------------
--------
>SEQ0000057/1-222
MEAAIAAFSLACTWENLDAHVSTVQRFQLREAEE--ACGRMDAGTLIEIVKKLFALHLAP
DPAALVLQVVKLLLGKCIRLFRWS-----TIEQAKVADWKEG-P-ECCAR-SLLEFLIAY
NIVEFSHEIIIFVQNKLCLGLADRPTDLIDYMIGGHLEVFHVLQFFNLEDKYTPFSLLKG
YIEKAKQHKE---IWIAHQLAQHKLAD-----SILAEIRYLIGQR--HAL---PNFAAGQ
AQPWM---
>SEQ0000053/1-113
M-----------------------------------------------------------
--------------QTCLIP----------------------------------------
----------------------PLMRDIIDELVKGEIEAVYFASESGLTKIFSPVSLLKS
YLKNSKKALELNSIKAIIKCVEDHKLESEFSLSLRKRASLLEKRKNKRHFGPFEPSPYSS
PQQYMAYT
>SEQ0000079/1-226
M-----LLHLDQAWSRALAHFASLDLSSLDASESEAICRRMDAAALWRFMKEVPALAAAV
DPPRLVLDVLSDFLWVLGILLRSF-----LVGRAAVADWTEKSLEKKMEVHIFLQMVAAF
GLKKFDEFLKLFKELRFGLGFEESVADVVQELIAGVIEAIYIAHEAGLLERFPPAPLLKS
YIKDSTDSIECSACKSVIRCVEACQLLPVFNTSLKEKVERMEKRKNKRRPVPG-------
-----GIA
>SEQ0000061/1-241
MLGGSEVFKLKHQWEDIKGHFLSLDKCELVNLNQAALCEQMDTKGLLKFLDELVALKCAT
DPARFVLNSLEGFFKSCIVLMEAAPALGEIKEQAKIAEWKSVDDASNGEAQAFLQLLTTF
NVDVLDDELCIVKQTVCCLGLNEKIPGIIEELVKHQIDAVHFVQAFGLSEAFPPAPLLKT
YVEELKD-RELLALRAVIKCIEEYKLQKECSLPLQKRVSELKPKRAKKRRLPDQYGPATA
----GV--
>SEQ0000099/1-233
MAVATTFFELEQLFTKLNNHFSFLQKFTLDSAADAALSRKMDAESLLMFVPEIAALKEAV
DAPRLVLDAVEEYLWACGLLIQGI-----IVERAGLLLWKEE------EMVMFLQMVVCF
GLRKVDEYLRSVRDMKVALEFGDKMIDIIDELVKGEMEAVYFASESGLTERFKPIELLNS
YVRNYENQLELTSIKDVIKCVEDHKLESKFRLKLKRRVSQLDKRKRKRGYAPLEPA----
AQQYLAYR
>SEQ0000025/1-229
MKAALNAFDLQLSWSDLDSHFTTVHRFLLQPFSPSNFCEKMDGVGLMNYVAELGAFRHAP
DAGAMVLGALERFHKTCIVMLKQR-----ASVRARLAAWKED------GALGLLHLVCAF
GFVEFSDELFI-KDIKLFIKIFGKRDDIVQKLIDDHILAVKYILEFNLADRISPVPILKA
CVDEAKK-REINTLRSVIKTIESYKLESEYPLSLEQHIEQLKRNKIKRR----SFGPNVP
SAGYMY--
>SEQ0000083/1-243
MG----SFEFQQLWRELSDHFTSLEKSALKTLEDAAFCLKMEAKDFWRFVAKIVALSECV
DPAKFVLEAISEVFWACVVLLESIPVVVDVKEKAKFATWKARGGIENVDVHTFLQHLVTF
GIVEEDDLYRLVKQMKLALGLGDKMPDMIGELISGQLDAVHFTYEVGLVDKFPPVPLLKA
FLRDAKKGKEQSALKAVIKCIEEYKLEAEFPPNLKKRLEQLEKKKNKRRATQYPLAPVPN
AYGYA-YY
>SEQ0000051/1-175
M--ATEFFDLDRLFTTLTTHFKSLQKSSLESAAETALCRRMDSSGLLRFVAEIRAIMEAV
DPARLTLDAVDELVWACGILVQAFPE--CVVERAAILNWKEEGGGGGGEAVMFLQMVLGF
GLKRFDEFLRLVRDMKLAIGFGEKMGVVIAVLCGFRMSVTFVAR----------LMLSKA
SVYN---------LMALI------------------------------------------
--------
>SEQ0000112/1-126
MG----SFDFQQLWKELSDHFSSLEKSALNTLRDAAFCLKMDALGFWGFVAEMEALGECI
DPAKFVLEAISEVFWACVLVLESVPVMVDVKKLAKVAKWKVRGGVENVDVHTFLQHLVTF
GIVSNDGLXX--------------------------------------------------
------------------------------------------------------------
--------
>SEQ0000004/1-224
MEASINAFDLQHSWSEIDSHFSSLQRL---A-----FCEKNDGKGLGNYMEELNAIRCSE
NPAPLVLDAIEGSYRIFVLLLEAI-----LRERARIADWKP-------EALGFLHLVAAF
ELGLFSEEICYIKQATICIGLDNRIGVLVQKFLDGLLVAIRFIYENEMVGEFEPVSILKT
SLKNSRESKELSALRAVIKVVKEKNIESEFMEKLEECVKELEDRKNKRR----PYGLPAP
PRAFAGYY
>SEQ0000064/1-229
MDAAIAAFAVRLEWSELKEDLSDKERYQLVE-ARLALCENMDGEGLRKYVIEMSALQHAS
DPARLVLDAIQGYYRACILLLEASSVL--IKFLVRVAQWKSQ-DGPEGDAQAFLQLLVAY
GLSEYDEELCLVKQSALCLNLSHKIPEIVDHLAAGQIEALAFAHAFDMMDRLEPVSLLKT
YLKDARRCKELTAVKNVIKCIEDYRLEDDYPSPLHKRVVLLERRKAKRR-APA-------
-----G--
>SEQ0000018/1-237
MAVSIDAFDLEFQWLEIEEHFGNLEKSELEALDQAAFCEQMDAKGLLNFTEELVALESAM
EPARLVLDSLEGFYRSCLMFLEAAALLADTKQQAKIAEWKPAGDAANGEAEAFLKLLATF
RIAEFDEELCLVRQAELCLGLTHKMPGVIEVLVNGQIDAVHFVHAFELTERFPPVPLLKT
YLKDLRRAQELAALKAVIRCVEEYKLEADYALPLQKRVAQLEKKKPKKRGYPYV------
-----GYM
>SEQ0000078/1-220
M-----AVSLQYAWSSLYSDATAVDRLGF-------LSSAMDGPGLRAYLRALAALLVAP
DPGLLVLDAAAGFCVACRLLIDLD-----AREEARVAVWKRE------ETIAFLLLVGVF
GLVDVGDQVLLVERAEIFLDLDKHIPVLTQTMINGQLDAVRFIQALDLVHKYPLLPILRS
YITDAKNRKERTLLGALQKFIKEHKLEELPILEAKKRMTQLDQRKFPEAQPAGS--GGGN
NTSRD---
>SEQ0000020/1-214
MTLTVKSVELRLRWDELQKHLDFIQRSELDD-----LCGMMRSRGLRKYIEEVAALKGAP
KPAKLVLECIGRFFQASLLILEFL-----VKEEADAATWRKE----GGDARGLLLLVASF
GIPLFREDLRLIEISALRRFLLARVPDVIQGMIKQNVEAVDFAYTFGLEEKFPIWKILTS
FLREHKE-NYLSAMKSVTRCLEDHRVDSKLLSHIDEKIIQLEKLDSVIL-----------
--------
>SEQ0000117/1-86
M-----KEKLGFELSSTEKQLDFARCNMLN--------------------NELVVKKEQA
CKGSLIRH----------------------------------------------------
--------------------------LCCKELAMEQVEKIKILV----------------
--------KELCTVRNLISDC-----------SKEKQLEVLQDP----------------
--------
>SEQ0000114/1-172
M-----TCSATP-----RARSTTSTGHRLQ------AYARMDSTAFLGFVAEVAALKLRV
DPNKFLMDVITDFFWACVLILEAVPALADAREHAWMAVWRVTRRCCTAGANRCHDLPSLL
PLRKWP-ELRTYEQ----LDLGVETDMLYVDMKVGHLKVMLYGGQWCTFEVFVVVVLLAT
CLSNFTK--------------QNRRLE---------------------------------
--------
>SEQ0000013/1-216
MDAAVAAFRLVCRWEDVDAHLAAI--FHLGA---EAACANMDPSALVDSL--FPALLGAP
DPHALLVRAVGGVLANCVALVECP-----ALAHAKLARWKEG----AGAGWGLLTFIASY
DIAEFDDEIILFVKCELCLGLIGKMTDSVDHFIEGPIDAIRLARTFGLTGKYTPLAIMND
YIENAKKPKKVDALIFLWSAIDGYDMD-----SIKAEITQLLHQQ--QTA------YVGG
GQPFA---
>SEQ0000076/1-206
M-----SVGLTFQWNSLLLDANAVLLP---IAAVA-ICEGMGSRALRRFARVAAALLRAP
DPAALVLRAVGRYYAACLLLLELV--RAPLRAEARTASWRSE---KGRDAHGLILFMAAF
GVPEFPQELYLLACAVLKLLFARKMREVVADRLNGYQEAIGVILAFELQDAFPLAGIMSY
VVEKVVH-EELVLLRSISKYVEEHTPCS----DIAERIKLLEEMQ---------------
--------
>SEQ0000022/1-116
M---------------------------------------LLSHTLKRFS----------
------------------------------------------------------------
--------------SLLF------AIDVADGMIKGAVKAVDLAYTFGFEEKYSPRTALTS
FLQKSEEPKYLAALKSVVNCLEGHKIDVKLLPQLKNKITNLEKANASVSGSISDYGGFVE
GASRY---
>SEQ0000028/1-233
MGAAIAAFVLACAWGDLDSYISSLQRFQLREGEDGAVCAGMDVRGLLKLVHEYVVMRHAP
DAAALVLQVVQGFLGNCVGLIRCP-----TMKQAKLADWKED-T-GSSSSWGLLYFLISY
NIVEFSDEIFIFQQCELCLGLVNRITDLIDYLIGGQLEALLLTQAFNLIDKYTPLSLLKG
YVERAKQRKEVDSLMVAQNIVQQQITD-----FMLAEMKKLLDRSQQHGPKQQPSVAAIH
AQLWA---
>SEQ0000115/1-140
M-----VISLQYAWRAVETHASTLDRLGF-------ICSAMDGAGLRAYLRALAALQVAP
DPGLLVLSAAASFCASCRLLIALD-----AREEARVADWKRG------ETFAFLHLVGVF
GLVDVGSEVLLVERADAFLDLDQHMPSKSHFSLSTR-RAVKCLV----------------
------------------------------------------------------------
--------
>SEQ0000101/1-188
MAAAAAGFELEQLYQQLSDHFGSLERSALRSLDHSALCARMDSAAFLGFVAEMPALKLCV
DPAKFVMDAVADVFWACVLILEAVPALADARDRARMADWKEKGGVEGADAHAFLQLVATF
AVAREDPLYRIVRQMRLALGLEEQMADIIEELIARQLDAVNFAYEAGLQEKFPPVPLLKS
YLEDSKKG----------------------------------------------------
--------
>SEQ0000039/1-221
-------MDLEQEWKEFEDYFTELERLDLVALSQSALCEIMDGDGLRKYINELSALQCAI
DPARMVLGTLEGYHRACILLLECAVVLAVVKESAKVAQWKSQGDTA-GDAQAFLQLVATF
GIAEYNDELCLVRQTALCLGLTAKIPDVVDRLAKGQIEALSFAHSFGIMDRVLPIPLLKA
YLKEARRAKELAALKAVLKCIEEYQLESQYPSPLQKRVLHLEKRKTKRR-----------
--------
>SEQ0000017/1-229
M-----LFSLEQLYKTLSNHFSSLQKSSLDSAAEAAYCRKMDSSGLLRFMSEISAMEESV
DSARLVLDAVEEFVWACGMLMQAF--PALVVERAAVALWKGG----GEEAAMFMQMVAGF
GLKKFDEFLRQVRDMKLALGFGEKMGDIIDELVKGEIEAVYFASESGLTERFSPVSLLKS
YLHNSRKTVELNSIKTIIKCVEDHKLESEFSISLRKRATQLEKRKNKRHFATLPPFHSMW
--------
>SEQ0000123/1-58
--------------------------------------------------AEMVALAECV
NPMKFVLEAILEVFWACVLVLESIPVVVDVKEHATITTWKSHGDVENV------------
------------------------------------------------------------
------------------------------------------------------------
--------
>SEQ0000067/1-188
----------------------------------------MDGDGLRRFIHELGALKCAV
DPARMVVVALEAY-RACILLLECQVVLAVVKEVAKMAKWRSQKDAAGGDAQAFLQLLATF
GISEYDEELCLIKKTALCIGLSARIPAIVDKLVEGPIEALSMAKEFGIMDRIQPVSLLKN
YLKDARRAKELSATRSVLKCIEEYNLEADFPSPLHKRIFQLEKKKSKRR-----------
--------
>SEQ0000054/1-233
M--ATEFFDLDRLFTTLTTHFKSLQKSSLESAAETALCRRMDSSGLLRFVAEIRAIMEAV
DPARLTLDAVDELVWACGILVQAFPE--CVVERAAILNWKEEGGGGGGEAVMFLQMVLGF
GLKRFDEFLRLVRDMKLAIGFGEKMGDIIDELVKGEIEAVYFASESGLTKRFSPVSLLKS
YLKNSKKALELNSIKAIIKCVEDHKLESEFSLSLRKRASLLEKRKNKRHFGPFE------
----L-KP
>SEQ0000097/1-119
MNVEERCCELE--------------EV---EERKEEFSITIDGTSEEI---GIDNLRESS
DPAKLVLDIILNPTEGWIYLLEQM-----VRDEALLAELKAN------EALGFLLILSIY
GLVYFDDEVFIFKIAKLCLGFANKVSGMLI------------------------------
------------------------------------------------------------
--------
>SEQ0000011/1-196
----------------------------------------MNSRGLRKYIEKIVALKKAP
EPAKLVFECIGRFYQASILVLEYL-----LKAEAAAAAWRKE----GGDARGLTLFLACF
GIHVFREDIALVEISLLRGSLSSRCSDIAQGMMKGVVEALDLAYTFGFEERFSPQTVLNS
FLQKSNEPKYLSDLKSVINCLEGHKVDAKLLPKLKDTILKLEKVTPYMSSSVSGYGGYLD
GVPHY---
>SEQ0000014/1-222
MEAAIAAFTLACTWEDLDAHVSSVQRFQLREEEE--ACKRMDAETLVEFVKKLSAMHHAP
DPAALVLQVVKLLVVQCITLFRWS-----TTEQAKVADWKKD-T-VCCSR-GLLQFLIAY
NIVEFNHDIIIFVRIKLCLGLADRATGLIDYMIGGHPEVFHLVQNFNLEDKYPPFSLLKG
YIQKAKQHKE---LWIAHYLAEQKLTD-----SIMAEIKYLMSQR--HAL---PNFAAGH
AQPWS---
>SEQ0000118/1-65
MESKLKTIQLHRQYKEIEGHFNSTQRAELRWSEKKE------------------------
------------------------------------------------------------
----------------LC--------RILNELEEERIEL---------------------
---------------------EERK--------LMERIKEFDE-----------------
--------
>SEQ0000068/1-235
MSLALDAFELEREWKQLEEHFAQVERYELSALAQAALCENMDGEGLRRYVLELVAIRCAI
DPARLVLDALEGYSRACVLILESGSALAVIKERAKLARWKSLKDSSGADAQVFLQLLATY
GIAEYDEELCLVRQSALCLGLAPKIPDVVDKLAKGQVEALAFAHAFEIMDRVEPVPLLKA
YLKDARKAKELSALKSVIKCIEEYKLDAQFPPSLQKRIDVLERRKAKRR-ASQ-------
-----GFL
>SEQ0000081/1-236
MKAAIKAYELQHPWSDLDSYLTSVQRFLLETSSPNLLCEKMDGKGLRKYIAELDAMRCAS
DPGAMVLDALEGYYRGCVLLLEQM-----VRERAKMAEWKMI------EALGFLHLVAAY
GLGEFSDELVYFRQAILCVGLGDEVSDLIQKLLDGQLLAIKFIFEFQMSDRFPPVPLLQD
YLKESKMLKQLSALRAVIKVIEGHNLESQYPRSLEKRIEQLEKRKTKRR-IPQAYGHMVP
SAGYMQYY
>SEQ0000122/1-32
--------------------------------------------------AEMVALVECV
DPAKFMLEAISEVFWAYVLVLX--------------------------------------
------------------------------------------------------------
------------------------------------------------------------
--------
>SEQ0000119/1-225
MKTELQTLQLHRQWKHIEDQFDSTHRVELHWDSVKLFTVNMDGKALQIFLDEVIALGFSS
DPAKFVLDAMQGFYRSCILLLEQM-----VRNEAILSSWMTD------EVLGFLQLLASY
GLATFDDELLQLSLSGLFLGFADKISGIIQNLIKKHIEAIRVIYGFELVNEYPPVPLLKD
YLHCSKNIKRVADLKCALSCIQDYKIEYGPSLDLKKLIVNLEKRKPVAPTASP-------
-----PPK
>SEQ0000087/1-234
MDTLIDAFELEYKWKELEEHFHGLERFELEAFQQAALCKEMDAAGLHKFIDEINALRAAT
NAAHLVLDSLEGFYRTCIMLMECCDFLS-IKDRAKVAEWKPLDDASNGEAHAFLQLVASF
GIAGFDEELSLIRQTDLCLGLSEKMPGVIGVLVNGQIDAVNLAFAFDLTEQFCPVSLLKS
YLKDARKPRELVALKAVIKCIEEHKLDEQYPLPLQKRLVQLEKKKPKRR-YPQ-------
-----GYL
>SEQ0000124/1-18
------------------------------------------------------------
------------------------------------------------------------
------------------------------------------------------------
-----------------------------------KRVTQLEKKKLKRR-YPQ-------
-----G--
>SEQ0000043/1-229
-ENELKTMELQKQWKELEDHFDSIREKEVE------FSVKMDGKALQILLNEVIALGLSS
DPAKLVLDAMEGFHRSCNLLLEQT-----VRKEATLALWMID------DVLGFFNLLAAY
GLAAFDDELIRLKQTEFLLELGDKIPGFIQILILKPMEAIRFIFAFEMVNQFPPGPILRD
YLSGSKIIRRVADLMVVLKCIEDYKLETVFSPTLKQQIKDVERRKKAASCAPT-TSVVIT
TAAAAAGG
>SEQ0000071/1-181
ME--VKQVDLEQQFEGRVVELKSKEQVELEEEGKKEPNPAIDGRSLQFLRNDIVNLLASS
DPSKDVLDIIQNHIGSHVVLLEQM-----VQEEALLVNLKAN------VVLGYLLLLSIY
GLVYFDDEILYFKIAELFMGLTHKISDFVQNLIDQHVEAVRFICACNITNKNQSVDLLWE
LVQWAKVIQEIASL----------------------------------------------
--------
>SEQ0000031/1-243
MAAESAGFELEQLYQQLTEHFGSLERSTLRSLSDGDLCASMDSAGFFTFVAELDALKRCV
DPARFAMDAVSEVFWACVLILEAVPSLADARERARMAEWKDKGGVEGADAHAFLQHVATF
AVAKEDELYRIVRQMRLALGLEDEMDDIIEELITGQLDAVNFAYEAGLQEKFPPAPLLKA
YLEDSKKGKEQSALRAVIKCVEDHKLEAEFPLDLRERLEELEKKKNKRRA---LLGPSPT
AYAY--YY
>SEQ0000003/1-171
M-DKEKS-----QFEERVRDLQLKERAELEEDSKKELSPDIDERSLMLLSDDIGNLQGSS
DPSKVVLDIIQNPIDSHILLLKER-----VKEEAMLADLKAN------AILGFLLLLSIY
GLGSFNDDVLLFDIAELFLGFANKISDFVQSLIKQYDEAVRFSCAYNFSNNTQLVDIFQE
HVQNLNLI----------------------------------------------------
--------
>SEQ0000110/1-10
M------------------------QVKLEEEET--------------------------
------------------------------------------------------------
------------------------------------------------------------
------------------------------------------------------------
--------
>SEQ0000098/1-233
MAVATTFFELEQLFTKLNNHFSFLQKFTLDSAADAALSRKMDAESLLMFVPEIAALKEAV
DAPRLVLDAVEEYLWACGLLIQGI-----IVERAGLLLWKEE------EMVMFLQMVVCF
GLRKVDEYLRSVRDMKVALEFGDKMIDIIDELVKGEMEAVYFASESGLTERFKPIELLNS
YVRNYENQLELTSIKDVIKCVEDHKLESKFRLKLKRRVSQLDKRKRKRGYAPLEPA----
AQQYLAYR
>SEQ0000088/1-234
MDTLIDAFELEYKWKELEEHFHGLERFELEALQQAALCKEMDAAGLHKFIEEIHALRAAP
NAACLVLDSLKGFYRTCIMLMECCDFLS-IKDRAKVAEWKPLDDASNVEAHAFLQLLASF
GIAGFNEELSLIRQTDLCLGLSEKMPGVIEVLVNGQIDAVNLAFAFDLTEQFSPIPLLKS
YLKDARKPRELIALKAVIKCIEDHKLDDQYPLPLQKRATQLEKKKPKRR-YPQ-------
-----GYL
>SEQ0000012/1-209
MD--VKQVEFE-QLVGQLKEFKLKKQLELEEKGKKESSRAFGSTSLQLDTNGIANLQESS
DPSKLVLEMILNPIDYQIYLLEQM-----VRKEALLADLKAN------AVLGFLMLLSIY
KLLSFDDEVLLFKIAELFLGFANRVSDFVKHLINKIVAAVRFSCAYDLDDEDQLVDMLRE
HVQNAKLIQEIASLGTVLQCISENRLESADLLEIDHRILVLKS-----------------
--------
>SEQ0000121/1-67
M----------------------------------------DVSGLLKFYGDLVIME-VV
DPAILILDGVDEFMWAYGMLLEVFPEISYVEEKVGILRWKKE------------------
----------------------EKMQ--------------HI------------------
------------------------------------------------------------
--------
>SEQ0000005/1-121
M-----SLKAEKSWRDLESHFDSIERSEIEE-----LKREIESEEKKRFLREQEALQ---
-KNDMKLDVK-------------------MSEELLQQKYEEK------------------
-------------------KLEKKLKDCTRDLALEDLRWVS-------------------
-MRMTKR-RKLKHLNRALEEK-------------QKEVDLIEKKG---------------
--------
>SEQ0000002/1-230
MKAALKAYDLQHSWPDIDSHFTTLHRFHLQTFSNSNLCKKNDGKGLRDFIDELIAFKCAS
NPADMVLDALDGVFRSCNFLFQQR-----VRKKAKLFLWKVD------WTMAFLQFVAVY
DFLELNEELAYSDELDLYIALSDRVQDVIQKLIEGQILAVKFIFHFKLTEKTPPVPVLKA
YVNDAEK-REIHALKSVIKVIESYNLDSEFPRSIERRIEELSKGGIKRL----TFGPTVS
STGYMY--
>SEQ0000055/1-206
M-----SVTLAFQWDSVVLDVASIALFGPG------RCQQMNCRGVRRFVQVGGALRRAP
DPAALVLRAIGRYYAACTLLLELV--RAPLRQEARVATWRSG-SGGGVGARGLAFFMAAF
GVPEFPQELCLVACVVLKKLFVRKMRDVVIEMINAYLQAMRIILAFEFQEAFPLAPTLAL
IIEKLEH-EDLALLSSISKCMEDHKLSSE---SFAAKIALLEE-K---------------
--------
>SEQ0000030/1-218
MAEAVASYRLVSFWGNMEGYVGSAQGLSLHAKSLEAACERMDSFDMAELVGEFPALRRAP
DAPALALHAAGYVLDNLAALLRGR-----RAKEATMAKWIAE-EREHQATWALLQFVAAY
AIANLEKEMMVFDGGELILGLPDRATESINRLMKRHIDAVKVARAFNLIDKFPPVSVIKA
YVEKVKESEDVAALRSAKEAIEAHDSG-----SIMQEVHKLMRKRVYFV-----------
--------
>SEQ0000045/1-38
-ESELKSFEIHQQWKELQTHFDSTFCAELHEEEKER--------------------GLN-
------------------------------------------------------------
------------------------------------------------------------
------------------------------------------------------------
--------
>SEQ0000056/1-218
-AAAVATFRLVCRWEDLDAHLASIA-FHLDS---AAACANMDPSALVDVL--FPALLGAP
DPHALLVRAVGGFLSNCVALIECP-----ALAQAKLAHWKEG-A-DAGAGWGFLSFIASY
NIVEFGDEIILFVKCELCLGLIGKMTDSINHFIEGPLDAIRLAHTFSLTDKYPPLAIMND
YIENAKKLEKVDALIFSWSAIDGCDID-----SIKAEITQLLHQQ--QKP------YAGN
GQPFA---
>SEQ0000096/1-96
MNVEERCCELE--------------QVEFEEEIMKE-----DGASEEI---DIDNLRESS
DPAKIVLDIILNPIESRIYLLEKM-----VRDEALLAELKAN------EVLGFLLILSIY
GLHYFDDEV---------------------------------------------------
------------------------------------------------------------
--------
>SEQ0000024/1-200
R-----AFLPECLLLNISIEWLSFY-----------SSPTIDGRNLHFPS---VNLHTSS
DPAKLVLDIILVPIESHILLLEQM-----VREEALIAALKAS------TILGFLLLLSAY
GLVYFRDELFQLKQAELFLGFVDKIFDFVRNLIMQHIEAVRFICAYKLADKIQPVDLLRQ
HVAKVKSVEEIVGLRTVLECISENNLESHQDLEINDRIVDLEKVVSKRRP----------
--------
>SEQ0000100/1-247
MAAAAVGFELEQLYQHLSDHIGSLERSALRSLDHKGLCARMDSAAFLGFVAEMAALKLCV
DPAKFVMDAVADVFWACVLILEAVPALADARERARMAEWKDKGGVEGADAHAFLQHVATF
AVAKEDPLYRIVRQMRLALGLEEEMADIIEELIARQLDAVNFAYEAGLQEKFPPVPLLKS
YLEDSKKGKEQSALRAVIKCVEDRNLEAEFPLGLRKQLEELEKKKTKRRAAPHPVGSTPS
PYGYY-YY
>SEQ0000032/1-212
M-----ALGLSFHWNSFIRYATAISLPADQAAPVA-ICERMGSGELLRFVRAVPALRRAP
NPAELVLRAIGRYYAACELLLLSV--RAPLRAEARAASWRSS---KGRDARGLLLLMAAF
GVPEFPQEIFLLACAVLKKHFLDKLRDVVAHMLNGYHQTVATIIAFELQDAFPLSAIATC
VIERVGR-EKLALLRLLSKYVEDPKQC-----SIADRIAMLEQHQKNLG-----------
--------
>SEQ0000095/1-243
MG----SFEFQQLWKELSDHFSSLEKSALKSLRDAAFCLRMDAFGFFAFVAEMVALAECV
DPAKFVLEAISEVFWACVLVLESIPVVVDVKEHATIATWKSRGGVENLDVHTFLQHVVTF
GIVNDDDLYRLVKQMKLALGLAQQMPDMIEELISGQLDAVHFTYEVGLVEKFPPVPLLKS
FLKDAKKGKEQSALRAVIKCIEEYKLEDEFPPNLKKRLDQLEKKRNKRRAGQYPIAPAPN
AYGYT-YY
>SEQ0000070/1-211
ME--EKQVDLE-HFGSQLKGLKSKERMELKEEGEQEPSPTIDGRSLQFLPNDSANLLASS
DPSKDVLDIIQNPIDHHIDLLEQM-----VREEAMLAKLKAN------PVLGFLLLLSIY
GLVSFDDEILLFKISELFMGLAHKVSDFVQNLIMQYIEAVRFICAYNTATKNQSVGLLRE
HVQNARSIQEIASLGTVLQCLSDNNMESVDLLEIHGRIHELNRRR---------------
--------
>SEQ0000111/1-33
MNVEERCCELE--------------QVELEEKEKKE------------------------
----------------------QH----------------------------AMLILAEK
G-----------------------------------------------------------
------------------------------------------------------------
--------
>SEQ0000027/1-216
M-----ALSLQYAWRALYSDATALDRLGF-------LCSAMDGPGLRAYLPALAALLVAP
DPGRLVLSAAAGFCVACRLLVDLD-----ARDEARIADWKRG------ETIAFLLLVGAF
GLVDVGSEVLLVERAEAFLDLEKHMPVFIHTMIKGQLEAVKFIQALNLVEKYPLLPVLRS
YISDAAK-KERMLLGVLQKFIKDQKLEELPILIVKQRLAHLEKRKVPGAQ---P-RPG-S
HTSRD---
>SEQ0000044/1-91
------------------------------------------------------------
------------------------------------------------------------
------------------------------------MEAIRFIYAFEMVNQFPPGPILRD
YLSGSKIIRRVADLMVVLKCIEDYKLETVFSPTLKQQIKDVERRKKAASCAPT-TSVVIT
TAAAAAGG
>SEQ0000009/1-225
METAINAFDLQHSWSEIDSHFSSLQRFLLHR-----LCEKIDGIGLIKYLQEVAAIRYSP
DTASMVLDAIEGSNRVFVLLMEVI-----TRNRAKLAHWKS-------EALVFLHLVAAF
ELGEFDEELSYVKQALVCIGVDKRVGKLIKTLLDGPILAVKFMYECGMTDEFEPIPVLKS
YIKDCRESKEVSALKPLIKIIKDQNLESEFTQKVEERVEELEKRKKKRR----EYGFSVN
LTGFQG--
>SEQ0000090/1-235
MEEEINAFELEQQWVEIKQHFHDLEKLELEVLDQAAFCEQMDAKGLLNYIEEIVALQSAT
DPACLVLDLLEGFYKSCIIILEAATLLADTKQHAKIAEWRPADDAANGEAKAFFQLISTF
KIAEFDEELCLVRQAELCIGLIHKMPAVVESLINGQIAAVHFIHAFQLQESFPPVPLLKA
YLKNRRRIQELAALRAVIKCIEEYKLESDYPPTLRKRVLQLEKRKSKRR-YPH-------
-----SYL
>SEQ0000085/1-235
MRTLIDAFELEHKWKELEEHFHGLERFELEALEQAALCIEMDSDGLHKFIEEIFALKAAA
SPAGIVLDSLEGFYRTCIMLMECSILLMDIKRRAKIAEWKPLDEASNGEAHAFLQLLDTF
QIADFDEQLSLIRQTDLCLGLSEKMPGVIEVLVNGQIDAVNLAFAFELTEQFSPVPLLKS
YLKEARRPRELTALKAVIKCIEEHNLEEQYPVPLQKKVLQLEKKKPKRR-YPQ-------
-----GYL
>SEQ0000065/1-229
MDAAIAAFAVRLEWSELKEDLSDKERYQLVE-AQLALCENMDGEGLRKYVIEMSALQHAS
DPARLVLDAIQGYYRACILLLEASSVL--IKFLVRVAQWKSQ-DGPEGDAQAFLQLLVAY
GLSEYDEELCLVKQSALCLNLSHKIPEIVDHLAAGQIEALAFAHAFDMMDRLEPVSLLKT
YLKDARRCKELTAVKNVIKCIEDYRLEDDYPSPLHKRVVLLERRKAKRR-APA-------
-----G--
>SEQ0000072/1-224
MAVATTFFELEQLFTKLNNHFSFLQKFTLDSAADAALSRKMDAESLLMFVPEIAALKEAV
DAPRLVLDAVEEYLWACGLLIQGI-----IVERAGLLLWKEE------EMVMFLQMVVCF
GLRKVDEYLRSVRDMKVALEFGDKMI---------EMEAVYFASESGLTERFKPIELLNS
YVRNYENQLELTSIKDVIKCVEDHKLESKFRLKLKRRVSQLDKRKRKRGYAPLEPA----
AQQYLAYR
>SEQ0000034/1-240
MSGSSEVFKLKHNWEDIKAHFLNLEKCELANLNQTALCEQMDTAGLLKFLDELAALRCAT
DPARFVLGSLEGFFRSCIILMEATPALAEIRELAKIAEWKSVDDASDGEAQAFLQLLTTF
NVDVLDDELCLVKQTELCLCLNERIPDIIKELVNHQIDAVQFIHAFGLSESFPPAPLLKT
YVEELKD-RELLALRAVIKCIEEYKLQKDYPLPLQKRVAELKSKRAKKRR-PDQYGPTGG
----NG--
>SEQ0000049/1-196
----------------------------------------MDAKGLLSFSDELVALEIAT
EPARLVLDSLEGFYKSCVIFMEAAALLADIKQQAKFAEWKPAGDATNGEAEAFLQLLSTF
RIAEFDEELCLVRQAELCLGLTHKIPGVVESLVNGQIDAVRFIHAFQLTEIFPPVPLLKT
YLKDLRRSLELAALKVVIRCVEEYKLEADYPLPLQKRLAQLEKKKSKKK-YPHV------
-----GYM
>SEQ0000073/1-240
MSGGSEVFKLKHQWEDVEAHFTNLEKCELENVNQAALCEQMDTNGLLKYLDELVALKCAT
DPARFVLDSLEGFFRSCIVLMEAAHSLGEIMERSKIAEWRSVDDASDGEAQAFLQLLTTF
NVDILDDELCIVKQTELCLGLTERIPGIIEELIKHQIDAVQFIQAFGLSENFPPAPLLKT
YVDELKD-RELLALRAIIKCVEEYKLQKDCPLPLQKRIAGLKSKRAKKRR-PEQYGPTGA
----TG--
>SEQ0000016/1-235
MQTLIDAFELEHKWKQLEEHFHGLERFELEALRQAALCEDMDSEGLHKFIEEIQALKAAM
DPARFVLDSLEDFYRTCIMLMECSILLTDVKEIAKIAEWKPLDDASNGEAHAFLQLLATF
GIADFDEEISLIRQTDLCLGLSEKMPGVIEILINGQIDAVNLAFAFELTEQFSPVPLLKS
YLKEARKPRELTALKAVIKCIEDHKLEEEYPLPLQKRVVQLEKKKPKRR-YPQ-------
-----GYL
>SEQ0000033/1-241
MAA---GFELEQLYKQLEEHFASLERSSLRSLDDASMCARMDSAGFFGFVAEMPALKCCV
DPAKFVMDAVADVFWACVLILEAVPALADARERARMAEWKEKGGVEWTDAHAFLQHVATF
AVAREDGIYRIVRQMRLALGLEEDMADIIEELIAGQLDAVNFAYEAGLQEKFPPVPLLKS
YLADSKKGKEQSVLRAVIKCIEDRKLEAEFPLDLQRQLEELEKKKTKRRA---PPGSSPS
AYGYY-YY
>SEQ0000015/1-231
MAAAIAAFRLACAWEDLDGYISSLQRFQVRAEDAKAACAYMDAKRLVKLVSELAAFRCAP
DAAAVALHVVELFLANCVELVQMP-----TTEQAKMADWKED-P-ESCASWGLLYFLISY
NIVEFDKEIFLFQKSMLLLGLSNRIPELMDYLIGGQMDVLCLARVLNMVDKYPPLSLLKG
YVEKAKQRKELDDLRRAHFLVKKEITN-----TIREEINILLGKR--HGPKHEPDLAAGH
VQPWS---
>SEQ0000023/1-227
MKAALNAFNLHLSWLDLDSHFTTLHRFLLQPFSPSNLCEKMDGVGLMNYVAELGAFRHAP
DAGTMVLGALEVFHKACIVLLKQR-----ASVRARLAEWKED------GALGLLHLICAF
GFVEFSDELVFSEDFELCIGLTERVPDIVQKLIDDHIPAVKYILEFNLADRISPVPILKA
CVEEAKK-REINTLRMVIKTIESYKLESEYPLSLEQHIEQLKRNKIKRR----SFGPNVP
VL--L---
>SEQ0000080/1-208
MDSDLKTLQLRDKWKELENLSFSIKCLELKEG----LIDCRGGERMLLFLDEVCALQVSA
DPAILVLDAISKGFRSTVLLLEQM-----LRKRGTLAEWKSG------------------
------EDNGEVKQSDLCLGLEDKIPILIENLLKPMLEAMEYICAFELFSEFPPARILKA
YWRYNKKRREIETVRAIIKCIIDHRLESHCSPELENYILELEKGN----ATPT---LSAN
TSGVFGEY
>SEQ0000120/1-140
M-----------------------------------------------------ALGLSS
DPAKLVLDAMQGFYRSCLFLLEIM-----VKTEAMLSDWMRD------EVLGCLQLLASY
KLAVFDDKLLYLNQAELLLDLTDKISSFLKNLITNYTEAIRFIYAFELVNEFPPVPLLKE
FVKDIPT-ERITVLN------------------LNSQLSALSN-----------------
--------
>SEQ0000052/1-152
-ESELKSVEIHQQWKELQTHFDSTFCAELHEEEKERFSVQL-------------------
-------------------------------------------------------IVICY
GVVSGK--------------------GFIQILILKPMEAIRFIFAFEMVNQFPPGPILRD
YLSGSKIIRRVADLMVVLKCVEDYKLETVFSPTLKQQIKDVQRGKKAASCAPT-TSVVIT
TAAAAAGG
>SEQ0000091/1-230
M-----FFELEKLFTTLSTHFSSLQKSSLDSAAKAALWRKMDAAALLRFVAEIAAMEEAV
DPARLVVEAVEEFLWACGLVIQAMVS---IVERAVVVTWKEE------EVVMFLQMVVCF
GLRRFDDYLRFVRDMKLALQFGDKIIDIIDELIKGEIEAVYFSSESGLTERFPPIDLLKS
YHRNYKKHSELNSIKAIIKCVEDHKLESEFNLNLRKRATLLEKKKNKRSHAPFNPA----
TQQYLGYR
>SEQ0000019/1-243
MG----SFDFQQLWKELSDHFTSLEKSALKTLGEAAFCLKMDSKEFWRFIAQTEALAECV
DPAKFVLEAISEVFWACVLVLESIPVVVDVKERAKIATWKARGGIENVDVHTFLQHLVTF
GIVEEDDLYRLVKQMKLALGLGDKMADMIEELVNGQVDAVHFTYEVGLVDKFPPVPLLKA
FLRDSKKGKEQSALRAVIKCIEEYKLEAEFPPNLKKRLEQLEKKKNKRRATQYPLAPGPN
AYGYA-YY
>SEQ0000048/1-235
METLMDAFELEHKWKELEEHFHGLERFELEALEQAALCEQMDSEGLHKFIEEILALKAAV
NPAQLVLDSLEDFYRTCIMLMECSILLMDVKDQAKIAEWKPLDDANNGEAHAFLQLLATF
GIADFDEEISLIRQAELCLGLSERMPGVIEVLVNGQIDAVNLAFAFDLMELFSPVPLLKS
YLKEARKPRELTALKAVIKCIEEHKLEEQYPVPLQKRLHQLEKKKPKRR-YPQ-------
-----GYL
>SEQ0000082/1-163
MAPKAEAVQLGLRFDELQKHLDFIRKFAATPKDA--LCQTMCSRGLRKYVEEAAAIKLAP
KPAKLVLDCIGRFYQASVLVLEFL----LVKEEAEGAGWRKE----GGDARGLLAFVACF
GIPAFKEDLKLIKILALRRVLVPRIA----------------VYLCLHIHEYS------S
FLEGDRN-NEAKTLVV--------------------------------------------
--------
>SEQ0000089/1-213
MEEKINAFELKQQWVEIKQHFHDLEKLELEVLDQAA----------------------AT
NPARLVLDLLEGFYKSCIIILEAATLLADTKQQAKIAEWRPADDAANGEAKAFFQLISTF
RIAEFDEELCLVRQAELCIGLIHKMPVVVESLINGQIAAVHFIHAFQLQESFPPVPLLKA
YLKNRRRIQELSALRAVIKCIEEYKLESEYPPTLRKRVLQLEKRKSKRR-YPH-------
-----SYI
>SEQ0000108/1-74
M-----------ELESREREFNAIQRSELK--------------------EKEKAMKEKY
P----------------------------VVECSELQKEKNR------------------
----------------------ESLRECCDNLESKQLDSI--------------------
--------WHIICLQGIEDNA---------------------------------------
--------
>SEQ0000037/1-188
METLMEAFELEQKWKQLEDHFHGLEKFDLKVLEQAALCEEMNVNGLHKFIEEIVALRGAT
DPYGLVLASLEDFYRTCLMLMESAQLQTTIKERAKIAEWKSLDDASNGEAHAFLQLLATF
GIFEFADELCLLRQTELCLGLSQNMPGVIGVLVEGTIDAINLAYAFELTNQFEPVELLKA
YLQEVKSL----------------------------------------------------
--------
>SEQ0000063/1-224
M-----LLHLDQAWSRALAHFASLERSALEASASEAICRRMDAAALWRFMKEAPAVAMAV
DPPRLVLDVVSDFLWVLGMLLRSL-----LVERAFVTEWQENGTEKKEEAQIFVQMVAAF
GLKKFDEFLRLFRELRFALGFEESLRGIVEELIKGVIEAIFVAHEADLLERFPPVPLLKS
YLRNSTDSLEGNAYRSIIRCVESCQLQSVCPIVMKKKLAKLEKRKNKRRPVPG-------
-----G--
>SEQ0000116/1-245
MLGGSEVFNLKHQWEDIKGHFLSLDKFELVDLNQAALCEQMDTKGLLKFLAELVALKCAT
DPARLVLNSLEGFFKSCIVLMDAAPALGEIKEQAKIAEWKSVDDASNGEAQAFLQLLTTF
NVDVLDDELCIVKQTVSCLGLNEKIPGIIEELVKHQIDAVHFIQAFGLSETFPPAPLLKT
YIEEFKD-RELLTLRAVIKCIEEYKLQKEYSLPLQKRVSELKPKRAKKRRLPDQYGPAAA
QAPASV--
>SEQ0000038/1-234
MEALMEAFELEQKWKQLEDHFHGLEKFELKALEQAALCEEMNVNGLHKFIEEIVALRGAT
DPYGLVLASLEDFYRTCLMLMESAQLQTTIKERAKIAEWKSLDDASNGEAHAFLQLLATF
AIFEFADELCLLRQTELCLGLSQNMPGVIGVLIEGTIDAINLAYAFELTDQFEPVELLKA
YLKEVKSPRELSALKAVIKCIEEHKLDEKYPIPLQRRVIQLEKKRSKRR-H-S-------
-----GYI
>SEQ0000106/1-69
M-----------ELESREREFNAIQRSELK--------------------EKEKAMKEKY
P----------------------------VVECSELQKEKNR------------------
----------------------ESLRECCDNLESKQLDSI--------------------
--------WHIICLQV--------------------------------------------
--------
