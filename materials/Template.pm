#------------------------------------------------------------------------------
#--- Perl Modules Template                                                  ---
#------------------------------------------------------------------------------
#--- Description:
#--- This is the template file for Perl Modules of GreenPhyl project that
#--- should be used as starting point for new modules.
#--- This template was inspired from the work of Steven McDougall:
#---  http://world.std.com/~swmcd/steven/perl/module_pod.html
#--- and the work done for CIRAD (ID Team) and Phylogeny.fr.
#---
#--- Lignes containing "#+++" have to be modified or removed.
#--- Lignes containing "#---" have to be removed.
#--- Any content inside square brackets has to be modified, the rest can be
#--- left as is. Then, square brackets has to be removed.
#--- Once modifications have been done, "#+++" comments should be removed.
#---
#--- Authors:
#---  Valentin Guignon
#---
#--- Contributor:
#---  Steven McDougall
#---
#--- Template version:
#---  2.0.0
#---
#--- Date:
#---  02/05/2012
#------------------------------------------------------------------------------

=pod

=head1 NAME

[Greenphyl::PackageName - short description] #+++

#--- example: Geometry::Circle - manages a circle

=head1 SYNOPSIS

#--- Code of essential steps in using the module

#--- example:

    [my $instance = Greenphyl::PackageName->new(); #+++

    $instance->subName(); #+++

    ...] #+++

=head1 REQUIRES

#--- Module requierments

#--- example:

[Perl5.004, Exporter, Geometry::Point] #+++

=head1 EXPORTS

#--- Tells what this module will do to other namespaces when imported.

#--- example:

[Nothing] #+++

=head1 DESCRIPTION

#--- What this module can do, how to use it, supported objects, ...

[Package description.] #+++

=cut

package [Greenphyl::PackageName]; #+++


use strict;
use warnings;

use Carp qw(cluck confess croak);

#--- in case of inheritance:
#+++ use base qw([BaseClass]);

#--- for exports, add 'Exporter' to the base list.
#+++ our @EXPORT = qw(... list of always exported functions ...); 
#+++ our @EXPORT_OK = qw(... list of functions exported on demand...); 

use Greenphyl;

#--- when working with files
#+++ use Fatal qw(:void open close);




# Package constants
####################

=pod

=head1 CONSTANTS

B<$CONSTANT_NAME>:  ([constant nature]) #+++

[constant description and use] #+++

#--- Example:
#--- B<$DEBUG>: (boolean)
#--- When set to true, it enables debug mode.
#--- ...
#--- our $DEBUG = 0;
#---
=cut

#+++ our [$CONSTANT_NAME] = ["value"];




# Package variables
####################

=pod

=head1 VARIABLES

B<[$m_variable_name]>: ([variable nature]) #+++

[member variable description] #+++
Default: [variable default value if one] #+++

#--- Example:
#--- B<$m_output_method>: (integer)
#---
#--- used to store current output method;
#--- 0=raw text (default), 1=image, 2=html.
#---
#---     ...
#---
#--- my $m_output_method = 0;
#---

=cut

#+++ my/our [$m_variable_name] = ["value"];




# Package subs
###############

=pod

=head1 STATIC METHODS

=head2 CONSTRUCTOR

B<Description>: [Supports non-static calls.] #+++

[Creates a new instance.] #+++

B<ArgsCount>: [count] #+++

=over 4

=item $variable_name: ([variable nature]) ([requirements]) #+++ see below

#--- requirement can be:
#--- (R)=required,
#--- (O)=optional
#--- (U)=optional and must be undef if omitted
[variable description] #+++

=item $variable_name2: ([variable nature]) ([requirements]) #+++

[variable description] #+++

=back

B<Return>: ([Greenphyl::PackageName]) #+++

a new instance.

B<Caller>: [general] #+++ general, specific class name, ...

B<Exception>:

=over 4

[=item exception_type

description, case when it occurs] #+++ see below

=back

#--- Example:

=over 4

=item Range error

thrown when "argument x" is outside the supported range, for instance "-1".

=back

B<Example>:

    [example code] #+++

=cut

sub new
{
    my ($proto, [...]) = @_; #+++ add missing arguments
    my $class = ref($proto) || $proto;
#--- if needed, parameters check:
#    # parameters check
#    if (1 != @_)
#    {
#        confess "usage: my \$instance = Greenphyl::PackageName->new();"; #+++
#    }
#    elsif (...)
#    {
#        Throw('error' => 'I got this error message!', 'log' => 'and that is for log only');
#    }
#--- if needed, copy constructor test (non-static call):
#    # copy constructor
#    if (ref $proto)
#    {
#        ...
#    }
    # instance creation
    my $self = {};
#--- in case of inheritance, use this instead of the above line:
#    my $self = $class->SUPER::new();
    bless($self, $class);

#---    $self->{FIELD} = "value";

    return $self;
}

#+++ Destructor if needed
#=pod
#
#=head2 DESTRUCTOR
#
#B<Description>: destroys this instance.
#
#B<Caller>: perl system
#
#=cut
#
#sub DESTROY
#{
#+++    my ($self) = @_;
#}


=pod

=head1 ACCESSORS

=cut

#--- put here setters and getters (copy-paste text below)
#
#=pod
#
#=head2 get[MemberName] #+++
#
#B<Description>: #+++
#
#B<ArgsCount>: #+++
#
#=over 4
#
#=item $arg: #+++
#
#=back
#
#B<Return>: #+++
#
#B<Caller>: #+++
#
#B<Exception>: #+++
#
#B<Example>: #+++
#
#=cut
#
#sub get[MemberName] #+++
#{
#    my ($self) = @_;
#    # check parameters
#    if ((1 != @_) || (not ref($self)))
#    {
#        confess "usage: my \$value = \$instance->get[MemberName]();"; #+++
#    }
#    return $self->{[MEMBER_NAME]}; #+++
#}

#=pod
#
#=head2 setMemberName #+++
#
#B<Description>:  #+++
#
#B<ArgsCount>:  #+++
#
#=over 4
#
#=item $arg: #+++
#
#=back
#
#B<Return>:  #+++
#
#B<Caller>:  #+++
#
#B<Exception>:  #+++
#
#B<Example>:  #+++
#
#=cut
#
#sub set[MemberName] #+++
#{
#    my ($self, $value) = @_;
#    # check parameters
#    if ((2 != @_) || (not ref($self)))
#    {
#        confess "usage: my \$value = \$instance->set[MemberName](\$value);"; #+++
#    }
#    $self->{[MEMBER_NAME]} = [$value]; #+++
#}


=pod

=head1 METHODS

=cut

#=pod

#=head2 [subName] #+++
#
#B<Description>: #+++
#
#B<ArgsCount>: #+++
#
#=over 4
#
#=item $arg: #+++
#
#=back
#
#B<Return>: #+++
#
#B<Caller>: #+++
#
#B<Exception>: #+++
#
#B<Example>: #+++
#
#=cut
#
#sub [subName] #+++
#{
#    my ($self) = @_;
#    # check parameters
#    if ((1 != @_) || (not ref($self)))
#    {
#        confess "usage: my \$value = \$instance->[subName]();"; #+++
#    }
#    elsif (...)
#    {
#        Throw('error' => 'I got this error message!', 'log' => 'and that is for log only');
#    }
#    # Get script parameters
#    my $example_param  = GetParameterValues('example');
#    my @example_params = GetParameterValues('examples', $namespace, '[,;\s]+', $allow_file);
#}


#--- AUTOLOAD if needed.

#--- AUTOLOAD requires "use vars qw($AUTOLOAD);".

#=pod

#=head1 AUTOLOAD
#
#=cut
#
#=head2 AUTOLOAD
#
#B<Description>: #+++
#
#B<ArgsCount>: #+++
#
#=over 4
#
#=item $arg: #+++
#
#=back
#
#B<Return>: #+++
#
#B<Caller>: #+++
#
#B<Exception>: #+++
#
#B<Example>: #+++
#
#=cut
#
#sub AUTOLOAD
#{
#    my ($self) = @_;
#    (my $method_name = $AUTOLOAD) =~ s/.*://; # strip fully-qualified portion
#    return unless $method_name =~ m/[a-z]/;  # skip DESTROY and all-cap methods
#    [...] #+++
#    # auto-setter/getter example :
#    my ($accessor, $member) = ($method_name =~ m/^(set|get)(.*)/);
#    $member = lcfirst($member);
#    $member =~ s/([A-Z])/'_' . lc($1)/eg;
#
#    # check if the member exists and can be accessed
#    if (exists($self->{$member}))
#    {
#        if ('set' eq $accessor)
#        {
#            # set
#            $self->{$member} = shift;
#        }
#        return $self->{$member};
#    }
#}


return 1; # package return
__END__

=pod

=head1 DIAGNOSTICS

#--- Give and explain here every error message the the module may generate.
#--- Use: (W)=warning (optional), (D)=deprecation (optional),
#---      (S)=severe warning (mandatory), (F)=fatal error (trappable),
#---      (X)=very fatal error (non-trappable).
#--- example:
#--- =over 4
#--- =item Negative radius
#---
#--- (F) A circle may not be created with a negative radius.
#---
#--- =back #+++

=head1 AUTHORS

Mathieu ROUARD (Bioversity), m.rouard@cgiar.org

Valentin GUIGNON (Bioversity), v.guignon@cgiar.org

Matthieu CONTE (Syngenta)

Sebastien BRIOIS (Syngenta)

=head1 VERSION

Version [version.subversion.build] #+++

Date [DD/MM/YYY] #+++

=head1 SEE ALSO

#--- other documentation or objects related to this package
GreenPhyl documentation.

=cut
