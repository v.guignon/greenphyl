#!/usr/bin/perl
#------------------------------------------------------------------------------
#--- Perl CGI script Template                                               ---
#------------------------------------------------------------------------------
#--- Description:
#--- This is the template file for Perl scripts of GreenPhyl project that
#--- should be used as starting point for new CGI scripts.
#--- This template was inspired from the work of Steven McDougall:
#---  http://world.std.com/~swmcd/steven/perl/module_pod.html
#--- and the work done for CIRAD (ID Team) and Phylogeny.fr.
#---
#--- Lignes containing "#+++" have to be modified or removed.
#--- Lignes containing "#---" have to be removed.
#--- Any content inside square brackets has to be modified, the rest can be
#--- left as is. Then, square brackets has to be removed.
#--- Once modifications have been done, "#+++" comments should be removed.
#---
#--- Authors:
#---  Valentin Guignon
#---
#--- Template version:
#---  2.0.0
#---
#--- Date:
#---  02/05/2012
#------------------------------------------------------------------------------

=pod

=head1 NAME

[CGI Script name - short description] #+++
#--- example: welcome.cgi - Welcome Page

=head1 SYNOPSIS

    #+++ example URL with optional GET parameters

=head1 REQUIRES

#--- Module requierments
#--- example:
[Perl5, POSIX, Win32] #+++

=head1 DESCRIPTION

#--- What this script can do, how to use it, ...
[Script description]. #+++

=cut

use strict;
use warnings;

use FindBin;
use lib "$FindBin::Bin/../lib"; #+++ change path if in sub-directories
use lib "$FindBin::Bin/../local_lib";
use Carp qw (cluck confess croak);

use Greenphyl; # includes Config and Exceptions
use Greenphyl::Web; # includes CGI:standard and Templates

#--- when working with files
#+++ use Fatal qw(:void open close);

#--- is URL should be processed
#+++ use URI::Escape;

# use CGI::Carp qw(fatalsToBrowser); # useful for debugging
# use Data::Dumper; # useful for debugging




# Script global constants
##########################

=pod

=head1 CONSTANTS

B<$CONSTANT_NAME>: ([constant nature]) #+++

[constant description and use]. #+++

#--- Example:
#--- B<$DEBUG>: (boolean)
#--- When set to true, it enables debug mode.
#--- ...
#--- our $DEBUG = 0;
#---
=cut

#+++ our [$CONSTANT_NAME] = ["value"];




# Script global variables
##########################

=pod

=head1 VARIABLES

B<[g_variable_name]>: ([variable nature]) #+++

[variable description, use and default value]. #+++
Default: [variable default value if one] #+++

#--- Example:
#--- B<$g_output_method>: (integer)
#---
#--- used to store current output method;
#--- 0=raw text (default), 1=image, 2=html.
#---
#---     ...
#---
#--- my $g_output_method = 0;
#---

=cut

#+++ my [$g_variable_name] = ["value"];




# Script global functions
##########################

=pod

=head1 FUNCTIONS

=head2 [SubName] #+++

B<Description>: [function description]. #+++

B<ArgsCount>: [count of arguments] #+++

=over 4

=item $variable_name: ([variable nature]) ([requirements]) #+++ see below

#--- requirement can be:
#--- (R)=required,
#--- (O)=optional
#--- (U)=optional and must be undef if omitted (position of arguments maters)
[variable description]. #+++

=item $variable_name2: ([variable nature]) ([requirements]) #+++

[variable description]. #+++

=back

B<Return>: ([return type]) #+++

[return description]. #+++

B<Example>:

    [example code] #+++

=cut

#+++sub [SubName] #+++
#+++{
#+++    my ([...]) = @_; #+++ add missing arguments
#--- if needed, parameters check:
#+++    # parameters check
#+++    if (0 != @_)
#+++    {
#+++        confess "usage: SubName();"; #+++
#+++    }
#+++}

=pod

=head2 [DefaultRenderFunctionToCall] #+++

B<Description>: [function description]. #+++

B<ArgsCount>: 0

B<Return>: (string)

Returns an HTML string containing the GreenPhyl page for ... #+++

=cut

#+++sub [DefaultRenderFunctionToCall]
#+++{
#+++    #+++ do some stuff...
#+++    my $example_param  = GetParameterValues('example');
#+++    my @example_params = GetParameterValues('examples', $namespace, '[,;\s]+', $allow_file);
#+++    if (...) {Throw('error' => 'I got this error message!', 'log' => 'and that is for log only');}
#+++    # example of page rendering (see also minimal exemple in
#+++    # RenderFunctionToCall):
#+++    return RenderHTMLFullPage(
#+++        {
#+++            'mime'                => 'HTML',
#+++            'header_args'         => ['-cookie' => $cookie],
#+++            'css'                 => ['custom.autocomplete'],
#+++            'remove_css'          => ['jquery.autocomplete'],
#+++            'less'                => ['greenphyl3'],
#+++            'remove_less'         => ['greenphyl'],
#+++            'javascript'          => ['ipr?no_autoload=1'],
#+++            'remove_javascript'   => ['swfobject'],
#+++            'title'               => 'Hello World',
#+++            'before_content'      => '<img class="centered" src="http://www.greenphyl.fr/images/header_line.png"/>',
#+++            'dump_data'           =>
#+++                {
#+++                   'links' => [
#+++                     {
#+++                       'label'       => 'FASTA',
#+++                       'format'      => 'fasta',
#+++                       'object_type' => 'sequences',
#+++                       'file_name'   => 'sequences.fasta',
#+++                       'fields'      => 'seq_id',
#+++                     },
#+++                     {
#+++                       'label'       => 'Excel',
#+++                       'format'      => 'excel',
#+++                       'object_type' => 'sequences',
#+++                       'file_name'   => 'sequences.xls',
#+++                       'fields'      => 'seq_id',
#+++                     },
#+++                     {
#+++                       'label'       => 'CSV',
#+++                       'format'      => 'csv',
#+++                       'object_type' => 'sequences',
#+++                       'file_name'   => 'sequences.csv',
#+++                       'fields'      => 'seq_id',
#+++                     },
#+++                   ],
#+++                   'mode' => 'form',
#+++                   'parameters' => { 'seq_id' => 'At1g14920.1', },
#+++                 },
#+++            'pager_data'          =>
#+++                GeneratePagerData(
#+++                    {
#+++                        'total_entries' => $total_entry_count,
#+++                        'entry_ranges_label' => 'Items per page:',
#+++                    },
#+++                ),
#+++            'before_form_content' => 'pre_form.tt',
#+++            'content'             => 'full_template.tt',
#+++            'form_data'          =>
#+++                {
#+++                    'identifier' => 'my_form',
#+++                    'action'     => 'submitter.cgi',
#+++                    'method'     => 'POST',
#+++                    'enctype'    => 'application/x-www-form-urlencoded',
#+++                    'submit'     => 'Go go go!', #+++ note: submit name will be "my_form_submit"
#+++                    'nosubmit'   => 0,
#+++                    'noclear'    => 1,
#+++                },
#+++            'after_form_content'  => '<div id="from_ajax_result_container"></div>',
#+++            'after_content'       => 'pre_footer.tt',
#+++            'param1'             => 'val1',
#+++            'param2'             => 'val2',
#+++            'param3'             => 'val3',
#+++        },
#+++    );
#+++
#+++}


=pod

=head2 [RenderFunctionToCall] #+++

B<Description>: [function description]. #+++

B<ArgsCount>: 0

B<Return>: (string)

Returns an HTML string containing the GreenPhyl page for ... #+++

=cut

#+++sub [RenderFunctionToCall]
#+++{
#+++    #+++ do some stuff...
#+++    return RenderHTMLFullPage(
#+++        {
#+++            'title'             => 'Page title', #+++
#+++            'content'           => 'template_file.tt', #+++
#+++            #+++ add parameters
#+++        },
#+++    );
#+++}




# Script options
#################

=pod

=head1 OPTIONS

#--- describes GET/POST parameters given to the CGI script

=head2 Parameters

=over 4

=item B<[option_name]> ([option nature]): #+++

[option description]. #+++
Default: [option default value if one] #+++

=back
#--- Example:
#---
#--- No parameters requiered or parameters given using POST method.
#---
#--- =over 4
#---
#--- =item B<login> (string):
#---
#--- The user login.
#---
#--- =item B<tracker> (integer):
#---
#--- A tracker used to retrive the data element to display.
#---
#---=back

=cut


# CODE START
#############

# hash for function selection.
my $actions =
    {
        ''             => \&DefaultRenderFunctionToCall, #+++
        '<action_name>' => \&RenderFunctionToCall, #+++
        '<some_other_action>' => {
            ''                            => \&DefaultRenderFunctionToCall, #+++
            '<sub_action_as_submit_name>' => \&RenderFunctionToCall, #+++
        },
    };

# try block
eval
{
    # display page
    print ProceedPage($actions);
};

HandleErrors();

exit(0);

__END__
# CODE END
###########

=pod

=head1 DIAGNOSTICS

#--- Give and explain here every error message the the script may generate.
#--- Use: (W)=warning (optional), (D)=deprecation (optional),
#---      (S)=severe warning (mandatory), (F)=fatal error (trappable),
#---      (X)=very fatal error (non-trappable).
#--- example:
#---
#--- =over 4
#---
#--- =item *
#---
#--- Negative radius;
#--- (F) Can not draw a circle with a negative radius.
#---
#---=back #+++

=head1 AUTHORS

Mathieu ROUARD (Bioversity), m.rouard@cgiar.org

Valentin GUIGNON (Bioversity), v.guignon@cgiar.org

Matthieu CONTE (Syngenta)

Sebastien BRIOIS (Syngenta)

=head1 VERSION

Version [version.subversion.build] #+++

Date [DD/MM/YYY] #+++

=head1 SEE ALSO

#--- other documentation or objects related to this package
GreenPhyl documentation.

=cut
