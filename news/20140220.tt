[%~ 
    title = "Release Notes of version 4" 
    date  = "20/02/2014" 
~%] 
[%~ news = BLOCK ~%]
<div>
  <p>We
    are pleased to announce a new  release of  GreenPhylDB (version 4)</p>
  <h2>Database content</h2>
  <br />
  The following genomes were
  added:
  <ol start="1" type="1">
    <li>Amborella trichopoda v1 </li>
    <li>Cajanus cajan v1</li>
    <li>Cicer arietinum 	v1</li>
    <li>Citrus sinensi
      s 	v1 </li>
    <li>Elaeis guineensis v1 </li>
    <li>Gossypium raimondii v1 </li>
    <li>Hordeum vulgare v1</li>
    <li>Lotus japonicus v2.5</li>
    <li>Musa balbisiana
      v1</li>
    <li>Phaseolus vulgaris v1 </li>
    <li>Picea abies v1 </li>
    <li>Setaria
      italica v1 </li>
    <li>Solanum lycopersicum 	v2.4</li>
    <li>Solanum tuberosum
      v3</li>
  </ol>
  <p>We also updated the following  versions:</p>
  <ol>
    <li>Oryza
      sativa ( v6 to v7)</li>
    <li>Ostreococcus tauri (v2 to v4)</li>
    <li>Vitis
      vinifera (8x to 12x) </li>
  </ol>
  <p>Sources and versions are listed  in the <a
href="documentation.cgi?page=datasource">documentation section</a>.<br />
    <br />
    <strong>Phylogenetic analyses and homology predictions</strong> </p>
  <p>As for the
    previous versions, phylogenetic analyses and Reciprocical Blast Hits were
    performed. 5545 gene trees out of the 8347 clusters at level 1 are available.
    This number is lower than for the previous version and this is due to two main
    reasons. First, with addtional genomes the size of gene families is increasing
    drastically and calculation are more and more computer intensive. Second,
    protein annotations are increasingly heterogeneous in term of annotations
    quality, leading to multiple alignments that did not pass our quality filters
    and therefore phylogeny have not been launched because the orthology predictions
    would not be reliable enough. We plan to adress this issue and will expand the
    number of gene families with gene tree analyses.For each computed gene tree, we now provide 
    statitics on the alignment, filtration, and evolution of the gene tree.</p>
  <p>Further statistics about the database are available in the <a href="statistics.cgi">statistics section</a></p>
</div>
<div>
  <h2>New <a href="tools.cgi">tools</a> and functionalities</h2>
  <h3>Gene tree display</h3>
  <p>It is now possible to filter the species displayed in a gene tree but keeping into account the phylogeny performed for the whole set of species. This feature is particularly useful for big genes trees and/or if you are only interested in a subset of species.<br /></p>
  <br />
  <img src="../img/tutorials/treeFilter_small.jpg" alt="treeFilter" width="800" height="600" />
  <h3><a href="treepattern.cgi">TreePattern</a></h3>
  <p>A new way to explore phylogenetic trees. Define your evoltion pattern and look at the matching trees. <br /></p>
  <img src="../img/tutorials/treePattern_small.jpg" alt="treepattern" width="800" height="600" />
  <h3><a href="custom_family.cgi?p=create">Custom families</a> </h3>
  <p>We developed a user-friendly interface allowing either to customize pre-computed protein sequence clusters or to create new ones based on prior knowledge of a given gene family. Information can be then shared with collaborators and/or reviewers with a unique URL. We will therefore an addtional <a href="http://greenphyl-dev.cirad.fr/cgi-bin/custom_families.cgi?p=list&amp;level=0%2C1&amp;no_gp=1&amp;mmode=full">gene family list section</a> containing customised gene families by users who decide to share it publically. This tool intends also to support publication process.<br /></p>

    <img src="../img/tutorials/custom_family_small.jpg" alt="Custom families" width="800" height="600" /><br />
    
<h3><a href="go_browser.cgi">Gene ontology Browser</a></h3>
<p>It now possible to browse the full Gene Ontology, which used to be restricted to PlantSlim terms.</p>
  </li>
</div>

<br />
Should you have any questions, please do not hesitate to <a href="mailto:greenphyldb@cirad.fr">contact us</a>.&nbsp;Please note that the version 1, 2 and 3 remain accessible at the following  address:<br />
<a href="http://www.greenphyl.org/">http://www.greenphyl.org</a><br />
</p>
<p><strong>The GreenPhyl team</strong></p>

[%~ END ~%]