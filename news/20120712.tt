[%~
  title = "Release Notes of version 3"
  date  = "12/07/2012"
~%]
[%~ news = BLOCK ~%]

  We are pleased to announce the release of a new version of GreenPhyl.<br/>
  <br/>
  <b>Database content</b><br/>
  For this version some genomes were updated&nbsp; and 6 genomes were added:<br/>
  <ol>
    <li>Musa acuminata</li>
    <li>Theobroma cacao</li>
    <li>Manihot esculenta</li>
    <li>Phoenix dactylifera </li>
    <li>Cucumis sativus</li>
    <li>Malus Domestica</li>
  </ol>
  The source and the version of the sequences are listed in the <a href="http://www.greenphyl.org/v3/cgi-bin/documentation.cgi?page=datasource">documentation section</a>.<br/>
  <br/>
  <b>Gene family Annotation</b><br/>
  Since the version 2, more than <a href="http://www.greenphyl.org/v3/cgi-bin/stats.cgi">1500 additional</a> clusters were annotated.<br/>
  <br/>
  <b>Website</b><br/>
  The website was overhauled and redesigned using advanced Ajax and CSS technologies to offer better user-friendliness. <br/>
  <br/>
  <b>New tools </b><br/>
  <ul>
    <li><a href="http://www.greenphyl.org/v3/cgi-bin/quick_search.cgi">Quick search</a>: A new way to explore the database based on identifiers or keywords.</li>
    <li><a href="http://www.greenphyl.org/v3/cgi-bin/ipr2genomes.cgi">IPR combination tool</a>:&nbsp; It allows to create you own clustering of sequenced based on combinations of Interpro domains.</li>
  </ul>
  <b>Phylogenetic-based Pipeline </b><br/>
  Our pipeline&nbsp; has been modified. The masking of the multiple alignments is now performed by Trimal and the gene tree reconcilation is ensured by RAP-Green. The bootstrap procedure was replaced by the aLRT provided by PhyML. More details are available in the documentation.<br/>
  Approximately <a href="http://www.greenphyl.org/v3/cgi-bin/stats.cgi">7000 gene families</a> were analysed.<br/>
  <br/>
  Please note that the <a href="http://greenphyl.cirad.fr/v2/cgi-bin/index.cgi">version 2 will remain accessible</a>. The version 3 is also accessible at the following address:<br/>
  <a href="http://www.greenphyl.org/v3/cgi-bin/index.cgi">http://www.greenphyl.org/v3/</a><br/>
  <br/>
  Should you have any questions, please do not hesitate to <a href="mailto:greenphyldb@cirad.fr">contact us</a>.&nbsp; <br/>
  <br/>

[%~ END ~%]
