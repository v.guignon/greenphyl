GreenPhylDB
===========

CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Installation
 * Configuration
 * Maintainers
 * Sponsors & Partnership


INTRODUCTION
------------

GreenPhyl is a project that provides web resource designed for comparative and
functional genomics in plants. GreenPhyl databases contain catalogues of gene
families based on gene predictions of genomes, covering a broad taxonomy of
green plants. Result of our automatic clustering is manually annotated and
analyzed by a phylogenetic-based approach to predict homologous relationships.
It supports evolution and functional studies to identify candidate gene
affecting agronomic traits in crops.


REQUIREMENTS
------------

 * Required Perl modules
   GreenPhyl has been developed in Perl5 under UNIX and requires that various
   Perl modules be installed beforehand.  Please note that all these modules are
   available via the CPAN website (http://search.cpan.org/) or on the Bioperl
   website (http://www.bioperl.org/wiki/Installing_BioPerl):
   - DBI (version >= 4.006 for procedure calls)
   - DBIx::Simple
   - CGI
   - CGI::Session
   - GD
   - BioPerl
   - Template
   - JSON
   - File::Temp
   - Tie::IxHash 
   - Statistics::Descriptive
   - LWP::Simple
   - Data::SpreadPagination
   - Data::Tabular::Dumper
   - URI::Escape
   - List::Compare
   - XML::Simple
   - XML::LibXML::Reader
   - XML::LibXML
   - XML::DOM
   - LockFile::Simple
   - Proc::Background
   - File::Slurp 
   - Text::CSV_XS
   - GO::Parser

   It is necessary to patch 2 files in BioPerl that are provided with the
   GreenPhyl package:
   - Bio::TreeIO::phyloxml
   - Bio::Annotation::Relation

   Installing a new module can be as simple as typing
   `perl -MCPAN -e 'install  List::Compare'`.

 * Other programs
   - Blast
   - Graphviz (http://www.graphviz.org/)
   - MetaMeme (http://meme.sdsc.edu/)
   - HMMER (http://hmmer.janelia.org/)
   - Libxml2 (http://www.xmlsoft.org/)

 * Disk space
  The full installation (with scripts & data) takes about 20 Gb. Data files take
  a huge part of it with for instance 8 Go only for HMM files.


INSTALLATION
------------

 * Get the source code (git clone);
 * Setup a database using the SQL scripts in "website/admin": gp_mysql_schema.sql + gp_mysql_procedures.sql + gp_mysql_triggers.sql;
 * Copy the content of "greenphyl_instance" somewhere and customize it;
 * Merge your copy of "greenphyl_instance" into your "website" directory;
 * Do the appropriate setup on your server in order to have the "website/htdocs" as your web server root and serve the Perl CGI scripts stored in the "website/cgi-bin" as the "/cgi-bin" directory of your site.


CONFIGURATION
-------------
Edit `/website/local_lib/Greenphyl/Config.pm` and `/website/local_lib/Greenphyl/URLConfig.pm`.


MAINTAINERS
-----------

Valentin Guignon, v.guignon@cgiar.org
Mathieu Rouard, m.rouard@cgiar.org


SPONSORS & PARTNERSHIP
----------------------

Bioversity International
CIRAD
Syngenta
